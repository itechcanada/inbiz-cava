﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTECH.InbizERP.BusinessLogic
{
    [Serializable]
    public class Accommodation
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public List<AccommodationItem> Rooms { get; set; }
        public bool IsSpecial { get; set; }

       
    }

    [Serializable]
    public class AccommodationItem
    {
        public int ItemID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int NoOfBeds { get; set; }
        public List<string> Amenities { get; set; }
        public double Price { get; set; }
    }
}

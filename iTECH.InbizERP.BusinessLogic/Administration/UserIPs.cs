﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class UserIPs
    {

        public string IPAddress { get; set; }
        public int IPAddressID { get; set; }
        public int UserID { get; set; }
        public int CreatedByUserID { get; set; }
       



        public Boolean Save(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (this.IPAddressID > 0)
                {
                    string sqlUpdate = " UPDATE UserIPs SET  IPAddress = @IPAddress, lastUpdatedBy = @lastUpdatedBy, LastUpdatedOn = @LastUpdatedOn WHERE UserID = @UserID  ";
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("UserID", this.UserID, MyDbType.Int),
                    DbUtility.GetParameter("IPAddress", this.IPAddress, MyDbType.String),
                    DbUtility.GetParameter("lastUpdatedBy",this.CreatedByUserID , MyDbType.Int),
                    DbUtility.GetParameter("LastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    //DbUtility.GetParameter("idUserIPs", this.IPAddressID, MyDbType.Int),
                    });
                }
                else
                {
                    string sql = " INSERT INTO UserIPs (UserID, IPAddress, CreatedBy, CreatedOn) VALUES (@UserID, @IPAddress, @CreatedBy, @CreatedOn) ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("UserID", this.UserID, MyDbType.Int),
                    DbUtility.GetParameter("IPAddress", this.IPAddress, MyDbType.String),
                    DbUtility.GetParameter("CreatedBy",this.CreatedByUserID , MyDbType.Int),
                    DbUtility.GetParameter("CreatedOn", DateTime.Now, MyDbType.DateTime)
                    });
                }
                this.IPAddressID = dbHelp.GetLastInsertID();
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean Delete(DbHelper dbHelp, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (this.IPAddressID > 0)
                {
                    string sqlUpdate = " UPDATE UserIPs SET IsActive = 1  lastUpdatedBy = @lastUpdatedBy, LastUpdatedOn = @LastUpdatedOn WHERE idUserIPs = @idUserIPs  ";
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("lastUpdatedBy",this.CreatedByUserID , MyDbType.Int),
                    DbUtility.GetParameter("LastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("idUserIPs", this.IPAddressID, MyDbType.Int),
                    });
                }
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<UserIPs> GetIPsList(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<UserIPs> lResult = new List<UserIPs>();
                string sql = "SELECT idUserIPs, UserID, IPAddress FROM UserIPs WHERE IsActive = 1  ";
                if (this.IPAddressID > 0)
                {
                    sql += " AND idUserIPs = " + this.IPAddressID;
                }

                if (this.UserID > 0)
                {
                    sql += " AND UserID = " + this.UserID;
                }
                sql += " ORDER BY idUserIPs Desc ";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new UserIPs { IPAddressID = BusinessUtility.GetInt(dr["idUserIPs"]), UserID = BusinessUtility.GetInt(dr["UserID"]), IPAddress = BusinessUtility.GetString(dr["IPAddress"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}

﻿#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql; 
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;
#endregion
#region "Modify Version "
/**********************************
 * Modified By   :: Ankit Choure
 * Modified Date :: 13-JULY-2011
 * ********************************/
#endregion
namespace iTECH.InbizERP.BusinessLogic
{
    public class SysTaxCode
    {
        public int sysTaxCode { get; set; }
        public string sysTaxDesc { get; set; }
        public double sysTaxPercentage { get; set; }
        public int sysTaxSequence { get; set; }
        public bool sysTaxOnTotal { get; set; }
        public string TaxOnTotalTaxCode { get; set; }
       
        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sqlCheck = "SELECT COUNT(*) FROM systaxcode WHERE sysTaxCode=@sysTaxCode AND sysTaxDesc=@sysTaxDesc";
                string sql = "INSERT INTO systaxcode(sysTaxCode,sysTaxDesc,sysTaxPercentage,sysTaxSequence,sysTaxOnTotal,TaxOnTotalTaxCode)";
                sql += " VALUES(@sysTaxCode,@sysTaxDesc,@sysTaxPercentage,@sysTaxSequence,@sysTaxOnTotal,@TaxOnTotalTaxCode)";
                object val = dbHelp.GetValue(sqlCheck, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysTaxCode", this.sysTaxCode, MyDbType.String),
                    DbUtility.GetParameter("sysTaxDesc", this.sysTaxDesc, MyDbType.String)
                });
                if (BusinessUtility.GetInt(val) > 0)
                {
                    throw new Exception("Tax Code Already Exists");
                }
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysTaxCode", this.sysTaxCode, MyDbType.String),
                    DbUtility.GetParameter("sysTaxDesc", this.sysTaxDesc, MyDbType.String), 
                    DbUtility.GetParameter("sysTaxPercentage", this.sysTaxPercentage, MyDbType.Double), 
                    DbUtility.GetParameter("sysTaxSequence", this.sysTaxSequence, MyDbType.Int), 
                    DbUtility.GetParameter("sysTaxOnTotal", this.sysTaxOnTotal, MyDbType.Boolean), 
                    DbUtility.GetParameter("TaxOnTotalTaxCode", this.TaxOnTotalTaxCode, MyDbType.String)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "UPDATE systaxcode SET sysTaxPercentage=@sysTaxPercentage,sysTaxSequence=@sysTaxSequence,sysTaxOnTotal=@sysTaxOnTotal,TaxOnTotalTaxCode=@TaxOnTotalTaxCode";
                sql += " WHERE sysTaxCode=@sysTaxCode AND sysTaxDesc=@sysTaxDesc";                                
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysTaxCode", this.sysTaxCode, MyDbType.String),
                    DbUtility.GetParameter("sysTaxDesc", this.sysTaxDesc, MyDbType.String), 
                    DbUtility.GetParameter("sysTaxPercentage", this.sysTaxPercentage, MyDbType.Double), 
                    DbUtility.GetParameter("sysTaxSequence", this.sysTaxSequence, MyDbType.Int), 
                    DbUtility.GetParameter("sysTaxOnTotal", this.sysTaxOnTotal, MyDbType.Boolean), 
                    DbUtility.GetParameter("TaxOnTotalTaxCode", this.TaxOnTotalTaxCode, MyDbType.String)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }            
        }

        public void Delete(DbHelper dbHelp, int txGroup, string txCode)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "DELETE FROM systaxcode";
                sql += " WHERE sysTaxCode=@sysTaxCode AND sysTaxDesc=@sysTaxDesc";
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysTaxCode", txGroup, MyDbType.Int),
                    DbUtility.GetParameter("sysTaxDesc", txCode, MyDbType.String)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            } 
        }

        public bool PopulateObject(DbHelper dbHelp, int txGroup, string txCode)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            bool retVal = false;
            try
            {
                string sql = "SELECT * FROM systaxcode ";
                sql += " WHERE sysTaxCode=@sysTaxCode AND sysTaxDesc=@sysTaxDesc";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysTaxCode", txGroup, MyDbType.Int),
                    DbUtility.GetParameter("sysTaxDesc", txCode, MyDbType.String)
                }))
                {
                    if (dr.Read())
                    {
                        retVal = true;
                        this.sysTaxCode = BusinessUtility.GetInt(dr["sysTaxCode"]);
                        this.sysTaxDesc = BusinessUtility.GetString(dr["sysTaxDesc"]);
                        this.sysTaxOnTotal = BusinessUtility.GetBool(dr["sysTaxOnTotal"]);
                        this.sysTaxPercentage = BusinessUtility.GetDouble(dr["sysTaxPercentage"]);
                        this.sysTaxSequence = BusinessUtility.GetInt(dr["sysTaxSequence"]);
                        this.TaxOnTotalTaxCode = BusinessUtility.GetString(dr["TaxOnTotalTaxCode"]);
                    }
                    return retVal;
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool PopulateObject(int txGroup, string txCode)
        {
            DbHelper dbHelp = new DbHelper();
            dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            bool retVal = false;
            try
            {
                string sql = "SELECT * FROM systaxcode ";
                sql += " WHERE sysTaxCode=@sysTaxCode AND sysTaxDesc=@sysTaxDesc";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysTaxCode", txGroup, MyDbType.Int),
                    DbUtility.GetParameter("sysTaxDesc", txCode, MyDbType.String)
                }))
                {
                    if (dr.Read())
                    {
                        retVal = true;
                        this.sysTaxCode = BusinessUtility.GetInt(dr["sysTaxCode"]);
                        this.sysTaxDesc = BusinessUtility.GetString(dr["sysTaxDesc"]);
                        this.sysTaxOnTotal = BusinessUtility.GetBool(dr["sysTaxOnTotal"]);
                        this.sysTaxPercentage = BusinessUtility.GetDouble(dr["sysTaxPercentage"]);
                        this.sysTaxSequence = BusinessUtility.GetInt(dr["sysTaxSequence"]);
                        this.TaxOnTotalTaxCode = BusinessUtility.GetString(dr["TaxOnTotalTaxCode"]);
                    }
                    return retVal;
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public DataTable GetAllTaxes(DbHelper dbHelp, int taxGroup, string searchText)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT CONCAT(CAST(tc.sysTaxCode AS CHAR), '~', tc.sysTaxDesc) AS ID, tg.sysTaxCodeDescID AS TaxGroupID, tg.sysTaxCodeDescText AS TaxGroupDesc, tc.sysTaxDesc AS TaxCode, tc.sysTaxPercentage,tc.sysTaxSequence,tc.sysTaxOnTotal,tc.TaxOnTotalTaxCode FROM systaxcode tc INNER JOIN systaxcodedesc tg ON tg.sysTaxCodeDescID=tc.sysTaxCode";
                sql += " WHERE (tc.sysTaxCode=@sysTaxCode OR @sysTaxCode = 0)";
                if (!string.IsNullOrEmpty(searchText))
                {
                    sql += " AND (tc.sysTaxDesc LIKE CONCAT('%', @SearchText ,'%') OR tg.sysTaxCodeDescText LIKE CONCAT('%', @SearchText ,'%'))";
                    sql += " ORDER BY tg.sysTaxCodeDescID, tc.sysTaxSequence";
                    return dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("sysTaxCode", taxGroup, MyDbType.Int),
                        DbUtility.GetParameter("SearchText", searchText, MyDbType.String)
                    });
                }
                sql += " ORDER BY tg.sysTaxCodeDescID, tc.sysTaxSequence";
                return dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysTaxCode", taxGroup, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<SysTaxCode> GetAllTaxes(DbHelper dbHelp, int taxGroup)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<SysTaxCode> lResult = new List<SysTaxCode>();
            try
            {
                string sql = "SELECT * FROM systaxcode ";
                sql += " WHERE sysTaxCode=@sysTaxCode ORDER BY sysTaxSequence";
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysTaxCode", taxGroup, MyDbType.Int)
                }))
                {
                    while (dr.Read())
                    {
                        SysTaxCode tc = new SysTaxCode();
                        tc.sysTaxCode = BusinessUtility.GetInt(dr["sysTaxCode"]);
                        tc.sysTaxDesc = BusinessUtility.GetString(dr["sysTaxDesc"]);
                        tc.sysTaxOnTotal = BusinessUtility.GetBool(dr["sysTaxOnTotal"]);
                        tc.sysTaxPercentage = BusinessUtility.GetDouble(dr["sysTaxPercentage"]);
                        tc.sysTaxSequence = BusinessUtility.GetInt(dr["sysTaxSequence"]);
                        tc.TaxOnTotalTaxCode = BusinessUtility.GetString(dr["TaxOnTotalTaxCode"]);
                        lResult.Add(tc);
                    }
                    return lResult;
                }
            }
            catch 
            {
                
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        #region Dropdown helper functions 
        public void FillListControl(DbHelper dbHelp, int taxGroup, ListControl lCtrl, ListItem rootItem)
        {
            try
            {
                lCtrl.DataSource = this.GetAllTaxes(dbHelp, taxGroup, string.Empty);
                lCtrl.DataTextField = "TaxCode";
                lCtrl.DataValueField = "TaxCode";
                lCtrl.DataBind();               
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch 
            {                
                throw;
            }
        }
        #endregion

        //public string GetFilterQueryBySearchCriteria(ParameterCollection pCol, string SearchData)
        //{
        //    pCol.Clear();
        //    string strSQL = "SELECT sysTaxCode, sysTaxCodeDescText, sysTaxDesc, sysTaxPercentage, sysTaxSequence, sysTaxOnTotal FROM systaxcode inner join systaxcodedesc on sysTaxCodeDescID=sysTaxCode  WHERE 1=1 ";
        //    if (!string.IsNullOrEmpty(SearchData))
        //    {
        //        strSQL += " And sysTaxCode LIKE CONCAT('%', @SearchData, '%') or sysTaxCodeDescText LIKE CONCAT('%', @SearchData, '%')  ";
        //        pCol.Add("@SearchData", SearchData);
        //    }
        //    strSQL += " order by sysTaxCode, sysTaxDesc, sysTaxSequence ";
        //    return strSQL;
        //}
    }
}

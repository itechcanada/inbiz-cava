﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web.UI.WebControls;
using System.Web;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class SysProcessGroup
    {       
        #region Property
        public int ProcessID { get; set; }
        public string ProcessCode { get; set; }
        public string ProcessDescription { get; set; }
        public double ProcessFixedCost { get; set; }
        public double ProcessCostPerHour { get; set; }
        public double ProcessCostPerUnit { get; set; }
        #endregion

        #region Functions
        public string GetSql(ParameterCollection pCol, string searchText)
        {
            pCol.Clear();
            string strSQL = "SELECT ProcessID, ProcessCode, ProcessDescription, ProcessFixedCost, ProcessCostPerHour, ProcessCostPerUnit FROM sysprocessgroup WHERE 1=1 ";
            if (!string.IsNullOrEmpty(searchText))
            {
                strSQL += " And ProcessCode LIKE CONCAT('%', @SearchData, '%') or ProcessDescription LIKE CONCAT('%', @SearchData, '%')  ";
                pCol.Add("@SearchData", searchText);
            }
            strSQL += " order by ProcessDescription ";
            return strSQL;
        }

        public void PopulateObject(int processID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.PopulateObject(dbHelp, processID);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(DbHelper dbHelp, int processID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQL = "SELECT ProcessID, ProcessCode, ProcessDescription, ProcessFixedCost, ProcessCostPerHour, ProcessCostPerUnit FROM sysprocessgroup where ProcessID= @ProcessID";
            MySqlParameter[] p = { 
                                    new MySqlParameter("@ProcessID", processID)
                                 };
            MySqlDataReader drObj = null;
            try
            {

                using (drObj = dbHelp.GetDataReader(strSQL, System.Data.CommandType.Text, p))
                {
                    if (drObj.Read())
                    {
                        this.ProcessCostPerHour = BusinessUtility.GetDouble(drObj["ProcessCostPerHour"]);
                        this.ProcessCostPerUnit = BusinessUtility.GetDouble(drObj["ProcessCostPerUnit"]);
                        this.ProcessFixedCost = BusinessUtility.GetDouble(drObj["ProcessFixedCost"]);
                        this.ProcessID = BusinessUtility.GetInt(drObj["ProcessID"]);
                        this.ProcessDescription = BusinessUtility.GetString(drObj["ProcessDescription"]);
                        this.ProcessCode = BusinessUtility.GetString(drObj["ProcessCode"]);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Insert the Process Group Information 
        /// </summary>
        /// <returns>if -1 then Proces Code Exists in Data base if -2 then Proces Description  </returns>
        public void Insert()
        {
            DbHelper dbHelper = new DbHelper(true);
            // Check Process Code Exists or not
            string sqlProcesCode = "Select Count(ProcessDescription) From sysprocessgroup  WHERE  ProcessCode= @ProcessCode";

            // Check Process Code Exists or not
            string sqlProcessDis = "Select Count(ProcessCode) From sysprocessgroup  WHERE  ProcessDescription= @ProcessDescription";


            string sql = "Insert sysprocessgroup (ProcessCode, ProcessDescription, ProcessFixedCost, ProcessCostPerHour, ProcessCostPerUnit) Value ( ";
            sql += "@ProcessCode, @ProcessDescription, @ProcessFixedCost, @ProcessCostPerHour, @ProcessCostPerUnit)";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("ProcessCode", this.ProcessCode,typeof(string)),
                                     DbUtility.GetParameter("ProcessDescription", this.ProcessDescription,typeof(string)),
                                     DbUtility.GetParameter("ProcessFixedCost", this.ProcessFixedCost,typeof(double)),
                                     DbUtility.GetParameter("ProcessCostPerHour", this.ProcessCostPerHour,typeof(double)),
                                     DbUtility.GetParameter("ProcessCostPerUnit", this.ProcessCostPerUnit,typeof(double))
                                 };
            try
            {

                object objScalr1 = dbHelper.GetValue(sqlProcesCode, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ProcessCode", this.ProcessCode, typeof(string)) });
                if (BusinessUtility.GetInt(objScalr1) > 0)
                {
                    throw new Exception(CustomExceptionCodes.PROCESS_CODE_ALREADY_EXISTS);
                }


                object objScalr2 = dbHelper.GetValue(sqlProcessDis, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ProcessDescription", this.ProcessDescription, typeof(string)) });
                if (BusinessUtility.GetInt(objScalr2) > 0)
                {
                    throw new Exception(CustomExceptionCodes.PROCESS_DESCRIPTION_ALREADY_EXISTS);
                }

                dbHelper.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);
                this.ProcessID = dbHelper.GetLastInsertID();                
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }
        /// <summary>
        /// Update the Process Group Information 
        /// </summary>
        /// <returns></returns>
        public bool Update()
        {
            string sql = "UPDATE sysprocessgroup set ProcessCode=@ProcessCode, ProcessDescription=@ProcessDescription, ";
            sql += "ProcessFixedCost=@ProcessFixedCost, ProcessCostPerHour=@ProcessCostPerHour, ProcessCostPerUnit=@ProcessCostPerUnit";
            sql += " Where ProcessID=@ProcessID";

            string sqlExistProcessCode = "SELECT COUNT(*) FROM sysprocessgroup WHERE ProcessCode=@ProcessCode AND ProcessID<>@ProcessID";
            DbHelper dbHelper = new DbHelper();
            try
            {
                object val = dbHelper.GetValue(sqlExistProcessCode, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ProcessID", this.ProcessID,typeof(int)),
                    DbUtility.GetParameter("ProcessCode", this.ProcessCode,typeof(string))
                });

                if (BusinessUtility.GetInt(val) > 0)
                {
                    throw new Exception(CustomExceptionCodes.PROCESS_CODE_ALREADY_EXISTS);
                }

                dbHelper.ExecuteNonQuery(sql, System.Data.CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ProcessID", this.ProcessID,typeof(int)),
                    DbUtility.GetParameter("ProcessCode", this.ProcessCode,typeof(string)),
                    DbUtility.GetParameter("ProcessDescription", this.ProcessDescription,typeof(string)),
                    DbUtility.GetParameter("ProcessFixedCost", this.ProcessFixedCost,typeof(double)),
                    DbUtility.GetParameter("ProcessCostPerHour", this.ProcessCostPerHour,typeof(double)),
                    DbUtility.GetParameter("ProcessCostPerUnit", this.ProcessCostPerUnit,typeof(double))
                });
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public bool Delete(int processID)
        {
            string strDelete = "DELETE FROM sysprocessgroup WHERE ProcessID=@ProcessID;";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(strDelete, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ProcessID", processID, MyDbType.Int) });
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateShippingService(DbHelper dbHelp, int processId)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                this.PopulateObject(dbHelp, processId);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

        #region Sivananda's Methods

        /// <summary>
        /// Key: ProcessID, Value: ProcessDesc with price
        /// </summary>
        /// <param name="curCode"></param>
        /// <returns></returns>
        public Dictionary<int, string> GetProcesses(string curCode)
        {
            string sql = "SELECT * FROM sysprocessgroup WHERE IsWebEnabled=1";
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            Dictionary<int, string> dic = new Dictionary<int, string>();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, null);
                while (dr.Read())
                {
                    dic[BusinessUtility.GetInt(dr["ProcessID"])] = string.Format("{0}-{1}-{2}{3:F}", dr["ProcessCode"], dr["ProcessDescription"], curCode, dr["ProcessFixedCost"]);
                }
                return dic;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public object GetServicesForWeb(string curCode)
        {
            List<object> lResult = new List<object>();

            string sql = string.Format("SELECT p.productID, p.prdName, p.prdEndUserSalesPrice, p.prdSeq FROM  products p WHERE (p.prdType = {0}) AND (p.prdIsActive = 1)", (int)StatusProductType.ServiceProduct);
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;            
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, null);
                while (dr.Read())
                {
                    var v = new { ProcessID = BusinessUtility.GetInt(dr["productID"]), ProcessDesc = string.Format("{0}-({1}{2:F})", dr["prdName"], curCode, dr["prdEndUserSalesPrice"]) };                    
                    //dic[BusinessUtility.GetInt(dr["ProcessID"])] = string.Format("{0}-{1}-{2}{3:F}", dr["ProcessCode"], dr["ProcessDescription"], curCode, dr["ProcessFixedCost"]);
                    lResult.Add(v);
                }
                return lResult;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public IDataReader GetReserverdServicesForWeb(DbHelper dbHelp, int orderID, string curCode)
        {
            Dictionary<int, string> lResult = new Dictionary<int, string>();

            string sql = string.Format("SELECT p.productID, p.prdName, oi.ordProductUnitPrice, p.prdSeq, oi.ordProductQty FROM  products p INNER JOIN orderitems oi ON oi.ordProductID=p.ProductID WHERE (p.prdType = {0}) AND (p.prdIsActive = 1) AND oi.ordID={1}", (int)StatusProductType.ServiceProduct, orderID);                        
            try
            {
                return dbHelp.GetDataReader(sql, CommandType.Text, null);
            }
            catch
            {

                throw;
            }           
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using System.Web.UI.WebControls;
using System.Web;

using iTECH.Library.DataAccess.MySql; 
using iTECH.Library.Utilities;
using System.Data.Odbc;



namespace iTECH.InbizERP.BusinessLogic
{
    public class SysCurrencies
    {
        #region Private Member
        private double _currencyRelativePrice;
        private string _currencyCode;
        private bool _currencyActive;
        #endregion
        #region Property
        public double CurrencyRelativePrice
        {
            get { return _currencyRelativePrice; }
            set { _currencyRelativePrice = value; }
        }
        public string CurrencyCode
        {
            get { return _currencyCode; }
            set { _currencyCode = value; }
        }
        public bool CurrencyActive
        {
            get { return _currencyActive; }
            set { _currencyActive = value; }
        }
        #endregion
        /// <summary>
        /// Get all Currency 
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllCurrency()
        {
            string strSQL = null;
            strSQL = "SELECT CurrencyCode FROM syscurrencies WHERE CurrencyActive='1' order by CurrencyCode ";
            DbHelper dbHelper = new DbHelper();
            try
            {
                return  dbHelper.GetDataTable(strSQL, System.Data.CommandType.Text,null);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }
        /// <summary>
        /// to Fill the Grid View 
        /// </summary>
        /// <returns></returns>
        public string GetSql(ParameterCollection pCol, string searchText)
        {
            pCol.Clear();
            string sql = "SELECT Distinct CurrencyCode, CurrencyRelativePrice, CompanyBasCur FROM syscurrencies, syscompanyinfo Where 1=1";
            if (!string.IsNullOrEmpty(searchText))
            {
                sql += " And CurrencyCode LIKE CONCAT('%', @SearchData, '%')  ";
                pCol.Add("@SearchData", searchText);
            }   
            return sql;
        }
        /// <summary>
        /// Get The currency Information
        /// </summary>
        public void PopulateObject(string currencyCode)
        {
            string strSQL = null;
            strSQL = "SELECT  CurrencyRelativePrice, CurrencyActive FROM syscurrencies where CurrencyCode= @CurrencyCode";
            MySqlParameter[] p = { 
                                    new MySqlParameter("@CurrencyCode", currencyCode)
                                 };
            DbHelper dbHelper = new DbHelper();
            MySqlDataReader drObj = default(MySqlDataReader);
            try
            {

                drObj = dbHelper.GetDataReader(strSQL, System.Data.CommandType.Text, p);
                while (drObj.Read())
                {
                    _currencyRelativePrice = BusinessUtility.GetDouble(drObj["CurrencyRelativePrice"]);
                    _currencyActive = BusinessUtility.GetBool(drObj["CurrencyActive"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
                if (drObj != null && !drObj.IsClosed)
                {
                    drObj.Close();
                }
            }
        }

        
        public OperationResult Insert()
        {
            DbHelper dbHelper = new DbHelper(true);
            
            string sqlExists = "Select Count(CurrencyCode) From syscurrencies WHERE CurrencyCode = @CurrencyCode";
            string sqlInsert = "Insert syscurrencies (CurrencyCode, CurrencyRelativePrice, CurrencyActive) Value ( ";
            sqlInsert += "@CurrencyCode, @CurrencyRelativePrice, @CurrencyActive)";

            try
            {
                object scalarCount = dbHelper.GetValue(sqlExists, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("CurrencyCode", this._currencyCode, typeof(string)) });
                if (BusinessUtility.GetInt(scalarCount) > 0) {
                    return OperationResult.AlreadyExists;
                }

                MySqlParameter[] p = { 
                                     DbUtility.GetParameter("CurrencyCode", this._currencyCode,typeof(string)),
                                     DbUtility.GetParameter("CurrencyRelativePrice", this._currencyRelativePrice,typeof(double)),
                                     DbUtility.GetParameter("CurrencyActive", this._currencyActive,typeof(bool))                                  
                                 };
                dbHelper.ExecuteNonQuery(sqlInsert, System.Data.CommandType.Text, p);
                return OperationResult.Success;
            }
            catch { throw; }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Update the Company Information 
        /// </summary>
        /// <returns></returns>
        public bool Update()
        {
            string sql = "UPDATE syscurrencies set CurrencyRelativePrice=@CurrencyRelativePrice, CurrencyActive=@CurrencyActive";
            sql += " Where CurrencyCode=@CurrencyCode";

            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("CurrencyCode", this._currencyCode,typeof(string)),
                                     DbUtility.GetParameter("CurrencyRelativePrice", this._currencyRelativePrice,typeof(double)),
                                     DbUtility.GetParameter("CurrencyActive", this._currencyActive,typeof(bool))                                  
                                 };
            DbHelper dbHelper = new DbHelper();
            try
            {
                dbHelper.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public bool Delete(string currencyCode) {
            string sqlDelete = "DELETE FROM syscurrencies WHERE CurrencyCode=@CurrencyCode";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("CurrencyCode", currencyCode, MyDbType.String) });
                return true;
            }
            catch
            {
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static double GetRelativePrice(string currency)
        {            
            try
            {
                return GetRelativePrice(null, currency);
            }
            catch
            {
                return 1.00D;
            }            
        }

        public static double GetRelativePrice(DbHelper dbHelp, string currency)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT CurrencyRelativePrice FROM syscurrencies where CurrencyCode=@CurrencyCode";            
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("CurrencyCode", currency, MyDbType.String) });
                return BusinessUtility.GetDouble(val) > 0 ? BusinessUtility.GetDouble(val) : 1.00D;
            }
            catch
            {
                return 1.00D;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        #region Dropdown Helper Functions
        public void FillCurrencyDropDown(DbHelper dbHelp, ListControl lCtrl, int defaultcompID, ListItem rootItem)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT CurrencyCode FROM syscurrencies WHERE CurrencyActive='1' order by CurrencyCode ";            
            try
            {
                lCtrl.DataSource = dbHelp.GetDataTable(sql, System.Data.CommandType.Text, null);
                lCtrl.DataTextField = "CurrencyCode";
                lCtrl.DataValueField = "CurrencyCode";
                lCtrl.DataBind();
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
                if (defaultcompID > 0)
                {
                    SysCompanyInfo ci = new SysCompanyInfo();
                    ci.PopulateObject(defaultcompID, dbHelp);
                    lCtrl.SelectedValue = ci.CompanyBasCur;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class WarehouseSalesGoal
    {
        public string SalesGoalWhsCode { get; set; }
        public int idwarehousesalesgoal { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public double SalesAmountPerDay { get; set; }
        public Boolean Save(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {

                if (this.idwarehousesalesgoal > 0 && !string.IsNullOrEmpty(SalesGoalWhsCode))
                {
                    string sqlUpdate = " UPDATE warehousesalesgoal SET fromDate = @fromDate, toDate = @toDate, salesAmountPerDay = @salesAmountPerDay WHERE warehouseCode = @warehouseCode and idwarehousesalesgoal=@idwarehousesalesgoal ";
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("warehouseCode", this.SalesGoalWhsCode, MyDbType.String),
                    DbUtility.GetParameter("idwarehousesalesgoal", this.idwarehousesalesgoal, MyDbType.Int),
                    DbUtility.GetParameter("fromDate", this.FromDate, MyDbType.DateTime),
                    DbUtility.GetParameter("toDate", this.ToDate, MyDbType.DateTime),
                    DbUtility.GetParameter("salesAmountPerDay", this.SalesAmountPerDay, MyDbType.Double),
                    });

                    Savewarehousessalesgoalperday(this.SalesGoalWhsCode, this.FromDate, this.ToDate, this.SalesAmountPerDay);
                }
                else
                {
                    string sql = " INSERT INTO warehousesalesgoal (fromDate,toDate,salesAmountPerDay,warehouseCode) VALUES (@fromDate,@toDate,@salesAmountPerDay,@warehouseCode) ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("fromDate", this.FromDate, MyDbType.DateTime),
                    DbUtility.GetParameter("toDate", this.ToDate, MyDbType.DateTime),
                    DbUtility.GetParameter("salesAmountPerDay", this.SalesAmountPerDay, MyDbType.Double),
                    DbUtility.GetParameter("warehouseCode", this.SalesGoalWhsCode, MyDbType.String)
                    });

                    Savewarehousessalesgoalperday(this.SalesGoalWhsCode, this.FromDate, this.ToDate, this.SalesAmountPerDay);
                }
                this.idwarehousesalesgoal = dbHelp.GetLastInsertID();
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public DataTable GetSalesGoalList(DbHelper dbHelp, string whsCode, int sSalesGoalID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = " select idwarehousesalesgoal as idUserSalesGoal,fromDate,toDate,salesAmountPerDay,warehouseCode from warehousesalesgoal where warehouseCode ='" + whsCode + "'";
                if (sSalesGoalID > 0)
                {
                    sql += " and idwarehousesalesgoal = " + sSalesGoalID;
                }
                sql += " ORDER BY fromDate DESC";
                DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString(sql), CommandType.Text, null);
                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        private Boolean Savewarehousessalesgoalperday(string sWhsCode, DateTime dtGoalStart, DateTime dtGoalEnd, Double dblSalesPerDay)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                string sqlInsert = "";
                string sqlUpdate = "";
                int iDaysCountToAdd = 0;
                int i = 0;
                DateTime dtLastUserSavedToDate = DateTime.MinValue;
                DateTime dtToAdd;
                string sql = "";

                #region GetUserLastSetGoalDate
                TimeSpan span = (dtGoalEnd).Subtract(dtGoalStart);
                iDaysCountToAdd = (int)span.TotalDays;
                if (iDaysCountToAdd >= 0)
                {
                    i = 0;
                    while (i <= iDaysCountToAdd)
                    {
                        dtToAdd = dtGoalStart.AddDays(i);
                        sql = " SELECT count(*) FROM warehousessalesgoalperday   WHERE  Date(SalesDate) = DATE(@dtToAdd)  AND warehouseCode = @warehouseCode ";
                        object objRtn = dbTransactionHelper.GetValue(sql, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("warehouseCode", sWhsCode, MyDbType.String),
                        DbUtility.GetParameter("dtToAdd", dtToAdd, MyDbType.DateTime)
                        });

                        if (BusinessUtility.GetInt(objRtn) > 0)
                        {
                            sqlUpdate = " UPDATE warehousessalesgoalperday SET  SalesPerDay = CASE  WHEN  SalesPerDay >= " + dblSalesPerDay + " THEN  SalesPerDay ELSE " + dblSalesPerDay + " END   WHERE  Date(SalesDate) = DATE(@dtToAdd) AND warehouseCode = @warehouseCode ";
                            dbTransactionHelper.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("warehouseCode", sWhsCode, MyDbType.String),
                                            DbUtility.GetParameter("dtToAdd", dtToAdd, MyDbType.DateTime),
                                            DbUtility.GetParameter("SalesPerDay", dblSalesPerDay, MyDbType.Double),
                                            });

                        }
                        else
                        {
                            sqlInsert = " INSERT INTO warehousessalesgoalperday (warehouseCode, SalesDate, SalesPerDay ) VALUES (@warehouseCode, @SalesDate, @SalesPerDay) ";
                            dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                                                        DbUtility.GetParameter("warehouseCode", sWhsCode, MyDbType.String),
                                                        DbUtility.GetParameter("SalesDate", dtToAdd, MyDbType.DateTime),
                                                        DbUtility.GetParameter("SalesPerDay", dblSalesPerDay, MyDbType.Double)
                                                        });

                        }
                        i += 1;
                    }
                }

                #endregion
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class SysShippingCompany
    {
        public int ShipCompanyID { get; set; }
        public string ShipCompany { get; set; }
        public string ShipCompanyUrl { get; set; }
        public bool IsActive { get; set; }

        public void PopulateListControl(DbHelper dbHelp, ListControl lCtrl, ListItem rootItem)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "SELECT * FROM z_sys_shipping_company WHERE IsActive=1";
                lCtrl.DataSource = dbHelp.GetDataTable(sql, CommandType.Text, null);
                lCtrl.DataTextField = "ShipCompany";
                lCtrl.DataValueField = "ShipCompanyID";
                lCtrl.DataBind();
                if (rootItem != null)
                {
                    lCtrl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<SysShippingCompany> GetShippingCompany(DbHelper dbHelp, int iShipCompanyID, string sShipCompany)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<SysShippingCompany> lResult = new List<SysShippingCompany>();
                string sql = "SELECT * FROM z_sys_shipping_company WHERE IsActive=1 ";
                if (iShipCompanyID > 0)
                {
                    sql += " AND ShipCompanyID = " + iShipCompanyID;
                }

                if (sShipCompany != "")
                {
                    sql += " AND ShipCompany = '"+  sShipCompany+"' "  ;
                }

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new SysShippingCompany { ShipCompanyUrl = BusinessUtility.GetString(dr["ShipCompanyUrl"]), ShipCompany = BusinessUtility.GetString(dr["ShipCompany"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }

}

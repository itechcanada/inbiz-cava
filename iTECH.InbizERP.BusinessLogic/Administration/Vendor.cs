﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql; 
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;


namespace iTECH.InbizERP.BusinessLogic
{
    public class Vendor
    {
        #region Private Member
        private int _vendorID;

        private string _vendorName;
        private string _vendorEmailID;
        private string _vendorFax;
        private string _vendorPhone;
        private string _vendorPOPref;
        private string _vendorStatus;
        private string _vendorCurrency;
        private string _vendorContactPersonName;
        private string _vendorLang;
        private string _vendorContactPersonPhone;
        private string _vendorContactPersonEmailID;
        
        private bool _vendorActive;
        private bool _vendorValidated;

        private DateTime _vendorCreatedOn;
        private DateTime _vendorLastUpdatedOn;
        private Addresses _headQuaterAddress;
        private Addresses _billToAddress;
        private Addresses _shpToAddress;       
        
     
        #endregion
        #region property
        public Addresses ShpToAddress
        {
            get { return _shpToAddress; }
            set { _shpToAddress = value; }
        }

        public Addresses BillToAddress
        {
            get { return _billToAddress; }
            set { _billToAddress = value; }
        }

        public Addresses HeadQuaterAddress
        {
            get { return _headQuaterAddress; }
            set { _headQuaterAddress = value; }
        }
        public int VendorID
        {
            get { return _vendorID; }
            set { _vendorID = value; }
        }
        public string VendorName
        {
            get { return _vendorName; }
            set { _vendorName = value; }
        }
        public string VendorEmailID
        {
            get { return _vendorEmailID; }
            set { _vendorEmailID = value; }
        }
        public string VendorFax
        {
            get { return _vendorFax; }
            set { _vendorFax = value; }
        }
        public string VendorPhone
        {
            get { return _vendorPhone; }
            set { _vendorPhone = value; }
        }
        public string VendorPOPref
        {
            get { return _vendorPOPref; }
            set { _vendorPOPref = value; }
        }
        public string VendorStatus
        {
            get { return _vendorStatus; }
            set { _vendorStatus = value; }
        }
        public string VendorCurrency
        {
            get { return _vendorCurrency; }
            set { _vendorCurrency = value; }
        }
        public string VendorContactPersonName
        {
            get { return _vendorContactPersonName; }
            set { _vendorContactPersonName = value; }
        }
        public string VendorLang
        {
            get { return _vendorLang; }
            set { _vendorLang = value; }
        }
        public string VendorContactPersonPhone
        {
            get { return _vendorContactPersonPhone; }
            set { _vendorContactPersonPhone = value; }
        }
        public string VendorContactPersonEmailID
        {
            get { return _vendorContactPersonEmailID; }
            set { _vendorContactPersonEmailID = value; }
        }
        public bool VendorActive
        {
            get { return _vendorActive; }
            set { _vendorActive = value; }
        }
        public bool VendorValidated
        {
            get { return _vendorValidated; }
            set { _vendorValidated = value; }
        }
        public DateTime VendorCreatedOn
        {
            get { return _vendorCreatedOn; }
            set { _vendorCreatedOn = value; }
        }
        public DateTime VendorLastUpdatedOn
        {
            get { return _vendorLastUpdatedOn; }
            set { _vendorLastUpdatedOn = value; }
        }

        #endregion
        
        #region Functions
        public string GetSQL(ParameterCollection pCol, string status, string searchText)
        {
            pCol.Clear();
            string strSQL = "SELECT vendorId, vendorName, vendorPhone, vendorEmailID, vendorFax, (SELECT Count(*) FROM purchaseorders i where poVendorid=vendorId) as TotalPO FROM vendor where vendorActive=1 ";
            if (!string.IsNullOrEmpty(status))
            {
                strSQL += " And vendorStatus= @StatusData ";
                pCol.Add("@StatusData", status);
            }
            if (!string.IsNullOrEmpty(searchText))
            {
                strSQL += " And vendorName LIKE CONCAT('%', @SearchData, '%') or vendorPhone LIKE CONCAT('%', @SearchData, '%') or vendorEmailID LIKE CONCAT('%', @SearchData, '%')  ";
                pCol.Add("@SearchData", searchText);
            }
            strSQL += " order by vendorName ";
            return strSQL;
        }

        public void PopulateObject(DbHelper dbHelp, int vendorID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQL = "SELECT vendorName, vendorEmailID, vendorFax, vendorPhone, vendorPOPref, vendorStatus, vendorCurrency, vendorActive, vendorValidated, vendorCreatedOn, vendorLastUpdatedOn, vendorContactPersonName, vendorLang, vendorContactPersonPhone, vendorContactPersonEmailID FROM vendor where vendorID= @vendorID";
            MySqlParameter[] p = { 
                                    new MySqlParameter("@vendorID", vendorID)
                                 };            
            try
            {
                using (MySqlDataReader drObj = dbHelp.GetDataReader(strSQL, System.Data.CommandType.Text, p))
                {
                    while (drObj.Read())
                    {
                        _vendorName = BusinessUtility.GetString(drObj["vendorName"]);
                        _vendorEmailID = BusinessUtility.GetString(drObj["vendorEmailID"]);
                        _vendorFax = BusinessUtility.GetString(drObj["vendorFax"]);
                        _vendorPhone = BusinessUtility.GetString(drObj["vendorPhone"]);
                        _vendorPOPref = BusinessUtility.GetString(drObj["vendorPOPref"]);
                        _vendorStatus = BusinessUtility.GetString(drObj["vendorStatus"]);
                        _vendorCurrency = BusinessUtility.GetString(drObj["vendorCurrency"]);
                        _vendorActive = BusinessUtility.GetBool(drObj["vendorActive"]);
                        _vendorValidated = BusinessUtility.GetBool(drObj["vendorValidated"]);
                        _vendorCreatedOn = BusinessUtility.GetDateTime(drObj["vendorCreatedOn"]);
                        _vendorLastUpdatedOn = BusinessUtility.GetDateTime(drObj["vendorLastUpdatedOn"]);
                        _vendorContactPersonName = BusinessUtility.GetString(drObj["vendorContactPersonName"]);
                        _vendorContactPersonEmailID = BusinessUtility.GetString(drObj["vendorContactPersonEmailID"]);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        /// <summary>
        /// Update vendor Information based on Vendor Id
        /// </summary>
        /// <returns></returns>
        public bool Update()
        {
            //SQL to Update              
            string sql = "UPDATE vendor set  vendorName=@vendorName, vendorEmailID=@vendorEmailID, vendorFax=@vendorFax, vendorPhone=@vendorPhone, vendorPOPref=@vendorPOPref, vendorStatus=@vendorStatus, vendorCurrency=@vendorCurrency, vendorActive=@vendorActive, vendorValidated=@vendorValidated , ";
            sql += "vendorLastUpdatedOn=@vendorLastUpdatedOn, vendorContactPersonName=@vendorContactPersonName, vendorLang=@vendorLang, vendorContactPersonPhone=@vendorContactPersonPhone, vendorContactPersonEmailID=@vendorContactPersonEmailID  WHERE vendorID = @vendorID";
            MySqlParameter[] p = { 
                                    
                                     DbUtility.GetParameter("vendorID", this._vendorID,typeof(int)),
                                     DbUtility.GetParameter("vendorName", this._vendorName,typeof(string)),
                                     DbUtility.GetParameter("vendorEmailID", this._vendorEmailID,typeof(string)),
                                     DbUtility.GetParameter("vendorFax", this._vendorFax,typeof(string)),
                                     DbUtility.GetParameter("vendorPhone", this._vendorPhone,typeof(string)),
                                     DbUtility.GetParameter("vendorPOPref", this._vendorPOPref,typeof(string)),
                                     DbUtility.GetParameter("vendorStatus", this._vendorStatus,typeof(string)),
                                     DbUtility.GetParameter("vendorCurrency", this._vendorCurrency,typeof(string)),
                                     DbUtility.GetParameter("vendorActive", this._vendorActive,typeof(bool)),
                                     DbUtility.GetParameter("vendorValidated", this._vendorValidated,typeof(bool)),
                                     DbUtility.GetParameter("vendorLastUpdatedOn", this._vendorLastUpdatedOn,typeof(DateTime)),
                                     DbUtility.GetParameter("vendorContactPersonName", this._vendorContactPersonName,typeof(string)),
                                     DbUtility.GetParameter("vendorLang", this._vendorLang,typeof(string)),
                                     DbUtility.GetParameter("vendorContactPersonPhone", this._vendorContactPersonPhone,typeof(string)),
                                     DbUtility.GetParameter("vendorContactPersonEmailID", this._vendorContactPersonEmailID,typeof(string))
                                 };
            DbHelper dbHelper = new DbHelper();
            try
            {
                dbHelper.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);
                this._headQuaterAddress.AddressSourceID = this._vendorID;
                this._headQuaterAddress.Save(dbHelper);

                this._billToAddress.AddressSourceID = this._vendorID;
                this._billToAddress.Save(dbHelper);

                this._shpToAddress.AddressSourceID = this._vendorID;
                this._shpToAddress.Save(dbHelper);
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Insert the vendor Information 
        /// </summary>
        /// <returns>if Return >0 then Save successfull
        /// if value is -1 then Vender exists in Database
        /// </returns>
        public OperationResult Insert()
        {
            //Check For Vendor Duplication record Exists or not
            string sqlVendorDupli = "SELECT count(*) FROM Vendor where vendorName = @vendorName and vendorEmailID = @vendorEmailID";
            MySqlParameter[] pVendorDupli = { 
                                     DbUtility.GetParameter("vendorName", this._vendorName,typeof(string)),
                                     DbUtility.GetParameter("vendorEmailID", this._vendorEmailID,typeof(string))
                                    };

            //SQL to Insert              
            string sql = "Insert vendor (vendorName, vendorEmailID, vendorFax, vendorPhone, vendorPOPref, vendorStatus, vendorCurrency, vendorActive, vendorValidated, vendorCreatedOn, vendorLastUpdatedOn, vendorContactPersonName, vendorLang, vendorContactPersonPhone, vendorContactPersonEmailID) Value ( ";
            sql += "  @vendorName, @vendorEmailID, @vendorFax, @vendorPhone, @vendorPOPref, @vendorStatus, @vendorCurrency, @vendorActive, @vendorValidated, @vendorCreatedOn, @vendorLastUpdatedOn, @vendorContactPersonName, @vendorLang, @vendorContactPersonPhone, @vendorContactPersonEmailID)";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("vendorName", this._vendorName,typeof(string)),
                                     DbUtility.GetParameter("vendorEmailID", this._vendorEmailID,typeof(string)),
                                     DbUtility.GetParameter("vendorFax", this._vendorFax,typeof(string)),
                                     DbUtility.GetParameter("vendorPhone", this._vendorPhone,typeof(string)),
                                     DbUtility.GetParameter("vendorPOPref", this._vendorPOPref,typeof(string)),
                                     DbUtility.GetParameter("vendorStatus", this._vendorStatus,typeof(string)),
                                     DbUtility.GetParameter("vendorCurrency", this._vendorCurrency,typeof(string)),
                                     DbUtility.GetParameter("vendorActive", this._vendorActive,typeof(bool)),
                                     DbUtility.GetParameter("vendorValidated", this._vendorValidated,typeof(bool)),
                                     DbUtility.GetParameter("vendorCreatedOn", this._vendorCreatedOn,typeof(DateTime)),
                                     DbUtility.GetParameter("vendorLastUpdatedOn", this._vendorLastUpdatedOn,typeof(DateTime)),
                                     DbUtility.GetParameter("vendorContactPersonName", this._vendorContactPersonName,typeof(string)),
                                     DbUtility.GetParameter("vendorLang", this._vendorLang,typeof(string)),
                                     DbUtility.GetParameter("vendorContactPersonPhone", this._vendorContactPersonPhone,typeof(string)),
                                     DbUtility.GetParameter("vendorContactPersonEmailID", this._vendorContactPersonEmailID,typeof(string))
                                 };
            DbHelper dbHelper = new DbHelper(true);
            try
            {
                object objScalr1 = dbHelper.GetValue(sqlVendorDupli, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("vendorName", this._vendorName, typeof(string)), DbUtility.GetParameter("vendorEmailID", this._vendorEmailID, typeof(string)) });
                if (BusinessUtility.GetInt(objScalr1) > 0) {
                    throw new Exception(CustomExceptionCodes.VENDOR_ALREADY_EXISTS);
                }

                if (dbHelper.ExecuteNonQuery(sql, System.Data.CommandType.Text, p) > 0)
                {                    
                    int venderid=dbHelper.GetLastInsertID();
                    this._headQuaterAddress.AddressSourceID = venderid;
                    this._headQuaterAddress.Save(dbHelper);

                    this._billToAddress.AddressSourceID = venderid;
                    this._billToAddress.Save(dbHelper);

                    this._shpToAddress.AddressSourceID = venderid;
                    this._shpToAddress.Save(dbHelper);
                    this._vendorID = venderid;
                }
                return OperationResult.Success;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public bool Delete(int vendorID) {
            string sql = "DELETE FROM vendor WHERE vendorId=@vendorId";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("vendorId", vendorID, MyDbType.Int) });
                return true;
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion
    }
}

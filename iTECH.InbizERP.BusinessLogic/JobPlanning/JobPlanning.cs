﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;


namespace iTECH.InbizERP.BusinessLogic
{
    public class JobPlanning
    {
        public int JobPlanningID { get; set; }
        public int UserID { get; set; }
        public int OrderID { get; set; }
        public bool Active { get; set; }
        public int CustomerID { get; set; }
        public string CustomerType { get; set; }
        public DateTime JobDueDate { get; set; }
        public string JobNote { get; set; }

        public void Save(DbHelper dbHelp)
        {
            string sqlExist = "SELECT COUNT(*) FROM jobplanning WHERE OrderID=@OrderID AND UserID=@UserID";
            string sqlInsert = "INSERT INTO jobplanning(UserID,OrderID,Active,CustomerID,CustomerType,JobDueDate,JobNote) VALUES";
            sqlInsert += "(@UserID,@OrderID,@Active,@CustomerID,@CustomerType,@JobDueDate,@JobNote)";
            string sqlUpdate = "UPDATE jobplanning SET Active=@Active,CustomerID=@CustomerID,CustomerType=@CustomerType,JobDueDate=@JobDueDate,JobNote=@JobNote";
            sqlUpdate += " WHERE UserID=@UserID AND OrderID=@OrderID";
            
            try
            {
                object val = dbHelp.GetValue(sqlExist, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("UserID", this.UserID, MyDbType.Int),
                    DbUtility.GetParameter("OrderID", this.OrderID, MyDbType.Int)
                });
                if (BusinessUtility.GetInt(val) > 0)
                {
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("UserID", this.UserID, MyDbType.Int),
                        DbUtility.GetParameter("OrderID", this.OrderID, MyDbType.Int),
                        DbUtility.GetParameter("Active", this.Active, MyDbType.Boolean),
                        DbUtility.GetParameter("CustomerID", this.CustomerID, MyDbType.Int),
                        DbUtility.GetParameter("CustomerType", this.CustomerType, MyDbType.String),
                        DbUtility.GetParameter("JobDueDate", this.JobDueDate, MyDbType.DateTime),
                        DbUtility.GetParameter("JobNote", this.JobNote, MyDbType.String)
                    });
                }
                else
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("UserID", this.UserID, MyDbType.Int),
                        DbUtility.GetParameter("OrderID", this.OrderID, MyDbType.Int),
                        DbUtility.GetParameter("Active", this.Active, MyDbType.Boolean),
                        DbUtility.GetParameter("CustomerID", this.CustomerID, MyDbType.Int),
                        DbUtility.GetParameter("CustomerType", this.CustomerType, MyDbType.String),
                        DbUtility.GetParameter("JobDueDate", this.JobDueDate, MyDbType.DateTime),
                        DbUtility.GetParameter("JobNote", this.JobNote, MyDbType.String)
                    });
                }
            }
            catch
            {
                throw;
            }            
        }

        public string GetJobAssignedUsers(int orderid)
        {            
            string strSQL = "SELECT  concat(u.userFirstName,' ',IFNULL(u.userLastName, '')) as User FROM jobplanning as s Inner join users u on u.userID=s.UserID where OrderID=@OrderID ";
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            List<string> lstUsers = new List<string>();
            try
            {
                dr = dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderID", orderid, MyDbType.Int)
                });
                while (dr.Read())
                {
                    lstUsers.Add(BusinessUtility.GetString(dr["User"]));
                }
                return string.Join(", ", lstUsers.ToArray());
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTECH.InbizERP.BusinessLogic
{
    public interface IPrint
    {
        int RequestID { get; set; }
        string DocType { get; set; }        
        void InitDetails();
    }
}

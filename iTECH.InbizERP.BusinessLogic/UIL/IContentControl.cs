﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iTECH.InbizERP.BusinessLogic
{
    public interface IContentControl
    {
        int PageID { get; }
        void Initialize();
    }
}
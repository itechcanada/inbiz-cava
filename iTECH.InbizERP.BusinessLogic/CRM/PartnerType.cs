﻿#region References
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql; 
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;
#endregion
#region "Modify Version "
/**********************************
 * Modified By   :: Ankit Choure
 * Modified Date :: 29-JULY-2011
 * ********************************/
#endregion

namespace iTECH.InbizERP.BusinessLogic
{
    public class PartnerType
    {
        #region Private Member
        private int _parterTypeID;

        private string _parterTypeLang;
        private string _partnerTypeDesc;

        private bool _partnerTypeActive;

        #endregion
        #region Private Property
        public int ParterTypeID
        {
            get { return _parterTypeID; }
            set { _parterTypeID = value; }
        }
        public string ParterTypeLang
        {
            get { return _parterTypeLang; }
            set { _parterTypeLang = value; }
        }
        public string PartnerTypeDesc
        {
            get { return _partnerTypeDesc; }
            set { _partnerTypeDesc = value; }
        }
        public bool PartnerTypeActive
        {
            get { return _partnerTypeActive; }
            set { _partnerTypeActive = value; }
        }
        #endregion
        #region Public Function
        public int Insert()
        {
            //SQL to Insert              
            string sql = "INSERT INTO partnertype (ParterTypeLang, PartnerTypeDesc, PartnerTypeActive) ";
            sql += " VALUES (@ParterTypeLang, @PartnerTypeDesc, @PartnerTypeActive)";
            MySqlParameter[] p = { 
                                    DbUtility.GetParameter("ParterTypeLang", this._parterTypeLang,typeof(string)),
                                    DbUtility.GetParameter("PartnerTypeDesc", this._partnerTypeDesc,typeof(string)),
                                    DbUtility.GetParameter("PartnerTypeActive", this._partnerTypeActive,typeof(bool))
                                 };
            DbHelper dbHelper = new DbHelper(true);
            try
            {
                if (dbHelper.ExecuteNonQuery(sql, System.Data.CommandType.Text, p) > 0)
                {
                    return dbHelper.GetLastInsertID();
                }
                return 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        ///  Update Partners Details Into Partners table 
        /// </summary>
        /// <returns>as bool true/false </returns>
        public bool Update()
        {
            //SQL to Update              
            string sql = "UPDATE partnertype SET  ParterTypeLang=@ParterTypeLang, PartnerTypeDesc=@PartnerTypeDesc, PartnerTypeActive=@PartnerTypeActive ";
            sql += " Where ParterTypeID=@ParterTypeID ";
            MySqlParameter[] p = { 
                                    DbUtility.GetParameter("ParterTypeID", this._parterTypeID,typeof(int)),
                                    DbUtility.GetParameter("ParterTypeLang", this._parterTypeLang,typeof(string)),
                                    DbUtility.GetParameter("PartnerTypeDesc", this._partnerTypeDesc,typeof(string)),
                                    DbUtility.GetParameter("PartnerTypeActive", this._partnerTypeActive,typeof(bool))
                                 };
            DbHelper dbHelper = new DbHelper();
            try
            {
                dbHelper.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Get the partner type information by partnertype Id 
        /// </summary>
        public void GetPartnersInfo()
        {
            string strSQL = null;
            strSQL = "SELECT  ParterTypeLang, PartnerTypeDesc, PartnerTypeActive from partnertype ";
            strSQL += " WHERE ParterTypeID=@ParterTypeID  ";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("PartnerID", this._parterTypeID,typeof(int) )
                                 };
            DbHelper dbHelper = new DbHelper();
            MySqlDataReader objDr = default(MySqlDataReader);
            try
            {

                objDr = dbHelper.GetDataReader(strSQL, System.Data.CommandType.Text, p);
                while (objDr.Read())
                {
                    this._parterTypeLang = BusinessUtility.GetString(objDr["ParterTypeLang"]);
                    this._partnerTypeDesc = BusinessUtility.GetString(objDr["PartnerTypeDesc"]);
                    this._partnerTypeActive = BusinessUtility.GetBool(objDr["PartnerTypeActive"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
                if (objDr != null && !objDr.IsClosed)
                {
                    objDr.Close();
                }
            }

        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class CustomerExtended
    {
        public int PartnerID { get; set; }       
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SpirtualName { get; set; }
        public string Sex { get; set; }
        public DateTime DOB { get; set; }
        public double Age { get; set; }
        public string Nationality { get; set; }
        public string WorkExperience { get; set; }
        public string Illnesses { get; set; }
        public string Medication { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyTelephone { get; set; }
        public string EmergencyAltTelephone { get; set; }
        public string EmergencyRelationship { get; set; }
        public string ReasonToJoinStaff { get; set; }
        public string IsMember { get; set; } //sysstatus
        public string MembershipID { get; set; }
        public bool IsPartOfStaffInPast { get; set; }
        public bool ReceiveNewsLetters { get; set; }
        public int GuestType { get; set; }
        public bool EnableSocialPlugins { get; set; }


        public CustomerExtended()
        {
            this.Age = 0D;
            this.DOB = DateTime.MinValue;
            this.EmergencyAltTelephone = string.Empty;
            this.EmergencyContactName = string.Empty;
            this.EmergencyRelationship = string.Empty;
            this.EmergencyTelephone = string.Empty;
            this.FirstName = string.Empty;
            this.Illnesses = string.Empty;
            this.IsMember = string.Empty;
            this.IsPartOfStaffInPast = false;
            this.LastName = string.Empty;
            this.Medication = string.Empty;
            this.MembershipID = string.Empty;
            this.Nationality = string.Empty;
            this.PartnerID = 0;
            this.ReasonToJoinStaff = string.Empty;
            this.ReceiveNewsLetters = false;
            this.Sex = string.Empty;
            this.SpirtualName = string.Empty;
            this.WorkExperience = string.Empty;
            this.GuestType = (int)StatusGuestType.Default;
            this.EnableSocialPlugins = false;
        }

        public void Save(DbHelper dbHelp)
        {
            string sqlCheckCount = "SELECT COUNT(*) FROM z_customer_extended WHERE PartnerID=@PartnerID"; 

            string sqlInsert = "INSERT INTO z_customer_extended";
            sqlInsert += "(PartnerID,FirstName, LastName, SpirtualName, Sex, DOB, Age, Nationality, WorkExperience, Illnesses, ";
            sqlInsert += "Medication, EmergencyContactName, EmergencyTelephone, EmergencyAltTelephone, EmergencyRelationship, ";
            sqlInsert += "ReasonToJoinStaff, IsMember, MembershipID, IsPartOfStaffInPast, ReceiveNewsLetters,GuestType,EnableSocialPlugins)";
            sqlInsert += " VALUES (@PartnerID,@FirstName, @LastName, @SpirtualName, @Sex, @DOB, @Age, @Nationality, @WorkExperience, @Illnesses, ";
            sqlInsert += "@Medication, @EmergencyContactName, @EmergencyTelephone, @EmergencyAltTelephone, @EmergencyRelationship, ";
            sqlInsert += "@ReasonToJoinStaff, @IsMember, @MembershipID, @IsPartOfStaffInPast, @ReceiveNewsLetters,@GuestType,@EnableSocialPlugins)";
    
            string sqlUpdate = "UPDATE z_customer_extended SET FirstName=@FirstName, LastName=@LastName,SpirtualName=@SpirtualName,Sex=@Sex,";
            sqlUpdate += "DOB=@DOB,Age=@Age,Nationality=@Nationality,WorkExperience=@WorkExperience,Illnesses=@Illnesses,Medication=@Medication,";
            sqlUpdate += "EmergencyContactName=@EmergencyContactName,EmergencyTelephone=@EmergencyTelephone,EmergencyAltTelephone=@EmergencyAltTelephone,";
            sqlUpdate += "EmergencyRelationship=@EmergencyRelationship,ReasonToJoinStaff=@ReasonToJoinStaff,IsMember=@IsMember,MembershipID=@MembershipID,";
            sqlUpdate += "IsPartOfStaffInPast=@IsPartOfStaffInPast,ReceiveNewsLetters=@ReceiveNewsLetters,GuestType=@GuestType,EnableSocialPlugins=@EnableSocialPlugins WHERE PartnerID=@PartnerID";

            //dbHelp 
            try
            {
                object chkCount = dbHelp.GetValue(sqlCheckCount, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerID", this.PartnerID, MyDbType.Int)
                });

                if (BusinessUtility.GetInt(chkCount) > 0)
                {
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("FirstName", this.FirstName, MyDbType.String),
                        DbUtility.GetParameter("LastName", this.LastName, MyDbType.String),
                        DbUtility.GetParameter("SpirtualName", this.SpirtualName, MyDbType.String),
                        DbUtility.GetParameter("Sex", this.Sex, MyDbType.String),
                        DbUtility.GetParameter("DOB", this.DOB, MyDbType.DateTime),
                        DbUtility.GetParameter("Age", this.Age, MyDbType.Double),
                        DbUtility.GetParameter("Nationality", this.Nationality, MyDbType.String),
                        DbUtility.GetParameter("WorkExperience", this.WorkExperience, MyDbType.String),
                        DbUtility.GetParameter("Illnesses", this.Illnesses, MyDbType.String),
                        DbUtility.GetParameter("Medication", this.Medication, MyDbType.String),
                        DbUtility.GetParameter("EmergencyContactName", this.EmergencyContactName, MyDbType.String),
                        DbUtility.GetParameter("EmergencyTelephone", this.EmergencyTelephone, MyDbType.String),
                        DbUtility.GetParameter("EmergencyAltTelephone", this.EmergencyAltTelephone, MyDbType.String),
                        DbUtility.GetParameter("EmergencyRelationship", this.EmergencyRelationship, MyDbType.String),
                        DbUtility.GetParameter("ReasonToJoinStaff", this.ReasonToJoinStaff, MyDbType.String),
                        DbUtility.GetParameter("IsMember", this.IsMember, MyDbType.String),
                        DbUtility.GetParameter("MembershipID", this.MembershipID, MyDbType.String),
                        DbUtility.GetParameter("IsPartOfStaffInPast", this.IsPartOfStaffInPast, MyDbType.Boolean),
                        DbUtility.GetParameter("ReceiveNewsLetters", this.ReceiveNewsLetters, MyDbType.Boolean),
                        DbUtility.GetParameter("PartnerID", this.PartnerID, MyDbType.Int),
                        DbUtility.GetParameter("GuestType", this.GuestType, MyDbType.Int),
                        DbUtility.GetParameter("EnableSocialPlugins", this.EnableSocialPlugins, MyDbType.Boolean)
                    });
                }
                else
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("PartnerID", this.PartnerID, MyDbType.Int),
                        DbUtility.GetParameter("FirstName", this.FirstName, MyDbType.String),
                        DbUtility.GetParameter("LastName", this.LastName, MyDbType.String),
                        DbUtility.GetParameter("SpirtualName", this.SpirtualName, MyDbType.String),
                        DbUtility.GetParameter("Sex", this.Sex, MyDbType.String),
                        DbUtility.GetParameter("DOB", this.DOB, MyDbType.DateTime),
                        DbUtility.GetParameter("Age", this.Age, MyDbType.Double),
                        DbUtility.GetParameter("Nationality", this.Nationality, MyDbType.String),
                        DbUtility.GetParameter("WorkExperience", this.WorkExperience, MyDbType.String),
                        DbUtility.GetParameter("Illnesses", this.Illnesses, MyDbType.String),
                        DbUtility.GetParameter("Medication", this.Medication, MyDbType.String),
                        DbUtility.GetParameter("EmergencyContactName", this.EmergencyContactName, MyDbType.String),
                        DbUtility.GetParameter("EmergencyTelephone", this.EmergencyTelephone, MyDbType.String),
                        DbUtility.GetParameter("EmergencyAltTelephone", this.EmergencyAltTelephone, MyDbType.String),
                        DbUtility.GetParameter("EmergencyRelationship", this.EmergencyRelationship, MyDbType.String),
                        DbUtility.GetParameter("ReasonToJoinStaff", this.ReasonToJoinStaff, MyDbType.String),
                        DbUtility.GetParameter("IsMember", this.IsMember, MyDbType.String),
                        DbUtility.GetParameter("MembershipID", this.MembershipID, MyDbType.String),
                        DbUtility.GetParameter("IsPartOfStaffInPast", this.IsPartOfStaffInPast, MyDbType.Boolean),
                        DbUtility.GetParameter("ReceiveNewsLetters", this.ReceiveNewsLetters, MyDbType.Boolean),
                        DbUtility.GetParameter("GuestType", this.GuestType, MyDbType.Int),
                        DbUtility.GetParameter("EnableSocialPlugins", this.EnableSocialPlugins, MyDbType.Boolean)
                    });
                }
            }
            catch
            {

                throw;
            }            
        }

        public void PopulateObject(DbHelper dbHelp, int partnerID)
        {
            string sql = "SELECT * FROM z_customer_extended WHERE PartnerID=@PartnerID";
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerID", partnerID, MyDbType.Int)
                });
                while (dr.Read())
                {
                    this.Age = BusinessUtility.GetDouble(dr["Age"]);
                    this.DOB = BusinessUtility.GetDateTime(dr["DOB"]);
                    this.EmergencyAltTelephone = BusinessUtility.GetString(dr["EmergencyAltTelephone"]);
                    this.EmergencyContactName = BusinessUtility.GetString(dr["EmergencyContactName"]);
                    this.EmergencyRelationship = BusinessUtility.GetString(dr["EmergencyRelationship"]);
                    this.EmergencyTelephone = BusinessUtility.GetString(dr["EmergencyTelephone"]);
                    this.FirstName = BusinessUtility.GetString(dr["FirstName"]);
                    this.Illnesses = BusinessUtility.GetString(dr["Illnesses"]);
                    this.IsMember = BusinessUtility.GetString(dr["IsMember"]);
                    this.IsPartOfStaffInPast = BusinessUtility.GetBool(dr["IsPartOfStaffInPast"]);
                    this.LastName = BusinessUtility.GetString(dr["LastName"]);
                    this.Medication = BusinessUtility.GetString(dr["Medication"]);
                    this.MembershipID = BusinessUtility.GetString(dr["MembershipID"]);
                    this.Nationality = BusinessUtility.GetString(dr["Nationality"]);
                    this.PartnerID = BusinessUtility.GetInt(dr["PartnerID"]);
                    this.ReasonToJoinStaff = BusinessUtility.GetString(dr["ReasonToJoinStaff"]);
                    this.ReceiveNewsLetters = BusinessUtility.GetBool(dr["ReceiveNewsLetters"]);
                    this.Sex = BusinessUtility.GetString(dr["Sex"]);
                    this.SpirtualName = BusinessUtility.GetString(dr["SpirtualName"]);
                    this.WorkExperience = BusinessUtility.GetString(dr["WorkExperience"]);
                    this.GuestType = BusinessUtility.GetInt(dr["GuestType"]);
                    this.EnableSocialPlugins = BusinessUtility.GetBool(dr["EnableSocialPlugins"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }
            }
        }

        //public void Insert(int userid) {
        //    DbHelper dbHelp = new DbHelper(true);
        //    try
        //    {
        //        Partners part = SavePartner(dbHelp, userid);
        //        this.PartnerID = part.PartnerID;
        //        if (part.PartnerID > 0)
        //        {
        //            PartnerContacts pmContact = SavePartnerPrimaryContact(dbHelp, part.PartnerID, userid);

        //            if (pmContact.ContactID > 0)
        //            {
        //                this.PrimaryContactID = pmContact.ContactID;

        //                //Insert Imergency Contact Information
        //                PartnerContacts emContact = SaveEmergencyContact(dbHelp, part.PartnerID, userid);
        //                this.EmergencyContactID = emContact.ContactID;

        //                string sqlGustInsert = "INSERT INTO z_customer_extended (PartnerContactID, SpirtualName,IsMember,MembershipID,EmergencyContactID)";
        //                sqlGustInsert += " VALUES(@PartnerContactID, @SpirtualName,@IsMember,@MembershipID,@EmergencyContactID)";

        //                int? emContactID = this.EmergencyContactID > 0 ? (int?)this.EmergencyContactID : null;

        //                dbHelp.ExecuteNonQuery(sqlGustInsert, CommandType.Text, new MySqlParameter[] { 
        //                DbUtility.GetParameter("PartnerContactID", this.PrimaryContactID, MyDbType.Int),                        
        //                DbUtility.GetParameter("SpirtualName", this.SpirtualName, MyDbType.String),
        //                DbUtility.GetParameter("IsMember", this.IsMember, MyDbType.Boolean),
        //                DbUtility.GetParameter("MembershipID", this.MembershipID, MyDbType.String),                        
        //                DbUtility.GetIntParameter("EmergencyContactID", emContactID)
        //            });


        //            }
        //        }
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        //private Partners SavePartner(DbHelper dbHelp, int userid) {
        //    Partners part = new Partners();
        //    part.InActiveReason = string.Empty;
        //    part.PartnerAcronyme = string.Empty;
        //    part.PartnerActive = true;
        //    part.PartnerAdditionPhone = string.Empty;
        //    part.PartnerCommissionCode = string.Empty;
        //    part.PartnerCourierCode = string.Empty;
        //    part.PartnerCreatedBy = userid;
        //    part.PartnerCreditAvailable = 0;
        //    part.PartnerCurrencyCode = string.Empty;
        //    part.PartnerDesc1 = string.Empty;
        //    part.PartnerDesc2 = string.Empty;
        //    part.PartnerDiscount = 0;
        //    part.PartnerEmail = this.EmailAddress;
        //    part.PartnerFax = string.Empty;
        //    part.PartnerInvoiceNetTerms = string.Empty;
        //    part.PartnerInvoicePreference = string.Empty;
        //    part.PartnerLang = string.Empty;
        //    part.PartnerLastUpdatedBy = userid;
        //    part.PartnerLocked = false;
        //    part.PartnerLongName = string.Format("{0} {1}", this.LastName, this.FirstName).Trim();
        //    part.PartnerNote = string.Empty;
        //    part.PartnerPhone = this.TelephoneNo;
        //    part.PartnerPhone2 = string.Empty;
        //    part.PartnerShipBlankPref = false;
        //    part.PartnerStatus = PartnerStatus.ACTIVE;
        //    part.PartnerTaxCode = 0;
        //    part.PartnerType = 1; //I dont know why?
        //    part.PartnerValidated = "1";
        //    part.PartnerValue = string.Empty;
        //    part.PartnerWebsite = string.Empty;

        //    List<Addresses> lstAddress = new List<Addresses>();
        //    Addresses hqAddr = new Addresses();
        //    hqAddr.AddressCity = this.City;
        //    hqAddr.AddressCountry = this.Country;
        //    hqAddr.AddressLine1 = this.AddressLine1;
        //    hqAddr.AddressLine2 = this.AddressLine2;
        //    hqAddr.AddressLine3 = string.Empty;
        //    hqAddr.AddressPostalCode = this.PostalCode;
        //    hqAddr.AddressRef = AddressReference.DISTRIBUTER;
        //    hqAddr.AddressState = this.State;
        //    hqAddr.AddressType = AddressType.HEAD_OFFICE;
        //    lstAddress.Add(hqAddr);

        //    Addresses billToAdd = new Addresses();
        //    billToAdd.AddressCity = this.City;
        //    billToAdd.AddressCountry = this.Country;
        //    billToAdd.AddressLine1 = this.AddressLine1;
        //    billToAdd.AddressLine2 = this.AddressLine2;
        //    billToAdd.AddressLine3 = string.Empty;
        //    billToAdd.AddressPostalCode = this.PostalCode;
        //    billToAdd.AddressRef = AddressReference.DISTRIBUTER;
        //    billToAdd.AddressState = this.State;
        //    billToAdd.AddressType = AddressType.BILL_TO_ADDRESS;
        //    lstAddress.Add(billToAdd);

        //    Addresses shipToAdd = new Addresses();
        //    shipToAdd.AddressCity = this.City;
        //    shipToAdd.AddressCountry = this.Country;
        //    shipToAdd.AddressLine1 = this.AddressLine1;
        //    shipToAdd.AddressLine2 = this.AddressLine2;
        //    shipToAdd.AddressLine3 = string.Empty;
        //    shipToAdd.AddressPostalCode = this.PostalCode;
        //    shipToAdd.AddressRef = AddressReference.DISTRIBUTER;
        //    shipToAdd.AddressState = this.State;
        //    shipToAdd.AddressType = AddressType.SHIP_TO_ADDRESS;
        //    lstAddress.Add(shipToAdd);

        //    part.Insert(dbHelp, userid, (int)PartnerTypes.Distributer, lstAddress);

        //    return part;
        //}

        //private PartnerContacts SavePartnerPrimaryContact(DbHelper dbHelp, int partnerID, int userid) {
        //    PartnerContacts contact = new PartnerContacts();
        //    contact.ContactCreatedBy = userid;
        //    contact.ContactDOB = this.DOB;
        //    contact.ContactEmail = this.EmailAddress;
        //    contact.ContactFirstName = this.FirstName;
        //    contact.ContactLastName = this.LastName;
        //    contact.ContactLastUpdatedBy = userid;
        //    contact.ContactPartnerID = partnerID;
        //    contact.ContactPhone = this.TelephoneNo;
        //    contact.IsSubscribeEmail = this.ReceiveNewsLetters;

        //    contact.ContactActive = true;
        //    contact.ContactCellPhone = string.Empty;
        //    contact.ContactFax = string.Empty;
        //    contact.ContactHomePhone = string.Empty;
        //    contact.ContactLangPref = AppLanguageCode.EN;
        //    contact.ContactNote = string.Empty;
        //    contact.ContactPagerNo = string.Empty;
        //    contact.ContactPhoneExt = string.Empty;
        //    contact.ContactPrimary = true;
        //    contact.ContactSalutation = string.Empty;
        //    contact.ContactSecEmail = string.Empty;
        //    contact.ContactSelSpecialization = 0;
        //    contact.ContactSex = this.Sex;
        //    contact.ContactTitleEn = string.Empty;
        //    contact.ContactTitleFr = string.Empty;
        //    contact.Relationship = string.Empty;

        //    Addresses ctAddress = new Addresses();
        //    ctAddress.AddressCity = this.City;
        //    ctAddress.AddressCountry = this.Country;
        //    ctAddress.AddressLine1 = this.AddressLine1;
        //    ctAddress.AddressLine2 = this.AddressLine2;
        //    ctAddress.AddressLine3 = string.Empty;
        //    ctAddress.AddressPostalCode = this.PostalCode;
        //    ctAddress.AddressRef = AddressReference.CUSTOMER_CONTACT;
        //    ctAddress.AddressState = this.State;
        //    ctAddress.AddressType = AddressType.BILL_TO_ADDRESS;
        //    contact.Insert(dbHelp, userid, ctAddress);

        //    return contact;
        //}

        //private PartnerContacts SaveEmergencyContact(DbHelper dbHelp, int partnerID, int userid)
        //{
        //    PartnerContacts ctImergency = new PartnerContacts();
        //    ctImergency.ContactCreatedBy = userid;
        //    ctImergency.ContactLastUpdatedBy = userid;
        //    ctImergency.ContactCellPhone = this.EmergencyAlternetPhoneNumber;
        //    ctImergency.ContactFirstName = this.EmergencyContactName;
        //    ctImergency.ContactPartnerID = partnerID;
        //    ctImergency.ContactPhone = this.EmergencyTelephone;
        //    ctImergency.Relationship = this.EmergencyRelationship;

        //    ctImergency.ContactActive = true;
        //    ctImergency.ContactDOB = DateTime.MinValue;
        //    ctImergency.ContactEmail = string.Empty;
        //    ctImergency.ContactFax = string.Empty;
        //    ctImergency.ContactHomePhone = string.Empty;
        //    ctImergency.ContactLangPref = AppLanguageCode.EN;
        //    ctImergency.ContactLastName = string.Empty;
        //    ctImergency.ContactNote = string.Empty;
        //    ctImergency.ContactPagerNo = string.Empty;
        //    ctImergency.ContactPhoneExt = string.Empty;
        //    ctImergency.ContactPrimary = false;
        //    ctImergency.ContactSalutation = string.Empty;
        //    ctImergency.ContactSecEmail = string.Empty;
        //    ctImergency.ContactSelSpecialization = 0;
        //    ctImergency.ContactSex = string.Empty;
        //    ctImergency.ContactTitleEn = string.Empty;
        //    ctImergency.ContactTitleFr = string.Empty;
        //    ctImergency.IsSubscribeEmail = false;


        //    Addresses ctEmerAddress = new Addresses();
        //    ctEmerAddress.AddressCity = this.City;
        //    ctEmerAddress.AddressCountry = this.Country;
        //    ctEmerAddress.AddressLine1 = this.AddressLine1;
        //    ctEmerAddress.AddressLine2 = this.AddressLine2;
        //    ctEmerAddress.AddressLine3 = string.Empty;
        //    ctEmerAddress.AddressPostalCode = this.PostalCode;
        //    ctEmerAddress.AddressRef = AddressReference.CUSTOMER_CONTACT;
        //    ctEmerAddress.AddressState = this.State;
        //    ctEmerAddress.AddressType = AddressType.BILL_TO_ADDRESS;

        //    ctImergency.Insert(dbHelp, userid, ctEmerAddress);

        //    return ctImergency;
        //}

        //public void PopulateObject(int guestid) {
        //    string sql = "SELECT * FROM vw_guests WHERE GuestID=@GuestID";
        //    DbHelper dbHelp = new DbHelper(true);
        //    MySqlDataReader dr = null;
        //    try
        //    {
        //        dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("GuestID", guestid, MyDbType.Int) });
        //        if (dr.Read()) {
        //            this.DOB = BusinessUtility.GetDateTime(dr["DOB"]);
        //            this.EmailAddress = BusinessUtility.GetString(dr["EmailAddress"]);
        //            this.EmergencyAlternetPhoneNumber = BusinessUtility.GetString(dr["EmergencyAlternetPhoneNumber"]);
        //            this.EmergencyContactID = BusinessUtility.GetInt(dr["EmergencyContactID"]);
        //            this.EmergencyContactName = BusinessUtility.GetString(dr["EmergencyContactName"]);
        //            this.EmergencyRelationship = BusinessUtility.GetString(dr["EmergencyRelationship"]);
        //            this.EmergencyTelephone = BusinessUtility.GetString(dr["EmergencyTelephone"]);
        //            this.FirstName = BusinessUtility.GetString(dr["FirstName"]);
        //            this.FullName = BusinessUtility.GetString(dr["FullName"]);
        //            this.PartnerID = BusinessUtility.GetInt(dr["GuestID"]);
        //            this.IsActive = BusinessUtility.GetBool(dr["IsActive"]);
        //            this.IsMember = BusinessUtility.GetString(dr["IsMember"]);
        //            this.LastName = BusinessUtility.GetString(dr["LastName"]);
        //            this.MembershipID = BusinessUtility.GetString(dr["MembershipID"]);
        //            this.PrimaryContactID = BusinessUtility.GetInt(dr["PrimaryContactID"]);
        //            this.ReceiveNewsLetters = BusinessUtility.GetBool(dr["ReceiveNewsLetters"]);
        //            this.Sex = BusinessUtility.GetString(dr["Sex"]);
        //            this.SpirtualName = BusinessUtility.GetString(dr["SpirtualName"]);
        //            this.Status = BusinessUtility.GetString(dr["Status"]);                                      
        //            this.TelephoneNo = BusinessUtility.GetString(dr["TelephoneNo"]);                    
        //        }
        //        dr.Close();

        //        Addresses addr = new Addresses();
        //        if (this.PartnerID > 0)
        //        {
        //            addr.PopulateObject(this.PartnerID, AddressReference.DISTRIBUTER, AddressType.BILL_TO_ADDRESS);
        //            this.AddressLine1 = addr.AddressLine1;
        //            this.AddressLine2 = addr.AddressLine2;
        //            this.City = addr.AddressCity;
        //            this.Country = addr.AddressCountry;
        //            this.PostalCode = addr.AddressPostalCode;
        //            this.State = addr.AddressState;
        //        }
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally {
        //        dbHelp.CloseDatabaseConnection(dr);
        //    }
        //}                
    }
}

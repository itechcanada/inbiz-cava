﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;


namespace iTECH.InbizERP.BusinessLogic
{
    public class PartnerContacts
    {
        public int ContactID { get; set; }
        public int ContactPartnerID { get; set; }
        public string ContactSalutation { get; set; }
        public string ContactLastName { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactTitleFr { get; set; }
        public string ContactTitleEn { get; set; }
        public string ContactPhone { get; set; }
        public string ContactPhoneExt { get; set; }
        public string ContactEmail { get; set; }
        public string ContactSecEmail { get; set; }
        public string ContactFax { get; set; }
        public DateTime ContactDOB { get; set; }
        public string ContactSex { get; set; }
        public string ContactLangPref { get; set; }
        public string ContactHomePhone { get; set; }
        public string ContactCellPhone { get; set; }
        public string ContactPagerNo { get; set; }
        public DateTime ContactCreatedOn { get; set; }
        public int ContactCreatedBy { get; set; }
        public DateTime ContactLastUpdatedOn { get; set; }
        public int ContactLastUpdatedBy { get; set; }
        public bool ContactActive { get; set; }
        public bool ContactPrimary { get; set; }
        public int ContactSelSpecialization { get; set; }
        public string ContactNote { get; set; }
        public bool IsSubscribeEmail { get; set; }
        public string Relationship { get; set; }
        public string FaceBook { get; set; }
        public string Twitter { get; set; }
        public string LinkedIn { get; set; }
        public string ContactType { get; set; }
        public string DOB { get; set; }


        public bool Insert(int userid, Addresses addr)
        {
            string sql = "INSERT INTO partnercontacts( ContactPartnerID, ContactSalutation, ContactLastName, ContactFirstName, ContactTitleFr, ContactTitleEn, ContactPhone, ContactPhoneExt, ContactEmail, ContactSecEmail, ContactFax, ContactDOB, ContactSex, ContactLangPref, ContactHomePhone, ContactCellPhone, ContactPagerNo, ContactCreatedOn, ContactCreatedBy, ContactLastUpdatedOn, ContactLastUpdatedBy, ContactActive, ContactPrimary, ContactSelSpecialization, ContactNote,IsSubscribeEmail,Relationship) VALUES(";
            sql += "@ContactPartnerID, @ContactSalutation, @ContactLastName, @ContactFirstName, @ContactTitleFr, @ContactTitleEn, @ContactPhone, @ContactPhoneExt, ";
            sql += "@ContactEmail, @ContactSecEmail, @ContactFax, @ContactDOB, @ContactSex, @ContactLangPref, @ContactHomePhone, @ContactCellPhone, @ContactPagerNo, ";
            sql += "@ContactCreatedOn, @ContactCreatedBy, @ContactLastUpdatedOn, @ContactLastUpdatedBy, @ContactActive, @ContactPrimary, @ContactSelSpecialization, ";
            sql += "@ContactNote,@IsSubscribeEmail,@Relationship)";

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                int? contactSelSpecilization = this.ContactSelSpecialization > 0 ? (int?)this.ContactSelSpecialization : null;
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ContactPartnerID", this.ContactPartnerID, MyDbType.Int),
                    DbUtility.GetParameter("ContactSalutation", this.ContactSalutation, MyDbType.String),
                    DbUtility.GetParameter("ContactLastName", this.ContactLastName, MyDbType.String),
                    DbUtility.GetParameter("ContactFirstName", this.ContactFirstName, MyDbType.String),
                    DbUtility.GetParameter("ContactTitleFr", this.ContactTitleFr, MyDbType.String),
                    DbUtility.GetParameter("ContactTitleEn", this.ContactTitleEn, MyDbType.String),
                    DbUtility.GetParameter("ContactPhone", this.ContactPhone, MyDbType.String),
                    DbUtility.GetParameter("ContactPhoneExt", this.ContactPhoneExt, MyDbType.String),
                    DbUtility.GetParameter("ContactEmail", this.ContactEmail, MyDbType.String),
                    DbUtility.GetParameter("ContactFax", this.ContactFax, MyDbType.String),
                    DbUtility.GetParameter("ContactDOB", this.ContactDOB, MyDbType.DateTime),
                    DbUtility.GetParameter("ContactSex", this.ContactSex, MyDbType.String),
                    DbUtility.GetParameter("ContactLangPref", this.ContactLangPref, MyDbType.String),
                    DbUtility.GetParameter("ContactHomePhone", this.ContactHomePhone, MyDbType.String),
                    DbUtility.GetParameter("ContactCellPhone", this.ContactCellPhone, MyDbType.String),
                    DbUtility.GetParameter("ContactPagerNo", this.ContactPagerNo, MyDbType.String),
                    DbUtility.GetParameter("ContactCreatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ContactCreatedBy", userid, MyDbType.Int),
                    DbUtility.GetParameter("ContactLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ContactLastUpdatedBy", userid, MyDbType.Int),
                    DbUtility.GetParameter("ContactActive", this.ContactActive, MyDbType.Boolean),
                    DbUtility.GetParameter("ContactPrimary", this.ContactPrimary, MyDbType.Boolean),
                    DbUtility.GetIntParameter("ContactSelSpecialization", contactSelSpecilization),
                    DbUtility.GetParameter("ContactNote", this.ContactNote, MyDbType.String),
                    DbUtility.GetParameter("IsSubscribeEmail", this.IsSubscribeEmail, MyDbType.Boolean),
                    DbUtility.GetParameter("Relationship", this.Relationship, MyDbType.String)
                });

                this.ContactID = dbHelp.GetLastInsertID();
                if (this.ContactID > 0)
                {
                    addr.AddressSourceID = this.ContactID;
                    addr.Save(dbHelp);
                }
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool Insert(DbHelper dbHelp, int userid, Addresses addr)
        {
            string sql = "INSERT INTO partnercontacts( ContactPartnerID, ContactSalutation, ContactLastName, ContactFirstName, ContactTitleFr, ContactTitleEn, ContactPhone, ContactPhoneExt, ContactEmail, ContactSecEmail, ContactFax, ContactDOB, ContactSex, ContactLangPref, ContactHomePhone, ContactCellPhone, ContactPagerNo, ContactCreatedOn, ContactCreatedBy, ContactLastUpdatedOn, ContactLastUpdatedBy, ContactActive, ContactPrimary, ContactSelSpecialization, ContactNote,IsSubscribeEmail,Relationship) VALUES(";
            sql += "@ContactPartnerID, @ContactSalutation, @ContactLastName, @ContactFirstName, @ContactTitleFr, @ContactTitleEn, @ContactPhone, @ContactPhoneExt, ";
            sql += "@ContactEmail, @ContactSecEmail, @ContactFax, @ContactDOB, @ContactSex, @ContactLangPref, @ContactHomePhone, @ContactCellPhone, @ContactPagerNo, ";
            sql += "@ContactCreatedOn, @ContactCreatedBy, @ContactLastUpdatedOn, @ContactLastUpdatedBy, @ContactActive, @ContactPrimary, @ContactSelSpecialization, ";
            sql += "@ContactNote,@IsSubscribeEmail,@Relationship)";

            try
            {
                int? contactSelSpecilization = this.ContactSelSpecialization > 0 ? (int?)this.ContactSelSpecialization : null;
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ContactPartnerID", this.ContactPartnerID, MyDbType.Int),
                    DbUtility.GetParameter("ContactSalutation", this.ContactSalutation, MyDbType.String),
                    DbUtility.GetParameter("ContactLastName", this.ContactLastName, MyDbType.String),
                    DbUtility.GetParameter("ContactFirstName", this.ContactFirstName, MyDbType.String),
                    DbUtility.GetParameter("ContactTitleFr", this.ContactTitleFr, MyDbType.String),
                    DbUtility.GetParameter("ContactTitleEn", this.ContactTitleEn, MyDbType.String),
                    DbUtility.GetParameter("ContactPhone", this.ContactPhone, MyDbType.String),
                    DbUtility.GetParameter("ContactPhoneExt", this.ContactPhoneExt, MyDbType.String),
                    DbUtility.GetParameter("ContactEmail", this.ContactEmail, MyDbType.String),
                    DbUtility.GetParameter("ContactSecEmail", this.ContactSecEmail, MyDbType.String),
                    DbUtility.GetParameter("ContactFax", this.ContactFax, MyDbType.String),
                    DbUtility.GetParameter("ContactDOB", this.ContactDOB, MyDbType.DateTime),
                    DbUtility.GetParameter("ContactSex", this.ContactSex, MyDbType.String),
                    DbUtility.GetParameter("ContactLangPref", this.ContactLangPref, MyDbType.String),
                    DbUtility.GetParameter("ContactHomePhone", this.ContactHomePhone, MyDbType.String),
                    DbUtility.GetParameter("ContactCellPhone", this.ContactCellPhone, MyDbType.String),
                    DbUtility.GetParameter("ContactPagerNo", this.ContactPagerNo, MyDbType.String),
                    DbUtility.GetParameter("ContactCreatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ContactCreatedBy", userid, MyDbType.Int),
                    DbUtility.GetParameter("ContactLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ContactLastUpdatedBy", userid, MyDbType.Int),
                    DbUtility.GetParameter("ContactActive", this.ContactActive, MyDbType.Boolean),
                    DbUtility.GetParameter("ContactPrimary", this.ContactPrimary, MyDbType.Boolean),
                    DbUtility.GetIntParameter("ContactSelSpecialization", contactSelSpecilization),
                    DbUtility.GetParameter("ContactNote", this.ContactNote, MyDbType.String),
                    DbUtility.GetParameter("IsSubscribeEmail", this.IsSubscribeEmail, MyDbType.Boolean),
                    DbUtility.GetParameter("Relationship", this.Relationship, MyDbType.String),
                });

                this.ContactID = dbHelp.GetLastInsertID();
                if (this.ContactID > 0)
                {
                    addr.AddressSourceID = this.ContactID;
                    addr.Save(dbHelp);
                }
                return true;
            }
            catch
            {

                throw;
            }
        }

        public bool Insert(int userid)
        {
            StringBuilder sbQuery = new StringBuilder();

            sbQuery.Append(" INSERT INTO partnercontacts( ContactPartnerID, ContactSalutation, ContactLastName, ContactFirstName, ContactTitleFr, ContactTitleEn, ContactPhone, ContactPhoneExt, ContactEmail, ");
            sbQuery.Append(" ContactSecEmail, ContactFax, ContactDOB, ContactSex, ContactLangPref, ContactHomePhone, ContactCellPhone, ContactPagerNo, ContactCreatedOn, ContactCreatedBy,  ");
            sbQuery.Append("  ContactActive, ContactPrimary, ContactSelSpecialization, ContactNote,IsSubscribeEmail, FaceBook, Twitter, LinkedIn, ContactType ) ");
            sbQuery.Append(" VALUES( @ContactPartnerID, @ContactSalutation, @ContactLastName, @ContactFirstName, @ContactTitleFr, @ContactTitleEn, @ContactPhone, @ContactPhoneExt, @ContactEmail, @ContactSecEmail, ");
            sbQuery.Append(" @ContactFax, @ContactDOB, @ContactSex, @ContactLangPref, @ContactHomePhone, @ContactCellPhone, @ContactPagerNo, @ContactCreatedOn, @ContactCreatedBy,  ");
            sbQuery.Append(" @ContactActive, @ContactPrimary, @ContactSelSpecialization, @ContactNote,@IsSubscribeEmail, @FaceBook, @Twitter, @LinkedIn, @ContactType ) ");
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                int? contactSelSpecilization = this.ContactSelSpecialization > 0 ? (int?)this.ContactSelSpecialization : null;
                dbHelp.ExecuteNonQuery(BusinessUtility.GetString(sbQuery), CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ContactPartnerID", this.ContactPartnerID, MyDbType.Int),
                    DbUtility.GetParameter("ContactSalutation", this.ContactSalutation, MyDbType.String),
                    DbUtility.GetParameter("ContactLastName", this.ContactLastName, MyDbType.String),
                    DbUtility.GetParameter("ContactFirstName", this.ContactFirstName, MyDbType.String),
                    DbUtility.GetParameter("ContactTitleFr", this.ContactTitleFr, MyDbType.String),
                    DbUtility.GetParameter("ContactTitleEn", this.ContactTitleEn, MyDbType.String),
                    DbUtility.GetParameter("ContactPhone", this.ContactPhone, MyDbType.String),
                    DbUtility.GetParameter("ContactPhoneExt", this.ContactPhoneExt, MyDbType.String),
                    DbUtility.GetParameter("ContactEmail", this.ContactEmail, MyDbType.String),
                    DbUtility.GetParameter("ContactFax", this.ContactFax, MyDbType.String),
                    DbUtility.GetParameter("ContactDOB", this.ContactDOB, MyDbType.DateTime),
                    DbUtility.GetParameter("ContactSex", this.ContactSex, MyDbType.String),
                    DbUtility.GetParameter("ContactLangPref", this.ContactLangPref, MyDbType.String),
                    DbUtility.GetParameter("ContactHomePhone", this.ContactHomePhone, MyDbType.String),
                    DbUtility.GetParameter("ContactCellPhone", this.ContactCellPhone, MyDbType.String),
                    DbUtility.GetParameter("ContactPagerNo", this.ContactPagerNo, MyDbType.String),
                    DbUtility.GetParameter("ContactCreatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ContactCreatedBy", userid, MyDbType.Int),
                    //DbUtility.GetParameter("ContactLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    //DbUtility.GetParameter("ContactLastUpdatedBy", userid, MyDbType.Int),
                    DbUtility.GetParameter("ContactActive", this.ContactActive, MyDbType.Boolean),
                    DbUtility.GetParameter("ContactPrimary", this.ContactPrimary, MyDbType.Boolean),
                    DbUtility.GetIntParameter("ContactSelSpecialization", contactSelSpecilization),
                    DbUtility.GetParameter("ContactNote", this.ContactNote, MyDbType.String),
                    DbUtility.GetParameter("IsSubscribeEmail", this.IsSubscribeEmail, MyDbType.Boolean),
                    DbUtility.GetParameter("FaceBook", this.FaceBook, MyDbType.String),
                    DbUtility.GetParameter("Twitter", this.Twitter, MyDbType.String),
                    DbUtility.GetParameter("LinkedIn", this.LinkedIn, MyDbType.String),
                    DbUtility.GetParameter("ContactType", this.ContactType, MyDbType.String),
                    DbUtility.GetParameter("ContactSecEmail", this.ContactSecEmail, MyDbType.String)
                });
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool Update(int userid)
        {
            StringBuilder sbUpdate = new StringBuilder();
            sbUpdate.Append(" UPDATE partnercontacts SET  ContactSalutation = @ContactSalutation, ContactLastName = @ContactLastName, ContactFirstName = @ContactFirstName, ");
            sbUpdate.Append(" ContactTitleFr = @ContactTitleFr, ContactTitleEn = @ContactTitleEn, ContactPhone = @ContactPhone, ContactPhoneExt = @ContactPhoneExt, ContactEmail = @ContactEmail,  ");
            sbUpdate.Append(" ContactSecEmail = @ContactSecEmail, ContactFax = @ContactFax, ContactDOB = @ContactDOB, ContactSex = @ContactSex, ContactLangPref = @ContactLangPref,  ");
            sbUpdate.Append(" ContactHomePhone = @ContactHomePhone, ContactCellPhone = @ContactCellPhone, ContactPagerNo = @ContactPagerNo, ContactLastUpdatedOn = @ContactLastUpdatedOn, ");
            sbUpdate.Append(" ContactLastUpdatedBy = @ContactLastUpdatedBy, ContactActive = @ContactActive, ContactPrimary = @ContactPrimary, ContactSelSpecialization = @ContactSelSpecialization,  ");
            sbUpdate.Append(" ContactNote = @ContactNote, IsSubscribeEmail = @IsSubscribeEmail,  FaceBook = @FaceBook, Twitter = @Twitter,  ");
            sbUpdate.Append(" LinkedIn = @LinkedIn, ContactType = @ContactType  ");
            sbUpdate.Append(" WHERE ContactID = @ContactID AND ContactPartnerID = @ContactPartnerID");
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                int? contactSelSpecilization = this.ContactSelSpecialization > 0 ? (int?)this.ContactSelSpecialization : null;
                dbHelp.ExecuteNonQuery(BusinessUtility.GetString(sbUpdate), CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ContactPartnerID", this.ContactPartnerID, MyDbType.Int),
                    DbUtility.GetParameter("ContactSalutation", this.ContactSalutation, MyDbType.String),
                    DbUtility.GetParameter("ContactLastName", this.ContactLastName, MyDbType.String),
                    DbUtility.GetParameter("ContactFirstName", this.ContactFirstName, MyDbType.String),
                    DbUtility.GetParameter("ContactTitleFr", this.ContactTitleFr, MyDbType.String),
                    DbUtility.GetParameter("ContactTitleEn", this.ContactTitleEn, MyDbType.String),
                    DbUtility.GetParameter("ContactPhone", this.ContactPhone, MyDbType.String),
                    DbUtility.GetParameter("ContactPhoneExt", this.ContactPhoneExt, MyDbType.String),
                    DbUtility.GetParameter("ContactEmail", this.ContactEmail, MyDbType.String),
                    DbUtility.GetParameter("ContactFax", this.ContactFax, MyDbType.String),
                    DbUtility.GetParameter("ContactDOB", this.ContactDOB, MyDbType.DateTime),
                    DbUtility.GetParameter("ContactSex", this.ContactSex, MyDbType.String),
                    DbUtility.GetParameter("ContactLangPref", this.ContactLangPref, MyDbType.String),
                    DbUtility.GetParameter("ContactHomePhone", this.ContactHomePhone, MyDbType.String),
                    DbUtility.GetParameter("ContactCellPhone", this.ContactCellPhone, MyDbType.String),
                    DbUtility.GetParameter("ContactPagerNo", this.ContactPagerNo, MyDbType.String),
                    //DbUtility.GetParameter("ContactCreatedOn", DateTime.Now, MyDbType.DateTime),
                    //DbUtility.GetParameter("ContactCreatedBy", userid, MyDbType.Int),
                    DbUtility.GetParameter("ContactLastUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ContactLastUpdatedBy", userid, MyDbType.Int),
                    DbUtility.GetParameter("ContactActive", this.ContactActive, MyDbType.Boolean),
                    DbUtility.GetParameter("ContactPrimary", this.ContactPrimary, MyDbType.Boolean),
                    DbUtility.GetIntParameter("ContactSelSpecialization", contactSelSpecilization),
                    DbUtility.GetParameter("ContactNote", this.ContactNote, MyDbType.String),
                    DbUtility.GetParameter("IsSubscribeEmail", this.IsSubscribeEmail, MyDbType.Boolean),
                    DbUtility.GetParameter("FaceBook", this.FaceBook, MyDbType.String),
                    DbUtility.GetParameter("Twitter", this.Twitter, MyDbType.String),
                    DbUtility.GetParameter("LinkedIn", this.LinkedIn, MyDbType.String),
                    DbUtility.GetParameter("ContactType", this.ContactType, MyDbType.String),
                    DbUtility.GetParameter("ContactID", this.ContactID, MyDbType.Int),
                    DbUtility.GetParameter("ContactSecEmail", this.ContactSecEmail, MyDbType.String)
                    
                    
                });
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public PartnerContacts GetPrimaryContact(int partnerID)
        {
            string sql = "SELECT * FROM partnercontacts WHERE ContactPartnerID = @ContactPartnerID AND ContactPrimary=1 LIMIT 1";
            MySqlDataReader dr = null;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ContactPartnerID", partnerID, MyDbType.Int) });
                if (dr.Read())
                {
                    this.ContactActive = BusinessUtility.GetBool(dr["ContactActive"]);
                    this.ContactCellPhone = BusinessUtility.GetString(dr["ContactCellPhone"]);
                    this.ContactCreatedBy = BusinessUtility.GetInt(dr["ContactCreatedBy"]);
                    this.ContactCreatedOn = BusinessUtility.GetDateTime(dr["ContactCreatedOn"]);
                    this.ContactDOB = BusinessUtility.GetDateTime(dr["ContactDOB"]);
                    this.ContactEmail = BusinessUtility.GetString(dr["ContactEmail"]);
                    this.ContactFax = BusinessUtility.GetString(dr["ContactFax"]);
                    this.ContactFirstName = BusinessUtility.GetString(dr["ContactFirstName"]);
                    this.ContactHomePhone = BusinessUtility.GetString(dr["ContactHomePhone"]);
                    this.ContactID = BusinessUtility.GetInt(dr["ContactID"]);
                    this.ContactLangPref = BusinessUtility.GetString(dr["ContactLangPref"]);
                    this.ContactLastName = BusinessUtility.GetString(dr["ContactLastName"]);
                    this.ContactLastUpdatedBy = BusinessUtility.GetInt(dr["ContactLastUpdatedBy"]);
                    this.ContactLastUpdatedOn = BusinessUtility.GetDateTime(dr["ContactLastUpdatedOn"]);
                    this.ContactNote = BusinessUtility.GetString(dr["ContactNote"]);
                    this.ContactPagerNo = BusinessUtility.GetString(dr["ContactPagerNo"]);
                    this.ContactPartnerID = BusinessUtility.GetInt(dr["ContactPartnerID"]);
                    this.ContactPhone = BusinessUtility.GetString(dr["ContactPhone"]);
                    this.ContactPhoneExt = BusinessUtility.GetString(dr["ContactPhoneExt"]);
                    this.ContactPrimary = BusinessUtility.GetBool(dr["ContactPrimary"]);
                    this.ContactSalutation = BusinessUtility.GetString(dr["ContactSalutation"]);
                    this.ContactSecEmail = BusinessUtility.GetString(dr["ContactSecEmail"]);
                    this.ContactSelSpecialization = BusinessUtility.GetInt(dr["ContactSelSpecialization"]);
                    this.ContactSex = BusinessUtility.GetString(dr["ContactSex"]);
                    this.ContactTitleEn = BusinessUtility.GetString(dr["ContactTitleEn"]);
                    this.ContactTitleFr = BusinessUtility.GetString(dr["ContactTitleFr"]);
                    this.IsSubscribeEmail = BusinessUtility.GetBool(dr["IsSubscribeEmail"]);
                    //this.Relationship = BusinessUtility.GetString(dr["Relationship"]);
                    this.FaceBook = BusinessUtility.GetString(dr["FaceBook"]);
                    this.Twitter = BusinessUtility.GetString(dr["Twitter"]);
                    this.LinkedIn = BusinessUtility.GetString(dr["LinkedIn"]);
                    this.ContactType = BusinessUtility.GetString(dr["ContactType"]);
                }
                return this;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
                dr.Close();
            }
        }

        public PartnerContacts GetContactDetail(int contactID)
        {
            string sql = "SELECT * FROM partnercontacts WHERE ContactID = @ContactID";
            MySqlDataReader dr = null;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ContactID", contactID, MyDbType.Int) });
                if (dr.Read())
                {
                    this.ContactActive = BusinessUtility.GetBool(dr["ContactActive"]);
                    this.ContactCellPhone = BusinessUtility.GetString(dr["ContactCellPhone"]);
                    this.ContactCreatedBy = BusinessUtility.GetInt(dr["ContactCreatedBy"]);
                    this.ContactCreatedOn = BusinessUtility.GetDateTime(dr["ContactCreatedOn"]);
                    this.ContactDOB = BusinessUtility.GetDateTime(dr["ContactDOB"]);
                    this.ContactEmail = BusinessUtility.GetString(dr["ContactEmail"]);
                    this.ContactFax = BusinessUtility.GetString(dr["ContactFax"]);
                    this.ContactFirstName = BusinessUtility.GetString(dr["ContactFirstName"]);
                    this.ContactHomePhone = BusinessUtility.GetString(dr["ContactHomePhone"]);
                    this.ContactID = BusinessUtility.GetInt(dr["ContactID"]);
                    this.ContactLangPref = BusinessUtility.GetString(dr["ContactLangPref"]);
                    this.ContactLastName = BusinessUtility.GetString(dr["ContactLastName"]);
                    this.ContactLastUpdatedBy = BusinessUtility.GetInt(dr["ContactLastUpdatedBy"]);
                    this.ContactLastUpdatedOn = BusinessUtility.GetDateTime(dr["ContactLastUpdatedOn"]);
                    this.ContactNote = BusinessUtility.GetString(dr["ContactNote"]);
                    this.ContactPagerNo = BusinessUtility.GetString(dr["ContactPagerNo"]);
                    this.ContactPartnerID = BusinessUtility.GetInt(dr["ContactPartnerID"]);
                    this.ContactPhone = BusinessUtility.GetString(dr["ContactPhone"]);
                    this.ContactPhoneExt = BusinessUtility.GetString(dr["ContactPhoneExt"]);
                    this.ContactPrimary = BusinessUtility.GetBool(dr["ContactPrimary"]);
                    this.ContactSalutation = BusinessUtility.GetString(dr["ContactSalutation"]);
                    this.ContactSecEmail = BusinessUtility.GetString(dr["ContactSecEmail"]);
                    this.ContactSelSpecialization = BusinessUtility.GetInt(dr["ContactSelSpecialization"]);
                    this.ContactSex = BusinessUtility.GetString(dr["ContactSex"]);
                    this.ContactTitleEn = BusinessUtility.GetString(dr["ContactTitleEn"]);
                    this.ContactTitleFr = BusinessUtility.GetString(dr["ContactTitleFr"]);
                    this.IsSubscribeEmail = BusinessUtility.GetBool(dr["IsSubscribeEmail"]);
                    //this.Relationship = BusinessUtility.GetString(dr["Relationship"]);
                    this.FaceBook = BusinessUtility.GetString(dr["FaceBook"]);
                    this.Twitter = BusinessUtility.GetString(dr["Twitter"]);
                    this.LinkedIn = BusinessUtility.GetString(dr["LinkedIn"]);
                    this.ContactType = BusinessUtility.GetString(dr["ContactType"]);

                    this.DOB= BusinessUtility.GetDateTimeString(this.ContactDOB, DateFormat.MMddyyyy);
                }
                return this;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
                dr.Close();
            }
        }

        public DataTable GetContactList(int partnerID, string langID)
        {
            string sql = "";
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT pc.ContactID, pc.ContactLastName, pc.ContactFirstName, pc.ContactPhone, pc.ContactEmail, pc.ContactTitle{0} AS ContactTitle, sysSts.sysAppDesc AS ContactType  FROM partnercontacts AS pc  ");
            sbQuery.Append(" INNER JOIN SysStatus sysSts ON pc.ContactType = sysSts.sysAppLogicCode AND sysSts.sysAppPfx ='CU'  AND sysSts.sysAppCode ='Ctyp' AND sysSts.sysAppCodeLang ='{0}' ");
            sbQuery.Append(" WHERE ContactPartnerID = @ContactPartnerID ");

            sql = string.Format(sbQuery.ToString(), langID);

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                DataTable dtContact = dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ContactPartnerID", partnerID, MyDbType.Int) });
                return dtContact;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

    }
}

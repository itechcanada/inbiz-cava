﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web.UI.WebControls;
using System.Web;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class PartnerHistory
    {
        public int TotalSalesOrders { get; set; }
        public int TotalInvoices { get; set; }
        public double LastAmountPaid { get; set; }
        public DateTime LastAmountReceivedDate { get; set; }
        public double UnpaidAmount { get; set; }

        public void PopulateObject(int partnerID) {
            DbHelper dbHelp = new DbHelper();
            try
            {

            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string ToHtml() {
            return string.Empty;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;

using iTECH.Library;
using iTECH.Library.DataAccess.MySql; 
using iTECH.Library.Utilities;
using MySql.Data.MySqlClient;

namespace iTECH.InbizERP.BusinessLogic
{
    class BLLTextDescriptor
    {
        public static int GetNewDescriptionID(DbHelper currentDataObj)
        {
            string sqlGetMaxDescId = "SELECT MAX(DescriptionID) AS DescriptionID FROM  cdl_textdescriptor";
            object objMaxDecId = currentDataObj.GetValue(sqlGetMaxDescId, CommandType.Text, null);

            int maxDescId = objMaxDecId != DBNull.Value && objMaxDecId != null ? (int)objMaxDecId : 0;

            return maxDescId + 1;
        }

        public static int Insert(DbHelper currentDataObj, int descriptionId, int appLangID, string desc)
        {
            string sqlTextDesc = "INSERT INTO cdl_textdescriptor  (DescriptionID, AppLanguageID, TextDescription) VALUES  (@DescriptionID, @AppLanguageID, @TextDescription)";
            MySqlParameter[] parameters =
                            {
                                new MySqlParameter("@DescriptionID", descriptionId),
                                new MySqlParameter("@AppLanguageID", appLangID),
                                new MySqlParameter("@TextDescription", desc)
                            };

            return currentDataObj.ExecuteNonQuery(sqlTextDesc, CommandType.Text, parameters);
        }

        public static int Update(DbHelper currentDataObj, int descriptionId, int appLangID, string desc)
        {
            string sqlTextDesc = "UPDATE cdl_textdescriptor  SET TextDescription = @TextDescription WHERE (DescriptionID  = @DescriptionID) AND (AppLanguageID = @AppLanguageID)";

            MySqlParameter[] parameters =
                            {
                                new MySqlParameter("@DescriptionID", descriptionId),
                                new MySqlParameter("@AppLanguageID", appLangID),
                                new MySqlParameter("@TextDescription", desc)
                            };

            return currentDataObj.ExecuteNonQuery(sqlTextDesc, CommandType.Text, parameters);
        }

        public static int Delete(DbHelper currentDataObj, int descriptionID)
        {
            string sqlTextDesc = "DELETE FROM cdl_textdescriptor  WHERE (DescriptionID  = @DescriptionID)";

            MySqlParameter[] parameters =
                            {
                                new MySqlParameter("@DescriptionID", descriptionID)
                            };

            return currentDataObj.ExecuteNonQuery(sqlTextDesc, CommandType.Text, parameters);
        }

        public static Dictionary<ApplicationLanguage, string> GetTextDescription(int descriptionID)
        {
            Dictionary<ApplicationLanguage, string> objDec = new Dictionary<ApplicationLanguage, string>();

            DbHelper objDataAccess = new DbHelper();
            string sqlGetDesc = "SELECT    DescriptionID, AppLanguageID, TextDescription FROM   cdl_textdescriptor WHERE DescriptionID = @DescriptionID";
            MySqlParameter[] parameters = 
            {
                new MySqlParameter("@DescriptionID", descriptionID)
            };
            MySqlDataReader dr = null;
            try
            {                
                dr = objDataAccess.GetDataReader(sqlGetDesc, CommandType.Text, parameters);
                
                while (dr.Read())
                {
                    ApplicationLanguage al = (ApplicationLanguage)Enum.ToObject(typeof(ApplicationLanguage), dr.GetInt32("AppLanguageID"));
                    string desc = dr.GetString("TextDescription");

                    objDec.Add(al, desc);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }
                objDataAccess.CloseDatabaseConnection();
            }

            return objDec;
        }
    }
}

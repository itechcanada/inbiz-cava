﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql; 
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class BLLCategories
    {
        private int _categoryID;
        private int _textDescriptionID;
        private Dictionary<ApplicationLanguage, string> _categoryName;
        private bool _isActive;

        public BLLCategories()
        {
            _categoryID = 0;
            _categoryName = new Dictionary<ApplicationLanguage, string>();
            _isActive = true;
        }

        public int CategoryID
        {
            get { return _categoryID; }
            set { _categoryID = value; }
        }

        public int TextDescriptionID
        {
            get { return _textDescriptionID; }
            set { _textDescriptionID = value; }
        }

        public Dictionary<ApplicationLanguage, string> CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public bool Insert()
        {
            bool retVal = false;

            DbHelper objDataAccess = new DbHelper();
            try
            {
                int descriptionId = BLLTextDescriptor.GetNewDescriptionID(objDataAccess);

                foreach (ApplicationLanguage key in this._categoryName.Keys)
                {
                    BLLTextDescriptor.Insert(objDataAccess, descriptionId, (int)key, _categoryName[key]);
                }

                string sqlCategoryInsert = "INSERT INTO cdl_syscategories (CategoryName, IsActive) VALUES (@CategoryName, @IsActive)";
                MySqlParameter[] parameters1 =
                            {
                                new MySqlParameter("@CategoryName", descriptionId),
                                new MySqlParameter("@IsActive", this._isActive ? 1 : 0)
                            };

                if (objDataAccess.ExecuteNonQuery(sqlCategoryInsert, CommandType.Text, parameters1) > 0)
                {                    
                    retVal = true;
                }
            }
            catch
            {                

                throw;
            }
            finally
            {
                objDataAccess.CloseDatabaseConnection();
            }

            return retVal;
        }

        public bool Update()
        {
            bool retVal = false;

            DbHelper objDataAccess = new DbHelper();            

            try
            {
                foreach (ApplicationLanguage key in this._categoryName.Keys)
                {
                    BLLTextDescriptor.Update(objDataAccess, this._textDescriptionID, (int)key, _categoryName[key]);
                }

                string sqlCategoryInsert = "UPDATE cdl_syscategories SET CategoryName = @CategoryName, IsActive = @IsActive WHERE (CategoryID = @CategoryID)";
                MySqlParameter[] parameters1 =
                            {
                                new MySqlParameter("@CategoryID", this._categoryID),
                                new MySqlParameter("@CategoryName", _textDescriptionID),
                                new MySqlParameter("@IsActive", this._isActive ? 1 : 0)
                            };

                if (objDataAccess.ExecuteNonQuery(sqlCategoryInsert, CommandType.Text, parameters1) > 0)
                {                    
                    retVal = true;
                }
            }
            catch
            {                
                throw;
            }
            finally
            {
                objDataAccess.CloseDatabaseConnection();
            }

            return retVal;
        }

        public bool Delete()
        {
            bool retVal = false;

            DbHelper objDataAccess = new DbHelper();           

            try
            {
                BLLTextDescriptor.Delete(objDataAccess, this._textDescriptionID);

                string sqlCategoryInsert = "DELETE FROM cdl_syscategories WHERE (CategoryID = @CategoryID)";
                MySqlParameter[] parameters1 =
                            {
                                new MySqlParameter("@CategoryID", this._categoryID)
                            };

                if (objDataAccess.ExecuteNonQuery(sqlCategoryInsert, CommandType.Text, parameters1) > 0)
                {                   
                    retVal = true;
                }
            }
            catch
            {
                
                throw;
            }
            finally
            {
                objDataAccess.CloseDatabaseConnection();
            }

            return retVal;
        }

        public void FillCurrentObject(int categoryID)
        {
            DbHelper objDataAccess = new DbHelper();
            MySqlDataReader dr = null;
            MySqlParameter[] parameters =
            {
                new MySqlParameter("@CategoryID", categoryID)
            };
            try
            {
                string sqlGetCateg = "SELECT CategoryID, CategoryName, IsActive FROM cdl_syscategories WHERE (CategoryID = @CategoryID)";
                dr = objDataAccess.GetDataReader(sqlGetCateg, CommandType.Text, parameters);

                while (dr.Read())
                {
                    this._categoryID = dr.GetInt32("CategoryID");
                    this._categoryName = BLLTextDescriptor.GetTextDescription(dr.GetInt32("CategoryName"));
                    this._isActive = dr.GetInt32("IsActive") == 1;
                    this._textDescriptionID = dr.GetInt32("CategoryName");
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }

                objDataAccess.CloseDatabaseConnection();
            }
        }

        public DataTable GetAllCategories(int appLangID)
        {
            string searchSQL = "SELECT cdl_syscategories.CategoryID, cdl_syscategories.CategoryName, cdl_syscategories.IsActive, cdl_textdescriptor.AppLanguageID, cdl_textdescriptor.TextDescription FROM cdl_syscategories INNER JOIN cdl_textdescriptor ON cdl_syscategories.CategoryName = cdl_textdescriptor.DescriptionID WHERE (cdl_textdescriptor.AppLanguageID = @AppLanguageID) AND (cdl_syscategories.IsActive = 1)";

            DbHelper objDataAccess = new DbHelper();

            try
            {
                MySqlParameter[] parameters = {
                                                  new MySqlParameter("@AppLanguageID", appLangID),
                                              };
                return objDataAccess.GetDataTable(searchSQL, CommandType.Text, parameters);
            }
            catch 
            {
                
                throw;
            }
        }

        public List<string> GetAutoCompleteCategories(string startsWith, int appLang)
        {
            List<string> strList = new List<string>();
            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("SELECT cdl_textdescriptor.TextDescription FROM cdl_syscategories");
            sbSQL.Append(" INNER JOIN cdl_textdescriptor ON cdl_syscategories.CategoryName = cdl_textdescriptor.DescriptionID");
            sbSQL.Append(" WHERE (cdl_textdescriptor.AppLanguageID = @AppLanguageID)");
            sbSQL.Append(" AND cdl_textdescriptor.TextDescription LIKE CONCAT(@StartsWith, '%')");
            MySqlParameter[] parameters = {
                                              new MySqlParameter("@AppLanguageID", appLang),
                                              new MySqlParameter("@StartsWith", startsWith)
                                          };
            DbHelper objDataAccess = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = objDataAccess.GetDataReader(sbSQL.ToString(), CommandType.Text, parameters);
                while (dr.Read())
                {
                    strList.Add(dr.GetString(0));
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                    dr.Close();
                objDataAccess.CloseDatabaseConnection();
            }

            return strList;
        }

        public List<int> GetAllassignedCategories(int custid)
        {
            string searchsql = "select customerid, categoryid from cdl_customercategories where (customerid = @customerid)";

            DbHelper objdataaccess = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                MySqlParameter[] parameters = {
                                                  new MySqlParameter("@customerid", custid)
                                              };
                dr = objdataaccess.GetDataReader(searchsql, CommandType.Text, parameters);
                List<int> arrcategs = new List<int>();
                while (dr.Read())
                {
                    arrcategs.Add(dr.GetInt32("categoryid"));
                }

                return arrcategs;
            }
            catch
            {

                throw;
            }
        }

        public DataTable GetAllAssignedCategoriesToCustomer(int custId, int appLangId)
        {
            string searchSQL = "SELECT cdl_syscategories.CategoryID, cdl_syscategories.IsActive, cdl_textdescriptor.AppLanguageID, cdl_textdescriptor.TextDescription AS CategoryName, cdl_customercategories.CustomerID FROM cdl_syscategories INNER JOIN cdl_textdescriptor ON cdl_syscategories.CategoryName = cdl_textdescriptor.DescriptionID INNER JOIN cdl_customercategories ON cdl_syscategories.CategoryID = cdl_customercategories.CategoryID WHERE (cdl_textdescriptor.AppLanguageID = @AppLanguageID) AND (cdl_syscategories.IsActive = 1) AND  (cdl_customercategories.CustomerID = @CustomerID)";

            DbHelper objDataAccess = new DbHelper();

            MySqlParameter[] parameters = {
                                              new MySqlParameter("@CustomerID", custId),
                                                  new MySqlParameter("@AppLanguageID", appLangId)
                                              };
            return objDataAccess.GetDataTable(searchSQL, CommandType.Text, parameters);
        }

        public bool AssignCategoriesToCustomer(int[] arrCategoryId, int custID)
        {
            bool retval = false;

            string sqlDeleteAll = "DELETE FROM cdl_customercategories WHERE (CustomerID = @CustomerID)";
            string sqlAssignCust = "INSERT INTO cdl_customercategories (CustomerID, CategoryID) VALUES (@CustomerID, @CategoryID)";

            DbHelper objDataAccess = new DbHelper();            

            try
            {
                MySqlParameter[] p1 = {
                                         new MySqlParameter("@CustomerID", custID)
                                     };

                objDataAccess.ExecuteNonQuery(sqlDeleteAll, CommandType.Text, p1);

                foreach (int i in arrCategoryId)
                {
                    MySqlParameter[] p2 = {
                                         new MySqlParameter("@CustomerID", custID),
                                         new MySqlParameter("@CategoryID", i)
                                     };

                    objDataAccess.ExecuteNonQuery(sqlAssignCust, CommandType.Text, p2);
                }
                
                retval = true;
            }
            catch
            {                
                return false;
            }
            finally
            {
                objDataAccess.CloseDatabaseConnection();
            }

            return retval;
        }

        public int GetMaxID()
        {
            int retVal = 0;
            string SQL = "SELECT MAX(CategoryID) FROM cdl_syscategories";
            object obj = new DbHelper().GetValue(SQL, CommandType.Text, null);

            if (obj != DBNull.Value && obj != null)
            {
                int.TryParse(obj.ToString(), out retVal);
            }

            return retVal;
        }
    }
}

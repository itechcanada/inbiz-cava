﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using MySql.Data.MySqlClient;
using System.Data;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Tokens
    {
        public int TokenNo { get; set; }
        public String TokenData { get; set; }
        public int UserID { get; set; }
        public String DeviceID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime TokenExpireDate { get; set; }
        public Boolean IsActive { get; set; }
        public int CheckInType { get; set; }

        public int Insert()
        {
            string sqlInsert = "INSERT INTO tokens (TokenData, UserID, DeviceID, CreatedDate, TokenExpireDate, IsActive,CheckInType) ";
            sqlInsert += " VALUES (@TokenData, @UserID, @DeviceID, @CreatedDate, @TokenExpireDate, @IsActive,@CheckInType)";
            MySqlParameter[] p = {
                                     DbUtility.GetParameter("TokenData", this.TokenData, typeof(string)),
                                     DbUtility.GetParameter("UserID", this.UserID, typeof(int)),
                                     DbUtility.GetParameter("DeviceID", this.DeviceID, typeof(string)),
                                     DbUtility.GetParameter("CreatedDate", DateTime.Now, typeof(DateTime)),
                                     DbUtility.GetParameter("TokenExpireDate", DateTime.Now.AddDays(3), typeof(DateTime)),
                                     DbUtility.GetParameter("IsActive", this.IsActive, typeof(Boolean)),
                                     DbUtility.GetParameter("CheckInType", this.CheckInType, typeof(int)),
                                 };
            DbHelper dbHelper = new DbHelper();
            try
            {
                if (dbHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, p) > 0)
                    this.TokenNo = dbHelper.GetLastInsertID();
                return 0;
            }
            catch { throw; }
            finally { dbHelper.CloseDatabaseConnection(); }
        }
        public void ExpirePreviousToken()
        {
            string sqlInsert = "UPDATE tokens SET TokenExpireDate=NOW(), IsActive=0 ";
            sqlInsert += " WHERE UserID=@UserID AND IsActive=1";
            DbHelper dbHelper = new DbHelper();
            try
            {
                dbHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("UserID", this.UserID, typeof(int)) });
            }
            catch { throw; }
            finally { dbHelper.CloseDatabaseConnection(); }
        }

        public bool IsValidToken(string emailID, string deviceID)
        {
            string sql = "SELECT t.IsActive FROM tokens t INNER JOIN users u ON u.UserID=t.UserID WHERE u.userLoginId=@EmailID ";
            sql += "AND t.TokenExpireDate>=@TokenExpireDate AND t.IsActive=1 AND u.userActive=1 AND t.DeviceID=@DeviceID";
            MySqlParameter[] p = {
                                     DbUtility.GetParameter("EmailID", emailID, typeof(string)),
                                     DbUtility.GetParameter("TokenExpireDate", DateTime.Now, typeof(DateTime)),
                                     DbUtility.GetParameter("DeviceID", deviceID, typeof(string))
                                 };
            DbHelper dbHelper = new DbHelper();
            try
            {
                object obj = dbHelper.GetValue(sql, System.Data.CommandType.Text, p);
                if (obj != null)
                    return BusinessUtility.GetBool(obj);
                return false;
            }
            catch { throw; }
            finally { dbHelper.CloseDatabaseConnection(); }
        }

        public bool IsValidTokenForPartner(string emailID, string deviceID)
        {
            string sql = "SELECT t.IsActive FROM tokens t INNER JOIN Partners p ON p.PartnerID=t.UserID WHERE p.PartnerEmail=@EmailID ";
            sql += "AND t.TokenExpireDate>=@TokenExpireDate AND t.IsActive=1 AND p.PartnerActive=1 AND t.DeviceID=@DeviceID";
            MySqlParameter[] p = {
                                     DbUtility.GetParameter("EmailID", emailID, typeof(string)),
                                     DbUtility.GetParameter("TokenExpireDate", DateTime.Now, typeof(DateTime)),
                                     DbUtility.GetParameter("DeviceID", deviceID, typeof(string))
                                 };
            DbHelper dbHelper = new DbHelper();
            try
            {
                object obj = dbHelper.GetValue(sql, System.Data.CommandType.Text, p);
                if (obj != null)
                    return BusinessUtility.GetBool(obj);
                return false;
            }
            catch { throw; }
            finally { dbHelper.CloseDatabaseConnection(); }
        }

        public void UpdateCheckInWith(int userID, int checkInWith)
        {
            string sqlInsert = "UPDATE tokens SET CheckInType=@CheckInType";
            sqlInsert += " WHERE UserID=@UserID AND IsActive=1";
            DbHelper dbHelper = new DbHelper();
            try
            {
                dbHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]
                                                                       {
                                                                           DbUtility.GetParameter("UserID", userID, typeof(int)),
                                                                           DbUtility.GetParameter("CheckInType", checkInWith, typeof(int)),
                                                                       });
            }
            catch { throw; }
            finally { dbHelper.CloseDatabaseConnection(); }
        }
    }
}
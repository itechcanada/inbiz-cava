﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Globalization;
using System.Data;

using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ReservationCartHelper
    {
        public static ReservationCart CurrentCart
        {
            get
            {
                if (HttpContext.Current.Session["CURRENT_RESERVATION_CART"] != null)
                {
                    return (ReservationCart)HttpContext.Current.Session["CURRENT_RESERVATION_CART"];
                }
                return new ReservationCart();
            }
            private set
            {
                if (value == null)
                {
                    HttpContext.Current.Session.Remove("CURRENT_RESERVATION_CART");
                }
                else
                {
                    HttpContext.Current.Session["CURRENT_RESERVATION_CART"] = value;
                }
            }
        }

        public static void InitCart(Dictionary<string, int[]> tokenData, bool applyBedPrice, string lang)
        {
            ReservationCart cart = new ReservationCart();
            cart.BindCart(tokenData, applyBedPrice, lang);
            CurrentCart = cart;
        }

        public static void EditCartItem(int cartItemID, DateTime checkInDate, DateTime checkoutDate)
        {
            ReservationCart cart = CurrentCart;
            var foundItemCol = from fi in cart.Items where fi.CartItemID == cartItemID select fi;

            if (foundItemCol.Count() > 0)
            {
                RsvCartItem foundItem = foundItemCol.First();

                if (!ProcessReservation.IsAccommodationAvailalbe(null, checkInDate, checkoutDate, foundItem.BedID, -1, false))
                {
                    cart.DataError = CustomExceptionCodes.ACCOMMODATION_NOT_AVAILABLE;                    
                }
                else
                {
                    foundItem.CheckInDate = checkInDate;
                    foundItem.CheckOutDate = checkoutDate;
                    cart.DataError = string.Empty;
                }                
            }
            CurrentCart = cart;
        }

        public static void UpdateCartItemPrice(bool applyPrice, string appLangCode)
        {
            DbHelper dbHelp = new DbHelper(true);
            ProductDiscount prdDiscount = new ProductDiscount();
            try
            {
                ReservationCart cart = CurrentCart;
                foreach (var item in cart.Items)
                {
                    if (!applyPrice)
                    {
                        item.BedPrice = 0.0D;
                        //item.TotalBedPrice = 0.0D;
                    }
                    else
                    {
                        //To Do to get discounted price                        
                        item.BedPrice = item.ActualBedPrice;
                    }
                }
                CurrentCart = cart;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }

        }

        public static void UpdateCartItemPrice(int cartItemID, double price)
        {
            ReservationCart cart = CurrentCart;
            var foundItemCol = from fi in cart.Items where fi.CartItemID == cartItemID select fi;

            if (foundItemCol.Count() > 0)
            {
                RsvCartItem foundItem = foundItemCol.First();
                foundItem.BedPrice = price;                
            }
            CurrentCart = cart;
        }

        public static void UpdateCartItem(int cartItemID, int guestID, string sex, string relationshipToPrimaryGuest, bool isCouple)
        {
            ReservationCart cart = CurrentCart;
            var foundItemCol = from fi in cart.Items where fi.CartItemID == cartItemID select fi;

            if (foundItemCol.Count() > 0)
            {
                RsvCartItem foundItem = foundItemCol.First();
                foundItem.IsCouple = isCouple;
                foundItem.GuestID = guestID;
                foundItem.RelationshipToPrimaryGuest = relationshipToPrimaryGuest;
                foundItem.Sex = sex;
            }
            CurrentCart = cart;
        }

        public static void SetError(string message)
        {
            ReservationCart cart = CurrentCart;
            cart.DataError = message;
            CurrentCart = cart;
        }
    }

    public class ReservationCart
    {
        public string ReservationNotes { get; set; }

        public string DataError { get; internal set; }

        public List<RsvCartItem> Items { get; private set; }

        public void BindCart(Dictionary<string, int[]> tokenData, bool applyBedPrice, string lang)
        {
            List<RsvCartItem> lResult = new List<RsvCartItem>();
            DbHelper dbHelp = new DbHelper(true);
            ProductDiscount prdDiscount = new ProductDiscount();
            try
            {                
                foreach (string key in tokenData.Keys)
                {
                    int bedID = 0;
                    if (!int.TryParse(key, out bedID))
                    {
                        continue;
                    }
                    int[] arrIDates = tokenData[key];
                    DateTime fromDate = DateTime.MinValue;
                    DateTime toDate = DateTime.MinValue;
                    for (int i = 0; i < arrIDates.Length; i++)
                    {
                        if (toDate != DateTime.MinValue) //Need To Reset flag values
                        {
                            fromDate = DateTime.MinValue;
                            toDate = DateTime.MinValue;
                        }

                        if (fromDate == DateTime.MinValue)
                        {
                            DateTime.TryParseExact(arrIDates[i].ToString(), "yyyyMMdd", DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None, out fromDate);
                        }

                        //Fetch Date on Index
                        DateTime indexDate = DateTime.MinValue;
                        DateTime.TryParseExact(arrIDates[i].ToString(), "yyyyMMdd", DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None, out indexDate);

                        //Fetch Next Record of Array if it is not last index
                        if (i != arrIDates.Length - 1)
                        {
                            int iNextDate = arrIDates[i + 1];
                            DateTime nextDate = DateTime.MinValue;
                            DateTime.TryParseExact(iNextDate.ToString(), "yyyyMMdd", DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None, out nextDate);

                            //if next date is not a next day of indexDate then set a indexDate as ToDate & add item to list
                            if (nextDate.Date > indexDate.Date.AddDays(1))
                            {
                                toDate = indexDate.AddDays(1);
                                RsvCartItem b = new RsvCartItem();                                
                                b.PopulateBedInfo(dbHelp, bedID, fromDate, toDate, lang);
                                if (!applyBedPrice)
                                {
                                    b.BedPrice = 0.00D;
                                }
                                else
                                {
                                    //To Do to get discounted price                                   
                                    b.BedPrice = b.ActualBedPrice;
                                }
                                lResult.Add(b);
                            }
                        }
                        else //Consider as a last record & set a range if from date exists
                        {
                            toDate = indexDate.AddDays(1);
                            RsvCartItem b = new RsvCartItem();
                            b.PopulateBedInfo(dbHelp, bedID, fromDate, toDate, lang);
                            if (!applyBedPrice)
                            {
                                b.BedPrice = 0.00D;
                            }
                            else
                            {
                                //To Do to get discounted price                                   
                                b.BedPrice = b.ActualBedPrice;
                            }
                            lResult.Add(b);
                        }
                    }
                }
                this.Items = lResult.OrderBy(x => x.SeqBuilding).ThenBy(x => x.SeqRoom).ThenBy(x => x.SeqBed).ToList<RsvCartItem>();
                int counter = 1;
                foreach (var item in this.Items)
                {
                    item.CartItemID = counter;
                    counter++;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    public class RsvCartItem
    {
        public int BedID { get; set; }
        public string BedTitle { get; set; }
        public string BuildingTitle { get; set; }
        public string RoomTitle { get; set; }
        public int RoomType { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public int TotalNights { get { return this.CheckOutDate.Subtract(this.CheckInDate).Days; } }
        public double ActualBedPrice { get; set; }
        public double BedPrice { get; set; }
        //public double TotalBedPrice { get { return Math.Round((double)this.TotalNights * this.BedPrice); } }
        public double TotalBedPrice { get { return (double)this.TotalNights * this.BedPrice; } }
        public int GuestID { get; set; }
        public bool IsCouple { get; set; }
        public int CartItemID { get; set; }
        public string RelationshipToPrimaryGuest { get; set; }
        public string ErrorData { get; set; }
        public string Sex { get; set; }

        //Read Only Public
        public int SeqBuilding { get; private set; }
        public int SeqRoom { get; private set; }
        public int SeqBed { get; private set; }        

        public void PopulateBedInfo(DbHelper dbHelp, int bedID, DateTime checkInDate, DateTime checkOutDate, string lang)
        {
            string sql = "SELECT * FROM vw_beds WHERE BedID=@BedID";
            ProductDiscount prdDiscount = new ProductDiscount();
            MySqlDataReader dr = null;
            try
            {
                this.CheckInDate = checkInDate;
                this.CheckOutDate = checkOutDate;
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("BedID", bedID, MyDbType.Int) });
                if (dr.Read())
                {
                    this.BedID = BusinessUtility.GetInt(dr["BedID"]);
                    this.BedTitle = BusinessUtility.GetString(dr["BedTitle"]);
                    this.BuildingTitle = BusinessUtility.GetString(dr["BuildingEn"]);
                    this.RoomTitle = BusinessUtility.GetString(dr["RoomEn"]);
                    this.ActualBedPrice = BusinessUtility.GetDouble(dr["Price"]);
                    this.RoomType = BusinessUtility.GetInt(dr["RoomType"]);
                    this.SeqBuilding = BusinessUtility.GetInt(dr["SeqBuilding"]);
                    this.SeqRoom = BusinessUtility.GetInt(dr["SeqRoom"]);
                    this.SeqBed = BusinessUtility.GetInt(dr["SeqBed"]);
                }
                dr.Close();
                dr.Dispose();
                if (this.BedID > 0)
                {
                    this.ActualBedPrice = prdDiscount.CalculateAccommodationPrice(dbHelp, this.BedID, this.ActualBedPrice, this.CheckInDate, this.CheckOutDate);    
                }                
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                    dr.Dispose();
                }
            }
        }
    }
}

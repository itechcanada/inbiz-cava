﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using System.Data;

using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;

namespace iTECH.InbizERP.BusinessLogic
{
    public class PurchaseOrderCartHelper
    {
        #region Helper Members

        public static void InitCart(DbHelper dbHelp, int poID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                PurchaseOrderCart cart = new PurchaseOrderCart();
                cart.BindCart(dbHelp, poID);
                HttpContext.Current.Session["CURRENT_PO_CART"] = cart;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public static void InitLocalCart()
        {
            PurchaseOrderCart cart = new PurchaseOrderCart();
            cart.BindLocalCart();
            HttpContext.Current.Session["CURRENT_PO_CART"] = cart;
        }

        public static void Dispose()
        {
            CurrentCart = null;
        }

        public static PurchaseOrderCart CurrentCart
        {
            get
            {
                if (HttpContext.Current.Session["CURRENT_PO_CART"] != null)
                {
                    return (PurchaseOrderCart)HttpContext.Current.Session["CURRENT_PO_CART"];
                }
                return new PurchaseOrderCart();
            }
            private set
            {
                if (value == null)
                {
                    HttpContext.Current.Session.Remove("CURRENT_PO_CART");
                }
                else
                {
                    HttpContext.Current.Session["CURRENT_PO_CART"] = value;
                }
            }
        }        

        #region Items
        public static void AddToCart(PurchaseOrderCartItem item, bool findRow)
        {
            PurchaseOrderCart cart = CurrentCart;
            var foundItemCol = from fi in cart.Items where fi.ProductID == item.ProductID && findRow == true select fi;

            if (foundItemCol.Count() == 1)
            {
                PurchaseOrderCartItem foundItem = foundItemCol.First();
                foundItem.Price = item.Price;
                foundItem.ProductName = item.ProductName;
                foundItem.Quantity = foundItem.Quantity + item.Quantity;
                foundItem.Price = item.Price;
                foundItem.LandedPrice = item.LandedPrice;
                foundItem.ProductColor = item.ProductColor;
                foundItem.ProductSize = item.ProductSize;

                //Update OrderItem In Database 
                if (item.PurchaseOrderItemID > 0)
                {
                    //OrderItems oi = new OrderItems();
                    //oi.OrderItemID = foundItem.OrderItemID;
                    //oi.OrderItemDesc = foundItem.ProductName;
                    //oi.OrdProductDiscount = foundItem.Discount;
                    //oi.OrdProductDiscountType = foundItem.DisType;
                    //oi.OrdProductQty = foundItem.Quantity;
                    //oi.OrdProductTaxGrp = foundItem.TaxGrp;
                    //oi.OrdProductUnitPrice = foundItem.Price;

                    //oi.Update();
                }
            }
            else
            {                
                if (cart.Items.Count > 0)
                {
                    item.ID = cart.Items.Max(maxid => maxid.ID) + 1;   
                }
                else
                {
                    item.ID = 1;
                }
                cart.Items.Add(item);

                //Add Order Item To Database                
                if (cart.PurchaseOrderID > 0)
                {
                    //OrderItems oi = new OrderItems();
                    //oi.OrderItemDesc = item.ProductName;
                    //oi.OrdProductDiscount = item.Discount;
                    //oi.OrdProductDiscountType = item.DisType;
                    //oi.OrdProductQty = item.Quantity;
                    //oi.OrdProductTaxGrp = item.TaxGrp;
                    //oi.OrdProductUnitPrice = item.Price;
                    //oi.OrdID = cart.OrderID;
                    //oi.OrdIsItemCanceled = false;
                    //oi.OrdProductID = item.ProductID;
                    //oi.OrdGuestID = item.GuestID;

                    //oi.Insert(item.WarehouseCode, true);
                }
            }

            CurrentCart = cart;
        }

        public static void EditCartItem(PurchaseOrderCartItem item)
        {
            PurchaseOrderCart cart = CurrentCart;
            var foundItemCol = from fi in cart.Items where fi.ID == item.ID select fi;
            if (foundItemCol.Count() > 0)
            {
                PurchaseOrderCartItem foundItem = foundItemCol.First();
                foundItem.ProductID = item.ProductID;                
                foundItem.Price = item.Price;
                foundItem.LandedPrice = item.LandedPrice;
                foundItem.ProductName = item.ProductName;
                foundItem.Quantity = item.Quantity;
                foundItem.UPCCode = item.UPCCode;
                foundItem.ProductColor = item.ProductColor;
                foundItem.ProductSize = item.ProductSize;
                

                //Update OrderItem In Database                 
                if (item.PurchaseOrderItemID > 0)
                {                    
                    PurchaseOrderItems poi = new PurchaseOrderItems();
                    poi.Update(null, item.PurchaseOrderItemID, foundItem.Quantity, foundItem.Price, foundItem.WarehouseCode, foundItem.ProductName, foundItem.LandedPrice);
                    
                    //OrderItems oi = new OrderItems();
                    //oi.OrderItemID = foundItem.OrderItemID;
                    //oi.OrderItemDesc = foundItem.ProductName;
                    //oi.OrdProductDiscount = foundItem.Discount;
                    //oi.OrdProductDiscountType = foundItem.DisType;
                    //oi.OrdProductQty = foundItem.Quantity;
                    //oi.OrdProductTaxGrp = foundItem.TaxGrp;
                    //oi.OrdProductUnitPrice = foundItem.Price;

                    //oi.Update();
                }

                CurrentCart = cart;
            }
        }

        public static void DeleteCartItem(int id)
        {
            PurchaseOrderCart cart = CurrentCart;
            var foundItemCol = from fi in cart.Items where fi.ID == id select fi;
            if (foundItemCol.Count() > 0)
            {
                var fI = foundItemCol.First();
                if (fI.PurchaseOrderItemID > 0)
                {
                    //new OrderItems().Delete(fI.PurchaseOrderItemID);
                }
                cart.Items.Remove(fI);
            }
            CurrentCart = cart;
        }

        public static PurchaseOrderCartItem GetItem(int id)
        {
            var foundItemCol = from fi in CurrentCart.Items where fi.ID == id select fi;
            if (foundItemCol.Count() > 0)
            {
                return foundItemCol.First();
            }
            return null;
        }        
        #endregion        

        public static List<PurchaseOrderItems> GetItemListFromCart()
        {
            List<PurchaseOrderItems> lResult = new List<PurchaseOrderItems>();
            foreach (var item in CurrentCart.Items)
            {
                PurchaseOrderItems i = new PurchaseOrderItems();
                i.PoItemPrdId = item.ProductID;
                i.PoItemShpToWhsCode = item.WarehouseCode;
                i.PoQty = item.Quantity;
                i.PoUnitPrice = item.Price;
                i.ProdIDDesc = item.ProductName;
                i.LandedPrice = item.LandedPrice;
                i.ProductColor = item.ProductColor;
                i.ProductSize = item.ProductSize;
                lResult.Add(i);
            }
            return lResult;
        }

        public static TotalSummary GetPOOrderTotal()
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                return CalculationHelper.GetPoOrderTotal(dbHelp, CurrentCart);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        #endregion
    }

    public class PurchaseOrderCart
    {
        public int PurchaseOrderID { get; set; }

        public List<PurchaseOrderCartItem> Items { get; private set; }

        public PurchaseOrderCart() {
            this.Items = new List<PurchaseOrderCartItem>();
        }

        public void BindLocalCart() { 

        }

        public void BindCart(DbHelper dbHelp, int poID)
        {
            this.PurchaseOrderID = poID;            
            try
            {
                PurchaseOrderItems pi = new PurchaseOrderItems();
                DataTable dt = pi.GetPurchaseOrderItems(dbHelp, poID);
                int iCounter = 1;
                foreach (DataRow item in dt.Rows)
                {
                    PurchaseOrderCartItem i = new PurchaseOrderCartItem();
                    i.ID = iCounter;
                    i.ProductID = BusinessUtility.GetInt(item["productID"]);
                    i.Price = BusinessUtility.GetDouble(item["poUnitPrice"]);
                    i.ProductName = BusinessUtility.GetString(item["prdName"]);
                    i.Quantity = BusinessUtility.GetDouble(item["poQty"]);
                    i.UPCCode = BusinessUtility.GetString(item["prdUPCCode"]);
                    i.VendorID = BusinessUtility.GetInt(item["VendorID"]);
                    i.VendorName = BusinessUtility.GetString( item["VendorName"]);
                    i.WarehouseCode = BusinessUtility.GetString(item["poItemShpToWhsCode"]);
                    i.PurchaseOrderItemID = BusinessUtility.GetInt(item["PurchaseOrderItemID"]);
                    i.LandedPrice = BusinessUtility.GetDouble(item["poLandedPrice"]);
                    i.ProductColor = BusinessUtility.GetString(item["Color"]);
                    i.ProductSize = BusinessUtility.GetString(item["Size"]);

                    this.Items.Add(i);
                    iCounter++;
                }

            }
            catch
            {
                throw;
            }            
        }
    }

    public class PurchaseOrderCartItem
    {
        public int ID { get; set; }
        public int PurchaseOrderItemID { get; set; }
        public int ProductID { get; set; }
        public string UPCCode { get; set; }
        public string ProductName { get; set; }
        public int VendorID { get; set; }
        public string VendorName { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
        public double LandedPrice { get; set; }
        public string WarehouseCode { get; set; }
        public string ProductColor { get; set; }
        public string ProductSize { get; set; }

    }
}

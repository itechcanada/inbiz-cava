﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ReturnCart
    {
        public int ID { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string ProductDesc { get; set; }
        public string WhsCode { get; set; }
        public int OrderItemID { get; set; }
        public int OrderID { get; set; }
        public int InvoiceID { get; set; }
        public int InvoiceItemID { get; set; }
        public double ProductInOrderQty { get; set; }
        public double PreviousReturnedQty { get; set; }
        public double ProductToReturnQty { get; set; }
        public double ProductUnitPrice { get; set; }
        public string UPCCode { get; set; }
        public int ItemTaxGroup { get; set; }       
    }

    public class ReturnProcessCart
    {
        public int ID { get; set; }
        public int OrderProcessItemID { get; set; }
        public int InvoiceProcessItemID { get; set; }
        public string ProcessCode { get; set; }
        public double ProcessAmount { get; set; }
        public double PreviousReturnAmount { get; set; }
        public double AmountToReturn { get; set; }
        public int OrderID { get; set; }
        public int InvoiceID { get; set; }
    }
}

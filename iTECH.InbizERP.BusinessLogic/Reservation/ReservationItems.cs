﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ReservationItems
    {
        public int ReservationItemID{get; set;}
        public int ReservationID{get; set;}
        public int BedID{get; set;}
        public DateTime CheckInDate{get; set;}
        public DateTime CheckOutDate{get; set;}
        public double PricePerDay{get; set;}
        public int TotalNights { get { return Convert.ToInt32((CheckOutDate.Date - CheckInDate.Date).TotalDays); } }
        public int GuestID{get; set;}
        public bool IsCouple{get; set;}
        public string RelationshipToPrimaryGuest { get; set; }
        public bool IsCanceled { get; set; }
        public string Sex { get; set; }

        //Other Properties Not in Schema
        public int RoomType { get; set; }
        public string RoomTitle { get; set; }
        public string BuildingTitle { get; set; }
        public string BedTitle { get; set; }        

        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sqlIItes = "INSERT INTO z_reservation_items (ReservationID,BedID,CheckInDate,CheckOutDate,PricePerDay,TotalNights,GuestID,IsCouple,RelationshipToPrimaryGuest,IsCanceled,Sex) ";
            sqlIItes += " VALUES (@ReservationID,@BedID,@CheckInDate,@CheckOutDate,@PricePerDay,@TotalNights,@GuestID,@IsCouple,@RelationshipToPrimaryGuest,@IsCanceled,@Sex)";
            try
            {
                dbHelp.ExecuteNonQuery(sqlIItes, CommandType.Text, new MySqlParameter[] { 
                            DbUtility.GetParameter("ReservationID", this.ReservationID, MyDbType.Int),
                            DbUtility.GetParameter("BedID",  this.BedID, MyDbType.Int),
                            DbUtility.GetParameter("CheckInDate",  this.CheckInDate, MyDbType.DateTime),
                            DbUtility.GetParameter("CheckOutDate",  this.CheckOutDate, MyDbType.DateTime),
                            DbUtility.GetParameter("PricePerDay",  this.PricePerDay, MyDbType.Double),
                            DbUtility.GetParameter("TotalNights",  this.TotalNights, MyDbType.Int),
                            DbUtility.GetParameter("GuestID",  this.GuestID, MyDbType.Int),
                            DbUtility.GetParameter("IsCouple",  this.IsCouple, MyDbType.Boolean),
                            DbUtility.GetParameter("RelationshipToPrimaryGuest",  this.RelationshipToPrimaryGuest, MyDbType.String),
                            DbUtility.GetParameter("IsCanceled",  false, MyDbType.Boolean),
                            DbUtility.GetParameter("Sex", this.Sex, MyDbType.String)
                        });
                this.ReservationItemID = dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void AddList(DbHelper dbHelp, List<ReservationItems> lst)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                foreach (ReservationItems ri in lst)
                {
                    ri.Insert(dbHelp);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void AddList(DbHelper dbHelp, List<RsvCartItem> lst, int reservationID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
               
                foreach (RsvCartItem ri in lst)
                {
                    ReservationItems item = new ReservationItems();
                    item.BedID = ri.BedID;
                    item.CheckInDate = ri.CheckInDate;
                    item.CheckOutDate = ri.CheckOutDate;
                    item.GuestID = ri.GuestID;
                    item.IsCanceled = false;
                    item.IsCouple = ri.IsCouple;
                    item.PricePerDay = ri.BedPrice;
                    item.RelationshipToPrimaryGuest = ri.RelationshipToPrimaryGuest;
                    item.ReservationID = reservationID;
                    item.Sex = ri.Sex;
                    item.Insert(dbHelp);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public DataTable GetReservationItemInfo(int orderID, int guestID)
        {
            string sql = "SELECT ri.CheckInDate,ri.CheckOutDate,ri.GuestID,ce.FirstName,ce.LastName,ce.Sex,r.ReservationID,ri.ReservationItemID  FROM  (((orders orders";
            sql += " INNER JOIN z_reservation r ON (orders.ordID = r.SoID)) INNER JOIN orderitems oi";
            sql += " ON (oi.ordID = orders.ordID)) INNER JOIN z_reservation_items ri  ON (ri.ReservationID = r.ReservationID))";
            sql += " INNER JOIN z_customer_extended ce ON (ri.GuestID = ce.PartnerID) WHERE ri.GuestID=@GuestID AND r.SoID=@SoID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                return dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("GuestID", guestID, MyDbType.Int),
                    DbUtility.GetParameter("SoID", orderID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }        

        public DataTable GetReservationGuests(DbHelper dbHelp, int reservationID)
        {
            string sql = "SELECT DISTINCT ri.GuestID,pe.FirstName,pe.LastName, p.PartnerLongName FROM (z_customer_extended pe";
            sql += " INNER JOIN partners p ON (pe.PartnerID = p.PartnerID))";
            sql += " INNER JOIN z_reservation_items ri ON (ri.GuestID = pe.PartnerID) WHERE ri.ReservationID=@ReservationID";
            
            try
            {
                return dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ReservationID", reservationID, MyDbType.Int) 
                });
            }
            catch
            {

                throw;
            }            
        }

        public DataTable GetReservationGuests(int reservationID)
        {
            DbHelper dbHelp = new DbHelper(true);

            try
            {
                return this.GetReservationGuests(dbHelp, reservationID);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ReservationItems> GetListByReservationID(DbHelper dbHelp, int reservationID)
        {
            string sql = "SELECT ri.BedID, ri.PricePerDay AS Price, ri.CheckInDate, ri.CheckOutDate, ri.GuestID, ri.IsCouple, ri.RelationshipToPrimaryGuest, b.BedTitle, b.BuildingEn, b.RoomEn, b.RoomType, ri.ReservationItemID  FROM z_reservation_items ri INNER JOIN vw_beds b ON b.BedID=ri.BedID  WHERE ReservationID = @ReservationID";
            MySqlDataReader dr = null;
            List<ReservationItems> lREsult = new List<ReservationItems>();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ReservationID", reservationID, MyDbType.Int)
                });
                while (dr.Read())
                {
                    ReservationItems b = new ReservationItems();
                    b.BedID = BusinessUtility.GetInt(dr["BedID"]);
                    b.PricePerDay = BusinessUtility.GetDouble(dr["Price"]);
                    b.CheckInDate = BusinessUtility.GetDateTime(dr["CheckInDate"]);
                    b.CheckOutDate = BusinessUtility.GetDateTime(dr["CheckOutDate"]);

                    b.BedTitle = BusinessUtility.GetString(dr["BedTitle"]);
                    b.BuildingTitle = BusinessUtility.GetString(dr["BuildingEn"]);
                    b.RoomTitle = BusinessUtility.GetString(dr["RoomEn"]);
                    b.RoomType = BusinessUtility.GetInt(dr["RoomType"]);
                    b.GuestID = BusinessUtility.GetInt(dr["GuestID"]);
                    b.IsCouple = BusinessUtility.GetBool(dr["IsCouple"]);
                    b.RelationshipToPrimaryGuest = BusinessUtility.GetString(dr["RelationshipToPrimaryGuest"]);
                    b.ReservationItemID = BusinessUtility.GetInt(dr["ReservationItemID"]);
                    lREsult.Add(b);
                }
                return lREsult;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }
            }
        }

        public List<ReservationItems> GetListByReservationID(int reservationID)
        {
            DbHelper dbHelp = new DbHelper(true);

            try
            {
                return this.GetListByReservationID(dbHelp, reservationID);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        //public void Update()
        //{
        //    string sql = "UPDATE z_reservation_items SET CheckInDate=@CheckInDate, CheckOutDate=@CheckOutDate, PricePerDay=@PricePerDay WHERE ReservationItemID=@ReservationItemID";
        //    DbHelper dbHelp = new DbHelper();
        //    try
        //    {
        //        dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
        //            DbUtility.GetParameter("CheckInDate", this.CheckInDate, MyDbType.DateTime),
        //            DbUtility.GetParameter("CheckOutDate", this.CheckOutDate, MyDbType.DateTime),
        //            DbUtility.GetParameter("PricePerDay", this.PricePerDay, MyDbType.Double),
        //            DbUtility.GetParameter("ReservationItemID", this.ReservationItemID, MyDbType.Int)
        //        });
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public void Update()
        {
            string sql = "UPDATE z_reservation_items SET CheckInDate=@CheckInDate,CheckOutDate=@CheckOutDate,PricePerDay=@PricePerDay,";
            sql += " TotalNights=@TotalNights,IsCanceled=@IsCanceled";
            sql += " WHERE ReservationItemID=@ReservationItemID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("CheckInDate", this.CheckInDate, MyDbType.DateTime),
                    DbUtility.GetParameter("CheckOutDate", this.CheckOutDate, MyDbType.DateTime),
                    DbUtility.GetParameter("PricePerDay", this.PricePerDay, MyDbType.Double),
                    DbUtility.GetParameter("TotalNights", this.TotalNights, MyDbType.Int),
                    DbUtility.GetParameter("IsCanceled", this.IsCanceled, MyDbType.Boolean),
                    DbUtility.GetParameter("ReservationItemID", this.ReservationItemID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void ChangeGuest(int guestID, int reservationItemID)
        {
            string sqlUpdate = "UPDATE z_reservation_items SET GuestID=@GuestID WHERE ReservationItemID=@ReservationItemID";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("GuestID", guestID, MyDbType.Int),
                    DbUtility.GetParameter("ReservationItemID", reservationItemID, MyDbType.Int)
                });
                
                OrderItems oi = new OrderItems();
                oi.ChangeGuest(dbHelp, guestID, reservationItemID);

                InvoiceItems ii = new InvoiceItems();
                ii.ChangeGuest(dbHelp, guestID, reservationItemID);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void MoveReservation(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "UPDATE z_reservation_items SET BedID=@BedID, CheckInDate=@CheckInDate,CheckOutDate=@CheckOutDate,PricePerDay=@PricePerDay";           
            sql += " WHERE ReservationItemID=@ReservationItemID";
            
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("CheckInDate", this.CheckInDate, MyDbType.DateTime),
                    DbUtility.GetParameter("CheckOutDate", this.CheckOutDate, MyDbType.DateTime),
                    DbUtility.GetParameter("BedID", this.BedID, MyDbType.Int),
                    DbUtility.GetParameter("ReservationItemID", this.ReservationItemID, MyDbType.Int),
                    DbUtility.GetParameter("PricePerDay", this.PricePerDay, MyDbType.Double)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public int GetUpcommingEventsCount(int guestID)
        {
            string sql = "SELECT COUNT(oi.orderItemDesc) FROM z_reservation_items ri INNER JOIN z_reservation r ";
            sql += " ON r.ReservationID = ri.ReservationID INNER JOIN orderitems oi";
            sql += " ON oi.ordReservationItemID=ri.ReservationItemID ";
            sql += " WHERE IFNULL(ri.IsCanceled, 0)=0 AND r.SoID > 0 AND ri.CheckInDate > @CheckInDate AND GuestID=@GuestID ORDER BY ri.CheckInDate";

            DbHelper dbHelp = new DbHelper(true);
            string strData = string.Empty;
           
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("GuestID", guestID, MyDbType.Int),
                    DbUtility.GetParameter("CheckInDate", DateTime.Today, MyDbType.DateTime)
                });

                return BusinessUtility.GetInt(val);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }

        }

        public DataTable GetUpcommingEvents(int guestID, string lang)
        {
            DbHelper dbHelp = new DbHelper();
            string sql = "SELECT ri.ReservationItemID, b.RoomType, ri.CheckInDate, ri.CheckOutDate, ri.TotalNights FROM z_reservation_items ri INNER JOIN z_reservation r ";
            sql += " ON r.ReservationID = ri.ReservationID INNER JOIN orderitems oi";
            sql += " ON oi.ordReservationItemID=ri.ReservationItemID INNER JOIN vw_beds b ON b.BedID =oi.ordProductID ";
            sql += " WHERE IFNULL(ri.IsCanceled, 0)=0 AND r.SoID > 0 AND ri.CheckInDate > @CheckInDate AND GuestID=@GuestID ORDER BY ri.CheckInDate";
            try
            {
                return dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("GuestID", guestID, MyDbType.Int),
                    DbUtility.GetParameter("CheckInDate", DateTime.Today, MyDbType.DateTime)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public List<int> GetSOHistory(int guestID)
        {
            List<int> lResult = new List<int>();

            string sql = "SELECT DISTINCT ordID FROM orderitems WHERE ordGuestID=@ordGuestID";

            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ordGuestID", guestID, MyDbType.Int)
                });
                while (dr.Read())
                {
                    lResult.Add(BusinessUtility.GetInt(dr["ordID"]));                 
                }

                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }            
        }

        //public void PopulateObject(int rItemID)
        //{
        //    DbHelper dbHelp = new DbHelper();
            
        //    try
        //    {
        //        this.PopulateObject(dbHelp, rItemID);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public void PopulateObject(DbHelper dbHelp, int rItemID) {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT * FROM z_reservation_items WHERE  ReservationItemID=@ReservationItemID";
            try
            {
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ReservationItemID", rItemID, MyDbType.Int)
                }))
                {
                    while (dr.Read())
                    {
                        this.BedID = BusinessUtility.GetInt(dr["BedID"]);
                        this.CheckInDate = BusinessUtility.GetDateTime(dr["CheckInDate"]);
                        this.CheckOutDate = BusinessUtility.GetDateTime(dr["CheckOutDate"]);
                        this.GuestID = BusinessUtility.GetInt(dr["GuestID"]);
                        this.IsCanceled = BusinessUtility.GetBool(dr["IsCanceled"]);
                        this.IsCouple = BusinessUtility.GetBool(dr["IsCouple"]);
                        this.PricePerDay = BusinessUtility.GetDouble(dr["PricePerDay"]);
                        this.RelationshipToPrimaryGuest = BusinessUtility.GetString(dr["RelationshipToPrimaryGuest"]);
                        this.ReservationID = BusinessUtility.GetInt(dr["ReservationID"]);
                        this.ReservationItemID = BusinessUtility.GetInt(dr["ReservationItemID"]);
                        this.Sex = BusinessUtility.GetString(dr["Sex"]);
                    }
                    dr.Close();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //Require during Reservartion cancelation for any individual bed & customer
        //public void CancelItem(int ritemID)
        //{
        //    //Set Bed Price 0 & Set Cancel = true
        //    string sql = "UPDATE z_reservation_items SET PricePerDay=0, IsCanceled=1 WHERE ReservationItemID=@ReservationItemID";
        //    DbHelper dbHelp = new DbHelper();
        //    try
        //    {
        //        dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
        //            DbUtility.GetParameter("ReservationItemID", ritemID, MyDbType.Int)
        //        });
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}        

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();            
            sb.Append("Room Type: ").Append(Globals.GetRoomType(this.RoomType)).Append(Environment.NewLine)
                .Append("Check-In Date: ").AppendFormat("{0:MMM dd, yyyy}", this.CheckInDate).Append(Environment.NewLine)
                .Append("Check-Out Date: ").AppendFormat("{0:MMM dd, yyyy}", this.CheckOutDate).Append(Environment.NewLine)
                .Append("Total Days: ").Append(this.TotalNights);

            return sb.ToString();
        }
    }
}

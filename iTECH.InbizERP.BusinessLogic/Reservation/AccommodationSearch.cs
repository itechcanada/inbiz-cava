﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;

using Context = System.Web.HttpContext;

namespace iTECH.InbizERP.BusinessLogic
{
    public class AccommodationSearch
    {
        public List<SearchResult> SearchedData {
            get {
                if (Context.Current.Session["DATA_TO_RESERVE"] != null)
                {
                    return (List<SearchResult>)Context.Current.Session["DATA_TO_RESERVE"];
                }
                return new List<SearchResult>();
            }
            private set {
                Context.Current.Session["DATA_TO_RESERVE"] = value;
            }
        }

        public List<ReservationServices> Services
        {
            get
            {
                if (Context.Current.Session["SERVICES_TO_RESERVE"] != null)
                {
                    return (List<ReservationServices>)Context.Current.Session["SERVICES_TO_RESERVE"];
                }
                return new List<ReservationServices>();
            }
            private set
            {
                Context.Current.Session["SERVICES_TO_RESERVE"] = value;
            }
        }
        public int BuildingID { get; set; }
        public int TotalGuest { get; set; }
        public List<Guest> GuestCriteria { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public int TotalCouple { get; set; }
        /// <summary>
        /// e.g.[GuestNo#1]-[GuestNo#2]
        /// </summary>
        public List<string> CouplePair { get; set; }

        public AccommodationSearch()
        {
            this.GuestCriteria = new List<Guest>();
            this.CouplePair = new List<string>();
        }        

        public List<SearchResult> GetSearchResult(string lang)
        {
            this.SearchedData = new List<SearchResult>(); //Reset Previous Searchresults
            this.Services = new List<ReservationServices>();

            ProductDiscount prdDiscount = new ProductDiscount();
            List<SearchResult> lResult = new  List<SearchResult>();
           
            DbHelper dbHelp = new DbHelper(true);

           

            //Sql to get based on priority beds
            //string sql = "SELECT be.* FROM vw_beds be WHERE RoomType=@RoomType";
            string sql = "SELECT DISTINCT be.* FROM vw_beds be "; //place criteria for accommodation type
            sql += " LEFT OUTER JOIN z_product_sex ps ON ps.ProductID=be.BedID";
            sql += " WHERE RoomType=@RoomType";
            if (this.BuildingID > 0)
            {
                sql += " AND be.BuildingID=@BuildingID";
            }

            try
            {                                          
                //Found Rooms
                List<FoundAccommodation> foundAccommodations = new List<FoundAccommodation>(); //Accoommodations Found                
                List<Guest> lstCouple1 = new List<Guest>();
                List<Guest> lstCouple2 = new List<Guest>();
                Dictionary<string, List<Guest>> lstOtherMale = new Dictionary<string, List<Guest>>();
                Dictionary<string, List<Guest>> lstOtherFemale = new Dictionary<string, List<Guest>>();
                List<string> lstBedFound = new List<string>();
                List<int> gstOccupied = new List<int>();
                List<int> lstBedOccupied = new List<int>();
                List<int> lRoomOccupied = new List<int>();

                Dictionary<string, List<Guest>> mSexAndSameAccType = new Dictionary<string, List<Guest>>();
                Dictionary<string, List<Guest>> fSexAndSameAccType = new Dictionary<string, List<Guest>>();

                Dictionary<int, FoundAccommodation> dicGuestAcc = new Dictionary<int, FoundAccommodation>(); //Guest No, Accommodation

                Dictionary<string, List<int>> roomsForMaleOrFemale = new Dictionary<string, List<int>>();
                roomsForMaleOrFemale["M"] = new List<int>();
                roomsForMaleOrFemale["F"] = new List<int>();                

                foreach (var item in this.GuestCriteria)
                {
                    if (lstCouple1.Contains(item) || lstCouple2.Contains(item) )
                    {
                        continue;
                    }
                    if (item.CoupleWith > 0)
                    {
                        if (lstCouple1.Count <= 0)
                        {
                            lstCouple1.Add(item);
                            lstCouple1.Add(this.GuestCriteria[item.CoupleWith - 1]);
                        }
                        else if (lstCouple2.Count <=0 && this.TotalCouple == 2)
                        {
                            lstCouple2.Add(item);
                            lstCouple2.Add(this.GuestCriteria[item.CoupleWith - 1]);
                        }
                    }
                    else if(item.Sex == "M")
                    {
                        if (!mSexAndSameAccType.ContainsKey(item.AccommodationType))
                        {
                            mSexAndSameAccType[item.AccommodationType] = new List<Guest>();
                        }
                        mSexAndSameAccType[item.AccommodationType].Add(item);
                    }
                    else 
                    {
                        if (!fSexAndSameAccType.ContainsKey(item.AccommodationType))
                        {
                            fSexAndSameAccType[item.AccommodationType] = new List<Guest>();
                        }
                        fSexAndSameAccType[item.AccommodationType].Add(item);
                    }
                }

                //Couple1
                string sqlCheckBedCount = "SELECT COUNT(BedID) FROM vw_beds v WHERE v.RoomID=@RoomID";
                foreach (var gst in lstCouple1)
                {
                    if (dicGuestAcc.ContainsKey(gst.GuestNo))
                    {
                        continue;
                    }

                     string sqlInner = sql;
                     List<MySqlParameter> p = new List<MySqlParameter>();
                     if (lstBedOccupied.Count > 0)
                     {
                         sqlInner += string.Format(" AND be.BedID NOT IN({0})", StringUtil.GetJoinedString(",", lstBedOccupied.ToArray()));
                     }
                     if (gst.CoupleWith > 0)
                     {
                         sqlInner += " ORDER BY be.PriorityCouple, be.SeqBuilding, be.SeqRoom, be.SeqBed";
                     }
                     else
                     {
                         sqlInner += " ORDER BY be.ProritySingle, be.SeqBuilding, be.SeqRoom, be.SeqBed";
                     }

                     p.Add(DbUtility.GetParameter("RoomType", BusinessUtility.GetInt(gst.AccommodationType), MyDbType.Int));
                     p.Add(DbUtility.GetParameter("BuildingID", this.BuildingID, MyDbType.Int));

                     DataTable dtBeds = dbHelp.GetDataTable(sqlInner, CommandType.Text, p.ToArray());

                     int roomID = 0, bedID = 0;
                     string lastGuestGender = string.Empty;
                     Dictionary<int, List<FoundAccommodation>> lstRooms = new Dictionary<int, List<FoundAccommodation>>(); //room, beds
                     FoundAccommodation fa;
                     int bCount = 0;
                     foreach (DataRow r in dtBeds.Rows)
                     {
                         lastGuestGender = string.Empty;
                         bedID = BusinessUtility.GetInt(r["BedID"]);
                         roomID = BusinessUtility.GetInt(r["RoomID"]);

                         //Check if room has 2 or 3 beds
                         object bedCount = dbHelp.GetValue(sqlCheckBedCount, CommandType.Text, new MySqlParameter[] { 
                             DbUtility.GetParameter("RoomID", roomID, MyDbType.Int)
                         });
                         bCount = BusinessUtility.GetInt(bedCount);
                         if (bCount == 0 || bCount < 2 || bCount > 3) //Only need to allow reserved in double & triple bed room for couple
                         {
                             continue;                             
                         }

                         //Check Room available for couple or not
                         if (!ProcessReservation.IsRoomAvailableForCouple(dbHelp, roomID, this.CheckInDate, this.CheckOutDate, -1))
                         {
                             continue;
                         }

                         //CheckBed for availability
                         if (ProcessReservation.IsAccommodationAvailalbe(dbHelp, this.CheckInDate, this.CheckOutDate, bedID, -1))
                         {
                             lstBedFound.Add(bedID.ToString());
                             lastGuestGender = this.GetLastGuestGender(dbHelp, this.CheckInDate, this.CheckOutDate, roomID);
                             fa = new FoundAccommodation();
                             fa.BedID = bedID;
                             fa.RoomID = roomID;
                             fa.RoomType = BusinessUtility.GetInt(r["RoomType"]);
                             fa.FemaleExists = lastGuestGender.ToUpper().Trim() == "F";
                             fa.MaleExists = lastGuestGender.ToUpper().Trim() == "M";
                             fa.FoundForCouple = gst.CoupleWith > 0;
                             fa.GuestNo = gst.GuestNo;
                             if (!lstRooms.ContainsKey(roomID))
                             {
                                 lstRooms[roomID] = new List<FoundAccommodation>();
                             }                             
                             lstRooms[roomID].Add(fa);
                         }
                     }

                     foreach (var k in lstRooms.Keys)
                     {
                         if (lstRooms[k].Count < 2)
                         {
                             continue;
                         }
                         dicGuestAcc[gst.GuestNo] = lstRooms[k][0];
                         dicGuestAcc[gst.CoupleWith] = lstRooms[k][1];
                         lstBedOccupied.Add(dicGuestAcc[gst.GuestNo].BedID);
                         lstBedOccupied.Add(dicGuestAcc[gst.CoupleWith].BedID);
                         roomsForMaleOrFemale[gst.Sex].Add(k);
                         lRoomOccupied.Add(k);
                         break;
                     }
                }

                //Couple2
                foreach (var gst in lstCouple2)
                {
                    if (dicGuestAcc.ContainsKey(gst.GuestNo))
                    {
                        continue;
                    }

                    string sqlInner = sql;
                    List<MySqlParameter> p = new List<MySqlParameter>();
                    if (lstBedOccupied.Count > 0)
                    {
                        sqlInner += string.Format(" AND be.BedID NOT IN({0})", StringUtil.GetJoinedString(",", lstBedOccupied.ToArray()));
                    }
                    if (gst.CoupleWith > 0)
                    {
                        sqlInner += " ORDER BY be.PriorityCouple, be.SeqBuilding, be.SeqRoom, be.SeqBed";
                    }
                    else
                    {
                        sqlInner += " ORDER BY be.ProritySingle, be.SeqBuilding, be.SeqRoom, be.SeqBed";
                    }

                    p.Add(DbUtility.GetParameter("RoomType", BusinessUtility.GetInt(gst.AccommodationType), MyDbType.Int));
                    p.Add(DbUtility.GetParameter("BuildingID", this.BuildingID, MyDbType.Int));

                    DataTable dtBeds = dbHelp.GetDataTable(sqlInner, CommandType.Text, p.ToArray());

                    int roomID = 0, bedID = 0;
                    string lastGuestGender = string.Empty;
                    Dictionary<int, List<FoundAccommodation>> lstRooms = new Dictionary<int, List<FoundAccommodation>>(); //room, beds
                    FoundAccommodation fa;
                    int bCount = 0;
                    foreach (DataRow r in dtBeds.Rows)
                    {
                        lastGuestGender = string.Empty;
                        bedID = BusinessUtility.GetInt(r["BedID"]);
                        roomID = BusinessUtility.GetInt(r["RoomID"]);

                        //Check if room has 2 or 3 beds
                        object bedCount = dbHelp.GetValue(sqlCheckBedCount, CommandType.Text, new MySqlParameter[] { 
                             DbUtility.GetParameter("RoomID", roomID, MyDbType.Int)
                         });
                        bCount = BusinessUtility.GetInt(bedCount);
                        if (bCount == 0 || bCount < 2 || bCount > 3) //Only need to allow reserved in double & triple bed room for couple
                        {
                            continue;
                        }

                        //Check Room available for couple or not
                        if (!ProcessReservation.IsRoomAvailableForCouple(dbHelp, roomID, this.CheckInDate, this.CheckOutDate, -1))
                        {
                            continue;
                        }

                        //CheckBed for availability
                        if (ProcessReservation.IsAccommodationAvailalbe(dbHelp, this.CheckInDate, this.CheckOutDate, bedID, -1))
                        {
                            lstBedFound.Add(bedID.ToString());
                            lastGuestGender = this.GetLastGuestGender(dbHelp, this.CheckInDate, this.CheckOutDate, roomID);
                            fa = new FoundAccommodation();
                            fa.BedID = bedID;
                            fa.RoomID = roomID;
                            fa.RoomType = BusinessUtility.GetInt(r["RoomType"]);
                            fa.FemaleExists = lastGuestGender.ToUpper().Trim() == "F";
                            fa.MaleExists = lastGuestGender.ToUpper().Trim() == "M";
                            fa.FoundForCouple = gst.CoupleWith > 0;
                            fa.GuestNo = gst.GuestNo;
                            if (!lstRooms.ContainsKey(roomID))
                            {
                                lstRooms[roomID] = new List<FoundAccommodation>();
                            }
                            lstRooms[roomID].Add(fa);
                        }
                    }

                    foreach (var k in lstRooms.Keys)
                    {
                        if (lstRooms[k].Count < 2)
                        {
                            continue;
                        }
                        dicGuestAcc[gst.GuestNo] = lstRooms[k][0];
                        dicGuestAcc[gst.CoupleWith] = lstRooms[k][1];
                        lstBedOccupied.Add(dicGuestAcc[gst.GuestNo].BedID);
                        lstBedOccupied.Add(dicGuestAcc[gst.CoupleWith].BedID);
                        roomsForMaleOrFemale[gst.Sex].Add(k);
                        lRoomOccupied.Add(k);
                        break;
                    }
                }

                //Male single                
                foreach (var key in mSexAndSameAccType.Keys)
                {
                    foreach (var gst in mSexAndSameAccType[key])
                    {
                        if (dicGuestAcc.ContainsKey(gst.GuestNo))
                        {
                            continue;
                        }

                        string sqlInner = sql;
                        List<MySqlParameter> p = new List<MySqlParameter>();
                        if (lstBedOccupied.Count > 0)
                        {
                            sqlInner += string.Format(" AND be.BedID NOT IN({0})", StringUtil.GetJoinedString(",", lstBedOccupied.ToArray()));
                        }
                        if (lRoomOccupied.Count > 0) //Room occupied by couple in current reservation need to skip for individuals
                        {
                            sqlInner += string.Format(" AND be.RoomID NOT IN({0})", StringUtil.GetJoinedString(",", lRoomOccupied.ToArray()));
                        }
                        if (gst.AccommodationType == ((int)StatusRoomType.Dormitory).ToString()) //If dormitory only for sex specific
                        {
                            sqlInner += string.Format(" AND (ps.Sex IS NULL OR ps.Sex={0})", (int)SexForAccommodation.Male);   
                        }                        
                        if (gst.CoupleWith > 0)
                        {
                            sqlInner += " ORDER BY be.PriorityCouple, be.SeqBuilding, be.SeqRoom, be.SeqBed";
                        }
                        else
                        {
                            sqlInner += " ORDER BY be.ProritySingle, be.SeqBuilding, be.SeqRoom, be.SeqBed";
                        }

                        p.Add(DbUtility.GetParameter("RoomType", BusinessUtility.GetInt(gst.AccommodationType), MyDbType.Int));
                        p.Add(DbUtility.GetParameter("BuildingID", this.BuildingID, MyDbType.Int));

                        DataTable dtBeds = dbHelp.GetDataTable(sqlInner, CommandType.Text, p.ToArray());

                        int roomID = 0, bedID = 0;
                        string lastGuestGender = string.Empty;
                        Dictionary<int, List<FoundAccommodation>> lstRooms = new Dictionary<int, List<FoundAccommodation>>(); //room, beds
                        FoundAccommodation fa;
                        foreach (DataRow r in dtBeds.Rows)
                        {
                            lastGuestGender = string.Empty;
                            bedID = BusinessUtility.GetInt(r["BedID"]);
                            roomID = BusinessUtility.GetInt(r["RoomID"]);

                            //Check if Room Reserver by a couple then do not allow single in same room
                            if (ProcessReservation.IsRoomReservedByCouple(dbHelp, roomID, this.CheckInDate, this.CheckOutDate, -1))
                            {
                                continue;
                            }

                            //CheckBed for availability
                            if (ProcessReservation.IsAccommodationAvailalbe(dbHelp, this.CheckInDate, this.CheckOutDate, bedID, -1))
                            {
                                lstBedFound.Add(bedID.ToString());
                                lastGuestGender = this.GetLastGuestGender(dbHelp, this.CheckInDate, this.CheckOutDate, roomID);
                                fa = new FoundAccommodation();
                                fa.BedID = bedID;
                                fa.RoomID = roomID;
                                fa.RoomType = BusinessUtility.GetInt(r["RoomType"]);
                                fa.FemaleExists = lastGuestGender.ToUpper().Trim() == "F";
                                fa.MaleExists = lastGuestGender.ToUpper().Trim() == "M";
                                fa.FoundForCouple = gst.CoupleWith > 0;
                                fa.GuestNo = gst.GuestNo;
                                if (!roomsForMaleOrFemale["F"].Contains(roomID) && (fa.MaleExists || (!fa.MaleExists && !fa.FemaleExists)))
                                {
                                    if (!lstRooms.ContainsKey(roomID))
                                    {
                                        lstRooms[roomID] = new List<FoundAccommodation>();
                                    }
                                    lstRooms[roomID].Add(fa);                                    
                                }
                            }
                        }

                        int iCounter = 0;
                        bool allAccommodated = false;                        
                        //try to keep in same room for same sex
                        foreach (var k in lstRooms.Keys)
                        {                            
                            if (iCounter == mSexAndSameAccType[key].Count)
                            {
                                allAccommodated = true;
                                break;
                            }
                            if (lstRooms[k].Count < mSexAndSameAccType[key].Count)
                            {
                                continue;
                            }                            
                            for (int i = 0; i < mSexAndSameAccType[key].Count; i++)
                            {
                                dicGuestAcc[mSexAndSameAccType[key][i].GuestNo] = lstRooms[k][i];
                                lstBedOccupied.Add(dicGuestAcc[mSexAndSameAccType[key][i].GuestNo].BedID);
                                roomsForMaleOrFemale["M"].Add(lstRooms[k][i].RoomID);                                
                                iCounter++;
                            }                            
                        }

                        //If not yet accommodated then go for adjecent room for them
                        if (!allAccommodated)
                        {
                            iCounter = 0;
                            foreach (var k in lstRooms.Keys)
                            {
                                if (iCounter == mSexAndSameAccType[key].Count)
                                {
                                    break;
                                }
                                for (int i = 0; i < lstRooms[k].Count; i++)
                                {
                                    if (lstBedOccupied.Contains(lstRooms[k][i].BedID))
                                    {
                                        continue;
                                    }
                                    foreach (var gstInner in mSexAndSameAccType[key])
                                    {
                                        if (dicGuestAcc.ContainsKey(gstInner.GuestNo))
                                        {
                                            continue;
                                        }

                                        dicGuestAcc[gstInner.GuestNo] = lstRooms[k][i];
                                        lstBedOccupied.Add(dicGuestAcc[gstInner.GuestNo].BedID);
                                        roomsForMaleOrFemale["M"].Add(lstRooms[k][i].RoomID);
                                        iCounter++;
                                        break;
                                    }                                    
                                }
                            }
                        }
                    }
                }

                //Female Single
                foreach (var key in fSexAndSameAccType.Keys)
                {
                    foreach (var gst in fSexAndSameAccType[key])
                    {
                        if (dicGuestAcc.ContainsKey(gst.GuestNo))
                        {
                            continue;
                        }

                        string sqlInner = sql;
                        List<MySqlParameter> p = new List<MySqlParameter>();
                        if (lstBedOccupied.Count > 0)
                        {
                            sqlInner += string.Format(" AND be.BedID NOT IN({0})", StringUtil.GetJoinedString(",", lstBedOccupied.ToArray()));
                        }
                        if (lRoomOccupied.Count > 0) //Room occupied by couple in current reservation need to skip for individuals
                        {
                            sqlInner += string.Format(" AND be.RoomID NOT IN({0})", StringUtil.GetJoinedString(",", lRoomOccupied.ToArray()));
                        }
                        if (gst.AccommodationType == ((int)StatusRoomType.Dormitory).ToString()) //If dormitory only for sex specific
                        {
                            sqlInner += string.Format(" AND (ps.Sex IS NULL OR ps.Sex={0})", (int)SexForAccommodation.Female);
                        }  
                        if (gst.CoupleWith > 0)
                        {
                            sqlInner += " ORDER BY be.PriorityCouple, be.SeqBuilding, be.SeqRoom, be.SeqBed";
                        }
                        else
                        {
                            sqlInner += " ORDER BY be.ProritySingle, be.SeqBuilding, be.SeqRoom, be.SeqBed";
                        }

                        p.Add(DbUtility.GetParameter("RoomType", BusinessUtility.GetInt(gst.AccommodationType), MyDbType.Int));
                        p.Add(DbUtility.GetParameter("BuildingID", this.BuildingID, MyDbType.Int));

                        DataTable dtBeds = dbHelp.GetDataTable(sqlInner, CommandType.Text, p.ToArray());

                        int roomID = 0, bedID = 0;
                        string lastGuestGender = string.Empty;
                        Dictionary<int, List<FoundAccommodation>> lstRooms = new Dictionary<int, List<FoundAccommodation>>(); //room, beds
                        FoundAccommodation fa;
                        foreach (DataRow r in dtBeds.Rows)
                        {
                            lastGuestGender = string.Empty;
                            bedID = BusinessUtility.GetInt(r["BedID"]);
                            roomID = BusinessUtility.GetInt(r["RoomID"]);

                            //Check if Room Reserver by a couple then do not allow single in same room
                            if (ProcessReservation.IsRoomReservedByCouple(dbHelp, roomID, this.CheckInDate, this.CheckOutDate, -1))
                            {
                                continue;
                            }

                            //CheckBed for availability
                            if (ProcessReservation.IsAccommodationAvailalbe(dbHelp, this.CheckInDate, this.CheckOutDate, bedID, -1))
                            {
                                lstBedFound.Add(bedID.ToString());
                                lastGuestGender = this.GetLastGuestGender(dbHelp, this.CheckInDate, this.CheckOutDate, roomID);
                                fa = new FoundAccommodation();
                                fa.BedID = bedID;
                                fa.RoomID = roomID;
                                fa.RoomType = BusinessUtility.GetInt(r["RoomType"]);
                                fa.FemaleExists = lastGuestGender.ToUpper().Trim() == "F";
                                fa.MaleExists = lastGuestGender.ToUpper().Trim() == "M";
                                fa.FoundForCouple = gst.CoupleWith > 0;
                                fa.GuestNo = gst.GuestNo;
                                if (!roomsForMaleOrFemale["M"].Contains(roomID) && (fa.FemaleExists || (!fa.MaleExists && !fa.FemaleExists)))
                                {
                                    if (!lstRooms.ContainsKey(roomID))
                                    {
                                        lstRooms[roomID] = new List<FoundAccommodation>();
                                    }
                                    lstRooms[roomID].Add(fa);                                    
                                }                                                         
                            }
                        }

                        int iCounter = 0;
                        bool allAccommodated = false;                        
                        //try to keep in same room for same sex
                        foreach (var k in lstRooms.Keys)
                        {
                            if (iCounter == fSexAndSameAccType[key].Count)
                            {
                                break;
                            }
                            if (lstRooms[k].Count < fSexAndSameAccType[key].Count)
                            {
                                continue;
                            }                            
                            for (int i = 0; i < fSexAndSameAccType[key].Count; i++)
                            {
                                dicGuestAcc[fSexAndSameAccType[key][i].GuestNo] = lstRooms[k][i];
                                lstBedOccupied.Add(dicGuestAcc[fSexAndSameAccType[key][i].GuestNo].BedID);
                                roomsForMaleOrFemale["F"].Add(lstRooms[k][i].RoomID);                                
                                iCounter++;
                            }                           
                        }

                        //If not yet accommodated then go for adjecent room for them
                        if (!allAccommodated)
                        {
                            iCounter = 0;
                            foreach (var k in lstRooms.Keys)
                            {
                                if (iCounter == fSexAndSameAccType[key].Count)
                                {
                                    break;
                                }
                                for (int i = 0; i < lstRooms[k].Count; i++)
                                {
                                    if (lstBedOccupied.Contains(lstRooms[k][i].BedID))
                                    {
                                        continue;
                                    }
                                    foreach (var gstInner in fSexAndSameAccType[key])
                                    {
                                        if (dicGuestAcc.ContainsKey(gstInner.GuestNo))
                                        {
                                            continue;
                                        }

                                        dicGuestAcc[gstInner.GuestNo] = lstRooms[k][i];
                                        lstBedOccupied.Add(dicGuestAcc[gstInner.GuestNo].BedID);
                                        roomsForMaleOrFemale["F"].Add(lstRooms[k][i].RoomID);
                                        iCounter++;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                
                foreach (var gno in dicGuestAcc.Keys)
                {
                    Guest gst = this.GuestCriteria[gno - 1];
                    SearchResult sr = new SearchResult();
                    sr.GuestNo = gst.GuestNo;
                    sr.CheckInDate = this.CheckInDate;
                    sr.CheckOutDate = this.CheckOutDate;
                    sr.Sex = gst.Sex;
                    sr.IsCouple = gst.CoupleWith > 0;
                    sr.NoOfGuest = this.GuestCriteria.Count;
                    sr.BedID = dicGuestAcc[gno].BedID;
                    sr.UnitPrice = prdDiscount.CalculateAccommodationPrice(dbHelp, dicGuestAcc[gno].BedID, this.CheckInDate, this.CheckOutDate);
                    sr.Amenities = ProcessInventory.GetBedAmenities(dbHelp, dicGuestAcc[gno].BedID, lang);
                    sr.RoomType = (int)this.GetAccommodationType(dbHelp, dicGuestAcc[gno].RoomID);
                    sr.RoomTypeDesc = this.GetAccommodationTypeDesc(dbHelp, dicGuestAcc[gno].RoomType, lang);
                    lResult.Add(sr);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
            this.SearchedData = lResult;
            return lResult;
        }

        private string GetLastGuestGender(DbHelper dbHelp, DateTime cInDate, DateTime coDate, int roomID)
        {
            string sql = "SELECT GuestID FROM z_reservation_items ri INNER JOIN z_reservation r ON r.ReservationID=ri.ReservationID  ";
            sql += " INNER JOIN vw_beds b ON b.BedID=ri.BedID WHERE ";
            sql += " (@Sdate BETWEEN ri.CheckInDate AND ADDDATE(ri.CheckOutDate, -1)) ";
            sql += " AND r.ReservationStatus=@ReservationStatus AND IFNULL(ri.IsCanceled, 0)=0 AND r.SoID > 0 AND b.RoomID=@RoomID";            
            try
            {
                TimeSpan diff = coDate.Subtract(cInDate);
                int gstID = 0;
                for (int i = 0; i < diff.Days; i++) //Loop through each date between Check In to Checkout & find the guest exists in that room
                {
                    object obj = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("Sdate",  cInDate, MyDbType.DateTime),                        
                        DbUtility.GetParameter("ReservationStatus", (int)StatusReservation.Processed, MyDbType.Int),
                        DbUtility.GetParameter("RoomID", roomID, MyDbType.Int)
                    });
                    gstID = BusinessUtility.GetInt(obj);
                    if (gstID > 0) break;
                }

                if (BusinessUtility.GetInt(gstID) > 0)
                {
                    Partners part = new Partners();
                    part.PopulateObject(dbHelp, BusinessUtility.GetInt(gstID));
                    return part.ExtendedProperties.Sex;
                }

                return string.Empty;
            }
            catch
            {
                throw;
            }
        }

        private int GetAccommodationType(DbHelper dbHelp, int roomid)
        {
            try
            {
                string sql = "SELECT RoomType FROM z_accommodation_rooms WHERE RoomID=@RoomID";
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("RoomID",  roomid, MyDbType.Int)
                });
                return BusinessUtility.GetInt(val);
            }
            catch 
            {
                
                throw;
            }
        }

        private string GetAccommodationTypeDesc(DbHelper dbHelp, int roomTypeID, string lang)
        {
            try
            {
                string sql = "SELECT s.sysAppDesc FROM sysstatus s WHERE";
                sql += " s.sysAppPfx='CT' AND s.sysAppCode='dlSGp' AND s.sysAppCodeLang=@Lang AND s.sysAppLogicCode=@RoomType";                
                
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("RoomType",  roomTypeID.ToString(), MyDbType.String),
                    DbUtility.GetParameter("Lang",  lang.ToLower(), MyDbType.String)
                });
                return BusinessUtility.GetString(val);
            }
            catch 
            {
                
                throw;
            }
        }

        public void AddServiceItem(int serviceProductID, int qty)
        {
            List<ReservationServices> servList = this.Services;
            Product proc = new Product();
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                ReservationServices serv = null;
                proc.PopulateObject(dbHelp, serviceProductID);
                foreach (var item in servList)
                {
                    if (item.ProcessID == proc.ProductID)
                    {
                        serv = item;
                        break;
                    }
                }
                if (serv == null)
                {
                    serv = new ReservationServices();
                    servList.Add(serv);
                }                
                serv.ProcessCost = proc.PrdEndUserSalesPrice;
                serv.ProcessDesc = proc.PrdName;
                serv.ProcessID = proc.ProductID;
                serv.ProcessUnits += qty;
                if (serv.ProcessUnits == 0)
                {
                    servList.Remove(serv);
                }                              
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }

            this.Services = servList;
        }

        public void DeleteServiceItem(int serviceProductID)
        {
            List<ReservationServices> servList = this.Services;            
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                ReservationServices serv = null;                
                foreach (var item in servList)
                {
                    if (item.ProcessID == serviceProductID)
                    {
                        serv = item;
                        break;
                    }
                }
                if (serv != null)
                {
                    servList.Remove(serv);
                }
                this.Services = servList;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }

            this.Services = servList;
        }

        public void SetEmptyServiceItemCart()
        {
            this.Services = new List<ReservationServices>();
        }
    }

    internal class FoundAccommodation
    {
        public int RoomID { get; set; }
        public int BedID { get; set; }
        public int RoomType { get; set; }
        public bool MaleExists { get; set; }
        public bool FemaleExists { get; set; }
        public bool FoundForCouple { get; set; }
        public int GuestNo { get; set; }
    }

    public class SearchResult
    {                
        public int GuestNo { get; set; }
        public int GuestID { get; set; }
        public int BedID { get; set; }        
        public string Sex { get; set; }               
        public string Amenities{ get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }        
        public int RoomType { get; set; }
        public string RoomTypeDesc { get; set; }
        public double UnitPrice { get; set; }
        public bool IsCouple { get; set; }
        public int NoOfGuest { get; set; }

        //Read Only
        public int NoOfNight { get { return this.CheckOutDate.Subtract(this.CheckInDate).Days; } }
        //public double SubTotal { get { return Math.Round((double)this.NoOfNight * this.UnitPrice); } }        
        public double SubTotal { get { return (double)this.NoOfNight * this.UnitPrice; } }        
    }

    public class ReservationServices
    {
        public int ProcessID { get; set; }
        //public string ProcessCode { get; set; }
        public double ProcessCost { get; set; }
        public string ProcessDesc { get; set; }
        public int ProcessUnits { get; set; }
        //public double ProcessSubTotal { get { return Math.Round((double)ProcessUnits * ProcessCost, 2); } }
        public double ProcessSubTotal { get { return (double)ProcessUnits * ProcessCost; } }
    }

    public class Guest
    {
        public int GuestNo { get; set; }
        public string Sex { get; set; }
        public string AccommodationType { get; set; }
        public int CoupleWith { get; set; } //GuestNo
    }

    //public enum SpecialType
    //{
    //    None = 0,
    //    WeekEndSpecial = 1,
    //    WeekSpecial = 2,
    //    TwoWeeksSpecial = 3,
    //    OneNight = 4
    //} 
}

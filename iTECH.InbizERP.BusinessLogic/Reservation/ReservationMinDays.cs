﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ReservationSpecialDays
    {
        public int SpecialDayID { get; set; }
        public string SpecialNameEn { get; set; }
        public string SpecialNameFr { get; set; }
        public string MessageToDisplayEn { get; set; }
        public string MessageToDisplayFr { get; set; }
        public DateTime IfCheckinDate { get; set; }
        public int MinDaysToBeReserved { get; set; }
        public bool IsActive { get; set; }

        public List<int> Buildings { get; private set; }

        public ReservationSpecialDays()
        {
            this.Buildings = new List<int>();
        }

        public void Insert()
        {
            string sql = "INSERT INTO z_reservation_spdays(SpecialNameEn,SpecialNameFr,MessageToDisplayEn,MessageToDisplayFr,IfCheckinDate,MinDaysToBeReserved,IsActive) VALUES(@SpecialNameEn,@SpecialNameFr,@MessageToDisplayEn,@MessageToDisplayFr,@IfCheckinDate,@MinDaysToBeReserved,@IsActive)";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("IfCheckinDate", this.IfCheckinDate, MyDbType.DateTime),
                    DbUtility.GetParameter("MinDaysToBeReserved", this.MinDaysToBeReserved, MyDbType.Int),
                    DbUtility.GetParameter("IsActive", this.IsActive, MyDbType.Boolean),
                    DbUtility.GetParameter("SpecialNameEn", this.SpecialNameEn, MyDbType.String),
                    DbUtility.GetParameter("SpecialNameFr", this.SpecialNameFr, MyDbType.String),
                    DbUtility.GetParameter("MessageToDisplayEn", this.MessageToDisplayEn, MyDbType.String),
                    DbUtility.GetParameter("MessageToDisplayFr", this.MessageToDisplayFr, MyDbType.String)
                });
                this.SpecialDayID = dbHelp.GetLastInsertID();
                if (this.SpecialDayID > 0)
                {
                    string sqlBuild = "INSERT INTO z_reservation_spdays_building(SpecialDayID,BuildingID) VALUES({0},{1})";
                    foreach (var item in this.Buildings)
                    {
                        dbHelp.ExecuteNonQuery(string.Format(sqlBuild, this.SpecialDayID, item), CommandType.Text, null);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update()
        {
            string sql = "UPDATE z_reservation_spdays SET SpecialNameEn=@SpecialNameEn,SpecialNameFr=@SpecialNameFr,MessageToDisplayEn=@MessageToDisplayEn,MessageToDisplayFr=@MessageToDisplayFr,IfCheckinDate=@IfCheckinDate,MinDaysToBeReserved=@MinDaysToBeReserved,IsActive=@IsActive WHERE SpecialDayID=@SpecialDayID";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("IfCheckinDate", this.IfCheckinDate, MyDbType.DateTime),
                    DbUtility.GetParameter("MinDaysToBeReserved", this.MinDaysToBeReserved, MyDbType.Int),
                    DbUtility.GetParameter("IsActive", this.IsActive, MyDbType.Boolean),
                    DbUtility.GetParameter("SpecialDayID", this.SpecialDayID, MyDbType.Int),
                    DbUtility.GetParameter("SpecialNameEn", this.SpecialNameEn, MyDbType.String),
                    DbUtility.GetParameter("SpecialNameFr", this.SpecialNameFr, MyDbType.String),
                    DbUtility.GetParameter("MessageToDisplayEn", this.MessageToDisplayEn, MyDbType.String),
                    DbUtility.GetParameter("MessageToDisplayFr", this.MessageToDisplayFr, MyDbType.String)
                });
                dbHelp.ExecuteNonQuery(string.Format("DELETE FROM z_reservation_spdays_building WHERE SpecialDayID={0}", this.SpecialDayID), CommandType.Text, null);
                if (this.SpecialDayID > 0)
                {
                    string sqlBuild = "INSERT INTO z_reservation_spdays_building(SpecialDayID,BuildingID) VALUES({0},{1})";
                    foreach (var item in this.Buildings)
                    {
                        dbHelp.ExecuteNonQuery(string.Format(sqlBuild, this.SpecialDayID, item), CommandType.Text, null);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Delete(int id)
        {
            string sql = "DELETE FROM z_reservation_spdays WHERE SpecialDayID=@SpecialDayID";
            string sqlDelete = "DELETE FROM z_reservation_spdays_building WHERE SpecialDayID=@SpecialDayID";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[] {                    
                    DbUtility.GetParameter("SpecialDayID", id, MyDbType.Int)
                });
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] {                    
                    DbUtility.GetParameter("SpecialDayID", id, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(int id)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.PopulateObject(dbHelp, id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(DbHelper dbHelp, int id)
        {
            string sql = "SELECT * FROM z_reservation_spdays WHERE SpecialDayID=@SpecialDayID";            
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("SpecialDayID", id, MyDbType.Int)
                });
                if (dr.Read())
                {
                    this.IfCheckinDate = BusinessUtility.GetDateTime(dr["IfCheckinDate"]);
                    this.IsActive = BusinessUtility.GetBool(dr["IsActive"]);
                    this.SpecialDayID = BusinessUtility.GetInt(dr["SpecialDayID"]);
                    this.MinDaysToBeReserved = BusinessUtility.GetInt(dr["MinDaysToBeReserved"]);
                    this.SpecialNameEn = BusinessUtility.GetString(dr["SpecialNameEn"]);
                    this.SpecialNameFr = BusinessUtility.GetString(dr["SpecialNameFr"]);
                    this.MessageToDisplayEn = BusinessUtility.GetString(dr["MessageToDisplayEn"]);
                    this.MessageToDisplayFr = BusinessUtility.GetString(dr["MessageToDisplayFr"]);
                }
                dr.Close();
                dr.Dispose();
                dr = dbHelp.GetDataReader(string.Format("SELECT * FROM z_reservation_spdays_building  WHERE SpecialDayID={0}", id), CommandType.Text, null);
                while (dr.Read())
                {
                    this.Buildings.Add(BusinessUtility.GetInt(dr["BuildingID"]));
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                    dr.Dispose();
                }
            }
        }

        /// <summary>
        /// If returns -1 means no need to validate minimum no of dayes being  reserved
        /// </summary>
        /// <param name="dbHelp"></param>
        /// <param name="checkinDate"></param>
        /// <returns></returns>
        public int GetMinDaysToBeReserved(DbHelper dbHelp, DateTime checkinDate, int daysBeingReserved)
        {
            string sql = "SELECT MinDaysToBeReserved FROM z_reservation_spdays WHERE IfCheckinDate=@IfCheckinDate AND MinDaysToBeReserved > @DaysBeingReserved AND IsActive=1";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("IfCheckinDate",  checkinDate, MyDbType.DateTime),
                    DbUtility.GetParameter("DaysBeingReserved", daysBeingReserved, MyDbType.Int)
                });
                return BusinessUtility.GetInt(val) > 0 ? BusinessUtility.GetInt(val) : -1;
            }
            catch
            {
                throw;
            }            
        }

        /// <summary>
        /// If returns -1 means no need to validate minimum no of dayes being  reserved
        /// </summary>
        /// <param name="dbHelp"></param>
        /// <param name="checkinDate"></param>
        /// <returns></returns>
        public int GetMinDaysToBeReserved(DateTime checkinDate, int daysBeingReserved)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                return this.GetMinDaysToBeReserved(dbHelp, checkinDate, daysBeingReserved);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql(ParameterCollection pCol, DateTime searchDate, string lang)
        {
            pCol.Clear();
            string sql = "SELECT SpecialDayID, SpecialName" + lang + " AS `Name`, MessageToDisplay" + lang + " AS Message, IfCheckinDate,MinDaysToBeReserved,IsActive FROM z_reservation_spdays";
            if (searchDate != DateTime.MinValue)
            {
                sql += " WHERE IfCheckinDate = ";
                pCol.Add("@Search", BusinessUtility.GetDateTimeString(searchDate, "yyyy-MM-dd HH:mm:ss"));
            }
            return sql;
        }

        public bool IsValidChecInkDate(DateTime checkinDate, int minDaysBeingReserved, List<int> accType)
        {
            string sql = "SELECT DISTINCT z.SpecialDayID  FROM z_reservation_spdays z INNER JOIN z_reservation_spdays_building b ON b.SpecialDayID = z.SpecialDayID";
            sql += " INNER JOIN z_accommodation_rooms r ON r.BuildingID = b.BuildingID ";
            sql += " WHERE z.IfCheckinDate=@IfCheckinDate AND r.RoomType=@RoomType AND MinDaysToBeReserved > @DaysBeingReserved";
            //sql += " WHERE (@IfCheckinDate BETWEEN z.IfCheckinDate AND ADDDATE(z.IfCheckinDate, z.MinDaysToBeReserved - 1))";
            //sql += " AND r.RoomType=@RoomType AND MinDaysToBeReserved > IF(IfCheckinDate <> @IfCheckinDate, -1, @DaysBeingReserved) AND  z.IsActive = 1;";            
            DbHelper dbHelp = new DbHelper(true);           
            try
            {
                object val;
                int id = 0;
                foreach (var item in accType)
                {
                    val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                         DbUtility.GetParameter("IfCheckinDate",  checkinDate, MyDbType.DateTime),
                         DbUtility.GetParameter("RoomType",  item, MyDbType.Int),
                         DbUtility.GetParameter("DaysBeingReserved",  minDaysBeingReserved, MyDbType.Int)
                    });
                    id = BusinessUtility.GetInt(val);
                    if (id > 0)
                    {
                        this.PopulateObject(dbHelp, id);
                        return false;
                    }
                }
                               
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}

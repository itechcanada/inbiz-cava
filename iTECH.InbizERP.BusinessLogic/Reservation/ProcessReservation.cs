﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;

using Newtonsoft.Json;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProcessReservation
    {               
        //public static bool UpdateReservationStatus(int reservationID, int orderid, StatusReservation status)
        //{
        //    DbHelper dbHelp = new DbHelper(true);
        //    try
        //    {
        //        //Update Reservation Status 
        //        dbHelp.ExecuteNonQuery("UPDATE z_reservation SET ReservationStatus=@ReservationStatus, SoID=@SoID WHERE ReservationID=@ReservationID", CommandType.Text,
        //            new MySqlParameter[] { 
        //                DbUtility.GetParameter("ReservationID", reservationID, MyDbType.Int),
        //                DbUtility.GetParameter("SoID", orderid, MyDbType.Int),
        //                DbUtility.GetParameter("ReservationStatus", (int)status, MyDbType.Int)
        //            });

        //        return true;
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        //public static DataTable GetBedInfo(DateTime dt, int bedID) {
        //    string sql = "SELECT r.ReserveFor, r.SoID, ri.TotalNights, ri.GuestID, ri.IsCouple, ce.Sex, ce.LastName  FROM z_reservation_items ri INNER JOIN z_reservation r ON ";
        //    sql += " r.ReservationID=ri.ReservationID INNER JOIN z_customer_extended ce ON ce.PartnerID=ri.GuestID ";
        //    sql += " WHERE ri.IsCanceled = 0 AND ";
        //    sql += " (ri.CheckInDate <= @Sdate AND ri.CheckOutDate > @Sdate) AND r.ReservationStatus=@ReservationStatus AND ri.BedID=@BedID AND r.SoID > 0";
            
        //    DbHelper dbHelp = new DbHelper();                   
        //    try
        //    {                          
        //        return dbHelp.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { 
        //            DbUtility.GetParameter("Sdate",  dt, MyDbType.DateTime),
        //            DbUtility.GetParameter("ReservationStatus", (int)StatusReservation.Processed, MyDbType.Int),
        //            DbUtility.GetParameter("BedID", bedID, MyDbType.Int)
        //        });               
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public static ReservationDescription GetReservationDescription(DateTime onDate, int bedID)
        {
            string sql = "SELECT r.ReserveFor, r.SoID, ri.TotalNights, ri.GuestID, ri.IsCouple, ce.Sex, ce.LastName, ce.FirstName, ce.SpirtualName,ri.ReservationItemID, ";
            sql += " CASE IFNULL(i.invID, 0) WHEN 0 THEN 0 ELSE 1 END AS IsInvoiceCreated";
            sql += " FROM z_reservation_items ri INNER JOIN z_reservation r ON ";
            sql += " r.ReservationID=ri.ReservationID INNER JOIN z_customer_extended ce ON ce.PartnerID=ri.GuestID INNER JOIN partners p ON p.PartnerID=ce.PartnerID AND p.PartnerActive=1";
            sql += " LEFT OUTER JOIN invoices i ON i.invForOrderNo=r.SoID";
            sql += " WHERE ri.IsCanceled = 0 AND ";
            sql += " (ri.CheckInDate <= @Sdate AND ri.CheckOutDate > @Sdate) AND r.ReservationStatus=@ReservationStatus AND ri.BedID=@BedID AND r.SoID > 0";

            DbHelper dbHelp = new DbHelper();
            ReservationDescription rsvDesc = null;
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("Sdate",  onDate, MyDbType.DateTime),
                    DbUtility.GetParameter("ReservationStatus", (int)StatusReservation.Processed, MyDbType.Int),
                    DbUtility.GetParameter("BedID", bedID, MyDbType.Int)
                });
                if (dr.Read())
                {
                    rsvDesc = new ReservationDescription();
                    rsvDesc.FirstName = BusinessUtility.GetString(dr["FirstName"]);
                    rsvDesc.GuestID = BusinessUtility.GetInt(dr["GuestID"]);
                    rsvDesc.IsCouple = BusinessUtility.GetBool(dr["IsCouple"]);
                    rsvDesc.LastName = BusinessUtility.GetString(dr["LastName"]);
                    rsvDesc.ReserveFor = BusinessUtility.GetInt(dr["ReserveFor"]);
                    rsvDesc.Sex = BusinessUtility.GetString(dr["Sex"]);
                    rsvDesc.SOID = BusinessUtility.GetInt(dr["SOID"]);
                    rsvDesc.SpirtualName = BusinessUtility.GetString(dr["SpirtualName"]);
                    rsvDesc.TotalNights = BusinessUtility.GetInt(dr["TotalNights"]);
                    rsvDesc.ReservationItemID = BusinessUtility.GetInt(dr["ReservationItemID"]);
                    rsvDesc.IsInvoiceCreated = BusinessUtility.GetBool(dr["IsInvoiceCreated"]);
                }
                return rsvDesc;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public static DataTable SearchGuest(string fName, string lName, string email, string phone, StatusGuestType guestType, int rid, bool isSalesRestricted) {
            string sql = "SELECT * FROM vw_guests g ";
            if (isSalesRestricted)
            {
                sql += " INNER JOIN salesrepcustomer s on s.CustomerID=g.PartnerID";
            }
            sql += " WHERE 1=1 ";
            List<MySqlParameter> pList = new List<MySqlParameter>();
            if (!string.IsNullOrEmpty(fName))
            {
                sql += " AND FirstName LIKE CONCAT('%',IFNULL(@FirstName ,''),'%')";
                pList.Add(DbUtility.GetParameter("FirstName", fName, MyDbType.String));
            }
            if (!string.IsNullOrEmpty(lName))
            {
                sql += " AND LastName LIKE CONCAT('%',IFNULL(@LastName ,''),'%')";
                pList.Add(DbUtility.GetParameter("LastName", lName, MyDbType.String));
            }
            if (!string.IsNullOrEmpty(email))
            {
                sql += " AND PartnerEmail LIKE CONCAT('%',IFNULL(@PartnerEmail ,''),'%')";
                pList.Add(DbUtility.GetParameter("PartnerEmail", email, MyDbType.String));
            }
            if (!string.IsNullOrEmpty(phone))
            {
                sql += " AND PartnerPhone LIKE CONCAT('%',IFNULL(@PartnerPhone ,''),'%')";
                pList.Add(DbUtility.GetParameter("PartnerPhone", phone, MyDbType.String));
            }

            if (guestType == StatusGuestType.Guest || guestType == StatusGuestType.CourseParticipants)
            {
                sql += string.Format(" AND (GuestType={0} OR GuestType={1})", (int)StatusGuestType.Guest, (int)StatusGuestType.CourseParticipants);
            }
            else
            {
                sql += " AND GuestType=@GuestType";
                pList.Add(DbUtility.GetParameter("GuestType", (int)guestType, MyDbType.Int));
            }            

            sql += " AND PartnerID NOT IN (SELECT GuestID FROM z_reservation_items WHERE ReservationID=@ReservationID)";
            sql += " ORDER BY LastName";
            pList.Add(DbUtility.GetParameter("ReservationID", rid, MyDbType.Int));
            DbHelper dbHelp = new DbHelper(true);            
            List<CustomerExtended> lResult = new List<CustomerExtended>();
            try
            {
                return dbHelp.GetDataTable(sql, CommandType.Text, pList.ToArray());                       
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static DataTable SearchGuest(string keyword, StatusGuestType guestType, int rid, bool isSalesRestricted)
        {
            string sql = "SELECT * FROM vw_guests g ";
            if (isSalesRestricted)
            {
                sql += " INNER JOIN salesrepcustomer s on s.CustomerID=g.PartnerID";
            }
            sql += " WHERE 1=1 ";
            List<MySqlParameter> pList = new List<MySqlParameter>();
            if (!string.IsNullOrEmpty(keyword))
            {
                sql += " AND (FirstName LIKE CONCAT('%',IFNULL(@Keyword ,''),'%')";
                sql += " OR LastName LIKE CONCAT('%',IFNULL(@Keyword ,''),'%')";
                sql += " OR PartnerEmail LIKE CONCAT('%',IFNULL(@Keyword ,''),'%')";
                sql += " OR PartnerPhone LIKE CONCAT('%',IFNULL(@Keyword ,''),'%'))";
                pList.Add(DbUtility.GetParameter("Keyword", keyword, MyDbType.String));
            }

            if (guestType == StatusGuestType.Guest || guestType == StatusGuestType.CourseParticipants)
            {
                sql += string.Format(" AND (GuestType={0} OR GuestType={1})", (int)StatusGuestType.Guest, (int)StatusGuestType.CourseParticipants);
            }
            else
            {
                sql += " AND GuestType=@GuestType";
                pList.Add(DbUtility.GetParameter("GuestType", (int)guestType, MyDbType.Int));
            }  

            sql += " AND PartnerID NOT IN (SELECT GuestID FROM z_reservation_items WHERE ReservationID=@ReservationID)";
            sql += " ORDER BY LastName";
            pList.Add(DbUtility.GetParameter("ReservationID", rid, MyDbType.Int));
            DbHelper dbHelp = new DbHelper(true);
            List<CustomerExtended> lResult = new List<CustomerExtended>();
            try
            {
                return dbHelp.GetDataTable(sql, CommandType.Text, pList.ToArray());
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }        

        public static int[] SearchedGuestIDs(string fName, string lName, string email, string phone, int rid) {
            string sql = "SELECT DISTINCT PartnerID FROM vw_guests WHERE 1=1 ";
            List<MySqlParameter> pList = new List<MySqlParameter>();
            if (!string.IsNullOrEmpty(fName)) {
                sql += " AND FirstName LIKE CONCAT('%',IFNULL(@FirstName ,''),'%')";
                pList.Add(DbUtility.GetParameter("FirstName", fName, MyDbType.String));
            }
            if (!string.IsNullOrEmpty(lName))
            {
                sql += " AND LastName LIKE CONCAT('%',IFNULL(@LastName ,''),'%')";
                pList.Add(DbUtility.GetParameter("LastName", lName, MyDbType.String));
            }
            if (!string.IsNullOrEmpty(email))
            {
                sql += " AND PartnerEmail LIKE CONCAT('%',IFNULL(@PartnerEmail ,''),'%')";
                pList.Add(DbUtility.GetParameter("PartnerEmail", email, MyDbType.String));
            }
            if (!string.IsNullOrEmpty(phone))
            {
                sql += " AND PartnerPhone LIKE CONCAT('%',IFNULL(@PartnerPhone ,''),'%')";
                pList.Add(DbUtility.GetParameter("PartnerPhone", phone, MyDbType.String));
            }
            sql += " AND PartnerID NOT IN (SELECT GuestID FROM z_reservation_items WHERE ReservationID=@ReservationID)";
            sql += " ORDER BY LastName";
            pList.Add(DbUtility.GetParameter("ReservationID", rid, MyDbType.Int));
            DbHelper dbHelp = new DbHelper(true);
            MySqlDataReader dr = null;
            List<int> lResult = new List<int>();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, pList.ToArray());
                while (dr.Read())
                {
                    lResult.Add(BusinessUtility.GetInt(dr["PartnerID"]));
                }
                return lResult.ToArray();
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        #region Accommodation Availability
        /// <summary>
        /// Validate Blackouts & Availability both
        /// </summary>
        /// <param name="lstBeds"></param>
        /// <returns></returns>
        public static bool IsAccommodationAvailalbe(DbHelper dbHelp, List<RsvCartItem> lstCartBeds)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                foreach (RsvCartItem item in lstCartBeds)
                {
                    if (!ProcessReservation.IsAccommodationAvailalbe(dbHelp, item.CheckInDate, item.CheckOutDate, item.BedID, -1))
                    {
                        return false;
                    }                    
                }
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public static bool IsAccommodationAvailalbe(DbHelper dbHelp, List<ReservationItems> lstCartBeds)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                foreach (ReservationItems item in lstCartBeds)
                {
                    if (!ProcessReservation.IsAccommodationAvailalbe(dbHelp, item.CheckInDate, item.CheckOutDate, item.BedID, -1))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        /// <summary>
        /// Has Option to exclude validation of blackouts
        /// </summary>
        /// <param name="lstBeds"></param>
        /// <param name="validateBlackouts"></param>
        /// <returns></returns>
        public static bool IsAccommodationAvailalbe(DbHelper dbHelp, List<RsvCartItem> lstCartBeds, bool validateBlackouts)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                foreach (RsvCartItem item in lstCartBeds)
                {
                    if (!ProcessReservation.IsAccommodationAvailalbe(dbHelp, item.CheckInDate, item.CheckOutDate, item.BedID, -1, validateBlackouts))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }



        /// <summary>
        /// Has Option to exclude validation of blackouts
        /// </summary>
        /// <param name="dbHelp"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="bedID"></param>
        /// <param name="excludeReservationItemID"></param>
        /// <param name="validateBlackouts"></param>
        /// <returns></returns>
        public static bool IsAccommodationAvailalbe(DbHelper dbHelp, DateTime fromDate, DateTime toDate, int bedID, int excludeReservationItemID, bool validateBlackouts)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sqlBlackOut = "SELECT COUNT(*) FROM z_product_blackouts WHERE ((@Sdate >= StartDate AND @Sdate <= EndDate) OR (@Edate >= StartDate AND @Edate <= EndDate)) AND ProductID=@ProductID";

            //string sql = "SELECT COUNT(*) FROM z_reservation_items ri INNER JOIN z_reservation r ON r.ReservationID=ri.ReservationID WHERE ";
            //sql += " (@Sdate BETWEEN ri.CheckInDate AND ADDDATE(ri.CheckOutDate, -1)) ";
            //sql += " AND r.ReservationStatus=@ReservationStatus AND ri.BedID=@BedID AND IFNULL(ri.IsCanceled, 0)=0 AND r.SoID > 0";
            //sql += " AND ri.ReservationItemID <> IFNULL(@ReservationItemID, 0)";            

            string sql = "SELECT COUNT(*)";
            sql += " FROM z_reservation_items ri INNER JOIN z_reservation r ON r.ReservationID = ri.ReservationID";
            sql += " WHERE ((@Sdate BETWEEN ri.CheckInDate AND ADDDATE(ri.CheckOutDate, -1))";
            sql += " OR (@Tdate BETWEEN ADDDATE(ri.CheckInDate, 1) AND ri.CheckOutDate)";
            sql += " OR (ri.CheckInDate BETWEEN @Sdate AND ADDDATE(@Tdate, -1))";
            sql += " OR (ri.CheckOutDate BETWEEN ADDDATE(@Sdate,  1) AND @Tdate))";
            sql += string.Format(" AND r.ReservationStatus = {0}", (int)StatusReservation.Processed);
            sql += " AND ri.BedID =@BedID";
            sql += " AND IFNULL(ri.IsCanceled, 0)=0 AND r.SoID > 0";
            sql += " AND ri.ReservationItemID <> IFNULL(@ReservationItemID, 0)";

            try
            {
                //first need to verify is daterange fall between black outs dates
                if (validateBlackouts)
                {
                    object blCnt = dbHelp.GetValue(sqlBlackOut, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("Sdate",  fromDate.Date, MyDbType.DateTime),
                        DbUtility.GetParameter("Edate",  toDate.Date, MyDbType.DateTime),
                        DbUtility.GetParameter("ProductID", bedID, MyDbType.Int)
                    });

                    if (BusinessUtility.GetInt(blCnt) > 0)
                    {
                        return false;
                    }
                }

                int? exRsvItmID = excludeReservationItemID > 0 ? (int?)excludeReservationItemID : null;

                object count = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("Sdate",  fromDate, MyDbType.DateTime),  
                    DbUtility.GetParameter("Tdate",  toDate, MyDbType.DateTime),  
                    DbUtility.GetParameter("BedID", bedID, MyDbType.Int),
                    DbUtility.GetIntParameter("ReservationItemID", exRsvItmID)
                });

                return BusinessUtility.GetInt(count) <= 0;
               
                //If not black out then need to check if it reserved or not
                //int? exRsvItmID = excludeReservationItemID > 0 ? (int?)excludeReservationItemID : null;
                //TimeSpan diff = toDate.Subtract(fromDate);
                //for (int i = 0; i < diff.Days; i++)
                //{
                //    DateTime dateToValidate = fromDate.AddDays(i);
                //    object count = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                //        DbUtility.GetParameter("Sdate",  dateToValidate, MyDbType.DateTime),                        
                //        DbUtility.GetParameter("ReservationStatus", (int)StatusReservation.Processed, MyDbType.Int),
                //        DbUtility.GetParameter("BedID", bedID, MyDbType.Int),
                //        DbUtility.GetIntParameter("ReservationItemID", exRsvItmID),
                //        DbUtility.GetParameter("CheckCouple", excludeIfHasCouple, MyDbType.Boolean)
                //    });

                //    if (BusinessUtility.GetInt(count) > 0)
                //    {
                //        return false;
                //    }                   
                //}
                //return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public static bool IsAccommodationAvailalbe(DbHelper dbHelp, DateTime fromDate, DateTime toDate, int bedID, int excludeReservationItemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                return ProcessReservation.IsAccommodationAvailalbe(dbHelp, fromDate, toDate, bedID, excludeReservationItemID, true);
            }
            catch 
            {
                
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //public static bool IsAccommodationAvailalbe(DateTime fromDate, DateTime toDate, int bedID, int excludeReservationItemID)
        //{
        //    DbHelper dbHelp = new DbHelper(true);
        //    try
        //    {
        //        return ProcessReservation.IsAccommodationAvailalbe(dbHelp, fromDate, toDate, bedID, excludeReservationItemID);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        //public static bool IsAccommodationAvailalbe(DateTime fromDate, DateTime toDate, int bedID, int excludeReservationItemID, bool validateBlackouts)
        //{
        //    DbHelper dbHelp = new DbHelper(true);
        //    try
        //    {
        //        return ProcessReservation.IsAccommodationAvailalbe(dbHelp, fromDate, toDate, bedID, excludeReservationItemID, validateBlackouts);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public static string IsBlackoutReason(DbHelper dbHelp, int bedID, DateTime dateToValidate, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            string sql = "SELECT st." + Globals.GetFieldName("Reason", lang) + " AS ReasonDesc FROM z_product_blackouts pb ";
            sql += " INNER JOIN z_blackout_reasons st ON st.ID = pb.Reason ";
            sql += " WHERE pb.ProductID = @ProductID AND @DateValidate BETWEEN pb.StartDate AND pb.EndDate LIMIT 1";
            
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("Lang", lang, MyDbType.String),
                    DbUtility.GetParameter("ProductID", bedID, MyDbType.Int),
                    DbUtility.GetParameter("DateValidate", dateToValidate, MyDbType.DateTime)
                });

                return BusinessUtility.GetString(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public static bool IsRoomReservedByCouple(DbHelper dbHelp, int roomID, DateTime sDate, DateTime tDate, int excludeReservationItemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT COUNT(ri.IsCouple)";
            sql += " FROM z_reservation_items ri INNER JOIN z_reservation r ON r.ReservationID = ri.ReservationID";
            sql += " INNER JOIN vw_beds b ON b.BedID=ri.BedID";
            sql += " WHERE ((@Sdate BETWEEN ri.CheckInDate AND ADDDATE(ri.CheckOutDate, -1))";
            sql += " OR (@Tdate BETWEEN ADDDATE(ri.CheckInDate, 1) AND ri.CheckOutDate)";
            sql += " OR (ri.CheckInDate BETWEEN @Sdate AND ADDDATE(@Tdate, -1))";
            sql += " OR (ri.CheckOutDate BETWEEN ADDDATE(@Sdate,  1) AND @Tdate))";
            sql += string.Format(" AND r.ReservationStatus = {0}", (int)StatusReservation.Processed);
            sql += " AND b.RoomID =@RoomID AND ri.IsCouple = 1";
            sql += " AND IFNULL(ri.IsCanceled, 0)=0 AND r.SoID > 0";
            sql += " AND ri.ReservationItemID <> IFNULL(@ReservationItemID, 0)";

            try
            {
                int? exRsvItmID = excludeReservationItemID > 0 ? (int?)excludeReservationItemID : null;

                object count = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("Sdate",  sDate, MyDbType.DateTime),  
                    DbUtility.GetParameter("Tdate",  tDate, MyDbType.DateTime),  
                    DbUtility.GetParameter("RoomID", roomID, MyDbType.Int),
                    DbUtility.GetIntParameter("ReservationItemID", exRsvItmID)
                });

                return BusinessUtility.GetInt(count) > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public static bool IsRoomAvailableForCouple(DbHelper dbHelp, int roomID, DateTime sDate, DateTime tDate, int excludeReservationItemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT COUNT(*)";
            sql += " FROM z_reservation_items ri INNER JOIN z_reservation r ON r.ReservationID = ri.ReservationID";
            sql += " INNER JOIN vw_beds b ON b.BedID=ri.BedID";
            sql += " WHERE ((@Sdate BETWEEN ri.CheckInDate AND ADDDATE(ri.CheckOutDate, -1))";
            sql += " OR (@Tdate BETWEEN ADDDATE(ri.CheckInDate, 1) AND ri.CheckOutDate)";
            sql += " OR (ri.CheckInDate BETWEEN @Sdate AND ADDDATE(@Tdate, -1))";
            sql += " OR (ri.CheckOutDate BETWEEN ADDDATE(@Sdate,  1) AND @Tdate))";
            sql += string.Format(" AND r.ReservationStatus = {0}", (int)StatusReservation.Processed);
            sql += " AND b.RoomID =@RoomID";
            sql += " AND IFNULL(ri.IsCanceled, 0)=0 AND r.SoID > 0";
            sql += " AND ri.ReservationItemID <> IFNULL(@ReservationItemID, 0)";

            try
            {
                int? exRsvItmID = excludeReservationItemID > 0 ? (int?)excludeReservationItemID : null;

                object count = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("Sdate",  sDate, MyDbType.DateTime),  
                    DbUtility.GetParameter("Tdate",  tDate, MyDbType.DateTime),  
                    DbUtility.GetParameter("RoomID", roomID, MyDbType.Int),
                    DbUtility.GetIntParameter("ReservationItemID", exRsvItmID)
                });

                return BusinessUtility.GetInt(count) <= 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;


namespace iTECH.InbizERP.BusinessLogic
{
    public class SecurityModules
    {
        private int _moduleID;

        public int ModuleID
        {
            get { return _moduleID; }
            set { _moduleID = value; }
        }
        private string _moduleName;

        public string ModuleName
        {
            get { return _moduleName; }
            set { _moduleName = value; }
        }
        private string _moduleDisplayNameEn;

        public string ModuleDisplayNameEn
        {
            get { return _moduleDisplayNameEn; }
            set { _moduleDisplayNameEn = value; }
        }
        private string _moduleDisplayNameFr;

        public string ModuleDisplayNameFr
        {
            get { return _moduleDisplayNameFr; }
            set { _moduleDisplayNameFr = value; }
        }
        private bool _isActive;

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public void Save() {
            string sqlInsert = "INSERT INTO z_security_modules (ModuleName,ModuleDisplayNameEn,ModuleDisplayNameFr,IsActive) VALUES (@ModuleName,@ModuleDisplayNameEn,@ModuleDisplayNameFr,@IsActive)";
            string sqlUpdate = "UPDATE z_security_modules SET ModuleName=@ModuleName,ModuleDisplayNameEn=@ModuleDisplayNameEn,ModuleDisplayNameFr=@ModuleDisplayNameFr,IsActive=@IsActive WHERE ModuleID=@ModuleID";

            DbHelper dbHelp = new DbHelper();
            List<MySqlParameter> p = new List<MySqlParameter>();
            p.Add(DbUtility.GetParameter("ModuleDisplayNameEn", this.ModuleDisplayNameEn, MyDbType.String));
            p.Add(DbUtility.GetParameter("ModuleDisplayNameFr", this.ModuleDisplayNameFr, MyDbType.String));
            p.Add(DbUtility.GetParameter("ModuleName", this.ModuleName, MyDbType.String));
            p.Add(DbUtility.GetParameter("IsActive", this.IsActive, MyDbType.Boolean));
            
            try
            {
                if(this.ModuleID > 0){
                   p.Add(DbUtility.GetParameter("ModuleID", this.ModuleID, MyDbType.Int));
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, p.ToArray());
                }
                else
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, p.ToArray());
                }
            }
            catch 
            {                
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(int moduleID) {
            string sql = "SELECT ModuleID, ModuleName,ModuleDisplayNameEn,ModuleDisplayNameFr,IsActive FROM z_security_modules WHERE ModuleID=@ModuleID";            

            DbHelper dbHelp = new DbHelper();
            List<MySqlParameter> p = new List<MySqlParameter>();
            p.Add(DbUtility.GetParameter("ModuleID", moduleID, MyDbType.Int));
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, p.ToArray());
                if (dr.Read()) {
                    this.IsActive = BusinessUtility.GetBool(dr["IsActive"]);
                    this.ModuleDisplayNameEn = BusinessUtility.GetString(dr["ModuleDisplayNameEn"]);
                    this.ModuleDisplayNameFr = BusinessUtility.GetString(dr["ModuleDisplayNameFr"]);
                    this.ModuleID = BusinessUtility.GetInt(dr["ModuleID"]);
                    this.ModuleName = BusinessUtility.GetString(dr["ModuleName"]);                    
                }
            }
            catch 
            {                
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public string GetSql(string lang) {
            return string.Format("SELECT ModuleID, ModuleName, ModuleDisplayName{0} AS ModuleDisplayName FROM z_security_modules WHERE IsActive=1", lang);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class CategoryWithProduct
    {
        public List<object> CategoryAndProducts(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<object> lstResult = new List<object>();
            string strSQL = @"SELECT subcatId,catname,subcatName,concat(userFirstName,' ',UserLastName) as UserName, 
                                  CASE subcatIsActive WHEN 1 THEN 'green.gif' WHEN 2 THEN 'orange.gif' ELSE 'red.gif' END as subcatIsActive ,
                                  subcatStatus,subcatUpdatedOn,
                                  CASE subcatImagePath WHEN '' THEN '../images/nopic.jpg' ELSE subcatImagePath END AS subcatImagePath, 
                                  subcatWebSeq, subcategory.eBayCatgNo 
                        FROM        subcategory
                        INNER JOIN  users    ON  users.userid=subcategory.subcatUpdatedBy 
                        INNER JOIN  category ON subcategory.subcatCatid=category.catid AND category.catDescLang=@Lang
                        WHERE       subcatStatus='1' 
                        AND         subcatdescLang =@Lang
                        ORDER BY    subcatWebSeq";
            try
            {
                MySqlDataReader dr = dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("Lang", lang, MyDbType.String) });
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        Dictionary<object, object> dictResult = new Dictionary<object, object>();
                        dictResult.Add("SubCategoryID", BusinessUtility.GetString(dr["subcatId"]));
                        dictResult.Add("Category", BusinessUtility.GetString(dr["catname"]));
                        dictResult.Add("SubCategory", BusinessUtility.GetString(dr["subcatName"]));
                        dictResult.Add("UpdatedByUser", BusinessUtility.GetString(dr["UserName"]));
                        dictResult.Add("SubCategoryStatus", BusinessUtility.GetString(dr["subcatStatus"]));
                        dictResult.Add("SubCategoryImagePath", BusinessUtility.GetString(dr["subcatImagePath"]));
                        lstResult.Add(dictResult);
                    }
                    if (!dr.IsClosed) dr.Close();
                }
                foreach (Dictionary<object, object> dictResult in lstResult)
                {
                    dictResult.Add("Products", this.ProductsBySubCategory(dbHelp, BusinessUtility.GetInt(dictResult["SubCategoryID"]), lang));
                }
                return lstResult;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }
        public List<object> ProductsBySubCategory(DbHelper dbHelp, int subCategoryID, string lang)
        {

            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<object> lstResult = new List<object>();
            string strSQL = @" SELECT p.productID, p.prdIntID, p.prdExtID, p.prdType, p.prdUPCCode, p.prdName, p.prdIsKit, p.prdMinQtyPerSO, p.prdWeight, p.prdWeightPkg, p.prdInoclTerms, p.prdCreatedUserID, p.prdCreatedOn, p.prdLastUpdatedByUserID, 
                                   p.prdLastUpdatedOn, p.prdSalePricePerMinQty, p.prdEndUserSalesPrice, p.prdIsSpecial, p.prdDiscount, p.prdMinQtyPOTrig, p.prdAutoPO, p.prdPOQty, p.prdIsActive, p.prdIsWeb, p.prdComissionCode, p.isPOSMenu, p.prdCategory,
                                   p.prdSubcategory, p.prdWebSalesPrice, p.prdDiscountType, p.prdHeight, p.prdLength, p.prdHeightPkg, p.prdLengthPkg, p.prdWidth, p.prdWidthPkg, p.prdExtendedCategory, p.prdAccomodationType, p.prdSeq, p.TaxGroupID,
                                   pd.prdName, pd.prdSmallDesc, pd.prdLargeDesc,
                                   IFNull(prdSmallImagePath,'DefalutImage.jpg') AS SmallImage,
                                   IFNull(prdLargeImagePath,'DefalutImage.jpg') AS LargeImage,
                                   p.prdIsGlutenFree, p.prdIsVegetarian, p.prdIsContainsNuts, p.prdIsCookedSushi, p.prdSpicyLevel
                             FROM       products p
                             INNER JOIN prddescriptions pd ON P.productID=pd.id AND pd.descLang=@lang
                             LEFT JOIN  prdimages pi ON pi.prdID=P.productID
                             WHERE      p.prdISActive=1 AND p.prdIsWeb=1
                             AND        p.prdSubCategory=@subCategory ORDER BY p.prdName";
            MySqlParameter[] p = {
                                     DbUtility.GetParameter("subCategory", subCategoryID, MyDbType.Int),
                                     DbUtility.GetParameter("lang", lang, MyDbType.String)
                                 };
            try
            {
                MySqlDataReader dr = dbHelp.GetDataReader(strSQL, CommandType.Text, p);
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        Dictionary<object, object> dictResult = new Dictionary<object, object>();
                        dictResult.Add("ProductID", BusinessUtility.GetString(dr["productID"]));
                        dictResult.Add("Type", BusinessUtility.GetString(dr["prdType"]));
                        dictResult.Add("UPCCode", BusinessUtility.GetString(dr["prdUPCCode"]));
                        dictResult.Add("Name", BusinessUtility.GetString(dr["prdName"]));
                        dictResult.Add("IsKit", BusinessUtility.GetString(dr["prdIsKit"]));
                        dictResult.Add("SalesPrice", BusinessUtility.GetString(dr["prdEndUserSalesPrice"]));
                        dictResult.Add("IsSpecial", BusinessUtility.GetString(dr["prdIsSpecial"]));
                        dictResult.Add("Discount", BusinessUtility.GetString(dr["prdDiscount"]));
                        dictResult.Add("DiscountType", BusinessUtility.GetString(dr["prdDiscountType"]));
                        dictResult.Add("WebSalesPrice", BusinessUtility.GetString(dr["prdWebSalesPrice"]));
                        dictResult.Add("TaxGroupID", BusinessUtility.GetString(dr["TaxGroupID"]));
                        dictResult.Add("SmallDesc", BusinessUtility.GetString(dr["prdSmallDesc"]));
                        dictResult.Add("LargeDesc", BusinessUtility.GetString(dr["prdLargeDesc"]));
                        dictResult.Add("SmallImage", BusinessUtility.GetString(dr["SmallImage"]));
                        dictResult.Add("LargeImage", BusinessUtility.GetString(dr["LargeImage"]));

                        dictResult.Add("IsGlutenFree", BusinessUtility.GetString(dr["prdIsGlutenFree"]));
                        dictResult.Add("IsVegetarian", BusinessUtility.GetString(dr["prdIsVegetarian"]));
                        dictResult.Add("IsContainsNuts", BusinessUtility.GetString(dr["prdIsContainsNuts"]));
                        dictResult.Add("IsCookedSushi", BusinessUtility.GetString(dr["prdIsCookedSushi"]));
                        dictResult.Add("SpicyLevel", BusinessUtility.GetString(dr["prdSpicyLevel"]));
                        lstResult.Add(dictResult);
                    }
                    if (!dr.IsClosed) dr.Close();
                }
                foreach (Dictionary<object, object> dictResult in lstResult)
                {
                    dictResult.Add("ProductTax", this.ProductTax(dbHelp, BusinessUtility.GetInt(dictResult["ProductID"])));
                }
                return lstResult;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }
        public List<object> ProductTax(DbHelper dbHelp, int productID)
        {

            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<object> lstResult = new List<object>();
            string strSQL = @" SELECT DISTINCT sysTaxCode, sysTaxDesc, sysTaxPercentage, sysTaxSequence, sysTaxOnTotal, TaxOnTotalTaxCode
                                FROM systaxcode stc INNER JOIN prdquantity pq ON pq.prdTaxCode=stc.sysTaxCode WHERE pq.prdID=@ProductID ORDER BY sysTaxSequence ";
            try
            {
                MySqlDataReader dr = dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ProductID", productID, MyDbType.Int) });
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        Dictionary<object, object> dictResult = new Dictionary<object, object>();
                        dictResult.Add("TaxDesc", BusinessUtility.GetString(dr["sysTaxDesc"]));
                        dictResult.Add("Tax", BusinessUtility.GetString(dr["sysTaxPercentage"]));
                        dictResult.Add("IsOnTotal", BusinessUtility.GetString(dr["sysTaxOnTotal"]));
                        lstResult.Add(dictResult);
                    }
                    if (!dr.IsClosed) dr.Close();
                }
                return lstResult;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }
        public int GetCountStatus(DbHelper dbHelp, int statusID)
        {
            string strSQL = "";
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            if (statusID == 11)
            {
                strSQL = @" SELECT COUNT(*)  FROM postransaction WHERE IsOnHold=@IsOnHold AND IsClosed = @IsClosed AND posTransStatus IN('9','10','11','12') ";
            }
            else
            {
                strSQL = @" SELECT COUNT(*)  FROM postransaction WHERE posTransStatus=@statusID and IsOnHold=@IsOnHold AND IsClosed = @IsClosed ";//AND DATEDIFF(posTransDateTime,NOW())=0
            }
            try
            {
                if (statusID == 11)
                {
                    return BusinessUtility.GetInt(dbHelp.GetValue(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("IsOnHold", true, MyDbType.Boolean), DbUtility.GetParameter("IsClosed", false, MyDbType.Boolean) }));
                }
                else
                {
                    return BusinessUtility.GetInt(dbHelp.GetValue(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("statusID", statusID, MyDbType.String), DbUtility.GetParameter("IsOnHold", false, MyDbType.Boolean), DbUtility.GetParameter("IsClosed", false, MyDbType.Boolean) }));
                }
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }
        public void SetStatus(DbHelper dbHelp, int statusTypeID, int transID, int userID, string timeInMin, string IsOnHold)
        {
            bool mustClose = false;
            string strSQL = "";
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }


            if (statusTypeID == 11)
            {
                strSQL = @"UPDATE postransaction SET IsOnHold=@IsOnHold WHERE posTransId=@transID";
            }
            else
            {
                strSQL = @"UPDATE postransaction SET posTransStatus=@statusTypeID, IsOnHold=@IsOnHold WHERE posTransId=@transID";
            }
            try
            {
                if (statusTypeID == 11)
                {

                    dbHelp.ExecuteNonQuery(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("IsOnHold", true, MyDbType.Boolean),
                                                                                                                DbUtility.GetParameter("transID", transID, MyDbType.Int)});
                }
                else
                {
                    dbHelp.ExecuteNonQuery(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("statusTypeID", statusTypeID, MyDbType.String),
                                                                                                                DbUtility.GetParameter("IsOnHold", BusinessUtility.GetBool(IsOnHold), MyDbType.Boolean),
                                                                                                                DbUtility.GetParameter("transID", transID, MyDbType.Int)});
                }
                this.InsertPOSTransStatusHistory(dbHelp, statusTypeID, transID, userID, timeInMin);
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }
        public void InsertPOSTransStatusHistory(DbHelper dbHelp, int statusTypeID, int transID, int userID, string timeInMin)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQL = @"INSERT INTO postransitionstatushistory ( posTransID, StatusTypeID, UserID, CreatedDate,TimeInMin)
                                                            VALUES   ( @posTransID, @StatusTypeID, @UserID, NOW(),@TimeInMin)";
            try
            {
                dbHelp.ExecuteNonQuery(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("StatusTypeID", statusTypeID, MyDbType.String),
                                                                                        DbUtility.GetParameter("posTransID", transID, MyDbType.Int),
                                                                                        DbUtility.GetParameter("UserID", userID, MyDbType.Int),
                                                                                        DbUtility.GetParameter("TimeInMin", timeInMin, MyDbType.String)});
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public List<object> GetTransition(DbHelper dbHelp, int transID, int limit)
        {
            bool mustClose = false;
            double points = 0.0;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<object> lstResult = new List<object>();
            //            string strSQL = @"SELECT DISTINCT pt.posTransId, posTransType, posTransDateTime, posTransRegCode, posTransUserID, posTransStatus, posSubTotal,
            //                                              posTax1,posTax1Desc, posTax2,posTax2Desc, posTax3, posTax3Desc, posTax4, posTax4Desc, posTotalValue,
            //                                              posoldTransid, posTabNote, posGiftReason, posAuthorizedBy, posPercentDiscount,
            //                                              (SELECT MAX(CreatedDate) FROM postransitionstatushistory WHERE posTransID=pt.posTransId) AS HstyDateTime,
            //                                              (SELECT TimeInMin FROM postransitionstatushistory WHERE posTransID=pt.posTransId AND StatusTypeID=pt.posTransStatus ORDER BY CreatedDate DESC LIMIT 1 )       TimeInMin
            //                              FROM postransaction pt
            //                              WHERE posTransStatus!='1'  ";//AND DATEDIFF(posTransDateTime,NOW())=0
            //            strSQL += transID > 0 ? " AND pt.posTransId=" + transID : " ";
            //            strSQL += " ORDER BY posTransStatus,HstyDateTime,posTransDateTime,pt.posTransId ";
            //            strSQL += limit > 0 ? " LIMIT " + limit : "";

            string strSQL = @"SELECT DISTINCT result.*,
                                (SELECT MAX(CreatedDate) FROM postransitionstatushistory WHERE posTransID=result.posTransId) AS HstyDateTime,
                                (SELECT TimeInMin FROM postransitionstatushistory WHERE posTransID=result.posTransId AND StatusTypeID=result.posTransStatus ORDER BY CreatedDate DESC LIMIT 1 ) AS TimeInMin
                                , CustPhoneNo, CustEmailID, PickUpDateTime, AdditionalRequest,PartnerID
                                FROM (
                                        SELECT DISTINCT posTransId, posTransType, posTransDateTime, posTransRegCode, posTransUserID, posTransStatus, posSubTotal,
                                        posTax1,posTax1Desc, posTax2,posTax2Desc, posTax3, posTax3Desc, posTax4, posTax4Desc, posTotalValue,
                                        posoldTransid, posTabNote, posGiftReason, posAuthorizedBy, posPercentDiscount , PaidAmount , 0 AS MyOrder,IsOnHold, IsClosed
                                        FROM postransaction  WHERE posTransStatus ='10'
                                     UNION ALL
                                        SELECT DISTINCT posTransId, posTransType, posTransDateTime, posTransRegCode, posTransUserID, posTransStatus, posSubTotal,
                                        posTax1,posTax1Desc, posTax2,posTax2Desc, posTax3, posTax3Desc, posTax4, posTax4Desc, posTotalValue,
                                        posoldTransid, posTabNote, posGiftReason, posAuthorizedBy, posPercentDiscount, PaidAmount, 1 AS MyOrder,IsOnHold, IsClosed
                                        FROM postransaction WHERE posTransStatus IN ('9','12')
                                    ) AS result 
                               LEFT JOIN postransitioncustomerdetails ptcd ON  ptcd.posTransitionID=result.posTransId
                                   WHERE 1=1  AND IsOnHold = 0 AND IsClosed = 0 ";//AND DATEDIFF(posTransDateTime,NOW())=0
            strSQL += transID > 0 ? " AND posTransId=" + transID : " ";
            strSQL += " ORDER BY MyOrder,posTransStatus,posTransId"; //MyOrder,posTransStatus,HstyDateTime,posTransDateTime,posTransId 
            strSQL += limit > 0 ? " LIMIT " + limit : "";

            try
            {
                MySqlDataReader dr = dbHelp.GetDataReader(strSQL, CommandType.Text, null);
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        Dictionary<object, object> dictResult = new Dictionary<object, object>();
                        dictResult.Add("TransID", BusinessUtility.GetString(dr["posTransId"]));
                        dictResult.Add("StatusTypeID", BusinessUtility.GetString(dr["posTransStatus"]));
                        dictResult.Add("Date", (string.IsNullOrEmpty(BusinessUtility.GetString(dr["HstyDateTime"])) ? BusinessUtility.GetDateTime(dr["posTransDateTime"]).ToString("yyyy-MM-dd HH:mm") : BusinessUtility.GetDateTime(dr["HstyDateTime"]).ToString("yyyy-MM-dd HH:mm")));
                        dictResult.Add("PoCreationDate", BusinessUtility.GetDateTime(dr["posTransDateTime"]).ToString("yyyy-MM-dd HH:mm"));
                        dictResult.Add("SubTotal", BusinessUtility.GetString(dr["posSubTotal"]));
                        dictResult.Add("Tax1", BusinessUtility.GetString(dr["posTax1"]));
                        dictResult.Add("Tax1Desc", BusinessUtility.GetString(dr["posTax1Desc"]));
                        dictResult.Add("Tax2", BusinessUtility.GetString(dr["posTax2"]));
                        dictResult.Add("Tax2Desc", BusinessUtility.GetString(dr["posTax2Desc"]));
                        dictResult.Add("Tax3", BusinessUtility.GetString(dr["posTax3"]));
                        dictResult.Add("Tax3Desc", BusinessUtility.GetString(dr["posTax3Desc"]));
                        dictResult.Add("Tax4", BusinessUtility.GetString(dr["posTax4"]));
                        dictResult.Add("Tax4Desc", BusinessUtility.GetString(dr["posTax4Desc"]));
                        dictResult.Add("Total", BusinessUtility.GetString(dr["posTotalValue"]));
                        dictResult.Add("TableNote", BusinessUtility.GetString(dr["posTabNote"]));
                        dictResult.Add("TimeInMin", BusinessUtility.GetString(dr["TimeInMin"]));

                        dictResult.Add("CustPhoneNo", BusinessUtility.GetString(dr["CustPhoneNo"]));
                        dictResult.Add("CustEmailID", BusinessUtility.GetString(dr["CustEmailID"]));
                        dictResult.Add("PickUpDateTime", BusinessUtility.GetDateTime(dr["PickUpDateTime"]).ToString("yyyy-MM-dd HH:mm"));
                        dictResult.Add("AdditionalRequest", BusinessUtility.GetString(dr["AdditionalRequest"]));
                        dictResult.Add("PartnerID", BusinessUtility.GetString(dr["PartnerID"]));
                        dictResult.Add("IsOnHold", BusinessUtility.GetBool(dr["IsOnHold"]));
                        //Added by mukesh 20130802
                        int sOrderPaid = 0;
                        if (BusinessUtility.GetInt(dr["PaidAmount"]) == BusinessUtility.GetInt(dr["posTotalValue"]))
                        {
                            sOrderPaid = (int)RESTAURANTMENU_PAYMENTSTATUS.PAID;
                        }
                        else
                        {
                            sOrderPaid = (int)RESTAURANTMENU_PAYMENTSTATUS.NOTPAID;
                        }
                        if (CheckPaymetThrLoyalPoint(BusinessUtility.GetString(dr["PartnerID"]), BusinessUtility.GetString(dr["posTransId"]), null))
                        {
                            //sOrderPaid = string.Format("<font style='color:Green;font-weight:bold;font-family:Tahoma;font-size:15px;'>{0}</font>", "PAID with points");
                            if (BusinessUtility.GetInt(dr["PaidAmount"]) > 0)
                            {
                                sOrderPaid = (int)RESTAURANTMENU_PAYMENTSTATUS.POINTSCREDIT;
                            }
                            else if (this.UnPaidPartialAmt(BusinessUtility.GetInt(dr["posTransId"]), null) > 0)
                            {
                                sOrderPaid = (int)RESTAURANTMENU_PAYMENTSTATUS.POINTSPICKUP;
                            }
                            else
                            {
                                sOrderPaid = (int)RESTAURANTMENU_PAYMENTSTATUS.PAIDWITHPOINTS;
                            }
                        }
                        dictResult.Add("PaidStatus", sOrderPaid); //Added end mukesh 20130802
                        dictResult.Add("TotalAmount", Math.Round(BusinessUtility.GetDouble(dr["posTotalValue"]), 2)); //Added by mukesh 20130805
                        points = new LoyalPartnerHistory().GetPartnerLoyaltyPoints(null, BusinessUtility.GetInt(dr["PartnerID"]));
                        dictResult.Add("LoyaltyPoint", Math.Round(points, 2));
                        lstResult.Add(dictResult);
                    }
                    if (!dr.IsClosed) dr.Close();
                }
                foreach (Dictionary<object, object> dictResult in lstResult)
                {
                    dictResult.Add("OrderDetails", this.TransitionOrderDetails(dbHelp, BusinessUtility.GetInt(dictResult["TransID"])));
                    int PartnerID = string.IsNullOrEmpty(dictResult["PartnerID"].ToString()) ? 0 : BusinessUtility.GetInt(dictResult["PartnerID"]);
                    Partners partner = new Partners();
                    partner.PopulateObject(PartnerID);
                    Addresses add = new Addresses();
                    add.PopulateObject(partner.PartnerID, AddressReference.CUSTOMER_CONTACT, AddressType.SHIP_TO_ADDRESS);
                    dictResult.Add("Partners", partner);
                    dictResult.Add("PartLang", partner.PartnerLang);
                    dictResult.Add("Address", add);
                }
                return lstResult;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public List<object> TransitionOrderDetails(DbHelper dbHelp, int transID)
        {

            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<object> lstResult = new List<object>();
            //            string strSQL = @" SELECT ppn.posProductNo, ppn.posPrdTransID, ppn.posProductID, ppn.posPrdQty, ppn.posPrdUnitPrice, ppn.posPrdPrice,
            //                                      ppn.posPrdTaxCode, ppn.PosTax1, ppn.PosTax1Desc, ppn.PosTax2, ppn.PosTax2Desc, ppn.PosTax3, ppn.PosTax3Desc, ppn.PosTax4,
            //                                      ppn.PosTax4Desc,p.prdName
            //                              FROM       posproductno ppn
            //                              INNER JOIN products p ON p.productID=ppn.posProductID
            //                              WHERE ppn.posPrdTransID=@transID";
            string strSQL = @" SELECT DISTINCT ppn.posProductNo, ppn.posPrdTransID, ppn.posProductID, ppn.posPrdQty, ppn.posPrdUnitPrice,
                                               ppn.posPrdPrice,ppn.posPrdTaxCode, ppn.PosTax1, ppn.PosTax1Desc, ppn.PosTax2, ppn.PosTax2Desc,
                                               ppn.PosTax3, ppn.PosTax3Desc, ppn.PosTax4,ppn.PosTax4Desc,p.prdName,subcatName
                                FROM       posproductno ppn
                                INNER JOIN products p ON p.productID=ppn.posProductID
                                INNER JOIN subcategory sc ON sc.subcatId=p.prdSubCategory AND sc.subcatStatus='1' AND sc.subcatdescLang ='en'
                                WHERE ppn.posPrdTransID=@transID";
            try
            {
                MySqlDataReader dr = dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("transID", transID, MyDbType.Int) });
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        Dictionary<object, object> dictResult = new Dictionary<object, object>();
                        dictResult.Add("TransID", BusinessUtility.GetString(dr["posPrdTransID"]));
                        dictResult.Add("Qty", BusinessUtility.GetString(dr["posPrdQty"]));
                        dictResult.Add("UnitPrice", BusinessUtility.GetString(dr["posPrdUnitPrice"]));
                        dictResult.Add("Price", BusinessUtility.GetString(dr["posPrdPrice"]));
                        dictResult.Add("ProductName", BusinessUtility.GetString(dr["prdName"]));
                        dictResult.Add("SubCatName", BusinessUtility.GetString(dr["subcatName"]));
                        lstResult.Add(dictResult);
                    }
                    if (!dr.IsClosed) dr.Close();
                }
                return lstResult;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public string GetTransitionTableNote()
        {
            string returnResult = "";
            DbHelper dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQL = @" SELECT TableNoteEn FROM postranstablenotes WHERE IsUsed=0 LIMIT 1 ";
            string strSQLUpdate = @" UPDATE postranstablenotes  SET IsUsed=1 WHERE IsUsed=0 LIMIT 1  ";
            string strSQLUpdateReSET = @" UPDATE postranstablenotes  SET IsUsed=0  ";
            try
            {
                returnResult = BusinessUtility.GetString(dbHelp.GetValue(strSQL, CommandType.Text, null));
                dbHelp.ExecuteNonQuery(strSQLUpdate, CommandType.Text, null);
                while (string.IsNullOrEmpty(returnResult))
                {
                    dbHelp.ExecuteNonQuery(strSQLUpdateReSET, CommandType.Text, null);
                    returnResult = BusinessUtility.GetString(dbHelp.GetValue(strSQL, CommandType.Text, null));
                    dbHelp.ExecuteNonQuery(strSQLUpdate, CommandType.Text, null);
                }
                return returnResult;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public List<object> ShopTime(DbHelper dbHelp)
        {

            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<object> lstResult = new List<object>();
            string strSQL = @" SELECT DISTINCT Day,TimeFrom,TimeTo  FROM postransitionshoptime ORDER BY Day, TimeFrom";
            try
            {
                MySqlDataReader dr = dbHelp.GetDataReader(strSQL, CommandType.Text, null);
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        Dictionary<object, object> dictResult = new Dictionary<object, object>();
                        dictResult.Add("Day", BusinessUtility.GetString(dr["Day"]));
                        dictResult.Add("TimeFrom", BusinessUtility.GetString(dr["TimeFrom"]));
                        dictResult.Add("TimeTo", BusinessUtility.GetString(dr["TimeTo"]));
                        lstResult.Add(dictResult);
                    }
                    if (!dr.IsClosed) dr.Close();
                }
                return lstResult;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public int GetAndValidedUser(string loginid, string password)
        {
            string sqlvalidate = "SELECT userID FROM users WHERE userLoginId=@userLoginId AND userPassword=PASSWORD(@userPassword) AND userActive=1";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object scalar = dbHelp.GetValue(sqlvalidate, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("userLoginId", loginid, MyDbType.String), DbUtility.GetParameter("userPassword", password, MyDbType.String) });
                return scalar != null ? BusinessUtility.GetInt(scalar) : -1;
            }
            catch { throw; }
            finally { dbHelp.CloseDatabaseConnection(); }
        }
        public bool UpdateTrasitionPaidAmount(int trasitionID, double paidAmount)
        {
            string sql = "UPDATE postransaction SET PaidAmount=@PaidAmount WHERE posTransId=@posTransId";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                return BusinessUtility.GetInt(dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("PaidAmount", paidAmount, MyDbType.Double), DbUtility.GetParameter("posTransId", trasitionID, MyDbType.String) })) > 0;
            }
            catch { throw; }
            finally { dbHelp.CloseDatabaseConnection(); }
        }
        //Added by mukesh 20130821
        public int OrderStatus(int ordID)
        {
            int orderStatus = 0;
            //string sOrderPaid = "";
            DbHelper dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            //string strSQL = @" SELECT PaidAmount, posTotalValue FROM postransaction WHERE posTransId=@ordID ";
            string strSQL = @" SELECT posTransStatus FROM postransaction WHERE posTransId=@ordID ";
            try
            {
                MySqlDataReader dr = dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ordID", ordID, MyDbType.Int)
                });
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        orderStatus = BusinessUtility.GetInt(dr["posTransStatus"]);
                        //if (BusinessUtility.GetInt(dr["PaidAmount"]) == BusinessUtility.GetInt(dr["posTotalValue"]))
                        //{
                        //    sOrderPaid = string.Format("<font style='color:Green;font-weight:bold;font-family:Tahoma;font-size:15px;'>{0}</font>", "PAID");
                        //}
                        //else
                        //{
                        //    sOrderPaid = string.Format("<font style='color:Red;font-weight:bold;font-family:Tahoma;font-size:15px;'>{0}</font>", "NOT PAID");
                        //}

                    }
                    if (!dr.IsClosed) dr.Close();
                }
                //return sOrderPaid;
                return orderStatus;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }
        public List<object> OrderList(string emailID)
        {
            DbHelper dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<object> lstResult = new List<object>();
            string strSQL = @" select posTransId, posTransStatus from postransaction as post left join partners p on p.partnerID = post.posTransUserID where p.PartnerEmail = @PartnerEmail";
            try
            {
                MySqlDataReader dr = dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("PartnerEmail", emailID, MyDbType.String)});
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        Dictionary<object, object> dictResult = new Dictionary<object, object>();
                        dictResult.Add("OrderNo", BusinessUtility.GetString(dr["posTransId"]));
                        dictResult.Add("Status", BusinessUtility.GetInt(dr["posTransStatus"]));
                        lstResult.Add(dictResult);
                    }
                    if (!dr.IsClosed) dr.Close();
                }
                return lstResult;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }
        public List<object> OrderDetails(DbHelper dbHelp, int transID)
        {

            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<object> lstResult = new List<object>();
            string strSQL = @" SELECT DISTINCT ppn.posProductNo, ppn.posPrdTransID, ppn.posProductID, ppn.posPrdQty, ppn.posPrdUnitPrice,
                                               ppn.posPrdPrice,ppn.posPrdTaxCode, ppn.PosTax1, ppn.PosTax1Desc, ppn.PosTax2, ppn.PosTax2Desc,
                                               ppn.PosTax3, ppn.PosTax3Desc, ppn.PosTax4,ppn.PosTax4Desc,p.prdName,subcatName
                                FROM       posproductno ppn
                                INNER JOIN products p ON p.productID=ppn.posProductID
                                INNER JOIN subcategory sc ON sc.subcatId=p.prdSubCategory AND sc.subcatStatus='1' AND sc.subcatdescLang ='en'
                                WHERE ppn.posPrdTransID=@transID";
            try
            {
                MySqlDataReader dr = dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("transID", transID, MyDbType.Int) });
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        Dictionary<object, object> dictResult = new Dictionary<object, object>();
                        dictResult.Add("OrderID", BusinessUtility.GetString(dr["posPrdTransID"]));
                        dictResult.Add("ProductID", BusinessUtility.GetString(dr["posProductID"]));
                        dictResult.Add("Qty", BusinessUtility.GetString(dr["posPrdQty"]));
                        dictResult.Add("UnitPrice", BusinessUtility.GetString(dr["posPrdUnitPrice"]));
                        dictResult.Add("Price", BusinessUtility.GetString(dr["posPrdPrice"]));
                        dictResult.Add("ProductName", BusinessUtility.GetString(dr["prdName"]));
                        dictResult.Add("SubCatName", BusinessUtility.GetString(dr["subcatName"]));
                        lstResult.Add(dictResult);
                    }
                    if (!dr.IsClosed) dr.Close();
                }
                return lstResult;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public bool CheckPaymetThrLoyalPoint(string partnerID, string transID, DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQL = @" SELECT COUNT(*)  FROM z_loyal_partner_history WHERE PointsAddedBy = @PointsAddedBy and PosTransactionID = @PosTransactionID  and Points<0 ";
            try
            {
                if (BusinessUtility.GetInt(dbHelp.GetValue(strSQL, CommandType.Text, new MySqlParameter[] {
                    DbUtility.GetParameter("PointsAddedBy", BusinessUtility.GetInt(partnerID), MyDbType.Int),
                    DbUtility.GetParameter("PosTransactionID", BusinessUtility.GetInt(transID), MyDbType.Int)
                })) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }
        //Added end
        public List<object> GetTransition(DbHelper dbHelp, string confcode)
        {
            bool mustClose = false;
            double points = 0.0;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<object> lstResult = new List<object>();

            string strSQL = @"SELECT DISTINCT result.*,
                                (SELECT MAX(CreatedDate) FROM postransitionstatushistory WHERE posTransID=result.posTransId) AS HstyDateTime,
                                (SELECT TimeInMin FROM postransitionstatushistory WHERE posTransID=result.posTransId AND StatusTypeID=result.posTransStatus ORDER BY CreatedDate DESC LIMIT 1 ) AS TimeInMin
                                , CustPhoneNo, CustEmailID, PickUpDateTime, AdditionalRequest,PartnerID
                                FROM (
                                        SELECT DISTINCT posTransId, posTransType, posTransDateTime, posTransRegCode, posTransUserID, posTransStatus, posSubTotal,
                                        posTax1,posTax1Desc, posTax2,posTax2Desc, posTax3, posTax3Desc, posTax4, posTax4Desc, posTotalValue,
                                        posoldTransid, posTabNote, posGiftReason, posAuthorizedBy, posPercentDiscount , PaidAmount 
                                        FROM postransaction 
                                    ) AS result 
                               LEFT JOIN postransitioncustomerdetails ptcd ON  ptcd.posTransitionID=result.posTransId
                                   WHERE 1=1 and ";
            confcode = confcode.Trim();
            if (confcode.Length == 3)
            {
                strSQL += "posTabNote like '%" + confcode + "' AND DATEDIFF(posTransDateTime,NOW())<=1";
            }
            else
            {
                strSQL += "posTabNote = '" + confcode + "' ";//AND DATEDIFF(posTransDateTime,NOW())=0
            }

            strSQL += " ORDER BY posTransStatus,HstyDateTime,posTransDateTime,posTransId ";

            try
            {
                MySqlDataReader dr = dbHelp.GetDataReader(strSQL, CommandType.Text, null);
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        Dictionary<object, object> dictResult = new Dictionary<object, object>();
                        dictResult.Add("TransID", BusinessUtility.GetString(dr["posTransId"]));
                        dictResult.Add("StatusTypeID", BusinessUtility.GetString(dr["posTransStatus"]));
                        dictResult.Add("Date", (string.IsNullOrEmpty(BusinessUtility.GetString(dr["HstyDateTime"])) ? BusinessUtility.GetDateTime(dr["posTransDateTime"]).ToString("yyyy-MM-dd HH:mm") : BusinessUtility.GetDateTime(dr["HstyDateTime"]).ToString("yyyy-MM-dd HH:mm")));
                        dictResult.Add("PoCreationDate", BusinessUtility.GetDateTime(dr["posTransDateTime"]).ToString("yyyy-MM-dd HH:mm"));
                        dictResult.Add("SubTotal", BusinessUtility.GetString(dr["posSubTotal"]));
                        dictResult.Add("Tax1", BusinessUtility.GetString(dr["posTax1"]));
                        dictResult.Add("Tax1Desc", BusinessUtility.GetString(dr["posTax1Desc"]));
                        dictResult.Add("Tax2", BusinessUtility.GetString(dr["posTax2"]));
                        dictResult.Add("Tax2Desc", BusinessUtility.GetString(dr["posTax2Desc"]));
                        dictResult.Add("Tax3", BusinessUtility.GetString(dr["posTax3"]));
                        dictResult.Add("Tax3Desc", BusinessUtility.GetString(dr["posTax3Desc"]));
                        dictResult.Add("Tax4", BusinessUtility.GetString(dr["posTax4"]));
                        dictResult.Add("Tax4Desc", BusinessUtility.GetString(dr["posTax4Desc"]));
                        dictResult.Add("Total", BusinessUtility.GetString(dr["posTotalValue"]));
                        dictResult.Add("TableNote", BusinessUtility.GetString(dr["posTabNote"]));
                        dictResult.Add("TimeInMin", BusinessUtility.GetString(dr["TimeInMin"]));

                        dictResult.Add("CustPhoneNo", BusinessUtility.GetString(dr["CustPhoneNo"]));
                        dictResult.Add("CustEmailID", BusinessUtility.GetString(dr["CustEmailID"]));
                        dictResult.Add("PickUpDateTime", BusinessUtility.GetDateTime(dr["PickUpDateTime"]).ToString("yyyy-MM-dd HH:mm"));
                        dictResult.Add("AdditionalRequest", BusinessUtility.GetString(dr["AdditionalRequest"]));
                        dictResult.Add("PartnerID", BusinessUtility.GetString(dr["PartnerID"]));
                        //Added by mukesh 20130802
                        int sOrderPaid = 0;
                        if (BusinessUtility.GetInt(dr["PaidAmount"]) == BusinessUtility.GetInt(dr["posTotalValue"]))
                        {
                            sOrderPaid = (int)RESTAURANTMENU_PAYMENTSTATUS.PAID;
                        }
                        else
                        {
                            sOrderPaid = (int)RESTAURANTMENU_PAYMENTSTATUS.NOTPAID;
                        }
                        if (CheckPaymetThrLoyalPoint(BusinessUtility.GetString(dr["PartnerID"]), BusinessUtility.GetString(dr["posTransId"]), null))
                        {
                            //sOrderPaid = string.Format("<font style='color:Green;font-weight:bold;font-family:Tahoma;font-size:15px;'>{0}</font>", "PAID with points");
                            if (BusinessUtility.GetInt(dr["PaidAmount"]) > 0)
                            {
                                sOrderPaid = (int)RESTAURANTMENU_PAYMENTSTATUS.POINTSCREDIT;
                            }
                            else if (this.UnPaidPartialAmt(BusinessUtility.GetInt(dr["posTransId"]), null) > 0)
                            {
                                sOrderPaid = (int)RESTAURANTMENU_PAYMENTSTATUS.POINTSPICKUP;
                            }
                            else
                            {
                                sOrderPaid = (int)RESTAURANTMENU_PAYMENTSTATUS.PAIDWITHPOINTS;
                            }
                        }
                        dictResult.Add("PaidStatus", sOrderPaid); //Added end mukesh 20130802
                        dictResult.Add("TotalAmount", Math.Round(BusinessUtility.GetDouble(dr["posTotalValue"]), 2)); //Added by mukesh 20130805
                        points = new LoyalPartnerHistory().GetPartnerLoyaltyPoints(null, BusinessUtility.GetInt(dr["PartnerID"]));
                        dictResult.Add("LoyaltyPoint", Math.Round(points, 2));
                        lstResult.Add(dictResult);
                    }
                    if (!dr.IsClosed) dr.Close();
                }
                foreach (Dictionary<object, object> dictResult in lstResult)
                {
                    dictResult.Add("OrderDetails", this.TransitionOrderDetails(dbHelp, BusinessUtility.GetInt(dictResult["TransID"])));
                    int PartnerID = string.IsNullOrEmpty(dictResult["PartnerID"].ToString()) ? 0 : BusinessUtility.GetInt(dictResult["PartnerID"]);
                    Partners partner = new Partners();
                    partner.PopulateObject(PartnerID);
                    Addresses add = new Addresses();
                    add.PopulateObject(partner.PartnerID, AddressReference.CUSTOMER_CONTACT, AddressType.SHIP_TO_ADDRESS);
                    dictResult.Add("Partners", partner);
                    dictResult.Add("PartLang", partner.PartnerLang);
                    dictResult.Add("Address", add);
                }
                return lstResult;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public List<object> GetTransition(DbHelper dbHelp, bool isOnHold)
        {
            bool mustClose = false;
            double points = 0.0;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<object> lstResult = new List<object>();

            string strSQL = @"SELECT DISTINCT result.*,
                                (SELECT MAX(CreatedDate) FROM postransitionstatushistory WHERE posTransID=result.posTransId) AS HstyDateTime,
                                (SELECT TimeInMin FROM postransitionstatushistory WHERE posTransID=result.posTransId AND StatusTypeID=result.posTransStatus ORDER BY CreatedDate DESC LIMIT 1 ) AS TimeInMin
                                , CustPhoneNo, CustEmailID, PickUpDateTime, AdditionalRequest,PartnerID
                                FROM (
                                        SELECT DISTINCT posTransId, posTransType, posTransDateTime, posTransRegCode, posTransUserID, posTransStatus, posSubTotal,
                                        posTax1,posTax1Desc, posTax2,posTax2Desc, posTax3, posTax3Desc, posTax4, posTax4Desc, posTotalValue,
                                        posoldTransid, posTabNote, posGiftReason, posAuthorizedBy, posPercentDiscount , PaidAmount, IsOnHold, IsClosed, 0 AS myOrder
                                        FROM postransaction WHERE posTransStatus ='10'
                                        UNION ALL
                                        SELECT DISTINCT posTransId, posTransType, posTransDateTime, posTransRegCode, posTransUserID, posTransStatus, posSubTotal,
                                        posTax1,posTax1Desc, posTax2,posTax2Desc, posTax3, posTax3Desc, posTax4, posTax4Desc, posTotalValue,
                                        posoldTransid, posTabNote, posGiftReason, posAuthorizedBy, posPercentDiscount , PaidAmount, IsOnHold, IsClosed, 1 As myOrder
                                        FROM postransaction WHERE posTransStatus IN ('9','11','12')
                                    ) AS result 
                               LEFT JOIN postransitioncustomerdetails ptcd ON  ptcd.posTransitionID=result.posTransId
                                   WHERE 1=1 and IsClosed = 0 and ";
            if (isOnHold)
            {
                strSQL += "IsOnHold = 1 ";
            }


            strSQL += " ORDER BY myOrder,posTransStatus,HstyDateTime,posTransDateTime,posTransId ";

            try
            {
                MySqlDataReader dr = dbHelp.GetDataReader(strSQL, CommandType.Text, null);
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        Dictionary<object, object> dictResult = new Dictionary<object, object>();
                        dictResult.Add("TransID", BusinessUtility.GetString(dr["posTransId"]));
                        dictResult.Add("StatusTypeID", BusinessUtility.GetString(dr["posTransStatus"]));
                        dictResult.Add("Date", (string.IsNullOrEmpty(BusinessUtility.GetString(dr["HstyDateTime"])) ? BusinessUtility.GetDateTime(dr["posTransDateTime"]).ToString("yyyy-MM-dd HH:mm") : BusinessUtility.GetDateTime(dr["HstyDateTime"]).ToString("yyyy-MM-dd HH:mm")));
                        dictResult.Add("PoCreationDate", BusinessUtility.GetDateTime(dr["posTransDateTime"]).ToString("yyyy-MM-dd HH:mm"));
                        dictResult.Add("SubTotal", BusinessUtility.GetString(dr["posSubTotal"]));
                        dictResult.Add("Tax1", BusinessUtility.GetString(dr["posTax1"]));
                        dictResult.Add("Tax1Desc", BusinessUtility.GetString(dr["posTax1Desc"]));
                        dictResult.Add("Tax2", BusinessUtility.GetString(dr["posTax2"]));
                        dictResult.Add("Tax2Desc", BusinessUtility.GetString(dr["posTax2Desc"]));
                        dictResult.Add("Tax3", BusinessUtility.GetString(dr["posTax3"]));
                        dictResult.Add("Tax3Desc", BusinessUtility.GetString(dr["posTax3Desc"]));
                        dictResult.Add("Tax4", BusinessUtility.GetString(dr["posTax4"]));
                        dictResult.Add("Tax4Desc", BusinessUtility.GetString(dr["posTax4Desc"]));
                        dictResult.Add("Total", BusinessUtility.GetString(dr["posTotalValue"]));
                        dictResult.Add("TableNote", BusinessUtility.GetString(dr["posTabNote"]));
                        dictResult.Add("TimeInMin", BusinessUtility.GetString(dr["TimeInMin"]));

                        dictResult.Add("CustPhoneNo", BusinessUtility.GetString(dr["CustPhoneNo"]));
                        dictResult.Add("CustEmailID", BusinessUtility.GetString(dr["CustEmailID"]));
                        dictResult.Add("PickUpDateTime", BusinessUtility.GetDateTime(dr["PickUpDateTime"]).ToString("yyyy-MM-dd HH:mm"));
                        dictResult.Add("AdditionalRequest", BusinessUtility.GetString(dr["AdditionalRequest"]));
                        dictResult.Add("PartnerID", BusinessUtility.GetString(dr["PartnerID"]));
                        dictResult.Add("IsOnHold", BusinessUtility.GetBool(dr["IsOnHold"]));
                        //Added by mukesh 20130802
                        int sOrderPaid = 0;
                        if (BusinessUtility.GetInt(dr["PaidAmount"]) == BusinessUtility.GetInt(dr["posTotalValue"]))
                        {
                            sOrderPaid = (int)RESTAURANTMENU_PAYMENTSTATUS.PAID;
                        }
                        else
                        {
                            sOrderPaid = (int)RESTAURANTMENU_PAYMENTSTATUS.NOTPAID;
                        }
                        if (CheckPaymetThrLoyalPoint(BusinessUtility.GetString(dr["PartnerID"]), BusinessUtility.GetString(dr["posTransId"]), null))
                        {
                            //sOrderPaid = string.Format("<font style='color:Green;font-weight:bold;font-family:Tahoma;font-size:15px;'>{0}</font>", "PAID with points");
                            if (BusinessUtility.GetInt(dr["PaidAmount"]) > 0)
                            {
                                sOrderPaid = (int)RESTAURANTMENU_PAYMENTSTATUS.POINTSCREDIT;
                            }
                            else if (this.UnPaidPartialAmt(BusinessUtility.GetInt(dr["posTransId"]), null) > 0)
                            {
                                sOrderPaid = (int)RESTAURANTMENU_PAYMENTSTATUS.POINTSPICKUP;
                            }
                            else
                            {
                                sOrderPaid = (int)RESTAURANTMENU_PAYMENTSTATUS.PAIDWITHPOINTS;
                            }
                        }
                        dictResult.Add("PaidStatus", sOrderPaid); //Added end mukesh 20130802
                        dictResult.Add("TotalAmount", Math.Round(BusinessUtility.GetDouble(dr["posTotalValue"]), 2)); //Added by mukesh 20130805
                        points = new LoyalPartnerHistory().GetPartnerLoyaltyPoints(null, BusinessUtility.GetInt(dr["PartnerID"]));
                        dictResult.Add("LoyaltyPoint", Math.Round(points, 2));
                        lstResult.Add(dictResult);
                    }
                    if (!dr.IsClosed) dr.Close();
                }
                foreach (Dictionary<object, object> dictResult in lstResult)
                {
                    dictResult.Add("OrderDetails", this.TransitionOrderDetails(dbHelp, BusinessUtility.GetInt(dictResult["TransID"])));
                    int PartnerID = string.IsNullOrEmpty(dictResult["PartnerID"].ToString()) ? 0 : BusinessUtility.GetInt(dictResult["PartnerID"]);
                    Partners partner = new Partners();
                    partner.PopulateObject(PartnerID);
                    Addresses add = new Addresses();
                    add.PopulateObject(partner.PartnerID, AddressReference.CUSTOMER_CONTACT, AddressType.SHIP_TO_ADDRESS);
                    dictResult.Add("Partners", partner);
                    dictResult.Add("PartLang", partner.PartnerLang);
                    dictResult.Add("Address", add);
                }
                return lstResult;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public List<object> TotalAmt(DbHelper dbHelp, int transID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<object> lstResult = new List<object>();
            string strSQL = @" SELECT DISTINCT posSubTotal, posTransUserID from postransaction WHERE posTransId=@transID";
            try
            {
                MySqlDataReader dr = dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("transID", transID, MyDbType.Int) });
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        lstResult.Add(BusinessUtility.GetString(dr["posSubTotal"]));
                        lstResult.Add(BusinessUtility.GetString(dr["posTransUserID"]));
                    }
                    if (!dr.IsClosed) dr.Close();
                }
                return lstResult;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public bool CheckPointAddedExist(string transID, DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQL = @" SELECT COUNT(*)  FROM z_loyal_partner_history WHERE PosTransactionID = @PosTransactionID ";
            try
            {
                if (BusinessUtility.GetInt(dbHelp.GetValue(strSQL, CommandType.Text, new MySqlParameter[] {
                    DbUtility.GetParameter("PosTransactionID", BusinessUtility.GetInt(transID), MyDbType.Int)
                })) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public List<object> ROrderDetails(DbHelper dbHelp, int transID)
        {

            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<object> lstResult = new List<object>();
            string strSQL = @" SELECT DISTINCT ppn.posPrdTransID, ppn.posProductID, ppn.posPrdQty, p.prdEndUserSalesPrice,p.prdName,subcatName
                                FROM  posproductno ppn
                                INNER JOIN products p ON p.productID=ppn.posProductID
                                INNER JOIN subcategory sc ON sc.subcatId=p.prdSubCategory AND sc.subcatStatus='1' AND sc.subcatdescLang ='en'
                                WHERE ppn.posPrdTransID=@transID";

            try
            {
                MySqlDataReader drquery = dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("transID", transID, MyDbType.Int) });
                if (drquery != null)
                {
                    while (drquery.Read())
                    {
                        Dictionary<object, object> dictResult = new Dictionary<object, object>();
                        dictResult.Add("OrderID", BusinessUtility.GetString(drquery["posPrdTransID"]));
                        dictResult.Add("ProductID", BusinessUtility.GetString(drquery["posProductID"]));
                        dictResult.Add("Qty", BusinessUtility.GetString(drquery["posPrdQty"]));
                        dictResult.Add("UnitPrice", BusinessUtility.GetString(drquery["prdEndUserSalesPrice"]));
                        dictResult.Add("ProductName", BusinessUtility.GetString(drquery["prdName"]));
                        dictResult.Add("SubCatName", BusinessUtility.GetString(drquery["subcatName"]));

                        int count = 0;
                        foreach (Dictionary<object, object> dictR in this.ProductTax(null, BusinessUtility.GetInt(drquery["posProductID"])))
                        {
                            count++;
                            dictResult.Add("Tax" + count, BusinessUtility.GetString(dictR["Tax"]));
                            dictResult.Add("Tax" + count + "Desc", BusinessUtility.GetString(dictR["TaxDesc"]));
                        }
                        if (count <= 4)
                        {
                            for (int i = count + 1; i < 5; i++)
                            {
                                dictResult.Add("Tax" + i, "");
                                dictResult.Add("Tax" + i + "Desc", "");
                            }
                        }
                        lstResult.Add(dictResult);
                    }
                    if (!drquery.IsClosed) drquery.Close();
                }

                return lstResult;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public void DeleteOrder(DbHelper dbHelp, int transID)
        {
            bool mustClose = false;
            string strSQL = "";
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }


            strSQL = @"UPDATE postransaction SET IsClosed=@IsClosed WHERE posTransId=@transID";
            try
            {
                dbHelp.ExecuteNonQuery(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("IsClosed", true, MyDbType.Boolean),
                                                                                                                DbUtility.GetParameter("transID", transID, MyDbType.Int)});
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public void InsertPOSFavProducts(DbHelper dbHelp, int posProductID, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                #region AddProductinUserFavourateList
                string sqlInsert = "";

                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append("SELECT * FROM posFavourateProducts WHERE  posProductId =@posProductId  AND posFavUserId =@posFavUserId ");

                MySqlParameter[] pSelect = {                                            
                                        DbUtility.GetParameter("posProductId", posProductID, MyDbType.Int),
                                        DbUtility.GetParameter("posFavUserId", userID, MyDbType.Int),
                                     };
                DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString(sbQuery.ToString()), System.Data.CommandType.Text, pSelect);
                if (dt.Rows.Count == 0)
                {
                    sqlInsert = @"INSERT INTO posFavourateProducts (posProductId, posFavUserId, CreationDateTime)
                                VALUES (@posProductId, @posFavUserId,  @CreationDateTime)";
                    MySqlParameter[] pInsert = {                                            
                                        DbUtility.GetParameter("posProductId", posProductID, MyDbType.Int),
                                        DbUtility.GetParameter("posFavUserId", userID, MyDbType.Int),
                                        DbUtility.GetParameter("CreationDateTime", DateTime.Now, MyDbType.DateTime)
                                     };

                    dbHelp.ExecuteNonQuery(sqlInsert, System.Data.CommandType.Text, pInsert);
                }
                else if (dt.Rows.Count > 0)
                {
                    sqlInsert = @"UPDATE posFavourateProducts SET  IsFavourate = 1
                                WHERE posProductId = @posProductId AND  posFavUserId = @posFavUserId ";
                    MySqlParameter[] pInsert = {                                            
                                        DbUtility.GetParameter("posProductId", posProductID, MyDbType.Int),
                                        DbUtility.GetParameter("posFavUserId", userID, MyDbType.Int)
                                     };
                    dbHelp.ExecuteNonQuery(sqlInsert, System.Data.CommandType.Text, pInsert);
                }
                #endregion
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }


        public void DeletePOSFavProducts(DbHelper dbHelp, int posProductID, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                #region RemoveProductinUserFavourateList
                string sqlInsert = "";


                sqlInsert = @"UPDATE posFavourateProducts SET  IsFavourate = 0
                                WHERE posProductId = @posProductId AND  posFavUserId = @posFavUserId ";
                MySqlParameter[] pInsert = {                                            
                                        DbUtility.GetParameter("posProductId", posProductID, MyDbType.Int),
                                        DbUtility.GetParameter("posFavUserId", userID, MyDbType.Int)
                                     };
                dbHelp.ExecuteNonQuery(sqlInsert, System.Data.CommandType.Text, pInsert);

                #endregion
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public List<object> ProductsUserFavourate(DbHelper dbHelp, int userID, string lang)
        {

            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            List<object> lstResult = new List<object>();
            string strSQL = @" SELECT p.productID, p.prdIntID, p.prdExtID, p.prdType, p.prdUPCCode, p.prdName, p.prdIsKit, p.prdMinQtyPerSO, p.prdWeight, p.prdWeightPkg, p.prdInoclTerms, p.prdCreatedUserID, p.prdCreatedOn, p.prdLastUpdatedByUserID, 
                                   p.prdLastUpdatedOn, p.prdSalePricePerMinQty, p.prdEndUserSalesPrice, p.prdIsSpecial, p.prdDiscount, p.prdMinQtyPOTrig, p.prdAutoPO, p.prdPOQty, p.prdIsActive, p.prdIsWeb, p.prdComissionCode, p.isPOSMenu, p.prdCategory,
                                   p.prdSubcategory, p.prdWebSalesPrice, p.prdDiscountType, p.prdHeight, p.prdLength, p.prdHeightPkg, p.prdLengthPkg, p.prdWidth, p.prdWidthPkg, p.prdExtendedCategory, p.prdAccomodationType, p.prdSeq, p.TaxGroupID,
                                   pd.prdName, pd.prdSmallDesc, pd.prdLargeDesc,
                                   IFNull(prdSmallImagePath,'DefalutImage.jpg') AS SmallImage,
                                   IFNull(prdLargeImagePath,'DefalutImage.jpg') AS LargeImage,
                                   p.prdIsGlutenFree, p.prdIsVegetarian, p.prdIsContainsNuts, p.prdIsCookedSushi, p.prdSpicyLevel, p.prdSubCategory
                             FROM       products p
                             INNER JOIN  posFavourateProducts AS favPrd ON p.ProductID = favPrd.posProductID
                             INNER JOIN prddescriptions pd ON P.productID=pd.id AND pd.descLang=@lang
                             LEFT JOIN  prdimages pi ON pi.prdID=P.productID
                             WHERE      p.prdISActive=1 AND p.prdIsWeb=1
                             AND favPrd.posFavUserID = @userID AND favPrd.IsFavourate = 1  ORDER BY p.prdName";
            MySqlParameter[] p = {
                                     DbUtility.GetParameter("userID", userID, MyDbType.Int),
                                     DbUtility.GetParameter("lang", lang, MyDbType.String)
                                 };
            try
            {
                MySqlDataReader dr = dbHelp.GetDataReader(strSQL, CommandType.Text, p);
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        Dictionary<object, object> dictResult = new Dictionary<object, object>();
                        dictResult.Add("ProductID", BusinessUtility.GetString(dr["productID"]));
                        dictResult.Add("Type", BusinessUtility.GetString(dr["prdType"]));
                        dictResult.Add("UPCCode", BusinessUtility.GetString(dr["prdUPCCode"]));
                        dictResult.Add("Name", BusinessUtility.GetString(dr["prdName"]));
                        dictResult.Add("IsKit", BusinessUtility.GetString(dr["prdIsKit"]));
                        dictResult.Add("SalesPrice", BusinessUtility.GetString(dr["prdEndUserSalesPrice"]));
                        dictResult.Add("IsSpecial", BusinessUtility.GetString(dr["prdIsSpecial"]));
                        dictResult.Add("Discount", BusinessUtility.GetString(dr["prdDiscount"]));
                        dictResult.Add("DiscountType", BusinessUtility.GetString(dr["prdDiscountType"]));
                        dictResult.Add("WebSalesPrice", BusinessUtility.GetString(dr["prdWebSalesPrice"]));
                        dictResult.Add("TaxGroupID", BusinessUtility.GetString(dr["TaxGroupID"]));
                        dictResult.Add("SmallDesc", BusinessUtility.GetString(dr["prdSmallDesc"]));
                        dictResult.Add("LargeDesc", BusinessUtility.GetString(dr["prdLargeDesc"]));
                        dictResult.Add("SmallImage", BusinessUtility.GetString(dr["SmallImage"]));
                        dictResult.Add("LargeImage", BusinessUtility.GetString(dr["LargeImage"]));

                        dictResult.Add("IsGlutenFree", BusinessUtility.GetString(dr["prdIsGlutenFree"]));
                        dictResult.Add("IsVegetarian", BusinessUtility.GetString(dr["prdIsVegetarian"]));
                        dictResult.Add("IsContainsNuts", BusinessUtility.GetString(dr["prdIsContainsNuts"]));
                        dictResult.Add("IsCookedSushi", BusinessUtility.GetString(dr["prdIsCookedSushi"]));
                        dictResult.Add("SpicyLevel", BusinessUtility.GetString(dr["prdSpicyLevel"]));
                        dictResult.Add("SubCategoryID", BusinessUtility.GetString(dr["prdSubCategory"]));
                        lstResult.Add(dictResult);
                    }
                    if (!dr.IsClosed) dr.Close();
                }
                foreach (Dictionary<object, object> dictResult in lstResult)
                {
                    dictResult.Add("ProductTax", this.ProductTax(dbHelp, BusinessUtility.GetInt(dictResult["ProductID"])));
                }
                return lstResult;
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }

        public double UnPaidPartialAmt(int transID, DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string strSQLPaidPoint = @" SELECT Points  FROM z_loyal_partner_history WHERE PosTransactionID = @PosTransactionID ";
            string strSQLPaypalPaidAmt = @" SELECT DISTINCT PaidAmount from postransaction WHERE posTransId=@transID";
            try
            {
                MySqlDataReader dr2 = dbHelp.GetDataReader(strSQLPaypalPaidAmt, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("transID", transID, MyDbType.Int) });
                if (dr2 != null)
                {
                    if (dr2.Read())
                    {
                        if (BusinessUtility.GetDouble(dr2["PaidAmount"]) > 0)
                        {
                            if (!dr2.IsClosed) dr2.Close();
                            return 0.0;
                        }
                        if (!dr2.IsClosed) dr2.Close();
                    }
                }
                MySqlDataReader dr1 = dbHelp.GetDataReader(strSQLPaidPoint, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("PosTransactionID", transID, MyDbType.Int) });
               
                if (dr1 != null)
                {
                    SysWarehouses _whs = new SysWarehouses();
                    double amtPerPoint = _whs.GetLoyalAmtPerPoint(System.Configuration.ConfigurationManager.AppSettings["WebSaleWhsCode"]);
                    if (dr1.Read())
                    {
                        int paidPoint = (BusinessUtility.GetInt(dr1["Points"]));
                        if (paidPoint < 0)
                            paidPoint = paidPoint * (-1);
                        double paidAmt = (paidPoint * amtPerPoint);
                        List<object> lstResult = new List<object>();
                        lstResult = TotalAmt(null, BusinessUtility.GetInt(transID));
                        double totalAmt = BusinessUtility.GetDouble(lstResult[0]);
                        double unPaidAmt = totalAmt - paidAmt;

                        if (!dr1.IsClosed) dr1.Close();
                        return unPaidAmt;
                    }
                    else
                    {
                        if (!dr1.IsClosed) dr1.Close();
                        return 0.0;
                    }
                }
                else
                {
                    if (!dr1.IsClosed) dr1.Close();
                    return 0.0;
                }
            }
            catch { throw; }
            finally { if (mustClose) dbHelp.CloseDatabaseConnection(); }
        }
    }
}
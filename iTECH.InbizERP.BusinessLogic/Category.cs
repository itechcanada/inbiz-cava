﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web.UI.WebControls;
using System.Web;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Category
    {
        private int _CategoryID;

        public int CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }
        private int _ParentID;

        public int ParentID
        {
            get { return _ParentID; }
            set { _ParentID = value; }
        }
        private string _CategoryEn;

        public string CategoryEn
        {
            get { return _CategoryEn; }
            set { _CategoryEn = value; }
        }
        private string _CategoryFr;

        public string CategoryFr
        {
            get { return _CategoryFr; }
            set { _CategoryFr = value; }
        }
        private bool _IsEditable;

        public bool IsEditable
        {
            get { return _IsEditable; }
            set { _IsEditable = value; }
        }

        private string _CategoryGroup;

        public string CategoryGroup
        {
            get { return _CategoryGroup; }
            set { _CategoryGroup = value; }
        }
        public int CategorySubGroup { get; set; }

        public OperationResult Insert() {
            string exsits = "SELECT COUNT(*) FROM z_categories WHERE CategoryEn=@CategoryEn AND ParentID=@ParentID AND CategoryGroup=@CategoryGroup";
            string insert = "INSERT INTO z_categories (ParentID,CategoryEn,CategoryFr,IsEditable,CategoryGroup,CategorySubGroup) VALUES (@ParentID,@CategoryEn,@CategoryFr,@IsEditable,@CategoryGroup,@CategorySubGroup)";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                int? catSubgroup = this.CategorySubGroup > 0 ? (int?)this.CategorySubGroup : null;
                object count = dbHelp.GetValue(exsits, CommandType.Text, 
                    new MySqlParameter[] { 
                        DbUtility.GetParameter("CategoryEn", this.CategoryEn, MyDbType.String), 
                        DbUtility.GetParameter("CategoryGroup", this.CategoryGroup, MyDbType.String),
                        DbUtility.GetParameter("ParentID", this.ParentID, MyDbType.Int)
                    });
                if (BusinessUtility.GetInt(count) > 0) {
                    return OperationResult.AlreadyExists;
                }
                MySqlParameter[] p = { 
                                         DbUtility.GetParameter("CategoryEn", this.CategoryEn, MyDbType.String),
                                         DbUtility.GetParameter("CategoryFr", this.CategoryFr, MyDbType.String),
                                         DbUtility.GetParameter("ParentID", this.ParentID, MyDbType.Int),
                                         DbUtility.GetParameter("IsEditable", true, MyDbType.Boolean),
                                         DbUtility.GetParameter("CategoryGroup", this.CategoryGroup, MyDbType.String),
                                         DbUtility.GetIntParameter("CategorySubGroup", catSubgroup)
                                     };
                object id = dbHelp.GetValue(insert, CommandType.Text, p);
                this.CategoryID = BusinessUtility.GetInt(id);
                return OperationResult.Success;
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public OperationResult Update() {
            string exsits = "SELECT COUNT(*) FROM z_categories WHERE CategoryEn=@CategoryEn AND CategoryID<>@CategoryID AND ParentID=@ParentID AND CategoryGroup=@CategoryGroup";
            string insert = "UPDATE z_categories  SET ParentID=@ParentID,CategoryEn=@CategoryEn,CategoryFr=@CategoryFr,CategoryGroup=@CategoryGroup, CategorySubGroup=@CategorySubGroup  WHERE CategoryID=@CategoryID";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                int? catSubgroup = this.CategorySubGroup > 0 ? (int?)this.CategorySubGroup : null;
                object count = dbHelp.GetValue(exsits, CommandType.Text, 
                    new MySqlParameter[] { 
                        DbUtility.GetParameter("CategoryEn", this.CategoryEn, MyDbType.String), 
                        DbUtility.GetParameter("CategoryID", this.CategoryID, MyDbType.Int), 
                        DbUtility.GetParameter("CategoryGroup", this.CategoryGroup, MyDbType.String),
                        DbUtility.GetParameter("ParentID", this.ParentID, MyDbType.Int)
                    });
                if (BusinessUtility.GetInt(count) > 0)
                {
                    return OperationResult.AlreadyExists;
                }
                MySqlParameter[] p = { 
                                         DbUtility.GetParameter("CategoryEn", this.CategoryEn, MyDbType.String),
                                         DbUtility.GetParameter("CategoryFr", this.CategoryFr, MyDbType.String),
                                         DbUtility.GetParameter("ParentID", this.ParentID, MyDbType.Int),
                                         DbUtility.GetParameter("CategoryID", this.CategoryID, MyDbType.Int),
                                         DbUtility.GetParameter("CategoryGroup", this.CategoryGroup, MyDbType.String),
                                         DbUtility.GetIntParameter("CategorySubGroup", catSubgroup)
                                     };
                dbHelp.ExecuteNonQuery(insert, CommandType.Text, p);
                return OperationResult.Success;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public OperationResult Delete(int categoryID) {
            string hasChild = "SELECT COUNT(*) FROM z_categories WHERE ParentID = @CategoryID OR CategoryID IN(SELECT prdExtendedCategory FROM products)";
            string sql = "DELETE FROM z_categories WHERE CategoryID=@CategoryID";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object chCount = dbHelp.GetValue(hasChild, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("CategoryID", categoryID, MyDbType.Int) });
                if (BusinessUtility.GetInt(chCount) > 0)
                {
                    return OperationResult.Failuer;
                }
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("CategoryID", categoryID, MyDbType.Int) });
                return OperationResult.Success;
            }
            catch
            {
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(int categoryID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.PopulateObject(dbHelp, categoryID);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject( DbHelper dbHelp, int categoryID)
        {
            string sql = "SELECT * FROM  z_categories WHERE CategoryID=@CategoryID";
           
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("CategoryID", categoryID, MyDbType.Int) });
                if (dr.Read())
                {
                    this.CategoryEn = BusinessUtility.GetString(dr["CategoryEn"]);
                    this.CategoryFr = BusinessUtility.GetString(dr["CategoryFr"]);
                    this.CategoryGroup = BusinessUtility.GetString(dr["CategoryGroup"]);
                    this.CategoryID = BusinessUtility.GetInt(dr["CategoryID"]);
                    this.IsEditable = BusinessUtility.GetBool(dr["IsEditable"]);
                    this.ParentID = BusinessUtility.GetInt(dr["ParentID"]);
                    this.CategorySubGroup = BusinessUtility.GetInt(dr["CategorySubGroup"]);
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                }
            }
        }

        public string GetSql(ParameterCollection pCol, string searchText, string lang) { 
            pCol.Clear();
            string sql = "SELECT z.CategoryID, z.ParentID, " + Globals.GetFieldName("z.Category", lang) + " AS CategoryName, " + Globals.GetFieldName("zp.Category", lang) + " AS ParentCategory, z.IsEditable, z.CategoryGroup, z.CategorySubGroup FROM z_categories z LEFT OUTER JOIN z_categories zp ON zp.CategoryID=z.ParentID WHERE z.IsEditable = 1";
            if (!string.IsNullOrEmpty(searchText)) {
                sql += " AND (z.CategoryEn LIKE CONCAT(@SearchText, '%') OR zp.CategoryEn LIKE CONCAT(@SearchText, '%'))";
                pCol.Add("@SearchText", searchText);
            }
            sql += " ORDER BY CategoryGroup, 4, 3";
            return sql;
        }

        public string GetParentCategoriesSql(ParameterCollection pCol, string catGroup, string searchText, string lang)
        {
            pCol.Clear();
            string sql = "SELECT z.CategoryID, z.ParentID, " + Globals.GetFieldName("z.Category", lang) + " AS CategoryName, " + Globals.GetFieldName("zp.Category", lang) + " AS ParentCategory, z.IsEditable, z.CategoryGroup, z.CategorySubGroup FROM z_categories z LEFT OUTER JOIN z_categories zp ON zp.CategoryID=z.ParentID WHERE z.IsEditable = 1 AND IFNULL(z.ParentID, 0) = 0";
            if (!string.IsNullOrEmpty(catGroup))
            {
                sql += " AND z.CategoryGroup=@CategoryGroup";
                pCol.Add("@CategoryGroup", catGroup);
            }
            if (!string.IsNullOrEmpty(searchText))
            {
                sql += " AND (z.CategoryEn LIKE CONCAT(@SearchText, '%') OR zp.CategoryEn LIKE CONCAT(@SearchText, '%'))";
                pCol.Add("@SearchText", searchText);
            }
            sql += " ORDER BY CategoryGroup, 4, 3";
            return sql;
        }

        public string GetChildCategoriesSql(ParameterCollection pCol, string catGroup, string parentCatGroup, string searchText, string lang)
        {
            pCol.Clear();
            string sql = "SELECT z.CategoryID, z.ParentID, " + Globals.GetFieldName("z.Category", lang) + " AS CategoryName, " + Globals.GetFieldName("zp.Category", lang) + " AS ParentCategory, z.IsEditable, z.CategoryGroup, z.CategorySubGroup FROM z_categories z LEFT OUTER JOIN z_categories zp ON zp.CategoryID=z.ParentID WHERE z.IsEditable = 1 AND IFNULL(z.ParentID, 0) <> 0";
            if (!string.IsNullOrEmpty(catGroup))
            {
                sql += " AND z.CategoryGroup=@CategoryGroup";
                pCol.Add("@CategoryGroup", catGroup);
            }
            if (!string.IsNullOrEmpty(parentCatGroup))
            {
                sql += " AND zp.CategoryGroup=@PCategoryGroup";
                pCol.Add("@PCategoryGroup", parentCatGroup);
            }
            if (!string.IsNullOrEmpty(searchText))
            {
                sql += " AND (z.CategoryEn LIKE CONCAT(@SearchText, '%') OR zp.CategoryEn LIKE CONCAT(@SearchText, '%'))";
                pCol.Add("@SearchText", searchText);
            }
            sql += " ORDER BY CategoryGroup, 4, 3";
            return sql;
        }


        public void FillParentCategory(DropDownList ddl, string lang, int catIdToExclude, string categoryGroup, ListItem rootItem) {
            string sql = "SELECT CategoryID, " + Globals.GetFieldName("Category", lang) + " AS CategoryName FROM z_categories WHERE ParentID=0 AND CategoryID <> @CategoryID ";
            List<MySqlParameter> p = new List<MySqlParameter>();
            if (!string.IsNullOrEmpty(categoryGroup) && categoryGroup != StatusCategoryGroup.ALL)
            {
                sql += " AND CategoryGroup=@CategoryGroup ";
                p.Add(DbUtility.GetParameter("CategoryGroup", categoryGroup, MyDbType.String));
            }
            
            sql += "  AND IsEditable=1 ORDER BY 2 ";
            p.Add(DbUtility.GetParameter("CategoryID", catIdToExclude, MyDbType.Int));
            DbHelper dbHelper = new DbHelper();
            try
            {
                ddl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, p.ToArray());
                ddl.DataTextField = "CategoryName";
                ddl.DataValueField = "CategoryID";
                ddl.DataBind();
                if (rootItem != null)
                {
                    ddl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public void FillBuildings(DropDownList ddl, string lang, ListItem rootItem)
        {
            string sql = "SELECT CategoryID, " + Globals.GetFieldName("Category", lang) + " AS CategoryName FROM z_categories WHERE ParentID=0 AND CategoryGroup=@CategoryGroup AND IsEditable=1 ORDER BY Seq";
            DbHelper dbHelper = new DbHelper();
            try
            {
                ddl.DataSource = dbHelper.GetDataTable(sql, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("CategoryGroup", StatusCategoryGroup.BUILDING, MyDbType.String) });
                ddl.DataTextField = "CategoryName";
                ddl.DataValueField = "CategoryID";
                ddl.DataBind();
                if (rootItem != null)
                {
                    ddl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public List<OptionList> GetRooms(int buildingid, string lang)
        {
            string sql = "SELECT CategoryID, " + Globals.GetFieldName("Category", lang) + " AS CategoryName, CategorySubGroup FROM z_categories ";
            sql += " WHERE ParentID=@ParentID AND CategoryGroup=@CategoryGroup AND IsEditable=1 ORDER BY Seq";
            DbHelper dbHelper = new DbHelper();            
            List<OptionList> lResult = new List<OptionList>();
            try
            {
                DataTable dt = dbHelper.GetDataTable(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ParentID", buildingid, MyDbType.Int), DbUtility.GetParameter("CategoryGroup", StatusCategoryGroup.ROOM, MyDbType.String) });
                foreach (DataRow dr in dt.Rows)
                {
                    OptionList ol = new OptionList();
                    ol.id = BusinessUtility.GetString(dr["CategoryID"]);
                    string roomType = this.GetCategorySubGroup(dbHelper, BusinessUtility.GetInt(dr["CategorySubGroup"]), lang);
                    ol.Name = string.Format("{0}{1}{2}", roomType.Trim(), !string.IsNullOrEmpty(roomType) ? " - " : "", dr["CategoryName"]);
                    lResult.Add(ol);
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        public string GetCategorySubGroup(DbHelper dbHelp, int idVal, string lang)
        {
            string sql = "SELECT sysAppDesc FROM sysstatus WHERE sysApppfx='CT' AND sysAppCode='dlSGp' AND sysAppCodeLang=@sysAppCodeLang AND sysAppLogicCode=@sysAppLogicCode";
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysAppCodeLang", lang, MyDbType.String),
                    DbUtility.GetParameter("sysAppLogicCode", idVal, MyDbType.String)
                });
                return BusinessUtility.GetString(val);
            }
            catch
            {

                throw;
            }            
        }
    }
}

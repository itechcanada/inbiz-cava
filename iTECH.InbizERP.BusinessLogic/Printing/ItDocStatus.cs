﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ItDocStatus
    {
        public int DocID { get; set; }
        public string DocType { get; set; }
        public string DocNo { get; set;  }
        public int DocCtr { get; set; }
        public DateTime DocPrintDateTime { get; set; }
        public int DocPrintedBy { get; set; }
        public int DocCompanyID { get; set; }

        public bool Insert(DbHelper dbHelp, int printBy)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sqlDuplicat = "SELECT count(*) FROM itdocstatus  WHERE docNo=@docNo AND docPrintedBy=@docPrintedBy AND docCompanyID=@docCompanyID";
            string sqlInsert = "INSERT INTO itdocstatus(docType, docNo, docCtr, docPrintDateTime, docPrintedBy,docCompanyID) VALUES";
            sqlInsert += "(@docType, @docNo, @docCtr, @docPrintDateTime, @docPrintedBy,@docCompanyID)";

            try
            {
                object val = dbHelp.GetValue(sqlDuplicat, CommandType.Text, new MySqlParameter[] {                     
                    DbUtility.GetParameter("docNo", this.DocNo, MyDbType.String),                                        
                    DbUtility.GetParameter("docPrintedBy", this.DocPrintedBy, MyDbType.Int),
                    DbUtility.GetParameter("docCompanyID", this.DocCompanyID, MyDbType.Int)                   
                });
                if (BusinessUtility.GetInt(val) <= 0)
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("docType", this.DocType, MyDbType.String),
                        DbUtility.GetParameter("docNo", this.DocNo, MyDbType.String),
                        DbUtility.GetParameter("docCtr", this.DocCtr, MyDbType.Int),
                        DbUtility.GetParameter("docPrintDateTime", this.DocPrintDateTime, MyDbType.DateTime),
                        DbUtility.GetParameter("docPrintedBy", printBy, MyDbType.Int),
                        DbUtility.GetParameter("docCompanyID", this.DocCompanyID, MyDbType.Int) 
                    });
                    return true;
                }
                return false;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update(DbHelper dbHelp, string docNo, int printBy, int companyID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sqlUpdate = "UPDATE itdocstatus SET docCtr = docCtr + 1  WHERE docNo=@docNo AND docPrintedBy=@docPrintedBy AND docCompanyID=@docCompanyID";
            try
            {
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] {                     
                    DbUtility.GetParameter("docNo", docNo, MyDbType.String),                                        
                    DbUtility.GetParameter("docPrintedBy", printBy, MyDbType.Int),
                    DbUtility.GetParameter("docCompanyID", companyID, MyDbType.Int)                   
                });
            }
            catch 
            {                
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool IsDuplicate(DbHelper dbHelp, string docNo, int printBy, int comID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sqlDuplicat = "SELECT count(*) FROM itdocstatus  WHERE docNo=@docNo AND docPrintedBy=@docPrintedBy AND docCompanyID=@docCompanyID";

            try
            {
                object val = dbHelp.GetValue(sqlDuplicat, CommandType.Text, new MySqlParameter[] {                     
                    DbUtility.GetParameter("docNo", docNo, MyDbType.String),                                        
                    DbUtility.GetParameter("docPrintedBy", printBy, MyDbType.Int),
                    DbUtility.GetParameter("docCompanyID", comID, MyDbType.Int)                   
                });
                return BusinessUtility.GetInt(val) > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}

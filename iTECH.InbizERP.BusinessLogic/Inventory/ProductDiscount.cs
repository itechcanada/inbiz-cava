﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;

namespace iTECH.InbizERP.BusinessLogic
{
/*    public class ProductDiscount
    {       
        public int ProductDiscountID { get; set; }
        public int ProductID { get; set; }
        public ProductDiscountType DiscountType { get; set; }
        public int StartDay { get; set; }
        public int EndDay { get; set; }
        public int StartMonth { get; set; }
        public int EndMonth { get; set; }
        public int StartYear { get; set; }
        public int EndYear { get; set; }
        public DateTime StartDate { get { return new DateTime(DateTime.Now.Year, this.StartMonth, this.StartDay); } }
        public DateTime EndDate { get { return new DateTime(DateTime.Now.Year, this.EndMonth, this.EndDay); } }
        public string Day { get; set; }
        public double PercentageDiscount { get; set; }
        public int MinDays { get; set; }
        public int Seq { get; set; }
        public int RoomID { get; set; }

        public void Insert(DbHelper dbHelp)
        {
            string sqlExsits = "SELECT COUNT(*) FROM z_product_discount WHERE ProductID=@ProductID AND DiscountType=@DiscountType AND StartDay=@StartDay AND EndDay=@EndDay AND StartMonth=@StartMonth AND EndMonth=@EndMonth AND Day=@Day";            

            string sqlInsert = "INSERT INTO z_product_discount(ProductID,RoomID,DiscountType,StartDay,StartMonth,EndDay,EndMonth,Day,PercentageDiscount,MinDays,Seq)";
            sqlInsert += " VALUES(@ProductID,@RoomID,@DiscountType,@StartDay,@StartMonth,@EndDay,@EndMonth,@Day,@PercentageDiscount,@MinDays,@Seq)";
            string sqlRoomID = "SELECT prdExtendedCategory FROM products WHERE productID=@ProductID";
            try
            {
                object val = dbHelp.ExecuteNonQuery(sqlExsits, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ProductID", this.ProductID, MyDbType.Int),
                    DbUtility.GetParameter("DiscountType", (int)this.DiscountType, MyDbType.Int),
                    DbUtility.GetParameter("StartDay", this.StartDay, MyDbType.Int),
                    DbUtility.GetParameter("EndDay", this.EndDay, MyDbType.Int),
                    DbUtility.GetParameter("StartMonth", this.StartMonth, MyDbType.Int),
                    DbUtility.GetParameter("EndMonth", this.EndMonth, MyDbType.Int),
                    DbUtility.GetParameter("Day", this.Day, MyDbType.String)

                });

                if (BusinessUtility.GetInt(val) > 0)
                {
                    throw new Exception(CustomExceptionCodes.DISCOUNT_DETAIL_ALREADYE_EXISTS);
                }
                object rid = dbHelp.GetValue(sqlRoomID, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ProductID", this.ProductID, MyDbType.Int)
                });
                dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ProductID", this.ProductID, MyDbType.Int),
                    DbUtility.GetParameter("RoomID", rid, MyDbType.Int),
                    DbUtility.GetParameter("DiscountType", (int)this.DiscountType, MyDbType.Int),
                    DbUtility.GetParameter("StartDay", this.StartDay, MyDbType.Int),
                    DbUtility.GetParameter("EndDay", this.EndDay, MyDbType.Int),
                    DbUtility.GetParameter("StartMonth", this.StartMonth, MyDbType.Int),
                    DbUtility.GetParameter("EndMonth", this.EndMonth, MyDbType.Int),
                    DbUtility.GetParameter("Day", this.Day, MyDbType.String),
                    DbUtility.GetParameter("PercentageDiscount", this.PercentageDiscount, MyDbType.Double),
                    DbUtility.GetParameter("MinDays", this.MinDays, MyDbType.Int),
                    DbUtility.GetParameter("Seq", this.Seq, MyDbType.Int)
                });
                this.ProductDiscountID = dbHelp.GetLastInsertID();
            }
            catch 
            {                
                throw;
            }
        }

        public void Insert() {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.Insert(dbHelp);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }        

        public void PopulateObject(int id)
        {
            string sql = "SELECT * FROM z_product_discount WHERE ProductDiscountID=@ProductDiscountID";
            MySqlDataReader dr = null;
            DbHelper dbHelp = new DbHelper();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ProductDiscountID", id, MyDbType.Int)
                });
                if (dr.Read())
                {
                    this.Day = BusinessUtility.GetString(dr["Day"]);
                    this.DiscountType = (ProductDiscountType)BusinessUtility.GetInt(dr["DiscountType"]);
                    this.EndDay = BusinessUtility.GetInt(dr["EndDay"]);
                    this.MinDays = BusinessUtility.GetInt(dr["MinDays"]);
                    this.PercentageDiscount = BusinessUtility.GetDouble(dr["PercentageDiscount"]);
                    this.ProductDiscountID = BusinessUtility.GetInt(dr["ProductDiscountID"]);
                    this.ProductID = BusinessUtility.GetInt(dr["ProductID"]);
                    this.Seq = BusinessUtility.GetInt(dr["Seq"]);
                    this.StartDay = BusinessUtility.GetInt(dr["StartDay"]);
                    this.EndMonth = BusinessUtility.GetInt(dr["EndMonth"]);
                    this.StartMonth = BusinessUtility.GetInt(dr["StartMonth"]);
                    this.RoomID = BusinessUtility.GetInt(dr["RoomID"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public void Update(DbHelper dbHelp)
        {
            string sqlExsits = "SELECT COUNT(*) FROM z_product_discount WHERE ProductID=@ProductID AND DiscountType=@DiscountType AND StartDay=@StartDay AND EndDay=@EndDay AND StartMonth=@StartMonth AND EndMonth=@EndMonth AND Day=@Day AND ProductDiscountID<>@ProductDiscountID";

            string sqlUpdate = "UPDATE z_product_discount SET DiscountType=@DiscountType,StartDay=@StartDay,EndDay=@EndDay,StartMonth=@StartMonth,EndMonth=@EndMonth,Day=@Day,";
            sqlUpdate += "PercentageDiscount=@PercentageDiscount,MinDays=@MinDays,Seq=@Seq WHERE ProductDiscountID=@ProductDiscountID";            
            try
            {
                object val = dbHelp.ExecuteNonQuery(sqlExsits, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ProductDiscountID", this.ProductDiscountID, MyDbType.Int),
                    DbUtility.GetParameter("ProductID", this.ProductID, MyDbType.Int),
                    DbUtility.GetParameter("DiscountType", (int)this.DiscountType, MyDbType.Int),
                    DbUtility.GetParameter("StartDay", this.StartDay, MyDbType.Int),
                    DbUtility.GetParameter("EndDay", this.EndDay, MyDbType.Int),
                    DbUtility.GetParameter("StartMonth", this.StartMonth, MyDbType.Int),
                    DbUtility.GetParameter("EndMonth", this.EndMonth, MyDbType.Int),
                    DbUtility.GetParameter("Day", this.Day, MyDbType.String)
                });

                if (BusinessUtility.GetInt(val) > 0)
                {
                    throw new Exception(CustomExceptionCodes.DISCOUNT_DETAIL_ALREADYE_EXISTS);
                }

                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ProductDiscountID", this.ProductDiscountID, MyDbType.Int),
                    DbUtility.GetParameter("ProductID", this.ProductID, MyDbType.Int),
                    DbUtility.GetParameter("DiscountType", (int)this.DiscountType, MyDbType.Int),
                    DbUtility.GetParameter("StartDay", this.StartDay, MyDbType.Int),
                    DbUtility.GetParameter("EndDay", this.EndDay, MyDbType.Int),
                    DbUtility.GetParameter("StartMonth", this.StartMonth, MyDbType.Int),
                    DbUtility.GetParameter("EndMonth", this.EndMonth, MyDbType.Int),
                    DbUtility.GetParameter("Day", this.Day, MyDbType.String),
                    DbUtility.GetParameter("PercentageDiscount", this.PercentageDiscount, MyDbType.Double),
                    DbUtility.GetParameter("MinDays", this.MinDays, MyDbType.Int),
                    DbUtility.GetParameter("Seq", this.Seq, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }           
        }

        public void Update()
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.Update(dbHelp);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void DeleteByProductID(int productID)
        {
            string sqlDelete = "DELETE FROM z_product_discount WHERE ProductID=@ProductID";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ProductID", productID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }        

        public void Delete(int id)
        {
            string sqlDelete = "DELETE FROM z_product_discount WHERE ProductDiscountID=@ProductDiscountID";            
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ProductDiscountID", id, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql(ParameterCollection pCol, int produtID)
        {
            pCol.Clear();
            string sql = "SELECT ProductDiscountID,ProductID,DiscountType,StartDay,StartMonth,EndDay,EndMonth,Day,PercentageDiscount,MinDays,Seq ";
            sql += "";
            sql += " FROM z_product_discount";
            if (produtID > 0)
            {
                sql += " WHERE ProductID=@ProductID";
                pCol.Add("@ProductID", produtID.ToString());
            }
            else
            {
                sql += " WHERE ProductID=-1";
            }

            return sql;
        }

        public double CalculateAccommodationPrice(DbHelper dbHelp, int productID, double productPrice, DateTime fromDate, DateTime toDate)
        {
            //string sql = "SELECT * FROM z_product_discount WHERE ProductID=@ProductID AND @MinDays >= MinDays ORDER BY Seq";
            //Retreive all discount on product 
            string sql = "SELECT * FROM z_product_discount WHERE ProductID=@ProductID ORDER BY MinDays DESC";
            int minDays = toDate.Subtract(fromDate).Days; //calcualte total days being reserved
            double total = 0.0D;
            List<DiscountToken> lstDiscount = new List<DiscountToken>();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ProductID", productID, MyDbType.Int)
                    });
                while (dr.Read())
                {
                    DiscountToken tk = new DiscountToken();
                    tk.TotalDaysToApply = BusinessUtility.GetInt(dr["MinDays"]);
                    tk.DiscountToApply = BusinessUtility.GetDouble(dr["PercentageDiscount"]);
                    tk.DiscountType = (ProductDiscountType)BusinessUtility.GetInt(dr["DiscountType"]);
                    tk.DayToValidate = BusinessUtility.GetString(dr["Day"]);
                    tk.StartDay = BusinessUtility.GetInt(dr["StartDay"]);
                    tk.StartMonth = BusinessUtility.GetInt(dr["StartMonth"]);
                    tk.EndDay = BusinessUtility.GetInt(dr["EndDay"]);
                    tk.EndMonth = BusinessUtility.GetInt(dr["EndMonth"]);

                    lstDiscount.Add(tk);
                }

                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                    dr.Dispose();
                }

                Dictionary<DateTime, double> lstDatesDiscount = new Dictionary<DateTime, double>();
                List<DateTime> lstDates = new List<DateTime>();
                List<DateTime> lstDatesApplied = new List<DateTime>();
                for (int i = 0; i < minDays; i++)
                {
                    lstDates.Add(fromDate.AddDays(i));
                    lstDatesDiscount[fromDate.AddDays(i)] = 0.0D;
                }

                foreach (DiscountToken t in lstDiscount)
                {
                    foreach (var cDate in lstDates)
                    {
                        if (DateTime.IsLeapYear(cDate.Year)) //check if date being validated has a leep year need to trim date for 29th Feb.
                        {
                            if (t.StartMonth == 2 && t.StartDay > 28)
                            {
                                t.StartMonth = 3;
                                t.StartDay = 1;
                            }
                            if (t.EndMonth == 2 && t.EndDay > 28)
                            {
                                t.EndMonth = 3;
                                t.EndDay = 1;
                            }
                        }
                        DateTime targetSDate = new DateTime(cDate.Year, t.StartMonth, t.StartDay); //set start date to validate date range with same year of date being validated
                        DateTime targetEDate = new DateTime(cDate.Year, t.EndMonth, t.EndDay);//set end date to validate date range with same year of date being validated
                        int diffOffset = targetEDate.Subtract(cDate).Days; //Set days difference from start date being validated to Checkout Date  
                        int diffOffsetRange = toDate.Subtract(cDate).Days;

                        if (t.DiscountType == ProductDiscountType.DateRange)
                        {
                            if (diffOffsetRange >= t.TotalDaysToApply &&
                                diffOffset >= t.TotalDaysToApply &&
                                cDate >= targetSDate &&
                                cDate <= targetEDate &&
                                !lstDatesApplied.Contains(cDate))
                            {
                                for (int i = 0; i < t.TotalDaysToApply; i++)
                                {
                                    if (lstDatesDiscount.ContainsKey(cDate.AddDays(i)))
                                    {
                                        lstDatesApplied.Add(cDate.AddDays(i));
                                        lstDatesDiscount[cDate.AddDays(i)] = Math.Round((productPrice * (t.DiscountToApply / 100.00D)), 2); //calculate discount   
                                    }
                                }
                            }
                        }
                        else if (!string.IsNullOrEmpty(t.DayToValidate))
                        {
                            DayOfWeek dow = Globals.GetDayOfWeek(t.DayToValidate); //Day of week need to valicated
                            if (cDate.DayOfWeek == dow &&
                                diffOffsetRange >= t.TotalDaysToApply &&
                                diffOffset >= t.TotalDaysToApply &&
                                cDate >= targetSDate &&
                                cDate <= targetEDate &&
                                !lstDatesApplied.Contains(cDate))
                            {
                                for (int i = 0; i < t.TotalDaysToApply; i++)
                                {
                                    if (lstDatesDiscount.ContainsKey(cDate.AddDays(i)))
                                    {
                                        lstDatesApplied.Add(cDate.AddDays(i));
                                        lstDatesDiscount[cDate.AddDays(i)] = Math.Round((productPrice * (t.DiscountToApply / 100.00D)), 2); //calculate discount   
                                    }
                                }
                            }
                        }
                    }
                }

                foreach (var item in lstDatesDiscount.Keys)
                {
                    total += (productPrice - lstDatesDiscount[item]);
                }

                if (ConfigurationManager.AppSettings.AllKeys.Contains("AllowAmountToBeRounded") && ConfigurationManager.AppSettings["AllowAmountToBeRounded"] == "true")
                {
                    return Math.Round(total / (double)minDays); //Return unit price
                }
                return Math.Round(total / (double)minDays, 2); //Return unit price
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null && !dr.IsClosed)
                {
                    dr.Close();
                    dr.Dispose();
                }
            }
        }

        public double CalculateAccommodationPrice(DbHelper dbHelp, int productID, DateTime fromDate, DateTime toDate)
        {
            double productPrice = 0.0D;
            string sqlPrdActualPrice = "SELECT Price FROM vw_beds WHERE BedID=@BedID";
            try
            {
                object val = dbHelp.GetValue(sqlPrdActualPrice, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("BedID", productID, MyDbType.Int)
                });
                productPrice = BusinessUtility.GetDouble(val);

                return this.CalculateAccommodationPrice(dbHelp, productID, productPrice, fromDate, toDate);
            }
            catch
            {
                throw;
            }
        }

        public double CalculateAccommodationPrice(int productID, DateTime fromDate, DateTime toDate)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                return this.CalculateAccommodationPrice(dbHelp, productID, fromDate, toDate);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        private class DiscountToken
        {
            public int TotalDaysToApply { get; set; }
            public ProductDiscountType DiscountType { get; set; }
            public string DayToValidate { get; set; }
            public double DiscountToApply { get; set; }
            public int StartDay { get; set; }
            public int StartMonth { get; set; }
            public int EndDay { get; set; }
            public int EndMonth { get; set; }
        }

        #region Room Level Methods

        public List<DiscountView> GetAllDiscountByRoom(int roomID)
        {
            string sql = "SELECT DISTINCT RoomID,DiscountType,StartDay,StartMonth,EndDay,EndMonth,Day,PercentageDiscount,MinDays,Seq FROM z_product_discount WHERE RoomID=@RoomID";
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            List<DiscountView> lResult = new List<DiscountView>();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("RoomID",  roomID, MyDbType.Int)
                });
                while (dr.Read())
                {
                    DiscountView dv = new DiscountView();
                    dv.Day = BusinessUtility.GetString(dr["Day"]);
                    dv.DiscountType = BusinessUtility.GetInt(dr["DiscountType"]);
                    dv.EndDay = BusinessUtility.GetInt(dr["EndDay"]);
                    dv.EndMonth = BusinessUtility.GetInt(dr["EndMonth"]);
                    dv.MinDays = BusinessUtility.GetInt(dr["MinDays"]);
                    dv.PercentageDiscount = BusinessUtility.GetDouble(dr["PercentageDiscount"]);
                    dv.RoomID = BusinessUtility.GetInt(dr["RoomID"]);
                    dv.StartDay = BusinessUtility.GetInt(dr["StartDay"]);
                    dv.StartMonth = BusinessUtility.GetInt(dr["StartMonth"]);
                    dv.Seq = BusinessUtility.GetInt(dr["Seq"]);
                    lResult.Add(dv);
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public void DeleteByRoomID(DbHelper dbHelp, string rowKey, params int[] roomID)
        {
            string sqlDelete = "DELETE FROM z_product_discount WHERE RoomID IN ({0})";
            sqlDelete = string.Format(sqlDelete, StringUtil.GetJoinedString(",", roomID));
            sqlDelete += " AND CONCAT(CAST(DiscountType AS CHAR(1)), '_', CAST(StartMonth AS CHAR(2)), '_', CAST(StartDay AS CHAR(2)), '_', CAST(EndMonth AS CHAR(2)), '_', CAST(EndDay AS CHAR(2)))=@RowKeyWithoutRoom";
            try
            {
                string[] strKeys = rowKey.Split('_');
                string rowKeyWitout = string.Empty;
                if (strKeys.Length > 0)
                {
                    List<string> lst = strKeys.ToList<string>();
                    lst.RemoveAt(0);
                    rowKeyWitout = string.Join("_", lst.ToArray());
                }
                if (roomID.Length > 0)
                {
                    dbHelp.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("RowKeyWithoutRoom", rowKeyWitout, MyDbType.String)
                    });     
                }                             
            }
            catch
            {

                throw;
            }            
        }

        public void DeleteByRoomID(string rowKey, params int[] roomID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.DeleteByRoomID(dbHelp, rowKey, roomID);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// Returns last Inserted ProductID
        /// </summary>
        /// <param name="arrRoomID"></param>
        /// <param name="insertedIds"></param>
        /// <returns></returns>
        public void ApplyDiscountToRooms(List<DiscountView> lstExistData, params int[] roomID)
        {
            string sqlDel = string.Format("DELETE FROM z_product_discount WHERE RoomID IN ({0})", StringUtil.GetJoinedString(",", roomID));
            string sql = "INSERT INTO z_product_discount(ProductID, RoomID, DiscountType,StartDay,StartMonth,EndDay,EndMonth,Day,PercentageDiscount,MinDays,Seq)";
            sql += " SELECT productID AS ProductID, prdExtendedCategory AS RoomID, @DiscountType AS DiscountType, @StartDay AS StartDay, @StartMonth AS StartMonth, @EndDay AS EndDay,";
            sql += " @EndMonth AS EndMonth, @Day AS Day, @PercentageDiscount AS PercentageDiscount, @MinDays AS MinDays, 1 AS Seq";
            sql += string.Format(" FROM products WHERE prdExtendedCategory IN({0})", StringUtil.GetJoinedString(",", roomID));

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                if (roomID.Length > 0)
                {
                    dbHelp.ExecuteNonQuery(sqlDel, CommandType.Text, null);

                    foreach (var item in lstExistData)
                    {
                        dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                            DbUtility.GetParameter("DiscountType", item.DiscountType, MyDbType.Int),
                            DbUtility.GetParameter("StartDay", item.StartDay, MyDbType.Int),
                            DbUtility.GetParameter("StartMonth", item.StartMonth, MyDbType.Int),
                            DbUtility.GetParameter("EndDay", item.EndDay, MyDbType.Int),
                            DbUtility.GetParameter("EndMonth", item.EndMonth, MyDbType.Int),
                            DbUtility.GetParameter("Day", item.Day, MyDbType.String),
                            DbUtility.GetParameter("PercentageDiscount", item.PercentageDiscount, MyDbType.Double),
                            DbUtility.GetParameter("MinDays", item.MinDays, MyDbType.Int)
                        });   
                    }  
                 
                    //In last add new entry
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("DiscountType", (int)this.DiscountType, MyDbType.Int),
                        DbUtility.GetParameter("StartDay", this.StartDay, MyDbType.Int),
                        DbUtility.GetParameter("StartMonth", this.StartMonth, MyDbType.Int),
                        DbUtility.GetParameter("EndDay", this.EndDay, MyDbType.Int),
                        DbUtility.GetParameter("EndMonth", this.EndMonth, MyDbType.Int),
                        DbUtility.GetParameter("Day", this.Day, MyDbType.String),
                        DbUtility.GetParameter("PercentageDiscount", this.PercentageDiscount, MyDbType.Double),
                        DbUtility.GetParameter("MinDays", this.MinDays, MyDbType.Int)
                    });
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateDiscountToRoom(string rowKey, params int[] roomID)
        {
            string sqlUpdate = "UPDATE z_product_discount SET StartDay=@StartDay,EndDay=@EndDay,StartMonth=@StartMonth,EndMonth=@EndMonth,Day=@Day,";
            sqlUpdate += "PercentageDiscount=@PercentageDiscount,MinDays=@MinDays,Seq=@Seq WHERE ";
            sqlUpdate += string.Format(" RoomID IN({0})", StringUtil.GetJoinedString(",", roomID));            
            sqlUpdate += " AND CONCAT(CAST(DiscountType AS CHAR(1)), '_', CAST(StartMonth AS CHAR(2)), '_', CAST(StartDay AS CHAR(2)), '_', CAST(EndMonth AS CHAR(2)), '_', CAST(EndDay AS CHAR(2)))=@RowKeyWithoutRoom";

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                string[] strKeys = rowKey.Split('_');
                string rowKeyWitout = string.Empty;
                if (strKeys.Length > 0)
                {
                    List<string> lst = strKeys.ToList<string>();
                    lst.RemoveAt(0);
                    rowKeyWitout = string.Join("_", lst.ToArray());
                }
                if (roomID.Length > 0)
                {
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("RowKeyWithoutRoom", rowKeyWitout, MyDbType.String),                        
                        DbUtility.GetParameter("StartDay", this.StartDay, MyDbType.Int),
                        DbUtility.GetParameter("EndDay", this.EndDay, MyDbType.Int),
                        DbUtility.GetParameter("StartMonth", this.StartMonth, MyDbType.Int),
                        DbUtility.GetParameter("EndMonth", this.EndMonth, MyDbType.Int),
                        DbUtility.GetParameter("Day", this.Day, MyDbType.String),
                        DbUtility.GetParameter("PercentageDiscount", this.PercentageDiscount, MyDbType.Double),
                        DbUtility.GetParameter("MinDays", this.MinDays, MyDbType.Int),
                        DbUtility.GetParameter("Seq", this.Seq, MyDbType.Int)
                    });
                }   
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public DiscountView GetDiscountView(string rowKey)
        {
            string sql = "SELECT * FROM z_product_discount";
            sql += " WHERE CONCAT(CAST(RoomID AS CHAR(10)), '_',CAST(DiscountType AS CHAR(1)), '_', CAST(StartMonth AS CHAR(2)), '_', CAST(StartDay AS CHAR(2)), '_', CAST(EndMonth AS CHAR(2)), '_', CAST(EndDay AS CHAR(2)))=@RowKey";
            sql += " LIMIT 1";

            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            DiscountView dv = new DiscountView();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("RowKey", rowKey, MyDbType.String)
                });
                if (dr.Read())
                {
                    dv.Day = BusinessUtility.GetString(dr["Day"]);
                    dv.DiscountType = BusinessUtility.GetInt(dr["DiscountType"]);
                    dv.EndDay = BusinessUtility.GetInt(dr["EndDay"]);
                    dv.EndMonth = BusinessUtility.GetInt(dr["EndMonth"]);
                    dv.MinDays = BusinessUtility.GetInt(dr["MinDays"]);
                    dv.PercentageDiscount = BusinessUtility.GetDouble(dr["PercentageDiscount"]);
                    dv.RoomID = BusinessUtility.GetInt(dr["RoomID"]);
                    dv.StartDay = BusinessUtility.GetInt(dr["StartDay"]);
                    dv.StartMonth = BusinessUtility.GetInt(dr["StartMonth"]);
                    dv.Seq = BusinessUtility.GetInt(dr["Seq"]);
                }
                return dv;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }
        #endregion
    }

    public class DiscountView
    {
        public int RoomID { get; set; }
        public int DiscountType { get; set; }
        public int StartDay { get; set; }
        public int StartMonth { get; set; }
        public int EndDay { get; set; }
        public int EndMonth { get; set; }
        public int MinDays { get; set; }
        public string Day { get; set; }
        public int Seq { get; set; }
        public double PercentageDiscount { get; set; }
        public DateTime StartDate { get { return new DateTime(DateTime.Now.Year, this.StartMonth, this.StartDay); } }
        public DateTime EndDate { get { return new DateTime(DateTime.Now.Year, this.EndMonth, this.EndDay); } }
        public string GridPrimaryKey { get { return string.Format("{0}_{1}_{2}_{3}_{4}_{5}", this.RoomID, this.DiscountType, this.StartMonth, this.StartDay, this.EndMonth, this.EndDay); } }
    }  */
    
    public class ProductDiscount
    {
        public int DiscountID { get; set; }
        public int ProductID { get; set; }
        public int RoomID { get; set; }
        public DiscountCategory DiscountCategory { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Day { get; set; }
        public int MinDays { get; set; }
        public DiscountApplyOn ApplyType { get; set; }
        public double PercentageDiscount { get; set; }
        public double PackagePrice { get; set; }
        public int Seq { get; set; }

        public void ApplyDiscoountToRooms(List<ProductDiscountView> lstDiscount, params int[] roomID)
        {
            DbHelper dbHelp = new DbHelper(true);
            string sqlDeleteAllDiscount = string.Format("DELETE FROM z_product_discount WHERE RoomID IN ({0});", StringUtil.GetJoinedString(",", roomID));
            string sqlInsert = "INSERT INTO z_product_discount (ProductID,RoomID,DiscountCategory,StartDate,EndDate,Day,MinDays,ApplyType,PercentageDiscount,PackagePrice,Seq)";
            sqlInsert += " SELECT productID AS ProductID, prdExtendedCategory AS RoomID, @DiscountCategory AS DiscountCategory,";
            sqlInsert += " @StartDate AS StartDate,@EndDate AS EndDate,@Day AS Day,@MinDays AS MinDays,";
            sqlInsert += " @ApplyType AS ApplyType,@PercentageDiscount AS PercentageDiscount, @PackagePrice AS PackagePrice,@Seq AS Seq FROM ";
            sqlInsert += string.Format(" products WHERE prdExtendedCategory IN({0})", StringUtil.GetJoinedString(",", roomID));

            try
            {
                dbHelp.ExecuteNonQuery(sqlDeleteAllDiscount, CommandType.Text, null);
                foreach (var item in lstDiscount)
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ApplyType", (int)item.ApplyType, MyDbType.Int),
                        DbUtility.GetParameter("Day", item.Day, MyDbType.String),
                        DbUtility.GetParameter("DiscountCategory", (int)item.DiscountCategory, MyDbType.Int),
                        DbUtility.GetParameter("EndDate", item.EndDate, MyDbType.DateTime),
                        DbUtility.GetParameter("MinDays", item.MinDays, MyDbType.Int),
                        DbUtility.GetParameter("PercentageDiscount", item.PercentageDiscount, MyDbType.Double),                    
                        DbUtility.GetParameter("Seq", item.Seq, MyDbType.Int),
                        DbUtility.GetParameter("StartDate", item.StartDate, MyDbType.DateTime),
                        DbUtility.GetParameter("PackagePrice", item.PackagePrice, MyDbType.Double)
                    });
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void ApplyDiscountToProduct(List<ProductDiscountView> lstDiscount, int productID)
        {
            DbHelper dbHelp = new DbHelper(true);
            string sqlDeleteAllDiscount = string.Format("DELETE FROM z_product_discount WHERE ProductID = {0};", productID);
            string sqlInsert = "INSERT INTO z_product_discount (ProductID,RoomID,DiscountCategory,StartDate,EndDate,Day,MinDays,ApplyType,PercentageDiscount,Seq)";
            sqlInsert += " SELECT productID AS ProductID, prdExtendedCategory AS RoomID, @DiscountCategory AS DiscountCategory,";
            sqlInsert += " @StartDate AS StartDate,@EndDate AS EndDate,@Day AS Day,@MinDays AS MinDays,";
            sqlInsert += " @ApplyType AS ApplyType,@PercentageDiscount AS PercentageDiscount,@Seq AS Seq FROM ";
            sqlInsert += string.Format(" products WHERE products.ProductID ={0}", productID);
            try
            {
                dbHelp.ExecuteNonQuery(sqlDeleteAllDiscount, CommandType.Text, null);
                foreach (var item in lstDiscount)
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ApplyType", (int)item.ApplyType, MyDbType.Int),
                    DbUtility.GetParameter("Day", item.Day, MyDbType.String),
                    DbUtility.GetParameter("DiscountCategory", (int)item.DiscountCategory, MyDbType.Int),
                    DbUtility.GetParameter("EndDate", item.EndDate, MyDbType.DateTime),
                    DbUtility.GetParameter("MinDays", item.MinDays, MyDbType.Int),
                    DbUtility.GetParameter("PercentageDiscount", item.PercentageDiscount, MyDbType.Double),                    
                    DbUtility.GetParameter("Seq", item.Seq, MyDbType.Int),
                    DbUtility.GetParameter("StartDate", item.StartDate, MyDbType.DateTime)
                });
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductDiscountView> GetAllDiscountByRoom(int roomID)
        {
            string sql = "SELECT DISTINCT d.DiscountCategory, d.StartDate,d.EndDate,d.Day,d.MinDays,d.ApplyType,d.PercentageDiscount,d.Seq,d.PackagePrice";
            sql += " FROM z_product_discount d WHERE d.RoomID=@RoomID ORDER BY MinDays DESC";
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            List<ProductDiscountView> lResult = new List<ProductDiscountView>();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                DbUtility.GetParameter("RoomID", roomID, MyDbType.Int)
            });
                int iCounter = 0;
                while (dr.Read())
                {
                    ProductDiscountView pd = new ProductDiscountView();
                    pd.ApplyType = (DiscountApplyOn)BusinessUtility.GetInt(dr["ApplyType"]);
                    pd.Day = BusinessUtility.GetString(dr["Day"]);
                    pd.DiscountCategory = (DiscountCategory)BusinessUtility.GetInt(dr["DiscountCategory"]);
                    pd.EndDate = BusinessUtility.GetDateTime(dr["EndDate"]);
                    pd.MinDays = BusinessUtility.GetInt(dr["MinDays"]);
                    pd.PercentageDiscount = BusinessUtility.GetDouble(dr["PercentageDiscount"]);
                    pd.RowIndex = ++iCounter;
                    pd.Seq = BusinessUtility.GetInt(dr["Seq"]);
                    pd.StartDate = BusinessUtility.GetDateTime(dr["StartDate"]);
                    pd.PackagePrice = BusinessUtility.GetDouble(dr["PackagePrice"]);

                    //Update Year if applied on all year
                    if (pd.ApplyType == DiscountApplyOn.AllYears)
                    {
                        if (pd.StartDate.Month == 2 && pd.StartDate.Day > 28 && DateTime.DaysInMonth(DateTime.Today.Year, 2) == 28)
                        {
                            pd.StartDate = new DateTime(DateTime.Today.Year, 3, 1);
                        }
                        else
                        {
                            pd.StartDate = new DateTime(DateTime.Today.Year, pd.StartDate.Month, pd.StartDate.Day);
                        }

                        if (pd.EndDate.Month == 2 && pd.EndDate.Day > 28 && DateTime.DaysInMonth(DateTime.Today.Year, 2) == 28)
                        {
                            pd.EndDate = new DateTime(DateTime.Today.Year, 3, 1);
                        }
                        else
                        {
                            pd.EndDate = new DateTime(DateTime.Today.Year, pd.EndDate.Month, pd.EndDate.Day);
                        }
                    }

                    lResult.Add(pd);
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public List<ProductDiscountView> GetAllDiscountByBed(int productID)
        {            
            DbHelper dbHelp = new DbHelper();
            
            try
            {
                return this.GetAllDiscountByBed(dbHelp, productID);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductDiscountView> GetAllDiscountByBed(DbHelper dbHelp, int productID)
        {
            string sql = "SELECT DISTINCT d.DiscountCategory, d.StartDate,d.EndDate,d.Day,d.MinDays,d.ApplyType,d.PercentageDiscount,d.Seq,d.PackagePrice";
            sql += " FROM z_product_discount d WHERE d.ProductID=@ProductID ORDER BY MinDays DESC";            
            MySqlDataReader dr = null;
            List<ProductDiscountView> lResult = new List<ProductDiscountView>();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                DbUtility.GetParameter("ProductID", productID, MyDbType.Int)
            });
                int iCounter = 0;
                while (dr.Read())
                {
                    ProductDiscountView pd = new ProductDiscountView();
                    pd.ApplyType = (DiscountApplyOn)BusinessUtility.GetInt(dr["ApplyType"]);
                    pd.Day = BusinessUtility.GetString(dr["Day"]);
                    pd.DiscountCategory = (DiscountCategory)BusinessUtility.GetInt(dr["DiscountCategory"]);
                    pd.EndDate = BusinessUtility.GetDateTime(dr["EndDate"]);
                    pd.MinDays = BusinessUtility.GetInt(dr["MinDays"]);
                    pd.PercentageDiscount = BusinessUtility.GetDouble(dr["PercentageDiscount"]);
                    pd.RowIndex = ++iCounter;
                    pd.Seq = BusinessUtility.GetInt(dr["Seq"]);
                    pd.StartDate = BusinessUtility.GetDateTime(dr["StartDate"]);
                    pd.PackagePrice = BusinessUtility.GetDouble(dr["PackagePrice"]);

                    //Update Year if applied on all year
                    if (pd.ApplyType == DiscountApplyOn.AllYears)
                    {
                        if (pd.StartDate.Month == 2 && pd.StartDate.Day > 28 && DateTime.DaysInMonth(DateTime.Today.Year, 2) == 28)
                        {
                            pd.StartDate = new DateTime(DateTime.Today.Year, 3, 1);
                        }
                        else
                        {
                            pd.StartDate = new DateTime(DateTime.Today.Year, pd.StartDate.Month, pd.StartDate.Day);
                        }

                        if (pd.EndDate.Month == 2 && pd.EndDate.Day > 28 && DateTime.DaysInMonth(DateTime.Today.Year, 2) == 28)
                        {
                            pd.EndDate = new DateTime(DateTime.Today.Year, 3, 1);
                        }
                        else
                        {
                            pd.EndDate = new DateTime(DateTime.Today.Year, pd.EndDate.Month, pd.EndDate.Day);
                        }
                    }

                    lResult.Add(pd);
                }
                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr!= null && !dr.IsClosed)
                {
                    dr.Close();
                }                
            }
        }

        //Below function was working till 2012-12-24 (Updated version of similar function starts just under this commented version)
        //public double CalculateAccommodationPrice(DbHelper dbHelp, int productID, double productPrice, DateTime fromDate, DateTime toDate)
        //{
        //    //string sql = "SELECT * FROM z_product_discount WHERE ProductID=@ProductID AND @MinDays >= MinDays ORDER BY Seq";
        //    //Retreive all discount on product 
        //    //string sql = "SELECT * FROM z_product_discount WHERE ProductID=@ProductID ORDER BY MinDays DESC";

        //    int minDays = toDate.Subtract(fromDate).Days; //calcualte total days being reserved
        //    double total = 0.0D;
        //    List<ProductDiscountView> lstDiscount = this.GetAllDiscountByBed(dbHelp, productID); //Retreive all available discount by bed        
        //    try
        //    {
        //        Dictionary<DateTime, double> lstDatesDiscount = new Dictionary<DateTime, double>();
        //        List<DateTime> lstDates = new List<DateTime>();
        //        List<DateTime> lstDatesApplied = new List<DateTime>();
        //        for (int i = 0; i < minDays; i++)
        //        {
        //            lstDates.Add(fromDate.AddDays(i));
        //            lstDatesDiscount[fromDate.AddDays(i)] = 0.0D;
        //        }

        //        foreach (ProductDiscountView t in lstDiscount)
        //        {
        //            foreach (var cDate in lstDates)
        //            {
        //                DateTime targetSDate = t.StartDate; //set start date to validate date range with same year of date being validated
        //                DateTime targetEDate = t.EndDate; //set end date to validate date range with same year of date being validated
        //                if (t.ApplyType == DiscountApplyOn.AllYears)
        //                {
        //                    if (!DateTime.IsLeapYear(cDate.Year) && t.StartDate.Month == 2 && t.StartDate.Day > 28)
        //                    {
        //                        targetSDate = new DateTime(cDate.Year, 3, 1);
        //                    }
        //                    else
        //                    {
        //                        targetSDate = new DateTime(cDate.Year, t.StartDate.Month, t.StartDate.Day);
        //                    }
        //                    if (!DateTime.IsLeapYear(cDate.Year) && t.EndDate.Month == 2 && t.EndDate.Day > 28)
        //                    {
        //                        targetEDate = new DateTime(cDate.Year, 3, 1);
        //                    }
        //                    else
        //                    {
        //                        targetEDate = new DateTime(cDate.Year, t.EndDate.Month, t.EndDate.Day);
        //                    }
        //                }
        //                int diffOffset = targetEDate.Subtract(cDate).Days; //Set days difference from start date being validated to Checkout Date  
        //                int diffOffsetRange = toDate.Subtract(cDate).Days;

        //                if (t.DiscountCategory == DiscountCategory.DateRange || t.DiscountCategory == DiscountCategory.PackageSpecial)
        //                {
        //                    if (diffOffsetRange >= t.MinDays &&
        //                        //diffOffset >= t.MinDays &&
        //                        diffOffset >= 0 &&
        //                        cDate >= targetSDate &&
        //                        cDate <= targetEDate &&
        //                        !lstDatesApplied.Contains(cDate))
        //                    {
        //                        for (int i = 0; i < t.MinDays; i++)
        //                        {
        //                            if (lstDatesDiscount.ContainsKey(cDate.AddDays(i)))
        //                            {
        //                                lstDatesApplied.Add(cDate.AddDays(i));
        //                                lstDatesDiscount[cDate.AddDays(i)] = (productPrice * (t.PercentageDiscount / 100.00D)); //calculate discount   
        //                            }
        //                        }
        //                    }
        //                }
        //                else if (!string.IsNullOrEmpty(t.Day))
        //                {
        //                    DayOfWeek dow = Globals.GetDayOfWeek(t.Day); //Day of week need to valicated
        //                    if (cDate.DayOfWeek == dow &&
        //                        diffOffsetRange >= t.MinDays &&
        //                        //diffOffset >= t.MinDays &&
        //                        diffOffset >= 0 &&
        //                        cDate >= targetSDate &&
        //                        cDate <= targetEDate &&
        //                        !lstDatesApplied.Contains(cDate))
        //                    {
        //                        for (int i = 0; i < t.MinDays; i++)
        //                        {
        //                            if (lstDatesDiscount.ContainsKey(cDate.AddDays(i)))
        //                            {
        //                                lstDatesApplied.Add(cDate.AddDays(i));
        //                                lstDatesDiscount[cDate.AddDays(i)] = (productPrice * (t.PercentageDiscount / 100.00D)); //calculate discount   
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        foreach (var item in lstDatesDiscount.Keys)
        //        {
        //            total += (productPrice - lstDatesDiscount[item]);
        //        }

        //        /*if (ConfigurationManager.AppSettings.AllKeys.Contains("AllowAmountToBeRounded") && ConfigurationManager.AppSettings["AllowAmountToBeRounded"] == "true")
        //        {
        //            return Math.Round(total / (double)minDays); //Return unit price
        //        }*/
        //        return total / (double)minDays; //Return unit price
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {

        //    }
        //}

        /// <summary>
        /// Update on 2012-12-24 to fix prize calculation
        /// </summary>        
        public double CalculateAccommodationPrice(DbHelper dbHelp, int productID, double productPrice, DateTime fromDate, DateTime toDate)
        {
            //string sql = "SELECT * FROM z_product_discount WHERE ProductID=@ProductID AND @MinDays >= MinDays ORDER BY Seq";
            //Retreive all discount on product 
            //string sql = "SELECT * FROM z_product_discount WHERE ProductID=@ProductID ORDER BY MinDays DESC";
            
            int minDays = toDate.Subtract(fromDate).Days; //calcualte total days being reserved
            double total = 0.0D;
            List<ProductDiscountView> lstDiscount = this.GetAllDiscountByBed(dbHelp, productID); //Retreive all available discount by bed        
            try
            {               
                Dictionary<DateTime, double> lstDatesDiscount = new Dictionary<DateTime, double>();
                List<DateTime> lstDates = new List<DateTime>();
                List<DateTime> lstDatesApplied = new List<DateTime>();
                for (int i = 0; i < minDays; i++)
                {
                    lstDates.Add(fromDate.AddDays(i));
                    lstDatesDiscount[fromDate.AddDays(i)] = 0.0D;
                }

                foreach (ProductDiscountView t in lstDiscount)
                {
                    foreach (var cDate in lstDates)
                    {                        
                        DateTime targetSDate = t.StartDate; //set start date to validate date range with same year of date being validated
                        DateTime targetEDate = t.EndDate; //set end date to validate date range with same year of date being validated
                        if (t.ApplyType == DiscountApplyOn.AllYears)
                        {
                            if (!DateTime.IsLeapYear(cDate.Year) && t.StartDate.Month == 2 && t.StartDate.Day > 28)
                            {
                                targetSDate = new DateTime(cDate.Year, 3, 1);
                            }
                            else
                            {
                                targetSDate = new DateTime(cDate.Year, t.StartDate.Month, t.StartDate.Day);
                            }
                            if (!DateTime.IsLeapYear(cDate.Year) && t.EndDate.Month == 2 && t.EndDate.Day > 28)
                            {
                                targetEDate = new DateTime(cDate.Year, 3, 1);
                            }
                            else
                            {
                                targetEDate = new DateTime(cDate.Year, t.EndDate.Month, t.EndDate.Day);
                            }
                        }
                        int diffOffset = targetEDate.Subtract(cDate).Days; //How far is date being validate from checkout date in discount checkout date //Set days difference from start date being validated to Checkout Date  
                        int diffOffsetRange = toDate.Subtract(cDate).Days;//How far is date being validate from checkout date in current reservation

                        if (t.DiscountCategory == DiscountCategory.DateRange || t.DiscountCategory == DiscountCategory.PackageSpecial)
                        {
                            if (diffOffsetRange >= t.MinDays &&
                                diffOffset >= t.MinDays &&
                                //diffOffset >= 0 &&
                                cDate >= targetSDate &&
                                cDate <= targetEDate &&
                                !lstDatesApplied.Contains(cDate))
                            {
                                for (int i = 0; i < t.MinDays; i++)
                                {
                                    if (lstDatesDiscount.ContainsKey(cDate.AddDays(i)))
                                    {
                                        lstDatesApplied.Add(cDate.AddDays(i));
                                        lstDatesDiscount[cDate.AddDays(i)] = (productPrice * (t.PercentageDiscount / 100.00D)); //calculate discount   
                                    }
                                }
                            }
                        }
                        else if (!string.IsNullOrEmpty(t.Day))
                        {
                            DayOfWeek dow = Globals.GetDayOfWeek(t.Day); //Day of week need to valicated
                            if (cDate.DayOfWeek == dow &&
                                diffOffsetRange >= t.MinDays &&
                                diffOffset >= t.MinDays &&
                                //diffOffset >= 0 &&
                                cDate >= targetSDate &&
                                cDate <= targetEDate &&
                                !lstDatesApplied.Contains(cDate))
                            {
                                for (int i = 0; i < t.MinDays; i++)
                                {
                                    if (lstDatesDiscount.ContainsKey(cDate.AddDays(i)))
                                    {
                                        lstDatesApplied.Add(cDate.AddDays(i));
                                        lstDatesDiscount[cDate.AddDays(i)] = (productPrice * (t.PercentageDiscount / 100.00D)); //calculate discount   
                                    }
                                }
                            }
                        }
                    }
                }

                foreach (var item in lstDatesDiscount.Keys)
                {
                    total += (productPrice - lstDatesDiscount[item]);
                }

                /*if (ConfigurationManager.AppSettings.AllKeys.Contains("AllowAmountToBeRounded") && ConfigurationManager.AppSettings["AllowAmountToBeRounded"] == "true")
                {
                    return Math.Round(total / (double)minDays); //Return unit price
                }*/
                return total / (double)minDays; //Return unit price
            }
            catch
            {
                throw;
            }
            finally
            {
                
            }
        }

        public double CalculateAccommodationPrice(DbHelper dbHelp, int productID, DateTime fromDate, DateTime toDate)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            double productPrice = 0.0D;
            string sqlPrdActualPrice = "SELECT Price FROM vw_beds WHERE BedID=@BedID";
            try
            {
                object val = dbHelp.GetValue(sqlPrdActualPrice, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("BedID", productID, MyDbType.Int)
                });
                productPrice = BusinessUtility.GetDouble(val);

                return this.CalculateAccommodationPrice(dbHelp, productID, productPrice, fromDate, toDate);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //public double CalculateAccommodationPrice(int productID, DateTime fromDate, DateTime toDate)
        //{
        //    DbHelper dbHelp = new DbHelper(true);
        //    try
        //    {
        //        return this.CalculateAccommodationPrice(dbHelp, productID, fromDate, toDate);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}
    }

    public class ProductDiscountView
    {
        public int RowIndex { get; set; }
        public int DiscountID { get; set; }
        public int ProductID { get; set; }
        public int RoomID { get; set; }
        public DiscountCategory DiscountCategory { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Day { get; set; }
        public int MinDays { get; set; }
        public DiscountApplyOn ApplyType { get; set; }
        public double PercentageDiscount { get; set; }
        public double PackagePrice { get; set; }
        public int Seq { get; set; }
    }
    
    public enum DiscountCategory
    {
        DateRange = 1,
        DaySpecial = 2,
        PackageSpecial = 3
    }

    public enum DiscountApplyOn
    {
        AllYears = 1,
        DuringSelectedDateRangeOnly = 2
    }
}

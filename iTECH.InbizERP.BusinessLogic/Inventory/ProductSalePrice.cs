﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web.UI.WebControls;
using System.Web;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductSalePrice
    {
        int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        int _prdSalesPriceID;

        public int PrdSalesPriceID
        {
            get { return _prdSalesPriceID; }
            set { _prdSalesPriceID = value; }
        }
        int _fromQty;

        public int FromQty
        {
            get { return _fromQty; }
            set { _fromQty = value; }
        }
        int _toQty;

        public int ToQty
        {
            get { return _toQty; }
            set { _toQty = value; }
        }
        double _salesPrice;

        public double SalesPrice
        {
            get { return _salesPrice; }
            set { _salesPrice = value; }
        }
        int _fromRushQty;

        public int FromRushQty
        {
            get { return _fromRushQty; }
            set { _fromRushQty = value; }
        }
        int _toRushQty;

        public int ToRushQty
        {
            get { return _toRushQty; }
            set { _toRushQty = value; }
        }
        double _rushSalesPrice;

        public double RushSalesPrice
        {
            get { return _rushSalesPrice; }
            set { _rushSalesPrice = value; }
        }

        public string GetSql(ParameterCollection pCol, int productID)
        {
            pCol.Clear();
            string sql = "SELECT  id , prdSalesPriceID, fromQty, toQty, salesPrice, fromRushQty, toRushQty, rushSalesPrice ";
            sql += " FROM productsalesprice where prdSalesPriceID=@prdSalesPriceID order by prdSalesPriceID, fromQty";
            pCol.Add("@prdSalesPriceID", productID.ToString());
            return sql;
        }

        public void Insert()
        {
            string sqlCheck = "SELECT count(*) FROM productsalesprice where prdSalesPriceID=@prdSalesPriceID ";
            sqlCheck += " AND ((@fromQty BETWEEN fromQty AND toQty) OR (@toQty BETWEEN fromQty AND toQty)";
            sqlCheck += " OR (fromQty BETWEEN @fromQty AND @toQty))";
            string sqlInsert = "INSERT INTO productsalesprice (prdSalesPriceID,fromQty,toQty,salesPrice,fromRushQty,toRushQty,rushSalesPrice)";
            sqlInsert += " VALUES(@prdSalesPriceID,@fromQty,@toQty,@salesPrice,@fromRushQty,@toRushQty,@rushSalesPrice)";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object val = dbHelp.GetValue(sqlCheck, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("prdSalesPriceID", this.PrdSalesPriceID, MyDbType.Int),
                    DbUtility.GetParameter("fromQty", this.FromQty, MyDbType.Int),
                    DbUtility.GetParameter("toQty", this.ToQty, MyDbType.Int)
                });
                if (BusinessUtility.GetInt(val) > 0)
                {
                    throw new Exception(CustomExceptionCodes.PRODUCT_QTY_RANGE_ALREADY_EXISTS);
                }
                dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                     DbUtility.GetParameter("prdSalesPriceID", this.PrdSalesPriceID, MyDbType.Int),
                    DbUtility.GetParameter("fromQty", this.FromQty, MyDbType.Int),
                    DbUtility.GetParameter("toQty", this.ToQty, MyDbType.Int),
                    DbUtility.GetParameter("fromRushQty", this.FromRushQty, MyDbType.Int),
                    DbUtility.GetParameter("toRushQty", this.ToRushQty, MyDbType.Int),
                    DbUtility.GetParameter("salesPrice", this.SalesPrice, MyDbType.Double),
                    DbUtility.GetParameter("rushSalesPrice", this.RushSalesPrice, MyDbType.Double)
                });
                this.Id = dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Delete(int id)
        {
            string sql = "DELETE FROM productsalesprice WHERE id=@id";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("id", id, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}

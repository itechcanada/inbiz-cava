﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductPublishEdit
    {
        public int MasterID { get; set; }

        public Boolean Save(int userID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {

                int iMasterID = this.MasterID;

                ProductMasterDetail objPMDetail = new ProductMasterDetail();
                objPMDetail.MasterID = this.MasterID;
                objPMDetail.PopulateObject(null, BusinessUtility.GetInt(this.MasterID));

                string sSqlCollection = " SELECT * FROM  ProdMstCol WHERE MasterID =  " + this.MasterID;
                DataTable dtCollection = dbTransactionHelper.GetDataTable(sSqlCollection, CommandType.Text, null);

                string sSqlStyle = " SELECT * FROM PrdMstStyle WHERE MasterID =  " + this.MasterID;
                DataTable dtStyle = dbTransactionHelper.GetDataTable(sSqlStyle, CommandType.Text, null);


                string sSqlMaterial = " SELECT * FROM PrdMstMaterial WHERE MasterID =  " + this.MasterID;
                DataTable dtMaterial = dbTransactionHelper.GetDataTable(sSqlMaterial, CommandType.Text, null);

                string sSqlTexture = " SELECT * FROM PrdMstTexture WHERE MasterID =  " + this.MasterID;
                DataTable dtTexture = dbTransactionHelper.GetDataTable(sSqlTexture, CommandType.Text, null);

                string sSqlColor = " SELECT * FROM PrdMstColor WHERE MasterID =  " + this.MasterID;
                DataTable dtColor = dbTransactionHelper.GetDataTable(sSqlColor, CommandType.Text, null);

                // string sSqlSizeCatg = " SELECT pSize.SizeID FROM PrdMstSizeCatg AS MstSize INNER JOIN productsizecatg AS pSize ON pSize.SizeCatgName = MstSize.SizeCatgID WHERE MstSize.MasterID =  " + this.MasterID;
                string sSqlSizeCatg = " SELECT pSize.SizeID, COALESCE(psz.Seq,0) AS Seq  FROM PrdMstSizeCatg AS MstSize ";
                sSqlSizeCatg += " INNER JOIN productsizecatg AS pSize ON pSize.SizeCatgName = MstSize.SizeCatgID  ";
                sSqlSizeCatg += " INNER JOIN ProductSize AS psz ON psz.SizeID = pSize.SizeID ";
                sSqlSizeCatg += " WHERE MstSize.MasterID =  " + this.MasterID;
                sSqlSizeCatg += " ORDER BY Seq ";
                DataTable dtSizeCatg = dbTransactionHelper.GetDataTable(sSqlSizeCatg, CommandType.Text, null);

                int iRowCnt = 0;

                #region Publish Product Color Products if Not Exists
                string sSqlProductColorNotPublushed = "  Select ColorID FROM prdMstColor WHERE ColorID NOT IN ( SELECT ColID FROM Products WHERE MasterID=" + this.MasterID + ") AND MasterID= " + this.MasterID + " ";
                DataTable dtColorNPubsh = dbTransactionHelper.GetDataTable(sSqlProductColorNotPublushed, CommandType.Text, null);
                foreach (DataRow dRowColorNPubsh in dtColorNPubsh.Rows)
                {
                    int iColorIDNPblsh = BusinessUtility.GetInt(dRowColorNPubsh["ColorID"]);
                    string sLength = "";
                    string sChestLength = "";
                    string sSoldLength = "";
                    string sBicepLength = "";
                    string sProdWeight = "";


                    sLength = BusinessUtility.GetString(BusinessUtility.GetDouble(objPMDetail.PrdStartLength));
                    sChestLength = BusinessUtility.GetString(BusinessUtility.GetDouble(objPMDetail.PrdStartChest));
                    sSoldLength = BusinessUtility.GetString(BusinessUtility.GetDouble(objPMDetail.PrdStartShoulder));
                    sBicepLength = BusinessUtility.GetString(BusinessUtility.GetDouble(objPMDetail.PrdStartBicep));
                    sProdWeight = BusinessUtility.GetString(BusinessUtility.GetDouble(objPMDetail.PrdWeight));

                    
                    foreach (DataRow drSize in dtSizeCatg.Rows)
                    {
                        int iSizeID = BusinessUtility.GetInt(drSize["SizeID"]);
                        int iCollectionID = BusinessUtility.GetInt(dtCollection.Rows[0]["CollectionID"]);
                        int iMaterialID = BusinessUtility.GetInt(dtMaterial.Rows[0]["MaterialID"]);
                        string sStyleName = BusinessUtility.GetString(dtStyle.Rows[0]["Style"]);
                        string sBarCode = iCollectionID.ToString("00") + this.MasterID.ToString("0000") + iMaterialID.ToString("00") + iColorIDNPblsh.ToString("0000") + iSizeID.ToString("00");

                        sLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sLength) + (BusinessUtility.GetDouble(objPMDetail.PrdIncrementLength) * iRowCnt));
                        sChestLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sChestLength) + (BusinessUtility.GetDouble(objPMDetail.PrdIncrementChest) * iRowCnt));
                        sSoldLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sSoldLength) + (BusinessUtility.GetDouble(objPMDetail.PrdIncrementShoulder) * iRowCnt));
                        sBicepLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sBicepLength) + (BusinessUtility.GetDouble(objPMDetail.PrdIncrementBicep) * iRowCnt));
                        sProdWeight = BusinessUtility.GetString(BusinessUtility.GetDouble(sProdWeight) + (BusinessUtility.GetDouble(objPMDetail.ProdWeightIncrement) * iRowCnt));
                        iRowCnt = +1;
                        string sqlInsertProduct = " INSERT INTO Products (MasterID, prdName, prdUPCCode, prdSalePricePerMinQty, prdEndUserSalesPrice, prdIntID, prdWebSalesPrice, prdWeight, ";
                        sqlInsertProduct += " prdLength, prdWeightPkg, prdLengthPkg, prdWidthPkg, prdHeightPkg,  ";
                        sqlInsertProduct += "  PrdMinQtyPOTrig, prdPOQty, prdAutoPO, prdIsSpecial, prdIsKit, isPOSMenu, prdIsWeb, prdIsActive,  ";
                        sqlInsertProduct += " prdInoclTerms, prdCreatedUserID, prdCreatedOn, prdType, CollectionID, Style, MatID, ColID, SizeID, prdFOBPrice, prdLandedPrice)   ";
                        sqlInsertProduct += " VALUES ( @MasterID, @prdName, @prdUPCCode, @prdSalePricePerMinQty, @prdEndUserSalesPrice, @prdIntID, @prdWebSalesPrice, @prdWeight, @prdLength,  ";
                        sqlInsertProduct += " @prdWeightPkg, @prdLengthPkg, @prdWidthPkg, @prdHeightPkg,   ";
                        sqlInsertProduct += " @prdMinQtyPO, @prdQtyPO, @prdAutoPO, @prdIsSpecial, @prdIsKit, @isPOSMenu, @prdIsWeb,  ";
                        sqlInsertProduct += " @prdIsActive, @prdNotes, @prdCreatedUserID, @prdCreatedOn, 1, @CollectionID, @Style, @MatID, @ColID, @SizeID,  @prdFOBPrice, @prdLandedPrice )  ";
                        sqlInsertProduct += " ";
                        dbTransactionHelper.ExecuteNonQuery(sqlInsertProduct, CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int ),
                                            DbUtility.GetParameter("PrdName", objPMDetail.PrdName, MyDbType.String ),
                                            DbUtility.GetParameter("PrdUPCCode", sBarCode, MyDbType.String ),
                                            DbUtility.GetParameter("PrdSalePricePerMinQty",  objPMDetail.PrdSalePricePerMinQty, MyDbType.Double ),
                                            DbUtility.GetParameter("PrdEndUserSalesPrice",  objPMDetail.PrdEndUserSalesPrice, MyDbType.Double ),
                                            DbUtility.GetParameter("PrdIntID", sBarCode, MyDbType.String ),
                                            DbUtility.GetParameter("PrdWebSalesPrice", objPMDetail.PrdWebSalesPrice, MyDbType.Double ),
                                            //DbUtility.GetParameter("PrdWeight", objPMDetail.PrdWeight, MyDbType.String),
                                            DbUtility.GetParameter("PrdWeight", sProdWeight, MyDbType.String),
                                            DbUtility.GetParameter("PrdLength", objPMDetail.PrdLength, MyDbType.String ),
                                            DbUtility.GetParameter("PrdWeightPkg", objPMDetail.PrdWeightPkg, MyDbType.String ),
                                            DbUtility.GetParameter("PrdLengthPkg", objPMDetail.PrdLengthPkg, MyDbType.String ),
                                            DbUtility.GetParameter("PrdWidthPkg", objPMDetail.PrdWidthPkg, MyDbType.String ),
                                            DbUtility.GetParameter("PrdHeightPkg", objPMDetail.PrdHeightPkg, MyDbType.String ),
                                            DbUtility.GetParameter("PrdMinQtyPO", objPMDetail.PrdMinQtyPO, MyDbType.Int ),
                                            DbUtility.GetParameter("PrdQtyPO", objPMDetail.PrdQtyPO, MyDbType.Int ),
                                            DbUtility.GetParameter("PrdAutoPO", objPMDetail.PrdAutoPO, MyDbType.Boolean ),
                                            DbUtility.GetParameter("PrdIsSpecial", objPMDetail.PrdIsSpecial, MyDbType.Boolean ),
                                            DbUtility.GetParameter("PrdIsKit", objPMDetail.PrdIsKit, MyDbType.Boolean ),
                                            DbUtility.GetParameter("isPOSMenu", objPMDetail.IsPOSMenu, MyDbType.Boolean ),
                                            DbUtility.GetParameter("PrdIsWeb", objPMDetail.PrdIsWeb, MyDbType.Boolean ),
                                            DbUtility.GetParameter("PrdIsActive", objPMDetail.PrdIsActive, MyDbType.Boolean ),
                                            DbUtility.GetParameter("PrdNotes", objPMDetail.PrdNotes, MyDbType.String ),
                                            DbUtility.GetParameter("PrdCreatedUserID", userID, MyDbType.Int ),
                                            DbUtility.GetParameter("PrdCreatedOn", DateTime.Now, MyDbType.DateTime ),
                                            DbUtility.GetParameter("CollectionID", iCollectionID, MyDbType.Int ),
                                            DbUtility.GetParameter("Style", sStyleName, MyDbType.String ),
                                            DbUtility.GetParameter("MatID", iMaterialID, MyDbType.Int ),
                                            DbUtility.GetParameter("ColID", iColorIDNPblsh, MyDbType.Int ),
                                            DbUtility.GetParameter("SizeID", iSizeID, MyDbType.Int ),
                                            DbUtility.GetParameter("prdFOBPrice",  objPMDetail.PrdFOBPrice, MyDbType.Double ),
                                            DbUtility.GetParameter("prdLandedPrice",  objPMDetail.PrdLandedPrice, MyDbType.Double )
                                        });

                    }
                }
                #endregion

                ProductCategory objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 1;
                var lstProductCatg = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);

                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 2;
                var lstProductNickLine = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);

                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 9;
                var lstProductGauge = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);

                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 3;
                var lstProductExtraCatg = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);

                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 4;
                var lstProductSleeve = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);

                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 5;
                var lstProductSilhouette = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);

                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 6;
                var lstProductTrend = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);

                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 7;
                var lstProductkeyWord = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);


                objProdCatg = new ProductCategory();
                objProdCatg.CatgHrdID = 8;
                var lstProductWebSiteSubCatg = objProdCatg.GetStyleCatg(null, this.MasterID, Globals.CurrentAppLanguageCode);


                int i = 0;
                string sql = "";//" SELECT * FROM Products WHERE MasterID = @MasterID";
                sql += " SELECT p.*, psz.seq, psz.SizeEn FROM Products AS p ";
                sql += " INNER JOIN ProductSize AS psz ON p.SizeID = psz.SizeID ";
                sql += " WHERE p.MasterID = @MasterID ORDER BY p.ColID, psz.seq ASC ";
                DataTable dtProdcuts = dbTransactionHelper.GetDataTable(sql, CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int )});

                int iSelectedColr = 0;
                iRowCnt = 0;
                foreach (DataRow dRowPmaster in dtProdcuts.Rows)
                {
                    #region EditPublishedProduct
                    int iProductID = BusinessUtility.GetInt(dRowPmaster["ProductID"]);
                    int iCollectionID = BusinessUtility.GetInt(dRowPmaster["CollectionID"]);
                    string sStyleName = BusinessUtility.GetString(dRowPmaster["Style"]);
                    int iMaterialID = BusinessUtility.GetInt(dRowPmaster["MatID"]);
                    int iTexture = BusinessUtility.GetInt(dRowPmaster["TextureID"]);
                    int iColorID = BusinessUtility.GetInt(dRowPmaster["ColID"]);
                    int iSizeID = BusinessUtility.GetInt(dRowPmaster["SizeID"]);


                    string sLength = "";
                    string sChestLength = "";
                    string sSoldLength = "";
                    string sBicepLength = "";
                    string sPrdWeight = "";


                    sLength = BusinessUtility.GetString(BusinessUtility.GetDouble(objPMDetail.PrdStartLength));
                    sChestLength = BusinessUtility.GetString(BusinessUtility.GetDouble(objPMDetail.PrdStartChest));
                    sSoldLength = BusinessUtility.GetString(BusinessUtility.GetDouble(objPMDetail.PrdStartShoulder));
                    sBicepLength = BusinessUtility.GetString(BusinessUtility.GetDouble(objPMDetail.PrdStartBicep));
                    sPrdWeight = BusinessUtility.GetString(BusinessUtility.GetDouble(objPMDetail.PrdWeight));


                    if (iProductID > 0)
                    {
                        if (iSelectedColr == 0)
                        {
                            iSelectedColr = iColorID;
                        }

                        if (iSelectedColr == iColorID)
                        {
                            //if (iRowCnt > 0)
                            //{
                            //    iRowCnt = +1;
                            //}
                        }
                        else if ((iSelectedColr != iColorID) && (iSelectedColr > 0))
                        {
                            iSelectedColr = iColorID;
                            iRowCnt = 0;
                        }

                        sLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sLength) + (BusinessUtility.GetDouble(objPMDetail.PrdIncrementLength) * iRowCnt));
                        sChestLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sChestLength) + (BusinessUtility.GetDouble(objPMDetail.PrdIncrementChest) * iRowCnt));
                        sSoldLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sSoldLength) + (BusinessUtility.GetDouble(objPMDetail.PrdIncrementShoulder) * iRowCnt));
                        sBicepLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sBicepLength) + (BusinessUtility.GetDouble(objPMDetail.PrdIncrementBicep) * iRowCnt));
                        sPrdWeight = BusinessUtility.GetString(BusinessUtility.GetDouble(sPrdWeight) + (BusinessUtility.GetDouble(objPMDetail.ProdWeightIncrement) * iRowCnt));
                        iRowCnt += 1;
                        string sqlInsertProduct = " UPDATE Products SET prdSalePricePerMinQty = @prdSalePricePerMinQty, prdEndUserSalesPrice = @prdEndUserSalesPrice, prdWebSalesPrice = @prdWebSalesPrice, prdWeight = @prdWeight, ";
                        sqlInsertProduct += " prdLength = @prdLength, prdWeightPkg = @prdWeightPkg, prdLengthPkg = @prdLengthPkg, prdWidthPkg = @prdWidthPkg, prdHeightPkg = @prdHeightPkg,  ";
                        sqlInsertProduct += "  PrdMinQtyPOTrig = @prdMinQtyPO, prdPOQty = @prdQtyPO, prdAutoPO = @prdAutoPO, prdIsSpecial = @prdIsSpecial, prdIsKit = @prdIsKit, isPOSMenu = @isPOSMenu, prdIsWeb = @prdIsWeb, prdIsActive = @prdIsActive,  ";
                        sqlInsertProduct += " prdInoclTerms = @prdNotes, prdLastUpdatedByUserID = @prdLastUpdatedByUserID, prdLastUpdatedOn = @prdLastUpdatedOn, prdType = 1,  prdFOBPrice = @prdFOBPrice, prdLandedPrice = @prdLandedPrice   ";
                        sqlInsertProduct += " WHERE MasterID = @MasterID AND productID = @ProductID ";
                        sqlInsertProduct += " ";

                        dbTransactionHelper.ExecuteNonQuery(sqlInsertProduct, CommandType.Text, new MySqlParameter[]{
                                            DbUtility.GetParameter("MasterID", this.MasterID, MyDbType.Int ),
                                            DbUtility.GetParameter("ProductID", iProductID, MyDbType.Int ),
                                            DbUtility.GetParameter("PrdSalePricePerMinQty",  objPMDetail.PrdSalePricePerMinQty, MyDbType.Double ),
                                            DbUtility.GetParameter("PrdEndUserSalesPrice",  objPMDetail.PrdEndUserSalesPrice, MyDbType.Double ),
                                            DbUtility.GetParameter("PrdWebSalesPrice", objPMDetail.PrdWebSalesPrice, MyDbType.Double ),
                                            //DbUtility.GetParameter("PrdWeight", objPMDetail.PrdWeight, MyDbType.String),
                                            DbUtility.GetParameter("PrdWeight", sPrdWeight, MyDbType.String),
                                            DbUtility.GetParameter("PrdLength", objPMDetail.PrdLength, MyDbType.String ),
                                            DbUtility.GetParameter("PrdWeightPkg", objPMDetail.PrdWeightPkg, MyDbType.String ),
                                            DbUtility.GetParameter("PrdLengthPkg", objPMDetail.PrdLengthPkg, MyDbType.String ),
                                            DbUtility.GetParameter("PrdWidthPkg", objPMDetail.PrdWidthPkg, MyDbType.String ),
                                            DbUtility.GetParameter("PrdHeightPkg", objPMDetail.PrdHeightPkg, MyDbType.String ),
                                            DbUtility.GetParameter("PrdMinQtyPO", objPMDetail.PrdMinQtyPO, MyDbType.Int ),
                                            DbUtility.GetParameter("PrdQtyPO", objPMDetail.PrdQtyPO, MyDbType.Int ),
                                            DbUtility.GetParameter("PrdAutoPO", objPMDetail.PrdAutoPO, MyDbType.Boolean ),
                                            DbUtility.GetParameter("PrdIsSpecial", objPMDetail.PrdIsSpecial, MyDbType.Boolean ),
                                            DbUtility.GetParameter("PrdIsKit", objPMDetail.PrdIsKit, MyDbType.Boolean ),
                                            DbUtility.GetParameter("isPOSMenu", objPMDetail.IsPOSMenu, MyDbType.Boolean ),
                                            DbUtility.GetParameter("PrdIsWeb", objPMDetail.PrdIsWeb, MyDbType.Boolean ),
                                            DbUtility.GetParameter("PrdIsActive", objPMDetail.PrdIsActive, MyDbType.Boolean ),
                                            DbUtility.GetParameter("PrdNotes", objPMDetail.PrdNotes, MyDbType.String ),
                                            DbUtility.GetParameter("prdLastUpdatedByUserID", userID, MyDbType.Int ),
                                            DbUtility.GetParameter("prdLastUpdatedOn", DateTime.Now, MyDbType.DateTime ),
                                            DbUtility.GetParameter("prdFOBPrice",  objPMDetail.PrdFOBPrice, MyDbType.Double ),
                                            DbUtility.GetParameter("prdLandedPrice",  objPMDetail.PrdLandedPrice, MyDbType.Double ),
                                        });



                        string sqlDelete = " DELETE FROM prodCatgSelVal WHERE ProdID = @ProdID";
                        dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                                                DbUtility.GetParameter("ProdID", iProductID, MyDbType.Int),
                                                });


                        foreach (var item in lstProductCatg)
                        {
                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };

                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                        }

                        foreach (var item in lstProductNickLine)
                        {
                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };

                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                        }


                        foreach (var item in lstProductGauge)
                        {
                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };

                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                        }

                        foreach (var item in lstProductExtraCatg)
                        {
                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };

                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                        }

                        foreach (var item in lstProductSleeve)
                        {
                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };

                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                        }

                        foreach (var item in lstProductSilhouette)
                        {
                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };

                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                        }


                        foreach (var item in lstProductTrend)
                        {
                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };

                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                        }

                        foreach (var item in lstProductkeyWord)
                        {
                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };

                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                        }


                        foreach (var item in lstProductWebSiteSubCatg)
                        {
                            string sqlInsertProdCatg = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) VALUES (@productID, @prodSysCatgDtlID, @ProdSyCatgHdrID) ";
                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("prodSysCatgDtlID", item.CatgID, MyDbType.Int),
                                                DbUtility.GetParameter("ProdSyCatgHdrID", item.CatgHrdID, MyDbType.Int),
                                                };
                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                        }


                        sqlDelete = " DELETE FROM productAssociatedStyle WHERE productID = @productID";
                        dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                });

                        ProductStyle objProductStyle = new ProductStyle();
                        var lstAssociatedMasterStyle = objProductStyle.GetProductMasterAssociatedStyle(null, this.MasterID, iCollectionID);
                        foreach (var item in lstAssociatedMasterStyle)
                        {
                            string sqlInsertProdCatg = "INSERT INTO productAssociatedStyle (ProductID, MasterID, Style, CollectionID ) VALUES (@ProductID, @MasterID, @Style, @CollectionID) ";
                            MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("MasterID", item.MasterID, MyDbType.Int),
                                                DbUtility.GetParameter("Style", item.StyleName, MyDbType.String),
                                                DbUtility.GetParameter("CollectionID", item.CollectionID, MyDbType.Int),
                                                };
                            dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                        }


                        sqlDelete = " DELETE FROM productAssociateTexture WHERE ProductID = @ProductID  ";
                        dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("ProductID", iProductID, MyDbType.Int)
                        });

                        foreach (DataRow dRow in dtTexture.Rows)
                        {
                            int iTextureID = BusinessUtility.GetInt(dRow["TextureID"]);
                            if (iTextureID > 0)
                            {
                                string sqlInsertProdCatg = "INSERT INTO productAssociateTexture (ProductID, TextureID) VALUES (@ProductID, @TextureID) ";
                                MySqlParameter[] pInsertProdCatg = {                                            
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                DbUtility.GetParameter("TextureID", iTextureID, MyDbType.Int)
                                                };
                                dbTransactionHelper.ExecuteNonQuery(sqlInsertProdCatg, System.Data.CommandType.Text, pInsertProdCatg);
                            }
                        }

                        sqlDelete = " DELETE FROM ProductClothDesc WHERE productID = @productID";
                        dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                });

                        string sqlDesc = @"INSERT INTO ProductClothDesc (productID, style,  size, longueurLength, epauleShoulder, busteChest, bicep, material, collection, color) 
                            VALUES (@productID, @style,  @size, @longueurLength, @epauleShoulder, @busteChest, @bicep, @material, @Collection, @color)"; // @neckline, @sleeve, @silhouette, , @ExtraCatg  // neckline, sleeve, silhouette, , ExtraCatg texture, @texture, // , Gauge , @Gauge
                        MySqlParameter[] pInsert = {                                            
                                        DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                        DbUtility.GetParameter("style", sStyleName, MyDbType.String),
                                        DbUtility.GetParameter("size", iSizeID, MyDbType.Int),
                                        //DbUtility.GetParameter("texture", iTexture, MyDbType.Int),
                                        DbUtility.GetParameter("longueurLength", sLength, MyDbType.String),
                                        DbUtility.GetParameter("epauleShoulder", sSoldLength, MyDbType.String),
                                        DbUtility.GetParameter("busteChest", sChestLength, MyDbType.String),
                                        DbUtility.GetParameter("bicep", sBicepLength, MyDbType.String),
                                        DbUtility.GetParameter("material", iMaterialID, MyDbType.Int),
                                        DbUtility.GetParameter("Collection", iCollectionID, MyDbType.Int),
                                        DbUtility.GetParameter("color", iColorID, MyDbType.Int),
                                        //DbUtility.GetParameter("Gauge", objPMDetail.PrdGauge, MyDbType.Int),
                                     };
                        dbTransactionHelper.ExecuteNonQuery(sqlDesc, System.Data.CommandType.Text, pInsert);

                        sqlDelete = " DELETE FROM prddescriptions WHERE id = @productID";
                        dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                                                DbUtility.GetParameter("productID", iProductID, MyDbType.Int),
                                                });
                        string sqlDescEn = "INSERT INTO prddescriptions (id,descLang,prdName,prdSmallDesc,prdLargeDesc) VALUES (@id,@descLang,@prdName,@prdSmallDesc,@prdLargeDesc)";
                        MySqlParameter[] p = { 
                                     DbUtility.GetParameter("id", iProductID, MyDbType.Int),
                                     DbUtility.GetParameter("descLang", AppLanguageCode.EN, MyDbType.String),
                                     DbUtility.GetParameter("prdName", objPMDetail.PrdName, MyDbType.String),
                                     DbUtility.GetParameter("prdSmallDesc", objPMDetail.PrdEnDesc, MyDbType.String),
                                     DbUtility.GetParameter("prdLargeDesc", objPMDetail.PrdEnDesc, MyDbType.String)
                                            };

                        dbTransactionHelper.ExecuteNonQuery(sqlDescEn, System.Data.CommandType.Text, p);


                        string sqlDescFr = "INSERT INTO prddescriptions (id,descLang,prdName,prdSmallDesc,prdLargeDesc) VALUES (@id,@descLang,@prdName,@prdSmallDesc,@prdLargeDesc)";
                        MySqlParameter[] p1 = { 
                                     DbUtility.GetParameter("id", iProductID, MyDbType.Int),
                                     DbUtility.GetParameter("descLang", AppLanguageCode.FR, MyDbType.String),
                                     DbUtility.GetParameter("prdName", objPMDetail.PrdName, MyDbType.String),
                                     DbUtility.GetParameter("prdSmallDesc", objPMDetail.PrdFrDesc, MyDbType.String),
                                     DbUtility.GetParameter("prdLargeDesc", objPMDetail.PrdFrDesc, MyDbType.String)
                                            };
                        dbTransactionHelper.ExecuteNonQuery(sqlDescFr, System.Data.CommandType.Text, p1);
                    }
                    i += 1;

                    #endregion
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductBlackOut
    {
        public int BlackOutID { get; set; }
        public int ProductID { get; set; }
        public int RoomID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Reason { get; set; }

        public void Insert(DbHelper dbHelp)
        {
            string sqlCheck = "SELECT COUNT(*) FROM z_product_blackouts WHERE ProductID=@ProductID AND StartDate=@StartDate AND EndDate=@EndDate";
            string sql = "INSERT INTO z_product_blackouts (ProductID,RoomID,StartDate,EndDate,Reason) VALUES(@ProductID,@RoomID,@StartDate,@EndDate,@Reason)";
            string sqlRoomID = "SELECT prdExtendedCategory FROM products WHERE productID=@ProductID";
            try
            {
                object val = dbHelp.GetValue(sqlCheck, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ProductID", this.ProductID, MyDbType.Int),
                    DbUtility.GetParameter("StartDate", this.StartDate, MyDbType.DateTime),
                    DbUtility.GetParameter("EndDate", this.EndDate, MyDbType.DateTime)
                });
                if (BusinessUtility.GetInt(val) > 0)
                {
                    throw new Exception(CustomExceptionCodes.BLACK_OUT_DATE_REANGE_ALREADY_EXISTS);
                }
                object rid = dbHelp.GetValue(sqlRoomID, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ProductID", this.ProductID, MyDbType.Int)
                });
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ProductID", this.ProductID, MyDbType.Int),
                    DbUtility.GetParameter("RoomID", rid, MyDbType.Int),
                    DbUtility.GetParameter("StartDate", this.StartDate, MyDbType.DateTime),
                    DbUtility.GetParameter("EndDate", this.EndDate, MyDbType.DateTime),
                    DbUtility.GetParameter("Reason", this.Reason, MyDbType.Int)
                });
                this.BlackOutID = dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }            
        }

        public void Insert()
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.Insert(dbHelp);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update(DbHelper dbHelp)
        {
            string sqlCheck = "SELECT COUNT(*) FROM z_product_blackouts WHERE ProductID=@ProductID AND StartDate=@StartDate AND EndDate=@EndDate AND BlackOutID <> @BlackOutID";
            string sqlUpdate = "UPDATE z_product_blackouts SET StartDate=@StartDate, EndDate=@EndDate,Reason=@Reason WHERE BlackOutID=@BlackOutID";
            try
            {
                object val = dbHelp.GetValue(sqlCheck, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("BlackOutID", this.BlackOutID, MyDbType.Int),
                    DbUtility.GetParameter("ProductID", this.ProductID, MyDbType.Int),
                    DbUtility.GetParameter("StartDate", this.StartDate, MyDbType.DateTime),
                    DbUtility.GetParameter("EndDate", this.EndDate, MyDbType.DateTime)
                });
                if (BusinessUtility.GetInt(val) > 0)
                {
                    throw new Exception(CustomExceptionCodes.BLACK_OUT_DATE_REANGE_ALREADY_EXISTS);
                }
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("BlackOutID", this.BlackOutID, MyDbType.Int),
                    DbUtility.GetParameter("StartDate", this.StartDate, MyDbType.DateTime),
                    DbUtility.GetParameter("EndDate", this.EndDate, MyDbType.DateTime),
                    DbUtility.GetParameter("Reason", this.Reason, MyDbType.Int)
                });
            }
            catch 
            {
                
                throw;
            }
        }

        public void Update()
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.Update(dbHelp);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Delete(int id)
        {
            string sql = "DELETE FROM z_product_blackouts WHERE BlackOutID=@BlackOutID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("BlackOutID", id, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }       

        public void PopulateObject(DbHelper dbHelp, int id)
        {
            string sql = "SELECT * FROM z_product_blackouts WHERE BlackOutID=@BlackOutID";
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("BlackOutID", id, MyDbType.Int)
                });
                if (dr.Read())
                {
                    this.BlackOutID = BusinessUtility.GetInt(dr["BlackOutID"]);
                    this.EndDate = BusinessUtility.GetDateTime(dr["EndDate"]);
                    this.ProductID = BusinessUtility.GetInt(dr["ProductID"]);
                    this.Reason = BusinessUtility.GetInt(dr["Reason"]);
                    this.StartDate = BusinessUtility.GetDateTime(dr["StartDate"]);
                    this.RoomID = BusinessUtility.GetInt(dr["RoomID"]);
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (dr!=null && !dr.IsClosed)
                {
                    dr.Close();
                }
            }
        }

        public void PopulateObject(int id)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.PopulateObject(dbHelp, id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }        

        public string GetSql(ParameterCollection pCol, int productID, string lang)
        {
            pCol.Clear();
            string sql = "SELECT pb.*, st." + Globals.GetFieldName("Reason", lang) + " AS ReasonDesc FROM z_product_blackouts pb ";
            sql += " INNER JOIN z_blackout_reasons st ON st.ID = pb.Reason ";
            sql += " WHERE pb.ProductID = @ProductID";
            pCol.Add("@ProductID", productID.ToString());
            return sql;
        }        

        #region RoomLevel Methods
        public List<BlackoutView> GetAllByRoom(int roomID, string lang)
        {
            string sql = "SELECT DISTINCT pb.RoomID, pb.StartDate, pb.EndDate, pb.Reason, st." + Globals.GetFieldName("Reason", lang) + " AS ReasonDesc, ";
            sql += " CONCAT(CAST(pb.RoomID AS CHAR(10)), '_', DATE_FORMAT(pb.StartDate, '%Y%m%d'), '_', DATE_FORMAT(pb.EndDate, '%Y%m%d')) AS RowKey";
            sql += " FROM z_product_blackouts pb ";
            sql += " INNER JOIN z_blackout_reasons st ON st.ID = pb.Reason ";
            sql += " WHERE pb.RoomID = @RoomID ORDER BY pb.RoomID";
            DbHelper dbHelp = new DbHelper(true);
            MySqlDataReader dr = null;
            List<BlackoutView> lResult = new List<BlackoutView>();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("RoomID", roomID, MyDbType.Int)
                });
                while (dr.Read())
                {
                    BlackoutView bv = new BlackoutView();
                    bv.EndDate = BusinessUtility.GetDateTime(dr["EndDate"]);
                    bv.Reason = BusinessUtility.GetInt(dr["Reason"]);
                    bv.ReasonDesc = BusinessUtility.GetString(dr["ReasonDesc"]);
                    bv.RoomID = BusinessUtility.GetInt(dr["RoomID"]);
                    bv.StartDate = BusinessUtility.GetDateTime(dr["StartDate"]);
                    bv.GridPrimaryKey = BusinessUtility.GetString(dr["RowKey"]);
                    lResult.Add(bv);
                }

                return lResult;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public void DeleteByRoomID(string rowKey, params int[] roomID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.DeleteByRoomID(dbHelp, rowKey, roomID);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void DeleteByRoomID(DbHelper dbHelp, string rowKey, params int[] roomID)
        {
            string sql = "DELETE FROM z_product_blackouts WHERE RoomID IN ({0})";            
            sql = string.Format(sql, StringUtil.GetJoinedString(",", roomID));
            sql += " AND DATE_FORMAT(StartDate, '%Y%m%d')=@StartDate AND DATE_FORMAT(EndDate, '%Y%m%d')=@EndDate";
            string[] rowKeyParams = rowKey.Split('_');
            try
            {
                if (roomID.Length > 0)
                {
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("StartDate", rowKeyParams[1], MyDbType.String),
                        DbUtility.GetParameter("EndDate", rowKeyParams[2], MyDbType.String)
                    });
                }
            }
            catch
            {
                throw;
            }
        }               

        public void ApplyBlackoutToRooms(List<BlackoutView> lstExistData, params int[] roomID)
        {
            string sqlDel = string.Format("DELETE FROM z_product_blackouts WHERE RoomID IN ({0})", StringUtil.GetJoinedString(",", roomID));     
            string sql = "INSERT INTO z_product_blackouts(ProductID, RoomID, StartDate,EndDate,Reason)";
            sql += " SELECT productID AS ProductID, prdExtendedCategory AS RoomID, @StartDate AS StartDate, @EndDate AS EndDate, @Reason AS Reason";
            sql += string.Format(" FROM products WHERE prdExtendedCategory IN({0})", StringUtil.GetJoinedString(",", roomID));
            DbHelper dbHelp = new DbHelper(true);
            try
            {                
                if (roomID.Length > 0)
                {
                    dbHelp.ExecuteNonQuery(sqlDel, CommandType.Text, null);

                    foreach (var item in lstExistData)
                    {
                        dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                            DbUtility.GetParameter("StartDate", item.StartDate, MyDbType.DateTime),
                            DbUtility.GetParameter("EndDate", item.EndDate, MyDbType.DateTime),
                            DbUtility.GetParameter("Reason", item.Reason, MyDbType.Int)
                        });
                    }

                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("StartDate", this.StartDate, MyDbType.DateTime),
                        DbUtility.GetParameter("EndDate", this.EndDate, MyDbType.DateTime),
                        DbUtility.GetParameter("Reason", this.Reason, MyDbType.Int)
                    });
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateBlackoutToRoom(string rowKey, params int[] roomID)
        {
            string sql = "UPDATE z_product_blackouts SET StartDate=@StartDate, EndDate=@EndDate, Reason=@Reason";
            sql += string.Format(" WHERE RoomID IN({0})", StringUtil.GetJoinedString(",", roomID));
            sql += " AND DATE_FORMAT(StartDate, '%Y%m%d')=@StartDateOld AND DATE_FORMAT(EndDate, '%Y%m%d')=@EndDateOld";            
            string[] rowKeyParams = rowKey.Split('_');
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                //this.DeleteByRoomID(roomID);//First need to delete all in room
                if (roomID.Length > 0)
                {
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("StartDate", this.StartDate, MyDbType.DateTime),
                        DbUtility.GetParameter("EndDate", this.EndDate, MyDbType.DateTime),
                        DbUtility.GetParameter("Reason", this.Reason, MyDbType.Int),
                        DbUtility.GetParameter("StartDateOld", rowKeyParams[1], MyDbType.String),
                        DbUtility.GetParameter("EndDateOld", rowKeyParams[2], MyDbType.String)
                    });
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public BlackoutView GetBlackout(string rowKey)
        {
            string sql = "SELECT *, CONCAT(CAST(RoomID AS CHAR(10)), '_', DATE_FORMAT(StartDate, '%Y%m%d'), '_', DATE_FORMAT(EndDate, '%Y%m%d')) AS RowKey";
            sql += "  FROM z_product_blackouts WHERE ";
            sql += " CONCAT(CAST(RoomID AS CHAR(10)), '_', DATE_FORMAT(StartDate, '%Y%m%d'), '_', DATE_FORMAT(EndDate, '%Y%m%d')) = @RowKey LIMIT 1";
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            BlackoutView bv = new BlackoutView();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("RowKey", rowKey, MyDbType.String)
                });
                if (dr.Read())
                {                    
                    bv.EndDate = BusinessUtility.GetDateTime(dr["EndDate"]);                    
                    bv.Reason = BusinessUtility.GetInt(dr["Reason"]);
                    bv.StartDate = BusinessUtility.GetDateTime(dr["StartDate"]);
                    bv.RoomID = BusinessUtility.GetInt(dr["RoomID"]);
                    bv.GridPrimaryKey = BusinessUtility.GetString(dr["RowKey"]);
                }
                return bv;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }
        #endregion
    }

    public class BlackoutView
    {        
        public int RoomID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Reason { get; set; }
        public string ReasonDesc { get; set; }
        /// <summary>
        /// RoomID_StartDate[yyyyMMdd]_EndDate[yyyyMMdd]
        /// </summary>
        public string GridPrimaryKey { get; set; }
    }
}

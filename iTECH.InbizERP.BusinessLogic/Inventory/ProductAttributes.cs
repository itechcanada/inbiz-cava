﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductAttributes
    {
        public void Save(int[] attrIds, int productID) {
            string sqlDelete = "DELETE FROM z_product_attributes WHERE ProductID=@ProductID";
            string sqlInsert = "INSERT INTO z_product_attributes(AttributeID,ProductID) VALUES(@AttributeID,@ProductID)";

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sqlDelete, System.Data.CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ProductID", productID, MyDbType.Int)
                });
                foreach (int item in attrIds)
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, System.Data.CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("AttributeID", item, MyDbType.Int),
                        DbUtility.GetParameter("ProductID", productID, MyDbType.Int)
                    });
                }
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql(ParameterCollection pCol, int productID, string lang) {
            pCol.Clear();
            pCol.Add("@ProductID", productID.ToString());
            return "SELECT z.AttributeID, " + Globals.GetFieldName("z.AttributeDesc", lang) + " AS AttributeDesc, z.AttributeImage, z.IsActive,zp.ProductID FROM z_attributes z LEFT OUTER JOIN z_product_attributes zp ON zp.AttributeID=z.AttributeID AND zp.ProductID=@ProductID";
        }
    }
}

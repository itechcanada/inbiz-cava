﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductMasterHeader
    {
        public int MasterID { get; set; }
        public int MasterActive { get; set; }
        public int MasterPublished { get; set; }


        public List<ProductMasterHeader> GetMasterHeader(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductMasterHeader> lResult = new List<ProductMasterHeader>();
                string sql = "SELECT MasterID, MasterActive, MasterPublished FROM productMasterHdr WHERE 1 = 1 ";
                if (this.MasterID > 0)
                {
                    sql += " AND MasterID =" + this.MasterID;
                }

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductMasterHeader { MasterID = BusinessUtility.GetInt(dr["MasterID"]), MasterActive = BusinessUtility.GetInt(dr["MasterActive"]), MasterPublished = BusinessUtility.GetInt(dr["MasterPublished"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}

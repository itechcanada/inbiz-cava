﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductTexture
    {
        public string TextureEn { get; set; }
        public string TextureFr { get; set; }
        public string TextureName { get; set; }
        public int TextureID { get; set; }
        public string SearchKey { get; set; }
        public int IsActive { get; set; }
        public string IsActiveUrl { get; set; }
        public string ShortName { get; set; }


        public Boolean Save(DbHelper dbHelp, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {

                if (this.TextureID > 0)
                {
                    string sql = " UPDATE ProductTexture SET TextureFr = @TextureFr, TextureEn = @TextureEn, TextureActive = @TextureActive, TextureLastUpdBy = @TextureLastUpdBy, TexturetLastUpdOn = @TexturetLastUpdOn, ShortName = @ShortName WHERE TextureID = @TextureID ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("TextureFr", this.TextureFr, MyDbType.String),
                    DbUtility.GetParameter("TextureEn", this.TextureEn, MyDbType.String),
                    DbUtility.GetParameter("TextureActive", this.IsActive, MyDbType.Int),
                    DbUtility.GetParameter("TextureLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("TexturetLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("TextureID", this.TextureID, MyDbType.Int),
                    DbUtility.GetParameter("ShortName", this.ShortName, MyDbType.String),

                });
                    return true;
                }
                else
                {
                    string sql = " INSERT INTO ProductTexture (TextureFr, TextureEn, TextureActive, TextureLastUpdBy, TexturetLastUpdOn, ShortName) VALUES (@TextureFr, @TextureEn, @TextureActive, @TextureLastUpdBy, @TexturetLastUpdOn, @ShortName) ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("TextureFr", this.TextureFr, MyDbType.String),
                    DbUtility.GetParameter("TextureEn", this.TextureEn, MyDbType.String),
                    DbUtility.GetParameter("TextureActive", this.IsActive, MyDbType.Int),
                    DbUtility.GetParameter("TextureLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("TexturetLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ShortName", this.ShortName, MyDbType.String),
                });
                    this.TextureID = dbHelp.GetLastInsertID();
                    return true;
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public Boolean Delete(DbHelper dbHelp, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (this.TextureID > 0)
                {
                    string sql = " UPDATE ProductTexture SET TextureActive = @TextureActive, TextureLastUpdBy = @TextureLastUpdBy, TexturetLastUpdOn = @TexturetLastUpdOn WHERE TextureID =@TextureID ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("TextureActive", this.IsActive, MyDbType.Int),
                    DbUtility.GetParameter("TextureLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("TexturetLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("TextureID", this.TextureID, MyDbType.Int)

                });
                }
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductTexture> GetTextureList(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductTexture> lResult = new List<ProductTexture>();
                string sql = string.Format("SELECT TextureID AS TextureID, Texture{0} as TextureName, ShortName FROM ProductTexture WHERE TextureActive = 1 ", lang);

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductTexture { TextureName = BusinessUtility.GetString(dr["TextureName"]), TextureID = BusinessUtility.GetInt(dr["TextureID"]), ShortName = BusinessUtility.GetString(dr["ShortName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductTexture> GetAllTextureList(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductTexture> lResult = new List<ProductTexture>();
                string sql = string.Format("SELECT TextureID AS TextureID, TextureFr, TextureEn, Texture{0} as TextureName, TextureActive AS IsActive, CASE TextureActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as IsActiveUrl, ShortName  FROM ProductTexture WHERE 1=1  ", lang);
                if (BusinessUtility.GetString(this.SearchKey) != "")
                {
                    sql += string.Format(" AND Texture{0} like '%" + SearchKey + "%' ", lang);
                }
                if (this.TextureID > 0)
                {
                    sql += "AND TextureID = " + this.TextureID;
                }

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductTexture
                        {
                            TextureName = BusinessUtility.GetString(dr["TextureName"]),
                            TextureID = BusinessUtility.GetInt(dr["TextureID"]),
                            TextureEn = BusinessUtility.GetString(dr["TextureEn"]),
                            TextureFr = BusinessUtility.GetString(dr["TextureFr"]),
                            IsActiveUrl = BusinessUtility.GetString(dr["IsActiveUrl"]),
                            IsActive = BusinessUtility.GetInt(dr["IsActive"]),
                            ShortName = BusinessUtility.GetString(dr["ShortName"])
                        });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public Boolean AddTextureInStyle(int iMasterID, int[] iTextureID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {
                string sqlDelete = "DELETE FROM PrdMstTexture WHERE MasterID = @MasterID "; 
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                });

                foreach (var item in iTextureID)
                {
                    string sql = " INSERT INTO PrdMstTexture (MasterID, TextureID) VALUES (@MasterID, @TextureID) ";
                    dbTransactionHelper.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    DbUtility.GetParameter("TextureID",BusinessUtility.GetInt( item), MyDbType.Int)
                });
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }


        public Boolean DeleteTextureInStyle(int iMasterID, int iTextureID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                string sqlDelete = "DELETE FROM PrdMstTexture WHERE MasterID = @MasterID AND TextureID = @TextureID  ";
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    DbUtility.GetParameter("TextureID", iTextureID, MyDbType.Int),
                });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        public List<ProductTexture> GetStyleTexture(DbHelper dbHelp, int iStyleID, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductTexture> lResult = new List<ProductTexture>();
                string sql = string.Format("SELECT PM.TextureID AS TextureID, PM.Texture{0} as TextureName, PM.ShortName FROM ProductTexture PM INNER JOIN PrdMstTexture PMsT ON PMsT.TextureID = PM.TextureID WHERE PMsT.MasterID = @StyleID AND PM.TextureActive = 1 ", lang);
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("StyleID", iStyleID, MyDbType.Int)
                }))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductTexture { TextureName = BusinessUtility.GetString(dr["TextureName"]), TextureID = BusinessUtility.GetInt(dr["TextureID"]), ShortName = BusinessUtility.GetString(dr["ShortName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }



    }
}

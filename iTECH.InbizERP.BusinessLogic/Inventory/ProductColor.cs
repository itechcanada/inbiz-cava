﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using System.Data.Odbc;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductColor
    {
        private string _prdColiCode;

        public string PrdColiCode
        {
            get { return _prdColiCode; }
            set { _prdColiCode = value; }
        }
        private int _prdID;

        public int PrdID
        {
            get { return _prdID; }
            set { _prdID = value; }
        }

        public void Save(string[] colorCods, int productID)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                string sqlDelete = "DELETE FROM prdcolor WHERE prdID=@prdID";
                dbHelp.ExecuteNonQuery(sqlDelete, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("prdID", productID, MyDbType.Int) });
                string sqlInsert = "INSERT INTO prdcolor (prdColiCode,prdID) VALUES (@prdColiCode,@prdID)";
                foreach (string item in colorCods)
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("prdColiCode", item, MyDbType.String), DbUtility.GetParameter("prdID", productID, MyDbType.Int) });
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql(ParameterCollection pCol, int productID)
        {
            pCol.Clear();
            pCol.Add("@prdID", productID.ToString());
            return "SELECT s.colCode, s.colDesc, p.prdColiCode   FROM sysproductcolor s LEFT OUTER JOIN prdcolor p ON p.prdColiCode = s.colCode AND p.prdID = @prdID";
        }

        /* Used For Cava PRoduct Master */

        public string ColorEn { get; set; }
        public string ColorFr { get; set; }
        public string ColorName { get; set; }
        public int ColorID { get; set; }
        public string SearchKey { get; set; }
        public int IsActive { get; set; }
        public string IsActiveUrl { get; set; }
        public string ShortName { get; set; }
        public bool IsStylePublished { get; set; }


        public Boolean SaveColor(DbHelper dbHelp, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (this.ColorID > 0)
                {
                    string sql = " UPDATE ProductColor SET ColorFr = @ColorFr, ColorEn = @ColorEn, ColorActive = @ColorActive, ColorLastUpdBy = @ColorLastUpdBy, ColorLastUpdOn = @ColorLastUpdOn, ShortName = @ShortName WHERE ColorID = @ColorID ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ColorFr", this.ColorFr, MyDbType.String),
                    DbUtility.GetParameter("ColorEn", this.ColorEn, MyDbType.String),
                    DbUtility.GetParameter("ColorActive", this.IsActive, MyDbType.Int),
                    DbUtility.GetParameter("ColorLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("ColorLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ColorID", this.ColorID, MyDbType.Int),
                    DbUtility.GetParameter("ShortName", this.ShortName, MyDbType.String),
                });
                    return true;
                }
                else
                {
                    string sql = " INSERT INTO ProductColor (ColorFr, ColorEn, ColorActive, ColorLastUpdBy, ColorLastUpdOn, ShortName) VALUES (@ColorFr, @ColorEn, @ColorActive, @ColorLastUpdBy, @ColorLastUpdOn, @ShortName) ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ColorFr", this.ColorFr, MyDbType.String),
                    DbUtility.GetParameter("ColorEn", this.ColorEn, MyDbType.String),
                    DbUtility.GetParameter("ColorActive", this.IsActive, MyDbType.Int),
                    DbUtility.GetParameter("ColorLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("ColorLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ShortName", this.ShortName, MyDbType.String),
                });

                    this.ColorID = dbHelp.GetLastInsertID();
                    return true;
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean Delete(DbHelper dbHelp, int userID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                if (this.ColorID > 0)
                {
                    string sql = " UPDATE ProductColor SET ColorActive = @ColorActive, ColorLastUpdBy = @ColorLastUpdBy, ColorLastUpdOn = @ColorLastUpdOn WHERE ColorID = @ColorID ";
                    dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("ColorActive", 0, MyDbType.Int),
                    DbUtility.GetParameter("ColorLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("ColorLastUpdOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ColorID", this.ColorID, MyDbType.Int)
                });
                }
                return true;
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductColor> GetColorList(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductColor> lResult = new List<ProductColor>();
                string sql = string.Format("SELECT ColorID AS ColorID, Color{0} as ColorName, ShortName FROM ProductColor WHERE ColorActive = 1 ORDER BY Color{0} ", lang);

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductColor { ColorName = BusinessUtility.GetString(dr["ColorName"]), ColorID = BusinessUtility.GetInt(dr["ColorID"]), ShortName = BusinessUtility.GetString(dr["ShortName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductColor> GetAllColorList(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductColor> lResult = new List<ProductColor>();
                string sql = string.Format("SELECT colorID AS ColorID, ColorFr, ColorEn, Color{0} as ColorName, ColorActive AS IsActive, CASE ColorActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as IsActiveUrl, ShortName  FROM ProductColor WHERE 1=1  ", lang);
                if (BusinessUtility.GetString(this.SearchKey) != "")
                {
                    sql += string.Format(" AND Color{0} like '%" + SearchKey + "%' ", lang);
                }
                if (this.ColorID > 0)
                {
                    sql += "AND ColorID = " + this.ColorID;
                }

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductColor
                        {
                            ColorName = BusinessUtility.GetString(dr["ColorName"]),
                            ColorID = BusinessUtility.GetInt(dr["ColorID"]),
                            ColorEn = BusinessUtility.GetString(dr["ColorEn"]),
                            ColorFr = BusinessUtility.GetString(dr["ColorFr"]),
                            IsActiveUrl = BusinessUtility.GetString(dr["IsActiveUrl"]),
                            IsActive = BusinessUtility.GetInt(dr["IsActive"]),
                            ShortName = BusinessUtility.GetString(dr["ShortName"])
                        });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public Boolean AddColorInStyle(int iMasterID, int[] iColorID)
        {
            //bool mustClose = false;
            //if (dbHelp == null)
            //{
            //    mustClose = true;
            //    dbHelp = new DbHelper(true);
            //}

            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                //foreach (var item in iColorID)
                //{

                if (IsStylePublished == false)
                {
                    string sqlDelete = "DELETE FROM PrdMstColor WHERE MasterID = @MasterID ";//AND ColorID = @ColorID 
                    dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    //DbUtility.GetParameter("ColorID",BusinessUtility.GetInt( item), MyDbType.Int)
                });
                }
                //}


                foreach (var item in iColorID)
                {
                   string sql = " Select * FROM PrdMstColor WHERE MasterID = @MasterID AND ColorID = @ColorID ";
                   DataTable dtHasColored =  dbTransactionHelper.GetDataTable(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    DbUtility.GetParameter("ColorID",BusinessUtility.GetInt( item), MyDbType.Int)
                    });


                   if (dtHasColored.Rows.Count == 0)
                   {
                       sql = " INSERT INTO PrdMstColor (MasterID, ColorID) VALUES (@MasterID, @ColorID) ";
                       dbTransactionHelper.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    DbUtility.GetParameter("ColorID",BusinessUtility.GetInt( item), MyDbType.Int)
                    });
                   }
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                //if (mustClose) dbHelp.CloseDatabaseConnection();
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }


        public Boolean DeleteColorInStyle(int iMasterID, int iColorID)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                string sqlDelete = "DELETE FROM PrdMstColor WHERE MasterID = @MasterID AND ColorID = @ColorID  ";
                dbTransactionHelper.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.Int),
                    DbUtility.GetParameter("ColorID", iColorID, MyDbType.Int),
                });

                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                //if (mustClose) dbHelp.CloseDatabaseConnection();
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        public List<ProductColor> GetStyleColor(DbHelper dbHelp, int iStyleID, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductColor> lResult = new List<ProductColor>();
                string sql = string.Format("SELECT PM.ColorID AS ColorID, PM.Color{0} as ColorName, PM.ShortName FROM ProductColor PM INNER JOIN PrdMstColor PMsT ON PMsT.ColorID = PM.ColorID WHERE PMsT.MasterID = @StyleID AND PM.ColorActive = 1 ", lang);
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("StyleID", iStyleID, MyDbType.Int)
                }))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductColor { ColorName = BusinessUtility.GetString(dr["ColorName"]), ColorID = BusinessUtility.GetInt(dr["ColorID"]), ShortName = BusinessUtility.GetString(dr["ShortName"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool ChkColorExists(DbHelper dbHelp, string style, int CollectionID, int colorID)
        {   bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "select count(*) from ProductClothDesc where style = @style and Collection = @Collection and Color = @colorID ";
                object o = dbHelp.GetValue(sql, CommandType.Text,new MySqlParameter[]{
                    DbUtility.GetParameter("style", style, MyDbType.String),
                    DbUtility.GetParameter("Collection", CollectionID, MyDbType.Int),
                    DbUtility.GetParameter("colorID", colorID, MyDbType.Int)
                });

                if (BusinessUtility.GetInt(o) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductDescription
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _descLang;

        public string DescLang
        {
            get { return _descLang; }
            set { _descLang = value; }
        }
        private string _prdName;

        public string PrdName
        {
            get { return _prdName; }
            set { _prdName = value; }
        }
        private string _prdSmallDesc;

        public string PrdSmallDesc
        {
            get { return _prdSmallDesc; }
            set { _prdSmallDesc = value; }
        }
        private string _prdLargeDesc;

        public string PrdLargeDesc
        {
            get { return _prdLargeDesc; }
            set { _prdLargeDesc = value; }
        }

        public void PopulateObject(int id, string lang) {
            string sql = "SELECT * FROM prddescriptions WHERE id=@id AND descLang=@descLang";
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, System.Data.CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("id", id, MyDbType.Int), DbUtility.GetParameter("descLang", lang, MyDbType.String) });
                if (dr.Read()) {
                    this.Id = BusinessUtility.GetInt(dr["id"]);
                    this.DescLang = BusinessUtility.GetString(dr["descLang"]);
                    this.PrdLargeDesc = BusinessUtility.GetString(dr["prdLargeDesc"]);
                    this.PrdName = BusinessUtility.GetString(dr["prdName"]);
                    this.PrdSmallDesc = BusinessUtility.GetString(dr["prdSmallDesc"]);
                }
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public void Insert(DbHelper dbHelp) {
            string sql = "INSERT INTO prddescriptions (id,descLang,prdName,prdSmallDesc,prdLargeDesc) VALUES (@id,@descLang,@prdName,@prdSmallDesc,@prdLargeDesc)";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("id", this.Id, MyDbType.Int),
                                     DbUtility.GetParameter("descLang", this.DescLang, MyDbType.String),
                                     DbUtility.GetParameter("prdName", this.PrdName, MyDbType.String),
                                     DbUtility.GetParameter("prdSmallDesc", this.PrdSmallDesc, MyDbType.String),
                                     DbUtility.GetParameter("prdLargeDesc", this.PrdLargeDesc, MyDbType.String)
                                 };
            try
            {
                dbHelp.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);                
            }
            catch 
            {
                
                throw;
            }
        }

        public void Update() {
            DbHelper dbHelp = new DbHelper();
            string sql = "UPDATE prddescriptions SET prdName=@prdName,prdSmallDesc=@prdSmallDesc,prdLargeDesc=@prdLargeDesc WHERE id=@id AND descLang=@descLang";
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("id", this.Id, MyDbType.Int),
                                     DbUtility.GetParameter("descLang", this.DescLang, MyDbType.String),
                                     DbUtility.GetParameter("prdName", this.PrdName, MyDbType.String),
                                     DbUtility.GetParameter("prdSmallDesc", this.PrdSmallDesc, MyDbType.String),
                                     DbUtility.GetParameter("prdLargeDesc", this.PrdLargeDesc, MyDbType.String)
                                 };
            try
            {
                dbHelp.ExecuteNonQuery(sql, System.Data.CommandType.Text, p);
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}

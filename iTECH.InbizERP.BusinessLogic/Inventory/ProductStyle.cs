﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductStyle
    {
        public string StyleName { get; set; }
        public int StyleID { get; set; }
        public int CollectionID { get; set; }
        public int MasterID { get; set; }



        public Boolean Save(DbHelper dbHelp, int userID)
        {
            //bool mustClose = false;
            //if (dbHelp == null)
            //{
            //    mustClose = true;
            //    dbHelp = new DbHelper(true);
            //}

            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {

                string sqlInsert_pMasterHdr = " INSERT INTO productMasterHdr (MasterActive,MasterPublished,MasterCreatedBy,MasterCreatedOn) VALUES(@MasterActive, @MasterPublished, @MasterCreatedBy, @MasterCreatedOn)";
                dbTransactionHelper.ExecuteNonQuery(sqlInsert_pMasterHdr, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterActive", 0, MyDbType.Int),
                    DbUtility.GetParameter("MasterPublished", 0, MyDbType.Int),
                    DbUtility.GetParameter("MasterCreatedBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("MasterCreatedOn", DateTime.Now, MyDbType.DateTime),
                    
                });
                int iMasterID = dbTransactionHelper.GetLastInsertID();
                this.MasterID = iMasterID;

                string sqlInsert_ProdMstCol = " INSERT INTO ProdMstCol (MasterID, CollectionID ) VALUES(@MasterID, @CollectionID)";
                dbTransactionHelper.ExecuteNonQuery(sqlInsert_ProdMstCol, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("MasterID", iMasterID, MyDbType.String),
                    DbUtility.GetParameter("CollectionID", this.CollectionID, MyDbType.Int)
                });
                //int iProdMstColID = dbTransactionHelper.GetLastInsertID();


                string sqlInsertProductStyle = " INSERT INTO productstyle (style, styleActive, styleLastUpdBy, styleLastUpdOn) VALUES (@style, @styleActive, @styleLastUpdBy, @styleLastUpdOn) ";
                dbTransactionHelper.ExecuteNonQuery(sqlInsertProductStyle, CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("style", this.StyleName, MyDbType.String),
                    DbUtility.GetParameter("styleActive", 1, MyDbType.Int),
                    DbUtility.GetParameter("styleLastUpdBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("styleLastUpdOn", DateTime.Now, MyDbType.DateTime),
                });
                this.StyleID = dbTransactionHelper.GetLastInsertID();

                string sqlInsert_PrdMstStyle = " INSERT INTO PrdMstStyle (MasterID, Style ) VALUES(@MasterID, @Style)";
                dbTransactionHelper.ExecuteNonQuery(sqlInsert_PrdMstStyle, CommandType.Text, new MySqlParameter[]{
                   // DbUtility.GetParameter("MasterID", iMasterID, MyDbType.String),
                   DbUtility.GetParameter("MasterID", iMasterID, MyDbType.String),
                    DbUtility.GetParameter("Style", this.StyleName, MyDbType.String)
                });
                //int iProdMstColID = dbTransactionHelper.GetLastInsertID();


                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                //if (mustClose) dbHelp.CloseDatabaseConnection();
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        public List<ProductStyle> GetStyleList(DbHelper dbHelp, string lang)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductStyle> lResult = new List<ProductStyle>();
                //string sql = string.Format("SELECT styleID, Style FROM productstyle WHERE styleActive = 1 ", lang);
                //string sql = string.Format("SELECT MasterID AS styleID, Style FROM PrdMstStyle WHERE styleActive = 1 ", lang);
                string sql = string.Format("SELECT MasterID AS styleID, Style FROM PrdMstStyle WHERE 1 = 1 ", lang);
                //lResult.Add(new Collection { CollectionName = "", CollectionID = BusinessUtility.GetInt(0) });

                if (MasterID > 0)
                {
                    sql += " AND MasterID =" + this.MasterID;
                }

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductStyle { StyleName = BusinessUtility.GetString(dr["Style"]), StyleID = BusinessUtility.GetInt(dr["styleID"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public string GetStyleSql(ParameterCollection pCol, string sCollectionID, string sSearchStyle, string sSearchCollection, string sSearchMaterial, string sSearchColor, string sSearchSizeGroup, string sSearchHistory, string sSearchProdCatg, string sSearchProdKeyWord)
        {
            pCol.Clear();
            //string sql = "SELECT styleID, Style FROM productstyle WHERE styleActive = 1 ";

            string sql = " SELECT  Styl.MasterID AS MasterID, Styl.Style, Coll.CollectionID, Mat.MaterialID, Color.ColorID, SzCatg.SizeCatgID, pmHdr.MasterActive AS IsActive, pmHdr.MasterPublished AS IsPublished, PmCat.prodSysCatgDtlID  ";
            sql += " FROM ProdMstCol AS Coll  ";
            sql += " LEFT OUTER  JOIN PrdMstStyle AS Styl ON Styl.MasterID = Coll.MasterID  ";
            sql += " LEFT OUTER  JOIN PrdMstMaterial AS Mat ON Mat.MasterID = Coll.MasterID  ";
            sql += " LEFT OUTER  JOIN PrdMstColor AS Color ON Color.MasterID = Coll.MasterID  ";
            sql += " LEFT OUTER  JOIN PrdMstSizeCatg AS SzCatg ON SzCatg.MasterID = Coll.MasterID ";
            sql += " LEFT OUTER  JOIN ProductMasterHdr AS pmHdr ON  pmHdr.MasterID = Styl.MasterID   ";
            sql += " LEFT OUTER  JOIN prodMasterCatgSelVal AS PmCat ON  PmCat.MasterID = Styl.MasterID   "; // AND PmCat.ProdSyCatgHdrID IN(1,7,8)
            //sql += " LEFT OUTER  JOIN prodMasterCatgSelVal AS PmKeyWord ON  PmKeyWord.MasterID = Styl.MasterID  AND PmKeyWord.ProdSyCatgHdrID = 7 ";
            sql += " WHERE 1=1 ";

            if (!string.IsNullOrEmpty(sCollectionID) && sSearchHistory == "")
            {
                sql += " AND Coll.CollectionID = @CollectionID ";
                pCol.Add("@CollectionID", sCollectionID);
            }

            if (sSearchHistory != "")
            {
                if (sSearchStyle != "")
                {
                    sql += " AND Styl.Style like '%" + sSearchStyle + "%' ";
                }

                if (sSearchCollection != "")
                {
                    sql += " AND Coll.CollectionID = @CollectionID ";
                    pCol.Add("@CollectionID", sSearchCollection);
                }

                if (sSearchMaterial != "")
                {
                    sql += " AND Mat.MaterialID = @MaterialID ";
                    pCol.Add("@MaterialID", sSearchMaterial);
                }

                //if (sSearchColor != "")
                //{
                //    sql += " AND Color.ColorID = @ColorID ";
                //    pCol.Add("@ColorID", sSearchColor);
                //}

                string sSearch = "," + sSearchColor + ",";
                if (!sSearch.Contains(",0,"))
                {
                    if (sSearchColor != "")
                    {
                        sql += " AND Color.ColorID IN( @ColorID )";
                        pCol.Add("@ColorID", sSearchColor);
                    }
                }

                //if (sSearchSizeGroup != "")
                //{
                //    sql += " AND SzCatg.SizeCatgID = @SizeCatgID ";
                //    pCol.Add("@SizeCatgID", sSearchSizeGroup);
                //}

                if (sSearchSizeGroup != "")
                {
                    string[] sGroup = sSearchSizeGroup.Split(',');
                    string sSizeGroup = "";
                    foreach (string sizegroup in sGroup)
                    {
                        if (sSizeGroup == "")
                            sSizeGroup = "'" + sizegroup + "'";
                        else
                            sSizeGroup += "," + "'" + sizegroup + "'";
                    }
                    if (sSizeGroup != "")
                    {
                        sql += " AND SzCatg.SizeCatgID IN( " + sSizeGroup + " )";
                        //pCol.Add("@SizeCatgID", sSizeGroup);
                    }
                }


                //if (sSearchProdCatg != "")
                //{
                //    sql += " AND PmCat.prodSysCatgDtlID = @CatgID ";
                //    pCol.Add("@CatgID", sSearchProdCatg);
                //}

                if (sSearchProdCatg != "")
                {
                    //strSql += " AND pWebCatg.prodSysCatgDtlID = @CatgID AND pWebCatg.ProdSyCatgHdrID = 1 ";
                    sql += " AND PmCat.prodSysCatgDtlID IN (@CatgID) ";
                    pCol.Add("@CatgID", sSearchProdCatg);
                }

                //if (sSearchProdKeyWord != "")
                //{
                //    sql += " AND PmKeyWord.prodSysCatgDtlID = @KeyWordID ";
                //    pCol.Add("@KeyWordID", sSearchProdKeyWord);
                //}

            }
            sql += "  GROUP BY Styl.MasterID ";



            return sql;
        }


        public List<ProductStyle> GetCollectionStyleList(DbHelper dbHelp, int styleID, int collectionID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductStyle> lResult = new List<ProductStyle>();
                string sql = "";
                sql += " SELECT pms.Style AS Style FROM PrdMstStyle AS  pms ";
                sql += " INNER JOIN ProdMstCol AS pmc ON pmc.MasterID = pms.MasterID ";
                sql += " INNER JOIN productMasterHdr AS pmh ON pmh.MasterID = pmc.MasterID ";
                sql += " WHERE 1=1 AND pmh.MasterPublished =1 ";

                if (collectionID > 0)
                {
                    sql += " AND pmc.CollectionID =  " + collectionID;
                }

                if (styleID > 0)
                {
                    sql += " AND pms.idprdMasterStyle !=  " + styleID;
                }

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductStyle { StyleName = BusinessUtility.GetString(dr["Style"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public List<ProductStyle> GetProductMasterAssociatedStyle(DbHelper dbHelp, int iMasterID, int collectionID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<ProductStyle> lResult = new List<ProductStyle>();
                string sql = "";
                sql += " SELECT Style, CollectionID, MasterID FROM productMstAssociateStyle WHERE 1=1 ";
                if (collectionID > 0)
                {
                    sql += " AND CollectionID =  " + collectionID;
                }

                if (iMasterID > 0)
                {
                    sql += " AND MasterID =  " + iMasterID;
                }

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new ProductStyle { StyleName = BusinessUtility.GetString(dr["Style"]), CollectionID = BusinessUtility.GetInt(dr["CollectionID"]), MasterID = BusinessUtility.GetInt(dr["MasterID"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        public int GetStyleID(int prdID)
        {
            DbHelper dbHelp = new DbHelper();
            try
            {
                string sql = " select idprdMasterStyle from productClothdesc pcd join PrdMstStyle s on s.Style = pcd.style Where productID = @prdID  ";
                
                object styleID = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] {
                    DbUtility.GetParameter("prdID",prdID,MyDbType.Int)});
                dbHelp.CloseDatabaseConnection();
                return BusinessUtility.GetInt(styleID);
            }
            catch (Exception ex)
            {
                dbHelp.CloseDatabaseConnection();
                throw;
            }
        }
    }
}

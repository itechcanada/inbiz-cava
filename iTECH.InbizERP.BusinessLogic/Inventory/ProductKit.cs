﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web.UI.WebControls;
using System.Web;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductKit
    {
        private int _prdKitID;

        public int PrdKitID
        {
            get { return _prdKitID; }
            set { _prdKitID = value; }
        }
        private int _prdID;

        public int PrdID
        {
            get { return _prdID; }
            set { _prdID = value; }
        }
        private int _prdIncludeID;

        public int PrdIncludeID
        {
            get { return _prdIncludeID; }
            set { _prdIncludeID = value; }
        }
        private double _prdIncludeQty;

        public double PrdIncludeQty
        {
            get { return _prdIncludeQty; }
            set { _prdIncludeQty = value; }
        }

        public OperationResult Insert() {
            string exists = "SELECT COUNT(*) FROM productkits WHERE prdID=@prdID AND prdIncludeID=@prdIncludeID";
            string sqlInsert = "INSERT INTO productkits (prdID,prdIncludeID,prdIncludeQty) VALUES (@prdID,@prdIncludeID,@prdIncludeQty)";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object count = dbHelp.GetValue(exists, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("prdID", this.PrdID, MyDbType.Int), DbUtility.GetParameter("prdIncludeID", this.PrdIncludeID, MyDbType.Int) });
                if (BusinessUtility.GetInt(count) > 0) {
                    return OperationResult.AlreadyExists;
                }
                dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("prdID", this.PrdID, MyDbType.Int), DbUtility.GetParameter("prdIncludeID", this.PrdIncludeID, MyDbType.Int), DbUtility.GetParameter("prdIncludeQty", this.PrdIncludeQty, MyDbType.Double) });
                return OperationResult.Success;
            }
            catch
            {
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public bool Delete(int prdKitID) {
            string sql = "DELETE FROM productkits WHERE prdKitID=@prdKitID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("prdKitID", prdKitID, MyDbType.Int) });
                return true;
            }
            catch
            {
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql(ParameterCollection pCol, int productID, string lang) {
            pCol.Clear();
            string strSql = "select prdKitID,prdID,prdIncludeID,prdIncludeQty,pd.prdName,prdIntID from productkits ";
            strSql += " Inner Join prddescriptions as pd on pd.id=productkits.prdIncludeID Inner Join products as p on p.productID=productkits.prdIncludeID where prdID=@prdID AND descLang=@descLang";
            pCol.Add("@prdID", productID.ToString());
            pCol.Add("@descLang", lang);

            return strSql;
        }
    }
}

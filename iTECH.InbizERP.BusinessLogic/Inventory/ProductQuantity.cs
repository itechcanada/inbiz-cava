﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ProductQuantity
    {
        public int Id { get; set; }
        public int PrdID { get; set; }
        public string PrdWhsCode { get; set; }
        public double PrdOhdQty { get; set; }
        public double PrdQuoteRsv { get; set; }
        public double PrdSORsv { get; set; }
        public double PrdDefectiveQty { get; set; }
        public int PrdTaxCode { get; set; }
        public string PrdBlock { get; set; }
        public string PrdFloor { get; set; }
        public string PrdAisle { get; set; }
        public string PrdRack { get; set; }
        public string PrdBin { get; set; }
        public string Tagid { get; set; }

        //public OperationResult Insert() {
        //    DbHelper dbHelp = new DbHelper(true);
        //    try
        //    {
        //        return this.Insert(dbHelp);
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }            
        //}

        public void Insert(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string whsExists = "SELECT COUNT(*) FROM prdquantity WHERE prdID=@prdID AND prdWhsCode=@prdWhsCode";
            string sqlInsert = "INSERT INTO prdquantity(prdID, prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty, prdTaxCode, prdBlock, prdFloor, prdAisle, prdRack, prdBin ,tagid)";
            sqlInsert += "VALUES(@prdID, @prdWhsCode, @prdOhdQty, @prdQuoteRsv, @prdSORsv, @prdDefectiveQty, @prdTaxCode, @prdBlock, @prdFloor, @prdAisle, @prdRack, @prdBin ,@tagid)";
            int? taxCode = this.PrdTaxCode > 0 ? (int?)this.PrdTaxCode : null;
            MySqlParameter[] p = { 
                                     DbUtility.GetParameter("prdID", this.PrdID, MyDbType.Int),
                                     DbUtility.GetParameter("prdWhsCode", this.PrdWhsCode, MyDbType.String),
                                     DbUtility.GetParameter("prdOhdQty", this.PrdOhdQty, MyDbType.Double),
                                     DbUtility.GetParameter("prdQuoteRsv", this.PrdQuoteRsv, MyDbType.Double),
                                     DbUtility.GetParameter("prdSORsv", this.PrdSORsv, MyDbType.Double),
                                     DbUtility.GetParameter("prdDefectiveQty", this.PrdDefectiveQty, MyDbType.Double),
                                     DbUtility.GetIntParameter("prdTaxCode", taxCode),
                                     DbUtility.GetParameter("prdBlock", this.PrdBlock, MyDbType.String),
                                     DbUtility.GetParameter("prdFloor", this.PrdFloor, MyDbType.String),
                                     DbUtility.GetParameter("prdAisle", this.PrdAisle, MyDbType.String),
                                     DbUtility.GetParameter("prdRack", this.PrdRack, MyDbType.String),
                                     DbUtility.GetParameter("prdBin", this.PrdBin, MyDbType.String),
                                     DbUtility.GetParameter("tagid", this.Tagid, MyDbType.String)
                                 };
            try
            {
                object count = dbHelp.GetValue(whsExists, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("prdID", this.PrdID, MyDbType.Int), DbUtility.GetParameter("prdWhsCode", this.PrdWhsCode, MyDbType.String) });
                if (BusinessUtility.GetInt(count) > 0)
                {
                    throw new Exception(CustomExceptionCodes.ALREADY_EXISTS);
                }
                dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, p);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //public bool Update() {            
        //    DbHelper dbHelp = new DbHelper(true);            
        //    try
        //    {
        //        return this.Update(dbHelp);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public bool Update(DbHelper dbHelp)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sqlUpdate = "UPDATE prdquantity SET prdOhdQty=@prdOhdQty, prdQuoteRsv=@prdQuoteRsv, prdSORsv=@prdSORsv, prdDefectiveQty=@prdDefectiveQty, prdTaxCode=@prdTaxCode,";
            sqlUpdate += "prdBlock=@prdBlock, prdFloor=@prdFloor, prdAisle=@prdAisle,prdRack=@prdRack, prdBin=@prdBin WHERE id=@id";
            int? taxCode = this.PrdTaxCode > 0 ? (int?)this.PrdTaxCode : null;
            try
            {
                MySqlParameter[] p = { 
                                         DbUtility.GetParameter("prdOhdQty",  this.PrdOhdQty, MyDbType.Double),
                                         DbUtility.GetParameter("prdQuoteRsv",  this.PrdQuoteRsv, MyDbType.Double),
                                         DbUtility.GetParameter("prdSORsv",  this.PrdSORsv, MyDbType.Double),
                                         DbUtility.GetParameter("prdDefectiveQty",  this.PrdDefectiveQty, MyDbType.Double),
                                         DbUtility.GetIntParameter("prdTaxCode", taxCode),
                                         DbUtility.GetParameter("prdBlock",  this.PrdBlock, MyDbType.String),
                                         DbUtility.GetParameter("prdFloor",  this.PrdFloor, MyDbType.String),
                                         DbUtility.GetParameter("prdAisle",  this.PrdAisle, MyDbType.String),
                                         DbUtility.GetParameter("prdRack",  this.PrdRack, MyDbType.String),
                                         DbUtility.GetParameter("prdBin",  this.PrdBin, MyDbType.String),
                                         DbUtility.GetParameter("id",  this.Id, MyDbType.Int)
                                     };
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, p);
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }        

        //public void PopulateObject(int productID, string whsCode) {
        //    string sql = "SELECT * FROM prdquantity WHERE prdID=@prdID AND prdWhsCode=@prdWhsCode";
        //    DbHelper dbHelp = new DbHelper();
        //    MySqlDataReader dr = null;
        //    try
        //    {
        //        dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("prdID", productID, MyDbType.Int), DbUtility.GetParameter("prdWhsCode", whsCode, MyDbType.String) });
        //        if (dr.Read()) {
        //            this.Id = BusinessUtility.GetInt(dr["id"]);
        //            this.PrdID = BusinessUtility.GetInt(dr["prdID"]);
        //            this.PrdWhsCode = BusinessUtility.GetString(dr["prdWhsCode"]);
        //            this.PrdOhdQty = BusinessUtility.GetDouble(dr["prdOhdQty"]);
        //            this.PrdQuoteRsv = BusinessUtility.GetDouble(dr["prdQuoteRsv"]);
        //            this.PrdSORsv = BusinessUtility.GetDouble(dr["prdSORsv"]);
        //            this.PrdDefectiveQty = BusinessUtility.GetDouble(dr["prdDefectiveQty"]);
        //            this.PrdTaxCode = BusinessUtility.GetInt(dr["prdTaxCode"]);
        //            this.PrdBlock = BusinessUtility.GetString(dr["prdBlock"]);
        //            this.PrdBlock = BusinessUtility.GetString(dr["prdFloor"]);
        //            this.PrdAisle = BusinessUtility.GetString(dr["prdAisle"]);
        //            this.PrdRack = BusinessUtility.GetString(dr["prdRack"]);
        //            this.PrdBin = BusinessUtility.GetString(dr["prdBin"]);
        //            this.Tagid = BusinessUtility.GetString(dr["tagid"]);
        //        }
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public void PopulateObject(DbHelper dbHelp, int productID, string whsCode)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT * FROM prdquantity WHERE prdID=@prdID AND prdWhsCode=@prdWhsCode";                       
            try
            {
                using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("prdID", productID, MyDbType.Int), DbUtility.GetParameter("prdWhsCode", whsCode, MyDbType.String) }))
                {
                    if (dr.Read())
                    {
                        this.Id = BusinessUtility.GetInt(dr["id"]);
                        this.PrdID = BusinessUtility.GetInt(dr["prdID"]);
                        this.PrdWhsCode = BusinessUtility.GetString(dr["prdWhsCode"]);
                        this.PrdOhdQty = BusinessUtility.GetDouble(dr["prdOhdQty"]);
                        this.PrdQuoteRsv = BusinessUtility.GetDouble(dr["prdQuoteRsv"]);
                        this.PrdSORsv = BusinessUtility.GetDouble(dr["prdSORsv"]);
                        this.PrdDefectiveQty = BusinessUtility.GetDouble(dr["prdDefectiveQty"]);
                        this.PrdTaxCode = BusinessUtility.GetInt(dr["prdTaxCode"]);
                        this.PrdBlock = BusinessUtility.GetString(dr["prdBlock"]);
                        this.PrdBlock = BusinessUtility.GetString(dr["prdFloor"]);
                        this.PrdAisle = BusinessUtility.GetString(dr["prdAisle"]);
                        this.PrdRack = BusinessUtility.GetString(dr["prdRack"]);
                        this.PrdBin = BusinessUtility.GetString(dr["prdBin"]);
                        this.Tagid = BusinessUtility.GetString(dr["tagid"]);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public void PopulateObject(int productID, string whsCode)
        {
            DbHelper dbHelp = new DbHelper();
            dbHelp = null;
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT * FROM prdquantity WHERE prdID=@prdID AND prdWhsCode=@prdWhsCode";
            try
            {
                using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("prdID", productID, MyDbType.Int), DbUtility.GetParameter("prdWhsCode", whsCode, MyDbType.String) }))
                {
                    if (dr.Read())
                    {
                        this.Id = BusinessUtility.GetInt(dr["id"]);
                        this.PrdID = BusinessUtility.GetInt(dr["prdID"]);
                        this.PrdWhsCode = BusinessUtility.GetString(dr["prdWhsCode"]);
                        this.PrdOhdQty = BusinessUtility.GetDouble(dr["prdOhdQty"]);
                        this.PrdQuoteRsv = BusinessUtility.GetDouble(dr["prdQuoteRsv"]);
                        this.PrdSORsv = BusinessUtility.GetDouble(dr["prdSORsv"]);
                        this.PrdDefectiveQty = BusinessUtility.GetDouble(dr["prdDefectiveQty"]);
                        this.PrdTaxCode = BusinessUtility.GetInt(dr["prdTaxCode"]);
                        this.PrdBlock = BusinessUtility.GetString(dr["prdBlock"]);
                        this.PrdBlock = BusinessUtility.GetString(dr["prdFloor"]);
                        this.PrdAisle = BusinessUtility.GetString(dr["prdAisle"]);
                        this.PrdRack = BusinessUtility.GetString(dr["prdRack"]);
                        this.PrdBin = BusinessUtility.GetString(dr["prdBin"]);
                        this.Tagid = BusinessUtility.GetString(dr["tagid"]);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(DbHelper dbHelp, int id)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT * FROM prdquantity WHERE id=@id";                        
            try
            {
                using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("id", id, MyDbType.Int) }))
                {
                    if (dr.Read())
                    {
                        this.Id = BusinessUtility.GetInt(dr["id"]);
                        this.PrdID = BusinessUtility.GetInt(dr["prdID"]);
                        this.PrdWhsCode = BusinessUtility.GetString(dr["prdWhsCode"]);
                        this.PrdOhdQty = BusinessUtility.GetDouble(dr["prdOhdQty"]);
                        this.PrdQuoteRsv = BusinessUtility.GetDouble(dr["prdQuoteRsv"]);
                        this.PrdSORsv = BusinessUtility.GetDouble(dr["prdSORsv"]);
                        this.PrdDefectiveQty = BusinessUtility.GetDouble(dr["prdDefectiveQty"]);
                        this.PrdTaxCode = BusinessUtility.GetInt(dr["prdTaxCode"]);
                        this.PrdBlock = BusinessUtility.GetString(dr["prdBlock"]);
                        this.PrdBlock = BusinessUtility.GetString(dr["prdFloor"]);
                        this.PrdAisle = BusinessUtility.GetString(dr["prdAisle"]);
                        this.PrdRack = BusinessUtility.GetString(dr["prdRack"]);
                        this.PrdBin = BusinessUtility.GetString(dr["prdBin"]);
                        this.Tagid = BusinessUtility.GetString(dr["tagid"]);
                    }
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public bool Delete(int id)
        {
            string sql = "DELETE FROM prdquantity WHERE id=@id";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("id", id, MyDbType.Int) });
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public int GetProductQuantityCount(DbHelper dbHelp, int productID, out int id)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            id = -1;
            string sqlCheckCount = "SELECT COUNT(id) FROM prdquantity WHERE prdID=@prdID;";
            string sqlGetID = "SELECT id FROM prdquantity WHERE prdID=@prdID;";            
            try
            {
                object val = dbHelp.GetValue(sqlCheckCount, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("prdID", productID, MyDbType.Int)
                });
                if (BusinessUtility.GetInt(val) == 1)
                {
                    object oID = dbHelp.GetValue(sqlGetID, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("prdID", productID, MyDbType.Int)
                    });
                    id = BusinessUtility.GetInt(oID);
                }

                return BusinessUtility.GetInt(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql(ParameterCollection pCol, int productID)
        {
            pCol.Clear();
            pCol.Add("@prdID", productID.ToString());
            string strSql = "SELECT id,prdID, prdWhsCode, prdOhdQty, funGetProductQtyRserveInQo(prdID, prdWhsCode) AS  prdQuoteRsv, ";
            strSql += " funGetProductQtyRserveInSo(prdID, prdWhsCode) AS  prdSORsv, prdDefectiveQty,  (funGetProductQtyAvailable(prdID, prdWhsCode) - IFNULL(prdDefectiveQty,0)) ";
            strSql += " AS AvailableQty,concat(prdWhsCode,':',WarehouseDescription) as WarehouseDescription,sysTaxCodeDescText,prdDefectiveQty,";
            strSql += " funGetProductQtyInTransit(prdID, prdWhsCode) AS InTransit FROM prdquantity";
            strSql += " Left join systaxcodedesc as T on T.sysTaxCodeDescID=prdquantity.prdTaxCode";
            strSql += " inner join syswarehouses as w on w.WarehouseCode=prdquantity.prdWhsCode where prdID=@prdID and WarehouseActive=1";
            return strSql;
        }

        public bool IsProductQuantityExists(DbHelper dbHelp, int productid, string whsCode)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "SELECT COUNT(*) FROM prdquantity where prdID=@prdID AND prdWhsCode=@prdWhsCode";
            try
            {
                object scalar = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("prdID", productid, MyDbType.Int), DbUtility.GetParameter("prdWhsCode", whsCode, MyDbType.String) });
                return BusinessUtility.GetInt(scalar) > 0;
            }
            catch
            {
                throw;
            }
            finally {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        #region During Purchase Order Process
        internal bool UpdateProductQuantity(DbHelper dbHelp, int id, double qtyToBeAdjust)
        {
            string sql = "UPDATE prdquantity set prdOhdQty=@prdOhdQty WHERE id=@id";            
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("prdOhdQty", qtyToBeAdjust, MyDbType.Double), DbUtility.GetParameter("id", id, MyDbType.Int) });
                return true;
            }
            catch
            {
                throw;
            }            
        }

        public int GetTagID(DbHelper dbHelp)
        {
            string sql = "SELECT SELECT Max(tagid) FROM prdquantity {0}";
            try
            {
                object scalar = dbHelp.GetValue(string.Format(sql, "where prdID=@prdID"), CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("prdID", this.PrdID, MyDbType.Int) });
                int tagid = BusinessUtility.GetInt(scalar);
                if (tagid == 0)
                {
                    scalar = dbHelp.GetValue(string.Format(sql, ""), CommandType.Text, null);
                    tagid = BusinessUtility.GetInt(scalar);
                }

                return tagid + 1;
            }
            catch
            {
                return 1;
            }
        }

        //public double GetInTransitQty(int productID, string whsCode)
        //{
        //    DbHelper dbHelp = new DbHelper(true);
        //    try
        //    {
        //        List<MySqlParameter> p1 = new List<MySqlParameter>();
        //        List<MySqlParameter> p2 = new List<MySqlParameter>();
        //        string strSql = "select sum(poQty) as poQty from purchaseorderitems as pi ";
        //        strSql += " Inner join purchaseorders on purchaseorders.poID=pi.poID ";
        //        strSql += " where poItemPrdId=@poItemPrdId and poStatus='R'";
        //        p1.Add(DbUtility.GetParameter("poItemPrdId", productID, MyDbType.Int));
        //        if (!string.IsNullOrEmpty(whsCode))
        //        {
        //            strSql += " and poItemShpToWhsCode=@poItemShpToWhsCode";
        //            p1.Add(DbUtility.GetParameter("poItemShpToWhsCode", whsCode, MyDbType.String));
        //        }
        //        strSql += " Group by pi.poItemPrdId";

        //        string strSqlRcvdQty = "select sum(poRcvdQty) as poRcvdQty from purchaseorderitems as pi ";
        //        strSqlRcvdQty += " Inner join purchaseorders on purchaseorders.poID=pi.poID ";
        //        strSqlRcvdQty += " where poItemPrdId=@poItemPrdId and poStatus='R'";
        //        p2.Add(DbUtility.GetParameter("poItemPrdId", productID, MyDbType.Int));
        //        if (!string.IsNullOrEmpty(whsCode))
        //        {
        //            strSqlRcvdQty += " and poItemShpToWhsCode=@poItemShpToWhsCode";
        //            p2.Add(DbUtility.GetParameter("poItemShpToWhsCode", whsCode, MyDbType.String));
        //        }
        //        strSqlRcvdQty += " Group by pi.poItemPrdId";

        //        object total = dbHelp.GetValue(strSql, CommandType.Text, p1.ToArray());
        //        object rcvd = dbHelp.GetValue(strSqlRcvdQty, CommandType.Text, p2.ToArray());

        //        return BusinessUtility.GetDouble(total) - BusinessUtility.GetDouble(rcvd);
        //    }
        //    catch
        //    {

        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //} 
        #endregion

        #region DuringSalesProcess

        public void UpdateQuantityOnShipping(DbHelper dbHelp, int productID, double qtyToShip, string shpWhs)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sqlUpdate = "UPDATE prdquantity SET prdOhdQty=prdOhdQty - @ShippedQty WHERE prdID=@ID AND prdWhsCode=@WhsCode";
            try
            {
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ID", productID, MyDbType.Int),
                    DbUtility.GetParameter("ShippedQty", qtyToShip, MyDbType.Double),
                    DbUtility.GetParameter("WhsCode", shpWhs, MyDbType.String)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateQuantityOnReturn(DbHelper dbHelp, int productID, double qtyToReturn, string shpWhs)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
           
            string sqlUpdate = "UPDATE prdquantity SET prdOhdQty=IFNULL(prdOhdQty, 0) + @ReturnQty WHERE prdID=@ID AND prdWhsCode=@WhsCode";
            string sqlInsert = "insert into prdquantity (prdID,prdOhdQty,prdWhsCode) values(@ID,@ReturnQty,@WhsCode)";
            try
            {
                if (this.IsProductQuantityExists(null, productID, shpWhs))
                {
                    dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ID", productID, MyDbType.Int),
                    DbUtility.GetParameter("ReturnQty", qtyToReturn, MyDbType.Double),
                    DbUtility.GetParameter("WhsCode", shpWhs, MyDbType.String)
                });
                }
                else
                {
                    dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ID", productID, MyDbType.Int),
                    DbUtility.GetParameter("ReturnQty", qtyToReturn, MyDbType.Double),
                    DbUtility.GetParameter("WhsCode", shpWhs, MyDbType.String)
                });
                }

                
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion
    }

    //public enum QuantityType
    //{
    //    OnHead,
    //    QuoteReserved,
    //    SalesReserved,
    //    Defective
    //}
}

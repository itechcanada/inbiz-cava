﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;


namespace iTECH.InbizERP.ECommerce
{    
    public class SingleSignOn
    {
        // this creates a new token for the currently authenticated user
        // this would be called from an authenticated page that want to        
        public static string CreateToken(string userName)
        {
            AuthToken t = new AuthToken(userName);
            Token tk = t.ToToken();
            //MemoryStream ms = new MemoryStream();
            //BinaryFormatter bf = new BinaryFormatter();
            //bf.Serialize(ms, t);
            //return Convert.ToBase64String(EncryptAndSign(ms.ToArray()));
            SingleSignOn obj = new SingleSignOn();
            string serializeData = Newtonsoft.Json.JsonConvert.SerializeObject(tk, Newtonsoft.Json.Formatting.None);            
            return Convert.ToBase64String(UTF8Encoding.ASCII.GetBytes(serializeData));
        }

        // this helper function will take a serialized token and decode it back to a AuthToken
        // object, verifying the signature & encryption along the way		
        internal static AuthToken VerifyAndDecryptToken(string serializedToken)
        {
            // unpack the string into the data & sig
            byte[] data, sig;
            if (!UnpackSerializedString(serializedToken, out data, out sig))
                return null;
            // verify the signature
            if (!GetSigningRsa().VerifyData(data, new SHA1CryptoServiceProvider(), sig))
                return null;
            // decrypt the data
            byte[] token = Decrypt(GetEncRsa(), data);
            AuthToken t = DeserializeToken(token);
            if (!t.Expired)
                return t;
            return null;
        }

        static AuthToken DeserializeToken(byte[] data)
        {
            MemoryStream ms = new MemoryStream(data);
            BinaryFormatter bf = new BinaryFormatter();
            return (AuthToken)bf.Deserialize(ms);
        }

        // this takes a base64 serialized token, and splits it out into the data and signature		
        static bool UnpackSerializedString(string serializedToken, out byte[] data, out byte[] sig)
        {
            data = null;
            sig = null;
            byte[] serializedData = Convert.FromBase64String(serializedToken);
            // first 4 bytes say how much data there is
            int dataLen = BitConverter.ToInt32(serializedData, 0);
            // sanity check, dataLen should never be more than 4k
            if (dataLen > 4096)
                return false;
            data = new byte[dataLen];
            Buffer.BlockCopy(serializedData, 4, data, 0, dataLen);
            sig = new byte[serializedData.Length - 4 - dataLen];
            Buffer.BlockCopy(serializedData, 4 + dataLen, sig, 0, sig.Length);
            return true;
        }

        // takes the source array of bytes, encrypts it and signs it.
        static byte[] EncryptAndSign(byte[] src)
        {
            // encrypt it
            byte[] data = Encrypt(GetEncRsa(), src);
            // sign it
            byte[] sig = GetSigningRsa().SignData(data, new SHA1CryptoServiceProvider());
            byte[] res = new byte[data.Length + sig.Length + 4];
            Buffer.BlockCopy(BitConverter.GetBytes(data.Length), 0, res, 0, 4);
            Buffer.BlockCopy(data, 0, res, 4, data.Length);
            Buffer.BlockCopy(sig, 0, res, 4 + data.Length, sig.Length);
            return res;
        }

        // create a symetrical key, encrypt that with RSA, and encrypt the data with the symertrical key
        // see http://pages.infinit.net/ctech/20031101-0151.html
        static byte[] Encrypt(RSA rsa, byte[] input)
        {
            // by default this will create a 128 bits AES (Rijndael) object
            SymmetricAlgorithm sa = SymmetricAlgorithm.Create();
            ICryptoTransform ct = sa.CreateEncryptor();
            byte[] encrypt = ct.TransformFinalBlock(input, 0, input.Length);

            RSAPKCS1KeyExchangeFormatter fmt = new RSAPKCS1KeyExchangeFormatter(rsa);
            byte[] keyex = fmt.CreateKeyExchange(sa.Key);

            // return the key exchange, the IV (public) and encrypted data
            byte[] result = new byte[keyex.Length + sa.IV.Length + encrypt.Length];
            Buffer.BlockCopy(keyex, 0, result, 0, keyex.Length);
            Buffer.BlockCopy(sa.IV, 0, result, keyex.Length, sa.IV.Length);
            Buffer.BlockCopy(encrypt, 0, result, keyex.Length + sa.IV.Length, encrypt.Length);
            return result;
        }

        // does the reverse of Encrypt		
        static byte[] Decrypt(RSA rsa, byte[] input)
        {
            // by default this will create a 128 bits AES (Rijndael) object
            SymmetricAlgorithm sa = SymmetricAlgorithm.Create();

            byte[] keyex = new byte[rsa.KeySize >> 3];
            Buffer.BlockCopy(input, 0, keyex, 0, keyex.Length);

            RSAPKCS1KeyExchangeDeformatter def = new RSAPKCS1KeyExchangeDeformatter(rsa);
            byte[] key = def.DecryptKeyExchange(keyex);

            byte[] iv = new byte[sa.IV.Length];
            Buffer.BlockCopy(input, keyex.Length, iv, 0, iv.Length);

            ICryptoTransform ct = sa.CreateDecryptor(key, iv);
            byte[] decrypt = ct.TransformFinalBlock(input, keyex.Length + iv.Length, input.Length - (keyex.Length + iv.Length));
            return decrypt;
        }

        // gets an RSA key for doing encyption from the keystore			
        private static RSACryptoServiceProvider GetEncRsa()
        {
            CspParameters cp = new CspParameters();
            cp.KeyContainerName = "firstaidcentral.com:encrypt";
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(cp);
            return rsa;
        }

        // gets an RSA key for signing from the keystore
        private static RSACryptoServiceProvider GetSigningRsa()
        {
            CspParameters cp = new CspParameters();
            cp.KeyContainerName = "firstaidcentral.com:signing";
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(cp);
            return rsa;
        }

        // this is the web service entry point, the password we get back from server
        // will be what we generated in the CreateToken call above
        public bool Authenticate(string username,
                                   string password,
                                   string sourceIp)
        {
            if (username.Length <= 0)
                return false;

            //AuthToken t = VerifyAndDecryptToken(password);
            Token tk = (Token)Newtonsoft.Json.JsonConvert.DeserializeObject<Token>(UTF8Encoding.ASCII.GetString(Convert.FromBase64String(password)));
            AuthToken t = tk.ToAuthToken();
            if ((t == null) || (t.Username != username))
                return false;

            return usedids.IsNewId(t.TokenNumber, t.Expires);
        }

        // Convert an object to a byte array
        public byte[] ObjectToByteArray(Object obj)
        {            


            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }

        // Convert a byte array to an Object
        public Object ByteArrayToObject(byte[] arrBytes)
        {             
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            Object obj = (Object)binForm.Deserialize(memStream);
            return obj;
        }

        private static UsedIds usedids = new UsedIds();
    }

    // a helper class that keeps track of the set of token number's we've seen
    // and removes them from the list once they expire	
    public class UsedIds
    {
        private Hashtable ids, sync_ids;
        private ReaderWriterLock rwl;
        public UsedIds()
        {
            ids = new Hashtable();
            sync_ids = Hashtable.Synchronized(ids);
            rwl = new ReaderWriterLock();
            Thread cleanupThread = new Thread(new ThreadStart(this.Cleanup));
            cleanupThread.IsBackground = true;
            cleanupThread.Start();
        }

        // returns true if this id hasn't been used before, false if it has
        public bool IsNewId(long id, DateTime expires)
        {
            try
            {
                rwl.AcquireReaderLock(Timeout.Infinite);
                if (sync_ids.ContainsKey(id))
                    return false;
                sync_ids[id] = expires;
                return true;
            }
            finally
            {
                rwl.ReleaseReaderLock();
            }
        }

        public void Cleanup()
        {
            ArrayList expired = new ArrayList();
            while (true)
            {
                Thread.Sleep(60000);
                DateTime now = DateTime.Now;
                try
                {
                    rwl.AcquireWriterLock(Timeout.Infinite);
                    foreach (DictionaryEntry e in ids)
                    {
                        if (now > ((DateTime)e.Value))
                            expired.Add(e.Key);
                    }
                }
                finally
                {
                    rwl.ReleaseWriterLock();
                }
                foreach (long id in expired)
                    sync_ids.Remove(id);
                expired.Clear();
            }
        }
    }

    [Serializable]
    public class AuthToken
    {
        public AuthToken(string username)
        {
            this.token_number = Interlocked.Increment(ref m_next_token_number);
            this.expires = DateTime.Now.AddMinutes(5);
            this.username = username;
        }

        public string Username
        {
            get { return username; }
        }

        public DateTime Expires
        {
            get { return expires; }
        }
        public bool Expired
        {
            get { return DateTime.Now > expires; }
        }
        public long TokenNumber
        {
            get { return token_number; }
        }

        private long token_number;
        private DateTime expires;
        private string username;

        private static long m_next_token_number = 42;

        public Token ToToken() {
            Token t = new Token();
            t.Expired = this.Expired;
            t.Expires = this.Expires;
            t.TokenNumber = this.TokenNumber;
            t.UserName = this.Username;

            return t;
        }
    }

    public class Token {
        public string UserName
        {
            get;
            set;
        }
        public DateTime Expires
        {
            get;
            set;
        }
        public long TokenNumber
        {
            get;
            set;
        }
        public bool Expired
        {
            get;
            set;
        }

        public AuthToken ToAuthToken() {
            return new AuthToken(this.UserName);
        }
    }

    public class Crypto
    {
        private static byte[] _salt = Encoding.ASCII.GetBytes("o6806642kbM7c5");

        /// <summary>
        /// Encrypt the given string using AES.  The string can be decrypted using 
        /// DecryptStringAES().  The sharedSecret parameters must match.
        /// </summary>
        /// <param name="plainText">The text to encrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for encryption.</param>
        public static string EncryptStringAES(string plainText, string sharedSecret)
        {
            if (string.IsNullOrEmpty(plainText))
                throw new ArgumentNullException("plainText");
            if (string.IsNullOrEmpty(sharedSecret))
                throw new ArgumentNullException("sharedSecret");

            string outStr = null;                       // Encrypted string to return
            RijndaelManaged aesAlg = null;              // RijndaelManaged object used to encrypt the data.

            try
            {
                // generate the key from the shared secret and the salt
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, _salt);

                // Create a RijndaelManaged object
                // with the specified key and IV.
                aesAlg = new RijndaelManaged();
                aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                aesAlg.IV = key.GetBytes(aesAlg.BlockSize / 8);

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                    }
                    outStr = Convert.ToBase64String(msEncrypt.ToArray());
                }
            }
            finally
            {
                // Clear the RijndaelManaged object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            // Return the encrypted bytes from the memory stream.
            return outStr;
        }

        /// <summary>
        /// Decrypt the given string.  Assumes the string was encrypted using 
        /// EncryptStringAES(), using an identical sharedSecret.
        /// </summary>
        /// <param name="cipherText">The text to decrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for decryption.</param>
        public static string DecryptStringAES(string cipherText, string sharedSecret)
        {
            if (string.IsNullOrEmpty(cipherText))
                throw new ArgumentNullException("cipherText");
            if (string.IsNullOrEmpty(sharedSecret))
                throw new ArgumentNullException("sharedSecret");

            // Declare the RijndaelManaged object
            // used to decrypt the data.
            RijndaelManaged aesAlg = null;

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            try
            {
                // generate the key from the shared secret and the salt
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, _salt);

                // Create a RijndaelManaged object
                // with the specified key and IV.
                aesAlg = new RijndaelManaged();
                aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                aesAlg.IV = key.GetBytes(aesAlg.BlockSize / 8);

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                // Create the streams used for decryption.                
                byte[] bytes = Convert.FromBase64String(cipherText);
                using (MemoryStream msDecrypt = new MemoryStream(bytes))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }
            finally
            {
                // Clear the RijndaelManaged object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            return plaintext;
        }
    }
}
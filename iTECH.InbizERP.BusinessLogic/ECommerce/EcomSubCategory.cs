﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using System.Web.UI.WebControls;

using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class EcomSubCategory
    {
        public int SubcatId { get; set; }
        public string SubcatdescLang { get; set; }
        public int SubcatCatId { get; set; }
        public string SubcatName { get; set; }
        public bool SubcatIsActive { get; set; }
        public int SubcatUpdatedBy { get; set; }
        public DateTime SubcatUpdatedOn { get; set; }
        public string SubcatImagePath { get; set; }        
        public int SubcatWebSeq { get; set; }
        public bool SubcatStatus { get; set; }      
        public string eBayCatgNo { get; set; }

        public void Insert(int userID)
        {
            string sqlGetMaxID = "SELECT MAX(subcatId) FROM subcategory";
            string sql1 = "INSERT INTO subcategory (subcatdescLang, subcatCatId, subcatName, subcatIsActive, subcatUpdatedBy, subcatUpdatedOn, ";
            sql1 += " subcatWebSeq, subcatStatus, eBayCatgNo) VALUES";
            sql1 += " (@subcatdescLang, @subcatCatId, @subcatName, @subcatIsActive, @subcatUpdatedBy, @subcatUpdatedOn, ";
            sql1 += " @subcatWebSeq, @subcatStatus, @eBayCatgNo)";
            string sql2 = "INSERT INTO subcategory (subcatId, subcatdescLang, subcatCatId, subcatName, subcatIsActive, subcatUpdatedBy, subcatUpdatedOn, ";
            sql2 += " subcatWebSeq, subcatStatus, eBayCatgNo) VALUES";
            sql2 += " (@subcatId, @subcatdescLang, @subcatCatId, @subcatName, @subcatIsActive, @subcatUpdatedBy, @subcatUpdatedOn, ";
            sql2 += " @subcatWebSeq, @subcatStatus, @eBayCatgNo)";

            string sqlMaxSeq = "SELECT IFNULL(MAX(subcatWebSeq), 0) + 1 FROM subcategory";

            DbHelper dbHelp = new DbHelper(true);

            try
            {
                dbHelp.ExecuteNonQuery(sql1, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("subcatdescLang", "en", MyDbType.String),
                    DbUtility.GetParameter("subcatCatId", this.SubcatCatId, MyDbType.Int),
                    DbUtility.GetParameter("subcatName", this.SubcatName, MyDbType.String),
                    DbUtility.GetParameter("subcatIsActive", this.SubcatIsActive, MyDbType.Boolean),
                    DbUtility.GetParameter("subcatUpdatedBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("subcatUpdatedOn", DateTime.Now, MyDbType.DateTime),                    
                    DbUtility.GetParameter("subcatWebSeq", this.SubcatWebSeq, MyDbType.Int),
                    DbUtility.GetParameter("subcatStatus", this.SubcatStatus, MyDbType.Boolean),                    
                    DbUtility.GetParameter("eBayCatgNo", this.eBayCatgNo, MyDbType.String)
                });
                object val = dbHelp.GetValue(sqlGetMaxID, CommandType.Text, null);
                if (BusinessUtility.GetInt(val) > 0)
                {
                    dbHelp.ExecuteNonQuery(sql2, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("subcatId", val, MyDbType.Int),
                        DbUtility.GetParameter("subcatdescLang", "fr", MyDbType.String),
                        DbUtility.GetParameter("subcatCatId", this.SubcatCatId, MyDbType.Int),
                        DbUtility.GetParameter("subcatName", this.SubcatName, MyDbType.String),
                        DbUtility.GetParameter("subcatIsActive", this.SubcatIsActive, MyDbType.Boolean),
                        DbUtility.GetParameter("subcatUpdatedBy", userID, MyDbType.Int),
                        DbUtility.GetParameter("subcatUpdatedOn", DateTime.Now, MyDbType.DateTime),                        
                        DbUtility.GetParameter("subcatWebSeq", this.SubcatWebSeq, MyDbType.Int),
                        DbUtility.GetParameter("subcatStatus", this.SubcatStatus, MyDbType.Boolean),                        
                        DbUtility.GetParameter("eBayCatgNo", this.eBayCatgNo, MyDbType.String)
                    });

                    dbHelp.ExecuteNonQuery(sql2, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("subcatId", val, MyDbType.Int),
                        DbUtility.GetParameter("subcatdescLang", "sp", MyDbType.String),
                        DbUtility.GetParameter("subcatName", this.SubcatName, MyDbType.String),
                        DbUtility.GetParameter("subcatIsActive", this.SubcatIsActive, MyDbType.Boolean),
                        DbUtility.GetParameter("subcatUpdatedBy", userID, MyDbType.Int),
                        DbUtility.GetParameter("subcatUpdatedOn", DateTime.Now, MyDbType.DateTime),                        
                        DbUtility.GetParameter("subcatWebSeq", this.SubcatWebSeq, MyDbType.Int),
                        DbUtility.GetParameter("subcatStatus", this.SubcatStatus, MyDbType.Boolean),                        
                        DbUtility.GetParameter("eBayCatgNo", this.eBayCatgNo, MyDbType.String)
                    });
                }
                this.SubcatId = dbHelp.GetLastInsertID();

                object maxSeq = dbHelp.GetValue(sqlMaxSeq, CommandType.Text, null);

                this.UpdateSeq(dbHelp, this.SubcatId, BusinessUtility.GetInt(maxSeq));
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void InsertImage(int subcatId, string imgPath)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.InsertImage(dbHelp, subcatId, imgPath, null);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void InsertImage(DbHelper dbHelp, int subcatId, string imgPath, string lang)
        {
            string sql = "UPDATE subcategory SET subcatImagePath=@subcatImagePath WHERE subcatId=@subcatId  ";
            if (!string.IsNullOrEmpty(lang))
            {
                sql += " AND subcatdescLang=@Lang";
            }
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("subcatImagePath", imgPath, MyDbType.String),
                    DbUtility.GetParameter("subcatId", subcatId, MyDbType.Int),
                    DbUtility.GetParameter("Lang", lang, MyDbType.String)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void RemoveImage(int subcatId, string lang)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.InsertImage(dbHelp, subcatId, null, lang);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update(int userID)
        {

            string sqlLangDependent = "UPDATE subcategory SET subcatName=@subcatName WHERE subcatId=@subcatId AND subcatdescLang=@subcatdescLang ";
            string sqlLangIndependent = "UPDATE subcategory ";
            sqlLangIndependent += " SET subcatIsActive =@subcatIsActive, subcatUpdatedBy =@subcatUpdatedBy, ";
            sqlLangIndependent += " subcatUpdatedOn =@subcatUpdatedOn, ";
            sqlLangIndependent += " subcatStatus =@subcatStatus, eBayCatgNo=@eBayCatgNo";
            if (!string.IsNullOrEmpty(this.SubcatImagePath))
            {
                sqlLangIndependent += ",subcatImagePath=@subcatImagePath";
            }
            sqlLangIndependent += " WHERE subcatId=@subcatId";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sqlLangDependent, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("subcatName", this.SubcatName, MyDbType.String),
                    DbUtility.GetParameter("subcatId", this.SubcatId, MyDbType.Int),
                    DbUtility.GetParameter("subcatdescLang", this.SubcatdescLang, MyDbType.String)
                });

                dbHelp.ExecuteNonQuery(sqlLangIndependent, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("subcatId", this.SubcatId, MyDbType.Int),                                        
                    DbUtility.GetParameter("subcatIsActive", this.SubcatIsActive, MyDbType.Boolean),
                    DbUtility.GetParameter("subcatUpdatedBy", userID, MyDbType.Int),
                    DbUtility.GetParameter("subcatUpdatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("subcatImagePath", this.SubcatImagePath, MyDbType.String),                    
                    DbUtility.GetParameter("subcatStatus", this.SubcatStatus, MyDbType.Boolean),                    
                    DbUtility.GetParameter("eBayCatgNo", this.eBayCatgNo, MyDbType.String)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateSeq(int subcatId, int seq)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.UpdateSeq(dbHelp, subcatId, seq);
            }
            catch
            {
                
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateSeq(DbHelper dbHelp, int subcatId, int seq)
        {
            string sql = string.Format("UPDATE subcategory SET subcatWebSeq={0} WHERE subcatId={1}", seq, subcatId);
            
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, null);
            }
            catch
            {
                throw;
            }
            
        }

        public void Delete(int subcatId)
        {
            string sql = "UPDATE subcategory SET subcatStatus=0, subcatIsActive=0 WHERE subcatId=@subcatId";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("subcatId", subcatId, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(int id, string lang)
        {
            string sql = "SELECT * FROM subcategory WHERE subcatId=@subcatId AND subcatdescLang=@subcatdescLang";
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("subcatId", id, MyDbType.Int),
                    DbUtility.GetParameter("subcatdescLang", lang, MyDbType.String)
                });
                if (dr.Read())
                {
                    this.SubcatdescLang = BusinessUtility.GetString(dr["subcatdescLang"]);                    
                    this.SubcatId = BusinessUtility.GetInt(dr["subcatId"]);
                    this.SubcatImagePath = BusinessUtility.GetString(dr["subcatImagePath"]);
                    this.SubcatIsActive = BusinessUtility.GetBool(dr["subcatIsActive"]);                                        
                    this.SubcatName = BusinessUtility.GetString(dr["subcatName"]);                    
                    this.SubcatStatus = BusinessUtility.GetBool(dr["subcatStatus"]);
                    this.SubcatUpdatedBy = BusinessUtility.GetInt(dr["subcatUpdatedBy"]);
                    this.SubcatUpdatedOn = BusinessUtility.GetDateTime(dr["subcatUpdatedOn"]);
                    this.SubcatWebSeq = BusinessUtility.GetInt(dr["subcatWebSeq"]);
                    this.eBayCatgNo = BusinessUtility.GetString(dr["eBayCatgNo"]);
                    this.SubcatCatId = BusinessUtility.GetInt(dr["subcatCatId"]);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public string GetSql(ParameterCollection pCol, string searchKey, string lang)
        {
            pCol.Clear();
            string sql = "Select subcatId,catname,subcatName,concat(userFirstName,' ',UserLastName) as UserName, CASE subcatIsActive WHEN 1 THEN 'green.gif' WHEN 2 THEN 'orange.gif' ELSE 'red.gif' END as subcatIsActive ,subcatStatus,subcatUpdatedOn,";
            sql += " case subcatImagePath when '' then '../images/nopic.jpg' else subcatImagePath end as subcatImagePath, ";
            sql += " subcatWebSeq, subcategory.eBayCatgNo from subcategory  inner join  users on  users.userid=subcategory.subcatUpdatedBy ";
            sql += " inner join category on subcategory.subcatCatid=category.catid AND category.catDescLang=@Lang";
            sql += " where subcatStatus='1' and subcatdescLang =@Lang";
            if (!string.IsNullOrEmpty(searchKey))
            {
                sql += " AND subcatName LIKE CONCAT('%', @Search ,'%')";
                pCol.Add("@Search", DbType.String, searchKey);
            }
            sql += "  ORDER BY subcatWebSeq";
            pCol.Add("@Lang", lang);
            return sql;
        }

        public void FillDropDown(DropDownList ddl, int catID, ListItem rootItem, string lang)
        {
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader("SELECT * FROM subcategory WHERE subcatIsActive=1 AND subcatdescLang=@subcatdescLang AND subcatCatId=@subcatCatId", CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("subcatdescLang", lang, MyDbType.String),
                    DbUtility.GetParameter("subcatCatId", catID, MyDbType.Int)
                });
                ddl.DataSource = dr;
                ddl.DataTextField = "subcatName";
                ddl.DataValueField = "subcatId";
                ddl.DataBind();

                if (rootItem != null)
                {
                    ddl.Items.Insert(0, rootItem);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }
    }
}

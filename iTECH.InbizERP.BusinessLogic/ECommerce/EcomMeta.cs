﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using MySql.Data.MySqlClient;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;

using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace iTECH.InbizERP.ECommerce
{
    public class EcomMeta
    {
        private int _metaID;

        public int MetaID
        {
            get { return _metaID; }
            set { _metaID = value; }
        }
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _content;

        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }
        private string _dir;

        public string Dir
        {
            get { return _dir; }
            set { _dir = value; }
        }
        private string _httpEquiv;

        public string HttpEquiv
        {
            get { return _httpEquiv; }
            set { _httpEquiv = value; }
        }
        private string _lang;

        public string Lang
        {
            get { return _lang; }
            set { _lang = value; }
        }
        private string _scheme;

        public string Scheme
        {
            get { return _scheme; }
            set { _scheme = value; }
        }

        public static string GetMetaTags() {
            string sqlSelect = "SELECT e.MetaID, e.Name, e.Content, e.Dir, e.HttpEquiv, e.Lang, e.Scheme FROM ecom_meta e";                   
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            StringBuilder sb = new StringBuilder();
            try
            {
                dr = dbHelp.GetDataReader(sqlSelect, CommandType.Text, null);
                while (dr.Read()) {
                    HtmlMeta meta = new HtmlMeta();
                    meta.Name = BusinessUtility.GetString(dr["Name"]);
                    meta.Content = BusinessUtility.GetString(dr["Content"]);
                    if (dr["HttpEquiv"] != DBNull.Value) {
                        meta.HttpEquiv = BusinessUtility.GetString(dr["HttpEquiv"]);
                    }
                    if (dr["Dir"] != DBNull.Value)
                    {
                        meta.Attributes.Add("dir", BusinessUtility.GetString(dr["Dir"]));
                    }
                    if (dr["Lang"] != DBNull.Value)
                    {
                        meta.Attributes.Add("lang", BusinessUtility.GetString(dr["Lang"]));
                    }
                    if (dr["Scheme"] != DBNull.Value)
                    {
                        meta.Scheme = BusinessUtility.GetString(dr["Scheme"]);
                    }
                    
                    using (StringWriter sw = new StringWriter(sb))
                    {
                        using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
                        {
                            meta.RenderControl(textWriter);
                        }
                    }
                }

                return sb.ToString();
            }
            catch
            {
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }
    }
}

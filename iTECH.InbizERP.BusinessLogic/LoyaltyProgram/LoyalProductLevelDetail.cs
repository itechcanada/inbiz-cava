﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class LoyalProductLevelDetail
    {
        public int LoyalProductLevelDetailID { get; set; }
        public int LoyalProductLevelHeaderID { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public double MinAmount { get; set; }
        public double MaxAmount { get; set; }
        public double PointsPerUnit { get; set; }

        public void Insert(DbHelper dbHelp)
        {
            bool mustclose = false;
            if (dbHelp == null)
            {
                mustclose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "INSERT INTO z_loyal_product_level_dtl (LoyalProductLevelHeaderID,ValidFrom,ValidTo,MinAmount,MaxAmount,";
                sql += " PointsPerUnit) VALUES(@LoyalProductLevelHeaderID,@ValidFrom,@ValidTo,@MinAmount,@MaxAmount,@PointsPerUnit)";

                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("LoyalProductLevelHeaderID", this.LoyalProductLevelHeaderID, MyDbType.Int),
                    DbUtility.GetParameter("ValidFrom", this.ValidFrom, MyDbType.DateTime),
                    DbUtility.GetParameter("ValidTo", this.ValidTo, MyDbType.DateTime),
                    DbUtility.GetParameter("MinAmount", this.MinAmount, MyDbType.Double),
                    DbUtility.GetParameter("MaxAmount", this.MaxAmount, MyDbType.Double),
                    DbUtility.GetParameter("PointsPerUnit", this.PointsPerUnit, MyDbType.Double)
                });

                this.LoyalProductLevelDetailID = dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustclose) dbHelp.CloseDatabaseConnection();
            }
        }
        public void Update(DbHelper dbHelp)
        {
            bool mustclose = false;
            if (dbHelp == null)
            {
                mustclose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = @"UPDATE z_loyal_product_level_dtl SET LoyalProductLevelHeaderID=@LoyalProductLevelHeaderID,
                                         ValidFrom=@ValidFrom,ValidTo=@ValidTo,MinAmount=@MinAmount,MaxAmount=@MaxAmount,
                   PointsPerUnit=@PointsPerUnit WHERE LoyalProductLevelDetailID=@LoyalProductLevelDetailID";

                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("LoyalProductLevelDetailID", this.LoyalProductLevelDetailID, MyDbType.Int),
                    DbUtility.GetParameter("LoyalProductLevelHeaderID", this.LoyalProductLevelHeaderID, MyDbType.Int),
                   DbUtility.GetParameter("ValidFrom", this.ValidFrom, MyDbType.DateTime),
                    DbUtility.GetParameter("ValidTo", this.ValidTo, MyDbType.DateTime),
                    DbUtility.GetParameter("MinAmount", this.MinAmount, MyDbType.Double),
                    DbUtility.GetParameter("MaxAmount", this.MaxAmount, MyDbType.Double),
                    DbUtility.GetParameter("PointsPerUnit", this.PointsPerUnit, MyDbType.Double)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustclose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
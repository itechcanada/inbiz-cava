﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class AmountDenmoation
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public double Amount { get; set; }
        public int AmountCount { get; set; }


        public List<AmountDenmoation> GetAmountDemonationList()
        {
            bool mustClose = false;
            DbHelper dbHelp = null;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                List<AmountDenmoation> lResult = new List<AmountDenmoation>();
                string sql = "SELECT idAmountDemoniation, Description, AmountValue FROM AmountDemoniation ";

                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new AmountDenmoation { Description = BusinessUtility.GetString(dr["Description"]), ID = BusinessUtility.GetInt(dr["idAmountDemoniation"]), Amount = BusinessUtility.GetDouble(dr["AmountValue"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }


        public List<AmountDenmoation> GetLastClosedAmountDemonationList(string sRegCode)
        {
            bool mustClose = false;
            DbHelper dbHelp = null;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sQuery = "SELECT RegTransID FROM posregistertrans    where  RegCode ='" + sRegCode + "' AND IFNULL( RegCloseDateTime,'') !=''  order by RegTransID desc limit 1 ";
                object rVal = dbHelp.GetValue(sQuery, CommandType.Text, null);


                List<AmountDenmoation> lResult = new List<AmountDenmoation>();
                //string sql = "SELECT idAmountDemoniation, Description, AmountValue FROM AmountDemoniation ";

                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(" SELECT idAmountDemoniation, Description, AmountValue, Count FROM AmountDemoniation AS AmtDemt ");
                sbQuery.Append(" JOIN amountdesc AS AmtDesc ON AmtDemt.idAmountDemoniation = AmtDesc.idAmountDemonation  AND AmtDesc.Type='C' AND AmtDesc.RegTransID = " + BusinessUtility.GetInt(rVal) + "  ");

                using (MySqlDataReader dr = dbHelp.GetDataReader(BusinessUtility.GetString(sbQuery), CommandType.Text, null))
                {
                    while (dr.Read())
                    {
                        lResult.Add(new AmountDenmoation { Description = BusinessUtility.GetString(dr["Description"]), ID = BusinessUtility.GetInt(dr["idAmountDemoniation"]), Amount = BusinessUtility.GetDouble(dr["AmountValue"]), AmountCount = BusinessUtility.GetInt(dr["Count"]) });
                    }
                    return lResult;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public Boolean SaveAmountDemoniation(List<AmountDenmoation> liAmount, int iRegOpenCloseID, string sType)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                string sqlInsert = "";
                #region SaveAmountDemonation

                foreach (var iTem in liAmount)
                {
                    sqlInsert = " INSERT INTO AmountDesc (idAmountDemonation, Count, Type, RegTransID) VALUES (@idAmountDemonation, @Count, @Type, @RegTransID) ";
                    dbTransactionHelper.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[]{
                                                        DbUtility.GetParameter("@idAmountDemonation", BusinessUtility.GetInt( iTem.ID), MyDbType.Int),
                                                        DbUtility.GetParameter("@Count", BusinessUtility.GetInt( iTem.AmountCount), MyDbType.Int),
                                                        DbUtility.GetParameter("@Type", BusinessUtility.GetString( sType), MyDbType.String),
                                                        DbUtility.GetParameter("@RegTransID", BusinessUtility.GetInt( iRegOpenCloseID), MyDbType.Int)
                                                        });
                }
                #endregion
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

    }
}

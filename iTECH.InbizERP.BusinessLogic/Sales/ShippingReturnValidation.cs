﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data;
using System.Data.Odbc;

namespace iTECH.InbizERP.BusinessLogic
{
    public class ShippingReturnValidation
    {

        public int ProductID { get; set; }
        public string SKU { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public int ShpQty { get; set; }
        public int OrdQty { get; set; }
        public int Status { get; set; }
        public int Seq { get; set; }


        public Boolean SaveShippingValidationHistory(DataTable dtValidationShipping, string orderIDs, int userID, string sValidationFor)
        {
            DataTable dtNewShipping = new DataTable();
            dtNewShipping = dtValidationShipping.Copy();


            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();
            try
            {

                if (!orderIDs.Contains(","))
                {
                    int orderID = BusinessUtility.GetInt(orderIDs);
                    if (orderID > 0)
                    {
                        foreach (DataRow dRow in dtNewShipping.Rows)
                        {
                            if (BusinessUtility.GetInt(dRow["ProductID"]) > 0)
                            {
                                StringBuilder sbInsertQuery = new StringBuilder();
                                if (sValidationFor.ToUpper() == "shipping".ToUpper())
                                {
                                    sbInsertQuery.Append(" INSERT INTO OrderShippingValidation (OrdID, ProdID, ShpQty, OrdQty, Validationdatetime, ValidateBy, Status) ");
                                }
                                else if (sValidationFor.ToUpper() == "planshippment".ToUpper())
                                {
                                    sbInsertQuery.Append(" INSERT INTO orderShippmentvalidation (OrdID, ProdID, ShpQty, OrdQty, Validationdatetime, ValidateBy, Status) ");
                                }
                                else
                                {
                                    sbInsertQuery.Append(" INSERT INTO OrderTransferValidation (OrdID, ProdID, ShpQty, OrdQty, Validationdatetime, ValidateBy, Status) ");
                                }
                                sbInsertQuery.Append(" VALUES (@OrdID, @ProdID, @ShpQty, @OrdQty, @Validationdatetime, @ValidateBy, @Status) ");
                                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("OrdID", orderID, MyDbType.Int),
                        DbUtility.GetParameter("ProdID",BusinessUtility.GetInt(dRow["ProductID"]), MyDbType.Int),
                        DbUtility.GetParameter("ShpQty", BusinessUtility.GetInt(dRow["ShpQty"]), MyDbType.Int),
                        DbUtility.GetParameter("OrdQty", BusinessUtility.GetInt(dRow["OrdQty"]), MyDbType.Int),
                        DbUtility.GetParameter("Validationdatetime", DateTime.Now, MyDbType.DateTime),
                        DbUtility.GetParameter("ValidateBy", userID, MyDbType.Int),
                        DbUtility.GetParameter("Status", BusinessUtility.GetInt(dRow["Status"]), MyDbType.Int),
                        });
                            }
                        }
                    }
                }
                else
                {
                    string[] sOrderlst = orderIDs.Split(',');
                    foreach (string sOrder in sOrderlst)
                    {
                        foreach (DataRow dRow in dtNewShipping.Rows)
                        {
                            OrderItems objOrdItems = new OrderItems();
                            var lstOrdItems = objOrdItems.GetOrderItemList(sOrder, BusinessUtility.GetInt(dRow["ProductID"]));
                            foreach (var itemOrder in lstOrdItems)
                            {
                                if ((BusinessUtility.GetInt(itemOrder.OrdProductQty) > 0) && (BusinessUtility.GetInt(dRow["ShpQty"]) > 0))
                                {

                                    int pOrderQty = BusinessUtility.GetInt(itemOrder.OrdProductQty);
                                    int iTotalSipQty = BusinessUtility.GetInt(dRow["ShpQty"]);
                                    int QtyToShipinCPrd = 0;
                                    if (pOrderQty <= iTotalSipQty)
                                    {
                                        //iTotalSipQty
                                        QtyToShipinCPrd = pOrderQty;
                                        iTotalSipQty = iTotalSipQty - QtyToShipinCPrd;
                                    }
                                    else if (pOrderQty >= iTotalSipQty)
                                    {
                                        QtyToShipinCPrd = iTotalSipQty;
                                        iTotalSipQty = 0;
                                    }


                                    StringBuilder sbInsertQuery = new StringBuilder();
                                    if (sValidationFor.ToUpper() == "shipping".ToUpper())
                                    {
                                        sbInsertQuery.Append(" INSERT INTO OrderShippingValidation (OrdID, ProdID, ShpQty, OrdQty, Validationdatetime, ValidateBy, Status) ");
                                    }
                                    else if (sValidationFor.ToUpper() == "planshippment".ToUpper())
                                    {
                                        sbInsertQuery.Append(" INSERT INTO orderShippmentvalidation (OrdID, ProdID, ShpQty, OrdQty, Validationdatetime, ValidateBy, Status) ");
                                    }
                                    else
                                    {
                                        sbInsertQuery.Append(" INSERT INTO OrderTransferValidation (OrdID, ProdID, ShpQty, OrdQty, Validationdatetime, ValidateBy, Status) ");
                                    }
                                    sbInsertQuery.Append(" VALUES (@OrdID, @ProdID, @ShpQty, @OrdQty, @Validationdatetime, @ValidateBy, @Status) ");
                                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("OrdID", sOrder, MyDbType.Int),
                                    DbUtility.GetParameter("ProdID",BusinessUtility.GetInt(dRow["ProductID"]), MyDbType.Int),
                                    DbUtility.GetParameter("ShpQty", QtyToShipinCPrd, MyDbType.Int),
                                    DbUtility.GetParameter("OrdQty", BusinessUtility.GetInt(itemOrder.OrdProductQty), MyDbType.Int),
                                    DbUtility.GetParameter("Validationdatetime", DateTime.Now, MyDbType.DateTime),
                                    DbUtility.GetParameter("ValidateBy", userID, MyDbType.Int),
                                    DbUtility.GetParameter("Status", BusinessUtility.GetInt(dRow["Status"]), MyDbType.Int),
                                    });



                                    dRow["ShpQty"] = iTotalSipQty;
                                    dtNewShipping.AcceptChanges();

                                    //if (BusinessUtility.GetInt(dRow["ShpQty"]) <= BusinessUtility.GetInt(itemOrder.OrdProductQty))
                                    //{
                                    //    dRow["ShpQty"] = BusinessUtility.GetInt(itemOrder.OrdProductQty) - BusinessUtility.GetInt(dRow["ShpQty"]);
                                    //    dtNewShipping.AcceptChanges();
                                    //}
                                }
                                else
                                {
                                    StringBuilder sbInsertQuery = new StringBuilder();
                                    if (sValidationFor.ToUpper() == "shipping".ToUpper())
                                    {
                                        sbInsertQuery.Append(" INSERT INTO OrderShippingValidation (OrdID, ProdID, ShpQty, OrdQty, Validationdatetime, ValidateBy, Status) ");
                                    }
                                    else if (sValidationFor.ToUpper() == "planshippment".ToUpper())
                                    {
                                        sbInsertQuery.Append(" INSERT INTO orderShippmentvalidation (OrdID, ProdID, ShpQty, OrdQty, Validationdatetime, ValidateBy, Status) ");
                                    }
                                    else
                                    {
                                        sbInsertQuery.Append(" INSERT INTO OrderTransferValidation (OrdID, ProdID, ShpQty, OrdQty, Validationdatetime, ValidateBy, Status) ");
                                    }
                                    sbInsertQuery.Append(" VALUES (@OrdID, @ProdID, @ShpQty, @OrdQty, @Validationdatetime, @ValidateBy, @Status) ");
                                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(sbInsertQuery), CommandType.Text, new MySqlParameter[]{
                                    DbUtility.GetParameter("OrdID", sOrder, MyDbType.Int),
                                    DbUtility.GetParameter("ProdID",BusinessUtility.GetInt(dRow["ProductID"]), MyDbType.Int),
                                    DbUtility.GetParameter("ShpQty", BusinessUtility.GetInt(dRow["ShpQty"]), MyDbType.Int),
                                    DbUtility.GetParameter("OrdQty", BusinessUtility.GetInt(itemOrder.OrdProductQty), MyDbType.Int),
                                    DbUtility.GetParameter("Validationdatetime", DateTime.Now, MyDbType.DateTime),
                                    DbUtility.GetParameter("ValidateBy", userID, MyDbType.Int),
                                    DbUtility.GetParameter("Status", BusinessUtility.GetInt(dRow["Status"]), MyDbType.Int),
                                    });
                                }
                            }
                        }
                    }
                }
                dbTransactionHelper.CommitTransaction();
                return true;
            }
            catch
            {
                dbTransactionHelper.RollBackTransaction();
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }

        public DataTable GetShippingHistory(int orderID, string lang, string sValidationFor)
        {
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(" SELECT Products.ProductID, Products.prdUPCCode AS 'SKU',  pCollection.ShortName AS Collection, pCd.Style AS 'Style', pcol.Color{0} AS 'Color',  psz.Size{0} AS 'Size', osv.ShpQty AS ShpQty, osv.OrdQty AS OrdQty, ");
            sbQuery.Append(" osv.Status ");
            if (sValidationFor.ToUpper() == "shipping".ToUpper())
            {
                sbQuery.Append(" FROM OrderShippingValidation osv ");
            }
            else
            {
                sbQuery.Append(" FROM OrderTransferValidation osv ");
            }
            sbQuery.Append(" INNER JOIN Products ON Products.productID = osv.ProdID ");
            sbQuery.Append(" INNER JOIN ProductClothDesc AS pCd ON pCd.ProductID = Products.productID  ");
            sbQuery.Append(" LEFT OUTER JOIN productcolor pcol ON pcol.colorID = pCd.Color  ");
            sbQuery.Append(" LEFT OUTER JOIN productsize psz ON psz.sizeID = pCd.Size  ");
            sbQuery.Append(" INNER JOIN productcycle as pCollection ON pCollection.CycleID = pCd.Collection ");
            sbQuery.Append(" WHERE osv.OrdID = @OrdID ");
            DbHelper dbHelp = new DbHelper();
            try
            {
                return dbHelp.GetDataTable(string.Format(BusinessUtility.GetString(sbQuery), lang), CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("OrdID", orderID, MyDbType.Int) });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}

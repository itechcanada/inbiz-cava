﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

using Newtonsoft.Json;

namespace iTECH.InbizERP.BusinessLogic
{
    public class OrderTransaction
    {
        public int ID { get; set; }
        public string TransactionID { get; set; }
        public int OrderID { get; set; }
        public TransactionStatus TransactionStatusID { get; set; }
        public TransactionType TransactionTypeID { get; set; }
        public string PaymentMethod { get; set; }
        public string GatewayResponse { get; set; }
        public double Amount { get; set; }
        public DateTime TransactionDate { get; set; }
        public string GatewayErrors { get; set; }
        public string CardType { get; set; }
        public string IPAddress { get; set; }
        private string MetaData
        {
            get
            {
                if (this.MetaDataCollection.Count > 0)
                {
                    return JsonConvert.SerializeObject(this.MetaDataCollection);
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        public Dictionary<string, string> MetaDataCollection { get; private set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }

        public OrderTransaction() {
            this.MetaDataCollection = new Dictionary<string, string>();
        }

        public void Insert(DbHelper dbHelp, int userid)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            string sql = "INSERT INTO z_order_transaction (TransactionID, OrderID,TransactionStatusID,TransactionTypeID,PaymentMethod,GatewayResponse,Amount,TransactionDate,GatewayErrors,CardType,IPAddress,MetaData, CreatedBy,CreatedOn)";
            sql += " VALUES(@TransactionID, @OrderID,@TransactionStatusID,@TransactionTypeID,@PaymentMethod,@GatewayResponse,@Amount,@TransactionDate,@GatewayErrors,@CardType,@IPAddress,@MetaData, @CreatedBy,@CreatedOn)";            

            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("Amount", this.Amount, MyDbType.Double),
                    DbUtility.GetParameter("CreatedBy", userid, MyDbType.String),
                    DbUtility.GetParameter("CreatedOn", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("GatewayErrors", this.GatewayErrors, MyDbType.String),
                    DbUtility.GetParameter("CardType", this.CardType, MyDbType.String),
                    DbUtility.GetParameter("IPAddress", this.IPAddress, MyDbType.String),
                    DbUtility.GetParameter("GatewayResponse", this.GatewayResponse, MyDbType.String),
                    DbUtility.GetParameter("MetaData", this.MetaData, MyDbType.String),
                    DbUtility.GetParameter("OrderID", this.OrderID, MyDbType.Int),
                    DbUtility.GetParameter("PaymentMethod", this.PaymentMethod, MyDbType.String),
                    DbUtility.GetParameter("TransactionDate", this.TransactionDate, MyDbType.DateTime),
                    DbUtility.GetParameter("TransactionID", this.TransactionID, MyDbType.String),
                    DbUtility.GetParameter("TransactionStatusID", (int)this.TransactionStatusID, MyDbType.Int),
                    DbUtility.GetParameter("TransactionTypeID", (int)this.TransactionTypeID, MyDbType.Int)
                });
                this.ID = dbHelp.GetLastInsertID();
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //public bool Insert()
        //{
        //    DbHelper dbHelp = new DbHelper();
        //    try
        //    {                
        //        string sql = "INSERT INTO transaction (TansactionID, PaymentGatWay, Amount, Responce, OrderID, MetaData, CreatedDateTime) ";
        //        sql += " VALUES(@TansactionID, @PaymentGatWay, @Amount, @Responce, @OrderID, @MetaData, @CreatedDateTime)";
        //        dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
        //            DbUtility.GetParameter("TansactionID", this.TansactionID, MyDbType.String),
        //            DbUtility.GetParameter("PaymentGatWay", this.PaymentGatWay, MyDbType.String),
        //            DbUtility.GetParameter("Amount", this.Amount, MyDbType.Double),
        //            DbUtility.GetParameter("Responce", this.Responce, MyDbType.String),
        //            DbUtility.GetParameter("OrderID", this.OrderID, MyDbType.Int),
        //            DbUtility.GetParameter("MetaData", this.MetaData, MyDbType.String),
        //            DbUtility.GetParameter("CreatedDateTime", this.CreatedDateTime, MyDbType.DateTime)
        //        });
        //        this.TranID = dbHelp.GetLastInsertID();
        //        return this.TranID > 0;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public string GetSaleTransactionID(int orderID)
        {            
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                return this.GetSaleTransactionID(dbHelp, orderID);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSaleTransactionID(DbHelper dbHelp, int orderID)
        {
            string sql = string.Format("SELECT t.TransactionID FROM z_order_transaction t WHERE t.OrderID=@OrderID AND IFNULL(t.TransactionID, '')<>'' AND t.TransactionTypeID={0} AND t.TransactionStatusID={1}", (int)TransactionType.Sale, (int)TransactionStatus.Success);            
            try
            {
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderID", orderID, MyDbType.Int)
                });

                return BusinessUtility.GetString(val);
            }
            catch
            {
                throw;
            }            
        }
    }

    public enum TransactionStatus { 
        Success = 1,
        Failed = 2
    }

    public enum TransactionType
    {
        Sale = 1,
        Refund = 2
    }
}
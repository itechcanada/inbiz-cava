﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql; 
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class OrderTypeDetail
    {

        #region Private Member
        private int _userID;
        private int _orderTypeID;
        private int _orderTypeDtlID;

        private double _ordercommission;
        #endregion
        #region Property
        public int UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }
        public int OrderTypeID
        {
            get { return _orderTypeID; }
            set { _orderTypeID = value; }
        }
        public int OrderTypeDtlID
        {
            get { return _orderTypeDtlID; }
            set { _orderTypeDtlID = value; }
        }
        public double Ordercommission
        {
            get { return _ordercommission; }
            set { _ordercommission = value; }
        }
        #endregion
        
        /// <summary>
        /// Insert Order Type
        /// </summary>
       public int InsertOrdertype()
        {
            string strSQL = null;
            strSQL = "INSERT INTO ordertypedtl(orderTypeID, userID, ordercommission) VALUES(@orderTypeID, @userID, @ordercommission)";

            MySqlParameter[] p = { 
                                     new MySqlParameter("@orderTypeID", this._orderTypeID),
                                     new MySqlParameter("@userID", this._userID),
                                     new MySqlParameter("@ordercommission", this._ordercommission)
                                                                      
                                 };
            DbHelper dbHelper = new DbHelper();

            try
            {
                dbHelper.ExecuteNonQuery(strSQL, System.Data.CommandType.Text, p);
                int orderTypeDtlID = dbHelper.GetLastInsertID();
                if (orderTypeDtlID > 0)
                {
                    return orderTypeDtlID;
                }
                return 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

       public string DeleteOderTypeDetail(ParameterCollection pCol, int orderTypeDtlID)
       {
           pCol.Clear();
           string strSQL = "";
           strSQL = "Delete from ordertypedtl where orderTypeDtlID =@orderTypeDtlID";
           if (orderTypeDtlID<0)
           {
               orderTypeDtlID = 0;
           }
           pCol.Add("@orderTypeDtlID", DbType.Int32, orderTypeDtlID.ToString());
           return strSQL;
       }

        /// <summary>
        /// Fill the Drop Dwon List for Order Type
        /// </summary>
        /// <param name="dlOrderType"></param>
        public DataTable GetOrderTypes(string lang)
        {
            string strSQL = null;
            strSQL = "SELECT ordertypeID,orderTypeDesc from ordertype  where orderTypeLang=@orderTypeLang";
            MySqlParameter[] p = { 
                                     new MySqlParameter("@orderTypeLang", lang)
                                                                      
                                 };
            DbHelper dbHelper = new DbHelper();
            try
            {
                return dbHelper.GetDataTable(strSQL, System.Data.CommandType.Text, p);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// to Fill the Grid View 
        /// </summary>
        /// <returns></returns>
        public string GetSqlOrderTypes(ParameterCollection pCol, int userid, string lang)
        {
            pCol.Clear();
            string sql = "SELECT orderTypeDtlID,orderTypeDesc, userID, ordercommission FROM ordertypedtl Inner join ordertype on ordertype.orderTypeID=ordertypedtl.orderTypeID  Where 1=1";
            sql += " And userID = @userID And orderTypeLang like CONCAT('%', @orderTypeLang, '%')";
            pCol.Add("@userID", userid.ToString());
            pCol.Add("@orderTypeLang", lang);
            return sql;
        }

        /// <summary>
        /// Find Duplicate Order Type
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool CheckDuplicateOderType(int orderTypeID, int userid)
        {
            string strSQL = null;
            strSQL = "SELECT count(*) FROM ordertypedtl where orderTypeID=@orderTypeID and userID=@userID";
            MySqlParameter[] p = { 
                                     new MySqlParameter("@orderTypeID", orderTypeID),
                                     new MySqlParameter("@userID", userid)                                                                      
                                 };
            DbHelper dbHelper = new DbHelper();
            try
            {
                object obj=dbHelper.GetValue(strSQL, System.Data.CommandType.Text, p);
                return BusinessUtility.GetInt(obj) > 0;
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelper.CloseDatabaseConnection();
            }
        }

    }
}

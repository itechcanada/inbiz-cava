﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web.UI.WebControls;
using System.Web;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;


namespace iTECH.InbizERP.BusinessLogic
{
    public class OrderItemProcess
    {
        public int OrderItemProcID { get; set; }
        public int OrdID{ get; set; }
        public string OrdItemProcCode{ get; set; }
        public double OrdItemProcFixedPrice{ get; set; }
        public double OrdItemProcPricePerHour{ get; set; }
        public int OrdItemProcHours{ get; set; }
        public double OrdItemProcPricePerUnit{ get; set; }
        public int OrdItemProcUnits{ get; set; }
        public double OrdItemProcInternalCost{ get; set; } //Added by mukesh 20130523
        public int SysTaxCodeDescID{ get; set; }

        public double Tax1 { get; set; }
        public double Tax2 { get; set; }
        public double Tax3 { get; set; }
        public double Tax4 { get; set; }
        public double Tax5 { get; set; }
        public string TaxDesc1 { get; set; }
        public string TaxDesc2 { get; set; }
        public string TaxDesc3 { get; set; }
        public string TaxDesc4 { get; set; }
        public string TaxDesc5 { get; set; }
        public double TaxCalculated1 { get; set; }
        public double TaxCalculated2 { get; set; }
        public double TaxCalculated3 { get; set; }
        public double TaxCalculated4 { get; set; }
        public double TaxCalculated5 { get; set; }

        public void Insert()
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.Insert(dbHelp);
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Insert(DbHelper dbHelp)
        {
            string sqlInsert = "INSERT INTO orderitemprocess( ordID, ordItemProcCode, ordItemProcFixedPrice, ordItemProcPricePerHour, ordItemProcHours, ";
            sqlInsert += "ordItemProcPricePerUnit, ordItemProcUnits,sysTaxCodeDescID,ordItemProcInternalCost) VALUES(@ordID, @ordItemProcCode, @ordItemProcFixedPrice, ";
            sqlInsert += "@ordItemProcPricePerHour, @ordItemProcHours,@ordItemProcPricePerUnit, @ordItemProcUnits,@sysTaxCodeDescID,@ordItemProcInternalCost)"; //Edited by mukesh 20130523
            try
            {
                int? taxCode = this.SysTaxCodeDescID > 0 ? (int?)this.SysTaxCodeDescID : null;
                dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ordID", this.OrdID, MyDbType.Int),
                    DbUtility.GetParameter("ordItemProcCode", this.OrdItemProcCode, MyDbType.String),
                    DbUtility.GetParameter("ordItemProcFixedPrice", this.OrdItemProcFixedPrice, MyDbType.Double),
                    DbUtility.GetParameter("ordItemProcPricePerHour", this.OrdItemProcPricePerHour, MyDbType.Double),
                    DbUtility.GetParameter("ordItemProcHours", this.OrdItemProcHours, MyDbType.Int),
                    DbUtility.GetParameter("ordItemProcPricePerUnit", this.OrdItemProcPricePerUnit, MyDbType.Double),
                    DbUtility.GetParameter("ordItemProcUnits", this.OrdItemProcUnits, MyDbType.Int),
                    DbUtility.GetParameter("ordItemProcInternalCost", this.OrdItemProcInternalCost, MyDbType.Double), //Added by mukesh 20130523
                    DbUtility.GetIntParameter("sysTaxCodeDescID", taxCode)
                });
                this.OrderItemProcID = dbHelp.GetLastInsertID();

                if (this.OrderItemProcID > 0)
                {
                    this.UpdateTax(dbHelp, this.OrderItemProcID, this.SysTaxCodeDescID);
                }
            }
            catch 
            {                
                throw;
            }
        }

        public bool AddProcessItems(DbHelper dbHelp, List<OrderItemProcess> lstItems)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            //string sqlInsert = "INSERT INTO orderitemprocess( ordID, ordItemProcCode, ordItemProcFixedPrice, ordItemProcPricePerHour, ordItemProcHours, ";
            //sqlInsert += "ordItemProcPricePerUnit, ordItemProcUnits,sysTaxCodeDescID) VALUES(@ordID, @ordItemProcCode, @ordItemProcFixedPrice, ";
            //sqlInsert += "@ordItemProcPricePerHour, @ordItemProcHours,@ordItemProcPricePerUnit, @ordItemProcUnits,@sysTaxCodeDescID)";            
            try
            {
                foreach (OrderItemProcess item in lstItems)
                {
                    item.Insert(dbHelp);
                    //int? taxCode = item.SysTaxCodeDescID > 0 ? (int?)item.SysTaxCodeDescID : null;
                    //dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                    //    DbUtility.GetParameter("ordID", item.OrdID, MyDbType.Int),
                    //    DbUtility.GetParameter("ordItemProcCode", item.OrdItemProcCode, MyDbType.String),
                    //    DbUtility.GetParameter("ordItemProcFixedPrice", item.OrdItemProcFixedPrice, MyDbType.Double),
                    //    DbUtility.GetParameter("ordItemProcPricePerHour", item.OrdItemProcPricePerHour, MyDbType.Double),
                    //    DbUtility.GetParameter("ordItemProcHours", item.OrdItemProcHours, MyDbType.Int),
                    //    DbUtility.GetParameter("ordItemProcPricePerUnit", item.OrdItemProcPricePerUnit, MyDbType.Double),
                    //    DbUtility.GetParameter("ordItemProcUnits", item.OrdItemProcUnits, MyDbType.Int),
                    //    DbUtility.GetIntParameter("sysTaxCodeDescID", taxCode)
                    //});

                    //item.OrderItemProcID = dbHelp.GetLastInsertID();
                }
                
                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if(mustClose) dbHelp.CloseDatabaseConnection();
            }
        }       

        public void Update(DbHelper dbHelp, int processItemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            //string sqlCheck = "SELECT COUNT(*) FROM orderitemprocess WHERE orderItemProcID=@orderItemProcID AND IFNULL(sysTaxCodeDescID, 0)=@NewTaxGroup";
            string sql = "UPDATE orderitemprocess SET ordItemProcFixedPrice=@ordItemProcFixedPrice,ordItemProcPricePerHour=@ordItemProcPricePerHour,";
            sql += "ordItemProcHours=@ordItemProcHours,ordItemProcPricePerUnit=@ordItemProcPricePerUnit,ordItemProcUnits=@ordItemProcUnits,";
            sql += "sysTaxCodeDescID=@sysTaxCodeDescID,ordItemProcInternalCost=@ordItemProcInternalCost WHERE orderItemProcID=@orderItemProcID";  //Edited by mukesh 20130523          
            try
            {                
                int? taxGrp = this.SysTaxCodeDescID > 0 ? (int?)this.SysTaxCodeDescID : null;
                
                //var oTax = dbHelp.GetValue(sqlCheck, CommandType.Text, new MySqlParameter[] { 
                //    DbUtility.GetParameter("orderItemProcID", processItemID, MyDbType.Int),
                //    DbUtility.GetParameter("NewTaxGroup", taxGrp, MyDbType.Int)
                //});
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ordItemProcFixedPrice", this.OrdItemProcFixedPrice, MyDbType.Double),
                    DbUtility.GetParameter("ordItemProcPricePerHour", this.OrdItemProcPricePerHour, MyDbType.Double),
                    DbUtility.GetParameter("ordItemProcHours", this.OrdItemProcHours, MyDbType.Int),
                    DbUtility.GetParameter("ordItemProcPricePerUnit", this.OrdItemProcPricePerUnit, MyDbType.Double),
                    DbUtility.GetParameter("ordItemProcUnits", this.OrdItemProcUnits, MyDbType.Int),
                    DbUtility.GetParameter("ordItemProcInternalCost", this.OrdItemProcInternalCost, MyDbType.Double), //Added by mukesh 20130523
                    DbUtility.GetIntParameter("sysTaxCodeDescID", taxGrp),
                    DbUtility.GetParameter("orderItemProcID", processItemID, MyDbType.Int)
                });

                this.UpdateTax(dbHelp, processItemID, this.SysTaxCodeDescID);

                //if (BusinessUtility.GetInt(oTax) == 0) //If Tax was updated need to update tax information in order item
                //{
                //    this.UpdateTax(dbHelp, processItemID, this.SysTaxCodeDescID);
                //}
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void UpdateTax(DbHelper dbHelp, int orderProcessItemID, int taxGroupID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sql = "UPDATE orderitemprocess SET sysTaxCodeDescID=@TaxGroupID, Tax1=@Tax1, TaxDesc1=@TaxDesc1,";
                sql += "Tax2=@Tax2, TaxDesc2=@TaxDesc2,Tax3=@Tax3, TaxDesc3=@TaxDesc3,Tax4=@Tax4, TaxDesc4=@TaxDesc4,";
                sql += "Tax5=@Tax5,TaxDesc5=@TaxDesc5,TaxCalculated1=@TaxCalculated1,TaxCalculated2=@TaxCalculated2,TaxCalculated3=@TaxCalculated3,TaxCalculated4=@TaxCalculated4,TaxCalculated5=@TaxCalculated5 WHERE orderItemProcID=@orderItemProcID";
                string sqlAmount = "SELECT TaxGroupID, Amount FROM  vw_order_process_item_amount WHERE OrderProcessItemID=@OrderProcessItemID";
                Tax tx = new Tax();
                double amount = 0.00D;
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlAmount, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("OrderProcessItemID", orderProcessItemID, MyDbType.Int)
                }))
                {
                    if (dr.Read())
                    {
                        amount = BusinessUtility.GetDouble(dr["Amount"]);
                        if (taxGroupID <= 0)
                        {
                            taxGroupID = BusinessUtility.GetInt(dr["TaxGroupID"]);
                        }
                    }
                }
                if (taxGroupID > 0)
                {
                    tx.PopulateObject(dbHelp, taxGroupID, amount);
                }
                int? txGrp = taxGroupID > 0 ? (int?)taxGroupID : null;                
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetIntParameter("TaxGroupID", txGrp),
                    DbUtility.GetParameter("Tax1", tx.Tax1, MyDbType.Double),
                    DbUtility.GetParameter("Tax2", tx.Tax2, MyDbType.Double),
                    DbUtility.GetParameter("Tax3", tx.Tax3, MyDbType.Double),
                    DbUtility.GetParameter("Tax4", tx.Tax4, MyDbType.Double),
                    DbUtility.GetParameter("Tax5", tx.Tax5, MyDbType.Double),
                    DbUtility.GetParameter("TaxDesc1", tx.TaxDesc1, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc2", tx.TaxDesc2, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc3", tx.TaxDesc3, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc4", tx.TaxDesc4, MyDbType.String),
                    DbUtility.GetParameter("TaxDesc5", tx.TaxDesc5, MyDbType.String),
                    DbUtility.GetParameter("orderItemProcID", orderProcessItemID, MyDbType.Int),
                    DbUtility.GetParameter("TaxCalculated1", tx.TaxCalculated1, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated2", tx.TaxCalculated2, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated3", tx.TaxCalculated3, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated4", tx.TaxCalculated4, MyDbType.Double),
                    DbUtility.GetParameter("TaxCalculated5", tx.TaxCalculated5, MyDbType.Double)
                });

                //Set Tax Info with current object 
                this.SysTaxCodeDescID = taxGroupID;
                this.Tax1 = tx.Tax1;
                this.Tax2 = tx.Tax2;
                this.Tax3 = tx.Tax3;
                this.Tax4 = tx.Tax4;
                this.Tax5 = tx.Tax5;
                this.TaxDesc1 = tx.TaxDesc1;
                this.TaxDesc2 = tx.TaxDesc2;
                this.TaxDesc3 = tx.TaxDesc3;
                this.TaxDesc4 = tx.TaxDesc4;
                this.TaxDesc5 = tx.TaxDesc5;
                this.TaxCalculated1 = tx.TaxCalculated1;
                this.TaxCalculated2 = tx.TaxCalculated2;
                this.TaxCalculated3 = tx.TaxCalculated3;
                this.TaxCalculated4 = tx.TaxCalculated4;
                this.TaxCalculated5 = tx.TaxCalculated5;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        public void Delete(int processItemID)
        {
            string sqlDelete = "DELETE FROM orderitemprocess WHERE orderItemProcID=@orderItemProcID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sqlDelete, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("orderItemProcID", processItemID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        //public DataTable GetOrderProcessItems(DbHelper dbHelp, int orderID)
        //{
        //    bool mustClose = false;
        //    if (dbHelp == null)
        //    {
        //        mustClose = true;
        //        dbHelp = new DbHelper(true);
        //    }
        //    string strSQL = " SELECT orderItemProcID, i.ordID, ordItemProcCode, ProcessDescription, (ordItemProcFixedPrice*o.ordCurrencyExRate) as ordItemProcFixedPrice, ";
        //    strSQL += "(ordItemProcPricePerHour*o.ordCurrencyExRate) as ordItemProcPricePerHour,  OrdItemProcFixedPrice as InOrdItemProcFixedPrice,";
        //    strSQL += "ordItemProcPricePerHour as InOrdItemProcPricePerHour,ordItemProcPricePerUnit as InOrdItemProcPricePerUnit, ordItemProcHours,";
        //    strSQL += " (ordItemProcPricePerUnit*o.ordCurrencyExRate) as ordItemProcPricePerUnit, ordItemProcUnits, ";
        //    strSQL += "o.ordCurrencyExRate*(ordItemProcFixedPrice+(ordItemProcPricePerHour*ordItemProcHours)+(ordItemProcPricePerUnit*ordItemProcUnits)) as ProcessCost ,";
        //    strSQL += " IFNULL(systaxcodedesc.sysTaxCodeDescID,0) as sysTaxCodeDescID, IFNULL(systaxcodedesc.sysTaxCodeDescText, 'Warehous Tax') as sysTaxCodeDescText";
        //    strSQL += " FROM orderitemprocess i inner join orders o on o.ordID=i.ordID inner join sysprocessgroup on ProcessCode=ordItemProcCode";
        //    strSQL += " Left outer  join systaxcodedesc on i.sysTaxCodeDescID = systaxcodedesc.sysTaxCodeDescID where i.ordID=@ordID";
        //    strSQL += " order by ProcessDescription";            
        //    try
        //    {
        //        return dbHelp.GetDataTable(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ordID", orderID, MyDbType.Int) });
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if(mustClose) dbHelp.CloseDatabaseConnection();
        //    }
        //}

        public IDataReader GetOrderProcessItemsReader(DbHelper dbHelp, int orderID)
        {
            string strSQL = " SELECT orderItemProcID, i.ordID, ordItemProcCode, ProcessDescription, (ordItemProcFixedPrice*o.ordCurrencyExRate) as ordItemProcFixedPrice, ";
            strSQL += "(ordItemProcPricePerHour*o.ordCurrencyExRate) as ordItemProcPricePerHour,  OrdItemProcFixedPrice as InOrdItemProcFixedPrice,";
            strSQL += "ordItemProcPricePerHour as InOrdItemProcPricePerHour,ordItemProcPricePerUnit as InOrdItemProcPricePerUnit, ordItemProcHours,";
            strSQL += " (ordItemProcPricePerUnit*o.ordCurrencyExRate) as ordItemProcPricePerUnit, ordItemProcUnits, ordItemProcInternalCost, "; //Edited by mukesh 20130523
            strSQL += "o.ordCurrencyExRate*(ordItemProcFixedPrice+(ordItemProcPricePerHour*ordItemProcHours)+(ordItemProcPricePerUnit*ordItemProcUnits)) as ProcessCost ,"; 
            strSQL += " IFNULL(systaxcodedesc.sysTaxCodeDescID,0) as sysTaxCodeDescID, IFNULL(systaxcodedesc.sysTaxCodeDescText, 'Warehous Tax') as sysTaxCodeDescText,";
            strSQL += " i.Tax1,i.Tax2,i.Tax3,i.Tax4,i.Tax5,i.TaxCalculated1,i.TaxCalculated2,i.TaxCalculated3,i.TaxCalculated4,i.TaxCalculated5,";
            strSQL += "i.TaxDesc1,i.TaxDesc2,i.TaxDesc3,i.TaxDesc4,i.TaxDesc5";
            strSQL += " FROM orderitemprocess i inner join orders o on o.ordID=i.ordID inner join sysprocessgroup on ProcessCode=ordItemProcCode";
            strSQL += " Left outer  join systaxcodedesc on i.sysTaxCodeDescID = systaxcodedesc.sysTaxCodeDescID where i.ordID=@ordID";
            strSQL += " order by ProcessDescription";            
            try
            {
                return dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("ordID", orderID, MyDbType.Int) });
            }
            catch
            {
                throw;
            }            
        }

        public List<OrderItemProcess> GetProcessItemsList(DbHelper dbHelp, int orderID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
            }
            try
            {
                string sql = "SELECT * FROM orderitemprocess WHERE ordID=@OrderID";
                List<OrderItemProcess> lResult = new List<OrderItemProcess>();
                using (MySqlDataReader dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[]{ DbUtility.GetParameter("OrderID", orderID, MyDbType.Int) }))
                {
                    while (dr.Read())
                    {
                        OrderItemProcess i = new OrderItemProcess();
                        i.OrderItemProcID = BusinessUtility.GetInt(dr["orderItemProcID"]);
                        i.OrdID = BusinessUtility.GetInt(dr["ordID"]);
                        i.OrdItemProcCode = BusinessUtility.GetString(dr["ordItemProcCode"]);
                        i.OrdItemProcFixedPrice = BusinessUtility.GetDouble(dr["ordItemProcFixedPrice"]);
                        i.OrdItemProcPricePerHour = BusinessUtility.GetDouble(dr["ordItemProcPricePerHour"]);
                        i.OrdItemProcHours = BusinessUtility.GetInt(dr["ordItemProcHours"]);
                        i.OrdItemProcPricePerUnit = BusinessUtility.GetDouble(dr["ordItemProcPricePerUnit"]);
                        i.OrdItemProcUnits = BusinessUtility.GetInt(dr["ordItemProcUnits"]);
                        i.SysTaxCodeDescID = BusinessUtility.GetInt(dr["sysTaxCodeDescID"]);
                        i.Tax1 = BusinessUtility.GetDouble(dr["Tax1"]);
                        i.TaxCalculated1 = BusinessUtility.GetDouble(dr["TaxCalculated1"]);
                        i.TaxDesc1 = BusinessUtility.GetString(dr["TaxDesc1"]);
                        i.Tax2 = BusinessUtility.GetDouble(dr["Tax2"]);
                        i.TaxCalculated2 = BusinessUtility.GetDouble(dr["TaxCalculated2"]);
                        i.TaxDesc2 = BusinessUtility.GetString(dr["TaxDesc2"]);
                        i.Tax3 = BusinessUtility.GetDouble(dr["Tax3"]);
                        i.TaxCalculated3 = BusinessUtility.GetDouble(dr["TaxCalculated3"]);
                        i.TaxDesc3 = BusinessUtility.GetString(dr["TaxDesc3"]);
                        i.Tax4 = BusinessUtility.GetInt(dr["Tax4"]);
                        i.TaxCalculated4 = BusinessUtility.GetDouble(dr["TaxCalculated4"]);
                        i.TaxDesc4 = BusinessUtility.GetString(dr["TaxDesc4"]);
                        i.Tax5 = BusinessUtility.GetDouble(dr["Tax5"]);
                        i.TaxCalculated5 = BusinessUtility.GetDouble(dr["TaxCalculated5"]);
                        i.TaxDesc5 = BusinessUtility.GetString(dr["TaxDesc5"]);
                        lResult.Add(i);
                    }
                    return lResult;
                }
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        /// <summary>
        /// For sivananda web interface
        /// </summary>
        /// <param name="processId"></param>
        /// <param name="orderid"></param>
        public void AddProcessToOrder(int[] processId, int orderid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                foreach (var item in processId)
                {
                    SysProcessGroup procGrpItems = new SysProcessGroup();
                    procGrpItems.PopulateObject(dbHelp, item);

                    this.OrdID = orderid;
                    this.OrdItemProcCode = procGrpItems.ProcessCode;
                    this.OrdItemProcFixedPrice = procGrpItems.ProcessFixedCost;
                    this.OrdItemProcHours = 0;
                    this.OrdItemProcPricePerHour = 0.0D;
                    this.OrdItemProcPricePerUnit = 0.0D;
                    this.OrdItemProcUnits = 0;
                    this.SysTaxCodeDescID = 0;
                  
                    this.Insert(dbHelp);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }            
        }

        public int GetOrderProcessItemID(DbHelper dbHelp, string procCode, int orderID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                object val = dbHelp.GetValue("SELECT orderItemProcID FROM orderitemprocess WHERE ordItemProcCode=@ProcCode AND ordID=@OrderID", CommandType.Text,
                    new MySqlParameter[] { 
                        DbUtility.GetParameter("ProcCode", procCode, MyDbType.String),
                        DbUtility.GetParameter("OrderID", orderID, MyDbType.Int)
                    });
                return BusinessUtility.GetInt(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }

        //Added by mukesh 20130524
        public double GetOrderProcessInternalCost(DbHelper dbHelp, string procCode, int orderID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                object val = dbHelp.GetValue("SELECT ordItemProcInternalCost FROM orderitemprocess WHERE ordItemProcCode=@ProcCode AND ordID=@OrderID", CommandType.Text,
                    new MySqlParameter[] { 
                        DbUtility.GetParameter("ProcCode", procCode, MyDbType.String),
                        DbUtility.GetParameter("OrderID", orderID, MyDbType.Int)
                    });
                return BusinessUtility.GetDouble(val);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        //Added end

        //Added by mukesh 20130606
        public void UpdateOrderProcessInternalCost(DbHelper dbHelp, string procCode, int orderID, double internalcost)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }

            try
            {
                string sqlUpdate = "UPDATE orderitemprocess SET ordItemProcInternalCost=@ordItemProcInternalCost WHERE ordItemProcCode=@ProcCode AND ordID=@OrderID";
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                     DbUtility.GetParameter("ProcCode", procCode, MyDbType.String),
                        DbUtility.GetParameter("OrderID", orderID, MyDbType.Int),
                         DbUtility.GetParameter("ordItemProcInternalCost", internalcost, MyDbType.Double)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
        //Added end

        public void UpdateProcessFixedCost(DbHelper dbHelp, double fixedCost, int ordProcessItemID)
        {
            bool mustClose = false;
            if (dbHelp == null)
            {
                mustClose = true;
                dbHelp = new DbHelper(true);
            }
            try
            {
                string sqlUpdate = "UPDATE orderitemprocess SET ordItemProcFixedPrice=@FixedProice WHERE orderItemProcID=@OrderProcessItemID";
                dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("FixedProice", fixedCost, MyDbType.Double),
                    DbUtility.GetParameter("OrderProcessItemID", ordProcessItemID, MyDbType.Int)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                if (mustClose) dbHelp.CloseDatabaseConnection();
            }
        }
    }
}

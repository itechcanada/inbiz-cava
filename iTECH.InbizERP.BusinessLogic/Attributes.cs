﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web.UI.WebControls;
using System.Web;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Attributes
    {
        private int _AttributeID;

        public int AttributeID
        {
            get { return _AttributeID; }
            set { _AttributeID = value; }
        }
        private string _AttributeDescEn;

        public string AttributeDescEn
        {
            get { return _AttributeDescEn; }
            set { _AttributeDescEn = value; }
        }
        private string _AttributeDescFr;

        public string AttributeDescFr
        {
            get { return _AttributeDescFr; }
            set { _AttributeDescFr = value; }
        }
        private string _AttributeImage;

        public string AttributeImage
        {
            get { return _AttributeImage; }
            set { _AttributeImage = value; }
        }
        private bool _IsActive;

        public bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }

        public void Insert() {
            string sql = "INSERT INTO z_attributes (AttributeDescEn,AttributeDescFr,AttributeImage,IsActive) VALUES (@AttributeDescEn,@AttributeDescFr,@AttributeImage,@IsActive)";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object id = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("AttributeDescEn", this.AttributeDescEn, MyDbType.String),
                    DbUtility.GetParameter("AttributeDescFr", this.AttributeDescFr, MyDbType.String),
                    DbUtility.GetParameter("AttributeImage", this.AttributeImage, MyDbType.String),
                    DbUtility.GetParameter("IsActive", this.IsActive, MyDbType.Boolean)
                });
                this.AttributeID = BusinessUtility.GetInt(id);
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update() {
            string sql = "UPDATE z_attributes SET AttributeDescEn=@AttributeDescEn, AttributeDescFr=@AttributeDescFr, AttributeImage=@AttributeImage, IsActive=@IsActive WHERE AttributeID=@AttributeID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("AttributeDescEn", this.AttributeDescEn, MyDbType.String),
                    DbUtility.GetParameter("AttributeDescFr", this.AttributeDescFr, MyDbType.String),
                    DbUtility.GetParameter("AttributeImage", this.AttributeImage, MyDbType.String),
                    DbUtility.GetParameter("IsActive", this.IsActive, MyDbType.Boolean),
                    DbUtility.GetParameter("AttributeID", this.AttributeID, MyDbType.Int)
                });                
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void PopulateObject(int attrID) {
            string sql = "SELECT * FROM z_attributes WHERE AttributeID=@AttributeID";
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("AttributeID", attrID, MyDbType.Int) });
                if (dr.Read()) {
                    this.AttributeDescEn = BusinessUtility.GetString(dr["AttributeDescEn"]);
                    this.AttributeDescFr = BusinessUtility.GetString(dr["AttributeDescFr"]);
                    this.AttributeID = BusinessUtility.GetInt(dr["AttributeID"]);
                    this.AttributeImage = BusinessUtility.GetString(dr["AttributeImage"]);
                }
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public string GetSql(ParameterCollection pCol, string searchText, string lang) {
            pCol.Clear();
            string sql = "SELECT AttributeID," + Globals.GetFieldName("AttributeDesc", lang) + " AS AttributeName,AttributeImage,IsActive FROM z_attributes ";
            if (!string.IsNullOrEmpty(searchText)) {
                sql += " WHERE AttributeDescEn LIKE CONCAT(@Search, '%') OR AttributeDescFr LIKE CONCAT(@Search, '%')";
                pCol.Add("@Search", searchText);
            }
            return sql;
        }

        public void Delete(int attrID) {
            string sql = "DELETE FROM z_attributes WHERE AttributeID=@AttributeID";
            DbHelper dbHelp = new DbHelper();            
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { DbUtility.GetParameter("AttributeID", attrID, MyDbType.Int) });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }        
    }
}

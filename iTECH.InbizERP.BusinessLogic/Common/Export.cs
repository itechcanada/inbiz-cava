﻿//using System;
//using System.Data;
//using System.Web;
//using System.Web.SessionState;
//using System.IO;
//using System.Text;
//using System.Xml;
//using System.Xml.Xsl;
//using System.Threading;

//namespace iTECH.InbizERP.BusinessLogic
//{
//    /// <summary>
//    /// Exports datatable to CSV or Excel format.
//    /// This uses DataSet's XML features and XSLT for exporting.    
//    /// </summary>
//    public class Export
//    {
//        public enum ExportFormat : int { CSV = 1, Excel = 2 }; // Export format enumeration			        

//        #region Public Methods ExportDetails


//        /// <summary>
//        /// Method to get all the column headers in the datatable and
//        /// exorts in CSV / Excel format with all columns
//        /// </summary>
//        /// <param name="detailsTable"></param>
//        /// <param name="formatType"></param>
//        /// <param name="fileName"></param>
//        public static void ExportDetails(DataTable detailsTable, ExportFormat formatType, string fileName)
//        {
//            try
//            {
//                if (detailsTable.Rows.Count == 0)
//                    throw new Exception("There are no details to export.");

//                // Create Dataset
//                DataSet dsExport = new DataSet("Export");
//                DataTable dtExport = detailsTable.Copy();

//                //Strip html tags if any
//                foreach (DataRow r in dtExport.Rows)
//                {
//                    for (int i = 0; i < r.ItemArray.Length; i++)
//                    {
//                        r[i] = iTECH.Library.Utilities.BusinessUtility.StripHtml(iTECH.Library.Utilities.BusinessUtility.GetString(r[i]));
//                    }
//                }

//                dtExport.TableName = "Values";
//                dsExport.Tables.Add(dtExport);

//                // Getting Field Names
//                string[] sHeaders = new string[dtExport.Columns.Count];
//                string[] sFileds = new string[dtExport.Columns.Count];

//                for (int i = 0; i < dtExport.Columns.Count; i++)
//                {
//                    //sHeaders[i] = ReplaceSpclChars(dtExport.Columns[i].ColumnName);
//                    sHeaders[i] = dtExport.Columns[i].ColumnName;
//                    sFileds[i] = ReplaceSpclChars(dtExport.Columns[i].ColumnName);
//                }

//                ExportToFile(dsExport, sHeaders, sFileds, formatType, fileName);
//            }
//            catch (Exception Ex)
//            {
//                throw Ex;
//            }
//        }



//        /// <summary>
//        /// Method to get the specified column headers in the datatable and
//        ///	exorts in CSV / Excel format with specified columns
//        /// </summary>
//        /// <param name="detailsTable"></param>
//        /// <param name="columnList"></param>
//        /// <param name="formatType"></param>
//        /// <param name="fileName"></param>
//        public static void ExportDetails(DataTable detailsTable, int[] columnList, ExportFormat formatType, string fileName)
//        {
//            try
//            {
//                if (detailsTable.Rows.Count == 0)
//                    throw new Exception("There are no details to export");

//                // Create Dataset
//                DataSet dsExport = new DataSet("Export");
//                DataTable dtExport = detailsTable.Copy();
//                dtExport.TableName = "Values";
//                dsExport.Tables.Add(dtExport);

//                if (columnList.Length > dtExport.Columns.Count)
//                    throw new Exception("ExportColumn List should not exceed Total Columns");

//                // Getting Field Names
//                string[] sHeaders = new string[columnList.Length];
//                string[] sFileds = new string[columnList.Length];

//                for (int i = 0; i < columnList.Length; i++)
//                {
//                    if ((columnList[i] < 0) || (columnList[i] >= dtExport.Columns.Count))
//                        throw new Exception("ExportColumn Number should not exceed Total Columns Range");

//                    sHeaders[i] = dtExport.Columns[columnList[i]].ColumnName;
//                    sFileds[i] = ReplaceSpclChars(dtExport.Columns[columnList[i]].ColumnName);
//                }

//                ExportToFile(dsExport, sHeaders, sFileds, formatType, fileName);
//            }
//            catch (Exception Ex)
//            {
//                throw Ex;
//            }
//        }



//        /// <summary>
//        /// To get the specified column headers in the datatable and	
//        /// exorts in CSV / Excel format with specified columns and 
//        ///	with specified headers
//        /// </summary>
//        /// <param name="detailsTable"></param>
//        /// <param name="columnList"></param>
//        /// <param name="headers"></param>
//        /// <param name="formatType"></param>
//        /// <param name="fileName"></param>
//        public static void ExportDetails(DataTable detailsTable, int[] columnList, string[] headers, ExportFormat formatType, string fileName)
//        {
//            try
//            {
//                if (detailsTable.Rows.Count == 0)
//                    throw new Exception("There are no details to export");

//                // Create Dataset
//                DataSet dsExport = new DataSet("Export");
//                DataTable dtExport = detailsTable.Copy();
//                dtExport.TableName = "Values";
//                dsExport.Tables.Add(dtExport);

//                if (columnList.Length != headers.Length)
//                    throw new Exception("ExportColumn List and Headers List should be of same length");
//                else if (columnList.Length > dtExport.Columns.Count || headers.Length > dtExport.Columns.Count)
//                    throw new Exception("ExportColumn List should not exceed Total Columns");

//                // Getting Field Names
//                string[] sFileds = new string[columnList.Length];

//                for (int i = 0; i < columnList.Length; i++)
//                {
//                    if ((columnList[i] < 0) || (columnList[i] >= dtExport.Columns.Count))
//                        throw new Exception("ExportColumn Number should not exceed Total Columns Range");

//                    sFileds[i] = ReplaceSpclChars(dtExport.Columns[columnList[i]].ColumnName);
//                }

//                ExportToFile(dsExport, headers, sFileds, formatType, fileName);
//            }
//            catch (Exception Ex)
//            {
//                throw Ex;
//            }
//        }

//        #endregion // ExportDetails

//        #region Private Methods

//        /// <summary>
//        /// Method to Exports dataset into CSV / Excel format
//        /// </summary>
//        /// <param name="dsExport"></param>
//        /// <param name="sHeaders"></param>
//        /// <param name="sFileds"></param>
//        /// <param name="formatType"></param>
//        /// <param name="fileName"></param>
//        private static void ExportToFile(DataSet dsExport, string[] sHeaders, string[] sFileds, ExportFormat formatType, string fileName)
//        {
//            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;

//            try
//            {
//                // Appending Headers
//                response.Clear();
//                response.Buffer = true;

//                if (formatType == ExportFormat.CSV)
//                {
//                    response.ContentType = "text/csv";
//                    response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
//                }
//                else
//                {
//                    response.ContentType = "application/vnd.ms-excel";
//                    response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
//                }

//                // XSLT to use for transforming this dataset.						
//                MemoryStream stream = new MemoryStream();
//                XmlTextWriter writer = new XmlTextWriter(stream, Encoding.UTF8);

//                CreateStylesheet(writer, sHeaders, sFileds, formatType);
//                writer.Flush();
//                stream.Seek(0, SeekOrigin.Begin);

//                XmlDataDocument xmlDoc = new XmlDataDocument(dsExport);
//                //dsExport.WriteXml("Data.xml");
//                XslTransform xslTran = new XslTransform();
//                xslTran.Load(new XmlTextReader(stream), null, null);

//                System.IO.StringWriter sw = new System.IO.StringWriter();
//                xslTran.Transform(xmlDoc, null, sw, null);
//                //xslTran.Transform(System.Web.HttpContext.Current.Server.MapPath("Data.xml"), null, sw, null);

//                //Writeout the Content				
//                response.Write(sw.ToString());
//                sw.Close();
//                writer.Close();
//                stream.Close();
//                response.End();
//            }
//            catch (ThreadAbortException Ex)
//            {
//                string ErrMsg = Ex.Message;
//            }
//            catch (Exception Ex)
//            {
//                throw Ex;
//            }
//        }


//        /// <summary>
//        /// Method to replaces special characters with XML codes
//        /// </summary>
//        /// <param name="stdData"></param>
//        /// <returns></returns>
//        private static string ReplaceSpclChars(string stdData)
//        {
//            //			space 	-> 	_x0020_
//            //			%		-> 	_x0025_
//            //			#		->	_x0023_
//            //			&		->	_x0026_
//            //			/		->	_x002F_

//            stdData = stdData.Replace(" ", "_x0020_");
//            stdData = stdData.Replace("%", "_x0025_");
//            stdData = stdData.Replace("#", "_x0023_");
//            stdData = stdData.Replace("&", "_x0026_");
//            stdData = stdData.Replace("/", "_x002F_");
//            return stdData;
//        }

//        /// <summary>
//        /// Method to WriteStylesheet which creates XSLT file to apply on dataset's XML file 
//        /// </summary>
//        /// <param name="writer"></param>
//        /// <param name="sHeaders"></param>
//        /// <param name="sFileds"></param>
//        /// <param name="formatType"></param>
//        private static void CreateStylesheet(XmlTextWriter writer, string[] sHeaders, string[] sFileds, ExportFormat formatType)
//        {
//            try
//            {
//                // xsl:stylesheet
//                string ns = "http://www.w3.org/1999/XSL/Transform";
//                writer.Formatting = Formatting.Indented;
//                //Start Document
//                writer.WriteStartDocument();

//                //<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
//                writer.WriteStartElement("xsl", "stylesheet", ns);
//                writer.WriteAttributeString("version", "1.0");

//                //<xsl:output method="text" version="4.0" />
//                writer.WriteStartElement("xsl:output");
//                writer.WriteAttributeString("method", "text");
//                writer.WriteAttributeString("version", "4.0");
//                writer.WriteEndElement();//xsl:output

//                //The double quote as a variable, to avoid quoting confusion.
//                //<xsl:variable name="double-quote">"</xsl:variable>
//                writer.WriteStartElement("xsl:variable");
//                writer.WriteAttributeString("name", "double-quote");
//                writer.WriteString("\"");
//                writer.WriteEndElement();//xsl:variable

//                //<xsl:template match="/">
//                writer.WriteStartElement("xsl:template");
//                writer.WriteAttributeString("match", "/");

//                // xsl:value-of for headers
//                for (int i = 0; i < sHeaders.Length; i++)
//                {
//                    writer.WriteString("\"");
//                    //<xsl:value-of select="$val"/>
//                    writer.WriteStartElement("xsl:value-of");
//                    writer.WriteAttributeString("select", "'" + sHeaders[i] + "'");
//                    writer.WriteEndElement(); // xsl:value-of
//                    writer.WriteString("\"");
//                    if (i != sFileds.Length - 1) writer.WriteString((formatType == ExportFormat.CSV) ? "," : "	");
//                }

//                //<xsl:for-each select="Export/Values">
//                writer.WriteStartElement("xsl:for-each");
//                writer.WriteAttributeString("select", "Export/Values");
//                writer.WriteString("\r\n");

//                // xsl:value-of for data fields
//                for (int i = 0; i < sFileds.Length; i++)
//                {
//                    writer.WriteString("\"");
//                    //<xsl:value-of select="$val"/>
//                    //writer.WriteStartElement("xsl:value-of");
//                    //writer.WriteAttributeString("select", sFileds[i]);
//                    //writer.WriteEndElement(); // xsl:value-of

//                    //<xsl:call-template name="escape-quotes">
//                    //<xsl:with-param name="input" select="$text"/>
//                    //</xsl:call-template>
//                    writer.WriteStartElement("xsl:call-template");
//                    writer.WriteAttributeString("name", "escape-quotes");
//                    writer.WriteStartElement("xsl:with-param");
//                    writer.WriteAttributeString("name", "input");
//                    writer.WriteAttributeString("select", sFileds[i]);
//                    writer.WriteEndElement();
//                    writer.WriteEndElement();

//                    writer.WriteString("\"");
//                    if (i != sFileds.Length - 1) writer.WriteString((formatType == ExportFormat.CSV) ? "," : "	");
//                }

//                writer.WriteEndElement(); //</xsl:for-each>
//                writer.WriteEndElement(); //</xsl-template>

//                //Recursive template to manage double quotes in string
//                //<xsl:template name="escape-quotes">
//                writer.WriteStartElement("xsl:template");
//                writer.WriteAttributeString("name", "escape-quotes");

//                //<xsl:param name="input"/>
//                writer.WriteStartElement("xsl:param");
//                writer.WriteAttributeString("name", "input");
//                writer.WriteEndElement();//xsl:param

//                //<xsl:choose>
//                writer.WriteStartElement("xsl:choose");

//                //<xsl:when test="contains($input, $double-quote)">
//                writer.WriteStartElement("xsl:when");
//                writer.WriteAttributeString("test", "contains($input, $double-quote)");

//                //<xsl:value-of select="substring-before($input, $double-quote)"/>
//                writer.WriteStartElement("xsl:value-of");
//                writer.WriteAttributeString("select", "substring-before($input, $double-quote)");
//                writer.WriteEndElement();//xsl:value-of 

//                //<xsl:value-of select="$double-quote"/>
//                writer.WriteStartElement("xsl:value-of");
//                writer.WriteAttributeString("select", "$double-quote");
//                writer.WriteEndElement();//xsl:value-of 

//                //<xsl:value-of select="$double-quote"/>
//                writer.WriteStartElement("xsl:value-of");
//                writer.WriteAttributeString("select", "$double-quote");
//                writer.WriteEndElement();//xsl:value-of

//                //<xsl:call-template name="escape-quotes">
//                writer.WriteStartElement("xsl:call-template");
//                writer.WriteAttributeString("name", "escape-quotes");

//                //<xsl:with-param name="input" select="substring-after($input, $double-quote)"/>
//                writer.WriteStartElement("xsl:with-param");
//                writer.WriteAttributeString("name", "input");
//                writer.WriteAttributeString("select", "substring-after($input, $double-quote)");
//                writer.WriteEndElement(); //xsl:with-param

//                writer.WriteEndElement(); //</xsl:call-template>

//                writer.WriteEndElement();//</xsl:when>

//                //<xsl:otherwise>
//                writer.WriteStartElement("xsl:otherwise");

//                //<xsl:value-of select="$input"/>
//                writer.WriteStartElement("xsl:value-of");
//                writer.WriteAttributeString("select", "$input");
//                writer.WriteEndElement();//xsl:value-of

//                writer.WriteEndElement();//</xsl:otherwise>

//                writer.WriteEndElement();//</xsl:choose>

//                writer.WriteEndElement();//</xsl:template>

//                writer.WriteEndElement(); // </xsl:stylesheet>

//                writer.WriteEndDocument(); //document
//            }
//            catch (Exception Ex)
//            {
//                throw Ex;
//            }
//        }

//        #endregion

//    } 
//}


﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Web;

//using System.Data;

//using iTECH.Library.Utilities;
//using iTECH.Library.DataAccess.MySql;

//namespace iTECH.InbizERP.BusinessLogic
//{
//    #region Carts Entity
//    public class SalesCartItem
//    {
//        public int ID { get; set; }
//        public string UPCCode { get; set; }
//        public string ProductName { get; set; }
//        public double Quantity { get; set; }
//        public double Price { get; set; }
//        public double RangePrice { get; set; }
//        public string WarehouseCode { get; set; }
//        public int TaxGrp { get; set; }
//        public string TaxGrpDesc { get; set; }
//        public int Discount { get; set; }
//        public string DisType { get; set; }
//        public double ItemTotal { get; set; }
//        public int ProductID { get; set; }
//        public int GuestID { get; set; }
//        public int ReservationID { get; set; }
//        public int ReservationItemID { get; set; }
//        public int OrderItemID { get; set; }
//        public int OrderID { get; set; }
//        public string ItemGuestName { get; set; }
//        public bool IsCanceled { get; set; }
//    }

//    public class SalesProcessCartItem
//    {
//        public int ProcessID { get; set; }
//        public string ProcessCode { get; set; }
//        public int TaxGrpID { get; set; }
//        public string TaxGrpName { get; set; }
//        public string ProcessDescription { get; set; }
//        public double ProcessFixedCost { get; set; }
//        public double ProcessCostPerHour { get; set; }
//        public double ProcessCostPerUnit { get; set; }
//        public int TotalHour { get; set; }
//        public int TotalUnit { get; set; }
//        public double ProcessTotal { get; set; }
//        public int ProcessItemID { get; set; }
//        public int OrderID { get; set; }
//    } 
//    #endregion

//    public class SalesCartHelper
//    {
//        #region Public Properties
//        public DataTable CurrentItemCart
//        {
//            get
//            {
//                return (DataTable)HttpContext.Current.Session["SalesCart"];
//            }
//        }

//        public DataTable CurrentProcessItemCart
//        {
//            get
//            {
//                return (DataTable)HttpContext.Current.Session["SalesProcessCart"];
//            }
//        }

//        public double OrderDiscount
//        {
//            get
//            {
//                DataTable dt = this.CurrentItemCart;
//                if (dt.Rows.Count > 0)
//                {
//                    return BusinessUtility.GetDouble(dt.Rows[0]["OrdDiscount"]);
//                }
//                return 0.0D;
//            }
//        }

//        public string OrderDiscountType
//        {
//            get
//            {
//                DataTable dt = this.CurrentItemCart;
//                if (dt.Rows.Count > 0)
//                {
//                    return BusinessUtility.GetString(dt.Rows[0]["OrdDiscountType"]);
//                }
//                return "P";
//            }
//        } 
//        #endregion

//        #region Constructor
//        public SalesCartHelper()
//        {
//            if (HttpContext.Current.Session["SalesCart"] == null)
//            {
//                DataTable cart = new DataTable("Cart");
//                DataColumn idCol = new DataColumn("ID", typeof(int));
//                idCol.AutoIncrement = true;
//                idCol.AutoIncrementSeed = 1;
//                idCol.AutoIncrementStep = 1;

//                DataColumn[] pCol = { idCol };
//                cart.Columns.Add(pCol[0]);
//                cart.Columns.Add("UPCCode", typeof(string));
//                cart.Columns.Add("ProductName", typeof(string));
//                cart.Columns.Add("Quantity", typeof(double));
//                cart.Columns.Add("Price", typeof(double));
//                cart.Columns.Add("RangePrice", typeof(double));
//                cart.Columns.Add("WarehouseCode", typeof(string));
//                cart.Columns.Add("TaxGrp", typeof(int));
//                cart.Columns.Add("TaxGrpDesc", typeof(string));
//                cart.Columns.Add("Discount", typeof(int));
//                cart.Columns.Add("DisType", typeof(string));
//                cart.Columns.Add("ItemTotal", typeof(double));
//                cart.Columns.Add("ProductID", typeof(int));
//                cart.Columns.Add("GuestID", typeof(int));
//                cart.Columns.Add("ReservationID", typeof(int));
//                cart.Columns.Add("ReservationItemID", typeof(int));
//                cart.Columns.Add("OrderItemID", typeof(int));
//                cart.Columns.Add("OrderID", typeof(int));
//                cart.Columns.Add("ItemGuestName", typeof(string));
//                cart.Columns.Add("IsCanceled", typeof(bool));
//                cart.Columns.Add("OrdDiscount", typeof(double)); //Will only take effect for first row if discount applied.
//                cart.Columns.Add("OrdDiscountType", typeof(string)); //Will only take effect for first row if discount applied.
//                cart.Columns.Add("OriginalPrdName", typeof(string)); //To show productname
//                cart.PrimaryKey = pCol;

//                HttpContext.Current.Session["SalesCart"] = cart;
//            }

//            if (HttpContext.Current.Session["SalesProcessCart"] == null)
//            {
//                //Process Cart
//                DataTable processCart = new DataTable("ProcessCart");
//                DataColumn pCol = new DataColumn("ProcessID", typeof(int));
//                pCol.AutoIncrement = true;
//                pCol.AutoIncrementSeed = 1;
//                pCol.AutoIncrementStep = 1;
//                DataColumn[] pColProcess = { pCol };

//                processCart.Columns.Add(pColProcess[0]);

//                processCart.Columns.Add("ProcessCode", typeof(string));
//                //Add the 2 field one is Tax Grp d and Tax Grp Name 
//                processCart.Columns.Add("TaxGrpID", typeof(int));
//                processCart.Columns.Add("TaxGrpName", typeof(string));

//                processCart.Columns.Add("ProcessDescription", typeof(string));
//                processCart.Columns.Add("ProcessFixedCost", typeof(double));
//                processCart.Columns.Add("ProcessCostPerHour", typeof(double));
//                processCart.Columns.Add("ProcessCostPerUnit", typeof(double));
//                processCart.Columns.Add("TotalHour", typeof(int));
//                processCart.Columns.Add("TotalUnit", typeof(int));
//                processCart.Columns.Add("ProcessTotal", typeof(double));
//                processCart.Columns.Add("ProcessItemID", typeof(int));
//                processCart.Columns.Add("OrderID", typeof(int));
//                processCart.PrimaryKey = pColProcess;

//                HttpContext.Current.Session["SalesProcessCart"] = processCart;
//            }
//        } 
//        #endregion

//        #region Sales Cart Specific Methods        

//        public void SetOrderDiscount(double discount, string discountType)
//        {
//            DataTable dt = this.CurrentItemCart;

//            if (dt.Rows.Count > 0)
//            {
//                DataRow dr = dt.Rows[0]; //Add Order Discount Detail on first Row    
//                dr["OrdDiscount"] = discount;
//                dr["OrdDiscountType"] = discountType;
//                HttpContext.Current.Session["SalesCart"] = dt;
//            }
//        }

//        public void AddToCart(SalesCartItem item, double exchangeRate, bool findRow)
//        {
//            DataTable dt = this.CurrentItemCart;
//            DataRow dr = null;
//            if (findRow)
//            {
//                foreach (DataRow r in dt.Rows)
//                {
//                    if (BusinessUtility.GetInt(r["ProductID"]) == item.ProductID)
//                    {
//                        dr = r;
//                        break;
//                    }
//                }
//            }

//            if (dr != null)
//            {
//                dr["TaxGrp"] = item.TaxGrp;
//                dr["TaxGrpDesc"] = item.TaxGrpDesc;
//                dr["Quantity"] = BusinessUtility.GetDouble(dr["Quantity"]) + item.Quantity;
//                dr["Price"] = item.Price;

//                int discount = item.Discount;
//                double total = exchangeRate * (BusinessUtility.GetDouble(dr["Price"]) * BusinessUtility.GetDouble(dr["Quantity"]));
//                if (discount > 0)
//                {
//                    if (item.DisType.ToUpper().Trim() == "A")
//                    {
//                        total = total - discount;
//                    }
//                    else
//                    {
//                        total = total - Math.Round((total * ((double)discount / 100)), 2);
//                    }
//                }
//                dr["ItemTotal"] = total;

//                //Update OrderItem In Database 
//                if (item.OrderItemID > 0)
//                {
//                    OrderItems oi = new OrderItems();
//                    oi.OrderItemID = BusinessUtility.GetInt(dr["OrderItemID"]);
//                    oi.OrderItemDesc = BusinessUtility.GetString(dr["ProductName"]);
//                    oi.OrdProductDiscount = BusinessUtility.GetInt(dr["Discount"]);
//                    oi.OrdProductDiscountType = BusinessUtility.GetString(dr["DisType"]);
//                    oi.OrdProductQty = BusinessUtility.GetDouble(dr["Quantity"]);
//                    oi.OrdProductTaxGrp = BusinessUtility.GetInt(dr["TaxGrp"]);
//                    oi.OrdProductUnitPrice = BusinessUtility.GetDouble(dr["Price"]);

//                    oi.Update();
//                }
//            }
//            else
//            {
//                dr = dt.NewRow();                

//                dr["ProductID"] = item.ProductID;
//                dr["Discount"] = item.Discount;
//                dr["Price"] = item.Price;
//                dr["ProductName"] = item.ProductName;
//                dr["Quantity"] = item.Quantity;
//                dr["UPCCode"] = item.UPCCode;
//                dr["TaxGrp"] = item.TaxGrp;
//                dr["TaxGrpDesc"] = item.TaxGrpDesc;
//                dr["WarehouseCode"] = item.WarehouseCode;
//                dr["DisType"] = item.DisType;

//                int discount = item.Discount;
//                double total = exchangeRate * (BusinessUtility.GetDouble(dr["Price"]) * BusinessUtility.GetDouble(dr["Quantity"]));
//                if (discount > 0)
//                {
//                    if (item.DisType.ToUpper().Trim() == "A")
//                    {
//                        total = total - discount;
//                    }
//                    else
//                    {
//                        total = total - Math.Round((total * ((double)discount / 100)), 2);
//                    }
//                }
//                dr["ItemTotal"] = total;
//                dt.Rows.Add(dr);

//                //Add Order Item To Database                
//                if (item.OrderID > 0)
//                {
//                    OrderItems oi = new OrderItems();
//                    oi.OrderItemDesc = BusinessUtility.GetString(dr["ProductName"]);
//                    oi.OrdProductDiscount = BusinessUtility.GetInt(dr["Discount"]);
//                    oi.OrdProductDiscountType = BusinessUtility.GetString(dr["DisType"]);
//                    oi.OrdProductQty = BusinessUtility.GetDouble(dr["Quantity"]);
//                    oi.OrdProductTaxGrp = BusinessUtility.GetInt(dr["TaxGrp"]);
//                    oi.OrdProductUnitPrice = BusinessUtility.GetDouble(dr["Price"]);
//                    oi.OrdID = item.OrderID;
//                    oi.OrdIsItemCanceled = false;
//                    oi.OrdProductID = item.ProductID;

//                    oi.Insert(item.WarehouseCode, true);
//                }
//            }

//            HttpContext.Current.Session["SalesCart"] = dt;
//        }

//        public void EditCartItem(SalesCartItem item, double exchangeRate)
//        {
//            DataTable dt = this.CurrentItemCart;
//            DataRow dr = dt.Rows.Find(item.ID);
//            if (dr != null)
//            {
//                dr["ProductID"] = item.ProductID;
//                dr["Discount"] = item.Discount;
//                dr["Price"] = item.Price;
//                dr["ProductName"] = item.ProductName;
//                dr["Quantity"] = item.Quantity;
//                dr["UPCCode"] = item.UPCCode;
//                dr["TaxGrp"] = item.TaxGrp;
//                dr["TaxGrpDesc"] = item.TaxGrpDesc;
//                dr["WarehouseCode"] = item.WarehouseCode;
//                dr["DisType"] = item.DisType;
//                int discount = BusinessUtility.GetInt(dr["Discount"]);
//                double total = exchangeRate * (BusinessUtility.GetDouble(dr["Price"]) * BusinessUtility.GetDouble(dr["Quantity"]));
//                if (discount > 0)
//                {
//                    if (item.DisType.ToUpper().Trim() == "A")
//                    {
//                        total = total - discount;
//                    }
//                    else
//                    {
//                        total = total - Math.Round((total * ((double)discount / 100)), 2);
//                    }
//                }
//                dr["ItemTotal"] = total;

//                //Update OrderItem In Database                 
//                if (item.OrderItemID > 0)
//                {
//                    OrderItems oi = new OrderItems();
//                    oi.OrderItemID = BusinessUtility.GetInt(dr["OrderItemID"]);
//                    oi.OrderItemDesc = BusinessUtility.GetString(dr["ProductName"]);
//                    oi.OrdProductDiscount = BusinessUtility.GetInt(dr["Discount"]);
//                    oi.OrdProductDiscountType = BusinessUtility.GetString(dr["DisType"]);
//                    oi.OrdProductQty = BusinessUtility.GetDouble(dr["Quantity"]);
//                    oi.OrdProductTaxGrp = BusinessUtility.GetInt(dr["TaxGrp"]);
//                    oi.OrdProductUnitPrice = BusinessUtility.GetDouble(dr["Price"]);

//                    oi.Update();
//                }
//            }

//            HttpContext.Current.Session["SalesCart"] = dt;
//        }

//        public void DeleteCartItem(int id)
//        {
//            DataTable cart = this.CurrentItemCart;
//            DataRow rw = cart.Rows.Find(id);
//            if (rw != null)
//            {
//                //First Need To Delete From Database if It exist in database 
//                if (BusinessUtility.GetInt(rw["OrderItemID"]) > 0)
//                {
//                    OrderItems oi = new OrderItems();
//                    oi.Delete(BusinessUtility.GetInt(rw["OrderItemID"]));
//                }

//                cart.Rows.Remove(rw);
//            }
//            HttpContext.Current.Session["SalesCart"] = cart;
//        }

//        private void BindItemCart(DbHelper dbHelp, int orderID)
//        {
//            OrderItems ordItems = new OrderItems();
//            DataTable dtCart = this.CurrentItemCart;
//            IDataReader r = null;
//            try
//            {
//                r = ordItems.GetOrderItemsReader(dbHelp, orderID);
//                while (r.Read())
//                {
//                    DataRow dr = dtCart.NewRow();
//                    dr["ProductID"] = r["ordProductID"];
//                    dr["UPCCode"] = r["prdUPCCode"];
//                    dr["ProductName"] = r["prdName"];
//                    dr["Quantity"] = r["ordProductQty"];
//                    dr["Price"] = r["InOrdProductUnitPrice"];
//                    dr["RangePrice"] = 0;
//                    dr["WarehouseCode"] = r["ordShpWhsCode"];
//                    dr["TaxGrp"] = r["ordProductTaxGrp"];
//                    dr["TaxGrpDesc"] = r["sysTaxCodeDescText"];
//                    dr["Discount"] = r["ordProductDiscount"];
//                    dr["DisType"] = r["ordProductDiscountType"];
//                    dr["ItemTotal"] = r["amount"];
//                    dr["OrderItemID"] = r["orderItemID"];
//                    dr["GuestID"] = r["ordGuestID"];
//                    dr["ReservationID"] = r["ReservationID"];
//                    dr["ReservationItemID"] = r["ReservationItemID"];
//                    dr["OrderID"] = r["ordID"];
//                    dr["IsCanceled"] = r["ordIsItemCanceled"];
//                    dr["OrdDiscount"] = r["ordDiscount"];
//                    dr["OrdDiscountType"] = r["ordDiscountType"];
//                    dr["OriginalPrdName"] = r["OriginalPrdName"];
//                    int guestID = BusinessUtility.GetInt(dr["GuestID"]);
//                    if (guestID > 0)
//                    {
//                        Partners gst = new Partners();
//                        gst.PopulateObject(guestID);
//                        dr["ItemGuestName"] = gst.PartnerLongName;
//                    }
//                    dtCart.Rows.Add(dr);
//                }
//            }
//            catch
//            {
//                throw;
//            }
//            finally
//            {
//                if (r != null && !r.IsClosed)
//                {
//                    r.Close();
//                    r.Dispose();
//                }
//            }

//            HttpContext.Current.Session["SalesCart"] = dtCart;
//        }

//        public SalesCartItem GetItem(int id)
//        {
//            SalesCartItem item = new SalesCartItem();
//            DataRow dr = this.CurrentItemCart.Rows.Find(id);
//            if (dr != null)
//            {
//                item.Discount = BusinessUtility.GetInt(dr["Discount"]);
//                item.DisType = BusinessUtility.GetString(dr["DisType"]);
//                item.GuestID = BusinessUtility.GetInt(dr["GuestID"]);
//                item.ID = BusinessUtility.GetInt(dr["ID"]);
//                item.IsCanceled = BusinessUtility.GetBool(dr["IsCanceled"]);
//                item.ItemGuestName = BusinessUtility.GetString(dr["ItemGuestName"]);
//                item.ItemTotal = BusinessUtility.GetDouble(dr["ItemTotal"]);
//                item.OrderID = BusinessUtility.GetInt(dr["OrderID"]);
//                item.OrderItemID = BusinessUtility.GetInt(dr["OrderItemID"]);
//                item.Price = BusinessUtility.GetDouble(dr["Price"]);
//                item.ProductID = BusinessUtility.GetInt(dr["ProductID"]);
//                item.ProductName = BusinessUtility.GetString(dr["ProductName"]);
//                item.Quantity = BusinessUtility.GetDouble(dr["Quantity"]);
//                item.RangePrice = BusinessUtility.GetDouble(dr["RangePrice"]);
//                item.ReservationID = BusinessUtility.GetInt(dr["ReservationID"]);
//                item.ReservationItemID = BusinessUtility.GetInt(dr["ReservationItemID"]);
//                item.TaxGrp = BusinessUtility.GetInt(dr["TaxGrp"]);
//                item.TaxGrpDesc = BusinessUtility.GetString(dr["TaxGrpDesc"]);
//                item.UPCCode = BusinessUtility.GetString(dr["UPCCode"]);
//                item.WarehouseCode = BusinessUtility.GetString(dr["WarehouseCode"]);
//                return item;
//            }
//            return null;
//        }

//        public List<OrderItems> GetOrderItemsFromCart(int orderIdToSet)
//        {
//            List<OrderItems> ordItems = new List<OrderItems>();
//            foreach (DataRow dr in this.CurrentItemCart.Rows)
//            {
//                OrderItems oi = new OrderItems();
//                oi.OrderItemDesc = BusinessUtility.GetString(dr["ProductName"]);
//                oi.OrdID = orderIdToSet;
//                oi.OrdProductDiscount = BusinessUtility.GetInt(dr["Discount"]);
//                oi.OrdProductDiscountType = BusinessUtility.GetString(dr["DisType"]);
//                oi.OrdProductID = BusinessUtility.GetInt(dr["ProductID"]);
//                oi.OrdProductQty = BusinessUtility.GetDouble(dr["Quantity"]);
//                oi.OrdProductTaxGrp = BusinessUtility.GetInt(dr["TaxGrp"]);
//                oi.OrdProductUnitPrice = BusinessUtility.GetDouble(dr["Price"]);

//                ordItems.Add(oi);
//            }
//            return ordItems;
//        }
//        #endregion

//        #region Process Cart Specific Methods
//        private void BindProcessCart(DbHelper dbHelp, int orderID)
//        {
//            OrderItemProcess ordProcessItem = new OrderItemProcess();
//            DataTable dtProcess = this.CurrentProcessItemCart;
//            IDataReader r = null;

//            try
//            {
//                r = ordProcessItem.GetOrderProcessItemsReader(dbHelp, orderID);
//                while (r.Read())
//                {
//                    DataRow dr = dtProcess.NewRow();

//                    dr["ProcessCode"] = r["ordItemProcCode"];
//                    dr["TaxGrpID"] = r["sysTaxCodeDescID"];
//                    dr["TaxGrpName"] = r["sysTaxCodeDescText"];
//                    dr["ProcessDescription"] = r["ProcessDescription"];
//                    dr["ProcessFixedCost"] = r["InOrdItemProcFixedPrice"];
//                    dr["ProcessCostPerHour"] = r["InOrdItemProcPricePerHour"];
//                    dr["ProcessCostPerUnit"] = r["InOrdItemProcPricePerUnit"];
//                    dr["TotalHour"] = r["ordItemProcHours"];
//                    dr["TotalUnit"] = r["ordItemProcUnits"];
//                    dr["ProcessTotal"] = r["ProcessCost"];
//                    dr["ProcessItemID"] = r["orderItemProcID"];
//                    dr["OrderID"] = r["ordID"];

//                    dtProcess.Rows.Add(dr);
//                }
//            }
//            catch
//            {
//                throw;
//            }
//            finally
//            {
//                if (r != null && !r.IsClosed)
//                {
//                    r.Close();
//                    r.Dispose();
//                }
//            }

//            HttpContext.Current.Session["SalesProcessCart"] = dtProcess;
//        }

//        public void EditToProcessCart(SalesProcessCartItem item, double exchangeRate)
//        {
//            DataTable dt = this.CurrentProcessItemCart;
//            DataRow row = dt.Rows.Find(item.ProcessID);
//            if (row != null)
//            {
//                row["ProcessCode"] = item.ProcessCode;
//                row["TaxGrpID"] = item.TaxGrpID;
//                row["TaxGrpName"] = item.TaxGrpName;
//                row["ProcessDescription"] = item.ProcessDescription;
//                row["ProcessFixedCost"] = item.ProcessFixedCost;
//                row["ProcessCostPerHour"] = item.ProcessCostPerHour;
//                row["ProcessCostPerUnit"] = item.ProcessCostPerUnit;
//                row["TotalHour"] = item.TotalHour;
//                row["TotalUnit"] = item.TotalUnit;
//                row["ProcessTotal"] = exchangeRate * (BusinessUtility.GetDouble(row["ProcessFixedCost"]) + (BusinessUtility.GetDouble(row["ProcessCostPerHour"]) * BusinessUtility.GetDouble(row["TotalHour"])) + (BusinessUtility.GetDouble(row["ProcessCostPerUnit"]) * BusinessUtility.GetDouble(row["TotalUnit"])));

//                //To Do to save in db
//                if (item.ProcessItemID > 0)
//                {
//                    OrderItemProcess proce = new OrderItemProcess();
//                    proce.OrdID = item.OrderID;
//                    proce.OrdItemProcCode = item.ProcessCode;
//                    proce.OrdItemProcFixedPrice = item.ProcessFixedCost;
//                    proce.OrdItemProcHours = item.TotalHour;
//                    proce.OrdItemProcPricePerHour = item.ProcessCostPerHour;
//                    proce.OrdItemProcPricePerUnit = item.ProcessCostPerUnit;
//                    proce.OrdItemProcUnits = item.TotalUnit;
//                    proce.SysTaxCodeDescID = item.TaxGrpID;
//                    proce.OrderItemProcID = item.ProcessItemID;
//                    proce.Update(item.ProcessItemID);
//                }
//            }

//            HttpContext.Current.Session["SalesProcessCart"] = dt;
//        }

//        public void AddToProcessCart(SalesProcessCartItem item, double exchangeRate)
//        {
//            DataTable dt = this.CurrentProcessItemCart;
//            DataRow row = null;
//            foreach (DataRow r in dt.Rows)
//            {
//                if (BusinessUtility.GetString(r["ProcessCode"]).ToUpper().Trim() == item.ProcessCode.ToUpper().Trim())
//                {
//                    row = r;
//                    break;
//                }
//            }
//            if (row != null)
//            {
//                row["ProcessCode"] = item.ProcessCode;
//                row["TaxGrpID"] = item.TaxGrpID;
//                row["TaxGrpName"] = item.TaxGrpName;
//                row["ProcessDescription"] = item.ProcessDescription;
//                row["ProcessFixedCost"] = item.ProcessFixedCost;
//                row["ProcessCostPerHour"] = item.ProcessCostPerHour;
//                row["ProcessCostPerUnit"] = item.ProcessCostPerUnit;
//                row["TotalHour"] = item.TotalHour;
//                row["TotalUnit"] = item.TotalUnit;
//                row["ProcessTotal"] = exchangeRate * (BusinessUtility.GetDouble(row["ProcessFixedCost"]) + (BusinessUtility.GetDouble(row["ProcessCostPerHour"]) * BusinessUtility.GetDouble(row["TotalHour"])) + (BusinessUtility.GetDouble(row["ProcessCostPerUnit"]) * BusinessUtility.GetDouble(row["TotalUnit"])));

//                //To Do to save in db
//                if (item.OrderID > 0 && item.ProcessItemID > 0) //Ensure that record was exists in db.
//                {
//                    OrderItemProcess proce = new OrderItemProcess();
//                    proce.OrdID = item.OrderID;
//                    proce.OrdItemProcCode = item.ProcessCode;
//                    proce.OrdItemProcFixedPrice = item.ProcessFixedCost;
//                    proce.OrdItemProcHours = item.TotalHour;
//                    proce.OrdItemProcPricePerHour = item.ProcessCostPerHour;
//                    proce.OrdItemProcPricePerUnit = item.ProcessCostPerUnit;
//                    proce.OrdItemProcUnits = item.TotalUnit;
//                    proce.SysTaxCodeDescID = item.TaxGrpID;
//                    proce.OrderItemProcID = item.ProcessItemID;
//                    proce.Update(item.ProcessItemID);
//                }
//            }
//            else
//            {
//                row = dt.NewRow();
//                row["ProcessCode"] = item.ProcessCode;
//                row["TaxGrpID"] = item.TaxGrpID;
//                row["TaxGrpName"] = item.TaxGrpName;
//                row["ProcessDescription"] = item.ProcessDescription;
//                row["ProcessFixedCost"] = item.ProcessFixedCost;
//                row["ProcessCostPerHour"] = item.ProcessCostPerHour;
//                row["ProcessCostPerUnit"] = item.ProcessCostPerUnit;
//                row["TotalHour"] = item.TotalHour;
//                row["TotalUnit"] = item.TotalUnit;
//                row["ProcessTotal"] = exchangeRate * (BusinessUtility.GetDouble(row["ProcessFixedCost"]) + (BusinessUtility.GetDouble(row["ProcessCostPerHour"]) * BusinessUtility.GetDouble(row["TotalHour"])) + (BusinessUtility.GetDouble(row["ProcessCostPerUnit"]) * BusinessUtility.GetDouble(row["TotalUnit"])));                

//                //To Do to save in db
//                if (item.OrderID > 0)
//                {
//                    OrderItemProcess proce = new OrderItemProcess();
//                    proce.OrdID = item.OrderID;
//                    proce.OrdItemProcCode = item.ProcessCode;
//                    proce.OrdItemProcFixedPrice = item.ProcessFixedCost;
//                    proce.OrdItemProcHours = item.TotalHour;
//                    proce.OrdItemProcPricePerHour = item.ProcessCostPerHour;
//                    proce.OrdItemProcPricePerUnit = item.ProcessCostPerUnit;
//                    proce.OrdItemProcUnits = item.TotalUnit;
//                    proce.SysTaxCodeDescID = item.TaxGrpID;                    
//                    proce.Insert();
//                    row["ProcessItemID"] = proce.OrderItemProcID;
//                }

//                dt.Rows.Add(row);
//            }

//            HttpContext.Current.Session["SalesProcessCart"] = dt;
//        }

//        public void DeleteProcessCartItem(int id)
//        {
//            DataTable cart = this.CurrentProcessItemCart;
//            DataRow rw = cart.Rows.Find(id);
//            if (rw != null)
//            {
//                int procItemID = BusinessUtility.GetInt(rw["ProcessItemID"]);
//                if (procItemID > 0)
//                {
//                    OrderItemProcess proc = new OrderItemProcess();
//                    proc.Delete(procItemID);
//                }                

//                cart.Rows.Remove(rw);
//            }
//            HttpContext.Current.Session["SalesProcessCart"] = cart;
//        }

//        public List<OrderItemProcess> GetProcessItemsFromCart(int orderIdToSet)
//        {
//            List<OrderItemProcess> ordITemProcess = new List<OrderItemProcess>();
//            foreach (DataRow dr in this.CurrentProcessItemCart.Rows)
//            {
//                OrderItemProcess oip = new OrderItemProcess();
//                oip.OrdID = orderIdToSet;
//                oip.OrdItemProcCode = BusinessUtility.GetString(dr["ProcessCode"]);
//                oip.OrdItemProcFixedPrice = BusinessUtility.GetDouble(dr["ProcessFixedCost"]);
//                oip.OrdItemProcHours = BusinessUtility.GetInt(dr["TotalHour"]);
//                oip.OrdItemProcPricePerHour = BusinessUtility.GetDouble(dr["ProcessCostPerHour"]);
//                oip.OrdItemProcPricePerUnit = BusinessUtility.GetDouble(dr["ProcessCostPerUnit"]);
//                oip.OrdItemProcUnits = BusinessUtility.GetInt(dr["TotalUnit"]);
//                oip.SysTaxCodeDescID = BusinessUtility.GetInt(dr["TaxGrpID"]);

//                ordITemProcess.Add(oip);
//            }
//            return ordITemProcess;
//        }

//        public void SetItemAsCanceled(int orderItemID)
//        {
//            DataTable cart = this.CurrentItemCart;
//            DataRow dr = null;
//            foreach (DataRow r in cart.Rows)
//            {
//                if (orderItemID == BusinessUtility.GetInt(r["OrderItemID"]))
//                {
//                    dr = r;
//                    break;
//                }
//            }
//            if (dr != null)
//            {
//                dr["Price"] = 0.0D;
//                dr["ItemTotal"] = 0.0D;
//                dr["IsCanceled"] = true;
//            }
//            HttpContext.Current.Session["SalesCart"] = cart;
//        }

//        public SalesProcessCartItem GetProcessItem(int id)
//        {
//            SalesProcessCartItem item = new SalesProcessCartItem();
//            DataRow row = this.CurrentProcessItemCart.Rows.Find(id);
//            if (row != null)
//            {
//                item.ProcessCode = BusinessUtility.GetString(row["ProcessCode"]);
//                item.ProcessCostPerHour = BusinessUtility.GetDouble(row["ProcessCostPerHour"]);
//                item.ProcessCostPerUnit = BusinessUtility.GetDouble(row["ProcessCostPerUnit"]);
//                item.ProcessDescription = BusinessUtility.GetString(row["ProcessDescription"]);
//                item.ProcessFixedCost = BusinessUtility.GetDouble(row["ProcessFixedCost"]);
//                item.ProcessID = BusinessUtility.GetInt(row["ProcessID"]);
//                item.ProcessTotal = BusinessUtility.GetDouble(row["ProcessTotal"]);
//                item.TaxGrpID = BusinessUtility.GetInt(row["TaxGrpID"]);
//                item.TaxGrpName = BusinessUtility.GetString(row["TaxGrpName"]);
//                item.TotalHour = BusinessUtility.GetInt(row["TotalHour"]);
//                item.TotalUnit = BusinessUtility.GetInt(row["TotalUnit"]);
//                item.ProcessItemID = BusinessUtility.GetInt(row["ProcessItemID"]);
//                item.OrderID = BusinessUtility.GetInt(row["OrderID"]);
//                return item;
//            }
//            return null;
//        }         
//        #endregion

//        #region Generic Methods
//        public TotalSummary GetOrderTotal(double exRate, int comID, int partnerID)
//        {
//            DbHelper dbHelp = new DbHelper(true);
//            try
//            {
//                return CalculationHelper.GetOrderTotal(dbHelp, this, exRate, comID, partnerID);
//            }
//            catch 
//            {
                
//                throw;
//            }
//            finally
//            {
//                dbHelp.CloseDatabaseConnection();
//            }
//        }

//        public void BindCarts(int orderID)
//        {
//            DbHelper dbHelp = new DbHelper(true);

//            try
//            {
//                this.BindItemCart(dbHelp, orderID);
//                this.BindProcessCart(dbHelp, orderID);
//            }
//            catch
//            {
//                throw;
//            }
//            finally
//            {
//                dbHelp.CloseDatabaseConnection();
//            }
//        }        

//        public void Dispose()
//        {
//            HttpContext.Current.Session.Remove("SalesCart");
//            HttpContext.Current.Session.Remove("SalesProcessCart");
//        } 
//        #endregion
//    }       
//}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using System.Data.Odbc;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Bookmark
    {
        public int BookmarkID { get; set; }
        public string BookmarkName { get; set; }
        public string BookmarkUrl { get; set; }
        public int UserID { get; set; }

        public void Insert() {
            string sqlCheck = "SELECT COUNT(*) FROM z_bookmarks WHERE UserID=@UserID AND BookmarkName=@BookmarkName";
            string sqlInsert = "INSERT INTO z_bookmarks(BookmarkName,BookmarkUrl,UserID) VALUES(@BookmarkName,@BookmarkUrl,@UserID)";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object val = dbHelp.GetValue(sqlCheck, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("UserID", this.UserID, MyDbType.Int),
                    DbUtility.GetParameter("BookmarkName", this.BookmarkName, MyDbType.String)
                });

                if (BusinessUtility.GetInt(val) > 0)
                {
                    throw new Exception("BOOK_MARK_ALREADY_ESISTS");
                }

                dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("UserID", this.UserID, MyDbType.Int),
                    DbUtility.GetParameter("BookmarkName", this.BookmarkName, MyDbType.String),
                    DbUtility.GetParameter("BookmarkUrl", this.BookmarkUrl, MyDbType.String)
                });
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql(ParameterCollection pCol, int userid)
        {
            return string.Format("SELECT * FROM z_bookmarks z WHERE z.UserID = {0}", userid);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTECH.InbizERP.BusinessLogic
{
    public class OptionList
    {
        public OptionList()
        {
            this._id = "";
            this._name = "";
        }

        private string _id;

        public string id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web;

namespace iTECH.InbizERP.BusinessLogic
{
    public class GridSqlHelper
    {       
        #region JobPlanningHelper
        public static string GetAllJobPlanning(ParameterCollection pCol, string custName, string custPhone, int orderNo, string assignTo, DateTime fromDate, DateTime toDate)
        {
            pCol.Clear();
            string sql = "SELECT DISTINCT o.ordID,PartnerAcronyme,Jobduedate,partnerPhone,o.ordCustID,o.ordCustType, ";
            sql += " PartnerLongName AS CustomerName, CONCAT(IFNULL(u.userFirstName, ''), ' ',  IFNULL(u.userLastName, '')) AS AssignedTo, ordShpTrackNo, ordStatus, DATE_FORMAT(ordShpDate,'%m-%d-%Y') AS ordShpDate FROM orders o ";
            sql += " INNER JOIN orderitems i ON i.ordID=o.ordID LEFT JOIN partners ON partners.PartnerID =o.ordCustID ";
            sql += " LEFT JOIN jobplanning J ON J.orderID=o.ordID ";
            sql += " LEFT JOIN users u ON u.userID=J.userID ";
            sql += " WHERE (ordStatus='P') ";

            if (CurrentUser.IsInRole(RoleID.JOB_PLANNER) && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.JOB_PLANNING_MANAGER))
            {
                sql += "  AND u.UserID=@UserID";
                pCol.Add("@UserID", CurrentUser.UserID.ToString());
            }
            if (!string.IsNullOrEmpty(custName))
            {
                sql += " AND (PartnerLongName LIKE  CONCAT('%', @CustName ,'%') OR PartnerAcronyme LIKE CONCAT('%', @CustName ,'%'))";
                pCol.Add("@CustName", custName);
            }
            if (!string.IsNullOrEmpty(custPhone))
            {
                sql += " AND partnerPhone=@CustPhone";
                pCol.Add("@CustPhone", custPhone);
            }
            if (orderNo > 0)
            {
                sql += " AND o.ordID=@OrderNo";
                pCol.Add("@OrderNo", orderNo.ToString());
            }
            if (!string.IsNullOrEmpty(assignTo))
            {
                sql += " AND ((u.userFirstName LIKE CONCAT('%', @AssignTo, '%')) OR (u.userLastName LIKE CONCAT('%', @AssignTo, '%')))";
                pCol.Add("@AssignTo", assignTo);
            }
            if (fromDate != DateTime.MinValue && (toDate == DateTime.MinValue || toDate <= fromDate))
            {
                sql += " AND J.Jobduedate >= @FromDate";
                pCol.Add("@FromDate", fromDate.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            else if (fromDate == DateTime.MinValue && toDate != DateTime.MinValue)
            {
                sql += " AND J.Jobduedate <= @ToDate";
                pCol.Add("@ToDate", toDate.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            else if(fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
            {
                sql += " AND J.Jobduedate >= @FromDate AND J.Jobduedate <= @ToDate";
                pCol.Add("@FromDate", fromDate.ToString("yyyy-MM-dd HH:mm:ss"));
                pCol.Add("@ToDate", toDate.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            sql += " GROUP BY o.ordID ORDER BY o.ordID, CustomerName ";
            
            return sql;
        }
        #endregion
    }
}

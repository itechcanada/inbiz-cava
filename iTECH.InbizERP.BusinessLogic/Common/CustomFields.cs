﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

namespace iTECH.InbizERP.BusinessLogic
{
    public class CustomFields
    {
        public int ID { get; set; }        
        public string DescEn { get; set; }
        public string DescFr { get; set; }
        public string DescSp { get; set; }
        public int FieldType { get; set; }
        public int Module { get; set; }
        public bool IsRequired { get; set; }
        public bool IsActive { get; set; }

        public void Insert()
        {
            string sql = "INSERT INTO app_custom_fields(DescEn,DescFr,DescSp,FieldType,Module, IsRequired,IsActive) VALUES(@DescEn,@DescFr,@DescSp,@FieldType,@Module, @IsRequired,@IsActive)";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] {                     
                    DbUtility.GetParameter("DescEn", this.DescEn, MyDbType.String),
                    DbUtility.GetParameter("DescFr", this.DescFr, MyDbType.String),
                    DbUtility.GetParameter("DescSp", this.DescSp, MyDbType.String),
                    DbUtility.GetParameter("FieldType", this.FieldType, MyDbType.Int),
                    DbUtility.GetParameter("Module", this.Module, MyDbType.Int),
                    DbUtility.GetParameter("IsRequired", this.IsRequired, MyDbType.Boolean),
                    DbUtility.GetParameter("IsActive", this.IsActive, MyDbType.Boolean)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void Update()
        {
            string sql = "UPDATE app_custom_fields SET DescEn=@DescEn, DescFr=@DescFr, DescSp=@DescSp,FieldType=@FieldType,Module=@Module, IsRequired=@IsRequired WHERE ID=@ID";
            DbHelper dbHelp = new DbHelper();
            try
            {
                dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("DescEn", this.DescEn, MyDbType.String),
                    DbUtility.GetParameter("DescFr", this.DescFr, MyDbType.String),
                    DbUtility.GetParameter("DescSp", this.DescSp, MyDbType.String),
                    DbUtility.GetParameter("FieldType", this.FieldType, MyDbType.Int),
                    DbUtility.GetParameter("Module", this.Module, MyDbType.Int),
                     DbUtility.GetParameter("IsRequired", this.IsRequired, MyDbType.Boolean),
                    DbUtility.GetParameter("ID", this.ID, MyDbType.Int)
                });
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public string GetSql(ParameterCollection pCol, string keyWord, int module, string lang)
        {
            pCol.Clear();
            string sql = string.Format("SELECT *, {0} AS 'Desc' FROM app_custom_fields WHERE IsActive=1", Globals.GetFieldName("Desc", lang));
            if (!string.IsNullOrEmpty(keyWord))
            {
                sql += " AND (DescEn LIKE CONCAT(@Serarch, '%') OR DescFr LIKE CONCAT(@Serarch, '%') OR DescSp LIKE CONCAT(@Serarch, '%'))";
                pCol.Add("@Search", keyWord);
            }
            if (module > 0)
            {
                sql += " AND Module=@Module";
                pCol.Add("@Module", module.ToString());
            }
            return sql; 
        }

        public void PopulateObject(int id)
        {
            string sql = "SELECT * FROM app_custom_fields  WHERE ID=@ID";
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ID", id, MyDbType.Int)
                });
                if (dr.Read())
                {
                    this.DescEn = BusinessUtility.GetString(dr["DescEn"]);
                    this.DescFr = BusinessUtility.GetString(dr["DescFr"]);
                    this.DescSp = BusinessUtility.GetString(dr["DescSp"]);
                    this.FieldType = BusinessUtility.GetInt(dr["FieldType"]);
                    this.ID = BusinessUtility.GetInt(dr["ID"]);
                    this.IsActive = BusinessUtility.GetBool(dr["IsActive"]);
                    this.Module = BusinessUtility.GetInt(dr["Module"]);                    
                    this.IsRequired = BusinessUtility.GetBool(dr["IsRequired"]);
                }
            }
            catch
            {

                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public void Delete(int id)
        {
            string sql = "DELETE FROM app_custom_fields  WHERE ID=@ID";
            DbHelper dbHelp = new DbHelper();            
            try
            {
                dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ID", id, MyDbType.Int)
                });                
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public void SetValues(List<CustomFieldValues> lValues)
        {
            string sqlCheck = "SELECT COUNT(*) FROM z_custom_field_values WHERE RefFieldID=@RefFieldID AND RefPrimaryKey=@RefPrimaryKey";
            string sqlInsert = "INSERT INTO z_custom_field_values (RefFieldID, RefPrimaryKey, ValueData) VALUES(@RefFieldID, @RefPrimaryKey, @ValueData)";
            string sqlUpdate = "UPDATE z_custom_field_values SET ValueData=@ValueData WHERE RefFieldID=@RefFieldID AND RefPrimaryKey=@RefPrimaryKey";
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                object o = null;
                foreach (var item in lValues)
                {
                    o = dbHelp.GetValue(sqlCheck, CommandType.Text, new MySqlParameter[] { 
                          DbUtility.GetParameter("RefFieldID", item.RefFieldID, MyDbType.Int),
                          DbUtility.GetParameter("RefPrimaryKey", item.RefPrimaryKey, MyDbType.Int)
                    });
                    if (BusinessUtility.GetInt(o) > 0)
                    {
                        dbHelp.ExecuteNonQuery(sqlUpdate, CommandType.Text, new MySqlParameter[] { 
                            DbUtility.GetParameter("RefFieldID", item.RefFieldID, MyDbType.Int),
                            DbUtility.GetParameter("RefPrimaryKey", item.RefPrimaryKey, MyDbType.Int),
                            DbUtility.GetParameter("ValueData", item.ValueData, MyDbType.String)
                        });
                    }
                    else
                    {
                        dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                            DbUtility.GetParameter("RefFieldID", item.RefFieldID, MyDbType.Int),
                            DbUtility.GetParameter("RefPrimaryKey", item.RefPrimaryKey, MyDbType.Int),
                            DbUtility.GetParameter("ValueData", item.ValueData, MyDbType.String)
                        });
                    }
                }
            }
            catch
            {
                throw;
            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public List<CustomFieldValues> GetValues(int primaryKey, CustomFieldApplicableSection section)
        {
            string sql = "SELECT v.*, f.FieldType FROM z_custom_field_values v INNER JOIN app_custom_fields f ON f.ID=v.RefFieldID WHERE v.RefPrimaryKey=@RefPrimaryKey AND f.Module=@Module";
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;
            List<CustomFieldValues> lResult = new List<CustomFieldValues>();
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("RefPrimaryKey", primaryKey, MyDbType.Int),
                    DbUtility.GetParameter("Module", (int)section, MyDbType.Int)
                });
                int fType = 0;
                while (dr.Read())
                {
                    CustomFieldValues v = new CustomFieldValues();
                    v.ID = BusinessUtility.GetInt(dr["ID"]);
                    v.RefFieldID = BusinessUtility.GetInt(dr["RefFieldID"]);
                    v.RefPrimaryKey = BusinessUtility.GetInt(dr["RefPrimaryKey"]);
                    v.ValueData = BusinessUtility.GetString(dr["ValueData"]);
                    fType = BusinessUtility.GetInt(dr["FieldType"]);
                    switch (fType)
                    {
                        case (int)CustomFieldType.Currency:
                        case (int)CustomFieldType.Double:
                            v.TypedValue = BusinessUtility.GetDouble(v.ValueData);
                            break;
                        case (int)CustomFieldType.Integer:
                            v.TypedValue = BusinessUtility.GetInt(v.ValueData);
                            break;
                        case (int)CustomFieldType.StringSingleLine:
                        case (int)CustomFieldType.StringMultiline:
                            v.TypedValue = BusinessUtility.GetString(v.ValueData);
                            break;
                        default:
                            v.TypedValue = null;
                            break;
                    }
                    lResult.Add(v);
                }
                return lResult;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }

        public CustomFieldValues GetValue(int primaryKey, int refFieldID, CustomFieldApplicableSection section)
        {
            string sql = "SELECT v.*, f.FieldType FROM z_custom_field_values v INNER JOIN app_custom_fields f ON f.ID=v.RefFieldID WHERE v.RefPrimaryKey=@RefPrimaryKey AND f.Module=@Module AND v.RefFieldID=@RefFieldID";
            DbHelper dbHelp = new DbHelper();
            MySqlDataReader dr = null;            
            try
            {
                dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("RefPrimaryKey", primaryKey, MyDbType.Int),
                    DbUtility.GetParameter("Module", (int)section, MyDbType.Int),
                    DbUtility.GetParameter("RefFieldID", refFieldID, MyDbType.Int)
                });
                int fType = 0;
                CustomFieldValues v = new CustomFieldValues();
                if (dr.Read())
                {                    
                    v.ID = BusinessUtility.GetInt(dr["ID"]);
                    v.RefFieldID = BusinessUtility.GetInt(dr["RefFieldID"]);
                    v.RefPrimaryKey = BusinessUtility.GetInt(dr["RefPrimaryKey"]);
                    v.ValueData = BusinessUtility.GetString(dr["ValueData"]);
                    fType = BusinessUtility.GetInt(dr["FieldType"]);
                    switch (fType)
                    {
                        case (int)CustomFieldType.Currency:
                        case (int)CustomFieldType.Double:
                            v.TypedValue = BusinessUtility.GetDouble(v.ValueData);
                            break;
                        case (int)CustomFieldType.Integer:
                            v.TypedValue = BusinessUtility.GetInt(v.ValueData);
                            break;
                        case (int)CustomFieldType.StringSingleLine:
                        case (int)CustomFieldType.StringMultiline:
                            v.TypedValue = BusinessUtility.GetString(v.ValueData);
                            break;
                        case (int)CustomFieldType.BooleanCheckBox:
                            v.TypedValue = v.ValueData == "1";
                            break;
                        default:
                            v.TypedValue = null;
                            break;
                    }                   
                }
                return v;
            }
            catch
            {

                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection(dr);
            }
        }
    }

    public class CustomFieldValues
    {
        public int ID { get; set; }
        public int RefFieldID { get; set; }
        public int RefPrimaryKey { get; set; }
        public string ValueData { get; set; }
        public object TypedValue { get; internal set; }
    }
}

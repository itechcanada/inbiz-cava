﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Threading;
using System.Globalization;
using System.Data;
using System.Reflection;

using iTECH.Library.Utilities;
using iTECH.Library.Web;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;

namespace iTECH.InbizERP.BusinessLogic
{
    public class Globals
    {
        #region Popup Javascript Members
        public static void RegisterCloseDialogScript(Page pg)
        {
            //pg.ClientScript.RegisterClientScriptBlock(pg.GetType(), "CloseDialog", "closeIframeDialog();", true);
            pg.ClientScript.RegisterClientScriptBlock(pg.GetType(), "CloseDialog", "jQuery.FrameDialog.closeDialog();", true);
        }

        public static void RegisterCloseDialogScript(Page pg, string statements, bool includeStatementsBeforeClose)
        {
            //pg.ClientScript.RegisterClientScriptBlock(pg.GetType(), "CloseDialog", "closeIframeDialog();", true);
            if (includeStatementsBeforeClose)
            {
                pg.ClientScript.RegisterClientScriptBlock(pg.GetType(), "CloseDialog", string.Format("{0};jQuery.FrameDialog.closeDialog();", statements), true);
            }
            else
            {
                pg.ClientScript.RegisterClientScriptBlock(pg.GetType(), "CloseDialog", string.Format("jQuery.FrameDialog.closeDialog();{0}", statements), true);
            }
        }

        public static void RegisterReloadParentScript(Page pg)
        {
            pg.ClientScript.RegisterClientScriptBlock(pg.GetType(), "ReloadParent", "parent.location.href=parent.location.href;", true);
        }

        public static void RegisterParentRedirectPageUrl(Page pg, string url)
        {
            pg.ClientScript.RegisterClientScriptBlock(pg.GetType(), "ReloadParent", string.Format("parent.location.href='{0}';", url), true);
        }

        public static void RegisterScript(Page pg, string script)
        {
            string key = Guid.NewGuid().ToString();
            if (!pg.ClientScript.IsClientScriptBlockRegistered(key))
            {
                pg.ClientScript.RegisterClientScriptBlock(pg.GetType(), key, script, true);
            }
        }
        #endregion       

        #region Culture & App Language Members
        public static string CurrentCultureName
        {
            get
            {
                string cName = string.Empty;
                cName = BusinessUtility.GetString(HttpContext.Current.Request.QueryString["lang"]);
                switch (cName.Trim().ToLower())
                {
                    case AppLanguageCode.EN:
                        return AppCulture.ENGLISH_CANADA;
                    case AppLanguageCode.FR:
                        return AppCulture.FRENCH_CANADA;
                    default:
                        if (HttpContext.Current.Session["Language"] == null)
                        {
                            return AppCulture.FRENCH_CANADA; //Edited by mukesh 20130424
                        }
                        return HttpContext.Current.Session["Language"].ToString();
                }
            }
        }

        /// <summary>
        /// Set culture information in session storage.
        /// </summary>
        /// <param name="cultureName"></param>
        public static void SetCultureInfo(string cultureName)
        {
            try
            {
                HttpContext.Current.Session["Language"] = cultureName;
                CultureInfo cInfo = new CultureInfo(cultureName);
                Thread.CurrentThread.CurrentCulture = cInfo;
                Thread.CurrentThread.CurrentUICulture = cInfo;
            }
            catch
            {
                HttpContext.Current.Session["Language"] = AppCulture.DEFAULT;
                CultureInfo cInfo = new CultureInfo(AppCulture.DEFAULT);
                Thread.CurrentThread.CurrentCulture = cInfo;
                Thread.CurrentThread.CurrentUICulture = cInfo;
            }
        }

        /// <summary>
        /// Returns current app language name of length 2 char.
        /// </summary>
        public static string CurrentAppLanguageCode
        {
            get
            {
                switch (Globals.CurrentCultureName)
                {
                    case AppCulture.FRENCH_CANADA:
                        return AppLanguageCode.FR;
                    case AppCulture.ENGLISH_CANADA:
                        return AppLanguageCode.EN;
                    default:
                        return AppLanguageCode.DEFAULT;
                }
            }
        }
        #endregion

        #region Global Parameters

        public static string GetFieldName(string fileldNamePrefix, string lang)
        {
            return fileldNamePrefix + StringUtil.ToTitleCase(lang);
        }

        public static string GetPartnerType(int partnerTypeID)
        {
            switch (partnerTypeID)
            {
                case (int)PartnerTypeIDs.EndClient:
                    return StatusCustomerTypes.END_CLINET;
                case (int)PartnerTypeIDs.Distributer:
                    return StatusCustomerTypes.DISTRIBUTER;
                case (int)PartnerTypeIDs.Leads:
                    return StatusCustomerTypes.LEADS;
                case (int)PartnerTypeIDs.Reseller:
                    return StatusCustomerTypes.RESELLER;
                default:
                    return string.Empty;
            }
        }

        public static int GetPartnerTypeID(string partnerType)
        {
            switch (partnerType.ToUpper())
            {
                case StatusCustomerTypes.END_CLINET:
                    return (int)PartnerTypeIDs.EndClient;
                case StatusCustomerTypes.DISTRIBUTER:
                    return (int)PartnerTypeIDs.Distributer;
                case StatusCustomerTypes.LEADS:
                    return (int)PartnerTypeIDs.Leads;
                case StatusCustomerTypes.RESELLER:
                    return (int)PartnerTypeIDs.Reseller;
                default:
                    return 0;
            }
        }

        public static string GetPaymentVia(object idVal)
        {
            if (idVal == DBNull.Value) return string.Empty;
            switch (BusinessUtility.GetInt(idVal))
            {
                case (int)StatusAmountReceivedVia.Cash:
                    return "Cash";
                case (int)StatusAmountReceivedVia.Cheque:
                    return "Cheque";
                case (int)StatusAmountReceivedVia.CreditCard:
                    return "Credit Card";
                case (int)StatusAmountReceivedVia.MoneyOrder:
                    return "Money Order";
                case (int)StatusAmountReceivedVia.DirectWireTransfer:
                    return "Direct Wire Transfer";
                case (int)StatusAmountReceivedVia.DebitCard:
                    return "Debit Card";
                case (int)StatusAmountReceivedVia.WriteOff:
                    return "Write Off";
                case (int)StatusAmountReceivedVia.SwipeCard:
                    return "Swipe Card";
                case (int)StatusAmountReceivedVia.Paypal:
                    return "Paypal";
                case (int)StatusAmountReceivedVia.Credit:
                    return "Credit";
                default:
                    return string.Empty;
            }
        }

        public static string GetRoomType(int idVal)
        {
            DbHelper dbHelp = new DbHelper();
            try
            {
                string sql = "SELECT sysAppDesc FROM sysstatus WHERE ";
                sql += "sysAppCodeLang ='en' AND sysAppCodeActive=1 AND sysAppPfx='CT' AND sysAppCode='dlSGp' AND sysAppLogicCode=@sysAppLogicCode";
                object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("sysAppLogicCode", idVal, MyDbType.Int)
                });

                return BusinessUtility.GetString(val);
            }
            catch
            {
                return string.Empty;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }

        public static DayOfWeek GetDayOfWeek(string abbr)
        {
            switch (abbr.ToLower())
            {
                case "tue":
                    return DayOfWeek.Tuesday;
                case "wed":
                    return DayOfWeek.Wednesday;
                case "thu":
                    return DayOfWeek.Thursday;
                case "fri":
                    return DayOfWeek.Friday;
                case "sat":
                    return DayOfWeek.Saturday;
                case "sun":
                    return DayOfWeek.Sunday;
                case "mon":
                default:
                    return DayOfWeek.Monday;
            }
        }

        public static DataTable ToDataTable<T>(List<T> items, bool addAutoKey)
        {
            var tb = new DataTable(typeof(T).Name);

            if (addAutoKey)
            {
                DataColumn auto = new DataColumn("SystemAutoID", typeof(int));
                auto.AutoIncrement = true;
                auto.AutoIncrementSeed = 1;
                auto.AutoIncrementStep = 1;
                tb.Columns.Add(auto);
                tb.PrimaryKey = new DataColumn[] { auto };
            }

            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var prop in props)
            {
                tb.Columns.Add(prop.Name, prop.PropertyType);
            }

            foreach (var item in items)
            {
                DataRow nR = tb.NewRow();
                //var values = new object[props.Length];
                for (var i = 0; i < props.Length; i++)
                {
                    nR[addAutoKey ? i + 1 : i] = props[i].GetValue(item, null);
                }

                tb.Rows.Add(nR);
            }

            return tb;
        }

        #endregion

        #region Get SequenceNo Methods
        public static string GetNewBarCode(string sType, string pfx, string sFormat)
        {
            string sqlMax = "SELECT IFNULL(MAX(SeqNo), 0) FROM z_sequence_no WHERE SeqType=@SeqType";
            string sqlInsert = "INSERT INTO z_sequence_no(SeqNo,SeqType,SeqPfx,SeqFormat) VALUES(@SeqNo,@SeqType,@SeqPfx,@SeqFormat)";
            DbHelper dbHelp = new DbHelper(true);
            if (string.IsNullOrEmpty(sFormat))
            {
                sFormat = "{0}";
            }
            try
            {
                object val = dbHelp.GetValue(sqlMax, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("SeqType", sType, MyDbType.String)
                });
                int newSeq = BusinessUtility.GetInt(val) + 1;
                dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("SeqNo", newSeq, MyDbType.Int),
                    DbUtility.GetParameter("SeqType",  sType, MyDbType.String),
                    DbUtility.GetParameter("SeqPfx", pfx, MyDbType.String),
                    DbUtility.GetParameter("SeqFormat", sFormat, MyDbType.String)
                });
                if (!string.IsNullOrEmpty(sFormat))
                {
                    if (!string.IsNullOrEmpty(pfx))
                    {
                        return string.Format(sFormat, pfx, newSeq);
                    }
                    return string.Format(sFormat, newSeq);
                }
                return string.Format("{0}", newSeq);
            }
            catch
            {
                throw;
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
        #endregion

        #region SomeReadyTo use Global settings from App Settings
        public static bool IsAllowedRoundingDecimal {
            get {
                return AppConfiguration.AppSettings[AppSettingKey.AllowRoundingDecimalAmount].ToLower() == "true";
            }
        }
        public static int DecimalPlaceToBeRound {
            get
            {
                return BusinessUtility.GetInt(AppConfiguration.AppSettings[AppSettingKey.DecimalPlacesUpToRound].ToLower());
            }
        }
        #endregion
    }
}


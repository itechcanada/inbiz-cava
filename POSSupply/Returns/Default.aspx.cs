﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;


public partial class Returns_Default : BasePage
{
    private string strSOID = "";
    private Orders _objOrder = new Orders();
    int _currentRowIdx = -1;
    TotalSummary _lTotal;
    // On Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        //Security
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.SALES_MANAGER) && !CurrentUser.IsInRole(RoleID.SALES_REPRESENTATIVE))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        if (this.IsReturn)
        {
            ltReturn.Text = Resources.Resource.lblReturn;
        }
        else if (this.IsTransfer)
        {
            ltReturn.Text = Resources.Resource.lbltransfer;
        }

        if (!IsPostBack)
        {
            txtSearch.Focus();
        }


    }

    protected void btnOrderNO_Command(object sender, CommandEventArgs e)
    {
        string searchOn = e.CommandArgument.ToString();
        int recordsCount = 0;
        string soStatus = "";
        if (Request.QueryString.AllKeys.Contains("InvokeSrc") && Request.QueryString["InvokeSrc"] == "POS" && Request.QueryString.AllKeys.Contains("isReturn") && Request.QueryString["isReturn"] == "1")
        {
            soStatus = "S";
        }
        else if (Request.QueryString.AllKeys.Contains("InvokeSrc") && Request.QueryString["InvokeSrc"] == "POS" && Request.QueryString.AllKeys.Contains("isTransfer") && Request.QueryString["isTransfer"] == "1")
        {
            soStatus = "T";
        }
        switch (searchOn)
        {
            case "ON":
                recordsCount = new Orders().GetReturnsApplicableRecordCount(null, "ON", txtSearch.Text, CurrentUser.UserID, CurrentUser.IsSalesRistricted, this.WarehouseCode, soStatus);
                break;
            case "CI":
                recordsCount = new Orders().GetReturnsApplicableRecordCount(null, "CI", txtSearch.Text, CurrentUser.UserID, CurrentUser.IsSalesRistricted, this.WarehouseCode, soStatus);
                break;
            case "CN":
                recordsCount = new Orders().GetReturnsApplicableRecordCount(null, "CN", txtSearch.Text, CurrentUser.UserID, CurrentUser.IsSalesRistricted, this.WarehouseCode, soStatus);
                break;
            case "IN":
                recordsCount = new Orders().GetReturnsApplicableRecordCount(null, "IN", txtSearch.Text, CurrentUser.UserID, CurrentUser.IsSalesRistricted, this.WarehouseCode, soStatus);
                break;
        }

        if (recordsCount > 0)
        {
            if (Request.QueryString.AllKeys.Contains("InvokeSrc") && Request.QueryString["InvokeSrc"] == "POS" && Request.QueryString.AllKeys.Contains("isReturn") && Request.QueryString["isReturn"] == "1")
            {
                Response.Redirect(string.Format("~/Returns/ViewReturns.aspx?whs={0}&searchOn={1}&searchText={2}&InvokeSrc={3}&regcode={5}&isReturn={4}", dlWarehouse.SelectedValue, searchOn, txtSearch.Text, "POS", 1, this.POSRegCode));
            }
            else if (Request.QueryString.AllKeys.Contains("InvokeSrc") && Request.QueryString["InvokeSrc"] == "POS" && Request.QueryString.AllKeys.Contains("isTransfer") && Request.QueryString["isTransfer"] == "1")
            {
                Response.Redirect(string.Format("~/Returns/ViewReturns.aspx?whs={0}&searchOn={1}&searchText={2}&InvokeSrc={3}&isTransfer={4}", dlWarehouse.SelectedValue, searchOn, txtSearch.Text, "POS", 1));
            }
            else
            {
                Response.Redirect(string.Format("~/Returns/ViewReturns.aspx?whs={0}&searchOn={1}&searchText={2}", dlWarehouse.SelectedValue, searchOn, txtSearch.Text));
            }
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);
        }
    }

    public string WarehouseCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["whsCode"]);
        }
    }

    public string POSRegCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["regcode"]);
        }
    }


    public bool IsReturn
    {
        get
        {
            if (BusinessUtility.GetString(Request.QueryString["isReturn"]) == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public bool IsTransfer
    {
        get
        {
            if (BusinessUtility.GetString(Request.QueryString["isTransfer"]) == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
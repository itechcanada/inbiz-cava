﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Returns_mdReturn : BasePage
{
    bool isUpdateAndtransfer = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdProducts, grdProcess, JQGridTransfer))
        {
            if (this.InvoiceID > 0)
            {
                this.InvoiceFullReturn();
            }
            else if (this.OrderID > 0)
            {
                this.OrderFullReturn();
            }
        }
        if (this.IsTransfer)
        {
            pnlProcess.Visible = false;
            grdProducts.Visible = false;
        }
        else
        {
            pnlProcess.Visible = true;
            JQGridTransfer.Visible = false;
        }

        if ((this.IsTransfer == true) && (IsFullReturn == false))
        {
            btnValidateSKU.Visible = true;
            Orders ord = new Orders();
            string[] ordIDs = ord.GetOrderIDsSameGroup(BusinessUtility.GetString(this.OrderID));
            if (ordIDs.Length > 0)
            {
                if (BusinessUtility.GetInt(ordIDs[0]) > 0)
                {
                    mdbtnValidateSKU.Url = string.Format("~/shipping/validateSKU.aspx?oid={0}&validationfor={1}", string.Join(",", ordIDs), "transfer");
                }
            }
            else if (this.OrderID > 0)
            {
                mdbtnValidateSKU.Url = string.Format("~/shipping/validateSKU.aspx?oid={0}&validationfor={1}", this.OrderID, "transfer");
            }
        }
    }

    protected void page_init(object sender, EventArgs e)
    {
        Session["validateorder"] = null;
        Orders ord = new Orders();
        DbHelper dbHelp = new DbHelper();
        ord.PopulateObject(dbHelp, this.OrderID);
        if (ord.OrderTypeCommission == 2)
        {
            btnReturn.Text = Resources.Resource.lblReceive;// lbltransfer;//<%$Resources:Resource, lblSetReturnQty%>
            Label1.Text = Resources.Resource.lblSetTransferQty;

        }
        else
        {
            btnReturn.Text = Resources.Resource.lblReturn1;
            Label1.Text = Resources.Resource.lblSetReturnQty;
        }

    }

    private void OrderFullReturn()
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            dbHelp.OpenDatabaseConnection();
            OrderReturn rtn = new OrderReturn();
            OrderProcessReturn rtnProcess = new OrderProcessReturn();
            List<ReturnCart> returnCart = new List<ReturnCart>();
            List<ReturnProcessCart> returnProcessCart = new List<ReturnProcessCart>();

            Orders ord = new Orders();
            string[] ordIDs = ord.GetOrderIDsSameGroup(BusinessUtility.GetString(this.OrderID));

            int id = 1;
            if (ordIDs.Length > 0)
            {
                if (BusinessUtility.GetInt(ordIDs[0]) > 0)
                {
                    SalesCartHelper.InitCart(dbHelp, ordIDs);
                    OrderItems oi = new OrderItems();

                    foreach (var item in SalesCartHelper.CurrentCart.Items)
                    {
                        ReturnCart cart = new ReturnCart();
                        cart.ID = id;
                        cart.ProductName = item.OriginalPrdName;
                        cart.ProductDesc = item.ProductName;
                        cart.ProductID = item.ProductID;
                        cart.ProductInOrderQty = item.Quantity;
                        cart.PreviousReturnedQty = rtn.GetPreviousReturnedQty(dbHelp, oi.GetOrderItemIDs(null, ordIDs, item.ProductID));
                        //if (this.IsFullReturn || isUpdateAndtransfer)
                        //{
                            cart.ProductToReturnQty = (item.Quantity - cart.PreviousReturnedQty);
                        //}
                        //else
                        //{
                        //    cart.ProductToReturnQty = 0;
                        //}
                        cart.UPCCode = item.UPCCode;
                        returnCart.Add(cart);
                        id++;
                    }
                }
            }
            else if (this.OrderID > 0)
            {
                SalesCartHelper.InitCart(dbHelp, this.OrderID);

                foreach (var item in SalesCartHelper.CurrentCart.Items)
                {
                    ReturnCart cart = new ReturnCart();
                    cart.ID = id;
                    cart.OrderID = item.OrderID;
                    cart.OrderItemID = item.OrderItemID;
                    cart.ProductName = item.OriginalPrdName;
                    cart.ProductDesc = item.ProductName;
                    cart.ProductID = item.ProductID;
                    cart.ProductInOrderQty = item.Quantity;
                    cart.PreviousReturnedQty = rtn.GetPreviousReturnedQty(dbHelp, item.OrderItemID);
                    //cart.ProductToReturnQty = cart.ProductInOrderQty - cart.PreviousReturnedQty;
                    //if (this.IsFullReturn || isUpdateAndtransfer)
                    //{
                        cart.ProductToReturnQty = (item.Quantity - cart.PreviousReturnedQty);
                    //}
                    //else
                    //{
                    //    cart.ProductToReturnQty = 0;
                    //}
                    cart.WhsCode = item.WarehouseCode;
                    cart.UPCCode = item.UPCCode;
                    cart.ProductUnitPrice = item.Price;
                    cart.ItemTaxGroup = item.TaxGrp;
                    cart.InvoiceID = 0;
                    cart.InvoiceItemID = 0;
                    returnCart.Add(cart);
                    id++;
                }
            }

            Session["RETURN_CART"] = returnCart;

            //Orders ord = new Orders();
            ord.PopulateObject(dbHelp, this.OrderID);
            Session["RETURN_CART_CUSTID"] = ord.OrdCustID;


            id = 1;
            pnlProcess.Visible = SalesCartHelper.CurrentCart.ProcessItems.Count > 0;
            //Set Process Items in cart
            foreach (var item in SalesCartHelper.CurrentCart.ProcessItems)
            {
                ReturnProcessCart pc = new ReturnProcessCart();
                pc.ID = id;
                pc.PreviousReturnAmount = rtnProcess.GetPreviousReturnAmount(dbHelp, item.ProcessItemID);
                pc.ProcessAmount = rtnProcess.GetProcessAmount(dbHelp, item.ProcessItemID);
                pc.ProcessCode = item.ProcessCode;
                pc.InvoiceProcessItemID = 0;
                pc.OrderProcessItemID = item.ProcessItemID;
                pc.AmountToReturn = CalculationHelper.GetAmount(pc.ProcessAmount - pc.PreviousReturnAmount);
                pc.OrderID = this.OrderID;
                pc.InvoiceID = this.InvoiceID;
                returnProcessCart.Add(pc);
                id++;
            }

            Session["RETURN_PROCESS_CART"] = returnProcessCart;

            if (this.IsFullReturn || isUpdateAndtransfer)
            {
                //Perform full return & redirect to Refun/Credit screen
                //this.ProcessReturn(dbHelp);

                TotalSummary ts = CalculationHelper.GetOrderTotal(dbHelp, this.OrderID);

                if (ts.TotalAmountReceived > 0)
                {
                    Response.Redirect(string.Format("~/AccountsReceivable/mdReceive.aspx?rcvType={0}&invID={1}&oid={2}&isreturn=1", "refund", this.InvoiceID, this.OrderID), false);
                }
                else
                {
                    this.ProcessReturn(dbHelp);
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgReturnWasSuccessful);
                    Globals.RegisterReloadParentScript(this);
                }
            }
            else //Allow to edit quantity to return
            {
                pnlEditGrid.Visible = true;
            }
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private void InvoiceFullReturn()
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            dbHelp.OpenDatabaseConnection();
            InvoiceReturn rtn = new InvoiceReturn();
            InvoiceProcessReturn rtnProcess = new InvoiceProcessReturn();
            //Invoice ord = new Invoice();
            //ord.PopulateObject(dbHelp, this.InvoiceID);

            InvoiceCartHelper.InitCart(dbHelp, this.InvoiceID);
            List<ReturnCart> returnCart = new List<ReturnCart>();
            List<ReturnProcessCart> returnProcessCart = new List<ReturnProcessCart>();

            int id = 1;
            foreach (var item in InvoiceCartHelper.CurrentCart.Items)
            {
                ReturnCart cart = new ReturnCart();
                cart.ID = id;
                cart.OrderID = 0;
                cart.OrderItemID = 0;
                cart.ProductName = item.ProductName;
                cart.ProductDesc = item.ProductName;
                cart.ProductID = item.ProductID;
                cart.ProductInOrderQty = item.Quantity;
                cart.PreviousReturnedQty = rtn.GetPreviousReturnedQty(dbHelp, item.InvoiceItemID);
                //cart.ProductToReturnQty = cart.ProductInOrderQty - cart.PreviousReturnedQty;
                //if (this.IsFullReturn || IsFullReturn)
                //{
                    cart.ProductToReturnQty = (item.Quantity - cart.PreviousReturnedQty);
                //}
                //else
                //{
                //    cart.ProductToReturnQty = 0;
                //}
                cart.WhsCode = item.WarehouseCode;
                cart.UPCCode = item.UPCCode;
                cart.ProductUnitPrice = item.Price;
                cart.ItemTaxGroup = item.TaxGrp;
                cart.InvoiceID = item.InvoiceID;
                cart.InvoiceItemID = item.InvoiceItemID;
                returnCart.Add(cart);
                id++;
            }
            Session["RETURN_CART"] = returnCart;

            Invoice inv = new Invoice();
            inv.PopulateObject(dbHelp, this.InvoiceID);
            Session["RETURN_CART_CUSTID"] = inv.InvCustID;

            id = 1;
            pnlProcess.Visible = InvoiceCartHelper.CurrentCart.ProcessItems.Count > 0;
            //Set Process Items in cart
            foreach (var item in InvoiceCartHelper.CurrentCart.ProcessItems)
            {
                ReturnProcessCart pc = new ReturnProcessCart();
                pc.ID = id;
                pc.PreviousReturnAmount = rtnProcess.GetPreviousReturnAmount(dbHelp, item.ProcessItemID);
                pc.ProcessAmount = rtnProcess.GetProcessAmount(dbHelp, item.ProcessItemID);
                pc.InvoiceProcessItemID = item.ProcessItemID;
                pc.OrderProcessItemID = 0;
                pc.ProcessCode = item.ProcessCode;
                pc.AmountToReturn = CalculationHelper.GetAmount(pc.ProcessAmount - pc.PreviousReturnAmount);
                pc.OrderID = this.OrderID;
                pc.InvoiceID = this.InvoiceID;
                returnProcessCart.Add(pc);
                id++;
            }

            Session["RETURN_PROCESS_CART"] = returnProcessCart;

            if (this.IsFullReturn || IsFullReturn)
            {
                //Perform full return & redirect to Refun/Credit screen
                //this.ProcessReturn(dbHelp);

                TotalSummary ts = CalculationHelper.GetInvoiceTotal(dbHelp, this.InvoiceID);

                if (ts.TotalAmountReceived > 0)
                {
                    Response.Redirect(string.Format("~/AccountsReceivable/mdReceive.aspx?rcvType={0}&invID={1}&oid={2}&isreturn=1", "refund", this.InvoiceID, this.OrderID), false);
                }
                else
                {
                    this.ProcessReturn(dbHelp);
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgReturnWasSuccessful);
                    //Globals.RegisterCloseDialogScript(Page, string.Format("parent.PrintReturnReceipt('{0}')", Globals.CurrentAppLanguageCode), true);
                    Globals.RegisterReloadParentScript(this);
                }
            }
            else //Allow to edit quantity to return
            {
                pnlEditGrid.Visible = true;
            }
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private void ProcessReturn(DbHelper dbHelp)
    {
        try
        {
            OrderReturn oi = new OrderReturn();
            var lstSessionCart = Session["RETURN_CART"] as List<ReturnCart>;
            var lstReturnCart = Session["RETURN_PROCESS_CART"] as List<ReturnProcessCart>;
            if (lstSessionCart != null)
            {
                if (this.IsTransfer || isUpdateAndtransfer)
                {
                    Orders ord = new Orders();
                    string[] ordIDs = ord.GetOrderIDsSameGroup(BusinessUtility.GetString(this.OrderID));
                    if (ordIDs.Length > 0)
                    {
                        if (BusinessUtility.GetInt(ordIDs[0]) > 0)
                        {
                            oi.ReturnAndUpdateInventory(dbHelp, ordIDs, CurrentUser.UserID, lstSessionCart, lstReturnCart, true, "", "",0);
                        }
                    }
                    else
                    {
                        oi.ReturnAndUpdateInventory(dbHelp, this.OrderID, CurrentUser.UserID, lstSessionCart, lstReturnCart, true, "", "",0);
                    }
                }
                else
                {
                    oi.ReturnAndUpdateInventory(dbHelp, this.OrderID, CurrentUser.UserID, lstSessionCart, lstReturnCart, false, "", "",0);
                }
            }
        }
        catch
        {
            throw;
        }
    }

    private int OrderID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["orderid"], out id);
            return id;
        }
    }

    private int InvoiceID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["invid"], out id);
            return id;
        }
    }

    private bool IsFullReturn
    {
        get
        {
            if (Request.QueryString.AllKeys.Contains("fullReturn"))
            {
                return Request.QueryString["fullReturn"] == "1";
            }
            return false;
        }
    }

    private bool IsTransfer
    {
        get
        {
            if (Request.QueryString.AllKeys.Contains("IsTransfer"))
            {
                return Request.QueryString["IsTransfer"] == "1";
            }
            return false;
        }
    }

    protected void grdProducts_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 7)
        {
            e.CellHtml = string.Format("<input id='rtnQty_{0}' type='text' value='{1}' style='width:70px;'/>", e.RowIndex, e.CellHtml);
            //e.CellHtml = string.Format("<input id='rtnQty_{0}' type='text' value='{1}' style='width:70px;'/>", e.RowIndex, 0);
        }
        else if (e.ColumnIndex == 8)
        {
            e.CellHtml = string.Format(@"<a class=""inbiz_icon icon_save_24x24"" href=""javascript:;"" onclick=""saveReturnedDataInCart('rtnQty_{0}', '{1}')"" >Save</a>", e.RowIndex, e.RowKey);
        }
    }

    protected void grdProducts_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {

        if (Request.QueryString.AllKeys.Contains("editRowKey"))
        {
            var lstFromSession = Session["RETURN_CART"] as List<ReturnCart>;
            int id = 0;
            int qtyToUpdate = 0;
            if (lstFromSession != null && int.TryParse(Request.QueryString["editRowKey"], out id) && id > 0)
            {
                int.TryParse(Request.QueryString["qtyToReturn"], out qtyToUpdate);
                var itemtoUpdate = lstFromSession.Where(i => i.ID == id).FirstOrDefault();
                if (itemtoUpdate != null)
                {
                    if (itemtoUpdate.ProductInOrderQty >= itemtoUpdate.PreviousReturnedQty + qtyToUpdate)
                    {
                        itemtoUpdate.ProductToReturnQty = qtyToUpdate;
                        Session["RETURN_CART"] = lstFromSession;
                        if (this.InvoiceID > 0)
                        {
                            Invoice inv = new Invoice();
                            inv.PopulateObject(this.InvoiceID);
                            Session["RETURN_CART_CUSTID"] = inv.InvCustID;
                        }
                        else
                        {
                            Orders ord = new Orders();
                            ord.PopulateObject(this.OrderID);
                            Session["RETURN_CART_CUSTID"] = ord.OrdCustID;
                        }
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.errContReturnOverQty);
                    }
                }
            }
        }
        grdProducts.DataSource = Session["RETURN_CART"] != null ? Session["RETURN_CART"] : new List<ReturnCart>();
    }

    protected void JQGridTransfer_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 7)
        {
            e.CellHtml = string.Format("<input id='rtnQty_{0}' type='text' value='{1}' style='width:70px;'/>", e.RowIndex, e.CellHtml);
            //e.CellHtml = string.Format("<input id='rtnQty_{0}' type='text' value='{1}' style='width:70px;'/>", e.RowIndex, 0);
        }
        else if (e.ColumnIndex == 8)
        {
            e.CellHtml = string.Format(@"<a class=""inbiz_icon icon_save_24x24"" href=""javascript:;"" onclick=""saveTransferredDataInCart('rtnQty_{0}', '{1}')"" >Save</a>", e.RowIndex, e.RowKey);
        }
    }

    protected void JQGridTransfer_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {

        if (Request.QueryString.AllKeys.Contains("editRowKey"))
        {
            var lstFromSession = Session["RETURN_CART"] as List<ReturnCart>;
            int id = 0;
            int qtyToUpdate = 0;
            if (lstFromSession != null && int.TryParse(Request.QueryString["editRowKey"], out id) && id > 0)
            {
                int.TryParse(Request.QueryString["qtyToReturn"], out qtyToUpdate);
                var itemtoUpdate = lstFromSession.Where(i => i.ID == id).FirstOrDefault();
                if (itemtoUpdate != null)
                {
                    if (itemtoUpdate.ProductInOrderQty >= itemtoUpdate.PreviousReturnedQty + qtyToUpdate)
                    {
                        itemtoUpdate.ProductToReturnQty = qtyToUpdate;
                        Session["RETURN_CART"] = lstFromSession;

                        if (this.InvoiceID > 0)
                        {
                            Invoice inv = new Invoice();
                            inv.PopulateObject(this.InvoiceID);
                            Session["RETURN_CART_CUSTID"] = inv.InvCustID;
                        }
                        else
                        {
                            Orders ord = new Orders();
                            ord.PopulateObject(this.OrderID);
                            Session["RETURN_CART_CUSTID"] = ord.OrdCustID;
                        }
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.errContReturnOverQty);
                    }
                }
            }
        }
        JQGridTransfer.DataSource = Session["RETURN_CART"] != null ? Session["RETURN_CART"] : new List<ReturnCart>();
    }

    protected void btnReturn_Click(object sender, EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            //this.ProcessReturn(dbHelp);

            TotalSummary ts;
            if (this.InvoiceID > 0)
            {
                ts = CalculationHelper.GetInvoiceTotal(dbHelp, this.InvoiceID);
            }
            else
            {
                ts = CalculationHelper.GetOrderTotal(dbHelp, this.OrderID);
            }

            if (ts.TotalAmountReceived > 0)
            {
                Response.Redirect(string.Format("~/AccountsReceivable/mdReceive.aspx?rcvType={0}&invID={1}&oid={2}&isreturn=1&regcode={3}", "refund", this.InvoiceID, this.OrderID, this.POSRegCode), false);
            }
            else
            {
                if (this.IsTransfer)
                {
                    this.ProcessReturn(dbHelp);
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgTransferWasSuccessful);
                    if (BusinessUtility.GetString(Session["InvokeSrc"]).ToUpper() == "POS")
                    {
                        Globals.RegisterCloseDialogScript(Page, string.Format("parent.returnToPOS()"), true);
                    }
                    else
                    {
                        Globals.RegisterReloadParentScript(this);
                    }
                    //Globals.RegisterCloseDialogScript(Page, string.Format("parent.PrintReturnReceipt('{0}')", Globals.CurrentAppLanguageCode), true);
                }
                else
                {
                    if (BusinessUtility.GetString(Session["InvokeSrc"]).ToUpper() == "POS")
                    {

                        Response.Redirect(string.Format("~/AccountsReceivable/mdReceive.aspx?rcvType={0}&invID={1}&oid={2}&isreturn=1&IsRefundOnZeroBalance=1&regcode={3}", "refund", this.InvoiceID, this.OrderID, this.POSRegCode), false);
                        //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgReturnWasSuccessful);
                        ////Globals.RegisterScript(this, string.Format("PrintReturnReceipt({0})",Globals.CurrentAppLanguageCode));
                        //Globals.RegisterCloseDialogScript(Page, string.Format("parent.PrintReturnReceipt('{0}')", Globals.CurrentAppLanguageCode), true);
                        ////Globals.RegisterReloadParentScript(this);

                    }
                    else
                    {
                        Response.Redirect(string.Format("~/AccountsReceivable/mdReceive.aspx?rcvType={0}&invID={1}&oid={2}&isreturn=1&IsRefundOnZeroBalance=1", "refund", this.InvoiceID, this.OrderID), false);

                        //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgAmountRefundedSuccessfully);
                        //Globals.RegisterReloadParentScript(this);
                        //Globals.RegisterCloseDialogScript(Page, string.Format("parent.PrintRcpt('{0}')", Globals.CurrentAppLanguageCode), true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError + ex.Message);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    public string POSRegCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["regcode"]);
        }
    }

    protected void grdProcess_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 4)
        {
            e.CellHtml = string.Format("<input id='rtnProc_{0}' type='text' value='{1}' style='width:70px;'/>", e.RowIndex, e.CellHtml);
        }
        else if (e.ColumnIndex == 5)
        {
            e.CellHtml = string.Format(@"<a class=""inbiz_icon icon_save_24x24"" href=""javascript:;"" onclick=""saveReturnedProcessDataInCart('rtnProc_{0}', '{1}')"" >Save</a>", e.RowIndex, e.RowKey);
        }
    }
    protected void grdProcess_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains("editRowKey2"))
        {
            var lstFromSession = Session["RETURN_PROCESS_CART"] as List<ReturnProcessCart>;
            int id = 0;
            double qtyToUpdate = 0;
            if (lstFromSession != null && int.TryParse(Request.QueryString["editRowKey2"], out id) && id > 0)
            {
                qtyToUpdate = BusinessUtility.GetDouble(Request.QueryString["amtToReturn"]);
                var itemtoUpdate = lstFromSession.Where(i => i.ID == id).FirstOrDefault();
                if (itemtoUpdate != null)
                {
                    if (CalculationHelper.GetAmount(itemtoUpdate.AmountToReturn) >= CalculationHelper.GetAmount(itemtoUpdate.PreviousReturnAmount + qtyToUpdate))
                    {
                        itemtoUpdate.AmountToReturn = qtyToUpdate;
                        Session["RETURN_PROCESS_CART"] = lstFromSession;
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.errContReturnOverQty);
                    }
                }
            }
        }
        grdProcess.DataSource = Session["RETURN_PROCESS_CART"] != null ? Session["RETURN_PROCESS_CART"] : new List<ReturnCart>();
    }

    protected void btnCancel_OnClick(object sender, EventArgs e)
    {
        if (BusinessUtility.GetString(Session["InvokeSrc"]).ToUpper() == "POS")
        {
            Orders ord = new Orders();
            int partID = 0;
            ord.PopulateObject(this.OrderID);
            if (partID <= 0)
            {
                partID = ord.OrdCustID;
            }
            Globals.RegisterCloseDialogScript(Page, string.Format("parent.PrintReturnReceipt('{0}', '{1}', '{2}')", Globals.CurrentAppLanguageCode, 0, partID), true);
        }
        else
        {
            Globals.RegisterCloseDialogScript(this.Page);
        }
    }

    protected void btnUpdateAndTransfer_Click(object sender, EventArgs e)
    {
        btnReturn_Click(sender, e);
        //isUpdateAndtransfer = true;
        //if (this.InvoiceID > 0)
        //{
        //    this.InvoiceFullReturn();
        //}
        //else if (this.OrderID > 0)
        //{
        //    this.OrderFullReturn();
        //}
    }
}
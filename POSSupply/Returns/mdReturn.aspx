﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="mdReturn.aspx.cs" Inherits="Returns_mdReturn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
    <style type="text/css">
        .validate
        {
            background-color: Green;
            background-image: none;
        }
        
        .inValidate
        {
            background-color: Red;
            background-image: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="formContainer">
        <asp:Panel ID="pnlEditGrid" runat="server" Style="padding: 5px;">
            <h3>
                <asp:Label ID="Label1" Text="" runat="server" />
            </h3>
            <div id="grid_wrapperTrans">
                <trirand:JQGrid runat="server" ID="JQGridTransfer" Height="100%" PagerSettings-PageSize="50"
                    AutoWidth="true" OnDataRequesting="JQGridTransfer_DataRequesting" OnCellBinding="JQGridTransfer_CellBinding">
                    <Columns>
                        <trirand:JQGridColumn DataField="ID" HeaderText="ID" Width="50" Visible="false" PrimaryKey="true" />
                        <trirand:JQGridColumn DataField="ProductID" HeaderText="ID" Width="20" />
                        <trirand:JQGridColumn DataField="UPCCode" HeaderText="<%$ Resources:Resource, lblSrchSKU %>" Editable="false" Width="100"
                            Visible="true" />
                        <trirand:JQGridColumn DataField="ProductName" HeaderText="<%$ Resources:Resource, grdPOProductName %>" />
                        <trirand:JQGridColumn DataField="ProductDesc" HeaderText="Description" />
                        <trirand:JQGridColumn DataField="ProductInOrderQty" HeaderText="<%$ Resources:Resource, lblQtyInOrder%>"
                            Width="100" TextAlign="Center" />
                        <trirand:JQGridColumn DataField="PreviousReturnedQty" HeaderText="<%$ Resources:Resource, lblTransferredQty%>"
                            Width="100" TextAlign="Center" />
                        <trirand:JQGridColumn DataField="ProductToReturnQty" HeaderText="<%$ Resources:Resource, lblQtyToTransfer%>"
                            Width="100" TextAlign="Center" />
                        <trirand:JQGridColumn DataField="ID" HeaderText="Save" Editable="false" Width="30"
                            TextAlign="Center" />
                    </Columns>
                    <PagerSettings PageSize="100" PageSizeOptions="[100,150,200]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn="" />
                    <AppearanceSettings AlternateRowBackground="True" />
                    <ClientSideEvents LoadComplete="gridLoadComplete" BeforeRowSelect="beforeRowSelect" />
                </trirand:JQGrid>
            </div>
            <div id="grid_wrapper">
                <trirand:JQGrid runat="server" ID="grdProducts" Height="100%" PagerSettings-PageSize="50"
                    AutoWidth="true" OnDataRequesting="grdProducts_DataRequesting" OnCellBinding="grdProducts_CellBinding">
                    <Columns>
                        <trirand:JQGridColumn DataField="ID" HeaderText="ID" Width="50" Visible="false" PrimaryKey="true" />
                        <trirand:JQGridColumn DataField="ProductID" HeaderText="ID" Width="20" />
                        <trirand:JQGridColumn DataField="UPCCode" HeaderText="<%$ Resources:Resource, lblSrchSKU %>" Editable="false" Width="100"
                            Visible="true" />
                        <trirand:JQGridColumn DataField="ProductName" HeaderText="<%$ Resources:Resource, grdPOProductName %>" />
                        <trirand:JQGridColumn DataField="ProductDesc" HeaderText="Description" />
                        <trirand:JQGridColumn DataField="ProductInOrderQty" HeaderText="Qty In Order" Width="100"
                            TextAlign="Center" />
                        <trirand:JQGridColumn DataField="PreviousReturnedQty" HeaderText="Returned Qty" Width="100"
                            TextAlign="Center" />
                        <trirand:JQGridColumn DataField="ProductToReturnQty" HeaderText="Qty To Return" Width="100"
                            TextAlign="Center" />
                        <trirand:JQGridColumn DataField="ID" HeaderText="Save" Editable="false" Width="30"
                            TextAlign="Center" />
                    </Columns>
                    <PagerSettings PageSize="100" PageSizeOptions="[100,150,200]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn="" />
                    <AppearanceSettings AlternateRowBackground="True" />
                    <ClientSideEvents LoadComplete="gridLoadComplete" BeforeRowSelect="beforeRowSelect" />
                </trirand:JQGrid>
                <asp:Panel ID="pnlProcess" runat="server">
                    <br />
                    <br />
                    <h3>
                        <asp:Label ID="Label2" Text="Process Items" runat="server" />
                    </h3>
                    <trirand:JQGrid runat="server" ID="grdProcess" Height="100%" PagerSettings-PageSize="50"
                        AutoWidth="true" OnCellBinding="grdProcess_CellBinding" OnDataRequesting="grdProcess_DataRequesting">
                        <Columns>
                            <trirand:JQGridColumn DataField="ID" HeaderText="ID" Width="50" Visible="false" PrimaryKey="true" />
                            <trirand:JQGridColumn DataField="ProcessCode" HeaderText="Process Code" Editable="false"
                                Width="50" />
                            <trirand:JQGridColumn DataField="ProcessAmount" HeaderText="Actual Process Amount"
                                DataFormatString="{0:N}" TextAlign="Center" />
                            <trirand:JQGridColumn DataField="PreviousReturnAmount" HeaderText="Returned Amount"
                                DataFormatString="{0:N}" TextAlign="Center" />
                            <trirand:JQGridColumn DataField="AmountToReturn" HeaderText="Amount to Return" DataFormatString="{0:F}"
                                TextAlign="Center" />
                            <trirand:JQGridColumn DataField="ID" HeaderText="Save" Editable="false" Width="30"
                                TextAlign="Center" />
                        </Columns>
                        <PagerSettings PageSize="100" PageSizeOptions="[100,150,200]" />
                        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                            ShowDeleteButton="false" ShowSearchButton="false" />
                        <SortSettings InitialSortColumn="" />
                        <AppearanceSettings AlternateRowBackground="True" />
                        <ClientSideEvents LoadComplete="gridLoadComplete" BeforeRowSelect="beforeRowSelect" />
                    </trirand:JQGrid>
                </asp:Panel>
            </div>
            <div class="div_command">
                <asp:Button ID="btnUpdateAndTransfer" Text="<%$Resources:Resource,lblUpdateAndTransfer%>" runat="server"  CausesValidation="false"
                    OnClick="btnUpdateAndTransfer_Click"  />
                <asp:Button ID="btnValidateSKU" runat="server" Text="<%$Resources:Resource,lblValidateSKU%>"
                    Visible="false" />
                <iCtrl:IframeDialog ID="mdbtnValidateSKU" TriggerControlID="btnValidateSKU" Width="680"
                    Height="400" Url="AddEditCurrency.aspx?jscallback=reloadGrid" Title="<%$Resources:Resource,lblValidateSKU%>"
                    runat="server">
                </iCtrl:IframeDialog>
                <asp:Button ID="btnReturn" Text="" runat="server" OnClientClick="" CausesValidation="false"
                    OnClick="btnReturn_Click" />
                <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
                    CausesValidation="False" OnClick="btnCancel_OnClick" />


                <%--                 <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
                CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />--%>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        var performReturn = false;

        $("#global_message_container").prependTo("#formContainer");

        $grid1 = $("#<%=grdProducts.ClientID%>");
        $grid1.initGridHelper({
            searchPanelID: "pnlSearch",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        $grid3 = $("#<%=JQGridTransfer.ClientID%>");
        $grid3.initGridHelper({
            searchPanelID: "pnlSearch",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapperTrans"
        });

        function jqGridResize1() {
            $("#<%=grdProducts.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
            $("#<%=JQGridTransfer.ClientID%>").jqResizeAfterLoad("grid_wrapperTrans", 0);
        }

        $grid2 = $("#<%=grdProcess.ClientID%>");
        $grid2.initGridHelper({
            searchPanelID: "pnlSearch",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize2() {
            $("#<%=grdProcess.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }




        function reloadGrid(event, ui) {
            $grid1.trigger("reloadGrid");
            jqGridResize1();
            jqGridResize2();
        }

        function gridLoadComplete(data) {
            jqGridResize1();
            jqGridResize2();

            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        //prevent jq grid from row selection
        function beforeRowSelect(rowid, e) {
            return false;
        }

        function saveReturnedDataInCart(eleId, key) {
            $inp = $("#" + eleId);

            var dataToPost = {};
            dataToPost.editRowKey = key;
            dataToPost.qtyToReturn = $inp.val();

            for (var k in dataToPost) {
                $grid1.setPostDataItem(k, dataToPost[k]);
            }

            $grid1.trigger("reloadGrid");

            for (var k in dataToPost) {
                $grid1.removePostDataItem(k);
            }
        }

        function saveTransferredDataInCart(eleId, key) {
            $inp = $("#" + eleId);

            var dataToPost = {};
            dataToPost.editRowKey = key;
            dataToPost.qtyToReturn = $inp.val();

            for (var k in dataToPost) {
                $grid3.setPostDataItem(k, dataToPost[k]);
            }

            $grid3.trigger("reloadGrid");

            for (var k in dataToPost) {
                $grid3.removePostDataItem(k);
            }
        }

        function saveReturnedProcessDataInCart(eleId, key) {
            $inp = $("#" + eleId);

            var dataToPost = {};
            dataToPost.editRowKey2 = key;
            dataToPost.amtToReturn = $inp.val();

            for (var k in dataToPost) {
                $grid2.setPostDataItem(k, dataToPost[k]);
            }

            $grid2.trigger("reloadGrid");

            for (var k in dataToPost) {
                $grid2.removePostDataItem(k);
            }
        }
        $(document).ready(function () {
            $("#<%=btnUpdateAndTransfer.ClientID%>").hide();
            //            var s = ($("#jqgh_PreviousReturnedQty").html()).replace("Qty To Return", "Qty To Transfrer");
            //            $("#jqgh_PreviousReturnedQty").html(s);
        });

        function ValidateSKU(validSKU) {
            if (validSKU == "1") {
                $("#<%=btnValidateSKU.ClientID%>").addClass("validate");
                $("#<%=btnValidateSKU.ClientID%>").hide();
                $("#<%=btnUpdateAndTransfer.ClientID%>").show();
                $("#<%=btnUpdateAndTransfer.ClientID%>").addClass("validate");
                $("#<%=btnReturn.ClientID%>").hide();
                $grid3.trigger("reloadGrid");
            }
            else {
                $("#<%=btnValidateSKU.ClientID%>").addClass("inValidate");
            }
        }

    </script>
</asp:Content>

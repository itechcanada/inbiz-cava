﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;


public partial class Returns_mdReturnHistory : BasePage
{
    OrderReturn rtn = new OrderReturn();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdProducts))
        {

        }
        if (this.IsTransfer)
        {
            pnlProcess.Visible = false;
            grdProducts.Visible = false;
            grdProdTransfer.Visible = true;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (this.IsTransfer)
        {
            Label1.Text = Resources.Resource.lblReturnTransfer;
        }
        else
        {
            Label1.Text = Resources.Resource.lblReturnHistory;
        }

    }

    protected void grdProducts_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 5)
        {
            int ordItemID = 0;
            if (int.TryParse(e.CellHtml, out ordItemID) && ordItemID > 0)
            {
                //OrderItems oi 
            }
            //e.CellHtml = 
        }
        if (e.ColumnIndex == 8)
        {
            if (rtn.HasAccountReceivableID(null, BusinessUtility.GetInt(e.RowValues[0]), this.OrderID, this.InvoiceID))
            {
                e.CellHtml = string.Format("<a  onclick='printReceipt({0},{1})'  href=\"#\">{2}</a>", this.OrderID,e.RowValues[8], Resources.Resource.grdPrint);
            }
            else {
                e.CellHtml = "";
            }
        }
    }

    protected void grdProducts_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        
        grdProducts.DataSource = rtn.GetReturnHistoryItems(null, this.OrderID, this.InvoiceID);

        grdProdTransfer.DataSource = rtn.GetReturnHistoryItems(null, this.OrderID, this.InvoiceID);
    }

    public int OrderID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["oid"], out id);
            return id;
        }
    }

    public int InvoiceID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["invid"], out id);
            return id;
        }
    }

    private bool IsTransfer
    {
        get
        {
            if (Request.QueryString.AllKeys.Contains("IsTransfer"))
            {
                return Request.QueryString["IsTransfer"] == "1";
            }
            return false;
        }
    }

    protected void grdProcess_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {

    }
    protected void grdProcess_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        OrderReturn rtn = new OrderReturn();
        grdProcess.DataSource = rtn.GetReturnHistoryProcessItems(null, this.OrderID, this.InvoiceID);
    }
}
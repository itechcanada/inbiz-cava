﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Returns_ViewReturns : BasePage
{
    private string strSOID = "";
    private Orders _objOrder = new Orders();
    int _currentRowIdx = -1;
    TotalSummary _lTotal;
    // On Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        //Security
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.SALES_MANAGER) && !CurrentUser.IsInRole(RoleID.SALES_REPRESENTATIVE))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        //Set JqGrid variables
        //jqGridHelperScript.SetVariables(jgdvSalesOrderView.ClientID, "grid_wrapper", SearchPanel.ClientID, "btnSearch", 0);

        if (!IsPagePostBack(jgdvSalesOrderView))
        {
            btnBack.Visible = Request.QueryString.AllKeys.Contains("returnUrl");

            txtSearch.Text = Convert.ToString(Request.QueryString["searchText"]);
            ddlSearch.SelectedValue = Convert.ToString(Request.QueryString["searchOn"]);
            string backUrl = "";
            string strurl = "";
            //Set navigate url to transfer on order detail page to process return
            //string backUrl = ResolveUrl(string.Format("~/Returns/ViewReturns.aspx?searchOn={0}&searchText={1}", Request.QueryString["searchOn"], Request.QueryString["searchText"]));
            if (Request.QueryString.AllKeys.Contains("InvokeSrc") && Request.QueryString["InvokeSrc"] == "POS" && Request.QueryString.AllKeys.Contains("isReturn") && Request.QueryString["isReturn"] == "1")
            {
                backUrl = ResolveUrl("~/Returns/Default.aspx");
                strurl = ResolveUrl(string.Format("~/Sales/ViewOrderDetails.aspx?isreturn=1&returnUrl={0}&InvokeSrc={1}&regcode={2}", Server.UrlEncode(backUrl), "POS", this.POSRegCode));
            }
            else if (Request.QueryString.AllKeys.Contains("InvokeSrc") && Request.QueryString["InvokeSrc"] == "POS" && Request.QueryString.AllKeys.Contains("isTransfer") && Request.QueryString["isTransfer"] == "1")
            {
                backUrl = ResolveUrl("~/Returns/Default.aspx");
                strurl = ResolveUrl(string.Format("~/Sales/ViewOrderDetails.aspx?isTransfer=1&returnUrl={0}&InvokeSrc={1}", Server.UrlEncode(backUrl),"POS"));
            }
            else
            {
                 backUrl = ResolveUrl("~/Returns/Default.aspx");
                 strurl = ResolveUrl(string.Format("~/Sales/ViewOrderDetails.aspx?isreturn=1&returnUrl={0}", Server.UrlEncode(backUrl)));
            }
            hdnLinkToNavigateForSingleRecord.Value = strurl;

            //DropDownHelper.FillDropdown(ddlSearch, "SO", "dlSh1", Globals.CurrentAppLanguageCode, null);
            //DropDownHelper.FillDropdown(ddlSOStatus, "SO", "SOSts", Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.liSelectOrderStatus, ""));
            txtSearch.Focus();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

    }

    protected void jgdvSalesOrderView_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        int idxDsOrdStatus = 6;

        if (_currentRowIdx != e.RowIndex)
        {
            _currentRowIdx = e.RowIndex;
            _lTotal = CalculationHelper.GetOrderTotal(BusinessUtility.GetInt(e.RowValues[0]));
        }

        if (e.ColumnIndex == 3)
        {
            e.CellHtml = string.Format("{0:F}", _lTotal.GrandTotal);
        }
        else if (e.ColumnIndex == 4)
        {
            //To to get Paid Amout                                               
            e.CellHtml = string.Format("{0:F}", _lTotal.TotalAmountReceived);
        }
        else if (e.ColumnIndex == 5)
        {
            e.CellHtml = string.Format("{0:F}", _lTotal.OutstandingAmount);
        }
        else if (e.ColumnIndex == 6)
        {

        }
        else if (e.ColumnIndex == 8)
        {
            string strData = "";
            switch (e.CellHtml.ToString())
            {
                case SOStatus.NEW:
                    strData = Resources.Resource.liCreated;
                    break;
                case SOStatus.APPROVED:
                    strData = Resources.Resource.liApproved;
                    break;
                case SOStatus.IN_PROGRESS:
                    strData = Resources.Resource.liInProcess;
                    break;
                case SOStatus.HELD:
                    strData = Resources.Resource.liHeld;
                    break;
                case SOStatus.SHIPPED:
                    strData = Resources.Resource.liShipped;
                    break;
                case SOStatus.SHIPPING_HELD:
                    strData = Resources.Resource.liShippingHeld;
                    break;
                case SOStatus.INVOICED:
                    strData = Resources.Resource.liInvoiced;
                    break;
                case SOStatus.CLOSED:
                    strData = Resources.Resource.liClosed;
                    break;
                case SOStatus.CLOSED_INVOICED:
                    strData = Resources.Resource.liClosedInvoiced;
                    break;
                case SOStatus.READY_FOR_SHIPPING:
                    strData = Resources.Resource.liReadyForShipping;
                    break;
                case SOStatus.RESERVATION_CANCELED:
                    strData = "Reservation Canceled";
                    break;
            }
            e.CellHtml = strData;
        }
        else if (e.ColumnIndex == 11)
        {
            //string backUrl = ResolveUrl(string.Format("~/Returns/ViewReturns.aspx?searchOn={0}&searchText={1}", Request.QueryString["searchOn"], Request.QueryString["searchText"]));
            string backUrl = ResolveUrl("~/Returns/Default.aspx");
            string strurl = ResolveUrl(string.Format("~/Sales/ViewOrderDetails.aspx?SOID={0}&isreturn=1&returnUrl={1}&regcode={2}", e.RowKey, Server.UrlEncode(backUrl), this.POSRegCode));
            e.CellHtml = string.Format("<a href=\"{0}\">{1}</a>", strurl, Resources.Resource.lblEdit);
        }
    }

    protected void jgdvSalesOrderView_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        bool isSalesRescricted = CurrentUser.IsInRole(RoleID.SALES_REPRESENTATIVE) && BusinessUtility.GetBool(Session["SalesRepRestricted"]);        

        if (Request.QueryString.AllKeys.Contains("_history"))
        {
            sqldsSalesOrderView.SelectCommand = _objOrder.GetSalesToReturnSql(sqldsSalesOrderView.SelectParameters, Request.QueryString[ddlSearch.ClientID], Request.QueryString[txtSearch.ClientID], CurrentUser.UserID, isSalesRescricted);
        }
        else
        {
            sqldsSalesOrderView.SelectCommand = _objOrder.GetSalesToReturnSql(sqldsSalesOrderView.SelectParameters, Request.QueryString["searchOn"], Request.QueryString["searchText"], CurrentUser.UserID, isSalesRescricted);
        }
    }

    private int PartnerID
    {
        get
        {
            int partnerId = 0;
            int.TryParse(Request.QueryString["PartnerID"], out partnerId);
            return partnerId;
        }
    }

    public string POSRegCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["regcode"]);
        }
    }

    protected void imgCsv_Click(object sender, EventArgs e)
    {
        jgdvSalesOrderView.ExportToExcel("export.xls");
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.QueryString["returnUrl"]);
    }

    protected void jgdvSalesOrderView_PreRender(object sender, EventArgs e)
    {

    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/fullWidth.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Returns_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <style type="text/css">
        input[type=submit]{height:50px;}
        input[type=text], select {padding:5px; font-size:16px;}        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <h4>
        <asp:Literal ID="ltReturn" Text="<%$Resources:Resource, lblReturn%>" runat="server" />        
    </h4>
    <div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" colspan="2">
                    <asp:TextBox runat="server" Width="195px" ID="txtSearch" ValidationGroup="PrdSrch"
                        AutoPostBack="false"> </asp:TextBox><span class="style1"> *</span>
                    <asp:RequiredFieldValidator ID="reqvalSearch" runat="server" ControlToValidate="txtSearch"
                        ErrorMessage="<%$ Resources:Resource, PlzEntSearchCriteria %>" ValidationGroup="PrdSrch"
                        SetFocusOnError="true" Display="None"> </asp:RequiredFieldValidator>
                    <asp:Panel ID="pnlHide" runat="server" Visible="false">
                        <asp:DropDownList ID="dlWarehouse" runat="server" Width="280px" ValidationGroup="PrdSrch">
                        </asp:DropDownList>
                        <asp:CustomValidator ID="custvalPOWarehouse" runat="server" SetFocusOnError="true"
                            ErrorMessage="<%$ Resources:Resource, custvalPOWarehouse%>" ValidationGroup="PrdSrch"
                            ClientValidationFunction="funCheckWarehouseCode" Display="None"> </asp:CustomValidator>
                        <asp:CustomValidator ID="custvalWarehouse" runat="server" SetFocusOnError="true"
                            ErrorMessage="<%$ Resources:Resource, custvalPOWarehouse%>" ValidationGroup="Srch"
                            ClientValidationFunction="funCheckWarehouseCode" Display="None"> </asp:CustomValidator></asp:Panel>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <table align="center" cellpadding="5" cellspacing="5">
                        <tr>
                            <td>
                                <asp:Button ID="btnOrderNO" runat="server" CausesValidation="false" 
                                    Text="<%$ Resources:Resource,cmdCssOrderNo%>" Width="150px" 
                                    OnCommand="btnOrderNO_Command" CommandArgument="ON" />
                            </td>
                             <td>
                                <asp:Button ID="btnCustomerID" runat="server" CausesValidation="false" Text="<%$ Resources:Resource,lblCustomerID%>" CommandArgument="CI" Width="150px"  OnCommand="btnOrderNO_Command"  />
                            </td>
                            <td>
                                <asp:Button ID="btnCustomerName" runat="server" CausesValidation="false" Text="<%$ Resources:Resource,cmdCssCustomerName%>" CommandArgument="CN" Width="150px"  OnCommand="btnOrderNO_Command" />
                            </td>
                            <td>
                                <asp:Button ID="btnInvoiceNo" runat="server" CausesValidation="false" Text="<%$ Resources:Resource,lblINInvoiceNo%>" CommandArgument="IN" Width="150px"  OnCommand="btnOrderNO_Command" />
                            </td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="10" colspan="2">
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsShipping" runat="server" ShowMessageBox="false" ShowSummary="false"
            ValidationGroup="PrdSrch" />
        <asp:ValidationSummary ID="valsShippingW" runat="server" ShowMessageBox="false" ShowSummary="false"
            ValidationGroup="Srch" />
    </div>
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        function funCheckWarehouseCode(source, args) {
            if (document.getElementById('<%=dlWarehouse.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
        function doClick(buttonName, e) {
            //the purpose of this function is to allow the enter key to 
            //point to the correct button to click.
            var key;

            if (window.event)
                key = window.event.keyCode;     //IE
            else
                key = e.which;     //firefox

            if (key == 13) {
                //Get the button the user wants to have clicked
                var btn = document.getElementById(buttonName);
                if (btn != null) { //If we find the button click it
                    btn.click();
                    event.keyCode = 0
                }
            }
        }

        function jqGridResize() {
        }
    </script>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdReturnHistory.aspx.cs" Inherits="Returns_mdReturnHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div style="padding: 5px;">
        <h3>
            <asp:Label ID="Label1" Text=" " runat="server" />
        </h3>
        <div id="grid_wrapper">
            <trirand:JQGrid runat="server" ID="grdProducts" Height="100%" PagerSettings-PageSize="50"
                AutoWidth="true" OnDataRequesting="grdProducts_DataRequesting"
                OnCellBinding="grdProducts_CellBinding">
                <Columns>
                    <trirand:JQGridColumn DataField="ReturnID" HeaderText="ID" Width="50" Visible="false"
                        PrimaryKey="true" />
                    <trirand:JQGridColumn DataField="ReturnProductID" HeaderText="ID" Width="20" />
                    <trirand:JQGridColumn DataField="ProductUPCCode" HeaderText="UPC" Editable="false" Width="100" />
                    <trirand:JQGridColumn DataField="ProductName" HeaderText="<%$ Resources:Resource, grdPOProductName %>" />
                    <trirand:JQGridColumn DataField="ReturnProductQty" HeaderText="<%$ Resources:Resource, grdQtyReturned %>" Width="100"
                        TextAlign="Center" />
                    <trirand:JQGridColumn DataField="ReturnAmount" HeaderText="<%$ Resources:Resource, grdReturnedAmount %>" Editable="false"
                        Width="100" TextAlign="Right" DataFormatString="{0:F}" />
                    <trirand:JQGridColumn DataField="ReturnDate" HeaderText="<%$ Resources:Resource, grdReturnedData %>" Width="100"
                        TextAlign="Center" />
                    <trirand:JQGridColumn DataField="ReturnBy" HeaderText="<%$ Resources:Resource, grdReturnedBy %>" Width="100" TextAlign="Center" />
                    <trirand:JQGridColumn DataField="AccountReceivableID" HeaderText="<%$ Resources:Resource, grdPrint %>" Width="100" TextAlign="Center" />

                </Columns>
                <PagerSettings PageSize="100" PageSizeOptions="[100,150,200]" />
                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                    ShowDeleteButton="false" ShowSearchButton="false" />
                <SortSettings InitialSortColumn="" />
                <AppearanceSettings AlternateRowBackground="True" />
                <ClientSideEvents LoadComplete="gridLoadComplete" BeforeRowSelect="beforeRowSelect" />
            </trirand:JQGrid>

            <trirand:JQGrid runat="server" ID="grdProdTransfer" Height="100%" PagerSettings-PageSize="50"
                AutoWidth="true" OnDataRequesting="grdProducts_DataRequesting"
                OnCellBinding="grdProducts_CellBinding" Visible="false">
                <Columns>
                    <trirand:JQGridColumn DataField="ReturnID" HeaderText="ID" Width="50" Visible="false"
                        PrimaryKey="true" />
                    <trirand:JQGridColumn DataField="ReturnProductID" HeaderText="ID" Width="20" />
                    <trirand:JQGridColumn DataField="ProductUPCCode" HeaderText="UPC" Editable="false" Width="100" />
                    <trirand:JQGridColumn DataField="ProductName" HeaderText="<%$ Resources:Resource, grdPOProductName %>" />
                    <trirand:JQGridColumn DataField="ReturnProductQty" HeaderText="<%$ Resources:Resource, grdQtyTransferred %>" Width="100"
                        TextAlign="Center" />
                    <trirand:JQGridColumn DataField="ReturnAmount" HeaderText="Returned Amount" Editable="false"
                        Width="100" TextAlign="Right" DataFormatString="{0:F}" Visible="false" />
                    <trirand:JQGridColumn DataField="ReturnDate" HeaderText="<%$ Resources:Resource, grdTranferData %>" Width="100"
                        TextAlign="Center" />
                    <trirand:JQGridColumn DataField="ReturnBy" HeaderText="<%$ Resources:Resource, grdTransferBy %>" Width="100" TextAlign="Center" />
                </Columns>
                <PagerSettings PageSize="100" PageSizeOptions="[100,150,200]" />
                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                    ShowDeleteButton="false" ShowSearchButton="false" />
                <SortSettings InitialSortColumn="" />
                <AppearanceSettings AlternateRowBackground="True" />
                <ClientSideEvents LoadComplete="gridLoadComplete" BeforeRowSelect="beforeRowSelect" />
            </trirand:JQGrid>

            <asp:Panel ID="pnlProcess" runat="server">
                <br />
                <br />
                <trirand:JQGrid runat="server" ID="grdProcess" Height="100%" PagerSettings-PageSize="50"
                    AutoWidth="true" OnDataRequesting="grdProcess_DataRequesting"
                    OnCellBinding="grdProcess_CellBinding">
                    <Columns>
                        <trirand:JQGridColumn DataField="ReturnID" HeaderText="ID" Width="50" Visible="false"
                            PrimaryKey="true" />
                        <trirand:JQGridColumn DataField="ProcessCode" HeaderText="Process Code" Width="20" />
                        <trirand:JQGridColumn DataField="ReturnAmount" HeaderText="Returned Amount" Editable="false"
                            Width="100" TextAlign="Center" DataFormatString="{0:F}" />
                        <trirand:JQGridColumn DataField="ReturnDate" HeaderText="Returned Date" Width="100"
                            TextAlign="Center" />
                        <trirand:JQGridColumn DataField="ReturnBy" HeaderText="By" Width="100" TextAlign="Center" />
                    </Columns>
                    <PagerSettings PageSize="100" PageSizeOptions="[100,150,200]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn="" />
                    <AppearanceSettings AlternateRowBackground="True" />
                    <ClientSideEvents LoadComplete="gridLoadComplete" BeforeRowSelect="beforeRowSelect" />
                </trirand:JQGrid>
            </asp:Panel>
        </div>
        <div class="div_command">
            <%--<asp:Button ID="btnReturn" Text="<%$Resources:Resource,lblReturn1%>" runat="server"
            OnClientClick="" CausesValidation="false" OnClick="btnReturn_Click" />--%>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        var performReturn = false;

        $("#global_message_container").prependTo("#formContainer");

        $grid1 = $("#<%=grdProducts.ClientID%>");
        $grid1.initGridHelper({
            searchPanelID: "pnlSearch",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize1() {
            $("#<%=grdProducts.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }


        $grid3 = $("#<%=grdProdTransfer.ClientID%>");
        $grid3.initGridHelper({
            searchPanelID: "pnlSearch",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize3() {
            $("#<%=grdProdTransfer.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }


        $grid2 = $("#<%=grdProducts.ClientID%>");
        $grid2.initGridHelper({
            searchPanelID: "pnlSearch",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize2() {
            $("#<%=grdProcess.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }


        function reloadGrid(event, ui) {
            $grid1.trigger("reloadGrid");
            jqGridResize1();

        }

        function gridLoadComplete(data) {
            jqGridResize1();
            jqGridResize2();
            jqGridResize3();
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        //prevent jq grid from row selection
        function beforeRowSelect(rowid, e) {
            return false;
        }

        function printReceipt(printOrdID,accRcbID) {
            window.open('../Common/Print.aspx?ReqID=' + printOrdID + '&AccRcbID=' + accRcbID + '&DocTyp=RtnRcpt', 'Printing Receipt', 'width=300,height=300,toolbar=0,menubar=0,location=0,scrollbars=0,resizable=0,directories=0,status=0');
        }
    </script>
</asp:Content>


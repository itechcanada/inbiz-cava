﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ViewReturns.aspx.cs" Inherits="Returns_ViewReturns" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="cntTop" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>            
            <asp:Literal ID="Literal2" Text="<%$Resources:Resource, lblAdministration%>" runat="server" /> 
        </h1>
        <b>            
            <asp:Literal ID="Literal1" Text="<%$Resources:Resource, lblView%>" runat="server" /> :
            <asp:Literal ID="ltReturn" Text="<%$Resources:Resource, lblReturn%>" runat="server" /> 
        </b>
    </div>
    <div style="float: left;">
        
    </div>
    <div style="float: right;">
        <asp:LinkButton ID="imgCsv" runat="server" CssClass="inbiz_icon icon_export_28" 
            Text="Export To Excel" onclick="imgCsv_Click" />
        <asp:LinkButton ID="lnkAddEditVendor" runat="server" Visible="false"></asp:LinkButton>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="cntMaster" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="jgdvSalesOrderView" DataSourceID="sqldsSalesOrderView"
                    Height="300px" AutoWidth="true" 
                    OnCellBinding="jgdvSalesOrderView_CellBinding" 
                    OnDataRequesting="jgdvSalesOrderView_DataRequesting" 
                    onprerender="jgdvSalesOrderView_PreRender">
                    <Columns>
                        <trirand:JQGridColumn DataField="ordID" HeaderText="<%$ Resources:Resource, grdSOOrderNo %>"
                            PrimaryKey="True" />
                        <trirand:JQGridColumn DataField="PartnerAcronyme" HeaderText="<%$ Resources:Resource, grdCMAcronyme %>"
                            Editable="false" Visible="false" />
                        <trirand:JQGridColumn DataField="CustomerName" HeaderText="<%$ Resources:Resource, grdSOCustomerName %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="amount" HeaderText="<%$ Resources:Resource, POTotalAmount %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="ordID" HeaderText="Amount Paid"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="ordID" HeaderText="Total Balance"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="ordID" HeaderText="CheckIn-CheckOut" Editable="false" Visible="false" />                        
                        <trirand:JQGridColumn DataField="ordDate" HeaderText="<%$ Resources:Resource, grdSOCreatedDate %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="ordStatus" HeaderText="<%$ Resources:Resource, grdPOStatus %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="orderTypeDesc" HeaderText="<%$ Resources:Resource, grduserOderType %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="orderRejectReason" HeaderText="<%$ Resources:Resource, grdRejectedReason %>"
                            Editable="false" Visible="false"/>
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="ordID"
                            Sortable="false" TextAlign="Center" Width="80" />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents LoadComplete="loadComplete" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sqldsSalesOrderView" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
                <asp:HiddenField ID="hdnLinkToNavigateForSingleRecord" runat="server" />
            </div>                        
            <br />
            <div class="div_command">                                
                <asp:Button ID="btnBack" Text="<%$Resources:Resource, lblGoBack%>" 
                    CausesValidation="false" runat="server" onclick="btnBack_Click" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntLeft" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        Search form</div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"
                        AssociatedControlID="ddlSearch" CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlSearch" runat="server" Width="175px">
                        <%--<asp:ListItem Text="[All]" Value="" />--%>
                        <asp:ListItem Value="ON" Text="Order No" />
                        <asp:ListItem Value="CI" Text="Customer ID" />
                        <asp:ListItem Value="CN" Text="Customer Name" />
                        <asp:ListItem Value="IN" Text="Invoice No" />
                    </asp:DropDownList>
                </div>                
                <h4>
                    <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"
                        CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="Search" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="cphScripts" runat="Server">        
    <script type="text/javascript">
        $grid = $("#<%=jgdvSalesOrderView.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=jgdvSalesOrderView.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        function loadComplete(data) {
            if (data.rows.length == 1 && data.total == 1) {                
                location.href = $("#<%=hdnLinkToNavigateForSingleRecord.ClientID%>").val() + "&SOID=" + data.rows[0].id;
            }
            else {
                jqGridResize();
            }
        }
        
    </script>
</asp:Content>


﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="logout.aspx.cs" Inherits="logout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>  
    <script type="text/javascript">
        function loadLoginPage() {
            if (window != window.top) {
                parent.location.href = document.getElementById('hlLogin').getAttribute('href');
            }
            else {
                location.href = document.getElementById('hlLogin').getAttribute('href');
            }
        }
    </script>  
</head>
<body style="margin: auto;" onload="loadLoginPage();">
    <form id="form1" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td style="height:400px; text-align:center;">
                <h2 style="color:#af3333; border-bottom:2px solid #af3333; text-align:center;">Session has been Expired!</h2>
                Redirecting to login page...... please wait....!
                <br />
                Or
                <br />
                <asp:HyperLink ID="hlLogin" NavigateUrl="http://localhost:7103/InbizWeb4.0/AdminLogin.aspx" Text="Click here" runat="server" onclick="loadLoginPage(); return false;" /> to login!                
            </td>
        </tr>
    </table>    
    </form>    
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestImport.aspx.cs" Inherits="TestImport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 20%;
        }
        .style2
        {
            width: 3%;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <select onchange="funTest(this);">
            <option value="value">text</option>
        </select>
    </div>
    <table width="100%" class="table">
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblCSV" runat="server" CssClass="lblBold" Text="Select CSV File"></asp:Label>&nbsp;&nbsp;
                    </td>
                    <td class="style1">
                        <asp:FileUpload ID="CSVFile" runat="server" style="margin-left: 0px" />
                    </td>
                    <%--<td class="style3">
                        <asp:Label ID="Label1" runat="server" CssClass="lblBold" Text="Select Folder To Save"></asp:Label>&nbsp;&nbsp;
                    </td>
                   <td >
                        <asp:FileUpload ID="XLSXFile" runat="server" />
                        </td>--%>
                </tr>
                <tr>
                <td colspan="2" align="left">
                <asp:Button ID="btnConvertFile" runat="server" Text="Convert" 
                        onclick="btnConvertFile_Click" />
                </td>
                </tr>
                </table>
    </form>
</body>
</html>

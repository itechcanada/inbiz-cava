﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;

public partial class POS_ManageRegisterNew : BasePage
{
    clsPosRegisterTrans objRegTra = new clsPosRegisterTrans();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (BusinessUtility.GetString(Session["LoginID"]) == "" || BusinessUtility.GetString(Session["UserModules"]) == "")
        {
            Response.Redirect("~/AdminLogin.aspx");
        }

        if (BusinessUtility.GetString(Session["RegCode"]) == "")
        {
            Response.Redirect("~/Admin/selectRegister.aspx");
        }
        lblMsg.Text = "";
        lblMsg.ForeColor = System.Drawing.Color.Green;

        if (!IsPostBack)
        {

            lblHeading.Text = Resources.Resource.PosReprintHeading;
            lblPopupheading.Text = Resources.Resource.lblSetStartAmount;
            lblHeading.Text = Resources.Resource.msgManageRegister;


            DropDownHelper.FillPosRegister(ddlRegister, new ListItem(Resources.Resource.All, ""));
            foreach (ListItem li in ddlRegister.Items)
            {
                if (li.Value == BusinessUtility.GetString(Session["RegCode"]))
                {
                    li.Selected = true;
                }
            }

            CmdSetAmount.Attributes.Add("onclick", "return funSetAmount();");
        }
    }


    protected void jgdvCurrency_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 11)
        {
            e.CellHtml = String.Format("<a style='text-decoration:none;' href='#' onclick='AddEditRegister({0})' >Edit</a>", BusinessUtility.GetInt(e.RowKey));
        }
    }

    protected void jgdvCurrency_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        //if (Request.QueryString.AllKeys.Contains("InvokeSrc") && Request.QueryString["InvokeSrc"] == "POS")
        //{
        //    txtSearch.Text = BusinessUtility.GetString(Session["RegCode"]);
        //}

        if (Request.QueryString.AllKeys.Contains("_history"))
        {
            objRegTra.Status = BusinessUtility.GetString(Request.QueryString[ddlSearch.ClientID]);
            objRegTra.RegisterCode = BusinessUtility.GetString(Request.QueryString[ddlRegister.ClientID]);
            sqlsdviewRegTran.SelectCommand = objRegTra.funFillGrid();

            //sdsInvoices.SelectCommand = _inv.GetInvoiceSql(sdsInvoices.SelectParameters, Request.QueryString[ddlSearch.ClientID], Request.QueryString[ddlSOStatus.ClientID], Request.QueryString[txtSearch.ClientID], this.PartnerID, CurrentUser.UserID, isSalesRescricted);
            //grdInvoices.DataSource = _inv.GetInvoice(Request.QueryString[ddlSearch.ClientID], Request.QueryString[ddlSOStatus.ClientID], Request.QueryString[txtSearch.ClientID], this.PartnerID, CurrentUser.UserID, isSalesRescricted);
        }
        else
        {
            objRegTra.Status = BusinessUtility.GetString(ddlSearch.SelectedItem.Value);
            objRegTra.RegisterCode = BusinessUtility.GetString(ddlRegister.SelectedItem.Value);
            sqlsdviewRegTran.SelectCommand = objRegTra.funFillGrid();
            //if (Request.QueryString.AllKeys.Contains("InvokeSrc") && Request.QueryString["InvokeSrc"] == "POS")
            //{
            //    grdInvoices.DataSource = _inv.GetInvoice("RC", ddlSOStatus.SelectedValue, txtSearch.Text, this.PartnerID, CurrentUser.UserID, isSalesRescricted);
            //}
            //if (Request.QueryString.AllKeys.Contains("custid") && (!string.IsNullOrEmpty(Request.QueryString["custid"])))
            //{
            //    Partners prt = new Partners();
            //    prt.PopulateObject(BusinessUtility.GetInt(Request.QueryString["custid"]));
            //    grdInvoices.DataSource = _inv.GetInvoice("CN", ddlSOStatus.SelectedValue, prt.PartnerLongName, this.PartnerID, CurrentUser.UserID, isSalesRescricted);
            //}
            //else
            //{
            //    grdInvoices.DataSource = _inv.GetInvoice(ddlSearch.SelectedValue, ddlSOStatus.SelectedValue, txtSearch.Text, this.PartnerID, CurrentUser.UserID, isSalesRescricted);
            //}
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
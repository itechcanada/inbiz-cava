<%@ Page Language="VB" MasterPageFile="~/POSMaster.master" AutoEventWireup="false"
    CodeFile="ManageRegister.aspx.vb" Inherits="POS_ManageRegister" %>

<%@ Import Namespace=" Resources.Resource" %>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divModuleAlert').corner();
        $('#divAlertContent').corner();
        $('#divMainContainerTitle').corner();
    </script>
    <div id="divModuleAlert" class="divSectionTitle">
        <h2>
            <%=Resources.Resource.lblSearchOptions%>
        </h2>
    </div>
    <div id="divAlertContent" class="divSectionContent">
    </div>
</asp:Content>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <style type="text/css">
        .hide
        {
            border: 0;
            clip: rect(0 0 0 0);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px;
        }
    </style>
    <link href="../lib/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../lib/css/main.css" rel="stylesheet" type="text/css" />
    <link href="../lib/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="../lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <link href="../lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <link href="../lib/scripts/chosen.jquery/chosen.css" rel="stylesheet" type="text/css" />
    <script src="../lib/scripts/jquery-plugins/jquery.numeric.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-plugins/jquery.addplaceholder.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();
    </script>
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2 style="border: none;">
                        <asp:Literal runat="server" ID="lblHeading"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                </td>
                <td style="width: 150px;">
                </td>
            </tr>
        </table>
    </div>
    <div class="divMainContent">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                    <span id="spnlblMsg" runat="server" style="color: Green; font-weight: bold;"></span>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="right">
                    <table width="100%">
                        <tr>
                            <td width="80%">
                            </td>
                            <td width="10%">
                                <div class="buttonwrapper">
                                    <a id="A1" runat="server" class="ovalbutton" href="POS.aspx"><span class="ovalbutton"
                                        style="min-width: 55px; text-align: center;">
                                        <%=Resources.Resource.lblBack%></span></a></div>
                            </td>
                            <td width="10%">
                                <div class="buttonwrapper">
                                    <a id="CmdSetAmount" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                        style="min-width: 55px; text-align: center;">
                                        <%=Resources.Resource.CmdCssAdd%></span></a></div>
                            </td>
                        </tr>
                    </table>
                    <%--<asp:ImageButton runat=server ID=imgSetAmount  ImageUrl="~/images/Add.png"/>--%>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table width="100%;">
                        <tr>
                            <td valign="top">
                                <asp:Panel runat="server" ID="SearchPanel">
                                    <div class="searchBar" style="padding-right: 2px;">
                                        <h4>
                                            <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lblStatus%>"
                                                AssociatedControlID="ddlSearch" CssClass="filter-key"></asp:Label></h4>
                                        <div class="inner">
                                            <asp:DropDownList ID="ddlSearch" runat="server" Width="175px">
                                                <asp:ListItem Text="<%$ Resources:Resource, lblOpen%>" Value="O" Selected="True" />
                                                <asp:ListItem Text="<%$ Resources:Resource, lblAll%>" Value="A" />
                                            </asp:DropDownList>
                                        </div>
                                        <h4>
                                            <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lblRegister %>"
                                                AssociatedControlID="ddlRegister" CssClass="filter-key"></asp:Label></h4>
                                        <div class="inner">
                                            <asp:DropDownList ID="ddlRegister" runat="server" Width="175px">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="footer" style="padding-top: 5px;">
                                            <div class="submit">
                                                <%--<asp:Button  id="btnSearch" runat="server" Text="Search" />--%>
                                                <input id="btnSearchr" type="button" value="Search" onclick="callReload();" />
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                    <br />
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="left">
                    <div onkeypress="return disableEnterKey(event)">
                        <div id="grid_wrapper" style="width: 100%;">
                            <trirand:JQGrid runat="server" ID="jgdvCurrency" DataSourceID="sqlsdviewRegTran"
                                Height="300px" AutoWidth="True" OnCellBinding="jgdvCurrency_CellBinding" OnDataRequesting="jgdvCurrency_DataRequesting">
                                <Columns>
                                    <trirand:JQGridColumn DataField="RegTransID" HeaderText="" PrimaryKey="True" Visible="false" />
                                    <trirand:JQGridColumn DataField="sysRegDesc" HeaderText="<%$ Resources:Resource, grdPOSRegCode %>"
                                        Editable="false">
                                        <Formatter>
                                            <trirand:CustomFormatter FormatFunction="nullFormatter" />
                                        </Formatter>
                                    </trirand:JQGridColumn>
                                    <trirand:JQGridColumn DataField="RegStartDateTime" HeaderText="<%$ Resources:Resource, grdPOSRegStartDateTime %>"
                                        Editable="false">
                                        <Formatter>
                                            <trirand:CustomFormatter FormatFunction="nullFormatter" />
                                        </Formatter>
                                    </trirand:JQGridColumn>
                                    <trirand:JQGridColumn DataField="RegCloseDateTime" HeaderText="<%$ Resources:Resource, grdPOSRegCloseDateTime %>"
                                        Editable="false">
                                        <Formatter>
                                            <trirand:CustomFormatter FormatFunction="nullFormatter" />
                                        </Formatter>
                                    </trirand:JQGridColumn>
                                    <trirand:JQGridColumn DataField="RegStartAmount" HeaderText="<%$ Resources:Resource, grdPOSRegStartAmount %>"
                                        DataFormatString="{0:F}" Editable="false">
                                    </trirand:JQGridColumn>
                                    <trirand:JQGridColumn DataField="CalculatedCash" HeaderText="<%$ Resources:Resource, lblCalculatedCash %>"
                                        DataFormatString="{0:F}" Editable="false">
                                    </trirand:JQGridColumn>
                                    <trirand:JQGridColumn DataField="RegCloseAmount" HeaderText="<%$ Resources:Resource, grdPOSRegCloseAmount %>"
                                        DataFormatString="{0:F}" Editable="false">
                                    </trirand:JQGridColumn>
                                    <trirand:JQGridColumn DataField="UserName" HeaderText="<%$ Resources:Resource, grdPOSRegMgr %>"
                                        Editable="false">
                                        <Formatter>
                                            <trirand:CustomFormatter FormatFunction="nullFormatter" />
                                        </Formatter>
                                    </trirand:JQGridColumn>
                                    <trirand:JQGridColumn DataField="RegPOSUser" HeaderText="<%$ Resources:Resource, grdPOSRegPOSUser %>"
                                        Editable="false">
                                        <Formatter>
                                            <trirand:CustomFormatter FormatFunction="nullFormatter" />
                                        </Formatter>
                                    </trirand:JQGridColumn>
                                    <trirand:JQGridColumn DataField="lastUpdatedbyUser" HeaderText="<%$ Resources:Resource, lblLastUpdateBy %>"
                                        Editable="false">
                                        <Formatter>
                                            <trirand:CustomFormatter FormatFunction="nullFormatter" />
                                        </Formatter>
                                    </trirand:JQGridColumn>
                                    <trirand:JQGridColumn DataField="lastUpdatedDateTime" HeaderText="<%$ Resources:Resource, lblLastUpdatedDateTime %>"
                                        Editable="false">
                                        <Formatter>
                                            <trirand:CustomFormatter FormatFunction="nullFormatter" />
                                        </Formatter>
                                    </trirand:JQGridColumn>
                                    <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="RegTransID"
                                        Sortable="false" TextAlign="Center" Width="50" />
                                    <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="RegTransID"
                                        Visible="false" Sortable="false" TextAlign="Center" Width="50" />
                                </Columns>
                                <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                                    ShowDeleteButton="false" ShowSearchButton="false" />
                                <SortSettings InitialSortColumn=""></SortSettings>
                                <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                                <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" />
                            </trirand:JQGrid>
                            <iCtrl:IframeDialog ID="mdEditCurreny" Height="250" Width="490" Title="Add/Edit Currency"
                                Dragable="true" TriggerSelectorClass="edit-Currency" TriggerSelectorMode="Class"
                                UrlSelector="TriggerControlHrefAttribute" runat="server">
                            </iCtrl:IframeDialog>
                            <iCtrl:IframeDialog ID="mdDeleteCur" Width="400" Height="120" Title="Delete" Dragable="true"
                                TriggerSelectorClass="pop_delete" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
                                runat="server">
                            </iCtrl:IframeDialog>
                        </div>
                        <%--<asp:GridView ID="grdvViewRegister" runat="server" AllowSorting="True" DataSourceID="sqlsdviewRegTran"
                            Visible="false" EmptyDataRowStyle-HorizontalAlign="Center" AllowPaging="True"
                            PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0" EmptyDataText="No data Found"
                            GridLines="None" AutoGenerateColumns="False" UseAccessibleHeader="False" Style="border-collapse: separate;"
                            CssClass="view_grid650" DataKeyNames="RegTransID" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="sysRegDesc" HeaderStyle-Wrap="false" NullDisplayText="--"
                                    HeaderText="<%$ Resources:Resource, grdPOSRegCode %>" ReadOnly="True" SortExpression="sysRegDesc">
                                    <ItemStyle Width="120px" Wrap="true" HorizontalAlign="left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="RegStartDateTime" HeaderStyle-Wrap="false" NullDisplayText="--"
                                    HeaderText="<%$ Resources:Resource, grdPOSRegStartDateTime %>" ReadOnly="True"
                                    SortExpression="RegStartDateTime">
                                    <ItemStyle Width="190px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="RegCloseDateTime" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="left"
                                    NullDisplayText="--" HeaderText="<%$ Resources:Resource, grdPOSRegCloseDateTime %>"
                                    ReadOnly="True" SortExpression="RegCloseDateTime">
                                    <ItemStyle Width="190px" Wrap="true" HorizontalAlign="left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="RegStartAmount" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right"
                                    NullDisplayText="0" HeaderText="<%$ Resources:Resource, grdPOSRegStartAmount %>"
                                    ReadOnly="True" SortExpression="RegStartAmount">
                                    <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="RegCloseAmount" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right"
                                    NullDisplayText="0" HeaderText="<%$ Resources:Resource, grdPOSRegCloseAmount %>"
                                    ReadOnly="True" SortExpression="RegCloseAmount">
                                    <ItemStyle Width="120px" Wrap="true" HorizontalAlign="right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="UserName" HeaderStyle-Wrap="false" NullDisplayText="--"
                                    HeaderText="<%$ Resources:Resource, grdPOSRegMgr %>" ReadOnly="True" SortExpression="UserName">
                                    <ItemStyle Width="160px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="RegPOSUser" HeaderStyle-Wrap="false" NullDisplayText="--"
                                    HeaderText="<%$ Resources:Resource, grdPOSRegPOSUser %>" ReadOnly="True" SortExpression="RegPOSUser">
                                    <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="white">
                                    <ItemTemplate>
                                        
                                        <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("RegTransID") %>'
                                            CommandName="Edit" ToolTip="<%$ Resources:Resource, grdvEdit %>" ImageUrl="~/images/edit_icon.png" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, grdvDelete %>" HeaderStyle-HorizontalAlign="Center"
                                    HeaderStyle-ForeColor="white" Visible="false">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/del.png" ToolTip="<%$ Resources:Resource, grdvDelete %>"
                                            CommandArgument='<%# Eval("RegTransID") %>' CommandName="Delete" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle CssClass="grid_footer" />
                            <RowStyle CssClass="grid_rowstyle" />
                            <PagerStyle CssClass="grid_footer" />
                            <HeaderStyle CssClass="grid_header" Height="26px" />
                            <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                            <PagerSettings PageButtonCount="20" />
                        </asp:GridView>--%>
                        <asp:SqlDataSource ID="sqlsdviewRegTran" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                        <asp:HiddenField runat="server" ID="hdnTranID" />
                    </div>
                </td>
            </tr>
            <tr>
                <td height="10">
                </td>
            </tr>
        </table>
        <br />
    </div>
    <asp:HiddenField runat="server" ID="hdnValue" />
    <asp:HiddenField runat="server" ID="hdnRegTranID" />
    <div id="divSetAmount" runat="server">
        <asp:Panel runat="server" ID="pnlPosCat">
            <table cellpadding="0" cellspacing="0" width="350px" style="display: none;">
                <tr>
                    <td width='15px' background="../Images/popup_left.png">
                    </td>
                    <td align="left" valign="middle" class="popup_title_newbg">
                        <asp:Label class="discount_title" runat="server" ID="lblPopupheading"></asp:Label>
                    </td>
                    <td class="popup_title_newbg" align="right">
                        <a href="javascript:funPOSCloseSetAmount();">
                            <img border="0" src='../images/close_popup_btn.gif' width="24" height="24" /></a>
                    </td>
                    <td width='15px' background="../Images/popup_right.png">
                    </td>
                </tr>
            </table>
            <table width="100%" border='0'>
                <tr>
                    <td width="35%" align="right">
                        <%= Resources.Resource.POSStarAmt%>
                    </td>
                    <td align="left" width="65%">
                        <asp:TextBox runat="server" ID="txtstartAmt" ValidationGroup="Amount" CssClass="numericTextField"
                            Width="150px" MaxLength="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalstartAmt" ValidationGroup="Amount" runat="server"
                            ControlToValidate="txtstartAmt" ErrorMessage="<%$ Resources:Resource, reqvalPOSStartAmount %>"
                            SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator><span style="color: Red;
                                font-weight: bold; font-size: 11pt;"> *</span>
                        <asp:CompareValidator ID="compvalstartAmt" EnableClientScript="true" ValidationGroup="Amount"
                            SetFocusOnError="true" ControlToValidate="txtstartAmt" Operator="DataTypeCheck"
                            Type="Double" Display="None" ErrorMessage="<%$ Resources:Resource, compvalstartAmt %>"
                            runat="server" />
                    </td>
                </tr>
                <tr id="trClose" style="visibility: hidden;" runat="server">
                    <td align="right">
                        <asp:Label ID="Label3" Text="<%$ Resources:Resource, PosCloseAmt %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td align="left">
                        <asp:TextBox runat="server" ID="txtCloseAmount" ValidationGroup="Amount" CssClass="popup_input"
                            Width="150px" MaxLength="10"></asp:TextBox>
                        <asp:CompareValidator ID="cmpvalCloseAmt" EnableClientScript="true" ValidationGroup="Amount"
                            SetFocusOnError="true" ControlToValidate="txtCloseAmount" Operator="DataTypeCheck"
                            Type="Double" Display="None" ErrorMessage="<%$ Resources:Resource, cmpvalCloseAmt %>"
                            runat="server" />
                        <span style="color: Red; font-weight: bold; font-size: 11pt;">*</span>
                    </td>
                </tr>
                <tr>
                    <td align="center" runat="server" colspan="2">
                        <asp:ImageButton runat="server" ID="imgSetCloseAmount" ValidationGroup="Amount" ImageUrl="~/images/submit.png" />
                    </td>
                </tr>
            </table>
            <div class="div_command">
                <asp:Button ID="imgSetCloseAmountds" Text="<%$Resources:Resource, Ok%>" runat="server"
                    ValidationGroup="Amount" />
                <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
                    OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
            </div>
            <asp:ValidationSummary ID="valsumSetAmt" runat="server" ShowMessageBox="true" ValidationGroup="Amount"
                ShowSummary="false" />
        </asp:Panel>
    </div>
    <br />
    <br />
    <script type="text/javascript">
        //Variables
        var gridID = "<%=jgdvCurrency.ClientID %>";



        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.


        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function callReload() {
            $('#' + gridID).trigger("reloadGrid");
        }


        var nullFormatter = function (cellvalue, options, rowObject) {
            if (cellvalue === undefined || cellvalue === '') {
                cellvalue = '--';
            }
            return cellvalue;
        }
    </script>
    <script language="javascript">
        function funSetAmount() {

        AddEditRegister(0);
        return;


            document.getElementById('<%=lblMsg.ClientID%>').innerHTML = '';
            document.getElementById('<%=hdnValue.ClientID%>').value = '';
//            document.getElementById('<%=divSetAmount.ClientID%>').style.display = "block";

            $('#<%=divSetAmount.ClientID%>').dialog({ modal: true,
                height: 200,
           		width: 400,
           		position: 'center',
           		title:'Set Start Amount', });

            var browserWidth = 0;
            browserWidth = (document.documentElement.clientWidth - 350) / 2;
//            document.getElementById('<%=divSetAmount.ClientID%>').style.position = "absolute";
//            document.getElementById('<%=divSetAmount.ClientID%>').style.left = browserWidth + "px";
//            document.getElementById('<%=divSetAmount.ClientID%>').style.top = "100px";
            document.getElementById('<%=imgSetCloseAmount.ClientID%>').style.visibility = "visible";
            document.getElementById('<%=trClose.ClientID%>').style.visibility = "hidden";
            document.getElementById('<%=txtCloseAmount.ClientID%>').value = '';
            document.getElementById('<%=txtstartAmt.ClientID%>').value = '';
            document.getElementById('<%=lblPopupheading.ClientID%>').value = '<%= lblSetStartAmount %>';
            document.getElementById('<%=lblPopupheading.ClientID%>').innerHTML = '<%= lblSetStartAmount %>';
            document.getElementById('<%=txtstartAmt.ClientID%>').disabled = false;
            document.getElementById('<%=hdnValue.ClientID%>').value = (document.documentElement.clientWidth - 350) / 2;
            return false;
        }

        document.getElementById('<%=hdnValue.ClientID%>').value = (document.documentElement.clientWidth - 350) / 2;


        function AddEditRegister(RegTranID) {
            var vTitle = "<%=Resources.Resource.lblSetStartAmount %>";
            if(RegTranID>0)
            {
            vTitle = "<%=Resources.Resource.lblSetCloseAmount %>";
            }
            var postData = {};
            postData.RegTranID = RegTranID;
            var $dialog = jQuery.FrameDialog.create({ url: 'AddEditRegister.aspx?' + $.param(postData),
                title: vTitle,
                modal: true,
                width: 350,
                height: 200,
                autoOpen: false
            });
            $dialog.dialog('open');
        }

        function funPOSCloseSetAmount() {
//            document.getElementById('<%=divSetAmount.ClientID%>').style.display = "none";
            document.getElementById('<%=txtstartAmt.ClientID%>').disabled = false;
            document.getElementById('<%=hdnRegTranID.ClientID%>').value = '';
            document.getElementById('<%=hdnValue.ClientID%>').value = (document.documentElement.clientWidth - 350) / 2;
        }


        function ReloadPage(msg)
        {
       $('#' + gridID).trigger("reloadGrid");
       $("#<%=spnlblMsg.ClientID%>").html(msg);
        }
    </script>
</asp:Content>

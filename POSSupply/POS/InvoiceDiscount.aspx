﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="InvoiceDiscount.aspx.cs" Inherits="POS_InvoiceDiscount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="dialogAssignUser" style="padding: 5px;">
<%--        <h3>
            <asp:Label ID="Label1" Text="<%$Resources:Resource, btnAddNewColor%>" runat="server" />
        </h3>--%>
        <table>
            <tr>
                <td>
                    <%= Resources.Resource.PODiscount%> %
                </td>
                <td>
                    <asp:TextBox ID="txtDiscount" runat="server" MaxLength="4" CssClass="numericTextField" onkeyup='AllowMaxValue(this,100)' />
                    <asp:RequiredFieldValidator ID="reqvalManagerCode" ErrorMessage="<%$ Resources:Resource, reqvalDiscPer%>"
                        runat="server" ControlToValidate="txtDiscount" Display="None" SetFocusOnError="true"
                        ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
    </div>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, Ok%>" runat="server" OnClick="btnSave_Click" ValidationGroup="RegisterUser" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
            OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
<script type="text/javascript">
    function AllowMaxValue(input, limitVal) {
        if (input.value < 0) input.value = 0;
        if (input.value > limitVal) input.value = 100;
    }
</script>
</asp:Content>


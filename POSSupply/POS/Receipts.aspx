<%@ Page Language="VB" MasterPageFile="~/POSMaster.master" AutoEventWireup="false"
    CodeFile="Receipts.aspx.vb" Inherits="POS_Receipts" %>

<%--<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divModuleAlert').corner();
        $('#divAlertContent').corner();
        $('#divMainContainerTitle').corner();
    </script>
    <div id="divModuleAlert" class="divSectionTitle">
        <h2>
            <%=Resources.Resource.lblSearchOptions%>
        </h2>
    </div>
    <div id="divAlertContent" class="divSectionContent">
    </div>
</asp:Content>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();
    </script>
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblHeading"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                   
                </td>
                <td style="width: 150px;">
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">            
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right" width="50%">
                    <asp:CheckBox runat="server" AutoPostBack="true" Font-Bold="true" ForeColor="black"
                        ID="chkUser" Text="<%$ Resources:Resource, chkPOSAlluser %>" />
                </td>
                <td width="50%" align="right">
                    <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Search" ImageUrl="../images/xls.png" />
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <asp:Label CssClass="lblPosTran" runat="server" ID="lblPrdItem" Text=" <%$ Resources:Resource, lblPosTran %>"></asp:Label>
                    <asp:GridView ID="grdvViewTran" runat="server" AllowSorting="True" DataSourceID="sqlsdviewTran"
                        EmptyDataRowStyle-HorizontalAlign="Center" AllowPaging="True" 
                        PageSize="15" PagerSettings-Mode="Numeric"
                        CellPadding="0" EmptyDataText="No data Found" GridLines="none" AutoGenerateColumns="False"
                        UseAccessibleHeader="False" Style="border-collapse: separate;" CssClass="view_grid650"
                        DataKeyNames="posTransId" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="posTransId" HeaderStyle-Wrap="false" NullDisplayText="--"
                                HeaderText="<%$ Resources:Resource, grdPOSPauseID %>" ReadOnly="True" SortExpression="posTransId">
                                <ItemStyle Width="120px" Wrap="true" HorizontalAlign="left" />
                            </asp:BoundField>
                            <%--  <asp:BoundField DataField="posPayTypeDesc" HeaderStyle-Wrap="false" NullDisplayText="--" HeaderText="Pay by" ReadOnly="True" SortExpression="posPayTypeDesc" >
                    <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                </asp:boundField>--%>
                            <asp:BoundField DataField="posTransDateTime" HeaderStyle-Wrap="false" NullDisplayText="--"
                                HeaderText="<%$ Resources:Resource, grdPOSPauseDate %>" ReadOnly="True" SortExpression="posTransDateTime">
                                <ItemStyle Width="150px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="posSubTotal" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right"
                                NullDisplayText="--" HeaderText="<%$ Resources:Resource, grdPOSPauseSubTotal %>"
                                ReadOnly="True" SortExpression="posSubTotal">
                                <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="posTotalValue" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right"
                                NullDisplayText="--" HeaderText="<%$ Resources:Resource, grdPOSPauseTotal %>"
                                ReadOnly="True" SortExpression="posTotalValue">
                                <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="posTransStatus" HeaderStyle-Wrap="false" NullDisplayText="--"
                                HeaderText="<%$ Resources:Resource, grdPOSPauseStatus %>" ReadOnly="True" SortExpression="posTransStatus">
                                <ItemStyle Width="180px" Wrap="true" HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="userID" HeaderStyle-Wrap="false" NullDisplayText="--"
                                HeaderText="<%$ Resources:Resource, grdPOSUserID %>" ReadOnly="True" SortExpression="userID">
                                <ItemStyle Width="160px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="posTransType" HeaderStyle-Wrap="false" NullDisplayText="--"
                                HeaderText="<%$ Resources:Resource, grdPOSTranType %>" ReadOnly="True" SortExpression="posTransType">
                                <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdPrint %>" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-ForeColor="white">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("posTransId") & ":" & Eval("posAcctPayType") & ":" & Eval("posTransStatus") & ":" & Eval("posTransType") %>'
                                        CommandName="Edit" ToolTip="<%$ Resources:Resource, grdPrint %>" ImageUrl="~/images/view_24.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdvDelete %>" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-ForeColor="white" Visible="false">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                        ToolTip="<%$ Resources:Resource, grdvDelete %>" CommandArgument='<%# Eval("posTransId") %>'
                                        CommandName="Delete" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="grid_footer" />
                        <RowStyle CssClass="grid_rowstyle" />
                        <PagerStyle CssClass="grid_footer" />
                        <HeaderStyle CssClass="grid_header" Height="26px" />
                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                        <PagerSettings PageButtonCount="20" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqlsdviewTran" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                    <asp:HiddenField runat="server" ID="hdnTranID" />
                </td>
            </tr>
            <tr>
                <td height="10">
                </td>
            </tr>
        </table>
        <br />
    </div>
    <br />
    <br />
</asp:Content>

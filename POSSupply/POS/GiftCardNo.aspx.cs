﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class POS_GiftCardNo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                MessageState.SetGlobalMessage(MessageType.Success, "");
                txtGiftCardNo.Focus();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {

            GiftCard objGiftCard = new GiftCard();
            if (objGiftCard.IsExistsGiftCardNo(null, txtGiftCardNo.Text.Trim()) == true)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, (Resources.Resource.lblGiftCardNoAllreadySaved).Replace("#GIFTNO#", txtGiftCardNo.Text));
                return;
            }

            //SysRegister objSysRegister = new SysRegister();
            //if (objSysRegister.ValidateMangerCode(BusinessUtility.GetString(Session["RegCode"]), txtDiscount.Text) == true)
            //{
            Globals.RegisterCloseDialogScript(Page, string.Format("parent.SelectedGiftCard('{0}','{1}');", BusinessUtility.GetString(Request.QueryString["productID"]), txtGiftCardNo.Text), true);
            //}
            //else
            //{
            //    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblInvalidManageCode);
            //}
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
﻿<%@ Page Title="" Language="VB" MasterPageFile="~/POSMaster.master" AutoEventWireup="false"
    CodeFile="POS.aspx.vb" Inherits="POS_POS" %>

<%@ Import Namespace=" Resources.Resource" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <script type="text/javascript" src="../scripts/jquery.keypad.js"></script>
    <script type="text/javascript" language="JavaScript" src="itech.js"></script>
    <script src="combogrid-1.6.2/jquery/jquery-ui-1.8.9.custom.min.js" type="text/javascript"></script>
    <script src="combogrid-1.6.2/plugin/jquery.i18n.properties-1.0.9.js" type="text/javascript"></script>
    <script src="combogrid-1.6.2/plugin/jquery.ui.combogrid-1.6.2.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-plugins/jquery.blockUI.js" type="text/javascript"></script>
    <%=UIHelper.GetScriptTags("blockui", 1)%>
    <style type="text/css">
        .divButton
        {
            width: 90px; /*width: 100px;*/
            font-size: 15px;
            height: auto;
            text-align: center; /* background-color: #FB9515;*/
            padding: 0px;
        }
        
        .divGiftButton
        {
            width: 90px; /*width: 100px;*/
            font-size: 15px;
            height: auto;
            text-align: center; /* background-color: #FB9515;*/
            padding: 0px;
        }
        
        
        .CustTitleCss
        {
            color: #880015;
            font-family: 'Trebuchet MS' ,Helvetica,sans-serif;
            font-size: 22px;
            font-weight: bold;
            line-height: 26px;
        }
    </style>
    <input type="hidden" id="hdnCustomerAlert" />
    <input type="hidden" id="hdnCustMail" />
    <input type="hidden" id="hdnIsRegClosed" runat="server" />
    <div style="border-radius: 10px 10px 10px 10px;" id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 99.2%;">
            <tr>
                <td>
                    <%--style="width: 300px;"--%>
                </td>
                <td style="text-align: right;">
                </td>
                <td>
                    <%--style="width: 150px;"--%>
                </td>
            </tr>
            <tbody>
                <tr>
                    <td style="color: Red;" align="left">
                        <h2>
                            <literal id="lblCustomerTitle"></literal>
                            <asp:Literal runat="server" ID="lblTitle" Text=""></asp:Literal></h2>
                    </td>
                    <td style="text-align: left;" class="searchlbl">
                        <asp:Label ID="lblSearchPop" CssClass="lblBold" Text="<%$Resources:Resource, lblSearch %>"
                            runat="server" />
                    </td>
                    <td align="left">
                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width: 195px;">
                                    <asp:TextBox runat="server" Width="195px" AutoCompleteType="Disabled" ID="txtPopSearch"
                                        CssClass="ui-autocomplete-input searchinpt" />
                                </td>
                                <td align="right">
                                    <a id="lblCusSrh" style="padding: 5px 10px; font-size: 12px; width: 80px; font-weight: bold;"
                                        onclick=" return srhCustomer();">
                                        <%= Resources.Resource.lblVIP%></a> <a id="popPrdSearch" style="padding: 5px 10px;
                                            font-size: 12px; width: 80px; font-weight: bold;">
                                            <%=Resources.Resource.lblProducts %></a> <a id="btnManager" style="padding: 5px 10px;
                                                font-size: 12px; width: 80px; font-weight: bold;" onclick="return ChkUserPosManagerRole();">
                                                <%=Resources.Resource.lblManager %></a>
                                    <input id="hdnIsUserPosManagerRole" type="hidden" runat="server" value="0" />
                                    <input id="hdnManagerCode" type="hidden" runat="server" value="" />
                                    <input id="hdnIstLoadedCustID" type="hidden" runat="server" />
                                    <input id="hdnDefaultCustID" type="hidden" runat="server" />
                                    <input id="hdnShowDefaultProduct" type="hidden" runat="server" value="0" />
                                    <input id="hdnIsPandingPrintRcpt" type="hidden" runat="server" value="0" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="divMainContent" style="margin: 0px;">
        <table width="96%" border="0" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr class="titleBgColorPos">
                    <td align="left" colspan="2">
                        <asp:Panel runat="server" ID="pnlSearch" DefaultButton="imgSearch">
                            <table id="Table1" border="0" width="100%" runat="server">
                                <tbody>
                                    <tr>
                                        <td align="left" width="16%">
                                            <asp:DropDownList ID="dlSearch" runat="server" Width="145px" Visible="false">
                                                <asp:ListItem Value="UC" Text="Product UPC Code" />
                                                <asp:ListItem Value="TI" Text="Tag ID" />
                                            </asp:DropDownList>
                                            <asp:Label CssClass="lblBold" runat="server" ID="lblSearch" Text="<%$ Resources:Resource, POSSearch%>"></asp:Label>
                                        </td>
                                        <td align="left" width="16%">
                                            <asp:TextBox runat="server" Width="195px" AutoCompleteType="Disabled" ID="txtSearch"
                                                class="searchinpt inptcvr  csssrchprd"></asp:TextBox>
                                        </td>
                                        <td align="left" width="5%">
                                            <asp:ImageButton ID="imgSearch" runat="server" AlternateText="Search" CausesValidation="false"
                                                ImageUrl="../images/srch_icon_pos.png" Width="25px" OnClientClick="searchProdect();return false;" />
                                        </td>
                                        <td align="left" width="12%">
                                            <asp:ImageButton ID="ImgPosMenu" runat="server" AlternateText="POSMenu" CausesValidation="false"
                                                ImageUrl="../images/prdMenuNew.jpg" Visible="false" />
                                            <div class="buttonwrapper">
                                                <a id="quickslide" onclick="reprintInvoice();" class="ovalbutton" href="Javascript:void(0)">
                                                    <span class="ovalbutton">
                                                        <%=Resources.Resource.liliBatchReprint %></span> </a>
                                            </div>
                                        </td>
                                        <td width="52%" align="right">
                                            <table width="100%" cellspacing="4">
                                                <tbody>
                                                    <tr>
                                                        <td width="27%">
                                                            <div class="buttonwrapper">
                                                                <a id="CmdPrint" runat="server" onclick="cashTransation(true,''); return false;"
                                                                    class="ovalbutton" href="#"><span class="ovalbutton" style="min-width: 55px; text-align: center;">
                                                                        <%=Resources.Resource.cmdCssPrint%>
                                                                    </span></a>
                                                            </div>
                                                        </td>
                                                        <td width="28%" align="left">
                                                            <div class="buttonwrapper">
                                                                <a id="cmdNoReceipt" runat="server" onclick="ReceiptMail(); return false;" class="ovalbutton"
                                                                    href="#"><span class="ovalbutton" style="min-width: 70px; text-align: center;">
                                                                        <%=Resources.Resource.btnReceiptMail%>
                                                                    </span></a>
                                                            </div>
                                                        </td>
                                                        <% If Request.QueryString("TType") <> "R" Then%>
                                                        <td width="20%" style="display: none;">
                                                            <div class="buttonwrapper">
                                                                <a id="CmdTab" runat="server" causesvalidation="false" onclick="cashTransation(false,'Tab'); return false;"
                                                                    class="ovalbutton" href="#"><span class="ovalbutton" style="min-width: 50px; text-align: center;">
                                                                        <%=Resources.Resource.CmdCssTab%>
                                                                    </span></a>
                                                            </div>
                                                        </td>
                                                        <td align="center" width="20%">
                                                            <div class="buttonwrapper">
                                                                <a id="CmdCancel" runat="server" causesvalidation="false" onclick="cashTransation(false,'cancel'); return false;"
                                                                    class="ovalbutton" href="#"><span class="ovalbutton" style="min-width: 65px; text-align: center;">
                                                                        <%=Resources.Resource.CmdcssCancel%>
                                                                    </span></a>
                                                            </div>
                                                        </td>
                                                        <%Else%>
                                                        <td width="50%">
                                                        </td>
                                                        <% End If%>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <%--                <tr>
                    <td height="1" >
                    </td>
                </tr>--%>
                <tr>
                    <td align="left" valign="top">
                        <%--width="59%"--%>
                        <div id="divViewPrd" runat="server" class="leftsydwrap" style="display: block;">
                            <table class="tdBorder checkksumtable" style="background-image: url(../images/popup-bg-bottom-gray.jpg)"
                                border="0" width="100%">
                                <tbody>
                                    <tr class="head-row">
                                        <td>
                                        </td>
                                        <td style="text-align: center;">
                                            <%--width="20%"--%>
                                            <asp:Label ID="lblUPCCode" Text="<%$ Resources:Resource, POSUPC%>" CssClass="lblBold"
                                                runat="server" />
                                        </td>
                                        <td style="text-align: center;">
                                            <%--width="20%"--%>
                                            <asp:Label ID="lblPOProductName1" CssClass="lblBold" runat="server" Text="<%$ Resources:Resource, POProductName1 %>" />
                                        </td>
                                        <td style="text-align: center;">
                                            <%--width="8%"--%>
                                            <asp:Label ID="lblQuantity" Text="<%$ Resources:Resource, POSQuantity%>" CssClass="lblBold"
                                                runat="server" />
                                        </td>
                                        <td width="13%" style="text-align: center;">
                                            <%-- width="13%"--%>
                                            <asp:Label ID="lblPrice" Text="<%$ Resources:Resource, POSUnitPrice%>" CssClass="lblBold"
                                                runat="server" />
                                        </td>
                                        <td style="text-align: center;">
                                            <asp:Label ID="lblDisc" CssClass="lblBold" Text="<%$ Resources:Resource, lblPosGrdDisc%>"
                                                runat="server" />
                                        </td>
                                        <td style="text-align: center;">
                                            <asp:Label ID="lblTax" CssClass="lblBold" Text="<%$ Resources:Resource, lblPosGrdTax%>"
                                                runat="server" />
                                        </td>
                                        <td width="14%" style="text-align: center;">
                                            <%----%>
                                            <asp:Label ID="lblPOTotalprice" CssClass="lblBold" Text="<%$ Resources:Resource, POSTotalPrice%>"
                                                runat="server" />
                                        </td>
                                        <td width="12%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtProductId" runat="server" Width="0" Height="0" ReadOnly="true"
                                                CssClass="form-inputlarge frminpt dnone" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtUPCCode" runat="server" ReadOnly="true" CssClass="form-inputlarge frminpt"
                                                Style="width: 98px; padding-left: 2px;" />
                                        </td>
                                        <td align="center">
                                            <asp:TextBox ID="txtProductName" runat="server" ReadOnly="true" CssClass="form-inputlarge frminpt" />
                                        </td>
                                        <td align="center">
                                            <asp:TextBox ID="txtQuantity" runat="server" onKeyup="return funCalQuantity()" Style="text-align: right;
                                                width: 22px; padding-left: 1px;" MaxLength="3" ValidationGroup="lstSrch" CssClass="form-inputsml frminpt numericTextField" />
                                            <asp:RequiredFieldValidator ID="reqvalQuantity" runat="server" ControlToValidate="txtQuantity"
                                                ErrorMessage="<%$ Resources:Resource, POSreqvalQuantity%>" SetFocusOnError="true"
                                                Display="None" ValidationGroup="lstSrch"> </asp:RequiredFieldValidator>
                                        </td>
                                        <td align="center">
                                            <asp:TextBox ID="txtPrice" runat="server" ReadOnly="true" Style="text-align: right;
                                                padding-left: 2px; padding-right: 2px; width: 50px;" ValidationGroup="lstSrch"
                                                CssClass="form-inputsml frminpt numericTextField" />
                                            <asp:RequiredFieldValidator ID="reqvalPrice" runat="server" ControlToValidate="txtPrice"
                                                ErrorMessage="<%$ Resources:Resource, POSreqvalPrice%>" SetFocusOnError="true"
                                                Display="None" ValidationGroup="lstSrch"> </asp:RequiredFieldValidator>
                                        </td>
                                        <td align="center">
                                            <asp:TextBox ID="txtDisc" runat="server" ReadOnly="false" Style="padding-left: 2px;
                                                padding-right: 2px; width: 30px;" MaxLength="4" ValidationGroup="lstSrch" CssClass="form-inputsml frminpt numericTextField"
                                                onkeyup="AllowMaxValue(this,100);" />
                                            <asp:HiddenField ID="hdnDisc" runat="server" />
                                        </td>
                                        <td align="center">
                                            <asp:DropDownList ID="ddlTax" runat="server" CssClass="form-inputsml frminpt" Style="width: 100px">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hdnTaxValue" runat="server" />
                                        </td>
                                        <td align="center">
                                            <asp:TextBox ID="txtTotalPrice" runat="server" ReadOnly="true" Style="text-align: right;
                                                width: 80px; padding-left: 2px; padding-right: 2px;" ValidationGroup="lstSrch"
                                                CssClass="form-inputsml frminpt" />
                                        </td>
                                        <td>
                                            <div class="buttonwrapper">
                                                <a id="cmdAdd" runat="server" class="ovalbutton" validationgroup="lstSrch" href="#">
                                                    <span id="spnAdd" class="ovalbutton" style="min-width: 70px; text-align: center;">
                                                        <%=Resources.Resource.cmdCssAdd%>
                                                    </span></a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
    </div>
    <div class="mastergrid_main">
        <table id="grdRequest" runat="server" rules="all" border="1" cellpadding="3" cellspacing="0"
            class="mastergrid">
            <tbody>
                <tr id="grdRequest_header" align="left" valign="middle" class="grid-header" runat="server">
                    <td style="color: White; text-align: center">
                        <%=Resources.Resource.grdPOSUPC%>
                    </td>
                    <td style="color: White; text-align: center">
                        <%=Resources.Resource.grdPOProductName %>
                    </td>
                    <td style="color: White; text-align: center">
                        <%=Resources.Resource.grdPOSQty %>
                    </td>
                    <td style="color: White; text-align: center">
                        <%=Resources.Resource.grdPOPrdCostPrice %>
                    </td>
                    <td style="color: White; text-align: center">
                        <%=Resources.Resource.lblPosGrdDisc%>
                    </td>
                    <td style="color: White; text-align: center">
                        <%=Resources.Resource.lblPosGrdTax %>
                    </td>
                    <td style="color: White; text-align: center">
                        <%= Resources.Resource.grdPOSTotalPrice %>
                    </td>
                    <td style="color: White; text-align: center">
                        <%= Resources.Resource.grdEdit %>
                    </td>
                    <td style="color: White; text-align: center">
                        <%=Resources.Resource.grdDelete%>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <!-- END Change 
      <table><tbody>
       <tr >
        <td>-->
    <asp:SqlDataSource ID="sqldsRequest" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
    </td>
    <td class="rytboxbg" width="41%" valign="top">
        <div id="dvCollapse" class="pulloutmenu-wrap" style="display: none;">
            <!--	Pullout menu container	-->
            <a id="clicked" href="javascript:void(0)" class="pullout-btn" alt="POSMenu" onclick="return funPOSCategory('QM');">
                &nbsp;</a>
        </div>
        <table id="Table3" width="300px" runat="server" border="0" cellpadding="0" cellspacing="0">
            <%--width="350px"--%>
            <%--width="100%" --%>
            <tbody>
                <tr>
                    <td colspan="3">
                        <div id="divCardManually" runat="server" style="left: 170px; border-color: red; height: 0;
                            position: relative; top: 15px; display: none; margin: 0; border: 3px;" align="left">
                            <asp:Panel runat="server" ID="Panel1" DefaultButton="imgEnterCardManually">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                    ValidationGroup="CardManually" ShowSummary="false" />
                                <table class="popupTable" style="border-color: #CD9232;" cellpadding="0" cellspacing="0"
                                    height="150" width="400px">
                                    <tbody>
                                        <tr style="background-color: Gold; height: 40px; border-color: Red; border: 1px;
                                            border-style: solid;">
                                            <td align="center" width="97%">
                                                <asp:Label CssClass="lblHeading" runat="server" ID="Label2" Text="<%$ Resources:Resource, lblCardInfo %>"></asp:Label>
                                            </td>
                                            <td style="padding-right: 4px;" align="right" width="3%">
                                                <a name="Close" href="javascript:funCloseTransactionManually();">
                                                    <img border="0" src="../Images/ClosePopup.png" alt="<% = cmdPOSClose %>" /></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="10">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table cellpadding="2" cellspacing="2" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td align="right" width="35%">
                                                                <asp:Label ID="lblFirstName" Text="<%$ Resources:Resource, lblPOSCardFirstName %>"
                                                                    CssClass="lblBold" runat="server" />
                                                            </td>
                                                            <td align="left" width="65%">
                                                                <asp:TextBox runat="server" ID="txtFirstName" ValidationGroup="CardManually" Width="220px"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="reqvalFirstName" ValidationGroup="CardManually" runat="server"
                                                                    ControlToValidate="txtFirstName" ErrorMessage="<%$ Resources:Resource, reqvalPOSCardFirstName %>"
                                                                    SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator><span style="color: Red;
                                                                        font-weight: bold; font-size: 11pt;"> *</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="Label3" Text="<%$ Resources:Resource, lblPOScardLastName %>" CssClass="lblBold"
                                                                    runat="server" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox runat="server" ID="txtLastName" ValidationGroup="CardManually" Width="220px"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="reqvalLastName" ValidationGroup="CardManually" runat="server"
                                                                    ControlToValidate="txtLastName" ErrorMessage="<%$ Resources:Resource, reqvalPOSLastName %>"
                                                                    SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator><span style="color: Red;
                                                                        font-weight: bold; font-size: 11pt;"> *</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="Label4" Text="<%$ Resources:Resource, lblPOSCardNo %>" CssClass="lblBold"
                                                                    runat="server" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox runat="server" ID="txtCardNo" ValidationGroup="CardManually" Width="220px"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="reqvalcardNo" ValidationGroup="CardManually" runat="server"
                                                                    ControlToValidate="txtCardNo" ErrorMessage="<%$ Resources:Resource, reqvalPOSCard %>"
                                                                    SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator><span style="color: Red;
                                                                        font-weight: bold; font-size: 11pt;"> *</span>
                                                                <asp:CustomValidator ID="custvalCreditCard" runat="server" ClientValidationFunction="funNumericCreditCard"
                                                                    ValidationGroup="CardManually" Display="None" ControlToValidate="txtCardNo" ErrorMessage="<%$ Resources:Resource, custvalCreditCard %>"
                                                                    SetFocusOnError="true">
                                                                </asp:CustomValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="lblCardtype" Text="<%$ Resources:Resource, lblPOSCardType %>" CssClass="lblBold"
                                                                    runat="server" />
                                                            </td>
                                                            <td align="left" height="35">
                                                                <asp:RadioButton runat="server" ID="rbVisaManually" GroupName="Tran" />
                                                                <asp:Image runat="server" ID="Img1" Width="30px" Height="25px" ImageUrl="~/images/Visa.png" />
                                                                <asp:RadioButton runat="server" ID="rbMasterCardManually" GroupName="Tran" />
                                                                <asp:Image runat="server" ID="Image1" Width="30px" Height="25px" ImageUrl="~/images/MasterCard.png" />
                                                                <asp:RadioButton runat="server" ID="rbAmericanManually" GroupName="Tran" />
                                                                <asp:Image runat="server" ID="Image2" Width="30px" Height="25px" ImageUrl="~/images/American.png" />
                                                                <asp:RadioButton runat="server" ID="rbInteracManually" GroupName="Tran" />
                                                                <asp:Image runat="server" ID="Image3" Width="30px" Height="25px" ImageUrl="~/images/interac_logo.gif" />
                                                                <asp:CustomValidator ID="custSelectCard" runat="server" ValidationGroup="CardManually"
                                                                    ErrorMessage="<%$ Resources:Resource, custPOSSelectCardtype %>" ClientValidationFunction="funPaymentByManually"
                                                                    Display="None" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <asp:Label ID="Label5" Text="<%$ Resources:Resource, cmdPOSEXPDate %>" CssClass="lblBold"
                                                                    runat="server" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="dlMonth" runat="server">
                                                                </asp:DropDownList>
                                                                -
                                                                <asp:DropDownList ID="dlYear" runat="server">
                                                                </asp:DropDownList>
                                                                <span style="color: Red; font-weight: bold; font-size: 11pt;">*</span>
                                                                <asp:HiddenField runat="server" ID="hdnDate" />
                                                                <asp:HiddenField runat="server" ID="hdnYear" />
                                                                <asp:CustomValidator ID="custvalDatecompare" runat="server" ValidationGroup="CardManually"
                                                                    ErrorMessage="<%$ Resources:Resource, custvalPOSDatecompare %>" ClientValidationFunction="funDateCompare"
                                                                    Display="None" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="10">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                                <asp:ImageButton runat="server" ID="imgEnterCardManually" ValidationGroup="CardManually"
                                                                    ImageUrl="~/images/submit.png" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="2">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </asp:Panel>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <div>
                            <h2 class="popup_title_newbg">
                                <asp:Label runat="server" ID="lblNote1" Text="<%$ Resources:Resource, POSAmount %>"></asp:Label>
                            </h2>
                            <table class="popup_content_newbg inpt-largetable" width="100%" border="0" cellspacing="8"
                                cellpadding="0">
                                <%--width="100%"--%>
                                <tbody>
                                    <tr height="25">
                                        <td class="tdAlign" width="52%">
                                            <asp:Label ID="lblNetAmount" Style="font-size: 16px; font-weight: bold" Text="<%$ Resources:Resource, POSTotalAmount %>"
                                                CssClass="lblBold" runat="server" />
                                        </td>
                                        <td width="1%">
                                        </td>
                                        <td align="left" width="47%">
                                            <asp:TextBox ReadOnly="true" ID="txtTotalAmount" Text="0" runat="server" MaxLength="25"
                                                CssClass=" frminpt fom-inptxlarge txtBoxAmountForPOS" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdAlign">
                                            <asp:Label ID="lblReceived" Style="font-size: 16px; font-weight: bold" Text="<%$ Resources:Resource, POSReceived %>"
                                                CssClass="lblBold" runat="server" />
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtReceived" onKeyup="return funCalBalance()" onfocus="return funCalBalance()"
                                                runat="server" MaxLength="10" CssClass="  frminpt fom-inptxlarge txtBoxAmountForPOS numericTextField" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdAlign">
                                            <div style="float: left;">
                                                <%--<image id="imgPartialPayment" src="../images/srch_icon_pos.png" style="width:25px;"></image>--%>
                                                <asp:ImageButton ID="imgPartialPayment" runat="server" AlternateText="Search" CausesValidation="false"
                                                    ImageUrl="../images/money.png" Width="25px" OnClientClick="EnablePartialPayment(); return false;" />
                                            </div>
                                            <asp:Label ID="lblRefund" Text="<%$ Resources:Resource, POSBalance %>" Style="font-size: 16px;
                                                font-weight: bold" CssClass="lblBold" runat="server" />
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtRefund" runat="server" ReadOnly="true" MaxLength="20" CssClass=" frminpt fom-inptxlarge txtBoxAmountForPOS" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="center">
                        <asp:Label ID="lblTranMsg" runat="server" ForeColor="green" Font-Size="10" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
            </tbody>
        </table>
        <table width="100%" runat="server" id="tblTotalDesription" class="subtablepr" border="0"
            cellpadding="0" cellspacing="4">
            <tbody>
                <tr id="trSubTotal">
                    <td align="right" width="56%">
                        <asp:Label ID="lblSubTotal" Text="<%$ Resources:Resource, msgSubTotal %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td align="right" width="44%">
                        <asp:TextBox ID="txtSubTotal" Text="0" Style="text-align: right;" ReadOnly="true"
                            runat="server" Width="140px" CssClass="frminpt fom-inputlarge" />
                    </td>
                </tr>
                <tr runat="server" id="trTax1" visible="false">
                    <td align="right">
                        <asp:Label ID="lblTax1" Text="" CssClass="lblBold" runat="server" />
                    </td>
                    <td align="right">
                        <asp:TextBox ID="txtTax1" runat="server" Style="text-align: right;" ReadOnly="true"
                            Width="150PX" CssClass="frminpt fom-inputlarge" />
                    </td>
                </tr>
                <tr runat="server" id="trTax2" visible="false">
                    <td align="right">
                        <asp:Label ID="lblTax2" Text="" CssClass="lblBold" runat="server" />
                    </td>
                    <td align="right">
                        <asp:TextBox ID="txtTax2" runat="server" Style="text-align: right;" ReadOnly="true"
                            Width="150PX" CssClass="frminpt fom-inputlarge" />
                    </td>
                </tr>
                <tr runat="server" id="trTax3" visible="false">
                    <td align="right">
                        <asp:Label ID="lblTax3" Text="" CssClass="lblBold" runat="server" />
                    </td>
                    <td align="right">
                        <asp:TextBox ID="txtTax3" runat="server" Style="text-align: right;" ReadOnly="true"
                            Width="150PX" CssClass="frminpt fom-inputlarge" />
                    </td>
                </tr>
                <tr runat="server" id="trTax4" visible="false">
                    <td align="right">
                        <asp:Label ID="lblTax4" Text="" CssClass="lblBold" runat="server" />
                    </td>
                    <td align="right">
                        <asp:TextBox ID="txtTax4" runat="server" Style="text-align: right;" ReadOnly="true"
                            Width="150PX" CssClass="frminpt fom-inputlarge" />
                    </td>
                </tr>
                <tr style="display: none;">
                    <td align="right">
                        <asp:Label ID="lblTotal" Text="<%$ Resources:Resource, msgTotalPrice %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td align="right">
                        <asp:TextBox ID="txtTotal" Text="0" runat="server" Style="text-align: right;" ReadOnly="true"
                            Width="150PX" CssClass="frminpt fom-inputlarge" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table id="Table2" runat="server" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center">
                    <div style="width: 99%; height: 100%; display: block; float: left; padding-left: 5px;">
                        <div id="spnPaymentDesc" style="color: Green; font-weight: bold; font-size: 12px;
                            width: 100%; height: auto; margin-left: -5px; display: block; float: left">
                        </div>
                        <div style="clear: both">
                        </div>
                        <div style="float: left; cursor: pointer; width: 31.9%; margin: 0 3px 5px 16%;" class="divButton"
                            id="divCash" onclick="POSPaymentModeSection('divCash');SetFocus();  ">
                            <a class="ovalbutton" href="Javascript:;"><span class="ovalbutton">
                                <%=Resources.Resource.liARCash %></span> </a>
                        </div>
                        <div class="divButton" style="float: left; cursor: pointer; padding: 0px; width: 31.9%;
                            margin: 0px 3px 5px 0px;" id="divDebit" onclick="POSPaymentModeSection('divDebit');">
                            <a class="ovalbutton" href="Javascript:;"><span class="ovalbutton">
                                <%= Resources.Resource.lblDebit%></span> </a>
                        </div>
                        <div style="clear: both">
                        </div>
                        <div style="float: left; cursor: pointer; padding: 0px; width: 31.9%; margin: 0px 3px 5px 0px;"
                            class="divButton" id="divMC" onclick="POSPaymentModeSection('divMC');">
                            <a class="ovalbutton" href="Javascript:;" style="height: auto;"><span class="ovalbutton">
                                <%= Resources.Resource.btnPaymnetMC%>
                            </span></a>
                        </div>
                        <div style="float: left; cursor: pointer; padding: 0px; width: 31.9%; margin: 0px 3px 5px 0px;"
                            class="divButton" id="divAmEx" onclick="POSPaymentModeSection('divAmEx');">
                            <a class="ovalbutton" href="Javascript:;"><span class="ovalbutton">
                                <%=Resources.Resource.btnPaymnetAmEx%></span> </a>
                        </div>
                        <div class="divButton" id="divVisa" style="float: left; cursor: pointer; width: 31.9%;
                            margin: 0px 3px 5px 0px; padding: 0px;" onclick="POSPaymentModeSection('divVisa');">
                            <a class="ovalbutton" href="Javascript:;"><span class="ovalbutton">
                                <%= Resources.Resource.btnPaymnetVisa%></span> </a>
                        </div>
                        <div style="float: left; cursor: pointer; padding: 0px; width: 31.9%; margin: 0px 3px 5px 0px;"
                            class="divButton" id="divGiftCards" onclick="GiftCardsUse();">
                            <a class="ovalbutton" href="Javascript:;" style="height: auto;"><span class="ovalbutton">
                                <%=Resources.Resource.lblGiftCards%>
                            </span></a>
                        </div>
                        <div style="float: left; cursor: pointer; padding: 0px; width: 31.9%; margin: 0px 3px 5px 0px;"
                            class="divButton" id="divInstoreCredit" onclick="InStoreCredit()">
                            <asp:HiddenField ID="hdnIsHideInstoreCredit" runat="server" Value="0" />
                            <a class="ovalbutton" href="Javascript:;"><span class="ovalbutton">
                                <%= Resources.Resource.lblInstoreCredit%></span> </a>
                        </div>
                        <div class="divButton" id="divGifts" style="float: left; cursor: pointer; width: 31.9%;
                            margin: 0px 3px 5px 0px; padding: 0px; display: none;" onclick="POSPaymentModeSection('divGifts');">
                            <a class="ovalbutton" href="Javascript:;"><span class="ovalbutton">
                                <%= Resources.Resource.lblGifts%></span> </a>
                        </div>
                        <div class="divButton" id="divOGL" style="float: left; cursor: pointer; width: 31.9%;
                            margin: 0px 3px 5px 0px; padding: 0px;" onclick="POSPaymentModeSection('divOGL');">
                            <a class="ovalbutton" href="Javascript:;"><span class="ovalbutton">
                                <%= Resources.Resource.lblOGL%></span> </a>
                        </div>
                        <div style="clear: both">
                        </div>
                        <div style="float: left; cursor: pointer; padding: 0px; width: 31.9%; margin: 0px 3px 5px 0px;"
                            class="divButtonBlue" id="divReturn">
                            <a class="blueovalbutton" id="hrfReturn" runat="server" onclick="SavePageData();"
                                href="../Returns/Default.aspx?InvokeSrc=POS&isReturn=1"><span class="blueovalbutton">
                                    <%= Resources.Resource.lblReturn%></span> </a>
                        </div>
                        <div style="float: left; cursor: pointer; padding: 0px; width: 31.9%; margin: 0px 3px 5px 0px;"
                            class="divGiftButton divButtonBlue" id="divGiftReceipt" onclick="GiftReceipt('divGiftReceipt')">
                            <a class="blueovalbutton" href="Javascript:;" style="height: auto;"><span class="blueovalbutton">
                                <%=Resources.Resource.lblGiftReceipt%>
                            </span></a>
                        </div>
                        <div class="divButtonBlue" id="divDiscount" style="float: left; cursor: pointer;
                            width: 31.9%; margin: 0px 3px 5px 0px; padding: 0px;" onclick="AddInvoiceDiscount()">
                            <a class="blueovalbutton" href="Javascript:;"><span class="blueovalbutton">
                                <%= Resources.Resource.PODiscount%></span> </a>
                        </div>
                        <div style="clear: both">
                        </div>
                        <div style="float: left; cursor: pointer; padding: 0px; width: 31.9%; margin: 0px 3px 5px 0px;"
                            class="divButtonBlue" id="divLayaway" onclick="AddToLayaway()">
                            <a class="blueovalbutton" href="Javascript:;"><span class="blueovalbutton">
                                <%= Resources.Resource.lblLayaway%></span> </a>
                        </div>
                        <div style="float: left; cursor: pointer; padding: 0px; width: 31.9%; margin: 0px 3px 5px 0px;"
                            class="divButtonBlue" id="divFindGiftCard">
                            <a class="blueovalbutton" onclick="SavePageData();" href="../NewReport/report.aspx?rpt=GiftCardTransactions.xml&InvokeSrc=POS&isReturn=1&ShowLPannel=''">
                                <span class="blueovalbutton">
                                    <%= Resources.Resource.lblFindGiftCard%></span> </a>
                        </div>
                    </div>
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td colspan="2">
                                </td>
                            </tr>
                            <tr style="height: auto;">
                                <td valign="top" colspan="2" style="padding-bottom: 10px;">
                                    <div style="display: none;">
                                        <asp:Panel CssClass="payButtonGroup" ID="pnlCash" runat="server" HorizontalAlign="Center"
                                            Height="128px">
                                            <fieldset>
                                                <legend>Cash </legend>
                                                <table border="0" cellpadding="2" cellspacing="2">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label1" CssClass="lblBold" Text="Exact Cash" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label6" CssClass="lblBold" Text="Select Bill" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label11" CssClass="lblBold" Text="Cash" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label15" CssClass="lblBold" Text="Cash" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton runat="server" ToolTip="<%$ Resources:Resource, POSExactCash %>"
                                                                    ImageAlign="Middle" ID="imgExactCash" OnClientClick="return funExactCash()" CausesValidation="false"
                                                                    ImageUrl="~/images/ExactCash.png" Width="44px" Height="44px" />
                                                                <asp:RadioButton runat="server" ID="rbExactCash" onmousedown="return funExactCash()"
                                                                    GroupName="Transaction" />
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton runat="server" ToolTip="<%$ Resources:Resource, POSDollars %>" ImageAlign="Middle"
                                                                    ID="imgDollar" OnClientClick="return funDollar()" CausesValidation="false" ImageUrl="~/images/Dollar.png"
                                                                    Width="44px" Height="44px" />
                                                                <asp:RadioButton runat="server" ID="rbDollar" onmousedown="return funDollar()" GroupName="Transaction" />
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton runat="server" ToolTip="<%$ Resources:Resource, POSByCash %>" ID="imgCash"
                                                                    OnClientClick="return funOpenKeyPad()" ImageAlign="Middle" CausesValidation="false"
                                                                    ImageUrl="~/images/Cash.png" Width="44px" />
                                                                <asp:RadioButton runat="server" ID="rbCash" onmousedown="return funOpenKeyPad()"
                                                                    GroupName="Transaction" />
                                                            </td>
                                                            <td>
                                                                <asp:RadioButton runat="server" ID="rbInStoreCredit" onmousedown="return funExactCash()"
                                                                    GroupName="Transaction" />
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlCreditDebitCart" CssClass="payButtonGroup" runat="server" HorizontalAlign="Center"
                                            Height="128px">
                                            <fieldset>
                                                <legend>Debit/Credit </legend>
                                                <table border="0" cellpadding="2" cellspacing="2">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label10" CssClass="lblBold" Text="Visa" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label12" CssClass="lblBold" Text="Master" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label13" CssClass="lblBold" Text="Amex" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label14" CssClass="lblBold" Text="Intrac" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton runat="server" ImageAlign="Middle" ID="imgVisa" CausesValidation="false"
                                                                    ImageUrl="~/images/Visa.png" OnClientClick="funCustomVisaCash(); cashTransation(true,'rbVisa'); return false;"
                                                                    Height="44px" Width="44px" />
                                                                <asp:RadioButton runat="server" ID="rbVisa" onmousedown="return funCustomVisaCash();"
                                                                    GroupName="Transaction" />
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton runat="server" ID="imgMasterCard" ImageAlign="Middle" CausesValidation="false"
                                                                    ImageUrl="~/images/MasterCard.png" OnClientClick="funCustomMasterCash(); cashTransation(true,'rbMasterCard'); return false;"
                                                                    Height="44px" Width="44px" />
                                                                <asp:RadioButton runat="server" ID="rbMasterCard" onmousedown="return funCustomMasterCash();"
                                                                    GroupName="Transaction" />
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton runat="server" ID="imgAmerican" ImageAlign="Middle" CausesValidation="false"
                                                                    ImageUrl="~/images/american_express.png" OnClientClick="funCustomAmexCash(); cashTransation(true,'rbAmerican'); return false;"
                                                                    Height="44px" Width="44px" />
                                                                <asp:RadioButton runat="server" ID="rbAmerican" onmousedown="return funCustomAmexCash()"
                                                                    GroupName="Transaction" />
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton runat="server" ID="imgInterac" ImageAlign="Middle" CausesValidation="false"
                                                                    ImageUrl="~/images/interac_logo.gif" OnClientClick="funCustomIntracCash(); cashTransation(true,'rbInterac'); return false;"
                                                                    Height="44px" Width="44px" />
                                                                <asp:RadioButton runat="server" ID="rbInterac" onmousedown="return funCustomIntracCash()"
                                                                    GroupName="Transaction" />
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <div style="display: none;">
                                        <div id="divDollar" runat="server" style="position: relative; left: 0px; top: 0px;
                                            display: none; margin: 0; height: 0;" align="left">
                                            <asp:Panel runat="server" ID="Panel2" DefaultButton="imgEnterCardManually" CssClass="cash-selectwrap">
                                                <h2 class="hd-sec">
                                                    <a href="javascript:funCloseDollar();" class="popup-close">close</a> <span class="discount_title"
                                                        style="margin-bottom: 0 !important;">
                                                        <%=lblSelectDollar%>
                                                    </span>
                                                </h2>
                                                <table class="fomkol" bgcolor="#e8eef6" width="100%" cellpadding="0" cellspacing="5">
                                                    <tbody>
                                                        <tr>
                                                            <td height="2">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Button runat="server" OnClientClick="return funInsertAmount(100)" CssClass="dollar"
                                                                    ID="Imaton1" Text="$100" />&nbsp;&nbsp;
                                                                <asp:Button runat="server" OnClientClick="return funInsertAmount(50)" CssClass="dollar"
                                                                    ID="Imageton2" Text="$50" />&nbsp;&nbsp;
                                                                <asp:Button runat="server" OnClientClick="return funInsertAmount(20)" CssClass="dollar"
                                                                    ID="ImageBton3" Text="$20" />&nbsp;&nbsp;
                                                                <asp:Button runat="server" OnClientClick="return funInsertAmount(10)" CssClass="dollar"
                                                                    ID="ImgeButton4" Text="$10" />&nbsp;&nbsp;
                                                                <asp:Button runat="server" OnClientClick="return funInsertAmount(5)" CssClass="dollar"
                                                                    ID="ImageBut5" Text="$5" />&nbsp;&nbsp;
                                                                <asp:Button runat="server" OnClientClick="return funInsertAmount(2)" CssClass="dollar"
                                                                    ID="utton7" Text="$2" />&nbsp;&nbsp;
                                                                <asp:Button runat="server" OnClientClick="return funInsertAmount(1)" CssClass="dollar"
                                                                    ID="Ima6" Text="$1" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="2">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <%--<div style="float: right; cursor: pointer;" class="divButton" id="divGifts" onclick="POSPaymentModeSection('divGifts');">
                                        <a class="ovalbutton" href="Javascript:;"><span class="ovalbutton">
                                            <%=Resources.Resource.lblGifts %></span> </a>
                                    </div>--%>
                                </td>
                                <td valign="top">
                                    <%--<div style="float: left; cursor: pointer;" class="divButton" id="divWaste" onclick="POSPaymentModeSection('divWaste');">
                                        <a class="ovalbutton" href="Javascript:;"><span class="ovalbutton">
                                            <%= Resources.Resource.POSLost %></span> </a>
                                    </div>--%>
                                    <div style="display: none;">
                                        <asp:Panel ID="pnlGifts" runat="server" class="payButtonGroup" Style="height: 128px;
                                            text-align: center; float: left; width: 260px;">
                                            <fieldset>
                                                <legend>Gift </legend>
                                                <table border="0" cellpadding="2" cellspacing="2">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblGift" CssClass="lblBold" Text="Gift" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label7" CssClass="lblBold" Text="Staff Gift" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label8" CssClass="lblBold" Text="Reason for Gift" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton runat="server" ID="imgGifts" ToolTip="Gift" ImageAlign="Middle"
                                                                    OnClientClick="return funGift()" CausesValidation="false" ImageUrl="~/Images/gift.png"
                                                                    Height="44px" Width="44px" />
                                                                <asp:RadioButton runat="server" ID="rbGifts" onmousedown="return funGift()" GroupName="Transaction" />
                                                                <asp:RadioButton runat="server" ID="rbOGL" onmousedown="return funOGL()" GroupName="Transaction" />
                                                                <asp:RadioButton runat="server" ID="rbGiftCards" onmousedown="return funGift()" GroupName="Transaction" />
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton runat="server" ID="imgStaffGifts" ToolTip="Staff Gift" ImageAlign="Middle"
                                                                    OnClientClick="return funStaffGift()" CausesValidation="false" ImageUrl="~/Images/staff_gift.png"
                                                                    Height="44px" Width="44px" />
                                                                <asp:RadioButton runat="server" ID="rbStaffGifts" onmousedown="return funStaffGift()"
                                                                    GroupName="Transaction" />
                                                            </td>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="4">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlReasonForGift" runat="server">
                                                                                <asp:ListItem Text="Select Reason" />
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label9" CssClass="lblBold" Text="Authorized By" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlAuthorizedBy" runat="server">
                                                                                <asp:ListItem Text="Authorize By" Value="" />
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlWaste" runat="server" class="payButtonGroup" Style="height: 128px;
                                            float: right; width: 70px;">
                                            <fieldset>
                                                <legend>Waste </legend>
                                                <asp:ImageButton runat="server" ID="imgLost" ToolTip="<%$ Resources:Resource, POSLost %>"
                                                    ImageAlign="Middle" OnClientClick="return funLost()" CausesValidation="false"
                                                    ImageUrl="~/images/waste.png" Style="height: 44px; width: 44px; border-width: 0px;"
                                                    align="middle" class="waste-ico" />
                                                <asp:RadioButton runat="server" ID="rbLost" onmousedown="return funLost()" GroupName="Transaction" />
                                            </fieldset>
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" colspan="2" style="padding-bottom: 10px;">
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" colspan="2" style="padding-bottom: 10px;">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:Panel ID="pnlHidden" runat="server">
                        <asp:RadioButton Visible="false" runat="server" ID="rbEnterCardInfo" AutoPostBack="true"
                            GroupName="Transaction" />
                        <asp:ImageButton Visible="false" runat="server" ID="imgEnterCardInfo" ImageAlign="Middle"
                            OnClientClick="return funCardManuallyEnter()" CausesValidation="false" ImageUrl="~/images/ix_pay__thumb.jpg" />
                    </asp:Panel>
                    <asp:CustomValidator ID="cusvalPaymentBy" runat="server" ErrorMessage="<%$ Resources:Resource, POScusvalPaymentBy %>"
                        ClientValidationFunction="funPaymentBy" Display="None" />
                </td>
            </tr>
            <%--</tbody>--%>
        </table>
    </td>
    </tr>
    <tr>
        <td>
            <asp:HiddenField ID="hdnTax" runat="server"></asp:HiddenField>
        </td>
    </tr>
    <tr class="titleBgColorPos">
        <td colspan="2">
        </td>
    </tr>
    <tr>
        <td colspan="2" align="left">
        </td>
    </tr>
    <tr id="trProcess" runat="server">
        <td colspan="2" align="left" valign="top">
            <asp:Panel runat="server" ID="pnlProcess">
                <asp:HiddenField runat="server" ID="hdnPrice" />
                <%--Remaining part 1 End Here
                            <asp:GridView ID="grdProcess" runat="server" AllowSorting="True" DataSourceID="sqldsProcess"
                                AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0"
                                GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
                                CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="ProductID"
                                Width="790px">
                                <Columns>
                                    <asp:BoundField DataField="ProductID" HeaderText="<%$ Resources:Resource, grdPoProductID %>"
                                        ReadOnly="True" SortExpression="ProductID">
                                        <ItemStyle Width="70px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="prdUPCCode" HeaderText="<%$ Resources:Resource, grdPOSUPC%>"
                                        ReadOnly="True" SortExpression="prdUPCCode">
                                        <ItemStyle Width="70px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ProductName" HeaderText="<%$ Resources:Resource, grdPOProductName %>"
                                        ReadOnly="True" SortExpression="ProductName">
                                        <ItemStyle Width="250px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="prdOhdQty" HeaderStyle-HorizontalAlign="Right" HeaderText="<%$ Resources:Resource, grdvOnHandQty %>"
                                        ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                        <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="prdEndUserSalesPrice" HeaderStyle-HorizontalAlign="Right"
                                        HeaderText="<%$ Resources:Resource, grdPOPrdCostPrice %>" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                        <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="" HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("ProductID") %>'
                                                CommandName="Edit" CausesValidation="false" ImageUrl="~/images/add-btn.png" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="grid_footer" />
                                <RowStyle CssClass="grid_rowstyle" />
                                <PagerStyle CssClass="grid_footer" />
                                <HeaderStyle CssClass="grid_header" Height="26px" />
                                <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                <PagerSettings PageButtonCount="20" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="sqldsProcess" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                            <%--Remaining part 1 Start Here--%>
            </asp:Panel>
        </td>
    </tr>
    <tr class="titleBgColorPos">
        <td class="tdAlignLeft" colspan="2">
            <div id="divItems" runat="server">
            </div>
        </td>
    </tr>
    <tr>
        <td class="tdAlignLeft" colspan="2">
            <asp:Label ID="lblProIL" CssClass="lblBold" Text="<%$ Resources:Resource, POSPrdlst%>"
                runat="server" Visible="false" />
        </td>
    </tr>
    </tbody> </table>
    <%--Div for Popup of pos Menu --%>
    <div id="divPOSMenu" runat="server" style="display: none;">
        <asp:Panel runat="server" ID="pnl">
            <h2>
                <span class="discount_title" style="margin-bottom: 12px !important">
                    <%=lblPrdMenu%>
                    <a href="javascript:funClose();" id="A1" title="Back to Category Menu" align="right"
                        value="Back" class="ingridbtn-back cobtn"></a></span>
            </h2>
            <div class="prdgrid-scroller">
                <div class="scroll-holder">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr valign="top">
                                <td>
                                    <asp:DataList ID="dlstImages" runat="server" CellSpacing="12" Width="100%" Height="400px"
                                        DataSourceID="sqlImg" DataKeyField="ProductID" GridLines="none" RepeatColumns="8"
                                        RepeatDirection="Horizontal" CssClass="titleBgColorPos" OnItemDataBound="dlstImages_OnItemDataBound"
                                        OnItemCommand="dlstImages_OnItemCommand">
                                        <ItemTemplate>
                                            <table cellpadding="2" cellspacing="2" border="0" class="AlbumColor">
                                                <tr>
                                                    <td align="left">
                                                        <asp:Label ID="lblImag" runat="server" Text='<%# Eval("prdSmallImagePath") %>'></asp:Label>
                                                        <asp:ImageButton ID="imgPOSMenu" CausesValidation="false" runat="server" Width="90px"
                                                            Height="90px" ToolTip='<%# Eval("prdname") %>' CommandArgument='<%# Eval("ProductID") %>'
                                                            CommandName="Add" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="lblPrdName" ForeColor="black" Font-Bold="true" runat="server" Text='<%# Eval("prdname") %>'
                                                            ToolTip='<%# Eval("prdname") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top" Width="80px" Height="100px" />
                                    </asp:DataList>
                                    <asp:SqlDataSource ID="sqlImg" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td height="3">
                                    <asp:HiddenField ID="hdnListValue" runat="server" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <asp:HiddenField runat="server" ID="hdnPrdQty"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdnPrintUrl"></asp:HiddenField>
    <%--Div for Popup of pos category --%>
    <div id="divPOSCategory" runat="server" class="productgrid-popup" style="display: none;
        top: 140px;">
        <asp:Panel runat="server" ID="pnlPosCat">
            <h2>
                <span class="discount_title" style="margin-bottom: 12px !important; text-align: left;">
                    <%--<%=lblPOSPrdCatgMenu%>--%>
                    <literal id="ltrMenuHdg"></literal>
                    <a href="javascript:;" id="imgCmdBackToCatg" title="Back to Category Menu" align="right"
                        value="Back" class="ingridbtn-back cobtn"></a></span>
            </h2>
            <div class="prdgrid-scroller" style="padding: 5px 5px 5px 5px;">
                <div class="scroll-holder">
                    <table cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr valign="top">
                                <td>
                                    <asp:DataList ID="dlstCatgImgs" runat="server" DataSourceID="SqlDSrcPOSCatg" DataKeyField="prdPOSCatgID"
                                        GridLines="none" RepeatColumns="3" RepeatDirection="Horizontal" CssClass="titleBgColorPos"
                                        OnItemDataBound="dlstCatgImgs_OnItemDataBound ">
                                        <ItemTemplate>
                                            <div class="gridryt-space" style="padding-left: 12px;">
                                                <table cellpadding="2" cellspacing="8" border="0" class="AlbumColor " onclick="populetItemsByCategory(this); return false;"
                                                    id='tblPOsCate' runat="server" name='<%# Eval("prdPOSCatgID") %>'>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Label ID="lblPOSCatgImg" runat="server" Text='<%# Eval("prdPOSCatgImgPath") %>'></asp:Label>
                                                            <asp:HiddenField ID="hdnCateID" runat="server" Value='<%# Eval("prdPOSCatgID") %>' />
                                                            <asp:ImageButton ID="imgPOSCatgMenu" CausesValidation="false" runat="server" ToolTip='<%# Eval("prdPOSCatgDesc") %>'
                                                                Width="90px" Height="90px" CommandArgument='<%# Eval("prdPOSCatgID") %>' CommandName="ShowProducts" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Label ID="lblPrdPOSCatgDesc" ForeColor="black" Font-Bold="true" runat="server"
                                                                Text='<%# Eval("prdPOSCatgDesc") %>' ToolTip='<%# Eval("prdPOSCatgDesc") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top" Width="80px" Height="100px" />
                                    </asp:DataList>
                                    <asp:SqlDataSource ID="SqlDSrcPOSCatg" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td height="3">
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <table id="tblManager" border="0" style="display: none;" cellspacing="10">
                    <tr>
                        <td colspan="2">
                            <div class="buttonwrapper">
                                <a id="openCloseReg" runat="server" class="ovalbutton" validationgroup="lstSrch"
                                    onclick="SavePageData();" href="~/POS/ManageRegisterNew.aspx"><span class="ovalbutton"
                                        style="min-width: 70px; text-align: center;">
                                        <%=Resources.Resource.lblOpenCloseRegister%>
                                    </span></a>
                            </div>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td colspan="2">
                            <div class="buttonwrapper">
                                <a id="A2" runat="server" class="ovalbutton" validationgroup="lstSrch" onclick="SavePageData();"
                                    href="../Shipping/ClockIn.aspx?InvokeSrc=POS&ShowLPannel=''"><span class="ovalbutton"
                                        style="min-width: 70px; text-align: center;">
                                        <%=Resources.Resource.lblClockInOut%>
                                    </span></a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="buttonwrapper">
                                <a id="A9" runat="server" class="ovalbutton" validationgroup="lstSrch" onclick="SavePageData();"
                                    href="../Sales/ViewSalesOrder.aspx?Default=1&t=5&InvokeSrc=POS"><span class="ovalbutton"
                                        style="min-width: 70px; text-align: center;">
                                        <%=Resources.Resource.lblReserve%>
                                    </span></a>
                            </div>
                            <%--<div class="buttonwrapper">
                                <a id="A3" runat="server" class="ovalbutton" validationgroup="lstSrch" href="~/Returns/Default.aspx?InvokeSrc=POS&isReturn=1">
                                    <span class="ovalbutton" style="min-width: 70px; text-align: center;">
                                        <%=Resources.Resource.lblReturn%>
                                    </span></a>
                            </div>--%>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td colspan="2">
                            <div class="buttonwrapper">
                                <a id="A5" runat="server" class="ovalbutton" validationgroup="lstSrch" onclick="SavePageData();"
                                    href="../Invoice/ViewInvoice.aspx?Default=1&dflt=Y&t=8&InvokeSrc=POS&ShowLPannel=''">
                                    <span class="ovalbutton" style="min-width: 70px; text-align: center;">
                                        <%=Resources.Resource.btnTabrecipt%>
                                    </span></a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="buttonwrapper">
                                <a id="A6" runat="server" class="ovalbutton" validationgroup="lstSrch" onclick="SavePageData();"
                                    href="~/Returns/Default.aspx?InvokeSrc=POS&isTransfer=1"><span class="ovalbutton"
                                        style="min-width: 70px; text-align: center;">
                                        <%=Resources.Resource.lblReceiveTransfer%>
                                    </span></a>
                            </div>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td colspan="2">
                            <div class="buttonwrapper">
                                <a id="hrfViewSalesOrder" runat="server" class="ovalbutton" validationgroup="lstSrch"
                                    onclick="SavePageData();" href="../Sales/ViewSalesOrder.aspx?Default=1&t=5&ordType=2&InvokeSrc=POS&custID=custID">
                                    <span class="ovalbutton" style="min-width: 70px; text-align: center;">
                                        <%=Resources.Resource.lblRequestTransfers%>
                                    </span></a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="buttonwrapper">
                                <a id="A3" runat="server" class="ovalbutton" validationgroup="lstSrch" onclick="SavePageData();"
                                    href="../NewReport/report.aspx?rpt=InventoryHitParadePOS.xml&InvokeSrc=POS&ShowLPannel=''">
                                    <span class="ovalbutton" style="min-width: 70px; text-align: center;">
                                        <%=Resources.Resource.lblInventaireHitParade%>
                                    </span></a>
                            </div>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td colspan="2">
                            <div class="buttonwrapper">
                                <a id="A10" runat="server" class="ovalbutton" validationgroup="lstSrch" onclick="SavePageData();"
                                    href="../NewReport/report.aspx?rpt=TodaySalesReport.xml&InvokeSrc=POS&ShowLPannel=''">
                                    <span class="ovalbutton" style="min-width: 70px; text-align: center;">
                                        <%=Resources.Resource.lblTodaySales%>
                                    </span></a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="buttonwrapper">
                                <a id="A4" runat="server" class="ovalbutton" validationgroup="lstSrch" onclick="SavePageData();"
                                    href="../NewReport/report.aspx?rpt=DailySalesReport.xml&InvokeSrc=POS&ShowLPannel=''">
                                    <span class="ovalbutton" style="min-width: 70px; text-align: center;">
                                        <%=Resources.Resource.lblDailyPOSSales%>
                                    </span></a>
                            </div>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <div class="buttonwrapper">
                                <a id="A7" runat="server" class="ovalbutton" validationgroup="lstSrch" onclick="SavePageData();"
                                    href="../NewReport/report.aspx?rpt=SalesTransactionSummary.xml&InvokeSrc=POS&ShowLPannel=''">
                                    <span class="ovalbutton" style="min-width: 70px; text-align: center;">
                                        <%=Resources.Resource.lblSalesTransactionSummary%>
                                    </span></a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="buttonwrapper">
                                <a id="A8" runat="server" class="ovalbutton" validationgroup="lstSrch" onclick="SavePageData();"
                                    href="../NewReport/report.aspx?rpt=DailyPOSSalesDetails.xml&InvokeSrc=POS&ShowLPannel=''">
                                    <span class="ovalbutton" style="min-width: 70px; text-align: center;">
                                        <%=Resources.Resource.lblDailyPOSDetail%>
                                    </span></a>
                            </div>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table id="tblQuickMenu" style="display: none;">
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
    <asp:HiddenField ID="hdnchk" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnCtgID" runat="server"></asp:HiddenField>
    <%--Div for Popup of pos category Products--%>
    <div id="divPOSCatgProductMenu" runat="server" class="productgrid-popup splhack"
        style="display: none; text-align: center;">
        <asp:Panel runat="server" ID="Panel3">
            <h2 class="pos-relative">
                <span class="slideaction-cvr">&nbsp;</span><span class="discount_title">
                    <%=lblPrdMenu%>
                    <%-- <asp:ImageButton ID="imgCmdBackToCatg1" align="right" CausesValidation="false" runat="server"
                            CssClass="ingridbtn-back" ToolTip="<%$ Resources:Resource, ttipBcktoCtgMenu %>"
                             OnClientClick="return funBackToCat();" ImageUrl="#" />--%>
                    <input id="imgCmdBackToCatg1" title='<%$ Resources:Resource, ttipBcktoCtgMenu %>'
                        onclick="return funBackToCat();" runat="server" align="right" value="Back" class="ingridbtn-back" />
                </span>
            </h2>
            <div class="prdgrid-scroller">
                <div class="scroll-holder">
                    <table class="" width="100%">
                        <tbody>
                            <tr>
                                <td height="3" class="ingrid-action">
                                    <div class="pos-relative">
                                        <asp:HiddenField ID="HiddenField2" runat="server" />
                                        <asp:HiddenField ID="hdnPOSPrdMenu" runat="server" />
                                        <asp:HiddenField ID="hdnLeft" runat="server" />
                                        <asp:HiddenField ID="hdnkitLeft" runat="server" />
                                        <asp:ImageButton ID="imgCmdPrdKitInfo" align="left" CausesValidation="false" runat="server"
                                            ToolTip="<%$ Resources:Resource, ttipShwKitPrd %>" ImageUrl="../images/package_view.png"
                                            OnClientClick="showKitProducts(); return false;" Visible="false" />
                                    </div>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td>
                                    <asp:DataList ID="dlPOSCatgProduct" runat="server" DataSourceID="SqlDSrcPOSCatgProduct"
                                        DataKeyField="ProductID" GridLins="none" RepeatColumns="3" RepeatDirection="Horizontal"
                                        CssClass="titleBgColorPos">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnkit" runat="server" Value='<%# Eval("prdIsKit") %>' />
                                            <a id="aPrdKit" style="cursor: pointer;" class="gridryt-space">
                                                <table cellpadding="2" cellspacing="2" border="0" onclick="javascript:__doPostBack1(this);"
                                                    class="inner-grid in-poptd" id='tblPOsProd' name='<%# Eval("ProductID") & "~" & Eval("prdIsKit") %>'
                                                    runat="server">
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Label ID="imgbtnPOSCatgProductName" Style="cursor: hand;" ForeColor="black"
                                                                Font-Bold="true" runat="server" Text='<%# Eval("prdname") %> ' ToolTip='<%# Eval("prdname") %>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </a>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top" Width="80px" Height="100px" />
                                    </asp:DataList>
                                    <asp:SqlDataSource ID="SqlDSrcPOSCatgProduct" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                                    <input type="hidden" name="hdnProductID" id="hdnProductID" value="" runat="server" />
                                    <input type="hidden" name="hdnProductKit" id="hdnProductKit" value="" runat="server" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <%--div for PrductKitContent --%>
    <div id="divProductKitContent" runat="server" style="display: none; text-align: center;">
        <asp:Panel runat="server" ID="Panel24">
            <table border="0" cellpadding="0" cellspacing="0" width="500px">
                <tbody>
                    <tr>
                        <td style="background: [../images/popup_left.png];" width="15px">
                        </td>
                        <td class="popup_title_newbg" align="left" valign="middle">
                            <span class="discount_title">
                                <%=lblProductKitItem%></span>
                        </td>
                        <td class="popup_title_newbg" align="right">
                            <a href="javascript:funPOSProdKitClose();">
                                <img src="../images/close_popup_btn.gif" border="0" height="24" width="24"></a>
                        </td>
                        <td background="../images/popup_right.png" width="15px">
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td colspan="4">
                            <div>
                                <table id="grdPrdKitContent" runat="server" style="width: 500px; border-collapse: collapse;"
                                    rules="cols" border="1" cellpadding="3" cellspacing="0">
                                    <tbody>
                                        <tr style="color: White; background-color: rgb(95, 96, 98); font-weight: bold; height: 15px;"
                                            align="left" valign="middle">
                                            <td style="white-space: nowrap; color: White;">
                                                Name
                                            </td>
                                            <td style="white-space: nowrap; color: White;">
                                                Quantity
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <asp:SqlDataSource ID="SqlDSrcPOSProdKitContent" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr height="5px">
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td colspan="2" align="center">
                            <asp:HiddenField ID="hdnCatgID" runat="server"></asp:HiddenField>
                            <asp:Label runat="server" ID="lblcatprdkitmsg" ForeColor="Red"></asp:Label>
                            <asp:ImageButton ID="imgCmdAddKitPrd" CausesValidation="false" runat="server" ImageUrl="../images/Add.png" /><br />
                            <br />
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
    </div>
    <asp:ValidationSummary ID="valsSales" runat="server" ShowMessageBox="true" ShowSummary="false" />
    <asp:ValidationSummary ID="valsSales1" runat="server" ShowMessageBox="true" ShowSummary="false"
        ValidationGroup="lstSrch" />
    </div>
    <table>
        <tr runat="server" id="trTransaction" visible="false">
            <td colspan="3">
                <div id="divTransaction" runat="server" align="left" style="position: relative; left: 170px;
                    top: 133px; margin: 0; height: 0;">
                    <asp:Panel runat="server" ID="pnlTransaction" DefaultButton="imgCardSubmit">
                        <table cellpadding="0" cellspacing="0" width="350px" height="150">
                            <tr>
                                <td width='15px' background="../Images/popup_left.png">
                                </td>
                                <td align="left" valign="middle" class="popup_title_newbg">
                                    <span class="discount_title">
                                        <%=lblCardInfo%>
                                    </span>
                                </td>
                                <td class="popup_title_newbg" align="right">
                                    <a href="javascript:funCloseTransaction();">
                                        <img border="0" src='../images/close_popup_btn.gif' width="24" height="24" /></a>
                                </td>
                                <td width='15px' background="../Images/popup_right.png">
                                </td>
                            </tr>
                        </table>
                        <table width="350px" class="popup_content_newbg">
                            <tr id="Tr1" runat="server" visible="false">
                                <td align="center" colspan="2">
                                    <asp:TextBox runat="server" ID="txtCardInfo" ValidationGroup="Card" TextMode="password"
                                        Width="320px"></asp:TextBox><span style="color: Red; font-weight: bold; font-size: 11pt;">
                                            *</span>
                                    <asp:RequiredFieldValidator ID="reqvalCardInfo" ValidationGroup="Card" runat="server"
                                        ControlToValidate="txtCardInfo" ErrorMessage="<%$ Resources:Resource, reqvalCardInfo %>"
                                        SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td height="10">
                                    <applet id="idApplet" runat="server" code="applet.client.TenderRetailApplet.class"
                                        archive="STenderRetailClient.jar" width="200" height="90">
                                        <param name="host" value="<%= Session("host")%>" />
                                        <param name="hostPort" value="<%= Session("hostPort")%>" />
                                        <%--<param name="credit_DebitCardNumber" value="4242424242424242" />--%>
                                        <param name="transactionType" value="<%= Session("transactionType")%>" />
                                        <param name="amount" value="<%= Session("amount")%>" />
                                        <param name="transactionID" value="<%= Session("transactionID")%>" />
                                        <param name="terminalID" value="<%= Session("terminalID")%>" />
                                        <%--<param name="log-file" value="file:/C:/temp/iTECH.log " />--%>
                                    </applet>
                                </td>
                            </tr>
                            <tr id="Tr2" runat="server" visible="false">
                                <td align="center" colspan="2">
                                    <asp:ImageButton runat="server" ID="imgCardSubmit" ValidationGroup="Card" ImageUrl="~/images/submit.png" />
                                </td>
                            </tr>
                            <tr>
                                <td height="2">
                                </td>
                            </tr>
                            <asp:ValidationSummary ID="valCardInfo" runat="server" ShowMessageBox="true" ValidationGroup="Card"
                                ShowSummary="false" />
                        </table>
                    </asp:Panel>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <script language="javascript" type="text/javascript">
        var isMasterNeeded=false
          $(document).ready(function(){
                    $("#lblGlobalRegCode").html("<b>"+ secReg +"</b>");
          var iscompleted=loadBarCodeItems();
          $("body").click(function(){
                if(!isMasterNeeded){
                    $("#ctl00_cphMaster_lblMsg").html("");
                }else{
                    isMasterNeeded=false;
                }


$('#<%=txtReceived.ClientID%>').focus(function(event) {
    setTimeout(function() {$('#<%=txtReceived.ClientID%>').select();}, 0);
});
          });
          if(secTID!=""){            
            while(!iscompleted){}
            populetTabData(secTID)
          }
          if(customerID!=""){
          setCurrentGuest(customerID);
          }
          if(customerID =="")
          {
            var hdnDefaultCustID = $('#<%=hdnDefaultCustID.ClientID%>');
            if ( parseInt(hdnDefaultCustID.val()) > 0) {
            setCurrentGuest(hdnDefaultCustID.val());
            }
          }
          });
          var COMPort='<%= ConfigurationManager.AppSettings("defaultCOMPort") %>';        
          var localArr=new Array();          
          var localArrOrg=new Array();  
          var secLang;
          var secWareHouse;
          var secHost;
          var secHostPort;
          var secTerminalID;
          var secToken;
          var secUserId;
          var secTID='';
          var secCmpAddress='';
          var secCmpName='';
          var secCmpPhone='';
          var customerID = '';
          var ipAddress = '';
          var secReg ='<%= Session("RegCode") %>';
          var secRegMsg ='';
//          var secRegMsg ='<%= Session("RegMessage").ToString().Replace(vbNewLine, "__nl__") %>';
          var secRegName='<%= Session("RegName") %>';
          var secRegLogFile='<%= Session("RegLogFilePath") %>';
          var secMerchantID='<%= ConfigurationManager.AppSettings("MerchantID").ToLower %>';
          var secNoReceipt='<%= ConfigurationManager.AppSettings("IsReceipt").ToLower %>';  
          var lastInvoiceNo = '';
          var vMailToSend ='';
          var vIsGiftReceipt ='0';
          var vAmountRefundedVia ='';
            var visPrint ='' ;
            var vbtnType ='';
          
//          lblGlobalRegCode

                  
            <%
                Dim objCompany As New clsCompanyInfo
                objCompany.GetCompanyAddress()
                Dim objComman As New clsCommon
                Dim objProd As New clsProducts
                Dim strValue As New Hashtable
                objProd._CustID=  hdnDefaultCustID.Value 
                'strValue = objProd.subGetProductsArray()
                Dim value As String()
                Dim keys As System.Collections.IEnumerator
                Dim key As String
                Dim tax() As String = {"__taxGrp__"}
                keys = strValue.Keys.GetEnumerator
                If Request.QueryString("TType") = "P" Then
            %>  
                secTID='<%= Request.QueryString("TID")%>';
            <%
                End if
            %>
                customerID = '<%= Request.QueryString("custID")%>';
                secCmpName='<%= objCompany.CompanyName %>';
                secCmpPhone='<%= objCompany.CompanyPOPhone %>';
                secCmpAddress='<%= objCompany.CompanyAddressLine1 & "__nl__" & objCompany.CompanyCity & " " & objCompany.CompanyState & "__nl__" & objCompany.CompanyCountry & " " & objCompany.CompanyPostalCode %>';
                secLang='<%= objComman.funGetProductLang() %>';
                //secWareHouse='<%= Session("UserWarehouse") %>';
                secWareHouse='<%= Session("RegWhsCode") %>';
                secHost= '<%= Session("host") %>';
                secHostPort= '<%= Session("hostPort") %>';
                secTerminalID= '<%= Session("terminalID") %>';
                secToken= '<%= objProd.funGetToken(Session("UserID")) %>';
                var secUserId= '<%= Session("UserID") %>';
                //lastInvoiceNo = '<%= Session("lastInvoiceNo")%>'
            <%  
                While keys.MoveNext
                   key = keys.Current.ToString
                   value = strValue.Item(key).ToString.Split(tax,System.StringSplitOptions.None)
                   Dim data As String = value(0)
                   Dim tagSpliter() As String = {"__tags__"}
                   Dim taxData As String = (value(1).Split(tagSpliter,System.StringSplitOptions.None))(0)
                   Dim tags As String = (value(1).Split(tagSpliter,System.StringSplitOptions.None))(1)
            %>
                    //alert("<%= tags %>");
                   var obj=new Object;
                   obj.data="<%= data%>";
                   obj.tax="<%= taxData %>";
                   obj.tags="<%= tags %>";
                   obj.category="<%= objProd.funGetCategoryName(key) %>";
                   localArr["<%= key %>"]= obj;
                   localArrOrg["<%= key %>"] = obj;
            <%  
                End While
            %>
    </script>
    <script type="text/javascript">

        function setPrdUpcCode(val) {
            $('#<%=txtSearch.ClientID%>').val(val);
            $('#<%=imgSearch.ClientID%>').trigger('click');
        }
        var custCurrencyCode = "";
        var custType = "";
        var custInvoiceNetTerms = "";
        function setGuestInformation() {

            var vCustTitle = '<table><tr>';
            if (_objClientGuest.Alert != '') {
                vCustTitle += "<td> <image id='imgWarning' src='../Images/Warning.png' width='35px' style='cursor:pointer;' onclick='ShowCustomeAlert()' /></td>";
            }
            vCustTitle += "<td class='CustTitleCss'>" + _objClientGuest.PartnerLongName + "&nbsp;<span id='spnRedMark' style='color:red;'>*</span> &nbsp;" + _objClientGuest.PartnerPhone + "</td>";
            vCustTitle + "</tr></table>";

            //vCustTitle =  _objClientGuest.PartnerLongName + "&nbsp;&nbsp;" + _objClientGuest.PartnerPhone ;

            $("#lblCustomerTitle").html(vCustTitle);
            custCurrencyCode = _objClientGuest.PartnerCurrencyCode;
            custType = _objClientGuest.PartnerType;
            custInvoiceNetTerms = _objClientGuest.PartnerInvoiceNetTerms;
            //            alert(_objClientGuest.Alert);
            $("#hdnCustomerAlert").val(_objClientGuest.Alert);
            $("#hdnCustMail").val(_objClientGuest.PartnerEmail);


            //$("#divInstoreCredit").css("display", "none");
        }
        var custID = "";
        function setCurrentGuest(guestid) {
            $('#<%=txtPopSearch.ClientID%>').val("");
//            $('#<%=hdnDefaultCustID.ClientID%>').val(guestid);
            var gid = {};
            custID = guestid;
            gid.guestid = guestid;
            $.ajax({
                type: "POST",
                url: "ajax.aspx/GetGuest",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(gid),
                success: function (data) {
                    _objClientGuest = data.d[0];
                    $("#ctl00_cphMaster_txtDisc").attr('value', Number(_objClientGuest.PartnerDiscount));
                    $("#ctl00_cphMaster_hdnDisc").attr('value', Number(_objClientGuest.PartnerDiscount));
                    setGuestInformation();

                    // To Show/Hide Instore Cr. Option
                    if (Number(data.d[1]).toFixed(2) <= 0) {
                        $("#ctl00_cphMaster_hdnIsHideInstoreCredit").val("0");
                        //hdnIsHideInstoreCredit
                        $("#spnRedMark").css("display", "none");
                    }
                    else {
                        //  $("#divInstoreCredit").css("display", "block");
                        $("#ctl00_cphMaster_hdnIsHideInstoreCredit").val("1");
                    }


                    ApplyNewCustomerDiscount(Number(_objClientGuest.PartnerDiscount));
                    $("#ctl00_cphMaster_txtSearch").focus();
                    $("#ctl00_cphMaster_hrfViewSalesOrder").attr("href", "../Sales/ViewSalesOrder.aspx?Default=1&t=5&ordType=2&InvokeSrc=POS&custID=" + _objClientGuest.PartnerLongName + "&whsCode=" + secWareHouse + "&PartnerID=" + guestid);
                },
                error: function (request, status, errorThrown) {
                    alert(status);
                }
            });
        }


        function ShowCustomeAlert() {
            var url = 'ShowCustomerAlert.aspx';
            var queryData = {};
            queryData.Alert = $("#hdnCustomerAlert").val();
            var t = "<%=Resources.Resource.lblAlert%>";
            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 450,
                height: 250,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');
            return false;
        }

    </script>
    <script type="text/javascript">
        var isiPad = navigator.userAgent.match(/iPad/i) != null;
        $(document).ready(function () {

            if (secWareHouse != 'OGL') {
                $("#divOGL").css('display', 'none');
            }
            $('#<%=txtSearch.ClientID%>').focus();
            $("#clicked").click(function () {
                if ($('#ctl00_cphMaster_divPOSCategory').css('display') != "none") {
                    $('#ctl00_cphMaster_divPOSCategory').hide("slide", { direction: "right" }, 1000);
                    $('.pullout-btn').animate({ 'right': '-32px' }, 1000);
                    $("#clicked").removeClass('click-hide');
                }
                else if ($('#ctl00_cphMaster_divPOSCategory').css('display') == "none") {
                    $('#ctl00_cphMaster_divPOSCategory').show("slide", { direction: "right" }, 1000);
                    if (isiPad) {
                        $('.pullout-btn').animate({ 'right': '380px' }, 1000);
                    }
                    else {
                        $('.pullout-btn').animate({ 'right': '372px' }, 1000);
                    }
                    $("#clicked").addClass('click-hide');
                }
            });

            function setFocusSearchbox() {
                $('#<%=txtSearch.ClientID%>').focus();
            }

            //$("#quickslide").click(function () {
            //                if ($('#ctl00_cphMaster_divPOSCategory').css('display') == "none") {
            //                    $('#ctl00_cphMaster_divPOSCategory').show("slide", { direction: "right" }, 1000);
            //                    if (isiPad) {
            //                        $('.pullout-btn').animate({ 'right': '380px' }, 1000);
            //                    }
            //                    else {
            //                        $('.pullout-btn').animate({ 'right': '372px' }, 1000);
            //                    }
            //                    $("#clicked").addClass('click-hide');
            //                }
            //    });

            $("#imgCmdBackToCatg").click(function () {
                if ($('#ctl00_cphMaster_divPOSCategory').css('display') != "none") {
                    $('#ctl00_cphMaster_divPOSCategory').hide("slide", { direction: "right" }, 1000);
                    $('.pullout-btn').animate({ 'right': '-32px' }, 1000);
                    $("#clicked").removeClass('click-hide');
                }
            });



        });
    </script>
    <script type="text/javascript">
        $("#popPrdSearch").click(function () {
            var postData = {};
            postData.keyword = $("#<%=txtPopSearch.ClientID%>").val();
            //                                var prdSearchWin = window.open("ProductSearchModal.aspx?" + $.param(postData), "ProductSearch", "menubar=0,resizable=0,width=750,height=450,scrollbars=1");
            var $dialog = jQuery.FrameDialog.create({ url: 'ProductSearchModal.aspx?' + $.param(postData),
                title: "<%=Resources.Resource.lblPOSSelectProduct %>",
                //loadingClass: "loading-image",
                modal: true,
                width: 700,
                height: 460,
                autoOpen: false,
                close: function (event, ui) {
                    $("#ctl00_cphMaster_txtSearch").focus();
                }
            });
            $dialog.dialog('open');
        });

        function srhCustomer() {
            var vSearchKey = getParameterByName('srchKey');
            dataToPost = {}
            dataToPost.orderFrom = "POS";
            dataToPost.keyword = $("#<%=txtPopSearch.ClientID%>").val();
            if (vSearchKey != "") {
                dataToPost.srchKey = vSearchKey;
            }

            //var $dialog = jQuery.FrameDialog.create({ url: '../Sales/mdSearchCustomer.aspx?' + $.param(dataToPost),
            var $dialog = jQuery.FrameDialog.create({ url: '../POS/SearchCustomer.aspx?' + $.param(dataToPost),
                title: "<%=Resources.Resource.lblSelectVIP %>",
                //loadingClass: "loading-image",
                modal: true,
                width: 700,
                height: 460,
                autoOpen: false
            });
            $dialog.dialog('open');

            return false;
        }


        //Auto complete initilizer
        jQuery(document).ready(function () {
            //"keyup" event handler to reset input fields
            $("#<%=txtPopSearch.ClientID%>").live('keyup', function (e) {
                if (e.keyCode == 13) {
                    $("#popPrdSearch").click();
                    return false;
                }
            });

            var vSearchKey = getParameterByName('srchKey');
            if (vSearchKey != "") {
                srhCustomer();
            }

            if ($("#<%=hdnShowDefaultProduct.ClientID%>").val() == "1") {
                $("#popPrdSearch").trigger("click");
            }

        });
    </script>
    <script language="javascript" type="text/javascript">
        function SetFocus() {
            document.getElementById('<%=txtReceived.ClientID%>').focus();
            //$('#<%=txtReceived.ClientID%>').focus();
        }
        function POSPaymentModeSection(divID) {
            $('.divButton a').removeClass('ovalgreenbutton').addClass('ovalbutton');
            $('#' + divID + ' a').removeClass('ovalbutton').addClass('ovalgreenbutton');
            if (divID == "divExactCash") {
                CurrentPaymentType = "ExactCash";
                funExactCash();
            }
            else if (divID == "divDebit") {
                CurrentPaymentType = "Debit";
                funCustomIntracCash();
            }
            else if (divID == "divCredit") {
                CurrentPaymentType = "Credit";
                funCustomVisaCash();
            }
            else if (divID == "divVisa") {
                CurrentPaymentType = "Visa";
                funCustomVisaCash();
            }
            else if (divID == "divMC") {
                CurrentPaymentType = "MC";
                funCustomMasterCash();
            }
            else if (divID == "divAmEx") {
                CurrentPaymentType = "AmEx";
                funCustomAmexCash();
            }
            else if (divID == "divCash") {
                CurrentPaymentType = "Cash";
                funOpenKeyPad();
            }
            else if (divID == "divGifts") {
                CurrentPaymentType = "Gifts";
                funGift()
            }
            else if (divID == "divOGL") {
                CurrentPaymentType = "OGL";
                funOGL()
            }
            else if (divID == "divGiftCards") {
                CurrentPaymentType = "GiftCards";
                funGiftCards()
            }
            else {
                CurrentPaymentType = "Waste";
                funLost();
            }
        }
        function funDateCompare(source, arguments) {
            var hdDate = document.getElementById('<%=hdnDate.ClientID%>').value;
            var hdYear = document.getElementById('<%=hdnYear.ClientID%>').value;
            var selectDate = document.getElementById('<%=dlMonth.ClientID%>').value;
            var selectYear = document.getElementById('<%=dlYear.ClientID%>').value;

            if (hdYear == selectYear && selectDate < hdDate) {
                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;
            }
        }

        function funClose() {
            document.getElementById('<%=divPOSMenu.ClientID%>').style.display = "none";
        }
        function funPOSMenuOpen() {
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            //document.getElementById('ctl00_cphMaster_divPOSMenu').style.display = "block";
            var browserWidth = 0, browserwidth2 = 0, browserwidth3 = 0;
            browserWidth = (document.documentElement.clientWidth - 1000) / 2;
            document.getElementById('<%=divPOSMenu.ClientID%>').style.position = "absolute";
            //document.getElementById('ctl00_cphMaster_divPOSCategory').style.left = browserWidth + "px";
            var isiPad = navigator.userAgent.match(/iPad/i) != null;
            if (isiPad) {
                browserWidthNew = browserWidth - 18;
                document.getElementById('<%=divPOSMenu.ClientID%>').style.right = browserWidthNew + "px";
            }
            else {
                document.getElementById('<%=divPOSMenu.ClientID%>').style.right = browserWidth + "px";
            }
            document.getElementById('<%=divPOSMenu.ClientID%>').style.top = "188px";
            browserwidth2 = (document.documentElement.clientWidth - 800) / 2;
            var hdn = document.getElementById('<%=hdnPOSPrdMenu.ClientID%>');
            hdn.value = browserwidth2;
            browserwidth3 = (document.documentElement.clientWidth - 500) / 2;
            var hdnkit = document.getElementById('<%=hdnPOSPrdMenu.ClientID%>');
            hdnkit.value = browserwidth3;
            return false;
        }

        function funPOSCategory(menuType) {
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            //document.getElementById('<%=divPOSCategory.ClientID%>').style.display = "block";
            var browserWidth = 0, browserwidth2 = 0, browserwidth3 = 0;
            browserWidth = (document.documentElement.clientWidth - 1100) / 2;
            document.getElementById('<%=divPOSCategory.ClientID%>').style.position = "absolute";
            //document.getElementById('<%=divPOSCategory.ClientID%>').style.left = browserWidth + "px";
            var isiPad = navigator.userAgent.match(/iPad/i) != null;
            if (isiPad) {
                browserWidthNew = browserWidth - 18;
                document.getElementById('<%=divPOSCategory.ClientID%>').style.right = browserWidthNew + "px";
            }
            else {
                document.getElementById('<%=divPOSCategory.ClientID%>').style.right = browserWidth + "px";
            }
            //document.getElementById('<%=divPOSCategory.ClientID%>').style.top =  "188px";
            //            document.getElementById('<%=divPOSCategory.ClientID%>').style.top = $("#dvCollapse").css("top");

            browserwidth2 = (document.documentElement.clientWidth - 800) / 2;
            var hdn = document.getElementById('<%=hdnLeft.ClientID%>');
            hdn.value = browserwidth2;
            browserwidth3 = (document.documentElement.clientWidth - 500) / 2;
            var hdnkit = document.getElementById('<%=hdnLeft.ClientID%>');
            hdnkit.value = browserwidth3;


            if (menuType == "QM") {
                $("#tblQuickMenu").css("display", "block");
                $("#tblManager").css("display", "none");
                $("#ltrMenuHdg").html("<%=Resources.Resource.lblQuickMenu%>");
            }
            else if (menuType == "BM") {
                $("#tblManager").css("display", "block");
                $("#tblQuickMenu").css("display", "none");
                $("#ltrMenuHdg").html("<%=Resources.Resource.lblManager%>");
            }

            return false;
        }


        function Play() {
            playSound('../tada.wav');
        }

        function funPaymentBy(source, arguments) {
            if (document.getElementById('<%=rbExactCash.ClientID%>').checked != 1 && document.getElementById('<%=rbCash.ClientID%>').checked != 1 && document.getElementById('<%=rbDollar.ClientID%>').checked != 1 && document.getElementById('<%=rbLost.ClientID%>').checked != 1 && document.getElementById('<%=rbVisa.ClientID%>').checked != 1 && document.getElementById('<%=rbAmerican.ClientID%>').checked != 1 && document.getElementById('<%=rbMasterCard.ClientID%>').checked != 1 && document.getElementById('<%=rbInterac.ClientID%>').checked != 1)
                arguments.IsValid = false;
            else
                arguments.IsValid = true;
        }

        function funCalQuantity() {
            var Price = parseFloat(document.getElementById('<%=txtQuantity.ClientID%>').value * document.getElementById('<%=txtPrice.ClientID%>').value)
            document.getElementById('<%=txtTotalPrice.ClientID%>').value = Price.toFixed(2);
            return false;
        }

        function funCalBalance() {
            //var myFormattedNum = document.getElementById('<%=txtReceived.ClientID%>').value - document.getElementById('<%=txtTotalAmount.ClientID%>').value
            var myFormattedNum = (vRemaingAmountToBePaid - document.getElementById('<%=txtReceived.ClientID%>').value).toFixed(2); //  - document.getElementById('<%=txtTotalAmount.ClientID%>').value
            document.getElementById('<%=txtRefund.ClientID%>').value = Number(Math.round(myFormattedNum * 100) / 100).toFixed(2)
            //            if (document.getElementById('<%=txtReceived.ClientID%>').value == '') {
            //                document.getElementById('<%=txtRefund.ClientID%>').value = '0.00'
            //            }
            return false;
        }
        function funBlankReceived() {
            document.getElementById('<%=txtRefund.ClientID%>').value = Number(document.getElementById('<%=txtReceived.ClientID%>').value - document.getElementById('<%=txtTotalAmount.ClientID%>').value).toFixed(2);
            return false;
        }

        function funBlankSearchBox() {
            document.getElementById('<%=txtSearch.ClientID%>').innerText = '';
            document.getElementById('<%=txtSearch.ClientID%>').value = '';
            return false;
        }
        function funVisaAmount() {
            document.getElementById('<%=rbVisa.ClientID%>').checked = 1;
            document.getElementById('<%=rbCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbMasterCard.ClientID%>').checked = 0;
            document.getElementById('<%=rbInStoreCredit.ClientID%>').checked = 0;
            document.getElementById('<%=rbAmerican.ClientID%>').checked = 0;
            document.getElementById('<%=rbInterac.ClientID%>').checked = 0;
            document.getElementById('<%=txtRefund.ClientID%>').value = '0.00';
            document.getElementById('<%=txtReceived.ClientID%>').value = (document.getElementById('<%=txtTotalAmount.ClientID%>').value).toFixed(2);
            //document.getElementById('<%=divTransaction.ClientID%>').style.display = "block";
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbLost.ClientID%>').checked = 0;
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            return false;
        }

        function funCloseTransaction() {
            document.getElementById('<%=divTransaction.ClientID%>').style.display = "none";
            document.getElementById('<%=divCardManually.ClientID%>').style.display = "none";
            document.getElementById('<%=rbVisa.ClientID%>').checked = 0;
            document.getElementById('<%=rbCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbMasterCard.ClientID%>').checked = 0;
            document.getElementById('<%=rbAmerican.ClientID%>').checked = 0;
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            document.getElementById('<%=rbInterac.ClientID%>').checked = 0;
            document.getElementById('<%=rbEnterCardInfo.ClientID%>').checked = 0;
            document.getElementById('<%=rbInStoreCredit.ClientID%>').checked = 0;
        }

        function funMasterAmount() {
            document.getElementById('<%=rbVisa.ClientID%>').checked = 0;
            document.getElementById('<%=rbCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbMasterCard.ClientID%>').checked = 1;
            document.getElementById('<%=rbInStoreCredit.ClientID%>').checked = 0;
            document.getElementById('<%=rbAmerican.ClientID%>').checked = 0;
            document.getElementById('<%=rbInterac.ClientID%>').checked = 0;
            document.getElementById('<%=txtRefund.ClientID%>').value = '0.00';
            document.getElementById('<%=txtReceived.ClientID%>').value = (document.getElementById('<%=txtTotalAmount.ClientID%>').value).toFixed(2);
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbLost.ClientID%>').checked = 0;
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            return false;
        }
        //    
        function funAmericanAmount() {
            document.getElementById('<%=rbVisa.ClientID%>').checked = 0;
            document.getElementById('<%=rbCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbMasterCard.ClientID%>').checked = 0;
            document.getElementById('<%=rbAmerican.ClientID%>').checked = 1;
            document.getElementById('<%=rbInStoreCredit.ClientID%>').checked = 0;
            document.getElementById('<%=rbInterac.ClientID%>').checked = 0;
            document.getElementById('<%=txtRefund.ClientID%>').value = '0.00';
            document.getElementById('<%=txtReceived.ClientID%>').value = (document.getElementById('<%=txtTotalAmount.ClientID%>').value).toFixed(2);
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbLost.ClientID%>').checked = 0;
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            return false;
        }

        function funExactCash() {
            document.getElementById('<%=rbVisa.ClientID%>').checked = 0;
            document.getElementById('<%=rbCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbMasterCard.ClientID%>').checked = 0;
            document.getElementById('<%=rbAmerican.ClientID%>').checked = 0;
            document.getElementById('<%=rbExactCash.ClientID%>').checked = 1;
            document.getElementById('<%=rbInStoreCredit.ClientID%>').checked = 0;
            document.getElementById('<%=rbInterac.ClientID%>').checked = 0;
            document.getElementById('<%=rbOGL.ClientID%>').checked = 0;
            document.getElementById('<%=txtRefund.ClientID%>').value = '0.00';
            //document.getElementById('<%=txtReceived.ClientID%>').value = document.getElementById('<%=txtTotalAmount.ClientID%>').value;
            document.getElementById('<%=txtReceived.ClientID%>').value = Number(vRemaingAmountToBePaid).toFixed(2);
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbLost.ClientID%>').checked = 0;
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            document.getElementById('<%=txtReceived.ClientID%>').focus();
            return false;
        }

        function funInstoreCredit() {
            document.getElementById('<%=rbVisa.ClientID%>').checked = 0;
            document.getElementById('<%=rbCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbMasterCard.ClientID%>').checked = 0;
            document.getElementById('<%=rbAmerican.ClientID%>').checked = 0;
            document.getElementById('<%=rbExactCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbInStoreCredit.ClientID%>').checked = 1;
            document.getElementById('<%=rbInterac.ClientID%>').checked = 0;
            document.getElementById('<%=txtRefund.ClientID%>').value = '0.00';
            //document.getElementById('<%=txtReceived.ClientID%>').value = document.getElementById('<%=txtTotalAmount.ClientID%>').value;
            //document.getElementById('<%=txtReceived.ClientID%>').value = vRemaingAmountToBePaid;
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbLost.ClientID%>').checked = 0;
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            return false;
        }


        //Added by Hitendra On 2011-12-27
        function funCustomVisaCash() {
            document.getElementById('<%=rbVisa.ClientID%>').checked = 1;
            document.getElementById('<%=rbCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbMasterCard.ClientID%>').checked = 0;
            document.getElementById('<%=rbAmerican.ClientID%>').checked = 0;
            document.getElementById('<%=rbExactCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbInterac.ClientID%>').checked = 0;
            document.getElementById('<%=rbOGL.ClientID%>').checked = 0;
            document.getElementById('<%=txtRefund.ClientID%>').value = '0.00';
            document.getElementById('<%=rbInStoreCredit.ClientID%>').checked = 0;
            //document.getElementById('<%=txtReceived.ClientID%>').value = document.getElementById('<%=txtTotalAmount.ClientID%>').value;
            document.getElementById('<%=txtReceived.ClientID%>').value = Number(vRemaingAmountToBePaid).toFixed(2);
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbLost.ClientID%>').checked = 0;
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            document.getElementById('<%=txtReceived.ClientID%>').focus();
            return false;
        }

        //Added by Hitendra On 2011-12-27
        function funCustomMasterCash() {
            document.getElementById('<%=rbVisa.ClientID%>').checked = 0;
            document.getElementById('<%=rbCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbMasterCard.ClientID%>').checked = 1;
            document.getElementById('<%=rbAmerican.ClientID%>').checked = 0;
            document.getElementById('<%=rbExactCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbInStoreCredit.ClientID%>').checked = 0;
            document.getElementById('<%=rbInterac.ClientID%>').checked = 0;
            document.getElementById('<%=rbOGL.ClientID%>').checked = 0;
            document.getElementById('<%=txtRefund.ClientID%>').value = '0.00';
            //document.getElementById('<%=txtReceived.ClientID%>').value = document.getElementById('<%=txtTotalAmount.ClientID%>').value;
            document.getElementById('<%=txtReceived.ClientID%>').value = Number(vRemaingAmountToBePaid).toFixed(2);
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbLost.ClientID%>').checked = 0;
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            document.getElementById('<%=txtReceived.ClientID%>').focus();
            return false;
        }

        //Added by Hitendra On 2011-12-27
        function funCustomAmexCash() {
            document.getElementById('<%=rbVisa.ClientID%>').checked = 0;
            document.getElementById('<%=rbCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbMasterCard.ClientID%>').checked = 0;
            document.getElementById('<%=rbAmerican.ClientID%>').checked = 1;
            document.getElementById('<%=rbExactCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbInStoreCredit.ClientID%>').checked = 0;
            document.getElementById('<%=rbInterac.ClientID%>').checked = 0;
            document.getElementById('<%=rbOGL.ClientID%>').checked = 0;
            document.getElementById('<%=txtRefund.ClientID%>').value = '0.00';
            //document.getElementById('<%=txtReceived.ClientID%>').value = document.getElementById('<%=txtTotalAmount.ClientID%>').value;
            document.getElementById('<%=txtReceived.ClientID%>').value = Number(vRemaingAmountToBePaid).toFixed(2);
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbLost.ClientID%>').checked = 0;
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            document.getElementById('<%=txtReceived.ClientID%>').focus();
            return false;
        }

        //Added by Hitendra On 2011-12-27
        function funCustomIntracCash() {
            document.getElementById('<%=rbVisa.ClientID%>').checked = 0;
            document.getElementById('<%=rbCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbMasterCard.ClientID%>').checked = 0;
            document.getElementById('<%=rbAmerican.ClientID%>').checked = 0;
            document.getElementById('<%=rbExactCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbOGL.ClientID%>').checked = 0;
            document.getElementById('<%=rbInStoreCredit.ClientID%>').checked = 0;
            document.getElementById('<%=rbInterac.ClientID%>').checked = 1;
            document.getElementById('<%=txtRefund.ClientID%>').value = '0.00';
            //document.getElementById('<%=txtReceived.ClientID%>').value = document.getElementById('<%=txtTotalAmount.ClientID%>').value;
            document.getElementById('<%=txtReceived.ClientID%>').value = Number(vRemaingAmountToBePaid).toFixed(2);
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbLost.ClientID%>').checked = 0;
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            document.getElementById('<%=txtReceived.ClientID%>').focus();
            return false;
        }

        function funInteracAmount() {
            document.getElementById('<%=rbVisa.ClientID%>').checked = 0;
            document.getElementById('<%=rbCash.ClientID%>').checked = 0;
            document.getElementById('<%=rbMasterCard.ClientID%>').checked = 0;
            document.getElementById('<%=rbAmerican.ClientID%>').checked = 0;
            document.getElementById('<%=rbInStoreCredit.ClientID%>').checked = 0;
            document.getElementById('<%=rbInterac.ClientID%>').checked = 1;
            document.getElementById('<%=txtRefund.ClientID%>').value = '0.00';
            document.getElementById('<%=txtReceived.ClientID%>').value = (document.getElementById('<%=txtTotalAmount.ClientID%>').value).toFixed(2);
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbLost.ClientID%>').checked = 0;
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            return false;
        }

        function funMenuPOSMenuPrd() {
            alert("<%= lblNoPOSMenuPrd %>");
            return false;
        }

        function funNumericCreditCard(source, arguments) {
            if (isNaN(document.getElementById('<%=txtCardNo.ClientID%>').value))
                arguments.IsValid = false;
            else if (document.getElementById('<%=txtCardNo.ClientID%>').value.length < 12)
                arguments.IsValid = false;
            else
                arguments.IsValid = true;
        }


        function funInsertAmount(arguments) {
            if (arguments == "100")
            { document.getElementById('<%=txtReceived.ClientID%>').value = '100.00'; }
            else if (arguments == "50")
            { document.getElementById('<%=txtReceived.ClientID%>').value = '50.00'; }
            else if (arguments == "20")
            { document.getElementById('<%=txtReceived.ClientID%>').value = '20.00'; }
            else if (arguments == "10")
            { document.getElementById('<%=txtReceived.ClientID%>').value = '10.00'; }
            else if (arguments == "5")
            { document.getElementById('<%=txtReceived.ClientID%>').value = '5.00'; }
            else if (arguments == "2")
            { document.getElementById('<%=txtReceived.ClientID%>').value = '2.00'; }
            else if (arguments == "1")
            { document.getElementById('<%=txtReceived.ClientID%>').value = '1.00'; }
            document.getElementById('<%=rbcash.ClientID%>').checked = 1;
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbLost.ClientID%>').checked = 0;
            var deductedPrice = document.getElementById('<%=txtReceived.ClientID%>').value - document.getElementById('<%=txtTotalAmount.ClientID%>').value;
            document.getElementById('<%=txtRefund.ClientID%>').value = deductedPrice.toFixed(2); //document.getElementById('<%=txtReceived.ClientID%>').value - document.getElementById('<%=txtTotalAmount.ClientID%>').value;            
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            return false;
        }

        function funDollar() {
            document.getElementById('<%=divDollar.ClientID%>').style.display = "block";
            document.getElementById('<%=rbcash.ClientID%>').checked = 0;
            document.getElementById('<%=rbDollar.ClientID%>').checked = 1;
            document.getElementById('<%=rbDollar.ClientID%>').checked = true;
            document.getElementById('<%=rbLost.ClientID%>').checked = 0;
            document.getElementById('<%=txtReceived.ClientID%>').value = "0.00";
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            return false;
        }

        function funCloseDollar() {
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
        }

        function funLost() {
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=rbcash.ClientID%>').checked = 0;
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbLost.ClientID%>').checked = 1;
            document.getElementById('<%=rbOGL.ClientID%>').checked = 0;
            document.getElementById('<%=txtRefund.ClientID%>').value = "0.00";
            //document.getElementById('<%=txtReceived.ClientID%>').value = document.getElementById('<%=txtTotalAmount.ClientID%>').value;
            document.getElementById('<%=txtReceived.ClientID%>').value = Number(vRemaingAmountToBePaid).toFixed(2);
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            document.getElementById('<%=txtReceived.ClientID%>').focus();
            return false;
        }

        function funGift() {
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=rbcash.ClientID%>').checked = 0;
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbGifts.ClientID%>').checked = 1;
            document.getElementById('<%=rbOGL.ClientID%>').checked = 0;
            document.getElementById('<%=txtRefund.ClientID%>').value = "0.00";
            //document.getElementById('<%=txtReceived.ClientID%>').value = document.getElementById('<%=txtTotalAmount.ClientID%>').value;
            document.getElementById('<%=txtReceived.ClientID%>').value = Number(vRemaingAmountToBePaid).toFixed(2);
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            document.getElementById('<%=txtReceived.ClientID%>').focus();
            return false;
        }

        function funOGL() {
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=rbcash.ClientID%>').checked = 0;
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbGifts.ClientID%>').checked = 0;
            document.getElementById('<%=rbOGL.ClientID%>').checked = 1;
            document.getElementById('<%=txtRefund.ClientID%>').value = "0.00";
            //document.getElementById('<%=txtReceived.ClientID%>').value = document.getElementById('<%=txtTotalAmount.ClientID%>').value;
            document.getElementById('<%=txtReceived.ClientID%>').value = Number(vRemaingAmountToBePaid).toFixed(2);
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            document.getElementById('<%=txtReceived.ClientID%>').focus();
            return false;
        }

        function funGiftCards() {
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=rbcash.ClientID%>').checked = 0;
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbGifts.ClientID%>').checked = 0;
            document.getElementById('<%=rbOGL.ClientID%>').checked = 0;
            document.getElementById('<%=rbGiftCards.ClientID%>').checked = 1;
            document.getElementById('<%=txtRefund.ClientID%>').value = "0.00";
            //document.getElementById('<%=txtReceived.ClientID%>').value = document.getElementById('<%=txtTotalAmount.ClientID%>').value;
            document.getElementById('<%=txtReceived.ClientID%>').value = vRemaingAmountToBePaid;
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            document.getElementById('<%=txtReceived.ClientID%>').focus();
            return false;
        }

        function funStaffGift() {
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=rbcash.ClientID%>').checked = 0;
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbStaffGifts.ClientID%>').checked = 1;
            document.getElementById('<%=txtRefund.ClientID%>').value = "0.00";
            document.getElementById('<%=txtReceived.ClientID%>').value = (document.getElementById('<%=txtTotalAmount.ClientID%>').value).toFixed(2);
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            return false;
        }

        function funPOSCtgMenuClose() {
            document.getElementById('<%=divPOSCategory.ClientID%>').style.display = "none";
        }
        function funPOSCatgProductMenuClose() {
            document.getElementById('<%=divPOSCatgProductMenu.ClientID%>').style.display = "none";
            document.getElementById('<%=divProductKitContent.ClientID%>').style.display = "none";
        }

        function funPOSProdKitClose() {
            document.getElementById('<%=divProductKitContent.ClientID%>').style.display = "none";
            //document.getElementById('<%=divPOSCatgProductMenu.ClientID%>').style.display = "none";
        }

        function funMenuPOSMenuPrdKit() {
            alert("<%= lblNoPOSMenuKitPrd %>");
            return false;
        }

        function funBackToCat() {
            document.getElementById('<%=divPOSCatgProductMenu.ClientID%>').style.display = "none";
            document.getElementById('<%=divPOSCategory.ClientID%>').style.display = "block";
            var browserWidth = 0, browserwidth2 = 0, browserwidth3 = 0;
            browserWidth = (document.documentElement.clientWidth - 1000) / 2;
            document.getElementById('<%=divPOSCategory.ClientID%>').style.position = "absolute";
            var isiPad = navigator.userAgent.match(/iPad/i) != null;
            if (isiPad) {
                browserWidthNew = browserWidth - 18;
                document.getElementById('<%=divPOSCategory.ClientID%>').style.right = browserWidthNew + "px";
            }
            else {
                document.getElementById('<%=divPOSCategory.ClientID%>').style.right = browserWidth + "px";
            }
            browserwidth2 = (document.documentElement.clientWidth - 800) / 2;
            var hdn = document.getElementById('<%=hdnkitLeft.ClientID%>');
            hdn.value = browserwidth2;
            browserwidth3 = (document.documentElement.clientWidth - 500) / 2;
            var hdnkit = document.getElementById('<%=hdnkitLeft.ClientID%>');
            hdnkit.value = browserwidth3;
            return false;
        }
    </script>
    <script language="javascript" type="text/javascript">
        function funOpenKeyPad() {
            var txtReceived = $('#<%=txtReceived.ClientID%>');
            if (txtReceived.val() == "0.00") {
                txtReceived.val("")
                txtReceived.focus();
                //                document.getElementById('<%=txtReceived.ClientID%>').innerText = '';
                //                document.getElementById('<%=txtReceived.ClientID%>').value = '';
            }
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
            document.getElementById('<%=divDollar.ClientID%>').style.display = "none";
            document.getElementById('<%=rbcash.ClientID%>').checked = 1;
            document.getElementById('<%=rbDollar.ClientID%>').checked = 0;
            document.getElementById('<%=rbLost.ClientID%>').checked = 0;
            document.getElementById('<%=rbOGL.ClientID%>').checked = 0;

            //$('#<%=txtReceived.ClientID%>').keypad({ showOn: 'load' });
            return false;
        }

        function updateTransaction(sDATA, isRefund) {
            var query = "data=" + sDATA;
            var sErrMsg = "Error! slide-popup " + sURL + " Qry:" + query;
            var isAsync = false;
            var sURL = "./data.aspx"
            if (isRefund) {
                calliAJAX(sURL, query, sErrMsg, isAsync);
            }
            //window.open('./Print.aspx?card=yes&TID='+sDATA.split("~")[9]+'&WhsID='+secWareHouse+'&LangsID='+secLang,'Printing Receipt', 'width=300,height=300,toolbar=0,menubar=0,location=0,scrollbars=0,resizable=0,directories=0,status=0' );
            var URl = '<%=ConfigurationManager.AppSettings("PrintURL")%>' + '&WhsID=' + secWareHouse + '&LangsID=' + secLang + "&TID="//document.getElementById('<%=ConfigurationManager.AppSettings("PrintURL")%>').value;       

            return URl;
        }

        function getPrintUrl() {
            return '<%=ConfigurationManager.AppSettings("PrintURL")%>' + '&WhsID=' + secWareHouse + '&LangsID=' + secLang + "&TID="//document.getElementById('<%=ConfigurationManager.AppSettings("PrintURL")%>').value;
        }

        function transactionStatus(msg, status) {
            closeAllow = true;
            if (status == "1[49]") {
                location.href = "POS.aspx?Msg=" + msg;
            } else {
                $("#ctl00_cphMaster_lblTranMsg").css("display", "inline")
                alert(msg);
                $("#ctl00_cphMaster_lblTranMsg").html(msg);
            }
        }

        function __doPostBack1(control) {
            var controlName = $(control).attr('name')
            var result = controlName.split("~")
            if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                document.getElementById('<%=hdnProductID.ClientID%>').value = result[0];
                document.getElementById('<%=hdnProductKit.ClientID%>').value = result[1];
                theForm.submit();
            }
        }

        function __doPostBackCat(control) {
            return false;
        }

        var timerScreenLock = setTimeout(blockContentArea, 1140000); //1140000
                function blockContentArea() {
                    SavePageData();
                    $("body").block({
                        message: "<div><h4> Screen Locked!<h4><h4>To Unlock <a href='javascript:;' onclick='location.href=location.href;'>click here</a>.</h4></div>",
                        css: { border: '3px solid #a00' }
                    });
                    clearTimeout(timerScreenLock);
                }
    </script>
    <script type="text/javascript">
        function ChkUserPosManagerRole() {
            var hdnIsUserPosManagerRole = $('#<%=hdnIsUserPosManagerRole.ClientID%>');
            var hdnManagerCode = $('#<%=hdnManagerCode.ClientID%>');
            if (hdnIsUserPosManagerRole.val() == "1") {
                //            var hdnManagerCode = $('#<%=hdnManagerCode.ClientID%>');
                //            if (hdnManagerCode.val() == "") {
                funPOSCategory('BM');
                ShowSideMenu();
            }
            else {
                if (hdnManagerCode.val() == "") {
                    funPOSCategory('BM');
                    ShowSideMenu();
                }
                else {
                    var postData = {};
                    var $dialog = jQuery.FrameDialog.create({ url: 'ManageCodeValidation.aspx?' + $.param(postData),
                        title: "<%=Resources.Resource.lblValidateManagerCode %>",
                        modal: true,
                        width: 350,
                        height: 260,
                        autoOpen: false
                    });
                    $dialog.dialog('open');
                }
            }
        }


        function AddInvoiceDiscount() {
            if ($('#' + 'divDiscount' + ' a').hasClass("blueovalgreenbutton")) {
                $('#' + 'divDiscount' + ' a').addClass('blueovalbutton').removeClass('blueovalgreenbutton');
            }
            else {
                $('#' + 'divDiscount' + ' a').removeClass('blueovalbutton').addClass('blueovalgreenbutton');
            }

            var postData = {};
            var $dialog = jQuery.FrameDialog.create({ url: 'InvoiceDiscount.aspx?' + $.param(postData),
                title: "<%=Resources.Resource.PODiscount %>",
                modal: true,
                width: 350,
                height: 260,
                autoOpen: false
            });
            $dialog.dialog('open');
        }


        function ShowSideMenu() {
            if ($('#ctl00_cphMaster_divPOSCategory').css('display') == "none") {
                $('#ctl00_cphMaster_divPOSCategory').show("slide", { direction: "right" }, 1000);
                if (isiPad) {
                    $('.pullout-btn').animate({ 'right': '380px' }, 1000);
                }
                else {
                    $('.pullout-btn').animate({ 'right': '372px' }, 1000);
                }
                $("#clicked").addClass('click-hide');
            }
        }

        function ValidateManagerCode() {

            funPOSCategory('BM');
            ShowSideMenu();
        }

        function ApplyInvoiceDisc(DiscPer) {
            ApplyInvoiceDiscount(DiscPer);
        }

        function EnablePartialPayment() {
            $('.divButton a').removeClass('ovalgreenbutton').addClass('ovalbutton');
            TotalAmountBeforePartial = Number($("#ctl00_cphMaster_txtTotalAmount").attr("value"));
            isPartialPayment = 1;
            AddPaymentType(CurrentPaymentType)
            //CurrentPaymentType = "";
        }


        $("#<%=ddlTax.ClientID%>").change(function () {
            var sText = $("#<%=ddlTax.ClientID %> :selected").text();
            var sTaxID = $("#<%=ddlTax.ClientID %> :selected").val();


            $.ajax({
                type: "POST",
                url: "POSServices.asmx/GetTaxValue",
                data: "{taxCode:'" + sTaxID + "', taxDesc:'" + sText + "', lngID:'" + secLang + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    JsonObject = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (JsonObject.ResponseCode == 1 && JsonObject.Status == "OK") {
                        $("#<%=hdnTaxValue.ClientID %>").val(JsonObject.taxValue);
                        var ddlTaxText = $("#ctl00_cphMaster_ddlTax :selected").text();
                        var ddlTaxID = $("#ctl00_cphMaster_ddlTax :selected").val();
                        var vPrdActPrice = $('#ctl00_cphMaster_txtPrice').attr('value');
                        var vPrdDisc = $('#ctl00_cphMaster_txtDisc').attr('value');
                        var vTaxID = ddlTaxID;
                        var vTaxName = ddlTaxText;
                        var vTaxValue = $('#ctl00_cphMaster_hdnTaxValue').attr('value');
                        var quntity = $('#ctl00_cphMaster_txtQuantity').attr('value')
                        var totalPrice = $('#ctl00_cphMaster_txtTotalPrice').attr('value')

                        //                        var vPrdAmt = Number((Number(quntity)) * Number(vPrdActPrice));
                        //                        var dblPrdDisc = Number(vPrdAmt * vPrdDisc / 100);
                        //                        var dblPrdAftDisc = Number(vPrdAmt - dblPrdDisc);
                        //                        var dblTaxAmount = Number(dblPrdAftDisc * vTaxValue / 100)
                        //                        var dblPrdNetCost = Math.round(Number(dblPrdAftDisc + dblTaxAmount) * 100) / 100;
                        //                        $('#ctl00_cphMaster_txtTotalPrice').attr('value', dblPrdNetCost);

                        //                        $('#quantity_' + prId).html(Math.round((Number(pQuntity) + Number(quntity)) * 100) / 100);
                        //                        $('#total_price_' + prId).html(Number(dblPrdNetCost).toFixed(2));
                        //                        $('#p_disc_' + prId).html(vPrdDisc);
                        //                        $('#p_Tax_' + prId).html(ddlTaxText);
                        //                        $('#hdn_PTaxID_' + prId).val(ddlTaxID);

                    }
                    else if (JsonObject.ResponseCode == -1 && JsonObject.Status == "OK") {
                    }
                    else {
                    }
                },
                error: function () {
                }
            });

        });


        function redirectCustomerLeagcy(custID, srchKey) {
            SavePageData();
            window.location.href = '../NewReport/report.aspx?rpt=LegacyCustomerHistory.xml&InvokeSrc=POS&custid=' + custID + '&srchKey=' + srchKey;
        }

        function redirectCustomerDetial(custID, srchKey) {
            SavePageData();
            window.location.href = '../NewReport/report.aspx?rpt=SalesByCustomer.xml&InvokeSrc=POS&custid=' + custID + '&srchKey=' + srchKey;
        }

        function redirectCustomerReceipt(custID, srchKey) {
            SavePageData();
            window.location.href = '../Invoice/ViewInvoice.aspx?custid=' + custID + '&Default=1&dflt=Y&t=8&InvokeSrc=POS&srchKey=' + srchKey;
        }

        function addNewCustomer(custID, srchKey) {
            //SavePageData();
            window.location.href = '../Partner/CustomerEdit.aspx?pType=D&gType=0&create_POS=1&InvokeSrc=POS';
        }

        function redirectCustomerEdit(custID, srchKey, custcustType) {
            var gType = "";
            if (custcustType == "E") {
                gType = "1";
            }
            else {
                gType = "0";
            }
            SavePageData();
            window.location.href = '../Partner/CustomerEdit.aspx?custid=' + custID + '&pType=' + custcustType + '&gType=' + gType + '&InvokeSrc=POS&srchKey=' + srchKey;
        }


        function InStoreCredit() {
            if ($("#ctl00_cphMaster_hdnIsHideInstoreCredit").val() == "0")
                return;
            $('.divButton a').removeClass('ovalgreenbutton').addClass('ovalbutton');
            $('#' + 'divInstoreCredit' + ' a').removeClass('ovalbutton').addClass('ovalgreenbutton');
            // Get Added Instore Amount
            var vInstoreAmountAdded = paymentTypeValue('InStoreCredit');
            var postData = {};
            postData.custID = custID;
            postData.amountAdded = vInstoreAmountAdded;
            var $dialog = jQuery.FrameDialog.create({ url: 'InstoreCredit.aspx?' + $.param(postData),
                title: "<%=Resources.Resource.lblInstoreCredit %>",
                modal: true,
                width: 350,
                height: 260,
                autoOpen: false
            });
            $dialog.dialog('open');
        }


        function ApplyInStoreCredit(DiscPer) {
            $("#ctl00_cphMaster_txtReceived").attr("value", Number(DiscPer).toFixed(2));
            CurrentPaymentType = "InStoreCredit";
            if (isPartialPayment == "1") {
                AddPaymentType(CurrentPaymentType)
            }
            else {
                funInstoreCredit();
                $("#ctl00_cphMaster_txtReceived").attr("value", Number(DiscPer).toFixed(2));
                var myFormattedNum = Number(vRemaingAmountToBePaid) - Number(DiscPer);
                //                vRemaingAmountToBePaid = myFormattedNum;
                $("#ctl00_cphMaster_txtRefund").val(Number(Math.round(myFormattedNum * 100) / 100).toFixed(2));
            }
        }


        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
            return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        function ReceiptMail() {

            if ($("#ctl00_cphMaster_hdnIsRegClosed").val() == "1") {
                alert(getLocalizedString(secLang, "lblRegisterIsClosed"));
                return;
            }

            var postData = {};
            postData.custID = custID;
            postData.custEmail = $("#hdnCustMail").val();
            var $dialog = jQuery.FrameDialog.create({ url: 'ReceiptMail.aspx?' + $.param(postData),
                title: "<%=Resources.Resource.lblMailReceipt %>",
                modal: true,
                width: 400,
                height: 200,
                autoOpen: false
            });
            $dialog.dialog('open');
        }


        function SendReceiptMail(mailID) {
            vMailToSend = mailID;
            cashTransation(false, '')
        }

        function redirectAdvanceProductSearch(custID, srchKey) {
            SavePageData();
            window.location.href = '../Inventory/ProductQuery.aspx?InvokeSrc=POS&ShowLPannel=true';
        }


        function AddToLayaway() {
            if ($('#' + 'divLayaway' + ' a').hasClass("blueovalgreenbutton")) {
                $('#' + 'divLayaway' + ' a').addClass('blueovalbutton').removeClass('blueovalgreenbutton');
            }
            else {
                $('#' + 'divLayaway' + ' a').removeClass('blueovalbutton').addClass('blueovalgreenbutton');
            }


            if (IsProductItmeSelected()) {
                if (confirm("<%=Resources.Resource.lblSaveLaywayOrder%>")) {
                    isLayawayOrder = 1;
                    cashTransation(true, '');
                }
                else {
                    $('#' + 'divLayaway' + ' a').addClass('blueovalbutton').removeClass('blueovalgreenbutton');
                }
            }
            else {
                SavePageData();
                window.location.href = '../Sales/ViewSalesOrder.aspx?Default=1&t=5&ordType=5&InvokeSrc=POS&ShowLPannel=true&whscode=' + secWareHouse;
            }
        }


        jQuery(document).ready(function () {
            $("#ctl00_cphMaster_cmdAdd").hide();
            var vlaywayOrderID = getParameterByName('laywayOrderID');
            if (Number(vlaywayOrderID) > 0) {
                LoadLaywayOrderDetial(vlaywayOrderID);
            }
        });


        function GetGiftCardNo(productID) {
            var postData = {};
            postData.productID = productID;
            var $dialog = jQuery.FrameDialog.create({ url: 'GiftCardNo.aspx?' + $.param(postData),
                title: "<%=Resources.Resource.lblGiftCard %>",
                modal: true,
                width: 400,
                height: 200,
                autoOpen: false
            });
            $dialog.dialog('open');
        }


        function SelectedGiftCard(prdID, GiftCardNo) {
            AddGiftCardItem(prdID, GiftCardNo);
        }


        function GiftCardsUse() {
            $('.divButton a').removeClass('ovalgreenbutton').addClass('ovalbutton');
            $('#' + 'divGiftCards' + ' a').removeClass('ovalbutton').addClass('ovalgreenbutton');

            var postData = {};
            postData.custID = custID;
            var $dialog = jQuery.FrameDialog.create({ url: 'GiftCardVarifaction.aspx?' + $.param(postData),
                title: "<%=Resources.Resource.lblGiftCard %>",
                modal: true,
                width: 350,
                height: 260,
                autoOpen: false
            });
            $dialog.dialog('open');
        }


        function ApplyGiftCard(DiscPer, GiftCardNo) {
            vGiftCardNo = GiftCardNo;
            $("#ctl00_cphMaster_txtReceived").attr("value", Number(DiscPer).toFixed(2));
            CurrentPaymentType = "GiftCards";
            if (isPartialPayment == "1") {
                AddPaymentType(CurrentPaymentType)
            }
            else {
                funGiftCards();
                $("#ctl00_cphMaster_txtReceived").attr("value", Number(DiscPer).toFixed(2));
                var myFormattedNum = Number(vRemaingAmountToBePaid) - Number(DiscPer);
                $("#ctl00_cphMaster_txtRefund").val(Number(Math.round(myFormattedNum * 100) / 100).toFixed(2));
            }
        }



        $(document).ready(function () {
            $("input:text").focus(function () { $(this).select(); });
        });

        $("#ctl00_cphMaster_txtPrice").keyup(function (event) {
            if (event.keyCode == 13) {
                if (isItemEdit == "1") {
                    $("#ctl00_cphMaster_cmdAdd").click();
                }
            }
        });


        $("#ctl00_cphMaster_txtDisc").keyup(function (event) {
            if (event.keyCode == 13) {
                if (isItemEdit == "1") {
                    $("#ctl00_cphMaster_cmdAdd").click();
                }
            }
        });

        $("#ctl00_cphMaster_txtQuantity").keyup(function (event) {
            if (event.keyCode == 13) {
                if (isItemEdit == "1") {
                    $("#ctl00_cphMaster_cmdAdd").click();
                }
            }
        });

        $("#ctl00_cphMaster_ddlTax").keyup(function (event) {
            if (event.keyCode == 13) {
                if (isItemEdit == "1") {
                    $("#ctl00_cphMaster_cmdAdd").click();
                }
            }
        });

    </script>
    <script type="text/javascript">
        function ShowRefundVia(isPrint, btnType) {
            visPrint = isPrint;
            vbtnType = btnType;
            var postData = {};
            var $dialog = jQuery.FrameDialog.create({ url: 'PaymentViaPopup.aspx?' + $.param(postData),
                title: "<%=Resources.Resource.lblAmountRefundedVia %>",
                modal: true,
                width: 500,
                height: 260,
                autoOpen: false
            });
            $dialog.dialog('open');
        }

        function CloseDialogPopup(paymentViaID) {
            vAmountRefundedVia = paymentViaID;
            cashTransation(visPrint, vbtnType);
        }


        function AllowMaxValue(input, limitVal) {
            if (input.value < 0) input.value = 0;
            if (input.value > limitVal) input.value = 100;
        }

        $(".numericTextField").numeric();
        $(".integerTextField").numeric(false, function () { alert("Integers only"); this.value = ""; this.focus(); });

        //        //For binding
        //        window.onbeforeunload = SavePageData;
        function SavePageData() {
            var bUnSavedData = "false";
            var vhasRow = "false";
            var NewCustSelectedExceptDefault = "false";
            var hdnDefaultCustID = $('#<%=hdnDefaultCustID.ClientID%>');
            if (parseInt(custID) != parseInt(hdnDefaultCustID.val())) {
                NewCustSelectedExceptDefault = "true";
            }

            var trIds = document.getElementById("ctl00_cphMaster_grdRequest").getElementsByTagName("tr");
            if (trIds.length > 1) {
                vhasRow = "true";
            }

            if ((vhasRow == "true")) {
                bUnSavedData = "true";
            }

            if (NewCustSelectedExceptDefault == "true") {
                bUnSavedData = "true";
            }

            if (bUnSavedData == "true") {
//                if (confirm("<%=Resources.Resource.lblConfirmSaveData %>")) {
                    SaveSession(custID);
//                }
            }
        }
        $(document).ready(function () {
            var vReturnPartnerID = (getParameterByName('partID'));
            if (vReturnPartnerID == '') {
                GetSession();
            } else {
                window.history.replaceState("", "", "POS.aspx");
            }
        });
    </script>
</asp:Content>

Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Data.Odbc
Imports System.Data
Imports clsCommon
Partial Class POS_PinpadPrint
    Inherits System.Web.UI.Page
    Dim objCommon As New clsCommon
    Dim objCompany As New clsCompanyInfo

    Protected Sub POS_PinpadPrint_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            If Request.QueryString("WithCard") = "yes" Then
                subClose()
                Exit Sub
            End If
        End If
        Dim strLanguage As String
        If Request.QueryString("WithCard") = "yes" Then
            strLanguage = funGetProductLang()
        Else
            strLanguage = Request.QueryString("LangsID").Substring(0, 2)
            Session("UserWarehouse") = Request.QueryString("WhsID")
        End If

        lbldate.Text = POSDate & Format(Date.Parse(Now()), "MM/dd/yyyy") & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & POSTime & Format(Date.Parse(Now()), "HH:mm:ss")
        lblTransaction.Text = "(" & Request.QueryString("TID") & ")"
        lblProIL.Text = msgTransactionID & " " & Request.QueryString("TID")
        objCompany.GetCompanyAddress()
        lblCompanyName.Text = objCompany.CompanyName
        lblCompanyPhone.Text = objCompany.CompanyPOPhone
        compAddr.Value = objCompany.CompanyAddressLine1 & "<BR/>" & objCompany.CompanyCity & " " & objCompany.CompanyState & "<BR/>" & objCompany.CompanyCountry & " " & objCompany.CompanyPostalCode
        lblCustCompanyName.Text = objCompany.CompanyName
        lblCustCompanyName.ForeColor = Drawing.Color.Black
        lblCustCompanyPhone.Text = objCompany.CompanyPOPhone
        lblCustCompanyPhone.ForeColor = Drawing.Color.Black
        lblCustDate.Text = lbldate.Text
        lblCustDate.ForeColor = Drawing.Color.Black
        lblCustTransactionID.Text = lblTransaction.Text
        lblCustTransactionID.ForeColor = Drawing.Color.Black
        lblCustTran.Text = lblProIL.Text
        lblCustTran.ForeColor = Drawing.Color.Black
        lblMerCompanyName.Text = lblCompanyName.Text
        lblDebCompanyName.Text = lblCompanyName.Text
        lblInitCompanyName.Text = lblCompanyName.Text
        lblMerCompanyPhone.Text = lblCompanyPhone.Text
        lblDebCompanyPhone.Text = lblCompanyPhone.Text
        lblInitCompanyPhone.Text = lblCompanyPhone.Text
        lblMerDate.Text = lbldate.Text
        'lblInitDate.Text = lbldate.Text
        lblMerTransactionID.Text = lblTransaction.Text
        lblDebTransactionID.Text = lblTransaction.Text
        lblMerTran.Text = lblProIL.Text
        lblMsgc.Text = Request.QueryString("posmsg")
        lblMsgc.ForeColor = Drawing.Color.Black
        lblMsgM.Text = Request.QueryString("posmsg")
        'lblMsgD.Text = Request.QueryString("posmsg").Replace(" ", "&nbsp;").Replace("~", "<br />")

        If Request.QueryString("cardtype") = "R" Then
            trReprintP.Visible = True
            trReprint.Visible = True
        End If
        subGetPosProduct(strLanguage)
    End Sub
    Public Sub subGetPosProduct(ByVal strLang As String)
        Dim strCarName, strCardNo, strSubTotal, strTax1, strTax2, strTax3, strTax4, strTotal, strReceived, strReturn, strTaxDesc1, strTaxDesc2, strTaxDesc3, strTaxDesc4 As String
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        Dim objDataClass As New clsDataClass
        strSQL = " SELECT distinct posPrdTransID,sysRegMessage,prdUPCCode,posSubTotal,posCasReceived,posCashReturned,pt.posTotalValue,pt.posTax1,pt.posTax2,pt.posTax3,pt.posTax4,posProductID,Pdes.prdname as ProductName, posPrdQty, posPrdUnitPrice,posPrdPrice, posPrdTaxCode,prdUPCCode,pt.posTax1Desc,pt.posTax2Desc,pt.posTax3Desc,pt.posTax4Desc,posCardNo,posCardName,posCardLang, posCardAccountType, posCardMsg FROM posproductno "
        strSQL += " Inner join postransaccthdr on postransaccthdr.posAcctTransID=posproductno.posPrdTransID "
        strSQL += " left join postransacctdtl on postransacctdtl.posAcctHdrDtlId=postransaccthdr.posAcctHdrID "
        strSQL += " Inner join posTransaction as pt on pt.posTransId=posproductno.posPrdTransID"
        strSQL += " Inner join Products on Products.ProductID=posproductno.posProductID"
        strSQL += " Inner join sysregister on sysregister.sysRegcode= pt.posTransRegCode"
        strSQL += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID where posPrdTransID='" & Request.QueryString("TID") & "' and Pdes.descLang ='" + strLang + "'"
        drObj = objDataClass.GetDataReader(strSQL)
        Dim intI As Integer = 0
        While drObj.Read
            Dim tempRow As New TableRow()
            For nM As Integer = 0 To 3
                Dim tempCell As New TableCell()
                tempCell.Font.Size = 4
                If nM = 0 Then
                    tempCell.Text = drObj.Item("ProductName").ToString() & "<Br />" & drObj.Item("prdUPCCode").ToString()
                    tempCell.HorizontalAlign = HorizontalAlign.Left
                    tempCell.Width = 55
                ElseIf nM = 1 Then
                    tempCell.Text = drObj.Item("posPrdQty").ToString()
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 22
                ElseIf nM = 2 Then
                    tempCell.Text = drObj.Item("posPrdUnitPrice").ToString()
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 25
                ElseIf nM = 3 Then
                    tempCell.Text = drObj.Item("posPrdPrice").ToString()
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 40
                End If

                tempRow.Cells.Add(tempCell)

                strSubTotal = String.Format("{0:F}", CDbl(drObj.Item("posSubTotal").ToString()))
                strTax1 = String.Format("{0:F}", CDbl(drObj.Item("posTax1").ToString()))
                strTax2 = String.Format("{0:F}", CDbl(drObj.Item("posTax2").ToString()))
                strTax3 = String.Format("{0:F}", CDbl(drObj.Item("posTax3").ToString()))
                strTax4 = String.Format("{0:F}", CDbl(drObj.Item("posTax4").ToString()))

                strTaxDesc1 = drObj.Item("posTax1Desc").ToString()
                strTaxDesc2 = drObj.Item("posTax2Desc").ToString()
                strTaxDesc3 = drObj.Item("posTax3Desc").ToString()
                strTaxDesc4 = drObj.Item("posTax4Desc").ToString()


                strTotal = String.Format("{0:F}", CDbl(drObj.Item("posTotalValue").ToString()))
                strReceived = String.Format("{0:F}", CDbl(drObj.Item("posCasReceived").ToString()))
                strReturn = String.Format("{0:F}", CDbl(drObj.Item("posCashReturned").ToString()))
                lblCustTotalAmount.Text = "&nbsp;&nbsp; " & strTotal
                lblMerTotalAmount.Text = "&nbsp;&nbsp; " & strTotal
                lblDebTotalAmount.Text = "&nbsp;&nbsp; " & strTotal

            Next
            Table1.Rows.Add(tempRow)
            strCarName = drObj.Item("posCardName").ToString()
            strCardNo = drObj.Item("posCardNo").ToString()
            lblNote.Text = drObj.Item("sysRegMessage").ToString()
        End While
        lblCustCardName.Text = strCarName
        lblCustCardName.ForeColor = Drawing.Color.Black
        lblCustCardNo.Text = strCardNo
        lblCustCardNo.ForeColor = Drawing.Color.Black
        lblMCardName.Text = strCarName
        lblMCardNo.Text = strCardNo
        lblDCardName.Text = strCarName
        lblDCardNo.Text = strCardNo
        objDataClass.CloseDatabaseConnection()
        drObj.Close()
        subCreateTable(strSubTotal, strTax1, strTax2, strTax3, strTotal, strReceived, strReturn, strTax4, strTaxDesc1, strTaxDesc2, strTaxDesc3, strTaxDesc4)
    End Sub
    Public Sub subCreateTable(ByVal stSubtotal As String, ByVal strTax1 As String, ByVal strTax2 As String, ByVal strTax3 As String, ByVal strTotal As String, ByVal strReceived As String, ByVal strReturn As String, ByVal strTax4 As String, ByVal strTaxDesc1 As String, ByVal strTaxDesc2 As String, ByVal strTaxDesc3 As String, ByVal strTaxDesc4 As String)
        ' Create a TableHeaderRow. 
        Dim headerRow As New TableHeaderRow()
        headerRow.HorizontalAlign = HorizontalAlign.Right

        For nM As Integer = 0 To 3
            Dim headerTableCell As New TableHeaderCell()
            Dim footerTableCell As New TableCell()
            headerTableCell.Font.Size = 4
            If nM = 0 Then
                headerTableCell.Text = msgPrd
                headerTableCell.HorizontalAlign = HorizontalAlign.Left
                headerTableCell.Width = 55
            ElseIf nM = 1 Then
                headerTableCell.Text = msgQty
                headerTableCell.Width = 22
            ElseIf nM = 2 Then
                headerTableCell.Text = msgPrice
                headerTableCell.Width = 25
            ElseIf nM = 3 Then
                headerTableCell.Text = msgTotalPrice
                headerTableCell.Width = 40
            End If
            headerRow.Cells.Add(headerTableCell)
        Next
        Table1.Rows.AddAt(0, headerRow)

        Dim RowSub As New TableRow()
        For nM As Integer = 0 To 1
            Dim tempCell As New TableCell()
            tempCell.Font.Size = 4
            If nM = 0 Then
                tempCell.Text = msgSubTotal & "  -->"
                tempCell.HorizontalAlign = HorizontalAlign.Right
                tempCell.Width = 65
                tempCell.Font.Bold = True
            ElseIf nM = 1 Then
                tempCell.Text = stSubtotal
                tempCell.HorizontalAlign = HorizontalAlign.Right
                tempCell.Width = 25
            End If
            RowSub.Cells.Add(tempCell)
        Next
        tblsubTotal.Rows.Add(RowSub)


        For nR As Integer = 0 To 3
            Dim RowTax As New TableRow()
            Dim IsRow As Boolean = False
            For nM As Integer = 0 To 1
                Dim tempCell As New TableCell()
                tempCell.Font.Size = 4
                If nR = 0 Then
                    If strTax1 <> "0.00" Then
                        IsRow = True
                        If nM = 0 Then
                            tempCell.Text = strTaxDesc1 & "  -->"
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                            tempCell.Width = 65
                            tempCell.Font.Bold = True
                        ElseIf nM = 1 Then
                            tempCell.Text = strTax1
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                            tempCell.Width = 25
                        End If
                    End If
                ElseIf nR = 1 Then
                    If strTax2 <> "0.00" Then
                        IsRow = True
                        If nM = 0 Then
                            tempCell.Text = strTaxDesc2 & "  -->"
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                            tempCell.Font.Bold = True
                        ElseIf nM = 1 Then
                            tempCell.Text = strTax2
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                        End If
                    End If
                ElseIf nR = 2 Then
                    If strTax3 <> "0.00" Then
                        IsRow = True
                        If nM = 0 Then
                            tempCell.Text = strTaxDesc3 & "  -->"
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                            tempCell.Font.Bold = True
                        ElseIf nM = 1 Then
                            tempCell.Text = strTax3
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                        End If
                    End If
                ElseIf nR = 3 Then
                    If strTax4 <> "0.00" Then
                        IsRow = True
                        If nM = 0 Then
                            tempCell.Text = strTaxDesc4 & "  -->"
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                            tempCell.Font.Bold = True
                        ElseIf nM = 1 Then
                            tempCell.Text = strTax4
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                        End If
                    End If
                End If
                RowTax.Cells.Add(tempCell)
            Next
            If IsRow Then
                tblTax.Rows.Add(RowTax)
            End If
        Next

        For nR As Integer = 0 To 2
            Dim RowAmount As New TableRow()
            For nM As Integer = 0 To 1
                Dim tempCell As New TableCell()
                tempCell.Font.Size = 4
                If nR = 0 Then
                    If nM = 0 Then
                        tempCell.Text = msgTotal & "  -->"
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                        tempCell.Width = 65
                        tempCell.Font.Bold = True
                    ElseIf nM = 1 Then
                        tempCell.Text = strTotal
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                        tempCell.Width = 25
                    End If
                ElseIf nR = 1 Then
                    If nM = 0 Then
                        tempCell.Text = msgReceived & "  -->"
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                        tempCell.Font.Bold = True
                    ElseIf nM = 1 Then
                        If Request.QueryString("cardtype").ToLower = "r" Then
                            tempCell.Text = "0.00"
                        Else
                            tempCell.Text = strReceived
                        End If
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                    End If
                ElseIf nR = 2 Then
                    If nM = 0 Then
                        If Request.QueryString("cardtype").ToLower = "r" Then
                            tempCell.Text = msgPosRefund & "  -->"
                        Else
                            tempCell.Text = msgBalance & "  -->"
                        End If
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                        tempCell.Font.Bold = True
                        'tempCell.Text = msgBalance & "  -->"
                        'tempCell.HorizontalAlign = HorizontalAlign.Right
                        'tempCell.Font.Bold = True
                    ElseIf nM = 1 Then
                        If Request.QueryString("cardtype").ToLower = "r" Then
                            tempCell.Text = strReceived
                        Else
                            tempCell.Text = strReturn
                        End If
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                    End If
                End If
                RowAmount.Cells.Add(tempCell)
            Next
            tblAmount.Rows.Add(RowAmount)
        Next
    End Sub
    Private Sub subClose()
        Response.Write("<script>window.opener.location.reload();</script>")
        Response.Write("<script>window.close();</script>")
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        'If Session("Language") = "" Or Session("Language") Is Nothing Then
        '    Session("Language") = "en-CA"
        'End If
        'If Not Session("Language") = "en-CA" Then
        '    Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        'End If
        If Request.QueryString("lang") = "[48]" Or Request.QueryString("lang") = "[0]" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo("en-CA")
        Else
            Thread.CurrentThread.CurrentUICulture = New CultureInfo("fr-CA")
        End If
    End Sub
End Class

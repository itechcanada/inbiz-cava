<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Kitchen.aspx.cs" Inherits="POS_NewKitchen" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../lib/css/google/css/css3-buttons.css" rel="stylesheet" type="text/css" />
    <script src="../jQuery-plugins/itech.js" language="JavaScript" type="text/javascript"></script>
    <link href="~/lib/scripts/jquery.jqGrid-4.0.0/css/ui.jqgrid.css" rel="stylesheet"
        type="text/css" media="screen" />
    <link href="../lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <script src="../lib/scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src='../lib/scripts/jquery-ui-1.8.11.custom.min.js' type="text/javascript"></script>
    <script src="../lib/scripts/jquery-plugins/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <link href="../lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <script src="../lib/scripts/json2.js" type="text/javascript"></script>
    <link href="NewKitchenCss/css/style.css" type="text/css" rel="Stylesheet" />
    <link href="NewKitchenCss/css/KitchenCustom.css" type="text/css" rel="Stylesheet" />
    <script src="../lib/scripts/jquery-plugins/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">
        var userID = '<%=iTECH.InbizERP.BusinessLogic.CurrentUser.UserID%>';
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <%--<img src="NewKitchenCss/images/neworder.gif"/>--%>
        <div class="main_wrapper">
            <div id="header" class="fll">
                <div id="divPreviousButton" class="left_btn fll">
                    <a id="divPrevious" href="javascript:void(0)">
                        <img src="NewKitchenCss/images/left_arrow.png" /></a>
                </div>
                <div id="divNextButton" class="right_btn flr">
                    <a id="divNext" href="javascript:void(0)">
                        <img src="NewKitchenCss/images/right_arrow.png" /></a>
                </div>
                <div class="head_content">
                    <div id="dvSearch">
                        <div class="code_confirm fll">
                            <asp:TextBox ID="txtConfirmCode" runat="server" placeholder="<%$ Resources:Resource, grdPOSTabNote %>"
                                onkeypress="return AjxPostSearch(event)"></asp:TextBox>
                            <input type="image" src="NewKitchenCss/images/tick_submit.png" value="" onclick="return AjxPostSearch('13')" />
                        </div>
                        <div class="assets_main fll">
                            <div class="waitin" onclick="$('#txtConfirmCode').val(''); IsOnHold = true; displayRecordFrom = 0; AjaxCallForCount();">
                                <p id="divHeld" class="divheldcss">
                                </p>
                            </div>
                            <div class="assets" onclick="$('#txtConfirmCode').val(''); IsOnHold = false; displayRecordFrom = 0; AjaxCallForCount();">
                                <p id="divAccept">
                                </p>
                            </div>
                        </div>
                        <div class="pay_points fll">
                            <a href="javascript:void(0)" onclick="ShowLoyalty();">
                                <%=Resources.Resource.lblPayLoyalPoint%></a></div>
                        <div class="clear">
                        </div>
                    </div>
                    <div id="divLogin" class="dnone">
                        <div id="slide2">
                            <div style="background-color: Yellow; width: 100%;">
                                <a href="javascript:void(0)" style="float: right; padding: 5px;" onclick="CloseLoyalPoint()">
                                    <img src="NewKitchenCss/images/close-icon.png" /></a>
                            </div>
                            <div id="tblusernotfound" style="color: Red; text-align: center; font-size: 16px;">
                                <literal id="ltrErrorMsg"></literal>
                            </div>
                            <h1>
                                POINTS DE RECOMPENSE</h1>
                            <div class="email fll">
                                <input id="txtEmailID" type="text" placeholder="Email">
                                <a href="javascript:void(0)" onclick="ChkUserExits();">
                                    <img src="NewKitchenCss/images/brown_tick.png" class="email_submit" />
                                </a>
                            </div>
                            <div class="plus_btn flr">
                                <a href="javascript:void(0)" onclick="userRegistration();">
                                    <img src="NewKitchenCss/images/plus_icon.png" /></a>
                            </div>
                        </div>
                    </div>
                    <div id="divCreateUser" class="dnone">
                        <div id="slide3">
                            <h1>
                                POINTS DE RECOMPENSE</h1>
                            <div class="input_box fll">
                                <div class="input_fld fll">
                                    <input id="txtUserEmailID" type="text" runat="server" placeholder='Email'></div>
                                <div class="input_fld fll">
                                    <input id="txtPhone" type="text" runat="server" placeholder='<%$ Resources:Resource, lblPhone %>'></div>
                                <div class="input_fld fll">
                                    <input id="txtUserName" type="text" runat="server" placeholder='<%$ Resources:Resource, lblName %>'></div>
                                <div class="input_fld fll">
                                    <input id="txtUserAddress" type="text" runat="server" placeholder='<%$ Resources:Resource, lblAddress %>'></div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="userRegSubmit">
                                <a href="javascript:void(0)" onclick="FunUserRegistration();">
                                    <img alt="" src="NewKitchenCss/images/brown_tick.png" /></a>
                            </div>
                            <div class="close_btn fll">
                                <a href="javascript:void(0)" onclick="CancelButton();">
                                    <img alt="" src="NewKitchenCss/images/close_icom.png"></a></div>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                    <div id="divLoyaltyPoints" class="dnone">
                        <div id="slide4">
                            <h1>
                                POINTS DE RECOMPENSE</h1>
                            <div class="content_box1 fll">
                                <div class="text_row">
                                    <label>
                                        Email:
                                    </label>
                                    <span id="ltrLPEmailID">johnsmith@gmail.com</span>
                                </div>
                                <div class="text_row">
                                    <label>
                                        <%=Resources.Resource.lblName%>:
                                    </label>
                                    <span id="ltrLPName"></span>
                                </div>
                                <div class="text_row">
                                    <label>
                                        <%=Resources.Resource.lblPhone%>:
                                    </label>
                                    <span id="ltrLPPhone"></span>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="content_box2 fll">
                                <div class="prix fll">
                                    <%=Resources.Resource.lblPrice%>:<span>$<input type="text" id="txtPrice" class="validatenumeric lprice"
                                        maxlength="10" /></span>
                                </div>
                                <div class="points fll">
                                    POINTS:<literal id="ltrPointsValues"></literal>
                                </div>
                                <div class="payer_btn fll">
                                    <a href="javascript:void(0)" onclick="SaveLoyalPoints('M');">PAYER<br>
                                        EN POINTS</a></div>
                                <div class="point_btn fll">
                                    <a href="javascript:void(0)" onclick="SaveLoyalPoints('P');">AJOUTER
                                        <br>
                                        POINTS</a></div>
                                <div class="close fll">
                                    <a href="javascript:void(0)" onclick="CancelButton();">
                                        <img alt="" src="NewKitchenCss/images/close_box.png"></a></div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="content_box3 fll">
                                <div class="text_row">
                                    <label>
                                        <%=Resources.Resource.lblCustomerSinceDate%>:
                                    </label>
                                    <span id="ltrLPSinceDate"></span>
                                </div>
                                <div class="text_row">
                                    <label>
                                        <%=Resources.Resource.lblLastSaleDate%>:
                                    </label>
                                    <span id="ltrLPSaleDate"></span>
                                </div>
                                <div class="text_row">
                                    <label>
                                        <%=Resources.Resource.lblAddress%>:
                                    </label>
                                    <span id="ltrAddress"></span>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="content" class="fll">
                <div id="divOrderDetails1" class="popup1 fll">
                </div>
                <div id="divOrderDetails2" class="popup1 fll">
                </div>
                <div id="divOrderDetails3" class="popup1 fll">
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    <div id="divPop" style="display: none; background-color: rgba(10,10,10,.4); text-align: center;
        position: absolute; top: 0; width: 100%; height: 100%;">
        <%--height: 100%; width: 100%; --%>
        <div style="width: 100%; display: none;">
            <img src="NewKitchenCss/images/close-icon.png" style="float: right; cursor: pointer;"
                onclick="closedialog()" />
        </div>
        <div id="divPopinner" style="margin: auto; top: 42%; width: 80%; display: block;
            background-color: White; position: relative;">
            <div style="color: white; font-family: monterey_btregular; font-size: 40px; font-weight: normal;
                text-align: center; padding: 20px 0; border: 4px solid #FF6700;">
                <table border="0" style="width: 100%;">
                    <tr>
                        <td>
                            <div style="background-color: #DE6B37; width: 80%; cursor: pointer; margin: auto;"
                                id="divOrderHold">
                                <%= Resources.Resource.lblHeld%>&nbsp;
                            </div>
                        </td>
                        <td>
                            <div style="background-color: #DE6B37; width: 80%; cursor: pointer; margin: auto;"
                                id="divOrderDelete">
                                <%= Resources.Resource.lblKitchenDelete%>&nbsp;
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%--   <div id="divAcceptSubject" style="display: none;">
        <%=Resources.Resource.lblkitchenAcceptedSubject%>
    </div>--%>
    <div id="divAcceptMessage" style="display: none;">
        <%=Resources.Resource.lblkitchenAcceptedMessage_en%>
        <br />
        <br />
        <%=Resources.Resource.lblkitchenAcceptedMessage_fr%>
    </div>
    <%-- <div id="divHoldSubject" style="display: none;">
        <%=Resources.Resource.lblkitchenHoldSubject%>
    </div>--%>
    <%--<div id="divHoldMessage" style="display: none;">--%>
    <%--<%=Resources.Resource.lblkitchenHoldMessage_en%> <br /> <br />
        <%=Resources.Resource.lblkitchenHoldMessage_fr%>   + sRegisterInfo + "<br/>Merci !"--%>
    <%-- </div>--%>
    <div id="divReadySubject" style="display: none;">
        <%=Resources.Resource.lblkitchenReadySubject	%>
    </div>
    <div id="divReadyMessage" style="display: none;">
        <%=Resources.Resource.lblkitchenReadyMessage_en%>
        <br />
        <br />
        <%=Resources.Resource.lblkitchenReadyMessage_fr%>
    </div>
    <input type="hidden" id="hdnEmailID" />
    <input type="hidden" id="hdnOrderPrint1" />
    <input type="hidden" id="hdnOrderPrint2" />
    <input type="hidden" id="hdnOrderPrint3" />
    </form>
</body>
<script type="text/javascript">
    var currentAppLanguageCode = '<%= iTECH.InbizERP.BusinessLogic.Globals.CurrentAppLanguageCode %>';
    var sRegisterInfo = "";
    var jsonTrans = '';
    var currentStatusTypeCallForOrderDetails = 10;
    var displayRecordFrom = 0;
    var resourceText = "";
    var IsOnHold = false;
    var currStatusCode = 10;  //we starting with new order and ready to change its satatus (10), then 12,13
    var currStatusReadyCode = 12; //To Set for those records which is ready to change its status (12) and then 13
    var noOfRecordsInDisplayScreen = 0;
    var htmlSniptsData = '';
    var htmlSniptsPrintData = '';
    var vTransIDToHoldDelete = '';
    var autoLoad = false;

    $(document).ready(function () {
        RegisterInfo();
        $.get("HtmlSnipts/PrintOrderTemplate.htm", function (data) {
            htmlSniptsPrintData = data;
        });
        $.get("HtmlSnipts/OrderDisplayFormat.htm", function (data) {
            htmlSniptsData = data;
            AjaxCallForCount();

            $('#divPrevious').click(function () {
                currStatusCode = 10;    //To reset its value
                currStatusReadyCode = 12;   //To reset its value
                displayRecordFrom = Number(displayRecordFrom) - 3;
                ShowOrderDetails(currentStatusTypeCallForOrderDetails);
            });
            $('#divNext').click(function () {
                currStatusCode = 10;    //To reset its value
                currStatusReadyCode = 12;   //To reset its value
                displayRecordFrom = Number(displayRecordFrom) + 3;
                ShowOrderDetails(currentStatusTypeCallForOrderDetails);
            });
        });
    });

    function GetKitchenHtmlSnipts() {
        $.get("HtmlSnipts/OrderDisplayFormat.htm", function (data) {
            htmlSniptsData = data;
            //$(".result").html(data);
        });
    }

    function AjaxCallForCount() {
        var vSearchConfCode = $("#txtConfirmCode").val();
        $.ajax({
            type: "POST",
            url: '../Restaurant-Menu/Restaurant-Menu.asmx/AllStatusCount',
            data: "{confCode:'" + vSearchConfCode + "', IsOnHold:'" + IsOnHold + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var JsonObject = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (JsonObject.ResponseCode == 1 && JsonObject.Status == "OK") {
                    jsonTrans = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    ShowStatusCount(JsonObject.CountNEW, JsonObject.CountACCEPT, JsonObject.CountHELD, JsonObject.CountREADY);
                    ShowOrderDetails(currentStatusTypeCallForOrderDetails);
                }
                else { alert("ResponseCode = " + JsonObject.ResponseCode + " Status = " + JsonObject.Status); }
            },
            error: function (request, status, error) {
            }
        });
    }

    setInterval(function () { ChangeStatusLoop(); }, 60 * 1000 * 0.25); //To set this interval, purpose is no. of orders with its Status is refreced.

    function ChangeStatusLoop() {

        if (autoLoad == true)
            return;

        var timeInMin = "15";
        $.ajax({
            type: "POST",
            url: '../Restaurant-Menu/Restaurant-Menu.asmx/ChangesStatus',
            data: "{statusTypeID:'',transID:'',userID:'" + userID + "',timeInMin:'" + timeInMin + "',IsOnHold:'" + IsOnHold + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var JsonObject = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (JsonObject.ResponseCode == 1 && JsonObject.Status == "OK") {
                    jsonTrans = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    ShowStatusCount(JsonObject.CountNEW, JsonObject.CountACCEPT, JsonObject.CountHELD, JsonObject.CountREADY);
                    var confcode = $("#txtConfirmCode").val();
                    if (((noOfRecordsInDisplayScreen < 3) || (TotalRecored() < 3)) && (confcode == "")) {
                        currStatusCode = 10;    //To reset its value
                        currStatusReadyCode = 12;   //To reset its value
                        ShowOrderDetails(currentStatusTypeCallForOrderDetails);
                    }
                }
                else { alert("ResponseCode = " + JsonObject.ResponseCode + " Status = " + JsonObject.Status); }
            },
            error: function (request, status, error) {
            }
        });
    }

    function ChangeStatus(statusTypeID, transID) {
        blockContentArea($("body"), '<%=Resources.Resource.lblLoading %>');  //Screen Blocking start
        statusTypeID = statusTypeID;
        if (statusTypeID == 0) {
            statusTypeID = $("#hdnOrdStatus" + transID).val();
        }
        if (statusTypeID == 12) {
            statusTypeID = 13;
            $("#hdnOrdStatus" + transID).val("13");
        }
        if (statusTypeID == 9 && document.getElementById('btn' + transID) != null) {   //Going to change its status to "Accept"
            resourceText = ("<%=Resources.Resource.lblAccept %>");
            $('#lbl' + transID).html(resourceText);
            var sVar = " <img id=img" + transID + " src='NewKitchenCss/images/popup_submit.png'/>";
            $("#btn" + transID).html(sVar);
            $("#btn" + transID).addClass("submit fll");
            //currStatusCode = 12;
            $("#hdnOrdStatus" + transID).val("10");
            //statusTypeID = 10;

        }
        if (statusTypeID == 10) {   //Going to change its status "Ready"
            resourceText = ("<%=Resources.Resource.lblReady %>");
            $('#lbl' + transID).html(resourceText);
            var sVar = " <img id=img" + transID + " src='NewKitchenCss/images/delivery.png'  class='imgneworder'/>";
            $("#btn" + transID).html(sVar);
            $("#lblDolor" + transID).removeClass("dnone");
            //currStatusCode = 13;
            //currStatusReadyCode = 13;
            $("#hdnOrdStatus" + transID).val("12");
            statusTypeID = 12;
        }
        if (statusTypeID == 9) {
            statusTypeID = 10;
        }


        var timeInMin = "15";
        if ($("#rdbtn20Min" + transID).attr("checked") == true)
            timeInMin = $("#rdbtn20Min" + transID).val();
        else if ($("#rdbtn30Min" + transID).attr("checked") == true)
            timeInMin = $("#rdbtn30Min" + transID).val();
        else if ($("#rdbtn45Min" + transID).attr("checked") == true)
            timeInMin = $("#rdbtn45Min" + transID).val();
        $.ajax({
            type: "POST",
            url: '../Restaurant-Menu/Restaurant-Menu.asmx/ChangesStatus',
            data: "{statusTypeID:'" + statusTypeID + "',transID:'" + transID + "',userID:'" + userID + "',timeInMin:'" + timeInMin + "',IsOnHold:'" + IsOnHold + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var JsonObject = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (JsonObject.ResponseCode == 1 && JsonObject.Status == "OK") {
                    jsonTrans = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    ShowStatusCount(JsonObject.CountNEW, JsonObject.CountACCEPT, JsonObject.CountHELD, JsonObject.CountREADY);

                    if (statusTypeID == 13 || statusTypeID == 11) {   // To remove orders from showing panel, i.e Status is Picked Up that way to reload ShowOrdersDetails
                        currStatusCode = 10;    //To reset its value
                        currStatusReadyCode = 12;   //To reset its value
                        ShowOrderDetails(currentStatusTypeCallForOrderDetails);
                    }
                    unblockContentArea($("body"));  //Unblock the screen
                }
                else { alert("ResponseCode = " + JsonObject.ResponseCode + " Status = " + JsonObject.Status); }
            },
            error: function (request, status, error) {
            }
        });
        SendEmail(statusTypeID, transID);
    }

    var preCountNew = 0;
    function ShowStatusCount(countNEW, countACCEPT, countHELD, countREADY) {
        if (preCountNew != countNEW) {
            PlaySound();
        }
        preCountNew = countNEW;
        resourceText = ("<%=Resources.Resource.lblAccept %>");
        if (IsOnHold == true) {
            $("#divAccept").html("" + countACCEPT + "<br /> " + resourceText.replace("'", "\'"));
        }
        else {
            $("#divAccept").html("" + countACCEPT + "/" + countNEW + "<br /> " + resourceText.replace("'", "\'"));
        }
        resourceText = ("<%=Resources.Resource.lblHeld %>");
        $("#divHeld").html("" + countHELD + " " + resourceText.replace("'", "\'"));
        var confcode = $("#txtConfirmCode").val();
        if (displayRecordFrom == 0 && (TotalRecored()) > 3 && confcode == "") {
            $('#divNext').show();
        }
    }

    function TotalRecored() {
        var trans = jsonTrans.Transition;
        var totalRecored = 0;
        for (i = 0; i < trans.length; i++) {
            if (Number(trans[i].StatusTypeID) == 10 || Number(trans[i].StatusTypeID) == 9 || Number(trans[i].StatusTypeID) == 12) {
                totalRecored++;
            }
        }
        return totalRecored;
    }

    function ShowOrderDetails(statusTypeID) {
        var trans = jsonTrans.Transition;
        var totalRecored = TotalRecored();
        if (displayRecordFrom <= 0 || statusTypeID == 11)
            $('#divPrevious').hide();
        else
            $('#divPrevious').show();
        if (Number(displayRecordFrom + 3) >= Number(totalRecored) || statusTypeID == 11 || totalRecored <= 3)
            $('#divNext').hide();
        else
            $('#divNext').show();
        $('#divOrderDetails1').empty();
        $('#divOrderDetails2').empty();
        $('#divOrderDetails3').empty();
        currentStatusTypeCallForOrderDetails = statusTypeID;
        var countTrans = 0;
        var isWhileRun = true;
        while (isWhileRun) {
            for (i = displayRecordFrom; i < trans.length; i++) {
                var sConCode = trans[i].TableNote;  //Set confirmation code of its order
                var sSearchConCode = $("#txtConfirmCode").val().trim();    //Set confirmation code which you are searching
                if (Number(trans[i].StatusTypeID) == Number(statusTypeID) && trans[i].IsOnHold == IsOnHold || (sSearchConCode != '')) {
                    if ((sConCode == sSearchConCode) || sSearchConCode == '' || (sConCode.slice(-3) == sSearchConCode)) {
                        countTrans++;
                        htmlSniptsOrgData = htmlSniptsData;
                        htmlSniptsOrgPrintData = htmlSniptsPrintData;
                        if (IsOnHold == true) {
                            var resourceTextHold = "<%=Resources.Resource.lblHeld %>";
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#orderTitle#", resourceTextHold);
                            htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#orderTitle#", resourceTextHold);
                        }
                        else {
                            if (Number(trans[i].StatusTypeID) == 10) {
                                resourceText = "<%=Resources.Resource.lblAccept %>";
                                htmlSniptsOrgData = htmlSniptsOrgData.replace("#orderTitle#", resourceText);
                                htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#orderTitle#", resourceText);
                            }
                            else if (Number(trans[i].StatusTypeID) == 12) {
                                resourceText = "<%=Resources.Resource.lblReady %>";
                                htmlSniptsOrgData = htmlSniptsOrgData.replace("#orderTitle#", resourceText);
                                htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#orderTitle#", resourceText);
                            }
                            else if (Number(trans[i].StatusTypeID) == 13) {
                                resourceText = "<%=Resources.Resource.lblPickedUp %>";
                                htmlSniptsOrgData = htmlSniptsOrgData.replace("#orderTitle#", resourceText);
                                htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#orderTitle#", resourceText);
                            }
                            else {
                                resourceText = "<%=Resources.Resource.lblNew %>";
                                htmlSniptsOrgData = htmlSniptsOrgData.replace("#orderTitle#", resourceText);
                                htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#orderTitle#", resourceText);
                            }
                        }
                        var lblPickedUp = "<%=Resources.Resource.lblPickedUp %>";
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#PICK-UP#", lblPickedUp);
                        htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#PICK-UP#", lblPickedUp);
                        var lblOrderNo = "<%=Resources.Resource.lblKitchenOrderNo %>";

                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#lblTitleID#", 'lbl' + trans[i].TransID);
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#lblDolorID#", 'lblDolor' + trans[i].TransID);
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#POSORDDetail#", trans[i].TransID);
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#TransIDToHoldDelete#", trans[i].TransID);
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#hdntransID#", trans[i].TransID);
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#OrdStatus#", trans[i].StatusTypeID);
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#lblOrderNo#", lblOrderNo);
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#transID", trans[i].TransID);
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#poCreationDate", trans[i].PoCreationDate);
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#ConfirmatioCode#", trans[i].TableNote);
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#pickUpDateTime", trans[i].PickUpDateTime);
                        //Printdata
                        htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#lblTitleID#", 'lbl' + trans[i].TransID);
                        htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#hdntransID#", trans[i].TransID);
                        htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#OrdStatus#", trans[i].StatusTypeID);
                        htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#lblOrderNo#", lblOrderNo);
                        htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#transID", trans[i].TransID);
                        htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#poCreationDate", trans[i].PoCreationDate);
                        htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#ConfirmatioCode#", trans[i].TableNote);
                        htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#pickUpDateTime", trans[i].PickUpDateTime);

                        var lblQty = "<%=Resources.Resource.prnYSRQty %>";
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#lblQty#", lblQty);
                        htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#lblQty#", lblQty);
                        var lblPrice = "<%=Resources.Resource.lblKitchenPrdPrice %>";
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#lblPrice#", lblPrice);
                        htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#lblPrice#", lblPrice);
                        var lblName = "<%=Resources.Resource.lblKitchenPartnerName %>";
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#lblName#", lblName);
                        htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#lblName#", lblName);

                        var sItemDesc = '';
                        var orderDetails = trans[i].OrderDetails;
                        for (j = 0; j < orderDetails.length; j++) {
                            sItemDesc += "<tr>";
                            sItemDesc += "<td class='sno'>" + Number(j + 1) + "</td>";
                            sItemDesc += "<td class='desc'>" + orderDetails[j].ProductName + " (" + orderDetails[j].SubCatName + ")</td>";
                            sItemDesc += "<td class='qty'>" + orderDetails[j].Qty + "</td>";
                            sItemDesc += "<td class='amt'>$" + Number(orderDetails[j].UnitPrice).toFixed(2) + "</td>";
                            sItemDesc += "</tr>";
                        }

                        if (sItemDesc == '') {
                            sItemDesc = "<tr><td class='sno'>&nbsp;</td><td class='desc'>&nbsp;</td><td class='qty'>&nbsp;</td><td class='amt'>&nbsp;</td></tr>";
                        }
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#ItemDescription#", sItemDesc);
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#Notes#", trans[i].AdditionalRequest);
                        htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#ItemDescription#", sItemDesc);
                        htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#Notes#", trans[i].AdditionalRequest);
                        if (trans[i].Partners.PartnerLongName != "" && trans[i].Partners.PartnerLongName != null) {
                            if (Number(trans[i].LoyaltyPoint) > 0) {
                                htmlSniptsOrgData = htmlSniptsOrgData.replace("#partnerLongName", trans[i].Partners.PartnerLongName + "(" + Number(trans[i].LoyaltyPoint) + ")");
                                htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#partnerLongName", trans[i].Partners.PartnerLongName + "(" + Number(trans[i].LoyaltyPoint) + ")");
                            }
                            else {
                                htmlSniptsOrgData = htmlSniptsOrgData.replace("#partnerLongName", trans[i].Partners.PartnerLongName);
                                htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#partnerLongName", trans[i].Partners.PartnerLongName);
                            }
                        }
                        if (trans[i].CustPhoneNo != "" && trans[i].CustPhoneNo != null) {
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#partnerPhone", trans[i].CustPhoneNo);
                            htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#partnerPhone", trans[i].CustPhoneNo);
                        }
                        else
                            if (trans[i].Partners.PartnerPhone != "" && trans[i].Partners.PartnerPhone != null) {
                                htmlSniptsOrgData = htmlSniptsOrgData.replace("#partnerPhone", trans[i].Partners.PartnerPhone);
                                htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#partnerPhone", trans[i].Partners.PartnerPhone);
                            }
                        if (trans[i].CustEmailID != "" && trans[i].CustEmailID != null) {
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#partnerEmail", trans[i].CustEmailID);
                            htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#partnerEmail", trans[i].CustEmailID);
                        }
                        else if (trans[i].Partners.PartnerEmail != "" && trans[i].Partners.PartnerEmail != null) {
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#partnerEmail", trans[i].Partners.PartnerEmail);
                            htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#partnerEmail", trans[i].Partners.PartnerEmail);
                        }
                        if (trans[i].PaidStatus == "1") {
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#classPaymentStatus#", "pay_btn");
                            htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#classPaymentStatus#", "pay_btn");
                            var resourceText = "<%=Resources.Resource.lblPaid %>";
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#paymentStatus#", resourceText);
                            htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#paymentStatus#", resourceText);
                        }
                        else if (trans[i].PaidStatus == "2") {
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#classPaymentStatus#", "pay_pas_btn");
                            htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#classPaymentStatus#", "pay_pas_btn");
                            var resourceText = "<%=Resources.Resource.lblNotPaid %>";
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#paymentStatus#", resourceText);
                            htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#paymentStatus#", resourceText);
                        }
                        else if (trans[i].PaidStatus == "3") {
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#classPaymentStatus#", "pay_point_btn");
                            htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#classPaymentStatus#", "pay_point_btn");
                            var resourceText = "<%=Resources.Resource.lblPaidwithpoints %>";
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#paymentStatus#", resourceText);
                            htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#paymentStatus#", resourceText);
                        }
                        else if (trans[i].PaidStatus == "4") {
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#classPaymentStatus#", "pay_point_btn");
                            htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#classPaymentStatus#", "pay_point_btn");
                            var resourceText = "<%=Resources.Resource.lblPointPickup %>";
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#paymentStatus#", resourceText);
                            htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#paymentStatus#", resourceText);
                        }
                        else {
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#classPaymentStatus#", "pay_point_btn");
                            htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#classPaymentStatus#", "pay_point_btn");
                            var resourceText = "<%=Resources.Resource.lblPointCredit %>";
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#paymentStatus#", resourceText);
                            htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#paymentStatus#", resourceText);
                        }
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#totalAmount", Number(trans[i].TotalAmount).toFixed(2));
                        htmlSniptsOrgPrintData = htmlSniptsOrgPrintData.replace("#totalAmount", Number(trans[i].TotalAmount).toFixed(2));
                        var printValue = '';
                        if (Number(trans[i].StatusTypeID) == 9) {
                            printValue += "ChangeStatus(0," + trans[i].TransID + ");";
                        }
                        $("#hdnOrderPrint" + countTrans).val(htmlSniptsOrgPrintData);
                        printValue += " PrintDiv('hdnOrderPrint" + countTrans + "');  return false;";
                        htmlSniptsOrgData = htmlSniptsOrgData.replace("#printOnClick#", printValue);

                        var divHTML = '';
                        if (Number(trans[i].StatusTypeID) == 9) {
                            divHTML += " <a id=btn" + trans[i].TransID + " onclick='ChangeStatus(0," + trans[i].TransID + "); return false;' class='submit fll' href='javascript:;'>"; //class='ovalbutton'
                            divHTML += " <img id=img" + trans[i].TransID + " src='NewKitchenCss/images/neworder.gif'  class='imgneworder'/> </a>";
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#actionlink#", divHTML);
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#displaycss#", 'dnone');
                        }
                        else if (Number(trans[i].StatusTypeID) == 10) {
                            divHTML += " <a id=btn" + trans[i].TransID + " onclick='ChangeStatus(0," + trans[i].TransID + "); return false;'  href='javascript:;' class='submit fll'><img id=img" + trans[i].TransID + " src='NewKitchenCss/images/popup_submit.png'/></a>"; //class='ovalbutton'
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#actionlink#", divHTML);
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#displaycss#", 'dnone');
                        }
                        else if (Number(trans[i].StatusTypeID) == 13) {
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#actionlink#", '');
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#displaycss#", 'dnone');
                        }
                        else {

                            divHTML += " <a id=btn" + trans[i].TransID + " onclick='ChangeStatus(13," + trans[i].TransID + "); return false;' class='submit fll'  href='javascript:;'>"; //class='ovalbutton'
                            divHTML += " <img id=img" + trans[i].TransID + " src='NewKitchenCss/images/delivery.png'  class='imgneworder'/> </a>";
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#actionlink#", divHTML);
                            htmlSniptsOrgData = htmlSniptsOrgData.replace("#displaycss#", '');
                        }
                        $('#divOrderDetails' + countTrans).html(htmlSniptsOrgData)
                        noOfRecordsInDisplayScreen = countTrans;
                    } // end for loop
                }
                if (countTrans == 3) break;
            }

            if ((countTrans != 3 && statusTypeID == 10) && (sSearchConCode == '')) {
                statusTypeID = 9;
                isWhileRun = true;
            }
            else if ((countTrans != 3 && (statusTypeID == 9)) && (sSearchConCode == '')) {
                statusTypeID = 12;  // Status is Ready
                isWhileRun = true;
            }
            else {
                isWhileRun = false;
            }
        }
    }

    function PrintDiv(divID) {
        var data = $('#' + divID).val();
        var mywindow = window.open('', 'my div', 'height=auto,style="width:400px;float: left; position: relative; margin-right: 1%; margin-left: 2%; background-color:White; font-family:Arial; font-size:11px; color:Black;"');
        mywindow.document.write('<html><head><title>POS-ORDER</title>');
        mywindow.document.write('</head><body style="width:100%;">');
        data = data.replace('close_popup', "");
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();
        return true;
    }

    var soundObject = null;
    function PlaySound() {
        /*  if (soundObject != null) {
        document.body.removeChild(soundObject);
        soundObject.removed = true;
        soundObject = null;
        }
        soundObject = document.createElement("embed");
        soundObject.setAttribute("src", "sound.wav");
        soundObject.setAttribute("hidden", true);
        soundObject.setAttribute("autostart", true);
        */
        //  soundObject = document.createElement("audio");
        //   soundObject.setAttribute("src", "sound.wav");
        //   soundObject.setAttribute("style", "display:none");
        //   soundObject.setAttribute("autoplay", "autoplay");

        /*      document.body.appendChild(soundObject);*/
        var snd = new Audio("sound.wav"); // buffers automatically when created
        snd.load();
        snd.play();
    }

    function AjxPostSearch(e) {
        //        if (e.keyCode == 13 || e == "13") { //"Enter Key of Keyboard is 13 "
        //            AjaxCallForCount(); //Going to Search Confirmation Code
        //        }
    }

    function SendEmail(statusTypeID, transID) {
        var custEmailID = "";
        var custLang = "";
        var subject = "";
        var message = "";
        if (Number(statusTypeID) == Number(10)) {
            subject = '<%=iTECH.InbizERP.BusinessLogic.CurrentUser.CompanyName %>' + ' ' + '<%=Resources.Resource.lblOrderAccepted%>'; //" Order Accepted"; //lblOrderAccepted
            message = $('#divAcceptMessage').html();
        }
        if (Number(statusTypeID) == Number(12)) {
            subject = '<%=iTECH.InbizERP.BusinessLogic.CurrentUser.CompanyName %>' + ' ' + '<%=Resources.Resource.lblOrderReady%>'; //$('#divReadySubject').html(); //lblOrderReady
            message = $('#divReadyMessage').html();
        }

        if (Number(statusTypeID) == Number(11)) {
            subject = '<%=iTECH.InbizERP.BusinessLogic.CurrentUser.CompanyName %>' + ' ' + '<%=Resources.Resource.lblOrderHold%>'; //$('#divReadySubject').html(); //lblOrderReady
            message = '<%=Resources.Resource.lblkitchenHoldMessage_en%>' + " at " + sRegisterInfo + "<br/>Thank You !<br /> <br />" + '<%=Resources.Resource.lblkitchenHoldMessage_fr%>' + " " + sRegisterInfo + "<br/>Merci !";
        }

        if (Number(statusTypeID) == Number(10) || Number(statusTypeID) == Number(12) || Number(statusTypeID) == Number(11)) {

            var trans = jsonTrans.Transition;
            for (i = 0; i < trans.length; i++) {
                if (Number(trans[i].TransID) == Number(transID)) {
                    custEmailID = trans[i].CustEmailID;
                    //custLang = trans[i].PartLang;
                    //alert(custLang);
                    break;
                }
            }
            if (custEmailID == "" || custEmailID == null)
                return;


            var dataToPost = {};
            dataToPost.email = custEmailID;
            dataToPost.subject = subject;
            dataToPost.message = message;
            $.ajax({
                type: "POST",
                url: '../Handlers/MailHandler.ashx',
                data: $.param(dataToPost),
                success: function (response) { },
                error: function (response, status, error) {
                    alert("response=" + response + " status=" + status + " error=" + error);
                }
            });
        }
    }

    function blockContentArea(elementToBlock, loadingMessage) {
        $(elementToBlock).block({
            message: '<div>' + loadingMessage + '</div>',
            css: { border: '3px solid #a00' }
        });
    }
    function unblockContentArea(elementToBlock) {
        $(elementToBlock).unblock();
    }

    function ShowDeletePopup(sTransID) {
        if (sTransID != '') {
            vTransIDToHoldDelete = sTransID;

            $("#divPop").css("display", "block");
            showDivinCentre('divPopinner');
            $("#divPop").css("height", $(document).height());
        }
    }


    function showDivinCentre(divIDToHide) {

        var divWidth = $("#" + divIDToHide).css("width").replace("px", '');
        var divHeight = $("#" + divIDToHide).css("height").replace("px", '');
        var divId = divIDToHide; // id of the div that you want to show in center

        // Get the x and y coordinates of the center in output browser's window 
        var centerX, centerY;
        if (self.innerHeight) {
            centerX = self.innerWidth;
            centerY = self.innerHeight;
        }
        else if (document.documentElement && document.documentElement.clientHeight) {
            centerX = document.documentElement.clientWidth;
            centerY = document.documentElement.clientHeight;
        }
        else if (document.body) {
            centerX = document.body.clientWidth;
            centerY = document.body.clientHeight;
        }

        var offsetLeft = (centerX - divWidth) / 2;
        var offsetTop = (centerY - divHeight) / 2;

        // The initial width and height of the div can be set in the
        // style sheet with display:none; divid is passed as an argument to // the function
        var ojbDiv = document.getElementById(divId);

        ojbDiv.style.position = 'absolute';
        ojbDiv.style.top = offsetTop + 'px';
        ojbDiv.style.left = offsetLeft + 'px';
        ojbDiv.style.display = "block";

    }

    $("#divOrderHold").click(function () {
        if (vTransIDToHoldDelete != '') {
            ChangeStatus(11, vTransIDToHoldDelete);
            vTransIDToHoldDelete = '';
            closedialog();
        }
    });

    $("#divOrderDelete").click(function () {
        if (vTransIDToHoldDelete != '') {
            CloseOrder(vTransIDToHoldDelete);
            vTransIDToHoldDelete = '';
            displayRecordFrom = 0; AjaxCallForCount();
            closedialog();
        }
    });

    function CloseOrder(trnsID) {
        $.ajax({
            type: "POST",
            url: 'Kitchen.aspx/DeleteOrder',
            data: "{ordID:'" + trnsID + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var returnresult = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (returnresult.ResponseCode == 1 && returnresult.Status == "OK") {
                    if (returnresult.IsValidStatus == 'true') {
                    }
                }
                else { alert("ResponseCode = " + returnresult.ResponseCode + " Status = " + returnresult.Status); }
            },
            error: function (request, status, error) {
            }
        });
    }

    function closedialog() {
        $("#divPop").hide();
    }

    function ShowLoyalty() {
        autoLoad = true;
        $("#dvSearch").addClass("dnone");
        $("#divNext").hide();
        $("#divPrevious").hide();
        $("#content").addClass("dnone");
        $("#divLogin").removeClass("dnone");
        $("#divLogin").show();
        $("#txtEmailID").focus();
        $("#divCreateUser").hide();
        $("#divLoyaltyPoints").hide();
        $("#tblusernotfound").hide();
    }

    function ChkUserExits() {
        var userEmailID = $("#txtEmailID").val();
        if (userEmailID == null || userEmailID == "") {
            resourceText = ("<%=Resources.Resource.RfvLoginIDOrEmailID %>");
            alert(resourceText.replace("'", "\'"));
            return;
        }
        if (validateForm(userEmailID)) {
        }
        else
            return;

        $.ajax({
            type: "POST",
            url: "Kitchen.aspx/CheckUserExits",
            data: "{userEmailID:'" + userEmailID + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var returnData = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (returnData.ResponseCode == 1 && returnData.Status == "OK") {
                    $("#divLogin").hide();
                    $("#divCreateUser").hide();
                    $("#hdnEmailID").val(userEmailID);
                    $("#divLoyaltyPoints").show();
                    $("#ltrLPName").html(returnData.LPName);
                    $("#ltrLPEmailID").html(returnData.LPEmail);
                    $("#ltrLPPhone").html(returnData.LPPhone);
                    $("#ltrLPSinceDate").html(returnData.LPCreatedDate);
                    $("#ltrLPSaleDate").html(returnData.LPLastSaleDate);
                    $("#ltrPointsValues").html(Number(returnData.AvlLP));
                    $("#ltrAddress").html(returnData.Address);
                    $("#txtPrice").focus();
                }
                else if (returnData.ResponseCode == -1) {
                    $("#hdnEmailID").val(userEmailID);
                    $("#divLogin").show();
                    $("#tblusernotfound").show();
                    resourceText = ("<%=Resources.Resource.msgProvidedEmailIDDoesNotExists%>");
                    $("#ltrErrorMsg").html(resourceText.replace("'", "\'"));
                    $("#divCreateUser").hide();
                    $("#divLoyaltyPoints").hide();
                }
                else { alert(FormatUserErrorMessage(returnData.ResponseCode)); }
            },
            error: function (xhr, err) {
                alert(FormatUserErrorMessage("-3") + " \n " + FormatErrorMessage(xhr, err));
            }
        });
    }

    function userRegistration() {
        $("#divCreateUser").show();
        $("#txtUserEmailID").val($("#hdnEmailID").val());
        $("#txtUserEmailID").focus();
        $("#divLogin").hide();
        $("#divLoyaltyPoints").hide();
    }

    function validateForm(emailID) {
        var x = emailID;
        var atpos = x.indexOf("@");
        var dotpos = x.lastIndexOf(".");
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
            var msg = "Not a valid e-mail address ";
            if (currentAppLanguageCode == "fr")
                msg = "Pas une adresse e-mail valide";
            alert(msg);
            return false;
        }
        else
            return true;
    }

    $('.validatenumeric').keyup(function () {
        var val = $(this).val();
        var orignalValue = val;
        //var msg = "Only Integer Values allowed.";
        if (isNaN(val)) {
            orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
            $(this).val(orignalValue);
            resourceText = ("<%=Resources.Resource.msgPOItemPriceNumCheck%>");
            alert(resourceText.replace("'", "\'"));
        }

    });


    function FunUserRegistration() {
        var txtSignUpName = $("#txtUserName").val();
        var txtEmailID = $("#txtUserEmailID").val();
        var txtAddress = $("#txtUserAddress").val();
        var txtSignUpPassword = "";
        var txtConfirmPassword = "";
        var txtSignUpPhone = $("#txtPhone").val();
        if (txtEmailID == null || txtEmailID == "") {
            resourceText = ("<%=Resources.Resource.RfvLoginIDOrEmailID %>");
            alert(resourceText.replace("'", "\'"));
            return;
        }
        if (txtSignUpName == null || txtSignUpName == "") {
            resourceText = ("<%=Resources.Resource.RfvName %>");
            alert(resourceText.replace("'", "\'"));
            return;
        }

        if (txtSignUpPhone == null || txtSignUpPhone == "") {
            resourceText = ("<%=Resources.Resource.RfvPhone %>");
            alert(resourceText.replace("'", "\'"));
            return;
        }

        if (validateForm(txtEmailID)) {
        }
        else
            return;

        $.ajax({
            type: "POST",
            url: "../Restaurant-Menu/Restaurant-Menu.asmx/SignUp",
            data: "{name:'" + txtSignUpName + "',logInID:'" + txtEmailID + "',password:'" + txtSignUpPassword + "',phone:'" + txtSignUpPhone + "',address:'" + txtAddress + "',city:'',postalCode:'',state:'',country:''}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var returnData = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (returnData.ResponseCode == 1 && returnData.Status == "OK") {
                    $("#divCreateUser").hide();
                    $("#hdnEmailID").val(txtEmailID);
                    $("#divLoyaltyPoints").show();
                    $("#txtPrice").focus();
                    ShowPoints();
                }
                else if (returnData.ResponseCode == -1) {
                    resourceText = ("<%=Resources.Resource.EmailIDAlreadyExists%>");
                    alert(resourceText.replace("'", "\'"));
                }
                else { alert(FormatUserErrorMessage(returnData.ResponseCode)); }
            },
            error: function (xhr, err) {
                alert(FormatUserErrorMessage("-3") + " \n " + FormatErrorMessage(xhr, err));
            }
        });
    }

    function CancelButton() {
        $("#divCreateUser").hide();
        $("#divLoyaltyPoints").hide();
        $("#divLogin").show();
        $("#tblusernotfound").hide();
        $("#txtEmailID").val("");
    }

    function SaveLoyalPoints(loyalTypeValue) {
        var txtSalePrice = $("#txtPrice").val();
        var loyalType = loyalTypeValue;
        if (txtSalePrice == null || txtSalePrice == "" || Number(txtSalePrice) <= 0) {
            resourceText = ("<%=Resources.Resource.msgPOItemPriceNumCheck%>");
            alert(resourceText.replace("'", "\'"));
            return;
        }
        var userEmailID = $("#hdnEmailID").val();

        $.ajax({
            type: "POST",
            url: "Kitchen.aspx/SaveLPoint",
            data: "{txtPrice:'" + txtSalePrice + "',LPType:'" + loyalType + "',userEmailID:'" + userEmailID + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var returnData = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (returnData.ResponseCode == 1 && returnData.Status == "OK") {
                    if (returnData.IsValidStatus == "false") {
                        alert('<%=Resources.Resource.lblNotEnoughPoint %>');
                    }
                    else if (loyalType == 'P') {
                        alert('<%=Resources.Resource.lblPointsAddeddSuccessfully %>');
                    }
                    else {
                        alert('<%=Resources.Resource.lblPointsRedeemedSuccessfully %>');
                    }
                    ShowPoints();
                }
                else if (returnData.ResponseCode == -1) {
                    alert("Points Not Added.");
                }
                else { alert(FormatUserErrorMessage(returnData.ResponseCode)); }
            },
            error: function (xhr, err) {
                alert(FormatUserErrorMessage("-3") + " \n " + FormatErrorMessage(xhr, err));
            }
        });
    }

    function ShowPoints() {
        var userEmailID = $("#hdnEmailID").val();

        $.ajax({
            type: "POST",
            url: "Kitchen.aspx/AvailableLP",
            data: "{userEmailID:'" + userEmailID + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var returnData = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (returnData.ResponseCode == 1 && returnData.Status == "OK") {
                    $("#ltrLPName").html(returnData.LPName);
                    $("#ltrLPEmailID").html(returnData.LPEmail);
                    $("#ltrLPPhone").html(returnData.LPPhone);
                    $("#ltrLPSinceDate").html(returnData.LPCreatedDate);
                    $("#ltrLPSaleDate").html(returnData.LPLastSaleDate);
                    $("#ltrPointsValues").html(Number(returnData.AvlLP));
                }
                else if (returnData.ResponseCode == -1) {
                    alert("Points Not Calculated.");
                }
                else { alert(FormatUserErrorMessage(returnData.ResponseCode)); }
            },
            error: function (xhr, err) {
                alert(FormatUserErrorMessage("-3") + " \n " + FormatErrorMessage(xhr, err));
            }
        });
    }

    function CloseLoyalPoint() {
        autoLoad = false;
        $('#txtConfirmCode').val(''); IsOnHold = false; displayRecordFrom = 0;
        AjaxCallForCount();
        $("#dvSearch").removeClass("dnone");
        $("#content").removeClass("dnone");
        $("#divLogin").addClass("dnone");
        $("#divLogin").hide();
        $("#divCreateUser").hide();
        $("#divLoyaltyPoints").hide();
        $("#tblusernotfound").hide();
    }

    function RegisterInfo() {

        $.ajax({
            type: "POST",
            url: "../Restaurant-Menu/Restaurant-Menu.asmx/CompanyInfo",
            data: "{RegCode:'HIT'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var returnData = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (returnData.ResponseCode == 1 && returnData.Status == "OK") {

                    if (returnData.Tel != "") {
                        sRegisterInfo = returnData.Tel;
                    }
                }
                else if (returnData.ResponseCode == -1) {
                    alert("Register infomartion not getting.");
                }
                else { alert(FormatUserErrorMessage(returnData.ResponseCode)); }
            },
            error: function (xhr, err) {
                alert(FormatUserErrorMessage("-3") + " \n " + FormatErrorMessage(xhr, err));
            }
        });
    }

</script>
</html>

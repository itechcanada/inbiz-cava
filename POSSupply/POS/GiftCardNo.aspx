﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="GiftCardNo.aspx.cs" Inherits="POS_GiftCardNo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="dialogAssignUser" style="padding: 5px;">
<%--        <h3>
            <asp:Label ID="Label1" Text="<%$Resources:Resource, btnAddNewColor%>" runat="server" />
        </h3>--%>
        <table>
            <tr>
                <td>
                    <%= Resources.Resource.lblGiftCardNo%> 
                </td>
                <td>
                    <asp:TextBox ID="txtGiftCardNo" runat="server" MaxLength="20" Width="200" />&nbsp;<font style='color:red'>*</font>
                    <asp:RequiredFieldValidator ID="reqvalManagerCode" ErrorMessage="<%$ Resources:Resource, reqMsgGiftCardNo%>" EnableClientScript="true"
                        runat="server" ControlToValidate="txtGiftCardNo" Display="None" SetFocusOnError="true"
                        ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
    </div>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, Ok%>" runat="server" OnClick="btnSave_Click" ValidationGroup="RegisterUser" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
            OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
</asp:Content>



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;

using Trirand.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;


public partial class POS_ProductSearchModal : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MessageState.SetGlobalMessage(MessageType.Success, "");
        txtSearch.Text = Request.QueryString["keyword"] != null ? Request.QueryString["keyword"] : string.Empty;
    }
    protected void grdProducts_CellBinding(object sender, JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 5)
        {
            //e.CellHtml = CurrencyFormat.GetReplaceCurrencySymbol( string.Format("{0:F}",BusinessUtility.GetCurrencyString( _lTotal.GrandTotal,Globals.CurrentCultureName)));
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(e.CellHtml), "");// (string.Format("{0:F}", BusinessUtility.GetCurrencyString(_lTotal.GrandTotal, Globals.CurrentCultureName)));
        }
    }
    protected void grdProducts_DataRequesting(object sender, JQGridDataRequestEventArgs e)
    {
        string strSQL = "";
        if (this.GetSearchText() == "%")
        {
            strSQL = "";
        }
        else
        {
            strSQL = "SELECT distinct products.productID,  products.prdUPCCode,  Pdes.prdname as prdname,/*prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty, prdTaxCode, */prdEndUserSalesPrice,prdIsKit,prdSmallImagePath, prdLargeImagePath, pColor.Color{0} AS Color, pSize.Size{0} AS Size  FROM products ";
            strSQL += " INNER JOIN ProductClothDesc AS pClothDesc ON pClothDesc.ProductID = products.ProductID ";
            strSQL += " INNER JOIN ProductColor AS pColor on pColor.ColorID = pClothDesc.Color ";
            strSQL += " INNER JOIN ProductSize AS pSize ON pSize.SizeID = pClothDesc.Size ";
            strSQL += " /* Left join prdQuantity on prdQuantity.PrdID=Products.ProductID */";
            strSQL += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID" ;
            strSQL += " left join prdImages on prdImages.PrdID=Products.ProductID  ";
            strSQL += " where descLang =@descLang  and prdIsActive=1 AND (Pdes.prdname LIKE @SearchKey OR prdUPCCode LIKE @SearchKey)"; //and prdWhsCode=@prdWhsCode
            strSQL += " GROUP BY prdname,Color,Size ORDER BY 3";
        }
        if (Request.QueryString.AllKeys.Contains("_history"))
        {
            sqldsProcess.SelectParameters.Add("@descLang", DbType.String, Globals.CurrentAppLanguageCode);
            //sqldsProcess.SelectParameters.Add("@prdWhsCode", DbType.String, this.WarehouseCode);
            sqldsProcess.SelectParameters.Add("@SearchKey", DbType.String, this.GetSearchText());
            strSQL = string.Format(strSQL, Globals.CurrentAppLanguageCode);
            sqldsProcess.SelectCommand = strSQL;
        }
        else
        {
            sqldsProcess.SelectParameters.Add("@descLang", DbType.String, Globals.CurrentAppLanguageCode);
            //sqldsProcess.SelectParameters.Add("@prdWhsCode", DbType.String, this.WarehouseCode);
            sqldsProcess.SelectParameters.Add("@SearchKey", DbType.String, this.GetSearchText());
            strSQL = string.Format(strSQL, Globals.CurrentAppLanguageCode);
            sqldsProcess.SelectCommand = strSQL;
        }
    }

    public string WarehouseCode
    {
        get
        {
            if (BusinessUtility.GetString(Session["RegWhsCode"]) != "")
            {
                return BusinessUtility.GetString(Session["RegWhsCode"]);
            }
            else
            {
                return CurrentUser.UserDefaultWarehouse;
            }
        }
    }

    private string GetSearchText()
    {
        string search = string.Empty;
        if (Request.QueryString.AllKeys.Contains("_history"))
        {
            if (!string.IsNullOrEmpty(Request.QueryString[txtSearch.ClientID]))
            {
                search = "%" + Request.QueryString[txtSearch.ClientID] + "%";
                return search;
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                search = "%" + txtSearch.Text + "%";
                return search;
            }
        }
        return "%";
    }
    protected void grdProducts_RowSelecting(object sender, JQGridRowSelectEventArgs e)
    {

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class POS_InstoreCredit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                MessageState.SetGlobalMessage(MessageType.Success, "");
                AvailableCredit();
                txtDiscount.Focus();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (BusinessUtility.GetDouble(txtDiscount.Text) > BusinessUtility.GetDouble(hdnAvlCrdt.Value))
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.ErrMessageNotAvailableCredit);
                return;
            }
            //SysRegister objSysRegister = new SysRegister();
            //if (objSysRegister.ValidateMangerCode(BusinessUtility.GetString(Session["RegCode"]), txtDiscount.Text) == true)
            //{
            Globals.RegisterCloseDialogScript(Page, string.Format("parent.ApplyInStoreCredit({0});", txtDiscount.Text), true);
            //}
            //else
            //{
            //    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblInvalidManageCode);
            //}
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected void AvailableCredit()
    {
        CustomerCredit objCustCrdt = new CustomerCredit();
        double dblAvlCrdt = BusinessUtility.GetDouble(objCustCrdt.GetAvailableCredit(this.CustID));
        dblAvlCrdt = dblAvlCrdt - AmmountAdded;
        valAvailCredit.Text = CurrencyFormat.GetCompanyCurrencyFormat(dblAvlCrdt);
        //txtDiscount.Text = BusinessUtility.GetString(dblAvlCrdt);
        //hdnAvlCrdt.Value = BusinessUtility.GetString(dblAvlCrdt);
        txtDiscount.Text = string.Format("{0:F}", dblAvlCrdt);
        hdnAvlCrdt.Value = string.Format("{0:F}", dblAvlCrdt);
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }


    private int CustID
    {
        get
        {
            int invID = 0;
            int.TryParse(Request.QueryString["custID"], out invID);
            return invID;
        }
    }

    private double AmmountAdded
    {
        get
        {
            return BusinessUtility.GetDouble(Request.QueryString["amountAdded"]);
        }
    }
}
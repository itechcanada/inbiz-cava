﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;

using System.Web.Configuration;

using iTECH.Library.Utilities;
using System.Net;
using System.Web.Services;
using Newtonsoft.Json;
using iTECH.WebService.Utility;
using iTECH.Library.DataAccess.MySql;

using System.Web.Script.Services;

public partial class POS_NewKitchen : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Session["LoginID"] == null) || (Session["UserModules"] == null)) // || string.IsNullOrEmpty(Session["UserModules"])
        {
            Response.Redirect("~/AdminLogin.aspx");
        }
       // SessionStateSection sessionSection = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
       //Response.Write(sessionSection.Timeout.TotalSeconds.ToString());
    }
    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    [System.Web.Services.WebMethod]
    public static string DeleteOrder(string ordID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            CategoryWithProduct cwp = new CategoryWithProduct();
            cwp.DeleteOrder(null, BusinessUtility.GetInt(ordID));
            dictResult.OkStatus(ResponseCode.SUCCESS);
            dictResult.Add("IsValidStatus", "true");
        }
        catch (Exception ex)
        {
            dictResult.Add("IsValidStatus", "false");
        }
        return dictResult.ResultInInJSONFormat;
    }

    [System.Web.Services.WebMethod]
    public static string CheckUserExits(string userEmailID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            Partners partner = new Partners();
            if (partner.CheckEmailIDExists(null, userEmailID))
            {
                partner.PopulateObject(null, BusinessUtility.GetString(userEmailID));
                LoyalPartnerHistory plh = new LoyalPartnerHistory();
                int userInHandPoint = BusinessUtility.GetInt(plh.GetPartnerLoyaltyPoints(null, BusinessUtility.GetInt(partner.PartnerID)));
                DateTime lastSaleDate = partner.GetRecentPOSTransactionDate(null, BusinessUtility.GetInt(partner.PartnerID));
                dictResult.OkStatus(ResponseCode.SUCCESS);
                dictResult.Add("LPName", partner.PartnerLongName.ToString());
                dictResult.Add("LPEmail", partner.PartnerEmail);
                dictResult.Add("LPPhone", partner.PartnerPhone);
                dictResult.Add("LPCreatedDate", partner.PartnerCreatedOn.ToString("yyyy-MM-dd"));
                dictResult.Add("LPLastSaleDate", lastSaleDate.ToString("yyyy-MM-dd"));
                dictResult.Add("AvlLP", userInHandPoint.ToString());
                dictResult.Add("Address", partner.GetShipToAddress(null, partner.PartnerID, AddressReference.CUSTOMER_CONTACT).AddressLine1);
            }
            else
            {
                throw new Exception(CustomExceptionCodes.DUPLICATE_KEY);
            }

        }
        catch (Exception ex)
        {
            dictResult.ErrorStatus(ResponseCode.UNSUCCESS);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [System.Web.Services.WebMethod]
    public static string SaveLPoint(string txtPrice, string LPType, string userEmailID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            SysWarehouses _whs = new SysWarehouses();
            Partners part = new Partners();
            LoyalPartnerHistory lph = new LoyalPartnerHistory();
            int calculatedPoint = 0;
            part.PopulateObject(null, BusinessUtility.GetString(userEmailID));
            double amtPerPoint = 0.0;

            if (LPType == "M")
            {
                amtPerPoint = _whs.GetLoyalAmtPerPoint(System.Configuration.ConfigurationManager.AppSettings["WebSaleWhsCode"]);
            }
            else
            {
                amtPerPoint = _whs.GetAmtPerPoint(System.Configuration.ConfigurationManager.AppSettings["WebSaleWhsCode"]);
            }

            if (amtPerPoint > 0)
            {
                calculatedPoint = BusinessUtility.GetInt(BusinessUtility.GetDouble(txtPrice) / amtPerPoint);
            }
            if (LPType == "M")
            {
                int userInHandPoint = BusinessUtility.GetInt(lph.GetPartnerLoyaltyPoints(null, BusinessUtility.GetInt(part.PartnerID)));
                if ((userInHandPoint >= _whs.MinPointsForRedemption) && (userInHandPoint >= calculatedPoint))
                {
                    dictResult.Add("IsValidStatus", "true");
                    calculatedPoint = (-1 * calculatedPoint);
                }
                else
                {
                    dictResult.OkStatus(ResponseCode.SUCCESS);
                    dictResult.Add("IsValidStatus", "false");
                    return dictResult.ResultInInJSONFormat;
                }

            }

            lph.LoyalCategoryID = part.PartnerLoyalCategoryID;
            lph.LoyalPartnerID = part.PartnerID;
            lph.PointAuto = false;
            lph.Points = calculatedPoint;
            lph.PointsAddedBy = CurrentUser.UserID;
            lph.PointsAddedOn = DateTime.Now;
            lph.PosTransactionID = 0;
            lph.Insert(null);
            dictResult.OkStatus(ResponseCode.SUCCESS);
        }
        catch (Exception ex)
        {
            dictResult.ErrorStatus(ResponseCode.UNSUCCESS);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [System.Web.Services.WebMethod]
    public static string AvailableLP(string userEmailID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            Partners part = new Partners();
            part.PopulateObject(null, BusinessUtility.GetString(userEmailID));
            LoyalPartnerHistory plh = new LoyalPartnerHistory();
            int userInHandPoint = BusinessUtility.GetInt(plh.GetPartnerLoyaltyPoints(null, BusinessUtility.GetInt(part.PartnerID)));
            DateTime lastSaleDate = part.GetRecentPOSTransactionDate(null, BusinessUtility.GetInt(part.PartnerID));
            dictResult.OkStatus(ResponseCode.SUCCESS);
            dictResult.Add("LPName", part.PartnerLongName.ToString());
            dictResult.Add("LPEmail", part.PartnerEmail);
            dictResult.Add("LPPhone", part.PartnerPhone);
            dictResult.Add("LPCreatedDate", part.PartnerCreatedOn.ToString("MM-dd-yyyy"));
            dictResult.Add("LPLastSaleDate", lastSaleDate.ToString("MM-dd-yyyy"));
            dictResult.Add("AvlLP", userInHandPoint.ToString());
        }
        catch (Exception ex)
        {
            dictResult.ErrorStatus(ResponseCode.UNSUCCESS);
        }
        return dictResult.ResultInInJSONFormat;
    }
}
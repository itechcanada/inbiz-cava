<%@ Page Language="VB" MasterPageFile="~/POSMaster.master" AutoEventWireup="false"
    CodeFile="Refund.aspx.vb" Inherits="POS_Refund" %>

<%@ Import Namespace="Resources.Resource" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <%--    <script type="text/javascript" src='<%=ResolveUrl("~/lib/scripts/jquery-plugins/JqGridHelper2.js")%>'></script>--%>
    <style type="text/css">
        .ui-widget-content a
        {
            color: #185375;
        }
        .ui-jqgrid tr.jqgrow td
        {
            height: 40px;
            font-size: 15px;
        }
        .ui-jqgrid tr.jqgrow td a
        {
            font-size: 15px;
        }
        
        .ui-widget
        {
            font-size: 0.9em;
        }
    </style>
    <% Dim objCommon1 As New clsCommon
        Dim objCompany As New clsCompanyInfo
        objCompany.GetCompanyAddress()
    %>
    <script type="text/javascript" language="JavaScript" src="itech.js"></script>
    <script language="javascript" type="text/javascript">
        function ShowStatus(status) {
            alert("Selected order is in " + status + " status. To continue, change the order status to READY. ");
        }
        function updateTransaction(sDATA) {
            var query = "data=" + sDATA;
            var sErrMsg = "Error! slide-popup " + sURL + " Qry:" + query;
            var isAsync = false;
            var sURL = "./data.aspx"
            calliAJAX(sURL, query, sErrMsg, isAsync);
            var URl = document.getElementById('<%=hdnPrintUrl.ClientID%>').value;
            return URl;
        }

        function getPrintUrl() {

            return document.getElementById('<%=hdnPrintUrl.ClientID%>').value;
        }

        function transactionStatus(msg) {
            document.getElementById('<%=divTransaction.ClientID%>').style.display = "none";
            location.href = "Refund.aspx?TType=R&Msg=" + msg;
        }

        function funCloseTransaction() {
            document.getElementById('<%=divTransaction.ClientID%>').style.display = "none";
            document.getElementById('<%=lblTranMsg.ClientID%>').innerHTML = '';
        }

        var secReg = '<%= Session("RegCode") %>';
        var secRegName = '<%= Session("RegName") %>';
        var secMerchantID = '<%= ConfigurationManager.AppSettings("MerchantID").ToLower %>';
        var secWareHouse = '<%= Session("UserWarehouse") %>';
        var secHost = '<%= Session("host") %>';
        var secHostPort = '<%= Session("hostPort") %>';
        var secTerminalID = '<%= Session("terminalID") %>';
        var secUserId = '<%= Session("UserID") %>';
        var secNoReceipt = '<%= ConfigurationManager.AppSettings("IsReceipt").ToLower %>';
        var secRegLogFile = '<%= Session("RegLogFilePath") %>';
        var secLang = '<%= objCommon1.funGetProductLang() %>';
        var secRegMsg = '<%= Session("RegMessage").ToString().Replace(vbNewLine, "__nl__") %>';
        var secCmpName = '<%= objCompany.CompanyName %>';
        var secCmpPhone = '<%= objCompany.CompanyPOPhone %>';
        var secCmpAddress = '<%= objCompany.CompanyAddressLine1 & "__nl__" & objCompany.CompanyCity & " " & objCompany.CompanyState & "__nl__" & objCompany.CompanyCountry & " " & objCompany.CompanyPostalCode %>';
    </script>
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblHeading"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                </td>
                <td style="width: 150px;">
                </td>
            </tr>
        </table>
    </div>
    <div class="divMainContent">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="10">
                </td>
            </tr>
            <% If Request.QueryString("TType") = "R" Then%>
            <tr>
                <td align="left" colspan="2">
                    <asp:Panel runat="server" ID="pnlSearch" DefaultButton="imgSearch">
                        <table id="Table1" cellpadding="1" cellspacing="1" border="0" width="410px" runat="server">
                            <tr>
                                <td align="left">
                                    <asp:Label CssClass="lblBold" runat="server" ID="lblTranID" Text="<%$ Resources:Resource, POSRReceipt %>"> </asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label CssClass="lblBold" runat="server" ID="lblSearch" Text="<%$ Resources:Resource, POSRPrdUpccode %>"></asp:Label>
                                </td>
                                <td align="left" style="width: 25px;">
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:TextBox runat="server" onKeyup="return funEnablePaymentBy(2);" onclick="return funEnablePaymentBy(2);"
                                        CssClass="innerText" Width="155px" ID="txtTranID"></asp:TextBox>
                                </td>
                                <td align="left">
                                    <asp:TextBox runat="server" onKeyup="return funEnablePaymentBy(2);" onclick="return funEnablePaymentBy(2);"
                                        CssClass="innerText" Width="195px" ID="txtUpcCode"></asp:TextBox>
                                </td>
                                <%--<td align="left" width="18%">
                                    <asp:TextBox runat="server" onKeyup="return funEnablePaymentBy(2);" onclick="return funEnablePaymentBy(2);"
                                        CssClass="innerText" Width="195px" ID="txtProductTag"></asp:TextBox>
                                </td>--%>
                                <td align="left" style="width: 20px;">
                                    <asp:ImageButton ID="imgSearch" runat="server" AlternateText="Search" CausesValidation="false"
                                        ImageUrl="../images/search-btn.gif" />
                                </td>
                                <%--<td align="right" style="padding-right: 15px;">
                                    <div style="height: 22px">
                                    </div>
                                </td>--%>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <% If (Session("UserModules").ToString.Contains("ADM") = True Or Session("UserModules").ToString.Contains("PSMGR") = True) Then%>
            <tr>
                <td height="20" style="padding-left: 150px;" colspan="2">
                    <asp:Label ID="Label1" Text="<%$ Resources:Resource, POSOR %>" CssClass="lblBold"
                        runat="server" />&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td height="10">
                </td>
            </tr>
            <tr>
                <td style="padding-left: 5px;" colspan="2">
                    <asp:Label ID="lblRefundAmount" Text="<%$ Resources:Resource, POSRefundAmount %>"
                        CssClass="lblBold" runat="server" />&nbsp;&nbsp;
                    <asp:TextBox ID="txtRefundAmt" onclick="return funEnablePaymentBy(1);" onKeyup="return funEnablePaymentBy(1);"
                        Style="text-align: right;" runat="server" Width="150PX"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td height="10">
                </td>
            </tr>
            <% End If%>
            <tr>
                <td class="tdAlignLeft" colspan="2">
                    <asp:Label ID="lblProIL" CssClass="lblBold" Text="Product Item List" runat="server"
                        Visible="false" /><br />
                    <asp:GridView ID="grdRequest" runat="server" AllowSorting="False" AllowPaging="false"
                        PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0" GridLines="none" AutoGenerateColumns="False"
                        Style="border-collapse: separate;" CssClass="view_grid650" UseAccessibleHeader="False"
                        DataKeyNames="ProductID" Width="100%" ShowFooter="True">
                        <Columns>
                            <asp:BoundField DataField="ProductID" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdPoProductID %>"
                                ReadOnly="True" SortExpression="ProductID" Visible="false">
                                <ItemStyle Width="70px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="UPCCode" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdPOSUPC%>"
                                ReadOnly="True" SortExpression="UPCCode">
                                <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ProductName" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdPOProductName %>"
                                ReadOnly="True" SortExpression="ProductName">
                                <ItemStyle Width="250px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="right" HeaderText="<%$ Resources:Resource, grdPOSQty %>"
                                HeaderStyle-ForeColor="#ffffff">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtPQtyToRefund" MaxLength="10" Text='<%# Eval("Quantity", "{0}")%>'
                                        runat="server" Width="25px"></asp:TextBox>/<%# Eval("Quantity", "{0}")%><asp:TextBox
                                            ID="txtHdnPQty" Text='<%# Eval("Quantity", "{0}")%>' runat="server" Style="display: none;"></asp:TextBox>
                                    <asp:CompareValidator ID="cmpQty" ControlToValidate="txtPQtyToRefund" ControlToCompare="txtHdnPQty"
                                        ErrorMessage="Invalid refund quantity!" Operator="LessThanEqual" Type="Double"
                                        ValidationGroup="refamt" SetFocusOnError="true" Display="None" runat="server" />
                                    <asp:ValidationSummary ID="vsPQtyRef" ShowMessageBox="true" ShowSummary="false" ValidationGroup="refamt"
                                        runat="server" />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Button ID="btnUpdateCart" Text="Update Refund Amount" CommandName="UpdateRefund"
                                        ValidationGroup="refamt" runat="server" />
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Right" />
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="right" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="Quantity" HeaderStyle-HorizontalAlign="right" HeaderText="<%$ Resources:Resource, grdPOSQty %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff" Visible="false">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Price" HeaderStyle-HorizontalAlign="right" HeaderText="<%$ Resources:Resource, grdPOPrdCostPrice %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TotalPrice" HeaderStyle-HorizontalAlign="right" DataFormatString="{0:F}"
                                HeaderText="<%$ Resources:Resource, grdPOSTotalPrice %>" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdEdit %>" Visible="false"
                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEditIL" runat="server" CommandArgument='<%# Eval("ProductID") %>'
                                        CommandName="Edit" CausesValidation="false" ImageUrl="~/images/edit_icon.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField Visible="false" HeaderText="<%$ Resources:Resource, grdDelete %>"
                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDeleteIL" runat="server" ImageUrl="~/images/delete_icon.png"
                                        CausesValidation="false" CommandArgument='<%# Eval("ProductID") %>' CommandName="Delete" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="grid_footer" />
                        <RowStyle CssClass="grid_rowstyle" />
                        <PagerStyle CssClass="grid_footer" />
                        <HeaderStyle CssClass="grid_header" Height="26px" />
                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                        <PagerSettings PageButtonCount="20" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqldsRequest" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td height="10">
                </td>
            </tr>
            <tr>
                <td width="64%">
                    <asp:HiddenField runat="server" ID="hdnPrintUrl"></asp:HiddenField>
                    <table id="Table3" width="100%" runat="server" border="0">
                        <%--Credit card transaction--%>
                        <tr runat="server" id="trTransaction" visible="false">
                            <td colspan="3">
                                <div id="divTransaction" runat="server" align="left" style="position: relative; left: 170px;
                                    top: 133px; border: 3px; border-color: red; margin: 0; height: 0;">
                                    <asp:Panel runat="server" ID="pnlTransaction" DefaultButton="imgCardSubmit">
                                        <table cellpadding="0" class="popupTable" style="border-color: #CD9232;" cellspacing="0"
                                            width="350px" height="150">
                                            <tr style="background-color: Gold; height: 40px; border-color: Red; border: 1px;
                                                border-style: solid;">
                                                <td width="97%" align="center">
                                                    <asp:Label CssClass="lblHeading" runat="server" ID="lblNote2" Text="<%$ Resources:Resource, lblCardInfo %>"></asp:Label>
                                                </td>
                                                <td width="3%" align="right" style="padding-right: 4px;">
                                                    <a name="Close" href="javascript:funCloseTransaction();">
                                                        <img border="0" src="../Images/closePrd.png" alt="<% = cmdPOSClose %>" /></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="10">
                                                </td>
                                            </tr>
                                            <tr id="Tr1" runat="server" visible="false">
                                                <td align="center" colspan="2">
                                                    <asp:TextBox runat="server" ID="txtCardInfo" ValidationGroup="Card" TextMode="password"
                                                        Width="320px"></asp:TextBox><span style="color: Red; font-weight: bold; font-size: 11pt;">
                                                            *</span>
                                                    <asp:RequiredFieldValidator ID="reqvalCardInfo" ValidationGroup="Card" runat="server"
                                                        ControlToValidate="txtCardInfo" ErrorMessage="<%$ Resources:Resource, reqvalCardInfo %>"
                                                        SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="10">
                                                    <applet id="idApplet" runat="server" code="applet.client.iTECH_Inbiz.class" archive="iTECH_Inbiz.jar"
                                                        width="200" height="90">
                                                        <param name="host" value="<%= Session("host")%>" />
                                                        <param name="hostPort" value="<%= Session("hostPort")%>" />
                                                        <param name="transactionType" value="<%= Session("transactionType")%>" />
                                                        <param name="amount" value="<%= Double.Parse(Session("amount").ToString())*-1 %>" />
                                                        <param name="transactionID" value="<%= Session("transactionID")%>" />
                                                        <param name="terminalID" value="<%= Session("terminalID")%>" />
                                                        <% If ConfigurationManager.AppSettings("AppletLog").ToString.ToLower = "yes" Then%>
                                                        <param name="log-file" value="<%= Session("RegLogFilePath") %>" />
                                                        <% End If%>
                                                        <% If (Session("UserModules").ToString.Contains("ADM") = True Or Session("UserModules").ToString.Contains("PSMGR") = True) And CDbl(txtRefundAmt.Text) > 0 Then%>
                                                        <param name="isPrint" value="No" />
                                                        <% End If%>
                                                        <param name="isMrcPrint" value="<%= clsRegister.IsPrintMerchantCopry(Session("RegCode").ToString())%>" />
                                                    </applet>
                                                </td>
                                            </tr>
                                            <tr id="Tr2" runat="server" visible="false">
                                                <td align="center" colspan="2">
                                                    <asp:ImageButton runat="server" ID="imgCardSubmit" ValidationGroup="Card" ImageUrl="~/images/submit.png" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="2">
                                                </td>
                                            </tr>
                                            <asp:ValidationSummary ID="valCardInfo" runat="server" ShowMessageBox="true" ValidationGroup="Card"
                                                ShowSummary="false" />
                                        </table>
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="Center">
                                <table cellpadding="0" cellspacing="0" width="350px">
                                    <%--'FFFFE5 height=40px class=tdBorder--%>
                                    <tr>
                                        <%--   <td width='15px' background="../Images/popup_left.png">
                                        </td>--%>
                                        <td align="center" valign="middle" class="popup_title_newbg">
                                            <asp:Label runat="server" ID="Label2" Text="<%$ Resources:Resource, POSAmount %>"></asp:Label>
                                        </td>
                                        <%--  <td width='15px' background="../Images/popup_right.png">
                                        </td>--%>
                                    </tr>
                                </table>
                                <table width="350px" class="popup_content_newbg">
                                    <tr>
                                        <td height="15">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdAlign">
                                            <asp:Label ID="lblRefund" Text="<%$ Resources:Resource, POSRefund %>" Style="font-size: 16px;
                                                font-weight: bold" CssClass="lblBold" runat="server" />
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtRefund" ReadOnly="true" runat="server" Style="font-size: 16px;
                                                font-weight: bold; text-align: right;" MaxLength="20" CssClass="txtBoxPrd" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="15">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="center">
                                <asp:Label ID="lblTranMsg" runat="server" ForeColor="green" Font-Size="10" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="36%" class="titleBgColorPos">
                    <table id="Table2" width="100%" runat="server" border="0">
                        <tr>
                            <td height="15">
                            </td>
                        </tr>
                        <tr>
                            <td width="37%" align="right">
                                <asp:Label ID="lblSubTotal" Text="<%$ Resources:Resource, msgSubTotal %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td width="63%" align="right">
                                <asp:TextBox ID="txtSubTotal" Style="text-align: right;" ReadOnly="true" runat="server"
                                    Width="150PX"></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="trTax1" visible="false">
                            <td align="right">
                                <asp:Label ID="lblTax1" Text="" CssClass="lblBold" runat="server" />
                            </td>
                            <td align="right">
                                <asp:TextBox ID="txtTax1" runat="server" Style="text-align: right;" ReadOnly="true"
                                    Width="150PX"></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="trTax2" visible="false">
                            <td align="right">
                                <asp:Label ID="lblTax2" Text="" CssClass="lblBold" runat="server" />
                            </td>
                            <td align="right">
                                <asp:TextBox ID="txtTax2" runat="server" Style="text-align: right;" ReadOnly="true"
                                    Width="150PX"></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="trTax3" visible="false">
                            <td align="right">
                                <asp:Label ID="lblTax3" Text="" CssClass="lblBold" runat="server" />
                            </td>
                            <td align="right">
                                <asp:TextBox ID="txtTax3" runat="server" Style="text-align: right;" ReadOnly="true"
                                    Width="150PX"></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="trTax4" visible="false">
                            <td align="right">
                                <asp:Label ID="lblTax4" Text="" CssClass="lblBold" runat="server" />
                            </td>
                            <td align="right">
                                <asp:TextBox ID="txtTax4" runat="server" Style="text-align: right;" ReadOnly="true"
                                    Width="150PX"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblTotal" Text="<%$ Resources:Resource, msgTotalPrice %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="right">
                                <asp:TextBox ID="txtTotal" ReadOnly="true" Style="text-align: right;" runat="server"
                                    Width="150PX"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td height="15">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="5">
                    <asp:HiddenField ID="hdnTax" runat="server"></asp:HiddenField>
                </td>
            </tr>
            <tr bgcolor="#FAFAFA">
                <td colspan="2">
                    <table id="Table4" width="100%" runat="server" border="0">
                        <tr>
                            <td width="20%" align="center">
                                <asp:Label ID="lblPaymentBy" Text="<%$ Resources:Resource, POSPaymentBy %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td width="5%">
                            </td>
                            <td align="Left" width="75%">
                                <asp:RadioButton runat="server" onClick="return funCkeck(1);" ID="rbCash" GroupName="Transaction" />
                                <asp:ImageButton runat="server" ID="imgCash" AlternateText="Cash" ImageUrl="~/images/Cash.png" />
                                <asp:RadioButton runat="server" onClick="return funCkeck(2);" ID="rbVisa" GroupName="Transaction" />
                                <asp:ImageButton runat="server" ID="imgVisa" ImageUrl="~/images/Visa.png" />
                                <asp:RadioButton runat="server" onClick="return funCkeck(3);" ID="rbMasterCard" GroupName="Transaction" />
                                <asp:ImageButton runat="server" ID="imgMasterCard" ImageUrl="~/images/MasterCard.png" />
                                <asp:RadioButton runat="server" onClick="return funCkeck(4);" ID="rbAmerican" GroupName="Transaction" />
                                <asp:ImageButton runat="server" ID="imgAmerican" ImageUrl="~/images/American.png" />
                                <asp:RadioButton runat="server" onClick="return funCkeck(5);" ID="rbInterac" GroupName="Transaction" />
                                <asp:ImageButton runat="server" ID="imgInterac" ImageUrl="~/images/interac_logo.gif" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="30">
                <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                    <table width="100%">
                        <tr>
                            <td width="75%">
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="CmdRefund" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                        style="min-width: 80px; text-align: center;">
                                        <%=Resources.Resource.CmdCssRefund%></span></a></div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <asp:HiddenField runat="server" ID="hdnVal" />
            <asp:ValidationSummary ID="ValsRefund" runat="server" ShowMessageBox="true" ShowSummary="false" />
            <% Else%>
            <tr valign="top">
                <td align="left" colspan="2">
                    <asp:Label CssClass="lblBold" runat="server" ID="lblPrdItem" Text=" <%$ Resources:Resource, lblPosTran %>"></asp:Label>
                    <%--   <asp:GridView ID="grdvViewTran" runat="server" AllowSorting="True" DataSourceID="sqlsdviewTran"
                        EmptyDataRowStyle-HorizontalAlign="Center" AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric"
                        CellPadding="0" EmptyDataText="No data Found" Style="border-collapse: separate;"
                        CssClass="view_grid650" GridLines="none" AutoGenerateColumns="False" UseAccessibleHeader="False"
                        DataKeyNames="posTransId" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="posTransId" HeaderStyle-Wrap="false" NullDisplayText="--"
                                HeaderText="<%$ Resources:Resource, grdPOSPauseID %>" ReadOnly="True" SortExpression="posTransId">
                                <ItemStyle Width="120px" Wrap="true" HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PosTabNote" HeaderStyle-Wrap="false" NullDisplayText="--"
                                HeaderText="<%$ Resources:Resource, grdPOSTabNote %>" ReadOnly="True" SortExpression="PosTabNote">
                                <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="posTransDateTime" HeaderStyle-Wrap="false" NullDisplayText="--"
                                HeaderText="<%$ Resources:Resource, grdPOSPauseDate %>" ReadOnly="True" SortExpression="posTransDateTime">
                                <ItemStyle Width="150px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="posSubTotal" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right"
                                NullDisplayText="--" HeaderText="<%$ Resources:Resource, grdPOSPauseSubTotal %>"
                                ReadOnly="True" SortExpression="posSubTotal">
                                <ItemStyle Width="150px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="posTotalValue" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="Right"
                                NullDisplayText="--" HeaderText="<%$ Resources:Resource, grdPOSPauseTotal %>"
                                ReadOnly="True" SortExpression="posTotalValue">
                                <ItemStyle Width="150px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="posTransStatus" HeaderStyle-Wrap="false" NullDisplayText="--"
                                HeaderText="<%$ Resources:Resource, grdPOSPauseStatus %>" ReadOnly="True" SortExpression="posTransStatus">
                                <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="userID" HeaderStyle-Wrap="false" NullDisplayText="--"
                                HeaderText="User ID" ReadOnly="True" SortExpression="posPayTypeDesc">
                                <ItemStyle Width="160px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdvContinue %>" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-ForeColor="white">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("posTransId") %>'
                                        CommandName="Edit" ToolTip="<%$ Resources:Resource, grdvContinue %>" ImageUrl="~/images/arrow_24.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdvDelete %>" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-ForeColor="white">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                        ToolTip="<%$ Resources:Resource, grdvDelete %>" CommandArgument='<%# Eval("posTransId") %>'
                                        CommandName="Delete" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="grid_footer" />
                        <RowStyle CssClass="grid_rowstyle" />
                        <PagerStyle CssClass="grid_footer" />
                        <HeaderStyle CssClass="grid_header" Height="26px" />
                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                        <PagerSettings PageButtonCount="20" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqlsdviewTran" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>--%>
                    <asp:HiddenField runat="server" ID="hdnTranID" />
                    <asp:Panel runat="server" ID="SearchPanel">
                        <div style="display: none;">
                            <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" CssClass="filter-key" />
                            <asp:Label ID="lblConfirmCode" AssociatedControlID="txtConfirmCode" runat="server"
                                CssClass="filter-key" />
                            <asp:TextBox runat="server" Width="180px" ID="txtSearch" />
                            <input id="btnSearch" type="button" value="Go" style="height: 35px;" />
                        </div>
                        <div style="float: right;">
                            <table border="0" cellpadding="5px" cellspacing="5px" style="width: 100%; padding-bottom: 10px;">
                                <tr>
                                    <td style="float: left; min-width: 180px;">
                                        <asp:TextBox runat="server" Width="140px" ID="txtConfirmCode" Height="33px" placeholder=""
                                            Style="float: left;" />
                                        <a id="aGo" class="ovalbutton" style="margin-left: 5px; margin-right: 15px; width: 30px;
                                            float: right;" href="Javascript:;"><span class="ovalbutton">Go</span> </a>
                                    </td>
                                    <td style="float: left; min-width: 150px;">
                                        <a onclick="AssignStatusTypeID('12');" id="aReady" class="ovalbutton" style="margin-right: 15px;"
                                            href="Javascript:;"><span class="ovalbutton">Ready</span> </a>
                                    </td>
                                    <td style="float: left; min-width: 150px;">
                                        <a onclick="AssignStatusTypeID('10');" id="aActive" class="ovalbutton" style="margin-right: 15px;"
                                            href="Javascript:;"><span class="ovalbutton">Active</span> </a>
                                    </td>
                                    <td style="float: left; min-width: 150px;">
                                        <a onclick="AssignStatusTypeID('9');" id="aNew" class="ovalbutton" style="margin-right: 15px;"
                                            href="Javascript:;"><span class="ovalbutton">New</span> </a>
                                    </td>
                                    <td style="float: left; min-width: 150px;">
                                        <a onclick="AssignStatusTypeID('11');" id="aOnHold" class="ovalbutton" style="margin-right: 15px;"
                                            href="Javascript:;"><span class="ovalbutton">On Hold</span> </a>
                                    </td>
                                    <td style="float: left; min-width: 150px;">
                                        <a onclick="RefreshJQGrideView();" class="ovalbutton" style="margin-right: 15px;"
                                            href="Javascript:;"><span class="ovalbutton">Refresh</span> </a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <div style="clear: both;">
                    </div>
                    <div id="grid_wrapper" style="width: 100%;">
                        <trirand:JQGrid runat="server" ID="jgdvViewTran" DataSourceID="sqldsViewTran" Height="600px"
                            AutoWidth="True" OnCellBinding="jgdvViewTran_DataBinding">
                            <Columns>
                                <trirand:JQGridColumn DataField="posTransId" HeaderText="<%$ Resources:Resource, grdPOSPauseID %>"
                                    Editable="false" PrimaryKey="True" Width="60" />
                                <trirand:JQGridColumn DataField="PosTabNote" HeaderText="<%$ Resources:Resource, grdPOSTabNote %>"
                                    Editable="false" Width="150" />
                                <trirand:JQGridColumn DataField="posTransDateTime" HeaderText="<%$ Resources:Resource, grdPOSPauseDate %>"
                                    Editable="false" Width="120" />
                                <trirand:JQGridColumn DataField="posSubTotal" HeaderText="<%$ Resources:Resource, grdPOSPauseSubTotal %>"
                                    Editable="false" Width="75">
                                    <Formatter>
                                        <trirand:CurrencyFormatter DecimalPlaces="2" ThousandsSeparator="," Prefix="$" />
                                    </Formatter>
                                </trirand:JQGridColumn>
                                <trirand:JQGridColumn DataField="posTotalValue" HeaderText="<%$ Resources:Resource, grdPOSPauseTotal %>"
                                    Editable="false" Width="75">
                                    <Formatter>
                                        <trirand:CurrencyFormatter DecimalPlaces="2" ThousandsSeparator="," Prefix="$" />
                                    </Formatter>
                                </trirand:JQGridColumn>
                                <trirand:JQGridColumn DataField="PaidAmout" HeaderText="Paid Amount" Editable="false"
                                    Width="75">
                                    <Formatter>
                                        <trirand:CurrencyFormatter DecimalPlaces="2" ThousandsSeparator="," Prefix="$" />
                                    </Formatter>
                                </trirand:JQGridColumn>
                                <trirand:JQGridColumn DataField="posTransStatus" HeaderText="<%$ Resources:Resource, grdPOSPauseStatus %>"
                                    Editable="false" Width="80" />
                                     <trirand:JQGridColumn DataField="posTransStatus" HeaderText="<%$ Resources:Resource, lblPickedUp %>"
                                    Editable="false" Width="120" />
                                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdvContinue %>" DataField="posTransId"
                                    Sortable="false" TextAlign="Center" Width="100" />
                                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="posTransId"
                                    Sortable="false" TextAlign="Center" Width="50" />
                            </Columns>
                            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                                ShowDeleteButton="false" ShowSearchButton="false" />
                            <SortSettings InitialSortColumn=""></SortSettings>
                            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                            <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" />
                        </trirand:JQGrid>
                        <asp:SqlDataSource ID="sqldsViewTran" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
                        <iCtrl:IframeDialog ID="mdDelProcess" Width="400" Height="120" Title="Delete" Dragable="true"
                            TriggerSelectorClass="pop_delete" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
                            runat="server">
                        </iCtrl:IframeDialog>
                        <iCtrl:IframeDialog ID="ifdStatus" Width="400" Height="120" Title="Change Status" Dragable="true"
                            TriggerSelectorClass="popEditStatus" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
                            runat="server">
                        </iCtrl:IframeDialog>
                    </div>
                </td>
            </tr>
            <% End If%>
        </table>
        <br />
    </div>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            var resourceText1 = ("<%=Resources.Resource.grdPOSTabNote %>");
            $('#<%=txtConfirmCode.ClientID %>').attr("placeholder", resourceText1.replace("'", "\'"));


            $("#aGo").click(function () { $("#btnSearch").trigger('click'); });


            $("#aReady").removeClass("ovalbutton").addClass("ovallightgreenbutton");
            $("#aActive").removeClass("ovallightgreenbutton").addClass("ovalbutton");
            $("#aNew").removeClass("ovallightgreenbutton").addClass("ovalbutton");
            $("#aOnHold").removeClass("ovallightgreenbutton").addClass("ovalbutton");
            $("#aReady").click(function () {
                $("#aReady").removeClass("ovalbutton").addClass("ovallightgreenbutton");
                $("#aActive").removeClass("ovallightgreenbutton").addClass("ovalbutton");
                $("#aNew").removeClass("ovallightgreenbutton").addClass("ovalbutton");
                $("#aOnHold").removeClass("ovallightgreenbutton").addClass("ovalbutton");
            });




            $("#aActive").click(function () {
                $("#aActive").removeClass("ovalbutton").addClass("ovallightgreenbutton");
                $("#aReady").removeClass("ovallightgreenbutton").addClass("ovalbutton");
                $("#aNew").removeClass("ovallightgreenbutton").addClass("ovalbutton");
                $("#aOnHold").removeClass("ovallightgreenbutton").addClass("ovalbutton");
            });
            $("#aNew").click(function () {
                $("#aNew").removeClass("ovalbutton").addClass("ovallightgreenbutton");
                $("#aReady").removeClass("ovallightgreenbutton").addClass("ovalbutton");
                $("#aActive").removeClass("ovallightgreenbutton").addClass("ovalbutton");
                $("#aOnHold").removeClass("ovallightgreenbutton").addClass("ovalbutton");
            });
            $("#aOnHold").click(function () {
                $("#aOnHold").removeClass("ovalbutton").addClass("ovallightgreenbutton");
                $("#aReady").removeClass("ovallightgreenbutton").addClass("ovalbutton");
                $("#aActive").removeClass("ovallightgreenbutton").addClass("ovalbutton");
                $("#aNew").removeClass("ovallightgreenbutton").addClass("ovalbutton");
            });
        });
        function prompter(arguments) {
            //            alert(1);
            //            if (arguments=='1')
            //               {
            //                   if (document.getElementById('<%=rbCash.ClientID%>').disabled==false)
            //                   {
            //                        var hdn1 = document.getElementById('<%=hdnVal.ClientID%>'); 
            //                        hdn1.value="0";
            //                   }
            //               }
            //  confirm('Are you sure, Refund amount is correct?');
        }

        function funEnablePaymentBy(arguments) {
            if (arguments == "1") {
                if (document.getElementById('<%=txtRefundAmt.ClientID%>').disabled == false) {
                    document.getElementById('<%=rbCash.ClientID%>').disabled = false;
                    document.getElementById('<%=imgCash.ClientID%>').disabled = false;
                    document.getElementById('<%=rbVisa.ClientID%>').disabled = false;
                    document.getElementById('<%=imgVisa.ClientID%>').disabled = false;
                    document.getElementById('<%=rbMasterCard.ClientID%>').disabled = false;
                    document.getElementById('<%=imgMasterCard.ClientID%>').disabled = false;
                    document.getElementById('<%=rbAmerican.ClientID%>').disabled = false;
                    document.getElementById('<%=imgAmerican.ClientID%>').disabled = false;
                    document.getElementById('<%=rbInterac.ClientID%>').disabled = false;
                    document.getElementById('<%=imgInterac.ClientID%>').disabled = false;
                    document.getElementById('<%=txtRefund.ClientID%>').value = document.getElementById('<%=txtRefundAmt.ClientID%>').value;
                    //alert(document.getElementById('<%=txtRefund.ClientID%>').value);
                    document.getElementById('<%=txtSubTotal.ClientID%>').value = document.getElementById('<%=txtRefundAmt.ClientID%>').value;
                    document.getElementById('<%=txtTotal.ClientID%>').value = document.getElementById('<%=txtRefundAmt.ClientID%>').value;
                    return false;
                }
            }
            else if (arguments == "2") {
                document.getElementById('<%=txtRefundAmt.ClientID%>').value = "0.00";
                document.getElementById('<%=rbCash.ClientID%>').disabled = true;
                document.getElementById('<%=imgCash.ClientID%>').disabled = true;
                document.getElementById('<%=rbVisa.ClientID%>').disabled = true;
                document.getElementById('<%=imgVisa.ClientID%>').disabled = true;
                document.getElementById('<%=rbMasterCard.ClientID%>').disabled = true;
                document.getElementById('<%=imgMasterCard.ClientID%>').disabled = true;
                document.getElementById('<%=rbAmerican.ClientID%>').disabled = true;
                document.getElementById('<%=imgAmerican.ClientID%>').disabled = true;
                document.getElementById('<%=rbInterac.ClientID%>').disabled = true;
                document.getElementById('<%=imgInterac.ClientID%>').disabled = true;
            }
        }

        function funCkeck(arguments) {
            var hdn = document.getElementById('<%=hdnVal.ClientID%>');
            if (arguments == "1") {
                hdn.value = "1";
                document.getElementById('<%=rbCash.ClientID%>').checked = true;
                document.getElementById('<%=rbCash.ClientID%>').checked = 1;
            }
            else if (arguments == "2") {
                hdn.value = "2";
                document.getElementById('<%=rbVisa.ClientID%>').checked = true;
                document.getElementById('<%=rbVisa.ClientID%>').checked = 1;
            }
            else if (arguments == "3") {
                hdn.value = "3";
                document.getElementById('<%=rbMasterCard.ClientID%>').checked = true;
                document.getElementById('<%=rbMasterCard.ClientID%>').checked = 1;
            }
            else if (arguments == "4") {
                hdn.value = "4";
                document.getElementById('<%=rbAmerican.ClientID%>').checked = true;
                document.getElementById('<%=rbAmerican.ClientID%>').checked = 1;
            }
            else if (arguments == "5") {
                hdn.value = "5";
                document.getElementById('<%=rbInterac.ClientID%>').checked = true;
                document.getElementById('<%=rbInterac.ClientID%>').checked = 1;
            }
            // alert(10);
            return false;
        }        
    </script>
    <script type="text/javascript">
        //Variables
        var gridID = "<%=jgdvViewTran.ClientID %>";
        var searchPanelID = "<%=SearchPanel.ClientID %>";
        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }
        function RefreshJQGrideView() {
            $('#' + gridID).trigger("reloadGrid");
        }
        function AssignStatusTypeID(statusTyepID) {
            $('#<%=txtConfirmCode.ClientID %>').val("");
            $('#<%=txtSearch.ClientID %>').val(statusTyepID);
            $("#btnSearch").trigger('click');
        }
    </script>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class POS_AddEditRegister : System.Web.UI.Page
{
    clsPosRegisterTrans objRegTra = new clsPosRegisterTrans();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            //divSetAmount.Style.Add("display", "none");
            txtCloseAmount.Visible = false;
            try
            {
                MessageState.SetGlobalMessage(MessageType.Success, "");
                if (this.RegTransID == 0)
                {
                    trClose.Visible = false;
                    trCalculatedCash.Visible = false;
                    txtCloseAmount.Text = "";
                    txtCloseAmount.Text = "";
                    txtstartAmt.Focus();
                }
                else if (this.RegTransID > 0)
                {
                    //divSetAmount.Style.Add("display", "block");// Style("display") = "block";
                    txtstartAmt.Enabled = false;
                    objRegTra.RegTransID = BusinessUtility.GetString(this.RegTransID);
                    objRegTra.subGetRegiTran();
                    txtstartAmt.Text = CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(objRegTra.RegStartAmount)).Replace(",", "");
                    txtCalculatedCash.Text = CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(objRegTra.RegCalculatedCash)).Replace(",", "");
                    txtCloseAmount.Visible = true;
                    if (!string.IsNullOrEmpty(objRegTra.RegCloseAmount))
                    {
                        txtCloseAmount.Text = objRegTra.RegCloseAmount;
                        // cmdSetCloseAmount.Style("visibility") = "hidden"
                        //imgSetCloseAmount.Style("visibility") = "hidden";
                        btnSave.Visible = false;
                    }
                    else
                    {
                        txtCloseAmount.Text = objRegTra.funCalCloseAmt(txtstartAmt.Text, BusinessUtility.GetString(this.RegTransID));
                        //cmdSetCloseAmount.Style("visibility") = "visible"
                        //imgSetCloseAmount.Style("visibility") = "visible";
                        btnSave.Visible = true;
                    }

                    //LoadAmoutDetail();
                    txtCloseAmount.Focus();
                }

            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        LoadAmoutDetail();
        if (this.RegTransID == 0)
        {
            GetLastClosingAmountDemo();
        }
    }


    private List<AmountDenmoation> GetAmountDemo()
    {
        AmountDenmoation objAmtDemo = new AmountDenmoation();
        List<AmountDenmoation> li = objAmtDemo.GetAmountDemonationList();
        foreach (var item in li)
        {
            int id = BusinessUtility.GetInt(item.ID);
            TextBox txtCount = (TextBox)pnlAmount.FindControl("txtCount_" + id);
            if (txtCount != null)
            {
                if (BusinessUtility.GetInt(txtCount.Text) > 0)
                {
                    var itemtoUpdate = li.Where(i => i.ID == id).FirstOrDefault();
                    itemtoUpdate.AmountCount = BusinessUtility.GetInt(txtCount.Text);
                }
            }
        }
        return li;
    }

    private void GetLastClosingAmountDemo()
    {
        AmountDenmoation objAmtDemo = new AmountDenmoation();
        List<AmountDenmoation> li = objAmtDemo.GetLastClosedAmountDemonationList(BusinessUtility.GetString(Session["RegCode"]));
        foreach (var item in li)
        {
            int id = BusinessUtility.GetInt(item.ID);
            TextBox txtCount = (TextBox)pnlAmount.FindControl("txtCount_" + id);
            if (txtCount != null)
            {
                txtCount.Text = BusinessUtility.GetString(item.AmountCount);
                //if (BusinessUtility.GetInt(txtCount.Text) > 0)
                //{
                //    var itemtoUpdate = li.Where(i => i.ID == id).FirstOrDefault();
                //    itemtoUpdate.AmountCount = BusinessUtility.GetInt(txtCount.Text);
                //}
            }
        }
    }


    protected void LoadAmoutDetail()
    {

        AmountDenmoation objAmtDemo = new AmountDenmoation();
        var lstAmtDemo = objAmtDemo.GetAmountDemonationList();
        TextBox txtAmount = new TextBox();
        Label lblTitle = new Label();

        foreach (var lst in lstAmtDemo)
        {
            TableRow tRow = new TableRow();
            TableCell tCell = new TableCell();
            tCell.HorizontalAlign = HorizontalAlign.Center;

            lblTitle = new Label();
            lblTitle.ID = "lblTitle_" + BusinessUtility.GetString(lst.ID);
            lblTitle.Text = BusinessUtility.GetString(lst.Description);
            tCell.Controls.Add(lblTitle);
            tRow.Cells.Add(tCell);


            tCell = new TableCell();
            tCell.HorizontalAlign = HorizontalAlign.Center;
            txtAmount = new TextBox();
            txtAmount.Width = Unit.Pixel(100);
            txtAmount.ID = "txtCount_" + BusinessUtility.GetString(lst.ID);
            txtAmount.Attributes.Add("txtID", BusinessUtility.GetString(lst.ID));
            txtAmount.Attributes.Add("AmtValue", BusinessUtility.GetString(lst.Amount));
            txtAmount.Attributes.Add("onkeyup", "calcAmount(this);");
            txtAmount.CssClass = "cssCount numericTextField";

            tCell.Controls.Add(txtAmount);
            tRow.Cells.Add(tCell);

            tCell = new TableCell();
            tCell.HorizontalAlign = HorizontalAlign.Center;
            txtAmount = new TextBox();
            txtAmount.Width = Unit.Pixel(100);
            txtAmount.ID = "txtTotal_" + BusinessUtility.GetString(lst.ID);
            txtAmount.Enabled = false;
            txtAmount.CssClass = "cssTotal";
            tCell.Controls.Add(txtAmount);
            tRow.Cells.Add(tCell);
            tblAmountDemonation.Rows.Add(tRow);
        }

        // AddTotalRow

        TableRow tRowTotal = new TableRow();
        TableCell tCellTotal = new TableCell();
        tCellTotal.HorizontalAlign = HorizontalAlign.Center;

        lblTitle = new Label();
        lblTitle.ID = "lblTotal";
        lblTitle.Text = Resources.Resource.lblTotal;
        tCellTotal.Controls.Add(lblTitle);
        tRowTotal.Cells.Add(tCellTotal);


        tCellTotal = new TableCell();
        tCellTotal.HorizontalAlign = HorizontalAlign.Center;
        txtAmount = new TextBox();
        txtAmount.Width = Unit.Pixel(100);
        txtAmount.ID = "txtTotaCount";
        txtAmount.Enabled = false;
        tCellTotal.Controls.Add(txtAmount);
        tRowTotal.Cells.Add(tCellTotal);

        tCellTotal = new TableCell();
        tCellTotal.HorizontalAlign = HorizontalAlign.Center;
        txtAmount = new TextBox();
        txtAmount.Width = Unit.Pixel(100);
        txtAmount.ID = "txtTotalAmt";
        txtAmount.Enabled = false;
        tCellTotal.Controls.Add(txtAmount);
        tRowTotal.Cells.Add(tCellTotal);
        tblAmountDemonation.Rows.Add(tRowTotal);

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {

            List<AmountDenmoation> li = GetAmountDemo();
            if (this.RegTransID == 0)
            {
                objRegTra.RegCode = BusinessUtility.GetString(Session["RegCode"]);
                objRegTra.RegMgr = BusinessUtility.GetString(Session["userID"]);
                objRegTra.RegStartAmount = txtstartAmt.Text;
                objRegTra.RegStartDateTime = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss");//string.Format(System.DateTime.Parse(DateTime.Now), "yyyy-MM-dd HH:mm:ss");
                objRegTra.RegCloseDateTime = "sysNull";
                objRegTra.RegCloseAmount = "sysNull";
                objRegTra.RegPOSUser = "sysNull";
                if (objRegTra.funCheckCloseAmt() == true)
                {
                    objRegTra.funInsertRegiTran();
                    AmountDenmoation objAmtDemo = new AmountDenmoation();
                    if (BusinessUtility.GetInt(objRegTra.RegTransID) > 0)
                    {
                        objAmtDemo.SaveAmountDemoniation(li, BusinessUtility.GetInt(objRegTra.RegTransID), "O");
                    }
                    //lblMsg.Text = msgStartAmountSaved;
                    //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgStartAmountSaved);
                    //Globals.RegisterReloadParentScript(this.Page);
                    Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadPage('{0}');", Resources.Resource.msgStartAmountSaved), true);
                }
                else
                {
                    //lblMsg.Text = msgPreviousCloseAmt;
                    //lblMsg.ForeColor = System.Drawing.Color.Red;
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPreviousCloseAmt);
                }
            }
            else
            {
                objRegTra.RegTransID = BusinessUtility.GetString(this.RegTransID);
                objRegTra.RegStartAmount = txtstartAmt.Text;
                objRegTra.RegCloseAmount = txtCloseAmount.Text;
                objRegTra.RegMgr = BusinessUtility.GetString(Session["userID"]);
                objRegTra.RegCloseDateTime = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss");//Strings.Format(System.DateTime.Parse(DateAndTime.Now()), "yyyy-MM-dd HH:mm:ss");
                if (string.IsNullOrEmpty(txtCloseAmount.Text))
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.reqvalCloseAmt);
                    //lblMsg.Text = reqvalCloseAmt;
                    //lblMsg.ForeColor = System.Drawing.Color.Red;
                    //hdnRegTranID.Value = "";
                    txtstartAmt.Enabled = true;
                    //trClose.Style("visibility") = "hidden";
                    return;
                }
                objRegTra.funUpdateRegiTran();
                AmountDenmoation objAmtDemo = new AmountDenmoation();
                objAmtDemo.SaveAmountDemoniation(li, this.RegTransID, "C");
                //lblMsg.Text = msgCloseAmountSaved;
                //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgCloseAmountSaved);

                Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadPage('{0}');", Resources.Resource.msgCloseAmountSaved), true);
                //Globals.RegisterReloadParentScript(this.Page);
            }
            //hdnRegTranID.Value = "";
            txtstartAmt.Enabled = true;
            //trClose.Style("visibility") = "hidden";
            //subFillGrid();
            //Globals.RegisterReloadParentScript(this.Page);



            //SysRegister objSysRegister = new SysRegister();
            //if (objSysRegister.ValidateMangerCode(BusinessUtility.GetString(Session["RegCode"]), txtDiscount.Text) == true)
            //{
            //Globals.RegisterCloseDialogScript(Page, string.Format("parent.ApplyInvoiceDisc({0});", txtDiscount.Text), true);
            //Globals.RegisterReloadParentScript(this.Page);
            //Globals.RegisterCloseDialogScript(this.Page);

            //}
            //else
            //{
            //    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblInvalidManageCode);
            //}
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }


    private int RegTransID
    {
        get
        {
            int partnerId = 0;
            int.TryParse(Request.QueryString["RegTranID"], out partnerId);
            return partnerId;
        }
    }
}
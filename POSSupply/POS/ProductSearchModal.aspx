﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="ProductSearchModal.aspx.cs" Inherits="POS_ProductSearchModal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <asp:Panel ID="pnlSearch" runat="server">
        <table width="99%" border="0">
            <tr>
                <td>
                    <table border="0" cellpadding="2" cellspacing="2">
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lblKeyword %>"
                                    AssociatedControlID="txtSearch" CssClass="filter-key"></asp:Label>
                            </td>
                            <td valign="middle" align="left">
                                <asp:TextBox runat="server" Width="195px" ID="txtSearch" ValidationGroup="PrdSrch">
                                </asp:TextBox>
                            </td>
                            <td>
                                <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="right">
                    <input id="Button1" type="button" value="<%=Resources.Resource.lblAdvanceSearch%>"
                        onclick="AdvanceSearch();" />
                </td>
            </tr>
        </table>
        <%--Grid view Start for Customer--%>
        <div style="padding: 0px 5px;">
            <div id="grid_wrapper" style="width: 99%">
                <trirand:JQGrid runat="server" ID="grdProducts" DataSourceID="sqldsProcess" Height="300px"
                    AutoWidth="true" PagerSettings-PageSize="50" OnDataRequesting="grdProducts_DataRequesting"
                    OnCellBinding="grdProducts_CellBinding">
                    <Columns>
                        <trirand:JQGridColumn DataField="ProductID" HeaderText="<%$ Resources:Resource, grdPoProductID %>"
                            PrimaryKey="true" />
                        <trirand:JQGridColumn DataField="prdName" HeaderText="<%$ Resources:Resource, grdPOProductName %>" />
                        <trirand:JQGridColumn DataField="prdUPCCode" HeaderText="<%$ Resources:Resource, grdPOUPCCode %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="Color" HeaderText="<%$ Resources:Resource, lblColor %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="Size" HeaderText="<%$ Resources:Resource, lblSize %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="prdEndUserSalesPrice" HeaderText="<%$ Resources:Resource, grdPOPrdCostPrice %>"
                            TextAlign="Right">
                            <%--                            <Formatter>
                                <trirand:CurrencyFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="00"
                                    Prefix="$" ThousandsSeparator="," />
                            </Formatter>--%>
                        </trirand:JQGridColumn>
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn="" />
                    <AppearanceSettings AlternateRowBackground="True" />
                    <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="gridLoadComplete"
                        RowSelect="rowSelect" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sqldsProcess" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
                <br />
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdProducts.ClientID %>";
        var searchPanelID = "<%=pnlSearch.ClientID %>";
        var $grid = $("#<%=grdProducts.ClientID %>");

        function resize_the_grid() {
            $grid.fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $grid.onJqGridPaging();
        }


        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $grid.onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        function gridLoadComplete(data) {
            $(window).trigger("resize");
            $("#<%=txtSearch.ClientID%>").focus();
        }

        //Alphabat Search
        $(".alpha_search").click(function () {
            var alphaFilter = {};
            alphaFilter["_history"] = 0;
            alphaFilter["_alphaArgument"] = $(this).attr("search-data");
            $grid.appendPostData(alphaFilter);
            $grid.trigger("reloadGrid", [{ page: 1}]);
        });

        function rowSelect(id) {
            parent.setPrdUpcCode($grid.jqGrid('getCell', id, 'prdUPCCode'));
            jQuery.FrameDialog.closeDialog();
            //            opener.setPrdUpcCode($grid.jqGrid('getCell', id, 'prdUPCCode'));
            //            self.close();
            //alert($grid.jqGrid('getCell', id, 'prdUPCCode'));           
        }

        function AdvanceSearch() {
            parent.redirectAdvanceProductSearch();
            jQuery.FrameDialog.closeDialog();
        }
                       
    </script>
</asp:Content>

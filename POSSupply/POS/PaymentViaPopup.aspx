﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="PaymentViaPopup.aspx.cs" Inherits="POS_PaymentViaPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <script type="text/javascript" src="../scripts/jquery.keypad.js"></script>
    <script type="text/javascript" language="JavaScript" src="itech.js"></script>
    <script src="combogrid-1.6.2/jquery/jquery-ui-1.8.9.custom.min.js" type="text/javascript"></script>
    <script src="combogrid-1.6.2/plugin/jquery.i18n.properties-1.0.9.js" type="text/javascript"></script>
    <script src="combogrid-1.6.2/plugin/jquery.ui.combogrid-1.6.2.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-plugins/jquery.blockUI.js" type="text/javascript"></script>
    <link href="Css/style.css" rel="stylesheet" type="text/css" />

        <style type="text/css">
        .divButton
        {
            width: 90px; /*width: 100px;*/
            font-size: 15px;
            height: auto;
            text-align: center; /* background-color: #FB9515;*/
            padding: 0px;
        }
        
        .divGiftButton
        {
            width: 90px; /*width: 100px;*/
            font-size: 15px;
            height: auto;
            text-align: center; /* background-color: #FB9515;*/
            padding: 0px;
        }
        
        
        .CustTitleCss
        {
            color: #880015;
            font-family: 'Trebuchet MS' ,Helvetica,sans-serif;
            font-size: 22px;
            font-weight: bold;
            line-height: 26px;
        }
        
        body {
    background-color: #fff;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div id="divRefundedVia" runat="server" style="padding:10px;" >
        <asp:Panel runat="server" ID="Panel4">
            <table id="Table4" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%;">
                <tr>
                    <td align="center">
                        <div style="width: 99%; height: 100%; display: block; float: left; padding-left: 5px;">
                            <div style="float: left; cursor: pointer; width: 31.9%; margin: 0 3px 5px 16%;" class="divButton"
                                id="div2" runat="server" onclick="PaymentVia('0');">
                                <a class="ovalbutton" href="Javascript:;"><span class="ovalbutton">
                                    <%=Resources.Resource.liARCash %></span> </a>
                            </div>
                            <div class="divButton" style="float: left; cursor: pointer; padding: 0px; width: 31.9%;
                                margin: 0px 3px 5px 0px;" id="div3" onclick="PaymentVia('5');">
                                <a class="ovalbutton" href="Javascript:;"><span class="ovalbutton">
                                    <%= Resources.Resource.lblDebit%></span> </a>
                            </div>
                            <div style="float: left; cursor: pointer; padding: 0px; width: 31.9%; margin: 0px 3px 5px 0px;"
                                class="divButton" id="div4" onclick="PaymentVia('15');">
                                <a class="ovalbutton" href="Javascript:;" style="height: auto;"><span class="ovalbutton">
                                    <%= Resources.Resource.btnPaymnetMC%>
                                </span></a>
                            </div>
                        <%--    <div style="clear: both">
                            </div>--%>
                            <div style="float: left; cursor: pointer; padding: 0px; width: 31.9%; margin: 0px 3px 5px 0px;"
                                class="divButton" id="div5" onclick="PaymentVia('16');">
                                <a class="ovalbutton" href="Javascript:;"><span class="ovalbutton">
                                    <%=Resources.Resource.btnPaymnetAmEx%></span> </a>
                            </div>
                            <div class="divButton" id="div6" style="float: left; cursor: pointer; width: 31.9%;
                                margin: 0px 3px 5px 0px; padding: 0px;" onclick="PaymentVia('14');">
                                <a class="ovalbutton" href="Javascript:;"><span class="ovalbutton">
                                    <%= Resources.Resource.btnPaymnetVisa%></span> </a>
                            </div>
                            <div style="float: left; cursor: pointer; padding: 0px; width: 31.9%; margin: 0px 3px 5px 0px;"
                                class="divButton" id="div8" onclick="PaymentVia('13')">
                                <asp:HiddenField ID="HiddenField3" runat="server" Value="0" />
                                <a class="ovalbutton" href="Javascript:;"><span class="ovalbutton">
                                    <%= Resources.Resource.lblInstoreCredit%></span> </a>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <script type="text/javascript">
            function PaymentVia(paymentViaID) {
                parent.CloseDialogPopup(paymentViaID);
                jQuery.FrameDialog.closeDialog();
            }
        </script>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


﻿Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Data.Odbc
Imports System.Data
Imports System.Web.UI
Imports System.IO
Imports System.Xml
Imports System.Web.UI.WebControls
Imports clsCommon
Imports iTECH.InbizERP.BusinessLogic
Imports iTECH.Library.Utilities

Imports System.Web
Imports System.Web.Services
Imports System.Web.Script.Services
Partial Class POS_POS
    Inherits BasePage
    Private objProduct As New clsProducts
    Dim objDT As System.Data.DataTable
    Dim objDR As System.Data.DataRow
    Dim objDT_SP As System.Data.DataTable
    Dim objDR_SP As System.Data.DataRow
    Dim objTransaction As New clsPOSTransaction
    Dim objPosPrdNo As New clsPosProductNo
    Dim objPosTranAccHrd As New clsPosTransAccHdr
    Dim objTransDtl As New clsTransactionDtl
    Dim objPrint As New clsPrint
    Dim objCommon As New clsCommon
    Dim objPrdQty As New clsPrdQuantity
    Dim strPrductList As New ArrayList
    Dim objPrdPOSCatg As New clsPrdPOSCatg
    Dim objRegister As New clsRegister
    Dim objRegTra As New clsPosRegisterTrans
    Protected Sub POS_POS_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("SavedData") = ""
        Session("InvokeSrc") = ""
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Session("RegCode") = "" Then
            Response.Redirect("~/Admin/selectRegister.aspx")
        End If

        If (CurrentUser.IsInRole(RoleID.POS_MANAGER)) Then
            hdnIsUserPosManagerRole.Value = "1"
        Else
            hdnIsUserPosManagerRole.Value = "0"
        End If

        Dim keyPad As New HtmlLink()
        'keyPad.Attributes.Add("type", "text/css")
        'keyPad.Attributes.Add("rel", "stylesheet")
        'keyPad.Href = ResolveUrl("~/css/jquery.keypad.alt.css")
        'Page.Header.Controls.Add(keyPad)
        'Added by mukesh 20140724
        If objRegTra.funCheckCloseAmt() = True Then
            hdnIsRegClosed.Value = "1"
        Else
            hdnIsRegClosed.Value = "0"
        End If

        hdnDate.Value = DateTime.Now.Month
        If hdnDate.Value < 10 Then
            hdnDate.Value = "0" & DateTime.Now.Month
        End If

        lblTranMsg.Text = ""
        hdnYear.Value = DateTime.Now.Year
        divCardManually.Style("display") = "none"
        ' divTransaction.Style("display") = "none"
        divPOSMenu.Style("display") = "none"
        divPOSCategory.Style("display") = "none"
        'divPOSCatgProductMenu.Style("display") = "none"
        divProductKitContent.Style("display") = "none"
        lblMsg.Text = ""
        lblTranMsg.Text = ""
        lblTranMsg.ForeColor = Drawing.Color.Green
        lblMsg.ForeColor = Drawing.Color.Green
        lblProIL.Visible = False
        'lblProLst.Visible = False
        'This setting for Only Cash Transaction
        'If ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "" Then
        '    subDisEnabled()
        'Else
        subEnabled()
        'End If
        If txtReceived.Text = "" Then
            txtReceived.Text = "0"
        End If
        If Session("Msg") <> "" Then
            'lblTranMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If

        txtReceived.Attributes.Add("onKeyup", "funCalBalance();")
        If Request.QueryString("TType") = "P" Then
            'lblTitle.Text = msgPauseTranID & Request.QueryString("TID") & ")" & ""
        ElseIf Request.QueryString("TType") = "R" Then
            lblTitle.Text = msgRefundTranID & Request.QueryString("TID") & ")" & ""
            CmdTab.Visible = False
            CmdCancel.Visible = False
        Else
            'lblTitle.Text = " "
        End If

        If Request.QueryString("Print") = "Yes" Then
            Session.Remove("SalesCart")
        End If

        trProcess.Visible = True
        If Not IsPostBack Then

            'Dim objPrint As New InbizUser
            'objPrint.FillSalesRepresentatives(ddlSaleRep)

            'For Each li As ListItem In ddlSaleRep.Items
            '    If li.Value = Convert.ToString(CurrentUser.UserID) Then
            '        li.Selected = True
            '    End If
            'Next

            'ddlSaleRep.SelectedValue = CurrentUser.user


            'Dim strValue As New ArrayList
            'strValue = objProduct.subGetProductsArray()

            'Get Default Customer ID
            Dim sysReg As New SysRegister()
            sysReg.PopulateObject(BusinessUtility.GetString(Session("RegCode")))
            'SysRegister sysReg = new SysRegister();
            'sysReg.PopulateObject(strRegCode);
            Dim custID As Integer = BusinessUtility.GetInt(sysReg.SysDefaultCustomerID)
            If (custID > 0) Then
                hdnDefaultCustID.Value = custID
                hdnIstLoadedCustID.Value = custID
                hdnManagerCode.Value = BusinessUtility.GetString(sysReg.SysManagerCode)
            End If

            If BusinessUtility.GetString(Request.QueryString("partID")) <> "" Then
                hdnDefaultCustID.Value = BusinessUtility.GetInt(Request.QueryString("partID"))
                hdnShowDefaultProduct.Value = "1"
            End If

            'If BusinessUtility.GetString(Request.QueryString("pandingPrintReceipt")) = "1" Then
            '    hdnIsPandingPrintRcpt.Value = "1"
            'End If

            DropDownHelper.FillTaxGroup(ddlTax, CurrentUser.UserDefaultWarehouse, New ListItem(Resources.Resource.liTaxGroup, "0"))
            If ddlTax.Items.Count > 2 Then
                ddlTax.SelectedIndex = 1
            End If

            'Dim objCustCrdt As New CustomerCredit()
            'If BusinessUtility.GetDouble(objCustCrdt.GetAvailableCredit(BusinessUtility.GetInt(hdnDefaultCustID.Value))) <= 0 Then
            '    hdnIsHideInstoreCredit.Value = "1"
            'End If
            'CustomerCredit objCustCrdt = new CustomerCredit();
            '        double dblAvlCrdt = BusinessUtility.GetDouble(objCustCrdt.GetAvailableCredit(this.CustID));

            subPopulateYear()
            subPopulateMonth()
            txtSearch.Text = " "
            txtPopSearch.Focus()
            makeCart()
            Session("IsSalesCart") = "Yes"
            If Convert.ToString(Session("SalesCart")) <> "" Then
                'grdRequest.DataSource = Session("SalesCart")
                'grdRequest.DataBind()
            End If
            If Request.QueryString("TID") <> "" Then
                subGetPosProduct()
            Else
                txtTax1.Text = 0
                txtTax2.Text = 0
                txtSubTotal.Text = 0
                txtTotal.Text = 0
                txtTax3.Text = 0
                txtTax4.Text = 0
                txtTotalAmount.Text = 0
                txtReceived.Text = 0
                txtRefund.Text = 0
                subStringFormat()
            End If
            Session.Add("chkKit", "")
            EmptyFillGrd()
            If (ConfigurationManager.AppSettings("ShowPrdPOSCatg").ToUpper = "TRUE") Then 'And (objPrdPOSCatg.fillPOSCatgMenu("IsCatg") <> "0") 
                'divPOSCategory.Style("display") = "block"
                ImgPosMenu.Attributes.Add("onclick", "return funPOSCategory();")
                prdPOSCatgMenu()
            Else

                If objProduct.subFillPosMenu("ISPrd") = "" Then
                    ImgPosMenu.Attributes.Add("onclick", "return funMenuPOSMenuPrd();")
                Else
                    ImgPosMenu.Attributes.Add("onclick", "return funPOSMenuOpen();")
                    'divPOSMenu.Style("display") = "block"
                    PosMenu()
                End If
            End If

            If Request.QueryString("Msg") <> "" Then
                If Request.QueryString("Msg").ToString.ToLower = "approved" Then
                    Session.Add("msg", msgTransaction & " " & Session("transactionID") & " " & msgCompletedSuccess & "<br/> " & POSTotalAmount & Session("amount") & ", " & POSReceived & Session("amount") & ", " & POSBalance & txtRefund.Text)
                Else
                    Session.Add("msg", Request.QueryString("Msg").ToString().Replace("[br]", "<br>")) 'msgTransaction & " " & Session("transactionID") & " has been Failed."
                    lblTranMsg.ForeColor = Drawing.Color.Red
                End If
                Session.Remove("transactionID")
                Session.Remove("transactionType")
                Session.Remove("PosAcctId")
                Session.Remove("amount")
                Response.Redirect("POS.aspx")
            End If
        End If
        'If grdRequest.Rows.Count > 0 Then
        '    lblProIL.Visible = False
        'Else
        '    lblProIL.Visible = False
        'End If

        ' imgCmdBackToCatg1.Attributes.Add("onclick", "return funBackToCat();")


        'If ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "" Then
        '    imgVisa.Attributes.Add("onclick", "return funVisaAmount();")
        '    imgMasterCard.Attributes.Add("onclick", "return funMasterAmount();")
        '    imgInterac.Attributes.Add("onclick", "return funInteracAmount();")
        '    imgAmerican.Attributes.Add("onclick", "return funAmericanAmount();")
        'End If

        If IsPostBack Then
            Dim strPrdId As String = hdnProductID.Value
            If strPrdId <> "" Then
                ' ShowNAdd(strPrdId)
                ' divPOSCatgProductMenu.Style("display") = "block"
            End If
        End If

        A7.HRef = A7.HRef + "&regcode=" + BusinessUtility.GetString(Session("RegCode"))
        A4.HRef = A4.HRef + "&regcode=" + BusinessUtility.GetString(Session("RegCode"))
        A5.HRef = A5.HRef + "&regcode=" + BusinessUtility.GetString(Session("RegCode"))
        A10.HRef = A10.HRef + "&regcode=" + BusinessUtility.GetString(Session("RegCode"))
        hrfReturn.HRef = hrfReturn.HRef + "&regcode=" + BusinessUtility.GetString(Session("RegCode"))
        hrfReturn.HRef = hrfReturn.HRef + "&whscode=" + BusinessUtility.GetString(Session("RegWhsCode"))
        A3.HRef = A3.HRef + "&whscode=" + BusinessUtility.GetString(Session("RegWhsCode"))
        A2.HRef = A2.HRef + "&whsCode=" + BusinessUtility.GetString(Session("RegWhsCode"))
        A6.HRef = A6.HRef + "&whsCode=" + BusinessUtility.GetString(Session("RegWhsCode"))
        A9.HRef = A9.HRef + "&whsCode=" + BusinessUtility.GetString(Session("RegWhsCode"))
        A6.HRef = A6.HRef + "&regcode=" + BusinessUtility.GetString(Session("RegCode"))
        A8.HRef = A8.HRef + "&regcode=" + BusinessUtility.GetString(Session("RegCode"))


        'added by Hitendra dropdown reason for gift
        iTECH.InbizERP.BusinessLogic.DropDownHelper.FillReasonForGift(ddlReasonForGift, New ListItem("Select Reason", ""), iTECH.InbizERP.BusinessLogic.Globals.CurrentAppLanguageCode)
        DashboardSettings.FillAuthorizeBy(ddlAuthorizedBy, New ListItem("Authorize By", ""), iTECH.InbizERP.BusinessLogic.Globals.CurrentAppLanguageCode)
    End Sub
    Public Sub EmptyFillGrd()
        Dim sql As String
        sql = "select '' as ProductID,'' as prdIsKit,'' as prdname "
        SqlDSrcPOSCatgProduct.SelectCommand = sql
        sqldsRequest.SelectCommand = "select '' as ProductID,'' as prdIsKit,'1' as prdname "
        SqlDSrcPOSProdKitContent.SelectCommand = " select '' as prdKitID,'' as prdName,'' as prdIncludeQty "
    End Sub

    Public Sub subEnabled()
        rbVisa.Enabled = True
        imgVisa.Enabled = True
        rbMasterCard.Enabled = True
        imgMasterCard.Enabled = True
        rbAmerican.Enabled = True
        imgAmerican.Enabled = True
        rbInterac.Enabled = True
        imgInterac.Enabled = True
        rbEnterCardInfo.Enabled = True
        imgEnterCardInfo.Enabled = True
    End Sub
    Public Sub subDisEnabled()
        rbVisa.Enabled = False
        imgVisa.Enabled = False
        rbMasterCard.Enabled = False
        imgMasterCard.Enabled = False
        rbAmerican.Enabled = False
        imgAmerican.Enabled = False
        rbInterac.Enabled = False
        imgInterac.Enabled = False
        rbEnterCardInfo.Enabled = False
        imgEnterCardInfo.Enabled = False
    End Sub
    ' Make Cart
    Sub makeCart()
        objDT = New System.Data.DataTable("Cart")
        objDT.Columns.Add("ID", GetType(Integer))
        objDT.Columns("ID").AutoIncrement = True
        objDT.Columns("ID").AutoIncrementSeed = 1

        objDT.Columns.Add("ProductID", GetType(Integer))
        objDT.Columns.Add("UPCCode", GetType(String))
        objDT.Columns.Add("ProductName", GetType(String))
        objDT.Columns.Add("Quantity", GetType(Double))
        objDT.Columns.Add("Price", GetType(Double))
        objDT.Columns.Add("TotalPrice", GetType(Double))
        Session("SalesCart") = objDT
    End Sub
    ' Add Cart
    Sub AddToCart()
        If Convert.ToString(Session("SalesCart")) <> "" Then
            objDT = Session("SalesCart")
            Dim Product = txtProductId.Text
            Dim intQty As Double
            Dim blnMatch As Boolean = False
            For Each objDR In objDT.Rows
                If objDR("ProductID") = Product Then
                    objDR("Quantity") += String.Format("{0:F}", CDbl(txtQuantity.Text))
                    If IsNumeric(txtTotalPrice.Text) = True Then
                        objDR("TotalPrice") += String.Format("{0:F}", CDbl(txtTotalPrice.Text))
                    Else
                        Exit Sub
                    End If
                    intQty = objDR("Quantity")
                    blnMatch = True
                    Exit For
                End If
            Next
            'check Prd Qty
            'If objPrdQty.funCheckProductQuantity(txtProductId.Text, Session("UserWarehouse")) < intQty Then
            '    hdnPrdQty.Value = "Y"
            '    If IsNumeric(objDR("Quantity")) <> 0 Then
            '        objDR("Quantity") = objDR("Quantity") - txtQuantity.Text
            '        objDR("TotalPrice") = objDR("TotalPrice") - txtTotalPrice.Text
            '    End If
            '    Exit Sub
            'End If
            If Not blnMatch Then
                objDR = objDT.NewRow
                objDR("ProductID") = txtProductId.Text
                objDR("ProductName") = txtProductName.Text
                objDR("UPCCode") = txtUPCCode.Text

                If IsNumeric(txtQuantity.Text) = True Then
                    objDR("Quantity") = txtQuantity.Text
                Else
                    Exit Sub
                End If
                If IsNumeric(txtPrice.Text) = True Then
                    objDR("Price") = String.Format("{0:F}", CDbl(txtPrice.Text))
                Else
                    Exit Sub
                End If
                If IsNumeric(txtTotalPrice.Text) = True Then
                    objDR("TotalPrice") = String.Format("{0:F}", CDbl(txtTotalPrice.Text))
                Else
                    Exit Sub
                End If
                objDT.Rows.InsertAt(objDR, 0)
            End If
            txtQuantity.Text = ""
            lblMsg.Text = msgItemAddedSuccessfully
        Else
            lblMsg.Text = ""
        End If
    End Sub
    Protected Sub txtQuantity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQuantity.TextChanged
        If IsNumeric(txtQuantity.Text) <> True Then
            lblMsg.Text = msgQtyInNumeric
            lblMsg.ForeColor = Drawing.Color.Red
            txtQuantity.Focus()
            Exit Sub
        End If
        If txtQuantity.Text.Contains("-") Then
            lblMsg.Text = msgNegativeValueQty
            lblMsg.ForeColor = Drawing.Color.Red
            txtQuantity.Focus()
            Exit Sub
        End If
        If txtQuantity.Text <> 0 And txtQuantity.Text <> "" Then
            ' txtTotalPrice.Text = txtQuantity.Text * hdnPrice.Value
        End If
        ' txtTotalPrice.Text = String.Format("{0:F}", CDbl(txtTotalPrice.Text))
    End Sub
    ' On Edit To Cart
    Private Sub subEditToCart(ByVal strEditItem As String)
        objDT = Session("SalesCart")
        objDR = objDT.Rows(strEditItem)
        objDR("ProductID") = txtProductId.Text
        objDR("ProductName") = txtProductName.Text
        objDR("UPCCode") = txtUPCCode.Text
        objDR("Quantity") = txtQuantity.Text
        objDR("Price") = txtPrice.Text
        objDR("TotalPrice") = txtTotalPrice.Text
        'objDR("PrdTax") = hdnTax.Value
    End Sub
    Public Sub subAddProduct()
        If Convert.ToString(Session("eItm")) <> "" Then
            subEditToCart(Convert.ToString(Session("eItm")))
            Session.Remove("eItm")
        Else
            lblMsg.Text = ""
            If IsNumeric(txtQuantity.Text) <> True Then
                lblMsg.Text = msgQtyEnter
                txtQuantity.Focus()
                Exit Sub
            End If
            AddToCart()
            If hdnPrdQty.Value = "Y" Then
                txtTotalPrice.Text = txtPrice.Text
                Exit Sub
            End If
            'If Convert.ToString(Session("SalesCart")) <> "" Then
            '    grdRequest.DataSource = Session("SalesCart")
            '    'grdRequest.Sort(grdRequest.SortExpression, SortDirection.Descending)
            '    grdRequest.DataBind()

            '    txtPopSearch.Focus()
            'End If
            Dim strTax As ArrayList
            strTax = objPrint.funCalcTax(txtProductId.Text, Session("UserWarehouse"), txtTotalPrice.Text, "POS")
            Dim intI As Integer
            Dim strTaxDes As String
            Dim strTaxAmount As String
            While strTax.Count - 1 >= intI
                strTaxDes = ""
                strTaxDes = strTax.Item(intI).ToString.Substring(0, strTax.Item(intI).IndexOf("@,@"))
                strTaxAmount = strTax.Item(intI).ToString.Substring(strTax.Item(intI).IndexOf("@,@") + 3)
                If strTaxDes.ToLower = lblTax1.Text.ToLower Or lblTax1.Text.ToLower = "" Then
                    lblTax1.Text = strTaxDes
                    txtTax1.Text = CDbl(txtTax1.Text) + CDbl(strTaxAmount)
                    trTax1.Visible = True
                ElseIf strTaxDes.ToLower = lblTax2.Text.ToLower Or lblTax2.Text.ToLower = "" Then
                    lblTax2.Text = strTaxDes
                    txtTax2.Text = CDbl(txtTax2.Text) + CDbl(strTaxAmount)
                    trTax2.Visible = True
                ElseIf strTaxDes.ToLower = lblTax3.Text.ToLower Or lblTax3.Text.ToLower = "" Then
                    lblTax3.Text = strTaxDes
                    txtTax3.Text = CDbl(txtTax3.Text) + CDbl(strTaxAmount)
                    trTax3.Visible = True
                ElseIf strTaxDes.ToLower = lblTax4.Text.ToLower Or lblTax4.Text.ToLower = "" Then
                    lblTax4.Text = strTaxDes
                    txtTax4.Text = CDbl(txtTax4.Text) + CDbl(strTaxAmount)
                    trTax4.Visible = True
                End If
                intI += 1
            End While
            txtSubTotal.Text = CDbl(txtTotalPrice.Text) + CDbl(txtSubTotal.Text)
            txtTotal.Text = CDbl(txtTax1.Text) + CDbl(txtTax2.Text) + CDbl(txtSubTotal.Text) + CDbl(txtTax3.Text) + CDbl(txtTax4.Text)
            txtTotalAmount.Text = txtTotal.Text
            txtRefund.Text = 0
            subStringFormat()
        End If
        lblProIL.Visible = False
    End Sub
    Public Sub subStringFormat()
        txtTax1.Text = String.Format("{0:F}", CDbl(txtTax1.Text))
        txtTax2.Text = String.Format("{0:F}", CDbl(txtTax2.Text))
        txtTax3.Text = String.Format("{0:F}", CDbl(txtTax3.Text))
        txtTax4.Text = String.Format("{0:F}", CDbl(txtTax4.Text))
        txtSubTotal.Text = String.Format("{0:F}", CDbl(txtSubTotal.Text))
        txtTotal.Text = String.Format("{0:F}", CDbl(txtTotal.Text))
        txtTotalAmount.Text = String.Format("{0:F}", CDbl(txtTotalAmount.Text))
        txtRefund.Text = String.Format("{0:F}", CDbl(txtRefund.Text))
        If txtReceived.Text <> "" Then
            txtReceived.Text = String.Format("{0:F}", CDbl(txtReceived.Text))
        End If
    End Sub
    Protected Sub imgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
        lblTranMsg.Text = ""
        'If String.IsNullOrEmpty(txtSearch.Text.Trim()) Then
        'txtSearch.Text = hdntxtSearch.Value
        'End If

        If txtSearch.Text.Trim() <> "" Then

            If dlSearch.SelectedValue = "PN" Then
                'lblProLst.Visible = True
                'subFillGridProcess() ' Grid Ankit
                divViewPrd.Style("display") = "none"
                'trPrdItem.Visible = False
            Else
                subGetProductsInfo()
                If hdnPrdQty.Value = "Y" Then
                    lblMsg.Text = msgPrdNotAvailableStock
                    lblMsg.ForeColor = Drawing.Color.Red
                    hdnPrdQty.Value = ""
                    txtPopSearch.Focus()
                    Exit Sub
                End If
                trProcess.Visible = False
                txtPopSearch.Focus()
            End If
            txtSearch.Text = ""
        Else
            txtPopSearch.Focus()
            lblMsg.Text = msgEnterTextforSearch
        End If
    End Sub
    'Protected Sub grdProcess_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdProcess.RowDataBound
    '    If (e.Row.RowType = DataControlRowType.DataRow) Then
    '        If e.Row.Cells(4).Text <> "&nbsp;" Then
    '            e.Row.Cells(4).Text = String.Format("{0:F}", CDbl(e.Row.Cells(4).Text))
    '        End If
    '        Dim imgEdit As ImageButton
    '        imgEdit = CType(e.Row.FindControl("imgEdit"), ImageButton)
    '        If e.Row.RowIndex = 0 Then
    '            imgEdit.Focus()
    '        End If
    '        e.Row.Visible = True
    '    End If
    'End Sub
    'Protected Sub grdProcess_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdProcess.Sorting
    '    subFillGridProcess()
    'End Sub
    'Protected Sub grdProcess_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdProcess.PageIndexChanging
    '    subFillGridProcess()
    'End Sub
    'Public Sub subFillGridProcess()
    '    If txtSearch.Text.Trim <> "" Then
    '        Dim strValue As String = txtSearch.Text.Trim
    '        sqldsProcess.SelectCommand = objProduct.subFillProductForPOS(dlSearch.SelectedValue, funRemove(strValue))
    '        sqldsProcess.DataBind()
    '    End If
    'End Sub
    ' Sql Data Source Event Track
    'Protected Sub sqldsProcess_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsProcess.Selected
    '    If e.AffectedRows = 0 Then
    '        lblMsg.Text = NoDataFound
    '        lblMsg.ForeColor = Drawing.Color.Red
    '    Else
    '        lblMsg.Text = ""
    '        txtPopSearch.Focus()
    '    End If
    'End Sub
    'Editing
    'Protected Sub grdProcess_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdProcess.RowEditing
    '    divItems.Visible = True
    '    txtProductId.Text = grdProcess.Rows(e.NewEditIndex).Cells(0).Text
    '    txtUPCCode.Text = grdProcess.Rows(e.NewEditIndex).Cells(1).Text
    '    txtProductName.Text = grdProcess.Rows(e.NewEditIndex).Cells(2).Text
    '    txtQuantity.Text = "1" 'grdProcess.Rows(e.NewEditIndex).Cells(3).Text
    '    txtQuantity.ToolTip = grdProcess.Rows(e.NewEditIndex).Cells(3).Text
    '    txtPrice.Text = grdProcess.Rows(e.NewEditIndex).Cells(4).Text
    '    hdnPrice.Value = grdProcess.Rows(e.NewEditIndex).Cells(4).Text
    '    If hdnPrice.Value = "&nbsp;" Then
    '        hdnPrice.Value = 0
    '    End If
    '    txtTotalPrice.Text = grdProcess.Rows(e.NewEditIndex).Cells(4).Text
    '    If txtPrice.Text = "&nbsp;" Then
    '        txtPrice.Text = 0
    '    End If
    '    txtPrice.Text = String.Format("{0:F}", CDbl(txtPrice.Text))
    '    If txtTotalPrice.Text = "&nbsp;" Then
    '        txtTotalPrice.Text = 0
    '    End If
    '    txtTotalPrice.Text = String.Format("{0:F}", CDbl(txtTotalPrice.Text))
    '    divViewPrd.Style("display") = "block"
    '    'trPrdItem.Visible = True
    '    txtQuantity.Focus()
    '    lblMsg.Text = ""
    '    subAddProduct()
    '    txtQuantity.Text = 1
    '    txtPopSearch.Focus()
    '    txtSearch.Text = " "
    'End Sub
    'Search Sub Routine
    Public Sub subGetProductsInfo(Optional ByVal strPOSMenu As String = "")
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        Dim objDataClass As New clsDataClass
        If strPOSMenu = "" Then
            strSQL = objProduct.subFillProductForPOS(dlSearch.SelectedValue, funRemove(txtSearch.Text.Trim))
        Else
            strSQL = objProduct.subFillProductForPOS("POSMENU", strPOSMenu)
        End If

        drObj = objDataClass.GetDataReader(strSQL)
        Dim IntI As Integer = 0
        While drObj.Read
            divItems.Visible = True
            txtProductId.Text = drObj.Item("ProductID").ToString
            txtUPCCode.Text = drObj.Item("prdUPCCode").ToString
            txtProductName.Text = drObj.Item("ProductName").ToString
            txtQuantity.Text = "1" 'grdProcess.Rows(e.NewEditIndex).Cells(3).Text
            'txtQuantity.ToolTip = drObj.Item("prdIntID").ToString
            hdnPrice.Value = drObj.Item("prdEndUserSalesPrice").ToString
            txtPrice.Text = drObj.Item("prdEndUserSalesPrice").ToString
            txtTotalPrice.Text = drObj.Item("prdEndUserSalesPrice").ToString
            txtPrice.Text = String.Format("{0:F}", CDbl(txtPrice.Text))
            txtTotalPrice.Text = String.Format("{0:F}", CDbl(txtTotalPrice.Text))
            divViewPrd.Style("display") = "block"
            IntI += 1
        End While
        drObj.Close()
        objDataClass.CloseDatabaseConnection()
        lblMsg.Text = ""
        If IntI = 0 Then
            lblMsg.Text = msgPrdNotFound
            lblMsg.ForeColor = Drawing.Color.Red
        Else
            subAddProduct()
            txtQuantity.Text = 1
            txtQuantity.Focus()
        End If
    End Sub
    Public Function funGetTransaction(ByVal StrPrintReceipt As String) As Boolean
        If Convert.ToString(Session("SalesCart")) <> "" Then
            subData()
            If grdRequest.Rows.Count = 0 Then
                lblTranMsg.Text = msgPlsAddPrd
                lblTranMsg.ForeColor = Drawing.Color.Red
                Return False
            End If
            If objPosTranAccHrd.AcctPayType = 7 Then
                objTransaction.TransStatus = 0
                'ElseIf objPosTranAccHrd.AcctPayType <> 1 Then
                '    objTransaction.TransStatus = 2
            Else
                'objTransaction.TransStatus = 1
                If IsNumeric(txtReceived.Text) <> True Then
                    lblTranMsg.Text = msgEnterRecieveAmountInNumerice
                    lblTranMsg.ForeColor = Drawing.Color.Red
                    Return False
                End If
                If txtReceived.Text = "0.00" Or txtReceived.Text = "0" Then
                    lblTranMsg.Text = "Please enter Receive Amount."
                    lblTranMsg.ForeColor = Drawing.Color.Red
                    Return False
                End If
                If CDbl(txtTotalAmount.Text) <= 0 Then
                    'lblTranMsg.Text = msgTotalAmountCreditCardTansaction
                    lblTranMsg.ForeColor = Drawing.Color.Red
                    txtReceived.Focus()
                    Return False
                End If
                If CDbl(txtTotalAmount.Text) > CDbl(txtReceived.Text) Then
                    lblTranMsg.Text = msgAmountReceivedLessTotalAmount
                    lblTranMsg.ForeColor = Drawing.Color.Red
                    txtReceived.Focus()
                    Return False
                End If
            End If
            Dim strTranAccHrdID As String ' for postransaccthdr table
            Dim strTranID As String ' for posTransaction table
            If Request.QueryString("TID") <> "" Then
                Dim intJ As Integer
                subPause()
                If Request.QueryString("TType") = "P" Then
                    Session("transactionID") = Request.QueryString("TID") '+++++++++++++++++++++++++++++++++++++++++++++

                    If objPosTranAccHrd.AcctPayType <> 1 And objPosTranAccHrd.AcctPayType <> 7 And objPosTranAccHrd.AcctPayType <> 6 Then
                        If ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "tr" Then
                            trTransaction.Visible = True
                            hdnPrintUrl.Value = ConfigurationManager.AppSettings("PrintURL") & "&TID=" & Session("transactionID") & "&WhsID=" & Session("UserWarehouse") & "&LangsID=" & funGetProductLang()
                            divTransaction.Style("display") = "block"
                        End If
                    End If

                    objPosTranAccHrd.AcctTransID = Request.QueryString("TID")
                    objPosTranAccHrd.funInsertpostransaccthdr(strTranAccHrdID)
                    objTransDtl.posAcctHdrDtlId = strTranAccHrdID
                    'Transaction by Card
                    If objPosTranAccHrd.AcctPayType <> 1 And objPosTranAccHrd.AcctPayType <> 7 And objPosTranAccHrd.AcctPayType <> 6 Then
                        If ConfigurationManager.AppSettings("CreditCardAPI").ToLower <> "" Then
                            If funCardTransaction(strTranID) = False Then
                                Return False
                            Else
                                subDetectPrdQtyForCreditCard()
                            End If
                        End If
                    End If
                Else
                    objPosTranAccHrd.AcctTransID = Request.QueryString("TID")
                    objPosTranAccHrd.funUpdatePostransaccthdr()
                End If
            Else
                strTranID = funSave()
                Session("transactionID") = strTranID '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

                If objPosTranAccHrd.AcctPayType <> 1 And objPosTranAccHrd.AcctPayType <> 7 And objPosTranAccHrd.AcctPayType <> 6 Then
                    If ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "tr" Then
                        trTransaction.Visible = True
                        hdnPrintUrl.Value = ConfigurationManager.AppSettings("PrintURL") & "&TID=" & strTranID & "&WhsID=" & Session("UserWarehouse") & "&LangsID=" & funGetProductLang()
                        divTransaction.Style("display") = "block"
                    End If
                End If

                objPosTranAccHrd.AcctTransID = strTranID
                objPosTranAccHrd.funInsertpostransaccthdr(strTranAccHrdID)
                objTransDtl.posAcctHdrDtlId = strTranAccHrdID
                'Transaction by Card
                If objPosTranAccHrd.AcctPayType <> 1 And objPosTranAccHrd.AcctPayType <> 7 And objPosTranAccHrd.AcctPayType <> 6 Then
                    If ConfigurationManager.AppSettings("CreditCardAPI").ToLower <> "" Then
                        If funCardTransaction(strTranID) = False Then
                            Return False
                        Else
                            subDetectPrdQtyForCreditCard()
                        End If
                    End If
                End If
            End If
            'divTransaction.Style("display") = "block"
            Session.Remove("transactionID")
            Session.Remove("transactionType")
            Session.Remove("PosAcctId")
            Session.Remove("amount")
            Session.Remove("SalesCart")
            If Request.QueryString("TID") <> "" Then
                Session.Add("msg", msgTransaction & " " & Request.QueryString("TID") & " " & msgCompletedSuccess & "<br/> " & POSTotalAmount & txtTotal.Text & ", " & POSReceived & txtReceived.Text & ", " & POSBalance & txtRefund.Text)
            ElseIf objPosTranAccHrd.AcctPayType = 7 Then
                Session.Add("msg", msgTransaction & " " & strTranID & " has been Lost.")
            Else
                Session.Add("msg", msgTransaction & " " & strTranID & " " & msgCompletedSuccess & "<br/> " & POSTotalAmount & txtTotal.Text & ", " & POSReceived & txtReceived.Text & ", " & POSBalance & txtRefund.Text)
            End If

            If StrPrintReceipt = "1" Then 'check for print receipt
                If Request.Browser.Browser.ToString() = "IE" Then
                    If objPosTranAccHrd.AcctPayType <> 1 And objPosTranAccHrd.AcctPayType <> 7 Then
                        If Request.QueryString("TID") <> "" Then
                            Response.Redirect("~/POS/Print.aspx?TID=" & Request.QueryString("TID") & "&card=yes" & "&WithCard=yes")
                        Else
                            Response.Redirect("~/POS/Print.aspx?TID=" & strTranID & "&card=yes" & "&WithCard=yes")
                        End If
                    Else
                        If Request.QueryString("TID") <> "" Then
                            Response.Redirect("~/POS/Print.aspx?TID=" & Request.QueryString("TID") & "" & "&WithCard=yes")
                        Else
                            Response.Redirect("~/POS/Print.aspx?TID=" & strTranID & "" & "&WithCard=yes")
                        End If
                    End If
                Else
                    If objPosTranAccHrd.AcctPayType <> 1 And objPosTranAccHrd.AcctPayType <> 7 Then
                        If Request.QueryString("TID") <> "" Then
                            Response.Write("<script>window.open('Print.aspx?TID=" & Request.QueryString("TID") & "&card=yes&WithCard=yes', 'Print','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');</script>")
                        Else
                            Response.Write("<script>window.open('Print.aspx?TID=" & strTranID & "&card=yes&WithCard=yes', 'Print','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');</script>")
                        End If
                    Else
                        If Request.QueryString("TID") <> "" Then
                            Response.Write("<script>window.open('Print.aspx?TID=" & Request.QueryString("TID") & "&WithCard=yes', 'Print','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');</script>")
                        Else
                            Response.Write("<script>window.open('Print.aspx?TID=" & strTranID & "&WithCard=yes', 'Print','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');</script>")
                        End If
                    End If
                    Response.Write("<script>location.href='Pos.aspx';</script>")
                End If
            End If
            Return True
        Else
            lblMsg.Text = msgPrdAddInTemlst
            lblMsg.ForeColor = Drawing.Color.Red
            Return False
        End If
    End Function
    Public Function funCardTransaction(ByVal strTransactionID As String) As Boolean
        Dim strCreditCardNo, strExpiryDate, strTranDtlStatus As String
        Dim strName() As String
        If divCardManually.Style("display") = "none" And rbEnterCardInfo.Checked = False Then
            If ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "tr" Then
                ' Session("transactionType") = "00"
            Else
                Dim strValue() As String = txtCardInfo.Text.Split("^")
                If strValue.Length > 1 Then
                    strCreditCardNo = strValue(0).Remove(0, 2)
                    If strCreditCardNo.Length > 4 Then
                        objTransDtl.posCardNo = strCreditCardNo.Substring(strCreditCardNo.Length - 4)
                    Else
                        objTransDtl.posCardNo = strCreditCardNo
                    End If
                    strName = strValue(1).Split("/")
                    If strValue.Length = 3 Then
                        objTransDtl.posLastName = strName(0)
                        objTransDtl.posFirstName = strName(1)
                        objTransDtl.posCardExpDate = strValue(2).Substring(0, 2)
                        strExpiryDate = strValue(2).Substring(0, 4)
                    End If
                End If
            End If
        Else
            objTransDtl.posFirstName = txtFirstName.Text
            objTransDtl.posLastName = txtLastName.Text
            strCreditCardNo = txtCardNo.Text.Trim
            objTransDtl.posCardNo = txtCardNo.Text.Substring(txtCardNo.Text.Length - 4)
            objTransDtl.posCardExpDate = dlMonth.Text
            strExpiryDate = dlMonth.Text & dlYear.Text.Substring(2)
        End If
        Dim strPosAcctId As String
        objTransDtl.posTransEndTime = "sysNull"
        objTransDtl.posTransID = "sysNull"
        objTransDtl.posTranAcctType = "sale"
        objTransDtl.posTransStartDatetime = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
        objTransDtl.posTransAcctDtlStatus = 0
        strTranDtlStatus = 0
        objTransDtl.funInsertPosTransaction(strPosAcctId) 'postransacctdtl table
        Dim strPost As String
        Dim strResponse As String

        If ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "msi" Then
            strPost = funMSI(strCreditCardNo, strExpiryDate)
        ElseIf ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "vm" Then
            strPost = funVM(strCreditCardNo, strExpiryDate)
        End If

        If ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "tr" Then
            Session("PosAcctId") = strPosAcctId
            'divTransaction.Style("display") = "block"
            'strResponse = "1[49]~Approved~VISA~4242424242424242~1212~MANUAL~0010011260~12:56:30~09/09/03"
            Exit Function
        Else
            strResponse = funGetResponseToCard(strPost)
        End If

        'Condition when Tran Failed or Success
        If funPopulateObject(strResponse, strPosAcctId) = True Then
            objTransaction.TransStatus = 1
            objPosTranAccHrd.CasReceived = txtTotalAmount.Text
            objPosTranAccHrd.funUpdateRecievePostransaccthdr()
            objTransaction.funUpdateTransStaus(strTransactionID)
            Return True
        Else
            objTransaction.TransStatus = 0
            objPosTranAccHrd.CasReceived = 0
            objPosTranAccHrd.funUpdateRecievePostransaccthdr()
            objTransaction.funUpdateTransStaus(strTransactionID)
            Return False
        End If
    End Function
    'Do Request to Credit card Transaction(Federated_Canada_API.pdf)
    Public Function funMSI(ByVal strCreditCardNo As String, ByVal strExpiryDate As String) As String
        Dim strPost As String
        strPost = ConfigurationManager.AppSettings("MSIPostURL") & _
                          "username=" & ConfigurationManager.AppSettings("MSIUserID") & "&password=" & ConfigurationManager.AppSettings("MSIUserPassword") & _
                          "&type=sale&ccnumber=" & strCreditCardNo & "&cvv=999&ccexp=" & strExpiryDate & "&amount=" & objTransaction.TotalValue & ""

        Return strPost
    End Function
    'Do Request to Credit card Transaction(Virtual Merchant)
    Public Function funVM(ByVal strCreditCardNo As String, ByVal strExpiryDate As String) As String
        Dim strPost As String
        Dim strMID As String = Session("RegMerchantID")
        strPost = ConfigurationManager.AppSettings("VMPostURL") & "<txn><ssl_merchant_ID>" & strMID & "</ssl_merchant_ID><ssl_user_id>" & ConfigurationManager.AppSettings("VMUserID") & "</ssl_user_id><ssl_pin>" & ConfigurationManager.AppSettings("VMUserPassword") & "</ssl_pin><ssl_transaction_type>ccsale</ssl_transaction_type><ssl_card_number>" & strCreditCardNo & "</ssl_card_number><ssl_exp_date>" & strExpiryDate & "</ssl_exp_date><ssl_amount>" & objTransaction.TotalValue & "</ssl_amount></txn>"
        Return strPost
    End Function
    'For Get Response
    Public Function funGetResponseToCard(ByVal strPost As String) As String
        Dim strNewValue As String
        Dim strResponse As String
        ' Create the request obj 
        Dim req As System.Net.HttpWebRequest = DirectCast(System.Net.WebRequest.Create(strPost), System.Net.HttpWebRequest)
        ' Set values for the request back 
        req.Method = "POST"
        req.ContentType = "text/xml"
        Dim stOut As New StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII)
        stOut.Write(strNewValue)
        stOut.Close()
        ' Do the request to get the response 
        Dim stIn As New StreamReader(req.GetResponse().GetResponseStream())
        strResponse = stIn.ReadToEnd()
        stIn.Close()
        Return strResponse
    End Function
    Public Function funPopulateObjToMSI(ByVal strString As String, ByRef strResponseText As String) As String
        Dim strResponse, strAuthcode, strTransactionid As String
        Dim strAvsresponse, strCvvresponse, strOrderid, strResponse_code As String
        Dim str() As String = strString.ToLower.Split("&")
        For i As Int16 = 0 To str.Length - 1
            If str(i).Contains("avsresponse=") Then
                strAvsresponse = str(i).Substring(str(i).LastIndexOf("=") + 1)
            ElseIf str(i).Contains("cvvresponse=") Then
                objTransDtl.posCVVCode = str(i).Substring(str(i).LastIndexOf("=") + 1)
            ElseIf str(i).Contains("response=") Then
                strResponse = str(i).Substring(str(i).LastIndexOf("=") + 1)
            ElseIf str(i).Contains("responsetext=") Then
                strResponseText = str(i).Substring(str(i).LastIndexOf("=") + 1)
            ElseIf str(i).Contains("authcode=") Then
                strAuthcode = str(i).Substring(str(i).LastIndexOf("=") + 1)
            ElseIf str(i).Contains("transactionid=") Then
                objTransDtl.posTransID = str(i).Substring(str(i).LastIndexOf("=") + 1)
            ElseIf str(i).Contains("orderid=") Then
                strOrderid = str(i).Substring(str(i).LastIndexOf("=") + 1)
            ElseIf str(i).Contains("response_code=") Then
                objTransDtl.posExtReturnStatus = str(i).Substring(str(i).LastIndexOf("=") + 1)
            End If
        Next
        Return strResponse
    End Function
    Public Function funPopulateObjToVM(ByVal strString As String, ByRef strResponseText As String) As String
        Dim strResponse, strAuthcode, strTransactionid As String
        Dim strAvsresponse, strCvvresponse, strOrderid, strResponse_code As String
        Dim xmlNodelist As XmlNodeList
        Dim xmlListItem As New System.Xml.XmlDocument
        Dim xmlNode As XmlNode
        Try
            xmlListItem.LoadXml(strString)
        Catch ex As Exception
            Exit Function
        End Try
        xmlNodelist = xmlListItem.SelectNodes("txn")
        For Each xmlNode In xmlNodelist
            For intI As Int16 = 0 To xmlNode.ChildNodes.Count - 1
                If xmlNode.ChildNodes(intI).Name.ToLower = "errorcode" Then
                    strResponse = 0
                    objTransDtl.posExtReturnStatus = xmlNode.ChildNodes(intI).InnerText
                    objTransDtl.posTransID = 0
                    strResponseText = xmlNode.ChildNodes(2).InnerText
                    Exit For
                Else
                    strResponse = 1
                    If xmlNode.ChildNodes(intI).Name.ToLower = "ssl_txn_id" Then
                        objTransDtl.posTransID = xmlNode.ChildNodes(intI).InnerText
                    ElseIf xmlNode.ChildNodes(intI).Name.ToLower = "ssl_approval_code" Then
                        objTransDtl.posExtReturnStatus = xmlNode.ChildNodes(intI).InnerText
                    ElseIf xmlNode.ChildNodes(intI).Name.ToLower = "ssl_cvv2_response" Then
                        objTransDtl.posCVVCode = xmlNode.ChildNodes(intI).InnerText
                    End If
                End If
            Next
        Next
        Return strResponse
    End Function
    ' for Merchant Connect Multi Technical 
    Public Function funPopulateObjToTR(ByVal strString As String, ByRef strResponseText As String) As String
        Dim strResponse, strAuthcode, strTransactionid As String
        Dim strAvsresponse, strCvvresponse, strOrderid, strResponse_code As String
        Dim str() As String = strString.ToLower.Split("~")

        If str(0) = "1[49]" Then
            strResponse = 1
        Else
            strResponse = 0
        End If

        If str(2).ToLower = "visa" Then
            objPosTranAccHrd.AcctPayType = 2
            objTransDtl.posCardName = str(2)
        ElseIf str(2).ToLower = "masterCard" Then
            objPosTranAccHrd.AcctPayType = 3
            objTransDtl.posCardName = str(2)
        ElseIf str(2).ToLower = "american express" Then
            objPosTranAccHrd.AcctPayType = 4
            objTransDtl.posCardName = str(2)
        ElseIf str(2).ToLower = "interac" Then
            objPosTranAccHrd.AcctPayType = 5
            objTransDtl.posCardName = str(2)
        End If
        If str(3) <> "" Then
            objTransDtl.posCardNo = str(3)
        End If
        objTransDtl.posExtReturnStatus = str(4) & str(5)

        objTransDtl.posTransID = str(6)
        Return strResponse
    End Function
    Private Function funPopulateObject(ByVal strString As String, ByVal strPosAcctId As String) As Boolean
        Dim strResponseText, strResponse As String

        objTransDtl.posTransEndTime = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
        If ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "msi" Then
            strResponse = funPopulateObjToMSI(strString, strResponseText)
        ElseIf ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "vm" Then
            strResponse = funPopulateObjToVM(strString, strResponseText)
        ElseIf ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "tr" Then
            strResponse = funPopulateObjToTR(strString, strResponseText)
        End If
        If strResponse = "" Then
            Return False
        End If

        If strResponse = "1" Then
            objTransDtl.posTransAcctDtlStatus = "1"
        Else
            objTransDtl.posTransAcctDtlStatus = "9"
        End If
        objTransDtl.posTransEndTime = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
        objTransDtl.funUpdateTransaction(strPosAcctId)

        If strResponse = "1" Then
            objTransDtl.posTransAcctDtlStatus = "1"
            Return True
        Else
            objTransDtl.posTransAcctDtlStatus = "9"
            ' Dim strMsg() As String = strResponseText.Split("refid:")
            lblTranMsg.Text = strResponseText
            lblTranMsg.ForeColor = Drawing.Color.Red
            Return False
        End If
    End Function
    Public Sub subData()
        objTransaction.TransUserID = Session("UserID")
        objTransaction.TransRegCode = Session("RegCode")
        If Request.QueryString("TType") = "R" Then
            objTransaction.TransType = "R"
        Else
            objTransaction.TransType = "S"
        End If
        objTransaction.TransDateTime = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
        objTransaction.TotalValue = txtTotal.Text
        objTransDtl.posTotalValue = txtTotal.Text
        objTransaction.SubTotal = txtSubTotal.Text
        objTransaction.Tax1 = txtTax1.Text
        objTransaction.Tax2 = txtTax2.Text
        objTransaction.Tax3 = txtTax3.Text
        objTransaction.Tax4 = txtTax4.Text

        objTransaction.Tax1Desc = lblTax1.Text
        objTransaction.Tax2Desc = lblTax2.Text
        objTransaction.Tax3Desc = lblTax3.Text
        objTransaction.Tax4Desc = lblTax4.Text

        objTransaction.OldTransID = "sysNull"
        Session("amount") = objTransaction.TotalValue
        If ConfigurationManager.AppSettings("CreditCardAPI").ToLower <> "" Then
            objTransaction.TransStatus = 0
        Else
            objTransaction.TransStatus = 1
        End If
        Session("transactionType") = "00"
        If rbCash.Checked = True Then
            objPosTranAccHrd.AcctPayType = 1
            objTransaction.TransStatus = 1
        ElseIf rbLost.Checked = True Then  'Condition for lost transaction
            objPosTranAccHrd.AcctPayType = 7
            objTransaction.TransStatus = 0
        ElseIf rbVisa.Checked = True Then
            objPosTranAccHrd.AcctPayType = 2
            objTransDtl.posCardName = "Visa"
        ElseIf rbMasterCard.Checked = True Then
            objPosTranAccHrd.AcctPayType = 3
            objTransDtl.posCardName = "MasterCard"
        ElseIf rbAmerican.Checked = True Then
            objPosTranAccHrd.AcctPayType = 4
            objTransDtl.posCardName = "American Express"
        ElseIf rbInterac.Checked = True Then
            objPosTranAccHrd.AcctPayType = 5
            Session("transactionType") = "01"
        ElseIf rbExactCash.Checked = True Then
            objPosTranAccHrd.AcctPayType = 6
            objTransaction.TransStatus = 1
        End If

        If rbCash.Checked = True Then
            objPosTranAccHrd.CashReturned = txtRefund.Text
            objPosTranAccHrd.CasReceived = txtReceived.Text
        Else
            objPosTranAccHrd.CashReturned = 0
            objPosTranAccHrd.CasReceived = txtReceived.Text
        End If
    End Sub
    Public Function funSave(Optional ByVal strTrancType As String = "") As String
        Dim strTranID As String
        objTransaction.funInsertPosTransaction(strTranID)
        objPosPrdNo.PrdTransID = strTranID
        Dim intI As Integer
        Dim objTblPrdNo As DataTable
        Dim objRowPrdNo As DataRow
        objTblPrdNo = Session("SalesCart")
        For intI = 0 To objTblPrdNo.Rows.Count - 1
            objRowPrdNo = objTblPrdNo.Rows(intI)
            objPosPrdNo.ProductID = objRowPrdNo("ProductID")
            objPosPrdNo.PrdQty = objRowPrdNo("Quantity")
            objPosPrdNo.PrdUnitPrice = objRowPrdNo("Price")
            objPosPrdNo.PrdPrice = objRowPrdNo("TotalPrice")
            objPosPrdNo.PrdTaxCode = objTransaction.funPrdTax(objRowPrdNo("ProductID"), Session("UserWarehouse"))
            objPosPrdNo.funInsertPosProduct()
            If objPosTranAccHrd.AcctPayType <> 7 Then 'objPosTranAccHrd.AcctPayType = 1 And
                If strTrancType = "" Then
                    If objProduct.funGetPrdValue(objPosPrdNo.ProductID, "kit") = "1" Then
                        objPrdQty.subGetPrdKitOnHandQty(objPosPrdNo.ProductID, Session("UserWarehouse"), objPosPrdNo.PrdQty) 'reduce kit's product Qty
                        objPrdQty.updateProductQuantity(objPosPrdNo.ProductID, Session("UserWarehouse"), objPosPrdNo.PrdQty) ' reduce prd kit Qty
                        strPrductList.Add(objPosPrdNo.ProductID)
                    Else
                        objPrdQty.updateProductQuantity(objPosPrdNo.ProductID, Session("UserWarehouse"), objPosPrdNo.PrdQty)
                        objPrdQty.funGetReducePrdKitQty(objPosPrdNo.ProductID, Session("UserWarehouse"), "") ' reduce prd kit Qty
                        strPrductList.Add(objPosPrdNo.ProductID)
                    End If
                End If
            End If
        Next
        Return strTranID
    End Function



    Public Sub subDetectPrdQtyForCreditCard()
        Dim intI As Integer
        Dim objTblPrdNo As DataTable
        Dim objRowPrdNo As DataRow
        objTblPrdNo = Session("SalesCart")
        For intI = 0 To objTblPrdNo.Rows.Count - 1
            objRowPrdNo = objTblPrdNo.Rows(intI)
            If objPosTranAccHrd.AcctPayType <> 1 Then
                If objProduct.funGetPrdValue(objRowPrdNo("ProductID"), "kit") = "1" Then
                    objPrdQty.subGetPrdKitOnHandQty(objRowPrdNo("ProductID"), Session("UserWarehouse"), objRowPrdNo("Quantity")) 'reduce kit's product Qty
                    objPrdQty.updateProductQuantity(objRowPrdNo("ProductID"), Session("UserWarehouse"), objRowPrdNo("Quantity")) ' reduce prd kit Qty
                Else
                    objPrdQty.updateProductQuantity(objRowPrdNo("ProductID"), Session("UserWarehouse"), objRowPrdNo("Quantity"))
                    objPrdQty.funGetReducePrdKitQty(objRowPrdNo("ProductID"), Session("UserWarehouse"), "") ' reduce prd kit Qty
                End If
            End If
        Next
    End Sub

    Public Sub subGetPosProduct()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        Dim objDataClass As New clsDataClass
        strSQL = "SELECT posPrdTransID, posProductID,Pdes.prdname as ProductName, posPrdQty, posPrdUnitPrice,posPrdPrice, posPrdTaxCode,prdUPCCode FROM posproductno "
        strSQL += "Inner join Products on Products.ProductID=posproductno.posProductID"
        strSQL += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID where descLang ='" + objCommon.funGetProductLang + "' and posPrdTransID='" + Request.QueryString("TID") + "' "
        drObj = objDataClass.GetDataReader(strSQL)
        While drObj.Read
            objDR = objDT.NewRow
            objDR("ProductID") = drObj.Item("posProductID").ToString
            objDR("ProductName") = drObj.Item("ProductName").ToString()
            objDR("Quantity") = drObj.Item("posPrdQty").ToString
            objDR("Price") = drObj.Item("posPrdUnitPrice").ToString
            objDR("TotalPrice") = drObj.Item("posPrdPrice").ToString
            objDR("UPCCode") = drObj.Item("prdUPCCode").ToString
            objDT.Rows.Add(objDR)
        End While
        drObj.Close()
        objDataClass.CloseDatabaseConnection()
        Session("SalesCart") = objDT
        'grdRequest.DataSource = Session("SalesCart")
        'grdRequest.DataBind()

        objTransaction.TransId = Request.QueryString("TID")
        objTransaction.subGetTransactionInfo()
        txtSubTotal.Text = objTransaction.SubTotal
        txtTotal.Text = objTransaction.TotalValue

        If objTransaction.Tax1 <> "0" Then
            trTax1.Visible = True
        End If
        If objTransaction.Tax2 <> "0" Then
            trTax2.Visible = True
        End If
        If objTransaction.Tax3 <> "0" Then
            trTax3.Visible = True
        End If
        If objTransaction.Tax4 <> "0" Then
            trTax4.Visible = True
        End If

        txtTax1.Text = objTransaction.Tax1
        txtTax2.Text = objTransaction.Tax2
        txtTax3.Text = objTransaction.Tax3
        txtTax4.Text = objTransaction.Tax4

        lblTax1.Text = objTransaction.Tax1Desc
        lblTax2.Text = objTransaction.Tax2Desc
        lblTax3.Text = objTransaction.Tax3Desc
        lblTax4.Text = objTransaction.Tax4Desc

        objPosTranAccHrd.AcctTransID = objTransaction.TransId
        objPosTranAccHrd.subGetPostransaccthdr()
        If objPosTranAccHrd.AcctPayType = 1 Then
            rbCash.Checked = True
        ElseIf objPosTranAccHrd.AcctPayType = 2 Then
            rbVisa.Checked = True
        ElseIf objPosTranAccHrd.AcctPayType = 3 Then
            rbMasterCard.Checked = True
        ElseIf objPosTranAccHrd.AcctPayType = 4 Then
            rbAmerican.Checked = True
        ElseIf objPosTranAccHrd.AcctPayType = 7 Then
            rbLost.Checked = True
        End If



        If objPosTranAccHrd.CasReceived = "" Then
            txtReceived.Text = 0
        Else
            txtReceived.Text = objPosTranAccHrd.CasReceived
        End If
        If objPosTranAccHrd.CashReturned = "" Then
            txtRefund.Text = 0
        Else
            txtRefund.Text = objPosTranAccHrd.CashReturned
        End If
        txtTotalAmount.Text = txtTotal.Text
        txtPopSearch.Focus()
        subStringFormat()
    End Sub
    'for Paused
    Public Sub subPause(Optional ByVal strCancel As String = "")
        Dim strTranID As String = Request.QueryString("TID")
        objTransaction.TransId = strTranID
        objTransaction.funUpdateTransaction()
        objPosPrdNo.PrdTransID = strTranID
        objPosPrdNo.funDeletePosProduct()
        Dim intI As Integer
        Dim objTblPrdNo As DataTable
        Dim objRowPrdNo As DataRow
        objTblPrdNo = Session("SalesCart")
        For intI = 0 To objTblPrdNo.Rows.Count - 1
            objRowPrdNo = objTblPrdNo.Rows(intI)
            objPosPrdNo.ProductID = objRowPrdNo("ProductID")
            objPosPrdNo.PrdQty = objRowPrdNo("Quantity")
            objPosPrdNo.PrdUnitPrice = objRowPrdNo("Price")
            objPosPrdNo.PrdPrice = objRowPrdNo("TotalPrice")
            objPosPrdNo.PrdTaxCode = objTransaction.funPrdTax(objRowPrdNo("ProductID"), Session("UserWarehouse"))
            objPosPrdNo.funInsertPosProduct()
            If Request.QueryString("TType") = "P" And strCancel = "" Then
                If objProduct.funGetPrdValue(objPosPrdNo.ProductID, "kit") = "1" Then
                    objPrdQty.subGetPrdKitOnHandQty(objPosPrdNo.ProductID, Session("UserWarehouse"), objPosPrdNo.PrdQty) 'reduce kit's product Qty
                    objPrdQty.updateProductQuantity(objPosPrdNo.ProductID, Session("UserWarehouse"), objPosPrdNo.PrdQty) ' reduce prd kit Qty
                Else
                    objPrdQty.updateProductQuantity(objPosPrdNo.ProductID, Session("UserWarehouse"), objPosPrdNo.PrdQty)
                    objPrdQty.funGetReducePrdKitQty(objPosPrdNo.ProductID, Session("UserWarehouse"), "") ' reduce prd kit Qty
                End If
            End If
        Next
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    Protected Sub txtReceived_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtReceived.TextChanged
        If IsNumeric(txtReceived.Text) <> True Then
            lblTranMsg.Text = msgEnterRecieveAmountInNumerice
            lblTranMsg.ForeColor = Drawing.Color.Red
            Exit Sub
        End If
        If txtReceived.Text <> "0.00" Then
            txtRefund.Text = CDbl(txtReceived.Text) - CDbl(txtTotalAmount.Text)
            txtRefund.Text = Math.Round(CDbl(txtRefund.Text), 2)
            txtRefund.Text = String.Format("{0:F}", CDbl(txtRefund.Text))
        End If
    End Sub
    Protected Sub sqldsRequest_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsRequest.Selected

    End Sub
    Protected Sub rbCash_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCash.CheckedChanged
        rbCash.Checked = True
        If txtReceived.Text = "0.00" Then
            txtReceived.Text = ""
        End If
        divCardManually.Style("display") = "none"
        txtReceived.Focus()
        'Me.Form.DefaultButton = imgCmdPrint.UniqueID
    End Sub
    Public Sub PosMenu()
        sqlImg.SelectCommand = objProduct.subFillPosMenu()
        sqlImg.DataBind()
    End Sub
    ' Displays POS eigther Category menu or Product Menu depends on Web.Config "ShowPrdPOSCatg" Setting.
    Protected Sub ImgPosMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgPosMenu.Click
        If (ConfigurationManager.AppSettings("ShowPrdPOSCatg").ToUpper = "TRUE") And (objPrdPOSCatg.fillPOSCatgMenu("IsCatg") <> "0") Then
            divPOSCategory.Style("display") = "block"
            prdPOSCatgMenu()
        Else
            divPOSMenu.Style("display") = "block"
            PosMenu()
        End If
    End Sub
    Protected Sub dlstImages_OnItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlstImages.ItemDataBound
        Dim imgPOSMenu As ImageButton = CType(e.Item.FindControl("imgPOSMenu"), ImageButton)
        Dim lblImag As Label = CType(e.Item.FindControl("lblImag"), Label)

        If File.Exists(ConfigurationManager.AppSettings("POSCatLocation") & lblImag.Text) Then
            imgPOSMenu.ImageUrl = ConfigurationManager.AppSettings("RelativePath") & "/" & lblImag.Text
        Else
            imgPOSMenu.ImageUrl = ConfigurationManager.AppSettings("RelativePath") & "Noimage.jpg"
        End If
        lblImag.Text = ""
        Dim lblPrdName As Label = CType(e.Item.FindControl("lblPrdName"), Label)
        lblPrdName.Text = CType(e.Item.FindControl("lblPrdName"), Label).Text
    End Sub
    Protected Sub dlstImages_OnItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs)
        If e.CommandName = "Add" Then
            subGetProductsInfo(e.CommandArgument)
            divPOSMenu.Style("display") = "block"
            divPOSMenu.Style("position") = "absolute"
            divPOSMenu.Style("top") = "85px"
            divPOSMenu.Style("left") = hdnPOSPrdMenu.Value & "px"
        End If
        If hdnPrdQty.Value = "Y" Then
            lblMsg.Text = msgPrdNotAvailableStock
            lblMsg.ForeColor = Drawing.Color.Red
            hdnPrdQty.Value = ""
            txtPopSearch.Focus()
            ' Exit Sub
        End If
        trProcess.Visible = False
    End Sub
    Protected Sub sqlImg_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqlImg.Selected
        If e.AffectedRows = 0 Then
            divPOSMenu.Style("display") = "none"
        End If
    End Sub
    Protected Sub imgCardSubmit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCardSubmit.Click
        If funGetTransaction("1") = True Then
            Response.Redirect("~/POS/Pos.aspx")
        Else
            divTransaction.Style("display") = "none"
        End If
        txtCardInfo.Text = ""
    End Sub
    Protected Sub rbEnterCardInfo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEnterCardInfo.CheckedChanged
        rbEnterCardInfo.Checked = True
        txtReceived.Text = "0.00" 'txtTotalAmount.Text
        txtRefund.Text = "0.00"
        divTransaction.Style("display") = "none"
        divCardManually.Style("display") = "block"
        txtFirstName.Focus()
    End Sub
    Private Sub subPopulateYear()
        For idx As Int16 = 0 To 11
            dlYear.Items.Add(Now.Year + idx)
        Next
    End Sub
    Private Sub subPopulateMonth()
        For idx As Int16 = 1 To 12
            If idx < 10 Then
                dlMonth.Items.Add("0" & idx)
            Else
                dlMonth.Items.Add(idx)
            End If
        Next
        dlMonth.SelectedValue = Now.ToString("MM")
    End Sub
    Protected Sub imgEnterCardManually_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEnterCardManually.Click
        If funGetTransaction(1) = True Then
            Response.Redirect("~/POS/Pos.aspx")
        Else
            divCardManually.Style("display") = "none"
        End If
        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtCardNo.Text = ""
    End Sub

    Protected Sub prdPOSCatgMenu()
        SqlDSrcPOSCatg.SelectCommand = objPrdPOSCatg.fillPOSCatgMenu()
        SqlDSrcPOSCatg.DataBind()

        'Dim dv As DataView
        'Dim datatable As DataTable

        'dv = CType(SqlDSrcPOSCatg.Select(DataSourceSelectArguments.Empty), DataView)
        'datatable = dv.Table
    End Sub

    Protected Sub dlstCatgImgs_OnItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlstCatgImgs.ItemDataBound
        Dim imgPOSCatgMenu As ImageButton = CType(e.Item.FindControl("imgPOSCatgMenu"), ImageButton)
        Dim lblPOSCatgImg As Label = CType(e.Item.FindControl("lblPOSCatgImg"), Label)

        If File.Exists(ConfigurationManager.AppSettings("POSCatLocation") & lblPOSCatgImg.Text) Then
            imgPOSCatgMenu.ImageUrl = ConfigurationManager.AppSettings("imgPathPOSCatg") & lblPOSCatgImg.Text
        Else
            imgPOSCatgMenu.ImageUrl = ConfigurationManager.AppSettings("imgPathPOSCatg") & "Noimage.jpg"
        End If
        lblPOSCatgImg.Text = ""
        Dim lblPrdPOSCatgDesc As Label = CType(e.Item.FindControl("lblPrdPOSCatgDesc"), Label)

        lblPrdPOSCatgDesc.Text = CType(e.Item.FindControl("lblPrdPOSCatgDesc"), Label).Text
    End Sub
    ' Fill POS Products for given Category
    Protected Sub dlPOSCatgProduct_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlPOSCatgProduct.ItemDataBound

        'Dim tbl As HtmlTable = CType(e.Item.FindControl("tblPOsProd"), HtmlTable)

        'tbl.BgColor = "#FFFFFF"
        'tbl.BorderColor = "#2828FF"
        'tbl.Style("border-right") = "#000000 1pt solid"
        'tbl.Style("border-top") = " #000000 1pt solid"
        'tbl.Style("border-left") = "#000000 1pt solid"
        'tbl.Style("border-bottom") = " #000000 1pt solid"
        'tbl.Style("cursor") = "pointer"
        'tbl.Width = 100
        'tbl.Height = 55
        'If (hdnchk.Value = "1") Then
        '    ' Dim lblkit As Label = CType(e.Item.FindControl("lblkit"), Label)
        '    Dim hdnkit As HiddenField = CType(e.Item.FindControl("hdnkit"), HiddenField)
        '    If (hdnkit.Value = "1") Then
        '        tbl.BgColor = "#999EFF"
        '    End If
        'End If
    End Sub
    ' Back to Category Menu  
    'Protected Sub imgCmdBackToCatg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCmdBackToCatg1.Click
    '    divPOSCatgProductMenu.Style("display") = "none"
    '    divPOSCategory.Style("display") = "block"
    'End Sub
    ' info about prd Kit
    Protected Sub imgCmdPrdKitInfo_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles imgCmdPrdKitInfo.Command
        If Session("chkKit") = "" Then
            hdnchk.Value = "1"
            Session.Add("chkKit", "true")
        Else
            hdnchk.Value = "0"
            Session.Remove("chkKit")
        End If
        'divPOSCatgProductMenu.Style("display") = "block"
        SqlDSrcPOSCatgProduct.SelectCommand = objProduct.fillPosMenuForCatg(hdnCtgID.Value)
        SqlDSrcPOSCatgProduct.DataBind()
    End Sub
    ' Add Product from Product Kit Content Popup.
    Protected Sub imgCmdAddKitPrd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCmdAddKitPrd.Click
        If (hdnCatgID.Value <> "") Then
            subGetProductsInfo(hdnCatgID.Value)
            hdnCatgID.Value = ""
            If hdnPrdQty.Value = "Y" Then
                lblMsg.Text = msgPrdNotAvailableStock
                lblMsg.ForeColor = Drawing.Color.Red
                hdnPrdQty.Value = ""
                txtPopSearch.Focus()
                Exit Sub
            End If
        End If
        Session.Add("chkKit", "")
    End Sub
    Protected Sub SqlDSrcPOSProdKitContent_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SqlDSrcPOSProdKitContent.Selected
        If e.AffectedRows = 0 Then
            lblcatprdkitmsg.Text = "No Product is available in this kit."
            lblcatprdkitmsg.Font.Bold = True
            imgCmdAddKitPrd.Visible = False
        Else
            imgCmdAddKitPrd.Visible = True
            lblcatprdkitmsg.Text = ""
        End If
    End Sub

    Protected Sub ShowNAdd(ByVal prdId As String)
        If (hdnProductKit.Value = "1" And hdnchk.Value = "1") Then
            SqlDSrcPOSProdKitContent.SelectCommand = objProduct.fillProductKitContent(prdId)
            SqlDSrcPOSProdKitContent.DataBind()
            divProductKitContent.Style("display") = "block"
            'divPOSCatgProductMenu.Style("display") = "block"
            hdnCatgID.Value = prdId

            'divPOSCatgProductMenu.Style("position") = "absolute"
            'divPOSCatgProductMenu.Style("top") = "80px"
            'divPOSCatgProductMenu.Style("left") = hdnLeft.Value & "px"

            divProductKitContent.Style("position") = "absolute"
            divProductKitContent.Style("top") = "150px"
            divProductKitContent.Style("left") = hdnkitLeft.Value & "px"
        Else
            ' divPOSCatgProductMenu.Style("display") = "none"
            subGetProductsInfo(prdId)
            If hdnPrdQty.Value = "Y" Then
                lblMsg.Text = msgPrdNotAvailableStock
                lblMsg.ForeColor = Drawing.Color.Red
                hdnPrdQty.Value = ""
                txtPopSearch.Focus()
                Exit Sub
            End If
            trProcess.Visible = False
        End If
        hdnProductID.Value = ""
        hdnProductKit.Value = ""
    End Sub
    Protected Sub CmdPrint_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdPrint.ServerClick

    End Sub

    Protected Sub CmdCancel_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdCancel.ServerClick

    End Sub

    Protected Sub cmdNoReceipt_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNoReceipt.ServerClick

    End Sub

    Protected Sub CmdTab_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdTab.ServerClick

    End Sub

    Protected Sub cmdAdd_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAdd.ServerClick

    End Sub

End Class
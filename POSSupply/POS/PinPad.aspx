<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Pinpad.aspx.vb" Inherits="POS_Pinpad" %>

<%@ Import Namespace=" Resources.Resource" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" language="JavaScript" src="../scripts/jquery.js"></script>
    <script type="text/javascript" language="JavaScript" src="./itech.js"></script>
    <script type="text/javascript" language="JavaScript" src="../scripts/akModal/akModal.js"></script>
    <script type="text/javascript" language="JavaScript" src="../scripts/akModal/dimmer.js"></script>
    <script type="text/javascript" language="JavaScript">
        var massage = "";
        var secLang = parent.secLang
        function doTransation(tranInfo, tranType, sDATA, acct, cardLang, receiptMag) {
            massage = parent.doTransation(tranInfo, tranType, sDATA, acct, cardLang, receiptMag);
            return parent.updateTransaction(sDATA, false);
        }
        function getTransactionInfo(tranType) {
            $("#cmdClose").attr("onclick", "");
            $("#cmdClose").attr("disabled", "true");
            return parent.getTransactionInfo(tranType);
        }
        function getPrintUrl() {
            return parent.getPrintUrl();
        }
        function transactionStatus(msg, status) {
            if (msg == "") {
                parent.transactionStatus(massage, status);
            } else {
                parent.transactionStatus(msg, status);
            }
            document.getElementById("cmdClose").onclick = function () {
                javascript: parent.$.akModalRemove();
            }
            $("#cmdClose").attr("disabled", "false");
            $("#cmdClose").click();
        }

        function closePopup() {
            $("#cmdClose").click();
        }
        function updateTransaction(sDATA) {
            return parent.updateTransaction(sDATA, true);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="divTransaction" runat="server" align="left" style="border: 3px; border-color: red;
            margin: 0; height: 0;">
            <asp:Panel runat="server" ID="pnlTransaction">
                <table cellpadding="0" class="popupTable" style="border-color: #CD9232;" cellspacing="0"
                    width="350px" height="150">
                    <tr style="background-color: Gold; height: 40px; border-color: Red; border: 1px;
                        border-style: solid;">
                        <% If (Request.QueryString("transactionType") = "TS") Then%>
                        <td width="97%" align="center">
                            <asp:Label CssClass="lblHeading" runat="server" ID="lblNote2" Text="<%$ Resources:Resource, lblCloseBalanceRegister %>"></asp:Label>
                        </td>
                        <%ElseIf (Request.QueryString("transactionType") = "TI") Then%>
                        <td width="97%" align="center">
                            <asp:Label CssClass="lblHeading" runat="server" ID="Label1" Text="<%$ Resources:Resource, lblInitializePinPad %>"></asp:Label>
                        </td>
                        <%Else%>
                        <td width="97%" align="center">
                            <asp:Label CssClass="lblHeading" runat="server" ID="Label2" Text="<%$ Resources:Resource, lblCardInfo %>"></asp:Label>
                        </td>
                        <%End If%>
                        <td width="3%" align="right" style="padding-right: 4px;">
                            <a name="Close" id="cmdClose" href="javascript:void(0);" onclick="javascript:parent.$.akModalRemove();">
                                <img border="0" src="../Images/ClosePopup.png" alt="<% = cmdPOSClose %>" /></a>
                        </td>
                    </tr>
                    <tr>
                        <td height="10">
                        </td>
                    </tr>
                    <tr id="Tr1" runat="server" visible="false">
                        <td align="center" colspan="2">
                            <asp:TextBox runat="server" ID="txtCardInfo" ValidationGroup="Card" TextMode="password"
                                Width="320px"></asp:TextBox><span style="color: Red; font-weight: bold; font-size: 11pt;">
                                    *</span>
                            <asp:RequiredFieldValidator ID="reqvalCardInfo" ValidationGroup="Card" runat="server"
                                ControlToValidate="txtCardInfo" ErrorMessage="<%$ Resources:Resource, reqvalCardInfo %>"
                                SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td height="10">
                            <asp:HiddenField runat="server" ID="hdnPrintUrl"></asp:HiddenField>
                            <applet id="idApplet" runat="server" code="applet.client.iTECH_Inbiz.class" archive="iTECH_Inbiz.jar"
                                width="350" height="100">
                                <param name="host" value="<%= Request.QueryString("host") %>" />
                                <param name="hostPort" value="<%= Request.QueryString("hostPort")%>" />
                                <param name="transactionType" value="<%= Request.QueryString("transactionType")%>" />
                                <param name="terminalID" value="<%= Request.QueryString("terminalID")%>" />
                                <param name="productDetail" value="<%= Request.QueryString("productDetail")%>" />
                                <param name="taxDetail" value="<%= Request.QueryString("taxDetail")%>" />
                                <param name="cmpName" value="<%= Request.QueryString("secCmpName")%>" />
                                <param name="cmpPhone" value="<%= Request.QueryString("secCmpPhone")%>" />
                                <param name="cmpAddress" value="<%= Request.QueryString("secCmpAddress")%>" />
                                <param name="regMsg" value="<%= Request.QueryString("secRegMsg")%>" />
                                <%--<param name="credit_DebitCardNumber" value="4242424242424242" />--%>
                                <% If (Request.QueryString("amount") <> "") Then%>
                                <param name="amount" value="<%= Request.QueryString("amount")%>" />
                                <% End If%>
                                <% If ConfigurationManager.AppSettings("AppletLog").ToString.ToLower = "yes" And Request.QueryString("logFile") <> "" Then%>
                                <param name="log-file" value="<%= Request.QueryString("logFile") %>" />
                                <% End If%>
                                <param name="isMrcPrint" value="<%=clsRegister.IsPrintMerchantCopry(Session("RegCode").ToString())%>" />
                            </applet>
                        </td>
                    </tr>
                    <tr>
                        <td height="2">
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
    </form>
</body>
</html>

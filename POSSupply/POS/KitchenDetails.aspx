﻿<%@ Page Title="" Language="C#" MasterPageFile="~/POSMaster.master" AutoEventWireup="true"
    CodeFile="KitchenDetails.aspx.cs" Inherits="Restaurant_Menu_KitchenDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <script type="text/javascript">
        var userID = '<%= iTECH.Library.Web.UserProfile.UserID %>';
    </script>
    <script src="Kitchen_JS.js" type="text/javascript"></script>
    <style type="text/css">
        .SpanClass
        {
            font-size: 12px;
            padding: 5px;
            color: #000;
        }
        .countButton
        {
            width: 100px;
            font-size: 15px;
            height: 33px;
            text-align: center;
            background-color: #B5E61D;
            padding: 5px;
        }
        .DispayButton
        {
            width: 100px;
            font-size: 15px;
            height: 33px;
            text-align: center;
            padding: 5px;
        }
        .OrderDetail
        {
            border: 1px solid #FDA61B;
            height: 480px;
            float: left;
            position: relative;
            margin-right: 1%;
        }
        .scroll
        {
            overflow-y: scroll;
            height: 250px;
        }
    </style>
    <div style="float: left; width: 100%;">
        <div style="float: left;" class="DispayButton">
            <div id="divNew" style="padding-top: 4px;">
            </div>
        </div>
        <div style="float: left; width: 8px; font-size: 15px; height: 33px; padding: 5px;">
        </div>
        <div style="float: left;" class="DispayButton">
            <div id="divReady" style="padding-top: 4px;">
            </div>
        </div>
        <div style="float: right; cursor: pointer;" class="countButton" onclick="ShowOrderDetails('divParentAccept',10);"
            id="divParentAccept">
            <div style="padding-top: 4px;" id="divAccept">
            </div>
        </div>
        <div style="float: right; width: 8px; font-size: 15px; height: 33px; padding: 5px;">
        </div>
        <div style="float: right; cursor: pointer;" class="countButton" onclick="ShowOrderDetails('divParentHeld',11);"
            id="divParentHeld">
            <div style="padding-top: 4px;" id="divHeld">
            </div>
        </div>
        <div style="clear: both;">
        </div>
        <div style="padding-top: 50px;">
            <div class="OrderDetail" style="margin-left: 2%; width: 31%;" id="divOrderDetails1">
            </div>
            <div class="OrderDetail" style="width: 31%;" id="divOrderDetails2">
            </div>
            <div class="OrderDetail" style="width: 31%;" id="divOrderDetails3">
            </div>
        </div>
        <div style="clear: both;">
        </div>
    </div>
</asp:Content>

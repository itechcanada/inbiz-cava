﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="ProductSearchModal_old.aspx.cs" Inherits="POS_ProductSearchModal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <script src="js/jquery.dataTables.min.js" type="text/javascript"></script>      
    <link href="js/data_table.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
          input[type=text]
          {
              width:250px;
              padding:4px 2px;
              font-size:18px;              
          }
          .dataTables_filter {width:440px;}
          .dataTables_filter label{font-size:18px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <asp:Panel ID="pnlProducts" runat="server">
         <script type="text/javascript" charset="utf-8">
             $(document).ready(function () {
                 $('#tblProducts').dataTable({
                     "bPaginate": false,
                     "bLengthChange": false,
                     "bFilter": true,
                     "bSort": false,
                     "bInfo": false,
                     "bAutoWidth": false
                 });

                 $("input:text").eq(0).focus();
             });
		</script>  
        <table id="tblProducts" border="0" cellpadding="0" cellspacing="0" class="cal_table">
            <asp:Repeater ID="rptProducts" DataSourceID="sdsProducts" runat="server">
                <HeaderTemplate>
                    <thead>
                        <tr>
                            <th style="width:50px;">
                                ProductID
                            </th>
                            <th style="width:300px;">
                                Product Name
                            </th>
                            <th style="width:120px;">
                                UPC Code
                            </th>
                            <th>
                                Product Tags
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="click_tigger" upc='<%#Eval("prdUPCCode")%>'>
                        <td valign="top" class="center">
                            <%#Eval("productID")%>
                        </td>
                        <td valign="top" class="left">
                            <%#Eval("prdName")%>
                        </td>
                        <td valign="top" class="left">
                            <%#Eval("prdUPCCode")%>
                        </td>
                        <td valign="top" class="left">
                            <%#GetProductTags(Eval("productID"))%>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </FooterTemplate>
            </asp:Repeater>
        </table>
        <asp:SqlDataSource ID="sdsProducts" runat="server" 
            ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient">
        </asp:SqlDataSource>
    </asp:Panel>
     <asp:Panel ID="pnlSession" runat="server" HorizontalAlign="Center">
         <table border="0" cellpadding="0" cellspacing="0" class="cal_table">
             <tbody>
                 <tr>
                     <td class="center">
                          <b>Session Expired!</b>       
                     </td>
                 </tr>
             </tbody>
         </table>     
         <br /><br />
         <input type="button"  value="Close" onclick="self.close();" style="width:250px;" />   
    </asp:Panel>    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        $("tr.click_tigger").click(function () {
            opener.setPrdUpcCode($(this).attr('upc'));
            self.close();
            //alert(parent.window.location);
            //self.parent.document.getElementById(parentSearchBoxID).value = $(this).attr('ups');
        });
    </script>
</asp:Content>


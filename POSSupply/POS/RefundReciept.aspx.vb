Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Data.Odbc
Imports System.Data
Imports clsCommon
Partial Class POS_RefundReciept
    Inherits BasePage
    Dim objCommon As New clsCommon
    Dim objCompany As New clsCompanyInfo
    Protected Sub POS_Print_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            If Request.QueryString("WithCard") = "no" Then
                subClose()
                Exit Sub
            End If
        End If
        Dim strLanguage As String
        ' If Request.QueryString("WithCard") = "no" Then
        strLanguage = funGetProductLang()
        'Else
        'strLanguage = Request.QueryString("LangsID").Substring(0, 2)
        'Session("UserWarehouse") = Request.QueryString("WhsID")
        'End If
        lbldate.Text = POSDate & Format(Date.Parse(Now()), "MM/dd/yyyy") & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & POSTime & Format(Date.Parse(Now()), "HH:mm:ss")
        lblTransaction.Text = Request.QueryString("TID")
        lblProIL.Text = msgTransactionID & " " & Request.QueryString("TID")
        objCompany.GetCompanyAddress()
        lblCompanyName.Text = objCompany.CompanyName
        lblCompanyPhone.Text = objCompany.CompanyPOPhone

        lblComAdd.Text = objCompany.CompanyAddressLine1 & "<BR/>" & objCompany.CompanyCity & " " & objCompany.CompanyState & "<BR/>" & objCompany.CompanyCountry & " " & objCompany.CompanyPostalCode
        lblCustCompanyName.Text = lblCompanyName.Text
        lblCustCompanyPhone.Text = lblCompanyPhone.Text
        lblCustDate.Text = lbldate.Text
        lblCustTransactionID.Text = lblTransaction.Text
        lblCustTran.Text = lblProIL.Text
        lblRegMessage.Text = Session("RegMessage")

        lblCustAdd.Text = lblComAdd.Text
        lblMerAdd.Text = lblComAdd.Text
        lblMerCompanyName.Text = lblCompanyName.Text
        lblMerCompanyPhone.Text = lblCompanyPhone.Text
        lblMerDate.Text = lbldate.Text
        lblMerTransactionID.Text = lblTransaction.Text
        lblMerTran.Text = lblProIL.Text
        lblMsgc.Text = Request.QueryString("posmsg")
        lblMsgM.Text = Request.QueryString("posmsg")
        If Request.QueryString("reprint") = "yes" Then
            trReprintP.Visible = True
            trReprint.Visible = True
            trCustReprint.Visible = True
            trDebCustReprint.Visible = True
            trDebMReprint.Visible = True
        End If

        'Debit Card
        lblDebCompanyName.Text = lblCompanyName.Text
        lblDebComAddress.Text = lblComAdd.Text
        lblDebPhone.Text = lblCompanyPhone.Text
        lblDebTranBarcode.Text = lblTransaction.Text
        lblDebMCompanyName.Text = lblCompanyName.Text
        lblDebMaddress.Text = lblComAdd.Text
        lblDebMPhone.Text = lblCompanyPhone.Text
        lblDebTransMBarcode.Text = lblTransaction.Text

        subGetPosProduct(strLanguage)
    End Sub
    Public Sub subGetPosProduct(ByVal strLang As String)
        Dim strCarName, strCardNo, strSubTotal, strTax1, strTax2, strTax3, strTax4, strTotal, strReceived, strReturn, strTaxDesc1, strTaxDesc2, strTaxDesc3, strTaxDesc4, strCardLang, strCardAccount, strCardMsg As String
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        Dim objDataClass As New clsDataClass
        strSQL = " SELECT distinct posPrdTransID,prdUPCCode,posSubTotal,posCasReceived,posCashReturned,pt.posTotalValue,pt.posTax1,pt.posTax2,pt.posTax3,pt.posTax4,posProductID,Pdes.prdname as ProductName, posPrdQty, posPrdUnitPrice,posPrdPrice, posPrdTaxCode,prdUPCCode,pt.posTax1Desc,pt.posTax2Desc,pt.posTax3Desc,pt.posTax4Desc,posCardNo,posCardName,posCardLang, posCardAccountType, posCardMsg FROM posproductno "
        strSQL += " Inner join postransaccthdr on postransaccthdr.posAcctTransID=posproductno.posPrdTransID "
        strSQL += " left join postransacctdtl on postransacctdtl.posAcctHdrDtlId=postransaccthdr.posAcctHdrID "
        strSQL += " Inner join posTransaction as pt on pt.posTransId=posproductno.posPrdTransID"
        strSQL += " Inner join Products on Products.ProductID=posproductno.posProductID"
        strSQL += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID where posPrdTransID='" & Request.QueryString("TID") & "' and Pdes.descLang ='" + strLang + "'"
        drObj = objDataClass.GetDataReader(strSQL)
        Dim intI As Integer = 0
        While drObj.Read
            Dim tempRow As New TableRow()
            For nM As Integer = 0 To 3
                Dim tempCell As New TableCell()
                tempCell.Font.Size = 7
                If nM = 0 Then
                    tempCell.Text = drObj.Item("ProductName").ToString() & "<Br />" & drObj.Item("prdUPCCode").ToString()
                    tempCell.HorizontalAlign = HorizontalAlign.Left
                    tempCell.Width = 85
                ElseIf nM = 1 Then
                    tempCell.Text = drObj.Item("posPrdQty").ToString()
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 32
                ElseIf nM = 2 Then
                    tempCell.Text = drObj.Item("posPrdUnitPrice").ToString()
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 43
                ElseIf nM = 3 Then
                    tempCell.Text = drObj.Item("posPrdPrice").ToString()
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 70
                End If

                tempRow.Cells.Add(tempCell)

                strSubTotal = String.Format("{0:F}", CDbl(drObj.Item("posSubTotal").ToString()))
                strTax1 = String.Format("{0:F}", CDbl(drObj.Item("posTax1").ToString()))
                strTax2 = String.Format("{0:F}", CDbl(drObj.Item("posTax2").ToString()))
                strTax3 = String.Format("{0:F}", CDbl(drObj.Item("posTax3").ToString()))
                strTax4 = String.Format("{0:F}", CDbl(drObj.Item("posTax4").ToString()))

                strTaxDesc1 = drObj.Item("posTax1Desc").ToString()
                strTaxDesc2 = drObj.Item("posTax2Desc").ToString()
                strTaxDesc3 = drObj.Item("posTax3Desc").ToString()
                strTaxDesc4 = drObj.Item("posTax4Desc").ToString()

                strTotal = String.Format("{0:F}", CDbl(drObj.Item("posTotalValue").ToString()))
                strReceived = String.Format("{0:F}", CDbl(drObj.Item("posCasReceived").ToString()))
                'strReturn = String.Format("{0:F}", CDbl(drObj.Item("posCashReturned").ToString()))
                strReturn = strTotal
                lblCustTotalAmount.Text = "&nbsp;&nbsp; " & strTotal
                lblMerTotalAmount.Text = "&nbsp;&nbsp; " & strTotal
            Next
            Table1.Rows.Add(tempRow)
            strCarName = drObj.Item("posCardName").ToString()
            strCardNo = drObj.Item("posCardNo").ToString()
            strCardLang = drObj.Item("posCardLang").ToString()
            strCardAccount = drObj.Item("posCardAccountType").ToString()
            strCardMsg = drObj.Item("posCardMsg").ToString()
        End While
        lblCustCardName.Text = strCarName
        lblCustCardNo.Text = strCardNo
        lblMCardName.Text = strCarName
        lblMCardNo.Text = strCardNo

        lblDebTotalAmt.Text = lblMerTotalAmount.Text
        lblDebCardNo.Text = strCardNo
        lblDebMTotalAmt.Text = lblMerTotalAmount.Text
        lblDebMCardNo.Text = strCardNo
        If strCardMsg <> "" Then
            Dim strGetvalue() As String = strCardMsg.Split("~")
            If strGetvalue.Length > 1 Then
                lblDebDateTime.Text = strGetvalue(0)
                lblReference.Text = strGetvalue(1)
                lblAuthor.Text = strGetvalue(2)
                lblDebMsg.Text = strGetvalue(3)
                lblDebMDateTime.Text = strGetvalue(0)
                lblDebMReference.Text = strGetvalue(1)
                lblDebMAuthor.Text = strGetvalue(2)
                lblDebMMsg.Text = strGetvalue(3)
            End If
        End If
        lblDebMCopyType.Text = lblMerchantCopy
        lblDebCopyType.Text = lblCustomerCopy

        lblDebAccType.Text = strCardAccount
        lblDebMAccType.Text = strCardAccount

        objDataClass.CloseDatabaseConnection()
        drObj.Close()
        subCreateTable(strSubTotal, strTax1, strTax2, strTax3, strTotal, strReceived, strReturn, strTax4, strTaxDesc1, strTaxDesc2, strTaxDesc3, strTaxDesc4)
    End Sub
    Public Sub subCreateTable(ByVal stSubtotal As String, ByVal strTax1 As String, ByVal strTax2 As String, ByVal strTax3 As String, ByVal strTotal As String, ByVal strReceived As String, ByVal strReturn As String, ByVal strTax4 As String, ByVal strTaxDesc1 As String, ByVal strTaxDesc2 As String, ByVal strTaxDesc3 As String, ByVal strTaxDesc4 As String)
        ' Create a TableHeaderRow. 
        Dim headerRow As New TableHeaderRow()
        headerRow.HorizontalAlign = HorizontalAlign.Right

        For nM As Integer = 0 To 3
            Dim headerTableCell As New TableHeaderCell()
            Dim footerTableCell As New TableCell()
            headerTableCell.Font.Size = 7
            If nM = 0 Then
                headerTableCell.Text = msgPrd
                headerTableCell.HorizontalAlign = HorizontalAlign.Left
                headerTableCell.Width = 85
            ElseIf nM = 1 Then
                headerTableCell.Text = msgQty
                headerTableCell.Width = 32
            ElseIf nM = 2 Then
                headerTableCell.Text = msgPrice
                headerTableCell.Width = 43
            ElseIf nM = 3 Then
                headerTableCell.Text = msgTotalPrice
                headerTableCell.Width = 70
            End If
            headerRow.Cells.Add(headerTableCell)
        Next
        Table1.Rows.AddAt(0, headerRow)

        Dim RowSub As New TableRow()
        For nM As Integer = 0 To 1
            Dim tempCell As New TableCell()
            tempCell.Font.Size = 7
            If nM = 0 Then
                tempCell.Text = msgSubTotal & "  -->"
                tempCell.HorizontalAlign = HorizontalAlign.Right
                tempCell.Width = 165
                tempCell.Font.Bold = True
            ElseIf nM = 1 Then
                tempCell.Text = stSubtotal
                tempCell.HorizontalAlign = HorizontalAlign.Right
                tempCell.Width = 65
            End If
            RowSub.Cells.Add(tempCell)
        Next
        tblsubTotal.Rows.Add(RowSub)


      For nR As Integer = 0 To 3
            Dim RowTax As New TableRow()
            Dim IsRow As Boolean = False
            For nM As Integer = 0 To 1
                Dim tempCell As New TableCell()
                tempCell.Font.Size = 7
                If nR = 0 Then
                    If strTax1 <> "0.00" Then
                        IsRow = True
                        If nM = 0 Then
                            tempCell.Text = strTaxDesc1 & "  -->"
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                            tempCell.Width = 165
                            tempCell.Font.Bold = True
                        ElseIf nM = 1 Then
                            tempCell.Text = strTax1
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                            tempCell.Width = 65
                        End If
                    End If
                ElseIf nR = 1 Then
                    If strTax2 <> "0.00" Then
                        IsRow = True
                        If nM = 0 Then
                            tempCell.Text = strTaxDesc2 & "  -->"
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                            tempCell.Font.Bold = True
                        ElseIf nM = 1 Then
                            tempCell.Text = strTax2
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                        End If
                    End If
                ElseIf nR = 2 Then
                    If strTax3 <> "0.00" Then
                        IsRow = True
                        If nM = 0 Then
                            tempCell.Text = strTaxDesc3 & "  -->"
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                            tempCell.Font.Bold = True
                        ElseIf nM = 1 Then
                            tempCell.Text = strTax3
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                        End If
                    End If
                ElseIf nR = 3 Then
                    If strTax4 <> "0.00" Then
                        IsRow = True
                        If nM = 0 Then
                            tempCell.Text = strTaxDesc4 & "  -->"
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                            tempCell.Font.Bold = True
                        ElseIf nM = 1 Then
                            tempCell.Text = strTax4
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                        End If
                    End If
                End If
                RowTax.Cells.Add(tempCell)
            Next
            If IsRow Then
                tblTax.Rows.Add(RowTax)
            End If
        Next

        For nR As Integer = 0 To 2
            Dim RowAmount As New TableRow()
            For nM As Integer = 0 To 1
                Dim tempCell As New TableCell()
                tempCell.Font.Size = 7
                If nR = 0 Then
                    If nM = 0 Then
                        tempCell.Text = msgTotal & "  -->"
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                        tempCell.Width = 165
                        tempCell.Font.Bold = True
                    ElseIf nM = 1 Then
                        tempCell.Text = strTotal
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                        tempCell.Width = 65
                    End If
                ElseIf nR = 1 Then
                    If nM = 0 Then
                        tempCell.Text = msgReceived & "  -->"
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                        tempCell.Font.Bold = True
                    ElseIf nM = 1 Then
                        tempCell.Text = strReceived
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                    End If
                ElseIf nR = 2 Then
                    If nM = 0 Then
                        tempCell.Text = lblPOSRefund & "  -->"
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                        tempCell.Font.Bold = True
                    ElseIf nM = 1 Then
                        tempCell.Text = strReturn
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                    End If
                End If
                RowAmount.Cells.Add(tempCell)
            Next
            tblAmount.Rows.Add(RowAmount)
        Next
    End Sub
    Private Sub subClose()
        Response.Write("<script>window.opener.location.reload();</script>")
        Response.Write("<script>window.close();</script>")
    End Sub
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        Thread.CurrentThread.CurrentUICulture = New CultureInfo(objCommon.funPrintLang(Request.QueryString("TID")))
    End Sub
End Class

Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Data.Odbc
Imports System.Data
Imports System.Web.UI
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports iTECH.Library.Utilities
Imports iTECH.InbizERP.BusinessLogic
Imports clsCommon



Partial Class POS_Refund
    Inherits BasePage
    Dim objDT As System.Data.DataTable
    Dim objDR As System.Data.DataRow
    Dim objDT_SP As System.Data.DataTable
    Dim objDR_SP As System.Data.DataRow
    Dim objTransaction As New clsPOSTransaction
    Dim objPosPrdNo As New clsPosProductNo
    Dim objPosTranAccHrd As New clsPosTransAccHdr
    Dim objTransDtl As New clsTransactionDtl
    Dim objPrint As New clsPrint
    Dim objCommon As New clsCommon
    Dim objPrdQty As New clsPrdQuantity
    Dim objProduct As New clsProducts
    Protected Sub POS_Refund_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        hdnPrintUrl.Value = ConfigurationManager.AppSettings("PrintURL") & "&TID=&WhsID=" & Session("UserWarehouse") & "&LangsID=" & funGetProductLang()
        If Session("RegCode") = "" Then
            Response.Redirect("~/Admin/selectRegister.aspx")
        End If
        CmdRefund.Attributes.Add("onclick", "javascript:return " & _
                                  "confirm('" & msgRefundAmtCorrect & "')")
        imgCash.Attributes.Add("onclick", "javascript:return " & _
                                  "confirm('" & msgRefundAmtCorrect & "')")
        imgVisa.Attributes.Add("onclick", "javascript:return " & _
                                  "confirm('" & msgRefundAmtCorrect & "')")
        imgMasterCard.Attributes.Add("onclick", "javascript:return " & _
                                  "confirm('" & msgRefundAmtCorrect & "')")
        imgAmerican.Attributes.Add("onclick", "javascript:return " & _
                                  "confirm('" & msgRefundAmtCorrect & "')")
        imgInterac.Attributes.Add("onclick", "javascript:return " & _
                                  "confirm('" & msgRefundAmtCorrect & "')")
        lblMsg.Text = ""
        If grdRequest.Rows.Count = 0 Then
            txtSubTotal.Text = 0
            txtTotal.Text = 0
            txtRefund.Text = 0
        End If
        If Not Page.IsPostBack Then
            ' If jgdvViewTran.AjaxCallBackMode <> Trirand.Web.UI.WebControls.AjaxCallBackMode.None Then
            If Request.QueryString("TType") = "P" Then
                subFillGrid()
            End If
            txtSubTotal.Text = 0
            txtTotal.Text = 0
            txtTax1.Text = 0
            txtTax2.Text = 0
            txtTax3.Text = 0
            txtTax4.Text = 0
            txtRefund.Text = 0
            txtRefundAmt.Text = 0
            subStringFormat()
            'End If
        End If
        subDisEnabled()


        If Session("MsgRe") <> "" Then
            lblTranMsg.Text = Session("MsgRe").ToString
            Session.Remove("MsgRe")
            Session.Remove("SalesCart")
        End If

        If Not IsPostBack Then
            makeCart()
        End If
        If Request.QueryString("TType") = "P" Then
            lblHeading.Text = msgPOSTabPause
        Else
            lblHeading.Text = msgPosRefund
        End If

        If Session("msg") <> "" Then
            lblTranMsg.Text = Session("msg").ToString
            Session.Remove("msg")
        End If
        If grdRequest.Rows.Count > 0 Then
            lblProIL.Visible = True
        Else
            lblProIL.Visible = False
        End If

        If Request.QueryString("Msg") <> "" Then
            If Request.QueryString("Msg").ToString.ToLower = "approved" Then
                Session.Add("msg", msgTraCompletedSuccessfully)
            Else
                Session.Add("msg", Request.QueryString("Msg").ToString)
                lblTranMsg.ForeColor = Drawing.Color.Red
            End If
            Session.Remove("transactionID")
            Session.Remove("transactionType")
            Session.Remove("PosAcctId")
            Session.Remove("amount")
            Response.Redirect("refund.aspx?TType=R")
        End If
        txtTranID.Focus()
    End Sub
    Public Sub subDisEnabled()
        rbVisa.Enabled = False
        imgVisa.Enabled = False
        rbMasterCard.Enabled = False
        imgMasterCard.Enabled = False
        rbAmerican.Enabled = False
        imgAmerican.Enabled = False
        rbInterac.Enabled = False
        imgInterac.Enabled = False
        rbCash.Enabled = False
        imgCash.Enabled = False
    End Sub
    ' Make Cart
    Sub makeCart()
        objDT = New System.Data.DataTable("Cart")
        objDT.Columns.Add("ID", GetType(Integer))
        objDT.Columns("ID").AutoIncrement = True
        objDT.Columns("ID").AutoIncrementSeed = 1

        objDT.Columns.Add("ProductID", GetType(Integer))
        objDT.Columns.Add("UPCCode", GetType(String))
        objDT.Columns.Add("ProductName", GetType(String))
        objDT.Columns.Add("Quantity", GetType(Double))
        objDT.Columns.Add("Price", GetType(Double))
        objDT.Columns.Add("TotalPrice", GetType(Double))
        objDT.Columns.Add("posTax1Desc", GetType(String))
        objDT.Columns.Add("posTax2Desc", GetType(String))
        objDT.Columns.Add("posTax3Desc", GetType(String))
        objDT.Columns.Add("posTax4Desc", GetType(String))
        objDT.Columns.Add("posTax1", GetType(Double))
        objDT.Columns.Add("posTax2", GetType(Double))
        objDT.Columns.Add("posTax3", GetType(Double))
        objDT.Columns.Add("posTax4", GetType(Double))
        'objDT.Columns.Add("PrdTax", GetType(Double))
        objDT.Columns.Add("posPrdTransID", GetType(Integer))
        objDT.Columns.Add("posPrdUnitPrice", GetType(Double))
        objDT.Columns.Add("OposTax1", GetType(Double))
        objDT.Columns.Add("OposTax2", GetType(Double))
        objDT.Columns.Add("OposTax3", GetType(Double))
        objDT.Columns.Add("OposTax4", GetType(Double))
        Session("SalesCart") = objDT
    End Sub
    'when Paused Product item
    Public Sub subGetPosProduct(ByVal strTranID As String, ByVal strUpcCode As String)
        Dim strSQL As String
        'Dim drObj As OdbcDataReader

        Dim subTotal As Double = 0
        Dim tax1SubTotal As Double = 0
        Dim tax2SubTotal As Double = 0
        Dim tax3SubTotal As Double = 0
        Dim tax4SubTotal As Double = 0

        Double.TryParse(txtSubTotal.Text, subTotal)
        Double.TryParse(txtTax1.Text, tax1SubTotal)
        Double.TryParse(txtTax2.Text, tax2SubTotal)
        Double.TryParse(txtTax3.Text, tax3SubTotal)
        Double.TryParse(txtTax4.Text, tax4SubTotal)

        Dim subTotalToRefund As Double = 0
        Dim objDataClass As New clsDataClass
        Dim taxDic As New Dictionary(Of String, Double)

        Dim SQL1 As String = "SELECT pn.posPrdTransID," + vbCrLf + _
                             "       pn.posProductID,       " + vbCrLf + _
                             "       pdesc.prdname AS ProductName," + vbCrLf + _
                             "       pn.posPrdQty,       " + vbCrLf + _
                             "       pn.posPrdUnitPrice," + vbCrLf + _
                             "       pn.posPrdPrice," + vbCrLf + _
                             "       pn.posPrdTaxCode," + vbCrLf + _
                             "       prd.prdUPCCode," + vbCrLf + _
                             "       postransaccdt.posTransID," + vbCrLf + _
                             "       pn.posTax1," + vbCrLf + _
                             "       pn.posTax1Desc," + vbCrLf + _
                             "       pn.posTax2," + vbCrLf + _
                             "       pn.posTax2Desc," + vbCrLf + _
                             "       pn.posTax3," + vbCrLf + _
                             "       pn.posTax3Desc," + vbCrLf + _
                             "       pn.posTax4," + vbCrLf + _
                             "       pn.posTax4Desc" + vbCrLf + _
                             "  FROM posproductno AS pn" + vbCrLf + _
                             "       INNER JOIN products prd" + vbCrLf + _
                             "          ON prd.ProductID = pn.posProductID" + vbCrLf + _
                             "       INNER JOIN posTransaction ptrans" + vbCrLf + _
                             "          ON ptrans.posTransId = pn.posPrdTransID" + vbCrLf + _
                             "       LEFT JOIN postransaccthdr" + vbCrLf + _
                             "          ON postransaccthdr.posAcctTransID = ptrans.posTransId" + vbCrLf + _
                             "       LEFT JOIN postransacctdtl AS postransaccdt" + vbCrLf + _
                             "          ON postransaccdt.posAcctHdrDtlId = postransaccthdr.posAcctTransID" + vbCrLf + _
                             "       INNER JOIN prddescriptions AS pdesc" + vbCrLf + _
                             "          ON pdesc.ID = prd.ProductID       " + vbCrLf + _
                             " WHERE    ptrans.posTransStatus = '1'" + vbCrLf + _
                             "       AND pdesc.descLang = '" + clsCommon.funGetProductLang + "'" + vbCrLf + _
                             "       AND pn.posPrdTransID = " + strTranID + "" + vbCrLf + _
                             "       AND prd.prdUPCCode = '" + strUpcCode + "';"

        Dim SQL2 As String = "SELECT pn.posPrdTransID," + vbCrLf + _
                            "       pn.posProductID,       " + vbCrLf + _
                            "       pdesc.prdname AS ProductName," + vbCrLf + _
                            "       pn.posPrdQty,       " + vbCrLf + _
                            "       pn.posPrdUnitPrice," + vbCrLf + _
                            "       pn.posPrdPrice," + vbCrLf + _
                            "       pn.posPrdTaxCode," + vbCrLf + _
                            "       prd.prdUPCCode," + vbCrLf + _
                            "       postransaccdt.posTransID," + vbCrLf + _
                            "       pn.posTax1," + vbCrLf + _
                            "       pn.posTax1Desc," + vbCrLf + _
                            "       pn.posTax2," + vbCrLf + _
                            "       pn.posTax2Desc," + vbCrLf + _
                            "       pn.posTax3," + vbCrLf + _
                            "       pn.posTax3Desc," + vbCrLf + _
                            "       pn.posTax4," + vbCrLf + _
                            "       pn.posTax4Desc" + vbCrLf + _
                            "  FROM posproductno AS pn" + vbCrLf + _
                            "       INNER JOIN products prd" + vbCrLf + _
                            "          ON prd.ProductID = pn.posProductID" + vbCrLf + _
                            "       INNER JOIN posTransaction ptrans" + vbCrLf + _
                            "          ON ptrans.posTransId = pn.posPrdTransID" + vbCrLf + _
                            "       LEFT JOIN postransaccthdr" + vbCrLf + _
                            "          ON postransaccthdr.posAcctTransID = ptrans.posTransId" + vbCrLf + _
                            "       LEFT JOIN postransacctdtl AS postransaccdt" + vbCrLf + _
                            "          ON postransaccdt.posAcctHdrDtlId = postransaccthdr.posAcctTransID" + vbCrLf + _
                            "       INNER JOIN prddescriptions AS pdesc" + vbCrLf + _
                            "          ON pdesc.ID = prd.ProductID       " + vbCrLf + _
                            " WHERE    ptrans.posTransStatus = '1'" + vbCrLf + _
                            "       AND pdesc.descLang = '" + clsCommon.funGetProductLang + "'" + vbCrLf + _
                            "       AND pn.posPrdTransID = " + strTranID + "" + vbCrLf + _
                            "       AND pn.posProductID IN (SELECT ProductID FROM prdtags WHERE Tag = '" + strUpcCode + "');"

        'strSQL = "SELECT posPrdTransID, posProductID,Pdes.prdname as ProductName, posPrdQty, posPrdUnitPrice,posPrdPrice, posPrdTaxCode,prdUPCCode,PtranDtl.posTransID, pn.posTax1, pn.posTax1Desc, pn.posTax2, pn.posTax2Desc, pn.posTax3,pn.posTax3Desc, pn.posTax4, pn.posTax4Desc FROM posproductno as pn "
        'strSQL += " Inner join Products on Products.ProductID=pn.posProductID"
        'strSQL += " Inner join posTransaction on posTransaction.posTransId=pn.posPrdTransID "
        'strSQL += " left join postransaccthdr on postransaccthdr.posAcctTransID =posTransaction.posTransId"
        'strSQL += " Left join postransacctdtl as PtranDtl on PtranDtl.posAcctHdrDtlId= postransaccthdr.posAcctTransID "
        'strSQL += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID where posTransStatus='1' and descLang ='" + clsCommon.funGetProductLang + "' and posPrdTransID='" + strTranID + "' and  prdUPCCode='" + strUpcCode + "'"
        'strSQL += " OR posProductID IN (SELECT ProductID FROM prdtags WHERE Tag = '" & strUpcCode & "')"
        Dim dtSearchItems As New DataTable()
        dtSearchItems = objDataClass.GetDataTable(SQL1)
        If dtSearchItems.Rows.Count = 0 Then
            dtSearchItems = objDataClass.GetDataTable(SQL2)
        End If
        objDT = Session("SalesCart")
        For Each drObj As DataRow In dtSearchItems.Rows
            For Each objDR In objDT.Rows
                If objDR("ProductID") = drObj.Item("posProductID").ToString Then
                    lblMsg.Text = msgPrsExistInlst
                    objDataClass.CloseDatabaseConnection()
                    Exit Sub
                End If
            Next

            Dim sbSQL As New StringBuilder()
            sbSQL.Append("SELECT p.PosTax1, p.PosTax1Desc, p.PosTax2, p.PosTax2Desc, p.PosTax3, p.PosTax3Desc,")
            sbSQL.Append("p.PosTax4, p.PosTax4Desc, p.posPrdPrice, p.posPrdQty FROM posproductno p")
            sbSQL.Append(" Inner join posTransaction pt on pt.posTransId = p.posPrdTransID")
            sbSQL.Append(String.Format(" WHERE pt.posoldTransID = {0}", drObj.Item("posPrdTransID").ToString))
            sbSQL.Append(String.Format(" AND p.posProductID = {0}", drObj.Item("posProductID").ToString))

            Dim dtRefund As New DataTable()
            dtRefund = objDataClass.GetDataTable(sbSQL.ToString())

            objDR = objDT.NewRow
            objDR("ProductID") = drObj.Item("posProductID").ToString
            objDR("ProductName") = drObj.Item("ProductName").ToString()
            objDR("UPCCode") = drObj.Item("prdUPCCode").ToString
            objDR("posTax1Desc") = drObj.Item("posTax1Desc").ToString
            objDR("posTax2Desc") = drObj.Item("posTax2Desc").ToString
            objDR("posTax3Desc") = drObj.Item("posTax3Desc").ToString
            objDR("posTax4Desc") = drObj.Item("posTax4Desc").ToString
            objDR("Price") = drObj.Item("posPrdUnitPrice").ToString
            objDR("posPrdTransID") = drObj.Item("posPrdTransID").ToString
            objDR("posPrdUnitPrice") = drObj.Item("posPrdUnitPrice").ToString
            

            If dtRefund.Rows.Count > 0 Then
                objDR("Quantity") = CDbl(drObj.Item("posPrdQty")) - CDbl(dtRefund.Rows(0)("posPrdQty"))
                objDR("TotalPrice") = CDbl(drObj.Item("posPrdPrice")) - CDbl(dtRefund.Rows(0)("posPrdPrice"))
                objDR("posTax1") = CDbl(drObj.Item("posTax1")) - CDbl(dtRefund.Rows(0)("posTax1"))
                objDR("posTax2") = CDbl(drObj.Item("posTax2")) - CDbl(dtRefund.Rows(0)("posTax2"))
                objDR("posTax3") = CDbl(drObj.Item("posTax3")) - CDbl(dtRefund.Rows(0)("posTax3"))
                objDR("posTax4") = CDbl(drObj.Item("posTax4")) - CDbl(dtRefund.Rows(0)("posTax4"))
                objDR("OposTax1") = CDbl(drObj.Item("posTax1")) - CDbl(dtRefund.Rows(0)("posTax1"))
                objDR("OposTax2") = CDbl(drObj.Item("posTax2")) - CDbl(dtRefund.Rows(0)("posTax2"))
                objDR("OposTax3") = CDbl(drObj.Item("posTax3")) - CDbl(dtRefund.Rows(0)("posTax3"))
                objDR("OposTax4") = CDbl(drObj.Item("posTax4")) - CDbl(dtRefund.Rows(0)("posTax4"))
            Else
                objDR("Quantity") = drObj.Item("posPrdQty").ToString
                objDR("TotalPrice") = drObj.Item("posPrdPrice").ToString
                objDR("posTax1") = drObj.Item("posTax1").ToString
                objDR("posTax2") = drObj.Item("posTax2").ToString
                objDR("posTax3") = drObj.Item("posTax3").ToString
                objDR("posTax4") = drObj.Item("posTax4").ToString
                objDR("OposTax1") = drObj.Item("posTax1").ToString
                objDR("OposTax2") = drObj.Item("posTax2").ToString
                objDR("OposTax3") = drObj.Item("posTax3").ToString
                objDR("OposTax4") = drObj.Item("posTax4").ToString
            End If


            If drObj.Item("posTax1").ToString <> 0 Then
                taxDic.Add(drObj.Item("posTax1Desc").ToString, CDbl(objDR.Item("posTax1")))
            End If
            If drObj.Item("posTax2").ToString <> 0 Then
                taxDic.Add(drObj.Item("posTax2Desc").ToString, CDbl(objDR.Item("posTax2")))
            End If
            If drObj.Item("posTax3").ToString <> 0 Then
                taxDic.Add(drObj.Item("posTax3Desc").ToString, CDbl(objDR.Item("posTax3")))
            End If
            If drObj.Item("posTax4").ToString <> 0 Then
                taxDic.Add(drObj.Item("posTax4Desc").ToString, CDbl(objDR.Item("posTax4")))
            End If

            subTotal = subTotal + CDbl(objDR.Item("TotalPrice"))
            tax1SubTotal = tax1SubTotal + CDbl(objDR.Item("posTax1"))
            tax2SubTotal = tax2SubTotal + CDbl(objDR.Item("posTax2"))
            tax3SubTotal = tax3SubTotal + CDbl(objDR.Item("posTax3"))
            tax4SubTotal = tax4SubTotal + CDbl(objDR.Item("posTax4"))

            objDT.Rows.Add(objDR)

            For Each sKey As String In taxDic.Keys
                If sKey.ToLower() = lblTax1.Text.ToLower() Or lblTax1.Text.ToLower = "" Then
                    lblTax1.Text = sKey
                    Dim tx As Double = 0
                    Double.TryParse(txtTax1.Text, tx)
                    txtTax1.Text = tx + CDbl(taxDic(sKey))
                    trTax1.Visible = True
                ElseIf sKey.ToLower() = lblTax2.Text.ToLower Or lblTax2.Text.ToLower = "" Then
                    lblTax2.Text = sKey
                    Dim tx As Double = 0
                    Double.TryParse(txtTax2.Text, tx)
                    txtTax2.Text = tx + CDbl(taxDic(sKey))
                    trTax2.Visible = True
                ElseIf sKey.ToLower() = lblTax3.Text.ToLower Or lblTax3.Text.ToLower = "" Then
                    lblTax3.Text = sKey
                    Dim tx As Double = 0
                    Double.TryParse(txtTax3.Text, tx)
                    txtTax3.Text = tx + CDbl(taxDic(sKey))
                    trTax3.Visible = True
                ElseIf sKey.ToLower() = lblTax4.Text.ToLower Or lblTax4.Text.ToLower = "" Then
                    lblTax4.Text = sKey
                    Dim tx As Double = 0
                    Double.TryParse(txtTax4.Text, tx)
                    txtTax4.Text = tx + CDbl(taxDic(sKey))
                    trTax4.Visible = True
                End If
            Next
        Next
        objDataClass.CloseDatabaseConnection()
        Session("SalesCart") = objDT
        grdRequest.DataSource = Session("SalesCart")
        grdRequest.DataBind()

        objPosTranAccHrd.AcctTransID = txtTranID.Text
        objPosTranAccHrd.subGetPostransaccthdr()
        If objPosTranAccHrd.AcctPayType = 1 Or objPosTranAccHrd.AcctPayType = 6 Then
            rbCash.Checked = True
            rbVisa.Enabled = False
            rbMasterCard.Enabled = False
            rbAmerican.Enabled = False
            imgAmerican.Enabled = False
            imgMasterCard.Enabled = False
            imgVisa.Enabled = False
            ' div1.Style("display") = "block"
        ElseIf objPosTranAccHrd.AcctPayType = 2 Then
            rbVisa.Checked = True
        ElseIf objPosTranAccHrd.AcctPayType = 3 Then
            rbMasterCard.Checked = True
        ElseIf objPosTranAccHrd.AcctPayType = 4 Then
            rbAmerican.Checked = True
        ElseIf objPosTranAccHrd.AcctPayType = 5 Then
            rbInterac.Checked = True
            'ElseIf objPosTranAccHrd.AcctPayType = 6 Then
            '    rbExactCash.Checked = True
        End If

        txtSubTotal.Text = subTotal.ToString()
        txtTotal.Text = tax1SubTotal + tax2SubTotal + tax3SubTotal + tax4SubTotal + subTotal
        txtRefund.Text = txtTotal.Text
        subStringFormat()
        If grdRequest.Rows.Count > 0 Then
            lblProIL.Visible = True
        Else
            lblProIL.Visible = False
        End If
    End Sub

    Public Sub UpdateCart()
        'Quantity, Price

        Dim subTotalToRefund As Double = 0
        Dim taxSubTotalToRefund1 As Double = 0
        Dim taxSubTotalToRefund2 As Double = 0
        Dim taxSubTotalToRefund3 As Double = 0
        Dim taxSubTotalToRefund4 As Double = 0

        For Each gRow As GridViewRow In grdRequest.Rows
            Dim objDT As DataTable = CType(Session("SalesCart"), DataTable)
            Dim taxDic As New Dictionary(Of String, Double)
            Dim leftQty As Double = 0
            Dim rightQty As Double = 0
            Dim prdID As Integer = 0
            Dim posTransID As Integer = 0

            Dim txtPQtyToRefund As TextBox = CType(gRow.FindControl("txtPQtyToRefund"), TextBox)
            Dim txtOriginalQuantity As TextBox = CType(gRow.FindControl("txtHdnPQty"), TextBox)

            Double.TryParse(txtPQtyToRefund.Text, leftQty)
            rightQty = CType(txtOriginalQuantity.Text, Double)
            prdID = CType(objDT.Rows(gRow.RowIndex)("ProductID"), Integer)
            posTransID = CType(objDT.Rows(gRow.RowIndex)("posPrdTransID"), Integer)

            If leftQty >= 0 AndAlso leftQty <= rightQty Then
                Dim drObj As DataRow = objDT.Rows(gRow.RowIndex)

                drObj("Quantity") = leftQty
                drObj("TotalPrice") = CDbl(objDT.Rows(gRow.RowIndex)("posPrdUnitPrice")) * leftQty

                If drObj.Item("OposTax1").ToString <> 0 Then
                    Dim tx1 As Double = 0
                    Double.TryParse(drObj.Item("OposTax1").ToString(), tx1)
                    tx1 = (leftQty * (tx1 / rightQty))
                    objDT.Rows(gRow.RowIndex)("posTax1") = tx1
                    taxSubTotalToRefund1 = taxSubTotalToRefund1 + tx1
                    taxDic.Add(drObj.Item("posTax1Desc").ToString, tx1)
                    'strTax.Add(drObj.Item("posTax1Desc").ToString & "@,@" & tx1.ToString)
                End If
                If drObj.Item("OposTax2").ToString <> 0 Then
                    Dim tx2 As Double = 0
                    Double.TryParse(drObj.Item("OposTax2").ToString(), tx2)
                    tx2 = (leftQty * (tx2 / rightQty))
                    objDT.Rows(gRow.RowIndex)("posTax2") = tx2
                    taxSubTotalToRefund2 = taxSubTotalToRefund2 + tx2
                    taxDic.Add(drObj.Item("posTax2Desc").ToString, tx2)
                    'strTax.Add(drObj.Item("posTax2Desc").ToString & "@,@" & tx2.ToString)
                End If
                If drObj.Item("OposTax3").ToString <> 0 Then
                    Dim tx3 As Double = 0
                    Double.TryParse(drObj.Item("OposTax3").ToString(), tx3)
                    tx3 = (leftQty * (tx3 / rightQty))
                    objDT.Rows(gRow.RowIndex)("posTax3") = tx3
                    taxSubTotalToRefund3 = taxSubTotalToRefund3 + tx3
                    taxDic.Add(drObj.Item("posTax3Desc").ToString, tx3)
                    'strTax.Add(drObj.Item("posTax3Desc").ToString & "@,@" & tx3.ToString)
                End If
                If drObj.Item("OposTax4").ToString <> 0 Then
                    Dim tx4 As Double = 0
                    Double.TryParse(drObj.Item("OposTax4").ToString(), tx4)
                    tx4 = (leftQty * (tx4 / rightQty))
                    objDT.Rows(gRow.RowIndex)("posTax4") = tx4
                    taxSubTotalToRefund4 = taxSubTotalToRefund4 + tx4
                    taxDic.Add(drObj.Item("posTax4Desc").ToString, tx4)
                    'strTax.Add(drObj.Item("posTax4Desc").ToString & "@,@" & drObj.Item("posTax4").ToString)
                End If

                subTotalToRefund = subTotalToRefund + (CDbl(objDT.Rows(gRow.RowIndex)("posPrdUnitPrice")) * leftQty)


                For Each sKey As String In taxDic.Keys
                    If sKey.ToLower() = lblTax1.Text.ToLower() Or lblTax2.Text.ToLower = "" Then
                        lblTax1.Text = sKey
                        txtTax1.Text = taxSubTotalToRefund1
                        trTax1.Visible = True
                    ElseIf sKey.ToLower() = lblTax2.Text.ToLower Or lblTax2.Text.ToLower = "" Then
                        lblTax2.Text = sKey
                        txtTax2.Text = taxSubTotalToRefund2
                        trTax2.Visible = True
                    ElseIf sKey.ToLower() = lblTax3.Text.ToLower Or lblTax3.Text.ToLower = "" Then
                        lblTax3.Text = sKey
                        txtTax3.Text = taxSubTotalToRefund3
                        trTax3.Visible = True
                    ElseIf sKey.ToLower() = lblTax4.Text.ToLower Or lblTax4.Text.ToLower = "" Then
                        lblTax4.Text = sKey
                        txtTax4.Text = taxSubTotalToRefund4
                        trTax4.Visible = True
                    End If
                Next
            End If
        Next

        txtSubTotal.Text = subTotalToRefund
        txtTotal.Text = taxSubTotalToRefund1 + taxSubTotalToRefund2 + taxSubTotalToRefund3 + taxSubTotalToRefund4 + subTotalToRefund
        txtRefund.Text = txtTotal.Text
        subStringFormat()
        If grdRequest.Rows.Count > 0 Then
            lblProIL.Visible = True
        Else
            lblProIL.Visible = False
        End If

    End Sub

    Public Sub subStringFormat()
        txtTax1.Text = String.Format("{0:F}", BusinessUtility.GetDouble(txtTax1.Text))
        txtTax2.Text = String.Format("{0:F}", BusinessUtility.GetDouble(txtTax2.Text))
        txtTax3.Text = String.Format("{0:F}", BusinessUtility.GetDouble(txtTax3.Text))
        txtTax4.Text = String.Format("{0:F}", BusinessUtility.GetDouble(txtTax4.Text))
        txtSubTotal.Text = String.Format("{0:F}", BusinessUtility.GetDouble(txtSubTotal.Text))
        txtTotal.Text = String.Format("{0:F}", BusinessUtility.GetDouble(txtTotal.Text))
        txtRefund.Text = String.Format("{0:F}", BusinessUtility.GetDouble(txtRefund.Text))
        txtRefundAmt.Text = String.Format("{0:F}", BusinessUtility.GetDouble(txtRefundAmt.Text))
    End Sub
    Public Sub subData()
        'If ConfigurationManager.AppSettings("MerchantID").ToLower = "NO" Then
        objTransaction.OldTransID = txtTranID.Text
        'Else
        'objTransaction.OldTransID = "sysNull"
        'End If
        objTransaction.TotalValue = "-" & txtTotal.Text
        objTransDtl.posTotalValue = "-" & txtTotal.Text
        objTransaction.SubTotal = "-" & txtSubTotal.Text
        objTransaction.Tax1 = "-" & txtTax1.Text
        objTransaction.Tax2 = "-" & txtTax2.Text
        objTransaction.Tax3 = "-" & txtTax3.Text
        objTransaction.Tax4 = "-" & txtTax4.Text

        objTransaction.Tax1Desc = lblTax1.Text
        objTransaction.Tax2Desc = lblTax2.Text
        objTransaction.Tax3Desc = lblTax3.Text
        objTransaction.Tax4Desc = lblTax4.Text

        'objTransaction.TransStatus = 1
        objTransaction.TransUserID = Session("UserID")
        objTransaction.TransRegCode = Session("RegCode")
        objTransaction.TransType = "R"
        objTransaction.TransDateTime = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
        Session("transactionType") = "40"
        If rbCash.Checked = True Then
            objPosTranAccHrd.AcctPayType = 1
            objTransaction.TransStatus = 1
        ElseIf rbVisa.Checked = True Then
            objPosTranAccHrd.AcctPayType = 2
            objTransDtl.posCardName = "Visa"
        ElseIf rbMasterCard.Checked = True Then
            objPosTranAccHrd.AcctPayType = 3
            objTransDtl.posCardName = "MasterCard"
        ElseIf rbAmerican.Checked = True Then
            objPosTranAccHrd.AcctPayType = 4
            objTransDtl.posCardName = "American Express"
        ElseIf rbInterac.Checked = True Then
            Session("transactionType") = "41"
            objPosTranAccHrd.AcctPayType = 5
            objTransDtl.posCardName = "Interac"
            'ElseIf rbExactCash.Checked = True Then
            '    objPosTranAccHrd.AcctPayType = 6
            '    objTransaction.TransStatus = 1
        End If
        Session("amount") = objTransaction.TotalValue
        objPosTranAccHrd.CasReceived = txtTotal.Text
        'If rbCash.Checked = True Then
        '    objPosTranAccHrd.CashReturned = txtTotal.Text
        'Else
        objPosTranAccHrd.CashReturned = 0
        'End If
    End Sub
    'for Refund
    Public Function funRefund() As String
        Dim strTranID As String
        objTransaction.funInsertPosTransaction(strTranID)
        objPosPrdNo.PrdTransID = strTranID
        Dim intI As Integer
        Dim objTblPrdNo As DataTable
        Dim objRowPrdNo As DataRow
        objTblPrdNo = Session("SalesCart")
        For intI = 0 To objTblPrdNo.Rows.Count - 1
            objRowPrdNo = objTblPrdNo.Rows(intI)
            objPosPrdNo.ProductID = objRowPrdNo("ProductID")
            objPosPrdNo.PrdQty = objRowPrdNo("Quantity")
            objPosPrdNo.PrdUnitPrice = objRowPrdNo("Price")
            objPosPrdNo.PrdPrice = objRowPrdNo("TotalPrice")

            objPosPrdNo.Tax1 = objRowPrdNo("posTax1")
            objPosPrdNo.Tax2 = objRowPrdNo("posTax2")
            objPosPrdNo.Tax3 = objRowPrdNo("posTax3")
            objPosPrdNo.Tax4 = objRowPrdNo("posTax4")
            objPosPrdNo.Tax1Desc = objRowPrdNo("posTax1Desc")
            objPosPrdNo.Tax2Desc = objRowPrdNo("posTax2Desc")
            objPosPrdNo.Tax3Desc = objRowPrdNo("posTax3Desc")
            objPosPrdNo.Tax4Desc = objRowPrdNo("posTax4Desc")

            objPosPrdNo.PrdTaxCode = objTransaction.funPrdTax(objRowPrdNo("ProductID"), Session("UserWarehouse"))
            objPosPrdNo.funInsertPosProduct()

            ''If objPosTranAccHrd.AcctPayType = 1 Or (ConfigurationManager.AppSettings("MerchantID").ToLower = "No" And (objPosTranAccHrd.AcctPayType = "2" Or objPosTranAccHrd.AcctPayType = "3" Or objPosTranAccHrd.AcctPayType = "4" Or objPosTranAccHrd.AcctPayType = "5")) Then
            If objProduct.funGetPrdValue(objPosPrdNo.ProductID, "kit") = "1" Then
                objPrdQty.subGetPrdKitOnHandQty(objPosPrdNo.ProductID, Session("UserWarehouse"), objPosPrdNo.PrdQty, "R")
                objPrdQty.updateProductQuantity(objPosPrdNo.ProductID, Session("UserWarehouse"), objPosPrdNo.PrdQty, "R")
            Else
                objPrdQty.updateProductQuantity(objPosPrdNo.ProductID, Session("UserWarehouse"), objPosPrdNo.PrdQty, "R")
                objPrdQty.funGetReducePrdKitQty(objPosPrdNo.ProductID, Session("UserWarehouse"), "R") ' reduce prd kit Qty
            End If
            ''End If
        Next
        objPosTranAccHrd.AcctTransID = strTranID
        If IsNumeric(txtRefundAmt.Text) = False Then
            Return strTranID
        End If
        If (Session("UserModules").ToString.Contains("ADM") = True Or Session("UserModules").ToString.Contains("PSMGR") = True) And CDbl(txtRefundAmt.Text > 0) Then

            objPosPrdNo.ProductID = "1900500910"
            objPosPrdNo.PrdQty = "1"
            objPosPrdNo.PrdUnitPrice = txtRefundAmt.Text
            objPosPrdNo.PrdPrice = txtRefundAmt.Text

            objPosPrdNo.Tax1 = 0
            objPosPrdNo.Tax2 = 0
            objPosPrdNo.Tax3 = 0
            objPosPrdNo.Tax4 = 0
            objPosPrdNo.Tax1Desc = "sysNull"
            objPosPrdNo.Tax2Desc = "sysNull"
            objPosPrdNo.Tax3Desc = "sysNull"
            objPosPrdNo.Tax4Desc = "sysNull"

            objPosPrdNo.PrdTaxCode = "1900500910"
            objPosPrdNo.funInsertPosProduct()

        End If

        Return strTranID
    End Function
    Public Sub subAddPrdQtyForCreditCard()
        Dim intI As Integer
        Dim objTblPrdNo As DataTable
        Dim objRowPrdNo As DataRow
        objTblPrdNo = Session("SalesCart")
        For intI = 0 To objTblPrdNo.Rows.Count - 1
            objRowPrdNo = objTblPrdNo.Rows(intI)
            If objPosTranAccHrd.AcctPayType <> 1 Then
                If objProduct.funGetPrdValue(objPosPrdNo.ProductID, "kit") = "1" Then
                    objPrdQty.subGetPrdKitOnHandQty(objRowPrdNo("ProductID"), Session("UserWarehouse"), objRowPrdNo("Quantity"), "R") 'reduce kit's product Qty
                    objPrdQty.updateProductQuantity(objRowPrdNo("ProductID"), Session("UserWarehouse"), objRowPrdNo("Quantity"), "R") ' reduce prd kit Qty
                Else
                    objPrdQty.updateProductQuantity(objRowPrdNo("ProductID"), Session("UserWarehouse"), objRowPrdNo("Quantity"), "R")
                    objPrdQty.funGetReducePrdKitQty(objRowPrdNo("ProductID"), Session("UserWarehouse"), "R") ' reduce prd kit Qty
                End If
            End If
        Next
    End Sub
    Public Sub subFillGrid()
        'sqlsdviewTran.SelectCommand = objTransaction.subFillTransaction(Request.QueryString("TType"))
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    'Protected Sub grdvViewTran_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdvViewTran.PageIndexChanging
    '    subFillGrid()
    'End Sub
    'Protected Sub grdvViewTran_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdvViewTran.RowDataBound
    '    If (e.Row.RowType = DataControlRowType.DataRow) Then
    '        Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
    '        imgDelete.Attributes.Add("onclick", "javascript:return " & _
    '        "confirm(" & msgConformDeleteTransaction & ")")
    '        If e.Row.Cells(5).Text = 9 Then
    '            e.Row.Cells(5).Text = msgPaused
    '        ElseIf e.Row.Cells(5).Text = 0 Then
    '            e.Row.Cells(5).Text = msgFailed
    '        ElseIf e.Row.Cells(5).Text = 1 Then
    '            e.Row.Cells(5).Text = msgSuccessfullyCompleted
    '        End If
    '        If (e.Row.RowType = DataControlRowType.DataRow) Then
    '            e.Row.Cells(3).Text = String.Format("{0:F}", CDbl(e.Row.Cells(3).Text))
    '            e.Row.Cells(4).Text = String.Format("{0:F}", CDbl(e.Row.Cells(4).Text))
    '        End If
    '    End If
    'End Sub
    'Protected Sub grdvViewTran_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdvViewTran.RowDeleting
    '    Dim strTranID As String = grdvViewTran.DataKeys(e.RowIndex).Value.ToString()
    '    lblMsg.Text = msgTarnDelete
    '    sqlsdviewTran.DeleteCommand = objTransaction.funTransactionDelete(strTranID) 'Delete Product
    '    subFillGrid()
    'End Sub
    'Protected Sub grdvViewTran_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdvViewTran.RowEditing
    '    Dim strTranID As String = grdvViewTran.DataKeys(e.NewEditIndex).Value.ToString()
    '    Response.Redirect("POS.aspx?TID=" & strTranID & "&TType=" & Request.QueryString("TType"))
    'End Sub
    'Protected Sub grdvViewTran_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdvViewTran.Sorting
    '    subFillGrid()
    'End Sub
    Protected Sub imgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
        If txtUpcCode.Text <> "" And txtTranID.Text <> "" Then
            Dim strTranID As String = funRemove(txtTranID.Text)
            Dim strUpc As String = funRemove(txtUpcCode.Text)
            If objTransaction.ValidateTransactionID(strTranID, strUpc) Then
                subGetPosProduct(strTranID, strUpc)
            Else
                lblMsg.Text = msgPrdNotFound
            End If
        Else
            lblMsg.Text = msgEnterReceiptNoAndPrdUPC
        End If
    End Sub
    Public Function funCardTransaction(ByVal strTransactionID As String) As Boolean
        Dim strTranDtlStatus As String
        objTransDtl.posTransEndTime = "sysNull"
        objTransDtl.posTransID = "sysNull"
        objTransDtl.posTranAcctType = "refund"
        objTransDtl.posTransStartDatetime = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
        'objTransDtl.posCardNo = "sysNull"
        objTransDtl.posCardExpDate = "sysNull"
        objTransDtl.posTransAcctDtlStatus = 0
        strTranDtlStatus = 0
        objTransDtl.subGetDataForRefund(Session("TID"))
        Dim strPosAcctId As String
        objTransDtl.funInsertPosTransaction(strPosAcctId) 'postransacctdtl table
        Dim strPost As String
        Dim strResponse As String
        If ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "msi" Then
            strPost = funMSI()
        ElseIf ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "vm" Then
            strPost = funVM()
        ElseIf ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "tr" Then
            'Session("transactionType") = "40"
            Session("PosAcctId") = strPosAcctId
            'Exit Function 'Commented By Hitendra ON 8th Feb 2012
        End If

        'Following 3 lines of code added to allow directly set refund
        objTransaction.TransStatus = 1
        objTransaction.OldTransID = txtTranID.Text
        objTransaction.funUpdateTransStaus(strTransactionID, "id")
        Return True

        strResponse = funGetResponseToCard(strPost)

        'Condition when Tran Failed or Success
        If funPopulateObject(strResponse, strPosAcctId) = True Then
            objTransaction.TransStatus = 1
            objTransaction.OldTransID = txtTranID.Text
            objTransaction.funUpdateTransStaus(strTransactionID, "id")
            Return True
        Else
            objTransaction.TransStatus = 0
            objTransaction.funUpdateTransStaus(strTransactionID)
            Return False
        End If
    End Function
    'Do Request to Credit card Transaction(Federated_Canada_API.pdf)
    Public Function funMSI() As String
        Dim strPost As String
        strPost = ConfigurationManager.AppSettings("MSIPostURL") & _
              "username=" & ConfigurationManager.AppSettings("MSIUserID") & "&password=" & ConfigurationManager.AppSettings("MSIUserPassword") & _
              "&type=refund&amount=" & objTransaction.TotalValue & "&transactionid=" & Session("TID")
        Return strPost
    End Function
    'Do Request to Credit card Transaction(Virtual Merchant)
    Public Function funVM() As String
        Dim strPost As String
        Dim strMID As String = Session("RegMerchantID")
        'strPost = ConfigurationManager.AppSettings("VMPostURL") & "<txn><ssl_merchant_ID>" & strMID & "</ssl_merchant_ID><ssl_user_id>" & ConfigurationManager.AppSettings("VMUserID") & "</ssl_user_id><ssl_pin>" & ConfigurationManager.AppSettings("VMUserPassword") & "</ssl_pin><ssl_transaction_type>ccsale</ssl_transaction_type><ssl_card_number>" & strCreditCardNo & "</ssl_card_number><ssl_exp_date>" & strExpiryDate & "</ssl_exp_date><ssl_amount>" & objTransaction.TotalValue & "</ssl_amount></txn>"
        Return strPost
    End Function
    'For Get Response
    Public Function funGetResponseToCard(ByVal strPost As String) As String
        Dim strNewValue As String
        Dim strResponse As String
        ' Create the request obj 
        Dim req As System.Net.HttpWebRequest = DirectCast(System.Net.WebRequest.Create(strPost), System.Net.HttpWebRequest)
        ' Set values for the request back 
        req.Method = "POST"
        req.ContentType = "text/xml"
        Dim stOut As New StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII)
        stOut.Write(strNewValue)
        stOut.Close()
        ' Do the request to get the response 
        Dim stIn As New StreamReader(req.GetResponse().GetResponseStream())
        strResponse = stIn.ReadToEnd()
        stIn.Close()
        Return strResponse
    End Function
    Public Function funPopulateObjToMSI(ByVal strString As String, ByRef strResponseText As String) As String
        Dim strResponse, strAuthcode, strTransactionid As String
        Dim strAvsresponse, strCvvresponse, strOrderid, strResponse_code As String
        Dim str() As String = strString.ToLower.Split("&")
        For i As Int16 = 0 To str.Length - 1
            If str(i).Contains("avsresponse=") Then
                strAvsresponse = str(i).Substring(str(i).LastIndexOf("=") + 1)
            ElseIf str(i).Contains("cvvresponse=") Then
                objTransDtl.posCVVCode = str(i).Substring(str(i).LastIndexOf("=") + 1)
            ElseIf str(i).Contains("response=") Then
                strResponse = str(i).Substring(str(i).LastIndexOf("=") + 1)
            ElseIf str(i).Contains("responsetext=") Then
                strResponseText = str(i).Substring(str(i).LastIndexOf("=") + 1)
            ElseIf str(i).Contains("authcode=") Then
                strAuthcode = str(i).Substring(str(i).LastIndexOf("=") + 1)
            ElseIf str(i).Contains("transactionid=") Then
                objTransDtl.posTransID = str(i).Substring(str(i).LastIndexOf("=") + 1)
            ElseIf str(i).Contains("orderid=") Then
                strOrderid = str(i).Substring(str(i).LastIndexOf("=") + 1)
            ElseIf str(i).Contains("response_code=") Then
                objTransDtl.posExtReturnStatus = str(i).Substring(str(i).LastIndexOf("=") + 1)
            End If
        Next
        Return strResponse
    End Function
    Public Function funPopulateObjToVM(ByVal strString As String, ByRef strResponseText As String) As String
        Dim strResponse, strAuthcode, strTransactionid As String
        Dim xmlNodelist As XmlNodeList
        Dim xmlListItem As New System.Xml.XmlDocument
        Dim xmlNode As XmlNode
        Try
            xmlListItem.LoadXml(strString)
        Catch ex As Exception
            Exit Function
        End Try
        xmlNodelist = xmlListItem.SelectNodes("txn")
        For Each xmlNode In xmlNodelist
            For intI As Int16 = 0 To xmlNode.ChildNodes.Count - 1
                If xmlNode.ChildNodes(intI).Name.ToLower = "errorcode" Then
                    strResponse = 0
                    objTransDtl.posExtReturnStatus = xmlNode.ChildNodes(intI).InnerText
                    objTransDtl.posTransID = 0
                    strResponseText = xmlNode.ChildNodes(2).InnerText
                    Exit For
                Else
                    strResponse = 1
                    If xmlNode.ChildNodes(intI).Name.ToLower = "ssl_txn_id" Then
                        objTransDtl.posTransID = xmlNode.ChildNodes(intI).InnerText
                    ElseIf xmlNode.ChildNodes(intI).Name.ToLower = "ssl_approval_code" Then
                        objTransDtl.posExtReturnStatus = xmlNode.ChildNodes(intI).InnerText
                    ElseIf xmlNode.ChildNodes(intI).Name.ToLower = "ssl_cvv2_response" Then
                        objTransDtl.posCVVCode = xmlNode.ChildNodes(intI).InnerText
                    End If
                End If
            Next
        Next
        Return strResponse
    End Function
    Private Function funPopulateObject(ByVal strString As String, ByVal strPosAcctId As String) As Boolean
        Dim strResponseText, strResponse As String
        If ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "msi" Then
            strResponse = funPopulateObjToMSI(strString, strResponseText)
        ElseIf ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "vm" Then
            strResponse = funPopulateObjToVM(strString, strResponseText)
        End If

        If strResponse = "" Then
            Return False
        End If

        If strResponse = "1" Then
            objTransDtl.posTransAcctDtlStatus = "1"
        Else
            objTransDtl.posTransAcctDtlStatus = "9"
        End If
        objTransDtl.posTransEndTime = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
        objTransDtl.funUpdateTransaction(strPosAcctId)

        If strResponse = "1" Then
            objTransDtl.posTransAcctDtlStatus = "1"
            Return True
        Else
            objTransDtl.posTransAcctDtlStatus = "9"
            lblTranMsg.Text = strResponseText
            lblTranMsg.ForeColor = Drawing.Color.Red
            Return False
        End If
    End Function
    Protected Sub grdRequest_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdRequest.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            txtRefundAmt.Enabled = False
            'e.Row.Cells(4).Text = String.Format("{0:F}", CDbl(e.Row.Cells(4).Text))
            e.Row.Cells(5).Text = String.Format("{0:F}", CDbl(e.Row.Cells(5).Text))
        End If
    End Sub    
    Public Function funRefundProduct() As Boolean

        Dim strTranAccHrdID As String ' for postransaccthdr table
        Dim strTranID As String ' for posTransaction table

        If objPosTranAccHrd.AcctPayType <> 1 And objPosTranAccHrd.AcctPayType <> 6 And ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" Then
            objTransaction.TransStatus = 2
        Else
            objTransaction.TransStatus = 1
        End If
        objTransaction.PosGiftReason = String.Empty
        objTransaction.PosAuthorizedBy = String.Empty
        strTranID = funRefund()
        Session("transactionID") = strTranID

        If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" And (objPosTranAccHrd.AcctPayType = "2" Or objPosTranAccHrd.AcctPayType = "3" Or objPosTranAccHrd.AcctPayType = "4" Or objPosTranAccHrd.AcctPayType = "5") Then
            If ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "tr" Then
                trTransaction.Visible = True
                hdnPrintUrl.Value = ConfigurationManager.AppSettings("PrintURL") & "&TID=" & Session("transactionID") & "&WhsID=" & Session("UserWarehouse") & "&LangsID=" & funGetProductLang()
                divTransaction.Style("display") = "block"
            End If

        End If

        objPosTranAccHrd.funInsertpostransaccthdr(strTranAccHrdID)
        objTransDtl.posAcctHdrDtlId = strTranAccHrdID
        'Transaction by Card
        If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" And (objPosTranAccHrd.AcctPayType = "2" Or objPosTranAccHrd.AcctPayType = "3" Or objPosTranAccHrd.AcctPayType = "4" Or objPosTranAccHrd.AcctPayType = "5") Then
            If ConfigurationManager.AppSettings("CreditCardAPI").ToLower = "tr" Then
                If funCardTransaction(strTranID) = False Then
                    Return False
                Else
                    subAddPrdQtyForCreditCard()
                End If
            End If

        End If

        If (Session("UserModules").ToString.Contains("ADM") = True Or Session("UserModules").ToString.Contains("PSMGR") = True) And txtRefundAmt.Text <> "" And txtRefundAmt.Text <> "0.00" Then
            Session.Add("msgRe", msgTraCompletedSuccessfully)
            Response.Redirect("refund.aspx?TType=R")
        Else
            Session.Remove("TID")
            Session.Add("msgRe", msgTraCompletedSuccessfully)
            If Request.Browser.Browser.ToString() = "IE" Then
                Response.Redirect("~/POS/print.aspx?TID=" & strTranID & "&status=1")
            Else
                Response.Write("<script>window.open('print.aspx?TID=" & strTranID & "&status=1', 'print','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');</script>")
                Response.Write("<script>location.href='Refund.aspx?TType=R';</script>")
            End If
        End If
        Return True
    End Function

    Protected Sub CmdRefund_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdRefund.ServerClick

        subData()
        If rbCash.Enabled = False And hdnVal.Value = "" And CDbl(txtRefundAmt.Text) <= 0 Then
            If grdRequest.Rows.Count = 0 Then
                lblTranMsg.Text = msgPlsAddPrd
                lblTranMsg.ForeColor = Drawing.Color.Red
                Exit Sub
            End If
        Else
            If grdRequest.Rows.Count = 0 Then
                If hdnVal.Value = "" Then
                    objPosTranAccHrd.AcctPayType = 0
                Else
                    objPosTranAccHrd.AcctPayType = hdnVal.Value
                End If
                If funPosManRefund() = False Then
                    hdnVal.Value = ""
                    subEnableCtrl()
                    Exit Sub
                End If
            Else
                Exit Sub
            End If
        End If
        If funRefundProduct() = False Then
            hdnVal.Value = ""
            Exit Sub
        End If
    End Sub
    Public Sub subEnableCtrl()
        rbCash.Enabled = True
        imgCash.Enabled = True
        rbVisa.Enabled = True
        imgVisa.Enabled = True
        rbMasterCard.Enabled = True
        imgMasterCard.Enabled = True
        rbAmerican.Enabled = True
        imgAmerican.Enabled = True
        rbInterac.Enabled = True
        imgInterac.Enabled = True
    End Sub
    Public Function funPosManRefund() As Boolean
        If (Session("UserModules").ToString.Contains("ADM") = True Or Session("UserModules").ToString.Contains("PSMGR") = True) Then
            If IsNumeric(txtRefundAmt.Text) = False Then
                lblTranMsg.Text = msgRefundAmt
                lblTranMsg.ForeColor = Drawing.Color.Red
                Return False
            End If
            If CDbl(txtRefundAmt.Text) <= 0 Then
                lblTranMsg.Text = msgRefundAmtgreater
                lblTranMsg.ForeColor = Drawing.Color.Red
                Return False
            End If
            If objPosTranAccHrd.AcctPayType = "" Or objPosTranAccHrd.AcctPayType = "0" Then
                lblTranMsg.Text = POScusvalPaymentBy
                lblTranMsg.ForeColor = Drawing.Color.Red
                Return False
            End If
            objPosTranAccHrd.CasReceived = "-" & txtRefundAmt.Text
            objTransDtl.posTotalValue = "-" & txtRefundAmt.Text
            objTransaction.TotalValue = "-" & txtRefundAmt.Text
            Session("amount") = objTransaction.TotalValue
            objTransaction.SubTotal = "-" & txtRefundAmt.Text
        End If
        Return True
    End Function

    Protected Sub imgCash_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCash.Click
        objPosTranAccHrd.AcctPayType = "1"
        subData()
        If funPosManRefund() = False Then
            Exit Sub
        End If
        If funRefundProduct() = False Then
            Exit Sub
        End If
    End Sub

    Protected Sub imgInterac_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgInterac.Click
        objPosTranAccHrd.AcctPayType = "5"
        subData()
        If funPosManRefund() = False Then
            Exit Sub
        End If
        If funRefundProduct() = False Then
            Exit Sub
        End If
    End Sub

    Protected Sub imgMasterCard_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgMasterCard.Click
        objPosTranAccHrd.AcctPayType = "3"
        subData()
        If funPosManRefund() = False Then
            Exit Sub
        End If
        If funRefundProduct() = False Then
            Exit Sub
        End If
    End Sub

    Protected Sub imgVisa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgVisa.Click
        objPosTranAccHrd.AcctPayType = "2"
        subData()
        If funPosManRefund() = False Then
            Exit Sub
        End If
        If funRefundProduct() = False Then
            Exit Sub
        End If
    End Sub

    Protected Sub imgAmerican_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAmerican.Click
        objPosTranAccHrd.AcctPayType = "4"
        subData()
        If funPosManRefund() = False Then
            Exit Sub
        End If
        If funRefundProduct() = False Then
            Exit Sub
        End If
    End Sub
    Protected Sub sqldsRequest_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsRequest.Selected
        If e.AffectedRows = 0 Then
            txtRefundAmt.ReadOnly = False
        Else
            txtRefundAmt.ReadOnly = True
        End If
    End Sub

    Protected Sub grdRequest_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdRequest.RowCommand
        If e.CommandName = "UpdateRefund" Then
            UpdateCart()
        End If
    End Sub

    Protected Sub jgdvViewTran_DataRequesting(sender As Object, e As Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs) Handles jgdvViewTran.DataRequesting
        If Request.QueryString.AllKeys.Contains("_history") Then
            Dim statusTypeID As String = Request.QueryString(txtSearch.ClientID)
            If statusTypeID = "12" Then
                statusTypeID = "12','13"
            End If
            sqldsViewTran.SelectCommand = objTransaction.subFillTransactionByStatusTypeID(Request.QueryString(txtConfirmCode.ClientID), statusTypeID)
        Else
            sqldsViewTran.SelectCommand = objTransaction.subFillTransactionByStatusTypeID("", "12','13")
        End If
    End Sub

    Protected Sub jgdvViewTran_DataBinding(sender As Object, e As Trirand.Web.UI.WebControls.JQGridCellBindEventArgs) Handles jgdvViewTran.DataBinding
        If e.ColumnIndex = 6 Then
            e.CellHtml = GetStatusIntToString(e.CellHtml)
        End If
        If e.ColumnIndex = 7 Then
            Dim statusTypeID As String
            statusTypeID = e.CellHtml
            If statusTypeID = RESTAURANTMENU_ORDERSTATUS.PICKEDUP Then
                e.CellHtml = BusinessUtility.GetDateTime(e.RowValues(2)).ToString("dd-MM-yyyy HH:mm")
            Else
                e.CellHtml = "<a class='popEditStatus' style='font-weight:bold;' href='TransitionStatusChange.aspx?cmd=TransitionStatus&keys=" + e.RowKey + "&status=13&jscallback=reloadGrid'>" + Resources.Resource.lblPickedUp + "</a>"
            End If
        End If
        If e.ColumnIndex = 2 Then
            e.CellHtml = BusinessUtility.GetDateTime(e.CellHtml).ToString("dd-MM-yyyy HH:mm")
        End If
        If e.ColumnIndex = 8 Then
            If BusinessUtility.GetInt(e.RowValues(3)) = 12 Then
                e.CellHtml = String.Format("<a href='POS.aspx?TID={0}&TType={1}' ><b>{2}</b></a>", e.RowKey, Request.QueryString("TType"), Resources.Resource.grdvContinue)
            Else
                e.CellHtml = "<a href='javascript:;' onclick='ShowStatus(""" + GetStatusIntToString(e.RowValues(3)) + """);' ><b>" + Resources.Resource.grdvContinue + "</b></a>"
            End If
        End If
        If e.ColumnIndex = 9 Then
            e.CellHtml = "<a class='pop_delete' style='font-weight:bold;' href='../Admin/delete.aspx?cmd=Transition&keys=" + e.RowKey + "&jscallback=reloadGrid'>" + Resources.Resource.grdvDelete + "</a>"
            ''e.CellHtml = "<a class='pop_delete' href='../Admin/delete.aspx?cmd=Transition&keys=' > DELETE</a>"
        End If
    End Sub    
    Function GetStatusIntToString(statusCode As String) As String
        Dim statusTypeID As Int16
        statusTypeID = BusinessUtility.GetInt(statusCode)
        Dim statusType As String = ""
        If statusTypeID = RESTAURANTMENU_ORDERSTATUS.NEW Then
            statusType = Resources.Resource.lblNew          
        Else
            If statusTypeID = RESTAURANTMENU_ORDERSTATUS.ACCEPT Then
                statusType = Resources.Resource.lblAccept
            Else
                If statusTypeID = RESTAURANTMENU_ORDERSTATUS.HELD Then
                    statusType = Resources.Resource.lblHeld                    
                Else
                    If statusTypeID = RESTAURANTMENU_ORDERSTATUS.READY Then
                        statusType = Resources.Resource.lblReady                        
                    Else
                        If statusTypeID = RESTAURANTMENU_ORDERSTATUS.PICKEDUP Then
                            statusType = Resources.Resource.lblPickedUp
                        ElseIf statusTypeID = 1 Then
                            statusType = Resources.Resource.msgSuccessfullyCompleted
                        End If
                    End If
                End If
            End If
        End If
        Return statusType
    End Function   
End Class

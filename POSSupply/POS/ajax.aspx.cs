﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Newtonsoft.Json;
using System.Web.Services;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;
using System.Data;
using System.Text;


public partial class POS_ajax : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static object[] GetGuest(string guestid)
    {
        object[] arrData = new object[2];
        Partners cu = new Partners();
        try
        {
            CustomerCredit objCustCredit = new CustomerCredit();
            double dblCustomerCredit = BusinessUtility.GetDouble(objCustCredit.GetAvailableCredit(BusinessUtility.GetInt(guestid)));
            cu.PopulateObject(BusinessUtility.GetInt(guestid));
            arrData[0] = cu;
            arrData[1] = dblCustomerCredit;
            //arrData[1] = cu.GetBillToAddress(cu.PartnerID, cu.PartnerType);
        }
        catch
        {
            arrData[0] = cu;
            arrData[1] = 0;
            //arrData[1] = new Addresses();
        }
        return arrData;
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static object[] GetLaywayOrderDetail(string orderid)
    {
        object[] arrData = new object[5];
        Orders objOrd = new Orders();
        try
        {
            objOrd.PopulateObject(BusinessUtility.GetInt(orderid));

            if (objOrd.OrdID > 0 && objOrd.OrderTypeCommission == (int)OrderCommission.POS && objOrd.OrdStatus == SOStatus.APPROVED)
            {
                arrData[0] = 1; // To Success
                arrData[1] = objOrd.OrdCustID;
                

                StringBuilder sbHtml = new StringBuilder();

                #region itemHtml
                //OrderItems objOrdItems = new OrderItems();

                DbHelper dbHelp = new DbHelper(true);
                try
                {
                    dbHelp.Connection.Open(); //Make sure to open connection first to optimize performance
                    OrderItems ordItems = new OrderItems();
                    DataTable dt = ordItems.GetOrderItemsTable(dbHelp, objOrd.OrdID);
                    foreach (DataRow r in dt.Rows)
                    {
                        int prId = BusinessUtility.GetInt(r["ordProductID"]);
                        string upcCode = BusinessUtility.GetString(r["prdUPCCode"]);
                        int iTaxGrp = BusinessUtility.GetInt(r["ordProductTaxGrp"]);
                        string sTaxGrpDesc = BusinessUtility.GetString(r["sysTaxCodeDescText"]);
                        string pName = BusinessUtility.GetString(r["prdName"]);
                        int pQty = BusinessUtility.GetInt(r["ordProductQty"]);
                        double dblPrice = BusinessUtility.GetDouble(r["InOrdProductUnitPrice"]);
                        int pDiscount = BusinessUtility.GetInt(r["ordProductDiscount"]);
                        double dblAmount = BusinessUtility.GetDouble(r["amount"]);
                        sbHtml.Append("<tr style='color:#333333;' id='" + prId + "' onclick = 'updateQuantityGrid(" + prId + ")' > ");
                        sbHtml.Append("<td align='left' id='prUPC_" + prId + "' style='width:120px;'>" + upcCode + "<input type='hidden' id='hdn_PTaxID_" + prId + "'  value='" + iTaxGrp + "' /> <input type='hidden' id='hdn_PTaxValue_" + prId + "'  value='" + "" + "' /><input type='hidden' id='hdn_GiftCardNo_" + prId + "'  value='' />   </td>");
                        sbHtml.Append("<td align='left' id='prName_" + prId + "' style='width:250px;'>" + pName + "</td> ");
                        sbHtml.Append("<td align='right' style='width:80px;' id='quantity_" + prId + "'>" + pQty + "</td> ");
                        sbHtml.Append("<td align='right' style='width:80px;'id='unit_price_" + prId + "'>" + string.Format("{0:F}", dblPrice) + "</td>");
                        sbHtml.Append("<td align='right' style='width:80px;'id='p_disc_" + prId + "'>" + pDiscount + "</td>");
                        sbHtml.Append("<td align='right' style='width:80px;'id='p_Tax_" + prId + "'>" + sTaxGrpDesc + " </td>");
                        sbHtml.Append("<td align='right' style='width:80px;' id='total_price_" + prId + "'>" + string.Format("{0:F}", dblAmount) + "</td>");
                        sbHtml.Append("<td align='center' style='width:30px;'>");
                        sbHtml.Append("<input type='image' name='ctl00$cphMaster$grdRequest$ctl02$imgRefresh' id='ctl00_cphMaster_grdRequest_ctl02_imgRefresh' src='../images/edit-button.png' style='border-width:0px;' onclick='editUnitPrice(" + prId + "); return false;' /></td>");
                        sbHtml.Append("<td align='center' style='width:30px;'>");
                        sbHtml.Append("<input type='image' name='ctl00$cphMaster$grdRequest$ctl02$imgDeleteIL' id='ctl00_cphMaster_grdRequest_ctl02_imgDeleteIL' src='../images/delete-button.png' style='border-width:0px;' onclick='removeFromChart(" + prId + "); return false;' />");
                        sbHtml.Append("</td></tr>");
                    }
                }

                catch (Exception ex)
                {
                    MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError + ex.Message);
                }
                finally
                {
                    dbHelp.CloseDatabaseConnection();
                }

                #endregion


                arrData[2] = sbHtml.ToString();


                PreAccountRcv objPRCV = new PreAccountRcv();
                DataTable dtAcountReceivable = objPRCV.GetOrderPartialPaymentList(objOrd.OrdID, "en");
                string sPaymentDetail = "";
                foreach (DataRow dRow in dtAcountReceivable.Rows)
                {
                    if (!string.IsNullOrEmpty(BusinessUtility.GetString(dRow["sysAppDesc"])))
                    {

                        if (sPaymentDetail == "")
                        {
                            sPaymentDetail = BusinessUtility.GetString(dRow["sysAppDesc"]) + " : " + string.Format("{0:F}", Convert.ToDouble(dRow["AmountDeposit"].ToString()));
                        }
                        else
                        {
                            sPaymentDetail += ", " + BusinessUtility.GetString(dRow["sysAppDesc"]) + " : " + string.Format("{0:F}", Convert.ToDouble(dRow["AmountDeposit"].ToString()));
                        }
                    }
                }
                arrData[3] = sPaymentDetail;
                arrData[4] = objOrd.OrdSalesRepID;
            }
            else
            {
                arrData[0] = 0; // To Success
                arrData[1] = 0;
                arrData[2] = "";
                arrData[3] = "";
                arrData[4] = "";
            }
            //CustomerCredit objCustCredit = new CustomerCredit();
            //double dblCustomerCredit = BusinessUtility.GetDouble(objCustCredit.GetAvailableCredit(BusinessUtility.GetInt(guestid)));
            //cu.PopulateObject(BusinessUtility.GetInt(orderid));
            //arrData[0] = cu;
            //arrData[1] = dblCustomerCredit;
            //arrData[1] = cu.GetBillToAddress(cu.PartnerID, cu.PartnerType);
        }
        catch
        {
            arrData[0] = 0; // To Success
            arrData[1] = 0;
            arrData[2] = "";
            arrData[3] = "";
            arrData[4] = "";
            //arrData[1] = new Addresses();
        }
        return arrData;
    }



    [System.Web.Services.WebMethod(EnableSession = true)]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static object[] SaveSessionData(string custID, string sHtml, string ids, string lstProdID) //, string[] ids
    {
        object[] arrData = new object[4];
        Orders objOrd = new Orders();
        try
        {


                string[] item = { "__item__" };
                string[] tax = { "__taxGrp__" };
                int intI = 0;
                int intR = 0;
                string strProductDtl = BusinessUtility.GetString(sHtml);
                string[] strPrd = strProductDtl.Split(item, System.StringSplitOptions.None);
                string[] strTaxOnPrd = null;
                string[] strTaxValue = null;
                string[] strPrdItem = null;
                List<Product.ProductTaxDetail> lstPTaxDetail = new List<Product.ProductTaxDetail>();
                while (strPrd.Length - 1 > intI)
                {
                    strTaxValue = strPrd[intI].Split(tax, System.StringSplitOptions.None);
                    strPrdItem = strTaxValue[0].Split('~');
                    if (strPrdItem.Length > 1)
                    {
                        int prId = BusinessUtility.GetInt(strPrdItem[0]);
                        string pName = BusinessUtility.GetString(strPrdItem[2]);
                        int iTaxGrp = BusinessUtility.GetInt(strPrdItem[6]);
                        int pQty = BusinessUtility.GetInt(strPrdItem[1]);
                        double dblPrice = BusinessUtility.GetDouble(strPrdItem[3]);
                        int pDiscount = BusinessUtility.GetInt(strPrdItem[5]);
                        double dblAmount = BusinessUtility.GetDouble(strPrdItem[4]);
                        lstPTaxDetail.Add(new Product.ProductTaxDetail { ProductID = prId, TaxID = iTaxGrp });
                    }
                    intI += 1;
                }



            //ids= ids.Replace("null,","undefined,");
            HttpContext.Current.Session["SavedUserID"] = custID;
            HttpContext.Current.Session["SavedHtml"] = sHtml;
            HttpContext.Current.Session["SavedIds"] = ids;
            HttpContext.Current.Session["SavedProductLst"] = lstProdID;
            HttpContext.Current.Session["SavedTaxLst"] = lstPTaxDetail;
            arrData[0] = 1; // To Success
            arrData[1] = 0;
            arrData[2] = "";
            arrData[3] = "";
        }
        catch
        {
            arrData[0] = 0; // To Success
            arrData[1] = 0;
            arrData[2] = "";
            arrData[3] = "";
        }
        return arrData;
    }




    [System.Web.Services.WebMethod(EnableSession = true)]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static object[] GetSessionData(string custID, string sHtml)
    {
        object[] arrData = new object[4];
        Orders objOrd = new Orders();
        try
        {
            if (BusinessUtility.GetString(HttpContext.Current.Session["SavedUserID"]) != "")
            {
                StringBuilder sbHtml = new StringBuilder();

                arrData[0] = 1; // To Success
                arrData[1] = BusinessUtility.GetString(HttpContext.Current.Session["SavedUserID"]);

                string[] item = { "__item__" };
                string[] tax = { "__taxGrp__" };
                int intI = 0;
                int intR = 0;
                string strProductDtl = BusinessUtility.GetString(HttpContext.Current.Session["SavedHtml"]);
                string[] strPrd = strProductDtl.Split(item, System.StringSplitOptions.None);
                string[] strTaxOnPrd = null;
                string[] strTaxValue = null;
                string[] strPrdItem = null;

                while (strPrd.Length - 1 > intI)
                {
                    strTaxValue = strPrd[intI].Split(tax, System.StringSplitOptions.None);
                    strPrdItem = strTaxValue[0].Split('~');
                    if (strPrdItem.Length > 1)
                    {
                        int prId = BusinessUtility.GetInt(strPrdItem[0]);
                        string pName = BusinessUtility.GetString(strPrdItem[2]);
                        int iTaxGrp = BusinessUtility.GetInt(strPrdItem[6]);
                        int pQty = BusinessUtility.GetInt(strPrdItem[1]);
                        double dblPrice = BusinessUtility.GetDouble(strPrdItem[3]);
                        int pDiscount = BusinessUtility.GetInt(strPrdItem[5]);
                        double dblAmount = BusinessUtility.GetDouble(strPrdItem[4]);

                        string sTaxGrpDesc = "";// BusinessUtility.GetString(r["sysTaxCodeDescText"]);
                        string upcCode = ""; // BusinessUtility.GetString(r["prdUPCCode"]);

                        Product objPrd = new Product();
                        objPrd.PopulateObject(prId);
                        upcCode = objPrd.PrdUPCCode;
                        sTaxGrpDesc = "";

                        SysTaxCodeDesc objTaxGroup = new SysTaxCodeDesc();
                        objTaxGroup.PopulateObject(iTaxGrp);
                        sTaxGrpDesc = BusinessUtility.GetString(objTaxGroup.SysTaxCodeDescText);

                        sbHtml.Append("<tr style='color:#333333;' id='" + prId + "' onclick = 'updateQuantityGrid(" + prId + ")' > ");
                        sbHtml.Append("<td align='left' id='prUPC_" + prId + "' style='width:120px;'>" + upcCode + "<input type='hidden' id='hdn_PTaxID_" + prId + "'  value='" + iTaxGrp + "' /> <input type='hidden' id='hdn_PTaxValue_" + prId + "'  value='" + "" + "' /> <input type='hidden' id='hdn_GiftCardNo_" + prId + "'  value='' />   </td>");
                        sbHtml.Append("<td align='left' id='prName_" + prId + "' style='width:250px;'>" + pName + "</td> ");
                        sbHtml.Append("<td align='right' style='width:80px;' id='quantity_" + prId + "'>" + pQty + "</td> ");
                        sbHtml.Append("<td align='right' style='width:80px;'id='unit_price_" + prId + "'>" + string.Format("{0:F}", dblPrice) + "</td>");
                        sbHtml.Append("<td align='right' style='width:80px;'id='p_disc_" + prId + "'>" + pDiscount + "</td>");
                        sbHtml.Append("<td align='right' style='width:80px;'id='p_Tax_" + prId + "'>" + sTaxGrpDesc + " </td>");
                        sbHtml.Append("<td align='right' style='width:80px;' id='total_price_" + prId + "'>" + string.Format("{0:F}", dblAmount) + "</td>");
                        sbHtml.Append("<td align='center' style='width:30px;'>");
                        sbHtml.Append("<input type='image' name='ctl00$cphMaster$grdRequest$ctl02$imgRefresh' id='ctl00_cphMaster_grdRequest_ctl02_imgRefresh' src='../images/edit-button.png' style='border-width:0px;' onclick='editUnitPrice(" + prId + "); return false;' /></td>");
                        sbHtml.Append("<td align='center' style='width:30px;'>");
                        sbHtml.Append("<input type='image' name='ctl00$cphMaster$grdRequest$ctl02$imgDeleteIL' id='ctl00_cphMaster_grdRequest_ctl02_imgDeleteIL' src='../images/delete-button.png' style='border-width:0px;' onclick='removeFromChart(" + prId + "); return false;' />");
                        sbHtml.Append("</td></tr>");
                    }
                    intI += 1;
                }


                arrData[2] = BusinessUtility.GetString(sbHtml);
                arrData[3] = BusinessUtility.GetString(HttpContext.Current.Session["SavedIds"]);
            }
            else
            {
                arrData[0] = 0; // To Success
                arrData[1] = 0;
                arrData[2] = "";
                arrData[3] = "";
            }
        }
        catch
        {
            arrData[0] = 0; // To Success
            arrData[1] = 0;
            arrData[2] = "";
            arrData[3] = "";
        }
        return arrData;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static object[] DeleteSessionData(string custID, string sHtml)
    {
        object[] arrData = new object[4];
        Orders objOrd = new Orders();
        try
        {
            HttpContext.Current.Session.Remove("SavedUserID");
            HttpContext.Current.Session.Remove("SavedHtml");
            HttpContext.Current.Session.Remove("SavedIds");
            HttpContext.Current.Session.Remove("SavedProductLst");
            HttpContext.Current.Session.Remove("SavedTaxLst");
            arrData[0] = 1; // To Success
            arrData[2] = 0;
            arrData[2] = "";
            arrData[3] = "";
        }
        catch
        {
            arrData[0] = 0; // To Success
            arrData[1] = 0;
            arrData[2] = "";
            arrData[3] = "";
        }
        return arrData;
    }
}
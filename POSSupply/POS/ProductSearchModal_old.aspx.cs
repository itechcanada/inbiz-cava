﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class POS_ProductSearchModal : System.Web.UI.Page
{
    string _whsCode = string.Empty;   
    string _strLang = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //pnlSession.Visible = false;
        //pnlProducts.Visible = true;
        //_whsCode = "MTL";
        //_strLang = "en";
        if (!CurrentUser.IsAutheticated) {
            pnlSession.Visible = true;
            pnlProducts.Visible = false;
        }
        else
        {
            pnlSession.Visible = false;
            pnlProducts.Visible = true;
        }

        _whsCode = CurrentUser.UserDefaultWarehouse;        
        _strLang = Globals.CurrentAppLanguageCode;

        if (!IsPostBack) {
            FillData();
        }
    }

    private void FillData()
    {
        sdsProducts.SelectParameters.Clear();
        sdsProducts.SelectParameters.Add("@descLang", _strLang);
        sdsProducts.SelectParameters.Add("@prdWhsCode", _whsCode);

        string strSQL = "SELECT distinct productID,  prdUPCCode,  Pdes.prdname as prdname,prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty,prdEndUserSalesPrice,prdIsKit,prdSmallImagePath, prdLargeImagePath,prdTaxCode FROM products ";
        strSQL += " Left join prdQuantity on prdQuantity.PrdID=Products.ProductID ";
        strSQL += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID ";
        strSQL += " left join prdImages on prdImages.PrdID=Products.ProductID  ";
        strSQL += " where descLang =@descLang and prdWhsCode=@prdWhsCode and prdIsActive=1 ";

        sdsProducts.SelectCommand = strSQL;
    }

    protected string GetProductTags(object pid) {
        ProductTag pTags = new ProductTag();
        int prdId = BusinessUtility.GetInt(pid);
        return pTags.GetTagsByProductID(prdId).Replace("^", "<br>");
    }
}
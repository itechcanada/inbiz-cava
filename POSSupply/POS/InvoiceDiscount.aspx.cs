﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class POS_InvoiceDiscount : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                MessageState.SetGlobalMessage(MessageType.Success, "");
                txtDiscount.Focus();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            //SysRegister objSysRegister = new SysRegister();
            //if (objSysRegister.ValidateMangerCode(BusinessUtility.GetString(Session["RegCode"]), txtDiscount.Text) == true)
            //{
                Globals.RegisterCloseDialogScript(Page, string.Format( "parent.ApplyInvoiceDisc({0});", txtDiscount.Text), true);
            //}
            //else
            //{
            //    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblInvalidManageCode);
            //}
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
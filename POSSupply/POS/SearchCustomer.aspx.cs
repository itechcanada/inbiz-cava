﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

using Trirand.Web.UI.WebControls;

public partial class POS_SearchCustomer : BasePage
{
    Partners _part = new Partners();

    protected void Page_Load(object sender, EventArgs e)
    {

        int ordType = BusinessUtility.GetInt(Request.QueryString["orderType"]);
        txtSearch.Text = Request.QueryString["keyword"] != null ? Request.QueryString["keyword"] : string.Empty;
        if (ordType == 2)
        {
            lblcompanyID.Visible = false;
            dlWarehouses.Visible = false;
        }
        else
        {
            tblTran.Visible = false;
        }
        if (!IsPagePostBack(grdCustomers))
        {
            if (!CurrentUser.IsAutheticated)
            {
                pnlSession.Visible = true;
                pnlResults.Visible = false;
            }
            else
            {
                pnlSession.Visible = false;
                pnlResults.Visible = true;
            }

            string isPOSSearch = BusinessUtility.GetString(Request.QueryString["orderFrom"]);
            if (!string.IsNullOrEmpty(isPOSSearch))
            {
                //btnAddCustomer.OnClientClick = string.Format("parent.location.href='{0}?pType=D&gType=0&create_POS=1&InvokeSrc=POS'; return false;", ResolveUrl("~/Partner/CustomerEdit.aspx"));
            }
            else
            {
                //btnAddCustomer.OnClientClick = string.Format("parent.location.href='{0}?pType=D&gType=0&create_sales=1'; return false;", ResolveUrl("~/Partner/CustomerEdit.aspx"));
            }
            SysWarehouses wh = new SysWarehouses();
            wh.FillWharehouse(null, dlWarehouses, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));

            ListItem liWhs = dlWarehouses.Items.FindByValue(this.WarehouseCode);
            foreach (ListItem liItem in dlWarehouses.Items)
            {
                if (liItem.Selected == true)
                    liItem.Selected = false;
            }
            if (liWhs != null)
            {
                liWhs.Selected = true;
            }
            else
            {
                liWhs = dlWarehouses.Items.FindByValue(CurrentUser.UserDefaultWarehouse);
                if (liWhs != null)
                {
                    liWhs.Selected = true;
                }
            }

            wh.FillWharehouse(null, dlFromWhs, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem("", "0"));
            dlFromWhs.SelectedIndex = 0;
            wh.FillWharehouse(null, dlToWhs, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));
        }
    }

    public string WarehouseCode
    {
        get
        {
            if (BusinessUtility.GetString(Session["RegWhsCode"]) != "")
            {
                return BusinessUtility.GetString(Session["RegWhsCode"]);
            }
            else
            {
                return CurrentUser.UserDefaultWarehouse;
            }
        }
    }

    protected void grdCustomers_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        string data = "";

        if (e.ColumnIndex == 1) //Set Customer Name & Address            
        {
            data = BusinessUtility.GetString(e.RowValues[1]) + " ";
            data += BusinessUtility.GetString(e.RowValues[2]);
            _part.PopulateObject(BusinessUtility.GetInt(e.RowKey));
            Addresses addr = new Addresses();
            string pType = Globals.GetPartnerType(_part.PartnerType);
            addr.PopulateObject(BusinessUtility.GetInt(e.RowKey), pType, AddressType.BILL_TO_ADDRESS);
            data += "<br>" + addr.ToHtml();
            if (BusinessUtility.GetString(_part.PartnerPhone) != "")
            {
                data += "<br>" + Resources.Resource.lblTelephone + " : " + _part.PartnerPhone;
            }
            if (BusinessUtility.GetString(_part.PartnerEmail) != "")
            {
                data += "<br>" + Resources.Resource.lblEmail + " : " + _part.PartnerEmail;
            }
            CustomerCredit objCustCrdt = new CustomerCredit();

            SysCompanyInfo ci = new SysCompanyInfo();
            ci.PopulateObject(CurrentUser.DefaultCompanyID);

            double points = new LoyalPartnerHistory().GetPartnerLoyaltyPoints(null, _part.PartnerID);

            data += "<br>" + Resources.Resource.lblInstoreCredit + " : " + CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(objCustCrdt.GetAvailableCredit(BusinessUtility.GetInt(e.RowKey))));
            data += "<br>" + Resources.Resource.lblPoints + " : " + BusinessUtility.GetString(points);
            e.CellHtml = data.Trim();
        }
        else if (e.ColumnIndex == 2)
        {
            //double balAmt = ProcessInvoice.GetCustomerUnpaidAmount(BusinessUtility.GetInt(e.RowKey));
            //string style = balAmt > 0 ? "style='color:red;'" : "";
            //data = string.Format("<div {0}>{1} : {2} <br >", style, Resources.Resource.msgTtlNoSO, e.RowValues[3]);
            //data += string.Format("{0} : {1} <br >", Resources.Resource.msgTtlNoInv, e.RowValues[4]);
            //data += string.Format("{0} : {1:F} <br >", Resources.Resource.totalAmtRcvd, BusinessUtility.GetDouble(e.RowValues[5]));
            //data += string.Format("{0} : {1} <br >", Resources.Resource.LastAmtRcvdOn, BusinessUtility.GetDateTime(e.RowValues[6]) != DateTime.MinValue ? BusinessUtility.GetDateTime(e.RowValues[6]).ToString("MMM dd, yyyy") : "---");
            //data += string.Format("{0} : {1:F} </div>", Resources.Resource.lblUnPaidAmount, balAmt);
            //e.CellHtml = data.Trim();

            //double balAmt = ProcessInvoice.GetCustomerUnpaidAmount(BusinessUtility.GetInt(e.RowKey));
            double balAmt = ProcessInvoice.GetPOSCustomerUnpaidAmount(BusinessUtility.GetInt(e.RowKey));
            _part.PopulateObject(BusinessUtility.GetInt(e.RowKey));

            Invoice objInvoice = new Invoice();
            objInvoice.GetInvoiceCustSaleRepName(BusinessUtility.GetInt(e.RowValues[11]));

            string style = balAmt > 0 ? "style='color:red;'" : "";
            data = string.Format("<div {0}>", style);
            //data += string.Format("<br>{0} : {1} <br >",  Resources.Resource.msgTtlNoSO, e.RowValues[3]);
            data += string.Format("{0} : {1}", Resources.Resource.msgTtlNoInv, e.RowValues[4]);
            data += string.Format("<br >{0} : {1}", Resources.Resource.lblTotalReturns, e.RowValues[7]);
            data += string.Format("<br >{0} : {1}", Resources.Resource.lblLastTransactionDate, BusinessUtility.GetDateTime(e.RowValues[8]) != DateTime.MinValue ? BusinessUtility.GetDateTime(e.RowValues[8]).ToString("MMM dd, yyyy") : "---");

            string sWhrCode = "";
            sWhrCode = BusinessUtility.GetString(e.RowValues[9]);

            if (BusinessUtility.GetString(e.RowValues[10]) != "")
                sWhrCode += " - " + BusinessUtility.GetString(e.RowValues[10]);


            data += string.Format("<br >{0} : {1}", Resources.Resource.lblTransactionLocation, sWhrCode);
            data += string.Format("<br >{0} : {1}", Resources.Resource.lblServicedBy, objInvoice.InvSaleRep);
            //data += string.Format("{0} : {1:F} <br >", Resources.Resource.totalAmtRcvd, BusinessUtility.GetDouble(e.RowValues[5]));
            //data += string.Format("{0} : {1} <br >", Resources.Resource.LastAmtRcvdOn, BusinessUtility.GetDateTime(e.RowValues[6]) != DateTime.MinValue ? BusinessUtility.GetDateTime(e.RowValues[6]).ToString("MMM dd, yyyy") : "---");
            data += string.Format("<br>{0} : {1:F} ", Resources.Resource.lblTotalBalance, CurrencyFormat.GetCompanyCurrencyFormat(balAmt));
            data += "<br>" + Resources.Resource.lblCustomerSince + " " + _part.PartnerSince.ToString("MMM dd, yyyy");
            data += "</div>";

            e.CellHtml = data.Trim();
        }

        else if (e.ColumnIndex == 4)
        {
            int iPartnerID = BusinessUtility.GetInt(e.RowKey);
            string sHtml = string.Format("<div class='submit' style='width:100%;text-align:center;'> <input type='button' class='updateQty   '  id='" + "btn" + BusinessUtility.GetString(iPartnerID) + "' name='" + "btn" + BusinessUtility.GetString(iPartnerID) + "'  value= '" + Resources.Resource.lnkCustomerSelect + "' onclick =  SelectCustomer('{0}') /></div>", BusinessUtility.GetString(iPartnerID));
            e.CellHtml = sHtml;
        }
        else if (e.ColumnIndex == 5)
        {
            int iPartnerID = BusinessUtility.GetInt(e.RowKey);
            string sHtml = string.Format("<div class='submit' style='width:100%;text-align:center;margin-top:28px;margin-left:-4px;'> <input type='button' class='updateQty'  id='" + "btn" + BusinessUtility.GetString(iPartnerID) + "' name='" + "btn" + BusinessUtility.GetString(iPartnerID) + "'  value= '" + Resources.Resource.lblHistory + "' onclick =  CustomerDetail('{0}') /></div><div class='submit' style='width:100%;text-align:center;margin-left:-4px;margin-top:10px;'> <a href='#'  onclick =  CustomerLeagcy('{0}') >" + Resources.Resource.lnkLegacy + "</a></div>", BusinessUtility.GetString(iPartnerID));
            e.CellHtml = sHtml;
        }
        else if (e.ColumnIndex == 6)
        {
            int iPartnerID = BusinessUtility.GetInt(e.RowKey);
            string sHtml = string.Format("<div class='submit' style='width:100%;text-align:center;'> <input type='button' class=' updateQty '  id='" + "btn" + BusinessUtility.GetString(iPartnerID) + "' name='" + "btn" + BusinessUtility.GetString(iPartnerID) + "'  value= '" + Resources.Resource.lnkCustomerReceipt + "' onclick =  CustomerReceipt('{0}') /> </div>", BusinessUtility.GetString(iPartnerID));
            e.CellHtml = sHtml;
        }
        else if (e.ColumnIndex == 7)
        {
            int iPartnerID = BusinessUtility.GetInt(e.RowKey);
            string sPType = "";

            if (BusinessUtility.GetString(e.RowValues[12]) == Globals.GetPartnerTypeID(StatusCustomerTypes.DISTRIBUTER).ToString())
            {
                sPType = "D";
            }
            else if (BusinessUtility.GetString(e.RowValues[12]) == Globals.GetPartnerTypeID(StatusCustomerTypes.END_CLINET).ToString())
            {
                sPType = "E";
            }

            string sHtml = string.Format("<div class='submit' style='width:100%;text-align:center;'> <input type='button' class=' updateQty '  id='" + "btn" + BusinessUtility.GetString(iPartnerID) + "' name='" + "btn" + BusinessUtility.GetString(iPartnerID) + "'  value= '" + Resources.Resource.edit + "' onclick =  CustomerEdit('{0}','{1}') /> </div>", BusinessUtility.GetString(iPartnerID), sPType);
            e.CellHtml = sHtml;
        }
        //sHtmlContent += string.Format("<td style='vertical-align:top;'><div class='submit'> <input type='button' class='updateQty ui-button ui-widget ui-state-default ui-corner-all '  id='" + "btn" + BusinessUtility.GetString(dRow["productID"]) + "' name='" + "btn" + BusinessUtility.GetString(dRow["productID"]) + "'  value= '" + CommonResource.ResourceValue("lblUpdate") + "' onclick =  updateCollStyle('{0}') /> </div> </td> ", sPIDs);
    }

    protected void grdCustomers_DataRequesting(object sender, JQGridDataRequestEventArgs e)
    {
        bool isSalesRescricted = CurrentUser.IsInRole(RoleID.SALES_REPRESENTATIVE) && BusinessUtility.GetBool(Session["SalesRepRestricted"]);
        string isPOSSearch = BusinessUtility.GetString(Request.QueryString["orderFrom"]);
        string isSearchKey = BusinessUtility.GetString(Request.QueryString["srchKey"]);

        if (isSearchKey != "")
        {
            string sSrchTxt =BusinessUtility.GetString( Request.QueryString[txtSearch.ClientID]);
            if (isSearchKey != sSrchTxt && sSrchTxt != "")
            {
                sdsCustomers.SelectCommand = _part.GetSearchSql(sdsCustomers.SelectParameters, sSrchTxt, CurrentUser.UserID, isSalesRescricted, (int)PartnerTypeIDs.Distributer);
                return;
            }

            sdsCustomers.SelectCommand = _part.GetSearchSql(sdsCustomers.SelectParameters, isSearchKey, CurrentUser.UserID, isSalesRescricted, (int)PartnerTypeIDs.Distributer);
            return;
        }

        if ((txtSearch.Text != "") && BusinessUtility.GetString(Request.QueryString[txtSearch.ClientID]) == "")
        {
            sdsCustomers.SelectCommand = _part.GetSearchSql(sdsCustomers.SelectParameters, txtSearch.Text, CurrentUser.UserID, isSalesRescricted, (int)PartnerTypeIDs.Distributer);
            return;
        }

        if (BusinessUtility.GetString(Request.QueryString[txtSearch.ClientID]) != "")
        {
            sdsCustomers.SelectCommand = _part.GetSearchSql(sdsCustomers.SelectParameters, BusinessUtility.GetString(Request.QueryString[txtSearch.ClientID]), CurrentUser.UserID, isSalesRescricted, (int)PartnerTypeIDs.Distributer);
            return;
        }

        if (!string.IsNullOrEmpty(isPOSSearch) && string.IsNullOrEmpty(Request.QueryString[txtSearch.ClientID]))
        {
            sdsCustomers.SelectCommand = "";
        }
        else
        {
            if (Request.QueryString.AllKeys.Contains<string>("_history"))
            {
                sdsCustomers.SelectCommand = _part.GetSearchSql(sdsCustomers.SelectParameters, Request.QueryString[txtSearch.ClientID], CurrentUser.UserID, isSalesRescricted, (int)PartnerTypeIDs.Distributer);
            }
            else
            {
                sdsCustomers.SelectCommand = _part.GetSearchSql(sdsCustomers.SelectParameters, txtSearch.Text, CurrentUser.UserID, isSalesRescricted, (int)PartnerTypeIDs.Distributer);
            }
        }
    }

    protected void grdCustomers_RowSelecting(object sender, JQGridRowSelectEventArgs e)
    {
        _part.PopulateObject(BusinessUtility.GetInt(e.RowKey));
        string url = "";
        string isPOSSearch = BusinessUtility.GetString(Request.QueryString["orderFrom"]);
        if (!string.IsNullOrEmpty(isPOSSearch))
        {
            Globals.RegisterCloseDialogScript(Page, string.Format("parent.setCurrentGuest({0});", e.RowKey), true);
        }
        else
        {
            int ordType = BusinessUtility.GetInt(Request.QueryString["orderType"]);
            if (ordType == 2)
            {
                if (ValidatePage() == false)
                {
                    return;
                }
                string invokSrc = BusinessUtility.GetString(Request.QueryString["invokSrc"]);
                if (invokSrc == "POS")
                {
                    url = ResolveUrl("~/Sales/SalesEdit.aspx") + string.Format("?custID={0}&custName={1}&ComID={2}&FromWhsID={3}&ToWhsID={4}&orderType={5}&InvokeSrc={6}", e.RowKey, _part.PartnerLongName, CurrentUser.DefaultCompanyID, dlFromWhs.SelectedValue, dlToWhs.SelectedValue, ordType, "POS");
                }
                else
                {
                    url = ResolveUrl("~/Sales/SalesEdit.aspx") + string.Format("?custID={0}&custName={1}&ComID={2}&FromWhsID={3}&ToWhsID={4}&orderType={5}", e.RowKey, _part.PartnerLongName, CurrentUser.DefaultCompanyID, dlFromWhs.SelectedValue, dlToWhs.SelectedValue, ordType);
                }
            }
            else
            {
                url = ResolveUrl("~/Sales/SalesEdit.aspx") + string.Format("?custID={0}&custName={1}&ComID={2}&whsID={3}", e.RowKey, _part.PartnerLongName, CurrentUser.DefaultCompanyID, dlWarehouses.SelectedValue);
            }


            Globals.RegisterParentRedirectPageUrl(this, url);
        }
    }

    protected Boolean ValidatePage()
    {
        if (dlFromWhs.SelectedValue == "0")
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgFormWareHouseRequired);
            return false;
        }
        else if (dlFromWhs.SelectedValue == dlToWhs.SelectedValue)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgFormToNotSame);
            return false;
        }
        return true;
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="ReceiptMail.aspx.cs" Inherits="POS_ReceiptMail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="dialogAssignUser" style="padding: 5px;">
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label1" Text="<%$Resources:Resource,lblCMEmailTo%>" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtMailID" runat="server" MaxLength="50" />
                    <asp:HiddenField id="hdnMailID" runat="server"  />
                    <asp:RequiredFieldValidator ID="reqvalManagerCode" ErrorMessage="<%$ Resources:Resource, reqvalEmail%>"
                        runat="server" ControlToValidate="txtMailID" Display="Dynamic" SetFocusOnError="true"
                        ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMailID" ValidationGroup="RegisterUser"
                        ErrorMessage="<%$ Resources:Resource, reqvalCMValidEmailID %>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        Display="Dynamic">
                    </asp:RegularExpressionValidator>
                </td>
            </tr>
        </table>
    </div>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, Ok%>" runat="server" OnClick="btnSave_Click"
            ValidationGroup="RegisterUser" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
            OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var vMailID = getParameterByName('custEmail');
            if (vMailID != "") {
                $("#ctl00_cphMaster_txtMailID").val(vMailID);
                $("#ctl00_cphMaster_hdnMailID").val(vMailID);

                
            }
        });

        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
            return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    </script>
</asp:Content>

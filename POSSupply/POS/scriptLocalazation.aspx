<%@ Page Language="VB" AutoEventWireup="false" CodeFile="scriptLocalazation.aspx.vb"
    Inherits="POS_scriptLocalazation" %>

<%
    Dim objComman As New clsCommon
    Dim reader As System.Resources.ResXResourceReader
    Dim id As System.Collections.IDictionaryEnumerator
    If Request.QueryString("lang") = "sp" Then
        reader = New System.Resources.ResXResourceReader(Server.MapPath("~/App_GlobalResources/Resource.es-MX.resx"))
    ElseIf Request.QueryString("lang") = "fr" Then
        reader = New System.Resources.ResXResourceReader(Server.MapPath("~/App_GlobalResources/Resource.fr-CA.resx"))
    Else
        reader = New System.Resources.ResXResourceReader(Server.MapPath("~/App_GlobalResources/Resource.resx"))
    End If
    id = reader.GetEnumerator()
    While (id.MoveNext)
        If Request.QueryString("key") = id.Key.ToString Then%>
<%= objComman.replaceSpecialCh(id.value.ToString())%>
<%  End If
End While
%>

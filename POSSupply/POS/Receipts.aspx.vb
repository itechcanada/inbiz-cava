Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Data.Odbc
Imports System.Data
Imports System.web.UI
Imports System.IO
Imports System.Xml
Partial Class POS_Receipts
    Inherits BasePage
    Dim objTransaction As New clsPOSTransaction
    Dim objPosPrdNo As New clsPosProductNo
    Dim objPosTranAccHrd As New clsPosTransAccHdr
    Dim objTransDtl As New clsTransactionDtl
    Dim objCom As New clsCommon
    Protected Sub POS_Receipts_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Session("RegCode") = "" Then
            Response.Redirect("~/Admin/selectRegister.aspx")
        End If
        If Not Page.IsPostBack Then
            lblHeading.Text = PosReprintHeading
            subFillGrid()
        End If

        clsCSV.ExportCSV(Page, "Inv", grdvViewTran, New Integer() {7, 8}, imgCsv)

    End Sub
    Public Sub subFillGrid()
        If chkUser.Checked = True Then
            sqlsdviewTran.SelectCommand = objTransaction.funReprint("true")
        Else
            sqlsdviewTran.SelectCommand = objTransaction.funReprint("")
        End If
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    Protected Sub grdvViewTran_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdvViewTran.PageIndexChanging
        subFillGrid()
    End Sub
    Protected Sub grdvViewTran_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdvViewTran.RowCommand
        If e.CommandName = "Edit" Then
            Dim strTranID() As String = e.CommandArgument.ToString.Split(":")
            Dim strMsg As String
            If strTranID(2) = 9 Then
                strMsg = msgPaused
            ElseIf strTranID(2) = 0 Then
                strMsg = msgFailed
            ElseIf strTranID(2) = 1 Then
                strMsg = msgSuccessfullyCompleted
            End If
            If Request.Browser.Browser.ToString() = "IE" Then
                If strTranID(3) = "S" Then
                    Response.Redirect("Print.aspx?TID=" & strTranID(0) & "&TType=" & strTranID(1) & "&posmsg=" & strMsg & "&Status=" & strTranID(2) & "&Reprint=yes" & "&isPrint=no")
                Else
                    Response.Redirect("Print.aspx?TID=" & strTranID(0) & "&TType=" & strTranID(1) & "&posmsg=" & strMsg & "&Status=" & strTranID(2) & "&Reprint=yes" & "&isPrint=no")
                End If
            Else
                If strTranID(3) = "S" Then
                    Response.Write("<script>window.open('Print.aspx?TID=" & strTranID(0) & "&TType=" & strTranID(1) & "&posmsg=" & strMsg & "&Status=" & strTranID(2) & "&Reprint=yes&isPrint=no', 'Print','height=300,width=260,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');</script>")
                Else
                    Response.Write("<script>window.open('Print.aspx?TID=" & strTranID(0) & "&TType=" & strTranID(1) & "&posmsg=" & strMsg & "&Status=" & strTranID(2) & "&Reprint=yes&isPrint=no', 'Print','height=300,width=260,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');</script>")
                End If
                Response.Write("<script>location.href='Receipts.aspx';</script>")
            End If
        End If
    End Sub
    Protected Sub grdvViewTran_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdvViewTran.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
            "confirm(" & msgConformDeleteTransaction & ")")
            If e.Row.Cells(4).Text = 9 Then
                e.Row.Cells(4).Text = msgPaused
            ElseIf e.Row.Cells(4).Text = 0 Then
                e.Row.Cells(4).Text = msgFailed
            ElseIf e.Row.Cells(4).Text = 1 Then
                Dim posAcctPay As String
                posAcctPay = DataBinder.Eval(e.Row.DataItem, "posAcctPayType").ToString()
                If String.IsNullOrEmpty(posAcctPay) Then
                    e.Row.Cells(4).Text = msgSuccessfullyCompleted
                Else
                    If posAcctPay = "8" OrElse posAcctPay = "9" Then
                        e.Row.Cells(4).Text = msgSuccessfullyCompleted + String.Format(" ({0})", DataBinder.Eval(e.Row.DataItem, "PaymentBy"))
                    Else
                        e.Row.Cells(4).Text = msgSuccessfullyCompleted
                    End If
                End If
            End If
                If (e.Row.RowType = DataControlRowType.DataRow) Then
                    e.Row.Cells(2).Text = String.Format("{0:F}", CDbl(e.Row.Cells(2).Text))
                    e.Row.Cells(3).Text = String.Format("{0:F}", CDbl(e.Row.Cells(3).Text))
                End If
                e.Row.Cells(1).Text = objCom.GetFormatDate(e.Row.Cells(1).Text, 2)
                If e.Row.Cells(6).Text = "S" Then
                    e.Row.Cells(6).Text = msgPosSale
                Else
                    e.Row.Cells(6).Text = msgPosRefund
                End If
        End If
    End Sub
    'Protected Sub grdvViewTran_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdvViewTran.RowEditing
    '    Dim strTranID As String = grdvViewTran.DataKeys(e.NewEditIndex).Value.ToString()
    '    If Request.Browser.Browser.ToString() = "IE" Then
    '        Response.Redirect("Print.aspx?cardtype=R&TID=" & strTranID & "&TType=" & grdvViewTran.Rows(e.NewEditIndex).Cells(8).Text & "&WithCard=yes&posmsg=" & grdvViewTran.Rows(e.NewEditIndex).Cells(4).Text)
    '    Else
    '        Response.Write("<script>window.open('Print.aspx?cardtype=R&TID=" & strTranID & "&TType=" & grdvViewTran.Rows(e.NewEditIndex).Cells(8).Text & "&WithCard=yes&posmsg=" & grdvViewTran.Rows(e.NewEditIndex).Cells(4).Text & "', 'Print','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');</script>")
    '        Response.Write("<script>location.href='Receipts.aspx';</script>")
    '    End If
    'End Sub
    Protected Sub grdvViewTran_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdvViewTran.Sorting
        subFillGrid()
    End Sub

    Protected Sub chkUser_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkUser.CheckedChanged
        subFillGrid()
    End Sub
End Class

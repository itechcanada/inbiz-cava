﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PinpadPrint.aspx.vb" Inherits="POS_PinpadPrint" %>

<%@ Import Namespace="Resources.Resource" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <%=prjCompanyTitle%>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="../Css/style.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin: 0;">
    <form id="form1" runat="server">
    <asp:HiddenField runat="server" ID="compAddr" />
    <div align="Left" id="div1">
        <table width="160px" cellspacing="0" cellpadding="1" border="0">
            <% If Request.QueryString("IsReceipt") = "yes" And ConfigurationManager.AppSettings("IsReceipt").ToLower = "yes" Then%>
            <tr>
                <td>
                    <asp:Label CssClass="lblPOSPinpadPrintBold" ID="Label8" Text="." runat="server" />
                </td>
                <% Return%>
            </tr>
            <% End If%>
            <% If Request.QueryString("cardtype").ToLower = "p" Or Request.QueryString("cardtype").ToLower = "r" Or Request.QueryString("TType") <> "" Then%>
            <tr>
                <td class="POSPinpadPrintalign">
                    <asp:Label CssClass="lblPOSPinpadPrintBold" ID="lblCompanyName" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <span class="lblPOSPinpadPrintCust">
                        <%=compAddr.Value%>
                    </span>
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <asp:Label CssClass="lblPOSPinpadPrintCust" ID="lblCompanyPhone" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label runat="server" CssClass="lblPOSPinpadPrint" ID="lbldate"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left" style="padding-left: 2px;">
                    <asp:Label ID="lblProIL" CssClass="lblPinpadPOSPrint" runat="server" /><br />
                    <asp:Label runat="server" Style="font-family: IDAutomationHC39M; color: Black; font-size: 8px;"
                        ID="lblTransaction"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <span style="color: Black; font-size: xx-small">---------------------------------------------------------</span>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Table Width="160" ID="Table1" CellPadding="1" CellSpacing="1" runat="server">
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Table ID="tblsubTotal" Width="160" CellPadding="1" CellSpacing="1" GridLines="None"
                        runat="server">
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td>
                    <span style="color: Black; font-size: xx-small">---------------------------------------------------------</span>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Table ID="tblTax" Width="160" CellPadding="1" CellSpacing="1" GridLines="None"
                        runat="server">
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td>
                    <span style="color: Black; font-size: xx-small">---------------------------------------------------------</span>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Table ID="tblAmount" Width="160" CellPadding="1" CellSpacing="1" GridLines="None"
                        runat="server">
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td>
                    <span style="color: Black; font-size: xx-small">---------------------------------------------------------</span>
                </td>
            </tr>
            <tr>
                <td height="20">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label runat="server" CssClass="lblPOSPinpadPrint" ID="lblNote" Text=""></asp:Label>
                </td>
            </tr>
            <tr runat="server" id="trReprintP" visible="false">
                <td align="right" class="POSPinpadPrintalign">
                    <asp:Label ID="Label7" Text="<%$ Resources:Resource, PosReprint%>" CssClass="lblPOSPrintBold"
                        runat="server" />
                </td>
            </tr>
            <% End If%>
            <!--product print end -->
            <% If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" And (Request.QueryString("cardtype").ToLower = "t" Or Request.QueryString("TType") = "2" Or Request.QueryString("TType") = "3" Or Request.QueryString("TType") = "4" Or Request.QueryString("TType") = "5") Then%>
            <tr>
                <td class="POSPinpadPrintalign">
                    <asp:Label ID="lblCustCompanyName" CssClass="lblPOSPinpadPrintBold" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <span class="lblPOSPinpadPrintCust">
                        <%=compAddr.Value%>
                    </span>
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <asp:Label ID="lblCustCompanyPhone" CssClass="lblPOSPinpadPrintCust" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <asp:Label ID="lblMsgc" CssClass="lblPOSPinpadPrintCust" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label runat="server" CssClass="lblPOSPinpadPrintCust" ID="lblCustDate"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left" style="padding-left: 2px;">
                    <asp:Label ID="lblCustTran" CssClass="lblPOSPinpadPrintCust" runat="server" /><br />
                    <asp:Label runat="server" Style="font-family: IDAutomationHC39M; color: Black; font-size: 8px;"
                        ID="lblCustTransactionID"></asp:Label>
                </td>
            </tr>
            <% If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" Then%>
            <tr>
                <td align="left">
                    <asp:Label ID="Label1" CssClass="lblPOSPinpadPrintCust" Text="<%$ Resources:Resource, POSPrintCardName%>"
                        runat="server" />&nbsp;&nbsp;<asp:Label runat="server" CssClass="lblPOSPinpadPrintCust"
                            ID="lblCustCardName"></asp:Label><br />
                    <asp:Label ID="Label2" CssClass="lblPOSPinpadPrintCust" Text="<%$ Resources:Resource, POSPrintCardNo%>"
                        runat="server" />&nbsp;&nbsp;<asp:Label runat="server" CssClass="lblPOSPinpadPrintCust"
                            ID="lblCustCardNo"></asp:Label>
                </td>
            </tr>
            <% End If%>
            <tr>
                <td align="left" style="padding-left: 70px;">
                    <asp:Label runat="server" CssClass="lblPOSPinpadPrintBold" ID="lblCustPrintTotalAmount"
                        Text="<%$ Resources:Resource, lblPOSPrintTotalAmount%>" />
                    <asp:Label runat="server" CssClass="lblPOSPinpadPrintCust" ID="lblCustTotalAmount"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="5">
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label runat="server" CssClass="lblPOSPinpadPrintCust" ID="lblCustSign" Text="<%$ Resources:Resource, POSPrintSign%>"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="15">
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <asp:Label ID="lblCustomer" Text="<%$ Resources:Resource, PosCustomerPrint%>" CssClass="lblPOSPinpadPrintBold"
                        runat="server" />
                </td>
            </tr>
            <%--Merchant--%>
            <tr>
                <td height="25">
                </td>
            </tr>
            <% End If%>
            <% If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" And (Request.QueryString("cardtype").ToLower = "m" Or Request.QueryString("TType") = "2" Or Request.QueryString("TType") = "3" Or Request.QueryString("TType") = "4" Or Request.QueryString("TType") = "5") Then%>
            <tr>
                <td class="POSPinpadPrintalign">
                    <asp:Label ID="lblMerCompanyName" CssClass="lblPOSPinpadPrintBold" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <span class="lblPOSPinpadPrintCust">
                        <%=compAddr.Value%>
                    </span>
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <asp:Label ID="lblMerCompanyPhone" CssClass="lblPinpadPOSPrint" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <asp:Label ID="lblMsgM" CssClass="lblPOSPinpadPrint" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label runat="server" CssClass="lblPOSPinpadPrint" ID="lblMerDate"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left" style="padding-left: 2px;">
                    <asp:Label ID="lblMerTran" CssClass="lblPOSPinpadPrint" runat="server" /><br />
                    <asp:Label runat="server" Style="font-family: IDAutomationHC39M; color: Black; font-size: 8px;"
                        ID="lblMerTransactionID"></asp:Label>
                </td>
            </tr>
            <% If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" Then%>
            <tr>
                <td align="left">
                    <asp:Label ID="Label3" CssClass="lblPOSPinpadPrint" Text="<%$ Resources:Resource, POSPrintCardName%>"
                        runat="server" />&nbsp;&nbsp;<asp:Label runat="server" CssClass="lblPOSPinpadPrint"
                            ID="lblMCardName"></asp:Label><br />
                    <asp:Label ID="Label6" CssClass="lblPOSPinpadPrint" Text="<%$ Resources:Resource, POSPrintCardNo%>"
                        runat="server" />&nbsp;&nbsp;<asp:Label runat="server" CssClass="lblPOSPinpadPrint"
                            ID="lblMCardNo"></asp:Label>
                </td>
            </tr>
            <% End If%>
            <tr>
                <td align="left" style="padding-left: 70px;">
                    <asp:Label runat="server" CssClass="lblPOSPinpadPrintBold" ID="lblMerPrintTotalAmount"
                        Text="<%$ Resources:Resource, lblPOSPrintTotalAmount%>"></asp:Label>
                    <asp:Label runat="server" CssClass="lblPOSPinpadPrint" ID="lblMerTotalAmount"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="5">
                </td>
            </tr>
            <tr>
                <td height="15">
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <asp:Label ID="Label4" Text="<%$ Resources:Resource, PosMerchantPrint%>" CssClass="lblPOSPinpadPrintBold"
                        runat="server" />
                </td>
            </tr>
            <tr runat="server" id="trReprint" visible="false">
                <td align="right" class="POSPinpadPrintalign">
                    <asp:Label ID="Label5" Text="<%$ Resources:Resource, PosReprint%>" CssClass="lblPOSPinpadPrintBold"
                        runat="server" />
                </td>
            </tr>
            <% End If%>
            <!-- For itrect Service  -->
            <% If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" And (Request.QueryString("cardtype").ToLower = "d" Or Request.QueryString("cardtype").ToLower = "c") Then%>
            <tr>
                <td class="POSPinpadPrintalign">
                    <span class="lblPOSPinpadPrintCust">
                        <%=lblTransactionReceipt%>
                    </span>
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <asp:Label ID="lblDebCompanyName" CssClass="lblPOSPinpadPrintBold" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <span class="lblPOSPinpadPrintCust">
                        <%=compAddr.Value%>
                    </span>
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <asp:Label ID="lblDebCompanyPhone" CssClass="lblPOSPinpadPrintCust" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="Left" style="padding-left: 2px;">
                    <asp:Label runat="server" Style="font-family: IDAutomationHC39M; color: Black; font-size: 8px;"
                        ID="lblDebTransactionID"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="lblPOSPinpadPrintCust">TYPE :</span><%  If Request.QueryString("type") = "0" Then%>
                    <span class="lblPOSPinpadPrintCust">
                        <%= msgTypePurchase%>
                    </span>
                    <%ElseIf Request.QueryString("type") = "4" Then%>
                    <span class="lblPOSPinpadPrintCust">
                        <%= msgTypeRefund%>
                    </span>
                    <% End If%>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="lblPOSPinpadPrintCust">
                        <%=lblAcct%>
                    </span><span class="lblPOSPinpadPrintCust">
                        <% If Request.QueryString("acct") = "CHEQUING" Then%>
                        <%= msgAcctChequing%>
                        <%ElseIf Request.QueryString("acct") = "SAVING" Then%>
                        <%= msgAcctSavings%>
                        <%Else%>
                        <%= Request.QueryString("acct")%>
                        <% End If%>
                    </span>
                </td>
            </tr>
            <tr>
                <td align="left" style="padding-left: 50px;">
                    <asp:Label runat="server" CssClass="lblPOSPinpadPrintBold" ID="lblDebPrintTotalAmount"
                        Text="<%$ Resources:Resource, lblTotalAmount%>"></asp:Label>
                    <asp:Label runat="server" CssClass="lblPOSPinpadPrint" ID="lblDebTotalAmount"></asp:Label>
                </td>
            </tr>
            <% If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" Then%>
            <tr>
                <td align="left">
                    &nbsp;&nbsp;<asp:Label Visible="false" runat="server" CssClass="lblPOSPinpadPrint"
                        ID="lblDCardName"></asp:Label><br />
                    <asp:Label ID="Label12" CssClass="lblPOSPinpadPrint" Text="<%$ Resources:Resource, lblCardNo%>"
                        runat="server" />&nbsp;<asp:Label runat="server" CssClass="lblPOSPinpadPrint" ID="lblDCardNo"></asp:Label>
                </td>
            </tr>
            <tr>
                <%  Dim msgData() As String = Request.QueryString("posmsg").Split("~")%>
                <td align="left">
                    <span class="lblPOSPinpadPrintCust">
                        <%=lblDateTime%>
                    </span><span class="lblPOSPinpadPrintCust">
                        <%=msgData(0).Replace(" ", "&nbsp;&nbsp;")%>
                    </span>
                    <br />
                    <span class="lblPOSPinpadPrintCust">
                        <%=lblReference%>
                    </span><span class="lblPOSPinpadPrintCust">
                        <%=msgData(1).Replace(" ", "&nbsp;&nbsp;")%>
                    </span>
                    <br />
                    <span class="lblPOSPinpadPrintCust">
                        <%=lblAut%>
                    </span><span class="lblPOSPinpadPrintCust">
                        <%=msgData(2)%>
                    </span>
                    <br />
                </td>
            </tr>
            <tr style="height: 2px;">
            </tr>
            <%--<asp:Label ID="lblMsgD" CssClass="lblPOSPinpadPrint" runat="server" /></td>--%>
            <tr>
                <td align="Center">
                    <span class="lblPOSPinpadPrintCust">
                        <%=msgData(3)%>
                    </span>
                </td>
            </tr>
            <%--<tr>
                    <td align="Center">
                        <span class="lblPOSPinpadPrintCust">
                            <%=msgData(4)%>
                        </span>
                    </td>
                </tr>--%>
            <tr style="height: 5px;">
            </tr>
            <%  If Request.QueryString("cardtype").ToLower = "c" Then%>
            <tr>
                <td align="left">
                    <asp:Label runat="server" CssClass="lblPOSPinpadPrint" ID="lblMerSign" Text="<%$ Resources:Resource, POSPrintSign%>"></asp:Label>
                </td>
            </tr>
            <% End If%>
            <tr>
                <td align="Center">
                    <%  If Request.QueryString("copyType") = "M" Then%>
                    <span class="lblPOSPinpadPrint">
                        <%= lblMerchantCopy%>
                    </span>
                    <%ElseIf Request.QueryString("copyType") = "C" Then%>
                    <span class="lblPOSPinpadPrint">
                        <%= lblCustomerCopy%>
                    </span>
                    <% End If%>
                </td>
            </tr>
            <% End If%>
            <% End If%>
            <!--pin init and close-->
            <% If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" And (Request.QueryString("cardtype").ToUpper = "TI" Or Request.QueryString("cardtype").ToUpper = "TS" Or Request.QueryString("TType") = "2" Or Request.QueryString("TType") = "3" Or Request.QueryString("TType") = "4" Or Request.QueryString("TType") = "5") Then%>
            <tr>
                <td class="POSPinpadPrintalign">
                    <asp:Label ID="lblInitCompanyName" CssClass="lblPOSPinpadPrintBold" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <span class="lblPOSPinpadPrintCust">
                        <%=compAddr.Value%>
                    </span>
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <asp:Label ID="lblInitCompanyPhone" CssClass="lblPOSPinpadPrintCust" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPinpadPrintalign">
                    <% If (Request.QueryString("cardtype").ToUpper) = "TI" Then%>
                    <asp:Label ID="Label9" CssClass="lblPOSPinpadPrintBold" runat="server">Initialize Pin-Pad</asp:Label>
                    <% Else%>
                    <asp:Label ID="Label10" CssClass="lblPOSPinpadPrintBold" runat="server">Close/Balance Register</asp:Label>
                    <% End If%>
                </td>
            </tr>
            <%--<tr style="height:5px"></tr>
                     <tr>
                        <td align="left">
                            <asp:Label runat="server" CssClass="lblPOSPinpadPrint" ID="lblInitDate"></asp:Label>
                        </td>
                    </tr
                <tr style="height: 5px">
                </tr>--%>
            <% If (Request.QueryString("cardtype").ToUpper) = "TS" Then%>
            <tr>
                <%  Dim msgData() As String = Request.QueryString("posmsg").Split("~")%>
                <td align="left">
                    <span class="lblPOSPinpadPrintCust">Total Batch Amount (Debit) :</span> <span class="lblPOSPinpadPrintCust">
                        <%= msgData(1).Replace(" ", "&nbsp;&nbsp;")%>
                    </span>
                    <br />
                    <span class="lblPOSPinpadPrintCust">Number of transactions (Debit) :</span> <span
                        class="lblPOSPinpadPrintCust">
                        <%= msgData(0).Replace(" ", "&nbsp;&nbsp;")%>
                    </span>
                    <br />
                    <span class="lblPOSPinpadPrintCust">Total Batch Amount (Credit) :</span> <span class="lblPOSPinpadPrintCust">
                        <%= msgData(3).Replace(" ", "&nbsp;&nbsp;")%>
                    </span>
                    <br />
                    <span class="lblPOSPinpadPrintCust">Number of transactions (Credit) :</span> <span
                        class="lblPOSPinpadPrintCust">
                        <%= msgData(2).Replace(" ", "&nbsp;&nbsp;")%>
                    </span>
                    <br />
                    <%--<span class="lblPOSPinpadPrintCust">
                            Batch Control Number
                            :</span> <span class="lblPOSPinpadPrintCust">
                                <%=msgData(2)%>
                            </span><br />--%>
                </td>
            </tr>
            <tr style="height: 2px;">
            </tr>
            <%--<asp:Label ID="lblMsgD" CssClass="lblPOSPinpadPrint" runat="server" /></td>--%>
            <tr>
                <td align="Center">
                    <span class="lblPOSPinpadPrintCust">
                        <%=msgData(5)%>
                    </span>
                </td>
            </tr>
            <%Else%>
            <tr>
                <td align="Center">
                    <span class="lblPOSPinpadPrintCust">
                        <%=Request.QueryString("posmsg")%>
                    </span>
                </td>
            </tr>
            <%  End If
            End If%>
        </table>
    </div>
    </form>
</body>
</html>

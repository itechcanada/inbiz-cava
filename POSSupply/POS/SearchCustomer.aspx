﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="SearchCustomer.aspx.cs" Inherits="POS_SearchCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <style type="text/css">
        .ButtonCss
        {
            background: url("../lib/css/inbiz/images/ui-bg_highlight-soft_20_526785_1x100.png") repeat-x scroll 50% 50% #DBDDDD;
            border: 1px solid #DBDDDD;
            color: #FFFEFD;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <asp:Panel runat="server" ID="pnlSearch">
        <p>
            <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
        </p>
        <div class="searchBar" style="margin: 5px 0px;">
            <table border="0" cellpadding="0" cellspacing="0" width="99%">
                <tr>
                    <td>
                        <table border="0" cellpadding="2" cellspacing="2">
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblCustomer" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSOCustomer %>"
                                        CssClass="filter-key" />*
                                </td>
                                <td valign="middle" align="left">
                                    <asp:TextBox runat="server" Width="160px" ID="txtSearch"></asp:TextBox>
                                </td>
                                <td>
                                    <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblcompanyID" AssociatedControlID="dlWarehouses" Text="<%$ Resources:Resource, lblSoWhs%>"
                                        runat="server" CssClass="filter-key" />*
                                </td>
                                <td valign="middle" align="left">
                                    <asp:DropDownList ID="dlWarehouses" runat="server" Width="180px">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <table id="tblTran" runat="server">
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="lblFromWhs" AssociatedControlID="dlFromWhs" Text="<%$ Resources:Resource, lblFromWarehouse%>"
                                                    runat="server" CssClass="filter-key" />*
                                            </td>
                                            <td valign="middle" align="left">
                                                <asp:DropDownList ID="dlFromWhs" runat="server" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblToWhs" AssociatedControlID="dlToWhs" Text="<%$ Resources:Resource, lblToWarehouse%>"
                                                    runat="server" CssClass="filter-key" />*
                                            </td>
                                            <td valign="middle" align="left">
                                                <asp:DropDownList ID="dlToWhs" runat="server" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="text-align: right;">
                        <%--<asp:Button ID="btnAddCustomer" Text="<%$ Resources:Resource, lblAddVIP %>" OnClientClick="AddCustomer()"
                            runat="server" />--%>
                        <input type="button" id="btnAddCustomer" value="<%=Resources.Resource.lblAddVIP%>" onclick='AddCustomer()' />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlResults" runat="server">
        <div id="grid_wrapper" style="width: 99%;">
            <trirand:JQGrid runat="server" ID="grdCustomers" DataSourceID="sdsCustomers" Height="200px"
                Width="100%" AutoWidth="true" PagerSettings-PageSize="50" OnCellBinding="grdCustomers_CellBinding"
                OnDataRequesting="grdCustomers_DataRequesting">
                <%--OnRowSelecting="grdCustomers_RowSelecting"--%>
                <Columns>
                    <trirand:JQGridColumn DataField="PartnerID" PrimaryKey="true" Visible="false" />
                    <trirand:JQGridColumn DataField="PartnerID" HeaderText="<%$ Resources:Resource, lblCustName %>" />
                    <trirand:JQGridColumn DataField="PartnerID" HeaderText="<%$ Resources:Resource, lblHistory %>"
                        Editable="false" Sortable="false" />
                    <trirand:JQGridColumn HeaderText="" DataField="PartnerID" Visible="false" Sortable="false"
                        TextAlign="Center" Width="50">
                    </trirand:JQGridColumn>
                    <trirand:JQGridColumn DataField="PartnerID" HeaderText="<%$ Resources:Resource, lnkCustomerSelect %>"
                        Editable="false" Sortable="false" Width="90" />
                    <trirand:JQGridColumn DataField="PartnerID" HeaderText="<%$ Resources:Resource, lblHistory %>"
                        Editable="false" Sortable="false" Width="80" />
                    <trirand:JQGridColumn DataField="PartnerID" HeaderText="<%$ Resources:Resource, lnkCustomerReceipt %>"
                        Editable="false" Sortable="false" Width="80" />
                    <trirand:JQGridColumn DataField="PartnerID" HeaderText="<%$ Resources:Resource, edit %>"
                        Editable="false" Sortable="false" Width="60" />
                </Columns>
                <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                    ShowDeleteButton="false" ShowSearchButton="false" />
                <SortSettings InitialSortColumn="" />
                <AppearanceSettings AlternateRowBackground="True" />
                <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="gridLoadComplete" />
            </trirand:JQGrid>
            <asp:SqlDataSource ID="sdsCustomers" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
        </div>
        <asp:HiddenField ID="hdnCustomerName" runat="server" />
        <asp:HiddenField ID="hdnCustomerID" runat="server" />
        <asp:HyperLink ID="hlParentRedirect" NavigateUrl="~/Sales/Create.aspx" runat="server"
            Visible="false" />
        <script type="text/javascript">
            //Variables
            var gridID = "<%=grdCustomers.ClientID %>";
            var searchPanelID = "<%=pnlSearch.ClientID %>";
            var hdnCustomerID = "<%=hdnCustomerID.ClientID %>";
            var hdnCustomerName = "<%=hdnCustomerName.ClientID %>";

            //Function To Resize the grid
            function resize_the_grid() {
                $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
            }

            //Client side function before page change.
            function beforePageChange(pgButton) {
                $('#' + gridID).onJqGridPaging();
            }


            //Client side function on sorting
            function columnSort(index, iCol, sortorder) {
                $('#' + gridID).onJqGridSorting();
            }

            //Call Grid Resizer function to resize grid on page load.
            resize_the_grid();

            //Bind window.resize event so that grid can be resized on windo get resized.
            $(window).resize(resize_the_grid);

            //Initialize the search panel to perform client side search.
            $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

            function reloadGrid(event, ui) {
                $('#' + gridID).trigger("reloadGrid");
                //Call back Sync Message
                if (typeof getGlobalMessage == 'function') {
                    getGlobalMessage();
                }
            }

            function gridLoadComplete(data) {
                $(window).trigger("resize");
                ApplyGridButtonSetting();
                //addMouseHoverEvent();
                $("#<%=txtSearch.ClientID%>").focus();
            }             
        </script>
    </asp:Panel>
    <asp:Panel ID="pnlSession" runat="server" HorizontalAlign="Center">
        <table border="0" cellpadding="0" cellspacing="0" class="cal_table">
            <tbody>
                <tr>
                    <td class="center">
                        <b>Session Expired!</b>
                    </td>
                </tr>
            </tbody>
        </table>
        <br />
        <br />
        <input type="button" value="Close" onclick="jQuery.FrameDialog.closeDialog();" style="width: 250px;" />
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">

        function SelectCustomer(custID) {
            parent.setCurrentGuest(custID);
            jQuery.FrameDialog.closeDialog();
        }

        function AddCustomer() {
            parent.addNewCustomer();
            jQuery.FrameDialog.closeDialog();
        }


        function CustomerLeagcy(custID) {
            //window.open('../NewReport/report.aspx?rpt=SalesByCustomer.xml&custid=' + custID);
            var vSrchKey = $("#ctl00_cphMaster_txtSearch").val();
            parent.redirectCustomerLeagcy(custID, vSrchKey);
            jQuery.FrameDialog.closeDialog();
        }

        function CustomerDetail(custID) {
            //window.open('../NewReport/report.aspx?rpt=SalesByCustomer.xml&custid=' + custID);
            var vSrchKey = $("#ctl00_cphMaster_txtSearch").val();
            parent.redirectCustomerDetial(custID, vSrchKey);
            jQuery.FrameDialog.closeDialog();
        }

        function CustomerReceipt(custID) {
            //window.open('../Invoice/ViewInvoice.aspx?custid=' + custID + '&Default=1&dflt=Y&t=8');
            var vSrchKey = $("#ctl00_cphMaster_txtSearch").val();
            parent.redirectCustomerReceipt(custID, vSrchKey);
            jQuery.FrameDialog.closeDialog();
        }

        function CustomerEdit(custID, custcustType) {
            //window.open('../Invoice/ViewInvoice.aspx?custid=' + custID + '&Default=1&dflt=Y&t=8');
            var vSrchKey = $("#ctl00_cphMaster_txtSearch").val();
            parent.redirectCustomerEdit(custID, vSrchKey, custcustType);
            jQuery.FrameDialog.closeDialog();
        }


        function addMouseHoverEvent() {
            $(".updateQty").live('mouseover', function (e) {
                $($(this).addClass('ui-state-hover'));
            });

            $(".updateQty").live('mouseout', function (e) {
                $($(this).removeClass('ui-state-hover'));
            });
        }


        function ApplyGridButtonSetting() {

            $('.updateQty').each(function () {
                //                $(this).css('background-image', 'none');
                $(this).addClass("ButtonCss ui-button");

            });

        }


        //Auto complete initilizer
        jQuery(document).ready(function () {
            var vSearchKey = getParameterByName('srchKey');
            if (vSearchKey != "") {
                $("#ctl00_cphMaster_txtSearch").val(vSearchKey);
            }

            //            vSrchKey = $("#ctl00_cphMaster_txtSearch").val();
            //            if (vSrchKey != "") {
            //                $("#btnSearch").trigger("click");
            //            }
        });


        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
            return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }



        $(document).ready(function () {
            $("input:text").focus(function () { $(this).select(); });
        });



    </script>
</asp:Content>

Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Web.UI.WebControls
Imports System.Data.Odbc
Imports System.Data
Imports System.web.UI
Imports System.IO
Imports System.Xml
Partial Class POS_Data
    Inherits BasePage
    Dim objTransaction As New clsPOSTransaction
    Dim objPosPrdNo As New clsPosProductNo
    Dim objPosTranAccHrd As New clsPosTransAccHdr
    Dim objTransDtl As New clsTransactionDtl
    Dim objPrdQty As New clsPrdQuantity
    Private objProduct As New clsProducts
    Protected Sub POS_Data_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("data") <> "" Then
            funPopulateObjToTR(Session("PosAcctId"))
            Dim str As String = Request.QueryString("data")
        End If
    End Sub
    ' for Merchant Connect Multi Technical 
    Public Sub funPopulateObjToTR(ByVal strString As String)
        Dim strResponse, strTranID As String
        Dim str() As String = Request.QueryString("data").Split("~")

        If str(0) = "1[49]" Then
            strResponse = 1
        Else
            strResponse = 0
        End If

        If str(2).ToLower = "visa" Then
            objPosTranAccHrd.AcctPayType = 2
            objTransDtl.posCardName = str(2)
        ElseIf str(2).ToLower = "masterCard" Then
            objPosTranAccHrd.AcctPayType = 3
            objTransDtl.posCardName = str(2)
        ElseIf str(2).ToLower = "american express" Then
            objPosTranAccHrd.AcctPayType = 4
            objTransDtl.posCardName = str(2)
        ElseIf str(2).ToLower = "interac" Then
            objPosTranAccHrd.AcctPayType = 5
            objTransDtl.posCardName = str(2)
        End If
        If str(3) <> "" Then
            objTransDtl.posCardNo = str(3)
        End If
        objTransDtl.posExtReturnStatus = str(4) & str(5)

        objTransDtl.posTransID = str(6)

        objTransDtl.posTransEndTime = str(8) & " " & str(7)

        objPosTranAccHrd.AcctTransID = str(9)

        If str(10) = "[48]" Then
            objTransDtl.posCardLang = "en"
        ElseIf str(10) = "[49]" Then
            objTransDtl.posCardLang = "fr"
        Else
            objTransDtl.posCardLang = "fr"
        End If

        objTransDtl.posCardAccountType = str(11)
        objTransDtl.posCardMsg = str(12) & "~" & str(13) & "~" & str(14) & "~" & str(15)

        If strResponse = "1" Then
            objTransDtl.posTransAcctDtlStatus = "1"
        Else
            objTransaction.OldTransID = "sysNull"
            objTransDtl.posTransAcctDtlStatus = "9"
        End If

        objTransDtl.funUpdateTransaction(Session("PosAcctId"))
        If strResponse = "1" Then
            objTransaction.TransStatus = 1
            objPosTranAccHrd.CasReceived = Session("amount")
            objPosTranAccHrd.funUpdateRecievePostransaccthdr()
            objTransaction.funUpdateTransStaus(objPosTranAccHrd.AcctTransID)
            subDetectPrdQtyForCreditCard()
        Else
            objTransaction.TransStatus = 0
            objPosTranAccHrd.CasReceived = 0
            objPosTranAccHrd.funUpdateRecievePostransaccthdr()
            objTransaction.funUpdateTransStaus(objPosTranAccHrd.AcctTransID, "yes")
        End If
        Response.Redirect("~/POS/pos.aspx")
    End Sub
    Public Sub subDetectPrdQtyForCreditCard()
        Dim intI As Integer
        Dim objTblPrdNo As DataTable
        Dim objRowPrdNo As DataRow
        objTblPrdNo = Session("SalesCart")
        For intI = 0 To objTblPrdNo.Rows.Count - 1
            objRowPrdNo = objTblPrdNo.Rows(intI)
            If objPosTranAccHrd.AcctPayType <> 1 Then
                If objProduct.funGetPrdValue(objRowPrdNo("ProductID"), "kit") = "1" Then
                    objPrdQty.subGetPrdKitOnHandQty(objRowPrdNo("ProductID"), Session("UserWarehouse"), objRowPrdNo("Quantity")) 'reduce kit's product Qty
                    objPrdQty.updateProductQuantity(objRowPrdNo("ProductID"), Session("UserWarehouse"), objRowPrdNo("Quantity")) ' reduce prd kit Qty
                Else
                    objPrdQty.updateProductQuantity(objRowPrdNo("ProductID"), Session("UserWarehouse"), objRowPrdNo("Quantity"))
                    objPrdQty.funGetReducePrdKitQty(objRowPrdNo("ProductID"), Session("UserWarehouse"), "") ' reduce prd kit Qty
                End If
            End If
        Next
    End Sub
End Class

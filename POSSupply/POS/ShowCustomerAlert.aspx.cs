﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Library.Utilities;

public partial class POS_ShowCustomerAlert : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtCustomerAlert.Text = BusinessUtility.GetString(Request.QueryString["Alert"]);
        }
    }
}
Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Data.Odbc
Imports System.Data
Imports clsCommon
Imports iTECH.Library.Utilities
Imports iTECH.InbizERP.BusinessLogic
Partial Class POS_Print
    Inherits BasePage
    Dim objCommon As New clsCommon
    Dim objCompany As New clsCompanyInfo
    Dim objRegister As New SysRegister
    Protected Sub POS_Print_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            'If Request.QueryString("WithCard") = "yes" Then
            subClose()
            'Exit Sub
            'End If
        End If

        pnlPrintScript.Visible = Request.QueryString("isPrint") <> "no"
        Dim strLanguage As String
        ' If Request.QueryString("WithCard") = "yes" Then
        strLanguage = funGetProductLang()

        ' For Print Return Receipt Start
        If Request.QueryString("ReturnReceipt") = "yes" Then
            If (Session("RETURN_CART") IsNot Nothing) Then
                printReturnReceipt(strLanguage)
                Session("RETURN_CART") = Nothing
                Return
            End If
        End If
        ltrReturnReceipt.Text = ""

        If (Session("RETURN_CART") IsNot Nothing) Then

            Dim receiptCustID As Integer = BusinessUtility.GetInt(Request.QueryString("customerID"))
            Dim returnRcptCustID As Integer = BusinessUtility.GetInt(Session("RETURN_CART_CUSTID"))

            If ((receiptCustID > 0 And returnRcptCustID > 0) And receiptCustID = returnRcptCustID) Then
                If BusinessUtility.GetString(Request.QueryString("ReturnReceiptHistory")) = "yes" Then
                    Dim htmlToMail As String = ""
                    htmlToMail = CommonPrint.GetReturnReceiptHtmlSnipt(strLanguage)
                    ltrReturnReceipt.Text = htmlToMail
                Else
                    ltrReturnReceipt.Text = ""
                End If
            Else
                ltrReturnReceipt.Text = ""
            End If
        Else
            ltrReturnReceipt.Text = ""
        End If

        ltrGiftReceipt.Text = ""
        If BusinessUtility.GetString(Request.QueryString("printGiftReceipt") = "1") Then
            Dim htmlToMail As String = ""
            htmlToMail = CommonPrint.GetPOSGiftResceiptHtmlSnipt(BusinessUtility.GetInt(Request.QueryString("TID")), BusinessUtility.GetString(Session("RegCode")), BusinessUtility.GetString(Request.QueryString("WhsID")), strLanguage)
            If ltrReturnReceipt.Text <> "" Then
                ltrGiftReceipt.Text = "<div style='page-break-after: always;'></div>" + htmlToMail
            Else
                ltrGiftReceipt.Text = htmlToMail
            End If

            Return
        End If

        'If Request.QueryString("ReturnReceiptHistory") = "yes" Then
        '    If (Session("RETURN_CART") IsNot Nothing) Then
        '        printReturnReceipt(strLanguage)
        '        Session("RETURN_CART") = Nothing
        '        Return
        '    End If
        'End If


        ' Else
        'strLanguage = Request.QueryString("LangsID").Substring(0, 2)
        '  Session("UserWarehouse") = Request.QueryString("WhsID")
        '  End If

        'lbldate.Text = POSDate & Format(Date.Parse(Now()), "MM/dd/yyyy") & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & POSTime & Format(Date.Parse(Now()), "HH:mm:ss")
        lbldate.Text = POSDate & ": " & Format(Date.Parse(Now()), "MM/dd/yyyy")
        lblTime.Text = POSTime & ": " & Format(Date.Parse(Now()), "HH:mm:ss")

        lblTransaction.Text = "(" & Request.QueryString("TID") & ")"
        'lblProIL.Text = msgTransactionID & " " & Request.QueryString("TID")

        objRegister.PopulateObject(BusinessUtility.GetString(Session("RegCode")))
        objCompany.GetCompanyAddress()
        lblCompanyName.Text = objCompany.CompanyName
        lblCompanyPhone.Text = objCompany.CompanyPOPhone
        'lblComAdd.Text = objCompany.CompanyAddressLine1 & "<BR/>" & objCompany.CompanyCity & " " & objCompany.CompanyState & "<BR/>" & objCompany.CompanyCountry & " " & objCompany.CompanyPostalCode
        Dim sCompAdd As String
        sCompAdd = (objRegister.SysRegAddress & "<BR/>" & objRegister.SysRegCity & " " & objRegister.SysRegState & "<BR/>" & objCompany.CompanyCountry & " " & objRegister.SysRegPostalCode)
        If BusinessUtility.GetString(objRegister.SysRegPhone) <> "" Then
            sCompAdd = sCompAdd & "</br> PH.- " & objRegister.SysRegPhone
        End If

        lblComAdd.Text = sCompAdd


        lblCustCompanyName.Text = lblCompanyName.Text
        lblCustCompanyPhone.Text = lblCompanyPhone.Text

        lblCustTransactionID.Text = lblTransaction.Text
        'lblCustTran.Text = lblProIL.Text
        'lblRegMessage.Text = Session("RegMessage")

        ' Get Cust Name And Sales Rep Name
        Dim objInvoice As New Invoice
        Session("lastInvoiceNo") = BusinessUtility.GetString(Request.QueryString("TID"))
        objInvoice.GetInvoiceCustSaleRepName(BusinessUtility.GetInt(Request.QueryString("TID")))
        lblCustName.Text = Resources.Resource.lblCustName + ": " + BusinessUtility.GetString(objInvoice.InvCustName)
        lblSalesRep.Text = Resources.Resource.lblPrintSalesRep + ": " + BusinessUtility.GetString(objInvoice.InvSaleRep)

        lblCustAdd.Text = lblComAdd.Text
        lblMerAdd.Text = lblComAdd.Text
        lblMerCompanyName.Text = lblCompanyName.Text
        lblMerCompanyPhone.Text = lblCompanyPhone.Text
        lblMerDate.Text = lbldate.Text
        lblCustDate.Text = lbldate.Text
        lblMerTransactionID.Text = lblTransaction.Text
        'lblMerTran.Text = lblProIL.Text
        lblMsgc.Text = Request.QueryString("posmsg")
        lblMsgM.Text = Request.QueryString("posmsg")

        'for Reprint
        If Request.QueryString("Reprint") = "yes" Then
            trReprintP.Visible = True
            trReprint.Visible = True
            trCustReprint.Visible = True
            trDebCustReprint.Visible = True
            trDebMReprint.Visible = True

            lblReprintP.Text = PosReprint & "&nbsp; " & Format(Date.Parse(Now()), "MM/dd/yyyy HH:mm:ss")
            lblDebCustReprint.Text = lblReprintP.Text
            lblDebMReprint.Text = lblReprintP.Text
        End If

        'Debit Card
        lblDebCompanyName.Text = lblCompanyName.Text
        lblDebComAddress.Text = lblComAdd.Text
        lblDebPhone.Text = lblCompanyPhone.Text
        lblDebTranBarcode.Text = lblTransaction.Text
        lblDebMCompanyName.Text = lblCompanyName.Text
        lblDebMaddress.Text = lblComAdd.Text
        lblDebMPhone.Text = lblCompanyPhone.Text
        lblDebTransMBarcode.Text = lblTransaction.Text

        If funGetPosProduct(strLanguage) = False Then
            funGetPosProduct(strLanguage, "1")
        End If

        If BusinessUtility.GetString(Request.QueryString("isGiftReceipt") = "1") Then
            Dim htmlToMail As String = ""
            htmlToMail = "<div style='page-break-after: always;'></div>" & CommonPrint.GetPOSGiftResceiptHtmlSnipt(BusinessUtility.GetInt(Request.QueryString("TID")), BusinessUtility.GetString(Session("RegCode")), BusinessUtility.GetString(Request.QueryString("WhsID")), strLanguage)
            ltrGiftReceipt.Text = htmlToMail
        End If
    End Sub
    Public Function printReturnReceipt(ByVal strLang As String)
        If funGetReceipt(strLang) = False Then
            funGetReceipt(strLang, "1")
        End If

        Dim returnCart As List(Of ReturnCart) = CType(Session.Item("RETURN_CART"), List(Of ReturnCart))
        Dim itemtoUpdate = returnCart.Where(Function(i) i.ProductToReturnQty > 0)
        Dim item As ReturnCart = itemtoUpdate(0)

        ' Get Cust Name And Sales Rep Name
        If BusinessUtility.GetInt(item.InvoiceID) > 0 Then
            Dim objOrd As New Invoice()
            objOrd.PopulateObject(BusinessUtility.GetInt(item.InvoiceID))

            lbldate.Text = POSDate & ": " & Format(Date.Parse(Now()), "MM/dd/yyyy")
            lblTime.Text = POSTime & ": " & Format(Date.Parse(Now()), "HH:mm:ss")

            'lblTransaction.Text = "(" & objOrd.InvForOrderNo.ToString("D6") & ")"

            If (Session("AccountReceivableID") IsNot Nothing) Then
                lblTransaction.Text = "(" & Convert.ToInt32(Session("AccountReceivableID")).ToString("D6") & ")"
            Else
                lblTransaction.Text = "(" & objOrd.InvForOrderNo.ToString("D6") & ")"
            End If
            Session("AccountReceivableID") = Nothing
            objRegister.PopulateObject(BusinessUtility.GetString(objOrd.InvRegCode))
            objCompany.GetCompanyAddress()
            lblCompanyName.Text = objCompany.CompanyName
            lblCompanyPhone.Text = objCompany.CompanyPOPhone
            'lblComAdd.Text = objRegister.SysRegAddress & "<BR/>" & objRegister.SysRegCity & " " & objRegister.SysRegState & "<BR/>" & objCompany.CompanyCountry & " " & objRegister.SysRegPostalCode & "</br> PH.- " & objRegister.SysRegPhone
            Dim sCompAdd As String
            sCompAdd = objRegister.SysRegAddress & "<BR/>" & objRegister.SysRegCity & " " & objRegister.SysRegState & "<BR/>" & objCompany.CompanyCountry & " " & objRegister.SysRegPostalCode
            If BusinessUtility.GetString(objRegister.SysRegPhone) <> "" Then
                sCompAdd = sCompAdd & "</br> PH.- " & objRegister.SysRegPhone
            End If
            lblComAdd.Text = sCompAdd

            lblCustCompanyName.Text = lblCompanyName.Text
            lblCustCompanyPhone.Text = lblCompanyPhone.Text

            lblCustTransactionID.Text = lblTransaction.Text

            Dim objInvoiceDtl As New Invoice
            objInvoiceDtl.GetInvoiceCustSaleRepName(BusinessUtility.GetInt(item.InvoiceID))
            lblCustName.Text = Resources.Resource.lblCustName + ": " + BusinessUtility.GetString(objInvoiceDtl.InvCustName)
            lblSalesRep.Text = Resources.Resource.lblPrintSalesRep + ": " + BusinessUtility.GetString(objInvoiceDtl.InvSaleRep)
        Else

            Dim objOrd As New Orders()
            objOrd.PopulateObject(BusinessUtility.GetInt(item.OrderID))

            lbldate.Text = POSDate & ": " & Format(Date.Parse(Now()), "MM/dd/yyyy")
            lblTime.Text = POSTime & ": " & Format(Date.Parse(Now()), "HH:mm:ss")

            'lblTransaction.Text = "(" & item.OrderID.ToString("D6") & ")"
            'lblTransaction.Text = "(" & Convert.ToString(BusinessUtility.GetString(Session("AccountReceivableID"))).ToString("D6") & ")"

            If (Session("AccountReceivableID") IsNot Nothing) Then
                lblTransaction.Text = "(" & Convert.ToInt32(Session("AccountReceivableID")).ToString("D6") & ")"
            Else
                lblTransaction.Text = "(" & item.OrderID.ToString("D6") & ")"
            End If
            Session("AccountReceivableID") = Nothing
            objRegister.PopulateObject(BusinessUtility.GetString(objOrd.OrdRegCode))
            objCompany.GetCompanyAddress()
            lblCompanyName.Text = objCompany.CompanyName
            lblCompanyPhone.Text = objCompany.CompanyPOPhone
            'lblComAdd.Text = objRegister.SysRegAddress & "<BR/>" & objRegister.SysRegCity & " " & objRegister.SysRegState & "<BR/>" & objCompany.CompanyCountry & " " & objRegister.SysRegPostalCode & "</br> PH.- " & objRegister.SysRegPhone
            Dim sCompAdd As String
            sCompAdd = objRegister.SysRegAddress & "<BR/>" & objRegister.SysRegCity & " " & objRegister.SysRegState & "<BR/>" & objCompany.CompanyCountry & " " & objRegister.SysRegPostalCode
            If BusinessUtility.GetString(objRegister.SysRegPhone) <> "" Then
                sCompAdd = sCompAdd & "</br> PH.- " & objRegister.SysRegPhone
            End If
            lblComAdd.Text = sCompAdd

            lblCustCompanyName.Text = lblCompanyName.Text
            lblCustCompanyPhone.Text = lblCompanyPhone.Text

            lblCustTransactionID.Text = lblTransaction.Text

            Dim objInvoice As New Orders
            objInvoice.GetOrderCustSaleRepName(BusinessUtility.GetInt(item.OrderID))
            lblCustName.Text = Resources.Resource.lblCustName + ": " + BusinessUtility.GetString(objInvoice.OrdCustName)
            lblSalesRep.Text = Resources.Resource.lblPrintSalesRep + ": " + BusinessUtility.GetString(objInvoice.OrdCustSaleName)
        End If



        lblCustAdd.Text = lblComAdd.Text
        lblMerAdd.Text = lblComAdd.Text
        lblMerCompanyName.Text = lblCompanyName.Text
        lblMerCompanyPhone.Text = lblCompanyPhone.Text
        lblMerDate.Text = lbldate.Text
        lblCustDate.Text = lbldate.Text
        lblMerTransactionID.Text = lblTransaction.Text
        'lblMerTran.Text = lblProIL.Text
        lblMsgc.Text = Request.QueryString("posmsg")
        lblMsgM.Text = Request.QueryString("posmsg")

        'Debit Card
        lblDebCompanyName.Text = lblCompanyName.Text
        lblDebComAddress.Text = lblComAdd.Text
        lblDebPhone.Text = lblCompanyPhone.Text
        lblDebTranBarcode.Text = lblTransaction.Text
        lblDebMCompanyName.Text = lblCompanyName.Text
        lblDebMaddress.Text = lblComAdd.Text
        lblDebMPhone.Text = lblCompanyPhone.Text
        lblDebTransMBarcode.Text = lblTransaction.Text

    End Function


    Public Function funGetReceipt(ByVal strLang As String, Optional ByVal strDefault As String = "") As Boolean
        Dim strCarName, strCardNo, strSubTotal, strTax1, strTax2, strTax3, strTax4, strTotal, strReceived, strReturn, strTaxDesc1, strTaxDesc2, strTaxDesc3, strTaxDesc4, strCardLang, strCardAccount, strCardMsg, strTransType, strTranEndTime, strTranDateTime, strDiscount As String
        Dim strSQL As String
        Dim returnCart As List(Of ReturnCart) = CType(Session.Item("RETURN_CART"), List(Of ReturnCart))
        Dim itemtoUpdate = returnCart.Where(Function(i) i.ProductToReturnQty > 0)

        For Each item As ReturnCart In itemtoUpdate
        Next

        Dim intK As Integer = 0
        Dim dblSubTot As Double = 0.0
        For Each item As ReturnCart In itemtoUpdate
            Dim tempRow As New TableRow()
            For nM As Integer = 0 To 4
                Dim tempCell As New TableCell()
				tempCell.CssClass="lblPOSPrint"
                tempCell.Font.Size = 7

                If nM = 0 Then
                    If strDefault = "1" Then
                        tempCell.Text = "Refund Product <Br />00000000 "
                    Else
                        tempCell.Text = BusinessUtility.GetString(item.ProductName) & "<Br />" & BusinessUtility.GetString(item.UPCCode)
                    End If
                    tempCell.HorizontalAlign = HorizontalAlign.Left
                    tempCell.Width = 85
                ElseIf nM = 1 Then
                    tempCell.Text = BusinessUtility.GetString(item.ProductToReturnQty)
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 32
                ElseIf nM = 2 Then
                    tempCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(CDbl(BusinessUtility.GetString(item.ProductUnitPrice)), "")
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 43
                ElseIf nM = 3 Then
                    Dim pQty As Integer = BusinessUtility.GetDouble(item.ProductToReturnQty)
                    Dim pPrice As Double = BusinessUtility.GetDouble(item.ProductUnitPrice)

                    Dim pAmt As Double = pPrice * pQty
                    dblSubTot += pAmt
                    tempCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(CDbl(pAmt), "")
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 70
                End If

                tempRow.Cells.Add(tempCell)

                strSubTotal = String.Format("{0:F}", CDbl(dblSubTot))
                strTotal = String.Format("{0:F}", CDbl((BusinessUtility.GetDouble(strSubTotal)).ToString()))
                strTransType = "S"
                strReturn = String.Format("{0:F}", CDbl((BusinessUtility.GetDouble(strTotal) - BusinessUtility.GetDouble(strReceived)).ToString()))
                strSubTotal = CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strSubTotal), "")
                strTax1 = CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strTax1), "")
                strTax2 = CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strTax2), "")
                strTax3 = CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strTax3), "")
                strTax4 = CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strTax4), "")
                strDiscount = CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strDiscount), "")
                strTotal = CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strTotal), "")
                strReceived = CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strReceived), "")
                strReturn = CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strReturn), "")

                lblCustTotalAmount.Text = "&nbsp;&nbsp; " & strTotal
                lblMerTotalAmount.Text = "&nbsp;&nbsp; " & strTotal
            Next
            Table1.Rows.Add(tempRow)
            strCarName = String.Empty
            strCardNo = String.Empty
            strCardLang = String.Empty
            strCardAccount = String.Empty
            strCardMsg = String.Empty
            lblRegMessage.Text = String.Empty
            strTranEndTime = String.Empty
            strTranDateTime = DateAndTime.Now
            intK += 1
        Next
        If intK = 0 Then
            Return False
        End If
        lblCustCardName.Text = strCarName
        lblCustCardNo.Text = strCardNo
        lblMCardName.Text = strCarName
        lblMCardNo.Text = strCardNo

        lblDebTotalAmt.Text = lblMerTotalAmount.Text
        lblDebCardNo.Text = strCardNo
        lblDebMTotalAmt.Text = lblMerTotalAmount.Text
        lblDebMCardNo.Text = strCardNo
        If strCardMsg <> "" Then
            Dim strGetvalue() As String = strCardMsg.Split("~")
            If strGetvalue.Length > 1 Then
                lblDebDateTime.Text = strGetvalue(0)
                lblReference.Text = strGetvalue(1)
                lblAuthor.Text = strGetvalue(2)
                lblDebMsg.Text = strGetvalue(3)
                lblDebMDateTime.Text = strGetvalue(0)
                lblDebMReference.Text = strGetvalue(1)
                lblDebMAuthor.Text = strGetvalue(2)
                lblDebMMsg.Text = strGetvalue(3)
            End If
        End If
        Dim strDate() As String
        If Request.QueryString("Reprint") = "yes" Then
            If strTranEndTime <> "" Then
                If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" Then
                    strDate = strTranEndTime.Split(" ")
                Else
                    strDate = strTranDateTime.Split(" ")
                End If
                lbldate.Text = POSDate & strDate(0) & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & POSTime & strDate(1)
            End If
            lblMerDate.Text = lbldate.Text
            lblCustDate.Text = lbldate.Text
        End If
        lblDebMCopyType.Text = lblMerchantCopy
        lblDebCopyType.Text = lblCustomerCopy
        lblDebAccType.Text = strCardAccount
        lblDebMAccType.Text = strCardAccount

        If strTransType = "S" Then
            lblDebType.Text = msgTypePurchase
            lblMPosType.Text = msgTypePurchase
        Else
            lblDebType.Text = msgTypeRefund
            lblMPosType.Text = msgTypeRefund
        End If

        subCreateReceiptTable(strSubTotal, strTax1, strTax2, strTax3, strTotal, strReceived, strReturn, strTax4, strTaxDesc1, strTaxDesc2, strTaxDesc3, strTaxDesc4, strTransType, strDiscount, BusinessUtility.GetInt(Request.QueryString("TID")))
        Return True
    End Function

    Public Sub subCreateReceiptTable(ByVal stSubtotal As String, ByVal strTax1 As String, ByVal strTax2 As String, ByVal strTax3 As String, ByVal strTotal As String, ByVal strReceived As String, ByVal strReturn As String, ByVal strTax4 As String, ByVal strTaxDesc1 As String, ByVal strTaxDesc2 As String, ByVal strTaxDesc3 As String, ByVal strTaxDesc4 As String, ByVal strTransType As String, ByVal strDiscount As String, ByVal iInvID As Integer)
        Dim headerRow As New TableHeaderRow()
        headerRow.HorizontalAlign = HorizontalAlign.Right

        For nM As Integer = 0 To 4
            Dim headerTableCell As New TableHeaderCell()
            Dim footerTableCell As New TableCell()
			 headerTableCell.CssClass="lblPOSPrint" 
            headerTableCell.Font.Size = 7
            If nM = 0 Then
                headerTableCell.Text = msgPrd
                headerTableCell.HorizontalAlign = HorizontalAlign.Left
                headerTableCell.Width = 85
            ElseIf nM = 1 Then
                headerTableCell.Text = msgQty
                headerTableCell.Width = 32
            ElseIf nM = 2 Then
                headerTableCell.Text = msgPrice
                headerTableCell.Width = 43
            ElseIf nM = 3 Then
                headerTableCell.Text = msgTotalPrice
                headerTableCell.Width = 70
            End If
            headerRow.Cells.Add(headerTableCell)
        Next
        Table1.Rows.AddAt(0, headerRow)

        Dim RowSub As New TableRow()

        If (Session("ReceiptRefundAmount") IsNot Nothing) Then
            For nM As Integer = 0 To 1
                Dim tempCell As New TableCell()
				 tempCell.CssClass="lblPOSPrint" 
				 tempCell.Font.Size = 7
                If nM = 0 Then
                    tempCell.Text = msgRefund & "  --"
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 165
                    tempCell.Font.Bold = True
                ElseIf nM = 1 Then
                    'tempCell.Text = String.Format("{0:F}", CDbl((BusinessUtility.GetDouble(Session("ReceiptRefundAmount"))).ToString())) 'CurrencyFormat.GetCompanyCurrencyFormat(CDbl(BusinessUtility.GetDouble(Session("ReceiptRefundAmount"))), "") 'stSubtotal
                    tempCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(Session("ReceiptRefundAmount")), "") 'stSubtotal
                    'CurrencyFormat.GetCompanyCurrencyFormat(CDbl(pAmt), "")
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 65
                End If
                RowSub.Cells.Add(tempCell)
            Next
            Session("ReceiptRefundAmount") = Nothing
        Else
            For nM As Integer = 0 To 1
                Dim tempCell As New TableCell()
				tempCell.CssClass="lblPOSPrint"
                tempCell.Font.Size = 7
                If nM = 0 Then
                    tempCell.Text = msgTotal & "  --"
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 165
                    tempCell.Font.Bold = True
                ElseIf nM = 1 Then
                    tempCell.Text = stSubtotal
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 65
                End If
                RowSub.Cells.Add(tempCell)
            Next
        End If




        tblsubTotal.Rows.Add(RowSub)
        trLineTotalAmt.Visible = False
        trLineMessage.Visible = False
    End Sub

    Public Function funGetPosProduct(ByVal strLang As String, Optional ByVal strDefault As String = "") As Boolean
        Dim strCarName, strCardNo, strSubTotal, strTax1, strTax2, strTax3, strTax4, strTotal, strReceived, strReturn, strTaxDesc1, strTaxDesc2, strTaxDesc3, strTaxDesc4, strCardLang, strCardAccount, strCardMsg, strTransType, strTranEndTime, strTranDateTime, strDiscount As String
        Dim strSQL As String
        Dim custID As String
        Dim drObj As OdbcDataReader
        Dim objDataClass As New clsDataClass
        'If strDefault = "1" Then
        '    strSQL = " SELECT distinct posPrdTransID,sysRegMessage,prdUPCCode,posTransEndTime,posTransDateTime,posSubTotal,posCasReceived,posCashReturned,pt.posTotalValue,pt.posTax1,pt.posTax2,pt.posTax3,pt.posTax4,posProductID,Pdes.prdname as ProductName, posPrdQty, posPrdUnitPrice,posPrdPrice, posPrdTaxCode,prdUPCCode,pt.posTax1Desc,pt.posTax2Desc,pt.posTax3Desc,pt.posTax4Desc,posCardNo,posCardName,posCardLang, posCardAccountType, posCardMsg,postransType, posPercentDiscount FROM posproductno "
        '    strSQL += " Inner join postransaccthdr on postransaccthdr.posAcctTransID=posproductno.posPrdTransID "
        '    strSQL += " left join postransacctdtl on postransacctdtl.posAcctHdrDtlId=postransaccthdr.posAcctHdrID "
        '    strSQL += " Inner join posTransaction as pt on pt.posTransId=posproductno.posPrdTransID"
        '    strSQL += " left join Products on Products.ProductID=posproductno.posProductID"
        '    strSQL += " Inner join sysregister on sysregister.sysRegcode= pt.posTransRegCode"
        '    strSQL += " left join prddescriptions as Pdes on Pdes.ID=Products.ProductID where posPrdTransID='" & Request.QueryString("TID") & "' "
        'Else
        strSQL = " SELECT Distinct i.invItemID, i.invProductID,i.invProductDiscount AS posPercentDiscount,i.invProductTaxGrp,sysTaxCodeDescText, p.prdIntID, p.prdExtID, p.prdUPCCode,"
        strSQL += " CASE i.invProdIDDesc='' WHEN False THEN i.invProdIDDesc Else p.prdName END as ProductName, invProductQty AS posPrdQty, (invProductUnitPrice * invCurrencyExRate) as posPrdUnitPrice, invCurrencyCode, "
        strSQL += " (invProductQty * invProductUnitPrice) AS posPrdPrice,(select sum(invProductQty * invProductUnitPrice)  from invoiceitems invi where invi.invoices_invID ='" & Request.QueryString("TID") & "') AS posSubTotal,"
        strSQL += " i.Tax1 AS posTax1, i.Tax2 AS posTax2, i.Tax3 AS posTax3, i.Tax4 AS posTax4,i.TaxDesc1 AS posTax1Desc, i.TaxDesc2 AS posTax2Desc,i.TaxDesc3 AS posTax3Desc,i.TaxDesc4 AS posTax4Desc,o.invLastUpdatedOn AS posTransDateTime, "
        strSQL += " funGetReceivedAmountByInvoiceID(i.invoices_invID) AS posCasReceived,case i.invProductDiscountType when 'A' then (invProductQty * invProductUnitPrice * invCurrencyExRate) - i.invProductDiscount else  CASE i.invProductDiscount WHEN 0 THEN (invProductQty * invProductUnitPrice * invCurrencyExRate) ELSE (invProductQty * invProductUnitPrice * invCurrencyExRate) - ((invProductQty * invProductUnitPrice * invCurrencyExRate) * i.invProductDiscount/100) END End  as posTotalValue,"
        strSQL += " /*(case i.invProductDiscountType when 'A' then (invProductQty * invProductUnitPrice * invCurrencyExRate) - i.invProductDiscount else  CASE i.invProductDiscount WHEN 0 THEN (invProductQty * invProductUnitPrice * invCurrencyExRate) ELSE (invProductQty * invProductUnitPrice * invCurrencyExRate) - ((invProductQty * invProductUnitPrice * invCurrencyExRate) * i.invProductDiscount/100) END End - IFNULL(acc.ARAmtRcvd,0)) AS posCashReturned,acc.ARAmtRcvdVia AS postransType, */"
        strSQL += " invShpWhsCode, invCustID, invCustType,i.invProductDiscountType, reg.sysRegMessage, pColor.Color{0} AS Color, pSize.Size{0} AS Size,  p.prdIsNonStandard FROM invoiceitems i inner join invoices o on o.invID=i.invoices_invID "
        strSQL += " inner join products p on i.invProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.invProductTaxGrp "
        strSQL += " INNER JOIN ProductClothDesc AS pClothDesc ON pClothDesc.ProductID = p.ProductID "
        strSQL += " INNER JOIN ProductColor AS pColor on pColor.ColorID = pClothDesc.Color "
        strSQL += " INNER JOIN ProductSize AS pSize ON pSize.SizeID = pClothDesc.Size "
        strSQL += " left join accountreceivable acc on  acc.ARInvoiceNo = i.invoices_invID "
        strSQL += " Inner join sysregister AS reg on reg.sysRegcode= o.invRegCode "
        strSQL += " where  i.invoices_invID='" & Request.QueryString("TID") & "'"
        strSQL = String.Format(strSQL, strLang)
        ' and Pdes.descLang ='" + strLang + "'"
        'End If
        drObj = objDataClass.GetDataReader(strSQL)
        Dim intK As Integer = 0
        Dim dblSubTot As Double = 0.0
        While drObj.Read
            Dim tempRow As New TableRow()
            For nM As Integer = 0 To 4
                Dim tempCell As New TableCell()
				tempCell.CssClass="lblPOSPrint"
                tempCell.Font.Size = 7

                If nM = 0 Then
                    If strDefault = "1" Then
                        tempCell.Text = "Refund Product <Br />00000000 "
                    Else
                        Dim sPrdName As String = ""
                        If drObj.Item("prdIsNonStandard") = "1" Then
                            sPrdName = drObj.Item("ProductName").ToString() & "<Br />" & drObj.Item("prdUPCCode").ToString()
                        Else
                            sPrdName = drObj.Item("ProductName").ToString() & "<Br />" & drObj.Item("prdUPCCode").ToString()
                        End If
                        'tempCell.Text = drObj.Item("ProductName").ToString() & " (" & drObj.Item("Color").ToString() & ", " & drObj.Item("Size").ToString() & ") " & "<Br />" & drObj.Item("prdUPCCode").ToString()
                        tempCell.Text = sPrdName
                    End If
                    tempCell.HorizontalAlign = HorizontalAlign.Left
                    tempCell.Width = 85
                ElseIf nM = 1 Then
                    tempCell.Text = drObj.Item("posPrdQty").ToString()
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 32
                ElseIf nM = 2 Then
                    'tempCell.Text = String.Format("{0:F}", CDbl(drObj.Item("posPrdUnitPrice").ToString()))
                    tempCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(CDbl(drObj.Item("posPrdUnitPrice").ToString()), "")
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 43
                ElseIf nM = 3 Then
                    'tempCell.Text = String.Format("{0:F}", CDbl(drObj.Item("posPrdUnitPrice").ToString()))
                    tempCell.Text = BusinessUtility.GetInt(drObj.Item("posPercentDiscount")) ' CurrencyFormat.GetCompanyCurrencyFormat(CDbl(drObj.Item("posPrdUnitPrice").ToString()), "")
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 43
                ElseIf nM = 4 Then
                    'tempCell.Text = String.Format("{0:F}", CDbl(drObj.Item("posPrdPrice").ToString()))
                    Dim pDisc As Double = BusinessUtility.GetDouble(drObj.Item("posPercentDiscount"))
                    Dim pAmt As Double = BusinessUtility.GetDouble(drObj.Item("posPrdPrice"))
                    pAmt = pAmt - (pAmt * pDisc / 100)
                    'tempCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(CDbl(drObj.Item("posPrdPrice").ToString()), "")
                    dblSubTot += pAmt
                    tempCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(CDbl(pAmt), "")
                    tempCell.HorizontalAlign = HorizontalAlign.Right
                    tempCell.Width = 70
                End If

                tempRow.Cells.Add(tempCell)

                Dim objCalcHlpr As New TotalSummary()
                objCalcHlpr.InitInvoiceTotal(BusinessUtility.GetInt(Request.QueryString("TID")))

                Dim taxTotal As Double = 0.0
                For Each taxDtl As TaxItem In objCalcHlpr.ItemTaxList
                    'do something
                    'Carrier.CalculatedPrice =
                    'Carrier.
                    taxTotal += BusinessUtility.GetDouble(taxDtl.CalculatedPrice)
                Next

                strSubTotal = String.Format("{0:F}", CDbl(dblSubTot))
                strTotal = String.Format("{0:F}", CDbl((BusinessUtility.GetDouble(strSubTotal) + BusinessUtility.GetDouble(taxTotal)).ToString()))
                If Convert.ToString(drObj.Item("posCasReceived")) <> "" Then
                    strReceived = String.Format("{0:F}", CDbl(drObj.Item("posCasReceived").ToString()))
                Else
                    strReceived = "0.00"
                End If

                strTransType = "S"
                strReturn = String.Format("{0:F}", CDbl((BusinessUtility.GetDouble(strTotal) - BusinessUtility.GetDouble(strReceived)).ToString()))


                strSubTotal = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strSubTotal), ""))
                strTax1 = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strTax1), ""))
                strTax2 = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strTax2), ""))
                strTax3 = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strTax3), ""))
                strTax4 = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strTax4), ""))
                strDiscount = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strDiscount), ""))
                strTotal = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strTotal), ""))
                strReceived = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strReceived), ""))
                strReturn = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strReturn), ""))


                lblCustTotalAmount.Text = "&nbsp;&nbsp; " & strTotal
                lblMerTotalAmount.Text = "&nbsp;&nbsp; " & strTotal



            Next
            Table1.Rows.Add(tempRow)
            strCarName = String.Empty  'drObj.Item("posCardName").ToString()
            strCardNo = String.Empty 'drObj.Item("posCardNo").ToString()
            strCardLang = String.Empty 'drObj.Item("posCardLang").ToString()
            strCardAccount = String.Empty 'drObj.Item("posCardAccountType").ToString()
            strCardMsg = String.Empty 'drObj.Item("posCardMsg").ToString()
            'lblRegMessage.Text = String.Empty 'drObj.Item("sysRegMessage").ToString()
            lblRegMessage.Text = drObj.Item("sysRegMessage").ToString()
            strTranEndTime = String.Empty 'drObj.Item("posTransEndTime").ToString()
            strTranDateTime = drObj.Item("posTransDateTime").ToString()
            custID = drObj.Item("invCustID").ToString()
            intK += 1
        End While
        If intK = 0 Then
            Return False
        End If
        lblCustCardName.Text = strCarName
        lblCustCardNo.Text = strCardNo
        lblMCardName.Text = strCarName
        lblMCardNo.Text = strCardNo

        lblDebTotalAmt.Text = lblMerTotalAmount.Text
        lblDebCardNo.Text = strCardNo
        lblDebMTotalAmt.Text = lblMerTotalAmount.Text
        lblDebMCardNo.Text = strCardNo
        If strCardMsg <> "" Then
            Dim strGetvalue() As String = strCardMsg.Split("~")
            If strGetvalue.Length > 1 Then
                lblDebDateTime.Text = strGetvalue(0)
                lblReference.Text = strGetvalue(1)
                lblAuthor.Text = strGetvalue(2)
                lblDebMsg.Text = strGetvalue(3)
                lblDebMDateTime.Text = strGetvalue(0)
                lblDebMReference.Text = strGetvalue(1)
                lblDebMAuthor.Text = strGetvalue(2)
                lblDebMMsg.Text = strGetvalue(3)
            End If
        End If
        Dim strDate() As String
        If Request.QueryString("Reprint") = "yes" Then
            If strTranEndTime <> "" Then
                If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" Then
                    strDate = strTranEndTime.Split(" ")
                Else
                    strDate = strTranDateTime.Split(" ")
                End If
                lbldate.Text = POSDate & strDate(0) & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & POSTime & strDate(1)
            End If
            lblMerDate.Text = lbldate.Text
            lblCustDate.Text = lbldate.Text
        End If
        lblDebMCopyType.Text = lblMerchantCopy
        lblDebCopyType.Text = lblCustomerCopy
        lblDebAccType.Text = strCardAccount
        lblDebMAccType.Text = strCardAccount

        objDataClass.CloseDatabaseConnection()
        drObj.Close()

        If strTransType = "S" Then
            lblDebType.Text = msgTypePurchase
            lblMPosType.Text = msgTypePurchase
        Else
            lblDebType.Text = msgTypeRefund
            lblMPosType.Text = msgTypeRefund
        End If

        subCreateTable(strSubTotal, strTax1, strTax2, strTax3, strTotal, strReceived, strReturn, strTax4, strTaxDesc1, strTaxDesc2, strTaxDesc3, strTaxDesc4, strTransType, strDiscount, BusinessUtility.GetInt(Request.QueryString("TID")), BusinessUtility.GetInt(custID))
        Return True
    End Function
    Public Sub subCreateTable(ByVal stSubtotal As String, ByVal strTax1 As String, ByVal strTax2 As String, ByVal strTax3 As String, ByVal strTotal As String, ByVal strReceived As String, ByVal strReturn As String, ByVal strTax4 As String, ByVal strTaxDesc1 As String, ByVal strTaxDesc2 As String, ByVal strTaxDesc3 As String, ByVal strTaxDesc4 As String, ByVal strTransType As String, ByVal strDiscount As String, ByVal iInvID As Integer, ByVal iInvCustID As Integer)
        ' Create a TableHeaderRow. 
        Dim headerRow As New TableHeaderRow()
        headerRow.HorizontalAlign = HorizontalAlign.Right
        Dim objCustCrdt As New CustomerCredit
        Dim dblAvlCrdt As Double
        dblAvlCrdt = BusinessUtility.GetDouble(objCustCrdt.GetAvailableCredit(iInvCustID))

        For nM As Integer = 0 To 4
            Dim headerTableCell As New TableHeaderCell()
            Dim footerTableCell As New TableCell()
            headerTableCell.CssClass = "lblPOSPrint"
            headerTableCell.Font.Size = 7
            If nM = 0 Then
                headerTableCell.Text = msgPrd
                headerTableCell.HorizontalAlign = HorizontalAlign.Left
                headerTableCell.Width = 85
            ElseIf nM = 1 Then
                headerTableCell.Text = msgQty
                headerTableCell.Width = 32
            ElseIf nM = 2 Then
                headerTableCell.Text = msgPrice
                headerTableCell.Width = 43
            ElseIf nM = 3 Then
                headerTableCell.Text = lblPosGrdDisc
                headerTableCell.Width = 70
            ElseIf nM = 4 Then
                headerTableCell.Text = msgTotalPrice
                headerTableCell.Width = 70
            End If
            headerRow.Cells.Add(headerTableCell)
        Next
        Table1.Rows.AddAt(0, headerRow)

        Dim RowSub As New TableRow()
        For nM As Integer = 0 To 1
            Dim tempCell As New TableCell()
			tempCell.CssClass="lblPOSPrint"
            tempCell.Font.Size = 7
            If nM = 0 Then
                tempCell.Text = msgSubTotal & "  --"
                tempCell.HorizontalAlign = HorizontalAlign.Right
                tempCell.Width = 165
                tempCell.Font.Bold = True
            ElseIf nM = 1 Then
                tempCell.Text = stSubtotal
                tempCell.HorizontalAlign = HorizontalAlign.Right
                tempCell.Width = 65
            End If
            RowSub.Cells.Add(tempCell)
        Next
        tblsubTotal.Rows.Add(RowSub)
        Dim objCalcHlpr As New TotalSummary()
        objCalcHlpr.InitInvoiceTotal(BusinessUtility.GetInt(iInvID))

        Dim RowTax As New TableRow()
        Dim IsRow As Boolean = False
        For Each taxDtl As TaxItem In objCalcHlpr.ItemTaxList
            IsRow = True
            If (taxDtl.CalculatedPrice > 0) Then
                RowTax = New TableRow()
                Dim tempTaxDescCell As New TableCell()
				tempTaxDescCell.CssClass="lblPOSPrint"
                tempTaxDescCell.Font.Size = 7
                tempTaxDescCell.Text = taxDtl.TaxCode & "  --"
                tempTaxDescCell.HorizontalAlign = HorizontalAlign.Right
                tempTaxDescCell.Font.Bold = True
                tempTaxDescCell.Width = 158
                RowTax.Cells.Add(tempTaxDescCell)

                Dim tempTaxValueCell As New TableCell()
				tempTaxValueCell.CssClass="lblPOSPrint"
                tempTaxValueCell.Font.Size = 7
                tempTaxValueCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(CDbl(taxDtl.CalculatedPrice), "")
                tempTaxValueCell.HorizontalAlign = HorizontalAlign.Right
                RowTax.Cells.Add(tempTaxValueCell)

                tblTax.Rows.Add(RowTax)
            End If
        Next

        Dim objAccountReceivable As New AccountReceivable()
        Dim dtAcountReceivable As New DataTable()
        dtAcountReceivable = objAccountReceivable.GetInvoicePartialPaymentList(BusinessUtility.GetInt(iInvID))
        Dim IsAddPartialDesc As Boolean = False


        For nR As Integer = 0 To 4
            Dim RowAmount As New TableRow()
            For nM As Integer = 0 To 1
                Dim tempCell As New TableCell()
				tempCell.CssClass="lblPOSPrint"
                tempCell.Font.Size = 7
                If nR = 0 Then
                    If nM = 0 Then
                        tempCell.Text = msgTotal & "  --"
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                        tempCell.Width = 165
                        tempCell.Font.Bold = True
                    ElseIf nM = 1 Then
                        tempCell.Text = strTotal
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                        tempCell.Width = 65
                    End If
                ElseIf nR = 2 Then
                    If ((dtAcountReceivable.Rows.Count > 0) And IsAddPartialDesc = False) Then
                        IsAddPartialDesc = True
                        For Each dRow As DataRow In dtAcountReceivable.Rows
                            If BusinessUtility.GetString(dRow("Mode")) <> "" Then
                                Dim dProw As New TableRow()
                                Dim tempCellp As New TableCell()
								tempCellp.CssClass="lblPOSPrint"
                                tempCellp.Font.Size = 7

                                If (CDbl(dRow("ARAmtRcvd")) < 0) Then
                                    tempCellp.Text = lblPORefund & " " & BusinessUtility.GetString(dRow("Mode")) & "  --"
                                Else
                                    tempCellp.Text = BusinessUtility.GetString(dRow("Mode")) & "  --"
                                End If


                                tempCellp.HorizontalAlign = HorizontalAlign.Right
                                tempCellp.Width = 165
                                tempCellp.Font.Bold = True

                                Dim tempCellv As New TableCell()
                                tempCellv.CssClass = "lblPOSPrint"
                                tempCellv.Font.Size = 7
                                'tempCellv.Text = String.Format("{0:F}", CDbl(dRow("ARAmtRcvd").ToString())) CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(CDbl(strTax1), ""))
                                tempCellv.Text = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(CDbl(dRow("ARAmtRcvd")), "")) ' CurrencyFormat.GetCompanyCurrencyFormat(CDbl(dRow("ARAmtRcvd").ToString()))
                                tempCellv.HorizontalAlign = HorizontalAlign.Right
                                tempCellv.Width = 65
                                dProw.Cells.Add(tempCellp)
                                dProw.Cells.Add(tempCellv)
                                tblAmount.Rows.Add(dProw)
                            End If
                        Next
                    End If

                ElseIf nR = 3 Then
                    If nM = 0 Then
                        If strTransType = "S" Then
                            tempCell.Text = msgBalance & "  --"
                        Else
                            tempCell.Text = lblPOSRefund & "  --"
                        End If
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                        tempCell.Font.Bold = True
                    ElseIf nM = 1 Then
                        tempCell.Text = strReturn
                        tempCell.HorizontalAlign = HorizontalAlign.Right
                    End If
                ElseIf nR = 4 Then
                    If BusinessUtility.GetDouble(dblAvlCrdt) > 0 Then
                        If nM = 0 Then
                            If strTransType = "S" Then
                                tempCell.Text = lblInstoreCreditAvailable & "  --"
                            End If
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                            tempCell.Font.Bold = True
                        ElseIf nM = 1 Then
                            'tempCell.Text = dblAvlCrdt
                            tempCell.Text = String.Format("{0:F}", dblAvlCrdt)
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                        End If
                    End If
                ElseIf nR = 1 Then
                    If BusinessUtility.GetDouble(strDiscount) > 0 Then
                        If nM = 0 Then
                            If strTransType = "S" Then
                                tempCell.Text = "Discount(%)" & "  --"
                            End If
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                            tempCell.Font.Bold = True
                        ElseIf nM = 1 Then
                            tempCell.Text = strDiscount
                            tempCell.HorizontalAlign = HorizontalAlign.Right
                        End If
                    End If
               
                End If
                RowAmount.Cells.Add(tempCell)
            Next
            tblAmount.Rows.Add(RowAmount)
        Next
    End Sub
    Private Sub subClose()
        Response.Write("<script>window.opener.location.reload();</script>")
        Response.Write("<script>window.close();</script>")
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(objCommon.funPrintLang(Request.QueryString("TID")))
        Else
            If Not Session("Language") = "en-CA" Then
                Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
            End If
        End If
    End Sub
End Class

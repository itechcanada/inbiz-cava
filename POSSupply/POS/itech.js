var gAJAXReturnData = null;

//alert("Test");
function calliAJAX(sURL, QueryStr, sErrMsg, isAsync) {
    if (document.getElementById("loading") != null) {
        $("#loading").css("visibility", "visible");
    }
    gAJAXReturnData = "";
    $.ajax({
        type: "GET",
        async: isAsync,    //async:false
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url: sURL,
        data: QueryStr,
        error: function () {
            calliAlert(sErrMsg);
        },
        success: function (sMsg) {
            gAJAXReturnData = sMsg;
        }
    });
}

var closeAllow = true;
window.onbeforeunload = function (evt) {
    if (!closeAllow) {
        alert(getLocalizedString(secLang, "posMsgTransactionIsInProcess"))
        return closeAllow;
    }
}
function calliAlert(sMsg) {
    //    alert(sMsg);
}

//document.write("<script type=\"text/javascript\" src=\"json.js\" ></script>");
function fillProductSearchGrid(prId) {

    if (typeof localArr[prId] != 'undefined') {
        //alert($("#ctl00_cphMaster_grdProcess").css('display'));
        if (isDuplicate('ctl00_cphMaster_grdProcess', prId)) {
            return;
        }

        var trData = localArr[prId].data.split("~");
        var trHtml = "";
        trHtml += "<tr style='color:#333333;' id='" + prId + "'>"
        trHtml += "<td align='left' style='width:70px;'>" + prId + "</td>"
        trHtml += "<td align='left' style='width:70px;'>" + trData[2] + "</td>"
        trHtml += "<td align='left' style='width:250px;'>" + trData[1] + "</td>"
        trHtml += "<td align='right' style='width:80px;'>" + trData[3] + "</td>"
        trHtml += "<td align='right' style='width:80px;'>" + trData[7] + "</td>"
        trHtml += "<td align='center' style='width:30px;'>"
        trHtml += "<input type='image' name='ctl00$cphMaster$grdProcess$ctl02$imgEdit' id='ctl00_cphMaster_grdProcess_ctl02_imgEdit' onclick='addInChartFromSearchGrid(" + prId + "); return false;' src='./Univert_files/add.png' style='border-width:0px;' />"
        trHtml += "</td></tr>"
        $("#ctl00_cphMaster_grdProcess").append(trHtml);
        if ($("#ctl00_cphMaster_grdProcess").css('display') == "none") {
            $("#quantityGrid").css('display', "none")
            if (navigator.appName == "Microsoft Internet Explorer") {
                $("#ctl00_cphMaster_grdProcess").css('display', "block")

            } else {
                $("#ctl00_cphMaster_grdProcess").css('display', "table")

            }
        }
    }
}

function cleanSearchGrid() {
    var tableObj = document.getElementById("ctl00_cphMaster_grdProcess");
    var trArr = tableObj.getElementsByTagName("tr");
    var trCount = trArr.length - 1;
    while (trCount >= 1) {

        $(trArr[trCount]).remove();
        trCount--
    }
}

function isDuplicate(tableId, trID) {

    var tableObj = document.getElementById(tableId);
    var trArr = tableObj.getElementsByTagName("tr");
    var trCount = 1;
    while (trCount < trArr.length) {
        if ($(trArr[trCount]).attr("id") == trID) {
            return true;
        }
        trCount++
    }
    return false;
}

function addInChatGrid(prId) {

    if ($('#ctl00_cphMaster_txtUPCCode').attr('value') == "") {
        return;
    }

    if ($("#ctl00_cphMaster_lblTranMsg").css("display") == "inline") {
        $("#ctl00_cphMaster_lblTranMsg").css("display", "none")
    }
    if (typeof localArr[prId] != 'undefined') {
        //alert($("#ctl00_cphMaster_grdProcess").css('display'));        
        if (isDuplicate('ctl00_cphMaster_grdRequest', prId)) {

            //            var trData = localArr[prId].data.split("~");
            //            var trTax = localArr[prId].tax.split("^");
            //            var tGroupID = trTax[0].split("~");
            //            var vTGroupID = Number(tGroupID[4]);
            //            $("#ctl00_cphMaster_ddlTax option[value='" + vTGroupID + "']").attr("selected", "selected");


            var ddlTaxText = $("#ctl00_cphMaster_ddlTax :selected").text();
            var ddlTaxID = $("#ctl00_cphMaster_ddlTax :selected").val();
            var vPrdActPrice = $('#ctl00_cphMaster_txtPrice').attr('value');
            var vPrdDisc = $('#ctl00_cphMaster_txtDisc').attr('value');
            var totalPrice = $('#ctl00_cphMaster_txtTotalPrice').attr('value')
            var vTaxValue = $('#ctl00_cphMaster_hdnTaxValue').attr('value')
            var prArr = localArr[prId].data.split("~");
            var newPreData = prArr[0] + "~" + prArr[1] + "~" + prArr[2] + "~" + prArr[3] + "~" + prArr[4] + "~" + prArr[5] + "~" + vPrdActPrice + "~" + prArr[7] + "~" + prArr[8] + "~" + vPrdDisc + "~" + ddlTaxID + "~" + ddlTaxText + "~" + totalPrice + "~" + vTaxValue;
            localArr[prId].data = newPreData;
            localArr[prId].tax = $('#ctl00_cphMaster_hdnTaxValue').attr('value')

            addItemQuntity(prId);
            calculetTax(prId);
            calculateSubTotal();
            calculateTotal();

            var myFormattedNum = Number($("#ctl00_cphMaster_txtTotalAmount").attr("value")) - (Number($("#ctl00_cphMaster_txtReceived").attr("value")) + amountRecvived);
            $("#ctl00_cphMaster_txtRefund").val(Number(Math.round(myFormattedNum * 100) / 100).toFixed(2));
            $("#ctl00_cphMaster_cmdAdd").hide()
            $("#ctl00_cphMaster_txtSearch").focus();
            return;
        }
        var trData = localArr[prId].data.split("~");
        var trTax = localArr[prId].tax.split("^");
        var tGroupID = trTax[0].split("~");
        var vTGroupID = Number(tGroupID[4]);

        var isGiftCardProduct = trData[9];
        if (isGiftCardProduct == "1") {
            GetGiftCardNo(prId);
            //alert('Gift Card Product Added');
            return;
        }

        $("#ctl00_cphMaster_ddlTax option[value='" + vTGroupID + "']").attr("selected", "selected");
        var ddlTaxText = $("#ctl00_cphMaster_ddlTax :selected").text();
        var ddlTaxID = $("#ctl00_cphMaster_ddlTax :selected").val();

        var pQty = Math.round(Number($('#ctl00_cphMaster_txtQuantity').attr('value')) * 100) / 100
        dblPrdNetCost = GetProdCalculetedTax(prId) * pQty;
        var vPrdDisc = $('#ctl00_cphMaster_txtDisc').attr('value');
        $('#ctl00_cphMaster_txtTotalPrice').attr('value', Number(Math.round(Number(dblPrdNetCost) * 100) / 100).toFixed(2));

        var trHtml = "";
        //$("#ctl00_cphMaster_hdnTaxValue").val(trData[13]);
        $("#ctl00_cphMaster_hdnTaxValue").attr("value", localArr[prId].tax);
        trHtml += "<tr style='color:#333333;' id='" + prId + "' onclick = 'updateQuantityGrid(" + prId + ")' >"; //
        trHtml += "<td align='left' id='prUPC_" + prId + "' style='width:120px;'>" + trData[2] + "<input type='hidden' id='hdn_PTaxID_" + prId + "'  value='" + vTGroupID + "' /> <input type='hidden' id='hdn_PTaxValue_" + prId + "'  value='" + trData[13] + "' /> <input type='hidden' id='hdn_GiftCardNo_" + prId + "'  value='' />   </td>"
        trHtml += "<td align='left' id='prName_" + prId + "' style='width:250px;'>" + restoreSpecialCh(trData[1]) + "</td>"
        if (typeof $('#ctl00_cphMaster_txtQuantity').attr('value') == 'undefined') {
            trHtml += "<td align='right' style='width:80px;' id='quantity_" + prId + "'>1</td>"
        } else {
            //trHtml += "<td align='right' style='width:80px;' id='quantity_" + prId + "'>" + Math.round(Number($('#ctl00_cphMaster_txtQuantity').attr('value')) * 100) / 100 + "</td>"
            trHtml += "<td align='right' style='width:80px;' id='quantity_" + prId + "'>" + pQty + "</td>"
        }
        trHtml += "<td align='right' style='width:80px;'id='unit_price_" + prId + "'>" + Number(Math.round(Number(trData[6]) * 100) / 100).toFixed(2) + "</td>"
        // Add Disc And Tax Col
        trHtml += "<td align='right' style='width:80px;'id='p_disc_" + prId + "'>" + vPrdDisc + "</td>"
        trHtml += "<td align='right' style='width:80px;'id='p_Tax_" + prId + "'>" + ddlTaxText + " </td>"

        if (typeof $('#ctl00_cphMaster_txtTotalPrice').attr('value') == 'undefined') {
            //trHtml += "<td align='right' style='width:80px;' id='total_price_" + prId + "'>" + Number(Math.round(Number(trData[6]) * 100) / 100).toFixed(2) + "</td>"
            trHtml += "<td align='right' style='width:80px;' id='total_price_" + prId + "'>" + Number(Math.round(Number(dblPrdNetCost) * 100) / 100).toFixed(2) + "</td>"
        } else {
            //trHtml += "<td align='right' style='width:80px;' id='total_price_" + prId + "'>" + Number(Math.round(Number($('#ctl00_cphMaster_txtTotalPrice').attr('value') * 100)) / 100).toFixed(2) + "</td>"
            trHtml += "<td align='right' style='width:80px;' id='total_price_" + prId + "'>" + Number(Math.round(Number(dblPrdNetCost) * 100) / 100).toFixed(2) + "</td>"
        }
        trHtml += "<td align='center' style='width:30px;'>"
        trHtml += "<input type='image' name='ctl00$cphMaster$grdRequest$ctl02$imgRefresh' id='ctl00_cphMaster_grdRequest_ctl02_imgRefresh' src='../images/edit-button.png' style='border-width:0px;' onclick='editUnitPrice(" + prId + "); return false;' /></td>"
        trHtml += "<td align='center' style='width:30px;'>"
        trHtml += "<input type='image' name='ctl00$cphMaster$grdRequest$ctl02$imgDeleteIL' id='ctl00_cphMaster_grdRequest_ctl02_imgDeleteIL' src='../images/delete-button.png' style='border-width:0px;' onclick='removeFromChart(" + prId + "); return false;' />"
        trHtml += "</td></tr>"
        $("#ctl00_cphMaster_grdRequest_header").after(trHtml);
        setChartGridQuEfect()
        if ($("#ctl00_cphMaster_grdRequest").css('display') == "none") {
            if (navigator.appName == "Microsoft Internet Explorer") {
                $("#ctl00_cphMaster_grdRequest").css('display', "block")
            } else {
                $("#ctl00_cphMaster_grdRequest").css('display', "table")
            }
        }
        calculetTax(prId);
        calculateSubTotal();
        calculateTotal();

        var myFormattedNum = Number($("#ctl00_cphMaster_txtTotalAmount").attr("value")) - (Number($("#ctl00_cphMaster_txtReceived").attr("value")) + amountRecvived);
        $("#ctl00_cphMaster_txtRefund").val(Number(Math.round(myFormattedNum * 100) / 100).toFixed(2));
    }
}



function chkGiftCardItemInGrid(sGiftNo) {
    var trArr = document.getElementById("ctl00_cphMaster_grdRequest").getElementsByTagName("tr");
    var isGiftCardExistsInGrid = false;
    var i = 1;
    while (i < trArr.length) {
        var prId = $(trArr[i]).attr("id");
        var sGirdGiftno = $("#hdn_GiftCardNo_" + prId).val()
        if (sGiftNo == sGirdGiftno) {
            isGiftCardExistsInGrid = true;
        }
        i++;
    }
    return isGiftCardExistsInGrid;
}

function AddGiftCardItem(prId, GiftCardNo) {
    if (chkGiftCardItemInGrid(GiftCardNo) == true) {
        alert(getLocalizedString(secLang, "lblGiftCardNoAllreadyAdded"));
        return;
    }

    var trData = localArr[prId].data.split("~");
    var trTax = localArr[prId].tax.split("^");
    var tGroupID = trTax[0].split("~");
    var vTGroupID = Number(tGroupID[4]);

    var isGiftCardProduct = trData[9];

    $("#ctl00_cphMaster_ddlTax option[value='" + vTGroupID + "']").attr("selected", "selected");
    var ddlTaxText = $("#ctl00_cphMaster_ddlTax :selected").text();
    var ddlTaxID = $("#ctl00_cphMaster_ddlTax :selected").val();

    var pQty = Math.round(Number($('#ctl00_cphMaster_txtQuantity').attr('value')) * 100) / 100
    dblPrdNetCost = GetProdCalculetedTax(prId) * pQty;
    var vPrdDisc = $('#ctl00_cphMaster_txtDisc').attr('value');
    $('#ctl00_cphMaster_txtTotalPrice').attr('value', Number(Math.round(Number(dblPrdNetCost) * 100) / 100).toFixed(2));

    var trHtml = "";
    //$("#ctl00_cphMaster_hdnTaxValue").val(trData[13]);
    $("#ctl00_cphMaster_hdnTaxValue").attr("value", localArr[prId].tax);
    trHtml += "<tr style='color:#333333;' id='" + prId + "' onclick = 'updateQuantityGrid(" + prId + ")' >"; //
    trHtml += "<td align='left' id='prUPC_" + prId + "' style='width:120px;'>" + trData[2] + "<input type='hidden' id='hdn_PTaxID_" + prId + "'  value='" + vTGroupID + "' /> <input type='hidden' id='hdn_PTaxValue_" + prId + "'  value='" + trData[13] + "' /> <input type='hidden' id='hdn_GiftCardNo_" + prId + "'  value='" + GiftCardNo + "' />   </td>"
    trHtml += "<td align='left' id='prName_" + prId + "' style='width:250px;'>" + restoreSpecialCh(trData[1]) + "</td>"
    if (typeof $('#ctl00_cphMaster_txtQuantity').attr('value') == 'undefined') {
        trHtml += "<td align='right' style='width:80px;' id='quantity_" + prId + "'>1</td>"
    } else {
        //trHtml += "<td align='right' style='width:80px;' id='quantity_" + prId + "'>" + Math.round(Number($('#ctl00_cphMaster_txtQuantity').attr('value')) * 100) / 100 + "</td>"
        trHtml += "<td align='right' style='width:80px;' id='quantity_" + prId + "'>" + pQty + "</td>"
    }
    trHtml += "<td align='right' style='width:80px;'id='unit_price_" + prId + "'>" + Number(Math.round(Number(trData[6]) * 100) / 100).toFixed(2) + "</td>"
    // Add Disc And Tax Col
    trHtml += "<td align='right' style='width:80px;'id='p_disc_" + prId + "'>" + vPrdDisc + "</td>"
    trHtml += "<td align='right' style='width:80px;'id='p_Tax_" + prId + "'>" + ddlTaxText + " </td>"

    if (typeof $('#ctl00_cphMaster_txtTotalPrice').attr('value') == 'undefined') {
        //trHtml += "<td align='right' style='width:80px;' id='total_price_" + prId + "'>" + Number(Math.round(Number(trData[6]) * 100) / 100).toFixed(2) + "</td>"
        trHtml += "<td align='right' style='width:80px;' id='total_price_" + prId + "'>" + Number(Math.round(Number(dblPrdNetCost) * 100) / 100).toFixed(2) + "</td>"
    } else {
        //trHtml += "<td align='right' style='width:80px;' id='total_price_" + prId + "'>" + Number(Math.round(Number($('#ctl00_cphMaster_txtTotalPrice').attr('value') * 100)) / 100).toFixed(2) + "</td>"
        trHtml += "<td align='right' style='width:80px;' id='total_price_" + prId + "'>" + Number(Math.round(Number(dblPrdNetCost) * 100) / 100).toFixed(2) + "</td>"
    }
    trHtml += "<td align='center' style='width:30px;'>"
    trHtml += "<input type='image' name='ctl00$cphMaster$grdRequest$ctl02$imgRefresh' id='ctl00_cphMaster_grdRequest_ctl02_imgRefresh' src='../images/edit-button.png' style='border-width:0px;' onclick='editUnitPrice(" + prId + "); return false;' /></td>"
    trHtml += "<td align='center' style='width:30px;'>"
    trHtml += "<input type='image' name='ctl00$cphMaster$grdRequest$ctl02$imgDeleteIL' id='ctl00_cphMaster_grdRequest_ctl02_imgDeleteIL' src='../images/delete-button.png' style='border-width:0px;' onclick='removeFromChart(" + prId + "); return false;' />"
    trHtml += "</td></tr>"
    $("#ctl00_cphMaster_grdRequest_header").after(trHtml);
    setChartGridQuEfect()
    if ($("#ctl00_cphMaster_grdRequest").css('display') == "none") {
        if (navigator.appName == "Microsoft Internet Explorer") {
            $("#ctl00_cphMaster_grdRequest").css('display', "block")
        } else {
            $("#ctl00_cphMaster_grdRequest").css('display', "table")
        }
    }
    calculetTax(prId);
    calculateSubTotal();
    calculateTotal();

    var myFormattedNum = Number($("#ctl00_cphMaster_txtTotalAmount").attr("value")) - (Number($("#ctl00_cphMaster_txtReceived").attr("value")) + amountRecvived);
    $("#ctl00_cphMaster_txtRefund").val(Number(Math.round(myFormattedNum * 100) / 100).toFixed(2));
}

function setChartGridQuEfect() {
    var trArr = document.getElementById("ctl00_cphMaster_grdRequest").getElementsByTagName("tr");
    var i = 1;
    while (i < trArr.length) {
        var prId = $(trArr[i]).attr("id");
        if (i == 1) {
            $("#quantity_" + prId).css("font-weight", "bold");
            $("#quantity_" + prId).css("font-size", "12px");
        }
        else {
            $("#quantity_" + prId).css("font-weight", "normal");
            $("#quantity_" + prId).css("font-size", "11px");
            return;
        }
        i++;
    }
}

function populetTabData(tranId) {
    var sURL = "../POSService.asmx/funGetPrdDtlForTab";
    var QueryStr = "strTokan=" + secToken + "&strUserID=" + secUserId + "&strLang=" + secLang + "&strWhshouse=" + secWareHouse + "&strTranID=" + tranId + "&strRegcode=" + secReg
    var sErrMsg = "Error! loadBarCodeItems " + sURL + " Qry:" + QueryStr;
    var isAsync = false;

    gAJAXReturnData = null;
    calliAJAX(sURL, QueryStr, sErrMsg, isAsync);
    if (gAJAXReturnData != null) {
        var sData = load_xml_content_string(gAJAXReturnData);

        var sDataError = sData.split("__error__")
        if (sDataError.length > 1) {
            alert(getLocalizedString(secLang, sDataError[1]))
            //location.href="POS.aspx"
            return;
        }
        if (sData == "POSInvalidTokenNo") {
            alert(getLocalizedString(secLang, sData));
            return;
        }
        var sData_Token = sData.split("__token__");
        setNewToken(secToken, sData_Token[1]);
        var sDataArr = sData_Token[0].split("__item__");
        if (sDataArr.length < 1) {
            alert(sData_Token[0]);
        }
        $(sDataArr).each(function () {
            var objData = this.split("__taxGrp__");
            //addItemInLocal(objData[0],objData[1]);
            var data = objData[0]
            var raw = objData.length > 1 ? objData[1].split("__tags__") : "";
            var taxG = "";
            var tags = "";
            if (raw.length > 0) {
                taxG = raw[0];
                tags = raw[1];
            }

            addItemInLocal(data, taxG, tags);
            addItemInLocalOrg(data, taxG, tags);


            var proData = objData[0].split("~")
            if ((typeof proData[0] != "undefined") && proData[0] != "") {
                resetProduct(proData[0], proData[6])
                updateQuantityGrid(proData[0])
                $('#ctl00_cphMaster_txtQuantity').attr('value', proData[3]);
                //$('#ctl00_cphMaster_txtTotalPrice').attr('value',Number(proData[3])*Number(proData[6]))

                funCalQuantity();
                addInChatGrid(proData[0])
            }
        });
    }
}

function addInChartFromSearchGrid(prId) {
    $("#ctl00_cphMaster_grdProcess").css('display', "none");
    updateQuantityGrid(prId)
    addInChatGrid(prId)
}

var IsProductDelete = false;
function removeFromChart(prId) {
    //    var ddlTaxText = $("#ctl00_cphMaster_ddlTax :selected").text();
    //    var ddlTaxID = $("#ctl00_cphMaster_ddlTax :selected").val();
    //    var vPrdActPrice = $('#ctl00_cphMaster_txtPrice').attr('value');
    //    var vPrdDisc = $('#ctl00_cphMaster_txtDisc').attr('value');
    //    var totalPrice = $('#ctl00_cphMaster_txtTotalPrice').attr('value')

    var tbLength = document.getElementById('ctl00_cphMaster_grdRequest').getElementsByTagName('tr').length;
    var trId = document.getElementById('ctl00_cphMaster_grdRequest').getElementsByTagName('tr');
    reCalculetTax(prId);
    calculateSubTotal();
    while (tbLength == trId.length) {
        $("#" + prId).remove();
        trId = document.getElementById('ctl00_cphMaster_grdRequest').getElementsByTagName('tr');
    }
    if (trId.length < 2) {
        $("#ctl00_cphMaster_grdRequest").css('display', "none")
    }
    calculateSubTotal();
    calculateTotal();

    // Added by mukesh 20140731 
    var myFormattedNum = Number($("#ctl00_cphMaster_txtTotalAmount").attr("value")) - (Number($("#ctl00_cphMaster_txtReceived").attr("value")) + amountRecvived);
    $("#ctl00_cphMaster_txtRefund").val(Number(Math.round(myFormattedNum * 100) / 100).toFixed(2));
    //Added end

    $('#ctl00_cphMaster_txtUPCCode').attr('value', "");
    $('#ctl00_cphMaster_txtProductName').attr('value', "");
    $('#ctl00_cphMaster_txtQuantity').attr('value', "0");
    $('#ctl00_cphMaster_txtPrice').attr('value', Number("0").toFixed(2));
    $('#ctl00_cphMaster_txtTotalPrice').attr('value', Number("0").toFixed(2));

    IsProductDelete = true;
}

function updateQuantityGrid(prId) {
    if (IsProductDelete == true) {
        IsProductDelete = false;
        return;
    }
    if (typeof localArr[prId] != 'undefined') {

        var sData = localArr[prId].data.split("~");
        $('#ctl00_cphMaster_txtProductId').attr('value', sData[0]);
        $('#ctl00_cphMaster_txtUPCCode').attr('value', sData[2]);
        $('#ctl00_cphMaster_txtProductName').attr('value', restoreSpecialCh(sData[1]).replace('<br/>', ' '));

        if (isItemEdit == "1") {
            var t = (getLocalizedString(secLang, "POSEdit"))
            $("#spnAdd").html(t);
            $('#ctl00_cphMaster_txtQuantity').attr('value', Number($("#quantity_" + prId).html()));
            $('#ctl00_cphMaster_txtPrice').attr('value', Number($("#unit_price_" + prId).html()).toFixed(2));
            $('#ctl00_cphMaster_txtDisc').attr('value', Number($("#p_disc_" + prId).html()));
            $("#ctl00_cphMaster_ddlTax option[value='" + Number($("#hdn_PTaxID_" + prId).val()) + "']").attr("selected", "selected");
            $('#ctl00_cphMaster_txtTotalPrice').attr('value', Number($("#total_price_" + prId).html()).toFixed(2));
        }
        else {
            $('#ctl00_cphMaster_txtQuantity').attr('value', '1');
            $('#ctl00_cphMaster_txtPrice').attr('value', Number(Math.round(Number(sData[6]) * 100) / 100).toFixed(2));
            //$('#ctl00_cphMaster_txtDisc').attr('value', sData[9]);
            $('#ctl00_cphMaster_txtDisc').attr('value', Number($("#ctl00_cphMaster_hdnDisc").attr('value')));
            //$("#ctl00_cphMaster_ddlTax option[value='" + sData[10] + "']").attr("selected", "selected");

            var trData = localArr[prId].data.split("~");
            var trTax = localArr[prId].tax.split("^");
            var tGroupID = trTax[0].split("~");
            var vTGroupID = Number(tGroupID[4]);
            $("#ctl00_cphMaster_ddlTax option[value='" + vTGroupID + "']").attr("selected", "selected");

            var pQty = Math.round(Number($('#ctl00_cphMaster_txtQuantity').attr('value')) * 100) / 100
            dblPrdNetCost = GetProdCalculetedTax(prId) * pQty;
            var vPrdDisc = $('#ctl00_cphMaster_txtDisc').attr('value');

            $('#ctl00_cphMaster_txtTotalPrice').attr('value', Number(Math.round(Number(dblPrdNetCost) * 100) / 100).toFixed(2));
        }

        document.getElementById('ctl00_cphMaster_cmdAdd').onclick = function () {
            addInChatGrid(prId);
            return false;
        }
        document.getElementById('ctl00_cphMaster_txtPrice').onchange = function () {
            var Price = parseFloat(document.getElementById('ctl00_cphMaster_txtQuantity').value * document.getElementById('ctl00_cphMaster_txtPrice').value)
            document.getElementById('ctl00_cphMaster_txtTotalPrice').value = Number(Price).toFixed(2);
        }
        if ($("#ctl00_cphMaster_divViewPrd").css('display') == "none") {
            $("#ctl00_cphMaster_divViewPrd").css('display', "block")
        }

        //$("#ctl00_cphMaster_txtPrice").focus();

        var sDataOrg = localArrOrg[prId].data.split("~");
        if (sDataOrg[10] == "1") {
            $("#ctl00_cphMaster_txtPrice").removeAttr("readonly");
            $("#ctl00_cphMaster_txtPrice").focus();
        }
        else {
            $("#ctl00_cphMaster_txtDisc").focus();
            $("#ctl00_cphMaster_txtPrice").attr("readonly", 'readonly');
        }
    }
}

function addItemQuntity(prId) {
    //    var quntity = $('#ctl00_cphMaster_txtQuantity').attr('value')
    //    var totalPrice = $('#ctl00_cphMaster_txtTotalPrice').attr('value')
    //    var pQuntity = $('#quantity_' + prId).html();
    //    var pPrice = $('#total_price_' + prId).html();
    //    $('#quantity_' + prId).html(Math.round((Number(pQuntity) + Number(quntity)) * 100) / 100);
    //    $('#total_price_' + prId).html(Number(Math.round((Number(pPrice) + Number(totalPrice)) * 100) / 100).toFixed(2));


    var ddlTaxText = $("#ctl00_cphMaster_ddlTax :selected").text();
    var ddlTaxID = $("#ctl00_cphMaster_ddlTax :selected").val();
    var vPrdActPrice = $('#ctl00_cphMaster_txtPrice').attr('value');
    var vPrdDisc = $('#ctl00_cphMaster_txtDisc').attr('value');
    var vTaxID = ddlTaxID;
    var vTaxName = ddlTaxText;
    var vTaxValue = $('#ctl00_cphMaster_hdnTaxValue').attr('value');
    var quntity = $('#ctl00_cphMaster_txtQuantity').attr('value')
    var totalPrice = $('#ctl00_cphMaster_txtTotalPrice').attr('value')
    var pQuntity = $('#quantity_' + prId).html();
    var pPrice = $('#total_price_' + prId).html();
    //var pTotQty = (Number(pQuntity) + Number(quntity));
    var pTotQty = 0; //  (Number(pQuntity) + Number(quntity));

    if (isItemEdit == "1") {
        pTotQty = Number(quntity);
        isItemEdit = 0;
        var t = (getLocalizedString(secLang, "cmdCssAdd"))
        $("#spnAdd").html(t);
    }
    else {
        pTotQty = (Number(pQuntity) + Number(quntity));
    }
    //    var vPrdAmt = Number((Number(pQuntity) + Number(quntity)) * Number(vPrdActPrice));
    //    var dblPrdDisc = Number(vPrdAmt * vPrdDisc / 100);
    //    var dblPrdAftDisc = Number(vPrdAmt - dblPrdDisc);
    //    var dblTaxAmount = Number(dblPrdAftDisc * vTaxValue / 100)
    //    var dblPrdNetCost = Math.round(Number(dblPrdAftDisc + dblTaxAmount) * 100) / 100;
    //    $('#quantity_' + prId).html(Math.round((Number(pQuntity) + Number(quntity)) * 100) / 100);

    $('#p_disc_' + prId).html(Number(vPrdDisc));
    $('#quantity_' + prId).html(Math.round((pTotQty) * 100) / 100);
    dblPrdNetCost = GetProdCalculetedTax(prId) * pTotQty;
    $('#total_price_' + prId).html(Number(dblPrdNetCost).toFixed(2));
    $('#unit_price_' + prId).html(Number(vPrdActPrice).toFixed(2));
    $('#ctl00_cphMaster_txtTotalPrice').attr('value', Number(dblPrdNetCost).toFixed(2));
    //    dblPrdNetCost = CalcSingleProdPrice(prId);
    //    $('#total_price_' + prId).html(Number(dblPrdNetCost).toFixed(2));
    //    $('#ctl00_cphMaster_txtTotalPrice').attr('value', Number(dblPrdNetCost).toFixed(2));
    $('#p_disc_' + prId).html(vPrdDisc);
    $('#p_Tax_' + prId).html(ddlTaxText);
    $('#hdn_PTaxID_' + prId).val(ddlTaxID);
    //calculetTax(prId);
}


function CalcSingleProdPrice(prId) {
    var quntity = Number($('#ctl00_cphMaster_txtQuantity').attr('value'));
    return dblPrdNetCost = GetProdCalculetedTax(prId) * pTotQty;
}

function calculateSubTotal() {
    var trId = document.getElementById('ctl00_cphMaster_grdRequest').getElementsByTagName('tr');
    var i = 1;
    var subTotol = 0;
    while (i < trId.length) {
        subTotol += Number($('#total_price_' + $(trId[i]).attr('id')).html());
        i++;
    }
    $('#ctl00_cphMaster_txtSubTotal').attr('value', Number(Math.round(subTotol * 100) / 100).toFixed(2));
}

function calculateTotal() {

    var trId = $(document.getElementById("ctl00_cphMaster_tblTotalDesription")).find('tr');
    var i = 1;
    var total = 0;
    while (i < trId.length) {
        //while (i < trId.length + 1) {
        var input = trId[i].getElementsByTagName("input")[0];
        total += Number($(input).attr('value'));
        i++
    }

    total += Number($('#ctl00_cphMaster_txtSubTotal').attr('value'));
    $('#ctl00_cphMaster_txtTotal').attr('value', Math.round(total * 100) / 100);
    $('#ctl00_cphMaster_txtTotalAmount').attr('value', Number(Math.round(total * 100) / 100).toFixed(2));
    $('#ctl00_cphMaster_txtRefund').attr('value', Number(Math.round(total * 100) / 100).toFixed(2));

    TotalAmountBeforePartial = $('#ctl00_cphMaster_txtTotalAmount').attr('value');
    vRemaingAmountToBePaid = TotalAmountBeforePartial - amountRecvived;
}

function populetItemsByCategory(control) {
    var catID = $(control).attr('name')
    // alert(catID);
    var i = 0;
    var tampArr = new Array()
    var tampKeyArr = new Array();
    var tdCount = 0;
    var tblHtml = "<tr>";
    while (i < localArr.length) {
        if (typeof localArr[i] != 'undefined') {
            if (typeof localArr[i].category != 'undefined') {
                var tampCatArr = localArr[i].category.split("~");
                $(tampCatArr).each(function () {
                    if (this == catID) {
                        var prArr = localArr[i].data.split("~");
                        var key = prArr[1] + "_" + prArr[0]
                        tampKeyArr[i] = key;
                        tampArr[key] = localArr[i].data;
                    }
                });
            }
        }
        i++;
    }
    tampKeyArr.sort();
    i = 0
    $(tampKeyArr).each(function () {
        if (typeof tampArr[this] != "undefined") {
            var prData = tampArr[this].split("~");
            if (tdCount == 8) {
                tblHtml += "</tr><tr>";
                tdCount = 0
            }
            tblHtml += "<td class='gridryt-space' valign='top' align='center' style=''>"
            tblHtml += "<input name='ctl00$cphMaster$dlPOSCatgProduct$ctl00$hdnkit' id='ctl00_cphMaster_dlPOSCatgProduct_ctl00_hdnkit' value='0' type='hidden'> "
            tblHtml += "<a id='aPrdKit' style='cursor: pointer;'></a>"
            tblHtml += "<table id='productsByCategory_" + prData[0] + "' onclick='addInChartFromSearchGrid(" + prData[0] + "); return false;' style='border: 1pt solid rgb(0, 0, 0); cursor: pointer;' class='inner-grid in-poptd' border='0' cellpadding='2' cellspacing='2' height='55' width='90%'>"
            tblHtml += "<tbody><tr><td align='center'>"
            tblHtml += "<span id='ctl00_cphMaster_dlPOSCatgProduct_ctl00_imgbtnPOSCatgProductName' title='" + restoreSpecialCh(prData[1]) + "' style='color: #2d4157; font-weight: bold;'>" + restoreSpecialCh(prData[1]) + "</span>"
            tblHtml += "</td></tr></tbody></table>"
            tblHtml += "<a id='add' class='add-btn gridaction-btn' onclick='addInChartFromSearchGrid(" + prData[0] + "); return false;' href='javascript:void(0)'></a>"
            tblHtml += "<a id='del' class='del-btn gridaction-btn' onclick='addInChartFromSearchGrid(" + prData[0] + "); return false;'  href='javascript:void(0)'></a><div class='clr'></div></td>"
            i++;
            tdCount++;
        }
    });

    while (tdCount != 3) {
        tblHtml += "<td style='height: 100px; width: 80px;' valign='top'></td>"
        tdCount++
    }

    $("#ctl00_cphMaster_dlPOSCatgProduct").empty();
    $("#ctl00_cphMaster_dlPOSCatgProduct").append(tblHtml + "</tr>")
    //$("#ctl00_cphMaster_divPOSCategory").css("display","none")
    var browserWidth = 0;
    browserWidth = (document.documentElement.clientWidth - 1000) / 2;
    document.getElementById('ctl00_cphMaster_divPOSCatgProductMenu').style.position = "absolute";
    //document.getElementById('ctl00_cphMaster_divPOSCatgProductMenu').style.left=browserWidth +"px";
    var isiPad = navigator.userAgent.match(/iPad/i) != null;
    if (isiPad) {
        browserWidthNew = browserWidth - 18;
        document.getElementById('ctl00_cphMaster_divPOSCatgProductMenu').style.right = browserWidthNew + "px";
    }
    else {
        document.getElementById('ctl00_cphMaster_divPOSCatgProductMenu').style.right = browserWidth + "px";
    }
    //document.getElementById('ctl00_cphMaster_divPOSCatgProductMenu').style.right=browserWidth +"px";
    document.getElementById('ctl00_cphMaster_divPOSCatgProductMenu').style.top = "192px";
    $("#ctl00_cphMaster_divPOSCatgProductMenu").css("display", "block")


}

function getAllCalculatedTax(prId) {
    var taxs = localArr[prId].tax.split("^");
    var taxArr = new Array;
    var i = 0;
    while (i < taxs.length) {
        if (typeof taxs[i].split("~")[2] != 'undefined') {
            taxArr[i] = taxs[i].split("~")[0] + "~" + Number(getSingleTax(prId, taxs[i].split("~")[2]))
        }
        i++;
    }
    return taxArr;
}

function getSingleTax(prId, seqNo) {
    var taxs = localArr[prId].tax.split("^");
    var i = 0;
    while (i < taxs.length) {
        if (taxs[i].split("~")[2] == seqNo) {
            if (taxs[i].split("~")[3] == 1) {
                return getTaxOnTotal(prId);
            }
            //            return Math.round((Number(taxs[i].split("~")[1]) / 100) * 100) / 100 * Math.round(Number(localArr[prId].data.split("~")[6]) * 100) / 100
            //                       var vDis = Number($('#ctl00_cphMaster_txtDisc').attr('value'))

            var vDis = Number($('#p_disc_' + prId).html());

            //var pDis = Math.round((Number(localArr[prId].data.split("~")[9]) / 100) * 100);
            //var pDis = Math.round((Number(vDis) / 100) * 100);
            var pDis = vDis;  //Math.round((Number(vDis) / 100) * 100);
            //var pCost = Math.round((Number(localArr[prId].data.split("~")[6]) / 100) * 100);
            var pCost = Number((Number(localArr[prId].data.split("~")[6]) / 100) * 100).toFixed(2);
            var pDisAmt = (pCost - ((pCost * pDis) / 100))

            //var calcAmt = ((pDisAmt * Number(taxs[i].split("~")[1])) / 100)
            var calcAmt = Number((pDisAmt * Number(taxs[i].split("~")[1])) / 100).toFixed(2)
            return calcAmt;

            // Math.round((Number(taxs[i].split("~")[1]) / 100) * 100) / 100 * Math.round(Number(pDisAmt) * 100) / 100


            //return Math.round((Number(taxs[i].split("~")[1]) / 100) * 100) / 100 * Math.round(Number(pDisAmt) * 100) / 100
        }
        i++;
    }
}

function getTaxOnTotal(prId) {
    var taxs = localArr[prId].tax.split("^");
    var i = 0;
    var taxAmt = 0;
    var onTlIndex = 0;
    while (i < taxs.length) {
        if (typeof taxs[i].split("~")[2] != 'undefined') {
            if (taxs[i].split("~")[3] == 1) {
                onTlIndex = i;
            } else {
                taxAmt += getSingleTax(prId, taxs[i].split("~")[2])
            }
        }
        i++;
    }
    //return Math.round((Number(taxAmt + Number(localArr[prId].data.split("~")[6]))) * 100) / 100 * Math.round((Number(taxs[onTlIndex].split("~")[1]) / 100) * 100) / 100
    return Math.round((Number(taxAmt + Number(localArr[prId].data.split("~")[6]))) * 100) / 100 * Math.round((Number(taxs[onTlIndex].split("~")[1]) / 100) * 100) / 100
}

function calculetTax(prId) {
    $('#ctl00_cphMaster_trSubTotal').nextAll().remove();

    var trIds = document.getElementById("ctl00_cphMaster_grdRequest").getElementsByTagName("tr");
    var prQuery = ""
    var vAddAftrRow = "";
    $(trIds).each(function () {
        var prId = $(this).attr("id");
        if (prId != "" && prId != "ctl00_cphMaster_grdRequest_header") {

            var taxs = getAllCalculatedTax(prId);
            var i = 0
            var trTax = ""
            while (i < taxs.length) {
                if ((typeof taxs[i].split("~")[0] != 'undefined') && taxs[i].split("~")[0] != "" && taxs[i].split("~")[0] != "No Taxes") {
                    var taxName = taxs[i].split("~")[0];
                    var taxPerUnit = taxs[i].split("~")[1];
                    var preTax = getDuplicateTax(taxName)
                    if (preTax == "") {
                        trTax = "<tr id='ctl00_cphMaster_trTax_" + taxName.replace(" ", "") + "' name='taxTr'>"
                        trTax += "<td align='right'><span id='ctl00_cphMaster_lblTax_" + taxName.replace(" ", "") + "' class='lblBold' name='taxLable'>" + taxName + "</span></td>"
                        trTax += "<td align='right'><input name='ctl00$cphMaster$txtTax_" + taxName.replace(" ", "") + "' value='" + Number(calculetSameTax('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", ""), taxPerUnit, prId)).toFixed(2) + "' class='frminpt fom-inputlarge' readonly='readonly' id='ctl00_cphMaster_txtTax_" + taxName.replace(" ", "") + "' style='width: 140px; text-align: right;' type='text'></td></tr>"
                        if (vAddAftrRow == "") {
                            $('#ctl00_cphMaster_trSubTotal').after(trTax);
                            vAddAftrRow = "#ctl00_cphMaster_trTax_" + taxName.replace(" ", "")
                        }
                        else {
                            $(vAddAftrRow).after(trTax);
                        }
                    } else {
                        $('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", "")).attr("value", Number(calculetSameTax('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", ""), taxPerUnit, prId)).toFixed(2));
                    }
                }
                i++;
            }
        }
    });
    //$('#ctl00_cphMaster_trSubTotal').nextAll().remove();

    //$('#ctl00_cphMaster_trSubTotal').after(trTax);
}

function getDuplicateTax(taxName) {
    var taxLables = document.getElementsByName("taxLable");
    var j = 0;
    while (j < taxLables.length) {
        if ($(taxLables[j]).html() == taxName) {
            return $(taxLables[j]).attr("id").split("_")[3];
        }
        j++;
    }
    return "";
}

function calculetSameTax(fieldObj, value, prdID) {
    var preValue = $(fieldObj).attr('value');
    var taxTotal = 0;
    var pQty = Number($("#quantity_" + prdID).html());
    if (typeof preValue != "undefined") {
        //taxTotal = Number($("#ctl00_cphMaster_txtQuantity").attr("value")) * Number(value) + Number(preValue);
        taxTotal = Number(pQty) * Number(value) + Number(preValue);
    } else {
        //        taxTotal = Number($("#ctl00_cphMaster_txtQuantity").attr("value") * Number(value));
        taxTotal = Number(pQty * Number(value));
    }
    return Math.round(taxTotal * 100) / 100
}

function reCalculetTax(prId) {
    var taxs = getAllCalculatedTax(prId);
    var i = taxs.length - 1
    while (i >= 0) {
        if ((typeof taxs[i].split("~")[0] != 'undefined') && taxs[i].split("~")[0] != "") {
            var taxName = taxs[i].split("~")[0].replace(" ", "")
            var taxPerUnit = taxs[i].split("~")[1];
            $('#ctl00_cphMaster_txtTax_' + taxName).attr("value", Math.round((Number($('#ctl00_cphMaster_txtTax_' + taxName).attr("value")) - Number($("#quantity_" + prId).html()) * Number(taxPerUnit)) * 100) / 100);
            if (Math.round(Number($('#ctl00_cphMaster_txtTax_' + taxName).attr("value") * 100)) / 100 <= 0) {
                $('#ctl00_cphMaster_trTax_' + taxName).remove();
            }
        }
        i--;
    }
    calculateTotal();
}

function showKitProducts() {
    var prTbls = $('#ctl00_cphMaster_dlPOSCatgProduct').find('table');
    var i = 0
    while (i < prTbls.length) {
        var prId = prTbls[i].id.split("_")[1]
        var isKit = localArr[prId].data.split("~")[7];
        if (isKit == "1" && (typeof $(prTbls[i]).attr("id") != "undefined")) {
            $(prTbls[i]).attr("bgColor", "#999eff")
            prTbls[i].onclick = function () {
                var prId = $(this).attr("id").split("_")[1];
                populetProductKitContent(prId);
            }
        }
        i++
    }
}

function populetProductKitContent(prId) {

    var trArr = document.getElementById("ctl00_cphMaster_grdPrdKitContent").getElementsByTagName("tr")
    $(trArr).each(function () {
        if ((typeof $(this).attr("id") != "undefined") && $(this).attr("id") != "") {
            $(this).remove();
        }
    });
    var sURL = "../POSService.asmx/funGetKitItem";
    var QueryStr = "strTokan=" + secToken + "&strLang=" + secLang + "&strUserID=" + secUserId + "&strProductID=" + prId;
    var sErrMsg = "Error! loadBarCodeItems " + sURL + " Qry:" + QueryStr;
    var isAsync = false;
    gAJAXReturnData = null;
    calliAJAX(sURL, QueryStr, sErrMsg, isAsync);
    if (gAJAXReturnData != null) {
        var sData = load_xml_content_string(gAJAXReturnData);
        var sDataError = sData.split("__error__")
        if (sDataError.length > 1) {
            alert(getLocalizedString(secLang, sDataError[1]))
            location.href = "POS.aspx"
            return;
        }
        if (sData == "POSInvalidTokenNo") {
            alert(getLocalizedString(secLang, sData));
            return;
        }
        var sData_Token = sData.split("__token__");
        setNewToken(secToken, sData_Token[1]);
        var sDataArr = sData_Token[0].split("__item__");
        if (sDataArr[0] == "POSNoProductfound") {
            alert(getLocalizedString(secLang, sData_Token[0]));
            return;
        }
        $(sDataArr).each(function () {
            var objData = this.split("~");
            if (objData[0] != "") {
                var trHtml = "";
                trHtml += "<tr style='color: rgb(51, 51, 51);' id=" + objData[0] + ">";
                trHtml += "<td style='width: 200px;' align='left'>" + objData[1] + "</td>";
                trHtml += "<td style='width: 200px;' align='left'>" + objData[2] + "</td></tr>"
                $('#ctl00_cphMaster_grdPrdKitContent').append(trHtml);
            }
        });
    }
    document.getElementById("ctl00_cphMaster_imgCmdAddKitPrd").onclick = function () {

        addInChartFromSearchGrid(prId);
        $('#ctl00_cphMaster_divProductKitContent').css('display', "none");
        return false;
    }
    var browserWidth = 0;
    browserWidth = (document.documentElement.clientWidth - 500) / 2;
    $('#ctl00_cphMaster_divProductKitContent').css('display', "block");
    $('#ctl00_cphMaster_divProductKitContent').css('position', "absolute");
    $('#ctl00_cphMaster_divProductKitContent').css('top', "150px");
    $('#ctl00_cphMaster_divProductKitContent').css('left', browserWidth + "px");
}

function getReqGrdQuery() {
    var trIds = document.getElementById("ctl00_cphMaster_grdRequest").getElementsByTagName("tr");
    var prQuery = ""
    $(trIds).each(function () {
        var prId = $(this).attr("id");
        if (prId != "" && prId != "ctl00_cphMaster_grdRequest_header") {
            //prQuery += prId + "~" + $("#quantity_" + prId).html() + "~" + $("#prName_" + prId).html() + "~" + $("#unit_price_" + prId).html() + "~" + $("#total_price_" + prId).html() + "__taxGrp__" + getTaxRegQuery(prId) + "__item__";
            //prQuery += prId + "~" + $("#quantity_" + prId).html() + "~" + $("#prName_" + prId).html() + "~" + $("#unit_price_" + prId).html() + "~" + $("#total_price_" + prId).html() + "~" + $("#p_disc_" + prId).html() + "~" + $("#hdn_PTaxID_" + prId).val() + "~" + $("#hdn_GiftCardNo_" + prId).val() + "__taxGrp__" + getTaxRegQuery(prId) + "__item__";
            if ((isLayawayOrder == "1") && ($("#hdn_GiftCardNo_" + prId).val() != "")) {
            }
            else {
                prQuery = prQuery + prId + "~" + $("#quantity_" + prId).html() + "~" + $("#prName_" + prId).html() + "~" + $("#unit_price_" + prId).html() + "~" + $("#total_price_" + prId).html() + "~" + $("#p_disc_" + prId).html() + "~" + $("#hdn_PTaxID_" + prId).val() + "~" + $("#hdn_GiftCardNo_" + prId).val() + "__taxGrp__" + getTaxRegQuery(prId) + "__item__";
            }
        }
    });
    return prQuery;
}


function getReqGrdDetail() {
    var trIds = document.getElementById("ctl00_cphMaster_grdRequest").getElementsByTagName("tr");
    var prQuery = ""
    $(trIds).each(function () {
        var prId = $(this).attr("id");
        if (prId != "" && prId != "ctl00_cphMaster_grdRequest_header") {
            prQuery += prId + "~" + $("#prName_" + prId).html() + "~" + $("#prUPC_" + prId).html() + "~" + $("#quantity_" + prId).html() + "~" + $("#unit_price_" + prId).html() + "~" + $("#total_price_" + prId).html() + "__item__";
        }
    });
    return prQuery;
}

function getTaxRegQuery(prId) {
    var taxs = getAllCalculatedTax(prId);
    var taxQury = "";
    var i = 0;
    while (i < taxs.length) {
        taxQury += taxs[i].split("~")[0] + "~" + Math.round((Number(taxs[i].split("~")[1]) * Number($("#quantity_" + prId).html())) * 100) / 100 + "^"
        i++;
    }
    return taxQury;
}

function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
            var obj = actual[i];
            newArray.push(obj);
        } else {
            newArray.push(undefined);
        }
    }
    //    for (var i in actual) {
    //        if (actual[i]) {
    //            newArray.push(actual[i]);
    //        }
    //    }
    return newArray;
}

function addItemInLocal(objData, objTax, objTags) {
    var obj = new Object
    obj.data = objData
    obj.tax = objTax
    obj.tags = objTags
    var objId = obj.data.slice(0, obj.data.indexOf("~"));
    if ((typeof localArr[objId] == "undefined") && objId != "") {
        localArr[objId] = obj;
        //        localArr.cleanArray(undefined);
        //        cleanArray(localArr)
        return true;
    }
}




function addItemInLocalOrg(objData, objTax, objTags) {
    var obj = new Object
    obj.data = objData
    obj.tax = objTax
    obj.tags = objTags
    var objId = obj.data.slice(0, obj.data.indexOf("~"));
    if ((typeof localArrOrg[objId] == "undefined") && objId != "") {
        localArrOrg[objId] = obj;
        return true;
    }
}

function loadBarCodeItems() {
    var vlaywayOrderID = getParameterByName('laywayOrderID');
    var sURL = "../POSService.asmx/subGetProductsArray";
    var QueryStr = "strTokan=" + secToken + "&strLang=" + secLang + "&strWhsHouse=" + secWareHouse + "&strUserID=" + secUserId + "&strBarcode=" + "&strRegcode=" + secReg + "&strLaywayOrderID=" + vlaywayOrderID;
    var sErrMsg = "Error! loadBarCodeItems " + sURL + " Qry:" + QueryStr;
    var isAsync = false;
    gAJAXReturnData = null;
    calliAJAX(sURL, QueryStr, sErrMsg, isAsync);
    if (gAJAXReturnData != null) {
        var sData = parseXMLForBarCodeData(gAJAXReturnData); //load_xml_content_string(gAJAXReturnData);        
        var sDataError = sData.split("__error__")
        if (sDataError.length > 1) {
            alert(getLocalizedString(secLang, sDataError[1]))
            return;
        }
        if (sData == "POSInvalidTokenNo") {
            alert(getLocalizedString(secLang, sData));
            return;
        }
        var sData_Token = sData.split("__token__");
        setNewToken(secToken, sData_Token[1]);
        var sDataArr = sData_Token[0].split("__item__");
        if (sDataArr.length < 1) {
            alert(sData_Token[0]);
        }
        $(sDataArr).each(function () {
            var objData = this.split("__taxGrp__");
            var data = objData[0]
            var raw = objData.length > 1 ? objData[1].split("__tags__") : "";
            var taxG = "";
            var tags = "";
            if (raw.length > 0) {
                taxG = raw[0];
                tags = raw[1];
            }

            addItemInLocal(data, taxG, tags);
            addItemInLocalOrg(data, taxG, tags);
        });

        localArr = cleanArray(localArr);
    }
    return true;
}

function load_xml_content_string(xmlData) {
    var dataStr = xmlData.documentElement.childNodes[0].nodeValue
    if (dataStr.indexOf("<") == 0) {
        dataStr = dataStr.substr(dataStr.indexOf(">") + 1, dataStr.length - 1);
    }
    return dataStr;
}

function searchProdect() {
    var arrCount = 0;
    var pName = $("#ctl00_cphMaster_txtSearch").attr("value");
    $("#ctl00_cphMaster_txtSearch").attr("value", "");
    if ($.trim(pName) == "") {
        //        alert(getLocalizedString(secLang, "msgFillUPCCode"));
        return;
    }
    while (arrCount < localArr.length) {
        var dataArr = localArr[arrCount]
        if ((typeof dataArr != 'undefined') && dataArr.data != "") {
            var sData = dataArr.data.split("~");
            if (sData[2] == $.trim(pName)) {
                updateQuantityGrid(sData[0])
                addInChatGrid(sData[0]);
                $("#ctl00_cphMaster_txtSearch").focus();
                return;
            }
        }
        arrCount++;
    }

    while (arrCount < localArr.length) {
        var dataArr = localArr[arrCount]

        if ((typeof dataArr != 'undefined') && dataArr.data != "" && dataArr.tags != "") {
            var sData = dataArr.data.split("~");
            var sTags = dataArr.tags.split("^");
            for (var iCount = 0; iCount < sTags.length; iCount++) {
                if (sTags[iCount] == $.trim(pName)) {
                    updateQuantityGrid(sData[0])
                    addInChatGrid(sData[0]);
                    $("#ctl00_cphMaster_txtSearch").focus();
                    return;
                }
            }
        }
        arrCount++;
    }

    addItemByBarCodeService(pName);
}

function addItemByTagCodeService(barCode) {
    var sURL = "../POSService.asmx/subGetProductsArrayByTag";
    var QueryStr = "strTokan=" + secToken + "&strLang=" + secLang + "&strWhsHouse=" + secWareHouse + "&strUserID=" + secUserId + "&strBarcode=" + $.trim(barCode) + "&strRegcode=" + secReg;
    var sErrMsg = "Error! loadBarCodeItems " + sURL + " Qry:" + QueryStr;
    var isAsync = false;
    gAJAXReturnData = null;
    calliAJAX(sURL, QueryStr, sErrMsg, isAsync);
    if (gAJAXReturnData != null) {
        var sData = parseXMLForBarCodeData(gAJAXReturnData);
        var sDataError = sData.split("__error__")
        if (sDataError.length > 1) {
            alert(getLocalizedString(secLang, sDataError[1]))
            //location.href="POS.aspx"
            return;

        }
        if (sData == "POSInvalidTokenNo") {
            alert(getLocalizedString(secLang, sData));
            return;
        }
        var sDataArr = sData.split("__token__");
        setNewToken(secToken, sDataArr[1]);
        var objData = sDataArr[0].split("__taxGrp__");
        if (objData.length <= 1) {
            //alert(sDataArr[0]);
            $("#ctl00_cphMaster_lblMsg").html(getLocalizedString(secLang, sDataArr[0]));
            isMasterNeeded = true;
        }
        //addItemInLocal(objData[0],objData[1]);
        var data = objData[0]
        var raw = objData.length > 1 ? objData[1].split("__tags__") : "";
        var taxG = "";
        var tags = "";
        if (raw.length > 0) {
            taxG = raw[0];
            tags = raw[1];
        }

        addItemInLocal(data, taxG, tags);
        addItemInLocalOrg(data, taxG, tags);

        sData = objData[0].split("~");
        updateQuantityGrid(sData[0])
        addInChatGrid(sData[0]);
    }
}

function addItemByBarCodeService(barCode) {
    var vlaywayOrderID = getParameterByName('laywayOrderID');
    var sURL = "../POSService.asmx/subGetProductsArray";
    var QueryStr = "strTokan=" + secToken + "&strLang=" + secLang + "&strWhsHouse=" + secWareHouse + "&strUserID=" + secUserId + "&strBarcode=" + $.trim(barCode) + "&strRegcode=" + secReg + "&strLaywayOrderID=" + vlaywayOrderID;
    var sErrMsg = "Error! loadBarCodeItems " + sURL + " Qry:" + QueryStr;
    var isAsync = false;
    gAJAXReturnData = null;
    calliAJAX(sURL, QueryStr, sErrMsg, isAsync);
    if (gAJAXReturnData != null) {
        var sData = parseXMLForBarCodeData(gAJAXReturnData);
        var sDataError = sData.split("__error__")
        if (sDataError.length > 1) {
            alert(getLocalizedString(secLang, sDataError[1]))
            //location.href="POS.aspx"
            return;

        }
        if (sData == "POSInvalidTokenNo") {
            alert(getLocalizedString(secLang, sData));
            return;
        }
        var sDataArr = sData.split("__token__");
        setNewToken(secToken, sDataArr[1]);
        var objData = sDataArr[0].split("__taxGrp__");
        if (objData.length <= 1) {
            //alert(sDataArr[0]);
            $("#ctl00_cphMaster_lblMsg").html(getLocalizedString(secLang, sDataArr[0]));
            isMasterNeeded = true;
        }
        //addItemInLocal(objData[0],objData[1]);
        var data = objData[0]
        var raw = objData.length > 1 ? objData[1].split("__tags__") : "";
        var taxG = "";
        var tags = "";
        if (raw.length > 0) {
            taxG = raw[0];
            tags = raw[1];
        }

        addItemInLocal(data, taxG, tags);
        addItemInLocalOrg(data, taxG, tags);

        sData = objData[0].split("~");
        updateQuantityGrid(sData[0])
        addInChatGrid(sData[0]);
        $("#ctl00_cphMaster_txtSearch").focus();
    }
}

function setNewToken(preToken, newToken) {
    if (secToken == preToken) {
        secToken = newToken;
    }
}

function doAppletTransaction(transactionType) {
    var answer = false;
    if (secMerchantID != "yes") {
        var totalAmt = $('#ctl00_cphMaster_txtTotalAmount').attr('value');
        $('#ctl00_cphMaster_txtReceived').attr('value', Number(totalAmt).toFixed(2));
        return;
    }
    if (transactionType == "TS") {
        answer = confirm(getLocalizedString(secLang, "msgCloseBatch") + '<' + secRegName + '>');
    }
    if (transactionType == "TI") {
        answer = confirm(getLocalizedString(secLang, "msgInitializePinpad"));
    }
    if (transactionType == "00" || transactionType == "01") {
        if (document.getElementById("ctl00_cphMaster_grdRequest").getElementsByTagName("tr").length > 1) {
            var totalAmt = $('#ctl00_cphMaster_txtTotalAmount').attr('value');
            $('#ctl00_cphMaster_txtReceived').attr('value', Number(totalAmt).toFixed(2));
            //alert('./PinPad.aspx?transactionType='+transactionType+"&host="+secHost+"&secCmpName="+secCmpName+"&secCmpPhone="+secCmpPhone+"&secCmpAddress="+secCmpAddress+"&secRegMsg="+secRegMsg+"&hostPort="+secHostPort+"&terminalID="+secTerminalID+"&amount="+totalAmt+"&logFile="+secRegLogFile+"&productDetail="+getReqGrdDetail()+"&taxDetail="+getTax());
            $.showAkModal('./PinPad.aspx?transactionType=' + transactionType + "&host=" + secHost + "&secCmpName=" + secCmpName + "&secCmpPhone=" + secCmpPhone + "&secCmpAddress=" + secCmpAddress + "&secRegMsg=" + secRegMsg + "&hostPort=" + secHostPort + "&terminalID=" + secTerminalID + "&amount=" + totalAmt + "&logFile=" + secRegLogFile + "&productDetail=" + getReqGrdDetail() + "&taxDetail=" + getTax(), '', 390, 150);
        } else {
            alert(getLocalizedString(secLang, "msgNoProductselected"));
        }
    }
    if (answer) {
        $.showAkModal('./PinPad.aspx?transactionType=' + transactionType + "&host=" + secHost + "&hostPort=" + secHostPort + "&secCmpName=" + secCmpName + "&secCmpPhone=" + secCmpPhone + "&secCmpAddress=" + secCmpAddress + "&secRegMsg=" + secRegMsg + "&terminalID=" + secTerminalID + "&logFile=" + secRegLogFile, '', 390, 150);
    }
}

function getTransactionInfo(transactionType) {
    var subTotal = $("#ctl00_cphMaster_txtSubTotal").attr("value")
    var total = $("#ctl00_cphMaster_txtTotal").attr("value")
    var sURL = "../POSService.asmx/funGetTransactionID";
    var QueryStr = "strTokan=" + secToken + "&strRegCode=" + secReg + "&strTotalAmt=" + total + "&strUserID=" + secUserId + "&strTotalSubAmt=" + subTotal + "&strTax=" + getTax() + "&strTransType=" + transactionType + "&strReturnAmt=" + $("#ctl00_cphMaster_txtRefund").attr("value") + "&strReceivedAmt=" + $("#ctl00_cphMaster_txtReceived").attr("value") + "&strPreviousTranID=" + secTID;
    var sErrMsg = "Error! TransactionInfo " + sURL + " Qry:" + QueryStr;
    var isAsync = false;
    gAJAXReturnData = null;
    closeAllow = false;
    calliAJAX(sURL, QueryStr, sErrMsg, isAsync);
    if (gAJAXReturnData != null) {
        var sData = load_xml_content_string(gAJAXReturnData);
        if (sData == "POSInvalidTokenNo") {
            alert(getLocalizedString(secLang, sData));
            return;
        }
        var sDataError = sData.split("__error__")
        if (sDataError.length > 1) {
            alert(getLocalizedString(secLang, sDataError[1]))
            location.href = "POS.aspx"
            return "Error";
        }
        var serData = sData.split("__token__");
        setNewToken(secToken, serData[1]);
        return serData[0];
    }
}

function refresh(msg) {
    location.href = "POS.aspx?Msg=" + msg
}

function doTransation(transationInfo, transactionType, pinpadData, acct, cardLang, receiptMag) {
    var tranInfoArr = transationInfo.split("~");
    var total = $("#ctl00_cphMaster_txtTotal").attr("value")
    var sURL = "../POSService.asmx/funInsertTransactionDetail";
    var QueryStr = "strTokan=" + secToken + "&strUserID=" + secUserId + "&strWhshouse=" + secWareHouse + "&strTranID=" + tranInfoArr[0] + "&strTotalAmt=" + total + "&strTransType=" + transactionType + "&strProductDtl=" + getReqGrdQuery() + "&strTranAccHrdID=" + tranInfoArr[1] + "&strPosAccDtlId=" + tranInfoArr[2] + "&strReturnAmt=" + $("#ctl00_cphMaster_txtRefund").attr("value") + "&strReceivedAmt=" + $("#ctl00_cphMaster_txtReceived").attr("value") + "&strCardValue=" + pinpadData + "&strCardLangas=" + cardLang + "&strCardAccountType=" + acct + "&strCardMsg=" + receiptMag + "&strRegCode=" + secReg;
    var sErrMsg = "Error! TransactionInfo " + sURL + " Qry:" + QueryStr;
    var isAsync = false;
    gAJAXReturnData = null;
    calliAJAX(sURL, QueryStr, sErrMsg, isAsync);
    if (gAJAXReturnData != null) {
        var sData = load_xml_content_string(gAJAXReturnData);
        if (sData == "POSInvalidTokenNo") {
            alert(getLocalizedString(secLang, sData));
            return;
        }
        var sDataError = sData.split("__error__")
        if (sDataError.length > 1) {
            alert(getLocalizedString(secLang, sDataError[1]))
            location.href = "POS.aspx"
            return false;
        }
        var tokenData = sData.split("__token__");
        if (tokenData.length > 1) {
            setNewToken(secToken, tokenData[1]);
            return tokenData[0];
        }
        return sData;
    }
}

function cashTransation(isPrint, btnType) {


    if (btnType == 'cancel') {
        if (!confirm(getLocalizedString(secLang, "msgConfirmWouldYouLikeToCanceThisTrans"))) {
            return;
        }
    }



    ShowProgressBar($("body"), '<%=Resources.Resource.lblLoading %>');
    var sPaymentDtl = "";
    if (isLayawayOrder != 1 && btnType != "cancel") {
        var sBal = Number($("#ctl00_cphMaster_txtRefund").attr("value")).toFixed(2)
        if (sBal > 0) {
            alert(getLocalizedString(secLang, "lblCompletePaymentProcess"));
            unblockContentArea($("body"));  //Unblock the screen
            return;
        }
    }

    if (btnType != "cancel") {
        if (document.getElementById("ctl00_cphMaster_grdRequest").getElementsByTagName("tr").length <= 1) {
            alert(getLocalizedString(secLang, "POSNoProductfound"));
            unblockContentArea($("body"));  //Unblock the screen
            return;
        }

        if ($("#ctl00_cphMaster_hdnIsRegClosed").val() == "1") {
            alert(getLocalizedString(secLang, "lblRegisterIsClosed"));
            unblockContentArea($("body"));  //Unblock the screen
            return;
        }

        var ddlSalesRepID = $("#ctl00_ddlSaleRep :selected").val();
        if (ddlSalesRepID == "0") {
            alert(getLocalizedString(secLang, "msgARPlzSelSR"));
            unblockContentArea($("body"));  //Unblock the screen
            return;
        }
    }

    var refundedAmt = Number($("#ctl00_cphMaster_txtRefund").attr("value")).toFixed(2);
    if (refundedAmt < 0 && vAmountRefundedVia == "") {
        ShowRefundVia(isPrint, btnType);
        unblockContentArea($("body"));  //Unblock the screen
        return;
    }


    if (isPartialPayment == "1") {
        AddPaymentType(CurrentPaymentType);
        updatePaymentAmount(CurrentPaymentType);
        var v1 = JSONPartialPaymentDtl;

        sPaymentDtl = "";
        for (var i = 0; i < JSONPartialPaymentDtl.length; i = i + 1) {
            objPaymentDetail = JSONPartialPaymentDtl[i];
            if (objPaymentDetail != null) {
                if (objPaymentDetail.PaymentType != "") {
                    if (sPaymentDtl != "") {
                        sPaymentDtl += "~" + objPaymentDetail.PaymentType + ":" + objPaymentDetail.Amount + ":" + objPaymentDetail.GiftCardNo;
                    }
                    else {
                        sPaymentDtl += objPaymentDetail.PaymentType + ":" + objPaymentDetail.Amount + ":" + objPaymentDetail.GiftCardNo;
                    }

                }
            }
        }
    }
    var transactionType = getRedioControlValue("ctl00$cphMaster$Transaction");

    var strTabNote = ""
    if (btnType == "Tab") {
        transactionType = "9"
        var tempTabNote = prompt(getLocalizedString(secLang, "msgTabValidation"), "");
        if (tempTabNote != null) {
            strTabNote = tempTabNote;
        }
    }
    if (btnType == "cancel") {
        transactionType = "0"
    }
    if (transactionType == "rbDollar" || transactionType == "rbCash") {
        transactionType = "1";
    }
    if (transactionType == "rbVisa") {
        //transactionType = "2";
        transactionType = "14";
    }
    if (transactionType == "rbMasterCard") {
        //transactionType = "3";
        transactionType = "15";
    }
    if (transactionType == "rbAmerican") {
        //transactionType = "4";
        transactionType = "16";
    }
    if (transactionType == "rbInterac") {
        transactionType = "5";
    }
    if (transactionType == "rbExactCash") {
        transactionType = "6";
    }
    if (transactionType == "rbLost") {
        transactionType = "7";
    }
    if (transactionType == "rbGifts") {
        //transactionType = "8";
        transactionType = "12";
        //        if ($("#ctl00_cphMaster_ddlReasonForGift").val() == "") {
        //            alert("Please select reason for gift transaction!");
        //            return false;
        //        }
        //        if ($("#ctl00_cphMaster_ddlAuthorizedBy").val() == "") {
        //            alert("Please select Authorize By for gift transaction!");
        //            return false;
        //        }
    }
    if (transactionType == "rbStaffGifts") {
        transactionType = "9";
        if ($("#ctl00_cphMaster_ddlReasonForGift").val() == "") {
            alert("Please select reason for gift transaction!");
            unblockContentArea($("body"));  //Unblock the screen
            return false;
        }
        if ($("#ctl00_cphMaster_ddlAuthorizedBy").val() == "") {
            alert("Please select Authorize By for gift transaction!");
            unblockContentArea($("body"));  //Unblock the screen
            return false;
        }
    }
    if (transactionType == "rbInStoreCredit") {
        transactionType = "13";
    }

    if (transactionType == "rbGiftCards") {
        transactionType = "11";
    }

    if (transactionType == "rbOGL") {
        transactionType = "17";
    }

    if (isLayawayOrder != 1) {
        if (transactionType == "") {
            alert(getLocalizedString(secLang, "msgPleaseSelectPaymentType"));
            unblockContentArea($("body"));  //Unblock the screen
            return;
        }
    }
    //    }

    var subTotal = $("#ctl00_cphMaster_txtSubTotal").attr("value")
    var total = $("#ctl00_cphMaster_txtTotal").attr("value")
    total = TotalAmountBeforePartial;
    var strTax = getTax();
    var strReturnAmt = $("#ctl00_cphMaster_txtRefund").attr("value");
    var strReceivedAmt = $("#ctl00_cphMaster_txtReceived").attr("value");
    var strProductDtl = getReqGrdQuery();
    var giftReason = $("#ctl00_cphMaster_ddlReasonForGift").val();
    var authorizedBy = $("#ctl00_cphMaster_ddlAuthorizedBy").val();
    var ipAddress = ipAddress;
    var sSaleRepID = $("#ctl00_ddlSaleRep").val();

    $.ajax({
        type: "POST",
        url: "POSServices.asmx/funCashTransaction",
        data: "{lngID:'" + secLang + "',strTokan:'" + secToken + "',strUserID:'" + secUserId + "',strRegCode:'" + secReg + "',strTax:'" + strTax + "',strTotalAmt:'" + total + "',strTotalSubAmt:'" + subTotal + "',strTransType:'" + transactionType + "',strReturnAmt:'" + strReturnAmt + "',strReceivedAmt:'" + strReceivedAmt + "',strWhshouse:'" + secWareHouse + "',strProductDtl:'" + strProductDtl + "',strPreviousTranID:'" + secTID + "',strBtnType:'" + btnType + "',strTabNote:'" + strTabNote + "',giftReason:'" + giftReason + "',authorizedBy:'" + authorizedBy + "',ipAddress:'" + ipAddress + "',customerID:'" + custID + "',custCurrencyCode:'" + custCurrencyCode + "',custType:'" + custType + "',custInvoiceNetTerms:'" + custInvoiceNetTerms + "',salesRepID:'" + sSaleRepID + "',isPartialPayment:'" + isPartialPayment + "', totalAmountBeforePartial:'" + TotalAmountBeforePartial + "',partialPaymentDtl:'" + sPaymentDtl + "',isPrint:'" + isPrint + "',sMailID:'" + vMailToSend + "',isGiftReceipt:'" + vIsGiftReceipt + "',isLayawayOrder:'" + isLayawayOrder + "',laywayOrderID:'" + laywayOrderID + "',sVGiftCardNo:'" + vGiftCardNo + "',sVRefundedVia:'" + vAmountRefundedVia + "' }",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            JsonObject = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (JsonObject.ResponseCode == 1 && JsonObject.Status == "OK") {
                if (isLayawayOrder != 1) {
                    if (isPrint) {
                        var vPrintRcpt = "";
                        var vIsPrintRcpt = getParameterByName('pandingPrintReceipt');
                        if (vIsPrintRcpt == "1") {
                            vPrintRcpt = "&ReturnReceiptHistory=" + 'yes';
                        }

                        if (JsonObject.refundAmount <= 0) {
                            window.open('./Print.aspx?status=1&TID=' + JsonObject.InvoiceID + '&WhsID=' + secWareHouse + '&LangsID=' + secLang + '&isGiftReceipt=' + vIsGiftReceipt + vPrintRcpt + "&customerID=" + custID, 'Printing Receipt', 'width=300,height=300,toolbar=0,menubar=0,location=0,scrollbars=0,resizable=0,directories=0,status=0');
                            if (JsonObject.printMerchantCopy == true) {
                                window.open('./Print.aspx?status=1&TID=' + JsonObject.InvoiceID + '&WhsID=' + secWareHouse + '&LangsID=' + secLang + '&isGiftReceipt=' + vIsGiftReceipt + vPrintRcpt + "&customerID=" + custID, 'Merchant Receipt', 'width=300,height=300,toolbar=0,menubar=0,location=0,scrollbars=0,resizable=0,directories=0,status=0');
                            }
                        }
                        lastInvoiceNo = JsonObject.InvoiceID;
                        isPrintMerchantCopy = JsonObject.printMerchantCopy;
                        ShowMessage(JsonObject.msgSuccess, "");
                        ResetPOS();
                        unblockContentArea($("body"));  //Unblock the screen
                    }
                    else {
                        if (secNoReceipt == "yes" && !(transactionType == '0' || transactionType == '9')) {
                        }
                        ShowMessage(JsonObject.msgSuccess, "");
                        if (btnType != 'cancel') {
                            $.ajax({
                                url: "POSServices.asmx/SendReceiptMail",
                                data: "{lngID:'" + secLang + "',invID:'" + JsonObject.InvoiceID + "',strRegCode:'" + secReg + "',sMailID:'" + vMailToSend + "',sWhrCode:'" + secWareHouse + "',isGiftReceipt:'" + vIsGiftReceipt + "'}",
                                contentType: "application/json; charset=utf-8",
                                type: "POST",
                                dataType: 'json',
                                success: function (response) {
                                }
                            });
                        }
                        ResetPOS();
                        unblockContentArea($("body"));  //Unblock the screen
                    }
                }
                else {
                    ShowMessage(JsonObject.msgSuccess, "");
                    ResetPOS();
                    unblockContentArea($("body"));  //Unblock the screen
                }
            }
            else if (JsonObject.ResponseCode == -1 && JsonObject.Status == "OK") {
                //refresh(JsonObject.CustNotFound);
                ShowMessage(JsonObject.CustNotFound, "");
                ResetPOS();
                unblockContentArea($("body"));  //Unblock the screen
            }
            else {
                alert(getLocalizedString(secLang, JsonObject.catchError))
                //location.href = "POS.aspx"
                ResetPOS();
                unblockContentArea($("body"));  //Unblock the screen
                return;
                //refresh(JsonObject.catchError);
            }
        },
        error: function () {
            //refresh("NOT SUCCESS");
            ShowMessage("NOT SUCCESS", "");
            ResetPOS();
            unblockContentArea($("body"));  //Unblock the screen
        }
    });
}


function ResetPOS() {
    $('#ctl00_cphMaster_grdRequest tr:not(:first)').remove();
    $('#ctl00_cphMaster_trSubTotal').nextAll().remove();
    $('#ctl00_cphMaster_txtUPCCode').attr('value', "");
    $('#ctl00_cphMaster_txtProductName').attr('value', "");
    $('#ctl00_cphMaster_txtQuantity').attr('value', "");
    $('#ctl00_cphMaster_txtPrice').attr('value', "");
    $('#ctl00_cphMaster_txtTotalPrice').attr('value', "");
    $("#ctl00_cphMaster_ddlTax option[value='" + 5 + "']").attr("selected", "selected");
    $('#ctl00_cphMaster_txtTotal').attr('value', Number(0).toFixed(2));
    $('#ctl00_cphMaster_txtTotalAmount').attr('value', Number(0).toFixed(2));
    $('#ctl00_cphMaster_txtReceived').attr('value', Number(0).toFixed(2));
    $('#ctl00_cphMaster_txtSubTotal').attr('value', Number(0).toFixed(2));
    $('#ctl00_cphMaster_txtRefund').attr('value', Number(0).toFixed(2));

    $("input:radio").each(function (i) {
        this.checked = false;
    });

    vIsGiftReceipt = 0;
    vRemaingAmountToBePaid = 0;
    CurrentPaymentType = "";
    isPartialPayment = 0;
    CurrentPaymentType = "";
    TotalAmountBeforePartial = 0;
    JSONPartialPaymentDtl = new Array();
    isLayawayOrder = 0;
    laywayOrderID = 0;
    amountRecvived = 0;
    vGiftCardNo = "";
    vAmountRefundedVia = '';
    visPrint = '';
    vbtnType = '';


    $("#spnPaymentDesc").html("");
    $('.divButtonBlue a').removeClass('blueovalgreenbutton').addClass('blueovalbutton');
    var vDefaultCustIDTobeLoaded = $("#ctl00_cphMaster_hdnIstLoadedCustID").val();
    $('.divButton a').removeClass('ovalgreenbutton').addClass('ovalbutton');
    $("#ctl00_cphMaster_cmdAdd").hide();
    setCurrentGuest(vDefaultCustIDTobeLoaded);
    localArr = new Array();
    localArrOrg = new Array();
    loadBarCodeItems();

    jQuery.FrameDialog.closeDialog();
    $("#ctl00_cphMaster_txtSearch").focus();
}

function ShowMessage(message, mType) {
    var vFontColor = '';
    if (mType == 'err') {
        vFontColor = "red";
    }
    else {
        vFontColor = "green";
    }
    document.getElementById('ctl00_cphMaster_lblTranMsg').innerHTML = message;
    $("#ctl00_cphMaster_lblTranMsg").show();
}

function SendEmail(emailID, subject, message) {
    var custEmailID = emailID;
    //        var subject = vCompanyName + " - " + '<%=Resources.Resource.lblOrderPlaced %>'; //Order Placed"; //"Chuchai - Order Placed";lblOrderPlaced
    //        var message = $("#divEmail").html();
    var dataToPost = {};
    dataToPost.email = emailID;
    dataToPost.subject = subject;
    dataToPost.message = message;
    $.ajax({
        type: "POST",
        url: '../Handlers/MailHandler.ashx',
        data: $.param(dataToPost),
        success: function (response) { },
        error: function (response, status, error) { }
    });
}


function reprintInvoice() {
    if (Number(lastInvoiceNo) > 0) {
        window.open('./Print.aspx?status=1&TID=' + lastInvoiceNo + '&WhsID=' + secWareHouse + '&LangsID=' + secLang, 'Printing Receipt', 'width=300,height=300,toolbar=0,menubar=0,location=0,scrollbars=0,resizable=0,directories=0,status=0');
        if (isPrintMerchantCopy == true) {
            //alert(isPrintMerchantCopy);
            window.open('./Print.aspx?status=1&TID=' + lastInvoiceNo + '&WhsID=' + secWareHouse + '&LangsID=' + secLang , 'Merchant Receipt', 'width=300,height=300,toolbar=0,menubar=0,location=0,scrollbars=0,resizable=0,directories=0,status=0');
        }
    }
}
function getRedioControlValue(rdoName) {
    var elements = document.getElementsByName(rdoName);
    var i = 0;
    while (i < elements.length) {
        if (elements[i].checked) {
            return $(elements[i]).attr("value");
        }
        i++;
    }
    return '';
}

function getTax() {
    var trId = $(document.getElementById("ctl00_cphMaster_tblTotalDesription")).find('tr');
    var i = 1;
    var j = 0;
    var tax = "";
    //while (i < trId.length - 1) {
    while (i < trId.length) {
        var input = trId[i].getElementsByTagName("input")[0];
        var span = trId[i].getElementsByTagName("span")[0];
        tax += $(span).html() + "~" + $(input).attr("value") + "^"
        i++
        j++
    }
    return tax;
}

function getLocalizedString(lang, key) {
    var sURL = "./scriptLocalazation.aspx";
    var QueryStr = "lang=" + lang + "&key=" + key;
    var sErrMsg = "Error! TransactionInfo " + sURL + " Qry:" + QueryStr;
    var isAsync = false;
    gAJAXReturnData = null;
    calliAJAX(sURL, QueryStr, sErrMsg, isAsync);
    if (gAJAXReturnData != null) {
        return gAJAXReturnData
    }
    return "";
}

function replaceAll(ostr, ochs, nchs) {
    var str = ostr;
    if (!(typeof str == "undefined")) {
        var index = str.indexOf(ochs);
        while (index != -1) {
            str = str.replace(ochs, nchs);
            index = str.indexOf(ochs);
        }
    }
    return str;
}

function replaceSpecialCh(val) {
    // alert("replace "+val);
    val = replaceAll(val, ",", "__COMMA__");
    val = replaceAll(val, "$", "__DOLLAR__");
    val = replaceAll(val, "&", "__AND__");
    val = replaceAll(val, "%", "__PERCENT__");
    val = replaceAll(val, "'", "__QUOTE__");
    val = replaceAll(val, '"', "__DQUOTE__");
    val = replaceAll(val, '\\', "__BSLASH__");
    val = replaceAll(val, "\n", "__NEWLINE__N__");
    val = replaceAll(val, "\r", "__NEWLINE__R__");
    return val;
}
/**
The function is to restore special special character in the input string
*/
function restoreSpecialCh(controlValue) {
    // alert("restore : "+ controlValue);
    controlValue = replaceAll(controlValue, "__COMMA__", ",");
    controlValue = replaceAll(controlValue, "__DOLLAR__", "$");
    controlValue = replaceAll(controlValue, "__AND__", "&");
    controlValue = replaceAll(controlValue, "__PERCENT__", "%");
    controlValue = replaceAll(controlValue, "__QUOTE__", "'");
    controlValue = replaceAll(controlValue, "__DQUOTE__", '"');
    controlValue = replaceAll(controlValue, "__BSLASH__", '\\');
    controlValue = replaceAll(controlValue, "__NEWLINE__N__", "\n");
    controlValue = replaceAll(controlValue, "__NEWLINE__R__", "\r");
    return controlValue;
}

function parseXMLForBarCodeData(xmlData) {
    if (xmlData == undefined || xmlData == "")
        return "";
    var i = 0
    var str = ""
    while (i < xmlData.documentElement.childNodes.length) {
        if (typeof xmlData.documentElement.childNodes[i].childNodes[0] != "undefined") {
            //alert(xmlData.documentElement.childNodes[i].childNodes[0].nodeValue)

            if (xmlData.documentElement.childNodes[i].childNodes[0].nodeValue.search("__token__") != -1) {
                str += xmlData.documentElement.childNodes[i].childNodes[0].nodeValue
            } else if (xmlData.documentElement.childNodes[i].childNodes[0].nodeValue == "POSNoProductfound") {
                str = xmlData.documentElement.childNodes[i].childNodes[0].nodeValue;
            } else {
                str += xmlData.documentElement.childNodes[i].childNodes[0].nodeValue + "__item__";
            }
        }
        i++;
    }
    return str;
}

var isItemEdit = 0;
var editItemID = 0;
function editUnitPrice(prId) {
    isItemEdit = 1;
    var prArr = localArr[prId].data.split("~");
    editItemID = prId;

    var trData = localArr[prId].data.split("~");
    var trTax = localArr[prId].tax.split("^");
    var tGroupID = trTax[0].split("~");
    var vTGroupID = Number(tGroupID[4]);
    $("#ctl00_cphMaster_ddlTax option[value='" + vTGroupID + "']").attr("selected", "selected");
    var ddlTaxText = $("#ctl00_cphMaster_ddlTax :selected").text();
    var ddlTaxID = $("#ctl00_cphMaster_ddlTax :selected").val();

    //$("#ctl00_cphMaster_ddlTax option[value='" + prArr[10] + "']").attr("selected", "selected");
    $("#ctl00_cphMaster_ddlTax").trigger("change");
    $("#ctl00_cphMaster_hdnTaxValue").val(localArr[prId].tax); //   .attr("value",)
    //    $("#ctl00_cphMaster_txtPrice").keypad({
    //        showOn: 'load'
    //    });

    //    document.getElementById("ctl00_cphMaster_txtPrice").onblur = function () {
    //        $("#unit_price_" + prId).html(Number(Math.round((Number($("#ctl00_cphMaster_txtPrice").attr("value"))) * 100) / 100).toFixed(2));
    //        $("#quantity_" + prId).html("1");
    //        $("#total_price_" + prId).html(Number(Math.round((Number($("#ctl00_cphMaster_txtPrice").attr("value")) * Number($("#quantity_" + prId).html())) * 100) / 100).toFixed(2));
    //        reCalculetTax(prId);
    //        resetProduct(prId, Math.round((Number($("#ctl00_cphMaster_txtPrice").attr("value"))) * 100) / 100);
    //        calculetTax(prId);
    //        calculateSubTotal();
    //        calculateTotal();
    //    }
    //    setTimeout(function () { $("#ctl00_cphMaster_txtPrice").attr("value", 0); }, 10);

    //    var vPrdActPrice = Number($('#unit_price_' + prId).html()).toFixed(2);
    //    $("#ctl00_cphMaster_txtPrice").attr("value", vPrdActPrice);
    var vPrdActPrice = Number($('#p_disc' + prId).html());
    $("#ctl00_cphMaster_txtDisc").attr("value", vPrdActPrice);
    $("#ctl00_cphMaster_cmdAdd").show();
}

function resetProduct(prId, newUnitprice) {
    var ddlTaxText = $("#ctl00_cphMaster_ddlTax :selected").text();
    var ddlTaxID = $("#ctl00_cphMaster_ddlTax :selected").val();
    var vPrdActPrice = $('#ctl00_cphMaster_txtPrice').attr('value');
    var vPrdDisc = $('#ctl00_cphMaster_txtDisc').attr('value');
    var totalPrice = $('#ctl00_cphMaster_txtTotalPrice').attr('value')
    var vTaxValue = $('#ctl00_cphMaster_hdnTaxValue').attr('value')
    var prArr = localArr[prId].data.split("~");
    var newPreData = prArr[0] + "~" + prArr[1] + "~" + prArr[2] + "~" + prArr[3] + "~" + prArr[4] + "~" + prArr[5] + "~" + newUnitprice + "~" + prArr[7] + "~" + prArr[8] + "~" + vPrdDisc + "~" + ddlTaxID + "~" + ddlTaxText + "~" + totalPrice + "~" + vTaxValue;
    localArr[prId].data = newPreData;
}

function openCOMApplet() {
    var massage = $("#ctl00_cphMaster_txtTotal").attr("value");
    var appletTag = "<applet id=idApplet code='USBtoSerial.class' archive='RXTXApplet.jar' width=1 height=1>" +
    "<param name='port' value='" + COMPort + "'/>" +
    "<param name='msg' value='" + massage + "'/>" +
    "</applet>";

    var div = document.createElement("div");
    div.id = "comDiv"
    div.innerHTML = appletTag;
    document.body.appendChild(div);
}

function closeCOMApplet() {
    var comDiv = document.getElementById("comDiv");
    document.body.removeChild(comDiv);
}


var isPartialPayment = 0;
var CurrentPaymentType = "";
var TotalAmountBeforePartial = 0;
var JSONPartialPaymentDtl = new Array();
var isLayawayOrder = 0;
var laywayOrderID = 0;
var amountRecvived = 0;
var vGiftCardNo = "";

function AddPaymentType(paymentType) {
    if (vGiftCardNo != "") {
        if (CurrentPaymentType != "") {
            if (paymentTypeGiftCardExits(CurrentPaymentType, vGiftCardNo) == true) {
                updatePaymentGiftCardAmount(CurrentPaymentType, vGiftCardNo);
                return;
            }
        }
        CurrentPaymentType = paymentType;
        if (paymentType != "") {
            if (paymentTypeGiftCardExits(CurrentPaymentType, vGiftCardNo) == false) {
                var obj = new Object();
                obj.PaymentType = paymentType;
                obj.Amount = 0;
                obj.GiftCardNo = vGiftCardNo;
                JSONPartialPaymentDtl.push(obj);
            }
            updatePaymentGiftCardAmount(CurrentPaymentType, vGiftCardNo);
        }
        vGiftCardNo = "";
        return;
    }
    if (CurrentPaymentType != "") {
        if (paymentTypeExits(CurrentPaymentType) == true) {
            updatePaymentAmount(CurrentPaymentType);
            return;
        }
    }
    CurrentPaymentType = paymentType;
    if (paymentType != "") {
        if (paymentTypeExits(CurrentPaymentType) == false) {
            var obj = new Object();
            obj.PaymentType = paymentType;
            obj.Amount = 0;
            obj.GiftCardNo = "";
            JSONPartialPaymentDtl.push(obj);
        }
        updatePaymentAmount(CurrentPaymentType);
    }
}



function updatePartialPaymentAmount(paymentType, amount) {
    if (paymentType == "")
        return;
    for (var i = 0; i < JSONPartialPaymentDtl.length; i = i + 1) {
        objPaymentDetail = JSONPartialPaymentDtl[i];
        if (objPaymentDetail != null) {
            if (objPaymentDetail.PaymentType == paymentType) {
                objPaymentDetail.Amount = Number(objPaymentDetail.Amount) + Number(amount); // document.getElementById('<%=txtReceived.ClientID%>').value;
            }
        }
    }
}


var vRemaingAmountToBePaid = 0;
function updatePaymentAmount(paymentType) {
    if (paymentType == "")
        return;
    for (var i = 0; i < JSONPartialPaymentDtl.length; i = i + 1) {
        objPaymentDetail = JSONPartialPaymentDtl[i];
        if (objPaymentDetail != null) {
            if (objPaymentDetail.PaymentType == paymentType) {
                objPaymentDetail.Amount = Number(objPaymentDetail.Amount) + Number($("#ctl00_cphMaster_txtReceived").attr("value")); // document.getElementById('<%=txtReceived.ClientID%>').value;
            }
        }
    }

    var myFormattedNum = "0.00";
    if (Number($("#ctl00_cphMaster_txtReceived").attr("value")) > 0) {
        myFormattedNum = Number(vRemaingAmountToBePaid) - (Number($("#ctl00_cphMaster_txtReceived").attr("value")))
    }
    else {
        Number(vRemaingAmountToBePaid) - (Number($("#ctl00_cphMaster_txtReceived").attr("value")) + amountRecvived)
    }

    $("#ctl00_cphMaster_txtRefund").val(Number(Math.round(myFormattedNum * 100) / 100).toFixed(2));
    if ($("#ctl00_cphMaster_txtReceived").attr("value") == '') {
        $("#ctl00_cphMaster_txtRefund").val("0.00");
    }
    if ((myFormattedNum) != 0) {
        vRemaingAmountToBePaid = Number(Math.round(myFormattedNum * 100) / 100).toFixed(2);
        $("#ctl00_cphMaster_txtReceived").attr("value", "0.00");
    }

    var vPaymentDesc = "";
    var tPartialAmount = 0;
    for (var i = 0; i < JSONPartialPaymentDtl.length; i = i + 1) {
        objPaymentDetail = JSONPartialPaymentDtl[i];
        if (objPaymentDetail != null) {
            if (Number(objPaymentDetail.Amount) > 0) {
                if (vPaymentDesc == "") {
                    vPaymentDesc += objPaymentDetail.PaymentType + " : " + Number(objPaymentDetail.Amount).toFixed(2);
                    tPartialAmount = Number(objPaymentDetail.Amount);
                }
                else {
                    vPaymentDesc += ", " + objPaymentDetail.PaymentType + " : " + Number(objPaymentDetail.Amount).toFixed(2)
                    tPartialAmount += Number(objPaymentDetail.Amount);
                }
            }
        }
    }

    amountRecvived = tPartialAmount;
    $("#spnPaymentDesc").html(vPaymentDesc);

    CurrentPaymentType = "";
    return false;

}

function paymentTypeExits(paymentType) {
    for (var i = 0; i < JSONPartialPaymentDtl.length; i = i + 1) {
        objPaymentDetail = JSONPartialPaymentDtl[i];
        if (objPaymentDetail != null) {
            if (objPaymentDetail.PaymentType == paymentType) {
                return true;
            }
        }
    }
    return false;
}

function paymentTypeValue(paymentType) {
    for (var i = 0; i < JSONPartialPaymentDtl.length; i = i + 1) {
        objPaymentDetail = JSONPartialPaymentDtl[i];
        if (objPaymentDetail != null) {
            if (objPaymentDetail.PaymentType == paymentType) {
                return Number(objPaymentDetail.Amount).toFixed(2);
            }
            else {
                return 0;
            }
        }
        else {
            return 0;
        }
    }
}

function paymentTypeGiftCardExits(paymentType, GiftCardNo) {
    for (var i = 0; i < JSONPartialPaymentDtl.length; i = i + 1) {
        objPaymentDetail = JSONPartialPaymentDtl[i];
        if (objPaymentDetail != null) {
            if ((objPaymentDetail.PaymentType == paymentType) && (objPaymentDetail.GiftCardNo == GiftCardNo)) {
                return true;
            }
        }
    }
    return false;
}

function updatePaymentGiftCardAmount(paymentType, GiftCardNo) {
    if (paymentType == "")
        return;
    for (var i = 0; i < JSONPartialPaymentDtl.length; i = i + 1) {
        objPaymentDetail = JSONPartialPaymentDtl[i];
        if (objPaymentDetail != null) {
            if ((objPaymentDetail.PaymentType == paymentType) && (objPaymentDetail.GiftCardNo == GiftCardNo)) {
                objPaymentDetail.Amount = Number(objPaymentDetail.Amount) + Number($("#ctl00_cphMaster_txtReceived").attr("value"));
            }
        }
    }

    var myFormattedNum = "0.00";
    if (Number($("#ctl00_cphMaster_txtReceived").attr("value")) > 0) {
        myFormattedNum = Number(vRemaingAmountToBePaid) - (Number($("#ctl00_cphMaster_txtReceived").attr("value")))
    }
    else {
        Number(vRemaingAmountToBePaid) - (Number($("#ctl00_cphMaster_txtReceived").attr("value")) + amountRecvived)
    }

    $("#ctl00_cphMaster_txtRefund").val(Number(Math.round(myFormattedNum * 100) / 100).toFixed(2));
    if ($("#ctl00_cphMaster_txtReceived").attr("value") == '') {
        $("#ctl00_cphMaster_txtRefund").val("0.00");
    }
    if ((myFormattedNum) != 0) {
        vRemaingAmountToBePaid = Number(Math.round(myFormattedNum * 100) / 100).toFixed(2);
        $("#ctl00_cphMaster_txtReceived").attr("value", "0.00");
    }

    var vPaymentDesc = "";
    var tPartialAmount = 0;
    for (var i = 0; i < JSONPartialPaymentDtl.length; i = i + 1) {
        objPaymentDetail = JSONPartialPaymentDtl[i];
        if (objPaymentDetail != null) {
            if (Number(objPaymentDetail.Amount) > 0) {
                if (vPaymentDesc == "") {
                    vPaymentDesc += objPaymentDetail.PaymentType + " : " + Number(objPaymentDetail.Amount).toFixed(2);
                    tPartialAmount = Number(objPaymentDetail.Amount);
                }
                else {
                    vPaymentDesc += ", " + objPaymentDetail.PaymentType + " : " + Number(objPaymentDetail.Amount).toFixed(2)
                    tPartialAmount += Number(objPaymentDetail.Amount);
                }
            }
        }
    }

    amountRecvived = tPartialAmount;
    $("#spnPaymentDesc").html(vPaymentDesc);

    CurrentPaymentType = "";
    return false;

}


function SetListSelectedValue(elementID, valueSelect) {
    var selectedArray = new Array();
    var selObj = document.getElementById(elementID);
    var i;
    var count = 0;
    for (i = 0; i < selObj.options.length; i++) {
        var sText = selObj.options[i].text;
        if (selObj.options[i].value == valueSelect) {
            selObj.options[i].selected = true;
        }
    }
}


function ApplyInvoiceDiscount(discountValue) {
    $('#ctl00_cphMaster_trSubTotal').nextAll().remove();
    var trId = document.getElementById('ctl00_cphMaster_grdRequest').getElementsByTagName('tr');
    var i = 1;
    var subTotol = 0;
    while (i < trId.length) {
        var vPrdActPrice = Number($('#unit_price_' + $(trId[i]).attr('id')).html());
        var vPrdDisc = Number($('#p_disc_' + $(trId[i]).attr('id')).html());

        if ((vPrdDisc + discountValue) > 100) {
            vPrdDisc = 100;
        }
        else { vPrdDisc += discountValue; }
        $('#p_disc_' + $(trId[i]).attr('id')).html(vPrdDisc);
        var vTaxValue = Number($('#hdn_PTaxValue_' + $(trId[i]).attr('id')).val());
        var quntity = Number($('#quantity_' + $(trId[i]).attr('id')).html());
        var vPrdAmt = Number((Number(quntity)) * Number(vPrdActPrice));
        var dblPrdDisc = Number(vPrdAmt * vPrdDisc / 100);
        var dblPrdAftDisc = Number(vPrdAmt - dblPrdDisc);
        var dblTaxAmount = Number(dblPrdAftDisc * vTaxValue / 100)
        var dblPrdNetCost = dblPrdAftDisc; //Math.round(Number(dblPrdAftDisc + dblTaxAmount) * 100) / 100;
        $('#total_price_' + $(trId[i]).attr('id')).html(dblPrdNetCost.toFixed(2));

        var pID = Number($(trId[i]).attr('id'));
        var prArr = localArr[pID].data.split("~");
        var newPreData = prArr[0] + "~" + prArr[1] + "~" + prArr[2] + "~" + prArr[3] + "~" + prArr[4] + "~" + prArr[5] + "~" + vPrdActPrice + "~" + prArr[7] + "~" + prArr[8] + "~" + vPrdDisc + "~" + prArr[10] + "~" + prArr[11] + "~" + dblPrdNetCost + "~" + prArr[13];
        localArr[pID].data = newPreData;
        subTotol += dblPrdNetCost;
        var vAddAftrRow = "";
        var taxs = getAllCalculatedTax(pID);
        var j = 0
        var trTax = ""
        while (j < taxs.length) {
            if ((typeof taxs[j].split("~")[0] != 'undefined') && taxs[j].split("~")[0] != "") {
                var taxName = taxs[j].split("~")[0];
                var taxPerUnit = taxs[j].split("~")[1];
                var preTax = getDuplicateTax(taxName)
                if (preTax == "") {
                    trTax = "<tr id='ctl00_cphMaster_trTax_" + taxName.replace(" ", "") + "' name='taxTr'>"
                    trTax += "<td align='right'><span id='ctl00_cphMaster_lblTax_" + taxName.replace(" ", "") + "' class='lblBold' name='taxLable'>" + taxName + "</span></td>"
                    trTax += "<td align='right'><input name='ctl00$cphMaster$txtTax_" + taxName.replace(" ", "") + "' value='" + Number(calculetSameTax('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", ""), taxPerUnit, pID)).toFixed(2) + "' class='frminpt fom-inputlarge' readonly='readonly' id='ctl00_cphMaster_txtTax_" + taxName.replace(" ", "") + "' style='width: 150px; text-align: right;' type='text'></td></tr>"
                    if (vAddAftrRow == "") {
                        $('#ctl00_cphMaster_trSubTotal').after(trTax);
                        vAddAftrRow = "#ctl00_cphMaster_trTax_" + taxName.replace(" ", "")
                    }
                    else {
                        $(vAddAftrRow).after(trTax);
                    }
                } else {

                    var vLastVal = $('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", "")).attr("value");
                    $('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", "")).attr("value", Number(calculetSameTax('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", ""), taxPerUnit, pID)).toFixed(2));
                }
            }
            j++;
        }

        i++;
    }
    $('#ctl00_cphMaster_txtSubTotal').attr('value', Number(Math.round(subTotol * 100) / 100).toFixed(2));
    calculateSubTotal();
    calculateTotal();
}




function ApplyNewCustomerDiscount(discountValue) {
    if (Number(discountValue) <= 0) {
        return;
    }
    $('#ctl00_cphMaster_trSubTotal').nextAll().remove();
    var trId = document.getElementById('ctl00_cphMaster_grdRequest').getElementsByTagName('tr');
    var i = 1;
    var subTotol = 0;
    while (i < trId.length) {
        var vPrdActPrice = Number($('#unit_price_' + $(trId[i]).attr('id')).html());
        var vPrdDisc; // = Number($('#p_disc_' + $(trId[i]).attr('id')).html());
        vPrdDisc = discountValue;
        $('#p_disc_' + $(trId[i]).attr('id')).html(vPrdDisc);
        var vTaxValue = Number($('#hdn_PTaxValue_' + $(trId[i]).attr('id')).val());
        var quntity = Number($('#quantity_' + $(trId[i]).attr('id')).html());
        var vPrdAmt = Number((Number(quntity)) * Number(vPrdActPrice));
        var dblPrdDisc = Number(vPrdAmt * vPrdDisc / 100);
        var dblPrdAftDisc = Number(vPrdAmt - dblPrdDisc);
        var dblTaxAmount = Number(dblPrdAftDisc * vTaxValue / 100)
        var dblPrdNetCost = dblPrdAftDisc;
        $('#total_price_' + $(trId[i]).attr('id')).html(dblPrdNetCost.toFixed(2));
        var pID = Number($(trId[i]).attr('id'));
        var prArr = localArr[pID].data.split("~");
        var newPreData = prArr[0] + "~" + prArr[1] + "~" + prArr[2] + "~" + prArr[3] + "~" + prArr[4] + "~" + prArr[5] + "~" + vPrdActPrice + "~" + prArr[7] + "~" + prArr[8] + "~" + vPrdDisc + "~" + prArr[10] + "~" + prArr[11] + "~" + dblPrdNetCost + "~" + prArr[13];
        localArr[pID].data = newPreData;
        subTotol += dblPrdNetCost;
        var vAddAftrRow = "";
        var taxs = getAllCalculatedTax(pID);
        var j = 0
        var trTax = ""
        while (j < taxs.length) {
            if ((typeof taxs[j].split("~")[0] != 'undefined') && taxs[j].split("~")[0] != "") {
                var taxName = taxs[j].split("~")[0];
                var taxPerUnit = taxs[j].split("~")[1];
                var preTax = getDuplicateTax(taxName)
                if (preTax == "") {
                    trTax = "<tr id='ctl00_cphMaster_trTax_" + taxName.replace(" ", "") + "' name='taxTr'>"
                    trTax += "<td align='right'><span id='ctl00_cphMaster_lblTax_" + taxName.replace(" ", "") + "' class='lblBold' name='taxLable'>" + taxName + "</span></td>"
                    trTax += "<td align='right'><input name='ctl00$cphMaster$txtTax_" + taxName.replace(" ", "") + "' value='" + Number(calculetSameTax('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", ""), taxPerUnit, pID)).toFixed(2) + "' class='frminpt fom-inputlarge' readonly='readonly' id='ctl00_cphMaster_txtTax_" + taxName.replace(" ", "") + "' style='width: 140px; text-align: right;' type='text'></td></tr>"
                    if (vAddAftrRow == "") {
                        $('#ctl00_cphMaster_trSubTotal').after(trTax);
                        vAddAftrRow = "#ctl00_cphMaster_trTax_" + taxName.replace(" ", "")
                    }
                    else {
                        $(vAddAftrRow).after(trTax);
                    }
                } else {
                    var vLastVal = $('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", "")).attr("value");
                    $('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", "")).attr("value", Number(calculetSameTax('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", ""), taxPerUnit, pID)).toFixed(2));
                }
            }
            j++;
        }
        i++;
    }
    $('#ctl00_cphMaster_txtSubTotal').attr('value', Number(Math.round(subTotol * 100) / 100).toFixed(2));
    calculateSubTotal();
    calculateTotal();
}



function GetProdCalculetedTax(prId) {
    var taxs = getAllCalculatedTax(prId);
    var i = 0
    var trTax = ""
    var tTaxAmt = 0;
    while (i < taxs.length) {
        if ((typeof taxs[i].split("~")[0] != 'undefined') && taxs[i].split("~")[0] != "") {
            var taxName = taxs[i].split("~")[0];
            var taxPerUnit = taxs[i].split("~")[1];
            var preTax = getDuplicateTax(taxName)
            if (preTax == "") {
                //                trTax += "<tr id='ctl00_cphMaster_trTax_" + taxName.replace(" ", "") + "' name='taxTr'>"
                //                trTax += "<td align='right'><span id='ctl00_cphMaster_lblTax_" + taxName.replace(" ", "") + "' class='lblBold' name='taxLable'>" + taxName + "</span></td>"
                //                trTax += "<td align='right'><input name='ctl00$cphMaster$txtTax_" + taxName.replace(" ", "") + "' value='" + Number(calculetSameTax('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", ""), taxPerUnit)).toFixed(2) + "' class='frminpt fom-inputlarge' readonly='readonly' id='ctl00_cphMaster_txtTax_" + taxName.replace(" ", "") + "' style='width: 150px; text-align: right;' type='text'></td></tr>"
            } else {
                //                $('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", "")).attr("value", Number(calculetSameTax('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", ""), taxPerUnit)).toFixed(2));
                //$('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", "")).attr("value", );



                //                return Math.round((Number(taxs[i].split("~")[1]) / 100) * 100) / 100 * Math.round(Number(pDisAmt) * 100) / 100 
                //                tTaxAmt += Number(calculetSameTax('#ctl00_cphMaster_txtTax_' + taxName.replace(" ", ""), taxPerUnit));

                //                                tTaxAmt += Math.round((Number(taxs[i].split("~")[1]) / 100) * 100);
                tTaxAmt += (Number(taxs[i].split("~")[1]));
            }
        }
        i++;
    }
    var pDis = Math.round((Number($("#ctl00_cphMaster_txtDisc").attr('value')) / 100) * 100);
    //var pCost = Math.round((Number($("#ctl00_cphMaster_txtPrice").attr('value')) / 100) * 100);
    var pCost = Number((Number($("#ctl00_cphMaster_txtPrice").attr('value')) / 100) * 100).toFixed(2);

    var pDisAmt = (pCost - ((pCost * pDis) / 100))
    tTaxAmt = pDisAmt; // + tTaxAmt
    return tTaxAmt;
}


function GiftReceipt(divID) {
    if ($('#' + divID + ' a').hasClass("blueovalgreenbutton")) {
        $('#' + divID + ' a').addClass('blueovalbutton').removeClass('blueovalgreenbutton');
        vIsGiftReceipt = "0";
    }
    else {
        $('#' + divID + ' a').removeClass('blueovalbutton').addClass('blueovalgreenbutton');
        vIsGiftReceipt = "1";
    }
}

function ShowProgressBar(elementToBlock, loadingMessage) {
    $(elementToBlock).block({
        centerY: 0,
        css: { border: '3px solid #a00;', top: '0px', left: '', right: '10px', width: '70px', height: '70px', padding: '10px', background: 'none repeat scroll 0 0 rgba(0, 0, 0, 0)' },
        message: "<div ><image src='images/Loading.gif' style='width:70px;' ></image></div>"
    });
}
function unblockContentArea(elementToBlock) {
    $(elementToBlock).unblock();
}


function LoadLaywayOrderDetial(orderID) {
    laywayOrderID = orderID;
    if (laywayOrderID > 0) {
        var gid = {};
        gid.orderid = orderID;
        $.ajax({
            type: "POST",
            url: "ajax.aspx/GetLaywayOrderDetail",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(gid),
            success: function (data) {
                if (data.d[0] != "0") {
                    var layOrdCustID = data.d[1];
                    setCurrentGuest(layOrdCustID);
                    $('#ctl00_cphMaster_grdRequest tr:not(:first)').remove();
                    $("#ctl00_cphMaster_grdRequest_header").after(data.d[2]);
                    var trIds = document.getElementById("ctl00_cphMaster_grdRequest").getElementsByTagName("tr");
                    $(trIds).each(function () {
                        var prId = $(this).attr("id");
                        if (prId != "" && prId != "ctl00_cphMaster_grdRequest_header") {
                            calculetTax(prId);
                        }
                    });

                    calculateSubTotal();
                    calculateTotal();

                    if (data.d[3] != "") {
                        var vlstPaymentModes = data.d[3].split(",");

                        if (vlstPaymentModes.length > 1) {
                            isPartialPayment = 1;
                        }

                        var i = 0;
                        var tPartialAmount = 0;
                        $(vlstPaymentModes).each(function () {
                            var vpMode = vlstPaymentModes[i];
                            var vPaymentModes = vpMode.split(":");
                            var vPaymentType = $.trim(vPaymentModes[0]);
                            var vAmount = Number(vPaymentModes[1]);
                            if (vPaymentType != "" && vAmount > 0) {
                                CurrentPaymentType = vPaymentType;
                                AddPaymentType(vPaymentType);
                                updatePartialPaymentAmount(vPaymentType, vAmount);
                                tPartialAmount = tPartialAmount + vAmount;
                            }
                            i += 1;
                        });

                        var myFormattedNum = Number(vRemaingAmountToBePaid) - Number(tPartialAmount);
                        vRemaingAmountToBePaid = Number(Math.round(myFormattedNum * 100) / 100).toFixed(2);
                        $("#ctl00_cphMaster_txtRefund").val(Number(Math.round(myFormattedNum * 100) / 100).toFixed(2));

                        amountRecvived = tPartialAmount;
                    }

                    $("#spnPaymentDesc").html(data.d[3]);

                    if (data.d[4] != "") {
                        $("#ctl00_ddlSaleRep option[value='" + data.d[4] + "']").attr("selected", "selected");
                    }
                }
                else {
                    //alert('Layway Order Not Valid');
                    alert(getLocalizedString(secLang, "MsgNotValidLaywayOrder"));
                }
            },
            error: function (request, status, errorThrown) {
                alert(status);
            }
        });
    }
}


function IsProductItmeSelected() {
    var isSelectedItem = false;
    var trIds = document.getElementById("ctl00_cphMaster_grdRequest").getElementsByTagName("tr");
    $(trIds).each(function () {
        var prId = $(this).attr("id");
        if (prId != "" && prId != "ctl00_cphMaster_grdRequest_header") {
            isSelectedItem = true;
            return isSelectedItem;
        }
    });

    return isSelectedItem;
}


function GetSession() {
    var gid = {};
    gid.custID = 0;
    gid.sHtml = "";
    async: false;
    $.ajax({
        type: "POST",
        async: false,
        url: "ajax.aspx/GetSessionData",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(gid),
        success: function (data) {
            if (data.d[0] != "0") {
                setCurrentGuest(Number(data.d[1]));
                $('#ctl00_cphMaster_grdRequest tr:not(:first)').remove();
                $("#ctl00_cphMaster_grdRequest_header").after(data.d[2]);
                var trIds = document.getElementById("ctl00_cphMaster_grdRequest").getElementsByTagName("tr");

                //                                localArr = $.parseJSON(data.d[3]);
                //                                localArr = cleanArray(localArr);
                //                                localArrOrg = $.parseJSON(data.d[3]);

                //                $(trIds).each(function () {
                //                    var prId = $(this).attr("id");
                //                    if (prId != "" && prId != "ctl00_cphMaster_grdRequest_header") {
                //                        if (typeof localArr[prId] == 'undefined') {
                //                            //                            alert("Product Not Found " + prId);
                //                            var vBarCodeHtml = $("#prUPC_" + prId).html();
                //                            var vBCode = vBarCodeHtml.split('<');
                //                            //                            alert(vBCode[0]);
                //                            addItemByBarCodeService(vBCode[0]);
                //                        }
                //                                                else {
                //                                                    calculetTax(prId);
                //                                                }
                //                    }
                //                });


                $(trIds).each(function () {
                    var prId = $(this).attr("id");
                    if (prId != "" && prId != "ctl00_cphMaster_grdRequest_header") {
                        if (typeof localArr[prId] != 'undefined') {
                            calculetTax(prId);
                        }
                    }
                });
                calculateSubTotal();
                calculateTotal();
                DeleteSession();
            }
            else {
                //alert('Layway Order Not Valid');
            }
        },
        error: function (request, status, errorThrown) {
            alert(status);
        }
    });
}


function SaveSession(customerID) {
    var trIds = document.getElementById('ctl00_cphMaster_grdRequest').getElementsByTagName('tr');
    var lstProdID = '';
    $(trIds).each(function () {
        var prId = $(this).attr("id");
        if (prId != "" && prId != "ctl00_cphMaster_grdRequest_header") {
            if (typeof localArr[prId] != 'undefined') {
                //calculetTax(prId);
                if (lstProdID == '') {
                    lstProdID = prId;
                }
                else {
                    lstProdID += "," + prId;
                }
            }
        }
    });

    var strProductDtl = getReqGrdQuery();
    //    var strShortProductDtl = getShortGrdQuery
    var gid = {};
    gid.custID = customerID;
    gid.sHtml = strProductDtl; //  encodeURIComponent( trId);
    gid.ids = JSON.stringify(localArr);
    gid.lstProdID = lstProdID;
    $.ajax({
        type: "POST",
        async: false,
        url: "ajax.aspx/SaveSessionData",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(gid),
        //        data: "{custID:'" + customerID + "', sHtml:'" + strProductDtl + "', ids:'" + JSON.stringify(localArr) + "'}",
        success: function (data) {
            //            alert("Session Saved");
            if (data.d[0] != "0") {
                //                alert("Session Saved");
            }
            else {
                //alert('Layway Order Not Valid');
            }
        },
        error: function (request, status, errorThrown) {
            alert(status);
        }
    });
}


function DeleteSession() {
    var gid = {};
    gid.custID = 0;
    gid.sHtml = "";
    $.ajax({
        type: "POST",
        async: false,
        url: "ajax.aspx/DeleteSessionData",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(gid),
        success: function (data) {
            if (data.d[0] != "0") {

            }
            else {
                //alert('Layway Order Not Valid');
            }
        },
        error: function (request, status, errorThrown) {
            //            alert(status);
        }
    });
}


//function getShortGrdQuery() {
//    var trIds = document.getElementById("ctl00_cphMaster_grdRequest").getElementsByTagName("tr");
//    var prQuery = ""
//    $(trIds).each(function () {
//        var prId = $(this).attr("id");
//        if (prId != "" && prId != "ctl00_cphMaster_grdRequest_header") {
//            if (prQuery == "") {
//                prQuery = prId + "~" + $("#quantity_" + prId).html() + "~" + $("#unit_price_" + prId).html() + "~" + $("#p_disc_" + prId).html() + "~" + $("#hdn_PTaxID_" + prId).val();
////                prQuery += prId + "~" + $("#quantity_" + prId).html() + "~" + $("#prName_" + prId).html() + "~" + $("#unit_price_" + prId).html() + "~" + $("#total_price_" + prId).html() + "~" + $("#p_disc_" + prId).html() + "~" + $("#hdn_PTaxID_" + prId).val() + "~" + $("#hdn_GiftCardNo_" + prId).val() + "__taxGrp__" + getTaxRegQuery(prId);
//            }
//            else {
//                prQuery += "__item__" + prId + "~" + $("#quantity_" + prId).html() + "~" + $("#unit_price_" + prId).html() + "~" + $("#p_disc_" + prId).html() + "~" + $("#hdn_PTaxID_" + prId).val();
////                prQuery += "__item__" + prId + "~" + $("#quantity_" + prId).html() + "~" + $("#prName_" + prId).html() + "~" + $("#unit_price_" + prId).html() + "~" + $("#total_price_" + prId).html() + "~" + $("#p_disc_" + prId).html() + "~" + $("#hdn_PTaxID_" + prId).val() + "~" + $("#hdn_GiftCardNo_" + prId).val() + "__taxGrp__" + getTaxRegQuery(prId);
//            }

//        }
//    });
//    return prQuery;
//}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;
using iTECH.WebService.Utility;


public partial class POS_GiftCardVarifaction : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                MessageState.SetGlobalMessage(MessageType.Success, "");
                txtGiftCardNo.Focus();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        try
        {
            if ((BusinessUtility.GetDouble(hdnAvailableAmount.Value) > 0) && (BusinessUtility.GetDouble(txtDiscount.Text) == 0))
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgEnterGiftCardAmount);
                txtDiscount.Focus();
                return;
            }

            if ((BusinessUtility.GetDouble(hdnAvailableAmount.Value) == 0) && (BusinessUtility.GetDouble(txtDiscount.Text) > 0))
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgEnterValidAmount);
                txtDiscount.Focus();
                return;
            }

            if (BusinessUtility.GetDouble(txtDiscount.Text) > BusinessUtility.GetDouble(hdnAvailableAmount.Value))
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgEnterValidAmount);
                txtDiscount.Focus();
                return;
            }





            Globals.RegisterCloseDialogScript(Page, string.Format("parent.ApplyGiftCard('{0}','{1}');", BusinessUtility.GetString(txtDiscount.Text), txtGiftCardNo.Text), true);
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string ValidateGiftCard(string CardNo)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            GiftCard objGiftCard = new GiftCard();
            var lstGiftCard = objGiftCard.GetValidateGiftCardNo(null, CardNo);
            if (lstGiftCard != null)
            {
                if (lstGiftCard.Count > 0)
                {
                    dictResult.Add("vBalAmount", CurrencyFormat.GetAmountInUSCulture(objGiftCard.AvailableGiftCardValue(null, CardNo)));
                    dictResult.Add("vStatus", "OK");
                }
                else
                {
                    dictResult.Add("vBalAmount", "0.00");
                    dictResult.Add("vStatus", "NO");
                }
            }
            else
            {
                dictResult.Add("vBalAmount", "0.00");
                dictResult.Add("vStatus", "NO");
            }
        }
        catch (Exception e)
        {
            dictResult.Add("vBalAmount", "0.00");
            dictResult.Add("vStatus", "NO");
        }

        return dictResult.ResultInInJSONFormat;
    }
}
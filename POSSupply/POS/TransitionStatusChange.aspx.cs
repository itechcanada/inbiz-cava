﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Web;

public partial class POS_TransitionStatusChange : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (CurrentCommand.ToLower() == "transitionstatus" && !string.IsNullOrEmpty(KeysToDelete) && !string.IsNullOrEmpty(StatusTypeID))
        {
            clsPOSTransaction postTransition = new clsPOSTransaction();
            postTransition.funUpdateTransStausWithTime(KeysToDelete, StatusTypeID);
            if (!string.IsNullOrEmpty(this.JsCallBackFunction))            
                Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
        }

        Globals.RegisterReloadParentScript(this);
        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
           
    }
    public string CurrentCommand
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["cmd"]))
            {
                return Request.QueryString["cmd"].ToLower();
            }
            return "";
        }
    }
    private int GetKey(int ofIndex)
    {
        string cleneStr = StringUtil.GetCleneJoinedString(",", this.KeysToDelete);
        int[] keys = StringUtil.GetArrayFromJoindString(",", cleneStr);
        try
        {
            return keys[ofIndex];
        }
        catch
        {
            return 0;
        }
    }
    private string GetStringKey(int ofIndex)
    {
        string cleneStr = StringUtil.GetCleneJoinedString(",", this.KeysToDelete);
        string[] keys = cleneStr.Split(',');
        try
        {
            return keys[ofIndex];
        }
        catch
        {
            return string.Empty;
        }
    }
    public string KeysToDelete
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["keys"]))
            {
                return Request.QueryString["keys"];
            }
            return "";
        }
    }
    public string StatusTypeID
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["status"]))
            {
                return Request.QueryString["status"];
            }
            return "";
        }
    }
    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }
}
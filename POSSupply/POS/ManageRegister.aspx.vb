Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Data.Odbc
Imports System.Data
Imports System.web.UI
Imports System.IO
Imports iTECH.InbizERP.BusinessLogic
Imports iTECH.Library.Utilities

Partial Class POS_ManageRegister
    Inherits BasePage
    Dim objRegTra As New clsPosRegisterTrans
    Dim objCom As New clsCommon
    Protected Sub POS_ManageRegister_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Session("RegCode") = "" Then
            Response.Redirect("~/Admin/selectRegister.aspx")
        End If
        lblMsg.Text = ""
        lblMsg.ForeColor = Drawing.Color.Green
        divSetAmount.Style("display") = "none"

        DropDownHelper.FillPosRegister(ddlRegister, New ListItem(Resources.Resource.lblAll, ""))
        For Each li As ListItem In ddlRegister.Items
            If li.Value = BusinessUtility.GetString(Session("RegCode")) Then
                li.Selected = True
            End If
        Next
        'DropDownHelper.FillPosRegister(ddlRegister, "", New ListItem(Resources.Resource.lblAll, "0"))

        If Not Page.IsPostBack Then
            lblHeading.Text = PosReprintHeading
            subFillGrid()
        Else
            subFillGrid()

        End If
        lblPopupheading.Text = lblSetStartAmount
        CmdSetAmount.Attributes.Add("onclick", "return funSetAmount();")
        lblHeading.Text = msgManageRegister
    End Sub
    Public Sub subFillGrid()
        Dim sRegister As String = ddlRegister.SelectedItem.Value
        Dim sStatus As String = ddlSearch.SelectedItem.Value
        objRegTra.RegisterCode = sRegister
        objRegTra.Status = sStatus
        sqlsdviewRegTran.SelectCommand = objRegTra.funFillGrid()
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    'Protected Sub grdvViewRegister_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdvViewRegister.PageIndexChanging
    '    subFillGrid()
    'End Sub
    'Protected Sub grdvViewRegister_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdvViewRegister.RowCommand
    '    If e.CommandName = "Edit" Then
    '        divSetAmount.Style("display") = "block"
    '        trClose.Style("visibility") = "visible"
    '        'divSetAmount.Style("position") = "absolute"
    '        'divSetAmount.Style("top") = "100px"
    '        'divSetAmount.Style("left") = hdnValue.Value & "px"
    '        lblPopupheading.Text = lblSetCloseAmount
    '        txtstartAmt.Enabled = False
    '        funFillControl(e.CommandArgument)
    '        subFillGrid()
    '    End If
    'End Sub
    Public Function funFillControl(ByVal strRegTranID As String)
        objRegTra.RegTransID = strRegTranID
        hdnRegTranID.Value = strRegTranID
        objRegTra.subGetRegiTran()
        txtstartAmt.Text = objRegTra.RegStartAmount
        If objRegTra.RegCloseAmount <> "" Then
            txtCloseAmount.Text = objRegTra.RegCloseAmount
            ' cmdSetCloseAmount.Style("visibility") = "hidden"
            imgSetCloseAmount.Style("visibility") = "hidden"
        Else
            txtCloseAmount.Text = objRegTra.funCalCloseAmt(txtstartAmt.Text, strRegTranID)
            'cmdSetCloseAmount.Style("visibility") = "visible"
            imgSetCloseAmount.Style("visibility") = "visible"
        End If
    End Function
    'Protected Sub grdvViewRegister_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdvViewRegister.RowDataBound
    '    If (e.Row.RowType = DataControlRowType.DataRow) Then
    '        Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
    '        imgDelete.Attributes.Add("onclick", "javascript:return " & _
    '        "confirm(" & msgConformDeleteTransaction & ")")

    '        'Dim imgEdit As ImageButton = CType(e.Row.FindControl("imgEdit"), ImageButton)
    '        'imgEdit.Attributes.Add("onmousedown", "return funSetAmount(); ")

    '        If (e.Row.RowType = DataControlRowType.DataRow) Then
    '            e.Row.Cells(4).Text = String.Format("{0:F}", CDbl(e.Row.Cells(4).Text))
    '            e.Row.Cells(3).Text = String.Format("{0:F}", CDbl(e.Row.Cells(3).Text))
    '        End If
    '        e.Row.Cells(1).Text = objCom.GetFormatDate(e.Row.Cells(1).Text, 2)
    '        If e.Row.Cells(2).Text <> "--" Then
    '            e.Row.Cells(2).Text = objCom.GetFormatDate(e.Row.Cells(2).Text, 2)
    '        End If
    '    End If
    'End Sub
    'Protected Sub grdvViewRegister_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdvViewRegister.RowEditing

    'End Sub
    'Protected Sub grdvViewRegister_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdvViewRegister.Sorting
    '    subFillGrid()
    'End Sub
    'Protected Sub CmdSetAmount_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdSetAmount.ServerClick
    
    'End Sub
    Protected Sub imgSetCloseAmount_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSetCloseAmount.Click
        If hdnRegTranID.Value = "" Then
            objRegTra.RegCode = Session("RegCode")
            objRegTra.RegMgr = Session("userID")
            objRegTra.RegStartAmount = txtstartAmt.Text
            objRegTra.RegStartDateTime = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
            objRegTra.RegCloseDateTime = "sysNull"
            objRegTra.RegCloseAmount = "sysNull"
            objRegTra.RegPOSUser = "sysNull"
            If objRegTra.funCheckCloseAmt() = True Then
                objRegTra.funInsertRegiTran()
                lblMsg.Text = msgStartAmountSaved
            Else
                lblMsg.Text = msgPreviousCloseAmt
                lblMsg.ForeColor = Drawing.Color.Red
            End If
        Else
            objRegTra.RegTransID = hdnRegTranID.Value
            objRegTra.RegStartAmount = txtstartAmt.Text
            objRegTra.RegCloseAmount = txtCloseAmount.Text
            objRegTra.RegMgr = Session("userID")
            objRegTra.RegCloseDateTime = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
            If txtCloseAmount.Text = "" Then
                lblMsg.Text = reqvalCloseAmt
                lblMsg.ForeColor = Drawing.Color.Red
                hdnRegTranID.Value = ""
                txtstartAmt.Enabled = True
                trClose.Style("visibility") = "hidden"
                Exit Sub
            End If
            objRegTra.funUpdateRegiTran()
            lblMsg.Text = msgCloseAmountSaved
        End If
        hdnRegTranID.Value = ""
        txtstartAmt.Enabled = True
        trClose.Style("visibility") = "hidden"
        subFillGrid()
    End Sub

    Protected Sub jgdvCurrency_CellBinding(sender As Object, e As Trirand.Web.UI.WebControls.JQGridCellBindEventArgs)
        If e.ColumnIndex = 11 Then
            'e.CellHtml = String.Format("<a class=""edit-Currency""  href=""AddEditCurrency.aspx?CurrencyID={0}&jscallback={1}"" >Edit</a>", e.RowValues(0), "reloadGrid")
            e.CellHtml = String.Format("<a style='text-decoration:none;' href='#' onclick='AddEditRegister({0})' >Edit</a>", e.RowValues(0))
        End If
        If e.ColumnIndex = 5 Then
            'Dim delUrl As String = String.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_CURRENCY, e.RowKey, "reloadGrid")
            'e.CellHtml = String.Format("<a class=""pop_delete"" href=""{0}"">Delete</a>", delUrl)
        End If
    End Sub

    Protected Sub jgdvCurrency_DataRequesting(sender As Object, e As Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs)

        sqlsdviewRegTran.SelectCommand = objRegTra.funFillGrid()
    End Sub

    Protected Sub imgSetCloseAmountds_Click(sender As Object, e As System.EventArgs) Handles imgSetCloseAmountds.Click
        Response.Write("Hello Ram")
    End Sub
End Class

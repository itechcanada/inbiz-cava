﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" 
    CodeFile="GiftCardVarifaction.aspx.cs" Inherits="POS_GiftCardVarifaction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <style type="text/css">
        .messageboxerror
        {
            color: Red;
        }
        
        .messageboxok
        {
            color: Green;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="dialogAssignUser" style="padding: 5px;">
        <%--        <h3>
            <asp:Label ID="Label1" Text="<%$Resources:Resource, btnAddNewColor%>" runat="server" />
        </h3>--%>
        <table>
            <tr>
                <td>
                    <%= Resources.Resource.lblGiftCardNo%>
                </td>
                <td>
                    <asp:TextBox ID="txtGiftCardNo" runat="server" MaxLength="20" Width="200px" />&nbsp;<font
                        style='color: red'>*</font> </br><span id="msgbox" style="display: none;"></span>
                    <asp:RequiredFieldValidator ID="reqvalManagerCode" ErrorMessage="<%$ Resources:Resource, reqMsgGiftCardNo%>"
                        runat="server" ControlToValidate="txtGiftCardNo" Display="None" SetFocusOnError="true"
                        ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <%= Resources.Resource.lblAvailableAmount%>
                </td>
                <td>
                    <asp:TextBox ID="txtAvailableAmount" runat="server" MaxLength="20"  Text="0.00" Width="200px" ReadOnly="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" Text="<%$Resources:Resource,lblGiftCardAmount%>" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtDiscount" runat="server" MaxLength="10" CssClass="numericTextField" Width="200px"
                        Text="0.00" />
                    <asp:HiddenField ID="hdnAvlCrdt" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="<%$ Resources:Resource, reqvalDiscPer%>"
                        runat="server" ControlToValidate="txtDiscount" Display="None" SetFocusOnError="true"
                        ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
    </div>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, Ok%>" runat="server" OnClick="btnSave_Click" 
            ValidationGroup="RegisterUser" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
            OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
    </div>
    <asp:HiddenField ID="hdnValidSoNo" runat="server" />
    <asp:HiddenField ID="hdnAvailableAmount" runat="server" Value="0.00" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script language="javascript">
        $(document).ready(function () {
            var txtGiftCardNo = $("#<%=txtGiftCardNo.ClientID %>");
            var msgBox = $("#msgbox");
            var divCustName = $("#trCustName");


            txtGiftCardNo.live("blur", function () {
                if (txtGiftCardNo.val() == "") {
                    msgBox.hide();
                    divCustName.hide();
                    return;
                }
                //remove all the class add the messagebox classes and start fading
                msgBox.removeClass().addClass('messagebox').text('Checking...').fadeIn("slow");


                //check the username exists or not from ajax
                try {
                    $.ajax({
                        type: "POST",
                        url: "GiftCardVarifaction.aspx/ValidateGiftCard",
                        dataType: "json",
                        data: "{CardNo:'" + txtGiftCardNo.val() + "' }",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            JsonObject = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
                            if (JsonObject.vStatus == 'NO') //if username not avaiable
                            {
                                $("#<%=hdnValidSoNo.ClientID%>").val("");
                                msgBox.fadeTo(200, 0.1, function () //start fading the messagebox
                                {
                                    var tval = "<%=Resources.Resource.lblEnterValidGiftCardNo%>";
                                    //add message and change the class of the box and start fading
                                    $(this).html(tval).addClass('messageboxerror').fadeTo(900, 1);
                                    divCustName.hide();
                                });
                            }
                            else {
                                $("#<%=hdnValidSoNo.ClientID%>").val(txtGiftCardNo.val());
                                msgBox.fadeTo(200, 0.1, function ()  //start fading the messagebox
                                {
                                    //add message and change the class of the box and start fading
                                    $("#<%=txtAvailableAmount.ClientID%>").val(JsonObject.vBalAmount);
                                    $("#<%=hdnAvailableAmount.ClientID%>").val(JsonObject.vBalAmount);
                                    $(this).html('Valid').addClass('messageboxok').fadeTo(900, 1);
                                    divCustName.show();
                                    $("#<%=txtDiscount.ClientID%>").focus();
                                });
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                        }
                    });
                }
                catch (e) {
                }
            });
        });

    </script>
</asp:Content>

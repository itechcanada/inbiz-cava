<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RefundReciept.aspx.vb" Inherits="POS_RefundReciept" %>

<%@ Import Namespace="Resources.Resource" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%=prjCompanyTitle%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
    <link href="../Css/style.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin: 0;">
    <form id="form1" runat="server">
    <div align="Left" id="div1">
        <table width="230px" cellspacing="0" cellpadding="0" class="tablepos" border="0">
            <% If ((Request.QueryString("status") = "1") Or Request.QueryString("TType") = "1" Or Request.QueryString("TType") = "6") Then%>
            <tr>
                <td class="POSPrintalign">
                    <asp:Label CssClass="lblPOSPrintBold" ID="lblCompanyName" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPrintalign">
                    <asp:Label CssClass="lblPOSPrint" ID="lblComAdd" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPrintalign">
                    <asp:Label CssClass="lblPOSPrint" ID="lblCompanyPhone" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label runat="server" CssClass="lblPOSPrint" ID="lbldate"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <asp:Label ID="lblProIL" CssClass="lblPOSPrint" runat="server" /><br />
                    <asp:Label runat="server" Style="font-family: IDAutomationHC39M; color: Black; font-size: 14px;"
                        ID="lblTransaction"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <span style="color: Black;">---------------------------------------------------------</span>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Table Width="230" ID="Table1" CellPadding="1" CellSpacing="1" runat="server">
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Table ID="tblsubTotal" Width="230" CellPadding="1" CellSpacing="1" GridLines="None"
                        runat="server">
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td>
                    <span style="color: Black;">---------------------------------------------------------</span>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Table ID="tblTax" Width="230" CellPadding="1" CellSpacing="1" GridLines="None"
                        runat="server">
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td>
                    <span style="color: Black;">---------------------------------------------------------</span>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Table ID="tblAmount" Width="230" CellPadding="1" CellSpacing="1" GridLines="None"
                        runat="server">
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td>
                    <span style="color: Black;">---------------------------------------------------------</span>
                </td>
            </tr>
            <tr>
                <td height="20">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblRegMessage" Text=""></asp:Label>
                </td>
            </tr>
            <tr runat="server" id="trReprintP" visible="false">
                <td align="right" class="POSPrintalign">
                    <asp:Label ID="Label7" Text="<%$ Resources:Resource, PosReprint%>" CssClass="lblPOSPrintBold"
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td height="40">
                </td>
            </tr>
            <tr>
                <td>
                    <div style="page-break-after: always;">
                    </div>
                </td>
            </tr>
            <% End If%>
            <% If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" And (((Request.QueryString("TType") = "12" Or Request.QueryString("TType") = "13" Or Request.QueryString("TType") = "14") And Request.QueryString("status") = "1")) Then%>
            <tr>
                <td height="40">
                </td>
            </tr>
            <tr>
                <td class="POSPrintalign">
                    <asp:Label ID="lblCustCompanyName" CssClass="lblPOSPrintBold" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPrintalign">
                    <asp:Label ID="lblCustAdd" CssClass="lblPOSPrint" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPrintalign">
                    <asp:Label ID="lblCustCompanyPhone" CssClass="lblPOSPrint" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPrintalign">
                    <asp:Label ID="lblMsgc" CssClass="lblPOSPrint" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblCustDate"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <asp:Label ID="lblCustTran" CssClass="lblPOSPrint" runat="server" /><br />
                    <asp:Label runat="server" Style="font-family: IDAutomationHC39M; color: Black; font-size: 14px;"
                        ID="lblCustTransactionID"></asp:Label>
                </td>
            </tr>
            <% If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" Then%>
            <tr>
                <td align="left" style="padding-left: 40Px;">
                    <asp:Label ID="Label1" CssClass="lblPOSPrint" Text="<%$ Resources:Resource, POSPrintCardName%>"
                        runat="server" />&nbsp;&nbsp;<asp:Label runat="server" CssClass="lblPOSPrint" ID="lblCustCardName"></asp:Label><br />
                    <asp:Label ID="Label2" CssClass="lblPOSPrint" Text="<%$ Resources:Resource, POSPrintCardNo%>"
                        runat="server" />&nbsp;&nbsp;<asp:Label runat="server" CssClass="lblPOSPrint" ID="lblCustCardNo"></asp:Label>
                </td>
            </tr>
            <% End If%>
            <tr>
                <td align="right">
                    <asp:Label runat="server" CssClass="lblPOSPrintBold" ID="lblCustPrintTotalAmount"
                        Text="<%$ Resources:Resource, lblPOSRefund%>" />
                    <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblCustTotalAmount"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="5">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblCustSign" Text="<%$ Resources:Resource, POSPrintSign%>"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="15">
                </td>
            </tr>
            <tr>
                <td class="POSPrintalign">
                    <asp:Label ID="lblCustomer" Text="<%$ Resources:Resource, PosCustomerPrint%>" CssClass="lblPOSPrintBold"
                        runat="server" />
                </td>
            </tr>
            <tr runat="server" id="trCustReprint" visible="false">
                <td align="right" class="POSPrintalign">
                    <asp:Label ID="Label13" Text="<%$ Resources:Resource, PosReprint%>" CssClass="lblPOSPrintBold"
                        runat="server" />
                </td>
            </tr>
            <%--Merchant--%>
            <tr>
                <td height="40">
                </td>
            </tr>
            <tr>
                <td>
                    <div style="page-break-after: always;">
                    </div>
                </td>
            </tr>
            <% End If%>
            <% If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" And (((Request.QueryString("TType") = "12" Or Request.QueryString("TType") = "13" Or Request.QueryString("TType") = "14") And Request.QueryString("status") = "1")) Then%>
            <tr>
                <td class="POSPrintalign">
                    <asp:Label ID="lblMerCompanyName" CssClass="lblPOSPrintBold" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPrintalign">
                    <asp:Label ID="lblMerAdd" CssClass="lblPOSPrint" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPrintalign">
                    <asp:Label ID="lblMerCompanyPhone" CssClass="lblPOSPrint" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="POSPrintalign">
                    <asp:Label ID="lblMsgM" CssClass="lblPOSPrint" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblMerDate"></asp:Label>
                </td>
            </tr>
            <tr>
                <tr>
                    <td align="Left">
                        <asp:Label ID="lblMerTran" CssClass="lblPOSPrint" runat="server" /><br />
                        <asp:Label runat="server" Style="font-family: IDAutomationHC39M; color: Black; font-size: 14px;"
                            ID="lblMerTransactionID"></asp:Label>
                    </td>
                </tr>
                <% If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" Then%>
                <tr>
                    <td align="left" style="padding-left: 40Px;">
                        <asp:Label ID="Label3" CssClass="lblPOSPrint" Text="<%$ Resources:Resource, POSPrintCardName%>"
                            runat="server" />&nbsp;&nbsp;<asp:Label runat="server" CssClass="lblPOSPrint" ID="lblMCardName"></asp:Label><br />
                        <asp:Label ID="Label6" CssClass="lblPOSPrint" Text="<%$ Resources:Resource, POSPrintCardNo%>"
                            runat="server" />&nbsp;&nbsp;<asp:Label runat="server" CssClass="lblPOSPrint" ID="lblMCardNo"></asp:Label>
                    </td>
                </tr>
                <% End If%>
                <tr>
                    <td align="right">
                        <asp:Label runat="server" CssClass="lblPOSPrintBold" ID="lblMerPrintTotalAmount"
                            Text="<%$ Resources:Resource, lblPOSRefund%>"></asp:Label>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblMerTotalAmount"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="5">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblMerSign" Text="<%$ Resources:Resource, POSPrintSign%>"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="15">
                    </td>
                </tr>
                <tr>
                    <td class="POSPrintalign">
                        <asp:Label ID="Label4" Text="<%$ Resources:Resource, PosMerchantPrint%>" CssClass="lblPOSPrintBold"
                            runat="server" />
                    </td>
                </tr>
                <tr runat="server" id="trReprint" visible="false">
                    <td align="right" class="POSPrintalign">
                        <asp:Label ID="Label5" Text="<%$ Resources:Resource, PosReprint%>" CssClass="lblPOSPrintBold"
                            runat="server" />
                    </td>
                </tr>
                <% End If%>
                <%-- For Debit card print customercopy --%>
                <% If ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" And (Request.QueryString("TType") = "5" Or Request.QueryString("TType") = "2" Or Request.QueryString("TType") = "3" Or Request.QueryString("TType") = "4") Then%>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblCStartNote" CssClass="lblPOSPrint" Text="<%$ Resources:Resource, lblTransactionReceipt%>"
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td height="3">
                    </td>
                </tr>
                <tr>
                    <td class="POSPrintalign">
                        <asp:Label ID="lblDebCompanyName" CssClass="lblPOSPrintBold" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="POSPrintalign">
                        <asp:Label ID="lblDebComAddress" CssClass="lblPOSPrint" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="POSPrintalign">
                        <asp:Label ID="lblDebPhone" CssClass="lblPOSPrint" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="Left">
                        <asp:Label runat="server" Style="font-family: IDAutomationHC39M; color: Black; font-size: 14px;"
                            ID="lblDebTranBarcode"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="8">
                    </td>
                </tr>
                <tr>
                    <td align="Left">
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="Label9" Text="<%$ Resources:Resource, lblPosType%>"></asp:Label>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblDebType" Text="<%$ Resources:Resource, msgTypeRefund%>"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="Left">
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="Label10" Text="<%$ Resources:Resource, lblAcct%>"></asp:Label>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblDebAccType" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="10">
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label runat="server" CssClass="lblPOSPrintBold" ID="lbl10" Text="<%$ Resources:Resource, lblTotalAmount%>"></asp:Label>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblDebTotalAmt"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="15">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="Label11" Text="<%$ Resources:Resource, lblCardNo%>"></asp:Label>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblDebCardNo"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="3">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="Label12" Text="<%$ Resources:Resource, lblDateTime%>"></asp:Label>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblDebDateTime"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="3">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="Label14" Text="<%$ Resources:Resource, lblReference%>"></asp:Label>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblReference"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="3">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="Label16" Text="<%$ Resources:Resource, lblAut%>"></asp:Label>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblAuthor"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="15">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblDebMsg" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="20">
                    </td>
                </tr>
                <% If Request.QueryString("TType") = "2" Or Request.QueryString("TType") = "3" Or Request.QueryString("TType") = "4" Then%>
                <tr>
                    <td align="left">
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="Label18" Text="<%$ Resources:Resource, POSPrintSign%>"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="15">
                    </td>
                </tr>
                <% End If%>
                <tr>
                    <td class="POSPrintalign">
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblDebCopyType" Text=""></asp:Label>
                    </td>
                </tr>
                <tr runat="server" id="trDebCustReprint" visible="false">
                    <td align="right" class="POSPrintalign">
                        <asp:Label ID="Label15" Text="<%$ Resources:Resource, PosReprint%>" CssClass="lblPOSPrintBold"
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td height="40">
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="page-break-after: always;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblMstartNote" CssClass="lblPOSPrint" Text="<%$ Resources:Resource, lblTransactionReceipt%>"
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td height="3">
                    </td>
                </tr>
                <tr>
                    <td class="POSPrintalign">
                        <asp:Label ID="lblDebMCompanyName" CssClass="lblPOSPrintBold" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="POSPrintalign">
                        <asp:Label ID="lblDebMaddress" CssClass="lblPOSPrint" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="POSPrintalign">
                        <asp:Label ID="lblDebMPhone" CssClass="lblPOSPrint" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="Left">
                        <asp:Label runat="server" Style="font-family: IDAutomationHC39M; color: Black; font-size: 14px;"
                            ID="lblDebTransMBarcode"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="8">
                    </td>
                </tr>
                <tr>
                    <td align="Left">
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="Label19" Text="<%$ Resources:Resource, lblPosType%>"></asp:Label>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="Label20" Text="<%$ Resources:Resource, msgTypeRefund%>"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="Left">
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="Label21" Text="<%$ Resources:Resource, lblAcct%>"></asp:Label>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblDebMAccType" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="10">
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label runat="server" CssClass="lblPOSPrintBold" ID="Label23" Text="<%$ Resources:Resource, lblTotalAmount%>"></asp:Label>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblDebMTotalAmt"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="15">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="Label25" Text="<%$ Resources:Resource, lblCardNo%>"></asp:Label>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblDebMCardNo"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="3">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="Label27" Text="<%$ Resources:Resource, lblDateTime%>"></asp:Label>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblDebMDateTime"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="3">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="Label29" Text="<%$ Resources:Resource, lblReference%>"></asp:Label>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblDebMReference"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="3">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="Label31" Text="<%$ Resources:Resource, lblAut%>"></asp:Label>
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblDebMAuthor"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="15">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblDebMMsg" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="20">
                    </td>
                </tr>
                <% If Request.QueryString("TType") = "2" Or Request.QueryString("TType") = "3" Or Request.QueryString("TType") = "4" Then%>
                <tr>
                    <td align="left">
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="Label8" Text="<%$ Resources:Resource, POSPrintSign%>"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="15">
                    </td>
                </tr>
                <% End If%>
                <tr>
                    <td class="POSPrintalign">
                        <asp:Label runat="server" CssClass="lblPOSPrint" ID="lblDebMCopyType" Text=""></asp:Label>
                    </td>
                </tr>
                <tr runat="server" id="trDebMReprint" visible="false">
                    <td align="right" class="POSPrintalign">
                        <asp:Label ID="Label17" Text="<%$ Resources:Resource, PosReprint%>" CssClass="lblPOSPrintBold"
                            runat="server" />
                    </td>
                </tr>
                <% End If%>
        </table>
    </div>
    </form>
</body>
<script language="javascript">
    var PrintCommandObject = null;

    function printPage() {
        if (PrintCommandObject) {
            try {
                PrintCommandObject.ExecWB(6, 2);
                PrintCommandObject.outerHTML = "";
            }
            catch (e) {
                window.print();
            }
        }
        else {
            window.print();
        }
        var browserName = navigator.appName;
        if (navigator.appName != "Microsoft Internet Explorer") {
            window.close();
        }
        else {
            if (Request.QueryString["cardtype"] == "R") {
                location.href = "Receipts.aspx";
            } else {
                location.href = "Refund.aspx?TType=R";
            }
        }
    }


    window.onload = function () {
        Minimize();
        if (navigator.appName == "Microsoft Internet Explorer") {
            // attach and initialize print command ActiveX object
            try {
                var PrintCommand = '<object id="PrintCommandObject" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2" width="0" height="0"></object>';
                document.body.insertAdjacentHTML('beforeEnd', PrintCommand);
            }
            catch (e) { }
        }
        setTimeout(printPage, 1);
    };

    //function printIt(target) { 
    //  Minimize();
    //  winId=window.open('','printwin','width=2,height=2,left=900,top=800,'); 
    //  winId.document.write(target); 
    //  winId.document.close(); 
    //  winId.focus(); 
    //  if (window.print) winId.print(); 
    //  winId.close();//this line is added to close the window after printing
    //} 

    function Minimize() {
        window.innerWidth = 10;
        window.innerHeight = 10;
        window.screenX = screen.width;
        window.screenY = screen.height;
        alwaysLowered = true;
    }

</script>
</html>

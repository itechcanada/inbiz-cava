﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="InstoreCredit.aspx.cs" Inherits="POS_InstoreCredit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="dialogAssignUser" style="padding: 5px;">
        <%--        <h3>
            <asp:Label ID="Label1" Text="<%$Resources:Resource, btnAddNewColor%>" runat="server" />
        </h3>--%>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblAvailabeCredit" Text="<%$Resources:Resource,lblCustomerAvailableCredit1%>"
                        runat="server" />
                </td>
                <td>
                    <asp:Label ID="valAvailCredit" Text="" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" Text="<%$Resources:Resource,lblCreditAmount%>" runat="server" />
                </td>
                <td>
                    <asp:TextBox ID="txtDiscount" runat="server" MaxLength="10" CssClass="numericTextField" />
                    <asp:HiddenField id="hdnAvlCrdt" runat="server" />
                    <asp:RequiredFieldValidator ID="reqvalManagerCode" ErrorMessage="<%$ Resources:Resource, reqvalDiscPer%>"
                        runat="server" ControlToValidate="txtDiscount" Display="None" SetFocusOnError="true"
                        ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
    </div>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, Ok%>" runat="server" OnClick="btnSave_Click"
            ValidationGroup="RegisterUser" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
            OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
</asp:Content>

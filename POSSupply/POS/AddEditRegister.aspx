﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="AddEditRegister.aspx.cs" Inherits="POS_AddEditRegister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divSetAmount" runat="server">
        <asp:Panel runat="server" ID="pnlPosCat">
            <table cellpadding="0" cellspacing="0" width="350px" style="display: none;">
                <tr>
                    <td width='15px' background="../Images/popup_left.png"></td>
                    <td align="left" valign="middle" class="popup_title_newbg">
                        <asp:Label class="discount_title" runat="server" ID="lblPopupheading"></asp:Label>
                    </td>
                    <td class="popup_title_newbg" align="right">
                        <a href="javascript:funPOSCloseSetAmount();">
                            <img border="0" src='../images/close_popup_btn.gif' width="24" height="24" /></a>
                    </td>
                    <td width='15px' background="../Images/popup_right.png"></td>
                </tr>
            </table>
            <table width="100%" border='0'>
                <%--class="popup_content_newbg"--%>
                <%-- <tr>
                    <td height="10px">
                    </td>
                </tr>--%>
                <tr>
                    <td width="35%" align="right">
                        <%= Resources.Resource.POSStarAmt%>
                        <%-- <asp:Label ID="lblstartAmt" Text="<%$ Resources:Resource, POSStarAmt %>" CssClass="lblBold"
                            runat="server" />--%>
                    </td>
                    <td align="left" width="65%">
                        <asp:TextBox runat="server" ID="txtstartAmt" ValidationGroup="Amount" CssClass="numericTextField"
                            Width="150px" MaxLength="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalstartAmt" ValidationGroup="Amount" runat="server"
                            ControlToValidate="txtstartAmt" ErrorMessage="<%$ Resources:Resource, reqvalPOSStartAmount %>"
                            SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator><span style="color: Red; font-weight: bold; font-size: 11pt;"> *</span>
                        <%--<asp:CompareValidator ID="compvalstartAmt" EnableClientScript="true" ValidationGroup="Amount"
                            SetFocusOnError="true" ControlToValidate="txtstartAmt" Operator="DataTypeCheck"
                            Type="Double" Display="None" ErrorMessage="<%$ Resources:Resource, compvalstartAmt %>"
                            runat="server" />--%>
                    </td>
                </tr>
                <tr id="trCalculatedCash" runat="server">
                    <td width="35%" align="right">
                        <%= Resources.Resource.lblCalculatedCash%>
                    </td>
                    <td align="left" width="65%">
                        <asp:TextBox runat="server" ID="txtCalculatedCash" CssClass="numericTextField" Text="0.00"
                            Width="150px" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <%--                <tr><td colspan="2">
                
                </td></tr>--%>
                <tr id="trClose" runat="server">
                    <td align="right">
                        <asp:Label ID="Label3" Text="<%$ Resources:Resource, PosCloseAmt %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td align="left">
                        <asp:TextBox runat="server" ID="txtCloseAmount" ValidationGroup="Amount" CssClass="popup_input numericTextField"
                            Width="150px" MaxLength="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalLastName" ValidationGroup="Amount" runat="server"
                            ControlToValidate="txtCloseAmount" ErrorMessage="<%$ Resources:Resource, reqvalCloseAmt %>"
                            SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                        <span style="color: Red; font-weight: bold; font-size: 11pt;">*</span>
                    </td>
                </tr>
                <%-- <tr>
                    <td id="Td1" align="center" runat="server" colspan="2">
                        <asp:ImageButton runat="server" ID="imgSetCloseAmount" ValidationGroup="Amount" ImageUrl="~/images/submit.png" />
                    </td>
                </tr>--%>
            </table>
            <asp:Panel ID="pnlAmount" runat="server">
                <asp:Table ID="tblAmountDemonation" runat="server" Style="border: 1px; width: 100%;">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell>Denomination</asp:TableHeaderCell>
                        <asp:TableHeaderCell>Count</asp:TableHeaderCell>
                        <asp:TableHeaderCell>Total</asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                </asp:Table>
            </asp:Panel>
            <div class="div_command">
                <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" OnClick="btnSave_Click"
                    ValidationGroup="Amount" />
                <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
                    OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
            </div>
            <asp:ValidationSummary ID="valsumSetAmt" runat="server" ShowMessageBox="true" ValidationGroup="Amount"
                ShowSummary="false" />
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $(".numericTextField").numeric();
        $(".integerTextField").numeric(false, function () { alert("Integers only"); this.value = ""; this.focus(); });

        function calcAmount(elementID) {

            var vCount = 0;
            if ($("#" + elementID.id).val() == '') {
                vCount = 0;
            }
            else {
                vCount = parseInt($("#" + elementID.id).val());
            }
            var vID = $("#" + elementID.id).attr("txtid");
            var vValue = parseFloat($("#" + elementID.id).attr("amtvalue"));
            var vTotal = (vCount * vValue);
            if (vTotal != 'NaN') {

                $("#ctl00_cphMaster_txtTotal_" + vID).val(vTotal.toFixed(2));
            } else {
                $("#ctl00_cphMaster_txtTotal_" + vID).val("0".toFixed(2));
            }
            CalculatTotal();
        }

        function CalculatTotal() {
            var vGrandTotalQty = Number("0");
            $('.cssCount').each(function () {
                vGrandTotalQty = vGrandTotalQty + Number($(this).val());
            });

            vGrandTotalQty = Number("0");
            $('.cssTotal').each(function () {
                vGrandTotalQty = vGrandTotalQty + Number($(this).val());
            });
            $("#ctl00_cphMaster_txtTotalAmt").val(vGrandTotalQty.toFixed(2));

            $("#ctl00_cphMaster_txtTotaCount").val(vGrandTotalQty.toFixed(2));

            var vTransID = parseInt(getParameterByName('RegTranID'));
            if (vTransID > 0) {
                $("#ctl00_cphMaster_txtCloseAmount").val(vGrandTotalQty.toFixed(2));
            }
            else {
                $("#ctl00_cphMaster_txtstartAmt").val(vGrandTotalQty.toFixed(2));
            }
        }

        $(document).ready(function () {
            $("input:text").focus(function () { $(this).select(); });
            $("#ctl00_cphMaster_txtstartAmt").attr('readonly', 'true');
            $("#ctl00_cphMaster_txtCloseAmount").attr('readonly', 'true');

            $("#ctl00_cphMaster_txtCloseAmount").css('background-color', '#DEDEDE');
            $("#ctl00_cphMaster_txtstartAmt").css('background-color', '#DEDEDE');
            $("#ctl00_cphMaster_txtCount_1").trigger("onkeyup");
            $("#ctl00_cphMaster_txtCount_1").focus();

            var vTransID = parseInt(getParameterByName('RegTranID'));
            if (vTransID > 0) {

            }
            else {
                CalculatTotal();
            }

            $('.cssCount').each(function () {
                $(this).trigger("onkeyup");
            });

        });

        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
            return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

    </script>
</asp:Content>

Imports System.Data
Imports Trirand.Web.UI.WebControls

Partial Class Inventory_ViewCategory
    Inherits BasePage
    Public objCategory As New clsCategory
    Private strCatID As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        lblMsg.Text = ""
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If
        If Not Page.IsPostBack AndAlso jqgCategories.AjaxCallBackMode = AjaxCallBackMode.None Then

        End If
        If Request.QueryString("Approve") <> "" Then
            lblHeading.Text = Resources.Resource.PendingForApproval
        Else
            lblHeading.Text = Resources.Resource.ViewCategories
        End If
    End Sub
    Public Sub subFillGrid()
        sqlsdAppAdmin.SelectCommand = objCategory.funFillGrid(Request.QueryString("Approve"))  'fill Category Record
    End Sub
    'Protected Sub grdvAppCategory_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdvAppCategory.PageIndexChanging
    '    subFillGrid()
    'End Sub
    'Protected Sub grdvAppCategory_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdvAppCategory.RowDataBound
    '    If (e.Row.RowType = DataControlRowType.DataRow) Then
    '        Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
    '        imgDelete.Attributes.Add("onclick", "javascript:return " & _
    '        "confirm('" & msgAreYouSureYouWantToDeleteTheCategory & "')")
    '    End If
    'End Sub
    'Protected Sub grdvAppCategory_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdvAppCategory.RowDeleting
    '    strCatID = grdvAppCategory.DataKeys(e.RowIndex).Value.ToString()
    '    lblMsg.Text = msgCategoryHasBeenDeletedSuccessfully
    '    sqlsdAppAdmin.DeleteCommand = objCategory.funDeleteCategory(strCatID) 'Delete Category
    '    objCategory.CategoryID = strCatID
    '    objCategory.subGetCategoryInfo()
    '    Dim ObjDataClass As New clsDataClass()
    '    AdjustWebSeq(objCategory.CategoryWebSequence, ObjDataClass)
    '    subFillGrid()
    'End Sub
    'Protected Sub grdvAppCategory_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdvAppCategory.RowEditing
    '    Dim CatID As String = grdvAppCategory.DataKeys(e.NewEditIndex).Value.ToString()
    '    If Request.QueryString("Approve") <> "" Then
    '        Response.Redirect("AddCategory.aspx?CatID=" & CatID & "&Approve=" & Request.QueryString("Approve"))
    '    Else
    '        Response.Redirect("AddCategory.aspx?CatID=" & CatID)
    '    End If
    'End Sub
    Public Sub AdjustWebSeq(ByVal oldWebSeq As String, ByVal objclsDataClass As clsDataClass)
        Dim qry As String
        qry = "UPDATE category SET catWebSeq=catWebSeq-1 WHERE catWebSeq >" & oldWebSeq & " "
        objclsDataClass.SetData(qry)
    End Sub

    Protected Sub jqgCategories_DataRequesting(ByVal sender As Object, ByVal e As Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs) Handles jqgCategories.DataRequesting
        sqlsdAppAdmin.SelectCommand = objCategory.funFillGrid(Request.QueryString("Approve"))  'fill Category Record
    End Sub
End Class

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class ECommerce_SubCategoryEdit : BasePage
{
    EcomSubCategory _sCat = new EcomSubCategory();
    EcomCategory _cat = new EcomCategory();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            _cat.FillDropDown(ddlCategory, new ListItem("", ""), Globals.CurrentAppLanguageCode);
            rblLanguage.SelectedValue = this.EditLanguage;
            FillForm();            
        }
    }

    private void FillForm()
    {
        if (this.CategoryID > 0)
        {
            rblLanguage.Enabled = true;

            _sCat.PopulateObject(this.CategoryID, this.EditLanguage);
            ddlCategory.SelectedValue = _sCat.SubcatCatId.ToString();
            
            txtCatName.Text = _sCat.SubcatName;
            txteBayCatgNo.Text = _sCat.eBayCatgNo;            

            rblLanguage.SelectedValue = _sCat.SubcatdescLang;
            rbYes.Checked = _sCat.SubcatIsActive;

            imgCat.ImageUrl = "~/Thumbnail.ashx?p=" + Server.UrlEncode(ConfigurationManager.AppSettings["RelativePathForSubCategory"]) + Server.UrlEncode(_sCat.SubcatImagePath) + "&Cat=Subcat";
            aTag.HRef = Server.UrlEncode(ConfigurationManager.AppSettings["RelativePathForSubCategory"]) + _sCat.SubcatImagePath;
            if (string.IsNullOrEmpty(_sCat.SubcatImagePath))
            {
                liImage.Visible = false;
                liImageSelector.Visible = true;
            }
            else
            {
                liImage.Visible = true;
                liImageSelector.Visible = false;
            }
        }
        else
        {
            rblLanguage.SelectedValue = AppLanguageCode.DEFAULT;
            liImage.Visible = false;
            liImageSelector.Visible = true;
        }
    }

    private void SetData()
    {
        _sCat.SubcatdescLang = rblLanguage.SelectedValue;        
        _sCat.SubcatId = this.CategoryID;
        _sCat.SubcatCatId = BusinessUtility.GetInt(ddlCategory.SelectedValue);
        _sCat.SubcatIsActive = rbYes.Checked;        
        _sCat.SubcatName = txtCatName.Text;
        _sCat.SubcatStatus = true;
        _sCat.SubcatUpdatedBy = CurrentUser.UserID;
        _sCat.eBayCatgNo = txteBayCatgNo.Text;        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            this.SetData();
            if (this.CategoryID > 0)
            {                               
                if (ValidateCategoryImage())
                {
                    _sCat.SubcatImagePath = this.GetUploadFileName();
                    _sCat.Update(CurrentUser.UserID);

                    if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                    {
                        Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                    }
                    else
                    {
                        Globals.RegisterCloseDialogScript(Page);
                    }    
                }                
            }
            else
            {
                if (ValidateCategoryImage())
                {                    
                    _sCat.Insert(CurrentUser.UserID);
                    string imgPath = this.GetUploadFileName();
                    if (_sCat.SubcatId  > 0 && !string.IsNullOrEmpty(imgPath))
                    {
                        _sCat.InsertImage(_sCat.SubcatId, imgPath);
                    }

                    if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                    {
                        Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                    }
                    else
                    {
                        Globals.RegisterCloseDialogScript(Page);
                    }    
                }  
            }
        }
    }   

    private bool ValidateCategoryImage() {
        if (!fileCatImagePath.HasFile)
        {
            return true;
        }
        return FileManager.IsValidImageFile(fileCatImagePath.PostedFile.FileName);
    }

    private string GetUploadFileName() {
        if (!fileCatImagePath.HasFile) return string.Empty;
        string fileExt = System.IO.Path.GetExtension(fileCatImagePath.PostedFile.FileName);
        try
        {
            if (FileManager.IsValidImageFile(fileCatImagePath.PostedFile.FileName))
            {
                string fileName = BusinessUtility.GenerateRandomString(6, true) + fileExt;
                fileCatImagePath.SaveAs(Server.MapPath("~/Upload/subCategory/") + fileName);
                return fileName;
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCMInvalidFileExtension);
                return string.Empty;
            }
        }
        catch 
        {
            return string.Empty;
        }        
    }

    private int CategoryID {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["catID"], out id);
            return id;
        }
    }

    
    private string EditLanguage {
        get {
            switch (Request.QueryString["editLang"])
            {
                case AppLanguageCode.EN:
                    return AppLanguageCode.EN;
                case AppLanguageCode.FR:
                    return AppLanguageCode.FR;
                default:
                    return AppLanguageCode.DEFAULT;
            }                        
        }
    }

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }

    protected void imgCatImageUrl_Click(object sender, ImageClickEventArgs e)
    {
        if (this.CategoryID > 0)
        {
            _sCat.RemoveImage(this.CategoryID, rblLanguage.SelectedValue);

            if (!string.IsNullOrEmpty(this.JsCallBackFunction))
            {
                liImage.Visible = false;
                Globals.RegisterScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction));
            }               
        }        
    }
    protected void rblLanguage_SelectedIndexChanged(object sender, EventArgs e)
    {
        Response.Redirect(QueryString.GetModifiedUrl("editLang", rblLanguage.SelectedValue));
    }
}
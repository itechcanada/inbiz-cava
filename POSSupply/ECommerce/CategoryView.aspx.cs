﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.WebControls;
using iTECH.Library.Utilities;

public partial class ECommerce_CategoryView : BasePage
{
    EcomCategory _cat = new EcomCategory();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(jqgCategories))
        {
            lblHeading.Text = Resources.Resource.ViewCategories;
        }
    }

    protected void jqgCategories_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains("editSeq"))
        {
            int editRowKey, seqVal;
            int.TryParse(Request.QueryString["editRowKey"], out editRowKey);
            int.TryParse(Request.QueryString["seqVal"], out seqVal);
            if (editRowKey > 0)
            {
                _cat.UpdateSeq(editRowKey, seqVal);
            }
        }
        if (Request.QueryString.AllKeys.Contains("deleteCat"))
        {
            int deleteRowKey;
            int.TryParse(Request.QueryString["deleteRowKey"], out deleteRowKey);            
            if (deleteRowKey > 0)
            {
                _cat.Delete(deleteRowKey);
            }
        }

        sqlsdAppAdmin.SelectCommand = _cat.GetSql(sqlsdAppAdmin.SelectParameters, Request.QueryString[txtSearch.ClientID], Globals.CurrentAppLanguageCode);
    }

    protected void jqgCategories_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 5) //format Image
        {
            string imgPath = ConfigurationManager.AppSettings["RelativePathForCategory"] + e.CellHtml;
           e.CellHtml = "<a href='" + imgPath + "' target='_blank'>";
           e.CellHtml += "<img src='../Thumbnail.ashx?p=" + imgPath + "&Cat=Cat' /></a>";
        }
        if (e.ColumnIndex == 6) //Seq
        {
            e.CellHtml = string.Format("<input type='text' value='{0}' onchange='editSequence(this, {1});' style='width:50px;' />", e.CellHtml, e.RowKey);
        }
        if (e.ColumnIndex == 7) //Is Active
        {
            e.CellHtml = string.Format("<img src='{0}' />", ResolveUrl("~/Images/" + e.CellHtml));
        }
        
        if (e.ColumnIndex == 8) //Edit
        {
            string url = string.Format("CategoryEdit.aspx?catID={0}&editLang={1}&jscallback=reloadGrid", e.RowKey, Globals.CurrentAppLanguageCode);
            e.CellHtml = string.Format("<a class='edit_pop' href='{0}'>{1}</a>", url, Resources.Resource.lblEdit);
        }
        if (e.ColumnIndex == 9)
        {
            e.CellHtml = string.Format("<a href='javascript:;' onclick='deleteCategory({0});'>{1}</a>", e.RowKey, Resources.Resource.delete);
        }
    }
}
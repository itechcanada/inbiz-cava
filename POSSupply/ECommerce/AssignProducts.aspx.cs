﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.WebControls;
using iTECH.Library.Utilities;

public partial class ECommerce_AssignProducts : BasePage
{
    EcomCategory _cat = new EcomCategory();
    EcomSubCategory _sCat = new EcomSubCategory();
    Product _prd = new Product();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblHeading.Text = Resources.Resource.lnkInvProductAssociation;
            dlCatName.Attributes["data-placeholder"] = "[Select Category]";
            dlSubcatName.Attributes["data-placeholder"] = "[Select SubCategory]";            
            _cat.FillDropDown(dlCatName, new ListItem(""), Globals.CurrentAppLanguageCode);            
        }
    }

    protected void dlCatName_SelectedIndexChanged(object sender, EventArgs e)
    {
        lstProducts.Items.Clear();
        _sCat.FillDropDown(dlSubcatName, BusinessUtility.GetInt(dlCatName.SelectedValue), new ListItem(""), Globals.CurrentAppLanguageCode);
    }

    protected void dlSubcatName_SelectedIndexChanged(object sender, EventArgs e)
    {
        lstProducts.Items.Clear();

        int cID = BusinessUtility.GetInt(dlCatName.SelectedValue);
        int sCID = BusinessUtility.GetInt(dlSubcatName.SelectedValue);
        if (cID > 0 && sCID > 0)
        {
            _prd.FillProductsAccociations(lstProducts, cID, sCID, null, Globals.CurrentAppLanguageCode);
        }
        else
        {
            lstProducts.Items.Clear();
        }        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            int cID = BusinessUtility.GetInt(dlCatName.SelectedValue);
            int sCID = BusinessUtility.GetInt(dlSubcatName.SelectedValue);
            if (cID > 0 && sCID > 0)
            {
                List<int> prds = new List<int>();
                foreach (ListItem item in lstProducts.Items)
                {
                    if (item.Selected)
                    {
                        prds.Add(BusinessUtility.GetInt(item.Value));
                    }
                }
                _prd.AssignCategoryToProducts(cID, sCID, prds.ToArray());
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgProductHasBeenAssignedSuccessfully);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class ECommerce_CategoryEdit : BasePage
{
    EcomCategory _cat = new EcomCategory();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            rblLanguage.SelectedValue = this.EditLanguage;
            if (this.CategoryID > 0)
            {
                rblLanguage.Enabled = true;
                FillForm();                
            }
            else
            {
                liImage.Visible = false;
            }
        }
    }

    private void FillForm()
    {
        if (this.CategoryID > 0)
        {            
            _cat.PopulateObject(this.CategoryID, this.EditLanguage);

            txtCatName.Text = _cat.CatName;
            txteBayCatgNo.Text = _cat.eBayCatgNo;
            txtFreeText.Text = _cat.CatFreeText;

            rblLanguage.SelectedValue = _cat.CatDescLang;
            rbYes.Checked = _cat.CatIsActive;
            chkIsCustom.Checked = _cat.IsCustom;
            imgCat.ImageUrl = "~/Thumbnail.ashx?p=" + Server.UrlEncode(ConfigurationManager.AppSettings["RelativePathForCategory"]) + Server.UrlEncode(_cat.CatImagePath) + "&Cat=Cat";
            aTag.HRef = Server.UrlEncode(ConfigurationManager.AppSettings["RelativePathForCategory"]) + _cat.CatImagePath;
            if (string.IsNullOrEmpty(_cat.CatImagePath))
            {
                liImage.Visible = false;
                liImageSelector.Visible = true;
            }
            else
            {
                liImage.Visible = true;
                liImageSelector.Visible = false;
            }
            chkIsStarterCategory.Checked = _cat.IsStarterCategory;
        }
        else
        {
            rblLanguage.SelectedValue = AppLanguageCode.DEFAULT;
            liImage.Visible = false;
            liImageSelector.Visible = true;
        }
    }

    private void SetData()
    {
        _cat.CatDescLang = rblLanguage.SelectedValue;
        _cat.CatFreeText = txtFreeText.Text;
        _cat.CatId = this.CategoryID;
        _cat.CatIsActive = rbYes.Checked;
        _cat.CatIsFreeText = rblstCategory.SelectedValue == "1";
        _cat.CatName = txtCatName.Text;
        _cat.CatStatus = true;
        _cat.CatUpdatedBy = CurrentUser.UserID;
        _cat.eBayCatgNo = txteBayCatgNo.Text;
        _cat.IsCustom = chkIsCustom.Checked;
        _cat.IsStarterCategory = chkIsStarterCategory.Checked;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            this.SetData();
            if (this.CategoryID > 0)
            {                               
                if (ValidateCategoryImage())
                {
                    _cat.CatImagePath = this.GetUploadFileName();
                    _cat.Update(CurrentUser.UserID);

                    if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                    {
                        Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                    }
                    else
                    {
                        Globals.RegisterCloseDialogScript(Page);
                    }    
                }                
            }
            else
            {
                if (ValidateCategoryImage())
                {                    
                    _cat.Insert(CurrentUser.UserID);
                    string imgPath = this.GetUploadFileName();
                    if (_cat.CatId  > 0 && !string.IsNullOrEmpty(imgPath))
                    {
                        _cat.InsertImage(_cat.CatId, imgPath);
                    }

                    if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                    {
                        Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                    }
                    else
                    {
                        Globals.RegisterCloseDialogScript(Page);
                    }    
                }  
            }
        }
    }

    protected void rblstCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        liFreeText.Visible = rblstCategory.SelectedValue == "1";
    }

    private bool ValidateCategoryImage() {
        if (!fileCatImagePath.HasFile)
        {
            return true;
        }
        return FileManager.IsValidImageFile(fileCatImagePath.PostedFile.FileName);
    }

    private string GetUploadFileName() {
        if (!fileCatImagePath.HasFile) return string.Empty;
        string fileExt = System.IO.Path.GetExtension(fileCatImagePath.PostedFile.FileName);
        try
        {
            if (FileManager.IsValidImageFile(fileCatImagePath.PostedFile.FileName))
            {
                string fileName = BusinessUtility.GenerateRandomString(6, true) + fileExt;
                fileCatImagePath.SaveAs(Server.MapPath("~/Upload/Category/") + fileName);
                return fileName;
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCMInvalidFileExtension);
                return string.Empty;
            }
        }
        catch 
        {
            return string.Empty;
        }        
    }

    private int CategoryID {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["catID"], out id);
            return id;
        }
    }

    
    private string EditLanguage {
        get {
            switch (Request.QueryString["editLang"])
            {
                case AppLanguageCode.EN:
                    return AppLanguageCode.EN;
                case AppLanguageCode.FR:
                    return AppLanguageCode.FR;
                default:
                    return AppLanguageCode.DEFAULT;
            }                        
        }
    }

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }

    protected void imgCatImageUrl_Click(object sender, ImageClickEventArgs e)
    {
        if (this.CategoryID > 0)
        {
            _cat.RemoveImage(this.CategoryID, rblLanguage.SelectedValue);

            if (!string.IsNullOrEmpty(this.JsCallBackFunction))
            {
                liImage.Visible = false;
                Globals.RegisterScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction));
            }               
        }        
    }
    protected void rblLanguage_SelectedIndexChanged(object sender, EventArgs e)
    {
        Response.Redirect(QueryString.GetModifiedUrl("editLang", rblLanguage.SelectedValue));
    }
}
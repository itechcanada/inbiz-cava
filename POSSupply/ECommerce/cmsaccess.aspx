﻿<%@ Page Language="C#" %>
<%@ Import Namespace="iTECH.InbizERP.ECommerce" %>
<%@ Import Namespace="iTECH.Library.Utilities" %>
<%@ Import Namespace="iTECH.Library.Web" %>
<%@ Import Namespace="MySql.Data.MySqlClient" %>
<%@ Import Namespace="iTECH.Library.DataAccess.MySql" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="iTECH.InbizERP.BusinessLogic" %>
<script runat="server">
    protected string postUrl = "";
    
    

    protected void Page_Load(object sender, EventArgs e)
    {

        Response.Clear();
        Response.Write("Authenticating.....");
        StringBuilder sb = new StringBuilder();
        sb.Append("<html>");
        sb.AppendFormat(@"<body onload='document.forms[""form""].submit()'>");
        sb.AppendFormat("<form name='form' action='{0}' method='post'>", ConfigurationManager.AppSettings["CMS_ACCESS_URL"]);
        sb.AppendFormat("<input type='hidden' name='userid' value='{0}'>", CurrentUser.UserID);
        sb.AppendFormat("<input type='hidden' name='pageid' value='{0}'>", Request.QueryString["pageid"]);
        sb.AppendFormat("<input type='hidden' name='token' value='{0}'>", SingleSignOn.CreateToken(CurrentUser.UserID.ToString()));
        // Other params go here
        sb.Append("</form>");
        sb.Append("</body>");
        sb.Append("</html>");

        Response.Write(sb.ToString());        

        Response.End();
        
        
        /*// ensure that the user is authenticated
        if (!Request.IsAuthenticated)
        {
            //Response.StatusCode = 401;
            //Response.End();            
        }
        else
        {
            // we need to convert the default NT username string
            // into an user principal name format, which conviently look
            // like email addresses
            // you'll need to change this to match your environment
            // depending on how you can systematically map from your
            // AD users to their salesforce.com username
            // TODO: change domain part of UPN
            userid.Value = CurrentSessions.UserID;
            pageid.Value = Request.QueryString["pageid"];
            
            token.Value = SingleSignOn.CreateToken(userid.Value);
            Response.Write(token.Value);
            SingleSignOn aaa = new SingleSignOn();
            Response.Write("Authenticating.....");
            if (aaa.Authenticate(userid.Value, token.Value, Request.UserHostAddress))
            {
                Response.Write("Passed.....<br>");
                Response.Write("Loading.....<br>");
            }
            else
            {
                Response.Write("User session ended please login again to get access.....<br>");
            }

            postUrl = ConfigurationManager.AppSettings["CMS_ACCESS_URL"];    
        }*/
    }   
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
</body>
</html>

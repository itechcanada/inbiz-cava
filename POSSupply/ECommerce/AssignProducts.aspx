﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="AssignProducts.aspx.cs" Inherits="ECommerce_AssignProducts" %>
<%@ Import Namespace="System.Configuration" %>
<asp:Content ID="Content4" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <asp:Literal runat="server" ID="lblHeading"></asp:Literal></h1>
    </div>
    <div style="float: left; padding-top: 12px;">
       
    </div>
    <div style="float: right;">
        
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeft" runat="Server">
    <asp:Panel runat="server" ID="SearchPanel">
        <div class="searchBar">
            <div class="header">
                <div class="title">
                    Search form</div>
                <div class="icon">
                    <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
            </div>
            <h4>
                <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>" CssClass="filter-key"></asp:Label></h4>
            <div class="inner">
                <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
            </div>
            <div class="footer">
                <div class="submit">
                    <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch %>" />
                </div>
            </div>
        </div>
        <br />
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">   
    <div class="divMainContent">     
        <h3>
            <asp:Label ID="lblSectionTitle" Text="<%$ Resources:Resource, lnkInvProductAssociation%>" runat="server" />
        </h3>   
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="lblCategory" runat="server" Text="<%$ Resources:Resource, Category%>">
                    </asp:Label>
                </div>
                <div class="input" style="width:350px;">
                    <asp:DropDownList ID="dlCatName" CssClass="modernized_select" 
                        AutoPostBack="true" Width="270px" runat="server" 
                        onselectedindexchanged="dlCatName_SelectedIndexChanged">
                        <asp:ListItem Text="" />
                    </asp:DropDownList>
                    <span class="style1">*</span>                    
                </div>
                <div class="clearBoth"></div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblsubCategory" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, SubCategory%>"></asp:Label>
                </div>
                <div class="input" style="width:350px;">
                    <asp:DropDownList ID="dlSubcatName" CssClass="modernized_select" 
                        AutoPostBack="true" Width="270px" runat="server" 
                        onselectedindexchanged="dlSubcatName_SelectedIndexChanged">
                        <asp:ListItem Text="" />
                    </asp:DropDownList>
                    <span class="style1">*</span>                    
                </div>
                <div class="clearBoth"></div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label runat="server" CssClass="lblBold" ID="lblProduct" Text="<%$ Resources:Resource, SelectProduct%>"></asp:Label>
                </div>
                <div class="input" style="width:550px;">
                    <asp:ListBox ID="lstProducts" CssClass="modernized_select" SelectionMode="Multiple" runat="server" Width="400px">
                    </asp:ListBox>
                </div>
                <div class="clearBoth"></div>
            </li>
        </ul>
        <div class="div_command">            
            <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" 
                onclick="btnSave_Click" />
            <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" runat="server" CausesValidation="false" />
        </div>
    </div>      
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    
</asp:Content>


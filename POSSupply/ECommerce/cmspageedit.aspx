﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="cmspageedit.aspx.cs" Inherits="ECommerce_cmspageedit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <link href="lib/css/slider.css" rel="stylesheet" type="text/css" />
    <script src="lib/scripts/slides.min.jquery.js" type="text/javascript"></script>
    <style type="text/css">            
           .cms{border-top:1px dashed #000; border-bottom:1px dashed #000; cursor:pointer; min-height:10px; margin:5px;}
           .cms_hover{border-top:1px dashed red; border-bottom:1px dashed red;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            E-Commerce CMS
        </h1>
        <b>
            Manage CMS Page
        </b>
    </div>  
    <div style="float: left; padding-top: 12px;">
        <p>
           <b> Note* Click on respective page to edit content.</b>
        </p>
        <asp:Repeater ID="rptPages" DataSourceID="sdsPages" runat="server">
            <HeaderTemplate>
                <table border="0" cellpadding="0" cellspacing="0">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="width: 15px;">
                        <%#Container.ItemIndex + 1 %>.
                    </td>
                    <td>
                        <asp:HyperLink ID="Button1" CssClass="newWindow" Text='<%#Eval("MenuTextEn") %>' NavigateUrl='<%#string.Format("cmsaccess.aspx?pageid={0}", Eval("PageID"))   %>'
                            settings="fullscreen=yes,scrollbars=yes" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height:10px;"></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>            
        </asp:Repeater>      
        <%--<a href="http://www.google.co.in/" class="newWindow" settings="fullscreen=yes,scrollbars=yes">Test</a>  --%>
        <asp:SqlDataSource ID="sdsPages" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand="SELECT p.PageID, p.TemplateID, p.PageName, p.PageTitleEn, p.PageTitleFr, p.IsActive, p.ShowInMenu, 
            p.MenuTextEn, p.MenuTextFr, p.ParentID, p.SortOrder, p.LastModifiedDate, p.LastModifiedBy, t.TemplateName, 
            t.TemplatePath FROM cms_pages p 
            INNER JOIN cms_templates t ON t.TemplateID = p.TemplateID 
            INNER JOIN cms_website w ON w.WebSiteID=p.WebSiteID  
            WHERE p.WebSiteID = 1 AND w.IsActive = 1 AND p.IsActive = 1">
        </asp:SqlDataSource>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" Runat="Server">
    <asp:PlaceHolder ID="phContent" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        $(".cms_html_content").live("click", function (e) {
            e.preventDefault();
            $this = $(this);
            var contentid = $(this).attr('contentid');
            var placeholderid = $(this).attr('placeholderid');
            var placeholdereleid = $(this).attr('id');
            var $dialog = jQuery.FrameDialog.create({ url: 'htmleditor.aspx?contentid=' + contentid + '&pageid=<%=this.PageID %>&placeholderid=' + placeholderid + '&placeholdereleid=' + placeholdereleid,
                title: $this.attr("title"),
                loadingClass: "loading-image",
                modal: true,
                width: 800,
                height: 510,
                autoOpen: false,
                close: function (ev, ui) {
                    $(this).dialog('destroy');
                }
            });
            $dialog.dialog('open'); return false;
        });


        $(".cms_media_flash").live("click", function (e) {
            e.preventDefault();
            $this = $(this);
            var contentid = $(this).attr('contentid');
            var placeholderid = $(this).attr('placeholderid');
            var placeholdereleid = $(this).attr('id');
            var $dialog = jQuery.FrameDialog.create({ url: 'flashmediaedit.aspx?contentid=' + contentid + '&pageid=<%=this.PageID %>&placeholderid=' + placeholderid + '&placeholdereleid=' + placeholdereleid,
                title: $this.attr("title"),
                loadingClass: "loading-image",
                modal: true,
                width: 800,
                height: 510,
                autoOpen: false,
                close: function (ev, ui) {
                    $(this).dialog('destroy');
                }
            });
            $dialog.dialog('open'); return false;
        });


        $(".cms_rotating_banner").live("click", function (e) {
            e.preventDefault();
            $this = $(this);
            var placeholderid = $(this).attr('placeholderid');
            var placeholdereleid = $(this).children().attr('id');
            var $dialog = jQuery.FrameDialog.create({ url: 'adbanneredit.aspx?pageid=<%=this.PageID %>&placeholderid=' + placeholderid + '&placeholdereleid=' + placeholdereleid,
                title: $this.attr("title"),
                loadingClass: "loading-image",
                modal: true,
                width: 800,
                height: 510,
                autoOpen: false,
                close: function (ev, ui) {
                    $(this).dialog('destroy');
                }
            });
            $dialog.dialog('open'); return false;
        });
    </script>

     <script type="text/javascript">
         $(".cms").hover(function () {             
             $(this).toggleClass("cms_hover");
         }, function () {
             $(this).toggleClass("cms_hover");
         });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.newWindow').click(function (event) {
                var url = $(this).attr("href");
                var windowName = 'InibizCMS4.0';
                var settins = $(this).attr("settings");

                window.open(url, 'CMS', settins);                

                event.preventDefault();
            });           
        });
    </script>
</asp:Content>


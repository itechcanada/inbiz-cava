﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="SubCategoryView.aspx.cs" Inherits="ECommerce_SubCategoryView" %>
<%@ Import Namespace="System.Configuration" %>
<asp:Content ID="Content4" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <asp:Literal runat="server" ID="lblHeading"></asp:Literal></h1>
    </div>
    <div style="float: left; padding-top: 12px;">
        <asp:Button ID="btnAddCategory" runat="server" Text="Add Sub Category" />
        <iCtrl:IframeDialog ID="mdCreateCurrency" TriggerControlID="btnAddCategory" Width="650"
            Height="400" Url="SubCategoryEdit.aspx?jscallback=reloadGrid" Title="Add Sub Category" runat="server">
        </iCtrl:IframeDialog>
    </div>
    <div style="float: right;">
        
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeft" runat="Server">
    <asp:Panel runat="server" ID="SearchPanel">
        <div class="searchBar">
            <div class="header">
                <div class="title">
                    Search form</div>
                <div class="icon">
                    <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
            </div>
            <h4>
                <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>" CssClass="filter-key"></asp:Label></h4>
            <div class="inner">
                <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
            </div>
            <div class="footer">
                <div class="submit">
                    <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch %>" />
                </div>
            </div>
        </div>
        <br />
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">   
    <div class="divMainContent">        
        <asp:Label ID="lblMsg" runat="server" Font-Bold="true" ForeColor="green"></asp:Label>
        <div id="grid_wrapper" style="width: 100%;">
            <trirand:JQGrid runat="server" ID="jqgCategories" DataSourceID="sqlsdAppAdmin"
                Height="300px" AutoWidth="True" 
                ondatarequesting="jqgCategories_DataRequesting" 
                oncellbinding="jqgCategories_CellBinding">
                <Columns>   
                    <trirand:JQGridColumn DataField="subcatId" PrimaryKey="true" Visible="false"></trirand:JQGridColumn>
                    <trirand:JQGridColumn DataField="catname" HeaderText="<%$ Resources:Resource, Category %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="subcatName" HeaderText="<%$ Resources:Resource, SubCategory %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="eBayCatgNo" HeaderText="<%$ Resources:Resource, eBayCatgNo %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="UserName" HeaderText="<%$ Resources:Resource, UpdatedBy %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="subcatUpdatedOn" HeaderText="<%$ Resources:Resource, UpdatedOn %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="subcatImagePath" HeaderText="<%$ Resources:Resource, hdrImage %>"
                        Editable="false" TextAlign="Center" >                            
                    </trirand:JQGridColumn>
                    <trirand:JQGridColumn DataField="subcatWebSeq" HeaderText="<%$ Resources:Resource, hdrWebSequence %>"
                        Editable="false" TextAlign="Center" />
                    <trirand:JQGridColumn DataField="subcatIsActive" HeaderText="<%$ Resources:Resource, hdrStatus %>"
                        Editable="false" TextAlign="Center" >                           
                    </trirand:JQGridColumn>
                    <trirand:JQGridColumn DataField="subcatId" HeaderText="<%$ Resources:Resource, grdEdit %>"
                        Editable="false" TextAlign="Center">                           
                    </trirand:JQGridColumn>
                    <trirand:JQGridColumn DataField="subcatId" HeaderText="<%$ Resources:Resource, grdDelete %>"
                        Editable="false" TextAlign="Center" >                            
                    </trirand:JQGridColumn>
                </Columns>
                <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                    ShowDeleteButton="false" ShowSearchButton="false" />
                <SortSettings InitialSortColumn=""></SortSettings>
                <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="resize_the_grid" />
            </trirand:JQGrid>            
            <asp:SqlDataSource ID="sqlsdAppAdmin" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
        </div>
    </div>  
    <iCtrl:IframeDialog ID="mdEdit" Width="650" Height="400" Title="Edit Sub Category" Dragable="true"
        TriggerSelectorClass="edit_pop" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
        runat="server">
    </iCtrl:IframeDialog>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=jqgCategories.ClientID %>";
        var searchPanelID = "<%=SearchPanel.ClientID %>";


        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);        

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function editSequence(source, key) {
            //alert(key);
            var grid = $("#<%=jqgCategories.ClientID %>");            
            var dataToPost = {};
            dataToPost["editSeq"] = 1;
            dataToPost["editRowKey"] = key;
            dataToPost["seqVal"] = $(source).val();

            for (var k in dataToPost) {
                grid.setPostDataItem(k, dataToPost[k]);
            }
            grid.trigger("reloadGrid");

            for (var k in dataToPost) {
                grid.removePostDataItem(k);
            }
        }

        function deleteCategory(key) {
            if (confirm("Do you want to delete category?")) {
                var grid = $("#<%=jqgCategories.ClientID %>");
                var dataToPost = {};
                dataToPost["deleteCat"] = 1;
                dataToPost["deleteRowKey"] = key;

                for (var k in dataToPost) {
                    grid.setPostDataItem(k, dataToPost[k]);
                }
                grid.trigger("reloadGrid");

                for (var k in dataToPost) {
                    grid.removePostDataItem(k);
                }
            }
        }
    </script>
</asp:Content>


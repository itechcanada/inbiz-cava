﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="CategoryEdit.aspx.cs" Inherits="ECommerce_CategoryEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <style type="text/css">
        ul.form li div.lbl{width:150px !important;}        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div style="padding: 5px 10px;">
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="lblSelectLang" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, SelectLanguage%>"></asp:Label>
                </div>
                <div class="input">
                    <asp:RadioButtonList ID="rblLanguage" runat="server" RepeatDirection="Horizontal"
                        AutoPostBack="true" Enabled="false" 
                        onselectedindexchanged="rblLanguage_SelectedIndexChanged">
                        <asp:ListItem Text="<%$ Resources:Resource, English%>" Value="en" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, French%>" Value="fr"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, Spanish%>" Value="sp"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="clearBoth"></div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblCatName" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, CategoryName%>"></asp:Label>
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCatName" runat="server" MaxLength="100" Width="330px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqvalCatName" ControlToValidate="txtCatName" runat="server"
                                    ErrorMessage="<%$ Resources:Resource, msgPlzEntrCatName%>" Display="None" />
                </div>
                <div class="clearBoth"></div>
            </li>
            
            <li>
                <div class="lbl">
                    <asp:Label ID="lblSelCat" Text="<%$ Resources:Resource, lblInvCategory%>" runat="server"
                                    CssClass="lblBold" />
                </div>
                <div class="input">
                    <asp:RadioButtonList ID="rblstCategory" runat="server" RepeatDirection="Horizontal"
                        AutoPostBack="true" 
                        onselectedindexchanged="rblstCategory_SelectedIndexChanged">
                        <asp:ListItem Text="<%$ Resources:Resource, lblInvDefault%>" Value="0" Selected="True">
                        </asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, lblInvFreeText%>" Value="1"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="clearBoth"></div>
            </li>
            <li id="liFreeText" runat="server" visible="false">
                <div class="lbl">
                    <asp:Label ID="lblFreeText" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblInvFreeText%>"></asp:Label>
                </div>
                <div class="input">
                    <asp:TextBox ID="txtFreeText" runat="server" TextMode="MultiLine" />
                </div>
                <div class="clearBoth"></div>
            </li>
        </ul>
        <h3>
            <asp:Label ID="lblCommonfor" Text="Common for each language" runat="server" />
        </h3>
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="lbleBayCatgNo" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, eBayCatgNo%>"></asp:Label>
                </div>
                <div class="input">
                    <asp:TextBox ID="txteBayCatgNo" runat="server" MaxLength="20" Width="150px"></asp:TextBox>
                </div>
                <div class="clearBoth"></div>
            </li>
            
            <li id="liWebSeq" runat="server" visible="false">
                <div class="lbl">
                    <asp:Label ID="lblWebSeq" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, WebSequence%>"></asp:Label>
                </div>
                <div class="input">
                    <asp:DropDownList ID="dlWebSeq" Width="100px" runat="server">
                                </asp:DropDownList>
                </div>
                <div class="clearBoth"></div>
            </li>            
            <li>
                 <div class="lbl">
                    <asp:Label ID="Label1" runat="server" CssClass="lblBold" Text="Is Custom Category"></asp:Label>
                </div>
                <div class="input">
                    <asp:CheckBox ID="chkIsCustom" Text="" runat="server" />
                </div>
                <div class="clearBoth"></div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="Label2" runat="server" CssClass="lblBold" Text="Is Starter Category"></asp:Label>
                </div>
                <div class="input">
                    <asp:CheckBox ID="chkIsStarterCategory" Text="" runat="server" />
                </div>
                <div class="clearBoth"></div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblIsActive" CssClass="lblBold" runat="server" Text="<%$ Resources:Resource, IsCategoryActive%>"></asp:Label>
                </div>
                <div class="input">
                    <asp:RadioButton ID="rbYes" GroupName="rbIsActive" Checked="true" runat="server"
                                    Text="Yes" />&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:RadioButton ID="rbNo" GroupName="rbIsActive" runat="server" Text="No" TextAlign="Right" />
                </div>
                <div class="clearBoth"></div>
            </li>
            <li id="liImageSelector" runat="server">
                <div class="lbl">
                    <asp:Label ID="lblCatImagePath" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblImage%>"></asp:Label>
                </div>
                <div class="input">
                    <asp:FileUpload ID="fileCatImagePath" runat="server" />                    
                </div>
                <div class="clearBoth"></div>
            </li>
            <li id="liImage" runat="server" visible="false">
                <div class="lbl">
                    <asp:Label ID="lblCatImageUrl" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblImage%>"></asp:Label>
                </div>
                <div class="input">
                    <a runat="server" id="aTag" target="_blank">
                        <asp:Image ID="imgCat" ImageUrl="" runat="server" Width="120px" />
                    </a> &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:ImageButton ID="imgCatImageUrl" runat="server" 
                        ImageUrl="~/Images/delete_icon.png" onclick="imgCatImageUrl_Click" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
    </div>
    <div class="div-dialog-command" style="border-top:1px solid #ccc; padding:5px;">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" 
            onclick="btnSave_Click" />    
        <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
            CausesValidation="False" 
            OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />    
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


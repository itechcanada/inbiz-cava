﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.InbizERP.ECommerce;
using iTECH.Library.Utilities;

public partial class ECommerce_cmspageedit : BasePage
{
    private Control _contentControl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }

        //switch (this.PageID)
        //{
        //    case 1:
        //        LoadUserControl("~/CMS_Templates/HomeLayout.ascx");
        //        break;
        //    case 2:
        //        LoadUserControl("~/CMS_Templates/CommonContent.ascx");
        //        break;
        //}
    }

    private void FillPageContent()
    {    
        //CmsPage page = new CmsPage();
        //page.PopulateObject(this.PageID);
        //if (page.PageID > 0)
        //{
        //    LoadUserControl(page.TemplatePath);
        //}
    }

    private void LoadUserControl(string controlPath)
    {
        _contentControl = Page.LoadControl(controlPath);
        phContent.Controls.Clear();
        phContent.Controls.Add(_contentControl);
        _contentControl.ID = "ctlContent";
        IContentControl ctrl = (IContentControl)_contentControl;
        ctrl.Initialize();

        phContent.Visible = true;
    }

    public int PageID {
        get {
            return BusinessUtility.GetInt(Request.QueryString["pageid"]);
        }
    }    
}
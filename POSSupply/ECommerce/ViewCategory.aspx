<%@ Page Language="VB" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="false" CodeFile="ViewCategory.aspx.vb" Inherits="Inventory_ViewCategory" %>

<asp:Content ID="Content4" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <asp:Literal runat="server" ID="lblHeading"></asp:Literal></h1>
    </div>
    <div style="float: left; padding-top: 12px;">
        
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif" CssClass="csv_export" />
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeft" runat="Server">
    <asp:Panel runat="server" ID="SearchPanel">
        <div class="searchBar">
            <div class="header">
                <div class="title">
                    Search form</div>
                <div class="icon">
                    <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
            </div>
            <h4>
                <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
            <div class="inner">
                <asp:TextBox runat="server" Width="180px" ID="txtSearch" CssClass="filter-key"></asp:TextBox>
            </div>
            <div class="footer">
                <div class="submit">
                    <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch %>" />
                </div>
            </div>
        </div>
        <br />
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">   
    <div class="divMainContent">        
        <asp:Label ID="lblMsg" runat="server" Font-Bold="true" ForeColor="green"></asp:Label>
        <div id="grid_wrapper" style="width: 100%;">
            <trirand:JQGrid runat="server" ID="jqgCategories" DataSourceID="sqlsdAppAdmin"
                Height="300px" AutoWidth="True">
                <Columns>
                    <trirand:JQGridColumn DataField="catWebSeq" HeaderText="<%$ Resources:Resource, hdrWebSequence %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="catName" HeaderText="<%$ Resources:Resource, Category %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="eBayCatgNo" HeaderText="<%$ Resources:Resource, eBayCatgNo %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="UserName" HeaderText="<%$ Resources:Resource, UpdatedBy %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="catUpdatedOn" HeaderText="<%$ Resources:Resource, UpdatedOn %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="catImagePath" HeaderText="<%$ Resources:Resource, hdrImage %>"
                        Editable="false" >
                            <Formatter>
                                <trirand:CustomFormatter FormatFunction="formatCatImage" UnFormatFunction="unFormatCatImage" />
                            </Formatter>
                    </trirand:JQGridColumn>
                    <trirand:JQGridColumn DataField="catIsActive" HeaderText="<%$ Resources:Resource, hdrStatus %>"
                        Editable="false" >
                            <Formatter>
                                <trirand:CustomFormatter FormatFunction="formatIsActive" UnFormatFunction="unFormatIsActive" />
                            </Formatter>
                    </trirand:JQGridColumn>
                    <%--<trirand:JQGridColumn DataField="catId" HeaderText="<%$ Resources:Resource, grdEdit %>"
                        Editable="false" >
                            <Formatter>
                                <trirand:CustomFormatter FormatFunction="formatEdit" UnFormatFunction="unFormatEdit" />
                            </Formatter>
                    </trirand:JQGridColumn>--%>
                    <trirand:JQGridColumn DataField="catId" HeaderText="<%$ Resources:Resource, grdDelete %>"
                        Editable="false" >
                            <Formatter>
                                <trirand:CustomFormatter FormatFunction="formatDelete" UnFormatFunction="unFormatDelete" />
                            </Formatter>
                    </trirand:JQGridColumn>
                    <%--
                                       
                                                                               
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, hdrImage%>" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-ForeColor="white">
                                            <ItemTemplate>
                                                <a href='<% = ConfigurationManager.AppSettings("RelativePathForCategory") %><%# Container.DataItem("catImagePath")%>'
                                                    id="aTag" target="_blank">
                                                    <img src='../Thumbnail.ashx?p=<% = ConfigurationManager.AppSettings("RelativePathForCategory") %><%# Container.DataItem("catImagePath")%>&Cat=Cat' />
                                                </a>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="center" Width="40px" ForeColor="white" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:ImageField SortExpression="catIsActive" HeaderStyle-ForeColor="white" HeaderText="<%$ Resources:Resource, hdrStatus%>"
                                            DataImageUrlField="catIsActive" DataImageUrlFormatString="~/Images/{0}">
                                            <ItemStyle HorizontalAlign="center" Width="40px" VerticalAlign="Middle" />
                                        </asp:ImageField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, grdEdit%>" HeaderStyle-ForeColor="white"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("catId") %>'
                                                    CommandName="Edit" ImageUrl="~/images/edit_icon.png" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="40px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete%>" HeaderStyle-ForeColor="white"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                                    CommandArgument='<%# Eval("catId") %>' CommandName="Delete" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="40px" />
                                        </asp:TemplateField>

                    <trirand:JQGridColumn DataField="userid" HeaderText="" PrimaryKey="True" Visible="false" />
                    <trirand:JQGridColumn DataField="userName" HeaderText="<%$ Resources:Resource, grdUserName %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="userLoginId" HeaderText="<%$ Resources:Resource, grdUserLoginId %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="userModules" HeaderText="<%$ Resources:Resource, grdAssociatedModules %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="userEmail" HeaderText="<%$ Resources:Resource, grdEmail %>"
                        Editable="false" />
                    <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="userid"
                        Sortable="false" TextAlign="Center" Width="50" />
                    <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="userid"
                        Sortable="false" TextAlign="Center" Width="50">
                        <Formatter>
                            <trirand:CustomFormatter FormatFunction="formatDeleteLink" UnFormatFunction="unformatDeleteLink" />
                        </Formatter>
                    </trirand:JQGridColumn>--%>
                </Columns>
                <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                    ShowDeleteButton="false" ShowSearchButton="false" />
                <SortSettings InitialSortColumn=""></SortSettings>
                <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" />
            </trirand:JQGrid>            
            <asp:SqlDataSource ID="sqlsdAppAdmin" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
        </div>
    </div>  
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=jqgCategories.ClientID %>";
        var searchPanelID = "<%=SearchPanel.ClientID %>";


        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        setTimeout(resize_the_grid, 10);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function formatCatImage(cellValue, options, rowObject) {      
            var href = '<% = ConfigurationManager.AppSettings("RelativePathForCategory") %>' + cellValue;
            var cell_html = "<a href='" + href + "' originalValue='" + cellValue + "'  target='_blank'>";
            cell_html += "<img src='../Thumbnail.ashx?p=" + href + "&Cat=Cat' /></a>";            
            return cell_html;
        }
        function unFormatCatImage(cellValue, options, cellObject) {
            return $(cellObject.html()).attr("originalValue");
        }

        function formatIsActive(cellValue, options, rowObject) {
            var src = '<%=ResolveUrl("~/Images/") %>' + cellValue;
            var cell_html = "<img src='" + src + "' />";
            return cell_html;
        }
        function unFormatIsActive(cellValue, options, cellObject) {
            return $(cellObject.html()).attr("originalValue");
        }

        function formatDelete(cellValue, options, rowObject) {
            var lnk = "<a class='delete_user' href='delete.aspx?tbl=user&pk=" + cellValue + "' originalValue='" + cellValue + "' >Delete</a>";
            return lnk;
        }
        function unFormatDelete(cellValue, options, cellObject) {
            return $(cellObject.html()).attr("originalValue");
        }
    </script>
</asp:Content>




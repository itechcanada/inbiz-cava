Imports System
Imports System.IO
Imports System.Data.Odbc
Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Configuration.ConfigurationManager

Partial Class IN_Print
    Inherits BasePage
    Private sHtmlData As String = ""
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            subClose()
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("SOID") <> "" Then
                If Request.QueryString("DocTyp") = "QO" Or Request.QueryString("DocTyp") = "SO" Or Request.QueryString("DocTyp") = "IN" Then
                    subHTMLGen(Request.QueryString("SOID"), Request.QueryString("DocTyp"))
                End If
            End If
        End If
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    ' Sub routine to close popup
    Private Sub subClose()
        Response.Write("<script>window.opener.location.reload();</script>")
        Response.Write("<script>window.close();</script>")
    End Sub
    'Purchase Order
    Private Function funSOGen(ByVal strSOID As String) As String
        Return ""
    End Function
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)
        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + "<br />"
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + "<br />"
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + "<br />"
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + "<br />"
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + "<br />"
            Else
                strAddress += objAdr.AddressState + "<br />"
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + "<br />"
            Else
                strAddress += objAdr.AddressCountry + "<br />"
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function

    'Get Warehouse Address
    Protected Function funWarehouseAddress(ByVal strWhsCode As String)
        Dim strData As String = ""
        If strWhsCode <> "" Then
            Dim objWhs As New clsWarehouses
            objWhs.WarehouseCode = strWhsCode
            objWhs.getWarehouseInfo()
            If objWhs.WarehouseDescription <> "" Then
                strData += objWhs.WarehouseDescription + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseAddressLine1 <> "" Then
                strData += objWhs.WarehouseAddressLine1 + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseAddressLine2 <> "" Then
                strData += objWhs.WarehouseAddressLine2 + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseCity <> "" Then
                strData += objWhs.WarehouseCity + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseState <> "" Then
                strData += objWhs.WarehouseState + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseCountry <> "" Then
                strData += objWhs.WarehouseCountry + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehousePostalCode <> "" Then
                strData += objWhs.WarehousePostalCode
            End If
            objWhs = Nothing
        End If
        Return strData
    End Function
    Private Sub subHTMLGen(ByVal strReqID As String, ByVal strReqType As String)
        Dim objCompany As New clsCompanyInfo

        Dim objInvoice As New clsInvoices
        Dim objInvoiceItems As New clsInvoiceItems
        Dim objInvItemProcess As New clsInvItemProcess
        Dim objPrn As New clsPrint
        Dim objCom As New clsCommon
        If strReqID <> "" And strReqType <> "" Then
            objInvoice.InvID = strReqID
            objInvoice.getInvoicesInfo()
            objCompany.CompanyID = objInvoice.InvCompanyID
            'objCompany.CompanyName = objCompany.funGetFirstCompanyName
            objCompany.getCompanyInfo()
            Dim strAddress As String = objCompany.CompanyAddressLine1 & IIf(objCompany.CompanyAddressLine2 <> "", ", " & objCompany.CompanyAddressLine2, "") & Microsoft.VisualBasic.Chr(10) & IIf(objCompany.CompanyCity <> "", objCompany.CompanyCity, "") & IIf(objCompany.CompanyState <> "", ", " & objCompany.CompanyState, "") & IIf(objCompany.CompanyCountry <> "", ", " & objCompany.CompanyCountry, "") & IIf(objCompany.CompanyPostalCode <> "", ", " & objCompany.CompanyPostalCode, "")
            strAddress = IIf(strAddress <> "", strAddress, "-")
            Dim strTelFax As String = lblPhone & " " & IIf(objCompany.CompanyPOPhone <> "", objCompany.CompanyPOPhone, "-") & " | " & lblFax & " " & IIf(objCompany.CompanyFax <> "", objCompany.CompanyFax, "-")
            Dim strEmail As String = lblEmail & " " & objCompany.CompanyEmail
            'objInvoice.InvID = strReqID
            'objInvoice.getInvoicesInfo()

            Dim objUser As New clsUser
            objUser.UserID = objInvoice.InvLastUpdateBy
            objUser.getUserInfo()

            Dim strUseName As String = objUser.UserFirstName & " " & objUser.UserLastName

            Dim strTitle As String = ""
            If objInvoice.InvRefType = "CN" Then
                strTitle = prnCreditNote
            Else
                strTitle = prnInvoice
            End If

            Dim strCurrency As String = ""
            Dim strVendorInfo As String = ""
            strCurrency = objInvoice.InvCurrencyCode
            Dim objCust As New clsExtUser
            objCust.CustID = objInvoice.InvCustID
            objCust.CustType = objInvoice.InvCustType
            objCust.getCustomerInfoByType()
            Dim strCustName As String = objCust.CustName
            objCust = Nothing

            ' Bill To
            strVendorInfo += strCustName & "<BR>"
            strVendorInfo += Microsoft.VisualBasic.Chr(10)
            strVendorInfo += funPopulateAddress(objInvoice.InvCustType, "B", objInvoice.InvCustID)

            ' Shipping
            Dim strShippingInfo As String = ""
            strShippingInfo += strCustName & "<BR>"
            strShippingInfo += Microsoft.VisualBasic.Chr(10)
            strShippingInfo += funPopulateAddress(objInvoice.InvCustType, "S", objInvoice.InvCustID)

            Dim blnFlagIn As Boolean = False
            If objInvoiceItems.funGetDiscount(strReqID) Then
                blnFlagIn = True
            End If

            '//////////////////////////////////////////////////////
            sHtmlData += "<table width='800' border='1' align='center' cellpadding='0' cellspacing='0'>"
            sHtmlData += "<tr><td>"
            'Header Start
            sHtmlData += "<table width='800' border='0' align='center' cellspacing='0' cellpadding='5'>"
            sHtmlData += "<tr valign='middle'>"
            sHtmlData += "<td width='13%' rowspan='2' height=60 align='left'> "
            If objCompany.CompanyLogoPath <> "" Then
                sHtmlData += "<img id=img src=" & ConfigurationManager.AppSettings("CompanyLogoPath") & objCompany.CompanyLogoPath & " " & objCom.funImageSize(objCompany.CompanyLogoPath, "90", "80", "Comp") & "  />"
            End If
            sHtmlData += "</td>"
            sHtmlData += "<td width='62%' height=60 align='left'> "
            sHtmlData += "<b>" & objCompany.CompanyName & "</b><br /><br />"
            sHtmlData += strAddress & "<br />"
            sHtmlData += strTelFax & "<br />"
            sHtmlData += strEmail
            sHtmlData += "</td>"
            sHtmlData += "<td width='25%' height=60 align='Right'>"
            sHtmlData += "<H2>" & strTitle & "</H2>"
            sHtmlData += "</td>"
            sHtmlData += "</tr>"
            sHtmlData += "</table>"
            'Header Close

            'Line Start
            sHtmlData += "<table width='100%' cellpadding='0' cellspacing='0' style='border: 1px solid #666666; background: #FFFFFF;'><tr><td  width='100%' align='center'><img src='../images/spacer1.gif' width='800' style='border: 1px;background: #FFFFFF; background-color:#330066;'></td></tr></table>"
            'Line Close

            'Invoice Date/No/BillTO/ShipTo Start
            sHtmlData += "<table width='800' border='0' height='40' align='center' cellpadding='5' cellspacing='0' style='border: 0px solid #666666; background: #FFFFFF;'>"
            sHtmlData += "<tr><td>&nbsp;</td><td>"
            'Date/ No Start
            sHtmlData += "<table  border='1' width='40%' align='right' cellpadding='5'  cellspacing='0'>"
            sHtmlData += "<tr><td width='30%' align='left' height='20' >"
            sHtmlData += prnDate + ": <b>" + IIf(objInvoice.InvCreatedOn <> "", CDate(objInvoice.InvCreatedOn).ToString("MMM-dd yyyy"), "--")
            sHtmlData += "</b></td></tr>"

            sHtmlData += "<tr><td width='30%' align='left' height='20'>"
            sHtmlData += prnInvoiceNo + ": <b>" + objInvoice.InvRefNo
            sHtmlData += "</b></td></tr>"

            sHtmlData += "<tr><td width='30%' align='left' height='20'>"
            sHtmlData += prnCustPONo + ": <b>" + objInvoice.InvCustPO
            sHtmlData += "</b></td></tr></table>"
            'Date/No Close
            sHtmlData += "</td></tr>"

            sHtmlData += "<tr>"
            'BillTo Start
            sHtmlData += "<td>"

            sHtmlData += "<table align='center' cellpadding='5' cellspacing='0'>"
            sHtmlData += "<tr>"
            sHtmlData += "<td align='left'><font>" + prnBillTo + "</font></td>"
            sHtmlData += "</tr>"
            sHtmlData += "<tr>"
            sHtmlData += "<td align='left' width='375' height='100' style='border: 1px solid #666666; background: #FFFFFF;' valign=top>" + strVendorInfo + "</td>"
            sHtmlData += "</tr>"
            sHtmlData += "</table>"

            sHtmlData += "</td>"
            'BillTo Close
            'ShipTo Start
            sHtmlData += "<td>"

            sHtmlData += "<table align='center' cellpadding='5' cellspacing='0'>"
            sHtmlData += "<tr>"
            sHtmlData += "<td align='left'><font>" + prnShipTo + "</font></td>"
            sHtmlData += "</tr>"
            sHtmlData += "<tr>"
            sHtmlData += "<td align='left' width='375' height='100' style='border: 1px solid #666666; background: #FFFFFF;' valign=top>" + strShippingInfo + "</td>"
            sHtmlData += "</tr>"
            sHtmlData += "</table>"

            sHtmlData += "</td>"
            'ShipTo Close
            sHtmlData += "</tr>"
            sHtmlData += "</table>"
            'Invoice Date/No/BillTO/ShipTo Close
            Dim sStyle As String = "style='border: 1px solid #666666;'"
            'Invoice Items Start
            sHtmlData += "<br /><br />"
            sHtmlData += "<table border='1' align='center' width='99%' cellpadding='5' cellspacing='0'>"
            'Header Start
            sHtmlData += "<tr>"
            sHtmlData += "<td align='left' height='40'><b>" + grdPOUPCCode + "</b></td>"
            sHtmlData += "<td align='left' height='40'><b>" + POProduct + "</b></td>"
            sHtmlData += "<td align='left' height='40'><b>" + POTaxGrp + "</b></td>"
            If blnFlagIn = True Then
                sHtmlData += "<td align='right' height='40'><b>" + PODiscount + "</b></td>"
            End If
            sHtmlData += "<td align='right' height='40'><b>" + POQuantity + "</b></td>"
            sHtmlData += "<td align='right' height='40'><b>" + POUnitPrice + "</b></td>"
            sHtmlData += "<td align='right' height='40'><b>" + POAmount + "</b></td>"
            sHtmlData += "</tr>"
            'Header Close

            'Data Loop Start
            Dim drItems As OdbcDataReader
            drItems = objInvoiceItems.GetDataReader(objInvoiceItems.funFillGrid(strReqID))
            Dim dblSubTotal As Double = 0
            Dim dblTotal As Double = 0
            While drItems.Read
                sHtmlData += "<tr>"
                sHtmlData += "<td align='left' >" & drItems("prdUPCCode").ToString & "</td>"
                sHtmlData += "<td align='left' >" & drItems("prdName") & "</td>"
                Dim strGrpName As String
                If drItems("sysTaxCodeDescText").ToString <> "" Then
                    strGrpName = drItems("sysTaxCodeDescText").ToString
                Else
                    strGrpName = "--"
                End If
                sHtmlData += "<td align='left' >" & strGrpName & "</td>"
                If blnFlagIn = True Then
                    sHtmlData += "<td align='right' >" & drItems("invProductDiscount") & "</td>"
                End If
                sHtmlData += "<td align='right' >" & drItems("invProductQty") & "</td>"
                sHtmlData += "<td align='right' >" & String.Format("{0:F}", drItems("invProductUnitPrice")) & "</td>"
                sHtmlData += "<td align='right' >" & strCurrency & " " & String.Format("{0:F}", drItems("amount")) & "</td>"
                dblSubTotal += CDbl(drItems("amount"))
                sHtmlData += "</tr>"
            End While
            drItems.Close()
            'Data Loop Close

            sHtmlData += "</table>"
            sHtmlData += "<br /><br />"
            'Invoice Items Close

            'Invoice Process Start
            Dim drProcess As OdbcDataReader
            objInvItemProcess.Invoices_invID = strReqID
            drProcess = objInvItemProcess.GetDataReader(objInvItemProcess.funFillGridForProcess())
            Dim dblProcessTotal As Double = 0

            If drProcess.HasRows = True Then

                sHtmlData += "<table border='1' align='center' width='99%' cellpadding='5' cellspacing='0' bordercolor='#666666'>"
                'Header Start
                sHtmlData += "<tr>"
                sHtmlData += "<td align='left' height='40'><b>" + grdProcessDescription + "</b></td>"
                sHtmlData += "<td align='left' height='40'><b>" + grdProcessFixedCost + "</b></td>"
                sHtmlData += "<td align='left' height='40'><b>" + grdProcessCostPerHour + "</b></td>"
                sHtmlData += "<td align='right' height='40'><b>" + grdProcessCostPerUnit + "</b></td>"
                sHtmlData += "<td align='right' height='40'><b>" + prnTotalHour + "</b></td>"
                sHtmlData += "<td align='right' height='40'><b>" + prnTotalUnit + "</b></td>"
                sHtmlData += "<td align='right' height='40'><b>" + prnProcessCost + "</b></td>"
                sHtmlData += "</tr>"
                'Header Close

                While drProcess.Read
                    sHtmlData += "<tr>"
                    sHtmlData += "<td align='left' >" & drProcess("ProcessDescription") & "</td>"
                    sHtmlData += "<td align='left' >" & String.Format("{0:F}", drProcess("invItemProcFixedPrice")) & "</td>"
                    sHtmlData += "<td align='left' >" & String.Format("{0:F}", drProcess("invItemProcPricePerHour")) & "</td>"

                    sHtmlData += "<td align='right' >" & String.Format("{0:F}", drProcess("invItemProcPricePerUnit")) & "</td>"

                    sHtmlData += "<td align='right' >" & drProcess("invItemProcHours") & "</td>"
                    sHtmlData += "<td align='right' >" & drProcess("invItemProcUnits") & "</td>"
                    sHtmlData += "<td align='right' >" & strCurrency & " " & String.Format("{0:F}", drProcess("ProcessCost")) & "</td>"
                    dblProcessTotal += CDbl(drProcess("ProcessCost"))
                    sHtmlData += "</tr>"
                End While
                drProcess.Close()

                sHtmlData += "</table>"
                sHtmlData += "<br /><br />"
            End If
            'Invoice Process Close

            'Footer Start
            sHtmlData += "<table width='800' border='0'>"
            sHtmlData += "<tr>"

            'Ftr Left TD Start
            sHtmlData += "<td valign=top>"
            sHtmlData += "<table border='0' align='left' cellspacing='0' cellpadding='5'>"
            sHtmlData += "<tr>"
            sHtmlData += "<td width='75%' height=60 align=left>" & prnAuthorisedby & " " & strUseName & "</td>"
            sHtmlData += "</tr>"
            sHtmlData += "<tr>"
            sHtmlData += "<td width='25%' height=60 align=left>" & prnMemo & " " & objInvoice.InvComment & "</td>"
            sHtmlData += "</tr>"
            sHtmlData += "</table>"
            sHtmlData += "</td>"
            'Ftr Left TD Close

            'Ftr Right TD Start
            sHtmlData += "<td valign=top>"
            sHtmlData += "<table border='0' align='right' cellspacing='0' cellpadding='5'>"
            'Sub Total Start
            sHtmlData += "<tr>"
            sHtmlData += "<td align='right' width='75%'><b>" & prnSubTotal & "</b></td>"
            sHtmlData += "<td align='right' ><b>" & strCurrency & " " & String.Format("{0:F}", dblSubTotal) & "</b></td>"
            sHtmlData += "</tr>"
            'Sub Total Close

            'Tax Start
            Dim strTaxArray(,) As String
            Dim drSOItems As Data.Odbc.OdbcDataReader
            drSOItems = objInvoiceItems.GetDataReader(objInvoiceItems.funFillGrid(strReqID))

            While drSOItems.Read
                Dim strTax As ArrayList
                strTax = objPrn.funCalcTax(drSOItems("invProductID"), drSOItems("invShpWhsCode"), drSOItems("amount"), "", objInvoice.InvCustID, objInvoice.InvCustType, drSOItems("invProductTaxGrp").ToString)
                Dim strTaxItem As String
                Dim intI As Integer = 0
                While strTax.Count - 1 >= intI
                    strTaxItem = strTax.Item(intI).ToString
                    strTaxArray = objPrn.funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                    intI += 1
                End While
            End While

            Dim i As Integer = 0
            Dim j As Integer = 0
            If Not strTaxArray Is Nothing Then
                j = strTaxArray.Length / 2
            End If

            Dim dblTax As Double = 0

            While i < j
                sHtmlData += "<tr>"
                sHtmlData += "<td align='right' width='75%'>" & strTaxArray(0, i) & ": </td>"
                sHtmlData += "<td align='right' >" & strCurrency & " " & String.Format("{0:F}", strTaxArray(1, i))
                sHtmlData += "</td>"
                sHtmlData += "</tr>"
                dblTax += strTaxArray(1, i)
                i += 1
            End While
            'Tax Close


            sHtmlData += "<tr>"
            dblTotal = dblSubTotal + dblProcessTotal + dblTax
            sHtmlData += "<td align='right' width='75%'><b>" & prnTotal & "</b></td>"
            sHtmlData += "<td align='right' ><b>" & strCurrency & " " & String.Format("{0:F}", dblTotal)
            sHtmlData += "</b></td>"
            sHtmlData += "</tr>"
            sHtmlData += "</table>"
            sHtmlData += "</td>"
            'Ftr Right TD Close

            sHtmlData += "</tr>"
            sHtmlData += "</table>"
            'Footer Close

            sHtmlData += "<br /><br />"
            sHtmlData += "</td></tr>"
            sHtmlData += "</table>"

            objInvoice.CloseDatabaseConnection()
            objInvoice = Nothing
            objInvoiceItems.CloseDatabaseConnection()
            objInvoiceItems = Nothing
            objInvItemProcess.CloseDatabaseConnection()
            objInvItemProcess = Nothing

            lblData.ForeColor = Drawing.Color.Black
            lblData.Text = sHtmlData
        End If
    End Sub
End Class

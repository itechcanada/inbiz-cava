﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Invoice_BatchStatement : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper();
            try
            {
                SysCompanyInfo ci = new SysCompanyInfo();
                ci.FillCompany(dbHelp, dlstCompany, new ListItem(Resources.Resource.litComapnyName, ""));

                Partners part = new Partners();
                part.FillCustomers(dbHelp, ddlCustomers, new ListItem(""));
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }                       
        }
    }

    //protected void btnPrint_Click(object sender, EventArgs e)
    //{
    //    List<int> lstCustomers = new AccountReceivable().GetInvoiceCustomersByCompany(BusinessUtility.GetInt(dlstCompany.SelectedValue));
    //    if (lstCustomers.Count <= 0)
    //    {
    //        MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.PlzNoStatementFoundPrint);
    //        return;
    //    }

    //    int j = 0;
    //    List<int> lstToPrint;
    //    foreach (var custID in lstCustomers)
    //    {
    //        lstToPrint = new Invoice().GetBatchStatementInvoiceList(null, BusinessUtility.GetInt(dlstCompany.SelectedValue),
    //           custID, BusinessUtility.GetInt(dlstCreatedDate.SelectedValue), false, CurrentUser.IsSalesRistricted, CurrentUser.UserID);
    //        if (lstToPrint.Count <= 0)
    //        {
    //            continue;
    //        }

    //        Response.Write("<script>window.open('../AccountsReceivable/Statements.aspx?companyid=" + BusinessUtility.GetInt(dlstCompany.SelectedValue) + "&customerid=" + custID + "&days=" + BusinessUtility.GetInt(dlstCreatedDate.SelectedValue) + "','statement" + j + "','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');</script>");
    //        DateTime dt = DateTime.Now.AddSeconds(1);
    //        do
    //        {
    //            if (DateTime.Now > dt)
    //            {
    //                break;
    //            }
    //        } while (true);

    //        j++;
    //    }       
    //    if (j <= 0)
    //    {
    //        MessageState.SetGlobalMessage(MessageType.Information, Resources.Resource.PlzNoStatementFoundPrint);
    //    }
    //    else
    //    {
    //        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.PlzStatementPrintedSuccessfully);
    //    }
    //}

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        
        DbHelper dbHelp = new DbHelper(true);

        try
        {
            int custID = 0;
            if (ddlCustomers.SelectedIndex > 0)
            {
                int.TryParse(ddlCustomers.SelectedValue, out custID);
            }
            if (dlstCompany.SelectedIndex > 0 && dlstCreatedDate.SelectedIndex > 0)
            {
                Globals.RegisterScript(this, "window.open('../AccountsReceivable/Statements.aspx?companyid=" + BusinessUtility.GetInt(dlstCompany.SelectedValue) + "&days=" + dlstCreatedDate.SelectedValue + "&custID=" + custID + "','Statement','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');");
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.PlzStatementPrintedSuccessfully);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Information, Resources.Resource.PlzNoStatementFoundPrint);
            }
            

            //List<int> lstCustomers = new AccountReceivable().GetInvoiceCustomersByCompany(BusinessUtility.GetInt(dlstCompany.SelectedValue));
            //if (lstCustomers.Count <= 0)
            //{
            //    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.PlzNoStatementFoundPrint);
            //    return;
            //}
            //int j = 0;
            //List<int> lstToPrint;
            //foreach (var custID in lstCustomers)
            //{
            //    lstToPrint = new Invoice().GetBatchStatementInvoiceList(null, BusinessUtility.GetInt(dlstCompany.SelectedValue),
            //       custID, BusinessUtility.GetInt(dlstCreatedDate.SelectedValue), false, CurrentUser.IsSalesRistricted, CurrentUser.UserID);
            //    if (lstToPrint.Count <= 0)
            //    {
            //        continue;
            //    }

            //    Response.Write("<script>window.open('../AccountsReceivable/Statements.aspx?companyid=" + BusinessUtility.GetInt(dlstCompany.SelectedValue) + "&customerid=" + custID + "&days=" + BusinessUtility.GetInt(dlstCreatedDate.SelectedValue) + "','statement" + j + "','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');</script>");
            //    DateTime dt = DateTime.Now.AddSeconds(1);
            //    do
            //    {
            //        if (DateTime.Now > dt)
            //        {
            //            break;
            //        }
            //    } while (true);

            //    j++;
            //}
            //if (j <= 0)
            //{
            //    MessageState.SetGlobalMessage(MessageType.Information, Resources.Resource.PlzNoStatementFoundPrint);
            //}
            //else
            //{
            //    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.PlzStatementPrintedSuccessfully);
            //}
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }

        
    }
}
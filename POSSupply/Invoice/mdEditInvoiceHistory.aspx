﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdEditInvoiceHistory.aspx.cs" Inherits="Invoice_mdEditInvoiceHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div style="padding: 5px;">

        <trirand:JQGrid runat="server" ID="grdAccHst"
            AutoWidth="true" OnDataRequesting="grdAccHst_DataRequesting"
            OnCellBinding="grdAccHst_CellBinding">
            <Columns>
                <trirand:JQGridColumn DataField="AccHstID" HeaderText="ID" Width="50" Visible="false" />
                <trirand:JQGridColumn DataField="CreatedBy" HeaderText="<%$ Resources:Resource, lblCreatedBy %>" />
                <trirand:JQGridColumn DataField="CreatedDateTime" HeaderText="<%$ Resources:Resource, grdPOCreatedDate %>" />
                <trirand:JQGridColumn DataField="ItemUnitPrice" HeaderText="<%$ Resources:Resource, grdPOPrdCostPrice %>" DataFormatString="{0:f}" />
            </Columns>
            <PagerSettings PageSize="100" PageSizeOptions="[100,150,200]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn="" />
            <AppearanceSettings AlternateRowBackground="True" />
        </trirand:JQGrid>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
       
        
    </script>
</asp:Content>


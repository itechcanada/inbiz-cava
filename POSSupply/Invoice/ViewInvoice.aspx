﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ViewInvoice.aspx.cs" Inherits="Invoice_ViewInvoice" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="cntTop" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <%= Resources.Resource.lblView%>:
            <%= Resources.Resource.lblSalesOrder%>
        </b>
    </div>
    <div style="float: left; padding-top: 12px;">
       
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif" CssClass="csv_export" />        
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="cntMaster" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="grdInvoices" 
                    Height="300px" AutoWidth="True" OnCellBinding="grdInvoices_CellBinding" OnDataRequesting="grdInvoices_DataRequesting"> <%--DataSourceID="sdsInvoices"--%>
                    <Columns>
                        <trirand:JQGridColumn DataField="invID" HeaderText=""
                            PrimaryKey="True" Visible="false" />
                        <trirand:JQGridColumn DataField="InvRefNo" HeaderText="<%$ Resources:Resource, grdINInvoiceNo %>" />
                        <trirand:JQGridColumn DataField="invForOrderNo" HeaderText="<%$ Resources:Resource, grdINOrderNo %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="CustomerName" HeaderText="<%$ Resources:Resource, grdSOCustomerName %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="amount" HeaderText="<%$ Resources:Resource, POTotalAmount %>"
                            Editable="false" TextAlign="Right" />                                                  
                        <trirand:JQGridColumn DataField="invID" HeaderText="Amount Paid"
                            Editable="false" TextAlign="Right" />
                        <trirand:JQGridColumn DataField="invID" HeaderText="Total Balance"
                            Editable="false" TextAlign="Right" />                     
                        <trirand:JQGridColumn DataField="invDate" HeaderText="<%$ Resources:Resource, grdSOCreatedDate %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="invStatus" HeaderText="<%$ Resources:Resource, grdPOStatus %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="invCustPO" HeaderText="<%$ Resources:Resource, grdINCustPO %>"
                            Editable="false" Visible="false" />
                        <trirand:JQGridColumn DataField="invRefType" HeaderText="<%$ Resources:Resource, grdINRefType %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="orderTypeDesc" HeaderText="<%$ Resources:Resource, grduserOderType %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="InvoicePrintCounter" HeaderText="Print Count"
                            Editable="false" TextAlign="Center" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="invID"
                            Sortable="false" TextAlign="Center" Width="80" />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents LoadComplete="loadComplete" />
                </trirand:JQGrid>
                <%--<asp:SqlDataSource ID="sdsInvoices" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>--%>
            </div>
            <br />
            <div class="div_command">                                
                <asp:Button ID="btnBack" Text="<%$Resources:Resource, lblGoBack%>" 
                    CausesValidation="false" runat="server" onclick="btnBack_Click" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntLeft" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        Search form</div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"
                        AssociatedControlID="ddlSearch" CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlSearch" runat="server" Width="175px">
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lblStatus %>"
                        AssociatedControlID="ddlSOStatus" CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlSOStatus" runat="server" Width="175px">
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"
                        CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="Search" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $grid = $("#<%=grdInvoices.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=grdInvoices.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        function loadComplete(data) {
            if (data.rows.length == 1 && data.total == 1) {
                location.href = 'ViewInvoiceDetails.aspx?SOID=' + data.rows[0].id;
            }
            else {
                jqGridResize();
            }
        }
    </script>
</asp:Content>


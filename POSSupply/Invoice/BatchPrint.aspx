﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/fullWidth.master" AutoEventWireup="true"
    CodeFile="BatchPrint.aspx.cs" Inherits="Invoice_BatchPrint" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <h4>
        <%=Resources.Resource.lblBatchPrint%>
    </h4>
    <div style="text-align: center;">
        <table border="0" cellpadding="5" cellspacing="0" style="margin: auto; width: 570px;
            height: 267px;">
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblFromDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblRptFromDate %>">
                    </asp:Label>
                </td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtFromDate" CssClass="datepicker" Enabled="true" runat="server"
                        Width="90px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqvalFromDate" runat="server" ControlToValidate="txtFromDate"
                        ErrorMessage="<%$ Resources:Resource, msgRptPlzEntFromDate %>" SetFocusOnError="true"
                        Display="None">
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblToDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblRptToDate %>">
                    </asp:Label>
                </td>
                <td style="text-align: left">
                    <asp:TextBox ID="txtToDate" CssClass="datepicker" Enabled="true" runat="server" Width="90px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqvalToDate" runat="server" ControlToValidate="txtToDate"
                        ErrorMessage="<%$ Resources:Resource, msgRptPlzEntToDate %>" SetFocusOnError="true"
                        Display="None">
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdAlign">
                </td>
                <td>
                    <asp:RadioButtonList ID="rblstSrchByOrd" runat="server" RepeatDirection="horizontal">
                        <asp:ListItem Text="<%$ Resources:Resource, lblSrchByOrdCreationDate %>" Value="0"
                            Selected="true">
                        </asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, lblSrchByOrdShippedDate %>" Value="1">
                        </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr runat="server" id="trComapany">
                <td style="text-align: right">
                    <asp:Label ID="lblcompany" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblBaInvCompany %>">
                    </asp:Label>
                </td>
                <td style="text-align: left">
                    <asp:Label ID="txtCompany" runat="server" Width="200px" ReadOnly="true"></asp:Label>
                    <asp:DropDownList ID="dlstCompany" runat="server" Width="200px" Visible="false">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblSOStatus" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblInvSOStatus %>">
                    </asp:Label>
                </td>
                <td style="text-align: left">
                    <asp:DropDownList ID="dlstPOStatus" runat="server" Width="200px">
                        <asp:ListItem Value="" Text="<%$ Resources:Resource, liAll %>" />
                        <asp:ListItem Value="A" Text="<%$ Resources:Resource, liApproved %>" />
                        <asp:ListItem Value="P" Text="<%$ Resources:Resource, liInProcess %>" />
                        <asp:ListItem Value="S" Text="<%$ Resources:Resource, liShipped %>" />
                        <asp:ListItem Value="R" Text="<%$ Resources:Resource, liReadyForShipping %>" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="tdAlign">
                </td>
                <td>
                    <asp:RadioButtonList ID="rblstPrint" runat="server" RepeatDirection="horizontal">
                        <asp:ListItem Text="<%$ Resources:Resource, liBatchPrint %>" Value="0" Selected="true">
                        </asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, liliBatchReprint %>" Value="1">
                        </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td style="text-align: left">
                    <asp:Button ID="btnGenerate" Text="<%$Resources:Resource, btnPreviewInvoices%>" runat="server"
                        OnCommand="btnPreviewInvoices" CommandName="only_generate" />
                        
                         <%--OnCommand="btnPrint_Command" <%$Resources:Resource, btnPrint%>--%>
                         <%--<input type="button" value="Print 2 " onclick="printList();"/>--%>
                    <asp:Button ID="btnGenerateAndPrint" Text="<%$Resources:Resource, btnGenerateAndPrint%>"
                        runat="server" OnCommand="btnPrint_Command" CommandName="print_and_generate"
                        Visible="false" />
                </td>
            </tr>
            <asp:ValidationSummary ID="valsReport" runat="server" ShowMessageBox="true" ShowSummary="false" />
        </table>
    </div>
    <%--Added by mukesh 20130530--%>
    <%--//Added by mukesh 20130605--%>
    <div style="text-align:right"> 
    <asp:Button ID="btnPrint" Text="<%$Resources:Resource, btnPrint%> " runat="server"  OnClientClick="return printList();"
                         OnClick="btnPrint_Command" Visible = "false"/>
                         </div>
                         <%--//Added end--%>
    <div>
        <trirand:JQGrid runat="server" ID="grdCollection" DataSourceID="sdsCollection" Height="300px"
            AutoWidth="True" OnCellBinding="grdCollection_CellBinding" OnDataRequesting="grdCollection_DataRequesting" 
            MultiSelect="true" Width="137px" oninit="grdCollection_Init" 
            onprerender="grdCollection_PreRender" >
            <Columns>
                <trirand:JQGridColumn DataField="ordID" HeaderText="<%$ Resources:Resource, grdOrderNo %>"
                    PrimaryKey="True" />
                <trirand:JQGridColumn DataField="CustomerName" HeaderText="<%$ Resources:Resource, grdSOCustomerName %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="ordDate" HeaderText="<%$ Resources:Resource, grdOrderDate%>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="ordShpDate" HeaderText="<%$Resources:Resource,grdOrderShippedDate%>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="amtBeforeTax" HeaderText="<%$ Resources:Resource, grdAmountBeforeTaxes %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="TotalAmt" HeaderText="<%$ Resources:Resource, POTotalAmount %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="invID" HeaderText="<%$ Resources:Resource, grdInvoiceNo %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="invDate" HeaderText="<%$ Resources:Resource, grdInvoiceDate %>"
                    Editable="false" TextAlign="Center" />
                <trirand:JQGridColumn DataField="printcount" HeaderText="<%$ Resources:Resource, grdPrintCount%>"
                    Editable="false" TextAlign="Left" />
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
        </trirand:JQGrid>
        <asp:SqlDataSource ID="sdsCollection" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
    </div>
    <%--Added end--%>
    <asp:HiddenField  ID="hdnPrintOrderIDs" runat="server" />
</asp:Content>
<asp:Content ID="cntLeft" ContentPlaceHolderID="cphLeft" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" runat="Server">
    <script language="javascript" type="text/javascript">
        function funCompany(source, arguments) {
            if (document.getElementById('<%=dlstCompany.ClientID%>').value == 0) {
                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;
            }
        }

        //Added by mukesh 20130531

        $grid = $("#<%=grdCollection.ClientID%>");
        
        function printList() {
            var arrRooms = $grid.getGridParam('selarrrow');
            if (arrRooms.length <= 0) {
                alert("<%=Resources.Resource.msgJobSlectOneOrder%>");
                return false;
            }

            alert(arrRooms.join(","));
            $("#<%=hdnPrintOrderIDs.ClientID%>").val(arrRooms.join(","))
           
        }
//        Added end
    </script>
</asp:Content>



Imports System
Imports System.IO
Imports System.Data.Odbc
Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Configuration.ConfigurationManager
Partial Class Invoice_PrintYSR
    Inherits BasePage
    Private sHtmlData As String = ""
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            subClose()
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("SOID") <> "" Then
                If Request.QueryString("DocTyp") = "QO" Or Request.QueryString("DocTyp") = "SO" Or Request.QueryString("DocTyp") = "IN" Then
                    subHTMLGen(Request.QueryString("SOID"), Request.QueryString("DocTyp"), Request.QueryString("PageNo"))
                End If
            End If
        End If
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    ' Sub routine to close popup
    Private Sub subClose()
        Response.Write("<script>window.opener.location.reload();</script>")
        Response.Write("<script>window.close();</script>")
    End Sub
    'Purchase Order
    Private Function funSOGen(ByVal strSOID As String) As String
        Return ""
    End Function
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)
        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + "<br />"
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + "<br />"
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + "<br />"
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + "<br />"
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + "<br />"
            Else
                strAddress += objAdr.AddressState + "<br />"
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + "<br />"
            Else
                strAddress += objAdr.AddressCountry + "<br />"
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function
    'Get Warehouse Address
    Protected Function funWarehouseAddress(ByVal strWhsCode As String)
        Dim strData As String = ""
        If strWhsCode <> "" Then
            Dim objWhs As New clsWarehouses
            objWhs.WarehouseCode = strWhsCode
            objWhs.getWarehouseInfo()
            If objWhs.WarehouseDescription <> "" Then
                strData += objWhs.WarehouseDescription + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseAddressLine1 <> "" Then
                strData += objWhs.WarehouseAddressLine1 + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseAddressLine2 <> "" Then
                strData += objWhs.WarehouseAddressLine2 + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseCity <> "" Then
                strData += objWhs.WarehouseCity + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseState <> "" Then
                strData += objWhs.WarehouseState + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseCountry <> "" Then
                strData += objWhs.WarehouseCountry + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehousePostalCode <> "" Then
                strData += objWhs.WarehousePostalCode
            End If
            objWhs = Nothing
        End If
        Return strData
    End Function
    Private Sub subHTMLGen(ByVal strReqID As String, ByVal strReqType As String, ByVal strPage As String)
        Dim objCompany As New clsCompanyInfo
        Dim objInvoice As New clsInvoices
        Dim objInvoiceItems As New clsInvoiceItems
        Dim objInvItemProcess As New clsInvItemProcess
        Dim objPrn As New clsPrint
        Dim objCom As New clsCommon
        If strReqID <> "" And strReqType <> "" Then
            objInvoice.InvID = strReqID
            objInvoice.getInvoicesInfo()
            objCompany.CompanyID = objInvoice.InvCompanyID

            objCompany.getCompanyInfo()
            Dim strAddress As String = objCompany.CompanyAddressLine1 & IIf(objCompany.CompanyAddressLine2 <> "", ", " & objCompany.CompanyAddressLine2, "") & Microsoft.VisualBasic.Chr(10) & IIf(objCompany.CompanyCity <> "", objCompany.CompanyCity, "") & IIf(objCompany.CompanyState <> "", ", " & objCompany.CompanyState, "") & IIf(objCompany.CompanyCountry <> "", ", " & objCompany.CompanyCountry, "") & IIf(objCompany.CompanyPostalCode <> "", ", " & objCompany.CompanyPostalCode, "")
            strAddress = IIf(strAddress <> "", strAddress, "-")
            Dim strTelFax As String = lblPhone & " " & IIf(objCompany.CompanyPOPhone <> "", objCompany.CompanyPOPhone, "-") & " | " & lblFax & " " & IIf(objCompany.CompanyFax <> "", objCompany.CompanyFax, "-")
            Dim strEmail As String = lblEmail & " " & objCompany.CompanyEmail

            Dim objUser As New clsUser
            objUser.UserID = objInvoice.InvLastUpdateBy
            objUser.getUserInfo()

            Dim strUseName As String = objUser.UserFirstName & " " & objUser.UserLastName

            Dim strTitle As String = ""
            If objInvoice.InvRefType = "CN" Then
                strTitle = prnCreditNote
            Else
                strTitle = prnInvoice
            End If

            Dim strCurrency As String = ""
            Dim strVendorInfo As String = ""
            strCurrency = objInvoice.InvCurrencyCode
            Dim objCust As New clsExtUser
            objCust.CustID = objInvoice.InvCustID
            objCust.CustType = objInvoice.InvCustType
            objCust.getCustomerInfoByType()
            Dim strCustName As String = objCust.CustName

            If strPage = 1 Then
                Dim strCompanyStartingPage As String
                sHtmlData += "<div align='center'><b>" & objCompany.CompanyName & "</b><br />"
                sHtmlData += strAddress & "<br />"
                sHtmlData += strTelFax & "<br />"
                sHtmlData += strEmail & "</div>"
                If objCompany.CompanyID = "2" Then
                    sHtmlData += "<div align='left'><br/>" & InvoicePageForPuertoRico & "</div><br/><br/>"
                ElseIf objCompany.CompanyID = "1" Then
                    sHtmlData += "<div align='left'><br/>" & InvoicePageForYellowServer & "</div><br/><br/>"
                End If
            Else
                ' Bill To
                strVendorInfo += strCustName & "<BR>"
                strVendorInfo += Microsoft.VisualBasic.Chr(10)
                strVendorInfo += funPopulateAddress(objInvoice.InvCustType, "B", objInvoice.InvCustID)

                ' Shipping
                Dim strShippingInfo As String = ""
                strShippingInfo += strCustName & "<BR>"
                strShippingInfo += Microsoft.VisualBasic.Chr(10)
                strShippingInfo += funPopulateAddress(objInvoice.InvCustType, "S", objInvoice.InvCustID)

                Dim blnFlagIn As Boolean = False
                If objInvoiceItems.funGetDiscount(strReqID) Then
                    blnFlagIn = True
                End If


                Dim intContSize As Integer = 0
                ' Date
                sHtmlData += "<div style=""position: absolute; left: 390px; top:" & intContSize + 50 & "px; padding: 1em;font: normal 11px Tahoma, Arial;color: #494949;"">" + IIf(objInvoice.InvCreatedOn <> "", CDate(objInvoice.InvCreatedOn).ToString("MMM-dd yyyy"), "--") & "</div> "
                ' Invoice #
                sHtmlData += "<div style=""position: absolute; left: 615px; top:" & intContSize + 50 & "px; padding: 1em;font: normal 11px Tahoma, Arial;color: #494949;"">" & objInvoice.InvRefNo & "</div> "
                ' PO #
                sHtmlData += "<div style=""position: absolute; left: 385px; top:" & intContSize + 90 & "px; padding: 1em;font: normal 11px Tahoma, Arial;color: #494949;"">" & objInvoice.InvCustPO & "</div> "
                ' Account#
                sHtmlData += "<div style=""position: absolute; left: 615px; top:" & intContSize + 90 & "px; padding: 1em;font: normal 11px Tahoma, Arial;color: #494949;""></div> "
                ' Bill to
                sHtmlData += "<div style='position: absolute; left: 30px; top:" & intContSize + 180 & "px; padding: 1em;font: normal 11px Tahoma, Arial;color: #494949;text-align:left;'>" & strVendorInfo & "</div> "
                ' Shipped to
                sHtmlData += "<div style='position: absolute; left: 472px; top:" & intContSize + 180 & "px; padding: 1em;font: normal 11px Tahoma, Arial;color: #494949;text-align:left;'>" & strShippingInfo & "</div> "


                ' Product Detail
                sHtmlData += "<div style='position: absolute; left: 20px; top:" & intContSize + 345 & "px; padding: 1em;'>"
                sHtmlData += "<table border='0' align='center' width='720' cellpadding='5' cellspacing='0'>"
                'Data Loop Start
                Dim drItems As OdbcDataReader
                drItems = objInvoiceItems.GetDataReader(objInvoiceItems.funFillGrid(strReqID))
                Dim dblSubTotal As Double = 0
                Dim dblTotal As Double = 0
                Dim strTaxArray(,) As String
                While drItems.Read
                    sHtmlData += "<tr valign=top>"
                    sHtmlData += "<td align='left' width='9%' >" & drItems("invProductQty") & "</td>"
                    Dim strGrpName, strDiscount As String
                    If drItems("sysTaxCodeDescText").ToString <> "" Then
                        strGrpName = drItems("sysTaxCodeDescText").ToString
                    Else
                        strGrpName = "--"
                    End If
                    If blnFlagIn = True Then
                        strDiscount = drItems("invProductDiscount")
                    End If
                    Dim strTax As ArrayList
                    strTax = objPrn.funCalcTax(drItems("invProductID"), drItems("invShpWhsCode"), drItems("amount"), "", objInvoice.InvCustID, objInvoice.InvCustType, drItems("invProductTaxGrp").ToString)
                    Dim strTaxItem As String
                    Dim intI As Integer = 0
                    While strTax.Count - 1 >= intI
                        strTaxItem = strTax.Item(intI).ToString
                        strTaxArray = objPrn.funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                        intI += 1
                    End While
                    Dim i As Integer = 0
                    Dim j As Integer = 0
                    If Not strTaxArray Is Nothing Then
                        j = strTaxArray.Length / 2
                    End If
                    Dim dblTax As Double = 0
                    While i < j
                        dblTax += strTaxArray(1, i)
                        i += 1
                    End While
                    sHtmlData += "<td align='left' width='76%'>" & drItems("prdName") & "(" & drItems("prdUPCCode").ToString & ")</td>"
                    sHtmlData += "<td align='right' width='26%'>" & strCurrency & " " & String.Format("{0:F}", drItems("amount") + dblTax) & "</td>"
                    dblSubTotal += CDbl(drItems("amount") + dblTax)
                    sHtmlData += "</tr>"
                End While
                drItems.Close()
                'Data Loop Close

                sHtmlData += "</table>"
                sHtmlData += "</div> "

                ' 'Invoice Process Start
                Dim drProcess As OdbcDataReader
                objInvItemProcess.Invoices_invID = strReqID
                drProcess = objInvItemProcess.GetDataReader(objInvItemProcess.funFillGridForProcess())
                Dim dblProcessTotal As Double = 0

                If drProcess.HasRows = True Then
                    While drProcess.Read
                        dblProcessTotal += CDbl(drProcess("ProcessCost"))
                    End While
                    drProcess.Close()
                End If
                ' 'Invoice Process Close

                dblTotal = dblSubTotal + dblProcessTotal

                'Total Due
                sHtmlData += "<div style='position: absolute; left: 640px; top:" & intContSize + 649 & "px; padding: 1em;font: normal 11px Tahoma, Arial;color: #494949;'>" & strCurrency & " " & String.Format("{0:F}", dblTotal) & "</div> "
                'Invoice#
                sHtmlData += "<div style='position: absolute; left: 630px; top:" & intContSize + 784 & "px; padding: 1em;font: normal 11px Tahoma, Arial;color: #494949;'>" & objInvoice.InvRefNo & "</div> "
                'Account#
                sHtmlData += "<div style='position: absolute; left: 630px; top:" & intContSize + 824 & "px; padding: 1em;font: normal 11px Tahoma, Arial;color: #494949;'></div> "
                'Total
                sHtmlData += "<div style='position: absolute; left: 630px; top:" & intContSize + 1014 & "px; padding: 1em;font: normal 11px Tahoma, Arial;color: #494949;'>" & strCurrency & " " & String.Format("{0:F}", dblTotal) & "</div> "
                'Duplicate Invoice    
                If Request.QueryString("Dup") = "1" Then
                    sHtmlData += "<div style='position: absolute; left: 400px; top:" & intContSize + 1022 & "px; padding: 1em;font: normal 11px Tahoma, Arial;color: #494949;'>" & lblDuplicate & "</div> "
                End If
            End If

            'Invoice Items Close
            objInvoice.CloseDatabaseConnection()
            objInvoice = Nothing
            objInvoiceItems.CloseDatabaseConnection()
            objInvoiceItems = Nothing
            objInvItemProcess.CloseDatabaseConnection()
            objInvItemProcess = Nothing
            objCust = Nothing
            lblData.ForeColor = Drawing.Color.Black
            lblData.Text = sHtmlData
        End If
    End Sub

    Public Function yellowServer() As String
        Dim strData As String
        strData = " <P>Inceremente sus oportunidades por medio del poder de compra!</P>"
        strData += " <P>Gracias por su continuo interes en anunciar su negocio en Paginas Amarillas de Puerto Rico! Nuestro equipo trabaja arduamente para resolver una de las mas grandes </P>"
        strData += " <P>interrogantes&nbsp; para los peque�os y grandes negocios: Como ahorrar dinero en servicios y productos de acuerdo al tama�o de su empresa.</P>"
        strData += " <P>Entre mas grande sea la organizaci�n, mas grande la influencia y el poder de oferta seran. Por lo tanto, constantemente nos esforzamos en mejorar y expandir nuestra red </P>"
        strData += " <P>de compa�ias para brindarle a su negocio el poder de compra de nuestra comunidad de negocios en internet. El internet es la industria de mas rapido crecimiento en el </P>"
        strData += " <P>mundo y nos enfocamos en aprovechar las ventajas de las conexiones que las compa�ias pueden hacer por la web.</P>"
        strData += " <P>Esta guia de informaci�n describe los crecientes servicios a los que su compa��a puede acceder como parte de la comunidad Paginas Amarillas de Puerto Rico. Tambien </P>"
        strData += " <P>estan incluidos un codigo de acceso personalizado y su clave o password que le daran lo que usted necesita para comenzar a usar nuestro portal como una esencial </P>"
        strData += " <P>herramienta de negocios. Para acceder a su listado, visite nuestro sitio web en <A href='http://www.thepuertoricoyellowpages.com/'>www.thepuertoricoyellowpages.com</A>. En caso de alguna dificultad para navegar en nuestro </P>"
        strData += " <P>sitio web, nuestros especialistas de servicio al cliente estan listos para ayudarle.</P>"
        strData += " <P>Bienvenido a la comunidad de negocios en el internet de mas rapido crecimiento en el mundo.</P>"
        strData += " <P><BR>El equipo de Paginas Amarillas de Puerto Rico<BR></P>"
        Return strData
    End Function
End Class

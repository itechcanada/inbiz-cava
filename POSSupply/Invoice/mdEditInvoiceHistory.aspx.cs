﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Invoice_mdEditInvoiceHistory : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdAccHst))
        {

        }
    }
    protected void grdAccHst_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
    }

    protected void grdAccHst_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (this.InvItemID > 0)
        {
            InvoiceItems objInvItem = new InvoiceItems();
            grdAccHst.DataSource = objInvItem.GetInvoiceItemHistory(this.InvItemID);
            grdAccHst.DataBind();
        }

    }

    public int InvItemID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["InvItemID"], out id);
            return id;
        }
    }
}
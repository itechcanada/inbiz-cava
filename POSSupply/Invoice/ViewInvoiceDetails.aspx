﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="ViewInvoiceDetails.aspx.cs" Inherits="Invoice_ViewInvoiceDetails" %>

<%@ Register Src="../Controls/new/CustomFields.ascx" TagName="CustomFields" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <link href="../lib/scripts/collapsible-panel/collapsible_salesedit.css" rel="stylesheet"
        type="text/css" />
    <script src="../lib/scripts/collapsible-panel/jquery.collapsiblepanel.js" type="text/javascript"></script>
    <style type="text/css">
        .dialog li
        {
            line-height: 30px;
        }

        table.readonlye_info
        {
            margin-bottom: 5px;
        }

            table.readonlye_info td
            {
                padding: 2px 5px;
            }

                table.readonlye_info td.bold
                {
                    font-weight: bold;
                }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <asp:Literal ID="ltAdmin" Text="<%$Resources:Resource, lblAdministration%>" runat="server" /></h1>
        <b>
            <asp:Literal runat="server" ID="lblTitle" Text="<%$Resources:Resource, lblViewInvoiceDetails %>"></asp:Literal></b>
    </div>
    <div style="float: left; padding-top: 12px;">
    </div>
    <div style="float: right;">
        <%--<input type="button" value="Print Terms & Conditions"  onclick="window.open('terms.htm', 'Print', 'height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');"/>--%>
        <input type="button" id="btnPrintPOSGiftReceipt" value="<%=Resources.Resource.lblGiftReceipt%>"
            onclick="printPOSGiftReceipt();" />
        <input type="button" id="btnPrintPOSReceipt" value="<%=Resources.Resource.lblPrintPOSReceipt%>"
            onclick="printPOSReceipt();" />
        <span id="spSendXml" runat="server">
            <asp:Button ID="btnPostXml" Text="<%$Resources:Resource,lblPostXml %>" runat="server"
                CausesValidation="false" />
            <iCtrl:IframeDialog ID="mdPostXml" TriggerControlID="btnPostXml" Width="650" Height="460"
                Url="~/Invoice/mdPostInvoiceXml.aspx" Title="<%$Resources:Resource, lblPostXml %>"
                runat="server">
            </iCtrl:IframeDialog>
        </span><span id="spnFax" runat="server">
            <asp:LinkButton CssClass="inbiz_icon icon_fax_28" runat="server" ID="imgFax" />
            <iCtrl:IframeDialog ID="mdFax" TriggerControlID="imgFax" Width="550" Height="260"
                Url="~/Common/mdSendMailOrFax.aspx" Title="<%$Resources:Resource, lblPOFax %>"
                runat="server">
            </iCtrl:IframeDialog>
        </span><span id="spnMail" runat="server">
            <asp:LinkButton CssClass="inbiz_icon icon_email_28" runat="server" ID="imgMail" />
            <iCtrl:IframeDialog ID="mdMail" TriggerControlID="imgMail" Width="550" Height="260"
                Url="~/Common/mdSendMailOrFax.aspx" Title="<%$Resources:Resource, lblPOMail %>"
                runat="server">
            </iCtrl:IframeDialog>
        </span><span id="spnPrint" runat="server">
            <asp:LinkButton CssClass="inbiz_icon icon_print_28" runat="server" ID="imgPdf" CausesValidation="false" />
        </span>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <asp:Panel CssClass="panel" ID="ulSoDetails" runat="server">
        <h3>
            <asp:Literal runat="server" ID="ltTitle" Text="<%$Resources:Resource, lblViewInvoiceDetails %>"></asp:Literal>
        </h3>
        <div class="panelcontent">
            <table class="readonlye_info" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="bold">
                        <asp:Label ID="titleInvNo" Text="<%$ Resources:Resource, lblINInvoiceNo %>" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valInvNo" Text="" runat="server" />
                    </td>
                    <td class="bold">
                        <asp:Label ID="titleCompany" Text="<%$ Resources:Resource, lblSOPOComName %>" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valCompanyName" Text="" runat="server" />
                    </td>
                    <td class="bold">
                        <asp:Label ID="titleInvDate" Text="<%$ Resources:Resource, lblINInvoiceDate %>" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valInvDate" Text="" runat="server" />
                    </td>
                    <td class="bold">
                        <asp:Label ID="titleCustomerName" Text="<%$ Resources:Resource, lblSOCustomerName %>"
                            runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valCustomerName" Text="" runat="server" />
                        <a id="btnChangeCustomerName" runat="server" onclick="changeCustomer();" href="Javascript:void(0)">
                            <%=Resources.Resource.lblChangeCustomer%></a>
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblGrossProfit" Text="Gross Profit" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valGrossProfit" Text="" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="bold">
                        <asp:Label ID="titleInSoNo" Text="<%$ Resources:Resource, lblSOOrderNo %>" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valInSoNo" Text="" runat="server" />
                    </td>
                    <td class="bold">
                        <asp:Label ID="titleFax" Text="<%$ Resources:Resource, POFax %>" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valFax" Text="" runat="server" />
                    </td>
                    <td class="bold">
                        <asp:Label ID="titleWhsCode" Text="<%$ Resources:Resource, lblSOPOwhs %>" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valWhsCode" Text="" runat="server" />
                    </td>
                    <td class="bold">
                        <asp:Label ID="Label13" Text="<%$ Resources:Resource,lblSalesRep%>" runat="server" />
                    </td>
                    <td colspan="4">
                        <asp:ListBox ID="lbxSalesRepresentativs" SelectionMode="Multiple" runat="server"
                            Width="400px" CssClass="modernized_select"></asp:ListBox>
                    </td>
                </tr>
                <tr class="bold">
                    <td>
                        <asp:HyperLink ID="hlReturnHistory" NavigateUrl="" Text="" runat="server" Visible="true"
                            Font-Bold="true" />
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <div id="grid_wrapper" style="width: 100%;">
        <h3>
            <%=Resources.Resource.lblProducts%>
        </h3>
        <trirand:JQGrid runat="server" ID="grdProducts" Height="300px" PagerSettings-PageSize="50"
            AutoWidth="true" OnDataRequesting="grdProducts_DataRequesting" OnCellBinding="grdProducts_CellBinding">
            <Columns>
                <trirand:JQGridColumn DataField="ProductID" HeaderText="<%$ Resources:Resource, grdPoProductID %>" />
                <trirand:JQGridColumn DataField="UPCCode" HeaderText="<%$ Resources:Resource, grdPOUPCCode %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="ProductName" HeaderText="<%$ Resources:Resource, grdPOProductName %>" />
                <trirand:JQGridColumn DataField="ProductID" HeaderText="<%$ Resources:Resource, grdvPrdColor %>" />
                <trirand:JQGridColumn DataField="ProductID" HeaderText="<%$ Resources:Resource, lblProductSize %>" />
                <trirand:JQGridColumn DataField="Quantity" HeaderText="<%$ Resources:Resource, grdSOQuantity %>" />
                <trirand:JQGridColumn DataField="Price" HeaderText="<%$ Resources:Resource, grdPOPrdCostPrice %>">
                    <%--  <Formatter>
                        <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                            ThousandsSeparator="," />
                    </Formatter>--%>
                </trirand:JQGridColumn>
                <trirand:JQGridColumn DataField="TaxGrpDesc" HeaderText="Tax Group" />
                <trirand:JQGridColumn DataField="Discount" HeaderText="<%$ Resources:Resource, grdSaleDiscount %>" />
                <trirand:JQGridColumn DataField="WarehouseCode" HeaderText="<%$ Resources:Resource, grdPOWhsCode %>" />
                <trirand:JQGridColumn DataField="ItemTotal" HeaderText="<%$ Resources:Resource, POAmount %>">
                    <%-- <Formatter>
                        <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                            ThousandsSeparator="," />
                    </Formatter>--%>
                </trirand:JQGridColumn>
                <trirand:JQGridColumn DataField="ID" HeaderText="<%$ Resources:Resource, CmdCssEdit %>" Visible="false" PrimaryKey="true" />
                <trirand:JQGridColumn DataField="ID" HeaderText="<%$ Resources:Resource, lblHistory %>" Editable="false" Visible="true" />
            </Columns>
            <PagerSettings PageSize="100" PageSizeOptions="[100,150,200]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn="" />
            <AppearanceSettings AlternateRowBackground="True" />
            <ClientSideEvents LoadComplete="gridLoadComplete" />
        </trirand:JQGrid>
        <asp:SqlDataSource ID="sdsProducts" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
        <iCtrl:IframeDialog ID="mdEditProduct" Height="250" Width="500" Title="<%$ Resources:Resource, lblEditInvoceItem %>"
            Dragable="true" TriggerSelectorClass="edit_Product" TriggerSelectorMode="Class"
            UrlSelector="TriggerControlHrefAttribute" runat="server">
        </iCtrl:IframeDialog>
        <div>
            <table id="subTotalSummary1" border="0" cellpadding="0" cellspacing="0" style="float: right; font-weight: 700; text-align: right;">
            </table>
            <div style="clear: both;">
            </div>
        </div>
    </div>
    <div id="grid_wrapper1" style="width: 100%; margin-top: 15px;">
        <h3>Process Item(s)
        </h3>
        <trirand:JQGrid runat="server" ID="grdProcess" Height="150px" PagerSettings-PageSize="50"
            AutoWidth="true" OnCellBinding="grdProcess_CellBinding" OnDataRequesting="grdProcess_DataRequesting">
            <Columns>
                <trirand:JQGridColumn DataField="ProcessID" HeaderText="" PrimaryKey="true" Visible="false" />
                <trirand:JQGridColumn DataField="ProcessCode" HeaderText="<%$ Resources:Resource, grdProcessCode %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="ProcessDescription" HeaderText="<%$ Resources:Resource, grdProcessDescription %>" />
                <trirand:JQGridColumn DataField="TaxGrpName" HeaderText="<%$ Resources:Resource, grdTaxDescription %>" />
                <trirand:JQGridColumn DataField="ProcessFixedCost" HeaderText="<%$ Resources:Resource, grdProcessFixedCost %>">
                    <%--<Formatter>
                        <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                            ThousandsSeparator="," />
                    </Formatter>--%>
                </trirand:JQGridColumn>
                <trirand:JQGridColumn DataField="ProcessCostPerHour" HeaderText="<%$ Resources:Resource, grdProcessCostPerHour %>">
                    <%--<Formatter>
                        <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                            ThousandsSeparator="," />
                    </Formatter>--%>
                </trirand:JQGridColumn>
                <trirand:JQGridColumn DataField="ProcessCostPerUnit" HeaderText="<%$ Resources:Resource, grdProcessCostPerUnit %>">
                    <%--<Formatter>
                        <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                            ThousandsSeparator="," />
                    </Formatter>--%>
                </trirand:JQGridColumn>
                <trirand:JQGridColumn DataField="TotalHour" HeaderText="<%$ Resources:Resource, grdSOTotalHour %>" />
                <trirand:JQGridColumn DataField="TotalUnit" HeaderText="<%$ Resources:Resource, grdSOTotalUnit %>" />
                <trirand:JQGridColumn DataField="ProcessTotal" HeaderText="<%$ Resources:Resource, grdSOProcessCost %>">
                    <%-- <Formatter>
                        <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                            ThousandsSeparator="," />
                    </Formatter>--%>
                </trirand:JQGridColumn>
            </Columns>
            <PagerSettings PageSize="100" PageSizeOptions="[100,150,200]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn="" />
            <AppearanceSettings AlternateRowBackground="True" />
            <ClientSideEvents LoadComplete="gridLoadComplete1" />
        </trirand:JQGrid>
        <iCtrl:IframeDialog ID="IframeDialog2" Height="510" Width="800" Title="Edit Process Detail"
            Dragable="true" TriggerSelectorClass="edit_Process_Item" TriggerSelectorMode="Class"
            UrlSelector="TriggerControlHrefAttribute" runat="server">
        </iCtrl:IframeDialog>
        <div>
            <table id="subTotalSummary2" border="0" cellpadding="0" cellspacing="0" style="float: right; font-weight: 700; text-align: right;">
            </table>
            <div style="clear: both;">
            </div>
        </div>
    </div>
    <div>
        <table id="divTotalSummary" border="0" cellpadding="0" cellspacing="0" style="display: none; float: right; font-weight: 700; text-align: right; border-top: 1px solid #ccc;">
        </table>
        <div style="clear: both;">
        </div>
        <div id="divPayHistory" class="dialog" style="width: 400px;" title="Pay History">
        </div>
    </div>
    <div style="clear: both;">
    </div>
    <div class="panelcollapsed">
        <h3>
            <asp:Label CssClass="trigger_icon" ID="Label6" Text="<%$Resources:Resource,lblOtherDetails%>"
                runat="server" />
        </h3>
        <div class="panelcontent">
            <ul class="form">
                <li>
                    <div class="lbl">
                        <asp:Label ID="lbl6" runat="server" Text="<%$Resources:Resource, lblSOCustomer%>">
                        </asp:Label>
                    </div>
                    <div class="input">
                        <asp:Label ID="lblName" runat="server" Text="" Font-Bold="true">
                        </asp:Label>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblShpToAddress" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShpToAddress %>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:Label ID="txtShpToAddress" runat="server">
                        </asp:Label>
                        <br />
                        <asp:HyperLink ID="hlEditShippingAddress" CssClass="edit_address" NavigateUrl=""
                            runat="server" Text="[Edit Ship To Address]" />
                        <iCtrl:IframeDialog ID="mdAddress" TriggerSelectorMode="Class" TriggerSelectorClass="edit_address"
                            Width="900" Height="500" Title="<%$Resources:Resource, lblSOShpToAddress %>"
                            UrlSelector="TriggerControlHrefAttribute" runat="server">
                        </iCtrl:IframeDialog>
                        <%--<asp:RequiredFieldValidator ID="custValShpToAddress" runat="server" SetFocusOnError="true" ControlToValidate="txtShpToAddress"
                        ErrorMessage="<%$ Resources:Resource, msgSOPlzEntShpToAddress %>" 
                       Text="*">
                    </asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="lbl">
                        <asp:Label ID="lblBillToAddress" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOBillToAddress %>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:Label ID="txtBillToAddress" runat="server">
                        </asp:Label>
                        <br />
                        <asp:HyperLink ID="hlEditBillToAddress" CssClass="edit_address" NavigateUrl="" runat="server"
                            Text="[Edit Bill To Address]" ToolTip="<%$ Resources:Resource, lblSOBillToAddress %>" />
                        <%--<asp:RequiredFieldValidator ID="custvalBillToAddress" runat="server" SetFocusOnError="true" ControlToValidate="txtBillToAddress"
                        ErrorMessage="<%$ Resources:Resource, msgSOPlzEntBillToAddress %>" 
                        Text="*">
                    </asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <%--<li>
                <div class="lbl">
                    <asp:Label ID="lblTerms" CssClass="lblBold" Text="<%$ Resources:Resource, POShippingTerms%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtTerms" runat="server" TextMode="MultiLine" Rows="3">
                    </asp:TextBox>
                </div>
                <div class="lbl">
                    <asp:Label ID="lblNetTerms" CssClass="lblBold" Text="<%$ Resources:Resource, lblSONetTerms%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox MaxLength="4" ID="txtNetTerms" runat="server"></asp:TextBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>--%>
                <li style="display: none;">
                    <div class="lbl">
                        <asp:Label ID="lblResellerShipBlankPref" CssClass="lblBold" Text="<%$ Resources:Resource, lblResellerShipBlankPref%>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:RadioButtonList ID="rblstShipBlankPref" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="<%$ Resources:Resource, lblYes%>" Value="1">
                            </asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lblNo%>" Value="0" Selected="True">
                            </asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="lbl">
                        <asp:Label ID="lblISWeb" Text="<%$ Resources:Resource, lblSOSalesISWeb%>" CssClass="lblBold"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:RadioButtonList ID="rdlstISWeb" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1">
                            </asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0" Selected="True">
                            </asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="Label1" Text="<%$ Resources:Resource, lblSONetTerms%>" CssClass="lblBold"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:DropDownList ID="ddlNetTerms" runat="server" Enabled="true">
                        </asp:DropDownList>
                    </div>
                    <div class="lbl">
                        <asp:Label ID="Label2" Text="<%$ Resources:Resource, lblShipmentTrackingNo%>" CssClass="lblBold"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:Label ID="lblShpTrackNo" Text="" runat="server" />
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblShpWarehouse" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingWarehouse %>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:DropDownList ID="dlShpWarehouse" runat="server" Width="170px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="custvalShpWarehouse" runat="server" SetFocusOnError="true"
                            ErrorMessage="<%$ Resources:Resource, lblSOPlzSelShippingWarehouse %>" Text="*"
                            ControlToValidate="dlShpWarehouse" InitialValue="0">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="lbl">
                        <asp:Label ID="Label3" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOCustomerPO %>">
                        </asp:Label>
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtCustPO" runat="server" MaxLength="35" onkeypress="return disableEnterKey(event)"></asp:TextBox>
                    </div>
                    <%--<div class="lbl">
                    <asp:Label ID="lblShpTrackNo" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingTrackNo %>"
                        runat="server" Visible="False" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtShpTrackNo" runat="server" MaxLength="25" onkeypress="return disableEnterKey(event)"
                        Visible="False"></asp:TextBox>
                </div>--%>
                    <div class="clearBoth">
                    </div>
                </li>
                <%--<li>                
                <div class="lbl">
                    <asp:Label ID="lblShpDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingDate %>">
                    </asp:Label>
                </div>
                <div class="input">
                    <asp:TextBox ID="txtShippingDate" Enabled="true" runat="server" Width="90px" MaxLength="15"
                        onkeypress="return disableEnterKey(event)" CssClass="datepicker"></asp:TextBox>
                </div>
                <div id="divShpCode" runat="server" visible="false">
                    <div class="lbl">
                        <asp:Label ID="lblShpCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingCode %>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtShpCode" runat="server" MaxLength="15"></asp:TextBox>
                    </div>
                    <div class="clearBoth">
                    </div>
                </div>
                <div class="clearBoth">
                </div>
            </li>--%>
                <li>
                    <div class="lbl">
                        <asp:Label ID="Label4" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblStatus%>"></asp:Label>
                        *
                    </div>
                    <div class="input">
                        <asp:DropDownList ID="dlStatus" runat="server" Width="170px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="custvalStatus" runat="server" SetFocusOnError="true"
                            ControlToValidate="dlStatus" ErrorMessage="<%$ Resources:Resource, msgSOPlzSelStatus %>"
                            InitialValue="0">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="lbl">
                        <asp:Label ID="lblShpCost" Text="<%$ Resources:Resource, lblSOShippingCost %>" runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtShpCost" runat="server" MaxLength="6" onkeypress="return disableEnterKey(event)"
                            ReadOnly="true">
                        </asp:TextBox>
                        <asp:CompareValidator ID="comvalShpCost" EnableClientScript="true" SetFocusOnError="true"
                            ControlToValidate="txtShpCost" Operator="DataTypeCheck" Type="double" Display="None"
                            ErrorMessage="<%$ Resources:Resource, lblSOPlzEntNumericValueInShippingCost %>"
                            runat="server" />
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <%--<div class="lbl">
                    <asp:Label ID="lblShpCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingCode %>"
                        runat="server" Visible="False" />
                </div>
                <div class="input">   
                    <asp:TextBox ID="txtShpCode" runat="server" Width="150px" MaxLength="15" onkeypress="return disableEnterKey(event)"
                        Visible="False"></asp:TextBox>
                </div>--%>
                    <div class="lbl">
                        <asp:Label ID="lblNotes" CssClass="lblBold" Text="<%$ Resources:Resource, PONotes%>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Rows="3" Width="595px">
                        </asp:TextBox>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <uc1:CustomFields ID="CustomFields1" runat="server" />
    &nbsp;<div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnReceivePayment" Text="<%$Resources:Resource, cmdCssReceivePayment%>"
            runat="server" CausesValidation="False" OnClick="btnReceivePayment_Click" />
        <asp:Button ID="btnSaveAndGoToCal" Visible="false" Text="Save & Go To Calendar" runat="server"
            OnClick="btnSaveAndGoToCal_Click" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, btnCancel%>" runat="server"
            OnClick="btnCancel_Click" />
    </div>
    <asp:ValidationSummary ID="valsSales" runat="server" ShowMessageBox="true" ShowSummary="false" />
    <asp:HiddenField ID="hdnCompanyID" Value="" runat="server" />
    <asp:HiddenField ID="hdnPartnerID" Value="" runat="server" />
    <asp:HiddenField ID="hdnParterTypeID" Value="" runat="server" />
    <asp:HiddenField ID="hdnPartnerCurrencyCode" Value="" runat="server" />
    <asp:HiddenField ID="hdnInvoiceNoForOrder" Value="" runat="server" />
    <asp:HiddenField ID="hdnWhsCode" Value="" runat="server" />
    <asp:HiddenField ID="hdnCurrrencyExchangeRate" Value="" runat="server" />
    <asp:HiddenField ID="hdnInvRefNo" Value="" runat="server" />
    <asp:HiddenField ID="hdnInvRefType" Value="" runat="server" />
    <asp:HiddenField ID="hdnInvDate" Value="" runat="server" />
    <asp:HiddenField ID="hdnInvCommisionType" Value="" runat="server" />
    <asp:HiddenField ID="hdnInvStatus" Value="" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
    <ul id="orderMenu" class="menu inbiz-left-menu collapsible">
        <li class="active"><a href="#">Navigation</a>
            <ul style="display: block;">
                <li>
                    <asp:HyperLink ID="hlviewSalesOrders" CssClass="" NavigateUrl="ViewInvoice.aspx"
                        Text="<%$Resources:Resource, lblViewInvoice%>" runat="server" /></li>
            </ul>
        </li>
    </ul>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" runat="Server">
    <!--Functions For Order Items Grid-->
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdProducts.ClientID %>";

        //$("#grid_wrapper").hide();

        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            resize_the_grid();
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function gridLoadComplete(data) {
            /*var rCount = $('#' + gridID).getGridParam("reccount");            
            if (rCount > 0) {
            $("#grid_wrapper").show();
            resize_the_grid();
            }
            else {
            $("#grid_wrapper").hide();
            }*/
            showOrderTotalSummary();
            resize_the_grid();
        }
    </script>
    <!--Functions For Process Grid-->
    <script type="text/javascript">
        //Variables
        var gridProcessID = "<%=grdProcess.ClientID %>";

        $("#grid_wrapper1").hide();

        //Function To Resize the grid
        function resize_the_grid1() {
            $('#' + gridProcessID).fluidGrid({ example: '#grid_wrapper1', offset: -0 });
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid1();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid1);

        function reloadProcessGrid(event, ui) {
            $("#grid_wrapper1").show();
            $('#' + gridProcessID).trigger("reloadGrid");
            resize_the_grid1();
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function gridLoadComplete1(data) {
            var rCount = $('#' + gridProcessID).getGridParam("reccount");
            if (rCount > 0) {
                $("#grid_wrapper1").show();
                resize_the_grid1();
            }
            else {
                $("#grid_wrapper1").hide();
            }
            showOrderTotalSummary();
        }
    </script>
    <script type="text/javascript">
        //Show Total Summary
        function showOrderTotalSummary() {
            //$("#divTotalSummary");
            $.ajax({
                type: "POST",
                url: "ViewInvoiceDetails.aspx/GetCalculatedTotalHtml",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'invoiceid': <%=this.InvoiceID%>}",
                success: function (data) {
                    $("#divTotalSummary").show();
                    $("#subTotalSummary1").html(data.d[0]);
                    $("#subTotalSummary2").html(data.d[1]);
                    $("#divTotalSummary").html(data.d[2]);
                    $("#divPayHistory").html(data.d[3]);

                    $(".opener").click(function () {
                        $(".dialog").dialog("open");
                        return false;
                    });
                    $(".SetExchangeRate").attr("disabled", "disabled");
                },
                error: function (request, status, errorThrown) {
                    alert(status);
                }
            });
        }

        $(".dialog").dialog({
            autoOpen: false, modal: true, width: '500px'
        });

        $(document).ready(function () {
            if ($("#<%=hdnInvCommisionType.ClientID %>").val() == "5") {
                $("#btnPrintPOSReceipt").show();
                $("#btnPrintPOSGiftReceipt").show();
            }
            else {
                $("#btnPrintPOSReceipt").hide();
                $("#btnPrintPOSGiftReceipt").hide();
            }
        });
        $.extend({
            getParamValue: function (paramName) {
                parName = paramName.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
                var pattern = '[\\?&]' + paramName + '=([^&#]*)';
                var regex = new RegExp(pattern);
                var matches = regex.exec(window.location.href);
                if (matches == null) return '';
                else return decodeURIComponent(matches[1].replace(/\+/g, ' '));
            }
        });
        function pad(num, size) {
            var s = num + "";
            while (s.length < size) s = "0" + s;
            return s;
        }
        function printPOSReceipt() {
            var invNO = $.getParamValue('SOID');
            invNO = pad(invNO, 6);
            window.open('../POS/Print.aspx?status=1&TID=' + invNO, 'Printing Receipt', 'width=300,height=300,toolbar=0,menubar=0,location=0,scrollbars=0,resizable=0,directories=0,status=0');
        }

        function printPOSGiftReceipt() {
            var invNO = $.getParamValue('SOID');
            invNO = pad(invNO, 6);
            window.open('../POS/Print.aspx?status=1&TID=' + invNO + '&printGiftReceipt=1', 'Printing Receipt', 'width=300,height=300,toolbar=0,menubar=0,location=0,scrollbars=0,resizable=0,directories=0,status=0');
        }


        $(".SetExchangeRate").live('keyup', function (e) {
            var sExcRate = Number($("#txtExchangeRate").val()).toFixed(2);
            $("#<%=hdnCurrrencyExchangeRate.ClientID%>").val(sExcRate);
        });

        function setOrderAddress(addrID, ctlrID, addType) {
            var dataToPost = {};
            dataToPost.addrId = addrID;
            dataToPost.ordID = $("#<%=hdnInvoiceNoForOrder.ClientID%>").val();
            dataToPost.addType = addType;
            $.ajax({
                type: "POST",
                url: '../sales/ViewOrderDetails.aspx/SetCustomerOrderAddress',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(dataToPost),
                success: function (data) {
                    var addrObj = data.d;
                    $("#" + ctlrID).html(addrObj);
                },
                error: function (request, status, errorThrown) {
                    alert(status);
                    //                    masterUnblockContentArea(eleToBlock);
                }
            });
        }

        //Return History Popup
        $("#<%=hlReturnHistory.ClientID%>").live("click", function (e) {
            $this = $(this);
            var pageTitle = '';
            pageTitle = $("#<%=hlReturnHistory.ClientID%>").html();
            var $dialog = jQuery.FrameDialog.create({
                url: $this.attr("href"),
                //title: (pageTitle == "1") ? "<%=Resources.Resource.lblReturnTransfer%>" : ($(this).attr("title")) ? $(this).attr("title") : "<%=Resources.Resource.lblFullTransfer%>",
                title: pageTitle,
                loadingClass: "loading-image",
                modal: true,
                width: "800",
                height: "550",
                autoOpen: false
            });
            $dialog.dialog('open'); return false;

            return false;
        });

        function changeCustomer() {
            var $dialog = jQuery.FrameDialog.create({
                url: '../Sales/mdSearchCustomer.aspx?invokSrc=Invoice',
                title: "<%=Resources.Resource.lblSelectCustomer %>",
                //loadingClass: "loading-image",
                modal: true,
                width: 900,
                height: 460,
                autoOpen: false
            });
            $dialog.dialog('open');
            return false;
        }

        function setCurrentGuest(guestid) {
            var gid = {};
            custID = guestid;
            gid.guestid = guestid;
            $.ajax({
                type: "POST",
                url: "../POS/ajax.aspx/GetGuest",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(gid),
                success: function (data) {
                    _objClientGuest = data.d[0];
                    $("#<%=valCustomerName.ClientID %>").html(_objClientGuest.PartnerLongName);
                        $("#<%=hdnPartnerID.ClientID %>").val(_objClientGuest.PartnerID);
                    },
                    error: function (request, status, errorThrown) {
                        alert(status);
                    }
                });
            }

            function reloadParentPage() {
                location.reload(true);
            }

            function ShowInvoiceHistory(invItemID) {

                //Account Receivable  History Popup  href=""mdAccountHistory.aspx?AccountRcbID={0}&jscallback={1}""
                $this = $(this);
                var pageTitle = "<%=Resources.Resource.lblInvoiceItemHistory%>";
            var $dialog = jQuery.FrameDialog.create({
                url: "mdEditInvoiceHistory.aspx?InvItemID=" + invItemID,
                title: pageTitle,
                loadingClass: "loading-image",
                modal: true,
                width: "800",
                height: "350",
                autoOpen: false
            });
            $dialog.dialog('open'); return false;

            return false;
        }

    </script>
</asp:Content>

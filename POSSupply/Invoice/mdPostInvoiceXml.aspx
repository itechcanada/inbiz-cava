﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdPostInvoiceXml.aspx.cs" Inherits="Invoice_mdPostInvoiceXml" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <asp:Panel ID="pnlPostXml" runat="server">
        <div style="padding: 5px 10px;">
            <asp:Label ID="lblInvoiceXml" Text="<%$Resources:Resource, lblInvoiceXmlToPost%>" runat="server" Font-Bold="true" /><br />
            <asp:TextBox ID="txtXmlToPost" runat="server" Width="630px" Height="250px" TextMode="MultiLine" /><br />
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Required!" ControlToValidate="txtXmlToPost"
                runat="server" Display="Dynamic" />
            <asp:Label ID="Label1" Text="<%$Resources:Resource, lblPostURL%>" runat="server" Font-Bold="true" /><br />
            <asp:TextBox ID="txtUrlToPost" runat="server" Width="630px" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Required!" ControlToValidate="txtUrlToPost"
                runat="server" Display="Dynamic" />
            <asp:HiddenField ID="hdnxmlPostID" runat="server" />
        </div>
        <div class="div_command">
            <asp:Button ID="btnPost" Text="<%$Resources:Resource,lblPostXml%>" runat="server" OnClick="btnPost_Click" />
            <asp:Button ID="btnCancel" Text="<%$Resources:Resource, btnCancel%>" runat="server"
                CausesValidation="false" OnClientClick="jQuery.FrameDialog.closeDialog();" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlResp" runat="server" Visible="false">
        <div style="padding: 5px 10px;">
            <asp:Label ID="Label2" Text="<%$Resources:Resource, lblResult%>" runat="server" Font-Bold="true" /><br />
            <asp:TextBox ID="txtResult" runat="server" Width="630px" Height="250px" TextMode="MultiLine" /><br />            
        </div>
        <div class="div_command">            
            <asp:Button ID="Button2" Text="<%$Resources:Resource, lblClose%>" runat="server"
                CausesValidation="false" OnClientClick="jQuery.FrameDialog.closeDialog();" />
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Invoice_BatchPrint : BasePage
{
    //DbHelper dbHelp = new DbHelper(true);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsJqGridCall(grdCollection) && !IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.OpenDatabaseConnection();
                SysCompanyInfo ci = new SysCompanyInfo();

                ci.FillCompany(dbHelp, dlstCompany, new ListItem(Resources.Resource.litComapnyName, ""));
                //Added by mukesh 20130507
                dlstCompany.SelectedIndex = 1;
                txtCompany.Text = dlstCompany.SelectedItem.ToString();
                //Added end

                List<string> lstOrderStatusToGenerateInvoice = ci.GetValidOrderStatusToAllowInvoiceGeneration(dbHelp, CurrentUser.DefaultCompanyID);
                List<ListItem> itemsToRemove = new List<ListItem>();
                foreach (ListItem item in dlstPOStatus.Items)
                {
                    if (item.Value != "" && !lstOrderStatusToGenerateInvoice.Contains(item.Value))
                    {
                        itemsToRemove.Add(item);
                    }
                }
                foreach (var item in itemsToRemove)
                {
                    dlstPOStatus.Items.Remove(item);
                }

                //Added by mukesh 20130603
                Session["fdate"] = "";
                Session["tdate"] = "";
                Session["isReprint"] = "";
                Session["srchByOrder"] = "";
                Session["soStatus"] = "";
                //Added end
            }
            catch
            {

            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnPreviewInvoices(object sender, CommandEventArgs e)
    {
        Orders ord = new Orders();
        DateTime fDate = BusinessUtility.GetDateTime(txtFromDate.Text, DateFormat.MMddyyyy);
        DateTime tDate = BusinessUtility.GetDateTime(txtToDate.Text, DateFormat.MMddyyyy);
        tDate = tDate.AddHours(23).AddMinutes(59).AddSeconds(59);

        if (fDate == DateTime.MinValue)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgRptPlzEntFromDate);
            return;
        }
        if (tDate == DateTime.MinValue)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgRptPlzEntToDate);
            return;
        }
        if (fDate > tDate)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgRptPlzEntFromDateShouldLessThanToDate);
            return;
        }
        int comID = BusinessUtility.GetInt(dlstCompany.SelectedValue);

        DbHelper dbHelp = new DbHelper(true);
        try
        {
            dbHelp.OpenDatabaseConnection();
            bool isReprint = rblstPrint.SelectedValue == "1";
            bool srchByOrder = rblstSrchByOrd.SelectedValue == "1";
            //Added by mukesh 20130530
            Session["fdate"] = fDate;
            Session["tdate"] = tDate;
            Session["isReprint"] = isReprint;
            Session["srchByOrder"] = srchByOrder;
            Session["soStatus"] = dlstPOStatus.SelectedValue;
            btnPrint.Visible = true; //added by mukesh 20130605
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }


    }

    private int PrintBatch(DbHelper dbHelp, DataTable invoiceList, bool isReprint)//, int[] ordlst)
    {
        int[] ordlst = this.OrderIDs;
        int printCounter = 0;
        int invID = 0;
        int invRefID = 0;
        ItDocStatus doc;
        string fName;
        string fNameFullPath;
        if (!isReprint)
        {
            //Need To Crate Invoice & let proceed printing
            Invoice inv;
            Orders ord = new Orders();

            foreach (var item in ordlst)
            {
                ord.PopulateObject(dbHelp, item);
                bool isValid = new SysCompanyInfo().ValidateOrderStatusBeforeInvoiced(dbHelp, ord.OrdCompanyID, ord.OrdStatus);
                DataView dvRow = new DataView();
                dvRow = invoiceList.DefaultView;
                dvRow.RowFilter = "ordID =" + item;

                if (isValid)
                {

                    if (dvRow.ToTable().Rows.Count > 0)
                    {
                        invID = BusinessUtility.GetInt(dvRow.ToTable().Rows[0]["invID"]);
                        invRefID = BusinessUtility.GetInt(dvRow.ToTable().Rows[0]["InvRefNo"]);
                    }

                    inv = new Invoice();
                    //Set invoice id & set refno  
                    bool isInvoiceExist = inv.IsInvoiceExist(item, "IV");
                    if (!isInvoiceExist)
                    {
                        inv = this.CreateInvoice(dbHelp, ord);
                    }
                    else
                    {
                        inv = null;
                    }
                    if (inv != null)
                    {
                        invID = inv.InvID;
                        invRefID = inv.InvRefNo;
                        if (invID <= 0 || invRefID <= 0)
                        {
                            continue;
                        }
                        doc = new ItDocStatus();
                        doc.Update(dbHelp, invRefID.ToString(), CurrentUser.UserID, BusinessUtility.GetInt(dlstCompany.SelectedValue));
                        fName = string.Format("IN_{0}_{1:yyyyMMddhhmmss}.html", invRefID, DateTime.Now);
                        fNameFullPath = Server.MapPath("~/pdf/") + fName;
                        Response.Write("<script>window.open('../Common/Print.aspx?ReqID=" + invID + "&DocTyp=IN', '" + fName + "','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');</script>");
                    }
                    else
                    {
                        if (invRefID > 0)
                        {
                            doc = new ItDocStatus();
                            doc.Update(dbHelp, invRefID.ToString(), CurrentUser.UserID, BusinessUtility.GetInt(dlstCompany.SelectedValue));
                            fName = string.Format("IN_{0}_{1:yyyyMMddhhmmss}.html", invRefID, DateTime.Now);
                            fNameFullPath = Server.MapPath("~/pdf/") + fName;
                            Response.Write("<script>window.open('../Common/Print.aspx?ReqID=" + invID + "&DocTyp=IN', '" + fName + "','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');</script>");
                        }
                    }
                }
                else
                {
                    continue;
                }
            }
        }
        else
        {
            foreach (var item in ordlst)
            {
                DataView dvRow = new DataView();
                dvRow = invoiceList.DefaultView;
                dvRow.RowFilter = "ordID =" + item;

                foreach (DataRow item1 in dvRow.ToTable().Rows)
                {
                    invID = BusinessUtility.GetInt(item1["invID"]);
                    invRefID = BusinessUtility.GetInt(item1["InvRefNo"]);
                    doc = new ItDocStatus();
                    doc.Update(dbHelp, invRefID.ToString(), CurrentUser.UserID, BusinessUtility.GetInt(dlstCompany.SelectedValue));
                    fName = string.Format("IN_{0}_{1:yyyyMMddhhmmss}.html", invRefID, DateTime.Now);
                    fNameFullPath = Server.MapPath("~/pdf/") + fName;
                    Response.Write("<script>window.open('../Common/Print.aspx?ReqID=" + invID + "&DocTyp=IN', '" + fName + "','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');</script>");
                }
            }
        }
        printCounter++;
        return printCounter;
    }

    private Invoice CreateInvoice(DbHelper dbHelp, Orders ord)
    {
        try
        {
            //Need to update order status            
            //bool isValid = new SysCompanyInfo().ValidateOrderStatusBeforeInvoiced(dbHelp, ord.OrdCompanyID, ord.OrdStatus);
            //if (isValid)
            //{                
            //Generate Invoice
            Invoice objInv = new Invoice();
            bool isDistinctInvoice = ConfigurationManager.AppSettings["SeparateInvID"].ToLower() == "yes";
            objInv.PopulateObjectByOrder(dbHelp, ord.OrdID, InvoicesStatus.SUBMITTED, InvoiceReferenceType.INVOICE, CurrentUser.UserID);
            objInv.Insert(dbHelp, CurrentUser.UserID);
            if (objInv.InvID > 0)
            {
                if (objInv.InvRefNo == 0)
                {
                    objInv.UpdateInvoiceRefNo(dbHelp, objInv.InvID, objInv.InvCompanyID, objInv.InvRefType, isDistinctInvoice);
                }
                InvoiceItems invItems = new InvoiceItems();
                invItems.AddInvoiceItemsFromOrder(dbHelp, ord.OrdID, objInv.InvID);

                InvoiceItemProcess invProcess = new InvoiceItemProcess();
                invProcess.AddProcessInvoiceItemsForOrder(dbHelp, ord.OrdID, objInv.InvID);


                //Update account receivable
                PreAccountRcv arc = new PreAccountRcv();
                arc.MoveToActualAR(dbHelp, CurrentUser.UserID, ord.OrdID, objInv.InvID);

                //Once Invoice generated successfully just make a entry in sls Analysis table 
                new SlsAnalysis().RecordOnInvoiceGeneration(dbHelp, objInv.InvID);

                //Update Quantity In Inventory
                ProductQuantity pQty = new ProductQuantity();
                //pQty.UpdateProuctQuantityOnSalesProcess(orderID); //No need to update now because we are handling ths through database an on base of order staus 2012-12-20

                //Update order Status
                if (ord.OrdStatus == SOStatus.APPROVED)
                {
                    ord.OrdStatus = SOStatus.IN_PROGRESS;
                    ord.UpdateOrderStatus(dbHelp, ord.OrdID, SOStatus.IN_PROGRESS);
                }

                //Add to Batch Print
                ItDocStatus batchPrint = new ItDocStatus();
                batchPrint.DocCompanyID = BusinessUtility.GetInt(dlstCompany.SelectedValue);
                batchPrint.DocCtr = 1;
                batchPrint.DocNo = objInv.InvRefNo.ToString();
                batchPrint.DocPrintDateTime = DateTime.Now;
                batchPrint.DocPrintedBy = CurrentUser.UserID;
                batchPrint.DocType = "IV";

                if (batchPrint.Insert(dbHelp, CurrentUser.UserID))
                {
                    return objInv;
                    //Add invoice to print list
                    //invToPrint = string.Format("{0}^{1}", objInv.InvID, objInv.InvRefNo);
                    //lstToPrint.Add(invToPrint);
                }

            }
            return null;
            //}
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    //Added by mukesh 20130603 start
    protected void grdCollection_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 4)
        {
            e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
        }
        else if (e.ColumnIndex == 5)
        {
            e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
        }
    }

    protected void grdCollection_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        Orders ord = new Orders();
        DbHelper dbHelp = new DbHelper(true);

        try
        {
            dbHelp.OpenDatabaseConnection();
            if (!((Session["fdate"].ToString() == "") || (Session["tdate"].ToString() == "")))
            {
                DateTime fDate = Convert.ToDateTime(Session["fdate"]);
                DateTime tDate = Convert.ToDateTime(Session["tdate"]);
                bool isReprint = Convert.ToBoolean(Session["isReprint"]);
                bool srchByOrder = Convert.ToBoolean(Session["srchByOrder"]);
                string soStatus = Session["soStatus"].ToString();

                sdsCollection.SelectCommand = ord.GetSoBatchDetails(sdsCollection.SelectParameters, fDate, tDate, 5, soStatus, CurrentUser.UserID, CurrentUser.IsSalesRistricted, isReprint, new int[] { (int)OrderCommission.Reservation }, srchByOrder);
            }
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    protected void btnPrint_Command(object sender, EventArgs e)
    {

        DbHelper dbHelp = new DbHelper(true);
        Orders ord = new Orders();

        //if (!((Session["fdate"].ToString() == "") || (Session["tdate"].ToString() == "")))
        //{
        //    DateTime fDate = Convert.ToDateTime(Session["fdate"]);
        //    DateTime tDate = Convert.ToDateTime(Session["tdate"]);
        //    bool isReprint = Convert.ToBoolean(Session["isReprint"]);
        //    bool srchByOrder = Convert.ToBoolean(Session["srchByOrder"]);
        //    string soStatus = Session["soStatus"].ToString();

        //    DataTable dtOrders = ord.GetSoBatchDetailsDT(dbHelp, fDate, tDate, 5, soStatus, CurrentUser.UserID, CurrentUser.IsSalesRistricted, isReprint, new int[] { (int)OrderCommission.Reservation }, srchByOrder);
        //    int printCount = this.PrintBatch(dbHelp, dtOrders, isReprint);//, this.OrderIDs);    
        //}
    }

    protected void grdCollection_Init(object sender, EventArgs e)
    {
        if (Session["fdate"] != null)
        {
            if (!((Session["fdate"].ToString() == "") || (Session["tdate"].ToString() == "")))
            {

                bool isReprint = Convert.ToBoolean(Session["isReprint"]);


                if (!isReprint)
                {
                    grdCollection.Columns[6].Visible = false;
                    grdCollection.Columns[7].Visible = false;
                    grdCollection.Columns[8].Visible = false;
                }
                else
                {
                    grdCollection.Columns[6].Visible = true;
                    grdCollection.Columns[7].Visible = true;
                    grdCollection.Columns[8].Visible = true;
                }
            }
        }
    }
    protected void grdCollection_PreRender(object sender, EventArgs e)
    {
        if (!((Session["fdate"].ToString() == "") || (Session["tdate"].ToString() == "")))
        {

            bool isReprint = Convert.ToBoolean(Session["isReprint"]);


            if (!isReprint)
            {
                grdCollection.Columns[6].Visible = false;
                grdCollection.Columns[7].Visible = false;
                grdCollection.Columns[8].Visible = false;
            }
            else
            {
                grdCollection.Columns[6].Visible = true;
                grdCollection.Columns[7].Visible = true;
                grdCollection.Columns[8].Visible = true;
            }
        }
    }

    public int[] OrderIDs
    {
        get
        {
            string strArr = hdnPrintOrderIDs.Value;//Request.QueryString["oid"];
            if (!string.IsNullOrEmpty(strArr))
            {
                string[] arr = strArr.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                return Array.ConvertAll<string, int>(arr, a => BusinessUtility.GetInt(a));
            }
            return new List<int>().ToArray();
        }
    }
    //Added end
}


//private int PrintBatch(DbHelper dbHelp, DataTable invoiceList, bool isGenrateAndPrint, bool isReprint)
//   {
//       int invID;
//       int invRefID;
//       Invoice inv;
//       ItDocStatus doc;
//       string fName;
//       string fNameFullPath;
//       int printCounter = 0;
//       foreach (DataRow item in invoiceList.Rows)
//       {
//           invID = BusinessUtility.GetInt(item["invID"]);
//           invRefID = BusinessUtility.GetInt(item["InvRefNo"]);
//           inv = new Invoice();
//           //Added by mukesh 20130507
//           if (rblstPrint.SelectedValue == "1")
//           {
//               doc = new ItDocStatus();
//               doc.Update(dbHelp, invRefID.ToString(), CurrentUser.UserID, BusinessUtility.GetInt(dlstCompany.SelectedValue));
//               fName = string.Format("IN_{0}_{1:yyyyMMddhhmmss}.html", invRefID, DateTime.Now);
//               fNameFullPath = Server.MapPath("~/pdf/") + fName;
//               Response.Write("<script>window.open('../Common/Print.aspx?ReqID=" + invID + "&DocTyp=IN', '" + fName + "','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');</script>");
//           }
//           else
//           { //Added end 20130507
//               if (invID <= 0 || invRefID <= 0) // || inv.IsInovicePaid(dbHelp, invID)) //Commented by mukesh 20130426//if (invID <= 0 || invRefID <= 0 || inv.IsInovicePaid(dbHelp, invID))
//               {
//                   //Need To Crate Invoice & let proceed printing
//                   Orders ord = new Orders();
//                   ord.PopulateObject(dbHelp, BusinessUtility.GetInt(item["ordID"]));
//                   bool isValid = new SysCompanyInfo().ValidateOrderStatusBeforeInvoiced(dbHelp, ord.OrdCompanyID, ord.OrdStatus);
//                   if (!isReprint && isValid)
//                   {
//                       //Set invoice id & set refno                     
//                       inv = this.CreateInvoice(dbHelp, ord);
//                       if (inv != null)
//                       {
//                           invID = inv.InvID;
//                           invRefID = inv.InvRefNo;
//                       }
//                       if (invID <= 0 || invRefID <= 0)
//                       {
//                           continue;
//                       }

//                       //Added by mukesh 20130507
//                       if (isGenrateAndPrint)
//                       {
//                           if (rblstPrint.SelectedValue == "1") //Need to update document print counter
//                           {
//                               doc = new ItDocStatus();
//                               doc.Update(dbHelp, invRefID.ToString(), CurrentUser.UserID, BusinessUtility.GetInt(dlstCompany.SelectedValue));
//                           }
//                           fName = string.Format("IN_{0}_{1:yyyyMMddhhmmss}.html", invRefID, DateTime.Now);
//                           fNameFullPath = Server.MapPath("~/pdf/") + fName;
//                           Response.Write("<script>window.open('../Common/Print.aspx?ReqID=" + invID + "&DocTyp=IN', '" + fName + "','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');</script>");
//                       }
//                       //Added end
//                   }
//                   else
//                   {
//                       continue;
//                   }
//               }
//           } //Added by mukesh 20130507 closing of else
//           //Commented by mukesh 20130507 start
//           //if (isGenrateAndPrint)
//           //{
//           //    if (rblstPrint.SelectedValue == "1") //Need to update document print counter
//           //    {
//           //        doc = new ItDocStatus();
//           //        doc.Update(dbHelp, invRefID.ToString(), CurrentUser.UserID, BusinessUtility.GetInt(dlstCompany.SelectedValue));
//           //    }
//           //    fName = string.Format("IN_{0}_{1:yyyyMMddhhmmss}.html", invRefID, DateTime.Now);
//           //    fNameFullPath = Server.MapPath("~/pdf/") + fName;
//           //    Response.Write("<script>window.open('../Common/Print.aspx?ReqID=" + invID + "&DocTyp=IN', '" + fName + "','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');</script>");
//           //}
//           //commented end
//           printCounter++;
//           DateTime dt = DateTime.Now.AddSeconds(1);
//           do
//           {
//               if (DateTime.Now > dt)
//               {
//                   break;
//               }
//           } while (true);
//       }
//       return printCounter;
//   }
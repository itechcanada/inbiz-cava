﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Invoice_EditInvoice : BasePage
{
    protected static string sCurrencyBaseCode = "";

    protected void Page_Load(object sender, EventArgs e)
    {


        if (!CurrentUser.IsInRole(RoleID.ACCOUNT))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            SysCompanyInfo ci = new SysCompanyInfo();
            ci.PopulateObject(CurrentUser.DefaultCompanyID);
            sCurrencyBaseCode = ci.CompanyBasCur;
            if (this.UnitPrice != null)
            {
                txtPrice.Text = CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(this.UnitPrice.ToString().Replace(",", ".")));
            }
            txtPrice.Focus();
        }
    }

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                if (this.InvItmID > 0 && BusinessUtility.GetDouble(txtPrice.Text)>0)
                {
                    InvoiceItems objInvItem = new InvoiceItems();
                    objInvItem.AddInvoiceItemsHistory(this.InvItmID, BusinessUtility.GetDouble(txtPrice.Text));
                    if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                    {
                        Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                        
                    }
                    else
                    {
                        Globals.RegisterCloseDialogScript(Page);
                    }

                }
            }
            catch
            {

            }
        }
    }

    private int InvItmID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["InvItmID"]);
        }
    }
    private double UnitPrice
    {
        get
        {
            return BusinessUtility.GetDouble(Request.QueryString["UnitPrice"]);
        }
    }


    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }
    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="GenerateInvoice.aspx.vb" Inherits="Invoice_GenerateInvoice" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register src="../Controls/CommonSearch/InvoiceSearch.ascx" tagname="InvoiceSearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();
        $('#divMain2').corner();
        $('#divMainC2').corner();     
    </script>
     
       
    <uc1:InvoiceSearch ID="InvoiceSearch1" runat="server" />
     
       
</asp:Content>

<asp:Content ID="cntGenerateInvoice" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <%-- onkeypress="return disableEnterKey(event)"--%>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" colspan="3">
                    <asp:UpdatePanel ID="upnlMsg" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblMsg" runat="server" ForeColor="red" Font-Bold="true"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr height="90">
                <td class="tdAlign" width="40%">
                    <asp:Label ID="lblOrderNo" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOOrderNo %>"
                        runat="server" />
                </td>
                <td class="tdAlignLeft" width="20%">
                    <asp:TextBox ID="txtOrderNo" runat="server" Width="130px" MaxLength="5">
                    </asp:TextBox>
                </td>
                <td class="tdAlignLeft" width="40%">
                    <% If Session("UserModules").ToString.Contains("SAL") = True Or Session("UserModules").ToString.Contains("ADM") = True Then%>
                    <div class="buttonwrapper">
                        <a id="cmdAddNewOrder" runat="server" causesvalidation="false" class="ovalbutton"
                            onclick="cmdAddNewOrder_Click" href="#"><span class="ovalbutton" style="min-width: 120px;
                                text-align: center;">
                                <%=Resources.Resource.cmdCssAddNewOrder%></span></a></div>
                    <%--<asp:Button runat="server" ID="cmdAddNewOrder" CssClass="imgAddNewOrder"   CausesValidation="false" OnClick="cmdAddNewOrder_Click"  />--%>
                    <%End If%>
                    <div style="display: none;">
                        <asp:CheckBox ID="chkCreditNote" runat="server" Text="<%$ Resources:Resource, IsCreditNote %>">
                        </asp:CheckBox>&nbsp;</div>
                    <asp:RequiredFieldValidator ID="reqvalOrderNo" runat="server" ControlToValidate="txtOrderNo"
                        ErrorMessage="<%$ Resources:Resource, msgINPlzEntOrderNo %>" SetFocusOnError="true"
                        Display="None">
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr height="30">
                <td align="center" colspan="3" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                    <table width="100%">
                        <tr>
                            <td height="20" align="center" width="45%">
                            </td>
                            <td width="25%">
                                <div class="buttonwrapper">
                                    <a id="cmdsave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                        style="min-width: 120px; text-align: center;">
                                        <%=Resources.Resource.cmdCsssave%></span></a></div>
                            </td>
                            <td width="30%" align="left">
                                <div class="buttonwrapper">
                                    <a id="cmdReset" visible="false" runat="server" causesvalidation="false" class="ovalbutton"
                                        href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssReset%></span></a></div>
                            </td>
                        </tr>
                    </table>
                    <%--<asp:Button runat="server" ID="cmdSave" CssClass="imgSave"  />&nbsp;&nbsp;
            <asp:Button runat="server" ID="cmdReset" Visible="false"
            style="width:80px;height:26px;border-width:0px;background:url(../Images/Back.png);" 
            CausesValidation="false"  />--%>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsGenInvoice" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
    
    <script type="text/javascript" language="javascript">

        function doClick(buttonName, e) {
            //the purpose of this function is to allow the enter key to 
            //point to the correct button to click.
            var key;

            if (window.event)
                key = window.event.keyCode;     //IE
            else
                key = e.which;     //firefox

            if (key == 13) {
                //Get the button the user wants to have clicked
                var btn = document.getElementById(buttonName);
                if (btn != null) { //If we find the button click it
                    btn.click();
                    event.keyCode = 0
                }
            }
        }
    
    </script>
</asp:Content>

Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Partial Class Invoice_GenerateInvoice
    Inherits BasePage
    Private strSOID As String = ""
    Private objSO As New clsOrders

    Private otherConnectionString As String = String.Empty
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        lblTitle.Text = lblGenerateInvoice '"Generate Invoice"
        txtOrderNo.Attributes.Add("onKeyPress", "doClick('" + cmdSave.ClientID + "',event)")
        If (Request.QueryString("gnIn") = "Y") And (Request.QueryString("SOID") <> "") Then
            txtOrderNo.Text = Request.QueryString("SOID")
            txtOrderNo.Enabled = False
        End If
        If (Request.QueryString("ccn") = "Y") Then
            lblTitle.Text = lblCreateCreditNote '"Create Credit Note"
            chkCreditNote.Checked = True
            cmdAddNewOrder.Visible = False
        End If
        txtOrderNo.Focus()
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    'On Submit
    Protected Sub cmdSave_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.ServerClick
        If Not Page.IsValid Then
            'Exit Sub
        End If
        If txtOrderNo.Text = "" Then
            lblMsg.Text = msgINPlzEntOrderNo '"Please enter Order No."
            txtOrderNo.Focus()
            Exit Sub
        End If
        objSO.OrdID = txtOrderNo.Text
        objSO.getOrdersInfo()
        Dim rtnStatus As String = ""
        If objSO.OrdStatus = "A" Or objSO.OrdStatus = "P" Or objSO.OrdStatus = "D" Then '(chkCreditNote.Checked = True And objSO.OrdStatus = "D") Then
            If funInvoiceProcess(rtnStatus) = True Then
                If objSO.OrdStatus = "A" And chkCreditNote.Checked = False Then
                    objSO.OrdStatus = "P"
                    objSO.OrdLastUpdatedOn = Now.ToString("yyyy-MM-dd hh:mm:ss")
                    objSO.funUpdateOrderforGInvoice()
                    objSO.updateProductQuantityOnProcess()
                End If
                If chkCreditNote.Checked = False Then
                    Session.Add("Msg", InvoiceGeneratedSuccessfully)
                Else
                    Session.Add("Msg", CreditNoteGeneratedSuccessfully)
                End If
                Response.Redirect("ViewInvoice.aspx")
            Else
                Select Case rtnStatus
                    Case "1"
                        lblMsg.Text = InvoiceProcessError 'InvoiceCouldNotCreatedSuccessfully
                    Case "2"
                        lblMsg.Text = InvoiceProcessError 'InvoiceItemsCouldNotCreatedSuccessfully
                    Case "3"
                        lblMsg.Text = InvoiceProcessError 'InvoiceProcessCouldNotCreatedSuccessfully
                    Case "4"
                        lblMsg.Text = InvoiceAlreadyGenerated '"Invoice is already generated."
                    Case "5"
                        lblMsg.Text = CreditNoteAlreadyGenerated
                End Select
            End If
        Else
            lblMsg.Text = OrderStatusIsNotInProcess '"Order status is not ""In Process""."
        End If

    End Sub
    ' On Back
    Protected Sub cmdBack_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReset.ServerClick
        Response.Redirect("ViewInvoice.aspx")
    End Sub

    'Invoice Process 
    Private Function funInvoiceProcess(ByRef rtnStatus) As Boolean
        Dim bFlag As Boolean = False
        If txtOrderNo.Text <> "" Then
            objSO.OrdID = txtOrderNo.Text
            objSO.getOrdersInfo()
            Dim objInv As New clsInvoices
            subSetInvoiceData(objInv, objSO)
            If objInv.funCheckInvoiceForOrderNo = False Then
                If objInv.insertInvoices() = True Then
                    Dim strInvID As String
                    strInvID = objInv.funGetInvoiceID()
                    If objInv.InvRefNo = "0" Then
                        objInv.funUpdateInvRefNo(objInv.InvRefType, objInv.InvCompanyID, strInvID)
                    End If
                    If chkCreditNote.Checked = True Then
                        objInv.InvID = strInvID
                        objInv.funUpdateInvoiceForCreditNote()
                    End If
                    Dim objInvItem As New clsInvoiceItems
                    'Populate Invoice Item
                    bFlag = objInvItem.funPopulateInvoiceItem(txtOrderNo.Text, strInvID)
                    objInvItem = Nothing
                    If bFlag = False Then
                        rtnStatus = "2" 'Invoice Items Could not Created
                    End If
                    'Populate Invoice Item Process
                    Dim objINIP As New clsInvItemProcess
                    bFlag = objINIP.funPopulateInvoiceItemProcess(txtOrderNo.Text, strInvID)
                    objINIP = Nothing
                    If bFlag = False Then
                        rtnStatus = "3" 'Invoice Items Process Could not Created
                    End If
                Else
                    rtnStatus = "1" 'Invoice Could not Created
                End If
            Else
                If chkCreditNote.Checked = False Then
                    rtnStatus = "4" 'Invoice is already Created
                Else
                    rtnStatus = "5" 'Credit Note is already Created
                End If
            End If
        End If
        Return bFlag
    End Function
    'Populate Invoice Object
    Private Sub subSetInvoiceData(ByRef objIN As clsInvoices, ByRef objS As clsOrders)
        objIN.InvCustType = objS.OrdCustType
        objIN.InvCustID = objS.OrdCustID
        objIN.InvStatus = "S"
        objIN.InvComment = objS.OrdComment
        objIN.InvLastUpdateBy = Session("UserID").ToString
        objIN.InvLastUpdatedOn = Now.ToString("yyyy-MM-dd HH:MM:ss")
        objIN.InvShpCost = objS.OrdShpCost
        objIN.InvCurrencyCode = objS.OrdCurrencyCode
        objIN.InvCurrencyExRate = objS.OrdCurrencyExRate
        objIN.InvCustPO = objS.OrdCustPO
        objIN.InvShpWhsCode = objS.OrdShpWhsCode
        objIN.InvForOrderNo = objS.OrdID
        objIN.InvCompanyID = objS.ordCompanyID
        If objS.InvRefNo <> "" AndAlso objS.InvRefNo <> "0" Then
            objIN.InvRefNo = objS.InvRefNo
            objIN.InvCreatedOn = CDate(objS.invDate).ToString("yyyy-MM-dd hh:mm:ss")
        Else
            objIN.InvRefNo = 0
            objIN.InvCreatedOn = Now.ToString("yyyy-MM-dd hh:mm:ss")
        End If
        objIN.InvCommission = objS.ordercommission
        If chkCreditNote.Checked = True Then
            objIN.InvRefType = "CN"
        Else
            objIN.InvRefType = "IV"
        End If
    End Sub
    Protected Sub cmdAddNewOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~/Sales/Create.aspx")
    End Sub

    Protected Sub cmdAddNewOrder_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddNewOrder.ServerClick
        Response.Redirect("~/Sales/Create.aspx")
    End Sub

End Class

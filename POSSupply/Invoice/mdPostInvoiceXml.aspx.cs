﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Invoice_mdPostInvoiceXml : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtUrlToPost.Text = AppConfiguration.AppSettings[AppSettingKey.PostXmlInvoiceUrl];
            PopulateInvoiceXml();
        }
    }

    private void PopulateInvoiceXml()
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            System.Text.StringBuilder xmlInvoice = new System.Text.StringBuilder();

            Invoice inv = new Invoice();
            inv.PopulateObject(dbHelp, this.InvoiceID);

            if (inv.InvID <= 0) return;

            Orders ord = new Orders();
            ord.PopulateObject(dbHelp, inv.InvForOrderNo);

            if (ord.OrdID <= 0) return;

            Partners part = new Partners();
            part.PopulateObject(dbHelp, inv.InvCustID);

            InvoiceItems invItems = new InvoiceItems();

            PostXml pXml = new PostXml();
            pXml.PopulateObject(dbHelp, ord.PostedPOXmlID);

            MyXmlReader xReader = new MyXmlReader(pXml.PostedXmlData);

            string poPayLoadID = xReader.GetNodeAttribute("cXML", 0, "payloadID");
            string poOrderID = xReader.GetNodeAttribute("cXML/Request/OrderRequest/OrderRequestHeader", 0, "orderID");
            string poFromIndentity = xReader.GetNode("cXML/Header/From").InnerXml;
            string poToIdentity = xReader.GetNode("cXML/Header/To").InnerXml;
            string poSenderIndentity = xReader.GetNode("cXML/Header/Sender").InnerXml;

            DateTime docDateObj = inv.InvCreatedOn;
            string docDateTime = docDateObj.ToString("yyyy-MM-ddTHH:mm:sszzz");
            string invPayLoadID = string.Format("{0:yyyyMMddTHH:mm:sszzz}.invoice.{1}@mscdirect.com", docDateObj, inv.InvID);

            xmlInvoice.Append(@"<?xml version=""1.0"" encoding=""ISO-8859-1""?>")
                .Append(@"<!DOCTYPE cXML SYSTEM ""http://xml.cxml.org/schemas/cXML/1.2.009/InvoiceDetail.dtd"">")
                .AppendFormat(@"<cXML payloadID=""{0}"" timestamp=""{1}"">", invPayLoadID, docDateTime)
                .AppendFormat(@"<Header>")
                .AppendFormat(@"<From>{0}</From>", poToIdentity)
                .AppendFormat(@"<To>{0}</To>", poFromIndentity)
                //.AppendFormat(@"<Sender>{0}</Sender>", poSenderIndentity)
                .AppendFormat(@"<Sender><Credential domain=""DUNS""><Identity>{0}</Identity>", xReader.GetNodeText("cXML/Header/Sender/Credential/Identity"))
                .AppendFormat(@"<SharedSecret>cXML</SharedSecret></Credential>")
                .AppendFormat(@"<UserAgent>{0}</UserAgent></Sender>", xReader.GetNodeText("cXML/Header/Sender/UserAgent"))               
                .AppendFormat(@"</Header>")
                .Append("<Request>")
                .Append("<InvoiceDetailRequest>")
                .AppendFormat(@"<InvoiceDetailRequestHeader invoiceID=""{0}"" purpose=""standard"" operation=""new"" invoiceDate=""{1}"">", inv.InvID, docDateTime)
                .Append("<InvoiceDetailHeaderIndicator/>")
                .Append("<InvoiceDetailLineIndicator isAccountingInLine='yes'/>")
                .Append("<InvoicePartner>")
                .AppendFormat(@"<Contact role=""remitTo"">")
                .AppendFormat(@"<Name xml:lang=""en"">{0}</Name>", part.PartnerLongName)
                .Append("</Contact>")
                .AppendFormat(@"<IdReference identifier=""{0}"" domain=""accountReceivableID""/>", part.PartnerID)
                .Append("</InvoicePartner>")
                .Append("</InvoiceDetailRequestHeader>")
                .Append("<InvoiceDetailOrder>")
                .Append("<InvoiceDetailOrderInfo>")
                .AppendFormat(@"<OrderReference orderID=""{0}"">", poOrderID)
                .AppendFormat(@"<DocumentReference payloadID=""{0}.{1}""/>", poOrderID, poPayLoadID).Append("</OrderReference>")
                .AppendFormat(@"</InvoiceDetailOrderInfo>");

            //Add Invoice Items Now            
            var lstItems = invItems.GetInvoiceItemList(dbHelp, inv.InvID);
            int lineNo = 1;
            Product prd = new Product();
            foreach (var item in lstItems)
            {
                prd.PopulateObject(dbHelp, item.InvProductID);
                xmlInvoice.AppendFormat(@"<InvoiceDetailItem invoiceLineNumber=""{0}"" quantity=""{1}"">", lineNo, item.InvProductQty)
                    .AppendFormat("<UnitOfMeasure>EA</UnitOfMeasure>")
                    .AppendFormat(@"<UnitPrice><Money currency=""{0}"">{1}</Money></UnitPrice>", inv.InvCurrencyCode, item.InvProductUnitPrice)
                    .AppendFormat(@"<InvoiceDetailItemReference lineNumber=""{0}"">", lineNo)
                    .AppendFormat(@"<ItemID><SupplierPartID>{0}</SupplierPartID></ItemID>", prd.PrdExtID)
                    .AppendFormat(@"<Description xml:lang=""en"">{0}</Description>", item.InvProdIDDesc)
                    .AppendFormat("</InvoiceDetailItemReference>")
                    .AppendFormat("</InvoiceDetailItem>");
                lineNo++;
            }

            TotalSummary ts = new TotalSummary();
            ts.InitInvoiceTotal(dbHelp, inv.InvID);

            var vCombinedTaxList = ts.ItemTaxList;
            /*foreach (var item in ts.ProcessTaxList)
            {
                var vItem = vCombinedTaxList.Where(cd => cd.TaxCode == item.TaxCode).FirstOrDefault();
                if (vItem != null)
                {
                    vItem.CalculatedPrice += item.CalculatedPrice;
                }
                else
                {
                    vCombinedTaxList.Add(item);
                }
            }*/

            xmlInvoice.AppendFormat(@"</InvoiceDetailOrder>")
                .AppendFormat(@"<InvoiceDetailSummary>")
                .AppendFormat(@"<SubtotalAmount>")
                .AppendFormat(@"<Money currency=""{0}"">{1}</Money>", inv.InvCurrencyCode, ts.ItemSubTotal)
                .AppendFormat("</SubtotalAmount>")
                .AppendFormat("<Tax>")
                .AppendFormat(@"<Money currency=""{0}"">{1}</Money>", inv.InvCurrencyCode, ts.TotalItemTax)
                .AppendFormat(@"<Description xml:lang=""en"">Sales Tax</Description>");

            foreach (var item in vCombinedTaxList)
            {
                xmlInvoice.AppendFormat(@"<TaxDetail purpose=""tax"" category=""{0}"">", item.TaxCode)
                    .AppendFormat(@"<TaxAmount><Money currency=""{0}"">{1}</Money></TaxAmount>", inv.InvCurrencyCode, item.CalculatedPrice)
                    .AppendFormat("</TaxDetail>");
            }           

            xmlInvoice.AppendFormat("</Tax>")
                .AppendFormat(@"<GrossAmount><Money currency=""{0}"">{1}</Money></GrossAmount>", inv.InvCurrencyCode, ts.ItemSubTotal + ts.TotalItemTax);
            if (ts.ProcessSubTotal > 0)
            {
                xmlInvoice.AppendFormat(@"<ShippingAmount><Money currency=""{0}"">{1}</Money>", ts.CurrencyCode, ts.ProcessSubTotal + ts.TotalProcessTax)
                .AppendFormat("<Tax>")
                .AppendFormat(@"<Money currency=""{0}"">{1}</Money>", inv.InvCurrencyCode, ts.TotalProcessTax)
                .AppendFormat(@"<Description xml:lang=""en"">Shipping Tax</Description>");
                foreach (var item in ts.ProcessTaxList)
                {
                    xmlInvoice.AppendFormat(@"<TaxDetail purpose=""tax"" category=""{0}"">", item.TaxCode)
                        .AppendFormat(@"<TaxAmount><Money currency=""{0}"">{1}</Money></TaxAmount>", inv.InvCurrencyCode, item.CalculatedPrice)
                        .AppendFormat("</TaxDetail>");
                }
                xmlInvoice.AppendFormat("</Tax>").AppendFormat("</ShippingAmount>");
            }
            else
            {
                xmlInvoice.AppendFormat(@"<ShippingAmount><Money currency=""{0}""/></ShippingAmount>", ts.CurrencyCode);
            }
            
                xmlInvoice.AppendFormat(@"<SpecialHandlingAmount><Money currency=""{0}""/></SpecialHandlingAmount>", ts.CurrencyCode)
                .AppendFormat(@"<NetAmount><Money currency=""{0}"">{1}</Money></NetAmount>", inv.InvCurrencyCode, ts.GrandTotal)
                .AppendFormat(@"</InvoiceDetailSummary>")
                .AppendFormat(@"</InvoiceDetailRequest>")
                .AppendFormat(@"</Request>")
                .AppendFormat(@"</cXML>");

            txtXmlToPost.Text = xmlInvoice.ToString();
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }   

    protected void btnPost_Click(object sender, EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            //Check posting allowed IP
            //string[] strIps = System.Configuration.ConfigurationManager.AppSettings["PostXmlFeatureAllowedIP"].Split(',');
            //string reqIP = Request.UserHostAddress;
            //if (string.IsNullOrEmpty(reqIP) || !strIps.Contains(reqIP))
            //{
            //    MessageState.SetGlobalMessage(MessageType.Failure, "Requested IP doesn't allow to post xml Invoice.");
            //}

            PostXml xPost = new PostXml();
            int xPostID = xPost.SetPostedData(dbHelp, txtXmlToPost.Text, PostType.IN, PostDirection.EXT);
            if (xPostID > 0)
            {
                string resFromRemote = xPost.GetResponse(txtUrlToPost.Text, txtXmlToPost.Text);
                xPost.SetResponse(dbHelp, resFromRemote, xPostID);
                pnlPostXml.Visible = false;
                pnlResp.Visible = true;
                txtResult.Text = resFromRemote;
            }            
        }
        catch(Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            Globals.RegisterReloadParentScript(this);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private int InvoiceID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["invid"], out id);
            return id;
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master"  AutoEventWireup="true" CodeFile="EditInvoice.aspx.cs" Inherits="Invoice_EditInvoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="contentBottom" style="padding:5px; height:100px; overflow:auto;">
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
        <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0">
            
            <tr>
                <td class="text">
                    <asp:Label ID="lblPrice" runat="server" Text="<%$ Resources:Resource, grdPOPrdCostPrice %>"></asp:Label>
                </td>
                <td class="input">
                    <%=sCurrencyBaseCode %>&nbsp;<asp:TextBox ID="txtPrice" runat="server" CssClass="numericTextField"
                                ValidationGroup="lstSrch" MaxLength="10"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqvalPrice" runat="server" ValidationGroup="lstSrch"
                                Display="Dynamic" ErrorMessage="<%$ Resources:Resource, msgPOItemPrice%>" SetFocusOnError="true"
                                ControlToValidate="txtPrice"></asp:RequiredFieldValidator>
                </td>
            </tr>
            
        </table>        
    </div>
    
        <br />
        <div class="div-dialog-command">
            <asp:Button Text="<%$Resources:Resource, btnSave %>"  ID="btnSave" runat="server" OnClick="btnSave_Click" ValidationGroup="lstSrch"/>
            <asp:Button Text="<%$Resources:Resource, btnCancel %>"  ID="btnCancel" runat="server" CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
            <div class="clear">
            </div>
        </div>
        <asp:ValidationSummary ID="valsAdminWarehouse" runat="server" ShowMessageBox="true"
            ShowSummary="false" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        function funCheckAmountReceivedVia(source, args) {
            
        }
        </script>
</asp:Content>



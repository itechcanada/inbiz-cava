Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Configuration.ConfigurationManager
''
Imports System
Imports System.IO
Imports System.Data.Odbc
Imports System.Web.HttpContext
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Partial Class Invoice_Show
    Inherits BasePage
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            subClose()
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("SOID") <> "" Then
                If Request.QueryString("DocTyp") = "QO" Or Request.QueryString("DocTyp") = "SO" Or Request.QueryString("DocTyp") = "IN" Then
                    Dim strContent As String = ""
                    Dim objPrn As New clsPrintHTML(Request.QueryString("SOID"))
                    strContent = objPrn.PrintRequestDetail(Request.QueryString("SOID"), Request.QueryString("DocTyp"), Request.QueryString("Dup"))

                    Dim strFileName As String = Request.QueryString("DocTyp") + "_" + Request.QueryString("SOID") + "_" + Now.ToString("yyyyMMddhhmmss") + ".html"
                    Dim strFullName As String = Server.MapPath("~") + "/pdf/" + strFileName
                    If (Not File.Exists(strFullName)) Then
                        File.WriteAllText(strFullName, strContent)

                        Dim objDoc As New clsDocuments
                        objDoc.SysDocType = "PO"
                        objDoc.SysDocDistType = "P"
                        objDoc.SysDocDistDateTime = Now.ToString("yyyy-MM-dd hh:mm:ss")
                        objDoc.SysDocPath = AppSettings("PDFPath") + "" + strFileName
                        objDoc.SysDocEmailTo = ""
                        objDoc.SysDocFaxToPhone = ""
                        objDoc.SysDocFaxToAttention = ""
                        objDoc.SysDocBody = ""
                        objDoc.SysDocSubject = ""
                        objDoc.insertDocuments()
                        objDoc = Nothing
                    End If
                    Response.Redirect("../Common/Print.aspx?ReqID=" + Request.QueryString("SOID") + "&DocTyp=" + Request.QueryString("DocTyp") & "&Dup=" & Request.QueryString("Dup"))
                End If
            End If
        End If
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    ' Sub routine to close popup
    Private Sub subClose()
        Response.Write("<script>window.opener.location.reload();</script>")
        Response.Write("<script>window.close();</script>")
    End Sub
    'Purchase Order
    Private Function funSOGen(ByVal strSOID As String) As String
        Return ""
    End Function
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)
        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + Microsoft.VisualBasic.Chr(10)
            Else
                strAddress += objAdr.AddressState + Microsoft.VisualBasic.Chr(10)
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + Microsoft.VisualBasic.Chr(10)
            Else
                strAddress += objAdr.AddressCountry + Microsoft.VisualBasic.Chr(10)
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function

    'Get Warehouse Address
    Protected Function funWarehouseAddress(ByVal strWhsCode As String)
        Dim strData As String = ""
        If strWhsCode <> "" Then
            Dim objWhs As New clsWarehouses
            objWhs.WarehouseCode = strWhsCode
            objWhs.getWarehouseInfo()
            If objWhs.WarehouseDescription <> "" Then
                strData += objWhs.WarehouseDescription + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseAddressLine1 <> "" Then
                strData += objWhs.WarehouseAddressLine1 + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseAddressLine2 <> "" Then
                strData += objWhs.WarehouseAddressLine2 + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseCity <> "" Then
                strData += objWhs.WarehouseCity + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseState <> "" Then
                strData += objWhs.WarehouseState + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseCountry <> "" Then
                strData += objWhs.WarehouseCountry + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehousePostalCode <> "" Then
                strData += objWhs.WarehousePostalCode
            End If
            objWhs = Nothing
        End If
        Return strData
    End Function

End Class

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;

public partial class Invoice_ViewInvoice : BasePage
{
    private Invoice _inv = new Invoice();
    int _currentRowIdx = -1;
    TotalSummary _lTotal;
    // On Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        //Security
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.INVOICE_GENERATION) && !CurrentUser.IsInRole(RoleID.ACCOUNT_RECEIVABLE) && !CurrentUser.IsInRole(RoleID.COLLECTION_AGENT) && !CurrentUser.IsInRole(RoleID.COLLECTION_MANAGER))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        if (!IsPagePostBack(grdInvoices))
        {
            //lblTitle.Text = Resources.Resource.lblSOViewSalesOrder;
            btnBack.Visible = Request.QueryString.AllKeys.Contains("returnUrl");

            DropDownHelper.FillDropdown(ddlSearch, "IN", "dlSh1", Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.liSelect, ""));
            DropDownHelper.FillDropdown(ddlSOStatus, "IN", "dlSh2", Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.liSelect, ""));
            txtSearch.Focus();

            if (POSRegCode != "")
            {
                foreach (ListItem li in ddlSearch.Items)
                {
                    if (li.Value == "RC")
                    {
                        li.Selected = true;
                    }
                }
                txtSearch.Text = POSRegCode;
            }

            // Get User Grid Height
            InbizUser _usr = new InbizUser();
            _usr.PopulateObject(CurrentUser.UserID);
            if (BusinessUtility.GetInt(_usr.UserGridHeight) > 0)
            {
                grdInvoices.Height = _usr.UserGridHeight;
            }
        }
    }

    protected void grdInvoices_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        int idxDsWhsCode = 2;
        if (_currentRowIdx != e.RowIndex)
        {
            _currentRowIdx = e.RowIndex;
            _lTotal = CalculationHelper.GetInvoiceTotal(BusinessUtility.GetInt(e.RowKey));
        }

        if (e.ColumnIndex == 1)
        {
            e.CellHtml = string.Format("{0} ({1})", e.CellHtml, e.RowValues[idxDsWhsCode]);
        }
        else if (e.ColumnIndex == 4)
        {
            Invoice objInvoice = new Invoice();
            objInvoice.InvoiceDetail(BusinessUtility.GetInt(e.RowKey));
            string ordCurrencyCode = "";
            if (objInvoice != null)
            {
                ordCurrencyCode = objInvoice.InvCurrencyCode;
            }
            //e.CellHtml = string.Format("{0:F}", BusinessUtility.GetCurrencyString(_lTotal.GrandTotal,Globals.CurrentCultureName));
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(_lTotal.GrandTotal, ordCurrencyCode);
        }
        else if (e.ColumnIndex == 5)
        {
            Invoice objInvoice = new Invoice();
            objInvoice.InvoiceDetail(BusinessUtility.GetInt(e.RowKey));
            string ordCurrencyCode = "";
            if (objInvoice != null)
            {
                ordCurrencyCode = objInvoice.InvCurrencyCode;
            }

            //e.CellHtml = string.Format("{0:F}", BusinessUtility.GetCurrencyString(_lTotal.TotalAmountReceived,Globals.CurrentCultureName));
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(_lTotal.TotalAmountReceived, ordCurrencyCode);
        }
        else if (e.ColumnIndex == 6)
        {
            Invoice objInvoice = new Invoice();
            objInvoice.InvoiceDetail(BusinessUtility.GetInt(e.RowKey));
            string ordCurrencyCode = "";
            if (objInvoice != null)
            {
                ordCurrencyCode = objInvoice.InvCurrencyCode;
            }

            int invID = BusinessUtility.GetInt(e.RowKey);
            ////e.CellHtml = string.Format("{0:F}", BusinessUtility.GetCurrencyString(_lTotal.OutstandingAmount,Globals.CurrentCultureName));
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(_lTotal.OutstandingAmount - _lTotal.ReturnedOverPaidAmount, ordCurrencyCode);
        }
        else if (e.ColumnIndex == 8)
        {
            string strData = "";
            switch (e.CellHtml.ToString())
            {
                case InvoicesStatus.SUBMITTED:
                    strData = Resources.Resource.liINSubmitted;
                    break;
                case InvoicesStatus.RE_SUBMITTED:
                    strData = Resources.Resource.liINReSubmitted;
                    break;
                case InvoicesStatus.PAYMENT_RECEIVED:
                    strData = Resources.Resource.liINPaymentReceived;
                    break;
                case InvoicesStatus.PARTIAL_PAYMENT_RECEIVED:
                    strData = Resources.Resource.liINPartialPaymentReceived;
                    break;
            }
            if (!string.IsNullOrEmpty(strData))
            {
                e.CellHtml = strData;
            }
            if (_lTotal.OutstandingAmount <= 0)
            {
                e.CellHtml = Resources.Resource.liPaid;
            }
        }
        else if (e.ColumnIndex == 10)
        {
            string strData = "";
            switch (e.CellHtml.ToString())
            {
                case InvoiceReferenceType.INVOICE:
                    strData = Resources.Resource.liINInvoice;
                    break;
                case InvoiceReferenceType.CREDIT_NOTE:
                    strData = Resources.Resource.liINCreditNote;
                    break;
            }
            e.CellHtml = strData;
        }
        else if (e.ColumnIndex == 13)
        {
            e.CellHtml = "";
            string strurl = "ViewInvoiceDetails.aspx?SOID=" + e.RowValues[0];

            if ((BusinessUtility.GetString(Request.QueryString["InvokeSrc"]).ToUpper() == "POS".ToUpper() && BusinessUtility.GetString(Request.QueryString["srchKey"]).ToUpper() != ""))
            {
                strurl += string.Format("&srchKey={0}&InvokeSrc={1}", BusinessUtility.GetString(Request.QueryString["srchKey"]), BusinessUtility.GetString(Request.QueryString["InvokeSrc"]));
            }

            e.CellHtml = string.Format("<a href=\"{0}\">{1}</a>", strurl, Resources.Resource.lblEdit);
        }
    }

    protected void grdInvoices_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        bool isSalesRescricted = CurrentUser.IsInRole(RoleID.SALES_REPRESENTATIVE) && BusinessUtility.GetBool(Session["SalesRepRestricted"]);

        bool isAccountant = CurrentUser.IsInRole(RoleID.ACCOUNT);
        if (Request.QueryString.AllKeys.Contains("InvokeSrc") && Request.QueryString["InvokeSrc"] == "POS")
        {
            txtSearch.Text = BusinessUtility.GetString(Request.QueryString["regcode"]);
        }

        if (Request.QueryString.AllKeys.Contains("_history"))
        {
            //sdsInvoices.SelectCommand = _inv.GetInvoiceSql(sdsInvoices.SelectParameters, Request.QueryString[ddlSearch.ClientID], Request.QueryString[ddlSOStatus.ClientID], Request.QueryString[txtSearch.ClientID], this.PartnerID, CurrentUser.UserID, isSalesRescricted);
            grdInvoices.DataSource = _inv.GetInvoice(Request.QueryString[ddlSearch.ClientID], Request.QueryString[ddlSOStatus.ClientID], Request.QueryString[txtSearch.ClientID], this.PartnerID, CurrentUser.UserID, isSalesRescricted);
        }
        else
        {


            if (Request.QueryString.AllKeys.Contains("custid") && (!string.IsNullOrEmpty(Request.QueryString["custid"])))
            {
                Partners prt = new Partners();
                prt.PopulateObject(BusinessUtility.GetInt(Request.QueryString["custid"]));
                grdInvoices.DataSource = _inv.GetInvoice("CN", ddlSOStatus.SelectedValue, prt.PartnerLongName, this.PartnerID, CurrentUser.UserID, isSalesRescricted);
            }
            else if (Request.QueryString.AllKeys.Contains("InvokeSrc") && Request.QueryString["InvokeSrc"] == "POS")
            {
                grdInvoices.DataSource = _inv.GetInvoice("RC", ddlSOStatus.SelectedValue, txtSearch.Text, this.PartnerID, CurrentUser.UserID, isSalesRescricted);
            }
            else
            {
                grdInvoices.DataSource = _inv.GetInvoice(ddlSearch.SelectedValue, ddlSOStatus.SelectedValue, txtSearch.Text, this.PartnerID, CurrentUser.UserID, isSalesRescricted);
            }
        }
    }

    private int PartnerID
    {
        get
        {
            int partnerId = 0;
            int.TryParse(Request.QueryString["PartnerID"], out partnerId);
            return partnerId;
        }
    }

    private string POSRegCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["regcode"]);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.QueryString["returnUrl"]);
    }
}
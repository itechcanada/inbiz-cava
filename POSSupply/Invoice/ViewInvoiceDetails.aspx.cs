﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

using iTECH.Library.DataAccess.MySql;

public partial class Invoice_ViewInvoiceDetails : BasePage
{
    Partners _part = new Partners();
    SysCompanyInfo _comInfo = new SysCompanyInfo();
    Invoice _inv = new Invoice();
    InvoiceItems _InvItems = new InvoiceItems();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdProducts, grdProcess))
        {

            CustomFields1.Initialize(this.InvoiceID, CustomFieldApplicableSection.InvoiceAddEdit);
            if (this.InvoiceID <= 0)
            {
                Response.Redirect("~/Invoice/ViewInvoice.aspx");
            }
            if (CurrentUser.IsInRole(RoleID.ACCOUNT))
            {
                grdProducts.Columns[11].Visible = true;
            }
            if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
            {
                lblGrossProfit.Visible = false;
                valGrossProfit.Visible = false;
            }
            InvoiceCartHelper.Dispose();
            InitializeInvoiceDetails();
        }
    }

    private void InitializeInvoiceDetails()
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            dbHelp.OpenDatabaseConnection();

            //lnkFullReturn.Visible = false;
            //hlFullReturn.Visible = false;
            //Populate DDL
            SysWarehouses whs = new SysWarehouses();
            whs.FillWharehouse(dbHelp, dlShpWarehouse, CurrentUser.UserDefaultWarehouse, this.CompanyID, new ListItem("", "0"));
            DropDownHelper.FillDropdown(dbHelp, dlStatus, "IN", "INSts", Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.liINSelStatus, "0"));
            new NetTerm().PopulateListControl(dbHelp, ddlNetTerms, new ListItem(""));
            new InbizUser().FillSalesRepresentatives(dbHelp, lbxSalesRepresentativs, null, "");

            hlReturnHistory.Text = Resources.Resource.lblReturnHistory;

            bool isAccountant = CurrentUser.IsInRole(RoleID.ACCOUNT);

            if (isAccountant == true)
            {
                btnChangeCustomerName.Visible = true;
            }
            else
            {
                btnChangeCustomerName.Visible = false;
            }

            if (this.InvoiceID > 0)
            {
                _inv.PopulateObject(dbHelp, this.InvoiceID);
                if (this.InvoiceID > 0)
                {
                    hlReturnHistory.Visible = this.InvoiceID > 0 && new InvoiceReturn().HasReturn(dbHelp, this.InvoiceID);
                    hlReturnHistory.NavigateUrl = string.Format("~/Returns/mdReturnHistory.aspx?oid={0}&invid={1}", _inv.InvForOrderNo, this.InvoiceID);
                }
                
                TotalSummary ts = CalculationHelper.GetInvoiceTotal(dbHelp, _inv.InvID);
                //lnkFullReturn.Visible = ts.TotalAmountReceived <= 0;
                //To to to if there is received amount in AR

                txtBillToAddress.Text = ""; //TO DO
                txtCustPO.Text = _inv.InvCustPO;
                txtNotes.Text = _inv.InvComment;
                txtShpToAddress.Text = ""; //TO DO            

                dlShpWarehouse.SelectedValue = _inv.InvShpWhsCode;
                dlStatus.SelectedValue = _inv.InvStatus;

                hdnInvoiceNoForOrder.Value = _inv.InvForOrderNo.ToString();
                hdnCompanyID.Value = _inv.InvCompanyID.ToString();
                hdnPartnerID.Value = _inv.InvCustID.ToString();
                hdnParterTypeID.Value = Globals.GetPartnerTypeID(_inv.InvCustType).ToString();
                hdnPartnerCurrencyCode.Value = _inv.InvCurrencyCode;
                hdnWhsCode.Value = _inv.InvShpWhsCode;
                hdnCurrrencyExchangeRate.Value = _inv.InvCurrencyExRate.ToString();
                hdnInvRefNo.Value = _inv.InvRefNo.ToString();
                hdnInvRefType.Value = _inv.InvRefType;
                hdnInvDate.Value = BusinessUtility.GetDateTimeString(_inv.InvCreatedOn, DateFormat.MMddyyyy);
                hdnInvCommisionType.Value = _inv.InvTypeCommission.ToString();
                hdnInvStatus.Value = _inv.InvStatus;
                this.CartExchangeRate = _inv.InvCurrencyExRate;

                //hlEditBillToAddress.NavigateUrl = string.Format("~/Common/mdAddressEdit.aspx?addrSourceID={0}&addrRef={1}&addrType={2}&ctrl={3}", hdnPartnerID.Value, _inv.InvCustType, AddressType.BILL_TO_ADDRESS, txtBillToAddress.ClientID);
                //hlEditShippingAddress.NavigateUrl = string.Format("~/Common/mdAddressEdit.aspx?addrSourceID={0}&addrRef={1}&addrType={2}&ctrl={3}", hdnPartnerID.Value, _inv.InvCustType, AddressType.SHIP_TO_ADDRESS, txtShpToAddress.ClientID);

                ////Populate Address
                //Addresses billtoAddress = _part.GetBillToAddress(dbHelp, _inv.InvCustID, Globals.GetPartnerTypeID(_inv.InvCustType));
                //Addresses shipToAddress = _part.GetShipToAddress(dbHelp, _inv.InvCustID, Globals.GetPartnerTypeID(_inv.InvCustType));

                hlEditBillToAddress.NavigateUrl = string.Format("~/Partner/AddressCollection.aspx?addType={0}&custID={1}&pType={2}&src={3}&ctrl={4}", AddressType.BILL_TO_ADDRESS, hdnPartnerID.Value, _inv.InvCustType, "SO", txtBillToAddress.ClientID);
                hlEditShippingAddress.NavigateUrl = string.Format("~/Partner/AddressCollection.aspx?addType={0}&custID={1}&pType={2}&src={3}&ctrl={4}", AddressType.SHIP_TO_ADDRESS, hdnPartnerID.Value, _inv.InvCustType, "SO", txtShpToAddress.ClientID);

                //Populate Address
                //Addresses billtoAddress = _part.GetBillToAddress(dbHelp, _ord.OrdCustID, Globals.GetPartnerTypeID(_ord.OrdCustType));
                //Addresses shipToAddress = _part.GetShipToAddress(dbHelp, _ord.OrdCustID, Globals.GetPartnerTypeID(_ord.OrdCustType));

                Addresses billtoAddress = new Addresses();
                billtoAddress.GetOrderAddress(_inv.InvForOrderNo, AddressType.BILL_TO_ADDRESS);

                Addresses shipToAddress = new Addresses();
                shipToAddress.GetOrderAddress(_inv.InvForOrderNo, AddressType.SHIP_TO_ADDRESS);  


                txtBillToAddress.Text = billtoAddress.ToHtml();
                txtShpToAddress.Text = shipToAddress.ToHtml();

                //Make Cart with Invoice Items
                InvoiceCartHelper.InitCart(dbHelp, this.InvoiceID);

                FillReadOnlyOrderDetails(dbHelp);
                lblName.Text = string.Format("{0}", _part.PartnerLongName);

                //Order
                Orders ord = new Orders();
                ord.PopulateObject(dbHelp, _inv.InvForOrderNo);
                ord.FillSalesRepresentativesCommission(dbHelp, lbxSalesRepresentativs, _inv.InvForOrderNo);
                //Set selected Sales rep users
                //Retreive Sales Rep 
                List<int> lstRep = ord.GetOrderSalesRepUsers(dbHelp, _inv.InvForOrderNo);
                foreach (ListItem item in lbxSalesRepresentativs.Items)
                {
                    if (lstRep.Contains(BusinessUtility.GetInt(item.Value)))
                    {
                        item.Selected = true;
                    }
                }
                ListItem li = ddlNetTerms.Items.FindByText(ord.OrdNetTerms);
                if (li != null)
                {
                    li.Selected = true;
                }
                lblShpTrackNo.Text = ord.OrdShpCode;
            }


            //Enable Buttons Settings 
            spnFax.Visible = this.InvoiceID > 0;
            spnMail.Visible = this.InvoiceID > 0;
            spnPrint.Visible = this.InvoiceID > 0;
            spSendXml.Visible = this.InvoiceID > 0;

            if (this.InvoiceID > 0)//settings for Print,Fax & Mail feature
            {
                mdFax.Url = string.Format("~/Common/mdSendMailOrFax.aspx?DocTyp={0}&SOID={1}&cmd={2}", this.GetDocType(), this.InvoiceID, "F"); //F stands for Fax
                mdMail.Url = string.Format("~/Common/mdSendMailOrFax.aspx?DocTyp={0}&SOID={1}&cmd={2}", this.GetDocType(), this.InvoiceID, "M"); //M stands for Mail
                mdPostXml.Url = string.Format("~/Invoice/mdPostInvoiceXml.aspx?invid={0}", this.InvoiceID);

                string printUrl = string.Format("ShowPdf.aspx?SOID={0}&DocTyp={1}", this.InvoiceID, this.GetDocType());
                imgPdf.Attributes.Add("onclick", string.Format("window.open('{0}', 'Print', 'height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no'); return false;", printUrl));
            }

            //Security
            btnReceivePayment.Visible = CurrentUser.IsInRole(RoleID.ACCOUNT_RECEIVABLE) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR);
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }


    }

    private void FillReadOnlyOrderDetails(DbHelper dbHelp)
    {

        _comInfo.PopulateObject(_inv.InvCompanyID, dbHelp);
        _part.PopulateObject(dbHelp, _inv.InvCustID);

        InbizUser user = new InbizUser();

        valCompanyName.Text = _comInfo.CompanyName;
        valCustomerName.Text = _part.PartnerLongName;
        valFax.Text = _part.PartnerFax;
        valInvDate.Text = BusinessUtility.GetDateTimeString(_inv.InvCreatedOn, DateFormat.MMddyyyyHHmmss);
        valInSoNo.Text = _inv.InvForOrderNo.ToString();
        valInvNo.Text = _inv.InvRefNo.ToString();
        valWhsCode.Text = _inv.InvShpWhsCode;
        //valGrossProfit.Text = string.Format("{0:F} %", _inv.GetGrossProfitByInvoice(dbHelp, _inv.InvID));
        valGrossProfit.Text = GetGPPct();
    }

    protected void grdProducts_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        grdProducts.DataSource = InvoiceCartHelper.CurrentCart.Items;
        grdProducts.DataBind();
    }
    protected void grdProducts_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        int dsIdxDiscountType = 10;
        int dsIdxDiscount = 9;

        if (e.ColumnIndex == 3) //Column Product Color Name
        {
            //e.CellHtml = e.CellHtml.Replace(Environment.NewLine, "<br>");
            int iProductID = BusinessUtility.GetInt(e.CellHtml);
            Product objProduct = new Product();
            DataTable dtProduct = objProduct.GetProductColorSize(null, BusinessUtility.GetInt(iProductID));
            foreach (DataRow drItem in dtProduct.Rows)
            {
                e.CellHtml = BusinessUtility.GetString(drItem["Color"]);
            }
        }
        else if (e.ColumnIndex == 4) //Column Product Size Name
        {
            //e.CellHtml = e.CellHtml.Replace(Environment.NewLine, "<br>");
            int iProductID = BusinessUtility.GetInt(e.CellHtml);
            Product objProduct = new Product();
            DataTable dtProduct = objProduct.GetProductColorSize(null, BusinessUtility.GetInt(iProductID));
            foreach (DataRow drItem in dtProduct.Rows)
            {
                e.CellHtml = BusinessUtility.GetString(drItem["Size"]);
            }
        }
        else if (e.ColumnIndex == 6)
        {
           // e.CellHtml = CurrencyFormat.GetReplaceCurrencySymbol(  BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName));
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(e.CellHtml));
        }


        else if (e.ColumnIndex == 8) //Format Discount Column
        {
            string disType = BusinessUtility.GetString(e.RowValues[dsIdxDiscountType]);
            if (disType == "P")
            {
                e.CellHtml = string.Format("{0} %", e.RowValues[dsIdxDiscount]);
            }
            else
            {
                e.CellHtml =  CurrencyFormat.GetReplaceCurrencySymbol( string.Format("{0:F}", BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.RowValues[dsIdxDiscount]), Globals.CurrentCultureName)));
            }
        }
        else if (e.ColumnIndex == 10)
        {

            //Orders objOrdr = new Orders();
            //DbHelper dbHelp = new DbHelper(true);
            //bool mustClose = false;
            //if (dbHelp == null)
            //{
            //    mustClose = true;
            //    dbHelp = new DbHelper(true);
            //}
            Invoice objInvoice = new Invoice();
            objInvoice.InvoiceDetail(this.InvoiceID);
            string ordCurrencyCode = "";
            if (objInvoice != null)
            {
                ordCurrencyCode = objInvoice.InvCurrencyCode;
            }
            //if (BusinessUtility.GetString(ordCurrencyCode) == "")
            //{
            //    Partners objPartnr = new Partners();
            //    objPartnr.PopulateObject(dbHelp, this.CustomerID);
            //    ordCurrencyCode = objPartnr.PartnerCurrencyCode;
            //}
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(e.CellHtml), ordCurrencyCode);
            //e.CellHtml = CurrencyFormat.GetReplaceCurrencySymbol(  BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName));
        }
        else if (e.ColumnIndex == 11)
        {
            //e.CellHtml = string.Format(@"<a class=""edit_Product""  href=""EditInvoice.aspx?InvItmID={0}&jscallback={1}&UnitPrice={2}"" >{3}</a>", e.RowValues[14], "reloadParentPage", e.RowValues[4], Resources.Resource.CmdCssEdit);
            string url = string.Format("EditInvoice.aspx?InvItmID={0}&jscallback={1}&UnitPrice={2}", e.RowValues[14], "reloadParentPage", e.RowValues[4]);
            e.CellHtml = string.Format("<a class='edit_Product' href='{0}' >{1}</a>", url, Resources.Resource.lblEdit);
            //e.CellHtml = string.Format(@"<a class=""edit_Product""  /a>");
        }
        else if (e.ColumnIndex == 12)
        {
            //Check Is Account Adjusment History Exist 
            if (_InvItems.ExistInvoiceItemHistory(null, BusinessUtility.GetInt(e.RowValues[14])))
            {
                e.CellHtml = string.Format(@"<a onclick=""ShowInvoiceHistory({0});""  ><img src=""../Images/HistroyImage.jpg"" /></a>", e.RowValues[14]);
            }
            else
            {
                e.CellHtml = "";
            }
        }
    }

    protected void grdProcess_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        grdProcess.DataSource = InvoiceCartHelper.CurrentCart.ProcessItems;
        grdProcess.DataBind();
    }

    protected void grdProcess_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 4)
        {
            e.CellHtml = CurrencyFormat.GetReplaceCurrencySymbol( BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName));
        }
        else if (e.ColumnIndex == 5)
        {
            e.CellHtml = CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName));
        }
        else if (e.ColumnIndex == 6)
        {
            e.CellHtml =CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName));
        }
        else if (e.ColumnIndex == 9)
        {
            e.CellHtml = CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName));
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            SetData();
            _inv.InsertIntoHst(null, CurrentUser.UserID, this.InvoiceID);
            _inv.Update(CurrentUser.UserID);
            //Update Order Sales Reps
            Orders ord = new Orders();
            List<int> srUsers = new List<int>();
            foreach (ListItem item in lbxSalesRepresentativs.Items)
            {
                if (item.Selected)
                {
                    srUsers.Add(BusinessUtility.GetInt(item.Value));
                }
            }
            ord.SetOrderSalesRepUsers(null, _inv.InvForOrderNo, srUsers.ToArray());

            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgRecordUpdatedSuccessfully);
            //Response.Redirect("~/Invoice/ViewInvoice.aspx");

            string strurl = "";
            if ((BusinessUtility.GetString(Request.QueryString["InvokeSrc"]).ToUpper() == "POS".ToUpper() && BusinessUtility.GetString(Request.QueryString["srchKey"]).ToUpper() != ""))
            {
                strurl += string.Format("~/Invoice/ViewInvoice.aspx?srchKey={0}&InvokeSrc={1}", BusinessUtility.GetString(Request.QueryString["srchKey"]), BusinessUtility.GetString(Request.QueryString["InvokeSrc"]));
            }
            else
            {
                strurl = "~/Invoice/ViewInvoice.aspx";
            }
            Response.Redirect(strurl,false);
            

        }
    }

    protected void btnGenerateInvoice_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {

        }
    }

    private void SetData()
    {
        _inv.InvID = this.InvoiceID;
        _inv.InvCreatedOn = BusinessUtility.GetDateTime(hdnInvDate.Value, DateFormat.MMddyyyy);
        _inv.InvRefNo = BusinessUtility.GetInt(hdnInvRefNo.Value);
        _inv.InvComment = txtNotes.Text;
        _inv.InvCompanyID = BusinessUtility.GetInt(hdnCompanyID.Value);
        _inv.InvCurrencyCode = hdnPartnerCurrencyCode.Value;
        _inv.InvCurrencyExRate = BusinessUtility.GetDouble(hdnCurrrencyExchangeRate.Value);
        _inv.InvCustID = BusinessUtility.GetInt(hdnPartnerID.Value);
        _inv.InvCustPO = txtCustPO.Text;
        _inv.InvCustType = Globals.GetPartnerType(BusinessUtility.GetInt(hdnParterTypeID.Value));
        _inv.InvTypeCommission = BusinessUtility.GetInt(hdnInvCommisionType.Value);
        _inv.InvLastUpdateBy = CurrentUser.UserID;
        _inv.InvShpCost = BusinessUtility.GetDouble(txtShpCost.Text);
        _inv.InvShpWhsCode = dlShpWarehouse.SelectedValue;
        _inv.InvStatus = dlStatus.SelectedValue;
        _inv.InvLastUpdatedOn = DateTime.Now;
        _inv.InvForOrderNo = BusinessUtility.GetInt(hdnInvoiceNoForOrder.Value);
        _inv.InvRefNo = BusinessUtility.GetInt(hdnInvRefNo.Value);
        _inv.InvRefType = hdnInvRefType.Value;

        CustomFields1.Save();
    }

    public int CompanyID
    {
        get
        {
            int cid = 0;
            int.TryParse(Request.QueryString["ComID"], out cid);
            return cid > 0 ? cid : CurrentUser.DefaultCompanyID;
        }
    }

    public int InvoiceID
    {
        get
        {
            int oid = 0;
            int.TryParse(Request.QueryString["SOID"], out oid);
            return oid;
        }
    }

    public int CustomerID
    {
        get
        {
            int cid = 0;
            int.TryParse(Request.QueryString["custID"], out cid);
            return cid;
        }
    }

    public string WarehouseCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["whsID"]);
        }
    }

    private string GetDocType()
    {
        string sStatus = "IN";
        switch (hdnInvStatus.Value)
        {
            case "S":
            case "R":
            case "P":
                sStatus = "IN";
                break;
        }
        return sStatus;
    }

    private string GetGPPct()
    {
        // string GP = "";
        string GPPercentage = "";
        try
        {
            if (InvoiceCartHelper.CurrentCart.Items.Count > 0)
            {
                double totalGP = 0.0;
                double internalCost = 0.0;
                double totalAmt = 0.0;
                double processAmt = 0.0;
                double finalAmt = 0.0;

                DbHelper dbHelp = new DbHelper();
                SlsAnalysis slsAnalysis = new SlsAnalysis();
                clsProducts chkPrdVendorExist = new clsProducts();
                Product objprd = new Product();
                foreach (var item in InvoiceCartHelper.CurrentCart.Items)
                {
                    if (chkPrdVendorExist.funGetVendorCount(item.ProductID.ToString()) > 0)
                    {
                        double GetUnitCostPriceAvg = slsAnalysis.TotalCostPriceAvg(dbHelp, item.ProductID);
                        totalGP += BusinessUtility.GetDouble(item.Quantity) * BusinessUtility.GetDouble((item.Price) - GetUnitCostPriceAvg);
                        totalAmt += BusinessUtility.GetDouble((item.Quantity * item.Price) - (item.Quantity * item.Price * item.Discount / 100));
                    }
                    else
                    {
                        objprd.PopulateObject(item.ProductID);
                        totalGP += BusinessUtility.GetDouble(item.Quantity) * BusinessUtility.GetDouble((item.Price) - objprd.PrdLandedPrice);
                        totalAmt += BusinessUtility.GetDouble((item.Quantity * item.Price) - (item.Quantity * item.Price * item.Discount / 100));
                    }
                }
                foreach (var item in SalesCartHelper.CurrentCart.ProcessItems)
                {
                    internalCost += BusinessUtility.GetDouble(item.ProcessInternalCost);
                    processAmt += BusinessUtility.GetDouble(item.ProcessFixedCost);
                }
                finalAmt = totalGP + (processAmt - internalCost);

                //SysCompanyInfo ci = new SysCompanyInfo();
                //ci.PopulateObject(CurrentUser.DefaultCompanyID);

                if (finalAmt < 0.0)
                {
                    //GP = string.Format("<font style='color:Red;font-weight:bold;font-family:Tahoma'>{0} {1:F}</font>", ci.CompanyBasCur, CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(totalGP + (processAmt - internalCost)), Globals.CurrentCultureName)));//font-size:12px
                    GPPercentage = string.Format("<font style='color:Red;font-weight:bold;font-family:Tahoma'>{0:F} {1}</font>", (BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(((totalGP + (processAmt - internalCost)) / (totalAmt + processAmt)) * 100), Globals.CurrentCultureName)).Replace("$", ""), " %");
                }
                else
                {
                    //GP = string.Format("<font style='color:Green;font-weight:bold;font-family:Tahoma'>{0} {1:F}</font>", ci.CompanyBasCur, CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble((totalGP + (processAmt - internalCost))), Globals.CurrentCultureName)));//font-size:11px
                    GPPercentage = string.Format("<font style='color:Green;font-weight:bold;font-family:Tahoma'>{0:F} {1}</font>", (BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(((totalGP + (processAmt - internalCost)) / (totalAmt + processAmt)) * 100), Globals.CurrentCultureName)).Replace("$", ""), " %");
                }

            }
            return GPPercentage;
        }
        catch
        {
            return GPPercentage;
        }
    }

    #region Cart Members

    private DataTable TempCart
    {
        get
        {
            if (Session["SalesCartInv"] != null)
            {
                return (DataTable)Session["SalesCartInv"];
            }
            MakeCart();
            return (DataTable)Session["SalesCartInv"];
        }
        set
        {
            if (value == null)
            {
                Session.Remove("SalesCartInv");
            }
        }
    }

    private DataTable TempProcessCart
    {
        get
        {
            if (Session["SalesProcessCartInv"] != null)
            {
                return (DataTable)Session["SalesProcessCartInv"];
            }
            MakeProcessCart();
            return (DataTable)Session["SalesProcessCartInv"];
        }
        set
        {
            if (value == null)
            {
                Session.Remove("SalesProcessCartInv");
            }
        }
    }

    private double CartExchangeRate
    {
        get
        {
            if (Session["CART_EXCHAGE_RATE"] != null)
            {
                return (double)Session["CART_EXCHAGE_RATE"];
            }
            Session["CART_EXCHAGE_RATE"] = 1.0D;
            return (double)Session["CART_EXCHAGE_RATE"];
        }
        set
        {
            Session["CART_EXCHAGE_RATE"] = value;
        }
    }

    private void MakeCart()
    {
        DataTable cart = new DataTable("Cart");
        DataColumn idCol = new DataColumn("ID", typeof(int));
        idCol.AutoIncrement = true;
        idCol.AutoIncrementSeed = 1;
        idCol.AutoIncrementStep = 1;

        DataColumn[] pCol = { idCol };
        cart.Columns.Add(pCol[0]);
        cart.Columns.Add("UPCCode", typeof(string));
        cart.Columns.Add("ProductName", typeof(string));
        cart.Columns.Add("Quantity", typeof(double));
        cart.Columns.Add("Price", typeof(double));
        cart.Columns.Add("RangePrice", typeof(double));
        cart.Columns.Add("WarehouseCode", typeof(string));
        cart.Columns.Add("TaxGrp", typeof(int));
        cart.Columns.Add("TaxGrpDesc", typeof(string));
        cart.Columns.Add("Discount", typeof(string));
        cart.Columns.Add("DisType", typeof(string));
        cart.Columns.Add("ItemTotal", typeof(double));
        cart.Columns.Add("ProductID", typeof(int));
        cart.PrimaryKey = pCol;

        Session["SalesCartInv"] = cart;
    }

    private void MakeProcessCart()
    {
        //Process Cart
        DataTable processCart = new DataTable("ProcessCart");
        DataColumn pCol = new DataColumn("ProcessID", typeof(int));
        pCol.AutoIncrement = true;
        pCol.AutoIncrementSeed = 1;
        pCol.AutoIncrementStep = 1;
        DataColumn[] pColProcess = { pCol };

        processCart.Columns.Add(pColProcess[0]);

        processCart.Columns.Add("ProcessCode", typeof(string));
        //Add the 2 field one is Tax Grp d and Tax Grp Name 
        processCart.Columns.Add("TaxGrpID", typeof(int));
        processCart.Columns.Add("TaxGrpName", typeof(string));

        processCart.Columns.Add("ProcessDescription", typeof(string));
        processCart.Columns.Add("ProcessFixedCost", typeof(double));
        processCart.Columns.Add("ProcessCostPerHour", typeof(double));
        processCart.Columns.Add("ProcessCostPerUnit", typeof(double));
        processCart.Columns.Add("TotalHour", typeof(int));
        processCart.Columns.Add("TotalUnit", typeof(int));
        processCart.Columns.Add("ProcessTotal", typeof(double));
        processCart.PrimaryKey = pColProcess;

        Session["SalesProcessCartInv"] = processCart;
    }
    #endregion

    #region WebMethods

    [System.Web.Services.WebMethod]
    public static string[] GetCalculatedTotalHtml(int invoiceid)
    {
        try
        {
            List<string> lstHtmlData = new List<string>();
            TotalSummary lTotal = CalculationHelper.GetInvoiceTotal(invoiceid);
            //Set Lables for html to be generated
            //lTotal.SetLabel(TotalSummary.LabelKey.BalanceAmount, Resources.Resource.lblBalanceAmount);
            //lTotal.SetLabel(TotalSummary.LabelKey.GrandSubTotal, Resources.Resource.lblSubTotalOrder);
            //lTotal.SetLabel(TotalSummary.LabelKey.GrandTotal, Resources.Resource.lblOrderTotal);
            //lTotal.SetLabel(TotalSummary.LabelKey.ReceivedAmount, Resources.Resource.lblReceivedAmount);
            //lTotal.SetLabel(TotalSummary.LabelKey.SubTotalItems, Resources.Resource.lblSubTotalItems);
            //lTotal.SetLabel(TotalSummary.LabelKey.SubTotalServices, Resources.Resource.lblSubTotalServices);
            //lTotal.SetLabel(TotalSummary.LabelKey.TotalDiscount, Resources.Resource.lblDiscount);
            //lTotal.SetLabel(TotalSummary.LabelKey.TotalTax, Resources.Resource.lblTotalTax);
            DbHelper dbHelp = new DbHelper(true);
            Invoice _inv = new Invoice();
            _inv.PopulateObject(dbHelp, BusinessUtility.GetInt(invoiceid));

            lstHtmlData.Add(CommonHtml.GetItemTotalHtml(lTotal, BusinessUtility.GetInt(_inv.InvCustID), BusinessUtility.GetInt(_inv.InvForOrderNo)));
            lstHtmlData.Add(CommonHtml.GetProcessTotalHtml(lTotal));
            lstHtmlData.Add(CommonHtml.GetGrandTotalHtml(lTotal).Replace(CommonResource.ResourceValue("lblSubTotalOrder"), CommonResource.ResourceValue("lblInvoiceTotal")));
            return lstHtmlData.ToArray();
        }
        catch
        {
            return new List<string>().ToArray();
        }
    }

    [System.Web.Services.WebMethod]
    public static string SetCustomerOrderAddress(int addrId, int ordID, string addType)
    {
        Addresses objAddress = new Addresses();
        objAddress.GetAddress(addrId);


        // Set Order Address
        Orders objOrders = new Orders();
        objOrders.SaveOrderAddresse(ordID, objAddress.AddressLine1, objAddress.AddressLine2, objAddress.AddressLine3, objAddress.AddressCity, objAddress.AddressState, objAddress.AddressCountry, objAddress.AddressPostalCode, addType);
        return objAddress.ToHtml();
    }

    #endregion

    protected void btnReceivePayment_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("~/AccountsReceivable/InvoicePayment.aspx?SOID={0}", this.InvoiceID));
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        string strurl = "";
        
        //Response.Redirect("~/Invoice/ViewInvoice.aspx");

        if ((BusinessUtility.GetString(Request.QueryString["InvokeSrc"]).ToUpper() == "POS".ToUpper() && BusinessUtility.GetString(Request.QueryString["srchKey"]).ToUpper() != ""))
        {
            strurl += string.Format("~/Invoice/ViewInvoice.aspx?srchKey={0}&InvokeSrc={1}", BusinessUtility.GetString(Request.QueryString["srchKey"]), BusinessUtility.GetString(Request.QueryString["InvokeSrc"]));
        }
        else
        {
            strurl = "~/Invoice/ViewInvoice.aspx";
        }

        //Response.Redirect(strurl);
    }
    protected void btnSaveAndGoToCal_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            SetData();
            _inv.Update(CurrentUser.UserID);
            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgRecordUpdatedSuccessfully);
            Response.Redirect("~/Reservation/ReservationCalendar.aspx");
        }
    }
}
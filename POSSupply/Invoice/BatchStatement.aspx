﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/fullWidth.master" AutoEventWireup="true" CodeFile="BatchStatement.aspx.cs" Inherits="Invoice_BatchStatement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <h4>
        <%=Resources.Resource.lblBatchStatement%>
    </h4>
    <div style="text-align:center;">
      <table width="100%" border="0" cellpadding="5" cellspacing="0" style="margin:auto; width:500px;">                                             
            <tr runat="server" id="trComapany">
                <td style="text-align:right">
                    <asp:Label ID="lblcompany" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblBaInvCompany %>">
                    </asp:Label>
                </td>
                <td style="text-align:left">
                    <asp:DropDownList ID="dlstCompany" runat="server" Width="200px">
                    </asp:DropDownList>
                    <asp:CustomValidator ID="custvalCompany" runat="server" ClientValidationFunction="funCompany"
                        Display="None" ErrorMessage="<%$ Resources:Resource, PlzSelCompanyName %>"> 
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td style="text-align:right">
                    <asp:Label ID="lblBatchstatementCreatedDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblBatchstatementCreatedDate %>">
                    </asp:Label>
                </td>
                <td style="text-align:left">
                    <asp:DropDownList ID="dlstCreatedDate" runat="server" Width="200px">
                        <asp:ListItem Value="0" Text="<%$ Resources:Resource, liARSelectCreatedDays %>" />
                        <asp:ListItem Value="10" Text="<%$ Resources:Resource, liARCreatedDate10 %>" />
                        <asp:ListItem Value="15" Text="<%$ Resources:Resource, liARCreatedDate15 %>" />
                        <asp:ListItem Value="30" Text="<%$ Resources:Resource, liARCreatedDate30 %>" />
                        <asp:ListItem Value="60" Text="<%$ Resources:Resource, liARCreatedDate60 %>" />
                        <asp:ListItem Value="90" Text="<%$ Resources:Resource, liARCreatedDate90 %>" />
                        <asp:ListItem Value="180" Text="<%$ Resources:Resource, liARCreatedDate180 %>" />
                        <asp:ListItem Value="180p" Text="<%$ Resources:Resource, liARCreatedDate180+ %>" />
                    </asp:DropDownList>
                    <asp:CustomValidator ID="custvalCreatedDate" runat="server" ClientValidationFunction="funSelectCreatedDate"
                        Display="None" ErrorMessage="<%$ Resources:Resource, custvalBatchStatementCreatedDate %>"> 
                    </asp:CustomValidator>
                </td>
            </tr>   
            <tr>
                <td style="text-align:right">                    
                    <asp:Label ID="Label1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMSelectCustomer %>">
                    </asp:Label>
                </td>
                <td style="text-align:left">
                    <asp:DropDownList CssClass="modernized_select" ID="ddlCustomers" runat="server" Width="300px" data-placeholder="ALL">                        
                    </asp:DropDownList>
                </td>
            </tr>                    
            <tr>
               <td></td>
               <td style="text-align:left">
                <asp:Button ID="btnPrint" Text="<%$Resources:Resource, cmdCssPrint%>" 
                                     runat="server" onclick="btnPrint_Click" />
               </td>
            </tr>
            <asp:ValidationSummary ID="valsReport" runat="server" ShowMessageBox="true" ShowSummary="false" />
        </table>
    </div>
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" runat="Server">
    <script language="javascript">
        function funCompany(source, arguments) {
            if (document.getElementById('<%=dlstCompany.ClientID%>').value == 0) {
                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;
            }
        }

        function funSelectCreatedDate(source, arguments) {
            if (document.getElementById('<%=dlstCreatedDate.ClientID%>').value == 0) {
                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;
            }
        }
    </script>
</asp:Content>




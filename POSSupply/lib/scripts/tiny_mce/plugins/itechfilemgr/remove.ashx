﻿<%@ WebHandler Language="C#" Class="remove" %>

using System;
using System.Web;
using System.IO;
using iTECH.Library.Utilities;

public class remove : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        try
        {
            if (!string.IsNullOrEmpty(context.Request.QueryString["url"])) {
                //Delete File
                string filePath = context.Server.MapPath(context.Request.QueryString["url"]);
                string fileName = Path.GetFileName(filePath);
                File.Delete(filePath);

                //Delete Thumb File If file is Image
                if (FileManager.IsValidImageFileType(Path.GetExtension(filePath))) {
                    File.Delete(filePath.Replace(fileName, "thumb." + fileName));
                }
                
                
                context.Response.Write("1");
            }
            else
            {
                context.Response.Write("0");
            }
        }
        catch 
        {
            context.Response.Write("505");
        }               
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}
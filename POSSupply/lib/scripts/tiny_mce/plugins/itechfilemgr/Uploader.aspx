﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Uploader.aspx.cs" Inherits="FileManager_Uploader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--Site CSS--%>
    <link href="../../../../css/style.css" rel="stylesheet" type="text/css" />

    <link href="css/south-street/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
    <script src="js/jquery.format.1.05.js" type="text/javascript"></script>
    <style type="text/css">
        .hide
        {
            display: none;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $(".inpt_number").format({ precision: 0, allow_negative: false, autofix: true });
            $(".btnUpload").button();
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="padding:5px; margin:5px;">
         <asp:FileUpload ID="fuUpload" runat="server" />
         <asp:RequiredFieldValidator ID="rfvRequired" ControlToValidate="fuUpload" Display="Dynamic"
                    runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
        <br /><br />
         <b>Dimention(for images only):</b>
        <div style="padding:5px; border:1px solid #DFD9C3;">
           
            <table border="0" cellpadding="0" cellspacing="5">
                <tr>
                    <td>
                        Width
                    </td>
                    <td>
                        <asp:TextBox CssClass="inpt_number" ID="txtWidth" Text="0" runat="server" MaxLength="3"
                            Width="40px" />
                    </td>
                    <td>
                        Height
                    </td>
                    <td>
                        <asp:TextBox CssClass="inpt_number" ID="txtHeight" Text="0" runat="server" MaxLength="3"
                            Width="40px" />
                    </td>
                </tr>
            </table>
            <p>
                Note*: <span style="font-weight: normal;">If dimention not specified it will save image
                    in original dimention.</span>
                <br />
                Recommended maximum file size:
                <asp:Label ID="lblFileSize" Text="2 MB" runat="server" />
                (each)
                <br />
            </p>
        </div>
        <%--<br />
        <b>Add file description: (maximum 150 characters allowed)</b><br />
        <div style="padding:5px; border:1px solid #DFD9C3;">
            <asp:TextBox ID="txtFileDescription" runat="server" Width="200px" Height="47px" TextMode="MultiLine" />
        </div>--%>
        
        <div style="margin-top:10px;">
            <asp:Button ID="bntUpload" class="btnUpload" runat="server" Text="Upload" OnClick="bntUpload_Click" /><br />
             <span runat="server" style="font-weight: bold; color: Red" id="spanerror"></span>
            <asp:Label ID="lblGreen" ForeColor="Green" Text="" runat="server" />
            <asp:Label ID="lblRed" ForeColor="Red" Text="" runat="server" />
        </div>
        
        <asp:TextBox ID="txtPath" runat="server" style="display:none;"></asp:TextBox>
    </div>
    </form>
</body>
</html>

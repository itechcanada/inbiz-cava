﻿<%@ Page Language="C#" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Import Namespace="iTECH.Library.Utilities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();


        if (Request["funcation"].ToString() == "FilesULLI")
        {
            if (string.IsNullOrEmpty(Request["Path"].ToString()) || Request["Path"].ToString() == "/" || Request["Path"].ToString() == "/UserFiles/")
            {
                FilesULLI(ConfigurationManager.AppSettings["FileManagerRootPath"].ToString());
            }
            else
            {
                FilesULLI(Request["Path"].ToString());
            }
        }


        if (Request["funcation"].ToString() == "LoadFilesHTML")
        {
            if (string.IsNullOrEmpty(Request["Path"].ToString()) || Request["Path"].ToString() == "/")
            {
                LoadFilesHTML(ConfigurationManager.AppSettings["FileManagerRootPath"].ToString());
            }
            else
            {
                LoadFilesHTML(Request["Path"].ToString());
            }
        }        
    }

    private void FilesULLI(string DirPath)
    {
        DirPath = DirPath.Replace("//", "/");
        string fullPath = Server.MapPath(DirPath);        
        
        string[] files = Directory.GetFiles(fullPath);
        IComparer comp = new FileComparer(FileComparer.CompareBy.CreationTime);
        Array.Sort(files, comp);
        string[] directories = Directory.GetDirectories(fullPath);
        string renderHtml = "";
        string vertualPath = ResolveUrl(DirPath);
        foreach (string fl in files)
        {
            FileInfo fInfo = new FileInfo(fl);
            string ext = Path.GetExtension(fl);          
            /*<li><a href="/uploads/ninfee-36bae74a-99295865.jpg" class="insert">&nbsp;</a>
			<a href="/uploads/ninfee-36bae74a-99295865.jpg" class="delete">&nbsp;</a>
			<img alt="" src="/uploads/.thumbs/ninfee-36bae74a-99295865.jpg">
			</li>*/
            string fName = System.IO.Path.GetFileName(fl);
            string fPath = vertualPath.TrimEnd('/') + "/" + fName;
            //string fDesc = FileManager.GetFileInfo(fName);
            //fDesc = Server.HtmlEncode(fDesc.Replace(Environment.NewLine, "<br>"));
            //if (string.IsNullOrEmpty(fDesc)) {
            //    fDesc = "<b>" + fInfo.CreationTime.ToString() + "</b>";
            //}
            if (FileManager.IsValidImageFileType(ext))
            {
                if (!fName.StartsWith("thumb."))
                {
                    string fThumbName = "thumb." + fName;
                    string fThumbPath = vertualPath.TrimEnd('/') + "/" + fThumbName;
                    renderHtml += string.Format(@"<li class=""file_info""><a href=""{0}"" ftype=""image"" class=""insert"">&nbsp;</a><a href=""{0}"" ftype=""image"" class=""delete"">&nbsp;</a><div class=""smallipopHint"">{1}</div>", fPath, FileManager.GetImageInfoHtml(fPath));
                    //renderHtml += string.Format(@"<a href=""{0}"" class=""delete"">&nbsp;</a>", fPath);
                    //renderHtml += string.Format(@"<img alt="""" src=""{0}"" style=""max-width:100px;max-height:100px;"" /></li>", fPath);

                    if (File.Exists(Server.MapPath(ResolveClientUrl(fThumbPath))))
                    {
                        renderHtml += string.Format(@"<img alt="""" src=""{0}"" title="""" style=""max-width:100px;max-height:100px;"" /></li>", fThumbPath);
                    }
                    else
                    {
                        renderHtml += string.Format(@"<img alt="""" src=""{0}"" title="""" style=""max-width:100px;max-height:100px;"" /></li>", fPath);
                    }
                }
            }
            else if(ext == ".pdf")
            {
                renderHtml += string.Format(@"<li class=""file_info""><a href=""{0}"" ftype=""pdf"" class=""insert"">&nbsp;</a><a href=""{0}"" ftype=""image"" class=""delete"">&nbsp;</a><div class=""smallipopHint"">{1}</div>", fPath, FileManager.GetOtherFileInfo(fPath));
                //renderHtml += string.Format(@"<a href=""{0}"" class=""delete"">&nbsp;</a>", fPath);
                renderHtml += @"<img alt="""" src=""img/oficina_pdf.png"" title="""" style=""max-width:100px;max-height:100px;"" /></li>";
            }
        }
        if (string.IsNullOrEmpty(renderHtml)) renderHtml = "<li>File not found!</li>";
        
        Response.Clear();
        Response.Write(renderHtml);
        Response.End();
    }

    private void LoadFilesHTML(string DirPath)
    {

        string html = "";
        if (DirPath == "0")
        {
            html += "<li path='" + ConfigurationManager.AppSettings["FileManagerRootPath"].ToString() + "' class='jstree-closed' id='root2'><a onclick=LoadFiles('" + ConfigurationManager.AppSettings["FileManagerRootPath"].ToString() + "') href='#'>" + "Root" + "</a></li>";
        }
        else
        {

            string FullPath = Server.MapPath(DirPath);
            // additional securite check to make sure that no operation can done out of the user file
            String userFileFolder = Server.MapPath(ConfigurationManager.AppSettings["FileManagerRootPath"].ToString());
            if (!FullPath.Contains(userFileFolder))
                return;

            string[] Files = Directory.GetFiles(FullPath);
            string[] Directorys = Directory.GetDirectories(FullPath);
            foreach (string Dir in Directorys)
            {
                html += "<li path='" + (DirPath + System.IO.Path.GetFileName(Dir)) + "/" + "' class='jstree-closed'><a onclick=LoadFiles('" + (DirPath + System.IO.Path.GetFileName(Dir)).Replace(" ", "%20") + "/')  href='#'>" + Path.GetFileName(Dir) + "</a></li>";
                //html += "{\"FilePath\":\"" + DirPath + Path.GetFileName(Dir) + "/" + "\",";
                //html += "\"FileName\":\"" + Path.GetFileName(Dir) + "\",";
                //html += "\"FileType\":\"" + "2" + "\",";
                //html += "\"FileSize\":\"" + "" + "\",";
                //html += "\"Fileicon\":\"" + "icons/folder-32.png" + "\"";
                //html += "},";
            }
        }        

        Response.Clear();
        Response.Write(html);
        Response.End();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>

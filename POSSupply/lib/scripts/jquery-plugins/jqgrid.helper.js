﻿//Initialize the custom search panel to perform search on jqGrid.
(function ($) {
    $.fn.extend({
        onJqGridSorting: function () {
            this.setPostDataItem("_history", "0");
        },
        onJqGridPaging: function () {
            this.setPostDataItem("_history", "0");
        },
        initJqGridCustomSearch: function (options) {
            var defaults = {
                jqGridID: '',
                searchButtonID: 'btnSearch',
                keySelectorClass: 'filter-key',
                toBeRemoveParams: ''
            };
            var options = $.extend(defaults, options);
            var obj = $(this);
            $('#' + options.searchButtonID).click(function () {
                var iCount = 0;
                var grid = jQuery('#' + options.jqGridID);

                //Remove PostedData if available
                var arrRData = options.toBeRemoveParams.split(',');
                for (var i = 0; i < arrRData.length; i++) {
                    grid.removePostDataItem(arrRData[i]);
                }

                var filterArr = {};
                $('.' + options.keySelectorClass, obj).each(function () {
                    var ele = document.getElementById($(this).attr('for'));

                    if ($('#' + $(this).attr('for')).is('input[type=text]')) {
                        filterArr[ele.id] = ele.value;
                        iCount++;
                    }
                    else if ($('#' + $(this).attr('for')).is('select')) {
                        filterArr[ele.id] = ele.value;
                        iCount++;
                    }
                    else if ($('#' + $(this).attr('for')).is('input[type=radio]')) {
                        if (ele.checked) {
                            filterArr[ele.id] = ele.value;
                            iCount++;
                        }
                    }
                });
                filterArr["_history"] = "0";
                grid.appendPostData(filterArr);
                grid.trigger("reloadGrid", [{ page: 1}]);
            });

            $(this).keydown(function (e) {
                if (e.keyCode == 13) {
                    $(this).find('#' + options.searchButtonID).click();
                    return false;
                }
            });
        }
    });
})(jQuery);


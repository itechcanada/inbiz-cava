﻿(function ($) {
    //create UI Tab namespace
    $.UiTabs = $.UiTabs || {};

    //Default Options copy from jquery.ui.tabas.js
    $.UiTabs._defaultOptions = {
        add: null,
		ajaxOptions: null,
		cache: false,
		cookie: null, // e.g. { expires: 7, path: '/', domain: 'jquery.com', secure: true }
		collapsible: false,
		disable: null,
		disabled: [],
		enable: null,
		event: "change", //by default it was 'click' we have change to provide custom support
		fx: null, // e.g. { height: 'toggle', opacity: 'toggle', duration: 200 }
		idPrefix: "ui-tabs-",
		load: null,
		panelTemplate: "<div></div>",
		remove: null,
		select: null,
		show: null,
		spinner: "<em>Loading&#8230;</em>",
		tabTemplate: "<li><a href='#{href}'><span>#{label}</span></a></li>",
        tabContainerSelector: '.tabs', //Additional option
        tabSelector: 'ul.ui-tabs-nav a' //Additional option
    }

    $.UiTabs.Create = function (options) {
        //extend UiTabs options with passed in options.
        var opts = $.extend(
				$.UiTabs._defaultOptions,
				options || {}
			);

        var tabs = $(opts.tabContainerSelector);
        tabs.tabs(opts);

        // Define our own click handler for the tabs, overriding the default.
        tabs.find(opts.tabSelector).click(function () {
            var state = {},
            // Get the id of this tab widget.
                id = $(this).closest(opts.tabContainerSelector).attr('id'),
            // Get the index of this tab.
                idx = $(this).parent().prevAll().length;
            // Set the state!
            state[id] = idx;
            $.bbq.pushState(state);
        });

        // Bind an event to window.onhashchange that, when the history state changes,
        // iterates over all tab widgets, changing the current tab as necessary.
        $(window).bind('hashchange', function (e) {
            
            // Iterate over all tab widgets.
            tabs.each(function () {
                // Get the index for this tab widget from the hash, based on the
                // appropriate id property. In jQuery 1.4, you should use e.getState()
                // instead of $.bbq.getState(). The second, 'true' argument coerces the
                // string value to a number.
                var idx = $.bbq.getState(this.id, true) || 0;
                // Select the appropriate tab for this tab widget by triggering the custom
                // event specified in the .tabs() init above (you could keep track of what
                // tab each widget is on using .data, and only select a tab if it has
                // changed).
                $(this).find(opts.tabSelector).eq(idx).triggerHandler('change');
            });
        })

        // Since the event is only triggered when the hash changes, we need to trigger
        // the event now, to handle the hash the page may have loaded with.
        $(window).trigger('hashchange');
    }

})(jQuery);
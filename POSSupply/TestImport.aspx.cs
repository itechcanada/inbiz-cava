﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using iTECH.Library.DataAccess.MySql;
using iTECH.InbizERP.BusinessLogic;
using MySql.Data.MySqlClient;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using System.Data.Odbc;
public partial class TestImport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SysTaxCodeDesc obj = new SysTaxCodeDesc();
            obj.PopulateObject(12);

        }
    }
    protected void btnConvertFile_Click(object sender, EventArgs e)
    {
        try
        {
            if (CSVFile.HasFile)
            {
                string strFile = System.IO.Path.GetFileName(CSVFile.FileName).Trim();
                string[] strFileArr = strFile.Split('.');
                int count = strFileArr.Length - 1;
                if (strFileArr.Length == 1)
                {
                    Response.Write("Invalid File.");
                }
                else
                {
                    if (strFileArr[count].ToLower() == "csv")
                    {
                        
                        string SaveFolder = Server.MapPath("~/Upload/CSVFiles/Customers/");
                        string SaveLocation = SaveFolder + strFile;
                        CSVFile.SaveAs(SaveLocation);
                        ExcelHelper.ConvertCSVToXLSX(SaveFolder, strFile);
                        //DataTable dtexcel = new DataTable();
                        //string connectionString = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + SaveFolder + ";";
                        //OdbcConnection conn = new OdbcConnection(connectionString);
                        //conn.Open();
                        //string query = "SELECT  * FROM [" + strFile + "]";
                        //OdbcDataAdapter daexcel = new OdbcDataAdapter(query, conn);
                        //daexcel.Fill(dtexcel);
                        //string sFileNme = DateTime.Now.ToString("yyyymmddhhmmss");
                        //ExcelHelper.Instance.ExportToExcel(dtexcel, Request.PhysicalApplicationPath + "Upload\\CSVFiles\\Customers" + sFileNme + ".xlsx");
                        //Response.Redirect("~/Upload/CSVFiles/Customers" + sFileNme + ".xlsx");
                    }
                    else
                    {
                        Response.Write("Please Select CSV File to Convert.");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(Resources.Resource.msgCriticalError + ex.Message);
        }
    }
}
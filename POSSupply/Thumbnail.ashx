﻿<%@ WebHandler Language="C#" Class="Thumbnail" %>

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

public class Thumbnail : IHttpHandler {
    private Regex _nameValidationExpression = new Regex(@"[^\w/]");
    private int _thumbnailSize = 90;

    public void ProcessRequest(HttpContext context) {
        string strw;
        string AlbumName;
        string strPic;
        string strPicType;
        strw = context.Request.QueryString["p"];
      
        strPic = strw.Substring(strw.LastIndexOf("/") + 1);
        string[] str = strPic.Split('.');
        string photoName = str[0];

        if (string.IsNullOrEmpty(photoName))
        {
            return;
        } 

        if (_nameValidationExpression.IsMatch(photoName))
        {
            throw new HttpException(404, "Invalid photo name.");
        }


        strPicType = context.Request.QueryString["Pimg"];
        string cachePath;
        if (strPicType == "y")
        {
            cachePath = Path.Combine(HttpRuntime.CodegenDir, photoName + "PI.png");
        }
        else 
        {
            cachePath = Path.Combine(HttpRuntime.CodegenDir, photoName + ".png");
        }
        
        //string cachePath = Path.Combine(HttpRuntime.CodegenDir, photoName + ".png");
        if (File.Exists(cachePath)) {
            OutputCacheResponse(context, File.GetLastWriteTime(cachePath));
            context.Response.WriteFile(cachePath);
            return;
        }
        string photoPath ;
        string stCat = context.Request.QueryString["Cat"];
        
        if (stCat=="Cat")
        {
            photoPath = context.Server.MapPath("~/upload/Category/" + photoName + "." + str[1]);   
        }
        else if (stCat == "Subcat")
        {
            photoPath = context.Server.MapPath("~/upload/subCategory/" + photoName + "." + str[1]);
        }
        else if(stCat == "POSCatg")
        {
            //if (photoName = "")
            //{

            //}
            //else
            //{
                photoPath = context.Server.MapPath("~/upload/POSCategory/" + photoName + "." + str[1]); 
            //}
            
        }
        else
        {
            photoPath = context.Server.MapPath("~/upload/Product/"+ photoName + "." + str[1]); 
        }
        
       
        
        
        Bitmap photo;
        try {
            photo = new Bitmap(photoPath);
        }
                           
        catch (ArgumentException) {
            if (stCat == "POSCatg")
            {
                photoPath = context.Server.MapPath("~/upload/POSCategory/Noimage.jpg");
                photo = new Bitmap(photoPath);

            }
            else
            {
                throw new HttpException(404, "Photo not found.");
            }
        }
        context.Response.ContentType = "image/png";
        int width, height;
        if (photo.Width > photo.Height) {
            width = _thumbnailSize;
            height = photo.Height * _thumbnailSize / photo.Width;
        }
        else {
            width = photo.Width * _thumbnailSize / photo.Height;
            height = _thumbnailSize;
        }
        Bitmap target = new Bitmap(width, height);
        using (Graphics graphics = Graphics.FromImage(target)) {
            graphics.CompositingQuality = CompositingQuality.HighSpeed;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.CompositingMode = CompositingMode.SourceCopy;
            graphics.DrawImage(photo, 0, 0, width, height);
            using (MemoryStream memoryStream = new MemoryStream()) {
                target.Save(memoryStream, ImageFormat.Png);
                OutputCacheResponse(context, File.GetLastWriteTime(photoPath));
                using (FileStream diskCacheStream = new FileStream(cachePath, FileMode.Create)) {
                    memoryStream.WriteTo(diskCacheStream);
                }
               memoryStream.WriteTo(context.Response.OutputStream);
            }
        }
    }

    private static void OutputCacheResponse(HttpContext context, DateTime lastModified) {
        HttpCachePolicy cachePolicy = context.Response.Cache;
        cachePolicy.SetCacheability(HttpCacheability.Public);
        cachePolicy.VaryByParams["p"] = true;
        cachePolicy.SetOmitVaryStar(true);
        cachePolicy.SetExpires(DateTime.Now + TimeSpan.FromDays(365));
        cachePolicy.SetValidUntilExpires(true);
        cachePolicy.SetLastModified(lastModified);
    }
 
    public bool IsReusable {
        get {
            return true;
        }
    }
} 
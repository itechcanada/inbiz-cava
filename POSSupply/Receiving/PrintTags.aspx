﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PrintTags.aspx.vb" Inherits="Receiving_PrintTags" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="../lib/scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-plugins/jquery-framedialog-1.1.2.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <%--<asp:Label runat="server" Style="font-family: IDAutomationHC39M; color: Black; font-size: 14px;"
                    ID="lblDebTranBarcode1"></asp:Label>--%>
    <asp:Repeater ID="rptTags" runat="server">
        <ItemTemplate>
            <div style="width: 230px; height:60px;">
                <asp:Label runat="server" Style="font-family: IDAutomationHC39M; color: Black; font-size: 14px;"
                    ID="lblDebTranBarcode"><%# Container.DataItem%></asp:Label>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    </form>

    <script language="javascript" type="text/javascript">
        var PrintCommandObject = null;

        function printPage() {
            if (PrintCommandObject) {
                try {
                    PrintCommandObject.ExecWB(6, 2);
                    PrintCommandObject.outerHTML = "";
                }
                catch (e) {
                    window.print();

                }
            }
            else {
                window.print();
            }
            var browserName = navigator.appName;
            if (navigator.appName != "Microsoft Internet Explorer") {
                window.close();
            }
            else {
                window.close();
            }
        }

        window.onload = function () {
            Minimize();
            if (navigator.appName == "Microsoft Internet Explorer") {
                // attach and initialize print command ActiveX object
                try {
                    var PrintCommand = '<object id="PrintCommandObject" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2" width="0" height="0"></object>';
                    document.body.insertAdjacentHTML('beforeEnd', PrintCommand);
                }
                catch (e) { }
            }
            setTimeout(printPage, 1);
        };


        function Minimize() {
            window.innerWidth = 10;
            window.innerHeight = 10;
            window.screenX = screen.width;
            window.screenY = screen.height;
            alwaysLowered = true;
        }

    </script>
</body>
</html>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="ViewReceiving.aspx.cs" Inherits="Receiving_ViewReceiving" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <%= Resources.Resource.lblView%>:
            <%= Resources.Resource.lblVendor%>
        </b>
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="ImageButton1" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif"
            CssClass="csv_export" />
        <asp:LinkButton ID="lnkAddEditVendor" runat="server" Visible="false"></asp:LinkButton>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft" runat="Server">
    <%--Amit Shukla Start--%>
    <%--<script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();
    </script>
    <uc1:ReceivingSearch ID="ReceivingSearch1" runat="server" />--%>
    <div>
        <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        <%= Resources.Resource.lblSearchForm%></div>
                    <div class="icon">
                        <img src="../../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, liWarehouseLocation%>"
                        AssociatedControlID="dlWarehouse" CssClass="filter-key"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:DropDownList ID="dlWarehouse" runat="server" Width="185px" ValidationGroup="PrdSrch">
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"
                        AssociatedControlID="dlSearch" CssClass="filter-key"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:DropDownList ID="dlSearch" runat="server" Width="185px" ValidationGroup="PrdSrch">
                        <%--                <asp:ListItem Text="Search By" Value="" />
                <asp:ListItem Value="PO" Text="PO Number" />
                <asp:ListItem Text="Receipt No" Value="RN" />
                <asp:ListItem Text="Vendor ID" Value="VI" />
                <asp:ListItem Text="UPC Code" Value="UC" />
                <asp:ListItem Text="Today" Value="T" />
                <asp:ListItem Text="This Week" Value="C" />
                <asp:ListItem Text="Next Week" Value="N" />
                <asp:ListItem Text="All" Value="A" />--%>
                        <asp:ListItem Text="<%$ Resources:Resource, lblSearchBy%>" Value="" />
                        <asp:ListItem Value="PO" Text="<%$ Resources:Resource, cmdCssPONumber%>" />
                        <asp:ListItem Text="<%$ Resources:Resource, lblReceiptNo%>" Value="RN" />
                        <asp:ListItem Text="<%$ Resources:Resource, lblVendorID%>" Value="VI" />
                        <asp:ListItem Text="<%$ Resources:Resource, lblSrchSKU %>" Value="UC" />
                        <asp:ListItem Text="<%$ Resources:Resource, lblToday %>" Value="T" />
                        <asp:ListItem Text="<%$ Resources:Resource, lblThisWeek %>" Value="C" />
                        <asp:ListItem Text="<%$ Resources:Resource, lblNextWeek %>" Value="N" />
                        <asp:ListItem Text="<%$ Resources:Resource, lblAll %>" Value="A" />
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword%>"
                        AssociatedControlID="txtSearch" CssClass="filter-key"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch" ValidationGroup="PrdSrch"></asp:TextBox>
                    &nbsp;</div>
                <div class="footer">
                    <div class="submit">
                        <%--  <asp:Button runat="server" Text="Submit" ID="btnSubmit" ValidationGroup="PrdSrch" />--%>
                        <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="Literal1"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
    <%--Amit Shukla End--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphMaster" runat="Server">
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td
        {
            border-bottom-color: inherit;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            font-weight: normal;
            height: 50px;
            overflow: hidden;
            padding: 0 2px;
            white-space: pre;
        }
        body, td, th
        {
            color: #515151;
            font-family: Lucida Sans Unicode,Lucida Grande,sans-serif;
            font-size: 12px;
        }
    </style>
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <%--<td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td align="right" style="width: 150px;">
                </td>--%>
            </tr>
        </table>
    </div>
    <div onkeypress="return disableEnterKey(event)" class="divMainContent">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" colspan="2">
                    <asp:UpdatePanel ID="upnlMsg" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr>
                <td align="left" id="trSearch" runat="server" visible="false">
                </td>
                <td align="right">
                    <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Search" ImageUrl="../images/xls.png"
                        Visible="false" />
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left" valign="top">
                    <%--test by Amit--%>
                    <trirand:JQGrid runat="server" ID="jgrdRCV" DataSourceID="sqldsRcv" AutoWidth="True"
                        Height="300px" OnCellBinding="jgrdRCV_CellBinding" OnDataRequesting="jgrdRCV_DataRequesting">
                        <Columns>
                            <trirand:JQGridColumn DataField="recID" HeaderText="" PrimaryKey="True" Visible="false" />
                            <trirand:JQGridColumn DataField="recPOID" HeaderText="<%$ Resources:Resource, PONumber %>"
                                Editable="false" />
                            <trirand:JQGridColumn DataField="vendorID" HeaderText="<%$ Resources:Resource, grdPOVendorID %>"
                                Editable="false" />
                            <trirand:JQGridColumn DataField="vendorName" HeaderText="<%$ Resources:Resource, POVendorName %>"
                                Editable="false" />
                            <trirand:JQGridColumn DataField="poStatus" HeaderText="<%$ Resources:Resource, grdPOStatus %>"
                                Editable="false" />
                            <trirand:JQGridColumn DataField="recDateTime" HeaderText="<%$ Resources:Resource, grdReceivingDate %>"
                                Editable="false" DataFormatString="{0:MMM dd, yyyy}" />
                            <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="vendorID"
                                Sortable="false" TextAlign="Center" Width="100" />
                        </Columns>
                        <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                            ShowDeleteButton="false" ShowSearchButton="false" />
                        <SortSettings InitialSortColumn=""></SortSettings>
                        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                        <ClientSideEvents LoadComplete="loadComplete" />
                        <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" />
                    </trirand:JQGrid>
                    <%--test Ends here--%>
                    <asp:SqlDataSource ID="sqldsRcv" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                        ProviderName="MySql.Data.MySqlClient" OnSelected="sqldsUser_Selected"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr height="30" id="trNotes" runat="server">
                <td class="tdAlign">
                    <asp:Label ID="lblNotes" CssClass="lblBold" Text="<%$ Resources:Resource, PONotes%>"
                        runat="server" />
                </td>
                <td class="tdAlignLeft">
                    <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Width="400px">
                    </asp:TextBox><asp:HiddenField ID="POID" runat="server" />
                </td>
            </tr>
            <tr height="30" id="trStatus" runat="server">
                <td class="tdAlign">
                    <asp:Label ID="lblPOStatus" CssClass="lblBold" Text="<%$ Resources:Resource, lblStatus%>"
                        runat="server" />
                </td>
                <td class="tdAlignLeft">
                    <asp:DropDownList ID="dlPOStatus" runat="server" Width="135px">
                        <asp:ListItem Value="" Text="" />
                        <asp:ListItem Value="N" Text="<%$ Resources:Resource, liGenerated %>" />
                        <asp:ListItem Value="S" Text="<%$ Resources:Resource, liSubmitted %>" />
                        <asp:ListItem Value="A" Text="<%$ Resources:Resource, liAccepted %>" />
                        <asp:ListItem Value="Z" Text="<%$ Resources:Resource, liNotAccepted %>" />
                        <asp:ListItem Value="H" Text="<%$ Resources:Resource, liHeld %>" />
                        <asp:ListItem Value="R" Text="<%$ Resources:Resource, liReadyForReceiving %>" />
                        <asp:ListItem Value="C" Text="<%$ Resources:Resource, liClosed %>" />
                    </asp:DropDownList>
                    <span class="style1">*</span>
                    <asp:CustomValidator ID="custvalPOStatus" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, custvalPOStatus%>"
                        ClientValidationFunction="funCheckPOStatus" Display="None">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr height="30" id="trSave" runat="server">
                <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                    <asp:Button runat="server" ID="cmdSave" CssClass="imgSave" OnClick="cmdSave_Click" />&nbsp;&nbsp;
                    <asp:Button runat="server" ID="cmdReset" CssClass="imgReset" CausesValidation="false"
                        OnClick="cmdReset_Click" />
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <div>
                        <asp:Button ID="btncmdBack" runat="server" Text="<%$ Resources:Resource,cmdInvBack%>"
                            CausesValidation="false" OnClick="btncmdBack_Click" Width="150px" />
                    </div>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsProcurement" runat="server" ShowMessageBox="true"
            ShowSummary="false" />
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        function funCheckPOStatus(source, args) {
            if (document.getElementById('<%=dlPOStatus.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
    </script>
    <script type="text/javascript">
        //Variables
        var gridID = "<%=jgrdRCV.ClientID %>";
        var searchPanelID = "<%=SearchPanel.ClientID %>";


        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {

            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        function loadComplete(data) {
            if (data.rows.length == 1 && data.total == 1) {
                var vSrchKey = $("#ctl00_ctl00_cphFullWidth_cphLeft_txtSearch").val();
                var vSrchBy = $("#ctl00_ctl00_cphFullWidth_cphLeft_dlSearch :selected").val();
                location.href = 'RecevingDetails.aspx?rcvID=' + data.rows[0].id + '&opt=' + vSrchBy + '&srh=' + vSrchKey + '&refferUrl=';
            }

        }
        
        

    </script>
</asp:Content>

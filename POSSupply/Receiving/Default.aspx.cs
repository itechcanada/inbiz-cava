﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Receiving_Default : BasePage
{   
    // Private del_objRcv As New clsReceiving
    private Receiving objRcv = new Receiving();
    // On Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
       //Security
        if (!CurrentUser.IsInRole(RoleID.RECEIVING_MANAGER) && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR)) {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }
                       
        if (!Page.IsPostBack)
        {
            SysWarehouses wh = new SysWarehouses();
            wh.FillAllWharehouse(null, dlWarehouse, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));
            if (dlWarehouse.Items.Count > 1) {
                dlWarehouse.Items.Insert(1, new ListItem(Resources.Resource.liAllWarehouse, "ALL"));
            }
            else
            {
                dlWarehouse.Items.Add(new ListItem(Resources.Resource.liAllWarehouse, "ALL"));
            }
    
            txtSearch.Attributes.Add("onKeyPress", "doClick('" + btncmdUPC.ClientID + "',event)");
            txtSearch.Focus();
        }
    }
    
    // Search Sub Routine
    private void Search()
    {
        if (string.IsNullOrEmpty(txtSearch.Text))
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.PlzEntSearchCriteria);
            txtSearch.Focus();
        }
    }
    
    
    protected void btncmdPO_Click(object sender, System.EventArgs e)
    {
        if (string.IsNullOrEmpty(txtSearch.Text))
        {
            Search();
            return;
        }
        objRcv.RecPOID = BusinessUtility.GetInt(txtSearch.Text);
        objRcv.RecAtWhsCode = dlWarehouse.SelectedValue;
        if (objRcv.CheckRecevingForPO(objRcv.RecAtWhsCode, objRcv.RecPOID) == true)
        {
            Response.Redirect("ViewReceiving.aspx?opt=PO&srh=" + Server.UrlEncode(txtSearch.Text) + "&whs=" + dlWarehouse.SelectedValue);
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);            
        }
    }
    //'Search by Receipt
    protected void btncmdReceipt_Click(object sender, System.EventArgs e)
    {
        if (string.IsNullOrEmpty(txtSearch.Text))
        {
            Search();
            return;
        }
        objRcv.RecID = BusinessUtility.GetInt(txtSearch.Text);
        objRcv.RecAtWhsCode = dlWarehouse.SelectedValue;
        if (objRcv.CheckRecevingForReceiptNo(objRcv.RecAtWhsCode, objRcv.RecID) == true)
        {
            Response.Redirect("ViewReceiving.aspx?opt=RN&srh=" + Server.UrlEncode(txtSearch.Text) + "&whs=" + dlWarehouse.SelectedValue);
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);            
        }
    }
    //'Search by Vendor
    protected void btncmdVendor_Click(object sender, System.EventArgs e)
    {
        if (string.IsNullOrEmpty(txtSearch.Text))
        {
            Search();
            return;
        }
        objRcv.RecAtWhsCode = dlWarehouse.SelectedValue;
        string SearchData = txtSearch.Text;
        if (objRcv.CheckRecevingForVendor(SearchData, objRcv.RecAtWhsCode) == true)
        {
            Response.Redirect("ViewReceiving.aspx?opt=VI&srh=" + Server.UrlEncode(txtSearch.Text) + "&whs=" + dlWarehouse.SelectedValue);
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);            
        }

    }
    //'Search by UPC Code
    protected void btncmdUPC_Click(object sender, System.EventArgs e)
    {
        if (string.IsNullOrEmpty(txtSearch.Text))
        {
            Search();
            return;
        }
        objRcv.RecAtWhsCode = dlWarehouse.SelectedValue;
        string SearchData = txtSearch.Text;
        if (objRcv.CheckRecevingForUPCCode(SearchData, objRcv.RecAtWhsCode) == true)
        {
            Response.Redirect("ViewReceiving.aspx?opt=UC&srh=" + Server.UrlEncode(txtSearch.Text) + "&whs=" + dlWarehouse.SelectedValue);
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);         
        }
    }
    //'Search by Today
    protected void btncmdToday_Click(object sender, System.EventArgs e)
    {
        objRcv.RecAtWhsCode = dlWarehouse.SelectedValue;        
        if (objRcv.CheckReceving("T", objRcv.RecAtWhsCode) == true)
        {
            Response.Redirect("ViewReceiving.aspx?opt=T&srh=" + Server.UrlEncode(txtSearch.Text) + "&whs=" + dlWarehouse.SelectedValue);
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);         
        }
    }
    //'Search by This Week
    protected void btncmdThisWeek_Click(object sender, System.EventArgs e)
    {
        objRcv.RecAtWhsCode = dlWarehouse.SelectedValue;
        if (objRcv.CheckReceving("C", objRcv.RecAtWhsCode) == true)
        {
            Response.Redirect("ViewReceiving.aspx?opt=C&srh=" + Server.UrlEncode(txtSearch.Text) + "&whs=" + dlWarehouse.SelectedValue);
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);         
        }
    }
    //'Search by Next Week
    protected void btncmdNextWeek_Click(object sender, System.EventArgs e)
    {
        objRcv.RecAtWhsCode = dlWarehouse.SelectedValue;
        if (objRcv.CheckReceving("N", objRcv.RecAtWhsCode) == true)
        {
            Response.Redirect("ViewReceiving.aspx?opt=N&srh=" + Server.UrlEncode(txtSearch.Text) + "&whs=" + dlWarehouse.SelectedValue);
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);         
        }
    }
    //'Search by All
    protected void btncmdAll_Click(object sender, System.EventArgs e)
    {
        objRcv.RecAtWhsCode = dlWarehouse.SelectedValue;
        if (objRcv.CheckReceving("A", objRcv.RecAtWhsCode) == true)
        {
            Response.Redirect("ViewReceiving.aspx?opt=A&srh=" + Server.UrlEncode(txtSearch.Text) + "&whs=" + dlWarehouse.SelectedValue);
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);         
        }
    }
}

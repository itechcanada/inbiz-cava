﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UpdateReceivedQty.aspx.vb" Inherits="Receiving_UpdateReceivedQty" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="5" width="100%">
            <tr>
                <td style="width:30%">
                    Received Quantity
                </td>
                <td>
                    <asp:TextBox ID="txtRecQty" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width:30%">
                    Received Date
                </td>
                <td>
                    <asp:TextBox ID="txtReceivedDate" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

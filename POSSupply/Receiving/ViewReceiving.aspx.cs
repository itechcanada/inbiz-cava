﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Receiving_ViewReceiving : BasePage
{
    private Receiving objRcv = new Receiving();
    private PurchaseOrders objPO = new PurchaseOrders();
    string _searchText, _whs, _opt;
    // On Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security 
        if (!CurrentUser.IsInRole(RoleID.RECEIVING_MANAGER) && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        _opt = BusinessUtility.GetString(Request.QueryString["opt"]);
        _searchText = BusinessUtility.GetString(Request.QueryString["srh"]);
        _whs = BusinessUtility.GetString(Request.QueryString["whs"]);

        // lblTitle.Text = lblReceiving
        if (!IsPagePostBack(jgrdRCV))
        {
            Session["RCV_BACK_URL"] = Request.RawUrl;
            SysWarehouses wh = new SysWarehouses();
            wh.FillWharehouse(null, dlWarehouse, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));
            if (dlWarehouse.Items.Count > 1)
            {
                dlWarehouse.Items.Insert(1, new ListItem(Resources.Resource.liAllWarehouse, "ALL"));
            }
            else
            {
                dlWarehouse.Items.Add(new ListItem(Resources.Resource.liAllWarehouse, "ALL"));
            }

            FillSearchOptions();
            trNotes.Visible = false;
            trStatus.Visible = false;
            trSave.Visible = false;
        }
    }

    //Populate Grid
    public void FillSearchOptions()
    {
        if (Request.QueryString.Count > 0)
        {
            try
            {
                dlSearch.SelectedValue = BusinessUtility.GetString(Request.QueryString["opt"]);
                dlWarehouse.SelectedValue = BusinessUtility.GetString(Request.QueryString["whs"]);
                txtSearch.Text = BusinessUtility.GetString(Request.QueryString["srh"]);

                if (BusinessUtility.GetString(Request.QueryString["opt"]) != "" && BusinessUtility.GetString(Request.QueryString["srh"]) == "")
                {
                    txtSearch.Focus();
                }
                else if (BusinessUtility.GetString(Request.QueryString["whs"]) != "" && BusinessUtility.GetString(Request.QueryString["opt"]) == "")
                {

                    dlSearch.Focus();
                }
                else if (BusinessUtility.GetString(Request.QueryString["whs"]) == "")
                {
                    dlWarehouse.Focus();
                }
                else
                {
                    txtSearch.Focus();
                }
            }
            catch
            { }
        }
    }

    protected void sqldsUser_Selected(object sender, System.Web.UI.WebControls.SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows == 0 & string.IsNullOrEmpty(lblMsg.Text))
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);
            dlPOStatus.SelectedValue = "";
            txtNotes.Text = "";
            POID.Value = "";
            trNotes.Visible = false;
            trStatus.Visible = false;
            trSave.Visible = false;
        }
    }

    //Populate Object
    private void SetData(PurchaseOrders objPurOrd)
    {
        objPurOrd.PoStatus = dlPOStatus.SelectedValue;
        objPurOrd.PoNotes = txtNotes.Text;
        objPurOrd.PoAuthroisedByUserID = CurrentUser.UserID;
    }
    //On Submit
    protected void cmdSave_Click(object sender, System.EventArgs e)
    {
        if (!Page.IsValid)
        {
            return;
        }
        objPO.PoID = BusinessUtility.GetInt(POID.Value);
        SetData(objPO);
        if (objPO.UpdateOnApprove(null) == true)
        {
            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.PurchaseOrderUpdatedSuccessfully);
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.PurchaseOrderNotUpdatedSuccessfully);
        }
        dlPOStatus.SelectedValue = "";
        txtNotes.Text = "";
        POID.Value = "";
        trNotes.Visible = false;
        trStatus.Visible = false;
        trSave.Visible = false;
    }
    //On reset
    protected void cmdReset_Click(object sender, System.EventArgs e)
    {
        dlPOStatus.SelectedValue = "";
        txtNotes.Text = "";
        POID.Value = "";
    }


    protected void btncmdBack_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("~/Receiving/Default.aspx");
    }


    protected void jgrdRCV_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 0)
        {
            if (string.IsNullOrEmpty(BusinessUtility.GetString(e.RowValues[0])))
            {
                e.CellHtml = "N/A";
            }
        }
        if (e.ColumnIndex == 1)
        {
            if (string.IsNullOrEmpty(BusinessUtility.GetString(e.RowValues[1])))
            {
                e.CellHtml = "N/A";
            }
        }
        if (e.ColumnIndex == 2)
        {
            if (string.IsNullOrEmpty(BusinessUtility.GetString(e.RowValues[2])))
            {
                e.CellHtml = "N/A";
            }
        }
        if (e.ColumnIndex == 3)
        {
            if (string.IsNullOrEmpty(BusinessUtility.GetString(e.RowValues[3])))
            {
                e.CellHtml = "N/A";
            }
        }
        if (e.ColumnIndex == 4)
        {
            string strData = "";
            switch (e.RowValues[4].ToString())
            {
                case POStatus.NEW:
                    strData = Resources.Resource.liGenerated;
                    break;
                case POStatus.SUBMITTED:
                    strData = Resources.Resource.liSubmitted;
                    break;
                case POStatus.READY_FOR_RECEIVING:
                    strData = Resources.Resource.liReadyForReceiving;
                    break;
                case POStatus.ACCEPTED:
                    strData = Resources.Resource.liAccepted;
                    break;
                case POStatus.NOT_ACCEPTED:
                    strData = Resources.Resource.liNotAccepted;
                    break;
                case POStatus.HELD:
                    strData = Resources.Resource.liHeld;
                    break;
                case POStatus.RECEIVED:
                    strData = Resources.Resource.msgReceived;
                    break;
                case POStatus.CLOSED:
                    strData = Resources.Resource.liClosed;
                    break;
            }
            // e.RowValues(2) = strData
            e.CellHtml = strData;
            if (string.IsNullOrEmpty(BusinessUtility.GetString(e.RowValues[4])))
            {
                e.CellHtml = "N/A";
            }
        }
        if (e.ColumnIndex == 5)
        {
            if (string.IsNullOrEmpty(BusinessUtility.GetString(e.RowValues[5])))
            {
                e.CellHtml = "N/A";
            }
        }
        if (e.ColumnIndex == 6)
        {
            string vSrchKey = BusinessUtility.GetString(Request.QueryString["srh"]);
            string vSrchBy = BusinessUtility.GetString(Request.QueryString["opt"]);
            string backUrl = Session["RCV_BACK_URL"] != null ? (string)Session["RCV_BACK_URL"] : string.Empty; ;
            string strurl = string.Format("RecevingDetails.aspx?rcvID={0}&refferUrl={1}&opt={2}&srh={3}", e.RowValues[0], Server.UrlEncode(backUrl), vSrchBy, vSrchKey); //"RecevingDetails.aspx?rcvID=" + e.RowValues[0];
            //e.CellHtml = string.Format("<a href=\"{0}\">{1}</a>", strurl, Resources.Resource.lblEdit); //onclick='window.href={0}'//ui-button ui-widget ui-state-default ui-corner-all
            //e.CellHtml = string.Format("<input type='submit'  onclick='Testfun(" + " {0} " + ");' class='ui-button ui-widget ui-state-default ui-corner-all' value = '{1}'></input>", HttpUtility.UrlEncode(strurl), Resources.Resource.lblEdit);
            e.CellHtml = string.Format(" <a href=\"{0}\"><input type='button' class='ui-state-default ui-button ui-widget ui-corner-all' style='width:100px;font-size:13px;' value = '{1}'></input></a>", strurl, Resources.Resource.lblEdit);
        }

    }

    protected void jgrdRCV_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains<string>("_history"))
        {
            sqldsRcv.SelectCommand = objRcv.FillGrid(sqldsRcv.SelectParameters, Request.QueryString[dlSearch.ClientID], Request.QueryString[txtSearch.ClientID], Request.QueryString[dlWarehouse.ClientID]);
        }
        else
        {
            sqldsRcv.SelectCommand = objRcv.FillGrid(sqldsRcv.SelectParameters, _opt, _searchText, _whs);
        }
    }
}
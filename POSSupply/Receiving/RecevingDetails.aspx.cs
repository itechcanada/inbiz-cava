﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.InbizERP.BusinessLogic;

using iTECH.Library.DataAccess.MySql;

public partial class Receiving_RecevingDetails : BasePage
{
    //private Receiving _objRcv = new Receiving();    
    //private PurchaseOrders _objPO = new PurchaseOrders();
    //PurchaseOrderItems _poItems = new PurchaseOrderItems();    

    //On Page Load

    string _searchText, _whs, _opt;

    protected void Page_Load(object sender, EventArgs e)
    {
        _opt = BusinessUtility.GetString(Request.QueryString["opt"]);
        _searchText = BusinessUtility.GetString(Request.QueryString["srh"]);
        _whs = BusinessUtility.GetString(Request.QueryString["whs"]);

        if (!IsPagePostBack(jqgrdPOItems))
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                new SysWarehouses().FillWharehouse(dbHelp, dlWarehouse, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));
                cmdUpdateReceivingStatus.Attributes.Add("onclick", "javascript:return confirm('" + Resources.Resource.msgIsReceivedQuantityUpdated + "')");

                if (this.ReceivingID > 0)
                {
                    lblRCVNo.Text = this.ReceivingID.ToString();
                }
                //if (_opt != "")
                //{
                //    dlSearch.SelectedValue = _opt;
                //}
                //if (_searchText != "")
                //{
                //    txtSearch.Text = _searchText;
                //}

                dlSearch.SelectedValue = BusinessUtility.GetString(Request.QueryString["opt"]);
                txtSearch.Text = BusinessUtility.GetString(Request.QueryString["srh"]);

                if (_opt != "" && _searchText == "")
                {
                    txtSearch.Focus();
                }
                else if (_whs != "" && _opt == "")
                {

                    dlSearch.Focus();
                }
                else if (_whs == "")
                {
                    dlWarehouse.Focus();
                }
                else
                {
                    txtSearch.Focus();
                }

            }
            catch
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }


    //Set Data for Alert
    //private void subSetData(clsAlerts objA)
    //{
    //    objA.AlertPartnerContactID = this.ReceivingID.ToString();
    //    objA.AlertComID = _objRcv.RecPOID.ToString();
    //    objA.AlertDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
    //    //objA.AlertUserID = Session["UserID"]
    //    string strNote = "";
    //    strNote += Resources.Resource.alrtRcvQtyOfPrd + "'" + objPOItems.ProdIDDesc + "' ";
    //    //Quantity of Product 
    //    if ((objPOItems.GetRemainingPOItemsQty(objPOItems.PoItems) < 0))
    //    {
    //        strNote += Resources.Resource.alrtRcvMoreThanQtyInPO;
    //        //Received is more than the Quantity mentioned in PO 
    //    }
    //    else
    //    {
    //        strNote += Resources.Resource.alrtRcvLessThanQtyInPO;
    //        //Received is less than the Quantity mentioned in PO
    //    }
    //    strNote += "<br/><br/>PO:" + objPOItems.PoID + " <br/>";
    //    strNote += Resources.Resource.alrtRcvPOQty + " " + objPOItems.PoQty + "<br/>";
    //    //PO Quantity:
    //    strNote += Resources.Resource.alrtRcvRcvQty + " " + objPOItems.PoRcvdQty + "<br/>";
    //    //Received Quantity:
    //    objUsers.UserID = CurrentUser.UserID;
    //    objUsers.PopulateObject(CurrentUser.UserID);
    //    strNote += Resources.Resource.alrtRcvRcvUser + " " + objUsers.UserFirstName + " " + objUsers.UserLastName;
    //    //Receiving User:
    //    objA.AlertNote = strNote;
    //    objA.AlertRefType = "RCV";
    //}

    // Inserts Alert if Received Quantity is different from Ordered Quantity.
    //protected void GenerateAlert(string POItemID)
    //{
    //    double remainQty = objPOItems.GetRemainingPOItemsQty(POItemID);
    //    if (remainQty != 0) {
    //        clsAlerts objAlert = new clsAlerts();
    //        subSetData(objAlert);
    //        int[] userIds = objUsers.GetUserIDsByRole(RoleID.PROCURMENT_MANAGER);
    //        foreach (int user in userIds)
    //        {
    //            objAlert.AlertUserID = user.ToString();
    //            objAlert.insertAlerts();
    //        }
    //    }
    //}

    protected void jqgrdPOItems_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 8)
        {
            string strurl = string.Format("AcceptReceiving.aspx?poitemid={0}&Qty={1}", e.RowValues[0], e.RowValues[5]); //"AcceptReceiving.aspx?poItemPrdID=" + e.RowValues[0] + "&Qty=" + e.RowValues[5];           
            //e.CellHtml = string.Format("<a class=\"Accept-Receiving\" href=\"{0}\">{1}</a>", strurl, Resources.Resource.edit);
            e.CellHtml = string.Format(" <a class=\"Accept-Receiving\" href=\"{0}\"><input type='button' class='ui-state-default ui-button ui-widget ui-corner-all' style='width:100px;font-size:13px;' value = '{1}'></input></a>", strurl, Resources.Resource.lblEdit);

            //string sClick = "onclick=AcceptReceiving('" + e.RowValues[0] + "', '" + e.RowValues[5] + "')";
            //e.CellHtml = string.Format(" <input type='button' class='ui-state-default ui-button ui-widget ui-corner-all' style='width:100px;font-size:13px;' '" + sClick + "' value = '{0}'></input>", Resources.Resource.lblEdit);
        }
    }

    protected void jqgrdPOItems_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        //if (this.ReceivingID > 0)
        //{
        //    Receiving rcv = new Receiving();
        //    rcv.PopulateObject(null, this.ReceivingID);
        //    sqldsPOItems.SelectCommand = rcv.GetSql(sqldsPOItems.SelectParameters, rcv.RecAtWhsCode, rcv.RecPOID, rcv.RecID,Globals.CurrentAppLanguageCode);
        //}


        if (Request.QueryString.AllKeys.Contains<string>("_history"))
        {
            if (this.ReceivingID > 0)
            {
                Receiving rcv = new Receiving();
                rcv.PopulateObject(null, this.ReceivingID);
                sqldsPOItems.SelectCommand = rcv.GetSql(sqldsPOItems.SelectParameters, rcv.RecAtWhsCode, rcv.RecPOID, rcv.RecID, Globals.CurrentAppLanguageCode, Request.QueryString[dlSearch.ClientID], Request.QueryString[txtSearch.ClientID]);
            }

            //sqldsRcv.SelectCommand = objRcv.FillGrid(sqldsRcv.SelectParameters, Request.QueryString[dlSearch.ClientID], Request.QueryString[txtSearch.ClientID], Request.QueryString[dlWarehouse.ClientID]);
        }
        else
        {
            if (this.ReceivingID > 0)
            {
                Receiving rcv = new Receiving();
                rcv.PopulateObject(null, this.ReceivingID);
                sqldsPOItems.SelectCommand = rcv.GetSql(sqldsPOItems.SelectParameters, rcv.RecAtWhsCode, rcv.RecPOID, rcv.RecID, Globals.CurrentAppLanguageCode, _opt, _searchText);
            }

            //sqldsRcv.SelectCommand = objRcv.FillGrid(sqldsRcv.SelectParameters, _opt, _searchText, _whs);

        }

    }

    protected void cmdUpdateReceivingStatus_Click(object sender, System.EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            Receiving rcv = new Receiving();
            PurchaseOrders po = new PurchaseOrders();
            PurchaseOrderItems poItms = new PurchaseOrderItems();
            if (this.ReceivingID > 0)
            {
                rcv.RecID = this.ReceivingID;
                rcv.PopulateObject(dbHelp, this.ReceivingID);
                po.PopulateObject(dbHelp, rcv.RecPOID);

                if (poItms.GetRemainingItemQtyToReceive(dbHelp, rcv.RecPOID) == 0)
                {
                    po.UpdatePO(dbHelp, rcv.RecPOID, POStatus.RECEIVED, po.PoShpTerms);
                    rcv.RecByUserID = CurrentUser.UserID;
                    rcv.RecDateTime = DateTime.Now;
                    rcv.Update(dbHelp);
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCanNotClosePO);
                    //Can not Close the PO - Received Qty is not equal to Ordered Qty.
                    return;
                }
            }
            //Response.Redirect("ViewReceiving.aspx?upd=1" & "&opt=" & Request.QueryString("opt").ToString & "&srh=" & Request.QueryString("srh").ToString & "&whs=" & Request.QueryString("whs").ToString)
            Response.Redirect(this.RefferUrl, false);
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private int ReceivingID
    {
        get
        {
            int rid = 0;
            int.TryParse(Request.QueryString["rcvID"], out rid);
            return rid;
        }
    }

    private string RefferUrl
    {
        get
        {
            return !string.IsNullOrEmpty(Request.QueryString["refferUrl"]) ? Server.UrlDecode(Request.QueryString["refferUrl"]) : "Default.aspx";
        }
    }


    protected void btnUpdateAndCloseReceiving_Click(object sender, EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);
        PurchaseOrders po = new PurchaseOrders();
        PurchaseOrderItems poItms = new PurchaseOrderItems();
        Receiving rcv = new Receiving();

        try
        {
            dbHelp.OpenDatabaseConnection();
            rcv.PopulateObject(dbHelp, this.ReceivingID);
            var lstItems = poItms.GetItems(dbHelp, rcv.RecPOID);
            PurchaseOrderItems poItmToUpdate;
            RceivedItems rcvItemsToUpdate;
            po.PopulateObject(dbHelp, rcv.RecPOID);

            foreach (var item in lstItems)
            {
                rcvItemsToUpdate = new RceivedItems();
                rcvItemsToUpdate.RecId = rcv.RecID;
                rcvItemsToUpdate.PoItems = item.PoItems;
                rcvItemsToUpdate.RcvdQty = BusinessUtility.GetInt(item.PoQty - item.PoRcvdQty); //Make whole remaining quantity as received because we are just closing this Po
                rcvItemsToUpdate.PoItemRcvDateTime = DateTime.Now;


                poItmToUpdate = new PurchaseOrderItems();
                poItmToUpdate.PopulateObject(dbHelp, item.PoItems);
                poItmToUpdate.PoItemRcvDateTime = rcvItemsToUpdate.PoItemRcvDateTime;
                poItmToUpdate.PoRcvdQty = item.PoQty;

                //Update units for vendor in vendor association table Implemented on 2013-01-09                               
                ProductAssociateVendor pVnd = new ProductAssociateVendor();
                pVnd.NoOfUnits = item.PoQty;
                pVnd.PrdCostPrice = item.PoUnitPrice;
                pVnd.PrdID = item.PoItemPrdId;
                pVnd.PrdVendorID = po.PoVendorID;
                pVnd.PurchaseDate = poItmToUpdate.PoItemRcvDateTime;
                pVnd.PurchaseOrderNo = item.PoID;
                pVnd.UpdateUnitsOnReceiving(dbHelp);

                if (poItmToUpdate.UpdatePurchaseOrderItemQuantity(dbHelp, poItmToUpdate.PoItems, poItmToUpdate.PoRcvdQty, poItmToUpdate.PoItemRcvDateTime))
                {
                    if (poItmToUpdate.UpdateReceivedProductQuantity(dbHelp, poItmToUpdate.PoItemPrdId, poItmToUpdate.PoItemShpToWhsCode, rcvItemsToUpdate.RcvdQty))
                    {
                        rcvItemsToUpdate.Insert(dbHelp);
                    }
                }
            }

            //To do to close receiving
            //Update Pos Status to closed
            po.UpdatePO(dbHelp, rcv.RecPOID, POStatus.RECEIVED, string.Empty);

            //Update receiving
            rcv.RecByUserID = CurrentUser.UserID;
            rcv.RecDateTime = DateTime.Now;
            rcv.Update(dbHelp);



            //To Do to generate alerts

            Response.Redirect(this.RefferUrl, false);
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError + ex.Message);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;

public partial class Receiving_QuickReceiving : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!CurrentUser.IsInRole(RoleID.RECEIVING_MANAGER) && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        if (!IsPostBack)
        {
            SysWarehouses wh = new SysWarehouses();
            wh.FillWharehouse(null, dlWarehouses, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));
            liSalesQty.Visible = false;
            liSalesOrder.Visible = false;
            txtSKU.Focus();
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                if (rblAutoCommit.SelectedItem.Value == "0" && txtQty.Text == "0")
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblEnterQty);
                    txtQty.Focus();
                    return;
                }

                Product objProduct = new Product();
                int productID = objProduct.GetProductID(null, txtSKU.Text);
                if (productID > 0)
                {
                    // Chk Orders for BarCode
                    Receiving objRcv = new Receiving();
                    DataTable dtOrder = objRcv.CheckPOForSKU(dlWarehouses.SelectedItem.Value, txtSKU.Text);

                    if (ddlOrderList.Items.Count <= 0)
                    {

                        if (dtOrder != null)
                        {
                            ddlOrderList.Items.Clear();
                            if (dtOrder.Rows.Count > 1)
                            {
                                ListItem li = new ListItem();
                                li.Text = "";
                                li.Value = "";
                                ddlOrderList.Items.Add(li);
                                foreach (DataRow drow in dtOrder.Rows)
                                {
                                    ListItem lstItem = new ListItem();
                                    lstItem.Text = BusinessUtility.GetString(drow["poID"]);
                                    lstItem.Value = BusinessUtility.GetString(drow["poID"]);
                                    ddlOrderList.Items.Add(lstItem);
                                }

                                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblSelectOrder);
                                liSalesOrder.Visible = true;
                                ddlOrderList.Focus();
                                return;
                            }
                            else if (dtOrder.Rows.Count == 1)
                            {
                                foreach (DataRow drow in dtOrder.Rows)
                                {
                                    ListItem lstItem = new ListItem();
                                    lstItem.Text = BusinessUtility.GetString(drow["poID"]);
                                    lstItem.Value = BusinessUtility.GetString(drow["poID"]);
                                    ddlOrderList.Items.Add(lstItem);
                                }
                                ddlOrderList.Focus();
                            }
                        }
                    }

                    if (ddlOrderList.Items.Count <= 0)
                    {

                        MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblOrderNotFoundToReceive);
                        return;
                    }
                    else
                    {
                        if (ddlOrderList.SelectedItem.Value == "")
                        {
                            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblSelectOrder);
                            ddlOrderList.Focus();
                            return;
                        }
                    }

                    if (Receiving(productID) == true)
                    {
                        ResetPage();
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.lblReceivedSuccessfully);
                        txtSKU.Focus();
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
                    }

                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblInvalidSKU);
                }
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
            }
        }
    }


    protected bool Receiving(int productID)
    {
        DbHelper dbHelp = new DbHelper(true);
        bool status = false;
        try
        {
            int quantity = BusinessUtility.GetInt(txtQty.Text);
            
            PurchaseOrderItems poItms = new PurchaseOrderItems();
            poItms.PoID = BusinessUtility.GetInt(ddlOrderList.SelectedItem.Value);
            poItms.PoItemPrdId = productID;
            int poItemValues = poItms.GetPOItemValue(null, BusinessUtility.GetInt(ddlOrderList.SelectedItem.Value), productID); 

            //Populate Purchase Order Item       
            poItms.PopulateObject(dbHelp, BusinessUtility.GetInt(poItemValues));

            // Set Data for Receiving
            Receiving rcv = new Receiving();
            rcv.RecPOID = poItms.PoID;
            rcv.RecAtWhsCode = poItms.PoItemShpToWhsCode;
            rcv.RecByUserID = CurrentUser.UserID;
            rcv.RecDateTime = DateTime.Now;
            rcv.RecID = rcv.GetRecevingIDByPO(dbHelp, poItms.PoID);
            rcv.Update(dbHelp);

            //Set Data for Received items 
            RceivedItems rcvdItms = new RceivedItems();
            rcvdItms.RecId = rcv.RecID;
            rcvdItms.PoItems = poItms.PoItems;
            rcvdItms.RcvdQty = BusinessUtility.GetInt(txtQty.Text);
            rcvdItms.PoItemRcvDateTime = DateTime.Now;//BusinessUtility.GetDateTime(txtRcvDateTime.Text, DateFormat.MMddyyyy);
            if (rcvdItms.PoItemRcvDateTime == DateTime.MinValue)
            {
                rcvdItms.PoItemRcvDateTime = DateTime.Now;
            }

            poItms.PoItemRcvDateTime = rcvdItms.PoItemRcvDateTime;
            poItms.PoRcvdQty = poItms.PoRcvdQty + BusinessUtility.GetDouble(txtQty.Text);

            //Update units for vendor in vendor association table Implemented on 2013-01-09
            PurchaseOrders po = new PurchaseOrders();
            po.PopulateObject(dbHelp, poItms.PoID);
            ProductAssociateVendor pVnd = new ProductAssociateVendor();
            pVnd.NoOfUnits = BusinessUtility.GetDouble(txtQty.Text);
            pVnd.PrdCostPrice = poItms.PoUnitPrice;
            pVnd.PrdID = poItms.PoItemPrdId;
            pVnd.PrdVendorID = po.PoVendorID;
            pVnd.PurchaseDate = rcvdItms.PoItemRcvDateTime;
            pVnd.PurchaseOrderNo = poItms.PoID;
            pVnd.UpdateUnitsOnReceiving(dbHelp);


            // Updates PO Item Qty and Product Qty in Inventory
            if (poItms.UpdatePurchaseOrderItemQuantity(dbHelp, BusinessUtility.GetInt(poItms.PoItems), poItms.PoRcvdQty, poItms.PoItemRcvDateTime))
            {
                if (poItms.UpdateReceivedProductQuantity(dbHelp, poItms.PoItemPrdId, poItms.PoItemShpToWhsCode, BusinessUtility.GetDouble(txtQty.Text)))
                {
                    rcvdItms.Insert(dbHelp);
                    status = true;
                }

            }

        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
        return status;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ResetPage();
    }

    public void ResetPage()
    {
        liSalesOrder.Visible = false;
        liSalesQty.Visible = false;
        rblAutoCommit.SelectedItem.Value = "1";
        txtSKU.Text = "";
        txtQty.Text = "1";
        ddlOrderList.Items.Clear();
    }

    protected void rblAutoCommit_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblAutoCommit.SelectedItem.Value == "1")
        {
            liSalesQty.Visible = false;
            txtQty.Text = "1";
            txtSKU.Focus();
        }
        else if (rblAutoCommit.SelectedItem.Value == "0")
        {
            liSalesQty.Visible = true;
            txtQty.Text = "0";
            txtSKU.Focus();
        }
    }


    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

}
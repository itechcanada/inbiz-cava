﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="RecevingDetails.aspx.cs" Inherits="Receiving_RecevingDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <%= Resources.Resource.lblView%>:
            <%= Resources.Resource.lblVendor%>
        </b>
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif"
            CssClass="csv_export" />
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft" runat="Server">
    <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
        <div class="searchBar">
            <div class="header">
                <div class="title">
                    Search form</div>
                <div class="icon">
                    <img src="../../lib/image/iconSearch.png" style="width: 17px" /></div>
            </div>
            <h4>
                <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, liWarehouseLocation%>"
                    AssociatedControlID="dlWarehouse" CssClass="filter-key"></asp:Label>
            </h4>
            <div class="inner">
                <asp:DropDownList ID="dlWarehouse" runat="server" Width="185px" ValidationGroup="PrdSrch">
                </asp:DropDownList>
            </div>
            <h4>
                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"
                    AssociatedControlID="dlSearch" CssClass="filter-key"></asp:Label>
            </h4>
            <div class="inner">
                <asp:DropDownList ID="dlSearch" runat="server" Width="185px" ValidationGroup="PrdSrch">
                    <asp:ListItem Text="Search By" Value="" />
                    <%--<asp:ListItem Value="PO" Text="PO Number" />
                    <asp:ListItem Text="Receipt No" Value="RN" />
                    <asp:ListItem Text="Vendor ID" Value="VI" />--%>
                    <asp:ListItem Text="UPC Code" Value="UC" />
                    <asp:ListItem Text="Today" Value="T" />
                    <asp:ListItem Text="This Week" Value="C" />
                    <asp:ListItem Text="Next Week" Value="N" />
                    <asp:ListItem Text="All" Value="A" />
                </asp:DropDownList>
            </div>
            <h4>
                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword%>"
                    AssociatedControlID="txtSearch" CssClass="filter-key"></asp:Label>
            </h4>
            <div class="inner">
                <asp:TextBox runat="server" Width="180px" ID="txtSearch" ValidationGroup="PrdSrch"></asp:TextBox>
                &nbsp;</div>
            <div class="footer">
                <div class="submit">
                    <%--  <asp:Button runat="server" Text="Submit" ID="btnSubmit" ValidationGroup="PrdSrch" />--%>
                    <input id="btnSearch" type="button" value="search" />
                </div>
            </div>
        </div>
    </asp:Panel>
    <%--<uc1:ReceivingSearch ID="ReceivingSearch1" runat="server" />--%>
</asp:Content>
<asp:Content ID="Content4" runat="Server" ContentPlaceHolderID="cphMaster">
    <style type="text/css">
        .ui-jqgrid tr.jqgrow td
        {
            border-bottom-color: inherit;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            font-weight: normal;
            height: 50px;
            overflow: hidden;
            padding: 0 2px;
            white-space: pre;
        }
        body, td, th
        {
            color: #515151;
            font-family: Lucida Sans Unicode,Lucida Grande,sans-serif;
            font-size: 12px;
        }
    </style>
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <table border="0" cellpadding="5" cellspacing="5">
                <tr>
                    <td align="left" width="100px">
                        <asp:Label ID="lblRcv" Text="<%$ Resources:Resource, lblReceiptNo %>" CssClass="lblBold"
                            runat="server" Font-Bold="True" />
                    </td>
                    <td align="left" width="550px">
                        <asp:Label ID="lblRCVNo" CssClass="lblBold" runat="server" Font-Bold="True" />
                    </td>
                </tr>
            </table>
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="jqgrdPOItems" DataSourceID="sqldsPOItems" AutoWidth="True"
                    Height="300px" OnCellBinding="jqgrdPOItems_CellBinding" OnDataRequesting="jqgrdPOItems_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="poItemPrdId" HeaderText="<%$ Resources:Resource, POProductID %>"
                            PrimaryKey="True" />
                        <trirand:JQGridColumn DataField="prdUPCCode" HeaderText="<%$ Resources:Resource, grdPOUPCCode %>"
                            Editable="false" NullDisplayText="N/A" ConvertEmptyStringToNull="true" />
                        <trirand:JQGridColumn DataField="prdName" HeaderText="<%$ Resources:Resource, POProduct %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="textureName" HeaderText="<%$ Resources:Resource, lblGrdTexture %>"
                            Editable="false" Visible="false" />
                        <trirand:JQGridColumn DataField="colorName" HeaderText="<%$ Resources:Resource, lblPrdColor %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="sizeName" HeaderText="<%$ Resources:Resource, lblProductSize %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="Quantity" HeaderText="<%$ Resources:Resource, grdRcvQuantity %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="poItemRcvDateTime" HeaderText="<%$ Resources:Resource, grdReceivingDate %>"
                            Editable="false" NullDisplayText="N/A" ConvertEmptyStringToNull="true" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdRcvUpdateReceivedQuantity %>"
                            DataField="poItemPrdId" Sortable="false" TextAlign="Center" Width="173" />


                            <trirand:JQGridColumn DataField="poItems" HeaderText="<%$ Resources:Resource, POProductID %>" Visible="false"
                            />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <%--<ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" />--%>
                    <ClientSideEvents LoadComplete="loadComplete" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sqldsPOItems" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
                <iCtrl:IframeDialog ID="IframeDialog1" Height="350" Width="600" Title="<%$ Resources:Resource, lblUpdateReceivedQty %>"
                    Dragable="true" TriggerSelectorClass="Accept-Receiving" TriggerSelectorMode="Class"
                    UrlSelector="TriggerControlHrefAttribute" runat="server"><%--Height="360" Width="490"--%>
                </iCtrl:IframeDialog>
            </div>
            <div style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                <asp:Label ID="lblNote" runat="server" ForeColor="Red" Font-Bold="true" Text="<%$ Resources:Resource, lblRcvNote %>"></asp:Label>
                <div style="text-align: right;">
                    <asp:Button ID="btnUpdateAndCloseReceiving" runat="server" Text="<%$ Resources:Resource, lblUpdateAndCloseReceiving %>"
                        OnClick="btnUpdateAndCloseReceiving_Click" />
                    <asp:Button ID="cmdUpdateReceivingStatus" runat="server" Text="<%$ Resources:Resource, cmdCssCloseReceiving %>"
                        OnClick="cmdUpdateReceivingStatus_Click" />
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" language="javascript">
        function ConfirmMsg(msg) {
            if (Page_ClientValidate()) {
                return confirm(msg);
            }
            else {
                return false;
            }
        }
    </script>
    <%--<asp:TextBox ID="txtDateTest" runat="server" />     
   <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
        ValidationExpression="^(((0?[1-9]|1[012])/(0?[1-9]|1\d|2[0-8])|(0?[13456789]|1[012])/(29|30)|(0?[13578]|1[02])/31)/(19|[2-9]\d)\d{2}|0?2/29/((19|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(([2468][048]|[3579][26])00)))$" 
        runat="server" ErrorMessage="RegularExpressionValidator" 
        ControlToValidate="txtDateTest" ValidationGroup="ddd"></asp:RegularExpressionValidator>
    <asp:Button ID="Button1" runat="server" Text="Button" ValidationGroup="ddd" />--%>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=jqgrdPOItems.ClientID %>";
        var searchPanelID = "<%=SearchPanel.ClientID %>";


        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }


        function loadComplete(data) {
            if (data.rows.length == 1 && data.total == 1) {
                AcceptReceiving(data.rows[0].cell[9], data.rows[0].cell[6]);
                //location.href = 'CustomerEdit.aspx?custid=' + data.rows[0].id + '&pType=<%=Request.QueryString["pType"] %>&gType=<%=Request.QueryString["gType"] %>';
            }
            //            else if (data.rows.length == 0) {
            //                var qData = {};
            //                qData.pType = '<%=Request.QueryString["pType"] %>';
            //                qData.gType = '<%=Request.QueryString["gType"] %>';
            //                qData.smartGvId = $("#<%=txtSearch.ClientID%>").val();
            //                location.href = 'CustomerEdit.aspx?' + $.param(qData);
            //            }
            //            else {
            //                jqGridResize();
            //            }
        }


        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        //     Function used in Iframe control  
        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        $("#edit-skill").click(function () {
            return false;
        });


        function AcceptReceiving(itemID, Qty) {
            if (Qty > 0) {
                var postData = {};
                postData.poitemid = itemID;
                postData.Qty = Qty;
                var $dialog = jQuery.FrameDialog.create({ url: 'AcceptReceiving.aspx?' + $.param(postData),
                    title: "<%=Resources.Resource.lblUpdateReceivedQty %>",
                    modal: true,
                    width: 990,
                    height: 520,
                    autoOpen: false
                });
                $dialog.dialog('open');
            }
        }

    </script>
</asp:Content>

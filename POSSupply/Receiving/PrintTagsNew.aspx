﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintTagsNew.aspx.cs" Inherits="Receiving_PrintTagsNew" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="../lib/scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-plugins/jquery-framedialog-1.1.2.js" type="text/javascript"></script>
	<style>
@media all {
	.page-break	{ display: none; }
}

@media print {
	.page-break	{ display: block; page-break-before: always; }
}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:DataList ID="dlstTags" runat="server" BorderWidth="0px" CellPadding="1" RepeatColumns="1" RepeatDirection="Vertical" CellSpacing="0">
        <ItemTemplate>
		
    	<div class="page-break">
		 
			
			<div style="margin: 5px auto 0px auto;font-size:8px; font-family:Calibri; text-transform:uppercase;">
			<br/>
			<br/>
		
				<div style="font-size:10px;">
          <b> &nbsp;&nbsp;&nbsp;&nbsp;<%#Eval("Column2") %></b>
		  	
            &nbsp;&nbsp;&nbsp;&nbsp;<%#Eval("Column3") %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <%#Eval("Column6") %>&nbsp;<%#Eval("Column5") %></b>
			</div>		
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%#Eval("Column4") %>
             

 </div>		
			<div style="margin:7px 0px 0px 7px; font-size:10px; font-family:'<%#ConfigurationManager.AppSettings["BarCodeFont"]%>';">
            *<%#Eval("Column1") %>*
			 </div>	
			 
				
            
		</div>
        </ItemTemplate>
     </asp:DataList>  
    </form>

   <script language="javascript" type="text/javascript">
       var PrintCommandObject = null;

       function printPage() {
           if (PrintCommandObject) {
               try {
                   PrintCommandObject.ExecWB(6, 2);
                   PrintCommandObject.outerHTML = "";
               }
               catch (e) {
                   window.print();

               }
           }
           else {
               window.print();
           }
           var browserName = navigator.appName;
           if (navigator.appName != "Microsoft Internet Explorer") {
               window.close();
           }
           else {
               window.close();
           }
       }

       window.onload = function () {
           Minimize();
           if (navigator.appName == "Microsoft Internet Explorer") {
               // attach and initialize print command ActiveX object
               try {
                   var PrintCommand = '<object id="PrintCommandObject" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2" width="0" height="0"></object>';
                   document.body.insertAdjacentHTML('beforeEnd', PrintCommand);
               }
               catch (e) { }
           }
           setTimeout(printPage, 1);
       };


       function Minimize() {
           window.innerWidth = 10;
           window.innerHeight = 10;
           window.screenX = screen.width;
           window.screenY = screen.height;
           alwaysLowered = true;
       }

    </script>  
</body>
</html>


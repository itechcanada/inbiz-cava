﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Receiving_PrintTagsNew : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<PrintSchema> data = new List<PrintSchema>();

        List<PurchaseOrderCartItem> listcollection = (List<PurchaseOrderCartItem>)Session["ProductTagsToPrint"];
        if (listcollection != null)
        {
            if (listcollection.Count > 0)
            {
                //Vendor vc = new Vendor();
                //vc.PopulateObject(null, listcollection[0].VendorID);
                //SysCompanyInfo ci = new SysCompanyInfo();
                //ci.PopulateObject(CurrentUser.DefaultCompanyID);

                for (int j = 0; j < listcollection.Count; j++)
                {
                    Product.ProductClothDesc prd = new Product.ProductClothDesc();
                    prd.ProductID = listcollection[j].ProductID;
                    prd.getClothDesc(null, 0);

                    Product pd = new Product();
                    pd.PopulateObject(listcollection[j].ProductID);

                    ProductSize ps = new ProductSize();
                    ps.SizeID = prd.Size;
                    List<ProductSize> lPS = new List<ProductSize>();
                    lPS = ps.GetAllSizeList(null, "En");

                    ProductColor pc = new ProductColor();
                    pc.ColorID = prd.Color;
                    List<ProductColor> lPC = new List<ProductColor>();
                    lPC = pc.GetAllColorList(null, "fr");
                    if (lPS.Count > 0 && lPC.Count > 0)
                    {
                        for (int i = 1; i <= listcollection[j].Quantity; i++)
                        {
                            PrintSchema sch = new PrintSchema();
                            sch.Column1 = listcollection[j].UPCCode;

                            sch.Column2 = prd.Style;

                            sch.Column3 = lPS[0].SizeEn;// +"|" + lPS[0].SizeFr;//listcollection[j].ProductSize;
                            sch.Column4 = lPC[0].ColorFr;//ci.CompanyBasCur;
                            if (BusinessUtility.GetDouble(pd.PrdEndUserSalesPrice) > 0.0)
                            {
                                sch.Column6 = "$";
                                sch.Column5 = string.Format("{0:F}", BusinessUtility.GetDouble(pd.PrdEndUserSalesPrice));// +"/" + BusinessUtility.GetCurrencyString(pd.PrdEndUserSalesPrice, "fr-CA").Replace(" €", "").Replace("$", "");
                            }
                            data.Add(sch);
                        }
                    }
                }
            }

        }
        else if ((this.ProductIDs).Length > 0)
        {
            string[] valPrdIDs = ProductIDs.Split('~');
            string[] valPrdQties = ProductQties.Split('~');
            //foreach (string arrStr in valPrdIDs)
            if (valPrdIDs.Length == valPrdQties.Length)
            {
                for (int j = 0; j < valPrdIDs.Length; j++)
                {
                    Product.ProductClothDesc prd = new Product.ProductClothDesc();
                    prd.ProductID = BusinessUtility.GetInt(valPrdIDs[j]);
                    prd.getClothDesc(null, 0);

                    Product pd = new Product();
                    pd.PopulateObject(BusinessUtility.GetInt(valPrdIDs[j]));

                    ProductSize ps = new ProductSize();
                    ps.SizeID = prd.Size;
                    List<ProductSize> lPS = new List<ProductSize>();
                    lPS = ps.GetAllSizeList(null, "En");

                    ProductColor pc = new ProductColor();
                    pc.ColorID = prd.Color;
                    List<ProductColor> lPC = new List<ProductColor>();
                    lPC = pc.GetAllColorList(null, "fr");
                    if (lPS.Count > 0 && lPC.Count > 0)
                    {
                        for (int i = 1; i <= BusinessUtility.GetInt(valPrdQties[j]); i++)
                        {
                            PrintSchema sch = new PrintSchema();
                            sch.Column1 = pd.PrdUPCCode;

                            sch.Column2 = prd.Style;

                            sch.Column3 = lPS[0].SizeEn;// +"|" + lPS[0].SizeFr;//listcollection[j].ProductSize;
                            sch.Column4 = lPC[0].ColorFr;//ci.CompanyBasCur;
                            if (BusinessUtility.GetDouble(this.ProductPrice) > 0.0)
                            {
                                sch.Column6 = "$";
                                sch.Column5 = string.Format("{0:F}", BusinessUtility.GetDouble(this.ProductPrice));
                            }
                            data.Add(sch);
                        }
                    }
                }
            }
        }
        else
        {

            for (int i = 1; i <= this.Count; i++)
            {
                PrintSchema sch = new PrintSchema();
                sch.Column1 = this.Barcode;
                sch.Column2 = GetProductName();
                sch.Column3 = this.ProductSize;
                sch.Column4 = this.ProductColor;
                if (BusinessUtility.GetDouble(this.ProductPrice) > 0.0)
                {
                    sch.Column6 = "$";
                    sch.Column5 = string.Format("{0:F}", BusinessUtility.GetDouble(this.ProductPrice));
                }
                data.Add(sch);
            }
        }
        dlstTags.DataSource = data;
        dlstTags.DataBind();
        Session["ProductTagsToPrint"] = null;
    }

    private string GetCurrency()
    {
        if (!string.IsNullOrEmpty(this.CurrencyCode.Trim()))
        {
            return this.CurrencyCode.Trim().ToUpper();
        }
        return "CAD";
    }

    private string GetProductName()
    {
        if (this.ProductName.Trim().Length > 15)
        {
            return this.ProductName.Trim().Substring(0, 15);
        }
        return this.ProductName.Trim();
    }

    private string Barcode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["tag"]);
        }
    }

    public string ProductName
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["name"]);
        }
    }

    public string ProductSize
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["size"]);
        }
    }

    public string ProductColor
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["clr"]);
        }
    }

    public double ProductPrice
    {
        get
        {
            double p = 0;
            double.TryParse(Request.QueryString["price"], out p);
            return p;
        }
    }

    public string CurrencyCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["cur"]);
        }
    }

    public int Count
    {
        get
        {
            int c = 0;
            int.TryParse(Request.QueryString["count"], out c);
            return c;
        }
    }

    public string ProductIDs
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["Pids"]);
        }
    }
    public string ProductQties
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["pQties"]);
        }
    }
}
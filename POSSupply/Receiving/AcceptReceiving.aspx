﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AcceptReceiving.aspx.cs" Inherits="Receiving_AcceptReceiving" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div style="text-align: center;">
        <div style="margin: auto; padding:10px; text-align:left;">
            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
            <table border="0" cellpadding="5" cellspacing="5" width="100%">
                <tr>
                    <td style="width: 150px; text-align:left;">
                        <%= Resources.Resource.lblReceivedQty%>
                    </td>
                    <td style="text-align:left;">
                        <asp:TextBox ID="txtQuantity" runat="server" Text='' MaxLength="5" onkeypress="return isNumberKey(event)"></asp:TextBox>                        
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:left;">
                        <asp:RequiredFieldValidator ID="reqvalQty" runat="server" ControlToValidate="txtQuantity"
                            ValidationGroup="rcvDate" Display="None" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, msgRcvPlzEntNumericValue %>">
                        </asp:RequiredFieldValidator>
                       <%-- <asp:CompareValidator ID="compvalQty" ControlToValidate="txtQuantity" Operator="GreaterThanEqual"
                            ValueToCompare="0.001" Type="Double" runat="server" ErrorMessage="<%$ Resources:Resource, msgRcvPlzEntNumericValueGreaterThanZero %>"
                            ValidationGroup="rcvDate" Display="None" SetFocusOnError="true" /> --%>                       
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=Resources.Resource.lblReceivingDate %>
                    </td>
                    <td>
                        <asp:TextBox ID="txtRcvDateTime" CssClass="datepicker" runat="server" ValidationGroup="rcvDate"
                            Text='' MaxLength="10">
                        </asp:TextBox>
                        <asp:Label ID="lblDateTimeFormat" CssClass="lblBold" runat="server" Text="<%$ Resources:Resource, lblPODateFormat %>"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:RegularExpressionValidator ID="revalRcvDateTime" runat="server" ControlToValidate="txtRcvDateTime"
                            ValidationExpression="^(((0?[1-9]|1[012])/(0?[1-9]|1\d|2[0-8])|(0?[13456789]|1[012])/(29|30)|(0?[13578]|1[02])/31)/(19|[2-9]\d)\d{2}|0?2/29/((19|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(([2468][048]|[3579][26])00)))$"
                            SetFocusOnError="true" Display="None" ValidationGroup="rcvDate" ErrorMessage="<%$ Resources:Resource,  msgRcvPlzEntValidDate%>">
                        </asp:RegularExpressionValidator>                        
                    </td>
                </tr>
                <tr style="display:none;">
                    <td>
                        <%--<%= Resources.Resource.lblCreateTags%>--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkCreateTags" runat="server" Text="" Checked="true"  />                        
                    </td>
                </tr>
            </table>
            <table border="0" cellpadding="5" cellspacing="5" width="100%" >
                <tr style="display:none;">
                    <td style="width: 150px;">
                       <%-- <%= Resources.Resource.lblCreatePerProductTag%>--%>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkAutoGen" runat="server" Text="" Checked="false"  />                        
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px; text-align:left;">
                        <%= Resources.Resource.lblTag%>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTag" runat="server"></asp:TextBox>                        
                        <asp:ValidationSummary ID="valsReceving" runat="server" ShowMessageBox="true" ShowSummary="false"
                            ValidationGroup="rcvDate" />                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=Resources.Resource.lblPrintTags %>
                    </td>
                    <td>
                        <asp:CheckBox ID="chkPrintTags" runat="server" Text="" onclick='ToggelContainerDisplay("tagDesc", this);' />                        
                    </td>
                </tr>
                 
                <tr>
                    <td colspan="2">
                    </td>
                </tr>
            </table>
             <table id="tagDesc"" border="0" cellpadding="5" cellspacing="5" style="display: none;">
               <tr >
                    <td style="width: 150px;">
                        <%=Resources.Resource.lblPrice%>
                    </td>
                    <td>
                        <asp:TextBox ID ="txtPrice" runat="server" MaxLength="10" CssClass="numericTextField" ></asp:TextBox>  
                        <%-- <asp:CompareValidator ID="CompareValidator2" EnableClientScript="true" SetFocusOnError="true"
                    ControlToValidate="txtPrice" Operator="DataTypeCheck" Type="Double" ErrorMessage="<%$ Resources:Resource, CompvalPrdFobPrice %>"
                    runat="server">*</asp:CompareValidator> --%>           
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    </td>
                </tr>
            </table>
            <div align="right">
<%--                <asp:Button ID="cmdSubmit" runat="server" Text="<%$ Resources:Resource,cmdCssUpdate%>"
                    OnClientClick="return ConfirmMsg('Click Ok to update quantity')" OnClick="cmdSubmit_Click" />--%>
                                    <asp:Button ID="cmdSubmit" runat="server" Text="<%$ Resources:Resource,cmdCssUpdate%>"
                    OnClientClick="return confimrReceving()" OnClick="cmdSubmit_Click" />
            </div>
        </div>
    </div>
    <asp:SqlDataSource ID="sqldsPOItems" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
        ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript" language="javascript">


        function confimrReceving() {
            return confirm("<%=Resources.Resource.msgCnfrmReceivingOrder%>");
        }

        function AutoGenerate(ele) {
            var trTag = document.getElementById("trTag0")
            if (ele.checked) {
                trTag.style.display = "none";
            }
            else {
                trTag.style.display = "";
            }
        }
    </script>
    <script type="text/javascript">
        function ToggelContainerDisplay(containerID, chkBox) {
            if (chkBox.checked) {
                document.getElementById(containerID).style.display = "";
            }
            else {
                document.getElementById(containerID).style.display = "none";
            }
        }


        function ConfirmMsg(msg) {
            if (Page_ClientValidate()) {
                return confirm(msg);
            }
            else {
                return false;
            }
        }


        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 45 || charCode > 57)) {
                alert("Please enter Numbers only.");
                return false;
            }
            return true;
        }                                 
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $chkTag = $("#<%=chkPrintTags.ClientID%>");
            if ($chkTag.is(':checked')) {
                $("#tagDesc").toggle();
                
            }
        });
    </script>
</asp:Content>



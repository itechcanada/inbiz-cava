﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/fullWidth.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Receiving_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <style type="text/css">
        input[type=submit]{height:50px;}
        input[type=text], select {padding:5px; font-size:16px;}        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" Runat="Server">
     <h4>
        <%=Resources.Resource.lblReceiving%>
    </h4>
    <div>       
        <table width="100%" border="0" cellpadding="0" cellspacing="0">                                           
            <tr>
                <td align="center" colspan="2">
                    <asp:TextBox runat="server" Width="195px" ID="txtSearch" ValidationGroup="PrdSrch"
                        AutoPostBack="false"> </asp:TextBox><span class="style1"> *</span>
                    <asp:RequiredFieldValidator ID="reqvalSearch" runat="server" ControlToValidate="txtSearch"
                        ErrorMessage="<%$ Resources:Resource, PlzEntSearchCriteria %>" ValidationGroup="PrdSrch"
                        SetFocusOnError="true" Display="None"> </asp:RequiredFieldValidator>
                         <asp:DropDownList ID="dlWarehouse" runat="server" Width="280px" ValidationGroup="PrdSrch">
                    </asp:DropDownList>
                    <span class="style1">*</span>
                    <asp:CustomValidator ID="custvalPOWarehouse" runat="server" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:Resource, custvalPOWarehouse%>" ValidationGroup="PrdSrch"
                        ClientValidationFunction="funCheckWarehouseCode" Display="None"> </asp:CustomValidator>
                    <asp:CustomValidator ID="custvalWarehouse" runat="server" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:Resource, custvalPOWarehouse%>" ValidationGroup="Srch"
                        ClientValidationFunction="funCheckWarehouseCode" Display="None"> </asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <table align="center" cellpadding="5" cellspacing="5">
                        <tr>                            
                            <td>                                                                 
                                <div>
                                    <asp:Button ID="btncmdPO" runat="server" CausesValidation="false" 
                                        Text="<%$ Resources:Resource,cmdCssPONumber%>" validationgroup="PrdSrch" 
                                        OnClick="btncmdPO_Click" Width="150px"/>
                                </div>
                            </td>
                            <td>                                    
                                <div>
                                    <asp:Button ID="btncmdReceipt" runat="server" CausesValidation="false" 
                                        Text="<%$ Resources:Resource,cmdCssReceiptNo%>" validationgroup="PrdSrch" 
                                        OnClick="btncmdReceipt_Click" Width="150px"/>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <asp:Button ID="btncmdVendor" runat="server" CausesValidation="false" 
                                        Text="<%$ Resources:Resource,cmdCssVendorID%>" validationgroup="PrdSrch" 
                                        OnClick="btncmdVendor_Click" Width="150px"/>
                                </div>
                            </td>
                            <td> 
                                <div>
                                    <asp:Button ID="btncmdUPC" runat="server" CausesValidation="false" 
                                        Text="<%$ Resources:Resource,cmdCssUPCCode%>" validationgroup="PrdSrch" 
                                        OnClick="btncmdUPC_Click" Width="150px"/>
                                </div>
                            </td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="10" colspan="2">
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
               <%--     <asp:Label ID="lblRcv" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblReceiving%>"> </asp:Label>--%>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <br />
                    <div>
                        <asp:Button ID="btncmdToday" runat="server" CausesValidation="false" Text="<%$ Resources:Resource,cmdCssToday%>"
                            ValidationGroup="Srch" OnClick="btncmdToday_Click" Width="150px" />
                    </div>
                    <br />
                    <div>
                        <asp:Button ID="btncmdThisWeek" runat="server" CausesValidation="false" Text="<%$ Resources:Resource,cmdCssThisWeek%>"
                            ValidationGroup="Srch" OnClick="btncmdThisWeek_Click" Width="150px" />
                    </div>
                    <br />
                    <div>
                        <asp:Button ID="btncmdNextWeek" runat="server" CausesValidation="false" Text="<%$ Resources:Resource,cmdCssNextWeek%>"
                            ValidationGroup="Srch" OnClick="btncmdNextWeek_Click" Width="150px" />
                    </div>
                    <br />
                    <div>
                        <asp:Button ID="btncmdAll" runat="server" CausesValidation="false" Text="<%$ Resources:Resource,cmdCssAll%>"
                            ValidationGroup="Srch" OnClick="btncmdAll_Click" Width="150px" />
                    </div>
                    <br />
                    <br />
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsReceiving" runat="server" ShowMessageBox="true" ShowSummary="false"
            ValidationGroup="PrdSrch" />
        <asp:ValidationSummary ID="valsReceivingW" runat="server" ShowMessageBox="true" ShowSummary="false"
            ValidationGroup="Srch" />
    </div>
    <br />
    <br />
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        function funCheckWarehouseCode(source, args) {
            if (document.getElementById('<%=dlWarehouse.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
        function funCaptureEnterKey() {
            alert(event.keyCode);
            if (event.keyCode == 13) {
                alert('keycode= ' + event.keyCode);
                document.getElementById('<%=btncmdUPC.ClientID%>').focus();
            }
        }
        function doClick(buttonName, e) {
            //the purpose of this function is to allow the enter key to 
            //point to the correct button to click.
            var key;

            if (window.event)
                key = window.event.keyCode;     //IE
            else
                key = e.which;     //firefox

            if (key == 13) {
                //Get the button the user wants to have clicked
                var btn = document.getElementById(buttonName);
                if (btn != null) { //If we find the button click it
                    btn.click();
                    event.keyCode = 0
                }
            }
        }
    </script>
</asp:Content>


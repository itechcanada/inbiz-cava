﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="QuickReceiving.aspx.cs" Inherits="Receiving_QuickReceiving" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div style="float: left; width: 100%;">
        <div style="float: left; width: 215px;">
            <div class="col1">
                <img src="../lib/image/icon/ico_admin.png" alt="" />
            </div>
            <div class="col2">
                <h1>
                    <%= Resources.Resource.Administration %></h1>
            </div>
        </div>
        <div style="margin-left: 212px;">
            <h3>
                <asp:Label ID="Label1" Text="<%$Resources:Resource,lblQuickReceiving%>" runat="server" />
            </h3>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <asp:Panel ID="pnlContent" runat="server" DefaultButton="btnSave"> 
    <div id="grid_wrapper" style="width: 100%;">
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="lblWarehouse" Text="<%$ Resources:Resource, lblSoWhs%>" runat="server" />
                </div>
                <div class="input">
                    <asp:DropDownList ID="dlWarehouses" runat="server" Width="180px">
                    </asp:DropDownList>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblAutoCommt" Text="<%$ Resources:Resource, lblAutoCommit%>" runat="server" />
                </div>
                <div class="input">
                    <asp:RadioButtonList ID="rblAutoCommit" runat="server" RepeatDirection="Horizontal"
                        OnSelectedIndexChanged="rblAutoCommit_OnSelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Text="<%$ Resources:Resource, lblON%>" Value="1" Selected="True" />
                        <asp:ListItem Text="<%$ Resources:Resource, lblOff%>" Value="0" />
                    </asp:RadioButtonList>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="Label2" Text="<%$ Resources:Resource, lblSrchSKU%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtSKU" runat="server" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rqtxtSKU" ControlToValidate="txtSKU" SetFocusOnError="True"
                        EnableClientScript="true" runat="server" Text="<%$Resources:Resource, lblRequiredSKU%>"
                        ValidationGroup="validateSKU"></asp:RequiredFieldValidator>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li id="liSalesQty" runat="server">
                <div class="lbl">
                    <asp:Label ID="lblSalesQty" Text="<%$ Resources:Resource, grdPOQty%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtQty" runat="server" Text="1" Width="150px" />
                    <asp:RangeValidator runat="server" ID="rngDate" ControlToValidate="txtQty" Type="Integer"
                        MinimumValue="0" MaximumValue="99999" ErrorMessage="<%$Resources:Resource, errMsgDiscPer%>" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li id="liSalesOrder" runat="server">
                <div class="lbl">
                    <asp:Label ID="lblSalesOrder" Text="<%$ Resources:Resource, lblPoNo%>" runat="server" />
                </div>
                <div class="input">
                    <asp:DropDownList ID="ddlOrderList" runat="server" Width="150px">
                    </asp:DropDownList>
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
    </div>
    <div style="text-align: right; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" ValidationGroup="validateSKU"
            OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, btnCancel%>" runat="server"
            CausesValidation="false" OnClick="btnCancel_Click" />
    </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
    <asp:Panel runat="server" ID="SearchPanel">
    </asp:Panel>
    <div class="clear">
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" runat="Server">
</asp:Content>

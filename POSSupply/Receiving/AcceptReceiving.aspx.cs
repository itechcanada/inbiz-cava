﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.WebControls;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.DataAccess.MySql;

public partial class Receiving_AcceptReceiving : BasePage
{            
    //Receiving rcv = new Receiving();
    //RceivedItems rcvdItms = new RceivedItems();
    //PurchaseOrderItems poItms = new PurchaseOrderItems();
    //InbizUser _objUsers = new InbizUser();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);

            try
            {
                PurchaseOrderItems poItms = new PurchaseOrderItems();
                poItms.PopulateObject(dbHelp, this.PurchaseOrderItemID);
                Product prd = new Product();
                prd.PopulateObject(poItms.PoItemPrdId);
                txtTag.Text =  prd.PrdUPCCode;
                txtQuantity.Text = this.Quantity.ToString();
                txtQuantity.Focus();
            }
            catch
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }                      
        }
    }

    private int Quantity {
        get
        {
            int qty = 0;
            int.TryParse(Request.QueryString["Qty"], out qty);
            return qty;
        }
    }

    private int PurchaseOrderItemID
    {
        get
        {
            int puid = 0;
            int.TryParse(Request.QueryString["poitemid"], out puid);
            return puid;
        }
    }

    protected void cmdSubmit_Click(object sender, System.EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            int quantity = BusinessUtility.GetInt(txtQuantity.Text);
            //if (quantity <= 0)
            //{
            //    lblMsg.Text = Resources.Resource.msgRcvPlzEntNumericValueGreaterThanZero;
            //    return;
            //}

            if (chkCreateTags.Checked && !chkAutoGen.Checked && string.IsNullOrEmpty(txtTag.Text))
            {
                lblMsg.Text = "Please provide Product Tag!";
                return;
            }
            PurchaseOrderItems poItms = new PurchaseOrderItems();

            //Populate Purchase Order Item       
            poItms.PopulateObject(dbHelp, this.PurchaseOrderItemID);

            // Set Data for Receiving
            Receiving rcv = new Receiving();
            rcv.RecPOID = poItms.PoID;
            rcv.RecAtWhsCode = poItms.PoItemShpToWhsCode;
            rcv.RecByUserID = CurrentUser.UserID;
            rcv.RecDateTime = DateTime.Now;
            rcv.RecID = rcv.GetRecevingIDByPO(dbHelp, poItms.PoID);
            rcv.Update(dbHelp);

            //Set Data for Received items 
            RceivedItems rcvdItms = new RceivedItems();
            rcvdItms.RecId = rcv.RecID;
            rcvdItms.PoItems = poItms.PoItems;
            rcvdItms.RcvdQty = BusinessUtility.GetInt(txtQuantity.Text);
            rcvdItms.PoItemRcvDateTime = BusinessUtility.GetDateTime(txtRcvDateTime.Text, DateFormat.MMddyyyy);
            if (rcvdItms.PoItemRcvDateTime == DateTime.MinValue)
            {
                rcvdItms.PoItemRcvDateTime = DateTime.Now;
            }

            poItms.PoItemRcvDateTime = rcvdItms.PoItemRcvDateTime;
            poItms.PoRcvdQty = poItms.PoRcvdQty + BusinessUtility.GetDouble(txtQuantity.Text);

            //Update units for vendor in vendor association table Implemented on 2013-01-09
            PurchaseOrders po = new PurchaseOrders();
            po.PopulateObject(dbHelp, poItms.PoID);
            ProductAssociateVendor pVnd = new ProductAssociateVendor();
            pVnd.NoOfUnits = BusinessUtility.GetDouble(txtQuantity.Text);
            pVnd.PrdCostPrice = poItms.PoUnitPrice;
            pVnd.PrdID = poItms.PoItemPrdId;
            pVnd.PrdVendorID = po.PoVendorID;
            pVnd.PurchaseDate = rcvdItms.PoItemRcvDateTime;
            pVnd.PurchaseOrderNo = poItms.PoID;
            pVnd.UpdateUnitsOnReceiving(dbHelp);
            

            // Updates PO Item Qty and Product Qty in Inventory
            if (poItms.UpdatePurchaseOrderItemQuantity(dbHelp, this.PurchaseOrderItemID, poItms.PoRcvdQty, poItms.PoItemRcvDateTime))
            {
                if (poItms.UpdateReceivedProductQuantity(dbHelp, poItms.PoItemPrdId, poItms.PoItemShpToWhsCode, BusinessUtility.GetDouble(txtQuantity.Text)))
                {
                    rcvdItms.Insert(dbHelp);

                    //Create Tags for received Items 
                    if (chkCreateTags.Checked)
                    {
                        int iCount = 0;
                        int.TryParse(txtQuantity.Text, out iCount);

                        ProductTag objPrdTag = new ProductTag();
                        objPrdTag.ProductID = poItms.PoItemPrdId;
                        objPrdTag.ReceivingID = rcvdItms.RecId;

                        string uniqTagWithoutProductID = objPrdTag.GetUniqueTagWithoutProductID(dbHelp, rcv.RecPOID);

                        string[] posoWsh = uniqTagWithoutProductID.Split('-');
                        int poNO = 0;
                        int soNo = 0;
                        int.TryParse(posoWsh[0], out poNO);
                        int.TryParse(posoWsh[1], out soNo);

                        objPrdTag.POID = poNO;
                        objPrdTag.SOID = soNo;
                        objPrdTag.WareHouseCode = posoWsh[2];


                        List<string> tagList = new List<string>();

                        if (chkAutoGen.Checked)
                        {
                            for (int index = 1; index <= iCount; index++)
                            {
                                objPrdTag.Tag = string.Format("{0}-{1}-{2}", uniqTagWithoutProductID, objPrdTag.ProductID, index);
                                objPrdTag.Quantity = 1;
                                tagList.Add(objPrdTag.Tag);
                                objPrdTag.Insert(dbHelp);
                            }
                        }
                        else
                        {
                            objPrdTag.Tag = txtTag.Text;
                            objPrdTag.Quantity = iCount;

                            for (int index2 = 1; index2 <= iCount; index2++)
                            {
                                tagList.Add(objPrdTag.Tag);
                            }
                            objPrdTag.Insert(dbHelp);
                        }

                        Product.ProductClothDesc prdCD = new Product.ProductClothDesc();
                        prdCD.ProductID = poItms.PoItemPrdId;
                        prdCD.getClothDesc(null, 0);
                        ProductSize ps = new ProductSize();
                        ps.SizeID = prdCD.Size;
                        List<ProductSize> lPS = new List<ProductSize>();
                        lPS = ps.GetAllSizeList(null, "En");

                        ProductColor pc = new ProductColor();
                        pc.ColorID = prdCD.Color;
                        List<ProductColor> lPC = new List<ProductColor>();
                        lPC = pc.GetAllColorList(null, "fr");
                        //Session["ProductTagsToPrint"] = tagList;

                        if (chkPrintTags.Checked && lPS.Count>0 && lPC.Count>0)
                        {
                            string url = "";
                            if (BusinessUtility.GetDouble(txtPrice.Text) > 0.0)
                            {
                                //SysCompanyInfo ci = new SysCompanyInfo();
                                //ci.PopulateObject(CurrentUser.DefaultCompanyID);
                                url = string.Format("PrintTagsNew.aspx?tag={0}&name={1}&price={2}&clr={3}&count={4}&size={5}", txtTag.Text, prdCD.Style, txtPrice.Text, lPC[0].ColorFr, txtQuantity.Text, lPS[0].SizeEn);
                            }
                            else
                            {
                                url = string.Format("PrintTagsNew.aspx?tag={0}&name={1}&price={2}&clr={3}&count={4}&size={5}", txtTag.Text, prdCD.Style, "", lPC[0].ColorFr, txtQuantity.Text, lPS[0].SizeEn);
                            }
                            Globals.RegisterScript(this, "window.open('" + url + "', 'Print', 'height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');");
                            //Globals.RegisterScript(this, "window.open('PrintTags.aspx', 'Print','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');");
                            //Response.Write("<script>window.open('PrintTags.aspx', 'Print','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');</script>");
                        }

                        AlertHelper.SetAlertOnReceiving(dbHelp, poItms.PoItems, rcv.RecID, CurrentUser.UserID);
                        lblMsg.Text = Resources.Resource.msgRcvQuantityUpdatedSuccessfully;
                        //"Quantity updated successfully."                    
                    }
                }
                else
                {
                    lblMsg.Text = Resources.Resource.msgRcvQuantityNotUpdatedSuccessfully;
                }
            }
            Globals.RegisterCloseDialogScript(Page, "parent.reloadGrid();", true);
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }        
    }


    ////Set Data for Alert
    //private void SetAlertData(clsAlerts objA)
    //{
    //    objA.AlertPartnerContactID = rcv.RecID.ToString();
    //    objA.AlertComID = rcv.RecPOID.ToString();
    //    objA.AlertDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
    //    //objA.AlertUserID = Session["UserID"]
    //    string strNote = "";
    //    strNote += Resources.Resource.alrtRcvQtyOfPrd + "'" + poItms.ProdIDDesc + "' ";
    //    //Quantity of Product 
    //    if ((poItms.GetRemainingPOItemsQty(poItms.PoItems) < 0))
    //    {
    //        strNote += Resources.Resource.alrtRcvMoreThanQtyInPO;
    //        //Received is more than the Quantity mentioned in PO 
    //    }
    //    else
    //    {
    //        strNote += Resources.Resource.alrtRcvLessThanQtyInPO;
    //        //Received is less than the Quantity mentioned in PO
    //    }
    //    strNote += "<br/><br/>PO:" + poItms.PoID + " <br/>";
    //    strNote += Resources.Resource.alrtRcvPOQty + " " + poItms.PoQty + "<br/>";
    //    //PO Quantity:
    //    strNote += Resources.Resource.alrtRcvRcvQty + " " + poItms.PoRcvdQty + "<br/>";
    //    //Received Quantity:
    //    _objUsers.UserID = CurrentUser.UserID;
    //    _objUsers.PopulateObject(CurrentUser.UserID);
    //    strNote += Resources.Resource.alrtRcvRcvUser + " " + _objUsers.UserFirstName + " " + _objUsers.UserLastName;
    //    //Receiving User:
    //    objA.AlertNote = strNote;
    //    objA.AlertRefType = "RCV";
    //}

    ////Inserts Alert if Received Quantity is different from Ordered Quantity.
    //protected void GenerateAlert(int poItemID)
    //{
    //    double remainQty = poItms.GetRemainingPOItemsQty(poItemID);
    //    if (remainQty != 0) {
    //        clsAlerts objAlert = new clsAlerts();
    //        SetAlertData(objAlert);            
    //        int[] userIds = _objUsers.GetUserIDsByRole(RoleID.PROCURMENT_MANAGER);
    //        foreach (int user in userIds)
    //        {
    //            objAlert.AlertUserID = user.ToString();
    //            objAlert.insertAlerts();
    //        }
    //    }
    //}

}
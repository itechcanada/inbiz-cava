﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections.Generic
Imports System.Text

Partial Class Receiving_PrintTags
    Inherits Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not Session("ProductTagsToPrint") Is Nothing Then
            rptTags.DataSource = Session("ProductTagsToPrint")
            rptTags.DataBind()
        End If
        Session.Remove("ProductTagsToPrint")
    End Sub
End Class

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ViewPurchaseOrder.aspx.cs" Inherits="Procurement_ViewPurchaseOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            Administration</h1>
        <b>
            <asp:Literal runat="server" ID="lblTitle"></asp:Literal></b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <asp:Button ID="btnCreatePO" runat="server" Text="<%$ Resources:Resource, btnCreatePurchaseOrder%>"  />
        <iCtrl:IframeDialog ID="mdPO" TriggerControlID="btnCreatePO" Width="800" Height="460"
            Url="mdSearchVendor.aspx" Title="<%$ Resources:Resource, lblSelectVendorForOrder%>"  runat="server">
        </iCtrl:IframeDialog>
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif" CssClass="csv_export" />
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" ID="pnlSearch">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        <%= Resources.Resource.lblSearchForm%></div>
                    <div class="icon">
                        <img alt="" src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label CssClass="filter-key" AssociatedControlID="dlSearch" ID="Label2" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:DropDownList ID="dlSearch" runat="server" Width="185px"
                        ValidationGroup="PrdSrch">
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label CssClass="filter-key" AssociatedControlID="dlPOStatus" ID="Label3" runat="server" Text="<%$ Resources:Resource, lblStatus %>"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:DropDownList ID="dlPOStatus" runat="server" Width="185px">
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label CssClass="filter-key" AssociatedControlID="txtSearch" ID="Label4" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword%>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch"
                        ValidationGroup="PrdSrch"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%= Resources.Resource.lblSearchOptions %></h2>
            <p>
                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"></asp:Label></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntViewPurchaseOrder" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="JqgidPoView" Height="300px"
                    AutoWidth="True" OnCellBinding="JqgidPoView_CellBinding"
                    OnDataRequesting="JqgidPoView_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="poID" HeaderText="<%$ Resources:Resource, PONumber %>"
                            PrimaryKey="True" />
                        <trirand:JQGridColumn DataField="vendorName" HeaderText="<%$ Resources:Resource, POVendorName %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="amount" HeaderText="<%$ Resources:Resource, POTotalAmount %>"
                            Editable="false" TextAlign="Right">
                        </trirand:JQGridColumn>
                        <trirand:JQGridColumn DataField="poDate" HeaderText="<%$ Resources:Resource, grdPOCreatedDate %>" DataFormatString ="{0:$ ###,###.00}"
                            Editable="false" TextAlign="Center" />
                        <trirand:JQGridColumn DataField="poStatus" HeaderText="<%$ Resources:Resource, grdPOStatus %>"
                            Editable="false" DataFormatString="{0:MMM dd, yyyy}" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="poID"
                            Sortable="false" TextAlign="Center" Width="50" />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sqldsPO" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
            </div>
        </div>
        <div class="div_comman">                     
            <asp:Button ID="btnBack" Text="<%$Resources:Resource, cmdCssBack %>" 
                runat="server"  CausesValidation="false" onclick="btnBack_Click"/>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" runat="Server">    
    <script type="text/javascript">
        //Variables
        var gridID = "<%=JqgidPoView.ClientID %>";
        var searchPanelID = "<%=pnlSearch.ClientID %>";

        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }


        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }    
    </script>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;
using Trirand.Web.UI.WebControls;

public partial class Procurement_Create : BasePage
{
    //On Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.PROCURMENT_MANAGER))
        {
            Response.Redirect("~/AdminLogin.aspx");
        }
        if (!IsPagePostBack(grdProducts))
        {
            DbHelper dbHelp = new DbHelper(true);

            try
            {
                lblTitle.Text = Resources.Resource.CreatePO;
                SysCurrencies objSysCurrencies = new SysCurrencies();

                objSysCurrencies.FillCurrencyDropDown(dbHelp, dlCurrencyCode, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.lstSelectCurrency, "0"));
                DropDownHelper.FillDropdown(dbHelp, dlPOStatus, "PO", "POSts", Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.liSelectOrderStatus, "0"));

                SysCompanyInfo ci = new SysCompanyInfo();
                ci.CompanyID = this.CompanyID;
                ci.PopulateObject(ci.CompanyID, dbHelp);
                txtTerms.Text = ci.CompanyShpToTerms;
                txtNotes.Text = ci.CompanyPOTerms;
                btnDuplicat.Visible = this.PurchaseOrderID > 0;
                btnDuplicat.OnClientClick = string.Format(@"return confirm(""{0}"");", Resources.Resource.confirmDuplicatePO);

                if (this.VendorID > 0)
                {
                    //FillVendor(dbHelp);
                    Vendor objVendor = new Vendor();
                    objVendor.PopulateObject(dbHelp, this.VendorID);
                    dlCurrencyCode.SelectedValue = objVendor.VendorCurrency;
                    lblName.Text = Resources.Resource.lblPOVendor + ": " + objVendor.VendorName;
                    lblFax.Text = objVendor.VendorFax;
                    lblVendorName.Text = objVendor.VendorName;
                    popAddProduct.NavigateUrl = string.Format("mdSearchProduct.aspx?VdrID={0}&whsID={1}", this.VendorID, this.WarehouseCode);

                    SysCurrencies cur = new SysCurrencies();
                    cur.PopulateObject(objVendor.VendorCurrency);

                    //txtExRate.Text = string.Format("{0:F}", cur.CurrencyRelativePrice);
                    //hdnCurrencyExchangeRate.Value = string.Format("{0:F}", cur.CurrencyRelativePrice);
                    hdnCurrencyExchangeRate.Value =BusinessUtility.GetString(  cur.CurrencyRelativePrice);
                }
                if (this.PurchaseOrderID > 0)
                {
                    lblTitle.Text = Resources.Resource.lblEditPurchaseOrder;
                    PurchaseOrderCartHelper.InitCart(dbHelp, this.PurchaseOrderID);
                    InitializePO(dbHelp);
                }
                else
                {
                    PurchaseOrderCartHelper.InitLocalCart();
                }

                spnMail.Visible = this.PurchaseOrderID > 0;
                spnPrint.Visible = this.PurchaseOrderID > 0;
                spnFax.Visible = this.PurchaseOrderID > 0;
                mdMail.Url = string.Format("~/Common/mdSendMailOrFax.aspx?DocTyp={0}&SOID={1}&cmd={2}", "PO", this.PurchaseOrderID, "M"); //M stands for Mail
                mdFax.Url = string.Format("~/Common/mdSendMailOrFax.aspx?DocTyp={0}&SOID={1}&cmd={2}", "PO", this.PurchaseOrderID, "F"); //F stands for Fax
                string printUrl = string.Format("../Common/Print.aspx?ReqID={0}&DocTyp={1}", this.PurchaseOrderID, "PO");
                imgPdf.Attributes.Add("onclick", string.Format("window.open('{0}', 'Print', 'height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no'); return false;", printUrl));

                liCurrency.Visible = false;
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError + ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    //Populate Object
    private void SetData(PurchaseOrders pOrder)
    {
        pOrder.PoDate = DateTime.Now;

        if (CurrentUser.IsInRole(RoleID.PO_APPROVER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            pOrder.PoStatus = POStatus.SUBMITTED;
        }
        else
        {
            pOrder.PoStatus = POStatus.NEW;
        }
        //pOrder.PoStatus = POStatus.READY_FOR_RECEIVING;
        //Autometically set ready for receiving override all security rules        
        pOrder.PoVendorID = this.VendorID;
        pOrder.PoShipVia = txtShipVia.Text;
        pOrder.PoFOBLoc = txtFOBLoc.Text;
        pOrder.PoShpTerms = txtTerms.Text;
        pOrder.PoNotes = txtNotes.Text;
        pOrder.PoCreatedByUserID = CurrentUser.UserID;
        pOrder.PoAuthroisedByUserID = 0;
        pOrder.PoFaxedEmailDateTime = System.DateTime.MinValue;
        pOrder.PoCurrencyCode = dlCurrencyCode.SelectedValue;
        //pOrder.PoCurrencyExRate = BusinessUtility.GetDouble(txtExRate.Text);
        pOrder.PoCurrencyExRate = BusinessUtility.GetDouble(hdnCurrencyExchangeRate.Value);
        pOrder.PoCompanyID = this.CompanyID;
        pOrder.PoWhsCode = this.WarehouseCode;
        pOrder.PoForSoNo = BusinessUtility.GetInt(hdnValidSoNo.Value);
        pOrder.PoRef = txtYourRef.Text;
    }

    protected void InitializePO(DbHelper dbHelp)
    {
        PurchaseOrders pOrder = new PurchaseOrders();

        if (this.PurchaseOrderID > 0 && this.CompanyID > 0 && this.WarehouseCode.Length > 0)
        {
            liReceiving.Visible = true;
            liStatus.Visible = true;
            divApprove.Visible = true;

            pOrder.PopulateObject(dbHelp, this.PurchaseOrderID);

            //Po status 
            int i = 0, j = 0;
            if (pOrder.PoStatus == POStatus.SUBMITTED)
            {
                j = 1;
            }
            else if (pOrder.PoStatus == POStatus.ACCEPTED)
            {
                j = 2;
            }
            else if (pOrder.PoStatus == POStatus.NOT_ACCEPTED)
            {
                j = 3;
            }
            else if (pOrder.PoStatus == POStatus.HELD)
            {
                j = 4;
            }
            else if (pOrder.PoStatus == POStatus.RECEIVED_AND_PAID)
            {
                j = 5;
            }
            else if (pOrder.PoStatus == POStatus.CLOSED)
            {
                j = 6;
            }

            while (i < j)
            {
                dlPOStatus.Items.RemoveAt(1);
                i++;
            }
            if (j == 0)
            {
                dlPOStatus.Enabled = (CurrentUser.IsInRole(RoleID.ADMINISTRATOR) || CurrentUser.IsInRole(RoleID.PO_APPROVER) || CurrentUser.IsInRole(RoleID.PROCURMENT_MANAGER));
            }


            //dont allow to edit if po is closed or ready to receive
            //grdProducts.Columns[10].Visible = !(pOrder.PoStatus == POStatus.CLOSED || pOrder.PoStatus == POStatus.READY_FOR_RECEIVING || pOrder.PoStatus == POStatus.PAID || pOrder.PoStatus == POStatus.RECEIVED_AND_PAID);
            //divAddProduct.Visible = !(pOrder.PoStatus == POStatus.CLOSED || pOrder.PoStatus == POStatus.READY_FOR_RECEIVING || pOrder.PoStatus == POStatus.PAID || pOrder.PoStatus == POStatus.RECEIVED_AND_PAID);
            grdProducts.Columns[10].Visible = !(pOrder.PoStatus != POStatus.NEW);
            divAddProduct.Visible = !(pOrder.PoStatus != POStatus.NEW);

            txtTerms.Text = pOrder.PoShpTerms;
            txtNotes.Text = pOrder.PoNotes;
            txtShipVia.Text = pOrder.PoShipVia;
            txtFOBLoc.Text = pOrder.PoFOBLoc;

            lblPODate.Text = BusinessUtility.GetDateTimeString(pOrder.PoDate, DateFormat.MMddyyyy);
            lblPOID.Text = pOrder.PoID.ToString();
            hdnPoID.Value = pOrder.PoID.ToString();
            hdnCreatedUserID.Value = pOrder.PoCreatedByUserID.ToString();
            //'lblVendorName.Text = _objPurchaseOrders.VendorName
            txtTerms.Text = pOrder.PoShpTerms;

            Addresses addr = new Addresses();
            addr.PopulateObject(pOrder.PoVendorID, AddressReference.VENDOR, AddressType.HEAD_OFFICE);

            lblAddress.Text = addr.ToHtml();

            dlPOStatus.SelectedValue = pOrder.PoStatus;
            if (BusinessUtility.GetString(pOrder.PoClosedReason) != "")
            {
                ddlReason.SelectedValue = pOrder.PoClosedReason;
            }
            lblStatus.Text = dlPOStatus.SelectedItem.Text;
            //'lblFax.Text = _objPurchaseOrders.
            hdnPreStatus.Value = pOrder.PoStatus;

            if (dlPOStatus.SelectedValue == POStatus.READY_FOR_RECEIVING)
            {
                Receiving objRcv = new Receiving();
                objRcv.PopulateObjectByPoID(dbHelp, this.PurchaseOrderID);
                txtReceivingDate.Text = BusinessUtility.GetDateTimeString(objRcv.RecDateTime, DateFormat.MMddyyyy);
                objRcv = null;
            }
            else
            {
                txtReceivingDate.Text = BusinessUtility.GetDateTimeString(DateTime.Now.Date, DateFormat.MMddyyyy);
            }

            txtYourRef.Text = pOrder.PoRef;

            SysCompanyInfo objSysCompanyInfo = new SysCompanyInfo();
            objSysCompanyInfo.CompanyID = pOrder.PoCompanyID;
            objSysCompanyInfo.PopulateObject(objSysCompanyInfo.CompanyID, dbHelp);
            lblSOPOComNameTitle.Text = objSysCompanyInfo.CompanyName;

            SysWarehouses objSysWarehouses = new SysWarehouses();
            objSysWarehouses.WarehouseCode = pOrder.PoWhsCode;
            objSysWarehouses.PopulateObject(objSysWarehouses.WarehouseCode, dbHelp);
            lblSOPOwhsName.Text = objSysWarehouses.WarehouseCode + " : " + objSysWarehouses.WarehouseDescription;
            objSysWarehouses = null;
            hdnCurrencyExchangeRate.Value = string.Format("{0:F}", pOrder.PoCurrencyExRate);
        }
        else
        {
            liReceiving.Visible = false;
            liStatus.Visible = false;
            divApprove.Visible = false;
        }
    }

    protected void btnReset_Click(object sender, System.EventArgs e)
    {
        //Session["SRCart"] = null;
        //Session["IsCart"] = null;
        Response.Redirect("~/Procurement/ViewPurchaseOrder.aspx");
    }
    // On Save
    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        if (Page.IsValid)
        {
            //DataTable cart = this.TempCart;
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                if (this.PurchaseOrderID <= 0 && PurchaseOrderCartHelper.CurrentCart.Items.Count > 0)
                {
                    this.SavePO(dbHelp);
                    Response.Redirect("ViewPurchaseOrder.aspx?Default=1", false);
                }
                else if (this.PurchaseOrderID > 0)
                {
                    this.SavePO(dbHelp);
                    Response.Redirect("ViewPurchaseOrder.aspx?Default=1", false);
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblAddProducInPo);
                }
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void grdProducts_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        grdProducts.DataSource = PurchaseOrderCartHelper.CurrentCart.Items;
        Session["ProductTagsToPrint"] = PurchaseOrderCartHelper.CurrentCart.Items;
        //mdTagPrinting.Url += 

        //    url = string.Format("PrintTagsNew.aspx?tag={0}&name={1}&price={2}&cur={3}&count={4}&size={5}", txtTag.Text, prdCD.Style, txtPrice.Text, ci.CompanyBasCur, txtQuantity.Text, poItms.ProductSize);
        //                    }
        //                    else
        //                    {
        //                        url = string.Format("PrintTagsNew.aspx?tag={0}&name={1}&price={2}&cur={3}&count={4}", txtTag.Text, "", "", "", txtQuantity.Text);
        //                    }
        //                    Globals.RegisterScript(this, "window.open('" + url + "', 'Print', 'height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');");


    }

    protected void grdProducts_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 10)
        {
            e.CellHtml = string.Format("<a class=edit-Product  href=mdSearchProduct.aspx?VdrID={0}&editItemID={1} >Edit</a>", this.VendorID, e.RowKey);
        }
        else if (e.ColumnIndex == 7)
        {
            e.CellHtml = CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName));
        }
        else if (e.ColumnIndex == 8)
        {
            e.CellHtml = CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName));
        }
    }

    private void SavePO(DbHelper dbHelp)
    {
        PurchaseOrders pOrder = new PurchaseOrders();
        //Check if is for update or new order
        if (this.PurchaseOrderID <= 0)
        {
            SetData(pOrder);
            //if (CurrentUser.IsInRole(RoleID.PO_APPROVER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
            //{
            //    pOrder.PoStatus = POStatus.SUBMITTED;
            //}
            //else
            //{
            //    pOrder.PoStatus = POStatus.NEW;
            //}
            pOrder.PoStatus = POStatus.NEW;
            pOrder.Insert(dbHelp);
            //Insert Purchase Order
            List<PurchaseOrderItems> lstItems = PurchaseOrderCartHelper.GetItemListFromCart();
            foreach (var r in lstItems)
            {
                r.PoID = pOrder.PoID;
                r.Insert(dbHelp);
            }

            if (pOrder.PoStatus == POStatus.READY_FOR_RECEIVING || pOrder.PoStatus == POStatus.RECEIVED)
            {
                pOrder.UpdateForReceiving(dbHelp, pOrder.PoID, DateTime.Now); //Immidiate nned to set ready for receiving
                //To to generate alert for ready to receive.
            }
            else
            {
                //To Do to generate alert
                AlertHelper.SetAlertPoCreatedAlert(dbHelp, pOrder.PoID, CurrentUser.UserID);
            }
        }
        else //Update
        {


            // Updte Currency Exchange Rate
            pOrder.UpdatePOExchangeRate(dbHelp, this.PurchaseOrderID, BusinessUtility.GetDouble(hdnCurrencyExchangeRate.Value));

            //Populate existing Po
            pOrder.PopulateObject(dbHelp, this.PurchaseOrderID);

            //Add/Edit PoItem
            if (pOrder.PoStatus == POStatus.NEW)
            {
                List<PurchaseOrderItems> lstItems = PurchaseOrderCartHelper.GetItemListFromCart();
                foreach (var r in lstItems)
                {
                    r.PoID = this.PurchaseOrderID;
                    if (!r.POItemExists(dbHelp, this.PurchaseOrderID, r.PoItemPrdId))
                    {
                        r.Insert(dbHelp);
                    }
                    else
                    {
                        r.Update(dbHelp);
                    }
                }
            }

            //if order is set as closed do not allow to edit the order
            if (pOrder.PoStatus == POStatus.CLOSED)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPOCloseOrdersCanNotUpdated);
                return;
            }


            if (dlPOStatus.SelectedValue == "C")
            {
                pOrder.PoClosedReason = ddlReason.SelectedValue;
            }

            //check if po is going to approved
            if ((pOrder.PoStatus == POStatus.NEW) && dlPOStatus.SelectedValue == POStatus.SUBMITTED)
            {
                pOrder.PoID = this.PurchaseOrderID;
                pOrder.PoShpTerms = txtTerms.Text;
                pOrder.PoStatus = POStatus.SUBMITTED;

                pOrder.PoNotes = txtNotes.Text;
                pOrder.PoShipVia = txtShipVia.Text;
                pOrder.PoFOBLoc = txtFOBLoc.Text;
                pOrder.PoAuthroisedByUserID = CurrentUser.UserID;
                pOrder.PoRef = txtYourRef.Text;
                pOrder.UpdateOnApprove(dbHelp);
                //To do to set alers on approve of PO
            }
            //else if ((pOrder.PoStatus == POStatus.NEW || pOrder.PoStatus == POStatus.SUBMITTED) && (dlPOStatus.SelectedValue == POStatus.READY_FOR_RECEIVING || dlPOStatus.SelectedValue == POStatus.RECEIVED)) //Check if going to Ready for receiving
            //{
            else if ((pOrder.PoStatus == POStatus.NEW || pOrder.PoStatus == POStatus.SUBMITTED) && (dlPOStatus.SelectedValue == POStatus.READY_FOR_RECEIVING) || (dlPOStatus.SelectedValue == POStatus.RECEIVED)) //Check if going to Ready for receiving
            {
                pOrder.PoStatus = dlPOStatus.SelectedValue;
                pOrder.PoRef = txtYourRef.Text;
                pOrder.UpdateForReceiving(dbHelp, pOrder.PoID, DateTime.Now);

                //To to set alert on ready for reaciving

                RecievedJournal jrnl = new RecievedJournal();
                jrnl.WhenRecieved = BusinessUtility.GetDateTime(txtReceivingDate.Text, DateFormat.MMddyyyy);
                jrnl.PoId = this.PurchaseOrderID;
                jrnl.Insert(dbHelp);
            }
            else
            {
                pOrder.PoRef = txtYourRef.Text;
                pOrder.UpdatePO(dbHelp, pOrder.PoID, dlPOStatus.SelectedValue, txtTerms.Text);
            }
        }
    }

    //public void MakeCart()
    //{
    //    DataTable dtCart = new DataTable("PO_ITEMS_CART");
    //    DataColumn colPk = new DataColumn("ProductID", typeof(int));
    //    dtCart.Columns.Add(colPk);
    //    dtCart.Columns.Add("UPCCode", typeof(string));
    //    dtCart.Columns.Add("ProductName", typeof(string));
    //    dtCart.Columns.Add("VendorID", typeof(int));
    //    dtCart.Columns.Add("VendorName", typeof(string));
    //    dtCart.Columns.Add("Quantity", typeof(double));
    //    dtCart.Columns.Add("Price", typeof(double));
    //    dtCart.Columns.Add("WarehouseCode", typeof(string));
    //    dtCart.PrimaryKey = new DataColumn[] { colPk };

    //    Session["SRCart"] = dtCart;
    //}

    #region Properties
    //private DataTable TempCart
    //{
    //    get
    //    {
    //        if (Session["SRCart"] != null)
    //        {
    //            return (DataTable)Session["SRCart"];
    //        }
    //        MakeCart();
    //        return (DataTable)Session["SRCart"];
    //    }
    //    set
    //    {
    //        if (value != null)
    //        {
    //            Session["SRCart"] = value;
    //        }
    //        else
    //        {
    //            Session.Remove("SRCart");
    //        }
    //    }
    //}

    private int VendorID
    {
        get
        {
            int vid = 0;
            int.TryParse(Request.QueryString["VdrID"], out vid);
            return vid;
        }
    }

    private int PurchaseOrderID
    {
        get
        {
            int poid = 0;
            int.TryParse(Request.QueryString["POID"], out poid);
            return poid;
        }
    }

    private int CompanyID
    {
        get
        {
            int cid = 0;
            int.TryParse(Request.QueryString["ComID"], out cid);
            return cid > 0 ? cid : CurrentUser.DefaultCompanyID;
        }
    }

    private string WarehouseCode
    {
        get
        {
            string whsCode = BusinessUtility.GetString(Request.QueryString["whsID"]);
            return !string.IsNullOrEmpty(whsCode) ? whsCode : CurrentUser.UserDefaultWarehouse;
        }
    }
    #endregion
    protected void btnDuplicat_Click(object sender, EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            if (this.PurchaseOrderID > 0)
            {
                PurchaseOrders po = new PurchaseOrders();
                int newPoID = po.CreateDuplicateOrder(dbHelp, this.PurchaseOrderID, CurrentUser.UserID);

                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgOrderSuccessfullyDuplicated);
                Response.Redirect("ViewPurchaseOrder.aspx?Default=1", false);
            }
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }
}
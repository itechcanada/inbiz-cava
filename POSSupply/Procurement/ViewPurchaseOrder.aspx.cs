﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Procurement_ViewPurchaseOrder : BasePage
{   
    PurchaseOrders _pOrder = new PurchaseOrders();   
    
    // On Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        lblTitle.Text = Resources.Resource.lblViewPurchaseOrder;
        if (!IsPagePostBack(JqgidPoView))
        {
            DropDownHelper.FillDropdown(dlPOStatus, "PO", "POSts", Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.liSelectOrderStatus, ""));
            //DropDownHelper.FillDropdown(dlSearch, "PO", "dlSh1", Globals.CurrentAppLanguageCode, new ListItem("Select Search Field", ""));
            DropDownHelper.FillDropdown(dlSearch, "PO", "dlSh1", Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.lblSelectSearchField, ""));

            btnBack.Visible = !string.IsNullOrEmpty(Request.QueryString["backUrl"]);
        }
        txtSearch.Focus();
    }
    protected void JqgidPoView_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        _pOrder.PopulateObject(null, BusinessUtility.GetInt(e.RowKey));
        if (e.ColumnIndex == 5)
        {                        
            string strurl = "Create.aspx?POID=" + _pOrder.PoID + "&vdrID=" + _pOrder.PoVendorID + "&ComID=" + _pOrder.PoCompanyID + "&whsID=" + _pOrder.PoWhsCode;
            e.CellHtml = string.Format("<a href=\"{0}\">{1}</a>", strurl, Resources.Resource.lblEdit);
        }
        if ((e.ColumnIndex == 2))
        {
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(e.RowValues[2]), _pOrder.PoCurrencyCode);// "Ram" + _pOrder.PoCurrencyCode + " " + string.Format("{0:F}", BusinessUtility.GetCurrencyString(Convert.ToDouble(e.RowValues[2]), Globals.CurrentCultureName));
        }

        if ((e.ColumnIndex == 4))
        {
            string strData = "";
            switch (e.RowValues[4].ToString())
            {
                case POStatus.NEW:
                    strData = Resources.Resource.liGenerated;
                    break;
                case POStatus.SUBMITTED:
                    strData = Resources.Resource.liSubmitted;
                    break;
                case POStatus.ACCEPTED:
                    strData = Resources.Resource.liAccepted;
                    break;
                case POStatus.NOT_ACCEPTED:
                    strData = Resources.Resource.liNotAccepted;
                    break;
                case POStatus.HELD:
                    strData = Resources.Resource.liHeld;
                    break;
                case POStatus.READY_FOR_RECEIVING:
                    strData = Resources.Resource.liReadyForReceiving;
                    break;
                case POStatus.CLOSED:
                    strData = Resources.Resource.liClosed;
                    break;
                case POStatus.RECEIVED_AND_PAID:
                    strData = Resources.Resource.liReceivedAndPaid;
                    break;
                case POStatus.RECEIVED:
                    strData = Resources.Resource.msgReceived;
                    break;
                case POStatus.PAID:
                    strData = Resources.Resource.liPaid;
                    break;
            }
            e.CellHtml = strData;
        }
    }

    protected void  JqgidPoView_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        int vendroID = 0;
        int.TryParse(Request.QueryString["vendorID"], out vendroID);
        JqgidPoView.DataSource = _pOrder.GetSearchResult(null, Request.QueryString[dlSearch.ClientID], Request.QueryString[txtSearch.ClientID], Request.QueryString[dlPOStatus.ClientID], vendroID);
    }
    
    protected void btnBack_Click(object sender, EventArgs e)
    {
        string backUrl = Request.QueryString["backUrl"];
        if (!string.IsNullOrEmpty(backUrl))
        {
            Response.Redirect(backUrl);
        }
    }
}
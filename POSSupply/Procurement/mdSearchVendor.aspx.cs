﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Trirand.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Procurement_mdSearchVendor : BasePage
{   
    Vendor _objVendor = new Vendor();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdVendors))
        {
            SysWarehouses wh = new SysWarehouses();
            wh.FillWharehouse(null, dlWarehouses, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));
            txtSearch.Focus();
        }
    }
   
    //Populate Address
    private string FillAddressInfo(string sRef, string sType, string sSource)
    {
        string strAddress = "";
        Addresses objAddresses = new Addresses();
        //objAdr.getAddressInfo(sRef, sType, sSource)
        objAddresses.GetAddress(BusinessUtility.GetInt(sSource), sRef, sType);

        if (!string.IsNullOrEmpty(objAddresses.AddressLine1))
        {
            strAddress += objAddresses.AddressLine1 + ",";
        }
        if (!string.IsNullOrEmpty(objAddresses.AddressLine2))
        {
            strAddress += objAddresses.AddressLine2 + ",";
        }
        if (!string.IsNullOrEmpty(objAddresses.AddressLine3))
        {
            strAddress += objAddresses.AddressLine3;
        }
        strAddress += "<br>";
        if (!string.IsNullOrEmpty(objAddresses.AddressCity))
        {
            strAddress += objAddresses.AddressCity + ", (";
        }
        if (!string.IsNullOrEmpty(objAddresses.AddressState))
        {
            strAddress += objAddresses.AddressState + " )<br />";
        }
        if (!string.IsNullOrEmpty(objAddresses.AddressCountry))
        {
            strAddress += objAddresses.AddressCountry + "<br />";
        }
        if (!string.IsNullOrEmpty(objAddresses.AddressPostalCode))
        {
            strAddress += "Postal Code: " + objAddresses.AddressPostalCode;
        }
        objAddresses = null;
        return strAddress;
    }
    protected void  cmdAddVendor_Click(object sender, System.EventArgs e)
    {
        string URL = ResolveUrl("~/Admin/AddEditVendor.aspx?adVnd=Y");

        Globals.RegisterParentRedirectPageUrl(this, URL);               
    }

    protected void  btnNext_Click(object sender, System.EventArgs e)
    {
        if (dlWarehouses.SelectedIndex > 0)
        {
            string URL = null;
            SysWarehouses objSysWarehouses = new SysWarehouses();
            int strCompantID = objSysWarehouses.GetCompanyIdByWhsCode(dlWarehouses.SelectedValue);
            URL = "Create.aspx?vdrID=" + hdnSelectedVendorID.Value + "&ComID=" + strCompantID + "&whsID=" + dlWarehouses.SelectedValue + "&VName=" + txtSearch.Text;
            Globals.RegisterParentRedirectPageUrl(this, URL);   
        }
        else
        {
            lblMsg.Text = Resources.Resource.lblgeratePo;
        }
        //Response.Redirect(URL)
    }
    protected void  grdVendors_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains<string>("_history"))
        {
            sqldsVendor.SelectCommand = _objVendor.GetSQL(sqldsVendor.SelectParameters, "", Request.QueryString[txtSearch.ClientID]);
        }
        else
        {
            sqldsVendor.SelectCommand = _objVendor.GetSQL(sqldsVendor.SelectParameters, "", Request.QueryString[txtSearch.ClientID]);
        }
    }
    protected void grdVendors_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if ((e.ColumnIndex == 0))
        {
            string strCellText = null;
            strCellText = e.RowValues[1] + "<br />" + FillAddressInfo(AddressReference.VENDOR, AddressType.BILL_TO_ADDRESS, e.RowValues[0].ToString());
            strCellText = strCellText.Replace(Environment.NewLine, "<br />");
            e.CellHtml = strCellText;
        }
        if ((e.ColumnIndex == 2))
        {
            //e.CellHtml = String.Format("<a href=""{0}"">Select</a>", "javascript:void(0);")
        }
    }
    

    protected void  grdVendors_RowSelecting(object sender, Trirand.Web.UI.WebControls.JQGridRowSelectEventArgs e)
    {
        _objVendor.VendorID = BusinessUtility.GetInt(e.RowKey);
        hdnSelectedVendorID.Value = e.RowKey;
        _objVendor.PopulateObject(null, _objVendor.VendorID);
        txtSearch.Text = _objVendor.VendorName;
        if (_objVendor.VendorID > 0 && dlWarehouses.SelectedIndex > 0)
        {
            string URL = null;
            SysWarehouses objSysWarehouses = new SysWarehouses();
            int strCompantID = objSysWarehouses.GetCompanyIdByWhsCode(dlWarehouses.SelectedValue);
            URL = "Create.aspx?vdrID=" + hdnSelectedVendorID.Value + "&ComID=" + strCompantID + "&whsID=" + dlWarehouses.SelectedValue + "&VName=" + txtSearch.Text.Replace("'"," ");
            Globals.RegisterParentRedirectPageUrl(this, URL);            
        }
        else
        {
            lblMsg.Text = Resources.Resource.lblgeratePo;
            btnNext.Visible = true;
            grdVendors.Visible = false;
        }
    }
}
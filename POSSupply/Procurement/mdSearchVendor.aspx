﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdSearchVendor.aspx.cs" Inherits="Procurement_mdSearchVendor" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphHead" runat="Server">
    
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div >
        <asp:Panel runat="server" ID="pnlSearch">
            <p>
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </p>
            <div class="searchBar" style="padding:5px;">
                <table border="0" cellpadding="5" cellspacing="5" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblVendor" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblPOVendor %>" CssClass="filter-key"/>*
                        </td>
                        <td valign="middle" align="left">
                            <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblcompanyID" AssociatedControlID="dlWarehouses" Text="<%$ Resources:Resource, lblSoWhs%>"
                                runat="server" CssClass="filter-key"/>*
                        </td>
                        <td valign="middle" align="left">
                            <asp:DropDownList ID="dlWarehouses" runat="server" Width="185px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                        </td>
                        <td style="text-align:right; display:none;">
                            <asp:Button ID="cmdAddVendor" CausesValidation="false" runat="server" Text="<%$ Resources:Resource, lblAddNewVendor%>"  OnClick="cmdAddVendor_Click" />
                        </td>
                    </tr>
                </table>
            </div>            
        </asp:Panel>
        <div id="grid_wrapper" style="width: 99%;">
            <trirand:JQGrid runat="server" ID="grdVendors" DataSourceID="sqldsVendor" Height="200px"
                AutoWidth="true" PagerSettings-PageSize="50"
                OnDataRequesting="grdVendors_DataRequesting" OnCellBinding="grdVendors_CellBinding" OnRowSelecting="grdVendors_RowSelecting">
                <Columns>
                    <trirand:JQGridColumn DataField="vendorName" HeaderText="<%$ Resources:Resource, grdVendorName %>" />
                    <trirand:JQGridColumn DataField="TotalPO" HeaderText="<%$ Resources:Resource, lblHistory %>"
                        Editable="false" />
                    <trirand:JQGridColumn HeaderText="Select" DataField="vendorId" Sortable="false" TextAlign="Center"
                        Width="50" Visible="false">
                        <Formatter>
                            <trirand:CustomFormatter FormatFunction="formatEditLink" UnFormatFunction="unformatEditLink" />
                        </Formatter>
                    </trirand:JQGridColumn>
                    <trirand:JQGridColumn HeaderText="" DataField="vendorId" Visible="false" Sortable="false"
                        TextAlign="Center" Width="50" PrimaryKey="True">
                    </trirand:JQGridColumn>
                </Columns>
                <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                    ShowDeleteButton="false" ShowSearchButton="false" />
                <SortSettings InitialSortColumn="" />
                <AppearanceSettings AlternateRowBackground="True" />
            </trirand:JQGrid>
            <asp:SqlDataSource ID="sqldsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
        </div>
        <h2>
        </h2>
        <br />
        <div align="right">
            <asp:Button ID="btnNext" runat="server" Text="<%$ Resources:Resource, cmdCssNext%>"
                Visible="false" OnClick="btnNext_Click" />
        </div>
    </div>
    <asp:HiddenField ID="hdnSelectedVendorID" runat="server" />
    <asp:HiddenField ID="hdnSelectedVendorName" runat="server" />
    <asp:HiddenField ID="hdnCompanyID" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdVendors.ClientID %>";
        var searchPanelID = "<%=pnlSearch.ClientID %>";
        var hdnVendorId = "<%=hdnSelectedVendorID.ClientID %>";
        var hdnVendorName = "<%=hdnSelectedVendorName.ClientID %>";

        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }


        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }
        function formatEditLink(cellValue, options, rowObject) {
            var linkHtml = "<a class='select_row' href='javascript:void(0);' originalValue='" + cellValue + "'>Select</a>";
            return linkHtml;
        }

        function unformatEditLink(cellValue, options, cellObject) {
            return $(cellObject.html()).attr("originalValue");
        }

        function onVendorSelect(ids) {

        }
    </script>
</asp:Content>

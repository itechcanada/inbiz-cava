﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using iTECH.WebControls;

using Trirand.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Procurement_mdSearchProduct : BasePage
{
    private PurchaseOrders _objPurOrd = new PurchaseOrders();
    protected string sPHtml = "";
    protected void Page_Load(object sender, System.EventArgs e)
    {
        MessageState.SetGlobalMessage(MessageType.Success, "");
        if (!IsPagePostBack(grdProducts))
        {
            SearchBy = null;
            SearchText = null;
            SysWarehouses wh = new SysWarehouses();
            wh.FillAllWharehouse(null, dlWarehouse, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));

            ListItem liWhs = dlWarehouse.Items.FindByValue(this.WarehouseCode);
            foreach (ListItem liItem in dlWarehouse.Items)
            {
                if (liItem.Selected == true)
                    liItem.Selected = false;
            }
            if (liWhs != null)
            {
                liWhs.Selected = true;
            }
            else
            {
                liWhs = dlWarehouse.Items.FindByValue(CurrentUser.UserDefaultWarehouse);
                if (liWhs != null)
                {
                    liWhs.Selected = true;
                }
            }

            if (this.ItemID > 0)
            {
                FillEditItemDetails(this.ItemID);
                pnlSearch.Visible = false;
                pnlEditProductQuantity.Visible = true;

                txtQuantity.Focus();
            }
            else
            {
                txtSearch.Focus();
            }
        }
    }

    public string WarehouseCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["whsID"]);
        }
    }

    protected void grdProducts_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        SearchBy = Request.QueryString[dlSearch.ClientID];
        if (SearchBy != "QS")
        {
            SearchText = Request.QueryString[txtSearch.ClientID];
            string SearchCollection = Request.QueryString["_Collection"];
            sqldsProcess.SelectCommand = _objPurOrd.GetSQLProduct(sqldsProcess.SelectParameters, SearchBy, SearchText, Globals.CurrentAppLanguageCode, BusinessUtility.GetInt(SearchCollection));
        }
    }

    private int VendorID
    {
        get
        {
            int vid = 0;
            int.TryParse(Request.QueryString["VdrID"], out vid);
            return vid;
        }
    }

    private int ItemID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["editItemID"], out id);
            return id;
        }
    }

    public string SearchBy
    {
        get { return ((Session["SearchBy"] != null) ? Convert.ToString(Session["SearchBy"]) : string.Empty); }
        set { Session["SearchBy"] = value; }
    }

    public string SearchText
    {
        get { return ((Session["SearchText"] != null) ? Session["SearchText"].ToString() : string.Empty); }
        set { Session["SearchText"] = value; }
    }

    //protected void  imgSearch_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    //{
    //    //SearchBy = dlSearch.SelectedValue;
    //    //SearchText = txtSearch.Text;
    //}


    protected void btnQuickSearch_OnClick(object sender, System.EventArgs e)
    {
        Product objProduct = new Product();
        int productID = objProduct.GetProductID(null, txtSearch.Text);
        if (productID > 0)
        {

            hdnProcessProductID.Value = BusinessUtility.GetString(productID);
            sdsProductDetails.SelectCommand = _objPurOrd.GetSQLProductDetailSql(sdsProductDetails.SelectParameters, BusinessUtility.GetString(productID), Globals.CurrentAppLanguageCode);
            DataView dv = (DataView)sdsProductDetails.Select(DataSourceSelectArguments.Empty);
            if (dv.Count > 0)
            {
                txtProductId.Text = Convert.ToString(dv[0]["productID"]);
                txtUPCCode.Text = Convert.ToString(dv[0]["prdUPCCode"]);
                txtProductName.Text = Convert.ToString(dv[0]["prdName"]);
                hdnVendorID.Value = Convert.ToString(dv[0]["vendorID"]);
                txtVendorName.Text = Convert.ToString(dv[0]["vendorName"]);
                txtQuantity.Text = "1";//Convert.ToString(dv[0]["prdPOQty"]);
                //txtPrice.Text = Convert.ToString(dv[0]["prdCostPrice"]);
                //txtPrice.Text = Convert.ToString(dv[0]["prdFOBPrice"]);
                //txtLandedPrice.Text = Convert.ToString(dv[0]["prdLandedPrice"]);

                txtPrice.Text = CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(dv[0]["prdFOBPrice"].ToString().Replace(",", ".")));
                txtLandedPrice.Text = CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(dv[0]["prdLandedPrice"].ToString().Replace(",", ".")));

                //pnlSearch.Visible = false;
                //pnlEditProductQuantity.Visible = true;

                if (this.ItemID > 0)
                {
                    EditToCart(this.ItemID);
                }
                else
                {
                    AddToCart();
                }

            }
            Globals.RegisterScript(Page, "parent.reloadGrid(); ");
            //Globals.RegisterCloseDialogScript(Page, "parent.reloadGrid();", true);
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblInvalidSKU);
        }
        txtSearch.Text = "";
        txtSearch.Focus();
    }

    protected void grdProducts_RowSelecting(object sender, Trirand.Web.UI.WebControls.JQGridRowSelectEventArgs e)
    {
        hdnProcessProductID.Value = e.RowKey;
        sdsProductDetails.SelectCommand = _objPurOrd.GetSQLProductDetailSql(sdsProductDetails.SelectParameters, e.RowKey, Globals.CurrentAppLanguageCode);
        DataView dv = (DataView)sdsProductDetails.Select(DataSourceSelectArguments.Empty);
        if (dv.Count > 0)
        {
            txtProductId.Text = Convert.ToString(dv[0]["productID"]);
            txtUPCCode.Text = Convert.ToString(dv[0]["prdUPCCode"]);
            txtProductName.Text = Convert.ToString(dv[0]["prdName"]);
            hdnVendorID.Value = Convert.ToString(dv[0]["vendorID"]);
            txtVendorName.Text = Convert.ToString(dv[0]["vendorName"]);
            txtQuantity.Text = Convert.ToString(dv[0]["prdPOQty"]);
            //txtPrice.Text = Convert.ToString(dv[0]["prdCostPrice"]);
            //txtPrice.Text = Convert.ToString(dv[0]["prdFOBPrice"]);
            //txtLandedPrice.Text = Convert.ToString(dv[0]["prdLandedPrice"]);

            txtPrice.Text = CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(dv[0]["prdFOBPrice"].ToString().Replace(",", ".")));
            txtLandedPrice.Text = CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(dv[0]["prdLandedPrice"].ToString().Replace(",", ".")));

            pnlSearch.Visible = false;
            pnlEditProductQuantity.Visible = true;
        }

        txtQuantity.Focus();
    }


    protected void imgAdd_Click(object sender, System.EventArgs e)
    {
        if (IsValid)
        {
            if (this.ItemID > 0)
            {
                EditToCart(this.ItemID);
                //AddToCart()
            }
            else
            {
                lblMsg.Text = "";
                if (BusinessUtility.GetDouble(txtQuantity.Text) <= 0)
                {
                    lblMsg.Text = Resources.Resource.msgPOItemQuantity;
                    //"Please enter PO Item Quantity."
                    txtQuantity.Focus();
                    return;
                }
                //if (BusinessUtility.GetDouble()
                //{
                //    lblMsg.Text = Resources.Resource.msgPOItemPrice;
                //    //"Please enter PO Item Price."
                //    txtPrice.Focus();
                //    return;
                //}
                if (dlWarehouse.SelectedValue == "0")
                {
                    lblMsg.Text = Resources.Resource.custvalPOWarehouse;
                    //"Please select Warehouse."
                    dlWarehouse.Focus();
                    return;
                }
                AddToCart();
            }
        }
        Globals.RegisterCloseDialogScript(Page, "parent.reloadGrid();", true);
    }

    //On Edit To Cart
    private void EditToCart(int iteID)
    {
        PurchaseOrderCart cart = PurchaseOrderCartHelper.CurrentCart;
        if (cart != null && cart.Items.Count > 0)
        {
            PurchaseOrderCartItem item = PurchaseOrderCartHelper.GetItem(iteID);

            item.ProductID = BusinessUtility.GetInt(txtProductId.Text);
            item.ProductName = txtProductName.Text;
            item.UPCCode = txtUPCCode.Text;
            item.VendorID = this.VendorID;
            item.VendorName = txtVendorName.Text;
            item.WarehouseCode = dlWarehouse.SelectedValue;
            item.Quantity = BusinessUtility.GetDouble(txtQuantity.Text);
            item.Price = BusinessUtility.GetDouble(txtPrice.Text);
            item.LandedPrice = BusinessUtility.GetDouble(txtLandedPrice.Text);

            PurchaseOrderCartHelper.EditCartItem(item);
        }
    }

    private void FillEditItemDetails(int iteID)
    {
        PurchaseOrderCart cart = PurchaseOrderCartHelper.CurrentCart;
        if (cart != null && cart.Items.Count > 0)
        {
            PurchaseOrderCartItem item = PurchaseOrderCartHelper.GetItem(iteID);
            txtProductId.Text = item.ProductID.ToString();
            txtProductName.Text = item.ProductName;
            txtUPCCode.Text = item.UPCCode;
            hdnVendorID.Value = item.VendorID.ToString();
            txtVendorName.Text = item.VendorName;
            txtQuantity.Text = item.Quantity.ToString();
            txtPrice.Text = item.Price.ToString();
            dlWarehouse.SelectedValue = item.WarehouseCode;
            txtLandedPrice.Text = item.LandedPrice.ToString();
        }
    }


    // Add Cart
    public void AddToCart()
    {
        PurchaseOrderCartItem item = new PurchaseOrderCartItem();

        int pid = BusinessUtility.GetInt(txtProductId.Text);
        string strWHS = dlWarehouse.SelectedIndex > 0 ? dlWarehouse.SelectedValue : CurrentUser.UserDefaultWarehouse;
        item.ProductID = BusinessUtility.GetInt(txtProductId.Text);
        item.ProductName = txtProductName.Text;
        item.UPCCode = txtUPCCode.Text;
        item.VendorID = this.VendorID;
        item.VendorName = txtVendorName.Text;
        item.WarehouseCode = strWHS;
        item.Quantity = BusinessUtility.GetDouble(txtQuantity.Text);
        item.Price = BusinessUtility.GetDouble(txtPrice.Text);
        item.LandedPrice = BusinessUtility.GetDouble(txtLandedPrice.Text);

        Product objProduct = new Product();
        DataTable dtProduct = objProduct.GetProductColorSize(null, BusinessUtility.GetInt(txtProductId.Text));
        foreach (DataRow drItem in dtProduct.Rows)
        {
            item.ProductColor = BusinessUtility.GetString(drItem["Color"]);
            item.ProductSize = BusinessUtility.GetString(drItem["Size"]);
        }

        PurchaseOrderCartHelper.AddToCart(item, true);
    }

    protected void grdProducts_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if ((e.ColumnIndex == 10))
        {
            e.CellHtml = string.Format("<a href=\"{0}\">Select</a>", "javascript:void(0);");
        }
        else if (e.ColumnIndex == 9)
        {
            e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
            //e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml.Replace(",",".")), Globals.CurrentCultureName);
        }
    }


    protected void btnSelectedStyle_Onclick(object sender, EventArgs e)
    {
        imgAdd.Visible = false;
        liBarCode.Visible = false;
        liPrice.Visible = false;
        liQty.Visible = false;
        //liDiscount.Visible = false;
        btnAddProduct.Visible = true;
        txtProductName.Text = Convert.ToString(hdnPname.Value);
        PurchaseOrders prd = new PurchaseOrders();
        string sStyle = "";
        sStyle = hdnStyle.Value;
        if (dlSearch.SelectedValue == "ST" && sStyle == "")
        {
            sStyle = txtSearch.Text;
        }
        DataTable dt = prd.GetProductByStyleCollection(null, BusinessUtility.GetString(sStyle), 0, this.ProductType, Globals.CurrentAppLanguageCode, BusinessUtility.GetInt(hdnCollection.Value));
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                var result = from row in dt.AsEnumerable()
                             group row by row.Field<string>("Size") into grp
                             select new
                             {
                                 Size = grp.Key,
                                 MemberCount = grp.Count(),
                                 Sum = grp.Sum(r => BusinessUtility.GetInt(r["PrdPOQty"].ToString()))

                             };

                string sHtmlHeading = "<tr> ";
                sHtmlHeading += "<td> </td>";
                string sHtmlSizeTotalContent = "";
                int iItemSizeCount = 0;
                string sHtmlContent = "";

                foreach (var item in result)
                {
                    sHtmlHeading += "<td> <b> " + BusinessUtility.GetString(item.Size) + " </b> </td>";
                    iItemSizeCount = BusinessUtility.GetInt(item.MemberCount);
                    sHtmlSizeTotalContent += "<td style='vertical-align:top;'><span id=" + "sizetotal_" + BusinessUtility.GetString(item.Size).Replace("/", "_").Replace(" ", "_") + " type='text' style='width:100px;font-size:13px;font-weight:bold;'>" + BusinessUtility.GetInt(item.Sum) + "</span>  </td> ";
                }

                sHtmlHeading += "<td style='font-size:13px'><b> " + Resources.Resource.lblTotal + " </b> </td>";
                sHtmlHeading += "<td><b> " + Resources.Resource.lblFOBPrice + " </b> </td>";
                sHtmlHeading += "<td><b> " + Resources.Resource.lblLandedPrice + "</b> </br> </td>";
                sHtmlHeading += "</tr>";




                DataTable dtProductColor = prd.GetProductGroupBYColor(null, BusinessUtility.GetString(hdnStyle.Value), 0, this.ProductType, Globals.CurrentAppLanguageCode, BusinessUtility.GetInt(hdnCollection.Value));
                int i = 0;
                int sGrandTotal = 0;
                foreach (DataRow dRow in dtProductColor.Rows)
                //foreach (DataRow dRow in dt.Rows)
                {
                    string sClass = "cssPqty";
                    sHtmlContent += "<tr>";
                    sHtmlContent += "<td>Color  <b> " + BusinessUtility.GetString(dRow["Color"]) + "</b> </br>  SKU #    </td> ";
                    DataView datavw = new DataView();
                    datavw = dt.DefaultView;
                    datavw.RowFilter = "Color='" + BusinessUtility.GetString(dRow["Color"]) + "'";
                    DataTable dtProduct = datavw.ToTable();
                    int sColorQty = 0;
                    if (dtProduct.Rows.Count > 0)
                    {
                        string sCss = "";
                        string sColor = BusinessUtility.GetString(dRow["Color"]).Trim();
                        string sColorTotalID = "total_" + BusinessUtility.GetString(dRow["productID"]);
                        string sColorID = "clr_" + BusinessUtility.GetString(dRow["ColorID"]).Trim();
                        string sFunctionCall = "onblur='CalcColorTotalQty(this);'";
                        foreach (DataRow drProduct in dtProduct.Rows)
                        {
                            string sSize = BusinessUtility.GetString(drProduct["Size"]).Trim().Replace("/", "_").Replace(" ", "_");
                            string sSizeTotalID = "sizetotal_" + sSize;
                            sCss = sClass + " " + sColorID + " " + sSize + " " + "integerTextField";
                            sHtmlContent += "<td><input id=" + "qty" + BusinessUtility.GetString(drProduct["productID"]) + " value ='" + BusinessUtility.GetString(BusinessUtility.GetInt(drProduct["PrdPOQty"])) + "' " + sFunctionCall + "  vid='" + BusinessUtility.GetString(this.VendorID) + "'  pname='" + BusinessUtility.GetString(drProduct["prdName"]) + "' pbarcode='" + BusinessUtility.GetString(drProduct["prdUPCCode"]) + "'  class='" + sCss + "' ppid=" + BusinessUtility.GetString(dRow["productID"]) + "   pid=" + BusinessUtility.GetString(drProduct["productID"]) + "  color=" + sColorID + "  totalToSave=" + sColorTotalID + "  sizeS=" + BusinessUtility.GetString(sSize) + " sizetotalToSave=" + sSizeTotalID + "  type='text'   style='width:100px;'/> </br> " + BusinessUtility.GetString(drProduct["prdUPCCode"]) + " </td> ";
                            sColorQty += BusinessUtility.GetInt(drProduct["PrdPOQty"]);
                        }
                    }
                    sGrandTotal += sColorQty;
                    sHtmlContent += "<td style='vertical-align:top;'><span id=" + "total_" + BusinessUtility.GetString(dRow["productID"]) + " type='text' class='colorTotal' style='width:100px;font-size:13px;font-weight:bold;'>" + sColorQty + "</span>  </td> ";
                    sHtmlContent += "<td style='vertical-align:top;'><input id=" + "price_" + BusinessUtility.GetString(dRow["productID"]) + " type='text' value ='" + CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(dRow["prdFOBPrice"].ToString().Replace(",", "."))) + "' readonly='readonly'  CssClass='numericTextField'   style='width:100px;'/>  </td> "; //value='" + BusinessUtility.GetString(dRow["Price"]) + "'
                    sHtmlContent += "<td style='vertical-align:top;'><input id=" + "Discount_" + BusinessUtility.GetString(dRow["productID"]) + " type='text' value ='" + CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(dRow["prdLandedPrice"].ToString().Replace(",", "."))) + "' readonly='readonly' CssClass='numericTextField'  style='width:100px;'/>  </td> ";
                    sHtmlContent += "</tr>";
                    i += 1;
                }

                sHtmlContent += "<tr>";
                sHtmlContent += "<td style='font-size:13px'><b> " + Resources.Resource.lblTotal + " </b> </td>";
                sHtmlContent += sHtmlSizeTotalContent;
                sHtmlContent += "<td style='vertical-align:top;'><span id='grandtotal' type='text' style='width:100px;font-size:15px;font-weight:bold;'>" + sGrandTotal + "</span>  </td><td>&nbsp;</td><td>&nbsp;</td>";

                string sHtmlTable = "<table style='border:1;width:100%;padding:5px;'> ";
                sHtmlTable += sHtmlHeading;
                sHtmlTable += sHtmlContent;
                sHtmlTable += "</table> ";
                sPHtml = sHtmlTable;
            }
        }

        txtQuantity.Text = "1";
        pnlSearch.Visible = false;
        pnlEditProductQuantity.Visible = true;
    }

    private StatusProductType ProductType
    {
        get
        {
            return QueryStringHelper.GetProductType("pType");
        }
    }

    private DataTable TempCart
    {
        get
        {
            if (Session["SRCart"] != null)
            {
                return (DataTable)Session["SRCart"];
            }
            MakeCart();
            return (DataTable)Session["SRCart"];
        }
        set
        {
            if (value != null)
            {
                Session["SRCart"] = value;
            }
            else
            {
                Session.Remove("SRCart");
            }
        }
    }

    public void MakeCart()
    {
        DataTable dtCart = new DataTable("PO_ITEMS_CART");
        DataColumn colPk = new DataColumn("ProductID", typeof(int));

        dtCart.Columns.Add(colPk);
        dtCart.Columns.Add("UPCCode", typeof(string));
        dtCart.Columns.Add("ProductName", typeof(string));
        dtCart.Columns.Add("VendorID", typeof(int));
        dtCart.Columns.Add("VendorName", typeof(string));
        dtCart.Columns.Add("Quantity", typeof(double));
        dtCart.Columns.Add("Price", typeof(double));
        dtCart.Columns.Add("WarehouseCode", typeof(string));
        dtCart.PrimaryKey = new DataColumn[] { colPk };

        Session["SRCart"] = dtCart;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Collection> GetStyleCollection(string style)
    {
        Collection objCollection = new Collection();
        var t = objCollection.GetStyleCollection(null, style, Globals.CurrentAppLanguageCode);
        return t;
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SaveProductDetails(List<productDetail> ProductDetail) //List<productDetail> ProductDetail
    {

        try
        {

            var lstProductDetail = ProductDetail;
            //if (IsValid)
            foreach (var itemP in lstProductDetail)
            {
                if (BusinessUtility.GetDouble(itemP.PQTY) > 0)
                {
                    PurchaseOrderCartItem item = new PurchaseOrderCartItem();
                    int pid = BusinessUtility.GetInt(itemP.PID);
                    string strWHS = BusinessUtility.GetString(itemP.PWareHouseCode) != "" ? BusinessUtility.GetString(itemP.PWareHouseCode) : CurrentUser.UserDefaultWarehouse;// dlWarehouse.SelectedIndex > 0 ? dlWarehouse.SelectedValue : CurrentUser.UserDefaultWarehouse;
                    item.ProductID = BusinessUtility.GetInt(itemP.PID);
                    item.ProductName = BusinessUtility.GetString(itemP.PName);
                    item.UPCCode = BusinessUtility.GetString(itemP.PBarcode);
                    item.VendorID = BusinessUtility.GetInt(itemP.PvID);
                    item.VendorName = BusinessUtility.GetString(itemP.PvName);
                    item.WarehouseCode = strWHS;
                    item.Quantity = BusinessUtility.GetDouble(itemP.PQTY);

                    //item.Price = BusinessUtility.GetDoubleValueFromCurrencyString(itemP.PPrice, Globals.CurrentCultureName);
                    //item.LandedPrice = BusinessUtility.GetDoubleValueFromCurrencyString(itemP.PDisc, Globals.CurrentCultureName);

                    item.Price = BusinessUtility.GetDouble(itemP.PPrice);
                    item.LandedPrice = BusinessUtility.GetDouble(itemP.PDisc);

                    Product objProduct = new Product();
                    DataTable dtProduct = objProduct.GetProductColorSize(null, BusinessUtility.GetInt(pid));
                    foreach (DataRow drItem in dtProduct.Rows)
                    {
                        item.ProductColor = BusinessUtility.GetString(drItem["Color"]);
                        item.ProductSize = BusinessUtility.GetString(drItem["Size"]);
                    }

                    PurchaseOrderCartHelper.AddToCart(item, true);
                }
            }
            return "success";
        }
        catch (Exception e)
        {
            return "false";
        }
    }


    public class productDetail
    {
        public string PID { get; set; }
        public string PQTY { get; set; }
        public string PDisc { get; set; }
        public string PPrice { get; set; }
        //public string PDType { get; set; }
        public string PName { get; set; }
        public string PBarcode { get; set; }
        //public string PTaxGroup { get; set; }
        //public string PTaxDesc { get; set; }
        public string PWareHouseCode { get; set; }
        public string PvID { get; set; }
        public string PvName { get; set; }
    }
}
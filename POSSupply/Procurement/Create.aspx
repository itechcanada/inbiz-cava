﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="Create.aspx.cs" Inherits="Procurement_Create" %>

<asp:Content ID="ctHead" runat="server" ContentPlaceHolderID="cphHead">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            Administration</h1>
        <b>
            <asp:Literal runat="server" ID="lblTitle"></asp:Literal></b>
        <br />
        <b>
            <asp:Label ID="lblName" runat="server" CssClass="leftHeading">
            </asp:Label></b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <%--<span id="spAddProducts" runat="server">
            <asp:Button ID="btnSearchProduct" runat="server" Text="Add Order Item"  />
            <iCtrl:IframeDialog ID="ifrdSearchProduct" TriggerControlID="btnSearchProduct" Width="800"
                Height="510" Url="mdSearchProduct.aspx?VdrID=" Title="Search Product" runat="server"
                OnCloseClientFunction="reloadGrid">
            </iCtrl:IframeDialog>
        </span>--%>
    </div>
    <div style="float: right;">
        <span id="prdTagPrinting" runat="server">
            <asp:Button ID="btnPrintTags" Text="<%$Resources:Resource, lblPrintTags %>" runat="server"
                CausesValidation="false" />
            <%-- <iCtrl:IframeDialog ID="mdTagPrinting" TriggerControlID="btnPrintTags"
                Width="550" Height="230" Url="~/Receiving/PrintTagsNew.aspx" Title="Print Tags" runat="server">
            </iCtrl:IframeDialog>--%>
        </span><span id="spnFax" runat="server">
            <asp:LinkButton CssClass="inbiz_icon icon_fax_28" runat="server" ID="imgFax" />
            <iCtrl:IframeDialog ID="mdFax" TriggerControlID="imgFax" Width="550" Height="260"
                Url="~/Common/mdSendMailOrFax.aspx" Title="<%$Resources:Resource, lblPOFax %>"
                runat="server"></iCtrl:IframeDialog>
        </span><span id="spnMail" runat="server">
            <asp:LinkButton CssClass="inbiz_icon icon_email_28" runat="server" ID="imgMail" />
            <iCtrl:IframeDialog ID="mdMail" TriggerControlID="imgMail" Width="550" Height="260"
                Url="~/Common/mdSendMailOrFax.aspx" Title="<%$Resources:Resource, lblPOMail %>"
                runat="server"></iCtrl:IframeDialog></span> <span id="spnPrint" runat="server">
                    <asp:LinkButton CssClass="inbiz_icon icon_print_28" runat="server" ID="imgPdf" CausesValidation="false" />
                </span>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeft" runat="Server">
    <ul id="orderMenu" class="menu inbiz-left-menu collapsible">
        <li class="active" id="divAddProduct" runat="server"><a href="#">Action</a>
            <ul style="display: block;">
                <li class="open">
                    <asp:HyperLink ID="popAddProduct" CssClass="addProduct" NavigateUrl="#" runat="server"
                        ToolTip="<%$Resources:Resource, lblAddProduct%>" Text="<%$Resources:Resource, lblAddProduct%>">                                      
                    </asp:HyperLink>
                    <iCtrl:IframeDialog ID="ifrdSearchProduct" Width="1000" TriggerSelectorMode="Class"
                        TriggerSelectorClass="addProduct" Height="150" Position="top" UrlSelector="TriggerControlHrefAttribute"
                        runat="server"></iCtrl:IframeDialog><%--Height="510" --%>
                </li>
            </ul>
        </li>
        <li class="active"><a href="#">Navigation</a>
            <ul style="display: block;">
                <li>
                    <asp:HyperLink ID="hlviewSalesOrders" CssClass="" NavigateUrl="~/Procurement/ViewPurchaseOrder.aspx"
                        Text="<%$Resources:Resource, lblViewPurchaseOrder%>" runat="server" /></li>
            </ul>
        </li>
    </ul>
</asp:Content>
<asp:Content ID="cntCreatePO" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <div id="divApprove" runat="server" visible="false">
                <h3>
                    <asp:Literal ID="ltTitle" Text="<%$Resources:Resource,lblPurchaseOrder %>" runat="server" />
                </h3>
                <ul class="form">
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblPONumber" Text="<%$ Resources:Resource, PONumber1 %>" CssClass="lblBold"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:Label ID="lblPOID" CssClass="lblBold" runat="server" />
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblSOPOComName" Text="<%$ Resources:Resource, lblSOPOComName %>" CssClass="lblBold"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:Label ID="lblSOPOComNameTitle" CssClass="lblBold" runat="server" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblPODate1" Text="<%$ Resources:Resource, lblPODate %>" CssClass="lblBold"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:Label ID="lblPODate" CssClass="lblBold" runat="server" />
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblSOPOwhs" Text="<%$ Resources:Resource, lblSOPOwhs %>" CssClass="lblBold"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:Label ID="lblSOPOwhsName" CssClass="lblBold" runat="server" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblVendorName1" Text="<%$ Resources:Resource, POVendorName1 %>" CssClass="lblBold"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:Label ID="lblVendorName" CssClass="lblBold" runat="server" />
                        </div>
                        <div class="lbl">
                            <asp:Label ID="Label3" Text="<%$ Resources:Resource, POFax %>" CssClass="lblBold"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:Label ID="lblFax" CssClass="lblBold" runat="server" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="Label21" Text="<%$ Resources:Resource, lblSOPOStatus %>" CssClass="lblBold"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:Label ID="lblStatus" CssClass="lblBold" runat="server" />
                        </div>
                        <div class="lbl">
                            <asp:Label ID="Label2" Text="<%$ Resources:Resource, POAddress %>" CssClass="lblBold"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:Label ID="lblAddress" CssClass="lblBold" runat="server" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                </ul>
            </div>
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="grdProducts" Height="100%" PagerSettings-PageSize="50"
                    AutoWidth="true" OnDataRequesting="grdProducts_DataRequesting" OnCellBinding="grdProducts_CellBinding">
                    <Columns>
                        <trirand:JQGridColumn DataField="ProductID" HeaderText="<%$ Resources:Resource, grdPoProductID %>"
                            PrimaryKey="false" />
                        <trirand:JQGridColumn DataField="UPCCode" HeaderText="<%$ Resources:Resource, grdPOUPCCode %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="ProductName" HeaderText="<%$ Resources:Resource, grdPOProductName %>" />
                        <trirand:JQGridColumn DataField="ProductColor" HeaderText="<%$ Resources:Resource, grdvPrdColor %>" />
                        <trirand:JQGridColumn DataField="ProductSize" HeaderText="<%$ Resources:Resource, lblProductSize %>" />
                        <trirand:JQGridColumn DataField="VendorName" HeaderText="<%$ Resources:Resource, grdPOVendorName %>"
                            Visible="false" />
                        <trirand:JQGridColumn DataField="Quantity" HeaderText="<%$ Resources:Resource, grdPOPrdQuantity %>" />
                        <trirand:JQGridColumn DataField="Price" HeaderText="<%$ Resources:Resource, lblFOBPrice %>"
                            DataFormatString="{0:F}" />
                        <trirand:JQGridColumn DataField="LandedPrice" HeaderText="<%$ Resources:Resource, lblLandedPrice %>"
                            DataFormatString="{0:F}" />
                        <trirand:JQGridColumn DataField="WarehouseCode" HeaderText="<%$ Resources:Resource, grdPOWhsCode %>" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="vendorId"
                            Sortable="false" TextAlign="Center" Width="50" />
                        <trirand:JQGridColumn DataField="ID" Visible="false" PrimaryKey="true" />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn="" />
                    <AppearanceSettings AlternateRowBackground="True" />
                    <ClientSideEvents LoadComplete="resize_the_grid" BeforeRowSelect="beforeRowSelect" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sqldsRequest" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
                <iCtrl:IframeDialog ID="mdEditProduct" Height="510" Width="800" Title="Edit Product"
                    Dragable="true" TriggerSelectorClass="edit-Product" TriggerSelectorMode="Class"
                    OnCloseClientFunction="reloadGrid" UrlSelector="TriggerControlHrefAttribute"
                    runat="server">
                </iCtrl:IframeDialog>
            </div>
            <br />
            <div style="margin-top: 5px; border-top: 1px solid #ccc;">
                <table id="divTotalSummary" border="0" cellpadding="2" cellspacing="0" style="display: none;
                    float: right; font-weight: 700; text-align: right;">
                    <%--<tr><td>Total Amount Vaneodr Currency</td><td>HKD 0.00</td></tr>
                    <tr><td>Total Amount Vaneodr CDN</td><td>CDN 0.00</td></tr>
                    <tr><td>Exchange Rate</td><td>0.722</td></tr>--%>
                </table>
                <div style="clear: both;">
                </div>
            </div>
            <br />
            <div>
                <ul class="form">
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblSoNo" CssClass="lblBold" Text="<%$ Resources:Resource, lblSoNo %>"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtSoNo" runat="server" Width="70px">
                            </asp:TextBox>
                            <span id="msgbox" style="display: none; color: #000;"></span>
                        </div>
                        <div class="lbl" style="display: none;">
                            <asp:Label ID="Label1" CssClass="lblBold" Text="Customer Name:" runat="server" />
                        </div>
                        <div class="input" style="display: none;">
                            <asp:TextBox ID="txtCustName" runat="server" Width="400px" ReadOnly="true">
                            </asp:TextBox>
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblYourRef" CssClass="lblBold" Text="<%$ Resources:Resource, lblYourRef %>" runat="server" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtYourRef" runat="server" Width="200px" MaxLength="100">
                            </asp:TextBox>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblTerms" CssClass="lblBold" Text="<%$ Resources:Resource, POShippingTerms%>"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtTerms" runat="server" TextMode="MultiLine" Width="200px"></asp:TextBox>
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblNotes" CssClass="lblBold" Text="<%$ Resources:Resource, PONotes%>"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Width="200px"></asp:TextBox>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li id="liCurrency" runat="server" visible="false">
                        <div class="lbl">
                            <asp:Label ID="lblPOCurrencyCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOCurrencyCode%>"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:DropDownList ID="dlCurrencyCode" runat="server" Width="135px">
                            </asp:DropDownList>
                            <span class="style1">*</span>
                            <asp:CustomValidator ID="custvalPOCurrencyCode" runat="server" SetFocusOnError="true"
                                ErrorMessage="<%$ Resources:Resource, custvalPOCurrencyCode%>" ClientValidationFunction="funCheckCurrencyCode"
                                Display="None">
                            </asp:CustomValidator>
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblPOExRate" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOExRate%>"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtExRate" runat="server" Width="130px" MaxLength="5">
                            </asp:TextBox>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblShipVia" CssClass="lblBold" Text="<%$ Resources:Resource, POShipVia%>"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtShipVia" runat="server" Width="200px" onkeypress="return disableEnterKey(event)"></asp:TextBox>
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblFOBLoc" CssClass="lblBold" Text="<%$ Resources:Resource, POFOBLocation%>"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtFOBLoc" runat="server" Width="200px" onkeypress="return disableEnterKey(event)"></asp:TextBox>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li id="liStatus" runat="server" visible="false">
                        <div class="lbl">
                            <asp:Label ID="lblPOStatus" CssClass="lblBold" Text="<%$ Resources:Resource, lblStatus%>"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:DropDownList ID="dlPOStatus" runat="server" Width="135px" ValidationGroup="Procurement">
                            </asp:DropDownList>
                            <span class="style1">*</span>
                            <asp:CustomValidator ID="custvalPOStatus" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, custvalPOStatus%>"
                                ClientValidationFunction="funCheckPOStatus" Display="None" ValidationGroup="Procurement">
                            </asp:CustomValidator>
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblReason" runat="server" CssClass="lblBold" Font-Bold="true" Text="<%$ Resources:Resource, lblReason%>"></asp:Label>
                        </div>
                        <div class="input">
                            <asp:DropDownList ID="ddlReason" runat="server" Width="170px"  >
                                            <%--<asp:ListItem  Text="<%$Resources:Resource, lblSelectReason%>" Value="" Selected="True" />--%>
                                            <asp:ListItem Text="<%$Resources:Resource, lblVoided%>" Value="VOD" Selected="True" />
                                            <asp:ListItem Text="<%$Resources:Resource, lblNoInventory%>" Value="NOI" />
                                            <asp:ListItem Text="<%$Resources:Resource, lblCreatedInError%>" Value="CIE" />
                                            <asp:ListItem Text="<%$Resources:Resource, lblReceived%>" Value="REC" />
                                        </asp:DropDownList>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li id="liReceiving" runat="server" visible="false">
                        <div class="lbl">
                            <asp:Label ID="lblRecDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblReceivingDate%>">
                            </asp:Label>
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtReceivingDate" CssClass="datepicker" Enabled="true" ValidationGroup="Procurement"
                                runat="server" Width="90px" MaxLength="15">
                            </asp:TextBox>
                            <%=Resources.Resource.lblPODateFormat %>
                            <asp:RequiredFieldValidator ID="reqvalRcvDate" runat="server" ControlToValidate="txtReceivingDate"
                                Enabled="false" ErrorMessage="<%$ Resources:Resource, msgPOPlzEntRcvDate%>" SetFocusOnError="true"
                                Display="None" ValidationGroup="Procurement"></asp:RequiredFieldValidator>
                            <div style="display: none;">
                                <asp:TextBox ID="txtDate" Enabled="true" runat="server" Width="0px" MaxLength="15">
                                </asp:TextBox>
                            </div>
                            <asp:CompareValidator ID="cmpvalRcvDate" EnableClientScript="true" SetFocusOnError="true"
                                ControlToValidate="txtReceivingDate" ControlToCompare="txtDate" Operator="GreaterThanEqual"
                                Type="Date" Display="None" ValidationGroup="Procurement" ErrorMessage="<%$ Resources:Resource, msgPOPlzEntRcvDateGrtEqlCurDate%>"
                                runat="server" />
                            <asp:CustomValidator ID="custvalRcvDate" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, msgPOPlzEntRcvDate%>"
                                ClientValidationFunction="funCheckReceivingDate" Display="None" ValidationGroup="Procurement">
                            </asp:CustomValidator>
                        </div>
                    </li>
                </ul>
                <%--<div id="divApproveCommand" runat="server" style="text-align: right;" visible="false">
                    <asp:Button ID="btnSaveApprove" Text="<%$Resources:Resource, cmdCssSave%>" runat="server" OnClick="btnSaveApprove_Click" />
                    <asp:Button ID="btnApprove" Text="<%$Resources:Resource, cmdApprove%>" runat="server" OnClick="btnApprove_Click" />
                    <asp:Button ID="btnPoCancel" Text="<%$Resources:Resource, cmdCssCancel%>" runat="server" OnClick="btnPoCancel_Click" />
                </div>--%>
                <div runat="server" style="text-align: right;">
                    <asp:Button ID="btnDuplicat" Text="<%$Resources:Resource, cmdCssDuplicatePO%>" runat="server"
                        OnClick="btnDuplicat_Click" />
                    <asp:Button ID="btnSave" Text="<%$Resources:Resource, cmdCssSave%>" runat="server"
                        OnClick="btnSave_Click" />
                    <asp:Button ID="btnReset" Text="<%$Resources:Resource, cmdCssCancel%>" runat="server"
                        OnClick="btnReset_Click" />
                </div>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="Procurement" />
                <asp:ValidationSummary ID="valsProcurement" runat="server" ShowMessageBox="true"
                    ShowSummary="false" />
                <asp:ValidationSummary ID="valsProcurement1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="lstSrch" />
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnPreStatus" runat="server" />
    <asp:HiddenField runat="server" ID="hdnCreatedUserID" />
    <asp:HiddenField ID="hdnValidSoNo" runat="server" />
    <asp:HiddenField ID="hdnCtr" runat="server" />
    <asp:HiddenField ID="hdnPoID" runat="server" />
    <asp:HiddenField ID="hdnCurrencyExchangeRate" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" runat="Server">
    <script language="javascript" type="text/javascript">
        function funCheckCurrencyCode(source, args) {
            if (document.getElementById('<%=dlCurrencyCode.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
    </script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            var txtSoNo = $("#<%=txtSoNo.ClientID %>");
            var msgBox = $("#msgbox");
            var divCustName = $("#trCustName");
            var txtCustName = $("#<%=txtCustName.ClientID %>");

            txtSoNo.live("blur", function () {
                if (txtSoNo.val() == "") {
                    msgBox.hide();
                    txtCustName.val("");
                    divCustName.hide();
                    return;
                }
                //remove all the class add the messagebox classes and start fading
                msgBox.removeClass().addClass('messagebox').text('Checking...').fadeIn("slow");
                //check the username exists or not from ajax
                $.post("ValidateSONo.aspx", { sono: txtSoNo.val() }, function (data) {
                    //alert(data);
                    if (data == 'NO') //if username not avaiable
                    {
                        $("#<%=hdnValidSoNo.ClientID%>").val("");
                        msgBox.fadeTo(200, 0.1, function () //start fading the messagebox
                        {
                            //add message and change the class of the box and start fading
                            $(this).html('Please provide valid SO Number.').addClass('messageboxerror').fadeTo(900, 1);
                            divCustName.hide();
                        });
                    }
                    else {
                        $("#<%=hdnValidSoNo.ClientID%>").val(txtSoNo.val());
                        msgBox.fadeTo(200, 0.1, function ()  //start fading the messagebox
                        {
                            //add message and change the class of the box and start fading
                            $(this).html('Valid').addClass('messageboxok').fadeTo(900, 1);
                            txtCustName.val(data);
                            divCustName.show();
                        });
                    }
                });

            });

            if ($("#<%=dlPOStatus.ClientID%> :selected").val() != "C") {
                $("#<%=ddlReason.ClientID%>").hide();
                            $("#<%=lblReason.ClientID%>").hide();
                        }
        });

        function funCheckReceivingDate(source, args) {
            if (document.getElementById('<%=dlPOStatus.ClientID%>').value == "R" &&
            document.getElementById('<%=txtReceivingDate.ClientID%>').value == "") {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }
        function funCheckPOStatus(source, args) {
            if (document.getElementById('<%=dlPOStatus.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }                   
    </script>
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdProducts.ClientID %>";
        var IsChangeExchangeRate = false;


        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });

            showOrderTotalSummary();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function beforeRowSelect() {
            return false;
        }


        $("#<%=btnPrintTags.ClientID%>").click(function () {


            var url = '../Receiving/PrintTagsNew.aspx';
            window.open(url, 'Print', 'height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');
            //jQuery.FrameDialog.closeDialog();            
            return false;
        });


        //Show Total Summary
        function showOrderTotalSummary() {
            //$("#divTotalSummary");
            var dataToPost = {};
            dataToPost.sessionID = '<%=Session.SessionID%>';
            dataToPost.partnerid = '<%=hdnPoID.Value%>';
            $.ajax({
                type: "POST",
                url: "../ajax/salesAPI.aspx/GetCalculatedPOTotalHtml",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(dataToPost),
                success: function (data) {

                    //                    alert($("#<%=hdnCurrencyExchangeRate.ClientID%>").val());

                    $("#divTotalSummary").show();
                    $("#divTotalSummary").html(data.d[0]);
                    //                    $("#subTotalSummary2").html(data.d[1]);
                    //                    $("#divTotalSummary").html(data.d[2]);
                    //                    $("#divPayHistory").html(data.d[3]);
                    //                    $("#dvGP").html(data.d[4]); //Added by mukesh 20130607                     
                    //                    $("#dvGPPercentage").html(data.d[5]); //Added by mukesh 20130607                     

                    //                    $(".opener").click(function () {
                    //                        $(".dialog").dialog("open");
                    //                        return false;
                    //                    });
                },
                error: function (request, status, errorThrown) {
                    alert(status);


                }
            });
        }


        $(".SetExchangeRate").live('keyup', function (e) {
            var sExcRate = Number($("#txtExchangeRate").val()); //.toFixed(2)
            $("#<%=hdnCurrencyExchangeRate.ClientID%>").val(sExcRate);
        });


        $("#<%=dlPOStatus.ClientID%>").live("change", function () {
            var state = $(this).val();
            if (state == "C") {
                $("#<%=ddlReason.ClientID%>").show();
                $("#<%=lblReason.ClientID%>").show();
            }
            else {
                $("#<%=ddlReason.ClientID%>").hide();
                $("#<%=lblReason.ClientID%>").hide();
            }
        });
    </script>
</asp:Content>

﻿
Partial Class Procurement_ValidateSONo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.Form("sono") IsNot Nothing And Request.Form("sono") <> "" Then
            Dim sono As Integer
            If Integer.TryParse(Request.Form("sono"), sono) Then
                Dim objOrder As New clsOrders()
                Dim dt As Data.DataTable = objOrder.IsValidSO(Request.Form("sono"))
                If dt IsNot Nothing And dt.Rows.Count > 0 Then
                    ltResult.Text = dt.Rows(0)("CustName")
                Else
                    ltResult.Text = "NO"
                End If
            Else
                ltResult.Text = "NO"
            End If
        Else
            ltResult.Text = "NO"
        End If
    End Sub

End Class

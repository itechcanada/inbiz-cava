﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Sample.aspx.cs" Inherits="Sample" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head runat="server">
    <title></title>    
    <script src="lib/scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src='lib/scripts/jquery-ui-1.8.11.custom.min.js'></script>
    <script src="lib/scripts/jquery-plugins/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <script src="lib/scripts/json2.js" type="text/javascript"></script>    
</head>
<body>
    <form id="form1" runat="server">       
    <asp:TextBox CssClass="default_time_picker" ID="txtTimePicker" runat="server" />

    <input id="txtConfirmCode" name="txtConfirmCode" type="text" placeholder="CODE DE CONFIRMATION" onkeypress="return AjxPostSearch(event)" />

    <script type="text/javascript">
        $('.default_time_picker').datetimepicker({
            ampm: true, stepHour: 1, stepMinute: 10
        });

//        debug(fn);
        window.alert_ = window.alert;
        window.alert = function () {
            alert_.apply(window, arguments)
        };
//        debug(alert);

        function AjxPostSearch(e) {
                    if (e.keyCode == 13 || e == "13") { //"Enter Key of Keyboard is 13 "
                        //   AjaxCallForCount(); //Going to Search Confirmation Code

                        alert("Enter Call");
                    }
                }



                $(document).ready(function () {
                    alert("Ready Event Call ");



                });

    </script>
    </form>
</body>
</html>

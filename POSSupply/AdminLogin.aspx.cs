﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.Web;

public partial class AdminLogin : System.Web.UI.Page
{
    // On Page Load
    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!string.IsNullOrEmpty(Request.QueryString["login"]))
        {
            txtLoginID.Text = Request.QueryString["login"];
            txtPwd.Focus();
        }
        else
        {
            txtLoginID.Focus();
        }
        if (!IsPostBack)
        {
            if (Session["Msg"] != null && Session["Msg"].ToString().Length > 0)
            {
                lblMsg.Text = Session["Msg"].ToString();
                Session.Remove("Msg");
            }
        }        
    }
    //Initialize Culture
    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();        
    }

    public void Save()
    {
        try
        {
            HttpRuntime.Cache.Remove("HostSettings");
            InbizUser usr = ProcessUsers.GetUser(txtLoginID.Text, txtPwd.Text);
            if (usr == null)
            {
                Session.Add("msg", Resources.Resource.lblInvalidUser);
                Response.Redirect(Request.RawUrl, false);
                return;
            }
            if (usr.UserLang == "en")
            {
                Globals.SetCultureInfo(AppCulture.ENGLISH_CANADA);
            }
            else
            {
                Globals.SetCultureInfo(AppCulture.FRENCH_CANADA);
            }
            if (usr != null)
            {
                UserIPs objUserIPs = new UserIPs();
                objUserIPs.UserID = usr.UserID;
                var vIPList = objUserIPs.GetIPsList(null);
                string sIPAddress = "";
                if (vIPList.Count > 0)
                {
                    sIPAddress = vIPList[0].IPAddress;
                }

                if (sIPAddress == "" || sIPAddress == "0.0.0.0")
                {
                    Response.Redirect(string.Format("~/Admin/default.aspx?userModules={0}&regCode={1}", this.UserModule, this.RegCode), false);
                }
                else if (sIPAddress != "")
                {
                    string sValidIP = "";

                    string clientIp = (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"]).Split(',')[0].Trim();
                    sValidIP = clientIp;


                    Boolean isIPValid = false;
                    string[] arrIPAddresses = BusinessUtility.GetString(sIPAddress).Split(',');
                    string sInvalidIPAddr = "";
                    foreach (string sIPAddr in arrIPAddresses)
                    {
                        if (sIPAddr == sValidIP)
                        {
                            isIPValid = true;
                        }
                    }

                    if (isIPValid == true)
                    {
                        Response.Redirect(string.Format("~/Admin/default.aspx?userModules={0}&regCode={1}", this.UserModule, this.RegCode), false);
                    }
                    else
                    {
                        Session.Add("msg", Resources.Resource.lblInvalidUser);
                        Response.Redirect(Request.RawUrl, false);
                        return;
                    }
                }
            }
            else
            {
                Session.Add("msg", Resources.Resource.PlzEntValidLoginIDPassword);
                Response.Redirect(string.Format("~/Admin/default.aspx?userModules={0}&regCode={1}", this.UserModule, this.RegCode), false);
            }
        }
        catch (Exception ex)
        {
            //Session.Add("msg", "Critical error during login process!");
            Session.Add("msg", ex.Message);
            Response.Redirect(Request.RawUrl, false);
        }        
    }


    protected void btnLogin_Click(object sender, System.EventArgs e)
    {
        Save(); 
    }

    protected void btnReset_Click(object sender, System.EventArgs e)
    {
        txtLoginID.Text = "";
        txtPwd.Text = "";
        lblMsg.Text = "";
    }

    protected void btnForgetPwd_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("Password.aspx");
    }

    //Direct Launch Url Parameters
    private string Login {
        get {
            return Request.QueryString["login"];
        }
    }

    private string UserModule {
        get {
            return Request.QueryString["UserModules"];
        }
    }

    private string RegCode {
        get
        {
            return Request.QueryString["regCode"];
        }
    }   
}
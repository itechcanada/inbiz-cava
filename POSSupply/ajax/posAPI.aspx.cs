﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using MySql.Data.MySqlClient;

using iTECH.WebControls;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.InbizERP.BusinessLogic;

using Newtonsoft.Json;

public partial class ajax_posAPI : System.Web.UI.Page
{
    string _whsCode = string.Empty;
    string _strLang = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);
        int page = 0;
        int.TryParse(Request["page"], out page);
        int limit = 0;
        int.TryParse(Request["rows"], out limit);
        int sidx = 0;
        int.TryParse(Request["sidx"], out sidx);
        string sord = Request["sord"] != null ? Request["sord"] : string.Empty;
        string searchTerm = Request["searchTerm"] != null ? Request["searchTerm"] : string.Empty;
        if (sidx <= 0)
        {
            sidx = 1;
        }
        if (searchTerm == "")
        {
            searchTerm = "%";
        }
        else
        {
            searchTerm = "%" + searchTerm + "%";
        }
        PosSearchResponse res = new PosSearchResponse();
        try
        {
            int count = 0;
            double totalPages = 0.0D;
            _whsCode = CurrentUser.UserDefaultWarehouse;
            _strLang = Globals.CurrentAppLanguageCode;

            string sqlCount = "SELECT COUNT(distinct productID) FROM products ";
            sqlCount += " Left join prdQuantity on prdQuantity.PrdID=Products.ProductID ";
            sqlCount += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID ";
            sqlCount += " left join prdImages on prdImages.PrdID=Products.ProductID  ";
            sqlCount += " where descLang =@descLang and prdWhsCode=@prdWhsCode and prdIsActive=1 AND (Pdes.prdname LIKE @SearchKey OR prdUPCCode LIKE @SearchKey OR productID LIKE @SearchKey)";

            object val = dbHelp.GetValue(sqlCount, CommandType.Text, new MySqlParameter[] { 
                DbUtility.GetParameter("descLang", _strLang, MyDbType.String),
                DbUtility.GetParameter("prdWhsCode", _whsCode, MyDbType.String),
                DbUtility.GetParameter("SearchKey", searchTerm, MyDbType.String)
            });
            count = BusinessUtility.GetInt(val);
            if (count > 0)
            {
                totalPages = Math.Ceiling(BusinessUtility.GetDouble(count) / BusinessUtility.GetDouble(limit));
            }
            if (page > totalPages)
            {
                page = Convert.ToInt32(totalPages);
            }
            int start = limit * page - limit;
            string strSQL = string.Empty;
            if (totalPages != 0)
            {
                strSQL = "SELECT distinct productID,  prdUPCCode,  Pdes.prdname as prdname,prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty,prdEndUserSalesPrice,prdIsKit,prdSmallImagePath, prdLargeImagePath,prdTaxCode FROM products ";
                strSQL += " Left join prdQuantity on prdQuantity.PrdID=Products.ProductID ";
                strSQL += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID ";
                strSQL += " left join prdImages on prdImages.PrdID=Products.ProductID  ";
                strSQL += " where descLang =@descLang and prdWhsCode=@prdWhsCode and prdIsActive=1 AND (Pdes.prdname LIKE @SearchKey OR prdUPCCode LIKE @SearchKey OR productID LIKE @SearchKey)";
                strSQL += " ORDER BY " + sidx + " " + sord + " LIMIT " + start + "," + limit;
            }
            else
            {
                strSQL = "SELECT distinct productID,  prdUPCCode,  Pdes.prdname as prdname,prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty,prdEndUserSalesPrice,prdIsKit,prdSmallImagePath, prdLargeImagePath,prdTaxCode FROM products ";
                strSQL += " Left join prdQuantity on prdQuantity.PrdID=Products.ProductID ";
                strSQL += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID ";
                strSQL += " left join prdImages on prdImages.PrdID=Products.ProductID  ";
                strSQL += " where descLang =@descLang and prdWhsCode=@prdWhsCode and prdIsActive=1 AND (Pdes.prdname LIKE @SearchKey OR prdUPCCode LIKE @SearchKey OR productID LIKE @SearchKey)";
                strSQL += "  ORDER BY " + sidx + " " + sord;
            }
            res.page = page;
            res.total = Convert.ToInt32(totalPages);
            res.records = count;
            using (MySqlDataReader dr = dbHelp.GetDataReader(strSQL, CommandType.Text, new MySqlParameter[] { 
                DbUtility.GetParameter("descLang", _strLang, MyDbType.String),
                DbUtility.GetParameter("prdWhsCode", _whsCode, MyDbType.String),
                DbUtility.GetParameter("SearchKey", searchTerm, MyDbType.String)
            })) 
            {
                while (dr.Read())
                {
                    PosProductSearchColumn pCol = new PosProductSearchColumn();
                    pCol.ProductID = BusinessUtility.GetInt(dr["productID"]);
                    pCol.ProductName = BusinessUtility.GetString(dr["prdname"]);
                    pCol.UPCCode = BusinessUtility.GetString(dr["prdUPCCode"]);
                    pCol.ProductPrice = BusinessUtility.GetDouble(dr["prdEndUserSalesPrice"]);
                    res.rows.Add(pCol);  
                }               
            }
        }
        catch
        {

        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }


        Response.Clear();
        Response.Write(JsonConvert.SerializeObject(res));
        Response.Flush();
        Response.SuppressContent = true;
    }    
}
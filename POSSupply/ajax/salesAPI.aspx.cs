﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using iTECH.Library.DataAccess.MySql;//Added by mukesh 20130607
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class ajax_salesAPI : System.Web.UI.Page
{
    #region WebMethods
    [System.Web.Services.WebMethod]
    public static string DeleteCartItem(string sessionID, string productID)
    {
        if (!ValidateSession(sessionID))
        {
            return "0";
        }
        try
        {
            SalesCartHelper.DeleteCartItem(BusinessUtility.GetInt(productID));
            return "1";
        }
        catch
        {
            return "0";
        }
    }

    [System.Web.Services.WebMethod]
    public static string DeleteProcessCartItem(string sessionID, string processid)
    {
        if (!ValidateSession(sessionID))
        {
            return "0";
        }
        try
        {
            SalesCartHelper.DeleteProcessCartItem(BusinessUtility.GetInt(processid));
            return "1";
        }
        catch
        {
            return "0";
        }
    }

    [System.Web.Services.WebMethod]
    public static string[] GetCalculatedTotalHtml(string sessionID, string partnerid, string ordID)
    {
        if (!ValidateSession(sessionID))
        {
            return new List<string>().ToArray();
        }
        try
        {
            //DateTime dtStat1 = DateTime.Now;
            List<string> lstHtmlData = new List<string>();
            TotalSummary lTotal = SalesCartHelper.GetOrderTotal();
            //DateTime dtStat2 = DateTime.Now;
            //Set Lables for html to be generated
            //lTotal.SetLabel(TotalSummary.LabelKey.BalanceAmount, Resources.Resource.lblBalanceAmount);
            //lTotal.SetLabel(TotalSummary.LabelKey.GrandSubTotal, Resources.Resource.lblSubTotalOrder);
            //lTotal.SetLabel(TotalSummary.LabelKey.GrandTotal, Resources.Resource.lblOrderTotal);
            //lTotal.SetLabel(TotalSummary.LabelKey.ReceivedAmount, Resources.Resource.lblReceivedAmount);
            //lTotal.SetLabel(TotalSummary.LabelKey.SubTotalItems, Resources.Resource.lblSubTotalItems);
            //lTotal.SetLabel(TotalSummary.LabelKey.SubTotalServices, Resources.Resource.lblSubTotalServices);
            //lTotal.SetLabel(TotalSummary.LabelKey.TotalDiscount, Resources.Resource.lblDiscount);
            //lTotal.SetLabel(TotalSummary.LabelKey.TotalTax, Resources.Resource.lblTotalTax);

            //Added by mukesh 20130607
            string GP = "";
            string GPPercentage = "";
            if (SalesCartHelper.CurrentCart.Items.Count > 0)
            {
                double totalGP = 0.0;
                double internalCost = 0.0;
                double totalAmt = 0.0;
                double processAmt = 0.0;
                double finalAmt = 0.0;

                DbHelper dbHelp = new DbHelper();
                SlsAnalysis slsAnalysis = new SlsAnalysis();
                clsProducts chkPrdVendorExist = new clsProducts();
                Product objprd = new Product();
                foreach (var item in SalesCartHelper.CurrentCart.Items)
                {
                    if (chkPrdVendorExist.funGetVendorCount(item.ProductID.ToString()) > 0)
                    {
                        double GetUnitCostPriceAvg = slsAnalysis.TotalCostPriceAvg(dbHelp, item.ProductID);
                        totalGP += BusinessUtility.GetDouble(item.Quantity) * BusinessUtility.GetDouble((item.Price) - GetUnitCostPriceAvg);
                        totalAmt += BusinessUtility.GetDouble((item.Quantity * item.Price) - (item.Quantity * item.Price * item.Discount / 100));
                    }
                    else
                    {
                        objprd.PopulateObject(item.ProductID);
                        totalGP += BusinessUtility.GetDouble(item.Quantity) * BusinessUtility.GetDouble((item.Price) - objprd.PrdLandedPrice);
                        totalAmt += BusinessUtility.GetDouble((item.Quantity * item.Price)-(item.Quantity * item.Price * item.Discount / 100));
                    }
                }
                foreach (var item in SalesCartHelper.CurrentCart.ProcessItems)
                {
                    internalCost += BusinessUtility.GetDouble(item.ProcessInternalCost);
                    processAmt += BusinessUtility.GetDouble(item.ProcessFixedCost);
                }
                finalAmt = totalGP + (processAmt - internalCost);

                SysCompanyInfo ci = new SysCompanyInfo();
                ci.PopulateObject(CurrentUser.DefaultCompanyID);

                if (finalAmt < 0.0)
                {
                    //GP = string.Format("<font style='color:Red;font-weight:bold;font-family:Tahoma'>{0} {1:F}</font>", lTotal.CurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(totalGP + (processAmt - internalCost)),Globals.CurrentCultureName));//font-size:12px
                    GP = string.Format("<font style='color:Red;font-weight:bold;font-family:Tahoma'>{0} {1:F}</font>", ci.CompanyBasCur, CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(totalGP + (processAmt - internalCost)), Globals.CurrentCultureName)));//font-size:12px
                    GPPercentage = string.Format("<font style='color:Red;font-weight:bold;font-family:Tahoma'>{0:F} {1}</font>", (BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(((totalGP + (processAmt - internalCost)) / (totalAmt + processAmt)) * 100), Globals.CurrentCultureName)).Replace("$", ""), " %");
                }
                else
                {
                    //GP = string.Format("<font style='color:Green;font-weight:bold;font-family:Tahoma'>{0} {1:F}</font>", lTotal.CurrencyCode,BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble( (totalGP + (processAmt - internalCost))),Globals.CurrentCultureName));//font-size:11px
                    GP = string.Format("<font style='color:Green;font-weight:bold;font-family:Tahoma'>{0} {1:F}</font>", ci.CompanyBasCur, CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble((totalGP + (processAmt - internalCost))), Globals.CurrentCultureName)));//font-size:11px
                    GPPercentage = string.Format("<font style='color:Green;font-weight:bold;font-family:Tahoma'>{0:F} {1}</font>", (BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(((totalGP + (processAmt - internalCost)) / (totalAmt + processAmt)) * 100), Globals.CurrentCultureName)).Replace("$", ""), " %");
                }

            }
            //Added end
            
            lstHtmlData.Add(CommonHtml.GetItemTotalHtml(lTotal, BusinessUtility.GetInt(partnerid), BusinessUtility.GetInt(ordID)));
            //DateTime dtStat = DateTime.Now;
            lstHtmlData.Add(CommonHtml.GetProcessTotalHtml(lTotal));
            //DateTime dtStat3 = DateTime.Now;
            lstHtmlData.Add(CommonHtml.GetGrandTotalHtml(lTotal));
            //DateTime dtEnd = DateTime.Now;
            lstHtmlData.Add("");
            lstHtmlData.Add(GP); //Added by mukesh 20130607
            lstHtmlData.Add(GPPercentage); //Added by mukesh 20130607
            //DateTime dtEnd1 = DateTime.Now;
            return lstHtmlData.ToArray();
        }
        catch
        {
            return new List<string>().ToArray();
        }
    }


    [System.Web.Services.WebMethod]
    public static string[] GetCalculatedPOTotalHtml(string sessionID, string partnerid)
    {
        if (!ValidateSession(sessionID))
        {
            return new List<string>().ToArray();
        }
        try
        {
            List<string> lstHtmlData = new List<string>();

            //PurchaseOrderCartHelper.GetPOOrderTotal
            //TotalSummary lTotal = SalesCartHelper.GetOrderTotal();
            TotalSummary lTotal = PurchaseOrderCartHelper.GetPOOrderTotal();
            ////Set Lables for html to be generated
            ////lTotal.SetLabel(TotalSummary.LabelKey.BalanceAmount, Resources.Resource.lblBalanceAmount);
            ////lTotal.SetLabel(TotalSummary.LabelKey.GrandSubTotal, Resources.Resource.lblSubTotalOrder);
            ////lTotal.SetLabel(TotalSummary.LabelKey.GrandTotal, Resources.Resource.lblOrderTotal);
            ////lTotal.SetLabel(TotalSummary.LabelKey.ReceivedAmount, Resources.Resource.lblReceivedAmount);
            ////lTotal.SetLabel(TotalSummary.LabelKey.SubTotalItems, Resources.Resource.lblSubTotalItems);
            ////lTotal.SetLabel(TotalSummary.LabelKey.SubTotalServices, Resources.Resource.lblSubTotalServices);
            ////lTotal.SetLabel(TotalSummary.LabelKey.TotalDiscount, Resources.Resource.lblDiscount);
            ////lTotal.SetLabel(TotalSummary.LabelKey.TotalTax, Resources.Resource.lblTotalTax);

            ////Added by mukesh 20130607
            //string GP = "";
            //string GPPercentage = "";
            //if (SalesCartHelper.CurrentCart.Items.Count > 0)
            //{
            //    double totalGP = 0.0;
            //    double internalCost = 0.0;
            //    double totalAmt = 0.0;
            //    double processAmt = 0.0;
            //    double finalAmt = 0.0;
            //    DbHelper dbHelp = new DbHelper();
            //    SlsAnalysis slsAnalysis = new SlsAnalysis();
            //    foreach (var item in SalesCartHelper.CurrentCart.Items)
            //    {
            //        double GetUnitCostPriceAvg = slsAnalysis.TotalCostPriceAvg(dbHelp, item.ProductID);
            //        totalGP += BusinessUtility.GetDouble(item.Quantity) * BusinessUtility.GetDouble((item.Price) - GetUnitCostPriceAvg);
            //        totalAmt += BusinessUtility.GetDouble(item.ItemTotal);
            //    }
            //    foreach (var item in SalesCartHelper.CurrentCart.ProcessItems)
            //    {
            //        internalCost += BusinessUtility.GetDouble(item.ProcessInternalCost);
            //        processAmt += BusinessUtility.GetDouble(item.ProcessFixedCost);
            //    }
            //    finalAmt = totalGP + (processAmt - internalCost);
            //    if (finalAmt < 0.0)
            //    {
            //        GP = string.Format("<font style='color:Red;font-weight:bold;font-family:Tahoma'>{0} {1:F}</font>", lTotal.CurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(totalGP + (processAmt - internalCost)), Globals.CurrentCultureName));//font-size:12px
            //        GPPercentage = string.Format("<font style='color:Red;font-weight:bold;font-family:Tahoma'>{0:F} {1}</font>", (BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(((totalGP + (processAmt - internalCost)) / (totalAmt + processAmt)) * 100), Globals.CurrentCultureName)).Replace("$", ""), " %");
            //    }
            //    else
            //    {
            //        GP = string.Format("<font style='color:Green;font-weight:bold;font-family:Tahoma'>{0} {1:F}</font>", lTotal.CurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble((totalGP + (processAmt - internalCost))), Globals.CurrentCultureName));//font-size:11px
            //        GPPercentage = string.Format("<font style='color:Green;font-weight:bold;font-family:Tahoma'>{0:F} {1}</font>", (BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(((totalGP + (processAmt - internalCost)) / (totalAmt + processAmt)) * 100), Globals.CurrentCultureName)).Replace("$", ""), " %");
            //    }

            //}
            //Added end

            lstHtmlData.Add(CommonHtml.GetPoItemTotalHtml(lTotal, BusinessUtility.GetInt( partnerid)));
            //lstHtmlData.Add(CommonHtml.GetProcessTotalHtml(lTotal));
            //lstHtmlData.Add(CommonHtml.GetGrandTotalHtml(lTotal));
            //lstHtmlData.Add("");
            //lstHtmlData.Add(GP); //Added by mukesh 20130607
            //lstHtmlData.Add(GPPercentage); //Added by mukesh 20130607
            return lstHtmlData.ToArray();
        }
        catch
        {
            return new List<string>().ToArray();
        }
    }

    static bool ValidateSession(string reqSessionID)
    {
        return string.Equals(HttpContext.Current.Session.SessionID, reqSessionID);
    }

    #endregion
}
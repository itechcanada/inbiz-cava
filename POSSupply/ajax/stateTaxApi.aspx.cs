﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using MySql.Data.MySqlClient;

using iTECH.WebControls;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.InbizERP.BusinessLogic;

using Newtonsoft.Json;

public partial class ajax_stateTaxApi : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string country = Request.Form["country"];

        List<CountryStateTaxGroup> lResult = null;
        if (!string.IsNullOrEmpty(country))
        {
            lResult = new CountryStateTaxGroup().GetListCountry(null, country);
        }
        else
        {
            lResult = new List<CountryStateTaxGroup>();
        }
        Response.Clear();
        Response.Write(JsonConvert.SerializeObject(lResult));
        Response.Flush();
        Response.SuppressContent = true;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using MySql.Data.MySqlClient;

using iTECH.WebControls;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.InbizERP.BusinessLogic;

public partial class Admin_chartAPI : System.Web.UI.Page
{
    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static BarChartSettings GetSalesCompairDataByMonths(string sessionID, string yearRange, string whs, string salesBy)
    {
        //System.Threading.Thread.Sleep(5000);
        if (sessionID != HttpContext.Current.Session.SessionID)
        {
            return new BarChartSettings();
        }
        
        DbHelper dbHelp = new DbHelper(true);
       
        try
        {
            return DashboardDataAPI.GetSalesCompairDataByMonths(dbHelp, yearRange, whs, salesBy);
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static BarChartSettings GetTopCustomerData(string sessionID, string yearRange, string whs)
    {
        //System.Threading.Thread.Sleep(5000);
        if (sessionID != HttpContext.Current.Session.SessionID)
        {
            return new BarChartSettings();
        }
        DbHelper dbHelp = new DbHelper(true);        
        try
        {
            return DashboardDataAPI.GetTopCustomerData(dbHelp, yearRange, whs);
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static BarChartSettings GetTopProductsData(string sessionID, string yearRange, string by, string whs, string salesBy)
    {
        //System.Threading.Thread.Sleep(5000);
        if (sessionID != HttpContext.Current.Session.SessionID)
        {
            return new BarChartSettings();
        }
       
        DbHelper dbHelp = new DbHelper(true);
       
        try
        {
            return DashboardDataAPI.GetTopProductsData(dbHelp, yearRange, by, whs, salesBy);
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static PieChartSettings GetAccountRcvPieData(string sessionID, string whs)
    {
        //System.Threading.Thread.Sleep(5000);
        if (sessionID != HttpContext.Current.Session.SessionID)
        {
            return new PieChartSettings();
        }
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            return DashboardDataAPI.GetAccountRcvPieData(dbHelp, whs);
        }
        //catch {
        //    throw;
        //}
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }
}
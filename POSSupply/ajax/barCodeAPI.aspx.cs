﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using MySql.Data.MySqlClient;

using iTECH.WebControls;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.InbizERP.BusinessLogic;

using Newtonsoft.Json;

public partial class ajax_barCodeAPI : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string barCode = Globals.GetNewBarCode(Request.Form["pType"], Request.Form["pfx"], Request.Form["f"]);
        Response.Clear();
        Response.Write(barCode);
        Response.Flush();
        Response.SuppressContent = true;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Net;
using System.Xml;

using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

public partial class PostXml_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string fileName = Server.MapPath("sample.xml");
            string postData = File.ReadAllText(fileName);

            txtPoXml.Text = postData;
            txtUrlToPost.Text = "http://localhost:19276/InbizWebDvlp/PostXml/get.aspx";//"https://live2.itechcanada.com/PrabhDemo/PostXml/get.aspx";
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string xmlInv = new PostXml().GetResponse(txtUrlToPost.Text, txtPoXml.Text);
        txtInvXml.Text = xmlInv;
        pnlPo.Visible = false;
        pnlInvoice.Visible = true;
    }

    //public string GetResponse(string urlToPost, string dataToPost)
    //{
    //    //Logic below for evo post
    //    string url = urlToPost;
    //    string strResponseData = string.Empty;
    //    try
    //    {
    //        //string fileName = Server.MapPath("sample.xml");
    //        //string postData = File.ReadAllText(fileName); //"&lt;?xml version='1.0' encoding='UTF-8'?><request><header><userId>*GUEST</userId><password>guest</password></header><body><command>searchJobs</command><parms><parm>100</parm><parm>Open</parm><parm></parm><parm></parm><parm></parm><parm></parm><parm></parm><parm></parm></parms></body></request>"; //this.GetTextFromXMLFile(fileName);

    //        //Setup the http request.
    //        HttpWebRequest wrWebRequest = WebRequest.Create(url) as HttpWebRequest;
    //        wrWebRequest.Method = "POST";
    //        wrWebRequest.ContentLength = dataToPost.Length;
    //        wrWebRequest.ContentType = "text/xml";
    //        CookieContainer cookieContainer = new CookieContainer();
    //        wrWebRequest.CookieContainer = cookieContainer;

    //        using (StreamWriter swRequestWriter = new StreamWriter(wrWebRequest.GetRequestStream()))
    //        {
    //            swRequestWriter.Write(dataToPost);
    //            swRequestWriter.Close();

    //            // Get the response.
    //            HttpWebResponse hwrWebResponse = (HttpWebResponse)wrWebRequest.GetResponse();

    //            //Read the response
    //            using (StreamReader srResponseReader = new StreamReader(hwrWebResponse.GetResponseStream()))
    //            {
    //                strResponseData = srResponseReader.ReadToEnd();                                            
    //            }                
    //        }
    //        return strResponseData;
    //    }
    //    catch(Exception ex)
    //    {
    //        return ex.Message + Environment.NewLine + ex.StackTrace;           
    //    }
    //    finally
    //    {
    //        url = null;
    //    }
    //}

    //Working Function    


    protected void Button2_Click(object sender, EventArgs e)
    {
        try
        {
            //txtResponse.Text = this.GetResponse(txtInvoiceUrl.Text, txtInvoiceUrl.Text);
        }
        catch (Exception ex)
        {
            txtResponse.Text = ex.Message;
        }                   
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl);
    }
}

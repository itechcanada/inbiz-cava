﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Net;
using System.Xml;

using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

public partial class PostXml_get : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //To Do To validate
        this.SubmitXmlPoAndCreateSO();
    }

    private void SubmitXmlPoAndCreateSO()
    {        
        string strxml = string.Empty;
        Response.Clear();
        Response.ContentType = "text/xml";
        string xmlData = string.Empty;
        string payLoadID = string.Empty;
        string orderID = string.Empty;

        PostXml postXmlTrack = new PostXml();
        int postXmlTrackID = -1;
        
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            //Check posting allowed IP
            string[] strIps = System.Configuration.ConfigurationManager.AppSettings["PostXmlFeatureAllowedIP"].Split(',');
            string reqIP = Request.UserHostAddress;
            if (string.IsNullOrEmpty(reqIP) || !strIps.Contains(reqIP))
            {
                throw new Exception("Requested IP Address not allowed to post XML.");
            }

            //Read Requested Xml Data
            using (System.IO.StreamReader reader = new System.IO.StreamReader(Page.Request.InputStream))
            {
                xmlData = reader.ReadToEnd();
            }

            //Hard coded values from App Configuration
            int userID = BusinessUtility.GetInt(AppConfiguration.AppSettings[AppSettingKey.PostXmlDefaultUserID]);
            int companyID = BusinessUtility.GetInt(AppConfiguration.AppSettings[AppSettingKey.PostXmlDefaultCompanyID]);
            string whsCode = AppConfiguration.AppSettings[AppSettingKey.PostXmlWhsCode];
            
            //First step to save Raw xml posted data to database for future reference
            
            postXmlTrackID = postXmlTrack.SetPostedData(dbHelp, xmlData, PostType.PO, PostDirection.INT);            

            //to do to Parse Xml
            MyXmlReader xReader = new MyXmlReader(xmlData);

            payLoadID = xReader.GetNodeAttribute("FILE", 0, "payloadID");
            orderID = xReader.GetNodeAttribute("cXML/Request/OrderRequest/OrderRequestHeader", 0, "orderID");

            //Collect Order Information
            Orders ord = new Orders();
            Partners part = new Partners();

            Addresses billToAddr = new Addresses();
            Addresses shipToAddr = new Addresses();

            string pEmail = xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/ShipTo/Address/Email");
            string pName =  xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/ShipTo/Address/Name");
            string pPhone = string.Format("{0}-{1}-{2}", xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/ShipTo/Address/Phone/TelephoneNumber/CountryCode"),
                xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/ShipTo/Address/Phone/TelephoneNumber/AreaOrCityCode"),
                xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/ShipTo/Address/Phone/TelephoneNumber/Number"));
            string pCur =  xReader.GetNodeAttribute("cXML/Request/OrderRequest/OrderRequestHeader/Total/Money", 0, "currency");

            this.FillBillToAddress(xReader, billToAddr);
            this.FillShipToAddress(xReader, shipToAddr);

            this.ValidateShipToAddress(shipToAddr, billToAddr);

            part.PopulateCustomer(dbHelp, pEmail, pName, pPhone, pCur, userID, "en", billToAddr, shipToAddr);

            ord.InvDate = DateTime.Now;
            ord.InvRefNo = 0;
            ord.OrdComment = string.Empty;
            ord.OrdCompanyID = companyID; //Get Company ID
            ord.OrdCreatedBy = userID; //Get Creator User            
            ord.OrdCreatedFromIP = Request.ServerVariables["REMOTE_ADDR"];
            //var dateString = xReader.GetNodeAttribute("cXML/Request/OrderRequest/OrderRequestHeader", 0, "orderDate");
            ord.OrdCreatedOn = DateTime.Now; //BusinessUtility.GetDateTime(dateString, "yyyy-MM-ddTHH:mm:sszzz");
            ord.OrdCurrencyCode = pCur;
            ord.OrdCurrencyExRate = 1;
            ord.OrdCustID = part.PartnerID;//Search customer By long name & email address & phone no
            ord.OrdCustType = Globals.GetPartnerType(part.PartnerType);
            ord.OrdDiscount = 0.00D;
            ord.OrdDiscountType = "P";
            ord.OrderRejectReason = string.Empty;
            ord.OrderTypeCommission = (int)OrderCommission.Sales;
            ord.OrdLastUpdateBy = userID;
            ord.OrdLastUpdatedOn = DateTime.Now;
            ord.OrdNetTerms = part.PartnerInvoiceNetTerms;
            ord.OrdSalesRepID = userID;
            ord.OrdSaleWeb = false;
            ord.OrdShippingTerms = string.Empty;
            ord.OrdShpBlankPref = false;
            ord.OrdShpCode = string.Empty;
            ord.OrdShpCost = 0.00D;
            ord.OrdShpDate = DateTime.Now;
            ord.OrdShpTrackNo = string.Empty;
            ord.OrdShpWhsCode = whsCode;
            ord.OrdStatus = SOStatus.READY_FOR_SHIPPING;
            ord.OrdType = "SO";
            ord.OrdVerified = true;
            ord.OrdVerifiedBy = userID;
            ord.QutExpDate = DateTime.MinValue;
            ord.OrdCustPO = orderID;

            //first Validate for valid products
            OrderItems oItem = new OrderItems();
            XmlNodeList nList = xReader.GetNodeList("cXML/Request/OrderRequest/ItemOut");
            string extID = string.Empty;
            foreach (XmlNode nd in nList)
            {                
                MyXmlReader innerReader = new MyXmlReader(string.Format("<ItemOut>{0}</ItemOut>", nd.InnerXml));
                extID = innerReader.GetNodeText("ItemOut/ItemID/SupplierPartID");                

                oItem.PopulateOrderItem(dbHelp, ord.OrdID, part.PartnerID, extID, string.Empty, 0, whsCode, 0, 0, userID); //HIDE
            }

            //Save Order
            ord.Insert(dbHelp, userID); 

            //Set postxml id with Order for future reference
            ord.SetPostedXmlID(dbHelp, ord.OrdID, postXmlTrackID);

            //Retreive Ship to state tax
            CountryStateTaxGroup txG = new CountryStateTaxGroup();
            txG.PopulateObject(dbHelp, shipToAddr.AddressCountry, shipToAddr.AddressState);            
           
            string prdName = string.Empty;
            double unitPrice = 0.00D;
            double prdQty = 0.00D;
            foreach (XmlNode nd in nList)
            {
                if (nd.Attributes["quantity"] != null)
                {
                    prdQty = BusinessUtility.GetDouble(nd.Attributes["quantity"].Value);
                }
                
                MyXmlReader innerReader = new MyXmlReader(string.Format("<ItemOut>{0}</ItemOut>", nd.InnerXml));
                extID = innerReader.GetNodeText("ItemOut/ItemID/SupplierPartID");
                prdName = innerReader.GetNodeText("ItemOut/ItemDetail/Description");
                unitPrice = BusinessUtility.GetDouble(innerReader.GetNodeText("ItemOut/ItemDetail/UnitPrice/Money"));

                oItem.PopulateOrderItem(dbHelp, ord.OrdID, part.PartnerID, extID, prdName, unitPrice, whsCode, prdQty, txG.StateGroupTaxCode, userID); //HIDE
            }

            //Add shipping process
            //Add shipping process item to the order
            SysProcessGroup pg = new SysProcessGroup();
            pg.PopulateObject(dbHelp, BusinessUtility.GetInt(AppConfiguration.AppSettings[AppSettingKey.ShippingProcessID]));

            double processCost = BusinessUtility.GetDouble(xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/Shipping/Money"));

            //Add Process cost only if it is greater than 0
            if (processCost > 0)
            {
                SysWarehouses sWhs = new SysWarehouses();
                sWhs.PopulateObject(whsCode, dbHelp);

                OrderItemProcess process = new OrderItemProcess();
                process.OrdID = ord.OrdID;
                process.OrdItemProcCode = pg.ProcessCode;
                process.OrdItemProcFixedPrice = processCost;
                process.OrdItemProcHours = 0;
                process.OrdItemProcPricePerHour = 0;
                process.OrdItemProcPricePerUnit = 0;
                process.OrdItemProcUnits = 0;
                process.SysTaxCodeDescID = sWhs.WarehouseRegTaxCode; //Appy Warehouse tax code by defaut for process items

                process.Insert(dbHelp);
            }
           

            //Prepare Invoice
            //var invoiceToPost = this.GenerateInvoice(dbHelp, ord, part, payLoadID, userID, xReader);           
            //Set Payment Details
            PreAccountRcv arc = new PreAccountRcv();
            if (xReader.GetNode("cXML/Request/OrderRequest/OrderRequestHeader/Payment") != null)
            {
                TotalSummary ts = CalculationHelper.GetOrderTotal(dbHelp, ord.OrdID);

                arc.AmountDeposit = ts.GrandTotal;
                arc.CustomerID = part.PartnerID;
                arc.DateReceived = DateTime.Now;
                arc.OrderID = ord.OrdID;
                arc.PaymentMethod = (int)StatusAmountReceivedVia.CreditCard;
                arc.Notes = "Credit Card Details: " + Environment.NewLine;
                arc.Notes += string.Format("Number: {0}", xReader.GetNodeAttribute("cXML/Request/OrderRequest/OrderRequestHeader/Payment/PCard", 0, "number")) + Environment.NewLine;
                arc.Notes += string.Format("Expiration: {0}", xReader.GetNodeAttribute("cXML/Request/OrderRequest/OrderRequestHeader/Payment/PCard", 0, "expiration")) + Environment.NewLine;
                arc.Notes += string.Format("Name: {0}", xReader.GetNodeAttribute("cXML/Request/OrderRequest/OrderRequestHeader/Payment/PCard", 0, "name")) + Environment.NewLine;
                arc.Notes += string.Format("Street: {0}", xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/Payment/PCard/PostalAddress/Street")) + Environment.NewLine;
                arc.Notes += string.Format("City: {0}", xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/Payment/PCard/PostalAddress/City")) + Environment.NewLine;
                arc.Notes += string.Format("State: {0}", xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/Payment/PCard/PostalAddress/State")) + Environment.NewLine;
                arc.Notes += string.Format("PostalCode: {0}", xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/Payment/PCard/PostalAddress/PostalCode")) + Environment.NewLine;
                arc.Notes += string.Format("Country: {0}", xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/Payment/PCard/PostalAddress/Country"));
                arc.Insert(dbHelp);
            }
            

            string xmlResponse = @"<!DOCTYPE cXML SYSTEM ""http://xml.cXML.org/schemas/cXML/1.2.007/cXML.dtd"">";
            xmlResponse += string.Format(@"<cXML payloadID=""{0}"" timestamp=""{1}"">", payLoadID, DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz"));
            xmlResponse += string.Format(@"<Response><Status code=""200"" text=""Purchase Order {0} Received""/></Response>", orderID);
            xmlResponse += "</cXML>";

            postXmlTrack.SetResponse(dbHelp, xmlResponse, postXmlTrackID);

            //Record Response to post xml tracker           
            Response.Write(xmlResponse);
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message + "<br>" + ex.StackTrace);

            string xmlResponse = @"<!DOCTYPE cXML SYSTEM ""http://xml.cXML.org/schemas/cXML/1.2.007/cXML.dtd"">";
            xmlResponse += string.Format(@"<cXML payloadID=""{0}"" timestamp=""{1}"">", payLoadID, DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz"));
            xmlResponse += string.Format(@"<Response><Status code=""{0}"" text=""{1}""/></Response>", "500", ex.Message);
            xmlResponse += "</cXML>";

            try
            {
                postXmlTrack.SetResponse(dbHelp, xmlResponse, postXmlTrackID);
            }
            finally { }
            
            Response.Write(xmlResponse);
        }

        Response.End();
        Response.SuppressContent = true;
    }  

    //Private Methods
    private void FillShipToAddress(MyXmlReader xReader, Addresses shpAddress)
    {
        string[] addArr = xReader.GetNodeListArray("cXML/Request/OrderRequest/OrderRequestHeader/ShipTo/Address/PostalAddress/DeliverTo");

        shpAddress.AddressCity = xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/ShipTo/Address/PostalAddress/City");
        shpAddress.AddressCountry = xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/ShipTo/Address/PostalAddress/Country");
        if (addArr.Length == 1)
        {
            shpAddress.AddressLine1 = addArr[0];  
            //Retreive Street fot rest lines
            addArr = xReader.GetNodeListArray("cXML/Request/OrderRequest/OrderRequestHeader/ShipTo/Address/PostalAddress/Street");
            if (addArr.Length == 1)
            {
                shpAddress.AddressLine2 = addArr[0];                
            }
            else if (addArr.Length == 2)
            {
                shpAddress.AddressLine2 = addArr[0];
                shpAddress.AddressLine3 = addArr[1];
            }                 
        }
        else if (addArr.Length == 2)
        {
            shpAddress.AddressLine1 = addArr[0];
            shpAddress.AddressLine2 = addArr[1];
            //Retreive Street fot rest lines
            addArr = xReader.GetNodeListArray("cXML/Request/OrderRequest/OrderRequestHeader/ShipTo/Address/PostalAddress/Street");
            if (addArr.Length == 1)
            {
                shpAddress.AddressLine3 = addArr[0];
            }            
        }
        else if (addArr.Length == 3)
        {
            shpAddress.AddressLine1 = addArr[0];
            shpAddress.AddressLine2 = addArr[1];
            shpAddress.AddressLine3 = addArr[2];
        }
        else
        {
            addArr = xReader.GetNodeListArray("cXML/Request/OrderRequest/OrderRequestHeader/ShipTo/Address/PostalAddress/Street");
            if (addArr.Length == 1)
            {
                shpAddress.AddressLine1 = addArr[0];               
            }
            else if (addArr.Length == 2)
            {
                shpAddress.AddressLine1 = addArr[0];
                shpAddress.AddressLine2 = addArr[1];
            }
            else if (addArr.Length == 3)
            {
                shpAddress.AddressLine1 = addArr[0];
                shpAddress.AddressLine2 = addArr[1];
                shpAddress.AddressLine3 = addArr[2];
            }
        }

        shpAddress.AddressPostalCode = xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/ShipTo/Address/PostalAddress/PostalCode");
        shpAddress.AddressRef = Globals.GetPartnerType((int)PartnerTypeIDs.Distributer);
        shpAddress.AddressState = xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/ShipTo/Address/PostalAddress/State");
        shpAddress.AddressType = AddressType.SHIP_TO_ADDRESS;
    }

    private void FillBillToAddress(MyXmlReader xReader, Addresses billAddress)
    {
        string[] addArr = xReader.GetNodeListArray("cXML/Request/OrderRequest/OrderRequestHeader/BillTo/Address/PostalAddress/DeliverTo");

        billAddress.AddressCity = xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/BillTo/Address/PostalAddress/City");
        billAddress.AddressCountry = xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/BillTo/Address/PostalAddress/Country");
        if (addArr.Length == 1)
        {
            billAddress.AddressLine1 = addArr[0];
            //Retreive Street fot rest lines
            addArr = xReader.GetNodeListArray("cXML/Request/OrderRequest/OrderRequestHeader/BillTo/Address/PostalAddress/Street");
            if (addArr.Length == 1)
            {
                billAddress.AddressLine2 = addArr[0];
            }
            else if (addArr.Length == 2)
            {
                billAddress.AddressLine2 = addArr[0];
                billAddress.AddressLine3 = addArr[1];
            }
        }
        else if (addArr.Length == 2)
        {
            billAddress.AddressLine1 = addArr[0];
            billAddress.AddressLine2 = addArr[1];
            //Retreive Street fot rest lines
            addArr = xReader.GetNodeListArray("cXML/Request/OrderRequest/OrderRequestHeader/BillTo/Address/PostalAddress/Street");
            if (addArr.Length == 1)
            {
                billAddress.AddressLine3 = addArr[0];
            }
        }
        else if (addArr.Length == 3)
        {
            billAddress.AddressLine1 = addArr[0];
            billAddress.AddressLine2 = addArr[1];
            billAddress.AddressLine3 = addArr[2];
        }
        else
        {
            addArr = xReader.GetNodeListArray("cXML/Request/OrderRequest/OrderRequestHeader/BillTo/Address/PostalAddress/Street");
            if (addArr.Length == 1)
            {
                billAddress.AddressLine1 = addArr[0];
            }
            else if (addArr.Length == 2)
            {
                billAddress.AddressLine1 = addArr[0];
                billAddress.AddressLine2 = addArr[1];
            }
            else if (addArr.Length == 3)
            {
                billAddress.AddressLine1 = addArr[0];
                billAddress.AddressLine2 = addArr[1];
                billAddress.AddressLine3 = addArr[2];
            }
        }

        billAddress.AddressPostalCode = xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/BillTo/Address/PostalAddress/PostalCode");
        billAddress.AddressRef = Globals.GetPartnerType((int)PartnerTypeIDs.Distributer);
        billAddress.AddressState = xReader.GetNodeText("cXML/Request/OrderRequest/OrderRequestHeader/BillTo/Address/PostalAddress/State");
        billAddress.AddressType = AddressType.BILL_TO_ADDRESS;
    }

    private void ValidateShipToAddress(Addresses shipToAddr, Addresses billToAddr)
    {
        if (string.IsNullOrEmpty(shipToAddr.AddressCity))
        {
            shipToAddr.AddressCity = billToAddr.AddressCity;
        }
        if (string.IsNullOrEmpty(shipToAddr.AddressCountry))
        {
            shipToAddr.AddressCountry = billToAddr.AddressCountry;
        }        
        if (string.IsNullOrEmpty(shipToAddr.AddressLine1) && string.IsNullOrEmpty(shipToAddr.AddressLine2) && string.IsNullOrEmpty(shipToAddr.AddressLine3))
        {
            shipToAddr.AddressLine1 = billToAddr.AddressLine1;
            shipToAddr.AddressLine2 = billToAddr.AddressLine2;
            shipToAddr.AddressLine3 = billToAddr.AddressLine3;
        }
        if (string.IsNullOrEmpty(shipToAddr.AddressPostalCode))
        {
            shipToAddr.AddressPostalCode = billToAddr.AddressPostalCode;
        }
        if (string.IsNullOrEmpty(shipToAddr.AddressState))
        {
            shipToAddr.AddressState = billToAddr.AddressState;
        }       
    }
    
    //Working function
    //private void TestWorking()
    //{
    //    string strxml = @"<!DOCTYPE cXML SYSTEM ""http://xml.cXML.org/schemas/cXML/1.2.007/cXML.dtd"">";
    //    strxml += @"<cXML payloadID=""20081030121845.cXMLReceive.976747008@dmassoc.net"" timestamp=""2008-10-30T12:18:45+05:00"">";
    //    strxml += @"<Response><Status code=""200"" text=""Purchase Order FAC00P-2516 Received""/></Response>";
    //    strxml += "</cXML>";
    //    Response.Clear();
    //    Response.ContentType = "text/xml";

    //    using (System.IO.StreamReader reader = new System.IO.StreamReader(Page.Request.InputStream))
    //    {
    //        String xmlData = reader.ReadToEnd();            
    //        //Response.Write(xmlData);
    //    }

    //    Response.Write(strxml);
    //    Response.End();
    //    Response.SuppressContent = true;
    //}
}
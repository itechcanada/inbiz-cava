﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="PostXml_Default" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <asp:Panel ID="pnlPo" runat="server">
                <h4>
                    Xml PO Data To Post</h4>
                <asp:TextBox ID="txtPoXml" runat="server" Height="300px" Width="600px" TextMode="MultiLine" /><br />
                <br />
                <h4>Post Po Data Url</h4>
                <asp:TextBox ID="txtUrlToPost" runat="server" Width="600px"></asp:TextBox><br />  
                <asp:Button ID="Button1" runat="server" Text="Post Xml" OnClick="Button1_Click" />
                <asp:Button ID="Button3" runat="server" Text="Reset" OnClick="Button3_Click" />
            </asp:Panel>
            <asp:Panel ID="pnlInvoice" runat="server" Visible="false" >
                <br />
                <h4>
                    Xml Response</h4>
                <asp:TextBox ID="txtInvXml" runat="server" Height="300px" Width="600px" TextMode="MultiLine" /><br />                 
                <asp:Button ID="Button4" runat="server" Text="Reset" OnClick="Button3_Click" />
            </asp:Panel>
            <asp:Panel ID="pnlRes" runat="server" Visible="false">
                <asp:TextBox ID="txtResponse" runat="server" Height="300px" Width="600px" TextMode="MultiLine" />
                <asp:Button ID="Button5" runat="server" Text="Reset" OnClick="Button3_Click" />
            </asp:Panel>                              
        </div>
    </div>
    </form>
</body>
</html>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdUpComingEvents.aspx.cs" Inherits="Common_mdUpComingEvents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div style="padding: 5px; height: 280px; overflow: auto;">
        <div id="grid_wrapper">
            <trirand:JQGrid runat="server" ID="grdEvetns" Height="200px" Width="100%" AutoWidth="true"
                PagerSettings-PageSize="50" OnDataRequesting="grdEvetns_DataRequesting">
                <Columns>
                    <trirand:JQGridColumn DataField="ReservationItemID" PrimaryKey="true" Visible="false" />
                    <trirand:JQGridColumn DataField="RoomType" HeaderText="RoomType" TextAlign="Center" />
                    <trirand:JQGridColumn DataField="CheckInDate" HeaderText="Check In Date" DataFormatString="{0:MM/dd/yyyy}" Editable="false"
                        Sortable="false" TextAlign="Center" />
                    <trirand:JQGridColumn DataField="CheckOutDate" HeaderText="Check Out Date" Editable="false"
                        Sortable="false" TextAlign="Center" DataFormatString="{0:MM/dd/yyyy}" />
                    <trirand:JQGridColumn DataField="TotalNights" HeaderText="Total Days" Editable="false"
                        Sortable="false" TextAlign="Center" />
                </Columns>
                <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                    ShowDeleteButton="false" ShowSearchButton="false" />
                <SortSettings InitialSortColumn="" />
                <AppearanceSettings AlternateRowBackground="True" />
            </trirand:JQGrid>
        </div>
        <asp:SqlDataSource ID="sdsEvents" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdEvetns.ClientID %>";        

        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -2 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }


        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);
        

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function gridLoadComplete(data) {
            $(window).trigger("resize");            
        }               
    </script>
</asp:Content>


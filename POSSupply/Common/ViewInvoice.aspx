<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="ViewInvoice.aspx.vb" Inherits="Common_ViewInvoice" %>

<%@ Import Namespace="Resources.Resource" %>


<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
//        $('#divModuleAlert').corner();
//        $('#divAlertContent').corner();
        $('#divMainContainerTitle').corner();
        $('#divSectionTitle').corner();
        $('#<%=SearchPanel.ClientID%>').corner();
    </script>
    <%--<div id="divModuleAlert" class="divSectionTitle">
        <h2>
            <%=Resources.Resource.lblSearchOptions%>
        </h2>
    </div>
    <div id="divAlertContent" class="divSectionContent">
    </div>--%>
    <div id="divSectionTitle" class="divSectionTitle">
        <h2>
             <%=Resources.Resource.lblSearchOptions%>
        </h2>
    </div>
    <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel" DefaultButton="imgSearch">
       <%-- <asp:Label CssClass="lblBold" runat="server" ID="lblSearch" Text="<%$ Resources:Resource, lblINSearchByInvNoCustNameCustPOOrdNoInvStatus %>">
        </asp:Label>--%>

        <table id="tblSearch" cellpadding="1" cellspacing="1" border="0" runat="server">
            <tr>
                <td align="left">
                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"></asp:Label>
                    <br class="brMargin5"/>
                    <asp:DropDownList ID="dlSearch" runat="server" Width="175px" 
                        ValidationGroup="PrdSrch">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lblStatus %>"></asp:Label>
                    <br class="brMargin5"/>
                    <asp:DropDownList ID="dlPOStatus" runat="server" Width="175px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lblReferenceType %>"></asp:Label>
                    <br class="brMargin5"/>
                    <asp:DropDownList ID="dlRefType" runat="server" Width="175px" 
                        ValidationGroup="PrdSrch">
                        <asp:ListItem Value="" Text="<%$ Resources:Resource, liINSelectRefType %>" />
                        <%--<asp:ListItem Value="CN" Text="<%$ Resources:Resource, liINCreditNote %>"/>--%>
                        <asp:ListItem Value="IV" Text="<%$ Resources:Resource, liINInvoice %>" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword%>"></asp:Label>
                    <br class="brMargin5"/>
                    <asp:TextBox runat="server" Width="170px" ID="txtSearch" ValidationGroup="PrdSrch"
                        AutoPostBack="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:ImageButton ID="imgSearch" runat="server" AlternateText="Search" ImageUrl="../images/search-btn.gif"
                        ValidationGroup="PrdSrch" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

<asp:Content ID="cntViewInvoice" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                     <asp:UpdateProgress runat="server" ID="Updateprogress" AssociatedUpdatePanelID="udpnlViewInvoice"
                            DisplayAfter="10">
                            <ProgressTemplate>
                                <img src="../Images/wait22trans.gif" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
        <asp:HiddenField runat="server" ID="hdnLeft" />
    </div>

    <div class="divMainContent">
        <asp:UpdatePanel runat="server" ID="udpnlViewInvoice">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlMain">
                    <table width="100%" class="table">                       
                        <tr>
                            <td colspan="2">
                                <%--Popup Invoice Detail Start--%>
                                <asp:Panel runat="server" ID="pnlInvDetail">
                                    <div id="divViewInvoice" align="left" runat="server" style="display: none; border: 3px;
                                        border-color: black; margin: 0; height: 0;">
                                        <table cellpadding="2" class="popupTable" cellspacing="0" width="750px">
                                            <tr class="popupTR">
                                                <td align="center" colspan="2">
                                                    <table>
                                                        <tr>
                                                            <td style="width: 97%" align="center">
                                                                <asp:Label CssClass="lblHeading" runat="server" ID="lblNote1" Text="<%$ Resources:Resource, lblViewInvoiceDetails %>"></asp:Label>
                                                            </td>
                                                            <td align="right" style="padding-right: 4px; width: 3%">
                                                                <a name="Close" href="javascript:funCloseLoc();">
                                                                    <img border="0" src="../Images/closePrd.png" alt="Close" /></a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="10" colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblSOPOComName" Text="<%$ Resources:Resource, lblSOPOComName %>" CssClass="lblBold"
                                                        runat="server" />
                                                </td>
                                                <td style="width: 80%">
                                                    <asp:Label ID="lblInvComNameTitle" CssClass="lblBold" runat="server" />
                                                </td>
                                            </tr>
                                            <tr height="5">
                                                <td colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="lblSOPOwhs" Text="<%$ Resources:Resource, lblSOPOwhs %>" CssClass="lblBold"
                                                        runat="server" />
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblInvWhsName" CssClass="lblBold" runat="server" />
                                                </td>
                                            </tr>
                                            <tr height="5">
                                                <td colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="lblSODate1" Text="<%$ Resources:Resource, lblINInvoiceDate %>" CssClass="lblBold"
                                                        runat="server" />
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblSODate" CssClass="lblBold" runat="server" />
                                                </td>
                                            </tr>
                                            <tr height="5">
                                                <td colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="lblInvNumber" Text="<%$ Resources:Resource, lblINInvoiceNo %>" CssClass="lblBold"
                                                        runat="server" />
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblInvID" CssClass="lblBold" runat="server" />
                                                </td>
                                            </tr>
                                            <tr height="5">
                                                <td colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="lblSONumber" Text="<%$ Resources:Resource, lblSOOrderNo %>" CssClass="lblBold"
                                                        runat="server" />
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblSOID" CssClass="lblBold" runat="server" />
                                                </td>
                                            </tr>
                                            <tr height="5">
                                                <td colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="lblCustomerName1" Text="<%$ Resources:Resource, lblSOCustomerName %>"
                                                        CssClass="lblBold" runat="server" />
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblCustomerName" CssClass="lblBold" runat="server" />
                                                </td>
                                            </tr>
                                            <tr height="5">
                                                <td colspan="2">
                                                </td>
                                            </tr>
                                            <tr id="trAddress" runat="server" visible="false">
                                                <td align="left" valign="top">
                                                    <asp:Label ID="Label1" Text="<%$ Resources:Resource, lblSOReceivingAddress %>" CssClass="lblBold"
                                                        runat="server" />
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblAddress" CssClass="lblBold" runat="server" />
                                                </td>
                                            </tr>
                                            <tr height="5">
                                                <td colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="Label3" Text="<%$ Resources:Resource, POFax %>" CssClass="lblBold"
                                                        runat="server" />
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblFax" CssClass="lblBold" runat="server" />
                                                </td>
                                            </tr>
                                            <tr height="15">
                                                <td colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" valign="top">
                                                    <asp:GridView ID="grdOrdItems" runat="server" AllowSorting="false" DataSourceID="sqldsOrdItems"
                                                        AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0"
                                                        GridLines="none" AutoGenerateColumns="False" UseAccessibleHeader="False" Style="border-collapse: separate;"
                                                        CssClass="view_grid650" DataKeyNames="invItemID" Width="750px">
                                                        <Columns>
                                                            <asp:BoundField DataField="ordProductID" HeaderText="<%$ Resources:Resource, POProductID %>"
                                                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff" Visible="false">
                                                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="prdIntID" HeaderText="<%$ Resources:Resource, POInternalID %>"
                                                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="prdExtID" HeaderText="<%$ Resources:Resource, POExternalID %>"
                                                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="prdUPCCode" HeaderText="<%$ Resources:Resource, grdPOUPCCode %>"
                                                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="prdName" HeaderText="<%$ Resources:Resource, POProduct %>"
                                                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Width="200px" Wrap="true" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdPOShippingAddress %>" HeaderStyle-ForeColor="#ffffff"
                                                                HeaderStyle-HorizontalAlign="left">
                                                                <ItemTemplate>
                                                                    <div>
                                                                        <%#funAddressListing(Eval("invShpWhsCode"))%>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" Width="120px" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="sysTaxCodeDescText" NullDisplayText="--" HeaderStyle-HorizontalAlign="Center"
                                                                HeaderText="<%$ Resources:Resource, POTaxGrp %>" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="invProductDiscount" NullDisplayText="0" HeaderStyle-HorizontalAlign="Right"
                                                                HeaderText="<%$ Resources:Resource, grdSaleDiscount %>" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="invProductQty" HeaderText="<%$ Resources:Resource, POQuantity %>"
                                                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="invProductUnitPrice" HeaderText="<%$ Resources:Resource, POUnitPrice %>"
                                                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="invCurrencyCode" HeaderText="" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Wrap="true" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="amount" HeaderText="<%$ Resources:Resource, POAmount %>"
                                                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, POConfirm %>" HeaderStyle-HorizontalAlign="Center"
                                                                HeaderStyle-ForeColor="#ffffff" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="50px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" Visible="false"
                                                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                                                        CommandArgument='<%# Eval("invItemID") %>' CommandName="Delete" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <div style="display: none;">
                                                                        <asp:TextBox runat="server" Width="0px" ID="txtDisType" Text='<%# Eval("invProductDiscountType") %>'>
                                                                        </asp:TextBox>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" Width="0" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <FooterStyle CssClass="grid_footer" />
                                                        <RowStyle CssClass="grid_rowstyle" />
                                                        <PagerStyle CssClass="grid_footer" />
                                                        <HeaderStyle CssClass="grid_header" Height="26px" />
                                                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                                        <PagerSettings PageButtonCount="20" />
                                                    </asp:GridView>
                                                    <asp:SqlDataSource ID="sqldsOrdItems" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                                                </td>
                                            </tr>
                                            <tr runat="server" id="trCommission">
                                                <td colspan="7" align="right" style="padding-right: 10px;">
                                                    <asp:HiddenField runat="server" ID="hdnSaleAgentID" />
                                                    <asp:HiddenField runat="server" ID="hdnOrderID" />
                                                    <br />
                                                    <asp:Label ID="lblAgent" runat="server" Text="<%$ Resources:Resource, lblCommissionAgentName %>"
                                                        CssClass="lblBold"></asp:Label>&nbsp;&nbsp;
                                                    <asp:Label ID="lblCommAgentName" runat="server" Text="" CssClass="lblBold"></asp:Label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:Label ID="lblCom" runat="server" Text="<%$ Resources:Resource, lblSalesCommission %>"
                                                        CssClass="lblBold"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:Label ID="lblCommission"
                                                            runat="server" Text="" CssClass="lblBold"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" valign="top" align="left">
                                                    <br />
                                                    <asp:Label ID="lblProcessList" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOProcessItemList %>"
                                                        runat="server" Visible="false" /><br />
                                                    <asp:GridView ID="grdAddProcLst" runat="server" AllowSorting="False" AllowPaging="True"
                                                        PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0" GridLines="both" AutoGenerateColumns="False"
                                                        DataSourceID="sqldsAddProcLst" Style="border-collapse: separate;" CssClass="view_grid650"
                                                        UseAccessibleHeader="False" DataKeyNames="invItemProcID" Width="750px">
                                                        <Columns>
                                                            <asp:BoundField DataField="invItemProcID" HeaderStyle-ForeColor="#ffffff" HeaderText='ID:" '
                                                                Visible="false" ReadOnly="True" SortExpression="invItemProcID">
                                                                <ItemStyle Width="70px" Wrap="true" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="invItemProcCode" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdProcessCode %>"
                                                                Visible="false" ReadOnly="True" SortExpression="invItemProcCode">
                                                                <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="ProcessDescription" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdProcessDescription %>"
                                                                ReadOnly="True" SortExpression="ProcessDescription">
                                                                <ItemStyle Width="250px" Wrap="true" HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="invItemProcFixedPrice" HeaderText="<%$ Resources:Resource, grdProcessFixedCost %>"
                                                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="invItemProcPricePerHour" HeaderText="<%$ Resources:Resource, grdProcessCostPerHour %>"
                                                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="invItemProcPricePerUnit" HeaderText="<%$ Resources:Resource, grdProcessCostPerUnit %>"
                                                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="invItemProcHours" HeaderText="<%$ Resources:Resource, grdSOTotalHour %>"
                                                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="invItemProcUnits" HeaderText="<%$ Resources:Resource, grdSOTotalUnit %>"
                                                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="ProcessCost" HeaderText="<%$ Resources:Resource, grdSOProcessCost %>"
                                                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdEdit %>" Visible="false"
                                                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("invItemProcID") %>'
                                                                        CommandName="Edit" ImageUrl="~/images/edit_icon.png" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" Visible="false"
                                                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                                                        CausesValidation="false" CommandArgument='<%# Eval("invItemProcID") %>' CommandName="Delete" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <FooterStyle CssClass="grid_footer" />
                                                        <RowStyle CssClass="grid_rowstyle" />
                                                        <PagerStyle CssClass="grid_footer" />
                                                        <HeaderStyle CssClass="grid_header" Height="26px" />
                                                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                                        <PagerSettings PageButtonCount="20" />
                                                    </asp:GridView>
                                                    <asp:SqlDataSource ID="sqldsAddProcLst" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                                                </td>
                                            </tr>
                                            <tr height="15">
                                                <td colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <div id="divTerms" runat="server" align="center">
                                                        <table width="100%" style="border: 0;" border="0" align="center">
                                                            <tr>
                                                                <td colspan="7" align="right">
                                                                    <table align="right" cellpadding="3">
                                                                        <tr>
                                                                            <td align="left">
                                                                                <asp:Label ID="lblSubTotal1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOSubTotal %>">
                                                                                </asp:Label>
                                                                            </td>
                                                                            <td align="Right">
                                                                                <asp:Label ID="lblSubTotal" runat="server" CssClass="lblBold"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <%=TaxString %>
                                                                        <%-- <tr>
                                                 <td align="left">
                                                     <asp:Label ID="lblGST1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOGST %>">
                                                     </asp:Label>
                                                 </td>
                                                 <td align="Right">
                                                     <asp:Label ID="lblGST" runat="server" CssClass="lblBold"></asp:Label>
                                                 </td>
                                             </tr>
                                             <tr>
                                                 <td align="left">
                                                     <asp:Label ID="lblPST1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOPST %>">
                                                     </asp:Label>
                                                 </td>
                                                 <td align="Right">
                                                     <asp:Label ID="lblPST" runat="server" CssClass="lblBold"></asp:Label>
                                                 </td>
                                             </tr>
                                             <tr>
                                                 <td align="left">
                                                     <asp:Label ID="lblQST1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOQST %>">
                                                     </asp:Label>
                                                 </td>
                                                 <td align="Right">
                                                     <asp:Label ID="lblQST" runat="server" CssClass="lblBold"></asp:Label>
                                                 </td>
                                             </tr>--%>
                                                                        <tr>
                                                                            <td align="left">
                                                                                <asp:Label ID="lblAmount1" Text="<%$ Resources:Resource, lblSOTotal %>" CssClass="lblBold"
                                                                                    runat="server" />
                                                                            </td>
                                                                            <td align="Right">
                                                                                <asp:Label ID="lblAmount" CssClass="lblBold" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr height="25">
                                                                <td class="tdAlign" width="23%">
                                                                    <asp:Label ID="lblShpToAddress" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShpToAddress %>"
                                                                        runat="server" />
                                                                </td>
                                                                <td width="1%">
                                                                </td>
                                                                <td align="left" width="28%">
                                                                    <asp:TextBox ID="txtShpToAddress" runat="server" TextMode="MultiLine" Rows="5" ReadOnly="true">
                                                                    </asp:TextBox>
                                                                </td>
                                                                <td width="1%">
                                                                </td>
                                                                <td class="tdAlign" width="24%">
                                                                    <asp:Label ID="lblBillToAddress" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOBillToAddress %>"
                                                                        runat="server" />
                                                                </td>
                                                                <td width="1%">
                                                                </td>
                                                                <td align="left" width="22%">
                                                                    <asp:TextBox ID="txtBillToAddress" runat="server" TextMode="MultiLine" Rows="5" ReadOnly="true">
                                                                    </asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr height="25">
                                                                <td class="tdAlign">
                                                                    <asp:Label ID="lblTerms" CssClass="lblBold" Text="<%$ Resources:Resource, POShippingTerms%>"
                                                                        runat="server" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtTerms" runat="server" TextMode="MultiLine" Rows="3" ReadOnly="true">
                                                                    </asp:TextBox>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td class="tdAlign">
                                                                    <asp:Label ID="lblNetTerms" CssClass="lblBold" Text="<%$ Resources:Resource, lblSONetTerms%>"
                                                                        runat="server" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtNetTerms" runat="server" TextMode="SingleLine" MaxLength="4"
                                                                        ReadOnly="true">
                                                                    </asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr height="25" id="trShpPref" runat="server" visible="false">
                                                                <td class="tdAlign">
                                                                    <asp:Label ID="lblResellerShipBlankPref" CssClass="lblBold" Text="<%$ Resources:Resource, lblResellerShipBlankPref%>"
                                                                        runat="server" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:RadioButtonList ID="rblstShipBlankPref" runat="server" RepeatDirection="Horizontal"
                                                                        Enabled="false">
                                                                        <asp:ListItem Text="<%$ Resources:Resource, lblYes%>" Value="1">
                                                                        </asp:ListItem>
                                                                        <asp:ListItem Text="<%$ Resources:Resource, lblNo%>" Value="0" Selected="True">
                                                                        </asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td class="tdAlign">
                                                                    <asp:Label ID="lblISWeb" Text="<%$ Resources:Resource, lblSOSalesISWeb%>" CssClass="lblBold"
                                                                        runat="server" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:RadioButtonList ID="rblstISWeb" runat="server" RepeatDirection="Horizontal"
                                                                        Enabled="false">
                                                                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1">
                                                                        </asp:ListItem>
                                                                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0" Selected="True">
                                                                        </asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>
                                                            <tr height="25">
                                                                <td class="tdAlign">
                                                                    <asp:Label ID="lblShpWarehouse" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingWarehouse %>"
                                                                        runat="server" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:DropDownList ID="dlShpWarehouse" runat="server" Width="155px" Enabled="false">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td class="tdAlign">
                                                                    <asp:Label ID="lblShpCost" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingCost %>"
                                                                        runat="server" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtShpCost" runat="server" Width="150px" MaxLength="6" ReadOnly="true">
                                                                    </asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr height="25" id="trShpCost" runat="server" visible="false">
                                                                <td class="tdAlign">
                                                                    <asp:Label ID="lblShpTrackNo" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingTrackNo %>"
                                                                        runat="server" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtShpTrackNo" runat="server" Width="150px" MaxLength="25" ReadOnly="true">
                                                                    </asp:TextBox>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td class="tdAlign">
                                                                    <asp:Label ID="lblShpCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingCode %>"
                                                                        runat="server" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtShpCode" runat="server" Width="150px" MaxLength="15" ReadOnly="true">
                                                                    </asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr height="25" id="trShpDate" runat="server" visible="false">
                                                                <td class="tdAlign">
                                                                    <asp:Label ID="lblShpDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingDate %>">
                                                                    </asp:Label>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td align="left" colspan="5">
                                                                    <asp:TextBox ID="txtShippingDate" Enabled="true" runat="server" Width="90px" MaxLength="15"
                                                                        ReadOnly="true">
                                                                    </asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr height="25">
                                                                <td class="tdAlign">
                                                                    <asp:Label ID="lblNotes" CssClass="lblBold" Text="<%$ Resources:Resource, PONotes%>"
                                                                        runat="server" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td align="left" colspan="5">
                                                                    <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Rows="3" Width="550px"
                                                                        ReadOnly="true">
                                                                    </asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr height="25">
                                                                <td class="tdAlign">
                                                                    <asp:Label ID="lblCustPO" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOCustomerPO%>"
                                                                        runat="server" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtCustPO" runat="server" Width="130px" MaxLength="35" ReadOnly="true">
                                                                    </asp:TextBox>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td class="tdAlign">
                                                                    <asp:Label ID="lblPOStatus" CssClass="lblBold" Text="<%$ Resources:Resource, lblStatus%>"
                                                                        runat="server" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:DropDownList ID="dlStatus" runat="server" Width="135px" Enabled="false">
                                                                        <asp:ListItem Value="" Text="<%$ Resources:Resource, liINSelStatus %>" />
                                                                        <asp:ListItem Value="P" Text="<%$ Resources:Resource, liINPaymentReceived %>" />
                                                                        <asp:ListItem Value="S" Text="<%$ Resources:Resource, liINSubmitted %>" />
                                                                        <asp:ListItem Value="R" Text="<%$ Resources:Resource, liINReSubmitted %>" />
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr height="30" id="trCurrency" runat="server" visible="false">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblPOCurrencyCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOCurrencyCode%>"
                                                        runat="server" />
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:DropDownList ID="dlCurrencyCode" runat="server" Width="135px" Enabled="false">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr height="30" id="trExchageRate" runat="server" visible="false">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblPOExRate" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOExRate%>"
                                                        runat="server" />
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:TextBox ID="txtExRate" runat="server" Width="130px" MaxLength="5" ReadOnly="true">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                                <%--Popup Invoice Detail End--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <%-- <asp:UpdatePanel ID="upnlMsg" runat="server">--%>
                                <asp:Panel ID="pnlMsg" runat="server">
                                    <%--<ContentTemplate>--%>
                                    <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                                </asp:Panel>
                                <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
                            </td>
                        </tr>
                        <tr>
                            <td height="2" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="2">
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2" style="padding-right: 15px;">
                                <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Search" ImageUrl="../images/csv.png" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="top">
                                <asp:GridView ID="grdOrder" runat="server" AllowSorting="True" DataSourceID="sqldsOrder"
                                    AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0"
                                    GridLines="None" AutoGenerateColumns="False" Style="border-collapse: separate;"
                                    CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="invID" 
                                    Width="100%">
                                    <Columns>
                                        <asp:TemplateField SortExpression="InvRefNo" HeaderText="<%$ Resources:Resource, grdINInvoiceNo %>"
                                            HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="white">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" ID="lnkInvDetail" Font-Bold="true" CommandName="InvDetail"
                                                    CommandArgument='<%# Eval("invID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="left" Width="60px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="invForOrderNo" HeaderText="<%$ Resources:Resource, grdINOrderNo %>"
                                            ReadOnly="True" SortExpression="invForOrderNo">
                                            <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PartnerAcronyme" HeaderText="<%$ Resources:Resource, grdCMAcronyme %>"
                                            ReadOnly="True" SortExpression="PartnerAcronyme">
                                            <ItemStyle Width="75px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CustomerName" HeaderText="<%$ Resources:Resource, grdSOCustomerName %>"
                                            ReadOnly="True" SortExpression="CustomerName">
                                            <ItemStyle Width="240px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="amount" HeaderText="<%$ Resources:Resource, POTotalAmount %>"
                                            ReadOnly="True" SortExpression="amount">
                                            <HeaderStyle CssClass="price" />
                                            <ItemStyle Width="120px" Wrap="true" CssClass="price" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="invDate" HeaderText="<%$ Resources:Resource, grdSOCreatedDate %>"
                                            ReadOnly="True" SortExpression="invDate">
                                            <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="invStatus" HeaderText="<%$ Resources:Resource, grdPOStatus %>"
                                            ReadOnly="True" SortExpression="invStatus">
                                            <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="invCustPO" HeaderText="<%$ Resources:Resource, grdINCustPO %>"
                                            ReadOnly="True" SortExpression="invCustPO">
                                            <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="invRefType" HeaderText="<%$ Resources:Resource, grdINRefType %>"
                                            ReadOnly="True" SortExpression="invRefType">
                                            <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="orderTypeDesc" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grduserOderType %>"
                                            ReadOnly="True" SortExpression="orderTypeDesc">
                                            <ItemStyle Width="150px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, grdEdit %>" HeaderStyle-ForeColor="#ffffff"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("invID") %>'
                                                    CommandName="Edit" ImageUrl="~/images/edit_icon.png" />
                                                <div style="display: none;">
                                                    <asp:TextBox ID="txtCur" runat="server" Text='<%# Eval("invCurrencyCode") %>'>
                                                    </asp:TextBox>
                                                    <asp:TextBox ID="txtInvID" runat="server" Text='<%# Eval("invID") %>'>
                                                    </asp:TextBox>
                                                    <asp:TextBox ID="txtwhs" runat="server" Text='<%# Eval("invShpWhsCode") %>'>
                                                    </asp:TextBox>
                                                    <asp:TextBox ID="txtInvRefNo" runat="server" Text='<%# Eval("InvRefNo") %>'>
                                                    </asp:TextBox>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="70px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" Visible="false"
                                            HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                                    CommandArgument='<%# Eval("invID") %>' CommandName="Delete" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="70px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="grid_footer" />
                                    <RowStyle CssClass="grid_rowstyle" />
                                    <PagerStyle CssClass="grid_footer" />
                                    <HeaderStyle CssClass="grid_header" Height="26px" />
                                    <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                    <PagerSettings PageButtonCount="20" />
                                </asp:GridView>
                                <asp:SqlDataSource ID="sqldsOrder" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td height="10">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <table width="100%">
                                    <tr>
                                        <td height="20" align="center" width="40%">
                                        </td>
                                        <td width="60%">
                                            <div class="buttonwrapper">
                                                <a id="cmdBack" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                                    <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                        <%=Resources.Resource.cmdInvBack%></span></a></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <br />
    <br />
    <script type="text/javascript" language="javascript">
        function funOpenLocation() {
            document.getElementById('<%=divViewInvoice.ClientID%>').style.display = "block";
            return false;
        }

        function funCloseLoc() {
            document.getElementById('<%=divViewInvoice.ClientID%>').style.display = "none";
        }

        var browserWidth = 0, browserHeight = 0;
        browserwidth2 = (document.documentElement.clientWidth - 756) / 2;
        var hdn = document.getElementById('<%=hdnLeft.ClientID%>');
        hdn.value = browserwidth2;
    </script>
</asp:Content>

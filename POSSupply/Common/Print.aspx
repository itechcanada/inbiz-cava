<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Print.aspx.cs" Inherits="Common_Print" %>
<%@ Register src="PrintScript.ascx" tagname="PrintScript" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" style="margin:0;padding:0;height:100%;">
<head id="Head1" runat="server">
   
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
    <%--<link href="../POS/Css/style.css" rel="stylesheet" type="text/css" />  --%>
    <style type="text/css">
        .lblPOSPrintBold
{
    font: normal 11px Tahoma, Arial;
	color: #494949;
	font-weight:bold;
	font-size:9px;    
}
.lblPOSPrint
{
    font: normal 11px Tahoma, Arial;
	color: black;
	font-size:9px;    
}
    </style>
</head>
<body  style="background-color:#fff;" >
    <form id="form1" runat="server">
    <uc1:PrintScript ID="PrintScript1" runat="server" Visible="true" />
    <asp:PlaceHolder ID="phData" runat="server"></asp:PlaceHolder>
    <asp:Literal ID="ltPrint" Text="" runat="server" />
    </form>
</body>
</html>




Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Imports System.IO
Partial Class Common1_ViewInvoiceDetails
    Inherits Page
    Protected TaxString As String
    Private strSOID As String = ""
    Private objSO As New clsInvoices
    Private objSOItems As New clsInvoiceItems
    Private objSOIP As New clsInvItemProcess
    Private objCust As New clsExtUser
    Private objOrderType As New clsOrderType
    Private clsStatus As New clsSysStatus

    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        lblTitle.Text = lblViewInvoiceDetails '"View Invoice Details"
        If Convert.ToString(Request.QueryString("SOID")) <> "" Then
            strSOID = Convert.ToString(Request.QueryString("SOID"))
        End If

        If Session("UserModules").ToString.Contains("ARC") = False And Session("UserModules").ToString.Contains("ADM") = False Then
            cmdReceivePayment.Visible = False
        End If

        If Session("UserModules").ToString.Contains("INV") = False Then
            cmdSave.Visible = False
        End If

        If Not Page.IsPostBack Then
            clsStatus.subGetStatus(dlStatus, "IN", "INSts")
            subPopulateCurrency()
            subPopulateWarehouse()

            If Convert.ToString(Request.QueryString("SOID")) <> "" Then
                strSOID = Convert.ToString(Request.QueryString("SOID"))
                subFillGrid()
                objSO.InvID = strSOID
                objSO.getInvoicesInfo()
                lblInvWhsName.Text = objSO.InvShpWhsCode
                Dim objOrd As New clsOrders
                hdnSaleAgentID.Value = objOrd.funGetOrdSalesRepID(objSO.InvForOrderNo)
                hdnOrderID.Value = objSO.InvCommission


                objOrd.OrdID = objSO.InvForOrderNo
                objOrd.getOrdersInfo()

                txtTerms.Text = objOrd.ordShippingTerms
                subPopulateTerms()
                lblSODate.Text = IIf(objSO.InvCreatedOn <> "", CDate(objSO.InvCreatedOn).ToString("MMM-dd yyyy"), "--")
                lblInvID.Text = objSO.InvRefNo
                lblSOID.Text = objSO.InvForOrderNo
                objCust.CustID = objSO.InvCustID
                objCust.CustType = objSO.InvCustType
                objCust.getCustomerInfoByType()
                txtNetTerms.Text = objOrd.ordNetTerms
                dlCurrencyCode.SelectedValue = objSO.InvCurrencyCode
                txtExRate.Text = objSO.InvCurrencyExRate
                txtBillToAddress.Text = funPopulateAddress(objCust.CustType, "B", objCust.CustID).Replace("<br>", vbCrLf)
                txtShpToAddress.Text = funPopulateAddress(objCust.CustType, "S", objCust.CustID).Replace("<br>", vbCrLf)
                lblAddress.Text = funPopulateAddress(objCust.CustType, "S", objCust.CustID)
                lblCustomerName.Text = objCust.CustName
                'lblTerms.Text = objCust.CustNetTerms
                lblFax.Text = objCust.CustFax
                'lblAmount.Text = objSO.InvCurrencyCode & " " & CDbl(objSO.Amount) + CDbl(objSOIP.getProcessCostForOrderNo())
                txtNotes.Text = objSO.InvComment
                dlStatus.SelectedValue = objSO.InvStatus

                If objSO.InvRefType = "CN" Then
                    cmdReceivePayment.Visible = False
                End If

                txtShpCost.Text = objSO.InvShpCost
                'txtShpCode.Text = objSO.OrdShpCode
                dlCurrencyCode.SelectedValue = objSO.InvCurrencyCode
                txtCustPO.Text = objSO.InvCustPO
                dlShpWarehouse.SelectedValue = objSO.InvShpWhsCode
                subCalcTax()
                Dim strDocTyp As String = "IN"
                Select Case objSO.InvStatus
                    Case "S", "R", "P"
                        strDocTyp = "IN"
                End Select

                If ConfigurationManager.AppSettings("YellowSR").ToLower = "yes" Then
                    imgPrint.Visible = True
                    imgPrintINV.Visible = False
                Else
                    imgPrintINV.Attributes.Add("OnClick", "return openPDF('../Invoice/ShowPdf.aspx?SOID=" & Request.QueryString("SOID") & "&DocTyp=" & strDocTyp & "&Dup=1')")
                    'Me.imgPrint.Attributes.Add("OnClick", "return openPDF('print.aspx?SOID=" & Request.QueryString("SOID") & "&DocTyp=" & strDocTyp & "')")
                End If
                Me.imgMail.Attributes.Add("OnClick", "return popUp('" & divMail.ClientID & "');")
                Me.imgFax.Attributes.Add("OnClick", "return popUp('" & divFax.ClientID & "');")
                Me.cmdFax.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & ConfirmFaxNumber & " ')")
                txtFaxTo.Text = lblFax.Text
                txtAttention.Text = lblCustomerName.Text
                txtFaxSubject.Text = strDocTyp & "-" & lblSOID.Text
                txtMailTo.Text = objCust.CustEmail
                txtMailSubject.Text = strDocTyp & "-" & lblSOID.Text
            End If
        Else
            If Convert.ToString(Request.QueryString("SOID")) <> "" And strSOID <> "" Then
                subCalcTax()
            End If
        End If
    End Sub
    'Populate Address
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)
        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + "<br>"
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + "<br>"
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + "<br>"
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + "<br>"
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + "<br>"
            Else
                strAddress += objAdr.AddressState + "<br>"
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + "<br>"
            Else
                strAddress += objAdr.AddressCountry + "<br>"
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += lblPostalCode & " " + objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function
    ' Sql Data Source Event Track
    Protected Sub sqldsOrdItems_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsOrdItems.Selected
        subCalcAmount()
    End Sub
    'Populate Grid
    Public Sub subFillGrid()
        sqldsOrdItems.SelectCommand = objSOItems.funFillGrid(strSOID)  'fill Invoice Items
        objSOIP.Invoices_invID = strSOID
        objSOItems.Invoices_invID = strSOID
        sqldsAddProcLst.SelectCommand = objSOIP.funFillGridForProcess 'fill Order Items Process
    End Sub
    'On Row Data Bound
    Protected Sub grdOrdItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdOrdItems.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                       "confirm('" & SOItemConfirmDelete & " ')")
            e.Row.Cells(9).Text = String.Format("{0:F}", CDbl(e.Row.Cells(9).Text))
            Dim objCur As New clsCurrencies
            e.Row.Cells(11).Text = String.Format("{0:F}", CDbl(e.Row.Cells(11).Text)) & objCur.funBaseCurConversion(txtExRate.Text, e.Row.Cells(11).Text)
            Dim txtDisType As TextBox = CType(e.Row.Cells(11).FindControl("txtDisType"), TextBox)
            If txtDisType.Text = "P" Then
                e.Row.Cells(7).Text = e.Row.Cells(7).Text & "%"
            End If
        End If
    End Sub
    'Deleting
    Protected Sub grdOrdItems_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdOrdItems.RowDeleting
        'Dim strItemID As String = grdOrdItems.DataKeys(e.RowIndex).Value.ToString()
        'objSOItems.InvItemID = strItemID
        'sqldsOrdItems.DeleteCommand = objSOItems.funDeleteSOItems()
        'lblMsg.Text = "Sales Order Item Deleted Successfully"
        'subFillGrid()
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub

    'Populate Object
    Private Sub subSetData(ByRef objS As clsInvoices)
        objS.InvCustType = objS.InvCustType
        objS.InvCustID = objS.InvCustID
        objS.InvCreatedOn = CDate(objS.InvCreatedOn).ToString("yyyy-MM-dd hh:mm:ss")
        objS.InvStatus = dlStatus.SelectedValue
        objS.InvLastUpdateBy = Session("UserID").ToString
        objS.InvLastUpdatedOn = Now.ToString("yyyy-MM-dd hh:mm:ss")
        objS.InvShpWhsCode = dlShpWarehouse.SelectedValue
        objS.InvShpCost = objS.InvShpCost
        objS.InvCurrencyCode = dlCurrencyCode.SelectedValue
        objS.InvCurrencyExRate = txtExRate.Text
        objS.InvComment = txtNotes.Text
        objS.InvCustPO = txtCustPO.Text
        If Request.QueryString("ComID") <> "" Then
            objS.InvCompanyID = Request.QueryString("ComID")
        End If
    End Sub
    'On Submit
    Protected Sub cmdSave_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.ServerClick
        If Not Page.IsValid Then
            'Exit Sub
        End If
        If txtCustPO.Text = "" Then
            'lblMsg.Text = "Please enter Customer PO."
            'txtCustPO.Focus()
            'Exit Sub
        ElseIf dlStatus.SelectedItem.Text = "" Then
            lblMsg.Text = "Please select Invoice Status."
            dlStatus.Focus()
            Exit Sub
        End If
        objSO.InvID = Request.QueryString("SOID")
        objSO.getInvoicesInfo()
        subSetData(objSO)
        If dlStatus.SelectedValue = "P" Then
            If objSO.updateInvoices = True Then
                Session.Add("Msg", "Invoice Updated Successfully")
            Else
                Session.Add("Msg", "Invoice Not Updated Successfully")
            End If
        Else
            If objSO.updateInvoices = True Then
                Session.Add("Msg", "Invoice Updated Successfully")
            Else
                Session.Add("Msg", "Invoice Not Updated Successfully")
            End If
        End If
        If Request.QueryString("Col") <> "" Then
            Response.Redirect("~/Common/ViewInvoice.aspx?Col=" & Request.QueryString("Col"))
        Else
            Response.Redirect("~/Common/ViewInvoice.aspx?PartnerID=" & Request.QueryString("PartnerID"))
        End If
    End Sub
    ' On Back
    Protected Sub cmdBack_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReset.ServerClick
        If Request.QueryString("Col") <> "" And Request.QueryString("PartnerID") <> "" Then
            Response.Redirect("~/Common/ViewInvoice.aspx?Col=" & Request.QueryString("Col") & "&PartnerID=" & Request.QueryString("PartnerID"))
        Else
            Response.Redirect("~/Common/ViewInvoice.aspx?PartnerID=" & Request.QueryString("PartnerID"))
        End If
        ' Response.Redirect("ViewInvoice.aspx")
    End Sub

    'Get Address Listing
    Protected Function funAddressListing(ByVal strID As String)
        Dim strData As String = ""
        If strID <> "" Then
            Dim objWhs As New clsWarehouses
            objWhs.WarehouseCode = strID
            objWhs.getWarehouseInfo()
            strData = "<table align='left' width=100% cellspacing='0' cellpadding='0' border='0' style='border:0px'>"
            If objWhs.WarehouseCode <> "" Then
                'strData += "<tr><td align='left'>" + objWhs.WarehouseCode + "</td></tr>"
            End If
            If objWhs.WarehouseDescription <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseDescription + "</td></tr>"
            End If
            If objWhs.WarehouseAddressLine1 <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseAddressLine1 + "</td></tr>"
            End If
            If objWhs.WarehouseAddressLine2 <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseAddressLine2 + "</td></tr>"
            End If
            If objWhs.WarehouseCity <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseCity + "</td></tr>"
            End If
            If objWhs.WarehouseState <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseState + "</td></tr>"
            End If
            If objWhs.WarehouseCountry <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseCountry + "</td></tr>"
            End If
            If objWhs.WarehousePostalCode <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehousePostalCode + "</td></tr>"
            End If
            strData += "</table>"
            objWhs = Nothing
        End If
        Return strData
    End Function
    'Populate Currency
    Private Sub subPopulateCurrency()
        Dim objCur As New clsCurrencies
        objCur.PopulateCurrency(dlCurrencyCode)
        objCur = Nothing
    End Sub
    'Populate Warehouse
    Private Sub subPopulateWarehouse()
        Dim objWhs As New clsWarehouses
        objWhs.PopulateWarehouse(dlShpWarehouse)
        objWhs = Nothing
    End Sub
    'Populate Terms
    Private Sub subPopulateTerms()
        Dim objComp As New clsCompanyInfo
        objComp.CompanyID = objSO.InvCompanyID
        objComp.getCompanyInfo()
        'txtTerms.Text = objComp.CompanyShpToTerms
        lblInvComNameTitle.Text = objComp.CompanyName
        Dim objWhs As New clsWarehouses
        objWhs.WarehouseCompanyID = objComp.CompanyID
        'lblInvWhsName.Text = objWhs.funGetWhsName
        objComp = Nothing
        objWhs = Nothing
    End Sub
    'On Row Data Bound
    Protected Sub grdAddProcLst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAddProcLst.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & SOProcessItemConfirmDelete & " ')")
            e.Row.Cells(3).Text = String.Format("{0:F}", CDbl(e.Row.Cells(3).Text))
            e.Row.Cells(4).Text = String.Format("{0:F}", CDbl(e.Row.Cells(4).Text))
            e.Row.Cells(5).Text = String.Format("{0:F}", CDbl(e.Row.Cells(5).Text))
            e.Row.Cells(8).Text = String.Format("{0:F}", CDbl(e.Row.Cells(8).Text))
        End If
    End Sub
    ' On Row Deleting
    Protected Sub grdAddProcLst_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdAddProcLst.RowDeleting
        'Dim strItemID As String = grdAddProcLst.DataKeys(e.RowIndex).Value.ToString()
        'objSOIP.OrderItemProcID = strItemID
        'sqldsAddProcLst.DeleteCommand = objSOIP.funDeleteSOItemProcess()
        'lblMsg.Text = msgSOProcessDeletedSuccessfully '"Process Deleted Successfully"
        'subFillGrid()
    End Sub
    ' Sql Data Source Event Track
    Protected Sub sqldsAddProcLst_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsAddProcLst.Selected
        subCalcAmount()
    End Sub
    ' Calcualte Amount
    Private Sub subCalcAmount()
        objSOIP.Invoices_invID = strSOID
        objSOItems.Invoices_invID = strSOID
        'lblAmount.Text = dlCurrencyCode.SelectedValue & " " & (CDbl(objSOItems.getItemCostForOrderNo) + CDbl(objSOIP.getProcessCostForOrderNo()))
        If grdAddProcLst.Rows.Count > 0 Then
            lblProcessList.Visible = True
        Else
            lblProcessList.Visible = False
        End If
    End Sub
    ' Calculate Tax
    Private Sub subCalcTax()
        lblSubTotal.Text = "0"
        Dim drSOItems As Data.Odbc.OdbcDataReader
        Dim objPrn As New clsPrint
        Dim objCur As New clsCurrencies
        drSOItems = objSOItems.GetDataReader(objSOItems.funFillGrid(strSOID))
        Dim dblSubTotal As Double = 0
        Dim dblTax As Double = 0
        Dim dblProcessTotal As Double = 0
        Dim dblTotal As Double = 0
        Dim strTaxArray(,) As String
        Dim dblAmount As Double = 0
        Dim dblDiscountApplied As Double = 0
        While drSOItems.Read
            Dim strTax As ArrayList
            Dim strTaxItem As String
            strTax = objPrn.funCalcTax(drSOItems("invProductID"), drSOItems("invShpWhsCode"), drSOItems("amount"), "", drSOItems("invCustID"), drSOItems("invCustType"), drSOItems("invProductTaxGrp").ToString)
            Dim intI As Integer = 0
            While strTax.Count - 1 >= intI
                strTaxItem = strTax.Item(intI).ToString
                strTaxArray = objPrn.funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                intI += 1
            End While
            dblSubTotal = CDbl(dblSubTotal) + CDbl(drSOItems("amount"))
            lblSubTotal.Text = String.Format("{0:F}", dblSubTotal) 'dblSubTotal.ToString
        End While
        Dim i As Integer = 0
        Dim j As Integer = 0

        objSOItems.Invoices_invID = strSOID
        dblProcessTotal = CDbl(objSOIP.getProcessCostForInvoiceNo())
        strTaxArray = objPrn.funTotalProcessTax(dblProcessTotal, dlShpWarehouse.SelectedValue, strTaxArray)

        If Not strTaxArray Is Nothing Then
            j = strTaxArray.Length / 2
        End If
        TaxString = ""
        While i < j
            TaxString &= "<tr><td align=left><b>" & strTaxArray(0, i) & ":</b></td><td align=right><b>" & dlCurrencyCode.SelectedValue & " " & String.Format("{0:F}", strTaxArray(1, i)) & "</b></td></tr>"
            dblTax += strTaxArray(1, i)
            i += 1
        End While

        dblTotal = dblTotal + dblSubTotal + dblProcessTotal + dblTax
        lblAmount.Text = String.Format("{0:F}", dblTotal)
        Dim objUser As New clsUser
        Dim strCommission As String = objOrderType.funOderCommission(hdnOrderID.Value, hdnSaleAgentID.Value, lblSubTotal.Text)
        If strCommission = 0 Then
            trCommission.Visible = False
        End If
        lblCommAgentName.Text = objUser.funUserName(hdnSaleAgentID.Value)
        lblCommission.Text = dlCurrencyCode.SelectedValue & " " & strCommission
        lblSubTotal.Text = dlCurrencyCode.SelectedValue & " " & lblSubTotal.Text
        lblAmount.Text = dlCurrencyCode.SelectedValue & " " & lblAmount.Text & " " & objCur.funBaseCurConversion(txtExRate.Text, dblTotal)
        drSOItems.Close()
        objSOItems.CloseDatabaseConnection()
        objPrn = Nothing
    End Sub
    ' On Send Mail Clicks
    Protected Sub cmdMail_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdMail.ServerClick
        If Request.QueryString("SOID") <> "" Then
            Dim strDocTyp As String = "IN"
            Select Case objSO.InvStatus
                Case "S", "R", "P"
                    strDocTyp = "IN"
            End Select
            Dim strContent As String = ""
            Dim objPrn As New clsPrintHTML(Request.QueryString("SOID"))
            strContent = objPrn.PrintRequestDetail(Request.QueryString("SOID"), strDocTyp)

            Dim strFileName As String = Request.QueryString("DocTyp") + "_" + Request.QueryString("SOID") + "_" + Now.ToString("yyyyMMddhhmmss") + ".html"
            Dim strFullName As String = Server.MapPath("~") + "/pdf/" + strFileName
            File.WriteAllText(strFullName, strContent)

            Dim strMailFrom As String = Session("EmailID").ToString
            Dim strMailTo As String = txtMailTo.Text
            Dim strSub As String = txtMailSubject.Text
            Dim strMsg As String = ""
            strMsg = txtMailMessage.Text
            Dim obj As New clsCommon
            If obj.SendMail(strSub, strMsg, strMailTo, strMailFrom, strFullName) = True Then
                Dim objDoc As New clsDocuments
                objDoc.SysDocType = strDocTyp
                objDoc.SysDocDistType = "E"
                objDoc.SysDocDistDateTime = Now.ToString("yyyy-MM-dd hh:mm:ss")
                objDoc.SysDocPath = strFullName
                objDoc.SysDocEmailTo = strMailTo
                objDoc.SysDocFaxToPhone = ""
                objDoc.SysDocFaxToAttention = ""
                objDoc.SysDocBody = strMsg
                objDoc.SysDocSubject = strSub
                objDoc.insertDocuments()
                objDoc = Nothing
            End If
            obj = Nothing
        End If
    End Sub
    ' On Send Fax Clicks
    Protected Sub cmdFax_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFax.ServerClick
        If Request.QueryString("SOID") <> "" Then
            Dim strDocTyp As String = "IN"
            Select Case objSO.InvStatus
                Case "S", "R", "P"
                    strDocTyp = "IN"
            End Select
            Dim strContent As String = ""
            Dim objPrn As New clsPrintHTML(Request.QueryString("SOID"))
            strContent = objPrn.PrintRequestDetail(Request.QueryString("SOID"), strDocTyp)

            Dim strFileName As String = Request.QueryString("DocTyp") + "_" + Request.QueryString("SOID") + "_" + Now.ToString("yyyyMMddhhmmss") + ".html"
            Dim strFullName As String = Server.MapPath("~") + "/pdf/" + strFileName
            File.WriteAllText(strFullName, strContent)

            Dim strFaxInfo() As String
            Dim objCI As New clsCompanyInfo
            objSO.InvID = strSOID
            objSO.getInvoicesInfo()
            objCI.CompanyID = objSO.InvCompanyID
            strFaxInfo = objCI.funGetFirstCompanyFaxInfo().Split("~")
            Dim strMailFrom As String = Session("EmailID").ToString
            Dim strMailTo As String = ""
            Dim strSub As String = txtFaxSubject.Text
            Dim strMsg As String = ""
            If txtFaxTo.Text.Contains(strFaxInfo(0)) = False Then
                strMailTo = objCI.funFaxNumbers(txtFaxTo.Text) & "@" & strFaxInfo(0)
            Else
                strMailTo = txtFaxTo.Text
            End If
            If strFaxInfo(1).Contains("username:") Then
                strMsg += strFaxInfo(1) & vbCrLf
            End If
            If strFaxInfo(2).Contains("code:") Then
                strMsg += strFaxInfo(2) & vbCrLf
            End If
            strMsg += "Attention: " & txtAttention.Text & vbCrLf
            strMsg += "Message: "
            strMsg += txtFaxMessage.Text
            Dim obj As New clsCommon
            If obj.SendFax(strSub, strMsg, strMailTo, strMailFrom, strFullName) = True Then
                Dim objDoc As New clsDocuments
                objDoc.SysDocType = strDocTyp
                objDoc.SysDocDistType = "F"
                objDoc.SysDocDistDateTime = Now.ToString("yyyy-MM-dd hh:mm:ss")
                objDoc.SysDocPath = strFullName
                objDoc.SysDocEmailTo = ""
                objDoc.SysDocFaxToPhone = strMailTo
                objDoc.SysDocFaxToAttention = txtAttention.Text
                objDoc.SysDocBody = txtFaxMessage.Text
                objDoc.SysDocSubject = strSub
                objDoc.insertDocuments()
                objDoc = Nothing
            End If
            obj = Nothing
        End If
    End Sub
    Protected Sub imgPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPrint.Click
        'Response.Write("<script>window.open('printYSR.aspx?SOID=" & Request.QueryString("SOID") & "&DocTyp=IN&PageNo=1&Dup=1', 'Page1','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');</script>")
        Dim dt As DateTime = Now.AddSeconds(2)
        Do
            If Now > dt Then
                Exit Do
            End If
        Loop
        Response.Write("<script>window.open('../Invoice/printYSR.aspx?SOID=" & Request.QueryString("SOID") & "&DocTyp=IN&PageNo=2&Dup=1', 'Page2','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');</script>")
    End Sub
    Protected Sub cmdReceivePayment_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReceivePayment.ServerClick
        If Convert.ToString(Request.QueryString("SOID")) <> "" Then
            Response.Redirect("../AccountsReceivable/InvoicePayment.aspx?SOID=" & Request.QueryString("SOID").ToString & "&Col=" & Request.QueryString("Col").ToString)
        End If
    End Sub

    Protected Sub imgPrintINV_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPrintINV.Click
        Response.Write("<script>window.open('../Invoice/showpdf.aspx?SOID=" & Request.QueryString("SOID") & "&DocTyp=IN', 'Invoice','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');</script>")
    End Sub
End Class

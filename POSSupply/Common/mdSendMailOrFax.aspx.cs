﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Common_mdSendMailOrFax : BasePage
{
    private Control _contentControl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetEmailToAddress();
            switch (this.Command)
            {
                case "F":
                    pnlFax.Visible = true;
                    pnlMail.Visible = false;
                    txtFaxSubject.Text = string.Format("{0}-{1}", this.GetDocType(), Request.QueryString["SOID"]);
                    txtFaxTo.Focus();
                    break;
                default:
                    pnlFax.Visible = false;
                    pnlMail.Visible = true;
                    txtMailSubject.Text = string.Format("{0}-{1}", this.GetDocType(), Request.QueryString["SOID"]);                    
                    txtMailTo.Focus();
                    break;
            }
        }
        txtMailTo.Focus();
    }

    private void SetEmailToAddress()
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            SysCompanyInfo ci = new SysCompanyInfo();
            ci.PopulateObject(CurrentUser.DefaultCompanyID, dbHelp);
            hdnInternetFax.Value = ci.CompanyInternetFax;

            Partners part = new Partners();
            int reqID = 0;
            int.TryParse(Request.QueryString["SOID"], out reqID);
            switch (this.GetDocType())
            {
                case "IN":
                    Invoice inv = new Invoice();
                    inv.PopulateObject(dbHelp, reqID);
                    part.PopulateObject(dbHelp, inv.InvCustID);
                    txtMailTo.Text = part.PartnerEmail;
                    txtFaxTo.Text = part.PartnerFax;
                    break;
                case "SO":
                case "QO":
                    Orders ord = new Orders();
                    ord.PopulateObject(dbHelp, reqID);
                    part.PopulateObject(dbHelp, ord.OrdCustID);
                    txtMailTo.Text = part.PartnerEmail;
                    txtFaxTo.Text = part.PartnerFax;
                    break;
                case "PO":
                    PurchaseOrders po = new PurchaseOrders();
                    po.PopulateObject(dbHelp, reqID);
                    Vendor vd = new Vendor();
                    vd.PopulateObject(dbHelp, po.PoVendorID);
                    txtMailTo.Text = vd.VendorEmailID;
                    txtFaxTo.Text = vd.VendorFax;
                    break;
            }
        }
        catch
        {

        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
        
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            try
            {                
                if (!string.IsNullOrEmpty(Request.QueryString["SOID"]))
                {
                    string htmlSnnipt = @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">";
                    htmlSnnipt += @"<html xmlns=""http://www.w3.org/1999/xhtml""><head><title>Print</title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>#DATA_TO_WRITE#</body></html>";

                    //Initialize Print Data
                    //_contentControl = Page.LoadControl("~/PrintTemplates/OrderTemplate.ascx");
                    //phData.Controls.Clear();
                    //phData.Controls.Add(_contentControl);
                    //_contentControl.ID = "ctlContent";
                    //IPrint ctrl = (IPrint)_contentControl;
                    //ctrl.DocType = this.GetDocType();
                    //ctrl.RequestID = BusinessUtility.GetInt(Request.QueryString["SOID"]);
                    //ctrl.InitDetails();

                    //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    //using (StringWriter sw = new StringWriter(sb))
                    //{
                    //    using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
                    //    {
                    //        _contentControl.RenderControl(textWriter);
                    //    }
                    //}

                    int reqID = 0;
                    int.TryParse(Request.QueryString["SOID"], out reqID);
                    string strDocTyp = this.GetDocType();
                    string htmlToMail = string.Empty;
                    //string appName = System.Configuration.ConfigurationManager.AppSettings["ApplicationName"];
                    switch (strDocTyp.ToUpper().Trim())
                    {
                        case "QO":
                            htmlToMail = CommonPrint.GetOrderPrintHtmlSnipt(this, "QO", reqID);
                            break;
                        case "SO":
                            htmlToMail = CommonPrint.GetOrderPrintHtmlSnipt(this, "SO", reqID);
                            break;
                        case "IN":
                            htmlToMail = CommonPrint.GetInvoicePrintHtmlSnipt(this, reqID);
                            break;
                        case "PO":
                            htmlToMail = CommonPrint.GetPOPrintHtmlSnipt(this, reqID);
                            break;
                    }

                    string strContent = htmlSnnipt.Replace("#DATA_TO_WRITE#", htmlToMail);

                    //clsPrintHTML objPrn = new clsPrintHTML(Request.QueryString["SOID"]);
                    //strContent = objPrn.PrintRequestDetail(Request.QueryString["SOID"], strDocTyp, "");

                    string strFileName = strDocTyp + "_" + Request.QueryString["SOID"] + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".html";
                    string strFullName = Server.MapPath("~") + "/pdf/" + strFileName;
                    File.WriteAllText(strFullName, strContent);
                    
                    //SysCompanyInfo objCI = new SysCompanyInfo();
                    //objCI.PopulateObject(CurrentUser.DefaultCompanyID, null);
                    string comFax = txtFaxTo.Text;//objCI.CompanyInternetFax;                    
                    string strMailFrom = CurrentUser.EmailID;
                    string strMailTo = hdnInternetFax.Value.Replace("!FAXNUMBER!", this.GetNumberFromString(comFax));
                    string strSub = txtFaxSubject.Text;
                    string strMsg = "";
                    strMsg += "Attention: " + txtAttention.Text + Microsoft.VisualBasic.Constants.vbCrLf;
                    strMsg += "Message: ";
                    strMsg += txtFaxMessage.Text;
                    //clsCommon obj = new clsCommon();
                    //if (obj.SendFax(strSub, strMsg, strMailTo, strMailFrom, strFullName) == true)
                    if(EmailHelper.SendEmail(strMailFrom, strMailTo, strMsg, strSub, strFullName))
                    {
                        SysDocuments objDoc = new SysDocuments();
                        objDoc.SysDocType = strDocTyp;
                        objDoc.SysDocDistType = "F";
                        objDoc.SysDocDistDateTime = DateTime.Now;
                        objDoc.SysDocPath = strFullName;
                        objDoc.SysDocEmailTo = "";
                        objDoc.SysDocFaxToPhone = strMailTo;
                        objDoc.SysDocFaxToAttention = txtAttention.Text;
                        objDoc.SysDocBody = txtFaxMessage.Text;
                        objDoc.SysDocSubject = strSub;
                        objDoc.Insert(null);
                        objDoc = null;
                    }

                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgINFaxSendSuccessfully);
                }
            }
            catch 
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
            
        }

        Globals.RegisterReloadParentScript(this);
        //Globals.RegisterCloseDialogScript(this);
    }

    private string GetDocType()
    {
        return BusinessUtility.GetString(Request.QueryString["DocTyp"]);
    }

    protected void btnSendMail_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["SOID"]))
        {
            try
            {
                string htmlSnnipt = @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">";
                htmlSnnipt += @"<html xmlns=""http://www.w3.org/1999/xhtml""><head><title>Print</title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>#DATA_TO_WRITE#</body></html>";

                //Initialize Print Data
                //_contentControl = Page.LoadControl("~/PrintTemplates/OrderTemplate.ascx");
                ////phData.Controls.Clear();
                ////phData.Controls.Add(_contentControl);
                //_contentControl.ID = "ctlContent";
                //IPrint ctrl = (IPrint)_contentControl;
                //ctrl.DocType = this.GetDocType();
                //ctrl.RequestID = BusinessUtility.GetInt(Request.QueryString["SOID"]);
                //ctrl.InitDetails();

                //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                //using (StringWriter sw = new StringWriter(sb))
                //{
                //    using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
                //    {
                //        _contentControl.RenderControl(textWriter);
                //    }
                //}

                int reqID = 0;
                int.TryParse(Request.QueryString["SOID"], out reqID);
                string strDocTyp = this.GetDocType();
                string htmlToMail = string.Empty;
                //string appName = System.Configuration.ConfigurationManager.AppSettings["ApplicationName"];
                switch (strDocTyp.ToUpper().Trim())
                {
                    case "QO":
                        htmlToMail = CommonPrint.GetOrderPrintHtmlSnipt(this, "QO", reqID);
                        break;
                    case "SO":
                        htmlToMail = CommonPrint.GetOrderPrintHtmlSnipt(this, "SO", reqID);
                        break;
                    case "IN":
                        htmlToMail = CommonPrint.GetInvoicePrintHtmlSnipt(this, reqID);                     
                        break;
                    case "PO":
                        htmlToMail = CommonPrint.GetPOPrintHtmlSnipt(this, reqID);   
                        break;
                }

                string strContent = htmlSnnipt.Replace("#DATA_TO_WRITE#", htmlToMail);

                //clsPrintHTML objPrn = new clsPrintHTML(Request.QueryString["SOID"]);
                //strContent = objPrn.PrintRequestDetail(Request.QueryString["SOID"], strDocTyp, "");

                string strFileName = strDocTyp + "_" + Request.QueryString["SOID"] + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".html";
                string strFullName = Server.MapPath("~") + "/pdf/" + strFileName;
                File.WriteAllText(strFullName, strContent);

                string strMailFrom = System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"]; //CurrentUser.EmailID;
                string strMailTo = txtMailTo.Text;
                string strSub = txtMailSubject.Text;
                string strMsg = "";
                strMsg = txtMailMessage.Text;

                if (EmailHelper.SendEmail(strMailFrom, strMailTo, strMsg, strSub, strFullName))
                {
                    SysDocuments objDoc = new SysDocuments();
                    objDoc.SysDocType = strDocTyp;
                    objDoc.SysDocDistType = "E";
                    objDoc.SysDocDistDateTime = DateTime.Now;
                    objDoc.SysDocPath = strFullName;
                    objDoc.SysDocEmailTo = strMailTo;
                    objDoc.SysDocFaxToPhone = "";
                    objDoc.SysDocFaxToAttention = "";
                    objDoc.SysDocBody = strMsg;
                    objDoc.SysDocSubject = strSub;
                    objDoc.Insert(null);
                    objDoc = null;

                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgEmailWasSentSuccessfully);
                }

                //clsCommon obj = new clsCommon();
                //if (obj.SendMail(strSub, strMsg, strMailTo, strMailFrom, strFullName) == true)
                //{
                //    clsDocuments objDoc = new clsDocuments();
                //    objDoc.SysDocType = strDocTyp;
                //    objDoc.SysDocDistType = "E";
                //    objDoc.SysDocDistDateTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                //    objDoc.SysDocPath = strFullName;
                //    objDoc.SysDocEmailTo = strMailTo;
                //    objDoc.SysDocFaxToPhone = "";
                //    objDoc.SysDocFaxToAttention = "";
                //    objDoc.SysDocBody = strMsg;
                //    objDoc.SysDocSubject = strSub;
                //    objDoc.insertDocuments();
                //    objDoc = null;
                //}
                //obj = null;
            }
            catch 
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }

        Globals.RegisterReloadParentScript(this);
        //Globals.RegisterCloseDialogScript(this);
    }

    private string Command
    {
        get {
            return BusinessUtility.GetString(Request.QueryString["cmd"]);
        }
    }

    private string CallBackJs {
        get {
            return Request.QueryString["callback"];
        }
    }

    private string GetNumberFromString(string strData)
    {
        string sOut = string.Empty;
        foreach (var item in strData)
        {
            if (char.IsDigit(item))
            {
                sOut += item;
            }
        }
        return sOut;
    }
}
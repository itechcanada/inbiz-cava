Imports System
Imports System.IO
Imports System.Data.Odbc
Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Configuration.ConfigurationManager

Partial Class Common_PrintBatch
    Inherits Page
    Private sHtmlData As String = ""
    Protected Sub Common_PrintBatch_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Redirect("Print.aspx?" + Request.QueryString.ToString)
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            subClose()
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("ReqID") <> "" Then
                Dim objPrn As New clsPrintHTML(Request.QueryString("ReqID"))
                If Request.QueryString("DocTyp") = "PO" Then
                    sHtmlData = objPrn.PrintPurchaseOrder(Request.QueryString("ReqID"))
                ElseIf Request.QueryString("DocTyp") = "QO" Or Request.QueryString("DocTyp") = "SO" Or Request.QueryString("DocTyp") = "IN" Then
                    sHtmlData = objPrn.PrintRequestDetail(Request.QueryString("ReqID"), Request.QueryString("DocTyp"))
                End If
                lblData.ForeColor = Drawing.Color.Black
                lblData.Text = sHtmlData
            End If
        End If
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    ' Sub routine to close popup
    Private Sub subClose()
        Response.Write("<script>window.opener.location.reload();</script>")
        Response.Write("<script>window.close();</script>")
    End Sub
End Class

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using Trirand.Web.UI.WebControls;

public partial class Common_mdUpComingEvents : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void grdEvetns_DataRequesting(object sender, JQGridDataRequestEventArgs e)
    {
        grdEvetns.DataSource = new ReservationItems().GetUpcommingEvents(this.GuestID, Globals.CurrentAppLanguageCode);
        grdEvetns.DataBind();
    }

    public int GuestID {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["guestid"], out id);
            return id;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using Trirand.Web.UI.WebControls;

public partial class Common_mdSearchGuest : BasePage
{
    Partners _part = new Partners();
    ReservationItems _rsvItms = new ReservationItems();

    protected void Page_Load(object sender, EventArgs e)
    {
        //btnAddCustomer.OnClientClick = string.Format("parent.location.href='{0}?{1}'; return false;", ResolveUrl("~/Partner/CustomerEdit.aspx"), Request.QueryString);
        if (!IsPagePostBack(grdCustomers))
        {
            //if (!string.IsNullOrEmpty(this.JsCallback))
            //{
            //    grdCustomers.ClientSideEvents.RowSelect = "rowSelect";
            //    string script = "function rowSelect(id) {";
            //    script += string.Format("parent.{0}({1});", this.JsCallback, "id");
            //    script += "jQuery.FrameDialog.closeDialog();}";

            //    Page.ClientScript.RegisterStartupScript(this.GetType(), "rowSelect", script, true);
            //}

            btnAddCustomer.OnClientClick = string.Format("return parent.goToAddNewGuestPage('E', {0}, {1});", (int)this.GuestType, this.ReservationItemID);
            txtSearch.Focus();
        }
    }

    protected void grdCustomers_DataRequesting(object sender, JQGridDataRequestEventArgs e)
    {
        bool isSalesRescricted = CurrentUser.IsInRole(RoleID.SALES_REPRESENTATIVE) && BusinessUtility.GetBool(Session["SalesRepRestricted"]);
        string search = string.Empty;
        if (Request.QueryString.AllKeys.Contains<string>("_history"))
        {
            string srcKey = Request.QueryString[txtSearch.ClientID];
            grdCustomers.DataSource = ProcessReservation.SearchGuest(srcKey, this.GuestType, this.ReservationID, isSalesRescricted);
        }
        else
        {
            grdCustomers.DataSource = ProcessReservation.SearchGuest(Request.QueryString["fname"], Request.QueryString["lName"], Request.QueryString["email"], Request.QueryString["phone"], this.GuestType, this.ReservationID, isSalesRescricted);
        }
    }

    protected void grdCustomers_CellBinding(object sender, JQGridCellBindEventArgs e)
    {
        //if (e.ColumnIndex == 4)
        //{
        //    int eventsCount =  _rsvItms.GetUpcommingEventsCount(BusinessUtility.GetInt(e.RowKey));
        //    string url = string.Format("({1}) <a class='up_events' href='{0}' style='text-decoration:underline;'>Events</a>", ResolveUrl(string.Format("mdUpComingEvents.aspx?guestid={0}", e.RowKey)), eventsCount);
        //    e.CellHtml = eventsCount > 0 ? url : "(0) Events";
        //}
        //else if (e.ColumnIndex == 5)
        //{
        //    List<int> lstSo = _rsvItms.GetSOHistory(BusinessUtility.GetInt(e.RowKey));            
        //    string data = string.Empty;
        //    foreach (var item in lstSo)
        //    {
        //        data += string.Format("RSV-{0}, ", item);
        //    }
        //    e.CellHtml = data.Trim(' ', ',');
        //}
    }

    private StatusGuestType GuestType
    {
        get
        {
            return QueryStringHelper.GetGuestType("gType");
        }
    }

    private int ReservationItemID
    {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["ritmid"], out id);
            return id;
        }
    }

    public int ReservationID
    {
        get
        {
            int rid = 0;
            int.TryParse(Request.QueryString["rid"], out rid);
            return rid;
        }
    }

    private string JsCallback
    {
        get {
            return BusinessUtility.GetString(Request.QueryString["jscallback"]);
        }
    }

    private string Opt
    {
        get {
            return BusinessUtility.GetString(Request.QueryString["opt"]);
        }
    }

    protected void grdCustomers_RowSelecting(object sender, JQGridRowSelectEventArgs e)
    {
        int guestID = BusinessUtility.GetInt(e.RowKey);
        switch (this.Opt.Trim().ToLower())
        {
            case "change_guest":
                _rsvItms.ChangeGuest(guestID, this.ReservationItemID);
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallback), true);
                break;
            case "pick_guest":
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.setCurrentGuest({0});", e.RowKey), true);
                break;
            default:
                break;
        }               
    }
}
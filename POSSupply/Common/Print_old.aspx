<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Print_old.aspx.vb" Inherits="Common_Print" %>

<%@ Import Namespace="Resources.Resource" %>
<script language="javascript" type="text/javascript">
    var PrintCommandObject = null;
    function printPage() {
        if (PrintCommandObject) {
            try {
                PrintCommandObject.ExecWB(6, 2);
                PrintCommandObject.outerHTML = "";
            }
            catch (e) {
                window.print();
            }
        }
        else {
            window.print();
        }
        var browserName = navigator.appName;
        if (navigator.appName != "Microsoft Internet Explorer") {

            //window.close();

        }
        else {
            //window.close();
        }
    }

    window.onload = function () {
        Minimize();
        if (navigator.appName == "Microsoft Internet Explorer") {
            // attach and initialize print command ActiveX object
            try {
                var PrintCommand = '<object id="PrintCommandObject" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2" width="0" height="0"></object>';
                document.body.insertAdjacentHTML('beforeEnd', PrintCommand);
            }
            catch (e) { }
        }
        setTimeout(printPage, 1);
    };


    function Minimize() {
        window.innerWidth = 10;
        window.innerHeight = 10;
        window.screenX = screen.width;
        window.screenY = screen.height;
        alwaysLowered = true;
    }

</script>
<asp:label id="lblData" runat="server"></asp:label>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdSendMailOrFax.aspx.cs" Inherits="Common_mdSendMailOrFax" EnableEventValidation="false" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <style type="text/css">
        ul.form li div.lbl{width: 100px !important;}
        ul.form li div.input{width: 320px !important;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <asp:Panel ID="pnlFax" DefaultButton="btnSendFax" runat="server" Visible="false">
        <div style="padding: 5px 10px;">
            <ul class="form">
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblFaxTo" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOFaxTo%>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtFaxTo" runat="server" Width="300px" ValidationGroup="Fax" CssClass="popup_input">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalFax" runat="server" ControlToValidate="txtFaxTo"
                            ValidationGroup="Fax" Display="None" ErrorMessage="<%$ Resources:Resource, reqvalFax%>"
                            SetFocusOnError="true">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblAttention" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOAttention%>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtAttention" CssClass="popup_input" runat="server" Width="300px"
                            ValidationGroup="Fax">
                        </asp:TextBox>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblFaxSubject" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOSubject%>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtFaxSubject" CssClass="popup_input" runat="server" Width="300px"
                            ValidationGroup="Fax">
                        </asp:TextBox>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblFaxMessage" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOMessage%>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtFaxMessage" CssClass="popup_input" runat="server" TextMode="MultiLine"
                            Rows="4" Width="300px" ValidationGroup="Fax">
                        </asp:TextBox>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
            </ul>
        </div>
        <div class="div-dialog-command" style="border-top: 1px solid #ccc; padding: 5px;" >
            <asp:Button ID="btnSendFax" Text="<%$ Resources:Resource, cmdCssAdd %>" runat="server"
                OnClick="btnSend_Click" ValidationGroup="Fax" />
        </div>
        <asp:ValidationSummary ID="valsFax" runat="server" ShowMessageBox="true" ShowSummary="false"
            ValidationGroup="Fax" />
        <script type="text/javascript">
            $(document).ready(function () {
                $("#<%=txtFaxTo.ClientID%>").focus();
            });            
        </script>
    </asp:Panel>

    <asp:Panel ID="pnlMail"  DefaultButton="btnSendMail" runat="server">
        <div style="padding: 5px 10px;">
            <ul class="form">
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblMailTo" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOMailTo%>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtMailTo" CssClass="popup_input" runat="server" Width="300px" ValidationGroup="mail">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalMailTo" runat="server" ControlToValidate="txtMailTo"
                            ValidationGroup="mail" Display="None" ErrorMessage="<%$ Resources:Resource, reqvalMailTo%>"
                            SetFocusOnError="true">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revalexpEmail" runat="server" ControlToValidate="txtMailTo"
                            SetFocusOnError="true" ValidationGroup="mail" Display="None" ErrorMessage="<%$ Resources:Resource, revalexpEmail%>"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                        </asp:RegularExpressionValidator>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblMailSubject" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOSubject%>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtMailSubject" CssClass="popup_input" runat="server" Width="300px"
                            ValidationGroup="mail">
                        </asp:TextBox>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblMailMessage" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOMessage%>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtMailMessage" runat="server" CssClass="popup_input" TextMode="MultiLine"
                            Rows="4" Width="300px" ValidationGroup="mail">
                        </asp:TextBox>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li> 
            </ul>
        </div>
        <div class="div-dialog-command" style="border-top: 1px solid #ccc; padding: 5px;" >
            <asp:Button ID="btnSendMail" Text="<%$ Resources:Resource, cmdCssAdd %>" 
                runat="server" ValidationGroup="mail" onclick="btnSendMail_Click" />
        </div>
         <asp:ValidationSummary ID="valsMail" runat="server" ShowMessageBox="true" ValidationGroup="mail"
            ShowSummary="false" />
        <script type="text/javascript">
            $(document).ready(function () {
                $("#<%=txtMailTo.ClientID%>").focus();
            });
        </script>
    </asp:Panel>

    <asp:Panel ID="pnlPlaceHolder" runat="server" Visible="false">
        <asp:PlaceHolder ID="phData" runat="server"></asp:PlaceHolder>
    </asp:Panel>
    <asp:HiddenField ID="hdnInternetFax" runat="server" Value="" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


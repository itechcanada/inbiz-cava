﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdSysFiles.aspx.cs" Inherits="Common_mdSysFiles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1">All File(s)</a></li>
            <li><a href="#tabs-2">Upload File</a></li>            
        </ul>
        <div id="tabs-1">
            <div id="grid_wrapper">
                <trirand:JQGrid runat="server" ID="grdFiles" DataSourceID="sdsFiles" 
                    Height="200px" Width="100%" AutoWidth="true"
                    PagerSettings-PageSize="50" ondatarequesting="grdFiles_DataRequesting" 
                    oncellbinding="grdFiles_CellBinding" onrowdeleting="grdFiles_RowDeleting">
                    <Columns>
                        <trirand:JQGridColumn DataField="FileID" PrimaryKey="true" Visible="false" />
                        <trirand:JQGridColumn DataField="FilePath" HeaderText="File" TextAlign="Center" />
                        <trirand:JQGridColumn DataField="FileFormat" HeaderText="FileFormat" TextAlign="Center" />
                        <trirand:JQGridColumn DataField="FileType" HeaderText="FileType" TextAlign="Center" />
                         <trirand:JQGridColumn DataField="CreatedOn" HeaderText="CreatedOn" DataFormatString="{0:MM/dd/yyyy}"
                            Editable="false" TextAlign="Center" />       
                        <trirand:JQGridColumn DataField="FilePath" HeaderText="Download" TextAlign="Center" Width="100" Sortable="false" />    
                        <trirand:JQGridColumn DataField="FileID" HeaderText="Delete" TextAlign="Center" Width="100" Sortable="false" />                                        
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="true" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn="" />
                    <AppearanceSettings AlternateRowBackground="True" />
                    <ClientSideEvents LoadComplete="gridLoadComplete" />
                </trirand:JQGrid>
            </div>
            <asp:SqlDataSource ID="sdsFiles" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
        </div>
        <div id="tabs-2">
            <div style="padding: 5px; height: 150px; overflow: auto;">
                <ul class="form" style="margin:0px;">
                    <li>
                        <div class="lbl" style="width:120px;">
                            <asp:Label ID="lblFile1" Text="File 1" runat="server" />
                        </div>
                        <div class="input">
                            <asp:FileUpload ID="fuFile1" runat="server" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>   
                    <li>
                        <div class="lbl" style="width:120px;">
                            <asp:Label ID="Label1" Text="File Type" runat="server" />
                        </div>
                        <div class="input">
                            <asp:DropDownList ID="ddlFileType" runat="server">                                
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvFileType" ErrorMessage="*" ControlToValidate="ddlFileType"
                                runat="server" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>                
                    <li>
                        <div class="lbl" style="width:120px;">
                           
                        </div>
                        <div class="input">
                            <asp:Button ID="btnUpload" Text="Upload File(s)" runat="server" 
                                onclick="btnUpload_Click" />
                            <div id="custom_message" runat="server" style="color:Red;">
                                
                            </div>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnActiveTabIndex" Value="0" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">   
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdFiles.ClientID %>";
        var grid_isloaded = false;
        $hdnActiveTabIndex = $("#<%=hdnActiveTabIndex.ClientID%>");

        $(function () {
            $("#tabs").tabs({
                selected: $hdnActiveTabIndex.val(),
                show: function (event, ui) {
                    $hdnActiveTabIndex.val(ui.index);
                    if (ui.index == 0) {
                        resize_the_grid();
                    }
                }
            });
        });       

        //Function To Resize the grid
        function resize_the_grid() {
            if (!$("#tabs-1").is(":hidden")) {
                $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -2 });            
            }                   
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }


        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);


        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function loadGrid() {
            if (!grid_isloaded) {
                grid_isloaded = true;
            } else
                reloadGrid();
        }

        function gridLoadComplete(data) {
            $(window).trigger("resize");
        }

        function setSelectedRow(key) {
            var grid = $('#' + gridID);
            grid.setSelection(key);
        }             
    </script> 
</asp:Content>


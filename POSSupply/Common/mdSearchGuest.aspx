﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdSearchGuest.aspx.cs" Inherits="Common_mdSearchGuest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div style="padding: 5px; height: 350px; overflow: auto;">
        <asp:Panel ID="pnlSearch" runat="server">
            <div style="float:left;">
              <asp:Label ID="lblSearch" AssociatedControlID="txtSearch" CssClass="filter-key" Text="" runat="server" style="display:none;" />
                <asp:TextBox ID="txtSearch" runat="server" style="padding:4px;" />                
                <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch %>" />                            
            </div>
            <div style="float:right;">
                <asp:Button ID="btnAddCustomer" Text="<%$Resources:Resource, lblAddNewGuest%>" runat="server" />                
            </div>
            <div style="clear:both; margin-bottom:5px;">
            </div>          <%=Resources.Resource.lblTelephone %>              
        </asp:Panel>
        <div id="grid_wrapper">            
            <trirand:JQGrid runat="server" ID="grdCustomers" Height="200px"
                Width="100%" AutoWidth="true" PagerSettings-PageSize="50" OnCellBinding="grdCustomers_CellBinding"
                OnDataRequesting="grdCustomers_DataRequesting" 
                onrowselecting="grdCustomers_RowSelecting">
                <Columns>
                    <trirand:JQGridColumn DataField="PartnerID" PrimaryKey="true" Visible="false" />
                    <trirand:JQGridColumn DataField="FirstName" HeaderText="<%$Resources:Resource, lblFName%>"/>
                    <trirand:JQGridColumn DataField="LastName" HeaderText="<%$Resources:Resource, lblLName%>" Editable="false"
                        Sortable="false" />
                    <trirand:JQGridColumn DataField="SpirtualName" HeaderText="<%$Resources:Resource, lblSpiritualName%>" Editable="false"
                        Sortable="false" />
                    <trirand:JQGridColumn DataField="PartnerEmail" HeaderText="<%$Resources:Resource, lblEmail%>" Editable="false"
                        Sortable="false" />
                    <trirand:JQGridColumn DataField="PartnerPhone" HeaderText="<%$Resources:Resource, lblTelephone%>" Editable="false"
                        Sortable="false" />
                </Columns>
                <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                    ShowDeleteButton="false" ShowSearchButton="false" />
                <SortSettings InitialSortColumn="" />
                <AppearanceSettings AlternateRowBackground="True" />
                <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" 
                    LoadComplete="gridLoadComplete" RowSelect="rowSelect" />
             </trirand:JQGrid>
            <asp:SqlDataSource ID="sdsCustomers" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
            <iCtrl:IframeDialog ID="IframeDialog3" Height="350" Width="500" Title="Upcoming Events"
                Dragable="true" TriggerSelectorClass="up_events" TriggerSelectorMode="Class"
                UrlSelector="TriggerControlHrefAttribute" runat="server">
            </iCtrl:IframeDialog>
        </div>
    </div>  
    <div class="div-dialog-command" style="display:none;">
        <asp:HiddenField ID="hdnSelectedID" runat="server" Value="" />
        <asp:Button ID="btnChange" Text="Change" runat="server" Enabled="false" />
    </div>  
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdCustomers.ClientID %>";
        $grd = $("#<%=grdCustomers.ClientID %>");
        var searchPanelID = "<%=pnlSearch.ClientID %>";        

        //Function To Resize the grid
        function resize_the_grid() {
            $grd.fluidGrid({ example: '#grid_wrapper', offset: -2 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $grd.onJqGridPaging();
        }


        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $grd.onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        function reloadGrid(event, ui) {
            $grd.trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }



        function gridLoadComplete(data) {
            $(window).trigger("resize");
            $("#gs_FirstName").focus(); 
        }

        function rowSelect(id) {
            $("#<%=hdnSelectedID.ClientID%>").val(id);            
            //$("#<%=btnChange.ClientID%>").button({ disabled: false });
            //return false;
        }        
    </script>
</asp:Content>


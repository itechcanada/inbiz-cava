﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using Trirand.Web.UI.WebControls;

public partial class Common_mdSysFiles : BasePage
{
    SysFiles _sysFiles = new SysFiles();
    List<string> _msgList = new List<string>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdFiles))
        {
            DropDownHelper.FillDropdown(ddlFileType, "SY", "dlFty", Globals.CurrentAppLanguageCode, new ListItem(""));
        }
    }

    protected void grdFiles_DataRequesting(object sender, JQGridDataRequestEventArgs e)
    {
        sdsFiles.SelectCommand = _sysFiles.GetSql(sdsFiles.SelectParameters, this.TableRef, this.RefID);
        sdsFiles.DataBind();
    }

    protected void grdFiles_CellBinding(object sender, JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 5)
        {
            e.CellHtml = string.Format(@"<a class=""icon_download_36x36"" target=""_blank"" href=""{0}"">Download</a>", ResolveUrl(CommonFilePath.USER_FILES + e.RowValues[5]));
        }
        else if (e.ColumnIndex == 6)
        {
            e.CellHtml = string.Format(@"<a class=""icon_delete_32x32"" href=""javascript:void(0);"" onclick='setSelectedRow({0}); $(""#del_ctl00_cphMaster_grdFiles"").trigger(""click"");'>Delete</a>", e.RowKey);
        }
    }

    public int RefID {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["refid"], out id);
            return id;
        }
    }

    public string TableRef {
        get {
            return Request.QueryString["tblref"];
        }
    }

    public string FileType {
        get {
            return ddlFileType.SelectedValue;
        }
    }

    private void SaveFile(HttpPostedFile postedFile) {
        string fileName = string.Empty;
        try
        {
            if (_sysFiles.GetFilesCount(this.TableRef, this.RefID, this.FileType) >= 4)
            {
                throw new Exception(CustomExceptionCodes.MAX_FILE_COUNT_EXCEEDED);
            }

            _sysFiles.CreatedBy = CurrentUser.UserID;
            _sysFiles.CreatedOn = DateTime.Now;
            _sysFiles.FileFormat = System.IO.Path.GetExtension(postedFile.FileName);
            _sysFiles.FileRefID = this.RefID;
            _sysFiles.FileTableRef = this.TableRef;
            _sysFiles.FileType = this.FileType;

            fileName = postedFile.FileName.Replace("#", "_").Replace(" ", "");
            postedFile.SaveAs(Server.MapPath(CommonFilePath.USER_FILES) + fileName);

            _sysFiles.FilePath = fileName;
            _sysFiles.Insert(CurrentUser.UserID, 4);
            _msgList.Add(string.Format(@"<li style='color:green;'>File <b>""{0}""</b> uploaded successfully!</li>", fileName));
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.FILE_ALREAY_EXISTS)
            {
                _msgList.Add(string.Format(@"<li style='color:red;'>File <b>""{0}""</b> already exists!</li>", fileName));
            }
            else if (ex.Message == CustomExceptionCodes.MAX_FILE_COUNT_EXCEEDED)
            {
                _msgList.Add("<li style='color:red;'>Yau have reached at maximum file upload limit!</li>");
            }
            else
            {
                _msgList.Add("<li style='color:red;'>Unknown error occured!</li>");
            }
        }        
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {      
        if (fuFile1.HasFile)
        {
            SaveFile(fuFile1.PostedFile);            
        }
        else
        {
            _msgList.Add("<li style='color:red;'>Please select valid file!</li>");
        }

        custom_message.InnerHtml = "<ul style='list-style:none; margin:0px; padding:0px;'>";
        foreach (var item in _msgList)
        {
            custom_message.InnerHtml += item;
        }
        custom_message.InnerHtml += "</ul>";        
    }

    protected void grdFiles_RowDeleting(object sender, JQGridRowDeleteEventArgs e)
    {        
        e.Cancel = true;
        _sysFiles.Delete(BusinessUtility.GetInt(e.RowKey));
        sdsFiles.SelectCommand = _sysFiles.GetSql(sdsFiles.SelectParameters, this.TableRef, this.RefID);
        sdsFiles.DataBind();
    }
}
Imports Resources.Resource
Imports System.Threading
Imports System.Globalization

Partial Class Common_ViewInvoice
    Inherits Page
    Private strSOID As String = ""
    Private objIN As New clsInvoices
    Private strInvID As String = ""
    Protected TaxString As String
    Private objSO As New clsInvoices
    Private objSOItems As New clsInvoiceItems
    Private objSOIP As New clsInvItemProcess
    Private objCust As New clsExtUser
    Private objOrderType As New clsOrderType
    Private objStatus As New clsSysStatus
    ' On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If
        divViewInvoice.Style("display") = "none"
        lblTitle.Text = lblViewInvoice '"View Invoice"
        If Not Page.IsPostBack Then
            objStatus.subGetStatus(dlSearch, "IN", "dlSh1", "IN")
            objStatus.subGetStatus(dlPOStatus, "IN", "dlSh2")
            subFillGrid()
            txtSearch.Focus()
            subPopulateCurrency()
            subPopulateWarehouse()
        End If

        clsGrid.MaintainParams(Page, grdOrder, SearchPanel, lblMsg, imgSearch)

        clsCSV.ExportCSV(Page, "Inv", grdOrder, New Integer() {10, 11}, imgCsv)

        txtSearch.Focus()
    End Sub
    'Populate Grid
    Public Sub subFillGrid()
        sqldsOrder.SelectCommand = objIN.funFillGridForAllOrders(dlSearch.SelectedValue, txtSearch.Text, dlPOStatus.SelectedValue, dlRefType.SelectedValue, Request.QueryString("PartnerID"))
    End Sub
    'On Page Index Change
    Protected Sub grdOrder_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdOrder.PageIndexChanging
        subFillGrid()
    End Sub
    'On Click to link View popup.
    Protected Sub grdOrder_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdOrder.RowCommand
        If e.CommandName = "InvDetail" Then
            Response.Redirect("~/Common/ViewInvoiceDetails.aspx?SOID=" & e.CommandArgument & "&Col=" & Request.QueryString("Col") & "&PartnerID=" & Request.QueryString("PartnerID"))
        Else
            divViewInvoice.Style("display") = "none"
        End If
    End Sub
    'On Row Data Bound
    Protected Sub grdOrder_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdOrder.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & INConfirmDelete & " ')")
            Dim txtCur As TextBox = CType(e.Row.FindControl("txtCur"), TextBox)
            Dim txtInvID As TextBox = CType(e.Row.FindControl("txtInvID"), TextBox)
            Dim txtwhs As TextBox = CType(e.Row.FindControl("txtwhs"), TextBox)
            Dim txtInvRefNo As TextBox = CType(e.Row.FindControl("txtInvRefNo"), TextBox)

            Dim lnkInvDetail As LinkButton = CType(e.Row.FindControl("lnkInvDetail"), LinkButton)
            lnkInvDetail.Text = txtInvRefNo.Text & " (" & txtwhs.Text & ")"
            e.Row.Cells(4).Text = txtCur.Text & " <b>" & String.Format("{0:F}", funCalcTax(txtInvID.Text)) & "</b>"
            'objSOIP = Nothing
            Dim strData As String = ""
            Select Case e.Row.Cells(6).Text
                Case "S"
                    strData = liINSubmitted
                Case "R"
                    strData = liINReSubmitted
                Case "P"
                    strData = liINPaymentReceived
            End Select
            e.Row.Cells(6).Text = strData
            Select Case e.Row.Cells(8).Text
                Case "IV"
                    e.Row.Cells(8).Text = liINInvoice
                Case "CN"
                    e.Row.Cells(8).Text = liINCreditNote
            End Select
        End If
    End Sub
    ' Calculate Tax
    Private Function funCalcTax(ByVal SOID As String) As Double
        Dim objSOItems As New clsInvoiceItems
        Dim drSOItems As Data.Odbc.OdbcDataReader
        Dim objPrn As New clsPrint
        drSOItems = objSOItems.GetDataReader(objSOItems.funFillGrid(SOID))
        Dim dblSubTotal As Double = 0
        Dim dblTax As Double = 0
        Dim dblProcessTotal As Double = 0
        Dim dblTotal As Double = 0
        Dim strTaxArray(,) As String
        Dim dblAmount As Double = 0
        Dim dblDiscountApplied As Double = 0
        Dim strWhs As String
        While drSOItems.Read
            Dim strTax As ArrayList
            Dim strTaxItem As String
            strTax = objPrn.funCalcTax(drSOItems("invProductID"), drSOItems("invShpWhsCode"), drSOItems("amount"), "", drSOItems("invCustID"), drSOItems("invCustType"), drSOItems("invProductTaxGrp").ToString)
            Dim intI As Integer = 0
            strWhs = drSOItems("invShpWhsCode")
            While strTax.Count - 1 >= intI
                strTaxItem = strTax.Item(intI).ToString
                strTaxArray = objPrn.funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                intI += 1
            End While
            dblSubTotal = dblSubTotal + CDbl(drSOItems("amount"))
        End While
        Dim i As Integer = 0
        Dim j As Integer = 0

        Dim objSOIP As New clsInvItemProcess
        objSOIP.Invoices_invID = SOID
        dblProcessTotal = CDbl(objSOIP.getProcessCostForInvoiceNo())
        strTaxArray = objPrn.funTotalProcessTax(dblProcessTotal, strWhs, strTaxArray)

        If Not strTaxArray Is Nothing Then
            j = strTaxArray.Length / 2
        End If
        While i < j
            dblTax += strTaxArray(1, i)
            i += 1
        End While
        dblTotal = dblTotal + dblSubTotal + dblProcessTotal + dblTax
        drSOItems.Close()
        objSOItems.CloseDatabaseConnection()
        objSOIP.CloseDatabaseConnection()
        objSOIP = Nothing
        objPrn = Nothing
        Return dblTotal
    End Function
    'Deleting
    Protected Sub grdOrder_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdOrder.RowDeleting
        strSOID = grdOrder.DataKeys(e.RowIndex).Value.ToString()
        lblMsg.Text = InvoiceDeletedSuccessfully '"Invoice Deleted Successfully." 'ODeleteSucess
        objIN.InvID = strSOID
        objIN.funDeleteInvoiceItemProcess() 'Delete Invoice Item Process
        objIN.funDeleteInvoiceItems() 'Delete Invoice Items
        sqldsOrder.DeleteCommand = objIN.funDeleteInvoice() 'Delete Invoice
        subFillGrid()
    End Sub
    'Editing
    Protected Sub grdOrder_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdOrder.RowEditing
        strSOID = grdOrder.DataKeys(e.NewEditIndex).Value.ToString()
        Response.Redirect("~/Common/ViewInvoiceDetails.aspx?SOID=" & strSOID & "&Col=" & Request.QueryString("Col") & "&PartnerID=" & Request.QueryString("PartnerID"))
    End Sub
    'Sorting
    Protected Sub grdOrder_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdOrder.Sorting
        subFillGrid()
    End Sub
    ' Sql Data Source Event Track
    Protected Sub sqldsOrder_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsOrder.Selected
        If e.AffectedRows = 0 And lblMsg.Text = "" Then
            lblMsg.Text = NoDataFound
        End If
    End Sub
    ' Search Sub Routine
    Private Sub subSearch()
        subFillGrid()
        lblMsg.Text = ""
    End Sub
    ' On Search Image Click
    Protected Sub imgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
        subSearch()
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    ' On Text Change
    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        subSearch()
    End Sub
    'Populate Grid for Invoice Details
    Public Sub subFillInvoiceDetailGrid()
        sqldsOrdItems.SelectCommand = objSOItems.funFillGrid(strInvID)  'fill Invoice Items
        objSOIP.Invoices_invID = strInvID
        objSOItems.Invoices_invID = strInvID
        sqldsAddProcLst.SelectCommand = objSOIP.funFillGridForProcess 'fill Order Items Process
    End Sub
    'Get Address Listing
    Protected Function funAddressListing(ByVal strID As String)
        Dim strData As String = ""
        If strID <> "" Then
            Dim objWhs As New clsWarehouses
            objWhs.WarehouseCode = strID
            objWhs.getWarehouseInfo()
            strData = "<table align='left' width=100% cellspacing='0' cellpadding='0' border='0' style='border:0px'>"
            If objWhs.WarehouseCode <> "" Then
                'strData += "<tr><td align='left'>" + objWhs.WarehouseCode + "</td></tr>"
            End If
            If objWhs.WarehouseDescription <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseDescription + "</td></tr>"
            End If
            If objWhs.WarehouseAddressLine1 <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseAddressLine1 + "</td></tr>"
            End If
            If objWhs.WarehouseAddressLine2 <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseAddressLine2 + "</td></tr>"
            End If
            If objWhs.WarehouseCity <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseCity + "</td></tr>"
            End If
            If objWhs.WarehouseState <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseState + "</td></tr>"
            End If
            If objWhs.WarehouseCountry <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseCountry + "</td></tr>"
            End If
            If objWhs.WarehousePostalCode <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehousePostalCode + "</td></tr>"
            End If
            strData += "</table>"
            objWhs = Nothing
        End If
        Return strData
    End Function
    'Populate invoice details when click on Invoice Id.
    Protected Sub fillPopup(ByVal strINID As String)
        strInvID = strINID
        subFillInvoiceDetailGrid()
        objSO.InvID = strInvID
        objSO.getInvoicesInfo()
        Dim objOrd As New clsOrders
        hdnSaleAgentID.Value = objOrd.funGetOrdSalesRepID(objSO.InvForOrderNo)
        hdnOrderID.Value = objSO.InvCommission

        objOrd.OrdID = objSO.InvForOrderNo
        objOrd.getOrdersInfo()

        subPopulateTerms()
        lblSODate.Text = IIf(objSO.InvCreatedOn <> "", CDate(objSO.InvCreatedOn).ToString("MMM-dd yyyy"), "--")
        lblInvID.Text = objSO.InvRefNo
        lblSOID.Text = objSO.InvForOrderNo
        objCust.CustID = objSO.InvCustID
        objCust.CustType = objSO.InvCustType
        objCust.getCustomerInfoByType()
        txtNetTerms.Text = objOrd.ordNetTerms  'objCust.CustNetTerms
        dlCurrencyCode.SelectedValue = objSO.InvCurrencyCode
        txtExRate.Text = objSO.InvCurrencyExRate
        txtBillToAddress.Text = funPopulateAddress(objCust.CustType, "B", objCust.CustID).Replace("<br>", vbCrLf)
        txtShpToAddress.Text = funPopulateAddress(objCust.CustType, "S", objCust.CustID).Replace("<br>", vbCrLf)
        lblAddress.Text = funPopulateAddress(objCust.CustType, "S", objCust.CustID)
        lblCustomerName.Text = objCust.CustName
        'lblTerms.Text = objCust.CustNetTerms
        lblFax.Text = objCust.CustFax
        txtNotes.Text = objSO.InvComment
        If objSO.InvStatus = "P" Or objSO.InvStatus = "S" Or objSO.InvStatus = "R" Then
            dlStatus.SelectedValue = objSO.InvStatus
        Else
            dlStatus.SelectedValue = ""
        End If
        txtShpCost.Text = objSO.InvShpCost
        'txtShpCode.Text = objSO.OrdShpCode
        dlCurrencyCode.SelectedValue = objSO.InvCurrencyCode
        txtCustPO.Text = objSO.InvCustPO
        dlShpWarehouse.SelectedValue = objSO.InvShpWhsCode
        subCalcTax()
    End Sub
    'Populate Terms
    Private Sub subPopulateTerms()
        Dim objComp As New clsCompanyInfo
        objComp.CompanyID = objSO.InvCompanyID
        objComp.getCompanyInfo()
        txtTerms.Text = objComp.CompanyShpToTerms
        lblInvComNameTitle.Text = objComp.CompanyName
        Dim objWhs As New clsWarehouses
        objWhs.WarehouseCompanyID = objComp.CompanyID
        lblInvWhsName.Text = objWhs.funGetWhsName
        objComp = Nothing
        objWhs = Nothing
    End Sub
    'Populate Address
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)
        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + "<br>"
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + "<br>"
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + "<br>"
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + "<br>"
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + "<br>"
            Else
                strAddress += objAdr.AddressState + "<br>"
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + "<br>"
            Else
                strAddress += objAdr.AddressCountry + "<br>"
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += lblPostalCode & " " + objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function
    'Populate Currency
    Private Sub subPopulateCurrency()
        Dim objCur As New clsCurrencies
        objCur.PopulateCurrency(dlCurrencyCode)
        objCur = Nothing
    End Sub
    'Populate Warehouse
    Private Sub subPopulateWarehouse()
        Dim objWhs As New clsWarehouses
        objWhs.PopulateWarehouse(dlShpWarehouse)
        objWhs = Nothing
    End Sub
    ' Calculate Tax
    Private Sub subCalcTax()
        lblSubTotal.Text = "0"
        Dim drSOItems As Data.Odbc.OdbcDataReader
        Dim objPrn As New clsPrint
        Dim objCur As New clsCurrencies
        drSOItems = objSOItems.GetDataReader(objSOItems.funFillGrid(strInvID))
        Dim dblSubTotal As Double = 0
        Dim dblTax As Double = 0
        Dim dblProcessTotal As Double = 0
        Dim dblTotal As Double = 0
        Dim strTaxArray(,) As String
        Dim dblAmount As Double = 0
        Dim dblDiscountApplied As Double = 0
        While drSOItems.Read
            Dim strTax As ArrayList
            Dim strTaxItem As String
            strTax = objPrn.funCalcTax(drSOItems("invProductID"), drSOItems("invShpWhsCode"), drSOItems("amount"), "", drSOItems("invCustID"), drSOItems("invCustType"), drSOItems("invProductTaxGrp").ToString)
            Dim intI As Integer = 0
            While strTax.Count - 1 >= intI
                strTaxItem = strTax.Item(intI).ToString
                strTaxArray = objPrn.funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                intI += 1
            End While
            dblSubTotal = CDbl(dblSubTotal) + CDbl(drSOItems("amount"))
            lblSubTotal.Text = String.Format("{0:F}", dblSubTotal) 'dblSubTotal.ToString
        End While
        Dim i As Integer = 0
        Dim j As Integer = 0
        If Not strTaxArray Is Nothing Then
            j = strTaxArray.Length / 2
        End If
        TaxString = ""
        While i < j
            TaxString &= "<tr><td align=left><b>" & strTaxArray(0, i) & ":</b></td><td align=right><b>" & dlCurrencyCode.SelectedValue & " " & String.Format("{0:F}", strTaxArray(1, i)) & "</b></td></tr>"
            dblTax += strTaxArray(1, i)
            i += 1
        End While
        objSOItems.Invoices_invID = strSOID
        dblProcessTotal = CDbl(objSOIP.getProcessCostForInvoiceNo())
        dblTotal = dblTotal + dblSubTotal + dblProcessTotal + dblTax
        lblAmount.Text = String.Format("{0:F}", dblTotal)
        Dim objUser As New clsUser
        Dim strCommission As String = objOrderType.funOderCommission(hdnOrderID.Value, hdnSaleAgentID.Value, lblSubTotal.Text)
        If strCommission = 0 Then
            trCommission.Visible = False
        End If
        lblCommAgentName.Text = objUser.funUserName(hdnSaleAgentID.Value)
        lblCommission.Text = dlCurrencyCode.SelectedValue & " " & strCommission
        lblSubTotal.Text = dlCurrencyCode.SelectedValue & " " & lblSubTotal.Text
        lblAmount.Text = dlCurrencyCode.SelectedValue & " " & lblAmount.Text & " " & objCur.funBaseCurConversion(objSO.InvCurrencyExRate, dblTotal)
        drSOItems.Close()
        objSOItems.CloseDatabaseConnection()
        objPrn = Nothing
    End Sub

    Protected Sub cmdBack_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.ServerClick
        If Request.QueryString("returnUrl") <> "" Then
            Response.Redirect(Request.QueryString("returnUrl"))
        End If
        If Request.QueryString("Col") <> "" Then
            Response.Redirect("~/CollectionAgent/ViewFollowuplog.aspx?SOID=" & Request.QueryString("Col"))
        Else
            Response.Redirect("~/Partner/AddEditCustomer.aspx?PartnerID=" & Request.QueryString("PartnerID"))
        End If
    End Sub
End Class

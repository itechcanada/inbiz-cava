﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Common_mdAddressEdit : BasePage
{
    Addresses _addr = new Addresses();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            _addr.PopulateObject(this.AddressSourceID, this.AddressRef, this.AddressType);

            txtAddressLine1.Text = _addr.AddressLine1;
            txtAddressLine2.Text = _addr.AddressLine2;
            txtAddressLine3.Text = _addr.AddressLine3;
            txtCity.Text = _addr.AddressCity;
            txtCountry.Text = _addr.AddressCountry;
            txtPostalCode.Text = _addr.AddressPostalCode;
            txtState.Text = _addr.AddressState;
            hdnAddressID.Value = _addr.AddressID.ToString();
        }
    }

    public int AddressID
    {
        get
        {
            return BusinessUtility.GetInt(hdnAddressID.Value);
        }        
    }

    public string AddressRef
    {
        get {
            return BusinessUtility.GetString(Request.QueryString["addrRef"]);
        }        
    }

    public int AddressSourceID
    {
        get
        {
            int pk = 0;
            int.TryParse(Request.QueryString["addrSourceID"], out pk);
            return pk;
        }       
    }

    public string AddressType
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["addrType"]);
        }     
    }

    public string TargetDataControlID
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["ctrl"]))
            {
                return Request.QueryString["ctrl"];
            }
            return "";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid) {
            _addr.PopulateObject(this.AddressSourceID, this.AddressRef, this.AddressType);
            _addr.AddressRef = this.AddressRef;
            _addr.AddressSourceID = this.AddressSourceID;
            _addr.AddressType = this.AddressType;
            _addr.AddressLine1 = txtAddressLine1.Text;
            _addr.AddressLine2 = txtAddressLine2.Text;
            _addr.AddressLine3 = txtAddressLine3.Text;
            _addr.AddressCity = txtCity.Text;
            _addr.AddressCountry = txtCountry.Text;
            _addr.AddressPostalCode = txtPostalCode.Text;
            _addr.AddressState = txtState.Text;

            if (this.AddressID > 0)
            {                
                _addr.Save();
            }
            else
            {
                _addr.Save();
            }

            txtData.Text = _addr.ToString();

            Globals.RegisterCloseDialogScript(this, string.Format(@"parent.$(""#{0}"").html(""{1}"")", this.TargetDataControlID, _addr.ToHtml()), true);
        }
    }
}
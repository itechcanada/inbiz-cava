﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Data;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Common_Print : System.Web.UI.Page
{
    private Control _contentControl;



    protected void Page_Load(object sender, EventArgs e)
    {       
        //if (!CurrentUser.IsAutheticated)
        //{
        //    return;
        //}

        PrintScript1.Visible = !Request.IsLocal;

        string htmlSnnipt = @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">";
        htmlSnnipt += @"<html xmlns=""http://www.w3.org/1999/xhtml""><head><title>Print</title></head><body><div style='font-family: Tahoma Verdana Arial; width: 750px; font-size: 12px'>#DATA_TO_PRINT#</div></body></html>";
        
        
        int reqID = 0;
        int.TryParse(Request.QueryString["ReqID"], out reqID);

        switch (this.DocType.ToUpper().Trim())
        {
            case "QO"://To Print Quotation
                ltPrint.Text = CommonPrint.GetOrderPrintHtmlSnipt(this, "QO", reqID);
                break;
            case "SO": //To Print SO
                ltPrint.Text = CommonPrint.GetOrderPrintHtmlSnipt(this, "SO", reqID);
                break;
            case "IN"://To Print Invoice
                ltPrint.Text = CommonPrint.GetInvoicePrintHtmlSnipt(this, reqID);
                break;
            case "PO": //To Print PO
                ltPrint.Text = CommonPrint.GetPOPrintHtmlSnipt(this, reqID);
                break;
            case "ASM": //To Print Assembly List
                break;
            case "PKG": //To Print Package List
                ltPrint.Text = CommonPrint.GetPackagePrintHtmlSnipt(this, reqID);
                break;
            case "TKT": //To Print Package List
                ltPrint.Text = CommonPrint.GetTicketPrintHtmlSnipt(this, this.OrderIDs,Globals.CurrentAppLanguageCode);
                break;
            case "RTNRCPT": //To Print Package List
                ltPrint.Text = CommonPrint.GetReturnReceiptHtmlSnipt(Globals.CurrentAppLanguageCode, reqID, this.AccRcblID);
                break;
                
        } 
    }

    public int RequestID
    {
        get
        {
            int reqID = 0;
            int.TryParse(Request.QueryString["ReqID"], out reqID);
            return reqID;
        }       
    }

    public string DocType
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["DocTyp"]);
        }        
    }

    private void PrintQO()
    { 
        
    }

    protected string OrderIDs
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["ReqIDs"]);
        }
    }
    protected int AccRcblID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["AccRcbID"]);
        }       
    }
    
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hlLogin.NavigateUrl = Request.ApplicationPath;
    }
    protected void btnRedirect_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/AdminLogin.aspx");
    }
}
Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Partial Class CollectionAgent_InvoiceFollowup
    Inherits BasePage
    Protected TaxString As String
    Private strSOID As String = ""
    Private objSO As New clsInvoices
    Private objSOItems As New clsInvoiceItems
    Private objSOIP As New clsInvItemProcess
    Private objCust As New clsExtUser
    Private objActRcv As New clsAccountReceivable
    Private objCL As New clsCollectionLog

    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If
        lblTitle.Text = TitleAddLog
        If Convert.ToString(Request.QueryString("SOID")) <> "" Then
            strSOID = Convert.ToString(Request.QueryString("SOID"))
        End If
        If Not Page.IsPostBack Then
            txtDate.Text = Now.ToString("MM/dd/yyyy")
            If Convert.ToString(Request.QueryString("SOID")) <> "" Then
                strSOID = Convert.ToString(Request.QueryString("SOID"))
                objSO.InvID = strSOID
                objSO.getInvoicesInfo()
                lblSODate.Text = IIf(objSO.InvCreatedOn <> "", CDate(objSO.InvCreatedOn).ToString("MMM-dd yyyy"), "--")
                lblInvID.Text = objSO.InvRefNo 'objSO.InvID
                lblSOID.Text = objSO.InvForOrderNo
                objCust.CustID = objSO.InvCustID
                objCust.CustType = objSO.InvCustType
                objCust.getCustomerInfoByType()
                lblCustomerName.Text = objCust.CustName
                If objCust.CustPhone <> "" Then
                    lblPhone.Text = objCust.CustPhone
                Else
                    lblPhone.Text = "--"
                End If
                If objCust.CustFax <> "" Then
                    lblFax.Text = objCust.CustFax
                Else
                    lblFax.Text = "--"
                End If
                If objCust.CustEmail <> "" Then
                    lblEmail.Text = objCust.CustEmail
                Else
                    lblEmail.Text = "--"
                End If
                lblContactName.Text = "--"
                lblContactPhone.Text = "--"
                lblContactFax.Text = "--"
                lblContactEmail.Text = "--"

                lblAddress.Text = funPopulateAddress(objCust.CustType, "S", objCust.CustID)

                'lblInvoiceAmount.Text = String.Format("{0:F}", funCalcTax(strSOID))
                'lblBalanceAmount.Text = String.Format("{0:F}", CDbl(objActRcv.getAmountForInvoiceNo(strSOID)))
                'txtARAmtRcvd.Text = String.Format("{0:F}", CDbl(lblInvoiceAmount.Text) - CDbl(lblBalanceAmount.Text))
                'Dim sInvAmount As Double = funCalcTax(strSOID)
                'Dim sBalAmt As String = objActRcv.getAmountForInvoiceNo(strSOID)
                'Dim sWriteOffAmt As String = objActRcv.getWriteOffForInvoiceNo(strSOID)

                'lblInvoiceAmount.Text = String.Format("{0:F}", sInvAmount)
                'sBalAmt = CDbl(sBalAmt) - CDbl(sWriteOffAmt)
                'lblBalanceAmount.Text = String.Format("{0:F}", sBalAmt)
                'txtARAmtRcvd.Text = String.Format("{0:F}", CDbl(lblInvoiceAmount.Text) - CDbl(lblBalanceAmount.Text))
                Dim sCurCode As String = objSO.InvCurrencyCode
                Dim sInvAmount As Double = funCalcTax(strSOID)
                Dim sAmtRcvd As Double = objActRcv.getAmountForInvoiceNo(strSOID)
                Dim sWriteOffAmt As Double = objActRcv.getWriteOffForInvoiceNo(strSOID)
                Dim sBalAmt As Double = 0

                lblInvoiceAmount.Text = sCurCode & " " & String.Format("{0:F}", sInvAmount)
                lblAmountReceived.Text = sCurCode & " " & String.Format("{0:F}", sAmtRcvd)
                lblWriteOff.Text = sCurCode & " " & String.Format("{0:F}", sWriteOffAmt)

                sBalAmt = sInvAmount - sAmtRcvd - sWriteOffAmt
                lblBalanceAmount.Text = sCurCode & " " & String.Format("{0:F}", sBalAmt)
            End If
        Else
            If Convert.ToString(Request.QueryString("SOID")) <> "" And strSOID <> "" Then

            End If
        End If
    End Sub
    'Populate Address
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)
        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + "<br>"
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + "<br>"
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + "<br>"
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + "<br>"
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + "<br>"
            Else
                strAddress += objAdr.AddressState + "<br>"
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + "<br>"
            Else
                strAddress += objAdr.AddressCountry + "<br>"
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += lblPostalCode & " " + objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    'Populate Object
    Private Sub subSetData()
        objCL.ColHeaderID = strSOID 'lblInvID.Text
        objCL.ColFollowup = DateTime.ParseExact(txtFollowupDateTime.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd") & Now.ToString(" HH:mm:ss")
        objCL.ColFollowupUserID = Session("UserID")
        objCL.ColLogText = txtNote.Text
        objCL.ColActivityDatetime = Now.ToString("yyyy-MM-dd HH:mm:ss")
    End Sub
    'On Submit
    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        If Convert.ToString(Request.QueryString("SOID")) <> "" Then
            subSetData()
            If objCL.insertCollectionLog() = True Then
                GenerateAlert()
            End If
            Response.Redirect("ViewFollowUp.aspx")
        End If
    End Sub
    ' On Back
    Protected Sub cmdBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReset.Click
        Response.Redirect("ViewFollowUp.aspx")
    End Sub
    'Get Address Listing
    Protected Function funAddressListing(ByVal strID As String)
        Dim strData As String = ""
        If strID <> "" Then
            Dim objWhs As New clsWarehouses
            objWhs.WarehouseCode = strID
            objWhs.getWarehouseInfo()
            strData = "<table align='left' width=100% cellspacing='0' cellpadding='0' border='0' style='border:0px'>"
            If objWhs.WarehouseCode <> "" Then
                'strData += "<tr><td align='left'>" + objWhs.WarehouseCode + "</td></tr>"
            End If
            If objWhs.WarehouseDescription <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseDescription + "</td></tr>"
            End If
            If objWhs.WarehouseAddressLine1 <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseAddressLine1 + "</td></tr>"
            End If
            If objWhs.WarehouseAddressLine2 <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseAddressLine2 + "</td></tr>"
            End If
            If objWhs.WarehouseCity <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseCity + "</td></tr>"
            End If
            If objWhs.WarehouseState <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseState + "</td></tr>"
            End If
            If objWhs.WarehouseCountry <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehouseCountry + "</td></tr>"
            End If
            If objWhs.WarehousePostalCode <> "" Then
                strData += "<tr><td align='left'>" + objWhs.WarehousePostalCode + "</td></tr>"
            End If
            strData += "</table>"
            objWhs = Nothing
        End If
        Return strData
    End Function

    ' Calculate Tax
    Private Function funCalcTax(ByVal SOID As String) As Double
        Dim objSOItems As New clsInvoiceItems
        Dim drSOItems As Data.Odbc.OdbcDataReader
        Dim objPrn As New clsPrint
        drSOItems = objSOItems.GetDataReader(objSOItems.funFillGrid(SOID))
        Dim dblSubTotal As Double = 0
        Dim dblTax As Double = 0
        Dim dblProcessTotal As Double = 0
        Dim dblTotal As Double = 0
        Dim strTaxArray(,) As String
        Dim dblAmount As Double = 0
        Dim dblDiscountApplied As Double = 0
        Dim strWhs As String
        While drSOItems.Read
            Dim strTax As ArrayList
            Dim strTaxItem As String
            strTax = objPrn.funCalcTax(drSOItems("invProductID"), drSOItems("invShpWhsCode"), drSOItems("amount"), "", drSOItems("invCustID"), drSOItems("invCustType"), drSOItems("invProductTaxGrp").ToString)
            Dim intI As Integer = 0
            strWhs = drSOItems("invShpWhsCode")
            While strTax.Count - 1 >= intI
                strTaxItem = strTax.Item(intI).ToString
                strTaxArray = objPrn.funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                intI += 1
            End While
            dblSubTotal = dblSubTotal + CDbl(drSOItems("amount"))
        End While
        Dim i As Integer = 0
        Dim j As Integer = 0

        Dim objSOIP As New clsInvItemProcess
        objSOIP.Invoices_invID = SOID
        dblProcessTotal = CDbl(objSOIP.getProcessCostForInvoiceNo())
        strTaxArray = objPrn.funTotalProcessTax(dblProcessTotal, strWhs, strTaxArray)

        If Not strTaxArray Is Nothing Then
            j = strTaxArray.Length / 2
        End If
        While i < j
            dblTax += strTaxArray(1, i)
            i += 1
        End While
        
        dblTotal = dblTotal + dblSubTotal + dblProcessTotal + dblTax
        drSOItems.Close()
        objSOItems.CloseDatabaseConnection()
        objSOIP.CloseDatabaseConnection()
        objSOIP = Nothing
        objPrn = Nothing
        Return dblTotal
    End Function

    'Set Data for Alert
    Private Sub subSetAlertData(ByRef objA As clsAlerts)
        objA.AlertPartnerContactID = objCL.ColHeaderID ' Invoice ID
        objA.AlertComID = Session("UserID") 'Followup userID
        objA.AlertDateTime = DateTime.ParseExact(txtFollowupDateTime.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
        objA.AlertUserID = Request.QueryString("AgentID")
        Dim strNote As String = ""
        strNote += alrtlblColFollowupActivity & "<br><br>" 'You have a Collection Followup Activity.
        strNote += alrtlblInvNo & " " & objCL.ColHeaderID & "<br>" 'Invoice No. 
        strNote += alrtlblCustmer & " " & lblCustomerName.Text & "<br>" 'Customer:
        Dim objUsr As New clsUser
        objUsr.UserID = Session("UserID")
        objUsr.getUserInfo()
        strNote += alrtlblCreatedBy & " " & objUsr.UserSalutation & " " & objUsr.UserFirstName & " " & objUsr.UserLastName 'Created by:
        objUsr = Nothing
        objA.AlertNote = strNote
        objA.AlertRefType = "COL"
    End Sub
    ' Inserts Alert for collection followup
    Protected Sub GenerateAlert()
        Dim objAlert As New clsAlerts
        subSetAlertData(objAlert)
        objAlert.insertAlerts()
    End Sub
End Class

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ViewFollowUpLog.aspx.cs" Inherits="CollectionAgent_ViewFollowUpLog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <link href="../lib/scripts/collapsible-panel/collapsible_salesedit.css" rel="stylesheet" type="text/css" />
    <script src="../lib/scripts/collapsible-panel/jquery.collapsiblepanel.js" type="text/javascript"></script>
    <style type="text/css">
        .dialog li{line-height:30px;}
        table.readonlye_info{margin-bottom:5px;}
        table.readonlye_info td{ padding:2px 5px;}
        table.readonlye_info td.bold{font-weight:bold;}                                      
    </style>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img alt="" src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <asp:Literal runat="server" ID="lblTitle"></asp:Literal>
        </b>
    </div>
    <div>
        
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphLeft" runat="Server">
     <ul id="custMenu" class="menu inbiz-left-menu collapsible">
        <li class="active"><a href="#">Action</a>
            <asp:BulletedList ID="blstNav" DisplayMode="HyperLink" DataTextField="Text"
                DataValueField="Value" runat="server" style="display:block;">
            </asp:BulletedList>
        </li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div class="panel">
        <h3>
            <asp:Label ID="lblTitle2" Text="<%$Resources:Resource,lblFollowupLogDetails%>" runat="server" />
        </h3>
        <div class="panelcontent">
            <table class="readonlye_info" border="0" cellpadding="0" cellspacing="0">
                <%--<tr>
                    <td class="bold"></td>
                    <td></td>
                    <td></td>
                    <td class="bold"></td>
                    <td></td>
                    <td></td>
                    <td class="bold"></td>
                    <td></td>                    
                </tr>--%>
                <tr>
                    <td class="bold">
                        <asp:Label ID="lblSODate1" Text="<%$ Resources:Resource, lblINInvoiceDate %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblSODate" CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblInvNumber" Text="<%$ Resources:Resource, lblINInvoiceNo %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblInvID" CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblSONumber" Text="<%$ Resources:Resource, lblSOOrderNo %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblSOID" CssClass="lblBold" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="bold">
                        <asp:Label ID="lblCustomerName1" Text="<%$ Resources:Resource, lblSOCustomerName %>"
                            CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblCustomerName" CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblAREmail" CssClass="lblBold" Text="<%$ Resources:Resource, lblEmail%>"
                            runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblEmail" runat="server" CssClass="lblBold"> </asp:Label>
                    </td>
                    <td>
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblARPhone" CssClass="lblBold" Text="<%$ Resources:Resource, lblPhone%>"
                            runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblPhone" runat="server" CssClass="lblBold"> </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="bold" valign="top">
                        
                        <asp:Label ID="Label1" Text="<%$ Resources:Resource, POAddress %>" CssClass="lblBold"
                            runat="server" />
                        
                    </td>
                    <td valign="top">
                        <asp:Label ID="lblAddress" CssClass="lblBold" runat="server" />
                    </td>
                    <td valign="top">
                    </td>
                    <td class="bold" valign="top">
                        <asp:Label ID="lblARFax" Text="<%$ Resources:Resource, lblFax %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td valign="top">
                        <asp:Label ID="lblFax" CssClass="lblBold" runat="server" />
                    </td>
                    <td valign="top">
                    </td>
                    <td class="bold" valign="top">
                        <asp:Label ID="lblARColAsigned" CssClass="lblBold" Text="<%$ Resources:Resource, lblARColAsigned%>"
                            runat="server" />
                    </td>
                    <td valign="top">
                        <asp:Label ID="lblAssignedTo" CssClass="lblBold" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="bold">
                        <asp:Label ID="lblARInvoiceAmount" Text="<%$ Resources:Resource, lblARInvoiceAmount %>"
                            CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblInvoiceAmount" CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblARPreviousReceived" Text="<%$ Resources:Resource, lblARPreviousReceived %>"
                            CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblPreviousReceived" CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblARPreviousWriteOff" Text="<%$ Resources:Resource, lblARPreviousWriteOff %>"
                            CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblPreviousWriteOff" CssClass="lblBold" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="bold">
                        <asp:Label ID="lblARBalanceAmount" Text="<%$ Resources:Resource, lblARBalanceAmount %>"
                            CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblBalanceAmount" CssClass="lblBold" runat="server" BackColor="#FFFF00" />
                    </td>
                    <td>
                    </td>
                    <td class="bold">                        
                        <asp:Label ID="Label4" Text="<%$ Resources:Resource, lblCAContactName %>"
                            CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valContactName" Text="" runat="server" />
                    </td>
                    <td>
                    </td>
                    <td class="bold">
                        <asp:Label ID="Label5" Text="<%$ Resources:Resource, lblCAContactPhone %>"
                            CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valContactPhone" Text="" CssClass="lblBold" runat="server" />
                    </td>
                </tr>
            </table>                        
            <div class="div_command" style="padding-right: 0px;">                
            </div>            
        </div>
    </div>
    <div class="panel">
        <h3>
            <asp:Label ID="Label2" Text="<%$Resources:Resource, TitleAddLog%>" runat="server" />
        </h3>
         <div class="panelcontent">
             <ul class="form">                 
                 <li>
                     <div class="lbl">
                         <asp:Label ID="Label3" CssClass="lblBold" Text="<%$ Resources:Resource, lblCAFollowupDateTime%>"
                             runat="server" />
                     </div>
                     <div class="input">
                         <table border="0" cellpadding="0" cellspacing="0">
                             <tr>
                                 <td style="padding-right: 2px;">
                                     <asp:TextBox ID="txtDate" runat="server" Width="75px" CssClass="datepicker" />
                                 </td>
                                 <td style="padding: 0 2px;">
                                     <asp:TextBox ID="txtHH" runat="server" Width="25px" CssClass="integerTextField" MaxLength="2" />
                                 </td>
                                 <td style="padding: 0 2px;">
                                     <asp:TextBox ID="txtMM" runat="server" Width="25px" CssClass="integerTextField" MaxLength="2" />
                                 </td>
                                 <td style="padding: 0 2px;">
                                     <asp:DropDownList runat="Server" ID="ddlTT" Width="45">
                                         <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                         <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                     </asp:DropDownList>
                                 </td>
                             </tr>
                             <tr>
                                 <td style="padding: 0 2px;">
                                     (mm/dd/yyyy)
                                 </td>
                                 <td style="padding: 0 2px;">
                                     (hh)
                                 </td>
                                 <td style="padding: 0 2px;">
                                     (mm)
                                 </td>
                                 <td>
                                 </td>
                             </tr>
                         </table>
                     </div>
                     <div class="clearBoth">
                     </div>
                 </li>
                 <li>
                    <div class="lbl">
                        <asp:Label ID="lblARNote" CssClass="lblBold" Text="<%$ Resources:Resource, lblARNote%>"
                                        runat="server" />
                    </div>
                    <div class="input" style="width:450px;">
                        <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine"
                                        Rows="7" Width="440px"> </asp:TextBox>
                    </div>
                     <div class="clearBoth">
                     </div>
                 </li>
             </ul>
             <div class="div_command">                
                 <asp:Button ID="btnAddLog" Text="<%$Resources:Resource, cmdCssAdd%>" 
                     runat="server" ValidationGroup="log" onclick="btnAddLog_Click" />
             </div>
         </div>
    </div>
    <div id="grid_wrapper" style="width: 100%;">
        <trirand:JQGrid runat="server" ID="grdColLog" DataSourceID="sdsColLog" Height="300px" AutoWidth="True"
            OnCellBinding="grdColLog_CellBinding" OnDataRequesting="grdColLog_DataRequesting">
            <Columns>
                <%--<trirand:JQGridColumn DataField="AccountReceivableID" HeaderText="S.No."
                        Editable="false" Width="10" />     --%>
                <trirand:JQGridColumn DataField="InvRefNo" HeaderText="<%$ Resources:Resource, grdINInvoiceNo %>"
                    Editable="false" Width="50" TextAlign="Center" />
                <trirand:JQGridColumn DataField="ColFollowup" HeaderText="<%$ Resources:Resource, grdCAFollowupDateTime %>"
                    Editable="false" TextAlign="Center" Width="80" />
                <trirand:JQGridColumn DataField="ColLogText" HeaderText="<%$ Resources:Resource, grdCAFollowupLogText %>"
                    Editable="false" TextAlign="Left" />
                <trirand:JQGridColumn DataField="ColActivityDatetime" HeaderText="Created On"
                    Editable="false" TextAlign="Center" Width="80" />
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <ClientSideEvents LoadComplete="resize_the_grid" />
        </trirand:JQGrid>
        <asp:SqlDataSource ID="sdsColLog" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
    </div>
    <asp:ValidationSummary ID="valsSales" runat="server" ShowMessageBox="true" ShowSummary="false" />
    <asp:HiddenField ID="hdnPartnerID" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdColLog.ClientID %>";

        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();
        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);
       
    </script>
    <script type="text/javascript" language="javascript">

        function funConfirm(id) {
            var frm = document.forms[0];
            for (i = 0; i < frm.elements.length; i++) {
                if (frm.elements[i].type == "checkbox") {
                    if (frm.elements[i].checked == false) {
                        alert('<% =Resources.Resource.POConfirmItems %>');
                        return false;
                    }
                }
            }
            return true;
        }

        function openPDF(strOpen) {
            open(strOpen, "", 'height=365,width=810,left=180,top=150,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');
            return false;
        }
        function funClosePopUp(strOption) {
            document.getElementById(strOption).style.display = "none";
        }
        function popUp(strOption) {
            document.getElementById(strOption).style.display = "block";
            return false;
        }
        function popAlert(strOption) {
            alert(strOption);
            return false;
        }
    </script>
</asp:Content>


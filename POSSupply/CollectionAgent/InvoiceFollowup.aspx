<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="InvoiceFollowup.aspx.vb" Inherits="CollectionAgent_InvoiceFollowup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="Resources.resource" %>
<%@ Import Namespace="AjaxControlToolkit" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
   <script language="javascript" type="text/javascript">
       $('#divSearch').corner();
       $('#divSearchContent').corner();
       $('#divMainContainerTitle').corner();        
    </script>
    <div id="divSearch" class="divSectionTitle">
        <h2>
            <%= Resources.Resource.lblSearchOptions%>
        </h2>
    </div>
    <div id="divSearchContent" class="divSectionContent">
    </div>
</asp:Content>

<asp:Content ID="cntInvoiceFollowup" ContentPlaceHolderID="cphMaster" runat="Server">
   <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <%-- onkeypress="return disableEnterKey(event)"--%>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                    <asp:UpdatePanel ID="upnlMsg" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td align="left" width="50%" valign="top">
                    <table width="100%" border="0" cellpadding="2" cellspacing="1">
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblSODate1" Text="<%$ Resources:Resource, lblINInvoiceDate %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblSODate" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblInvNumber" Text="<%$ Resources:Resource, lblINInvoiceNo %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblInvID" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblSONumber" Text="<%$ Resources:Resource, lblSOOrderNo %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblSOID" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblARInvoiceAmount" Text="<%$ Resources:Resource, lblARInvoiceAmount %>"
                                    CssClass="lblBold" runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblInvoiceAmount" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblARAmountReceived" CssClass="lblBold" Text="<%$ Resources:Resource, lblARAmountReceived %>"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblAmountReceived" CssClass="lblBold" Text="<%$ Resources:Resource, lblARBalanceAmount %>"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblARWriteOff" Text="<%$ Resources:Resource, lblARWriteOff %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblWriteOff" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblARBalanceAmount" Text="<%$ Resources:Resource, lblARBalanceAmount %>"
                                    CssClass="lblBold" runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblBalanceAmount" CssClass="lblBold" runat="server" BackColor="#FFFF00" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblCAFollowupDateTime" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblCAFollowupDateTime %>">
                                </asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtFollowupDateTime" Enabled="true" runat="server" Width="90px"
                                    MaxLength="20" onkeypress="return disableEnterKey(event)">
                                </asp:TextBox>
                                <asp:ImageButton ID="imgAmtRcvdDate" CausesValidation="false" runat="server" ToolTip="Click to show calendar"
                                    ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                                <%=lblPODateFormat %><span class="style1"> *</span>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" TargetControlID="txtFollowupDateTime"
                                    runat="server" PopupButtonID="imgAmtRcvdDate" Format="MM/dd/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:RequiredFieldValidator ID="reqvalAmtRcvdDateTime" runat="server" ControlToValidate="txtFollowupDateTime"
                                    ErrorMessage="<%$ Resources:Resource, msgCAPlzEntCAFollowupDate %>" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                                <div style="display: none;">
                                    <asp:TextBox ID="txtDate" Enabled="true" runat="server" Width="0px" MaxLength="15">
                                    </asp:TextBox>
                                </div>
                                <asp:CompareValidator ID="cmpvalAmtRcvdDateTime" EnableClientScript="true" SetFocusOnError="true"
                                    ControlToValidate="txtFollowupDateTime" ControlToCompare="txtDate" Operator="GreaterThanEqual"
                                    Type="Date" Display="None" ErrorMessage="<%$ Resources:Resource, msgCAPlzEntAmtCAFollowupDateGTECurDate %>"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="left" width="50%" valign="top">
                    <table width="100%" border="0" cellpadding="2" cellspacing="1">
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblCustomerName1" Text="<%$ Resources:Resource, lblSOCustomerName %>"
                                    CssClass="lblBold" runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblCustomerName" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="trHeight" id="trAddress" runat="server">
                            <td class="tdAlign" valign="top">
                                <asp:Label ID="Label1" Text="<%$ Resources:Resource, POAddress %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblAddress" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblARPhone" CssClass="lblBold" Text="<%$ Resources:Resource, lblPhone%>"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblPhone" runat="server" CssClass="lblBold">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblARFax" Text="<%$ Resources:Resource, lblFax %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblFax" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblAREmail" CssClass="lblBold" Text="<%$ Resources:Resource, lblEmail%>"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblEmail" runat="server" CssClass="lblBold">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblCAContactName" CssClass="lblBold" Text="<%$ Resources:Resource, lblCAContactName%>"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblContactName" runat="server" CssClass="lblBold">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblCAContactPhone" CssClass="lblBold" Text="<%$ Resources:Resource, lblCAContactPhone%>"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblContactPhone" runat="server" CssClass="lblBold">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblCAContactFax" Text="<%$ Resources:Resource, lblCAContactFax %>"
                                    CssClass="lblBold" runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblContactFax" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="trHeight">
                            <td class="tdAlign">
                                <asp:Label ID="lblCAContactEmail" CssClass="lblBold" Text="<%$ Resources:Resource, lblCAContactEmail%>"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblContactEmail" runat="server" CssClass="lblBold">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <asp:Label ID="lblARNote" CssClass="lblBold" Text="<%$ Resources:Resource, lblARNote%>"
                        runat="server" /><br />
                    <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="4" Width="750px">
                    </asp:TextBox>
                </td>
            </tr>
            <tr height="30">
                <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                    <asp:Button runat="server" ID="cmdSave" CssClass="imgSave" />&nbsp;&nbsp;
                    <asp:Button runat="server" ID="cmdReset" Style="width: 80px; height: 26px; border-width: 0px;
                        background: url(../Images/Back.png);" CausesValidation="false" />
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsFollowup" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
    <br />
    <br />
    <script type="text/javascript" language="javascript">

    </script>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Trirand.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class CollectionAgent_ViewFollowUpLog : BasePage
{
    Invoice _inv = new Invoice();
    Customer objCust = new Customer();
    AccountReceivable _arc = new AccountReceivable();
    int _currentRow = -1;
    int _dsIdxInvID = 0;
    TotalSummary _total;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdColLog))
        {         
            if (this.InvoiceID > 0)
            {
              

                _inv.PopulateObject(this.InvoiceID);
                lblSODate.Text = _inv.InvCreatedOn.ToString("MMM-dd yyyy"); //'IIf(objSO.InvCreatedOn <> "", CDate(objSO.InvCreatedOn).ToString("MMM-dd yyyy"), "--")
                lblInvID.Text = _inv.InvRefNo.ToString(); //'objSO.InvRefNo
                lblSOID.Text = _inv.InvForOrderNo.ToString();

                hdnPartnerID.Value = _inv.InvCustID.ToString();
                objCust.PopulateObject(_inv.InvCustID);

                Addresses addr = new Addresses();
                //addr.PopulateObject(_inv.InvCustID, _inv.InvCustType, AddressType.SHIP_TO_ADDRESS);
                addr.GetOrderAddress(_inv.InvForOrderNo, AddressType.SHIP_TO_ADDRESS);
                lblAddress.Text = addr.ToHtml();
                lblCustomerName.Text = objCust.CustomerName;
                lblPhone.Text = !string.IsNullOrEmpty(objCust.Phone) ? objCust.Phone : "--";
                lblFax.Text = !string.IsNullOrEmpty(objCust.Fax) ? objCust.Fax : "--";
                lblEmail.Text = !string.IsNullOrEmpty(objCust.Email) ? objCust.Email : "--";

                double writeOff = _arc.GetWriteOffAmount(_inv.InvID);

                _total = CalculationHelper.GetInvoiceTotal(_inv.InvID);
                lblInvoiceAmount.Text = string.Format("{0} {1:F}", _total.CurrencyCode, BusinessUtility.GetCurrencyString(_total.GrandTotal,Globals.CurrentCultureName));
                lblPreviousReceived.Text = string.Format("{0} {1:F}", _total.CurrencyCode, BusinessUtility.GetCurrencyString(_total.TotalAmountReceived,Globals.CurrentCultureName));
                lblPreviousWriteOff.Text = string.Format("{0} {1:F}", _total.CurrencyCode, BusinessUtility.GetCurrencyString(writeOff,Globals.CurrentCultureName));

                lblBalanceAmount.Text = string.Format("{0} {1:F}", _total.CurrencyCode, BusinessUtility.GetCurrencyString(_total.OutstandingAmount,Globals.CurrentCultureName));


                string assingTo = _arc.AssignTo(_inv.InvID);
                if (!string.IsNullOrEmpty(assingTo))
                {
                    lblAssignedTo.Text = string.Format("[ {0} ]", assingTo);
                }
                else
                {
                    lblAssignedTo.Text = "";
                }

                Partners part = new Partners();
                part.PopulateObject(_inv.InvCustID);
                valContactName.Text = part.ExtendedProperties.EmergencyContactName;
                valContactPhone.Text = part.ExtendedProperties.EmergencyTelephone;

                this.BindNavigation();
            }
        }
    }

    private void BindNavigation()
    {
        Partners part = new Partners();
        part.PopulateObject(BusinessUtility.GetInt(hdnPartnerID.Value));
        string partType = StatusCustomerTypes.DISTRIBUTER;
        switch (part.PartnerType)
        {
            case (int)PartnerTypeIDs.Distributer:
                partType = StatusCustomerTypes.DISTRIBUTER;
                break;
            case (int)PartnerTypeIDs.EndClient:
                partType = StatusCustomerTypes.END_CLINET;
                break;
            case (int)PartnerTypeIDs.Leads:
                partType = StatusCustomerTypes.LEADS;
                break;
            case (int)PartnerTypeIDs.Reseller:
                partType = StatusCustomerTypes.RESELLER;
                break;
        }
        string urlCustDetails = ResolveUrl("~/Partner/CustomerEdit.aspx");       
        blstNav.Items.Add(new ListItem(Resources.Resource.btnColCustomerDetail, string.Format("{0}?custid={1}&pType={2}&gType={3}&Col={4}", urlCustDetails, part.PartnerID, partType, (int)part.ExtendedProperties.GuestType,  Request.QueryString["SOID"])));
        blstNav.Items.Add(new ListItem(Resources.Resource.lblSOHistory, string.Format("~/Sales/ViewSalesOrder.aspx?PartnerID={0}&returnUrl={1}", part.PartnerID, Server.UrlEncode(Request.RawUrl))));
        blstNav.Items.Add(new ListItem(Resources.Resource.lblInvoiceHistory, string.Format("~/Invoice/ViewInvoice.aspx?PartnerID={0}&returnUrl={1}", part.PartnerID, Server.UrlEncode(Request.RawUrl))));
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR) || CurrentUser.IsInRole(RoleID.ACCOUNT_RECEIVABLE))
        {
            blstNav.Items.Add(new ListItem(Resources.Resource.lblReceivePayment, string.Format("~/AccountsReceivable/InvoicePayment.aspx?SOID={0}&returnUrl={1}", this.InvoiceID, Server.UrlEncode(Request.RawUrl))));
            blstNav.Items.Add(new ListItem(Resources.Resource.lblARHistory, string.Format("~/AccountsReceivable/InvoicePayment.aspx?SOID={0}&returnUrl={1}", this.InvoiceID, Server.UrlEncode(Request.RawUrl))));
        }        
        blstNav.Items.Add(new ListItem(Resources.Resource.lblCustomerNotes, string.Format("{0}?custid={1}&pType={2}&gType={3}&Col={4}&section={5}", urlCustDetails, part.PartnerID, partType, (int)part.ExtendedProperties.GuestType, Request.QueryString["SOID"], (int)CRMSectionKey.CustomerNotes)));
        blstNav.Items.Add(new ListItem(Resources.Resource.lblCustomerActivities, string.Format("{0}?custid={1}&pType={2}&gType={3}&Col={4}&section={5}", urlCustDetails, part.PartnerID, partType, (int)part.ExtendedProperties.GuestType, Request.QueryString["SOID"], (int)CRMSectionKey.CustomerActivity)));
    }

    protected void grdColLog_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (_currentRow != e.RowIndex)
        {
            //_total = CalculationHelper.GetInvoiceTotal(BusinessUtility.GetInt(e.RowValues[_dsIdxInvID]));
        }
        if (e.ColumnIndex == 0)
        {
            e.CellHtml = e.CellHtml.Replace(Environment.NewLine, "<br>");
            if (string.IsNullOrEmpty(e.CellHtml))
            {
                e.CellHtml = "--";
            }
        }
        else if (e.ColumnIndex == 2)
        {
            //e.CellHtml = string.Format("{0:F}", _total.GrandTotal);
        }
    }

    protected void grdColLog_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        sdsColLog.SelectCommand = new CollectionLog().GetAllCollectionLog(sdsColLog.SelectParameters, -1, this.InvoiceID);
    }

    private int InvoiceID
    {
        get
        {
            int invID = 0;
            int.TryParse(Request.QueryString["SOID"], out invID);
            return invID;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("~/Invoice/ViewInvoiceDetails.aspx?SOID={0}", this.InvoiceID));
    }

    protected void btnSaveAndGoToCal_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Reservation/ReservationCalendar.aspx");
    }

    protected void btnAddLog_Click(object sender, EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);

        try
        {
            int hh, mm;
            int.TryParse(txtHH.Text, out hh);
            int.TryParse(txtMM.Text, out mm);
            string strDate = string.Format("{0} {1:00}:{2:00} {3}", txtDate.Text, hh, mm, ddlTT.SelectedValue);

            CollectionLog colLog = new CollectionLog();
            colLog.ColActivityDatetime = DateTime.Now;            
            colLog.ColFollowupUserID = CurrentUser.UserID;
            colLog.ColHeaderID = this.InvoiceID;
            colLog.ColLogText = txtNote.Text;
            colLog.ColFollowup = BusinessUtility.GetDateTime(strDate, "MM/dd/yyyy hh:mm tt");

            colLog.Insert(dbHelp);
            AlertHelper.SetCollectionLogAlert(dbHelp, colLog.ColFollowup, this.InvoiceID, CurrentUser.UserID, CurrentUser.UserID);
            Response.Redirect(Request.RawUrl);
        }        
        finally
        { 

        }       
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ViewFollowUp.aspx.cs" Inherits="CollectionAgent_ViewFollowUp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <asp:Literal runat="server" ID="lblTitle" Text="<%$Resources:Resource,TitleAssignAgents %>"></asp:Literal>
        </b>
    </div>
    <div style="float: left;">
        
    </div>
    <div style="float: right;">       
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <asp:Panel ID="pnlGridWrapper" runat="server" Width="100%">                        
                <trirand:JQGrid runat="server" ID="grdCollection" DataSourceID="sdsCollection"
                    Height="300px" AutoWidth="True" oncellbinding="grdCollection_CellBinding" 
                    ondatarequesting="grdCollection_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="invID" HeaderText="<%$ Resources:Resource, grdINInvoiceNo %>"
                            PrimaryKey="True" />                       
                        <trirand:JQGridColumn DataField="CustomerName" HeaderText="<%$ Resources:Resource, grdSOCustomerName %>"
                            Editable="false" />                        
                        <trirand:JQGridColumn DataField="PartnerPhone" HeaderText="<%$ Resources:Resource, grdCMPhone %>"
                            Editable="false" />                        
                        <trirand:JQGridColumn DataField="invDate" HeaderText="<%$Resources:Resource,grdSOCreatedDate%>" Editable="false" />                        
                        <trirand:JQGridColumn DataField="NetTerms" HeaderText="<%$ Resources:Resource, grdARNetTerms %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="invStatus" HeaderText="<%$ Resources:Resource, grdPOStatus %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="AssignedTo" HeaderText="<%$ Resources:Resource, grdAssignedTo %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="InvAmount" HeaderText="<%$ Resources:Resource, grdARInvoiceAmount %>"
                            Editable="false" TextAlign="Right"  />  
                        <trirand:JQGridColumn DataField="BalanceAmount" HeaderText="<%$ Resources:Resource, grdARBalanceAmount %>"
                            Editable="false" TextAlign="Right" />                         
                        <trirand:JQGridColumn HeaderText="Log" DataField="invID"
                            Sortable="false" TextAlign="Center" Width="80" />
                    </Columns>                    
                    <ClientSideEvents LoadComplete="jqGridResize" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sdsCollection" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>            
            </asp:Panel>                       
            <br />
            <div class="div_command">                                
                <asp:Button ID="btnBack" Text="<%$Resources:Resource, lblGoBack%>" 
                    CausesValidation="false" runat="server" Visible="false" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntLeft" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" ID="pnlSearch">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        <asp:Literal ID="ltSearchFormTitle" Text="<%$Resources:Resource, lblSearchForm%>" runat="server" />
                     </div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lblCMSearchBy%>"
                        AssociatedControlID="ddlSearch" CssClass="filter-key"></asp:Label>
                </h4> 
                <div class="inner">
                    <asp:DropDownList ID="ddlSearch" runat="server">                       
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lblCoCustomerName%>"
                        AssociatedControlID="txtCustomerName" CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                   <asp:TextBox runat="server" Width="180px" ID="txtCustomerName"></asp:TextBox>                    
                </div>
                <h4>
                    <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lblCoPhoneNo %>"
                        AssociatedControlID="txtCustPhoneNo" CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtCustPhoneNo"></asp:TextBox>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtAssign" runat="server" Text="<%$ Resources:Resource, lblColAssignedTo %>"
                        CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtAssign"></asp:TextBox>
                </div>
                <h4>
                    <asp:Label ID="Label1" AssociatedControlID="txtInvoiceNo" runat="server" Text="<%$ Resources:Resource, lblCoInvoiceNo %>"
                        CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtInvoiceNo"></asp:TextBox>
                </div>
                <h4>
                    <asp:Label ID="Label2" AssociatedControlID="dlCreatedDate" runat="server" Text="<%$ Resources:Resource, lblFromDueDate %>"
                        CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:DropDownList ID="dlCreatedDate" runat="server" Width="175px">
                    </asp:DropDownList>
                </div>                
                <div class="footer">
                    <div class="submit">                        
                        <asp:Button ID="btnSearch" Text="<%$Resources:Resource, Search%>" runat="server" CausesValidation="false" />                        
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="cphScripts" runat="Server">        
    <script type="text/javascript">
        $grid = $("#<%=grdCollection.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=pnlSearch.ClientID %>",
            searchButtonID: "<%=btnSearch.ClientID %>",
            gridWrapPanleID: "<%=pnlGridWrapper.ClientID %>"
        });

        function jqGridResize() {
            $grid.jqResizeAfterLoad("<%=pnlGridWrapper.ClientID%>", 0);
        }

        function reloadAfterAssing() {
            $grid.trigger("reloadGrid");
        }

        function assignCollection() {
            var arrRooms = $grid.getGridParam('selarrrow');
            if (arrRooms.length <= 0) {
                alert("<%=Resources.Resource.msgJobSlectOneOrder%>");
                return false;
            }
            dataToPost = {}
            dataToPost.invid = arrRooms.join(",");
            dataToPost.jscallback = "reloadAfterAssing";
            var $dialog = jQuery.FrameDialog.create({ url: 'AssignAgent.aspx?' + $.param(dataToPost),
                title: "<%=Resources.Resource.TitleAssignAgents %>",
                loadingClass: "loading-image",
                modal: true,
                width: 550,
                height: 300,
                autoOpen: false
            });
            $dialog.dialog('open');

            return false;
        }
    </script>
</asp:Content>



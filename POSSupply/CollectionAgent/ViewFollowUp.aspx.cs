﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class CollectionAgent_ViewFollowUp : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UIHelper.SetJqGridDefaults(grdCollection);
        if (!CurrentUser.IsInRole(RoleID.COLLECTION_AGENT) && !CurrentUser.IsInRole(RoleID.COLLECTION_MANAGER) && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }
        if (CurrentUser.IsInRole(RoleID.COLLECTION_AGENT) && !CurrentUser.IsInRole(RoleID.COLLECTION_MANAGER) && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            ddlSearch.Items.Clear();
            ddlSearch.Items.Add(new ListItem(Resources.Resource.liColAssigned, "AS"));
            ddlSearch.Items[0].Selected = true;
        }
        else
        {
            ddlSearch.Items.Clear();
            ddlSearch.Items.Add(new ListItem(Resources.Resource.liColNotAssigned, "NA"));
            ddlSearch.Items.Add(new ListItem(Resources.Resource.liColAssigned, "AS"));
            ddlSearch.Items.Add(new ListItem(Resources.Resource.liColAll, "AL"));
            ddlSearch.Items[0].Selected = true;
        }
        if (!IsPagePostBack(grdCollection))
        {
            DropDownHelper.FillDropdown(dlCreatedDate, "AR", "dlSh2", Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.liARRcvdVia, ""));
        }
    }

    protected void grdCollection_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        InvoiceCollection col = new InvoiceCollection();
        string sBy = Request.QueryString[ddlSearch.ClientID] == null ? ddlSearch.SelectedValue : Request.QueryString[ddlSearch.ClientID];
        int dayRange = 0;
        int.TryParse(Request.QueryString[dlCreatedDate.ClientID], out dayRange);
        string custname = Request.QueryString[txtCustomerName.ClientID];
        string custPhone = Request.QueryString[txtCustPhoneNo.ClientID];
        int invNo = 0;
        int.TryParse(Request.QueryString[txtInvoiceNo.ClientID], out invNo);
        string assignTo = Request.QueryString[txtAssign.ClientID];
        sdsCollection.SelectCommand = col.GetAllInvoiceToBeFollowup(sdsCollection.SelectParameters, sBy, dayRange, custname, custPhone, invNo, assignTo, CurrentUser.UserID);
    }

    protected void grdCollection_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 0)
        {
            e.CellHtml = string.Format("{0} ({1})", e.RowValues[1], e.RowValues[2]);
        }
        else if (e.ColumnIndex == 5)
        {
            switch (e.CellHtml)
            {
                case InvoicesStatus.SUBMITTED:
                    e.CellHtml = Resources.Resource.liINSubmitted;
                    break;
                case InvoicesStatus.RE_SUBMITTED:
                    e.CellHtml = Resources.Resource.liINReSubmitted;
                    break;
                case InvoicesStatus.PAYMENT_RECEIVED:
                    e.CellHtml = Resources.Resource.liINPaymentReceived;
                    break;
                case InvoicesStatus.PARTIAL_PAYMENT_RECEIVED:
                    e.CellHtml = Resources.Resource.liINPartialPaymentReceived;
                    break;
            }
        }
        else if (e.ColumnIndex == 7)
        {
            e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
        }
        else if (e.ColumnIndex == 8)
        {
            e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
        }
        else if (e.ColumnIndex == 9)
        {
            e.CellHtml = string.Format("<a href='ViewFollowUpLog.aspx?SOID={0}'>Log</a>", e.RowKey);
        }

    }
}
<%@ Application Language="VB" %>
<%@ Import Namespace="iTECH.Library.DataAccess.MySql" %>
<%@ Import Namespace="MySql.Data.MySqlClient" %>
<script runat="server">        
    Public Shared MyKillTimer As New System.Timers.Timer()
    
    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
        Application("gblKey") = clsCommon.funRandomString()
        
        Try
            MyKillTimer.Interval = 180000
            ' check sleeping connections every 3 minutes            
            AddHandler MyKillTimer.Elapsed, AddressOf MyKillTimer_Event
            
            MyKillTimer.AutoReset = True
            MyKillTimer.Enabled = True
        Catch excep As Exception
        End Try
    End Sub
    
    Private Sub MyKillTimer_Event(ByVal source As Object, ByVal e As System.Timers.ElapsedEventArgs)
        KillSleepingConnections(30)
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    'Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
    '    ' Code that runs when an unhandled error occurs
        
    '    If ConfigurationManager.AppSettings("IsErrorTrapRequired").ToUpper = "YES" Then
    '        Dim strError, strMessage, strStackTrace, strSource, strSite, fullMsg As String
    '        strMessage = Server.GetLastError().Message
    '        strStackTrace = Server.GetLastError().StackTrace
    '        strError = Server.GetLastError().GetBaseException().ToString()
    '        strSource = Server.GetLastError().Source
    '        strSite = Server.GetLastError().GetHashCode().ToString()
    '        Context.ClearError()
    '        fullMsg = "<font face='verdana' size='2'><b>IP Address:</b><br>" & Request.ServerVariables.Get("REMOTE_ADDR") & "<br><br><b>Message:</b><br>" & strMessage & "<br><br><b>Stack Trace: :</b><br>" & strStackTrace & "<br><br><b>Base Exception:</b><br>" & strError & "<br><br><b>Source:</b><br>" & strSource & "<br><br><b>Hash Code:</b><br>" & strSite & "</font>"
    '        Dim common As New clsCommon
    '        'common.SendErrorMail(fullMsg)
    '        common = Nothing
    '        Response.Write(fullMsg)
    '    End If
    'End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
        'iTECH.InbizERP.BusinessLogic.AppConfiguration.ClearCacheOnLogout() 'Clear values from cache for development purpose only
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
    
    
    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)        
        'Code that will execute on every request is begin.
        'If HttpContext.Current.Session("Language") Is Nothing Or HttpContext.Current.Session("Language") = "" Then
        '    HttpContext.Current.Session("Language") = "en-CA"
        'End If
        'If Not HttpContext.Current.Session("Language") = "en-CA" Then
        '    System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo(HttpContext.Current.Session("Language").ToString)
        'End If
        'KillSleepingConnections(30)
        'Dim allowedIPs As String = ConfigurationManager.AppSettings("MaintenanceModeIPAllowed")
        'allowedIPs = IIf(String.IsNullOrEmpty(allowedIPs), String.Empty, allowedIPs)
        
        'If Request.RawUrl.Contains("/montreal/") Then
        '    Context.RewritePath("~/Maintenance.aspx")
        'End If
        
        'If ConfigurationManager.AppSettings("MaintenanceMode") = "true" AndAlso Request.IsLocal = False Then 'AndAlso allowedIPs.Contains(Request.UserHostAddress) = False Then
        '    Context.RewritePath("~/Maintenance.aspx")
        'ElseIf HttpContext.Current.Request.IsSecureConnection.Equals(False) AndAlso HttpContext.Current.Request.IsLocal.Equals(False) Then
        '    Response.Redirect(Request.Url.ToString().Replace("http:", "https:"))
        'End If
    End Sub

    ''' <summary>
    ''' This function checks for any sleeping connections beyond a reasonable time and kills them.
    ''' Since .NET appears to have a bug with how pooling MySQL connections are handled and leaves
    ''' too many sleeping connections without closing them, we will kill them here.
    ''' </summary>
    ''' iMinSecondsToExpire - all connections sleeping more than this amount in seconds will be killed.
    ''' <returns>integer - number of connections killed</returns>
    Public Shared Function KillSleepingConnections(ByVal iMinSecondsToExpire As Integer) As Integer
        Dim strSQL As String = "show processlist"
        Dim m_ProcessesToKill As System.Collections.ArrayList = New ArrayList()
        Dim dbHelp As New DbHelper()        
        Dim myCmd As New MySqlCommand(strSQL, dbHelp.Connection)
        Dim MyReader As MySqlDataReader = Nothing
        
        Try
            dbHelp.Connection.Open()

            ' Get a list of processes to kill.
            MyReader = myCmd.ExecuteReader()
            While MyReader.Read()
                ' Find all processes sleeping with a timeout value higher than our threshold.
                Dim iPID As Integer = Convert.ToInt32(MyReader("Id").ToString())
                Dim strState As String = MyReader("Command").ToString()
                Dim iTime As Integer = Convert.ToInt32(MyReader("Time").ToString())

                If strState = "Sleep" AndAlso iTime >= iMinSecondsToExpire AndAlso iPID > 0 Then
                    ' This connection is sitting around doing nothing. Kill it.
                    m_ProcessesToKill.Add(iPID)
                End If
            End While

            MyReader.Close()

            For Each aPID As Integer In m_ProcessesToKill
                strSQL = String.Format("kill {0}", aPID)
                myCmd.CommandText = strSQL
                myCmd.ExecuteNonQuery()
            Next
        Catch excep As Exception
        Finally            
            dbHelp.CloseDatabaseConnection(MyReader)
        End Try

        Return m_ProcessesToKill.Count
    End Function
       
</script>
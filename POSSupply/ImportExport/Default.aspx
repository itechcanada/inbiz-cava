<%@ Page Language="VB" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="false" ValidateRequest="false" 
    CodeFile="Default.aspx.vb" Inherits="ImportExport_Default" %>

<%@ Register Src="../Controls/CommonSearch/CRMSearch.ascx" TagName="CRMSearch" TagPrefix="uc1" %>
<%@ Register Src="../Controls/CommonSearch/InventorySearch.ascx" TagName="InventorySearch"
    TagPrefix="uc2" %>
<asp:Content ID="cntTop" ContentPlaceHolderID="cphTop" runat="Server">
    <div >
        <div class="col1">
            <img src="../lib/image/icon/ico_admin.png" />
        </div>
        <div class="col2">
            <h1>
                Administration</h1>
            <b>Importing</b>
        </div>
    </div>
    
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <asp:HiddenField ID="hdnErrMsg" runat="server" />
    <div>
        <div onkeypress="return disableEnterKey(event)">
        <h3>Importing </h3>
            <table width="100%" class="table">
                <tr>
                    <td align="right" width="40%" class="text">
                        <asp:Label ID="lblCSV" runat="server" CssClass="lblBold" Text=""></asp:Label>&nbsp;&nbsp;
                    </td>
                    <td align="left" width="60%" class="input">
                        <asp:FileUpload ID="CSVFile" runat="server" />
                    </td>
                </tr>
                <tr runat="server" id="trAutoSR" visible="false" class="text">
                    <td align="right" class="text">
                        <asp:Label ID="Label1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblAutoAssignSR %>"></asp:Label>&nbsp;&nbsp;
                    </td>
                    <td align="left" class="input">
                        <asp:RadioButtonList runat="server" ID="rdAutoAssign" RepeatDirection="Horizontal">
                            <asp:ListItem Value="Y" Selected="True" Text="<%$ Resources:Resource, lblYes %>"></asp:ListItem>
                            <asp:ListItem Value="N" Text="<%$ Resources:Resource, lblNo %>"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="inputCBLbt">
                        <asp:Button ID="btnImport" runat="server" Text="Import" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left" class="text">
                        <asp:Label ID="lblNote" runat="server" CssClass="lblBold" Style="color: Green;" Text=""></asp:Label><br />
                        <br />
                        <asp:Label ID="lblFormat" runat="server" CssClass="lblBold" Style="color: Green;"></asp:Label><br />
                        <br />
                        <asp:Label ID="lblSeparator" runat="server" CssClass="lblBold" Style="color: Green;" Visible="false"
                            Text="Fields Terminated by , <br />Lines terminated by \n"></asp:Label>
                    </td>
                </tr>
                <%=strMsg %>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft" runat="Server">
    <div >
        <uc1:CRMSearch ID="CRMSearch1" runat="server" Visible="false" />
        <uc2:InventorySearch ID="InventorySearch1" runat="server" Visible="false" />
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphHead">
</asp:Content>

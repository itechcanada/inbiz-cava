Imports System.Threading
Imports System.Globalization
Imports System.Data.Odbc
Imports System.Data
Imports iCtrl = iTECH.WebControls
Imports iTECH.Library.Utilities

Partial Class ImportExport_Default
    Inherits BasePage
    Protected strMsg As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If  

        If Not IsPostBack Then
            If Request.QueryString("Type") = "C" Then
                trAutoSR.Visible = True
                lblTitle.Text = Resources.Resource.btnImpCustomer
                lblFormat.Text = "Fields:  " & "PartnerLongName, PartnerPhone, PartnerFax, PartnerEmail, PartnerWebsite, PartnerAcronyme, PartnerNote, PartnerTaxCode, PartnerCurrencyCode(CAD, USD, EUR), PartnerLang(en, fr), PartnerType(2=Distributor, 1=EndClient, 3=Reseller, 4=Leads), addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode, PartnerInvoiceNetTerms, PartnerDiscount, PartnerCreditAvailable, ContactName, ContactTitle, ContactPhone, ContactPhoneExt, ContactEmail, ContactFax"
                CRMSearch1.Visible = True
                InventorySearch1.Visible = False
                lblNote.Text = Resources.Resource.XLSXFormat
                lblCSV.Text = Resources.Resource.SelectXLSX
            ElseIf Request.QueryString("Type") = "P" Then
                lblTitle.Text = Resources.Resource.ImportProd
                lblFormat.Text = "Fields:  " & "prdExtID, prdUPCCode, prdName, prdMinQtyPerSO, prdSalePricePerMinQty, prdEndUserSalesPrice, prdWebSalesPrice, prdIsGlutenFree, prdSpicyLevel, prdIsContainsNuts, prdIsVegetarian, prdLength, prdLengthPkg, prdWidth, prdWidthPkg, prdSmallDesc, prdLargeDesc, prdWhsCode, prdOhdQty, prdWeight, prdType (1=Product), vendorID, prdCostPrice " 'Edited by mukesh 20130722 to add some columns'
                CRMSearch1.Visible = False
                InventorySearch1.Visible = True
                lblNote.Text = Resources.Resource.CSVFormat
                lblCSV.Text = Resources.Resource.SelectCSV
            ElseIf Request.QueryString("Type") = "S" Then
                lblTitle.Text = Resources.Resource.ImportProd
                lblFormat.Text = "Fields:  " & "ColorID, Name, Description Fr, Description En, Description En, Season/Col_ID, STYLE,Mat-ID, Gauge-ID, Texture-ID,Website Category-ID,Website Sub Catg-ID,Neckline-ID,Extra Catg-ID,Sleeve-ID,Silhouette-ID,Trend-ID,Keywords-ID,LengthSize,Length Increment,Chese,Chest Inc,Shoulder,Shoulder Inc,Biceps,Biceps Inc,FOB,Landed,WholeSalePrice,RetailPrice,WebPrice,ProdWgt,ProdWgtIncr,PackageWgt,PackageWidth,PackageLength,IsWebActive,IsPOSMenu,SizeID,Qty "
                CRMSearch1.Visible = False
                InventorySearch1.Visible = True
                lblNote.Text = Resources.Resource.XLSXFormat
                lblCSV.Text = Resources.Resource.SelectXLSX
            End If
        End If
    End Sub

    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
 
    Private Sub ImportDistributors()
        If CSVFile.HasFile Then
            Dim strFile As String = System.IO.Path.GetFileName(CSVFile.FileName).Trim
            Dim strFileArr() As String = Split((strFile), ".")
            Dim count As Int16 = strFileArr.Length - 1

            If strFileArr.Length = "1" Then
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, Resources.Resource.msgInvalidFileType)
            End If

            If strFileArr(count).ToLower = "csv" Then
                Try
                    Dim SaveLocation As String = Server.MapPath("~/Upload/CSVFiles/Distributors/") & strFile
                    CSVFile.SaveAs(SaveLocation)

                    Dim objImport As clsImport = New clsImport
                    Dim intRecCount As Integer = objImport.ImportDistributor(SaveLocation)


                    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Success, intRecCount & " Record(s) Imported Successfully.")
                Catch ex As Exception                    
                    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Critical, ex.Message)
                End Try
            Else
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, "Please Select CSV File for Import")

            End If
        End If
    End Sub
    Private Sub ImportResellers()
        If CSVFile.HasFile Then
            Dim strFile As String = System.IO.Path.GetFileName(CSVFile.FileName).Trim
            Dim strFileArr() As String = Split((strFile), ".")
            Dim count As Int16 = strFileArr.Length - 1

            If strFileArr.Length = "1" Then
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, "Invalid File.")
            End If

            If strFileArr(count).ToLower = "csv" Then
                Try
                    Dim SaveLocation As String = Server.MapPath("~/Upload/CSVFiles/Resellers/") & strFile
                    CSVFile.SaveAs(SaveLocation)

                    Dim objImport As clsImport = New clsImport
                    Dim intRecCount As Integer = objImport.ImportReseller(SaveLocation)

                    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Success, intRecCount & " Record(s) Imported Successfully.")
                Catch ex As Exception
                    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Critical, ex.Message)
                End Try
            Else
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, "Please Select CSV File for Import")
            End If
        End If
    End Sub

    Private Sub ImportEndClient()
        If CSVFile.HasFile Then
            Dim strFile As String = System.IO.Path.GetFileName(CSVFile.FileName).Trim
            Dim strFileArr() As String = Split((strFile), ".")
            Dim count As Int16 = strFileArr.Length - 1

            If strFileArr.Length = "1" Then
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, "Invalid File.")
            End If

            If strFileArr(count).ToLower = "csv" Then
                Try
                    Dim SaveLocation As String = Server.MapPath("~/Upload/CSVFiles/EndClient/") & strFile
                    CSVFile.SaveAs(SaveLocation)

                    Dim objImport As clsImport = New clsImport
                    Dim intRecCount As Integer = objImport.ImportEndClient(SaveLocation)

                    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Success, intRecCount & " Record(s) Imported Successfully.")
                Catch ex As Exception
                    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Critical, ex.Message)
                End Try
            Else
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, "Please Select CSV File for Import")
            End If
        End If
    End Sub

    Private Sub ImportVendors()
        If CSVFile.HasFile Then
            Dim strFile As String = System.IO.Path.GetFileName(CSVFile.FileName).Trim
            Dim strFileArr() As String = Split((strFile), ".")
            Dim count As Int16 = strFileArr.Length - 1

            If strFileArr.Length = "1" Then
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, "Invalid File.")
            End If

            If strFileArr(count).ToLower = "csv" Then
                Try
                    Dim SaveLocation As String = Server.MapPath("~/Upload/CSVFiles/Vendors/") & strFile
                    CSVFile.SaveAs(SaveLocation)

                    Dim objImport As clsImport = New clsImport
                    Dim intRecCount As Integer = objImport.ImportVendor(SaveLocation)

                    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Success, intRecCount & " Record(s) Imported Successfully.")
                Catch ex As Exception
                    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Critical, ex.Message)
                End Try
            Else
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, "Please Select CSV File for Import")
            End If
        End If
    End Sub

    Private Sub ImportProducts()
        If CSVFile.HasFile Then
            Dim strFile As String = System.IO.Path.GetFileName(CSVFile.FileName).Trim
            Dim strFileArr() As String = Split((strFile), ".")
            Dim count As Int16 = strFileArr.Length - 1

            If strFileArr.Length = "1" Then
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, "Invalid File.")
            End If

            If strFileArr(count).ToLower = "csv" Then
                Try
                    Dim SaveFolder As String = Server.MapPath("~/Upload/CSVFiles/Products/")
                    Dim SaveLocation As String = SaveFolder & strFile
                    CSVFile.SaveAs(SaveLocation)

                    Dim objImport As clsImport = New clsImport
                    Dim intRecCount As Integer = objImport.ImportProduct(SaveFolder, strFile)

                    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Success, intRecCount & " Record(s) Imported Successfully.")
                Catch ex As Exception
                    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Critical, ex.Message)
                End Try
            Else
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Information, "Please Select CSV File for Import")
            End If
        End If
    End Sub

    Private Sub ImportProductStyles()
        If CSVFile.HasFile Then
            Dim strFile As String = System.IO.Path.GetFileName(CSVFile.FileName).Trim
            Dim strFileArr() As String = Split((strFile), ".")
            Dim count As Int16 = strFileArr.Length - 1

            If strFileArr.Length = "1" Then
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, "Invalid File.")
            End If

            If strFileArr(count).ToLower = "xlsx" Then
                Try
                    Dim SaveFolder As String = Server.MapPath("~/Upload/CSVFiles/Products/")
                    Dim SaveLocation As String = SaveFolder & strFile
                    CSVFile.SaveAs(SaveLocation)

                    Dim objImport As Import = New Import
                    Dim objImportExcel As ExcelHelper = New ExcelHelper
                    Dim dt As DataTable = New DataTable
                    Dim ds As DataSet = New DataSet
                    dt = objImportExcel.exceldata(SaveLocation)
                    ds.Tables.Add(dt)
                    Dim errorMsg As String = objImport.ImportProductStyle(ds)
                    Dim intRecCount As Integer = 0
                    Dim errorMsgColl As String() = errorMsg.Split(",")
                    Dim counter As Integer = 0
                    For Each s As String In errorMsgColl
                        If s.Trim() <> "" And counter = 0 Then
                            intRecCount = s
                            counter += 1
                        ElseIf s.Trim() <> "" Then
                            strMsg = s
                        End If
                    Next s
                    hdnErrMsg.Value = BusinessUtility.GetString(objImport.ImportErrorMessage())
                    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Success, intRecCount & " Record(s) Imported Successfully.")
                Catch ex As Exception
                    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Critical, ex.Message)
                End Try
            Else
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Information, Resources.Resource.PleaseSelectXLSXFileforImport)
            End If
        End If
    End Sub

    Private Sub ImportCustomers()
        If CSVFile.HasFile Then
            Dim strFile As String = System.IO.Path.GetFileName(CSVFile.FileName).Trim
            Dim strFileArr() As String = Split((strFile), ".")
            Dim count As Int16 = strFileArr.Length - 1

            If strFileArr.Length = "1" Then
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, "Invalid File.")
            End If

            If strFileArr(count).ToLower = "xlsx" Then
                Try
                    Dim SaveFolder As String = Server.MapPath("~/Upload/CSVFiles/Customers/")
                    Dim SaveLocation As String = SaveFolder & strFile
                    CSVFile.SaveAs(SaveLocation)

                    Dim objImport As Import = New Import
                    Dim objImportExcel As ExcelHelper = New ExcelHelper
                    Dim dt As DataTable = New DataTable
                    Dim ds As DataSet = New DataSet
                    dt = objImportExcel.exceldata(SaveLocation)
                    ds.Tables.Add(dt)

                    'Dim objImport As clsImport = New clsImport
                    'Dim intRecCount As Integer = objImport.ImportCustomers(SaveFolder, strFile, rdAutoAssign.SelectedValue)
                    Dim errorMsg As String = objImport.ImportCustomer(ds, rdAutoAssign.SelectedValue)
                    Dim intRecCount As Integer = 0
                    Dim errorMsgColl As String() = errorMsg.Split(",")
                    Dim counter As Integer = 0
                    For Each s As String In errorMsgColl
                        If s.Trim() <> "" And counter = 0 Then
                            intRecCount = s
                            counter += 1
                        ElseIf s.Trim() <> "" Then
                            strMsg = s
                        End If
                    Next s

                    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Success, intRecCount & " Record(s) Imported Successfully.")
                Catch ex As Exception
                    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Critical, ex.Message)
                End Try
            Else
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, Resources.Resource.PleaseSelectXLSXFileforImport)
            End If
        End If
    End Sub

    Protected Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImport.Click
        'If Not CSVFile.HasFile Then
        '    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, "Please Select CSV File for Import")
        'End If

        If Request.QueryString("Type") = "C" Then
            If Not CSVFile.HasFile Then
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, Resources.Resource.PleaseSelectXLSXFileforImport)
                Return
            End If
            ImportCustomers()
        ElseIf Request.QueryString("Type") = "P" Then
            If Not CSVFile.HasFile Then
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, "Please Select CSV File for Import")
                Return
            End If
            ImportProducts()
        ElseIf Request.QueryString("Type") = "S" Then
            If Not CSVFile.HasFile Then
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, Resources.Resource.PleaseSelectXLSXFileforImport)
                Return
            End If
            ImportProductStyles()
        End If
    End Sub
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsSystemUsers
	Inherits clsDataClass
    Private _UserID, _UserLoginEmailID, _UserPasswd, _UserRole, _UserPhone, _UserSecEmail, _UserDept, _UserActive, _UserLastUpdatedOn, _UserLastLoggedID, _UserName As String
	Public Property UserID() As String
		Get
			Return _UserID
		End Get
		Set(ByVal value As String)
            _UserID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserLoginEmailID() As String
		Get
			Return _UserLoginEmailID
		End Get
		Set(ByVal value As String)
            _UserLoginEmailID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserPasswd() As String
		Get
			Return _UserPasswd
		End Get
		Set(ByVal value As String)
            _UserPasswd = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserRole() As String
		Get
			Return _UserRole
		End Get
		Set(ByVal value As String)
            _UserRole = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserPhone() As String
		Get
			Return _UserPhone
		End Get
		Set(ByVal value As String)
            _UserPhone = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserSecEmail() As String
		Get
			Return _UserSecEmail
		End Get
		Set(ByVal value As String)
            _UserSecEmail = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserDept() As String
		Get
			Return _UserDept
		End Get
		Set(ByVal value As String)
            _UserDept = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserActive() As String
		Get
			Return _UserActive
		End Get
		Set(ByVal value As String)
            _UserActive = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserLastUpdatedOn() As String
		Get
			Return _UserLastUpdatedOn
		End Get
		Set(ByVal value As String)
            _UserLastUpdatedOn = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserLastLoggedID() As String
		Get
			Return _UserLastLoggedID
		End Get
		Set(ByVal value As String)
            _UserLastLoggedID = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property UserName() As String
        Get
            Return _UserName
        End Get
        Set(ByVal value As String)
            _UserName = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert SystemUsers
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertSystemUsers() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO systemusers( UserLoginEmailID, UserPasswd, UserRole, UserPhone, UserSecEmail, UserDept, UserActive, UserLastUpdatedOn, UserLastLoggedID, UserName) VALUES('"
        strSQL += _UserLoginEmailID + "',password('"
        strSQL += _UserPasswd + "'),'"
		strSQL += _UserRole + "','"
		strSQL += _UserPhone + "','"
		strSQL += _UserSecEmail + "','"
		strSQL += _UserDept + "','"
		strSQL += _UserActive + "','"
        strSQL += _UserLastUpdatedOn + "','"
        strSQL += _UserLastLoggedID + "','"
        strSQL += _UserName + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update SystemUsers
    ''' </summary>
    ''' <param name="nUpdatePassword"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateSystemUsers(ByVal nUpdatePassword As Int16) As Boolean
        Dim strSQL As String
        strSQL = "UPDATE systemusers set "
        strSQL += "UserLoginEmailID='" + _UserLoginEmailID + "', "
        If nUpdatePassword = 1 Then
            strSQL += "UserPasswd=password('" + _UserPasswd + "'), "
        End If
        strSQL += "UserRole='" + _UserRole + "', "
        strSQL += "UserPhone='" + _UserPhone + "', "
        strSQL += "UserSecEmail='" + _UserSecEmail + "', "
        strSQL += "UserDept='" + _UserDept + "', "
        strSQL += "UserActive='" + _UserActive + "', "
        strSQL += "UserLastUpdatedOn='" + _UserLastUpdatedOn + "', "
        strSQL += "UserLastLoggedID='" + _UserLastLoggedID + "', "
        strSQL += "UserName='" + _UserName + "'"
        strSQL += " where UserID='" + _UserID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of SystemUserss
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getSystemUsersInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
        strSQL = "SELECT  UserLoginEmailID, UserPasswd, UserRole, UserPhone, UserSecEmail, UserDept, UserActive, UserLastUpdatedOn, UserLastLoggedID, UserName FROM systemusers where UserID='" + _UserID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_UserLoginEmailID = drObj.Item("UserLoginEmailID").ToString
			_UserPasswd = drObj.Item("UserPasswd").ToString
			_UserRole = drObj.Item("UserRole").ToString
			_UserPhone = drObj.Item("UserPhone").ToString
			_UserSecEmail = drObj.Item("UserSecEmail").ToString
			_UserDept = drObj.Item("UserDept").ToString
			_UserActive = drObj.Item("UserActive").ToString
			_UserLastUpdatedOn = drObj.Item("UserLastUpdatedOn").ToString
            _UserLastLoggedID = drObj.Item("UserLastLoggedID").ToString
            _UserName = drObj.Item("UserName").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Validate User
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidateUser() As Boolean
        Dim strSQL As String
        strSQL = "SELECT Count(*) FROM systemusers WHERE userLoginEmailID='" & _UserLoginEmailID & "' "
        strSQL &= "AND userPasswd=password('" & _UserPasswd & "') And userActive=1 "
        'Check Valid User
        If (Convert.ToUInt16(GetScalarData(strSQL)) > 0) Then
            Dim drObj As System.Data.Odbc.OdbcDataReader
            drObj = GetDataReader("SELECT UserID, UserLoginEmailID, UserRole FROM systemusers WHERE userLoginEmailID='" & _UserLoginEmailID & "' AND UserPasswd=password('" & _UserPasswd & "')")
            While drObj.Read
                HttpContext.Current.Session("UserID") = drObj.Item("userID").ToString
                HttpContext.Current.Session("LoginID") = drObj.Item("UserLoginEmailID").ToString
                HttpContext.Current.Session("UserRole") = drObj.Item("UserRole").ToString
                HttpContext.Current.Session("EmailID") = drObj.Item("UserLoginEmailID").ToString
                HttpContext.Current.Session("UserModules") = "ADM"
            End While
            drObj.Close()
            CloseDatabaseConnection()
            Return True
        Else
            Return False
        End If
    End Function
    ''' <summary>
    ''' Fill Grid of User
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String = ""
        strSQL = "SELECT UserID, UserLoginEmailID, UserRole, UserPhone, UserSecEmail, UserDept, CASE userActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as UserActive, UserLastUpdatedOn, UserLastLoggedID, UserName FROM systemusers order by UserLoginEmailID "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid of User
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal StatusData As String, ByVal SearchData As String) As String
        StatusData = clsCommon.funRemove(StatusData, False)
        SearchData = clsCommon.funRemove(SearchData, False)
        Dim strSQL As String = ""
        strSQL = "SELECT UserID, UserLoginEmailID, UserRole, UserPhone, UserSecEmail, UserDept, CASE userActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as UserActive, UserLastUpdatedOn, UserLastLoggedID, UserName FROM systemusers "
        If StatusData = "1" Then
            strSQL += " where userActive='" + StatusData + "' and "
            strSQL += " (UserLoginEmailID like '%" + SearchData + "%' or "
            strSQL += " UserName like '%" + SearchData + "%') "
        ElseIf StatusData = "0" Then
            strSQL += " where userActive='" + StatusData + "' and "
            strSQL += " (UserLoginEmailID like '%" + SearchData + "%' or "
            strSQL += " UserName like '%" + SearchData + "%') "
        ElseIf StatusData = "" Then
            strSQL += " where "
            strSQL += " (UserLoginEmailID like '%" + SearchData + "%' or "
            strSQL += " UserName like '%" + SearchData + "%') "
        End If
        strSQL += " order by UserLoginEmailID "
        Return strSQL
    End Function
    ''' <summary>
    ''' Delete User
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteUser(ByVal DeleteID As String) As String
        Dim strSQL As String
        strSQL = "UPDATE systemusers SET UserActive=0 WHERE UserID='" + DeleteID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Check Duplicate User Email ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateUserEmailID() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM systemusers where UserLoginEmailID='" & _UserLoginEmailID & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
End Class

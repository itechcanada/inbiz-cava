Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsProductColor
    Inherits clsDataClass
    Private _prdColiCode, _prdID As String
    Public Property prdColiCode() As String
        Get
            Return _prdColiCode
        End Get
        Set(ByVal value As String)
            _prdColiCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ProductID() As String
        Get
            Return _prdID
        End Get
        Set(ByVal value As String)
            _prdID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Insert ProductColor
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertProductColor() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO prdcolor(prdColiCode, prdID) VALUES( "
        strSQL += "'" + _prdColiCode + "','"
        strSQL += _prdID + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
   
    ''' <summary>
    ''' Populate objects of ProductColor
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetProductColorInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT prdColiCode,prdID FROM prdcolor where prdColiCode='" + _prdColiCode + "' and prdID='" + _prdID + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _prdColiCode = drObj.Item("_prdColiCode").ToString
            _prdID = drObj.Item("_prdID").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Find Color Code Name
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDuplicatePrdColor() As Boolean
        Dim strSQL As String
        Dim objDataClass As New clsDataClass
        strSQL = "SELECT count(*) FROM prdcolor where prdColiCode='" + _prdColiCode + "' and prdID='" + _prdID + "'"
        If objDataClass.GetScalarData(strSQL) <> 0 Then
            objDataClass = Nothing
            Return True
        Else
            objDataClass = Nothing
            Return False
        End If
    End Function
    ''' <summary>
    ''' Fill Product Color
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subFillPrdColor() As String
        Dim strSql As String = "select prdColiCode,prdID,colCode,colDesc from prdcolor"
        strSql += " Inner Join sysproductcolor  on sysproductcolor.colCode=prdcolor.prdColiCode where prdID ='" & HttpContext.Current.Request.QueryString("PrdID") & "'"
        'where descLang='" + Current.Session("Lang") + "' "
        Return strSql
    End Function
    ''' <summary>
    ''' Product Color Delete
    ''' </summary>
    ''' <param name="strPrdColorCodeID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subPrdColorDelete(ByVal strPrdColorCodeID As String) As String
        Dim strSql As String = "delete from prdcolor where prdColiCode='" + strPrdColorCodeID + "' and prdID ='" & HttpContext.Current.Request.QueryString("PrdID") & "'"
        Return strSql
    End Function
    ''' <summary>
    ''' Delete ProductColors
    ''' </summary>
    ''' <param name="strProductID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteProductColors(ByVal strProductID As String) As Boolean
        Dim strSQL As String
        strSQL = "Delete from prdcolor where "
        strSQL += " prdID ='" & strProductID & "' "

        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
End Class

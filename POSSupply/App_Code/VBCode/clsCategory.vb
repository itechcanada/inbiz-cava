Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.Odbc
'Imports clsCommon
''Imports System.Web.HttpContext

Public Class clsCategory

#Region "Private Variable"
    Private intCatId As String, strCatName As String, intCatUpdatedBy As String
    Private dateCatUpdatedOn As String, strCatImagePath As String, intIsActive As String
    Private intcatWebSeq As String, tintcatStatus As String
    Private _catIsFreeText As String, _catFreeText As String, _catdescLang As String, _eBayCatgNo As String

#End Region

#Region "Property"
    Public Property CategoryID() As String
        Get
            Return intCatId
        End Get
        Set(ByVal value As String)
            intCatId = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DescLang() As String
        Get
            Return _catdescLang
        End Get
        Set(ByVal value As String)
            _catdescLang = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CategoryName() As String
        Get
            Return strCatName
        End Get
        Set(ByVal value As String)
            Me.strCatName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CategoryUpdatedBy() As String
        Get
            Return intCatUpdatedBy
        End Get
        Set(ByVal value As String)
            Me.intCatUpdatedBy = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CategoryUpdatedOn() As String
        Get
            Return dateCatUpdatedOn
        End Get
        Set(ByVal value As String)
            Me.dateCatUpdatedOn = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CategoryImagePath() As String
        Get
            Return strCatImagePath
        End Get
        Set(ByVal value As String)
            Me.strCatImagePath = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property IsActive() As String
        Get
            Return intIsActive
        End Get
        Set(ByVal value As String)
            intIsActive = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CategoryWebSequence() As String
        Get
            Return intcatWebSeq
        End Get
        Set(ByVal value As String)
            intcatWebSeq = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CategoryStatus() As String
        Get
            Return tintcatStatus
        End Get
        Set(ByVal value As String)
            tintcatStatus = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CategoryIsFreeText() As String
        Get
            Return _catIsFreeText
        End Get
        Set(ByVal value As String)
            _catIsFreeText = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CategoryFreeText() As String
        Get
            Return _catFreeText
        End Get
        Set(ByVal value As String)
            _catFreeText = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property eBayCatgNo() As String
        Get
            Return _eBayCatgNo
        End Get
        Set(ByVal value As String)
            _eBayCatgNo = clsCommon.funRemove(value, True)
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

#Region "Functions"
    ''' <summary>
    ''' Find Category Name
    ''' </summary>
    ''' <param name="strCID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDuplicateCategory(Optional ByVal strCID As String = "") As Boolean
        Dim strSQL As String
        Dim objDataClass As New clsDataClass
        If strCID <> "" Then
            strSQL = "SELECT count(*) FROM category where catName='" & strCatName & "' And catStatus='1' and catId <>'" & strCID & "' and catdescLang = '" + _catdescLang + "'"
        Else
            strSQL = "SELECT count(*) FROM category where catName='" & strCatName & "' And catStatus='1' and catdescLang = '" + _catdescLang + "'"
        End If
        If objDataClass.GetScalarData(strSQL) <> 0 Then
            objDataClass.OpenDatabaseConnection()
            objDataClass.CloseDatabaseConnection()
            objDataClass = Nothing
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Return Pending Category Request 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funPendingCategoryReq() As String
        Dim objDataClass As New clsDataClass
        Dim strSql, strCatCount As String
        objDataClass.OpenDatabaseConnection()
        strSql = "SELECT Count(*) FROM category where catIsActive='2' and catStatus='1' and catdescLang = 'en' " '" + _catdescLang + "'"
        strCatCount = objDataClass.GetScalarData(strSql)
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return strCatCount
    End Function

    ''' <summary>
    ''' Insert Category Info
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertCategory() As Boolean
        Dim strsql As String = "INSERT INTO category(catdescLang, catName,catIsActive,catUpdatedBy,catUpdatedOn,catImagePath,catWebSeq,catStatus,catIsFreeText,catFreeText,eBayCatgNo)VALUES('en','" & _
                    CategoryName & "'," & IsActive & "," & CategoryUpdatedBy & ",'" & CategoryUpdatedOn & "','" & CategoryImagePath & "'," & CategoryWebSequence & ",'1'," & _catIsFreeText & ",'" & _catFreeText & "','" & _eBayCatgNo & "')"
        strsql = clsCommon.replaceSysNull(strsql)
        Dim objDataClass As New clsDataClass

        Dim intCount As Int32 = objDataClass.SetData(strsql)
        If intCount > 0 Then
            Dim catgID As Int32 = funGetMaxCategoryID()
            strsql = "INSERT INTO category( catId, catdescLang, catName,catIsActive,catUpdatedBy,catUpdatedOn,catImagePath,catWebSeq,catStatus,catIsFreeText,catFreeText,eBayCatgNo)VALUES( " & catgID & ", 'fr','" & _
                              CategoryName & "'," & IsActive & "," & CategoryUpdatedBy & ",'" & CategoryUpdatedOn & "','" & CategoryImagePath & "'," & CategoryWebSequence & ",'1'," & _catIsFreeText & ",'" & _catFreeText & "','" & _eBayCatgNo & "')"
            strsql = clsCommon.replaceSysNull(strsql)
            objDataClass.SetData(strsql)

            strsql = "INSERT INTO category( catId, catdescLang, catName,catIsActive,catUpdatedBy,catUpdatedOn,catImagePath,catWebSeq,catStatus,catIsFreeText,catFreeText,eBayCatgNo)VALUES(" & catgID & ", 'sp','" & _
                           CategoryName & "'," & IsActive & "," & CategoryUpdatedBy & ",'" & CategoryUpdatedOn & "','" & CategoryImagePath & "'," & CategoryWebSequence & ",'1'," & _catIsFreeText & ",'" & _catFreeText & "','" & _eBayCatgNo & "')"
            strsql = clsCommon.replaceSysNull(strsql)
            objDataClass.SetData(strsql)
        End If
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return True
    End Function

    ''' <summary>
    ''' Update Category Info
    ''' </summary>
    ''' <param name="strCatImage"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateCategory(ByVal strCatImage As String) As Boolean        
        Dim objDataClass As New clsDataClass
        Dim strSQL As String
        strSQL = "Update category set catIsActive='" & IsActive & "',catWebSeq='" & CategoryWebSequence & "', catUpdatedBy='" & CategoryUpdatedBy & "',catUpdatedOn='" & CategoryUpdatedOn & "',catIsFreeText=" & _catIsFreeText & " ,eBayCatgNo='" & _eBayCatgNo & "'"
        If strCatImage = "Yes" Then
            strSQL += ", catImagePath='" & CategoryImagePath & "'" 'when Image have Changed 
        End If
        strSQL += " Where catId='" & intCatId & "' "
        objDataClass.SetData(strSQL)

        'strSQL = "Update category set catName='" & CategoryName & "',catIsActive='" & IsActive & "',catWebSeq='" & CategoryWebSequence & "', catUpdatedBy='" & CategoryUpdatedBy & "',catUpdatedOn='" & CategoryUpdatedOn & "',catIsFreeText=" & _catIsFreeText & ",catFreeText='" & _catFreeText & "' "
        strSQL = "Update category set catName='" & CategoryName & "',catFreeText='" & _catFreeText & "' "
        strSQL += " Where catId='" & intCatId & "'  and catdescLang = '" + _catdescLang + "'"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        objDataClass.SetData(strSQL)
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return True
    End Function
    ''' <summary>
    ''' For Fill GridView
    ''' </summary>
    ''' <param name="strApprove"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(Optional ByVal strApprove As String = "") As String
        Dim strSql As String
        strSql = "Select catID,catName,concat(userFirstName,' ',UserLastName) as UserName, CASE catIsActive WHEN 1 THEN 'green.gif' WHEN 2 THEN 'orange.gif' ELSE 'red.gif' END as catIsActive ,catStatus,catUpdatedOn," & _
                  " case catImagePath when '' then '../images/nopic.jpg' else catImagePath end as catImagePath, " & _
                 " catWebSeq, eBayCatgNo from category  inner join  users on  users.userid=category.catUpdatedBy where catStatus='1' and catDescLang ='" + clsCommon.funGetProductLang() + "'"
        If strApprove <> "" Then
            strSql += "And catIsActive='2' "
        Else
            If HttpContext.Current.Session("UserRole") = "0" Then
                strSql += "And catIsActive<>'2' "
            End If
        End If
        strSql += "  ORDER BY catWebSeq"
        Return strSql
    End Function

    ''' <summary>
    ''' For Delete User(we Update Only User Status)
    ''' </summary>
    ''' <param name="strUserID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteCategory(ByVal strUserID As String) As String
        Dim strSql As String
        strSql = "Update category set catStatus='0' Where catId='" & strUserID & "' "  'and catdescLang = '" + funGetProductLang() + "'"
        Return strSql
    End Function
    ''' <summary>
    ''' Fill Category Name in Dropdown list
    ''' </summary>
    ''' <param name="dlCategoryName"></param>
    ''' <remarks></remarks>
    Public Sub subGetCategoryName(ByVal dlCategoryName As DropDownList)
        Dim objDataclass As New clsDataClass
        objDataclass.OpenDatabaseConnection()
        Dim strSQL As String
        strSQL = "SELECT catId,catName from  category where catIsActive='1' and catStatus='1'  and catdescLang = '" + clsCommon.funGetProductLang() + "'"
        dlCategoryName.DataSource = objDataclass.GetDataReader(strSQL)
        dlCategoryName.DataTextField = "catName"
        dlCategoryName.DataValueField = "catId"
        dlCategoryName.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = "----Select Category----"
        dlCategoryName.Items.Insert(0, itm)
        objDataclass.CloseDatabaseConnection()
        objDataclass = Nothing
    End Sub
    ''' <summary>
    ''' Get Category Information
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetCategoryInfo()
        Dim objDataClass As New clsDataClass
        Dim dbreader As OdbcDataReader
        Dim strSql As String
        objDataClass.OpenDatabaseConnection()
        strSql = "SELECT * FROM category where catId= '" & intCatId & "' and catdescLang ='" + clsCommon.funGetProductLang() + "'"
        dbreader = objDataClass.GetDataReader(strSql)
        While dbreader.Read
            strCatName = dbreader.Item("catName").ToString
            intIsActive = dbreader.Item("catIsActive").ToString
            intCatUpdatedBy = dbreader.Item("catUpdatedBy").ToString
            dateCatUpdatedOn = dbreader.Item("catUpdatedOn").ToString
            strCatImagePath = dbreader.Item("catImagePath").ToString
            intcatWebSeq = dbreader.Item("catWebSeq").ToString
            tintcatStatus = dbreader.Item("catStatus").ToString
            _catIsFreeText = dbreader.Item("catIsFreeText").ToString
            _catFreeText = dbreader.Item("catFreeText").ToString
            _catdescLang = dbreader.Item("catdescLang").ToString
        End While
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
    End Sub
    ''' <summary>
    ''' Get Max Category Id
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetMaxCategoryID() As Integer
        Dim objDataClass As New clsDataClass
        Dim strSql, strMaxCatId As String
        objDataClass.OpenDatabaseConnection()
        strSql = "SELECT MAX(catId) FROM category "
        strMaxCatId = Convert.ToString(objDataClass.GetScalarData(strSql))
        strMaxCatId = IIf(strMaxCatId = "", 0, strMaxCatId)
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return strMaxCatId
    End Function
    ''' <summary>
    ''' Reject Category
    ''' </summary>
    ''' <param name="strCID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funRejectCategory(Optional ByVal strCID As String = "") As Boolean
        Dim strSQL As String
        Dim objDataClass As New clsDataClass
        strSQL = "Update category set catStatus='0',catIsActive='0' Where catId='" & strCID & "'"
        objDataClass.SetData(strSQL)
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return True
    End Function
    Public Sub GetCategoryName(ByVal dlCategoryName As DropDownList)
        Dim objDataClass As New clsDataClass
        objDataClass.OpenDatabaseConnection()
        Dim strSQL As String = "SELECT catId,catName FROM category where catIsActive='1' and catStatus='1' and catdescLang ='" + clsCommon.funGetProductLang() + "' Order By catName"
        dlCategoryName.Items.Clear()
        dlCategoryName.DataSource = objDataClass.GetDataReader(strSQL)
        dlCategoryName.DataTextField = "catName"
        dlCategoryName.DataValueField = "catId"
        dlCategoryName.DataBind()
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
    End Sub
#End Region
End Class

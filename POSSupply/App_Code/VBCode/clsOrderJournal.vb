Imports Microsoft.VisualBasic
''Imports System.Web.HttpContext
Imports System.Data
Imports System.Data.Odbc
''Imports clsCommon
Public Class clsOrderJournal
    Inherits clsDataClass
    Private _tagid, _whenOrdered, _quantity, _productId, _unit, _warehouseOrdered, _datetime, _unitPrice, _salesOrder As String
    Public Property Tagid() As String
        Get
            Return _tagid
        End Get
        Set(ByVal value As String)
            _tagid = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property WhenOrdered() As String
        Get
            Return _whenOrdered
        End Get
        Set(ByVal value As String)
            _whenOrdered = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Quantity() As String
        Get
            Return _quantity
        End Get
        Set(ByVal value As String)
            _quantity = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ProductId() As String
        Get
            Return _productId
        End Get
        Set(ByVal value As String)
            _productId = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property unit() As String
        Get
            Return _unit
        End Get
        Set(ByVal value As String)
            _unit = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property WarehouseOrdered() As String
        Get
            Return _warehouseOrdered
        End Get
        Set(ByVal value As String)
            _warehouseOrdered = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Datetime() As String
        Get
            Return _datetime
        End Get
        Set(ByVal value As String)
            _datetime = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UnitPrice() As String
        Get
            Return _unitPrice
        End Get
        Set(ByVal value As String)
            _unitPrice = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property SalesOrder() As String
        Get
            Return _salesOrder
        End Get
        Set(ByVal value As String)
            _salesOrder = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Insert OrderJournal
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function InsertOrderJournal() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO OrderJournal(tagid,whenOrdered,quantity,productId,unit,warehouseOrdered,datetime,unitPrice,salesOrder ) VALUES('"
        strSQL += _tagid + "','"
        strSQL += _whenOrdered + "','"
        strSQL += _quantity + "','"
        strSQL += _productId + "','"
        strSQL += _unit + "','"
        strSQL += _warehouseOrdered + "','"
        strSQL += _datetime + "','"
        strSQL += _unitPrice + "','"
        strSQL += _salesOrder + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
End Class

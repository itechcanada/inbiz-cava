Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsPOSTransaction
    Inherits clsDataClass
    Private _posTransId, _posTransType, _posTransDateTime, _posTransRegCode, _posTransUserID, _posTransStatus, _posSubTotal, _posTax1, _posTax1Desc, _posTax2, _posTax2Desc, _posTax3, _posTax3Desc, _posTax4, _posTax4Desc, _posTotalValue, _posOldTransID, _PosTabNote, _posGiftReason, _posAuthorizedBy, _posPercentDiscount As String

#Region "Properties"
    Public Property TransId() As String
        Get
            Return _posTransId
        End Get
        Set(ByVal value As String)
            _posTransId = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property TransType() As String
        Get
            Return _posTransType
        End Get
        Set(ByVal value As String)
            _posTransType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property TransDateTime() As String
        Get
            Return _posTransDateTime
        End Get
        Set(ByVal value As String)
            _posTransDateTime = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property TransRegCode() As String
        Get
            Return _posTransRegCode
        End Get
        Set(ByVal value As String)
            _posTransRegCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property TransUserID() As String
        Get
            Return _posTransUserID
        End Get
        Set(ByVal value As String)
            _posTransUserID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property TransStatus() As String
        Get
            Return _posTransStatus
        End Get
        Set(ByVal value As String)
            _posTransStatus = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property SubTotal() As String
        Get
            Return _posSubTotal
        End Get
        Set(ByVal value As String)
            _posSubTotal = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Tax1() As String
        Get
            Return _posTax1
        End Get
        Set(ByVal value As String)
            _posTax1 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Tax2() As String
        Get
            Return _posTax2
        End Get
        Set(ByVal value As String)
            _posTax2 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Tax3() As String
        Get
            Return _posTax3
        End Get
        Set(ByVal value As String)
            _posTax3 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Tax4() As String
        Get
            Return _posTax4
        End Get
        Set(ByVal value As String)
            _posTax4 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Tax1Desc() As String
        Get
            Return _posTax1Desc
        End Get
        Set(ByVal value As String)
            _posTax1Desc = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Tax2Desc() As String
        Get
            Return _posTax2Desc
        End Get
        Set(ByVal value As String)
            _posTax2Desc = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Tax3Desc() As String
        Get
            Return _posTax3Desc
        End Get
        Set(ByVal value As String)
            _posTax3Desc = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Tax4Desc() As String
        Get
            Return _posTax4Desc
        End Get
        Set(ByVal value As String)
            _posTax4Desc = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property TotalValue() As String
        Get
            Return _posTotalValue
        End Get
        Set(ByVal value As String)
            _posTotalValue = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property OldTransID() As String
        Get
            Return _posOldTransID
        End Get
        Set(ByVal value As String)
            _posOldTransID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PosTabNote() As String
        Get
            Return _PosTabNote
        End Get
        Set(ByVal value As String)
            _PosTabNote = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PosGiftReason() As String
        Get
            Return _posGiftReason
        End Get
        Set(ByVal value As String)
            _posGiftReason = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PosAuthorizedBy() As String
        Get
            Return _posAuthorizedBy
        End Get
        Set(ByVal value As String)
            _posAuthorizedBy = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PosPercentDiscount() As String
        Get
            Return _posPercentDiscount
        End Get
        Set(ByVal value As String)
            _posPercentDiscount = clsCommon.funRemove(value, True)
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

#Region "Function"
    ''' <summary>
    ''' Insert Transaction
    ''' </summary>
    ''' <param name="sID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertPosTransaction(ByRef sID As String) As Boolean
        Dim posDiscount As Double = 0D
        Double.TryParse(_posPercentDiscount, posDiscount)
        Dim strSQL As String
        strSQL = "INSERT INTO postransaction(posTransType, posTransDateTime, posTransRegCode, posTransUserID, posTransStatus, posSubTotal, posTax1, posTax1Desc, posTax2, posTax2Desc, posTax3,posTax3Desc, posTax4, posTax4Desc, posTotalValue,posOldTransID,PosTabNote,posGiftReason,posAuthorizedBy,posPercentDiscount) VALUES('"
        strSQL += _posTransType + "','"
        strSQL += _posTransDateTime + "','"
        strSQL += _posTransRegCode + "','"
        strSQL += _posTransUserID + "','"
        strSQL += _posTransStatus + "','"
        strSQL += _posSubTotal + "','"
        strSQL += _posTax1 + "','"
        strSQL += _posTax1Desc + "','"
        strSQL += _posTax2 + "','"
        strSQL += _posTax2Desc + "','"
        strSQL += _posTax3 + "','"
        strSQL += _posTax3Desc + "','"
        strSQL += _posTax4 + "','"
        strSQL += _posTax4Desc + "','"
        strSQL += _posTotalValue + "','"
        strSQL += _posOldTransID + "','"
        strSQL += _PosTabNote + "',"
        strSQL += _posGiftReason + ","
        strSQL += _posAuthorizedBy + ","
        strSQL += posDiscount.ToString() + ")"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        sID = obj.GetScalarData("Select LAST_INSERT_ID()")
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update Invoices
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateTransaction() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE postransaction set "
        strSQL += "posTransType='" + _posTransType + "', "
        strSQL += "posTransDateTime='" + _posTransDateTime + "', "
        strSQL += "posTransRegCode='" + _posTransRegCode + "', "
        strSQL += "posTransUserID='" + _posTransUserID + "', "
        strSQL += "posTransStatus='" + _posTransStatus + "', "
        strSQL += "posSubTotal='" + _posSubTotal + "', "
        strSQL += "posTax1='" + _posTax1 + "', "
        strSQL += "posTax1Desc='" + _posTax1Desc + "', "
        strSQL += "posTax2='" + _posTax2 + "', "
        strSQL += "posTax2Desc='" + _posTax2Desc + "', "
        strSQL += "posTax3='" + _posTax3 + "', "
        strSQL += "posTax3Desc='" + _posTax3Desc + "', "
        strSQL += "posTax4='" + _posTax4 + "', "
        strSQL += "posTax4Desc='" + _posTax4Desc + "', "
        strSQL += "posTotalValue='" + _posTotalValue + "' "
        strSQL += " where posTransId='" + _posTransId + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of Transaction
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetTransactionInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT posTransType, posTransDateTime, posTransRegCode, posTransUserID, posTransStatus, posSubTotal, posTax1, posTax1Desc, posTax2, posTax2Desc, posTax3,posTax3Desc, posTax4, posTax4Desc, posTotalValue FROM postransaction where posTransId='" + _posTransId + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _posTransType = drObj.Item("posTransType").ToString
            _posTransDateTime = drObj.Item("posTransDateTime").ToString
            _posTransRegCode = drObj.Item("posTransRegCode").ToString
            _posTransUserID = drObj.Item("posTransUserID").ToString
            _posTransStatus = drObj.Item("posTransStatus").ToString
            _posSubTotal = drObj.Item("posSubTotal").ToString
            _posTax1 = drObj.Item("posTax1").ToString
            _posTax1Desc = drObj.Item("posTax1Desc").ToString
            _posTax2 = drObj.Item("posTax2").ToString
            _posTax2Desc = drObj.Item("posTax2Desc").ToString
            _posTax3 = drObj.Item("posTax3").ToString
            _posTax3Desc = drObj.Item("posTax3Desc").ToString
            _posTax4 = drObj.Item("posTax4").ToString
            _posTax4Desc = drObj.Item("posTax4Desc").ToString
            _posTotalValue = drObj.Item("posTotalValue").ToString
        End While
        CloseDatabaseConnection()
        drObj.Close()
    End Sub
    ''' <summary>
    ''' Get the Max TransactionID from POS
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetMaxTransaction() As Integer
        Dim strSql, strMaxTraId As String
        strSql = "SELECT MAX(posTransId) FROM postransaction"
        strMaxTraId = Convert.ToString(GetScalarData(strSql))
        strMaxTraId = IIf(strMaxTraId = "", 0, strMaxTraId)
        CloseDatabaseConnection()
        Return strMaxTraId
    End Function
    ''' <summary>
    ''' Fill sub Transaction
    ''' </summary>
    ''' <param name="TransType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subFillTransaction(Optional ByVal TransType As String = "") As String
        Dim strSql As String = " select DISTINCT posTransId,posTransType, posTransDateTime,posTransStatus, posSubTotal,posTotalValue,posPayTypeDesc, concat( u.userFirstName,' ',u.userLastName) as userID "
        strSql += " ,PosTabNote FROM postransaction "
        strSql += " Left join posTransAcctHdr on posTransAcctHdr.posAcctTransID=postransaction.posTransId "
        strSql += " Left join pospaymenttype on posTransAcctHdr.posAcctPayType=pospaymenttype.posPayTypeID "
        strSql += " Inner join users u on u.userID = postransaction.posTransUserID WHERE 1=1  AND posTransStatus!=0" 'AND DATEDIFF(posTransDateTime,NOW())=0
        If TransType = "P" Then
            strSql += " AND posTransStatus=9"
        End If
        strSql += " order by posTransDateTime desc , posTransId,posTransStatus"
        Return strSql
    End Function   
    Public Function subFillTransactionByStatusTypeID(ByVal confirmCode As String, Optional ByVal statusTypeID As String = "") As String
        Dim strSql As String = " select DISTINCT posTransId,posTransType, posTransDateTime,posTransStatus, posSubTotal,posTotalValue,posPayTypeDesc, concat( u.userFirstName,' ',u.userLastName) as userID "
        strSql += " ,PosTabNote, PaidAmount AS PaidAmout FROM postransaction "
        strSql += " Left join posTransAcctHdr on posTransAcctHdr.posAcctTransID=postransaction.posTransId "
        strSql += " Left join pospaymenttype on posTransAcctHdr.posAcctPayType=pospaymenttype.posPayTypeID "
        strSql += " LEFT join users u on u.userID = postransaction.posTransUserID WHERE 1=1  AND posTransStatus!=0" 'AND DATEDIFF(posTransDateTime,NOW())=0
        If Not String.IsNullOrEmpty(statusTypeID) Then
            strSql += " AND posTransStatus IN ('" + statusTypeID + "')"
        End If
        If Not String.IsNullOrEmpty(confirmCode) Then
            strSql += " AND PosTabNote='" + confirmCode + "' "
        End If
        strSql += " order by posTransDateTime desc , posTransId,posTransStatus"
        Return strSql
    End Function
    ''' <summary>
    ''' Transaction Delete
    ''' </summary>
    ''' <param name="strTranID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funTransactionDelete(ByVal strTranID As String) As String
        Dim objDataClass As New clsDataClass
        Dim strSql As String = "delete from posTransAcctHdr where posAcctTransID ='" & strTranID & "'"
        objDataClass.SetData(strSql)
        strSql = "delete from posproductno where posPrdTransID='" & strTranID & "'"
        objDataClass.SetData(strSql)
        strSql = "delete from postransaction where posTransId='" & strTranID & "'"
        objDataClass.SetData(strSql)
        objDataClass = Nothing
        Return strSql
    End Function
    ''' <summary>
    ''' Product Tax
    ''' </summary>
    ''' <param name="strPrdID"></param>
    ''' <param name="strWhsCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funPrdTax(ByVal strPrdID As String, ByVal strWhsCode As String) As Double
        Dim dblTotal As Double = 0
        Dim strTaxCode As String
        Dim objTaxCode As Object
        Dim strSQL As String = ""
        strSQL = "SELECT prdTaxCode FROM prdquantity where prdID='" & strPrdID & "' and prdWhsCode='" & strWhsCode & "' "
        objTaxCode = Convert.ToString(GetScalarData(strSQL))
        If objTaxCode.ToString = "" Then
            strSQL = ""
            strSQL = "SELECT sysRegTaxCode FROM sysregister where sysRegWhsCode='" & strWhsCode & "'"
            objTaxCode = Convert.ToString(GetScalarData(strSQL))
            If objTaxCode.ToString = "" Then
                Return 0
            Else
                strTaxCode = objTaxCode.ToString
            End If
        Else
            strTaxCode = objTaxCode.ToString
        End If
        Return objTaxCode
    End Function
    ''' <summary>
    ''' Transaction ID exists or not s
    ''' </summary>
    ''' <param name="strTranID"></param>
    ''' <param name="strPrdUPCCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funIsTransactionID(ByVal strTranID As String, ByVal strPrdUPCCode As String) As Boolean
        Dim strSQL As String
        strSQL = " SELECT  Count(posProductNo) FROM posproductno "
        strSQL += " Inner join Products on Products.ProductID=posproductno.posProductID"
        strSQL += " Inner join posTransaction on posTransaction.posTransId=posproductno.posPrdTransID "
        strSQL += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID where posTransStatus='1' and posTransType='S' and descLang ='" + clsCommon.funGetProductLang() + "' and posPrdTransID='" + strTranID + "' and  prdUPCCode='" + strPrdUPCCode + "'"
        'strSQL += " and PosRefundQty < posPrdQty"
        'strSQL += " and posProductID not in (SELECT posProductID FROM posproductno Inner join posTransaction on posTransaction.posTransId=posproductno.posPrdTransID where PosoldTransID='" + strTranID + "')"
        strSQL += " OR posProductID IN (SELECT ProductID FROM prdtags WHERE Tag = '" & strPrdUPCCode & "')"
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.GetScalarData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Validate Transaction 
    ''' </summary>
    ''' <param name="transID"></param>
    ''' <param name="sKey"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidateTransactionID(ByVal transID As Integer, ByVal sKey As String) As Boolean
        Dim bRet As Boolean = False
        Dim sellPrdQty As Double = 0
        Dim refundPrdQty As Double = 0
        sKey = clsCommon.funRemove(sKey)
        Dim SELL_SQL1 As String = _
             "SELECT SUM(pn.posPrdQty) AS SellProductQty" + vbCrLf + _
             "  FROM posproductno pn" + vbCrLf + _
             " WHERE pn.posPrdTransID = {0}" + vbCrLf + _
             "       AND pn.posProductID IN (SELECT products.productID" + vbCrLf + _
             "                                 FROM products" + vbCrLf + _
             "                                WHERE products.prdUPCCode = '{1}');" 

        Dim SELL_SQL2 As String = _
             "SELECT SUM(pn.posPrdQty) AS SellProductQty" + vbCrLf + _
             "  FROM posproductno pn" + vbCrLf + _
             " WHERE pn.posPrdTransID = {0}" + vbCrLf + _
             "       AND pn.posProductID IN (SELECT ProductID" + vbCrLf + _
             "                                 FROM prdtags" + vbCrLf + _
             "                                WHERE Tag = '{1}');"

        SELL_SQL1 = String.Format(SELL_SQL1, transID, sKey)
        SELL_SQL2 = String.Format(SELL_SQL2, transID, sKey)

        Dim sell_dt1 As New DataTable()
        Dim sell_dt2 As New DataTable()

        Dim objDal As New clsDataClass()

        sell_dt1 = objDal.GetDataTable(SELL_SQL1)
        sell_dt2 = objDal.GetDataTable(SELL_SQL2)

        If sell_dt1.Rows.Count > 0 AndAlso Not sell_dt1.Rows(0)(0) Is DBNull.Value Then
            Integer.TryParse(sell_dt1.Rows(0)(0).ToString(), sellPrdQty)
        ElseIf sell_dt2.Rows.Count > 0 AndAlso Not sell_dt2.Rows(0)(0) Is DBNull.Value Then
            Integer.TryParse(sell_dt2.Rows(0)(0).ToString(), sellPrdQty)
        End If

        If sellPrdQty = 0 Then
            Return False
        End If

        Dim REF_SQL1 As String = _
             "SELECT SUM(pn.posPrdQty) AS RefundProductQty" + vbCrLf + _
             "  FROM    posproductno pn" + vbCrLf + _
             "       INNER JOIN" + vbCrLf + _
             "          postransaction pt" + vbCrLf + _
             "       ON pt.posTransId = pn.posPrdTransID" + vbCrLf + _
             " WHERE pt.posoldTransid = {0}" + vbCrLf + _
             "       AND pn.posProductID IN (SELECT products.productID" + vbCrLf + _
             "                                 FROM products" + vbCrLf + _
             "                                WHERE products.prdUPCCode = '{1}');"

        Dim REF_SQL2 As String = _
             "SELECT SUM(pn.posPrdQty) AS RefundProductQty" + vbCrLf + _
             "  FROM    posproductno pn" + vbCrLf + _
             "       INNER JOIN" + vbCrLf + _
             "          postransaction pt" + vbCrLf + _
             "       ON pt.posTransId = pn.posPrdTransID" + vbCrLf + _
             " WHERE pt.posoldTransid = {0}" + vbCrLf + _
             "       AND pn.posProductID IN (SELECT ProductID" + vbCrLf + _
             "                                 FROM prdtags" + vbCrLf + _
             "                                WHERE Tag = '{1}');"

        REF_SQL1 = String.Format(REF_SQL1, transID, sKey)
        REF_SQL2 = String.Format(REF_SQL2, transID, sKey)

        Dim ref_dt1 As New DataTable()
        Dim ref_dt2 As New DataTable()

        ref_dt1 = objDal.GetDataTable(REF_SQL1)
        ref_dt2 = objDal.GetDataTable(REF_SQL2)

        If ref_dt1.Rows.Count > 0 AndAlso Not ref_dt1.Rows(0)(0) Is DBNull.Value Then
            Integer.TryParse(ref_dt1.Rows(0)(0).ToString(), refundPrdQty)
        ElseIf ref_dt2.Rows.Count > 0 AndAlso Not ref_dt2.Rows(0)(0) Is DBNull.Value Then
            Integer.TryParse(ref_dt2.Rows(0)(0).ToString(), refundPrdQty)
        End If

        Return refundPrdQty < sellPrdQty
    End Function

    ''' <summary>
    ''' Update Invoices
    ''' </summary>
    ''' <param name="strTransID"></param>
    ''' <param name="strTID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateTransStaus(ByVal strTransID As String, Optional ByVal strTID As String = "") As Boolean
        Dim strSQL As String
        strSQL = "UPDATE postransaction set "
        strSQL += "posTransStatus='" + _posTransStatus + "' "
        If strTID <> "" Then
            strSQL += " ,posOldTransID='" + _posOldTransID + "' "
        End If
        strSQL += " where posTransId='" + strTransID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' For Reprinting
    ''' </summary>
    ''' <param name="strUser"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funReprint(ByVal strUser As String) As String
        'else  case posCardName when '' then '-' else ifnull(posCardName,'-') end
        Dim strSql As String = "select postransaction.posTransId,posTransType, posTransDateTime,posTransStatus, posSubTotal,postransaction.posTotalValue,posPayTypeDesc, concat( u.userFirstName,' ',u.userLastName) as userID,posAcctPayType "
        strSql += " ,Case posAcctPayType when '1' then 'Cash' when '6' then 'Cash' when '7' then 'Waste' when '2' then 'Visa' when '3' then 'Master Card' when '4' then 'American Express' when '5' then 'Interac' when '8' then 'Gift' when '9' then 'Staff Gift' end as PaymentBy "
        strSql += " FROM postransaction "
        strSql += " Left join posTransAcctHdr on posTransAcctHdr.posAcctTransID=postransaction.posTransId "
        strSql += " Left join postransacctdtl on postransacctdtl.posAcctHdrDtlId=posTransAcctHdr.posAcctHdrID "
        strSql += " Left join pospaymenttype on posTransAcctHdr.posAcctPayType=pospaymenttype.posPayTypeID "
        strSql += " Inner join users u on u.userID = postransaction.posTransUserID "
        strSql += " where posTransStatus NOT IN ('9','10','11','12')"
        If strUser = "" Then
            strSql += " and userID='" & HttpContext.Current.Session("UserID") & "'"
        End If
        strSql += " order by posTransDateTime desc"
        Return strSql
    End Function



    Public Function funInsertPosTransactionCutomerDetails(ByRef posTransID As String, ByRef posTransCustPhoneNo As String, ByRef posTransCustEmailID As String, ByRef strCustPickUpDate As String, ByRef strCustAdditionalRequest As String, ByRef partnerID As String) As Boolean
        Dim posDiscount As Double = 0D
        Double.TryParse(_posPercentDiscount, posDiscount)
        Dim strSQL As String
        strSQL = "INSERT INTO postransitioncustomerdetails(posTransitionID, CustPhoneNo, CustEmailID,PickUpDateTime, AdditionalRequest,PartnerID) VALUES('"
        strSQL += posTransID + "','"
        strSQL += posTransCustPhoneNo + "','"
        strSQL += posTransCustEmailID + "','"
        strSQL += strCustPickUpDate + "','"
        strSQL += strCustAdditionalRequest + "','"
        strSQL += partnerID + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function funUpdatePosTransaction() As Boolean
        Dim posDiscount As Double = 0D
        Double.TryParse(_posPercentDiscount, posDiscount)       
        Dim strSQL As String
        strSQL = "UPDATE postransaction set "
        strSQL += "posTransType='" + _posTransType + "', "
        strSQL += "posTransDateTime='" + _posTransDateTime + "', "
        strSQL += "posTransRegCode='" + _posTransRegCode + "', "
        strSQL += "posTransUserID='" + _posTransUserID + "', "
        strSQL += "posTransStatus='" + _posTransStatus + "', "
        strSQL += "posSubTotal='" + _posSubTotal + "', "
        strSQL += "posTax1='" + _posTax1 + "', "
        strSQL += "posTax1Desc='" + _posTax1Desc + "', "
        strSQL += "posTax2='" + _posTax2 + "', "
        strSQL += "posTax2Desc='" + _posTax2Desc + "', "
        strSQL += "posTax3='" + _posTax3 + "', "
        strSQL += "posTax3Desc='" + _posTax3Desc + "', "
        strSQL += "posTax4='" + _posTax4 + "', "
        strSQL += "posTax4Desc='" + _posTax4Desc + "', "
        strSQL += "posGiftReason='" + _posGiftReason + "', "
        strSQL += "posAuthorizedBy='" + _posAuthorizedBy + "', "
        strSQL += "posPercentDiscount='" + posDiscount.ToString() + "', "
        strSQL += "posTotalValue='" + _posTotalValue + "' "
        strSQL += " where posTransId='" + _posTransId + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function funPosTransactionDelete(ByVal strTranID As String) As String
        Dim objDataClass As New clsDataClass
        Dim strSql As String = "delete from posTransAcctHdr where posAcctTransID ='" & strTranID & "'"
        objDataClass.SetData(strSql)
        strSql = "delete from posproductno where posPrdTransID='" & strTranID & "'"
        objDataClass.SetData(strSql)        
        objDataClass = Nothing
        Return strSql
    End Function

    Public Function funUpdatePosTransactionTableNote(ByVal posTabNote As String, ByVal posTransId As String) As Boolean
        Dim strSQL As String
        strSQL = "UPDATE postransaction set "
        strSQL += "posTabNote='" + posTabNote + "' "
        strSQL += " where posTransId='" + posTransId + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Function funUpdateTransStausWithTime(ByVal strTransID As String, ByVal strStatusTypeID As String) As Boolean
        Dim strSQL As String
        strSQL = "UPDATE postransaction set "
        strSQL += "posTransStatus='" + strStatusTypeID + "' "        
        strSQL += " ,posTransDateTime=NOW() "
        strSQL += " where posTransId='" + strTransID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region
End Class

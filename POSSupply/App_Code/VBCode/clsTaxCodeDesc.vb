Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports Resources.Resource
'Imports clsCommon
Public Class clsTaxCodeDesc
	Inherits clsDataClass
	Private _sysTaxCodeDescID, _sysTaxCodeDescText As String
	Public Property SysTaxCodeDescID() As String
		Get
			Return _sysTaxCodeDescID
		End Get
		Set(ByVal value As String)
            _sysTaxCodeDescID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property SysTaxCodeDescText() As String
		Get
			Return _sysTaxCodeDescText
		End Get
		Set(ByVal value As String)
            _sysTaxCodeDescText = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert TaxCodeDesc
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertTaxCodeDesc() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO systaxcodedesc( sysTaxCodeDescText) VALUES('"
		strSQL += _sysTaxCodeDescText + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update TaxCodeDesc
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateTaxCodeDesc() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE systaxcodedesc set "
		strSQL += "sysTaxCodeDescText='" + _sysTaxCodeDescText + "'"
		strSQL += " where sysTaxCodeDescID='" + _sysTaxCodeDescID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of TaxCodeDesc
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getTaxCodeDescInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  sysTaxCodeDescText FROM systaxcodedesc where sysTaxCodeDescID='" + _sysTaxCodeDescID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_sysTaxCodeDescText = drObj.Item("sysTaxCodeDescText").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Check Duplicate Tax Code
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateTaxesCode() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM systaxcodedesc where sysTaxCodeDescID='" & _sysTaxCodeDescID & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Check Duplicate Tax Description
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateTaxesDescription() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM systaxcodedesc where "
        strSQL += "sysTaxCodeDescText='" & _sysTaxCodeDescText & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    '''  Delete Taxes
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteTaxes(ByVal DeleteID As String) As String
        Dim strSQL As String
        strSQL = "DELETE FROM systaxcodedesc WHERE sysTaxCodeDescID='" + DeleteID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid for Taxes
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        strSQL = "SELECT sysTaxCodeDescID, sysTaxCodeDescText FROM systaxcodedesc "
        strSQL += "order by sysTaxCodeDescText "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid for Taxes
    ''' </summary>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal SearchData As String) As String
        Dim strSQL As String
        SearchData = clsCommon.funRemove(SearchData)
        strSQL = "SELECT sysTaxCodeDescID, sysTaxCodeDescText FROM systaxcodedesc "
        If SearchData <> "" Then
            strSQL += " where (sysTaxCodeDescID='" & SearchData & "' or sysTaxCodeDescText like '%" & SearchData & "%') "
        End If
        strSQL += "order by sysTaxCodeDescText "
        Return strSQL
    End Function
    ''' <summary>
    ''' Populate Tax Group
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub PopulateTaxGroup(ByVal ddl As DropDownList)
        Dim strSQL As String
        strSQL = "SELECT sysTaxCodeDescID, sysTaxCodeDescText FROM systaxcodedesc "
        strSQL += "order by sysTaxCodeDescText "
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "sysTaxCodeDescText"
        ddl.DataValueField = "sysTaxCodeDescID"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.liTaxGroup
        ddl.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
End Class

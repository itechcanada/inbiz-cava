Imports Microsoft.VisualBasic
''Imports clsCommon
Imports System.Data.Odbc
Imports System.Data

Public Class clsPrdPOSCatgSel
    Inherits clsDataClass
    Private _productID, _prdPOSCatgID As String
#Region "Properties"
    Public Property ProductID() As String
        Get
            Return _productID
        End Get
        Set(ByVal value As String)
            _productID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdPOSCatgID() As String
        Get
            Return _prdPOSCatgID
        End Get
        Set(ByVal value As String)
            _prdPOSCatgID = clsCommon.funRemove(value, True)
        End Set
    End Property
#End Region

    ''' <summary>
    '''  Insert Product POS Category
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertPrdPOSCatSel() As Boolean
        Dim strSQL As String
        'strSQL = "SELECT Count(*) FROM prdPOSCatgSel WHERE productID = "
        'strSQL += _productID & " AND prdPOSCatgID= "
        'strSQL += _prdPOSCatgID & " "
        Dim intData As Int16 = 0
      
        'If (intData = 0) Then
        strSQL = "INSERT INTO prdPOSCatgSel(productID, prdPOSCatgID) Values ( "
        strSQL += _productID & ", "
        strSQL += _prdPOSCatgID & "); "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        intData = PopulateData(strSQL)
        CloseDatabaseConnection()
        'End If
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Delete Product POS Category
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function deletePrdPOSCatgSel() As Boolean
        Dim strSQL As String
        strSQL = " DELETE FROM prdposcatgsel WHERE productID = "
        strSQL += _productID
        Dim intData As Int16 = 0
        intData = SetData(strSQL)
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Fill Selected Text
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function selectPrdPOSCatg() As String
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT prdposcatgID FROM prdposcatgsel WHERE productID = "
        strSQL += _productID
        Dim strResult As String = ""
        drObj = GetDataReader(strSQL)
        Dim i As Int16 = 0
        While drObj.Read
            If (i > 0) Then
                strResult += "~"
            End If
            strResult += drObj.Item("prdposcatgID").ToString
            i = i + 1
        End While
        drObj.Close()
        CloseDatabaseConnection()
        Return strResult
    End Function
    ''' <summary>
    ''' Inserts POS Categories for given Product
    ''' </summary>
    ''' <param name="strCatgLst"></param>
    ''' <param name="strPrdID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funAssignPOSCatg(ByVal strCatgLst As String, ByVal strPrdID As String) As Boolean
        Dim strCatgID() As String
        Dim obj As New clsDataClass
        strCatgID = strCatgLst.Split("~")
        _productID = strPrdID

        'Deletes all existing categories for given productId
        deletePrdPOSCatgSel()
        Dim intData As Int16
        If strCatgID.Length >= 1 Then
            For intI As Integer = 0 To strCatgID.Length - 2
                _prdPOSCatgID = strCatgID(intI)
                insertPrdPOSCatSel()
            Next
        End If
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
End Class

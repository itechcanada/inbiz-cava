Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data.Odbc
Imports System.Data
Imports System.IO
Imports System.Xml

Namespace ERPService
    <WebService(Namespace:="http://tempuri.org/")> _
    Public Class Service
        Inherits System.Web.Services.WebService

        Private Sub GetDates(ByRef stDate As Date, ByRef endDate As Date)
            Dim offset As Double = 0
            Select Case Today.DayOfWeek
                Case DayOfWeek.Monday : offset = 0
                Case DayOfWeek.Tuesday : offset = 0   '-1
                Case DayOfWeek.Wednesday : offset = 0 '-2
                Case DayOfWeek.Thursday : offset = 0  '-3
                Case DayOfWeek.Friday : offset = 0    '-4
                Case DayOfWeek.Saturday : offset = 0  '-5
                Case DayOfWeek.Sunday : offset = 0    '-6
            End Select
            endDate = DateAdd(DateInterval.Day, offset, Today)
            stDate = DateAdd(DateInterval.Day, -7 + offset, Today)
        End Sub

        'Verfy Token
        Private Function funVerifyToken(ByVal strToken As String, ByVal strUserNo As String) As String
            Dim objDB As New clsDataClass
            objDB.OpenDatabaseConnection()
            strToken = strToken.Replace("'", "''")
            Dim strSql As String = "SELECT count(tokenUser) FROM token inner join users on userID=tokenUser where (tokenId='" & strToken & "') and tokenStatus='1' and adddate(tokenTime,INTERVAL 30 Minute) > now() and userID='" & strUserNo & "' "
            Dim strData As String
            strData = objDB.GetScalarData(strSql)
            If strData = 1 Then
                strSql = "Update token set tokenTime=now() where tokenId='" & strToken & "' and tokenStatus='1' and adddate(tokenTime,INTERVAL 30 Minute) > now()"
                objDB.PopulateData(strSql)
            Else
                Dim strXML As String = "<?xml version=""1.0"" encoding=""utf-8"" ?>"
                strXML += "<Message>Unauthorized Access</Message>"
                Return strXML
            End If
            Return "1"
        End Function

        <WebMethod()> _
        Public Function GetGraphByProduct(ByVal strUserNo As String, ByVal strToken As String, ByVal strFromDate As String, ByVal strToDate As String) As String


            Dim strTknVrify As String = funVerifyToken(strToken, strUserNo)
            If strTknVrify <> "1" Then
                Return strTknVrify
            End If

            Dim objDB As New clsDataClass
            objDB.OpenDatabaseConnection()

            ' strFromDate = "2009-04-27"
            'strToDate = "2009-05-27"
            Dim strFrom As Date
            Dim strTo As Date
            If (strFromDate = "" And strToDate = "") Then

                GetDates(strFrom, strTo)
                strFromDate = strFrom.ToString("yyyy-MM-dd")
                strToDate = strTo.ToString("yyyy-MM-dd")
            End If



            Dim strCompanyID As String = ""
            strCompanyID = "SELECT CompanyName FROM syscompanyinfo limit 1 "
            strCompanyID = objDB.GetScalarData(strCompanyID)

            Dim strCurr As String = "SELECT CompanyBasCur FROM syscompanyinfo where CompanyName='" & strCompanyID & "'"
            strCurr = objDB.GetScalarData(strCurr)

            Dim strSql As String = ""
            strSql = " where "
            strSql += " (ordCreatedOn > '" & strFromDate & " 00:00:00' and "
            strSql += " ordCreatedOn < '" & strToDate & " 23:59:59') "
            strSql += " and ordStatus<>'N' "
            strSql += " order by prdName, ordCreatedOn "
            'strSql = "SELECT v.*,distName FROM vwsalesbyproduct v left join distributor on distID=ordcustID " & strSql
            strSql = "SELECT v.*,PartnerLongName as distName FROM vwsalesbyproduct v left join partners on partnerID=ordcustID " & strSql

            Dim strdate As String = Now.Date.ToString()
            Dim strXML As String = "<?xml version=""1.0"" encoding=""utf-8"" ?><ByProduct>"

            strXML += "<FromDate>" & strFrom & "</FromDate>"
            strXML += "<ToDate>" & strTo & "</ToDate>"
            strXML += "<BaseCurrency>" & strCurr & "</BaseCurrency>"
            Dim flag As Boolean = False
            Dim prName As String = ""
            Dim strName As String = ""
            Dim strAmount As Double
            Dim amt As Double
            Dim drData As OdbcDataReader
            Dim count As Integer = 0
            drData = objDB.GetDataReader(strSql)
            Try
                While drData.Read

                    If (count = 10) Then
                        prName = "Others"
                        strName = "Others"
                        strAmount = strAmount + drData.Item("amount")
                        flag = True
                    Else
                        prName = drData.Item("prdName")
                        If (prName <> strName And flag = True) Then
                            count = count + 1
                            strXML += "<ProductName>" & strName & "</ProductName>"
                            strXML += "<Amount>" & strAmount & "</Amount>"
                            strAmount = 0
                            strAmount = drData.Item("amount")
                        Else
                            strAmount = strAmount + drData.Item("amount")
                            flag = True
                        End If
                        strName = drData.Item("prdName")
                        amt = drData.Item("amount")
                    End If

                End While
                If (prName = strName And flag = True) Then
                    strXML += "<ProductName>" & prName & "</ProductName>"
                    strXML += "<Amount>" & strAmount & "</Amount>"
                End If
            Catch ex As Exception

            End Try
            strXML += "</ByProduct>"
            drData.Close()
            objDB.CloseDatabaseConnection()
            objDB = Nothing

            Return strXML
        End Function

        <WebMethod()> _
        Public Function GetGraphByPerson(ByVal strUserNo As String, ByVal strToken As String, ByVal strFromDate As String, ByVal strToDate As String) As String


            Dim strTknVrify As String = funVerifyToken(strToken, strUserNo)
            If strTknVrify <> "1" Then
                Return strTknVrify
            End If

            Dim objDB As New clsDataClass
            objDB.OpenDatabaseConnection()

            Dim strFrom As Date
            Dim strTo As Date
            If (strFromDate = "" And strToDate = "") Then


                GetDates(strFrom, strTo)
                strFromDate = strFrom.ToString("yyyy-MM-dd")
                strToDate = strTo.ToString("yyyy-MM-dd")
            End If

            Dim strCompanyID As String = ""
            strCompanyID = "SELECT CompanyName FROM syscompanyinfo limit 1 "
            strCompanyID = objDB.GetScalarData(strCompanyID)


            Dim strCurr As String = "SELECT CompanyBasCur FROM syscompanyinfo where CompanyName='" & strCompanyID & "'"
            strCurr = objDB.GetScalarData(strCurr)

            Dim strSql As String = ""
            strSql = " where "
            strSql += " (ordCreatedOn > '" & strFromDate & " 00:00:00' and "
            strSql += " ordCreatedOn < '" & strToDate & " 23:59:59') "
            strSql += " and ordStatus<>'N'  Group By SalesPerson Order By SalesPerson Asc "
            strSql = "select ordCreatedOn ,ordStatus, userLoginID,SalesPerson,sum(amount)as amount from vwsalesbyproduct " & strSql


            Dim strXML As String = "<?xml version=""1.0"" encoding=""utf-8"" ?><ByPerson>"

            strXML += "<FromDate>" & strFrom & "</FromDate>"
            strXML += "<ToDate>" & strTo & "</ToDate>"
            strXML += "<BaseCurrency>" & strCurr & "</BaseCurrency>"
            Dim flag As Boolean = False
            Dim prName As String = ""
            Dim strName As String = ""
            Dim strAmount As Double
            Dim amt As Double
            Dim drData As OdbcDataReader
            drData = objDB.GetDataReader(strSql)
            Try
                While drData.Read
                    prName = drData.Item("SalesPerson")
                    If (prName <> strName And flag = True) Then
                        strXML += "<SalesPerson>" & strName & "</SalesPerson>"
                        strXML += "<Amount>" & strAmount & "</Amount>"
                        strAmount = 0
                        strAmount = drData.Item("amount")
                    Else
                        strAmount = strAmount + drData.Item("amount")
                        flag = True
                    End If
                    strName = drData.Item("SalesPerson")
                    amt = drData.Item("amount")
                End While
                If (prName = strName And flag = True) Then
                    strXML += "<SalesPerson>" & prName & "</SalesPerson>"
                    strXML += "<Amount>" & strAmount & "</Amount>"
                End If
            Catch ex As Exception

            End Try
            strXML += "</ByPerson>"
            drData.Close()
            objDB.CloseDatabaseConnection()
            objDB = Nothing

            Return strXML
        End Function

        <WebMethod()> _
        Public Function GetGraphByCustomer(ByVal strUserNo As String, ByVal strToken As String, ByVal strFromDate As String, ByVal strToDate As String) As String

            Dim strTknVrify As String = funVerifyToken(strToken, strUserNo)
            If strTknVrify <> "1" Then
                Return strTknVrify
            End If

            Dim objDB As New clsDataClass
            objDB.OpenDatabaseConnection()

            Dim strFrom As Date
            Dim strTo As Date
            If (strFromDate = "" And strToDate = "") Then


                GetDates(strFrom, strTo)
                strFromDate = strFrom.ToString("yyyy-MM-dd")
                strToDate = strTo.ToString("yyyy-MM-dd")
            End If

            Dim strCompanyID As String = ""
            strCompanyID = "SELECT CompanyName FROM syscompanyinfo limit 1 "
            strCompanyID = objDB.GetScalarData(strCompanyID)


            Dim strCurr As String = "SELECT CompanyBasCur FROM syscompanyinfo where CompanyName='" & strCompanyID & "'"
            strCurr = objDB.GetScalarData(strCurr)
            Dim strSql As String = ""
            strSql = " where "
            strSql += " (ordCreatedOn > '" & strFromDate & " 00:00:00' and "
            strSql += " ordCreatedOn < '" & strToDate & " 23:59:59') "
            strSql += " and ordStatus<>'N' "
            strSql += " group by CustomerName order by CustomerName, o.ordID desc"
            strSql = "SELECT distinct o.ordID, PartnerLongName as CustomerName, sum(ordProductQty * (ordProductUnitPrice*o.ordCurrencyExRate)) as amount, DATE_FORMAT(ordCreatedOn,'%m-%d-%Y') as ordDate, ordStatus FROM orders o inner join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID " & strSql


            Dim strXML As String = "<?xml version=""1.0"" encoding=""utf-8"" ?><ByCustomer>"

            strXML += "<FromDate>" & strFrom & "</FromDate>"
            strXML += "<ToDate>" & strTo & "</ToDate>"
            strXML += "<BaseCurrency>" & strCurr & "</BaseCurrency>"
            Dim flag As Boolean = False
            Dim prName As String = ""
            Dim strName As String = ""
            Dim strAmount As Double
            Dim amt As Double
            Dim drData As OdbcDataReader
            drData = objDB.GetDataReader(strSql)
            Try
                While drData.Read
                    prName = drData.Item("CustomerName")
                    If (prName <> strName And flag = True) Then
                        strXML += "<SalesCustomer>" & strName & "</SalesCustomer>"
                        strXML += "<Amount>" & strAmount & "</Amount>"
                        strAmount = 0
                        strAmount = drData.Item("amount")
                    Else
                        strAmount = strAmount + drData.Item("amount")
                        flag = True
                    End If
                    strName = drData.Item("CustomerName")
                    amt = drData.Item("amount")
                End While
                If (prName = strName And flag = True) Then
                    strXML += "<SalesCustomer>" & prName & "</SalesCustomer>"
                    strXML += "<Amount>" & strAmount & "</Amount>"
                End If
            Catch ex As Exception

            End Try
            strXML += "</ByCustomer>"
            drData.Close()
            objDB.CloseDatabaseConnection()
            objDB = Nothing

            Return strXML
        End Function

        <WebMethod()> _
        Public Function GetGraphByRegister(ByVal strUserNo As String, ByVal strToken As String, ByVal strFromDate As String, ByVal strToDate As String) As String


            Dim strTknVrify As String = funVerifyToken(strToken, strUserNo)
            If strTknVrify <> "1" Then
                Return strTknVrify
            End If

            Dim objDB As New clsDataClass
            objDB.OpenDatabaseConnection()

            Dim strFrom As Date
            Dim strTo As Date
            If (strFromDate = "" And strToDate = "") Then


                GetDates(strFrom, strTo)
                strFromDate = strFrom.ToString("yyyy-MM-dd")
                strToDate = strTo.ToString("yyyy-MM-dd")
            End If

            Dim strCompanyID As String = ""
            strCompanyID = "SELECT CompanyName FROM syscompanyinfo limit 1 "
            strCompanyID = objDB.GetScalarData(strCompanyID)


            Dim strCurr As String = "SELECT CompanyBasCur FROM syscompanyinfo where CompanyName='" & strCompanyID & "'"
            strCurr = objDB.GetScalarData(strCurr)

            Dim strSql As String = ""
            strFromDate = strFromDate & " 00:00:00"
            strToDate = strToDate & " 23:59:59"
            strSql = "select distinct sysRegCode, if(sum(posTotalValue) Is Null,0, sum(posTotalValue)) as amount from sysregister left join postransaction on posTransRegCode=sysRegCode and (posTransDateTime > '" & strFromDate & "' and  posTransDateTime < '" & strToDate & "') and posTransType='S' and posTransStatus='1' where sysRegActive=1  Group By sysRegCode Order BY posTransRegCode Asc"


            Dim strXML As String = "<?xml version=""1.0"" encoding=""utf-8"" ?><ByRegister>"

            strXML += "<FromDate>" & strFrom & "</FromDate>"
            strXML += "<ToDate>" & strTo & "</ToDate>"
            strXML += "<BaseCurrency>" & strCurr & "</BaseCurrency>"
            Dim flag As Boolean = False
            Dim prName As String = ""
            Dim strName As String = ""
            Dim strAmount As Double
            Dim amt As Double
            Dim drData As OdbcDataReader
            drData = objDB.GetDataReader(strSql)
            Try
                While drData.Read
                    prName = drData.Item("sysRegCode")
                    If (prName <> strName And flag = True) Then
                        strXML += "<SalesRegister>" & strName & "</SalesRegister>"
                        strXML += "<Amount>" & strAmount & "</Amount>"
                        strAmount = 0
                        strAmount = drData.Item("amount")

                    Else
                        strAmount = strAmount + drData.Item("amount")
                        flag = True
                    End If
                    strName = drData.Item("sysRegCode")
                    amt = drData.Item("amount")
                End While
                If (prName = strName And flag = True) Then
                    strXML += "<SalesRegister>" & prName & "</SalesRegister>"
                    strXML += "<Amount>" & strAmount & "</Amount>"
                End If
            Catch ex As Exception

            End Try
            strXML += "</ByRegister>"
            drData.Close()
            objDB.CloseDatabaseConnection()
            objDB = Nothing

            Return strXML
        End Function

    End Class
End Namespace
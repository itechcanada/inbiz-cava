Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon

Public Class clseComMetaTags
    Inherits clsDataClass
    Private _tagID, _title, _keywords, _tagDesc, _whsCode, _descLang As String
#Region "Properties"
    Public Property TagID() As String
        Get
            Return _tagID
        End Get
        Set(ByVal value As String)
            _tagID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DescLang() As String
        Get
            Return _descLang
        End Get
        Set(ByVal value As String)
            _descLang = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Title() As String
        Get
            Return _title
        End Get
        Set(ByVal value As String)
            _title = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Keywords() As String
        Get
            Return _keywords
        End Get
        Set(ByVal value As String)
            _keywords = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property TagDesc() As String
        Get
            Return _tagDesc
        End Get
        Set(ByVal value As String)
            _tagDesc = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property WhsCode() As String
        Get
            Return _whsCode
        End Get
        Set(ByVal value As String)
            _whsCode = clsCommon.funRemove(value, True)
        End Set
    End Property
#End Region
    ''' <summary>
    ''' Insert eComMetaTag
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInserteComMetaTag() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO eComMetaTags(descLang, title, keywords, tagDesc, whsCode) VALUES('en', "
        strSQL += "'" + _title + "','"
        strSQL += _keywords + "','"
        strSQL += _tagDesc + "','"
        strSQL += _whsCode + "') "
        strSQL = clsCommon.replaceSysNull(strSQL)
        Dim objDataClass As New clsDataClass
        Dim intData As Int16 = 0
        intData = objDataClass.PopulateData(strSQL)
        If intData <= 0 Then
            objDataClass.CloseDatabaseConnection()
            Return False
        Else
            Dim intTagID As Int32 = funGetTagID()
            strSQL = "INSERT INTO eComMetaTags(descLang,tagID,title, keywords, tagDesc, whsCode) VALUES('fr', "
            strSQL += "'" + intTagID.ToString() + "','"
            strSQL += _title + "','"
            strSQL += _keywords + "','"
            strSQL += _tagDesc + "','"
            strSQL += _whsCode + "') "
            strSQL = clsCommon.replaceSysNull(strSQL)
            objDataClass.PopulateData(strSQL)
            strSQL = "INSERT INTO eComMetaTags(descLang,tagID,title, keywords, tagDesc, whsCode) VALUES('sp',"
            strSQL += "'" + intTagID.ToString() + "','"
            strSQL += _title + "','"
            strSQL += _keywords + "','"
            strSQL += _tagDesc + "','"
            strSQL += _whsCode + "') "
            strSQL = clsCommon.replaceSysNull(strSQL)
            objDataClass.PopulateData(strSQL)
            objDataClass.CloseDatabaseConnection()
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update eComMetaTag
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateeComMetaTag() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE eComMetaTags set "
        strSQL += "title='" + _title + "', "
        strSQL += "keywords='" + _keywords + "', "
        strSQL += "tagDesc='" + _tagDesc + "', "
        strSQL += "whsCode='" + _whsCode + "' "
        strSQL += "where descLang ='" + _descLang + "'"
        strSQL = clsCommon.replaceSysNull(strSQL)
        Dim objDataClass As New clsDataClass
        Dim intData As Int16 = 0
        intData = objDataClass.PopulateData(strSQL)
        objDataClass.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of eComMetaTag
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGeteComMetaTagInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  title, keywords, tagDesc, whsCode FROM eComMetaTags where tagID='" + _tagID + "' and descLang ='" + _descLang + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _tagID = drObj.Item("tagID").ToString
            _title = drObj.Item("title").ToString
            _keywords = drObj.Item("keywords").ToString
            _tagDesc = drObj.Item("tagDesc").ToString
            _whsCode = drObj.Item("whsCode").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    Public Sub subGeteComMetaTagInfoByWhsCode()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT tagID,title, keywords, tagDesc,whsCode FROM eComMetaTags where whsCode='" + _whsCode + "' and descLang ='" + _descLang + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _tagID = drObj.Item("tagID").ToString
            _title = drObj.Item("title").ToString
            _keywords = drObj.Item("keywords").ToString
            _tagDesc = drObj.Item("tagDesc").ToString
            _whsCode = drObj.Item("whsCode").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    Public Function funGetDuplicateeComMetaTag() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM eComMetaTags where whsCode='" & _whsCode & "' and descLang ='" & clsCommon.funGetProductLang() & "'"
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    Public Function funGetTagID() As String
        Dim strSQL As String
        Dim strTagID As String = ""
        Dim drObj As OdbcDataReader
        strSQL = "SELECT tagID from eComMetaTags where whsCode='" + _whsCode + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            strTagID = drObj.Item("tagID").ToString()
        End While
        Return strTagID
    End Function
End Class

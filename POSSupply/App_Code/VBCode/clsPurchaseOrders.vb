Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Imports iTECH.InbizERP.BusinessLogic
Public Class clsPurchaseOrders
	Inherits clsDataClass
    Private _poID, _poDate, _poStatus, _poVendorID, _poShipVia, _poFOBLoc, _poShpTerms, _poNotes, _poCreatedByUserID, _poAuthroisedByUserID, _poFaxedEmailDateTime, _poCurrencyCode, _poCurrencyExRate, _poCompanyID, _poWhsCode As String
    Private _VendorName, _VendorFax, _VendorEmailID, _VendorCurrency, _Address, _Amount As String
    Private _poForSoNo As String
    Public Property PoID() As String
        Get
            Return _poID
        End Get
        Set(ByVal value As String)
            _poID = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Property PoDate() As String
		Get
			Return _poDate
		End Get
		Set(ByVal value As String)
            _poDate = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PoStatus() As String
		Get
			Return _poStatus
		End Get
		Set(ByVal value As String)
            _poStatus = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PoVendorID() As String
		Get
			Return _poVendorID
		End Get
		Set(ByVal value As String)
            _poVendorID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PoShipVia() As String
		Get
			Return _poShipVia
		End Get
		Set(ByVal value As String)
            _poShipVia = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PoFOBLoc() As String
		Get
			Return _poFOBLoc
		End Get
		Set(ByVal value As String)
            _poFOBLoc = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PoShpTerms() As String
		Get
			Return _poShpTerms
		End Get
		Set(ByVal value As String)
            _poShpTerms = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PoNotes() As String
		Get
			Return _poNotes
		End Get
		Set(ByVal value As String)
            _poNotes = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PoCreatedByUserID() As String
		Get
			Return _poCreatedByUserID
		End Get
		Set(ByVal value As String)
            _poCreatedByUserID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PoAuthroisedByUserID() As String
		Get
			Return _poAuthroisedByUserID
		End Get
		Set(ByVal value As String)
            _poAuthroisedByUserID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PoFaxedEmailDateTime() As String
		Get
			Return _poFaxedEmailDateTime
		End Get
		Set(ByVal value As String)
            _poFaxedEmailDateTime = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PoCurrencyCode() As String
		Get
			Return _poCurrencyCode
		End Get
		Set(ByVal value As String)
            _poCurrencyCode = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PoCurrencyExRate() As String
		Get
			Return _poCurrencyExRate
		End Get
		Set(ByVal value As String)
            _poCurrencyExRate = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property PoWhsCode() As String
        Get
            Return _poWhsCode
        End Get
        Set(ByVal value As String)
            _poWhsCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property VendorName() As String
        Get
            Return _VendorName
        End Get
        Set(ByVal value As String)
            _VendorName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property VendorFax() As String
        Get
            Return _VendorFax
        End Get
        Set(ByVal value As String)
            _VendorFax = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property VendorEmail() As String
        Get
            Return _VendorEmailID
        End Get
        Set(ByVal value As String)
            _VendorEmailID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property VendorCurrency() As String
        Get
            Return _VendorCurrency
        End Get
        Set(ByVal value As String)
            _VendorCurrency = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Address() As String
        Get
            Return _Address
        End Get
        Set(ByVal value As String)
            _Address = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Amount() As String
        Get
            Return _Amount
        End Get
        Set(ByVal value As String)
            _Amount = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property poCompanyID() As String
        Get
            Return _poCompanyID
        End Get
        Set(ByVal value As String)
            _poCompanyID = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Property PoForSoNo() As String
        Get
            Return _poForSoNo
        End Get
        Set(ByVal value As String)
            _poForSoNo = clsCommon.funRemove(value, True)
        End Set
    End Property

	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    '''  Insert PurchaseOrders
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertPurchaseOrders() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO purchaseorders(poDate, poStatus, poVendorID, poShipVia, poFOBLoc, poShpTerms, poNotes, poCreatedByUserID, poAuthroisedByUserID, poFaxedEmailDateTime, poCurrencyCode, poCurrencyExRate,poCompanyID,PoWhsCode, poForSoNo) VALUES('"
		strSQL += _poDate + "','"
		strSQL += _poStatus + "','"
		strSQL += _poVendorID + "','"
		strSQL += _poShipVia + "','"
		strSQL += _poFOBLoc + "','"
		strSQL += _poShpTerms + "','"
		strSQL += _poNotes + "','"
		strSQL += _poCreatedByUserID + "','"
		strSQL += _poAuthroisedByUserID + "','"
		strSQL += _poFaxedEmailDateTime + "','"
		strSQL += _poCurrencyCode + "','"
        strSQL += _poCurrencyExRate + "','"
        strSQL += _poCompanyID + "','"
        strSQL += _poWhsCode + "'"
        If _poForSoNo <> Nothing And _poForSoNo <> Nothing Then
            strSQL += "," & _poForSoNo + ")"
        Else
            strSQL += ")"
        End If

		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    '''  Update PurchaseOrders
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updatePurchaseOrders() As Boolean
		Dim strSQL As String
        strSQL = "UPDATE purchaseorders set "
		strSQL += "poDate='" + _poDate + "', "
		strSQL += "poStatus='" + _poStatus + "', "
		strSQL += "poVendorID='" + _poVendorID + "', "
		strSQL += "poShipVia='" + _poShipVia + "', "
		strSQL += "poFOBLoc='" + _poFOBLoc + "', "
		strSQL += "poShpTerms='" + _poShpTerms + "', "
		strSQL += "poNotes='" + _poNotes + "', "
		strSQL += "poCreatedByUserID='" + _poCreatedByUserID + "', "
		strSQL += "poAuthroisedByUserID='" + _poAuthroisedByUserID + "', "
		strSQL += "poFaxedEmailDateTime='" + _poFaxedEmailDateTime + "', "
		strSQL += "poCurrencyCode='" + _poCurrencyCode + "', "
        strSQL += "poCurrencyExRate='" + _poCurrencyExRate + "', "
        strSQL += "poCompanyID='" + _poCompanyID + "',"
        strSQL += "poWhsCode='" + _poWhsCode + "'"
		strSQL += " where poID='" + _poID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of PurchaseOrders
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPurchaseOrdersInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
        strSQL = "SELECT  poDate, poStatus, poVendorID, poShipVia, poFOBLoc, poShpTerms, poNotes, poCreatedByUserID, poAuthroisedByUserID, poFaxedEmailDateTime, poCurrencyCode, poCurrencyExRate,poCompanyID,poWhsCode FROM purchaseorders where poID='" + _poID + "' "
		drObj = GetDataReader(strSql)
		While drObj.Read
			_poDate = drObj.Item("poDate").ToString
			_poStatus = drObj.Item("poStatus").ToString
			_poVendorID = drObj.Item("poVendorID").ToString
			_poShipVia = drObj.Item("poShipVia").ToString
			_poFOBLoc = drObj.Item("poFOBLoc").ToString
			_poShpTerms = drObj.Item("poShpTerms").ToString
			_poNotes = drObj.Item("poNotes").ToString
			_poCreatedByUserID = drObj.Item("poCreatedByUserID").ToString
			_poAuthroisedByUserID = drObj.Item("poAuthroisedByUserID").ToString
			_poFaxedEmailDateTime = drObj.Item("poFaxedEmailDateTime").ToString
			_poCurrencyCode = drObj.Item("poCurrencyCode").ToString
            _poCurrencyExRate = drObj.Item("poCurrencyExRate").ToString
            _poCompanyID = drObj.Item("poCompanyID").ToString
            _poWhsCode = drObj.Item("poWhsCode").ToString
		End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub

    ''' <summary>
    ''' Populate objects of PurchaseOrders
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getPurchaseOrders()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT p.*, vendorName, vendorFax, vendorEmailID,poWhsCode, concat(addressLine1,'<br>' , addressLine2, '<br>' , addressLine3, '<br>' , addressCity, ', ', addressState, '<br>' , addressCountry, ', Postal Code: ', addressPostalCode) as Address, vendorCurrency, sum(poQty*poUnitPrice) as Amount,poCompanyID FROM purchaseorders p inner join vendor v on p.poVendorID=v.vendorID left join addresses a on v.vendorID=a.addressSourceID and a.addressType='B' and a.addressRef='V' inner join purchaseorderitems i on i.poID=p.poID where p.poID='" + _poID + "' group by p.poID "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _poDate = drObj.Item("poDate").ToString
            _poStatus = drObj.Item("poStatus").ToString
            _poVendorID = drObj.Item("poVendorID").ToString
            _poShipVia = drObj.Item("poShipVia").ToString
            _poFOBLoc = drObj.Item("poFOBLoc").ToString
            _poShpTerms = drObj.Item("poShpTerms").ToString
            _poNotes = drObj.Item("poNotes").ToString
            _poCreatedByUserID = drObj.Item("poCreatedByUserID").ToString
            _poAuthroisedByUserID = drObj.Item("poAuthroisedByUserID").ToString
            _poFaxedEmailDateTime = drObj.Item("poFaxedEmailDateTime").ToString
            _poCurrencyCode = drObj.Item("poCurrencyCode").ToString
            _poCurrencyExRate = drObj.Item("poCurrencyExRate").ToString
            _VendorName = drObj.Item("vendorName").ToString
            _VendorFax = drObj.Item("vendorFax").ToString
            _VendorEmailID = drObj.Item("vendorEmailID").ToString
            _VendorCurrency = drObj.Item("vendorCurrency").ToString
            _Address = drObj.Item("Address").ToString
            _Amount = drObj.Item("Amount").ToString
            _poCompanyID = drObj.Item("poCompanyID").ToString
            _poWhsCode = drObj.Item("poWhsCode").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Return PurchaseOrders Warehouse Code
    ''' </summary>
    ''' <param name="strPOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getPOShippingWarehouseCode(ByVal strPOID As String) As String
        Dim strSQL As String
        Dim strData As String = ""
        Dim drObj As OdbcDataReader
        strSQL = "SELECT distinct poItemShpToWhsCode FROM purchaseorderitems where poID='" + strPOID + "' order by poItems "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            strData = drObj.Item("poItemShpToWhsCode").ToString & ","
        End While
        drObj.Close()
        CloseDatabaseConnection()
        strData = strData.Substring(0, strData.Length - 1)
        Return strData
    End Function
    ''' <summary>
    ''' Fill Grid of PO
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        strSQL = "SELECT p.poID, v.vendorName, sum(poQty * poUnitPrice * poCurrencyExRate) as amount, DATE_FORMAT(poDate,'%m-%d-%Y') as poDate, poStatus FROM purchaseorders p inner join vendor v on p.poVendorID=v.vendorID inner join purchaseorderitems i on p.poID = i.poID where poStatus='N' group by p.poID order by p.poID desc "
        Return strSQL
    End Function

    ''' <summary>
    ''' Fill Grid of PO
    ''' </summary>
    ''' <param name="sStatus"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal sStatus As String) As String
        Dim strSQL As String
        strSQL = "SELECT p.poID, v.vendorName, sum(poQty * poUnitPrice * poCurrencyExRate) as amount, DATE_FORMAT(poDate,'%m-%d-%Y') as poDate, poStatus,v.vendorID FROM purchaseorders p inner join vendor v on p.poVendorID=v.vendorID inner join purchaseorderitems i on p.poID = i.poID  where 1=1 "
        If sStatus <> "" Then
            strSQL += " and poStatus='" & sStatus & "' "
        End If
        If HttpContext.Current.Request.QueryString("VendorID") <> "" Then
            strSQL += " And v.vendorID='" & HttpContext.Current.Request.QueryString("VendorID") & "' "
        End If
        strSQL += " group by p.poID order by p.poID desc "
        Return strSQL
    End Function

    ''' <summary>
    ''' Fill Grid of PO
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGeneratedPOListFillGrid(ByVal StatusData As String, ByVal SearchData As String) As String
        Dim strSQL As String
        strSQL = "SELECT p.poID, v.vendorName, sum(poQty * poUnitPrice * poCurrencyExRate) as amount, DATE_FORMAT(poDate,'%m-%d-%Y') as poDate, poStatus FROM purchaseorders p inner join vendor v on p.poVendorID=v.vendorID inner join purchaseorderitems i on p.poID = i.poID "
        strSQL += "inner join products as pro on i.poItemPrdId = pro.productID"
        strSQL += " where poStatus='N' "
        'Code for search by Product Barcode or Product Name
        If StatusData = ProductSearchFields.ProductBarCode And SearchData <> "" Then
            strSQL += " and pro.prdUPCCode  = '" + SearchData + "' "
        ElseIf StatusData = "PN" And SearchData <> "" Then
            strSQL += " and pro.prdName like '%" + SearchData + "%' "
        ElseIf StatusData = "PO" And SearchData <> "" Then
            strSQL += " and p.poID = '" + SearchData + "' "
        ElseIf StatusData = "VN" And SearchData <> "" Then
            strSQL += " and vendorName like '%" + SearchData + "%' "
        End If
        strSQL += " group by p.poID order by p.poID desc, vendorName "
        Return strSQL
    End Function

    ''' <summary>
    ''' Fill Grid of PO
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <param name="sPoStatus"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGeneratedPOListFillGrid(ByVal StatusData As String, ByVal SearchData As String, ByVal sPoStatus As String) As String
        Dim strSQL As String
        strSQL = "SELECT p.poID, v.vendorName, sum(poQty * poUnitPrice * poCurrencyExRate) as amount, DATE_FORMAT(poDate,'%m-%d-%Y') as poDate, poStatus FROM purchaseorders p inner join vendor v on p.poVendorID=v.vendorID inner join purchaseorderitems i on p.poID = i.poID "
        strSQL += "inner join products as pro on i.poItemPrdId = pro.productID where 1=1"

        If StatusData = "PN" And SearchData <> "" And sPoStatus <> "" Then
            strSQL += " And pro.prdName like '%" + SearchData + "%'  and poStatus='" & sPoStatus & "' "
        ElseIf StatusData = ProductSearchFields.ProductName And SearchData <> "" And sPoStatus = "" Then
            strSQL += " And pro.prdName like '%" + SearchData + "%' "
        ElseIf StatusData = ProductSearchFields.ProductName And SearchData = "" And sPoStatus <> "" Then
            strSQL += " And poStatus='" & sPoStatus & "' "

        ElseIf StatusData = ProductSearchFields.ProductBarCode And SearchData <> "" And sPoStatus <> "" Then
            strSQL += " And pro.prdUPCCode = '" + SearchData + "' and poStatus='" & sPoStatus & "' "
        ElseIf StatusData = ProductSearchFields.ProductBarCode And SearchData <> "" And sPoStatus = "" Then
            strSQL += " And pro.prdUPCCode = '" + SearchData + "' "
        ElseIf StatusData = ProductSearchFields.ProductBarCode And SearchData = "" And sPoStatus <> "" Then
            strSQL += " And poStatus='" & sPoStatus & "' "

        ElseIf StatusData = "PO" And SearchData <> "" And sPoStatus <> "" Then
            strSQL += " And p.poID = '" + SearchData + "' and poStatus='" & sPoStatus & "' "
        ElseIf StatusData = "VN" And SearchData <> "" And sPoStatus <> "" Then
            strSQL += " And vendorName like '%" + SearchData + "%' and poStatus='" & sPoStatus & "' "
        ElseIf StatusData = "PO" And SearchData <> "" And sPoStatus = "" Then
            strSQL += " And p.poID = '" + SearchData + "' "
        ElseIf StatusData = "VN" And SearchData <> "" And sPoStatus = "" Then
            strSQL += " And vendorName like '%" + SearchData + "%' "
        ElseIf StatusData = "PO" And SearchData = "" And sPoStatus <> "" Then
            strSQL += " And poStatus='" & sPoStatus & "' "
        ElseIf StatusData = "VN" And SearchData = "" And sPoStatus <> "" Then
            strSQL += " And poStatus='" & sPoStatus & "' "
        End If
        If HttpContext.Current.Request.QueryString("VendorID") <> "" Then
            strSQL += " And v.vendorID='" & HttpContext.Current.Request.QueryString("VendorID") & "' "
        End If

        strSQL += " group by p.poID order by p.poID desc, vendorName "
        Return strSQL
    End Function

    ''' <summary>
    '''  Delete PO
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <remarks></remarks>
    Public Sub funDeletePOItems(ByVal DeleteID As String)
        Dim strSQL As String
        strSQL = "DELETE FROM purchaseorderitems WHERE poID='" + DeleteID + "' "
        Dim obj As New clsDataClass
        obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
    End Sub

    ''' <summary>
    '''  Update PO
    ''' </summary>
    ''' <param name="POID"></param>
    ''' <param name="Status"></param>
    ''' <remarks></remarks>
    Public Sub funUpdatePO(ByVal POID As String, ByVal Status As Char)
        Dim strSQL As String
        strSQL = "Update purchaseorders set poStatus='" & Status & "',poShpTerms='" & _poShpTerms & "' WHERE poID='" + POID + "' "
        Dim obj As New clsDataClass
        obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
    End Sub

    ''' <summary>
    '''  Delete PO
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeletePO(ByVal DeleteID As String) As String
        Dim strSQL As String
        strSQL = "Update purchaseorders set poStatus='C' WHERE poID='" + DeleteID + "' "
        Return strSQL
    End Function

    ''' <summary>
    '''  Fill grid with search criteria
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal StatusData As String, ByVal SearchData As String) As String
        Dim strSQL As String
        Dim strLang As String = "en"
        strLang = HttpContext.Current.Session("Language")
        If strLang = "en-CA" Then
            strLang = "en"
        ElseIf strLang = "fr-CA" Then
            strLang = "fr"
        ElseIf strLang = "es-MX" Then
            strLang = "sp"
        Else
            strLang = "en"
        End If
        SearchData = clsCommon.funRemove(SearchData)
        strSQL = "SELECT p.productID, p.prdName, prdSmallDesc, prdLargeDesc, vendorID, vendorName, p.prdPOQty, pv.prdCostPrice FROM products as p inner join prddescriptions as pd on pd.id=p.productID and pd.descLang='" & strLang & "' inner join prdassociatevendor as pv on pv.prdID=p.productID Inner Join vendor as v on v.vendorID=pv.prdVendorID "
        If StatusData = "PI" And SearchData <> "" Then
            strSQL += " where p.productID = '" + SearchData + "' "
        ElseIf StatusData = ProductSearchFields.ProductName And SearchData <> "" Then
            strSQL += " where p.prdName like '%" + SearchData + "%' "
        ElseIf StatusData = ProductSearchFields.ProductDescription And SearchData <> "" Then
            strSQL += " where "
            strSQL += " ("
            strSQL += " prdSmallDesc like '%" + SearchData + "%' or "
            strSQL += " prdLargeDesc like '%" + SearchData + "%' "
            strSQL += " )"
        ElseIf StatusData = "VN" And SearchData <> "" Then
            strSQL += " where vendorName like '%" + SearchData + "%' "
        End If
        strSQL += " order by p.prdName, vendorName "
        Return strSQL
    End Function

    ''' <summary>
    ''' Fill Grid for Update PO
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdatePOListFillGrid(ByVal StatusData As String, ByVal SearchData As String) As String
        Dim strSQL As String
        SearchData = clsCommon.funRemove(SearchData)
        strSQL = "SELECT p.poID, v.vendorID, v.vendorName, sum(poQty * poUnitPrice * poCurrencyExRate) as amount, DATE_FORMAT(poDate,'%m-%d-%Y') as poDate, p.poStatus, p.poNotes FROM purchaseorders p inner join vendor v on p.poVendorID=v.vendorID inner join purchaseorderitems i on p.poID = i.poID "
        If StatusData = "PO" And SearchData <> "" Then
            strSQL += " where p.poID = '" + SearchData + "' "
        ElseIf StatusData = "VN" And SearchData <> "" Then
            strSQL += " where v.vendorName like '%" + SearchData + "%' "
        End If
        strSQL += " group by p.poID order by p.poDate asc "
        Return strSQL
    End Function

    ''' <summary>
    ''' Update PurchaseOrders Status
    ''' </summary>
    ''' <param name="strData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updatePurchaseOrders(ByVal strData As String) As Boolean
        Dim strSQL As String
        strSQL = "UPDATE purchaseorders set "
        strSQL += "poStatus='" + _poStatus + "', "
        strSQL += "poNotes='" + _poNotes + "', "
        strSQL += "poShipVia='" + _poShipVia + "', "
        strSQL += "poFOBLoc='" + _poFOBLoc + "', "
        strSQL += "poAuthroisedByUserID='" + _poAuthroisedByUserID + "' "
        strSQL += " where poID='" + _poID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Fill grid with search criteria
    ''' </summary>
    ''' <param name="strVendorID"></param>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal strVendorID As String, ByVal StatusData As String, ByVal SearchData As String) As String
        Dim strSQL As String
        Dim strLang As String = "en"
        strLang = HttpContext.Current.Session("Language")
        If strLang = "en-CA" Then
            strLang = "en"
        ElseIf strLang = "fr-CA" Then
            strLang = "fr"
        ElseIf strLang = "es-MX" Then
            strLang = "sp"
        Else
            strLang = "en"
        End If
        SearchData = clsCommon.funRemove(SearchData)
        strSQL = "SELECT p.productID, p.prdName, p.prdIntID, p.prdExtID, p.prdUPCCode, prdSmallDesc, prdLargeDesc, vendorID, vendorName, p.prdPOQty, pv.prdCostPrice FROM products as p inner join prddescriptions as pd on pd.id=p.productID and pd.descLang='" & strLang & "' left join prdassociatevendor as pv on pv.prdID=p.productID left Join vendor as v on v.vendorID=pv.prdVendorID "
        If strVendorID <> "" Then
            'strSQL += " and v.vendorID=" & strVendorID & " "
        End If
        If StatusData = CustomerSearchFields.PartnerId And SearchData <> "" Then
            strSQL += " where p.productID = '" + SearchData + "' "
        ElseIf StatusData = ProductSearchFields.ProductName And SearchData <> "" Then
            strSQL += " where p.prdName like '%" + SearchData + "%' "
        ElseIf StatusData = ProductSearchFields.ProductInternalID And SearchData <> "" Then
            strSQL += " where p.prdIntID = '" + SearchData + "' "
        ElseIf StatusData = ProductSearchFields.ProductExternalID And SearchData <> "" Then
            strSQL += " where p.prdExtID = '" + SearchData + "' "
        ElseIf StatusData = ProductSearchFields.ProductBarCode And SearchData <> "" Then
            strSQL += " where p.prdUPCCode = '" + SearchData + "' "
        ElseIf StatusData = ProductSearchFields.ProductDescription And SearchData <> "" Then
            strSQL += " where "
            strSQL += " ("
            strSQL += " prdSmallDesc like '%" + SearchData + "%' or "
            strSQL += " prdLargeDesc like '%" + SearchData + "%' "
            strSQL += " )"
        ElseIf StatusData = "VN" And SearchData <> "" Then
            strSQL += " where vendorName like '%" + SearchData + "%' "
        End If
        strSQL += " group by p.productID order by p.prdName, vendorName "
        Return strSQL
    End Function

    Public Function GetProductDetailSql(ByVal strProductId As String) As String
        Dim strSQL As String
        Dim strLang As String = "en"
        strLang = HttpContext.Current.Session("Language")
        If strLang = "en-CA" Then
            strLang = "en"
        ElseIf strLang = "fr-CA" Then
            strLang = "fr"
        ElseIf strLang = "es-MX" Then
            strLang = "sp"
        Else
            strLang = "en"
        End If
        strProductId = clsCommon.funRemove(strProductId)
        strSQL = "SELECT p.productID, p.prdName, p.prdIntID, p.prdExtID, p.prdUPCCode, prdSmallDesc, prdLargeDesc, vendorID, vendorName, p.prdPOQty, pv.prdCostPrice FROM products as p inner join prddescriptions as pd on pd.id=p.productID and pd.descLang='" & strLang & "' left join prdassociatevendor as pv on pv.prdID=p.productID left Join vendor as v on v.vendorID=pv.prdVendorID "
        strSQL += " where p.productID = '" + strProductId + "' "

        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid of PO
    ''' </summary>
    ''' <param name="sPoID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateReceiving(ByVal sPoID As String) As Boolean
        Dim strSQL As String
        Dim bFlag As Boolean = False
        strSQL = "SELECT distinct poID, poItemShpToWhsCode FROM purchaseorderitems where poID='" & sPoID & "' order by poID "
        Dim drObj As OdbcDataReader
        drObj = GetDataReader(strSQL)
        Dim objRcv As New clsReceiving
        While drObj.Read
            objRcv.RecDateTime = Now.AddDays(7).ToString("yyyy-MM-dd 11:00:00")
            objRcv.RecPOID = drObj.Item("poID").ToString
            objRcv.RecAtWhsCode = drObj.Item("poItemShpToWhsCode").ToString
            objRcv.RecByUserID = ""
            bFlag = objRcv.insertReceiving()
            If bFlag = True Then
                Exit While
            End If
        End While
        objRcv = Nothing
        drObj.Close()
        CloseDatabaseConnection()
        Return True
    End Function
    ''' <summary>
    ''' Fill Grid of PO
    ''' </summary>
    ''' <param name="sPoID"></param>
    ''' <param name="sRecDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateReceiving(ByVal sPoID As String, ByVal sRecDate As String) As Boolean
        Dim strSQL As String
        Dim bFlag As Boolean = False
        Dim bRcvPoExistFlag As Boolean = False
        strSQL = "SELECT distinct poID, poItemShpToWhsCode FROM purchaseorderitems where poID='" & sPoID & "' order by poID "
        Dim drObj As OdbcDataReader
        drObj = GetDataReader(strSQL)
        Dim objRcv As New clsReceiving
        bRcvPoExistFlag = objRcv.funCheckRecevingForPOExist(sPoID)
        While drObj.Read
            objRcv.RecDateTime = DateTime.ParseExact(sRecDate, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd HH:mm:ss")
            objRcv.RecPOID = drObj.Item("poID").ToString
            objRcv.RecAtWhsCode = drObj.Item("poItemShpToWhsCode").ToString
            objRcv.RecByUserID = ""
            If bRcvPoExistFlag = True Then
                objRcv.RecID = objRcv.funGetRecevingIDForPO(sPoID)
                bFlag = objRcv.updateReceiving()
            Else
                bFlag = objRcv.insertReceiving()
            End If
            If bFlag = True Then
                Exit While
            End If
        End While
        objRcv = Nothing
        drObj.Close()
        CloseDatabaseConnection()
        Return True
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports iTECH.InbizERP.BusinessLogic
''Imports System.Web.HttpContext
''Imports clsCommon
'Imports Resources.Resource
Public Class clsEndClient
    Inherits clsDataClass
    Private _endclientID, _endclientName, _endclientPhone, _endclientFax, _endclientEmail, _endclientType, _endclientActive, _endclientValidated, _endclientTaxCode, _endclientCommissionCode, _endclientDiscount, _endclientCurrencyCode, _endclientInvoiceNetTerms, _endclientShipBlankPref, _endclientCourierCode, _endclientLang, _endclientStatus, _endCompanyID As String
    Public Property EndClientID() As String
        Get
            Return _endclientID
        End Get
        Set(ByVal value As String)
            _endclientID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientName() As String
        Get
            Return _endclientName
        End Get
        Set(ByVal value As String)
            _endclientName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientPhone() As String
        Get
            Return _endclientPhone
        End Get
        Set(ByVal value As String)
            _endclientPhone = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientFax() As String
        Get
            Return _endclientFax
        End Get
        Set(ByVal value As String)
            _endclientFax = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientEmail() As String
        Get
            Return _endclientEmail
        End Get
        Set(ByVal value As String)
            _endclientEmail = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientType() As String
        Get
            Return _endclientType
        End Get
        Set(ByVal value As String)
            _endclientType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientActive() As String
        Get
            Return _endclientActive
        End Get
        Set(ByVal value As String)
            _endclientActive = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientValidated() As String
        Get
            Return _endclientValidated
        End Get
        Set(ByVal value As String)
            _endclientValidated = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientTaxCode() As String
        Get
            Return _endclientTaxCode
        End Get
        Set(ByVal value As String)
            _endclientTaxCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientCommissionCode() As String
        Get
            Return _endclientCommissionCode
        End Get
        Set(ByVal value As String)
            _endclientCommissionCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientDiscount() As String
        Get
            Return _endclientDiscount
        End Get
        Set(ByVal value As String)
            _endclientDiscount = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientCurrencyCode() As String
        Get
            Return _endclientCurrencyCode
        End Get
        Set(ByVal value As String)
            _endclientCurrencyCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientInvoiceNetTerms() As String
        Get
            Return _endclientInvoiceNetTerms
        End Get
        Set(ByVal value As String)
            _endclientInvoiceNetTerms = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientShipBlankPref() As String
        Get
            Return _endclientShipBlankPref
        End Get
        Set(ByVal value As String)
            _endclientShipBlankPref = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientCourierCode() As String
        Get
            Return _endclientCourierCode
        End Get
        Set(ByVal value As String)
            _endclientCourierCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientLanguage() As String
        Get
            Return _endclientLang
        End Get
        Set(ByVal value As String)
            _endclientLang = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndClientStatus() As String
        Get
            Return _endclientStatus
        End Get
        Set(ByVal value As String)
            _endclientStatus = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndCompanyID() As String
        Get
            Return _endCompanyID
        End Get
        Set(ByVal value As String)
            _endCompanyID = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Insert EndClient
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertEndClient() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO endclient( endclientName, endclientPhone, endclientFax, endclientEmail, endclientType, endclientActive, endclientValidated, endclientTaxCode, endclientCommissionCode, endclientDiscount, endclientCurrencyCode, endclientInvoiceNetTerms, endclientShipBlankPref, endclientCourierCode, endclientLang, endclientStatus,endCompanyID ) VALUES('"
        strSQL += _endclientName + "','"
        strSQL += _endclientPhone + "','"
        strSQL += _endclientFax + "','"
        strSQL += _endclientEmail + "','"
        strSQL += _endclientType + "','"
        strSQL += _endclientActive + "','"
        strSQL += _endclientValidated + "','"
        strSQL += _endclientTaxCode + "','"
        strSQL += _endclientCommissionCode + "','"
        strSQL += _endclientDiscount + "','"
        strSQL += _endclientCurrencyCode + "','"
        strSQL += _endclientInvoiceNetTerms + "','"
        strSQL += _endclientShipBlankPref + "','"
        strSQL += _endclientCourierCode + "','"
        strSQL += _endclientLang + "','"
        strSQL += _endclientStatus + "','"
        strSQL += _endCompanyID + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update EndClient
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateEndClient() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE endclient set "
        strSQL += "endclientName='" + _endclientName + "', "
        strSQL += "endclientPhone='" + _endclientPhone + "', "
        strSQL += "endclientFax='" + _endclientFax + "', "
        strSQL += "endclientEmail='" + _endclientEmail + "', "
        strSQL += "endclientType='" + _endclientType + "', "
        strSQL += "endclientActive='" + _endclientActive + "', "
        strSQL += "endclientValidated='" + _endclientValidated + "', "
        strSQL += "endclientTaxCode='" + _endclientTaxCode + "', "
        strSQL += "endclientCommissionCode='" + _endclientCommissionCode + "', "
        strSQL += "endclientDiscount='" + _endclientDiscount + "', "
        strSQL += "endclientCurrencyCode='" + _endclientCurrencyCode + "', "
        strSQL += "endclientInvoiceNetTerms='" + _endclientInvoiceNetTerms + "', "
        strSQL += "endclientShipBlankPref='" + _endclientShipBlankPref + "', "
        strSQL += "endclientCourierCode='" + _endclientCourierCode + "', "
        strSQL += "endclientLang='" + _endclientLang + "', "
        strSQL += "endCompanyID ='" + _endCompanyID + "', "
        strSQL += "endclientStatus='" + _endclientStatus + "'"
        strSQL += " where endclientID='" + _endclientID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of EndClient
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getEndClientInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  endclientName, endclientPhone, endclientFax, endclientEmail, endclientType, endclientActive, endclientValidated, endclientTaxCode, endclientCommissionCode, endclientDiscount, endclientCurrencyCode, endclientInvoiceNetTerms, endclientShipBlankPref, endclientCourierCode, endclientLang, endclientStatus,endCompanyID FROM endclient where endclientID='" + _endclientID + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _endclientName = drObj.Item("endclientName").ToString
            _endclientPhone = drObj.Item("endclientPhone").ToString
            _endclientFax = drObj.Item("endclientFax").ToString
            _endclientEmail = drObj.Item("endclientEmail").ToString
            _endclientType = drObj.Item("endclientType").ToString
            _endclientActive = drObj.Item("endclientActive").ToString
            _endclientValidated = drObj.Item("endclientValidated").ToString
            _endclientTaxCode = drObj.Item("endclientTaxCode").ToString
            _endclientCommissionCode = drObj.Item("endclientCommissionCode").ToString
            _endclientDiscount = drObj.Item("endclientDiscount").ToString
            _endclientCurrencyCode = drObj.Item("endclientCurrencyCode").ToString
            _endclientInvoiceNetTerms = drObj.Item("endclientInvoiceNetTerms").ToString
            _endclientShipBlankPref = drObj.Item("endclientShipBlankPref").ToString
            _endclientCourierCode = drObj.Item("endclientCourierCode").ToString
            _endclientLang = drObj.Item("endclientLang").ToString
            _endclientStatus = drObj.Item("endclientStatus").ToString
            _endCompanyID = drObj.Item("endCompanyID").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Return EndClient ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetEndClientID() As String
        Dim strSQL As String
        strSQL = "SELECT endclientID FROM endclient where endclientName='" & _endclientName & "' and endclientEmail='" & _endclientEmail & "' "
        Dim strData As String = ""
        strData = GetScalarData(strSQL)
        Return strData
    End Function
    ''' <summary>
    ''' Check Duplicate EndClient
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateEndClient() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM endclient where endclientName='" & _endclientName & "' and endclientEmail='" & _endclientEmail & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    '''  Delete EndClient
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteEndClient(ByVal DeleteID As String) As String
        Dim strSQL As String
        strSQL = "UPDATE endclient set endclientActive=0 WHERE endclientID='" + DeleteID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill grid
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT endclientID, endclientName, endclientPhone, endclientFax, endclientEmail FROM endclient where endclientActive=1 order by endclientName "
        Else
            strSQL = "SELECT endclientID, endclientName, endclientPhone, endclientFax, endclientEmail FROM endclient inner join salesrepcustomer s on s.CustomerID=endclientID and s.CustomerType='" & AddressReference.END_CLIENT & "' and s.UserID='" & HttpContext.Current.Session("UserID") & "' where endclientActive=1 order by endclientName "
        End If

        Return strSQL
    End Function
    ''' <summary>
    ''' Fill grid with search criteria
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal StatusData As String, ByVal SearchData As String) As String
        Dim strSQL As String
        SearchData = clsCommon.funRemove(SearchData)
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT endclientID, endclientName, endclientPhone, endclientFax, endclientEmail FROM endclient where endclientActive=1 "
        Else
            strSQL = "SELECT endclientID, endclientName, endclientPhone, endclientFax, endclientEmail FROM endclient left join salesrepcustomer s on s.CustomerID=endclientID and s.CustomerType='" & AddressReference.END_CLIENT & "' and s.UserID='" & HttpContext.Current.Session("UserID") & "' where endclientActive=1 "
        End If

        If StatusData <> "" And SearchData <> "" Then
            strSQL += " and endclientStatus='" + StatusData + "' and "
            strSQL += " ("
            strSQL += " endclientName like '%" + SearchData + "%' or "
            strSQL += " endclientPhone like '%" + SearchData + "%' or "
            strSQL += " endclientEmail like '%" + SearchData + "%' "
            strSQL += " )"
        ElseIf StatusData <> "" And SearchData = "" Then
            strSQL += " and endclientStatus='" + StatusData + "' and "
            strSQL += " ("
            strSQL += " endclientName like '%" + SearchData + "%' or "
            strSQL += " endclientPhone like '%" + SearchData + "%' or "
            strSQL += " endclientEmail like '%" + SearchData + "%' "
            strSQL += " )"
        ElseIf StatusData = "" Then
            strSQL += " and "
            strSQL += " ("
            strSQL += " endclientName like '%" + SearchData + "%' or "
            strSQL += " endclientPhone like '%" + SearchData + "%' or "
            strSQL += " endclientEmail like '%" + SearchData + "%' "
            strSQL += " )"
        End If
        strSQL += " order by endclientName "
        Return strSQL
    End Function
    ''' <summary>
    ''' Populate EndClient
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub PopulateEndClient(ByVal ddl As DropDownList)
        Dim strSQL As String
        strSQL = "SELECT endclientID, endclientName FROM endclient WHERE endclientActive='1' order by endclientName "
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "endclientName"
        ddl.DataValueField = "endclientID"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.SelectEndClient
        ddl.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub

    ''' <summary>
    ''' Populate SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <remarks></remarks>
    Public Sub PopulateSR(ByVal chklst As CheckBoxList)
        Dim strSQL As String
        strSQL = "SELECT userID, concat(userFirstName, ' ',userLastName) as Name FROM users WHERE userActive='1' and userModules like '%SR%' order by Name "
        chklst.Items.Clear()
        chklst.DataSource = GetDataReader(strSQL)
        chklst.DataTextField = "Name"
        chklst.DataValueField = "userID"
        chklst.DataBind()
        CloseDatabaseConnection()
    End Sub

    ''' <summary>
    ''' Populate User's SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <param name="EndClientID"></param>
    ''' <remarks></remarks>
    Public Sub PopulateUserSR(ByVal chklst As CheckBoxList, ByVal EndClientID As String)
        Dim strSQL As String
        strSQL = "SELECT u.userID, concat(userFirstName, ' ',userLastName) as Name, 'A' as seq FROM users u inner join salesrepcustomer s on s.UserID=u.userID and CustomerID='" & EndClientID & "' and CustomerType='E' WHERE userActive='1' and userModules like '%SR%' Union SELECT u.userID, concat(userFirstName, ' ',userLastName) as Name, 'B' as seq FROM users u WHERE userActive='1' and userModules like '%SR%' and u.userID not in (Select UserID from salesrepcustomer where CustomerID='" & EndClientID & "' and CustomerType='" & AddressReference.END_CLIENT & "') order by seq, Name"
        chklst.Items.Clear()
        chklst.DataSource = GetDataReader(strSQL)
        chklst.DataTextField = "Name"
        chklst.DataValueField = "userID"
        chklst.DataBind()
        CloseDatabaseConnection()

        Dim SR As String = GetUserSR(EndClientID)
        For i As Int16 = 0 To chklst.Items.Count - 1
            If SR.Contains("~" & chklst.Items(i).Value & "~") Then
                chklst.Items(i).Selected = True
            End If
        Next
    End Sub

    ''' <summary>
    ''' Get User's SR
    ''' </summary>
    ''' <param name="EndClientID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetUserSR(ByVal EndClientID As String) As String
        Dim strSQL As String
        strSQL = "SELECT u.userID FROM users u inner join salesrepcustomer s on s.UserID=u.userID and CustomerID='" & EndClientID & "' and CustomerType='E' WHERE userActive='1' and userModules like '%SR%' "
        Dim SR As String = "~"
        Dim dr As OdbcDataReader = GetDataReader(strSQL)
        While dr.Read
            SR += dr.Item(0) & "~"
        End While
        CloseDatabaseConnection()
        Return SR
    End Function

    ''' <summary>
    ''' Insert SRs
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <param name="EndClientID"></param>
    ''' <remarks></remarks>
    Public Sub InsertSR(ByVal chklst As CheckBoxList, ByVal EndClientID As String)
        Dim objSR As New clsSalesRepCustomer
        Dim i As Integer
        For i = 0 To chklst.Items.Count - 1
            If chklst.Items(i).Selected = True Then
                objSR.UserID = chklst.Items(i).Value
                objSR.CustID = EndClientID
                objSR.CustType = AddressReference.END_CLIENT
                objSR.insertSalesRepCustomer()
            End If
        Next
    End Sub

    ''' <summary>
    ''' Update SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <param name="EndClientID"></param>
    ''' <remarks></remarks>
    Public Sub UpdateSR(ByVal chklst As CheckBoxList, ByVal EndClientID As String)
        Dim objSR As New clsSalesRepCustomer
        objSR.CustID = EndClientID
        objSR.CustType = AddressReference.END_CLIENT
        objSR.DeleteSalesRepCustomer()

        For i As Int16 = 0 To chklst.Items.Count - 1
            If chklst.Items(i).Selected = True Then
                objSR.UserID = chklst.Items(i).Value
                objSR.CustID = EndClientID
                objSR.CustType = AddressReference.END_CLIENT
                objSR.insertSalesRepCustomer()
            End If
        Next
    End Sub
    ''' <summary>
    ''' Return New EndClient ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetNewEndClientID() As String
        Dim strSQL As String
        strSQL = "SELECT Max(endclientID) FROM endclient "
        Dim strData As String = ""
        strData = GetScalarData(strSQL)
        Return strData
    End Function
End Class

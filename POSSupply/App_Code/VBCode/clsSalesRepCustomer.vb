Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsSalesRepCustomer
	Inherits clsDataClass
    Private _salesRepCustID, _UserID, _custID, _custType As String
	Public Property SalesRepCustID() As String
		Get
			Return _salesRepCustID
		End Get
		Set(ByVal value As String)
            _salesRepCustID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserID() As String
		Get
			Return _UserID
		End Get
		Set(ByVal value As String)
            _UserID = clsCommon.funRemove(value, True)
		End Set
	End Property
    Public Property CustID() As String
        Get
            Return _custID
        End Get
        Set(ByVal value As String)
            _custID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustType() As String
        Get
            Return _custType
        End Get
        Set(ByVal value As String)
            _custType = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert SalesRepCustomer
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertSalesRepCustomer() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO salesrepcustomer( UserID, CustomerID, CustomerType) VALUES('"
		strSQL += _UserID + "','"
        strSQL += _custID + "','"
        strSQL += _custType + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update SalesRepCustomer
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateSalesRepCustomer() As Boolean
		Dim strSQL As String
        strSQL = "UPDATE salesrepcustomer set "
		strSQL += "UserID='" + _UserID + "', "
        strSQL += "CustomerID='" + _custID + "', "
        strSQL += "CustomerType='" + _custType + "'"
		strSQL += " where salesRepCustID='" + _salesRepCustID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of SalesRepCustomer
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getSalesRepCustomerInfo()
		Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  UserID, CustomerID, CustomerType FROM salesrepcustomer where salesRepCustID='" + _salesRepCustID + "' "
		drObj = GetDataReader(strSql)
		While drObj.Read
			_UserID = drObj.Item("UserID").ToString
            _custID = drObj.Item("distID").ToString
            _custType = drObj.Item("resellerID").ToString
		End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Delete SalesRepCustomer
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DeleteSalesRepCustomer() As Boolean
        Dim strSQL As String
        strSQL = "Delete from salesrepcustomer where "
        strSQL += "CustomerID='" + _custID + "' and "
        strSQL += "CustomerType='" + _custType + "'"

        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Check Duplicate SR
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDuplicateSR() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM salesrepcustomer where CustomerID='" & _custID & "' " 'and UserID='" & _UserID & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function

    ''' <summary>
    ''' Get of SalesRepCustomer
    ''' </summary>
    ''' <param name="strCustID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funSalesRep(ByVal strCustID As String) As String
        Dim strSQL As String = ""
        Dim strSR As String = ""
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  concat(u.userFirstName,' ',u.userLastName) as User FROM assignleads as s Inner join users u on u.userID=s.UserID where CustomerID='" + strCustID + "'  "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            strSR += drObj.Item("User").ToString () & " ,"

        End While
        If strSR <> "" Then
            strSR = strSR.TrimEnd(",")
        End If
        drObj.Close()
        CloseDatabaseConnection()
        Return strSR
    End Function
    ''' <summary>
    '''  Update SalesRepCustomer
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funReassigend() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE salesrepcustomer set "
        strSQL += "UserID='" + _UserID + "', "
        strSQL += "CustomerType='" + _custType + "'"
        strSQL += " where CustomerID='" + _custID + "'"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Get of JobPlannings
    ''' </summary>
    ''' <param name="strOrderID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funJobPlanning(ByVal strOrderID As String) As String
        Dim strSQL As String = ""
        Dim strSR As String = ""
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  concat(u.userFirstName,' ',u.userLastName) as User FROM jobplanning as s Inner join users u on u.userID=s.UserID where OrderID='" + strOrderID + "'  "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            strSR += drObj.Item("User").ToString & " ,"
        End While
        If strSR <> "" Then
            strSR = strSR.TrimEnd(",")
        End If
        drObj.Close()
        CloseDatabaseConnection()
        Return strSR
    End Function
End Class

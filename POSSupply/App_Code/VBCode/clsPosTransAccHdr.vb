Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsPosTransAccHdr
    Inherits clsDataClass

    Private _posAcctHdrID, _posAcctTransID, _posAcctPayType, _posCasReceived, _posCashReturned As String

#Region "Properties"
    Public Property AcctHdrID() As String
        Get
            Return _posAcctHdrID
        End Get
        Set(ByVal value As String)
            _posAcctHdrID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property AcctTransID() As String
        Get
            Return _posAcctTransID
        End Get
        Set(ByVal value As String)
            _posAcctTransID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property AcctPayType() As String
        Get
            Return _posAcctPayType
        End Get
        Set(ByVal value As String)
            _posAcctPayType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CasReceived() As String
        Get
            Return _posCasReceived
        End Get
        Set(ByVal value As String)
            _posCasReceived = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CashReturned() As String
        Get
            Return _posCashReturned
        End Get
        Set(ByVal value As String)
            _posCashReturned = clsCommon.funRemove(value, True)
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

#Region "Function"
    ''' <summary>
    ''' Insert PosAccountHeader
    ''' </summary>
    ''' <param name="sID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertpostransaccthdr(ByRef sID As String) As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO postransaccthdr(posAcctTransID, posAcctPayType, posCasReceived, posCashReturned) VALUES('"
        strSQL += _posAcctTransID + "','"
        strSQL += _posAcctPayType + "','"
        strSQL += _posCasReceived + "','"
        strSQL += _posCashReturned + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        sID = obj.GetScalarData("Select LAST_INSERT_ID()")
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update PosPaymentType
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdatePostransaccthdr() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE postransaccthdr set "
        strSQL += "posAcctTransID='" + _posAcctTransID + "', "
        strSQL += "posAcctPayType='" + _posAcctPayType + "', "
        strSQL += "posCasReceived='" + _posCasReceived + "',"
        strSQL += "posCashReturned='" + _posCashReturned + "'"
        strSQL += " where posAcctTransID='" + _posAcctTransID + "'"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of PosPaymentType
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetPostransaccthdr()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT posAcctTransID, posAcctPayType, posCasReceived, posCashReturned FROM postransaccthdr where posAcctTransID='" + _posAcctTransID + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _posAcctTransID = drObj.Item("posAcctTransID").ToString
            _posAcctPayType = drObj.Item("posAcctPayType").ToString
            _posCasReceived = drObj.Item("posCasReceived").ToString
            _posCashReturned = drObj.Item("posCashReturned").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Get the Max TransactionID from POS
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetMaxTransAccHdr() As Integer
        Dim strSql, strMaxTraId As String
        strSql = "SELECT MAX(posAcctTransID) FROM postransaccthdr"
        strMaxTraId = Convert.ToString(GetScalarData(strSql))
        strMaxTraId = IIf(strMaxTraId = "", 0, strMaxTraId)
        CloseDatabaseConnection()
        Return strMaxTraId
    End Function

    ''' <summary>
    ''' Populate objects of PosPaymentType
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetTranID()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT posAcctTransID FROM postransaccthdr where posAcctHdrID='" + _posAcctHdrID + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _posAcctTransID = drObj.Item("posAcctTransID").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub

    ''' <summary>
    ''' Update Recieve
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateRecievePostransaccthdr() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE postransaccthdr set "
        strSQL += "posCasReceived='" + _posCasReceived + "'"
        strSQL += " where posAcctTransID='" + _posAcctTransID + "'"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region
End Class

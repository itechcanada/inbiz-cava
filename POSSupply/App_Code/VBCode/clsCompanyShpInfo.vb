Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsCompanyShpInfo
	Inherits clsDataClass
	Private _CompanyShpCode, _CompanyShpExtID, _CompanyShpActive As String
	Public Property CompanyShpCode() As String
		Get
			Return _CompanyShpCode
		End Get
		Set(ByVal value As String)
            _CompanyShpCode = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property CompanyShpExtID() As String
		Get
			Return _CompanyShpExtID
		End Get
		Set(ByVal value As String)
            _CompanyShpExtID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property CompanyShpActive() As String
		Get
			Return _CompanyShpActive
		End Get
		Set(ByVal value As String)
            _CompanyShpActive = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert CompanyShpInfo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertCompanyShpInfo() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO syscompanyshpinfo(CompanyShpCode, CompanyShpExtID, CompanyShpActive) VALUES('"
        strSQL += _CompanyShpCode + "','"
        strSQL += _CompanyShpExtID + "','"
        strSQL += _CompanyShpActive + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update CompanyShpInfo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateCompanyShpInfo() As Boolean
		Dim strSQL As String
        strSQL = "UPDATE syscompanyshpinfo set "
		strSQL += "CompanyShpExtID='" + _CompanyShpExtID + "', "
		strSQL += "CompanyShpActive='" + _CompanyShpActive + "'"
		strSQL += " where CompanyShpCode='" + _CompanyShpCode + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of CompanyShpInfo
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getCompanyShpInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  CompanyShpExtID, CompanyShpActive FROM syscompanyshpinfo where CompanyShpCode='" + _CompanyShpCode + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_CompanyShpExtID = drObj.Item("CompanyShpExtID").ToString
			_CompanyShpActive = drObj.Item("CompanyShpActive").ToString
		End While
        drObj.Close()
        CloseDatabaseConnection()
	End Sub
End Class

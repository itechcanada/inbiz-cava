Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
'Imports Resources.Resource
'Imports System.Threading
Imports System.Globalization
Public Class clsProductColorCode
    Inherits clsDataClass
    Private _colCode, _colDesc As String
    Public Property ColCode() As String
        Get
            Return _colCode
        End Get
        Set(ByVal value As String)
            _colCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ColDesc() As String
        Get
            Return _colDesc
        End Get
        Set(ByVal value As String)
            _colDesc = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Insert ProductColor
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertProductColorCode() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO sysproductcolor(colCode, colDesc) VALUES( "
        strSQL += "'" + _colCode + "','"
        strSQL += _colDesc + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    '''  Update ProductColor
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateProductColorCode() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE sysproductcolor set "
        strSQL += " colDesc='" + _colDesc + "'"
        strSQL += " where colCode='" + _colCode + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of ProductColor
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetProductColorCodeInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT colDesc FROM sysproductcolor where colCode='" + _colCode + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _colDesc = drObj.Item("colDesc").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill Product color in Dropdown list
    ''' </summary>
    ''' <param name="dlColor"></param>
    ''' <remarks></remarks>
    Public Sub subGetColor(ByVal dlColor As DropDownList)
        Dim objDataclass As New clsDataClass
        Dim strSQL As String
        strSQL = "SELECT colCode,colDesc from sysproductcolor"
        dlColor.DataSource = objDataclass.GetDataReader(strSQL)
        dlColor.DataTextField = "colDesc"
        dlColor.DataValueField = "colCode"
        dlColor.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.msgSelectColor
        dlColor.Items.Insert(0, itm)
        objDataclass.CloseDatabaseConnection()
        objDataclass = Nothing
    End Sub

    ''' <summary>
    ''' Populate Product color in CheckBoxList
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <remarks></remarks>
    Public Sub PopulateColor(ByVal chklst As CheckBoxList)
        Dim strSQL As String
        strSQL = "SELECT colCode,colDesc from sysproductcolor"
        chklst.Items.Clear()
        chklst.DataSource = GetDataReader(strSQL)
        chklst.DataTextField = "colDesc"
        chklst.DataValueField = "colCode"
        chklst.DataBind()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' sub Fill Color
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subFillColor() As String
        Dim strSql As String = "select colCode,colDesc from sysproductcolor"
        'where descLang='" + Current.Session("Lang") + "' "
        Return strSql
    End Function
    ''' <summary>
    ''' Update SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <param name="productID"></param>
    ''' <remarks></remarks>
    Public Sub UpdateColor(ByVal chklst As CheckBoxList, ByVal productID As String)
        Dim objColor As New clsProductColor
        objColor.funDeleteProductColors(productID)

        For i As Int16 = 0 To chklst.Items.Count - 1
            If chklst.Items(i).Selected = True Then
                objColor.prdColiCode = chklst.Items(i).Value
                objColor.ProductID = productID
                objColor.funInsertProductColor()
            End If
        Next
    End Sub

    ''' <summary>
    ''' sub Update Color
    ''' </summary>
    ''' <param name="strPrdColor"></param>
    ''' <param name="productID"></param>
    ''' <remarks></remarks>
    Public Sub subUpdateColor(ByVal strPrdColor As String, ByVal productID As String)
        Dim objColor As New clsProductColor
        objColor.funDeleteProductColors(productID)
        Dim strPrd() As String = strPrdColor.Split(",")
        For i As Int16 = 0 To strPrd.Length - 1
            objColor.prdColiCode = strPrd(i)
            objColor.ProductID = productID
            objColor.funInsertProductColor()
        Next
    End Sub
End Class

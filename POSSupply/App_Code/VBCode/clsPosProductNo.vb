Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon

Public Class clsPosProductNo
    Inherits clsDataClass

    Private _posProductNo, _posPrdTransID, _posProductID, _posPrdQty, _posPrdUnitPrice, _posPrdPrice, _posPrdTaxCode, _posTax1, _posTax1Desc, _posTax2, _posTax2Desc, _posTax3, _posTax3Desc, _posTax4, _posTax4Desc As String

#Region "Properties"
    Public Property POSProductNo() As String
        Get
            Return _posProductNo
        End Get
        Set(ByVal value As String)
            _posProductNo = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdTransID() As String
        Get
            Return _posPrdTransID
        End Get
        Set(ByVal value As String)
            _posPrdTransID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ProductID() As String
        Get
            Return _posProductID
        End Get
        Set(ByVal value As String)
            _posProductID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdQty() As String
        Get
            Return _posPrdQty
        End Get
        Set(ByVal value As String)
            _posPrdQty = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdUnitPrice() As String
        Get
            Return _posPrdUnitPrice
        End Get
        Set(ByVal value As String)
            _posPrdUnitPrice = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdPrice() As String
        Get
            Return _posPrdPrice
        End Get
        Set(ByVal value As String)
            _posPrdPrice = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdTaxCode() As String
        Get
            Return _posPrdTaxCode
        End Get
        Set(ByVal value As String)
            _posPrdTaxCode = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Property Tax1() As String
        Get
            Return _posTax1
        End Get
        Set(ByVal value As String)
            _posTax1 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Tax2() As String
        Get
            Return _posTax2
        End Get
        Set(ByVal value As String)
            _posTax2 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Tax3() As String
        Get
            Return _posTax3
        End Get
        Set(ByVal value As String)
            _posTax3 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Tax4() As String
        Get
            Return _posTax4
        End Get
        Set(ByVal value As String)
            _posTax4 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Tax1Desc() As String
        Get
            Return _posTax1Desc
        End Get
        Set(ByVal value As String)
            _posTax1Desc = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Tax2Desc() As String
        Get
            Return _posTax2Desc
        End Get
        Set(ByVal value As String)
            _posTax2Desc = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Tax3Desc() As String
        Get
            Return _posTax3Desc
        End Get
        Set(ByVal value As String)
            _posTax3Desc = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Tax4Desc() As String
        Get
            Return _posTax4Desc
        End Get
        Set(ByVal value As String)
            _posTax4Desc = clsCommon.funRemove(value, True)
        End Set
    End Property

#End Region

    Public Sub New()
        MyBase.New()
    End Sub

#Region "Function"
    ''' <summary>
    ''' Insert PosProduct
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertPosProduct() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO posproductno(posPrdTransID, posProductID, posPrdQty, posPrdUnitPrice,posPrdPrice, posPrdTaxCode, posTax1, posTax1Desc, posTax2, posTax2Desc, posTax3,posTax3Desc, posTax4, posTax4Desc) VALUES('"
        strSQL += _posPrdTransID + "','"
        strSQL += _posProductID + "','"
        strSQL += _posPrdQty + "','"
        strSQL += _posPrdUnitPrice + "','"
        strSQL += _posPrdPrice + "','"
        strSQL += _posPrdTaxCode + "','"
        strSQL += _posTax1 + "','"
        strSQL += _posTax1Desc + "','"
        strSQL += _posTax2 + "','"
        strSQL += _posTax2Desc + "','"
        strSQL += _posTax3 + "','"
        strSQL += _posTax3Desc + "','"
        strSQL += _posTax4 + "','"
        strSQL += _posTax4Desc + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update PosProduct
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdatePosProduct() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE posproductno set "
        strSQL += "posPrdTransID='" + _posPrdTransID + "', "
        strSQL += "posProductID='" + _posProductID + "', "
        strSQL += "posPrdQty='" + _posPrdQty + "', "
        strSQL += "posPrdUnitPrice='" + _posPrdUnitPrice + "', "
        strSQL += "posPrdPrice='" + _posPrdPrice + "', "
        strSQL += "posPrdTaxCode='" + _posPrdTaxCode + "', "
        strSQL += "posTax1='" + _posTax1 + "', "
        strSQL += "posTax1Desc='" + _posTax1Desc + "', "
        strSQL += "posTax2='" + _posTax2 + "', "
        strSQL += "posTax2Desc='" + _posTax2Desc + "', "
        strSQL += "posTax3='" + _posTax3 + "', "
        strSQL += "posTax3Desc='" + _posTax3Desc + "', "
        strSQL += "posTax4='" + _posTax4 + "'"
        strSQL += " where posProductID='" + _posProductID + "' and posPrdTransID='" + _posPrdTransID + "'"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of PosProduct
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetPosProduct()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT posPrdTransID, posProductID, posPrdQty, posPrdUnitPrice,posPrdPrice, posPrdTaxCode FROM posproductno where posProductNo='" + _posProductNo + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _posPrdTransID = drObj.Item("posPrdTransID").ToString
            _posProductID = drObj.Item("posProductID").ToString
            _posPrdQty = drObj.Item("posPrdQty").ToString
            _posPrdUnitPrice = drObj.Item("posPrdUnitPrice").ToString
            _posPrdPrice = drObj.Item("posPrdPrice").ToString
            _posPrdTaxCode = drObj.Item("posPrdTaxCode").ToString
            _posTax1 = drObj.Item("posTax1").ToString
            _posTax1Desc = drObj.Item("posTax1Desc").ToString
            _posTax2 = drObj.Item("posTax2").ToString
            _posTax2Desc = drObj.Item("posTax2Desc").ToString
            _posTax3 = drObj.Item("posTax3").ToString
            _posTax3Desc = drObj.Item("posTax3Desc").ToString
            _posTax4 = drObj.Item("posTax4").ToString
            _posTax4Desc = drObj.Item("posTax4Desc").ToString
        End While
        CloseDatabaseConnection()
        drObj.Close()
    End Sub
    ''' <summary>
    ''' Delete PosProduct
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeletePosProduct() As Boolean
        Dim strSQL As String
        strSQL = "Delete from posproductno where posPrdTransID='" & _posPrdTransID & "'"
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsPrdAssociations
	Inherits clsDataClass
	Private _id, _assProdID As String
	Public Property Id() As String
		Get
			Return _id
		End Get
		Set(ByVal value As String)
            _id = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property AssProdID() As String
		Get
			Return _assProdID
		End Get
		Set(ByVal value As String)
            _assProdID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert PrdAssociations
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertPrdAssociations() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO prdassociations(id, assProdID) VALUES("
        strSQL += "'" + _id + "','"
        strSQL += _assProdID + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    '''  Update PrdAssociations
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdatePrdAssociations() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE prdassociations set"
        strSQL += "assProdID='" + _assProdID + "'"
        strSQL += " where id='" + _id + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass

        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of PrdAssociations
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetPrdAssociationsInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  assProdID FROM prdassociations where id='" + _id + "' and  assProdID='" + _assProdID + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _assProdID = drObj.Item("assProdID").ToString
        End While
        CloseDatabaseConnection()
        drObj.Close()
    End Sub
    ''' <summary>
    ''' Find Association product Name
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDuplicateAssProduct() As Boolean
        Dim strSQL As String
        Dim objDataClass As New clsDataClass
        strSQL = "SELECT count(*) FROM prdassociations where assProdID='" & _assProdID & "' And Id ='" & _id & "'"
        If objDataClass.GetScalarData(strSQL) <> 0 Then
            objDataClass = Nothing
            Return True
        Else
            objDataClass = Nothing
            Return False
        End If
    End Function
    ''' <summary>
    ''' sub Fill Ass Product
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subFillAssProduct() As String
        Dim strSql As String = "select ap.assProdID,ap.id,pd.prdName from prdassociations as ap "
        strSql += " Inner Join prddescriptions as pd on pd.id=ap.AssprodID where ap.id ='" & HttpContext.Current.Request.QueryString("PrdID") & "' and descLang='en'"
        'where descLang='" + Current.Session("Lang") + "' "
        Return strSql
    End Function
    ''' <summary>
    ''' sub Ass Product Delete
    ''' </summary>
    ''' <param name="strProdID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subAssPrdDelete(ByVal strProdID As String) As String
        Dim strSql As String = "delete from prdassociations where assProdID='" & strProdID & "' And Id ='" & HttpContext.Current.Request.QueryString("PrdID") & "'"
        Return strSql
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsOrderItems
	Inherits clsDataClass
    Private _orderItemID, _ordID, _ordProductID, _ordProductQty, _ordProductUnitPrice, _ordProductTaxGrp, _ordProductDiscount, _orderItemDesc, _ordProductDiscountType As String
    Private _ordWHS As String
	Public Property OrderItemID() As String
		Get
			Return _orderItemID
		End Get
		Set(ByVal value As String)
            _orderItemID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdID() As String
		Get
			Return _ordID
		End Get
		Set(ByVal value As String)
            _ordID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdProductID() As String
		Get
			Return _ordProductID
		End Get
		Set(ByVal value As String)
            _ordProductID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdProductQty() As String
		Get
			Return _ordProductQty
		End Get
		Set(ByVal value As String)
            _ordProductQty = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdProductUnitPrice() As String
		Get
			Return _ordProductUnitPrice
		End Get
		Set(ByVal value As String)
            _ordProductUnitPrice = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property OrdWarehouse() As String
        Get
            Return _ordWHS
        End Get
        Set(ByVal value As String)
            _ordWHS = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ordProductTaxGrp() As String
        Get
            Return _ordProductTaxGrp
        End Get
        Set(ByVal value As String)
            _ordProductTaxGrp = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ordProductDiscount() As String
        Get
            Return _ordProductDiscount
        End Get
        Set(ByVal value As String)
            _ordProductDiscount = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property orderItemDesc() As String
        Get
            Return _orderItemDesc
        End Get
        Set(ByVal value As String)
            _orderItemDesc = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ordProductDiscountType() As String
        Get
            Return _ordProductDiscountType
        End Get
        Set(ByVal value As String)
            _ordProductDiscountType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    '''  Insert OrderItems
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertOrderItems() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO orderitems(ordID, ordProductID, ordProductQty, ordProductUnitPrice,ordProductTaxGrp,ordProductDiscount, orderItemDesc, ordProductDiscountType) VALUES('"
		strSQL += _ordID + "','"
		strSQL += _ordProductID + "','"
        strSQL += _ordProductQty + "','"
        strSQL += _ordProductUnitPrice + "','"
        strSQL += _ordProductTaxGrp + "','"
        strSQL += _ordProductDiscount + "','"
        strSQL += _orderItemDesc + "','"
        strSQL += _ordProductDiscountType + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update OrderItems
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateOrderItems() As Boolean
		Dim strSQL As String
        strSQL = "UPDATE orderitems set "
		strSQL += "ordID='" + _ordID + "', "
		strSQL += "ordProductID='" + _ordProductID + "', "
		strSQL += "ordProductQty='" + _ordProductQty + "', "
        strSQL += "ordProductUnitPrice='" + _ordProductUnitPrice + "',"
        strSQL += "ordProductTaxGrp='" + _ordProductTaxGrp + "',"
        strSQL += "ordProductDiscount='" + _ordProductDiscount + "', "
        strSQL += "orderItemDesc='" + _orderItemDesc + "', "
        strSQL += "ordProductDiscountType='" + _ordProductDiscountType + "' "
		strSQL += " where orderItemID='" + _orderItemID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of OrderItems
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getOrderItemsInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
        strSQL = "SELECT * FROM orderitems where orderItemID='" + _orderItemID + "' "
		drObj = GetDataReader(strSql)
        While drObj.Read
            _ordID = drObj.Item("ordID").ToString
            _ordProductID = drObj.Item("ordProductID").ToString
            _ordProductQty = drObj.Item("ordProductQty").ToString
            _ordProductUnitPrice = drObj.Item("ordProductUnitPrice").ToString
            _ordProductTaxGrp = drObj.Item("ordProductTaxGrp").ToString
            _ordProductDiscount = drObj.Item("ordProductDiscount").ToString
            _orderItemDesc = drObj.Item("orderItemDesc").ToString
            _ordProductDiscountType = drObj.Item("ordProductDiscountType").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill Grid of Sales Order Items
    ''' </summary>
    ''' <param name="SOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal SOID As String) As String
        Dim strSQL As String
        strSQL = "SELECT i.orderItemID, i.ordProductID,i.ordProductTaxGrp,i.ordProductDiscount, sysTaxCodeDescText, p.prdIntID, p.prdExtID, p.prdUPCCode,CASE i.orderItemDesc='' WHEN False THEN i.orderItemDesc Else p.prdName  END as prdName, ordProductQty, (ordProductUnitPrice * o.ordCurrencyExRate) as ordProductUnitPrice,ordProductUnitPrice as InOrdProductUnitPrice,ordCurrencyCode, "
        strSQL += " case i.ordProductDiscountType when 'A' then (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - i.ordProductDiscount else CASE i.ordProductDiscount WHEN 0 THEN (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) ELSE (ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) - ((ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) * i.ordProductDiscount/100) END End as  amount, "
        strSQL += "ordShpWhsCode, ordCustID, ordCustType,i.ordProductDiscountType FROM orderitems i inner join orders o on o.ordID=i.ordID inner join products p on i.ordProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.ordProductTaxGrp where i.ordID='" & SOID & "'"
        Return strSQL
    End Function
    ''' <summary>
    ''' Get the discount 
    ''' </summary>
    ''' <param name="SOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetDiscount(ByVal SOID As String) As Boolean
        Dim strSQL As String
        strSQL = "SELECT ordProductDiscount FROM orderitems where ordID='" & SOID & "'"
        Dim drObj As OdbcDataReader
        drObj = GetDataReader(strSQL)
        While drObj.Read
            If drObj.Item("ordProductDiscount").ToString <> "0" Then
                drObj.Close()
                CloseDatabaseConnection()
                Return True
            End If
        End While
        drObj.Close()
        CloseDatabaseConnection()
        Return False
    End Function
    ''' <summary>
    ''' Delete Sales Order Items
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteSOItems() As String
        Dim strSQL As String
        strSQL = "DELETE FROM orderitems WHERE orderItemID='" + _orderItemID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Check Product Quantity
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckProductQuantity() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM prdquantity where prdID='" & _ordProductID & "' and prdWhsCode='" & _ordWHS & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Update Purchase Order Quantity
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateProductQuantity() As Boolean
        Dim bResult As Boolean = False
        Dim objPrdQty As clsPrdQuantity
        If funCheckProductQuantity() = True Then
            objPrdQty = New clsPrdQuantity
            objPrdQty.ProductID = _ordProductID
            objPrdQty.prdWhsCode = _ordWHS
            objPrdQty.subGetProductQuantity()
            If IsNumeric(objPrdQty.prdQuoteRsv) = True Then
                objPrdQty.prdQuoteRsv = CDbl(objPrdQty.prdQuoteRsv) + CDbl(_ordProductQty)
            Else
                objPrdQty.prdQuoteRsv = _ordProductQty
            End If
            bResult = objPrdQty.funUpdateProductQuantity("QR")
            objPrdQty = Nothing
        Else
            objPrdQty = New clsPrdQuantity
            objPrdQty.ProductID = _ordProductID
            objPrdQty.prdWhsCode = _ordWHS
            objPrdQty.prdOhdQty = 0
            objPrdQty.prdQuoteRsv = _ordProductQty
            objPrdQty.prdSORsv = 0
            objPrdQty.prdDefectiveQty = 0
            objPrdQty.TaxCode = ordProductTaxGrp
            bResult = objPrdQty.funInsertPrdQuantity()
            objPrdQty = Nothing
        End If
        Return bResult
    End Function
    ''' <summary>
    ''' Get Item Cost For Order No
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getItemCostForOrderNo() As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT sum(ordProductQty * ordProductUnitPrice) as ItemCost FROM orderitems where ordID='" & _ordID & "' group by ordID "
        Try
            strData = GetScalarData(strSQL).ToString
        Catch ex As NullReferenceException
            strData = "0"
        Catch ex As Exception
            strData = "0"
        End Try
        Return strData
    End Function
    ' ''' <summary>
    ' ''' Populate objects of OrderItems
    ' ''' </summary>
    ' ''' <param name="strWhs"></param>
    ' ''' <param name="OderId"></param>
    ' ''' <param name="strQtyType"></param>
    ' ''' <remarks></remarks>
    'Public Sub subGetOrderItems(ByVal strWhs As String, ByVal OderId As String, ByVal strQtyType As String)
    '    Dim strSQL As String
    '    Dim objPrdQty As New clsPrdQuantity
    '    Dim objProduct As New clsProducts
    '    Dim drObj As OdbcDataReader
    '    strSQL = "SELECT * FROM orderitems where ordID='" + OderId + "' "
    '    drObj = GetDataReader(strSQL)
    '    While drObj.Read
    '        If objProduct.funGetPrdValue(drObj.Item("ordProductID").ToString, "kit") = "1" Then
    '            objPrdQty.subGetPrdKitForOrder(drObj.Item("ordProductID").ToString, strWhs, drObj.Item("ordProductQty").ToString, strQtyType) 'Subtract quotation reserve from product kit items 
    '            objPrdQty.funUpdateProductQtyFields(drObj.Item("ordProductID").ToString, strWhs, drObj.Item("ordProductQty").ToString, strQtyType) 'form Product kit
    '        Else
    '            objPrdQty.funUpdateProductQtyFields(drObj.Item("ordProductID").ToString, strWhs, drObj.Item("ordProductQty").ToString, strQtyType) 'from product
    '        End If
    '    End While
    '    drObj.Close()
    '    CloseDatabaseConnection()
    'End Sub
    ''' <summary>
    ''' Get Products for Given  Order Id
    ''' </summary>
    ''' <param name="SOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetOrderProductsByOrderId(ByVal SOID As String) As OdbcDataReader
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT * FROM orderitems where ordID='" + SOID + "' "
        drObj = GetDataReader(strSQL)
        Return drObj
    End Function
    ''' <summary>
    '''  Get order Product Qty for printing Assembly list.
    ''' </summary>
    ''' <param name="SOID"></param>
    ''' <param name="SOItemID"></param>
    ''' <param name="IsKit"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetOrderProductsQty(ByVal SOID As String, ByVal SOItemID As String, Optional ByVal IsKit As String = "") As Int32
        Dim strSQL As String = ""
        Dim intQty As Int32 = 0
        Dim drObj As OdbcDataReader
        strSQL = "SELECT ordProductQty as SOIQty FROM orderitems o Where ordID=" & SOID & " And ordProductID=" & SOItemID
        drObj = GetDataReader(strSQL)
        While drObj.Read
            intQty = Convert.ToInt32(drObj("SOIQty"))
        End While
        Return intQty
    End Function
    ''' <summary>
    ''' Count Sales Order Items
    ''' </summary>
    ''' <param name="OrdID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCountSOItems(ByVal OrdID As String) As Integer
        Dim strSQL As String
        strSQL = "select count(*) FROM orderitems WHERE ordID='" + OrdID + "' "
        Return GetScalarData(strSQL)
    End Function
End Class

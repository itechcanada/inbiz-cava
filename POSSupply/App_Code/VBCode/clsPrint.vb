Imports System
Imports System.IO
Imports System.Data.Odbc
Imports Resources.Resource
Imports System.Web.HttpContext
Imports iTextSharp.text
Imports iTextSharp.text.pdf

Public Class clsPrint
    Inherits PdfPageEventHelper

    Private bf As BaseFont = Nothing
    Private cb As PdfContentByte
    Private template As PdfTemplate
    Private strPOID As String

    Public Sub New()
    End Sub
    Public Sub New(ByVal ID As String)
        strPOID = ID
    End Sub
    Public Function PrintRequestDetail(ByVal strReqID As String) As String
        Dim strData As String
        'strData = subPDFGen(strReqID)
        strData = funPOGen(strReqID)
        Return strData
    End Function
    Public Function PrintPurchaseOrder(ByVal strPOID As String) As String
        Dim strData As String
        'strData = subPDFGen(strReqID)
        strData = funPOGen(strPOID)
        Return strData
    End Function
    'Request Details
    Private Function subPDFGen(ByVal strReqID As String) As String
        Dim document As Document = New Document
        Dim strPath As String = ""
        strPath = Current.Server.MapPath("~") & "\Pdf\PO_" & Now.ToString("yyyyMMddhhmmss") & ".pdf"
        If strReqID <> "" Then

            Dim writer As PdfWriter = PdfWriter.GetInstance(document, New FileStream(strPath, FileMode.Create))
            Dim footer As HeaderFooter = New HeaderFooter(New Phrase(prnPage & ": "), True)
            footer.Border = Rectangle.NO_BORDER
            footer.Alignment = 2
            document.Footer = footer
            Dim header As HeaderFooter = New HeaderFooter(New Phrase(" "), False)
            header.Border = Rectangle.NO_BORDER
            header.Alignment = 0
            document.Header = header
            document.Open()
           
            document.Add(New Paragraph(prnCompanyTitle & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 10)))
            document.Add(New Paragraph(prnAddress & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))
            document.Add(New Paragraph(prnTelFax & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))
            document.Add(New Paragraph(contactUsEmailCol & " " & contactUsCompanyEmail & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            Dim cb As PdfContentByte = writer.DirectContent
            cb.MoveTo(30.0F, document.PageSize.Height - 120.0F)
            cb.LineTo(document.PageSize.Width - 30.0F, document.PageSize.Height - 120.0F)
            cb.Stroke()

            document.Add(New Paragraph("Purchase Order Number" & ":" & Microsoft.VisualBasic.Space(25) & strReqID, FontFactory.GetFont("Tahoma", 12)))

            document.Close()
        Else
            Dim writer As PdfWriter = PdfWriter.GetInstance(document, New FileStream(strPath, FileMode.Create))
            Dim footer As HeaderFooter = New HeaderFooter(New Phrase(prnPage & ": "), True)
            footer.Border = Rectangle.NO_BORDER
            footer.Alignment = 2
            document.Footer = footer
            Dim header As HeaderFooter = New HeaderFooter(New Phrase(" "), False)
            header.Border = Rectangle.NO_BORDER
            header.Alignment = 0
            document.Header = header
            document.Open()
            
            document.Add(New Paragraph(prnCompanyTitle & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 10)))
            document.Add(New Paragraph(prnAddress & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))
            document.Add(New Paragraph(prnTelFax & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))
            document.Add(New Paragraph(contactUsEmailCol & " " & contactUsCompanyEmail & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            Dim cb As PdfContentByte = writer.DirectContent
            cb.MoveTo(30.0F, document.PageSize.Height - 120.0F)
            cb.LineTo(document.PageSize.Width - 30.0F, document.PageSize.Height - 120.0F)
            cb.Stroke()

            document.Add(New Paragraph(prnNoRecordFound & " ", FontFactory.GetFont("Tahoma", 16)))
        End If
        Return strPath.Substring(strPath.LastIndexOf("\") + 1)
    End Function
    'Purchase Order
    Private Function funPOGen(ByVal strPOID As String) As String
        Dim document As Document = New Document

        Dim strPath As String = ""
        strPath = Current.Server.MapPath("~") & "\Pdf\PO-" & strPOID & "_" & Now.ToString("yyyyMMddhhmmss") & ".pdf"

        Dim writer As PdfWriter = PdfWriter.GetInstance(document, New FileStream(strPath, FileMode.Create))
        Dim events As clsPrint = New clsPrint
        writer.PageEvent = events

        Dim p As Paragraph
        Dim cell As Cell

        Dim objCompany As New clsCompanyInfo
        Dim objPO As New clsPurchaseOrders
        Dim objPOItems As New clsPurchaseOrderItems

        If strPOID <> "" Then
            Dim header As HeaderFooter = New HeaderFooter(New Phrase(" "), False)
            header.Border = Rectangle.NO_BORDER
            header.Alignment = 0
            document.Header = header

            document.Open()

            Dim tblHeader As New Table(8)
            tblHeader.SetAlignment(Element.ALIGN_LEFT)
            tblHeader.Width = 100.0F
            tblHeader.AutoFillEmptyCells = True
            tblHeader.CellsFitPage = True
            tblHeader.BorderWidth = 0.0F
            tblHeader.BorderColor = New Color(255, 255, 255)
            tblHeader.Alignment = Element.ALIGN_LEFT
            tblHeader.BackgroundColor = New Color(255, 255, 255)

            objPO.PoID = strPOID
            objPO.getPurchaseOrders()

            objCompany.CompanyID = objPO.poCompanyID
            'objCompany.CompanyName = objCompany.funGetFirstCompanyName
            objCompany.getCompanyInfo()

            Dim logo As Image
            If objCompany.CompanyLogoPath <> "" Then
                logo = Image.GetInstance(Current.Request.PhysicalApplicationPath & "Upload\companylogo\" & objCompany.CompanyLogoPath)
                logo.BorderWidth = 0.0F
                logo.Border = 0.0F
                logo.BorderColor = Color.WHITE
                cell = New Cell(logo)
                cell.VerticalAlignment = Element.ALIGN_MIDDLE
                cell.BorderWidth = 0.0F
                tblHeader.AddCell(cell)
            End If

            p = New Paragraph(objCompany.CompanyName & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 10))
            cell = New Cell(p)

            Dim strAddress As String = objCompany.CompanyAddressLine1 & IIf(objCompany.CompanyAddressLine2 <> "", ", " & objCompany.CompanyAddressLine2, "") & IIf(objCompany.CompanyCity <> "", ", " & objCompany.CompanyCity, "") & IIf(objCompany.CompanyState <> "", ", " & objCompany.CompanyState, "") & IIf(objCompany.CompanyCountry <> "", ", " & objCompany.CompanyCountry, "") & IIf(objCompany.CompanyPostalCode <> "", ", " & objCompany.CompanyPostalCode, "")
            strAddress = IIf(strAddress <> "", strAddress, "-")
            p = New Paragraph(strAddress & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8))
            cell.Add(p)

            Dim strTelFax As String = lblPhone & " " & IIf(objCompany.CompanyPOPhone <> "", objCompany.CompanyPOPhone, "-") & " | " & lblFax & " " & IIf(objCompany.CompanyFax <> "", objCompany.CompanyFax, "-")
            p = New Paragraph(strTelFax & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8))
            cell.Add(p)
            p = New Paragraph(lblEmail & " " & IIf(objCompany.CompanyEmail <> "", objCompany.CompanyEmail, "-") & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8))
            cell.Add(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 5
            cell.BorderWidth = 0.0F
            tblHeader.AddCell(cell)

            Dim strTitle As String = prnPO
            p = New Paragraph(strTitle, FontFactory.GetFont("Tahoma", 11, Font.BOLD))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.VerticalAlignment = Element.ALIGN_MIDDLE
            cell.Colspan = 2
            cell.BorderWidth = 0.0F
            tblHeader.AddCell(cell)
            document.Add(tblHeader)

            Dim cb As PdfContentByte = writer.DirectContent
            cb.MoveTo(30.0F, document.PageSize.Height - 120.0F)
            cb.LineTo(document.PageSize.Width - 30.0F, document.PageSize.Height - 120.0F)
            cb.Stroke()

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            'objPO.PoID = strPOID
            'objPO.getPurchaseOrders()

            Dim tblnumber As New Table(5)
            tblnumber.SetAlignment(Element.ALIGN_LEFT)
            tblnumber.Width = 100.0F
            tblnumber.AutoFillEmptyCells = True
            tblnumber.CellsFitPage = True
            tblnumber.BorderWidth = 0.0F
            tblnumber.BorderColor = New Color(255, 255, 255)
            tblnumber.Alignment = Element.ALIGN_LEFT
            tblnumber.BackgroundColor = New Color(255, 255, 255)
            tblnumber.Cellspacing = 2
            tblnumber.Cellpadding = 3

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.Colspan = 4
            cell.Rowspan = 2
            cell.BorderWidth = 0.0F
            tblnumber.AddCell(cell)

            Dim strDate As String = prnDate
            Dim strNumber As String = prnPONo

            p = New Paragraph(strDate + ": " + IIf(objPO.PoDate <> "", CDate(objPO.PoDate).ToString("MMM-dd yyyy"), "--"), FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblnumber.AddCell(cell)

            p = New Paragraph(strNumber + ": " + objPO.PoID, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblnumber.AddCell(cell)

            document.Add(tblnumber)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            Dim strCurrency As String = ""
            Dim aTable As New Table(15)
            aTable.SetAlignment(Element.ALIGN_LEFT)
            aTable.Width = 100.0F
            aTable.AutoFillEmptyCells = True
            aTable.CellsFitPage = True
            aTable.Alignment = Element.ALIGN_LEFT
            aTable.Cellspacing = 2
            aTable.Cellpadding = 2
            aTable.BorderWidth = 0.0F
            aTable.BorderColor = New Color(255, 255, 255)
            Dim strVendorInfo As String = ""
            strCurrency = objPO.VendorCurrency

            p = New Paragraph(prnPOBillTo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            cell.Colspan = 7
            aTable.AddCell(cell)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            aTable.AddCell(cell)

            p = New Paragraph(prnShipTo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            cell.Colspan = 7
            aTable.AddCell(cell)

            ' Bill To
            strVendorInfo += objPO.VendorName
            strVendorInfo += Microsoft.VisualBasic.Chr(10)
            strVendorInfo += funPopulateAddress("V", "B", objPO.PoVendorID)
            p = New Paragraph(strVendorInfo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 7
            aTable.AddCell(cell)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            aTable.AddCell(cell)

            ' Shipping
            Dim strShippingInfo As String = ""
            Dim strWHS As String
            strWHS = objPO.getPOShippingWarehouseCode(strPOID).Split(",")(0)
            strShippingInfo += funWarehouseAddress(strWHS)

            p = New Paragraph(strShippingInfo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 7
            aTable.AddCell(cell)
            document.Add(aTable)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            Dim tblShipping As New Table(6)
            tblShipping.SetAlignment(Element.ALIGN_LEFT)
            tblShipping.Width = 100.0F
            tblShipping.AutoFillEmptyCells = True
            tblShipping.CellsFitPage = True
            tblShipping.Alignment = Element.ALIGN_LEFT
            tblShipping.BackgroundColor = New Color(255, 255, 255)
            tblShipping.Cellspacing = 2
            tblShipping.Cellpadding = 3

            'Header Start
            p = New Paragraph(prnShippingTerms, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            cell.Colspan = 2
            tblShipping.AddCell(cell)
            p = New Paragraph(prnShipVia, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblShipping.AddCell(cell)
            p = New Paragraph(prnFOBLocation, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblShipping.AddCell(cell)
            p = New Paragraph(prnNote, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            cell.Colspan = 2
            tblShipping.AddCell(cell)
            'Header End

            p = New Paragraph(IIf(objPO.PoShpTerms <> "", objPO.PoShpTerms, "--"), FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 2
            tblShipping.AddCell(cell)
            p = New Paragraph(IIf(objPO.PoShipVia <> "", objPO.PoShipVia, "--"), FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblShipping.AddCell(cell)
            p = New Paragraph(IIf(objPO.PoFOBLoc <> "", objPO.PoFOBLoc, "--"), FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblShipping.AddCell(cell)
            p = New Paragraph(IIf(objPO.PoNotes <> "", objPO.PoNotes, "--"), FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 2
            tblShipping.AddCell(cell)

            document.Add(tblShipping)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            Dim tblProduct As New Table(7)
            tblProduct.SetAlignment(Element.ALIGN_LEFT)
            tblProduct.Width = 100.0F
            tblProduct.AutoFillEmptyCells = True
            tblProduct.CellsFitPage = True
            tblProduct.Alignment = Element.ALIGN_LEFT
            tblProduct.BackgroundColor = New Color(255, 255, 255)
            tblProduct.Cellspacing = 2
            tblProduct.Cellpadding = 3

            'Header Start
            p = New Paragraph(POExternalID, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProduct.AddCell(cell)
            p = New Paragraph(grdPOUPCCode, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProduct.AddCell(cell)
            p = New Paragraph(POProduct, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            cell.Colspan = 2
            tblProduct.AddCell(cell)
            p = New Paragraph(POQuantity, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProduct.AddCell(cell)
            p = New Paragraph(POUnitPrice, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProduct.AddCell(cell)
            p = New Paragraph(POAmount, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProduct.AddCell(cell)
            'Header End

            Dim drPOItems As OdbcDataReader
            drPOItems = objPOItems.GetDataReader(objPOItems.funFillGrid(strPOID))
            Dim dblSubTotal As Double = 0
            While drPOItems.Read
                p = New Paragraph(drPOItems("prdExtID").ToString, FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                tblProduct.AddCell(cell)
                p = New Paragraph(drPOItems("prdUPCCode").ToString, FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                tblProduct.AddCell(cell)
                p = New Paragraph(drPOItems("prdName"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                cell.Colspan = 2
                tblProduct.AddCell(cell)
                p = New Paragraph(drPOItems("poQty"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProduct.AddCell(cell)
                p = New Paragraph(String.Format("{0:F}", drPOItems("poUnitPrice")), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProduct.AddCell(cell)
                p = New Paragraph(objPO.PoCurrencyCode & " " & String.Format("{0:F}", drPOItems("amount")), FontFactory.GetFont("Verdana", 7))
                dblSubTotal += CDbl(drPOItems("amount"))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProduct.AddCell(cell)
            End While
            drPOItems.Close()

            document.Add(tblProduct)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            Dim tblCalc As New Table(12)
            tblCalc.SetAlignment(Element.ALIGN_LEFT)
            tblCalc.Width = 100.0F
            tblCalc.AutoFillEmptyCells = True
            tblCalc.CellsFitPage = True
            tblCalc.Alignment = Element.ALIGN_LEFT
            tblCalc.Cellspacing = 2
            tblCalc.Cellpadding = 3
            tblCalc.BorderColor = New Color(255, 255, 255)

            'Calculate Sub Total Start

            ' Authorised By
            Dim objUser As New clsUser
            objUser.UserID = objPO.PoAuthroisedByUserID
            objUser.getUserInfo()
            Dim authorName As String = objUser.UserFirstName & " " & objUser.UserLastName

            p = New Paragraph(prnAuthorisedby & " " & authorName & Microsoft.VisualBasic.Chr(10) & Microsoft.VisualBasic.Chr(10) & prnMemo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 9
            cell.Rowspan = 1
            tblCalc.AddCell(cell)

            p = New Paragraph(prnTotal, FontFactory.GetFont("Verdana", 7, Font.BOLD))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderColor = New Color(255, 255, 255)
            tblCalc.AddCell(cell)

            p = New Paragraph(objPO.PoCurrencyCode & " " & String.Format("{0:F}", dblSubTotal), FontFactory.GetFont("Verdana", 7, Font.BOLD))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BorderColor = New Color(255, 255, 255)
            cell.Colspan = 2
            tblCalc.AddCell(cell)
            'Calculate Sub Total End

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)
            document.Add(tblCalc)

            document.Close()
            objPO.CloseDatabaseConnection()
            objPO = Nothing
            objPOItems.CloseDatabaseConnection()
            objPOItems = Nothing
        Else
            'When No Key Pass
            Dim header As HeaderFooter = New HeaderFooter(New Phrase(" "), False)
            header.Border = Rectangle.NO_BORDER
            header.Alignment = 0
            document.Header = header

            document.Open()

            objCompany.CompanyName = objCompany.funGetFirstCompanyName
            objCompany.getCompanyInfo()

            document.Add(New Paragraph(objCompany.CompanyName & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 10)))

            Dim strAddress As String = objCompany.CompanyAddressLine1 & IIf(objCompany.CompanyAddressLine2 <> "", ", " & objCompany.CompanyAddressLine2, "") & IIf(objCompany.CompanyCity <> "", ", " & objCompany.CompanyCity, "") & IIf(objCompany.CompanyState <> "", ", " & objCompany.CompanyState, "") & IIf(objCompany.CompanyCountry <> "", ", " & objCompany.CompanyCountry, "") & IIf(objCompany.CompanyPostalCode <> "", ", " & objCompany.CompanyPostalCode, "")
            strAddress = IIf(strAddress <> "", strAddress, "-")
            document.Add(New Paragraph(strAddress & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            Dim strTelFax As String = lblPhone & " " & IIf(objCompany.CompanyPOPhone <> "", objCompany.CompanyPOPhone, "-") & " | " & lblFax & " " & IIf(objCompany.CompanyFax <> "", objCompany.CompanyFax, "-")
            document.Add(New Paragraph(strTelFax & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            document.Add(New Paragraph(lblEmail & " " & IIf(objCompany.CompanyEmail <> "", objCompany.CompanyEmail, "-") & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            Dim cb As PdfContentByte = writer.DirectContent
            cb.MoveTo(30.0F, document.PageSize.Height - 120.0F)
            cb.LineTo(document.PageSize.Width - 30.0F, document.PageSize.Height - 120.0F)
            cb.Stroke()

            document.Add(New Paragraph(prnNoRecordFound & " ", FontFactory.GetFont("Tahoma", 16)))
        End If
        Return strPath.Substring(strPath.LastIndexOf("\") + 1)
    End Function
    Public Function PrintSalesRequestDetail(ByVal strReqID As String) As String
        Dim strData As String
        strData = subPDFSalesGen(strReqID)
        Return strData
    End Function
    Public Function PrintRequestDetail(ByVal strReqID As String, ByVal strReqType As String) As String
        Current.Session("QO") = ""
        Dim strData As String = ""
        If strReqType = "QO" Then
            If ConfigurationManager.AppSettings("QOFormat").ToLower = "a" Then
                Current.Session("QO") = "1"
                strData = subQOGeneration(strReqID, strReqType)
            Else
                strData = subPDFGeneration(strReqID, strReqType)
            End If
        ElseIf strReqType = "SO" Then
            strData = subPDFGeneration(strReqID, strReqType)
        ElseIf strReqType = "IN" Then
            strData = subInvoiceGeneration(strReqID, strReqType)
        ElseIf strReqType = "PL" Then
            strData = subItemListGeneration(strReqID, strReqType)
        ElseIf strReqType = "BL" Then
            strData = subItemListGeneration(strReqID, strReqType)
        End If
        Return strData
    End Function
    'Packing List and Bill of Lading
    Private Function subItemListGeneration(ByVal strReqID As String, ByVal strReqType As String) As String
        Dim document As Document = New Document

        Dim strPath As String = ""
        strPath = Current.Server.MapPath("~") & "\Pdf\" & strReqType & "-" & strReqID & "_" & Now.ToString("yyyyMMddhhmmss") & ".pdf"

        Dim writer As PdfWriter = PdfWriter.GetInstance(document, New FileStream(strPath, FileMode.Create))
        Dim events As clsPrint = New clsPrint
        writer.PageEvent = events

        Dim p As Paragraph
        Dim cell As Cell

        Dim objCompany As New clsCompanyInfo

        Dim objOrder As New clsOrders
        Dim objOrderItems As New clsOrderItems
        Dim objOrderItemProcess As New clsOrderItemProcess

        If strReqID <> "" And strReqType <> "" Then
            Dim header As HeaderFooter = New HeaderFooter(New Phrase(" "), False)
            header.Border = Rectangle.NO_BORDER
            header.Alignment = 0
            document.Header = header

            document.Open()

            Dim tblHeader As New Table(7)
            tblHeader.SetAlignment(Element.ALIGN_LEFT)
            tblHeader.Width = 100.0F
            tblHeader.AutoFillEmptyCells = True
            tblHeader.CellsFitPage = True
            tblHeader.BorderWidth = 0.0F
            tblHeader.BorderColor = New Color(255, 255, 255)
            tblHeader.Alignment = Element.ALIGN_LEFT
            tblHeader.BackgroundColor = New Color(255, 255, 255)

            objOrder.OrdID = strReqID
            objOrder.getOrdersInfo()
            objCompany.CompanyID = objOrder.ordCompanyID
            'objCompany.CompanyName = objCompany.funGetFirstCompanyName
            objCompany.getCompanyInfo()

            p = New Paragraph(objCompany.CompanyName & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 10))
            cell = New Cell(p)

            Dim strAddress As String = objCompany.CompanyAddressLine1 & IIf(objCompany.CompanyAddressLine2 <> "", ", " & objCompany.CompanyAddressLine2, "") & IIf(objCompany.CompanyCity <> "", ", " & objCompany.CompanyCity, "") & IIf(objCompany.CompanyState <> "", ", " & objCompany.CompanyState, "") & IIf(objCompany.CompanyCountry <> "", ", " & objCompany.CompanyCountry, "") & IIf(objCompany.CompanyPostalCode <> "", ", " & objCompany.CompanyPostalCode, "")
            strAddress = IIf(strAddress <> "", strAddress, "-")
            p = New Paragraph(strAddress & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8))
            cell.Add(p)

            Dim strTelFax As String = lblPhone & " " & IIf(objCompany.CompanyPOPhone <> "", objCompany.CompanyPOPhone, "-") & " | " & lblFax & " " & IIf(objCompany.CompanyFax <> "", objCompany.CompanyFax, "-")
            p = New Paragraph(strTelFax & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8))
            cell.Add(p)

            p = New Paragraph(lblEmail & " " & IIf(objCompany.CompanyEmail <> "", objCompany.CompanyEmail, "-") & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8))
            cell.Add(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 5
            cell.BorderWidth = 0.0F
            tblHeader.AddCell(cell)

            Dim strTitle As String = ""
            If strReqType = "PL" Then
                strTitle = prnPackingList
            ElseIf strReqType = "BL" Then
                strTitle = prnBillofLading
            End If
            p = New Paragraph(strTitle, FontFactory.GetFont("Tahoma", 11, Font.BOLD))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.VerticalAlignment = Element.ALIGN_MIDDLE
            cell.Colspan = 2
            cell.BorderWidth = 0.0F
            tblHeader.AddCell(cell)
            document.Add(tblHeader)

            Dim cb As PdfContentByte = writer.DirectContent
            cb.MoveTo(30.0F, document.PageSize.Height - 120.0F)
            cb.LineTo(document.PageSize.Width - 30.0F, document.PageSize.Height - 120.0F)
            cb.Stroke()

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            'objOrder.OrdID = strReqID
            'objOrder.getOrdersInfo()

            Dim tblnumber As New Table(5)
            tblnumber.SetAlignment(Element.ALIGN_LEFT)
            tblnumber.Width = 100.0F
            tblnumber.AutoFillEmptyCells = True
            tblnumber.CellsFitPage = True
            tblnumber.BorderWidth = 0.0F
            tblnumber.BorderColor = New Color(255, 255, 255)
            tblnumber.Alignment = Element.ALIGN_LEFT
            tblnumber.BackgroundColor = New Color(255, 255, 255)
            tblnumber.Cellspacing = 2
            tblnumber.Cellpadding = 3

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.Colspan = 4
            cell.Rowspan = 2
            cell.BorderWidth = 0.0F
            tblnumber.AddCell(cell)

            Dim strDate As String = prnDate
            Dim strNumber As String = ""
            strNumber = prnSONo

            p = New Paragraph(strDate + ": " + IIf(objOrder.OrdCreatedOn <> "", CDate(objOrder.OrdCreatedOn).ToString("MMM-dd yyyy"), "--"), FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblnumber.AddCell(cell)

            p = New Paragraph(strNumber + ": " + objOrder.OrdID, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblnumber.AddCell(cell)

            document.Add(tblnumber)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            Dim strCurrency As String = ""
            Dim aTable As New Table(15)
            aTable.SetAlignment(Element.ALIGN_LEFT)
            aTable.Width = 100.0F
            aTable.AutoFillEmptyCells = True
            aTable.CellsFitPage = True
            aTable.Alignment = Element.ALIGN_LEFT
            aTable.Cellspacing = 2
            aTable.Cellpadding = 2
            aTable.BorderWidth = 0.0F
            aTable.BorderColor = New Color(255, 255, 255)
            Dim strVendorInfo As String = ""
            strCurrency = objOrder.OrdCurrencyCode

            p = New Paragraph(prnBillTo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            cell.Colspan = 7
            aTable.AddCell(cell)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            aTable.AddCell(cell)

            p = New Paragraph(prnShipTo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            cell.Colspan = 7
            aTable.AddCell(cell)

            ' Bill To
            strVendorInfo += objOrder.OrdCustName
            strVendorInfo += Microsoft.VisualBasic.Chr(10)
            strVendorInfo += funPopulateAddress(objOrder.OrdCustType, "B", objOrder.OrdCustID)
            p = New Paragraph(strVendorInfo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 7
            aTable.AddCell(cell)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            aTable.AddCell(cell)

            ' Shipping
            Dim strShippingInfo As String = ""
            strShippingInfo += objOrder.OrdCustName
            strShippingInfo += Microsoft.VisualBasic.Chr(10)
            strShippingInfo += funPopulateAddress(objOrder.OrdCustType, "S", objOrder.OrdCustID)

            p = New Paragraph(strShippingInfo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 7
            aTable.AddCell(cell)
            document.Add(aTable)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            Dim tblProduct As New Table(6)
            tblProduct.SetAlignment(Element.ALIGN_LEFT)
            tblProduct.Width = 100.0F
            tblProduct.AutoFillEmptyCells = True
            tblProduct.CellsFitPage = True
            tblProduct.Alignment = Element.ALIGN_LEFT
            tblProduct.BackgroundColor = New Color(255, 255, 255)
            tblProduct.Cellspacing = 2
            tblProduct.Cellpadding = 3

            'Header Start
            p = New Paragraph(prnSerial, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProduct.AddCell(cell)

            p = New Paragraph(grdPOUPCCode, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            cell.Colspan = 2
            tblProduct.AddCell(cell)

            p = New Paragraph(POProduct, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            cell.Colspan = 2
            tblProduct.AddCell(cell)

            p = New Paragraph(POQuantity, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProduct.AddCell(cell)
            'Header End

            Dim drItems As OdbcDataReader
            drItems = objOrderItems.GetDataReader(objOrderItems.funFillGrid(strReqID))
            Dim intSno As Integer = 0
            While drItems.Read
                intSno += 1
                p = New Paragraph(intSno, FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                tblProduct.AddCell(cell)

                p = New Paragraph(drItems("prdUPCCode").ToString, FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                cell.Colspan = 2
                tblProduct.AddCell(cell)

                p = New Paragraph(drItems("prdName"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                cell.Colspan = 2
                tblProduct.AddCell(cell)

                p = New Paragraph(drItems("ordProductQty"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProduct.AddCell(cell)
            End While
            drItems.Close()

            document.Add(tblProduct)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            Dim tblMemo As New Table(1)
            tblMemo.SetAlignment(Element.ALIGN_LEFT)
            tblMemo.Width = 100.0F
            tblMemo.AutoFillEmptyCells = True
            tblMemo.CellsFitPage = True
            tblMemo.Alignment = Element.ALIGN_LEFT
            tblMemo.Cellspacing = 2
            tblMemo.Cellpadding = 3
            tblMemo.BorderColor = New Color(255, 255, 255)

            Dim objUser As New clsUser
            objUser.UserID = objOrder.OrdLastUpdateBy
            objUser.getUserInfo()

            Dim strUseName As String = objUser.UserFirstName & " " & objUser.UserLastName

            p = New Paragraph(prnAuthorisedby & " " & strUseName & Microsoft.VisualBasic.Chr(10) & Microsoft.VisualBasic.Chr(10) & prnMemo & " " & objOrder.OrdComment, FontFactory.GetFont("Verdana", 7))

            'p = New Paragraph(prnAuthorisedby & Microsoft.VisualBasic.Chr(10) & Microsoft.VisualBasic.Chr(10) & prnMemo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblMemo.AddCell(cell)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)
            document.Add(tblMemo)

            document.Close()
            objOrder.CloseDatabaseConnection()
            objOrder = Nothing
            objOrderItems.CloseDatabaseConnection()
            objOrderItems = Nothing
            objOrderItemProcess.CloseDatabaseConnection()
            objOrderItemProcess = Nothing
        Else
            Dim header As HeaderFooter = New HeaderFooter(New Phrase(" "), False)
            header.Border = Rectangle.NO_BORDER
            header.Alignment = 0
            document.Header = header

            document.Open()

            objCompany.CompanyName = objCompany.funGetFirstCompanyName
            objCompany.getCompanyInfo()

            document.Add(New Paragraph(objCompany.CompanyName & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 10)))

            Dim strAddress As String = objCompany.CompanyAddressLine1 & IIf(objCompany.CompanyAddressLine2 <> "", ", " & objCompany.CompanyAddressLine2, "") & IIf(objCompany.CompanyCity <> "", ", " & objCompany.CompanyCity, "") & IIf(objCompany.CompanyState <> "", ", " & objCompany.CompanyState, "") & IIf(objCompany.CompanyCountry <> "", ", " & objCompany.CompanyCountry, "") & IIf(objCompany.CompanyPostalCode <> "", ", " & objCompany.CompanyPostalCode, "")
            strAddress = IIf(strAddress <> "", strAddress, "-")
            document.Add(New Paragraph(strAddress & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            Dim strTelFax As String = lblPhone & " " & IIf(objCompany.CompanyPOPhone <> "", objCompany.CompanyPOPhone, "-") & " | " & lblFax & " " & IIf(objCompany.CompanyFax <> "", objCompany.CompanyFax, "-")
            document.Add(New Paragraph(strTelFax & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            document.Add(New Paragraph(lblEmail & " " & IIf(objCompany.CompanyEmail <> "", objCompany.CompanyEmail, "-") & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            Dim cb As PdfContentByte = writer.DirectContent
            cb.MoveTo(30.0F, document.PageSize.Height - 120.0F)
            cb.LineTo(document.PageSize.Width - 30.0F, document.PageSize.Height - 120.0F)
            cb.Stroke()

            document.Add(New Paragraph(prnNoRecordFound & " ", FontFactory.GetFont("Tahoma", 16)))

            document.Close()
        End If
        Return strPath.Substring(strPath.LastIndexOf("\") + 1)
    End Function
    'Request Details
    Private Function subPDFGeneration(ByVal strReqID As String, ByVal strReqType As String) As String
        Dim document As Document = New Document

        Dim strPath As String = ""
        strPath = Current.Server.MapPath("~") & "\Pdf\" & strReqType & "-" & strReqID & "_" & Now.ToString("yyyyMMddhhmmss") & ".pdf"

        Dim writer As PdfWriter = PdfWriter.GetInstance(document, New FileStream(strPath, FileMode.Create))
        Dim events As clsPrint = New clsPrint
        writer.PageEvent = events

        Dim p As Paragraph
        Dim cell As Cell

        Dim objCompany As New clsCompanyInfo

        Dim objOrder As New clsOrders
        Dim objOrderItems As New clsOrderItems
        Dim objOrderItemProcess As New clsOrderItemProcess

        If strReqID <> "" And strReqType <> "" Then
            Dim header As HeaderFooter = New HeaderFooter(New Phrase(" "), False)
            header.Border = Rectangle.NO_BORDER
            header.Alignment = 0
            document.Header = header

            document.Open()

            Dim tblHeader As New Table(7)
            tblHeader.SetAlignment(Element.ALIGN_LEFT)
            tblHeader.Width = 100.0F
            tblHeader.AutoFillEmptyCells = True
            tblHeader.CellsFitPage = True
            tblHeader.BorderWidth = 0.0F
            tblHeader.BorderColor = New Color(255, 255, 255)
            tblHeader.Alignment = Element.ALIGN_LEFT
            tblHeader.BackgroundColor = New Color(255, 255, 255)

            objOrder.OrdID = strReqID
            objOrder.getOrdersInfo()

            'objCompany.CompanyName = objCompany.funGetFirstCompanyName
            objCompany.CompanyID = objOrder.ordCompanyID
            objCompany.getCompanyInfo()

            p = New Paragraph(objCompany.CompanyName & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 10))
            cell = New Cell(p)

            Dim strAddress As String = objCompany.CompanyAddressLine1 & IIf(objCompany.CompanyAddressLine2 <> "", ", " & objCompany.CompanyAddressLine2, "") & IIf(objCompany.CompanyCity <> "", ", " & objCompany.CompanyCity, "") & IIf(objCompany.CompanyState <> "", ", " & objCompany.CompanyState, "") & IIf(objCompany.CompanyCountry <> "", ", " & objCompany.CompanyCountry, "") & IIf(objCompany.CompanyPostalCode <> "", ", " & objCompany.CompanyPostalCode, "")
            strAddress = IIf(strAddress <> "", strAddress, "-")
            p = New Paragraph(strAddress & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8))
            cell.Add(p)

            Dim strTelFax As String = lblPhone & " " & IIf(objCompany.CompanyPOPhone <> "", objCompany.CompanyPOPhone, "-") & " | " & lblFax & " " & IIf(objCompany.CompanyFax <> "", objCompany.CompanyFax, "-")
            p = New Paragraph(strTelFax & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8))
            cell.Add(p)

            p = New Paragraph(lblEmail & " " & IIf(objCompany.CompanyEmail <> "", objCompany.CompanyEmail, "-") & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8))
            Cell.Add(p)
            Cell.HorizontalAlignment = Element.ALIGN_LEFT
            Cell.Colspan = 5
            Cell.BorderWidth = 0.0F
            tblHeader.AddCell(Cell)

            Dim strTitle As String = ""
            If strReqType = "QO" Then
                strTitle = prnQuotation
            ElseIf strReqType = "SO" Then
                strTitle = prnSalesOrder
            End If
            p = New Paragraph(strTitle, FontFactory.GetFont("Tahoma", 11, Font.BOLD))
            Cell = New Cell(p)
            Cell.HorizontalAlignment = Element.ALIGN_RIGHT
            Cell.VerticalAlignment = Element.ALIGN_MIDDLE
            Cell.Colspan = 2
            Cell.BorderWidth = 0.0F
            tblHeader.AddCell(Cell)
            document.Add(tblHeader)

            Dim cb As PdfContentByte = writer.DirectContent
            cb.MoveTo(30.0F, document.PageSize.Height - 120.0F)
            cb.LineTo(document.PageSize.Width - 30.0F, document.PageSize.Height - 120.0F)
            cb.Stroke()

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            'objOrder.OrdID = strReqID
            'objOrder.getOrdersInfo()

            Dim tblnumber As New Table(5)
            tblnumber.SetAlignment(Element.ALIGN_LEFT)
            tblnumber.Width = 100.0F
            tblnumber.AutoFillEmptyCells = True
            tblnumber.CellsFitPage = True
            tblnumber.BorderWidth = 0.0F
            tblnumber.BorderColor = New Color(255, 255, 255)
            tblnumber.Alignment = Element.ALIGN_LEFT
            tblnumber.BackgroundColor = New Color(255, 255, 255)
            tblnumber.Cellspacing = 2
            tblnumber.Cellpadding = 3

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.Colspan = 4
            cell.Rowspan = 2
            cell.BorderWidth = 0.0F
            tblnumber.AddCell(cell)

            Dim strDate As String = prnDate
            Dim strNumber As String = ""
            If strReqType = "QO" Then
                strNumber = prnQuotationNo
            ElseIf strReqType = "SO" Then
                strNumber = prnSONo
            End If

            p = New Paragraph(strDate + ": " + IIf(objOrder.OrdCreatedOn <> "", CDate(objOrder.OrdCreatedOn).ToString("MMM-dd yyyy"), "--"), FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblnumber.AddCell(cell)

            p = New Paragraph(strNumber + ": " + objOrder.OrdID, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblnumber.AddCell(cell)

            document.Add(tblnumber)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            Dim strCurrency As String = ""
            Dim aTable As New Table(15)
            aTable.SetAlignment(Element.ALIGN_LEFT)
            aTable.Width = 100.0F
            aTable.AutoFillEmptyCells = True
            aTable.CellsFitPage = True
            aTable.Alignment = Element.ALIGN_LEFT
            aTable.Cellspacing = 2
            aTable.Cellpadding = 2
            aTable.BorderWidth = 0.0F
            aTable.BorderColor = New Color(255, 255, 255)
            Dim strVendorInfo As String = ""
            strCurrency = objOrder.OrdCurrencyCode

            p = New Paragraph(prnBillTo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            cell.Colspan = 7
            aTable.AddCell(cell)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            aTable.AddCell(cell)

            p = New Paragraph(prnShipTo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            cell.Colspan = 7
            aTable.AddCell(cell)

            ' Bill To
            strVendorInfo += objOrder.OrdCustName
            strVendorInfo += Microsoft.VisualBasic.Chr(10)
            strVendorInfo += funPopulateAddress(objOrder.OrdCustType, "B", objOrder.OrdCustID)
            p = New Paragraph(strVendorInfo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 7
            aTable.AddCell(cell)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            aTable.AddCell(cell)

            ' Shipping
            Dim strShippingInfo As String = ""
            strShippingInfo += objOrder.OrdCustName
            strShippingInfo += Microsoft.VisualBasic.Chr(10)
            strShippingInfo += funPopulateAddress(objOrder.OrdCustType, "S", objOrder.OrdCustID)

            p = New Paragraph(strShippingInfo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 7
            aTable.AddCell(cell)
            document.Add(aTable)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            Dim blnFlag As Boolean = False
            If objOrderItems.funGetDiscount(strReqID) Then
                blnFlag = True
            End If

            Dim tblProduct As New Table(8)
            tblProduct.SetAlignment(Element.ALIGN_LEFT)
            tblProduct.Width = 100.0F
            tblProduct.AutoFillEmptyCells = True
            tblProduct.CellsFitPage = True
            tblProduct.Alignment = Element.ALIGN_LEFT
            tblProduct.BackgroundColor = New Color(255, 255, 255)
            tblProduct.Cellspacing = 2
            tblProduct.Cellpadding = 3

            'Header Start
            p = New Paragraph(grdPOUPCCode, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            If blnFlag = False Then
                cell.Colspan = 2
            End If

            tblProduct.AddCell(cell)
            p = New Paragraph(POProduct, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            cell.Colspan = 2
            tblProduct.AddCell(cell)

            p = New Paragraph(POTaxGrp, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProduct.AddCell(cell)

             If blnFlag = True Then
                p = New Paragraph(PODiscount, FontFactory.GetFont("Verdana", 8))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                cell.BackgroundColor = New Color(214, 214, 214)
                tblProduct.AddCell(cell)
            End If

            p = New Paragraph(POQuantity, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProduct.AddCell(cell)
            p = New Paragraph(POUnitPrice, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProduct.AddCell(cell)
            p = New Paragraph(POAmount, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProduct.AddCell(cell)
            'Header End

            Dim drItems As OdbcDataReader
            drItems = objOrderItems.GetDataReader(objOrderItems.funFillGrid(strReqID))
            Dim dblSubTotal As Double = 0
            Dim dblTotal As Double = 0
            While drItems.Read
                p = New Paragraph(drItems("prdUPCCode").ToString, FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                If blnFlag = False Then
                    cell.Colspan = 2
                End If
                tblProduct.AddCell(cell)

                p = New Paragraph(drItems("prdName"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                cell.Colspan = 2
                tblProduct.AddCell(cell)

                Dim strGrpName As String
                If drItems("sysTaxCodeDescText").ToString <> "" Then
                    strGrpName = drItems("sysTaxCodeDescText").ToString
                Else
                    strGrpName = "--"
                End If

                p = New Paragraph(strGrpName, FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                tblProduct.AddCell(cell)

                If blnFlag = True Then
                    p = New Paragraph(drItems("ordProductDiscount").ToString, FontFactory.GetFont("Verdana", 7))
                    cell = New Cell(p)
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    tblProduct.AddCell(cell)
                End If

                p = New Paragraph(drItems("ordProductQty"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProduct.AddCell(cell)
                p = New Paragraph(String.Format("{0:F}", drItems("ordProductUnitPrice")), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProduct.AddCell(cell)
                p = New Paragraph(objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", drItems("amount")), FontFactory.GetFont("Verdana", 7))
                dblSubTotal += CDbl(drItems("amount"))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProduct.AddCell(cell)
            End While
            drItems.Close()

            document.Add(tblProduct)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            Dim tblSubTotal As New Table(12)
            tblSubTotal.SetAlignment(Element.ALIGN_LEFT)
            tblSubTotal.Width = 100.0F
            tblSubTotal.AutoFillEmptyCells = True
            tblSubTotal.CellsFitPage = True
            tblSubTotal.Alignment = Element.ALIGN_LEFT
            tblSubTotal.Cellspacing = 2
            tblSubTotal.Cellpadding = 3
            tblSubTotal.BorderColor = New Color(255, 255, 255)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 9
            tblSubTotal.AddCell(cell)

            p = New Paragraph(prnSubTotal, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderColor = New Color(255, 255, 255)
            tblSubTotal.AddCell(cell)

            p = New Paragraph(objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", dblSubTotal), FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BorderColor = New Color(255, 255, 255)
            cell.Colspan = 2
            tblSubTotal.AddCell(cell)

            document.Add(tblSubTotal)

            Dim tblProcess As New Table(8)
            tblProcess.SetAlignment(Element.ALIGN_LEFT)
            tblProcess.Width = 100.0F
            tblProcess.AutoFillEmptyCells = True
            tblProcess.CellsFitPage = True
            tblProcess.Alignment = Element.ALIGN_LEFT
            tblProcess.BackgroundColor = New Color(255, 255, 255)
            tblProcess.Cellspacing = 2
            tblProcess.Cellpadding = 3

            'Header Start
            p = New Paragraph(grdProcessDescription, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            cell.Colspan = 2
            tblProcess.AddCell(cell)
            p = New Paragraph(grdProcessFixedCost, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProcess.AddCell(cell)
            p = New Paragraph(grdProcessCostPerHour, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProcess.AddCell(cell)
            p = New Paragraph(grdProcessCostPerUnit, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProcess.AddCell(cell)
            p = New Paragraph(prnTotalHour, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProcess.AddCell(cell)
            p = New Paragraph(prnTotalUnit, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProcess.AddCell(cell)
            p = New Paragraph(prnProcessCost, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProcess.AddCell(cell)
            'Header End

            Dim drProcess As OdbcDataReader
            objOrderItemProcess.OrdID = strReqID
            drProcess = objOrderItemProcess.GetDataReader(objOrderItemProcess.funFillGridForProcess())
            Dim dblProcessTotal As Double = 0
            Dim IsProcess As Boolean = False
            While drProcess.Read
                p = New Paragraph(drProcess("ProcessDescription"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                cell.Colspan = 2
                tblProcess.AddCell(cell)
                p = New Paragraph(String.Format("{0:F}", drProcess("ordItemProcFixedPrice")), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                p = New Paragraph(String.Format("{0:F}", drProcess("ordItemProcPricePerHour")), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                p = New Paragraph(String.Format("{0:F}", drProcess("ordItemProcPricePerUnit")), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                p = New Paragraph(drProcess("ordItemProcHours"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                p = New Paragraph(drProcess("ordItemProcUnits"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                p = New Paragraph(objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", drProcess("ProcessCost")), FontFactory.GetFont("Verdana", 7))
                dblProcessTotal += CDbl(drProcess("ProcessCost"))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                IsProcess = True
            End While
            drProcess.Close()

            If IsProcess = True Then
                p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
                document.Add(p)
                document.Add(tblProcess)
            End If

            Dim tblCalc As New Table(12)
            tblCalc.SetAlignment(Element.ALIGN_LEFT)
            tblCalc.Width = 100.0F
            tblCalc.AutoFillEmptyCells = True
            tblCalc.CellsFitPage = True
            tblCalc.Alignment = Element.ALIGN_LEFT
            tblCalc.Cellspacing = 2
            tblCalc.Cellpadding = 3
            tblCalc.BorderColor = New Color(255, 255, 255)

            'Calculate Total Start
            Dim strTaxArray(,) As String
            Dim drSOItems As Data.Odbc.OdbcDataReader
            drSOItems = objOrderItems.GetDataReader(objOrderItems.funFillGrid(strReqID))

            While drSOItems.Read
                Dim strTax As ArrayList
                Dim strTaxItem As String
                strTax = funCalcTax(drSOItems("ordProductID"), drSOItems("ordShpWhsCode"), drSOItems("amount"), "", objOrder.OrdCustID, objOrder.OrdCustType, drSOItems("ordProductTaxGrp").ToString)
                Dim intI As Integer = 0
                While strTax.Count - 1 >= intI
                    strTaxItem = strTax.Item(intI).ToString
                    strTaxArray = funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                    intI += 1
                End While
            End While

            Dim i As Integer = 0
            Dim j As Integer = 0
            If Not strTaxArray Is Nothing Then
                j = strTaxArray.Length / 2
            End If

            Dim objUser As New clsUser
            objUser.UserID = objOrder.OrdLastUpdateBy
            objUser.getUserInfo()

            Dim strUseName As String = objUser.UserFirstName & " " & objUser.UserLastName

            p = New Paragraph(prnAuthorisedby & " " & strUseName & Microsoft.VisualBasic.Chr(10) & Microsoft.VisualBasic.Chr(10) & prnMemo & " " & objOrder.OrdComment, FontFactory.GetFont("Verdana", 7))



            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 9
            cell.Rowspan = j + 1
            tblCalc.AddCell(cell)

            Dim dblTax As Double = 0

            While i < j
                p = New Paragraph(strTaxArray(0, i), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                cell.BorderColor = New Color(255, 255, 255)
                tblCalc.AddCell(cell)
                p = New Paragraph(objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", strTaxArray(1, i)), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                cell.BorderColor = New Color(255, 255, 255)
                cell.Colspan = 2
                tblCalc.AddCell(cell)

                dblTax += strTaxArray(1, i)
                i += 1
            End While

            p = New Paragraph(prnTotal, FontFactory.GetFont("Verdana", 7, Font.BOLD))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderColor = New Color(255, 255, 255)
            tblCalc.AddCell(cell)
            dblTotal = dblSubTotal + dblProcessTotal + dblTax
            p = New Paragraph(objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", dblTotal), FontFactory.GetFont("Verdana", 7, Font.BOLD))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BorderColor = New Color(255, 255, 255)
            cell.Colspan = 2
            tblCalc.AddCell(cell)
            'Calculate Total End

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)
            document.Add(tblCalc)

            'Generic Quotation Remarks:
            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 5))
            document.Add(p)

            Dim rTable As New Table(6)
            rTable.SetAlignment(Element.ALIGN_LEFT)
            rTable.Width = 100.0F
            rTable.AutoFillEmptyCells = True
            rTable.CellsFitPage = True
            rTable.BorderWidth = 0.0F
            rTable.BorderColor = New Color(255, 255, 255)
            rTable.Alignment = Element.ALIGN_LEFT
            rTable.BackgroundColor = New Color(255, 255, 255)
            cell = New Cell(p)
            'p = New Paragraph(lblGenQuotationRemarks, FontFactory.GetFont("Tahoma", 7, Font.BOLD))
            'cell.Add(p)

            Dim strGenericQuotationRemarks As String = IIf(objCompany.CompanyGenQuotationRemarks <> "", objCompany.CompanyGenQuotationRemarks, "")
            p = New Paragraph(strGenericQuotationRemarks & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Verdana", 7))
            cell.Add(p)

            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 6
            cell.BorderWidth = 0.0F
            rTable.AddCell(cell)

            document.Add(rTable)

            document.Close()
            objOrder.CloseDatabaseConnection()
            objOrder = Nothing
            objOrderItems.CloseDatabaseConnection()
            objOrderItems = Nothing
            objOrderItemProcess.CloseDatabaseConnection()
            objOrderItemProcess = Nothing
        Else
            Dim header As HeaderFooter = New HeaderFooter(New Phrase(" "), False)
            header.Border = Rectangle.NO_BORDER
            header.Alignment = 0
            document.Header = header

            document.Open()

            objCompany.CompanyName = objCompany.funGetFirstCompanyName
            objCompany.getCompanyInfo()

            document.Add(New Paragraph(objCompany.CompanyName & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 10)))

            Dim strAddress As String = objCompany.CompanyAddressLine1 & IIf(objCompany.CompanyAddressLine2 <> "", ", " & objCompany.CompanyAddressLine2, "") & IIf(objCompany.CompanyCity <> "", ", " & objCompany.CompanyCity, "") & IIf(objCompany.CompanyState <> "", ", " & objCompany.CompanyState, "") & IIf(objCompany.CompanyCountry <> "", ", " & objCompany.CompanyCountry, "") & IIf(objCompany.CompanyPostalCode <> "", ", " & objCompany.CompanyPostalCode, "")
            strAddress = IIf(strAddress <> "", strAddress, "-")
            document.Add(New Paragraph(strAddress & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            Dim strTelFax As String = lblPhone & " " & IIf(objCompany.CompanyPOPhone <> "", objCompany.CompanyPOPhone, "-") & " | " & lblFax & " " & IIf(objCompany.CompanyFax <> "", objCompany.CompanyFax, "-")
            document.Add(New Paragraph(strTelFax & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            document.Add(New Paragraph(lblEmail & " " & IIf(objCompany.CompanyEmail <> "", objCompany.CompanyEmail, "-") & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            Dim cb As PdfContentByte = writer.DirectContent
            cb.MoveTo(30.0F, document.PageSize.Height - 120.0F)
            cb.LineTo(document.PageSize.Width - 30.0F, document.PageSize.Height - 120.0F)
            cb.Stroke()

            document.Add(New Paragraph(prnNoRecordFound & " ", FontFactory.GetFont("Tahoma", 16)))

            document.Close()
        End If
        Return strPath.Substring(strPath.LastIndexOf("\") + 1)
    End Function

    'Request Details
    Private Function subQOGeneration(ByVal strReqID As String, ByVal strReqType As String) As String
        Dim document As Document = New Document

        Dim strPath As String = ""
        strPath = Current.Server.MapPath("~") & "\Pdf\" & strReqType & "-" & strReqID & "_" & Now.ToString("yyyyMMddhhmmss") & ".pdf"

        Dim writer As PdfWriter = PdfWriter.GetInstance(document, New FileStream(strPath, FileMode.Create))
        Dim events As clsPrint = New clsPrint
        writer.PageEvent = events

        Dim p As Paragraph
        Dim cell As Cell

        Dim objCompany As New clsCompanyInfo

        Dim objOrder As New clsOrders
        Dim objOrderItems As New clsOrderItems
        Dim objOrderItemProcess As New clsOrderItemProcess

        If strReqID <> "" And strReqType <> "" Then
            Dim header As HeaderFooter = New HeaderFooter(New Phrase(" "), False)
            header.Border = Rectangle.NO_BORDER
            header.Alignment = 0
            document.Header = header

            document.Open()

            Dim tblHeader As New Table(8)
            tblHeader.SetAlignment(Element.ALIGN_LEFT)
            tblHeader.Width = 100.0F
            tblHeader.AutoFillEmptyCells = True
            tblHeader.CellsFitPage = True
            tblHeader.BorderWidth = 0.0F
            tblHeader.BorderColor = New Color(255, 255, 255)
            tblHeader.Alignment = Element.ALIGN_LEFT
            tblHeader.BackgroundColor = New Color(255, 255, 255)

            objOrder.OrdID = strReqID
            objOrder.getOrdersInfo()
            objCompany.CompanyID = objOrder.ordCompanyID
            objCompany.getCompanyInfo()

            Dim logo As Image
            If objCompany.CompanyLogoPath <> "" Then
                logo = Image.GetInstance(Current.Request.PhysicalApplicationPath & "Upload\companylogo\" & objCompany.CompanyLogoPath)
                cell = New Cell(logo)
                cell.VerticalAlignment = Element.ALIGN_MIDDLE
                cell.BorderWidth = 0.0F
                tblHeader.AddCell(cell)
            End If
            
            p = New Paragraph(objCompany.CompanyName & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 10))
            cell = New Cell(p)

            If objCompany.CompanyLogoPath <> "" Then
                cell.Colspan = 3
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                cell.VerticalAlignment = Element.ALIGN_MIDDLE
            Else
                cell.Colspan = 4
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                cell.VerticalAlignment = Element.ALIGN_TOP
            End If

            cell.BorderWidth = 0.0F
            tblHeader.AddCell(cell)

            Dim strTitle As String = ""
            If strReqType = "QO" Then
                strTitle = prnQuotation
            ElseIf strReqType = "SO" Then
                strTitle = prnSalesOrder
            End If
            p = New Paragraph(strTitle, FontFactory.GetFont("Tahoma", 11, Font.BOLD))
            cell = New Cell(p)
            cell.VerticalAlignment = Element.ALIGN_MIDDLE
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.Colspan = 4
            cell.BorderWidth = 0.0F
            tblHeader.AddCell(cell)
            document.Add(tblHeader)

            Dim tblAddress As New Table(6)
            tblAddress.SetAlignment(Element.ALIGN_LEFT)
            tblAddress.Width = 100.0F
            tblAddress.AutoFillEmptyCells = True
            tblAddress.CellsFitPage = True
            tblAddress.BorderWidth = 0.0F
            tblAddress.BorderColor = New Color(255, 255, 255)
            tblAddress.Alignment = Element.ALIGN_LEFT
            tblAddress.BackgroundColor = New Color(255, 255, 255)

            Dim strAddress As String = objCompany.CompanyAddressLine1 & IIf(objCompany.CompanyAddressLine2 <> "", ", " & objCompany.CompanyAddressLine2, "") & Microsoft.VisualBasic.Chr(10) & IIf(objCompany.CompanyCity <> "", objCompany.CompanyCity, "") & IIf(objCompany.CompanyState <> "", ", " & objCompany.CompanyState, "") & IIf(objCompany.CompanyCountry <> "", ", " & objCompany.CompanyCountry, "") & IIf(objCompany.CompanyPostalCode <> "", ", " & objCompany.CompanyPostalCode, "")
            strAddress = IIf(strAddress <> "", strAddress, "-")
            p = New Paragraph(strAddress & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8))
            cell = New Cell(p)

            Dim strTelFax As String = lblPhone & " " & IIf(objCompany.CompanyPOPhone <> "", objCompany.CompanyPOPhone, "-") & " | " & lblFax & " " & IIf(objCompany.CompanyFax <> "", objCompany.CompanyFax, "-")
            p = New Paragraph(strTelFax & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8))
            cell.Add(p)

            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 4
            cell.BorderWidth = 0.0F
            tblAddress.AddCell(cell)



            'objOrder.OrdID = strReqID
            'objOrder.getOrdersInfo()

            Dim strDate As String = prnDate
            Dim strNumber As String = ""
            If strReqType = "QO" Then
                strNumber = prnQuotationNo
            ElseIf strReqType = "SO" Then
                strNumber = prnSONo
            End If

            p = New Paragraph(strDate + ": ", FontFactory.GetFont("Verdana", 7, Font.BOLD))
            cell = New Cell(p)
            p = New Paragraph(strNumber + ": ", FontFactory.GetFont("Verdana", 7, Font.BOLD))
            cell.Add(p)
            p = New Paragraph(prnCustId + ": ", FontFactory.GetFont("Verdana", 7, Font.BOLD))
            cell.Add(p)
            cell.BorderWidth = 0.0F
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            tblAddress.AddCell(cell)

            p = New Paragraph(IIf(objOrder.OrdCreatedOn <> "", CDate(objOrder.OrdCreatedOn).ToString("MMM-dd yyyy"), "--"), FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            p = New Paragraph(objOrder.OrdID, FontFactory.GetFont("Verdana", 7))
            cell.Add(p)
            p = New Paragraph(objOrder.OrdCustID, FontFactory.GetFont("Verdana", 7))
            cell.Add(p)
            cell.BackgroundColor = Color.YELLOW
            cell.BorderWidth = 0.0F
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblAddress.AddCell(cell)

            document.Add(tblAddress)





            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 5))
            document.Add(p)

            Dim aTable As New Table(6)
            aTable.SetAlignment(Element.ALIGN_LEFT)
            aTable.Width = 100.0F
            aTable.AutoFillEmptyCells = True
            aTable.CellsFitPage = True
            aTable.Alignment = Element.ALIGN_LEFT
            aTable.BorderWidth = 0.0F
            aTable.BorderColor = New Color(255, 255, 255)

            Dim strCurrency As String = ""
            strCurrency = objOrder.OrdCurrencyCode

            p = New Paragraph(prnQuoteTo & ":", FontFactory.GetFont("Verdana", 7, Font.BOLD))
            cell = New Cell(p)
            p = New Paragraph(objOrder.OrdCustName, FontFactory.GetFont("Verdana", 7))
            cell.Add(p)
            cell.BorderWidth = 0.0F
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 4
            aTable.AddCell(cell)

            p = New Paragraph(prnQuoteValid & ": ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            p = New Paragraph(prnQuoteBy & ": ", FontFactory.GetFont("Verdana", 7))
            cell.Add(p)
            cell.BorderWidth = 0.0F
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            aTable.AddCell(cell)

            Dim objUser As clsUser = New clsUser
            objUser.UserID = objOrder.OrdSalesRepID
            objUser.getUserInfo()
            Dim sQutExpDt As String = "--"
            If (IsDate(objOrder.QutExpDate)) = True Then
                sQutExpDt = CDate(objOrder.QutExpDate).ToString("MMM-dd yyyy")
            End If
            p = New Paragraph(sQutExpDt, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            p = New Paragraph(objUser.UserFirstName & " " & objUser.UserLastName, FontFactory.GetFont("Verdana", 7))
            cell.Add(p)
            cell.BorderWidth = 0.0F
            cell.BackgroundColor = Color.YELLOW
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            aTable.AddCell(cell)

            Dim strVendorInfo As String = ""
            strVendorInfo += funPopulateQOAddress(objOrder.OrdCustType, "B", objOrder.OrdCustID)
            p = New Paragraph(strVendorInfo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.BorderWidth = 0.0F
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 6
            aTable.AddCell(cell)

            document.Add(aTable)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 5))
            document.Add(p)

            p = New Paragraph(prnComments & ": " & IIf(objOrder.OrdComment <> "", objOrder.OrdComment, "--"), FontFactory.GetFont("Verdana", 7, Font.BOLD))
            document.Add(p)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 5))
            document.Add(p)

            Dim tblInfo As New Table(6)
            tblInfo.SetAlignment(Element.ALIGN_LEFT)
            tblInfo.Width = 100.0F
            tblInfo.AutoFillEmptyCells = True
            tblInfo.CellsFitPage = True
            tblInfo.Alignment = Element.ALIGN_LEFT
            tblInfo.BackgroundColor = New Color(255, 255, 255)
            tblInfo.Cellspacing = 2
            tblInfo.Cellpadding = 3

            'Header Start
            p = New Paragraph(prnRepresentative, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = Color.YELLOW
            tblInfo.AddCell(cell)

            p = New Paragraph(prnNumberOrder, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = Color.YELLOW
            tblInfo.AddCell(cell)

            p = New Paragraph(prnDateOfShipment, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = Color.YELLOW
            tblInfo.AddCell(cell)

            p = New Paragraph(prnShippedBy, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = Color.YELLOW
            tblInfo.AddCell(cell)

            p = New Paragraph(prnFrancoDeparture, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = Color.YELLOW
            tblInfo.AddCell(cell)

            p = New Paragraph(prnConditions, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = Color.YELLOW
            tblInfo.AddCell(cell)
            'Header End

            p = New Paragraph(objUser.UserFirstName & " " & objUser.UserLastName, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = Color.YELLOW
            tblInfo.AddCell(cell)

            p = New Paragraph("--", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblInfo.AddCell(cell)

            p = New Paragraph(IIf(objOrder.OrdShpDate <> "", CDate(objOrder.OrdShpDate).ToString("MMM-dd yyyy"), "--"), FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblInfo.AddCell(cell)

            p = New Paragraph("--", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblInfo.AddCell(cell)

            p = New Paragraph("--", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblInfo.AddCell(cell)

            p = New Paragraph("--", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblInfo.AddCell(cell)

            document.Add(tblInfo)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 5))
            document.Add(p)

            Dim blnFlag As Boolean = False
            If objOrderItems.funGetDiscount(strReqID) Then
                blnFlag = True
            End If

            Dim tblProduct As New Table(6)
            tblProduct.SetAlignment(Element.ALIGN_LEFT)
            tblProduct.Width = 100.0F
            tblProduct.AutoFillEmptyCells = True
            tblProduct.CellsFitPage = True
            tblProduct.Alignment = Element.ALIGN_LEFT
            tblProduct.BackgroundColor = New Color(255, 255, 255)
            tblProduct.Cellspacing = 2
            tblProduct.Cellpadding = 3

            'Header Start
            p = New Paragraph(POQuantity, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = Color.YELLOW
            tblProduct.AddCell(cell)

            p = New Paragraph(POProduct, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = Color.YELLOW
            cell.Colspan = 2
            tblProduct.AddCell(cell)

            p = New Paragraph(POUnitPrice, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = Color.YELLOW
            tblProduct.AddCell(cell)

            p = New Paragraph(prnTax, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_CENTER
            cell.BackgroundColor = Color.YELLOW
            tblProduct.AddCell(cell)

            p = New Paragraph(POAmount, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = Color.YELLOW
            tblProduct.AddCell(cell)
            'Header End

            Dim drItems As OdbcDataReader
            drItems = objOrderItems.GetDataReader(objOrderItems.funFillGrid(strReqID))
            Dim dblSubTotal As Double = 0
            Dim dblTotal As Double = 0
            Dim strIndividualTax As ArrayList

            While drItems.Read
                p = New Paragraph(drItems("ordProductQty"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProduct.AddCell(cell)

                p = New Paragraph(drItems("prdName"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                cell.Colspan = 2
                tblProduct.AddCell(cell)

                p = New Paragraph(String.Format("{0:F}", drItems("ordProductUnitPrice")), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProduct.AddCell(cell)

                strIndividualTax = funCalcTax(drItems("ordProductID"), drItems("ordShpWhsCode"), drItems("amount"), "", objOrder.OrdCustID, objOrder.OrdCustType, drItems("ordProductTaxGrp").ToString)
                p = New Paragraph(IIf(strIndividualTax.Count > 0, "T", "-"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                tblProduct.AddCell(cell)

                p = New Paragraph(objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", drItems("amount")), FontFactory.GetFont("Verdana", 7))
                dblSubTotal += CDbl(drItems("amount"))
                cell = New Cell(p)
                cell.BackgroundColor = New Color(179, 255, 198)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProduct.AddCell(cell)
            End While
            drItems.Close()

            document.Add(tblProduct)

            Dim tblSubTotal As New Table(12)
            tblSubTotal.SetAlignment(Element.ALIGN_LEFT)
            tblSubTotal.Width = 100.0F
            tblSubTotal.AutoFillEmptyCells = True
            tblSubTotal.CellsFitPage = True
            tblSubTotal.Alignment = Element.ALIGN_LEFT
            tblSubTotal.Cellspacing = 2
            tblSubTotal.Cellpadding = 3
            tblSubTotal.BorderColor = New Color(255, 255, 255)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 9
            tblSubTotal.AddCell(cell)

            p = New Paragraph(prnSubTotal, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderColor = New Color(255, 255, 255)
            tblSubTotal.AddCell(cell)

            p = New Paragraph(objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", dblSubTotal), FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(179, 255, 198)
            cell.BorderWidthLeft = 1
            cell.BorderWidthRight = 1
            cell.BorderWidthTop = 1
            cell.BorderWidthBottom = 1
            cell.Colspan = 2
            tblSubTotal.AddCell(cell)

            document.Add(tblSubTotal)

            Dim tblProcess As New Table(12)
            tblProcess.SetAlignment(Element.ALIGN_LEFT)
            tblProcess.Width = 100.0F
            tblProcess.AutoFillEmptyCells = True
            tblProcess.CellsFitPage = True
            tblProcess.Alignment = Element.ALIGN_LEFT
            tblProcess.BackgroundColor = New Color(255, 255, 255)
            tblProcess.Cellspacing = 2
            tblProcess.Cellpadding = 3

            'Header Start
            p = New Paragraph(grdProcessDescription, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = Color.YELLOW
            cell.Colspan = 3
            tblProcess.AddCell(cell)
            p = New Paragraph(grdProcessFixedCost, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = Color.YELLOW
            tblProcess.AddCell(cell)
            p = New Paragraph(grdProcessCostPerHour, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = Color.YELLOW
            cell.Colspan = 2
            tblProcess.AddCell(cell)
            p = New Paragraph(grdProcessCostPerUnit, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = Color.YELLOW
            cell.Colspan = 2
            tblProcess.AddCell(cell)
            p = New Paragraph(prnTotalHour, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = Color.YELLOW
            tblProcess.AddCell(cell)
            p = New Paragraph(prnTotalUnit, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = Color.YELLOW
            tblProcess.AddCell(cell)
            p = New Paragraph(prnProcessCost, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = Color.YELLOW
            cell.Colspan = 2
            tblProcess.AddCell(cell)
            'Header End

            Dim drProcess As OdbcDataReader
            objOrderItemProcess.OrdID = strReqID
            drProcess = objOrderItemProcess.GetDataReader(objOrderItemProcess.funFillGridForProcess())
            Dim dblProcessTotal As Double = 0
            Dim IsProcess As Boolean = False
            While drProcess.Read
                p = New Paragraph(drProcess("ProcessDescription"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                cell.Colspan = 3
                tblProcess.AddCell(cell)
                p = New Paragraph(String.Format("{0:F}", drProcess("ordItemProcFixedPrice")), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                p = New Paragraph(String.Format("{0:F}", drProcess("ordItemProcPricePerHour")), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.Colspan = 2
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                p = New Paragraph(String.Format("{0:F}", drProcess("ordItemProcPricePerUnit")), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.Colspan = 2
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                p = New Paragraph(drProcess("ordItemProcHours"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                p = New Paragraph(drProcess("ordItemProcUnits"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                p = New Paragraph(objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", drProcess("ProcessCost")), FontFactory.GetFont("Verdana", 7))
                dblProcessTotal += CDbl(drProcess("ProcessCost"))
                cell = New Cell(p)
                cell.Colspan = 2
                cell.BackgroundColor = New Color(179, 255, 198)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                IsProcess = True
            End While
            drProcess.Close()

            If IsProcess = True Then
                p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
                document.Add(p)
                document.Add(tblProcess)
            End If

            Dim tblCalc As New Table(12)
            tblCalc.SetAlignment(Element.ALIGN_LEFT)
            tblCalc.Width = 100.0F
            tblCalc.AutoFillEmptyCells = True
            tblCalc.CellsFitPage = True
            tblCalc.Alignment = Element.ALIGN_LEFT
            tblCalc.Cellspacing = 2
            tblCalc.Cellpadding = 3
            tblCalc.BorderColor = New Color(255, 255, 255)

            'Calculate Total Start
            Dim strTaxArray(,) As String
            Dim drSOItems As Data.Odbc.OdbcDataReader
            drSOItems = objOrderItems.GetDataReader(objOrderItems.funFillGrid(strReqID))

            While drSOItems.Read
                Dim strTax As ArrayList
                Dim strTaxItem As String
                strTax = funCalcTax(drSOItems("ordProductID"), drSOItems("ordShpWhsCode"), drSOItems("amount"), "", objOrder.OrdCustID, objOrder.OrdCustType, drSOItems("ordProductTaxGrp").ToString)
                Dim intI As Integer = 0
                While strTax.Count - 1 >= intI
                    strTaxItem = strTax.Item(intI).ToString
                    strTaxArray = funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                    intI += 1
                End While
            End While

            Dim i As Integer = 0
            Dim j As Integer = 0
            If Not strTaxArray Is Nothing Then
                j = strTaxArray.Length / 2
            End If

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 9
            cell.Rowspan = j + 1
            tblCalc.AddCell(cell)

            Dim dblTax As Double = 0

            While i < j
                p = New Paragraph(strTaxArray(0, i), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                cell.BorderColor = New Color(255, 255, 255)
                tblCalc.AddCell(cell)
                p = New Paragraph(objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", strTaxArray(1, i)), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                cell.Colspan = 2
                cell.BackgroundColor = Color.YELLOW
                cell.BorderWidthLeft = 1
                cell.BorderWidthRight = 1
                cell.BorderWidthTop = 1
                tblCalc.AddCell(cell)

                dblTax += strTaxArray(1, i)
                i += 1
            End While

            p = New Paragraph(prnTotal, FontFactory.GetFont("Verdana", 7, Font.BOLD))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderColor = New Color(255, 255, 255)
            tblCalc.AddCell(cell)
            dblTotal = dblSubTotal + dblProcessTotal + dblTax
            p = New Paragraph(objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", dblTotal), FontFactory.GetFont("Verdana", 7, Font.BOLD))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.Colspan = 2
            cell.BorderWidthLeft = 1
            cell.BorderWidthRight = 1
            cell.BorderWidthTop = 1
            cell.BorderWidthBottom = 1
            cell.BackgroundColor = New Color(179, 255, 198)
            tblCalc.AddCell(cell)
            'Calculate Total End

            document.Add(tblCalc)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 8))
            document.Add(p)

            p = New Paragraph(prnContact & " " & objUser.UserFirstName & " " & objUser.UserLastName & ", " & objUser.UserEmail, FontFactory.GetFont("Verdana", 8))
            p.Alignment = Element.ALIGN_CENTER
            document.Add(p)

            'Generic Quotation Remarks:
            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 5))
            document.Add(p)

            Dim rTable As New Table(6)
            rTable.SetAlignment(Element.ALIGN_LEFT)
            rTable.Width = 100.0F
            rTable.AutoFillEmptyCells = True
            rTable.CellsFitPage = True
            rTable.BorderWidth = 0.0F
            rTable.BorderColor = New Color(255, 255, 255)
            rTable.Alignment = Element.ALIGN_LEFT
            rTable.BackgroundColor = New Color(255, 255, 255)
            cell = New Cell(p)
            'p = New Paragraph(lblGenQuotationRemarks, FontFactory.GetFont("Tahoma", 7, Font.BOLD))
            'cell.Add(p)

            Dim strGenericQuotationRemarks As String = IIf(objCompany.CompanyGenQuotationRemarks <> "", objCompany.CompanyGenQuotationRemarks, "")
            p = New Paragraph(strGenericQuotationRemarks & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Verdana", 8))
            cell.Add(p)

            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 6
            cell.BorderWidth = 0.0F
            rTable.AddCell(cell)

            document.Add(rTable)


            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 8))
            document.Add(p)

            p = New Paragraph(prnThanks, FontFactory.GetFont("Verdana", 8, Font.BOLD))
            p.Alignment = Element.ALIGN_CENTER
            document.Add(p)

            document.Close()
            objUser.CloseDatabaseConnection()
            objUser = Nothing
            objOrder.CloseDatabaseConnection()
            objOrder = Nothing
            objOrderItems.CloseDatabaseConnection()
            objOrderItems = Nothing
            objOrderItemProcess.CloseDatabaseConnection()
            objOrderItemProcess = Nothing
        Else
            Dim header As HeaderFooter = New HeaderFooter(New Phrase(" "), False)
            header.Border = Rectangle.NO_BORDER
            header.Alignment = 0
            document.Header = header

            document.Open()

            objCompany.CompanyName = objCompany.funGetFirstCompanyName
            objCompany.getCompanyInfo()

            document.Add(New Paragraph(objCompany.CompanyName & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 10)))

            Dim strAddress As String = objCompany.CompanyAddressLine1 & IIf(objCompany.CompanyAddressLine2 <> "", ", " & objCompany.CompanyAddressLine2, "") & IIf(objCompany.CompanyCity <> "", ", " & objCompany.CompanyCity, "") & IIf(objCompany.CompanyState <> "", ", " & objCompany.CompanyState, "") & IIf(objCompany.CompanyCountry <> "", ", " & objCompany.CompanyCountry, "") & IIf(objCompany.CompanyPostalCode <> "", ", " & objCompany.CompanyPostalCode, "")
            strAddress = IIf(strAddress <> "", strAddress, "-")
            document.Add(New Paragraph(strAddress & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            Dim strTelFax As String = lblPhone & " " & IIf(objCompany.CompanyPOPhone <> "", objCompany.CompanyPOPhone, "-") & " | " & lblFax & " " & IIf(objCompany.CompanyFax <> "", objCompany.CompanyFax, "-")
            document.Add(New Paragraph(strTelFax & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            document.Add(New Paragraph(lblEmail & " " & IIf(objCompany.CompanyEmail <> "", objCompany.CompanyEmail, "-") & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            document.Add(New Paragraph(prnNoRecordFound & " ", FontFactory.GetFont("Tahoma", 16)))

            document.Close()
        End If
        Return strPath.Substring(strPath.LastIndexOf("\") + 1)
    End Function

    'Request Details
    Private Function subInvoiceGeneration(ByVal strReqID As String, ByVal strReqType As String) As String
        Dim document As Document = New Document

        Dim strPath As String = ""
        strPath = Current.Server.MapPath("~") & "\Pdf\" & strReqType & "-" & strReqID & "_" & Now.ToString("yyyyMMddhhmmss") & ".pdf"

        Dim writer As PdfWriter = PdfWriter.GetInstance(document, New FileStream(strPath, FileMode.Create))
        Dim events As clsPrint = New clsPrint
        writer.PageEvent = events

        Dim p As Paragraph
        Dim cell As Cell

        Dim objCompany As New clsCompanyInfo

        Dim objInvoice As New clsInvoices
        Dim objInvoiceItems As New clsInvoiceItems
        Dim objInvItemProcess As New clsInvItemProcess

        If strReqID <> "" And strReqType <> "" Then
            Dim header As HeaderFooter = New HeaderFooter(New Phrase(" "), False)
            header.Border = Rectangle.NO_BORDER
            header.Alignment = 0
            document.Header = header

            document.Open()

            Dim tblHeader As New Table(7)
            tblHeader.SetAlignment(Element.ALIGN_LEFT)
            tblHeader.Width = 100.0F
            tblHeader.AutoFillEmptyCells = True
            tblHeader.CellsFitPage = True
            tblHeader.BorderWidth = 0.0F
            tblHeader.BorderColor = New Color(255, 255, 255)
            tblHeader.Alignment = Element.ALIGN_LEFT
            tblHeader.BackgroundColor = New Color(255, 255, 255)


            objInvoice.InvID = strReqID
            objInvoice.getInvoicesInfo()
            objCompany.CompanyID = objInvoice.InvCompanyID
            ' objCompany.CompanyName = objCompany.funGetFirstCompanyName
            objCompany.getCompanyInfo()

            p = New Paragraph(objCompany.CompanyName & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 10))
            cell = New Cell(p)

            Dim strAddress As String = objCompany.CompanyAddressLine1 & IIf(objCompany.CompanyAddressLine2 <> "", ", " & objCompany.CompanyAddressLine2, "") & IIf(objCompany.CompanyCity <> "", ", " & objCompany.CompanyCity, "") & IIf(objCompany.CompanyState <> "", ", " & objCompany.CompanyState, "") & IIf(objCompany.CompanyCountry <> "", ", " & objCompany.CompanyCountry, "") & IIf(objCompany.CompanyPostalCode <> "", ", " & objCompany.CompanyPostalCode, "")
            strAddress = IIf(strAddress <> "", strAddress, "-")
            p = New Paragraph(strAddress & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8))
            cell.Add(p)

            Dim strTelFax As String = lblPhone & " " & IIf(objCompany.CompanyPOPhone <> "", objCompany.CompanyPOPhone, "-") & " | " & lblFax & " " & IIf(objCompany.CompanyFax <> "", objCompany.CompanyFax, "-")
            p = New Paragraph(strTelFax & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8))
            cell.Add(p)

            p = New Paragraph(lblEmail & " " & IIf(objCompany.CompanyEmail <> "", objCompany.CompanyEmail, "-") & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8))
            cell.Add(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 5
            cell.BorderWidth = 0.0F
            tblHeader.AddCell(cell)

            'objInvoice.InvID = strReqID
            'objInvoice.getInvoicesInfo()

            Dim strTitle As String = ""
            If objInvoice.InvRefType = "CN" Then
                strTitle = prnCreditNote
            Else
                strTitle = prnInvoice
            End If

            p = New Paragraph(strTitle, FontFactory.GetFont("Tahoma", 11, Font.BOLD))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.VerticalAlignment = Element.ALIGN_MIDDLE
            cell.Colspan = 2
            cell.BorderWidth = 0.0F
            tblHeader.AddCell(cell)
            document.Add(tblHeader)

            Dim cb As PdfContentByte = writer.DirectContent
            cb.MoveTo(30.0F, document.PageSize.Height - 120.0F)
            cb.LineTo(document.PageSize.Width - 30.0F, document.PageSize.Height - 120.0F)
            cb.Stroke()

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            Dim tblnumber As New Table(5)
            tblnumber.SetAlignment(Element.ALIGN_LEFT)
            tblnumber.Width = 100.0F
            tblnumber.AutoFillEmptyCells = True
            tblnumber.CellsFitPage = True
            tblnumber.BorderWidth = 0.0F
            tblnumber.BorderColor = New Color(255, 255, 255)
            tblnumber.Alignment = Element.ALIGN_LEFT
            tblnumber.BackgroundColor = New Color(255, 255, 255)
            tblnumber.Cellspacing = 2
            tblnumber.Cellpadding = 3

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.Colspan = 4
            cell.Rowspan = 3
            cell.BorderWidth = 0.0F
            tblnumber.AddCell(cell)

            p = New Paragraph(prnDate + ": " + IIf(objInvoice.InvCreatedOn <> "", CDate(objInvoice.InvCreatedOn).ToString("MMM-dd yyyy"), "--"), FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblnumber.AddCell(cell)

            p = New Paragraph(prnInvoiceNo + ": " + objInvoice.InvRefNo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblnumber.AddCell(cell)

            p = New Paragraph(prnCustPONo + ": " + objInvoice.InvCustPO, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            tblnumber.AddCell(cell)

            document.Add(tblnumber)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            Dim strCurrency As String = ""
            Dim aTable As New Table(15)
            aTable.SetAlignment(Element.ALIGN_LEFT)
            aTable.Width = 100.0F
            aTable.AutoFillEmptyCells = True
            aTable.CellsFitPage = True
            aTable.Alignment = Element.ALIGN_LEFT
            aTable.Cellspacing = 2
            aTable.Cellpadding = 2
            aTable.BorderWidth = 0.0F
            aTable.BorderColor = New Color(255, 255, 255)
            Dim strVendorInfo As String = ""
            strCurrency = objInvoice.InvCurrencyCode

            p = New Paragraph(prnBillTo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            cell.Colspan = 7
            aTable.AddCell(cell)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            aTable.AddCell(cell)

            p = New Paragraph(prnShipTo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            cell.Colspan = 7
            aTable.AddCell(cell)

            Dim objCust As New clsExtUser
            objCust.CustID = objInvoice.InvCustID
            objCust.CustType = objInvoice.InvCustType
            objCust.getCustomerInfoByType()
            Dim strCustName As String = objCust.CustName
            objCust = Nothing

            ' Bill To
            strVendorInfo += strCustName
            strVendorInfo += Microsoft.VisualBasic.Chr(10)
            strVendorInfo += funPopulateAddress(objInvoice.InvCustType, "B", objInvoice.InvCustID)
            p = New Paragraph(strVendorInfo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 7
            aTable.AddCell(cell)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderWidth = 0.0F
            cell.BorderColor = New Color(255, 255, 255)
            aTable.AddCell(cell)

            ' Shipping
            Dim strShippingInfo As String = ""
            strShippingInfo += strCustName
            strShippingInfo += Microsoft.VisualBasic.Chr(10)
            strShippingInfo += funPopulateAddress(objInvoice.InvCustType, "S", objInvoice.InvCustID)

            p = New Paragraph(strShippingInfo, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 7
            aTable.AddCell(cell)
            document.Add(aTable)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            'p = New Paragraph(prnComments & ": " & IIf(objInvoice.InvComment <> "", objInvoice.InvComment, "--"), FontFactory.GetFont("Verdana", 7, Font.BOLD))
            'document.Add(p)
            'Dim tblShipping As New Table(5)
            'tblShipping.SetAlignment(Element.ALIGN_LEFT)
            'tblShipping.Width = 100.0F
            'tblShipping.AutoFillEmptyCells = True
            'tblShipping.CellsFitPage = True
            'tblShipping.Alignment = Element.ALIGN_LEFT
            'tblShipping.BackgroundColor = New Color(255, 255, 255)
            'tblShipping.Cellspacing = 2
            'tblShipping.Cellpadding = 3

            ''Header Start
            'p = New Paragraph(prnShippingTerms, FontFactory.GetFont("Verdana", 7))
            'cell = New Cell(p)
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'cell.BackgroundColor = New Color(214, 214, 214)
            'cell.Colspan = 2
            'tblShipping.AddCell(cell)
            'p = New Paragraph(prnShippingTrackNo, FontFactory.GetFont("Verdana", 7))
            'cell = New Cell(p)
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'cell.BackgroundColor = New Color(214, 214, 214)
            'tblShipping.AddCell(cell)
            'p = New Paragraph(prnShippingCode, FontFactory.GetFont("Verdana", 7))
            'cell = New Cell(p)
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'cell.BackgroundColor = New Color(214, 214, 214)
            'tblShipping.AddCell(cell)
            'p = New Paragraph(prnShippingDate, FontFactory.GetFont("Verdana", 7))
            'cell = New Cell(p)
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'cell.BackgroundColor = New Color(214, 214, 214)
            'tblShipping.AddCell(cell)
            ''Header End

            'p = New Paragraph(IIf(objInvoice.InvComment <> "", objInvoice.InvComment, "--"), FontFactory.GetFont("Verdana", 7))
            'cell = New Cell(p)
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'cell.Colspan = 2
            'tblShipping.AddCell(cell)
            'p = New Paragraph(IIf(objInvoice.OrdShpTrackNo <> "", objInvoice.OrdShpTrackNo, "--"), FontFactory.GetFont("Verdana", 7))
            'cell = New Cell(p)
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'tblShipping.AddCell(cell)
            'p = New Paragraph(IIf(objInvoice.OrdShpCode <> "", objInvoice.OrdShpCode, "--"), FontFactory.GetFont("Verdana", 7))
            'cell = New Cell(p)
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'tblShipping.AddCell(cell)
            'p = New Paragraph(IIf(objInvoice.OrdShpDate <> "", CDate(objInvoice.OrdShpDate).ToString("MMM-dd yyyy"), "--"), FontFactory.GetFont("Verdana", 7))
            'cell = New Cell(p)
            'cell.HorizontalAlignment = Element.ALIGN_LEFT
            'tblShipping.AddCell(cell)

            'document.Add(tblShipping)

            'p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            'document.Add(p)


            Dim blnFlagIn As Boolean = False
            If objInvoiceItems.funGetDiscount(strReqID) Then
                blnFlagIn = True
            End If

            Dim tblProduct As New Table(8)
            tblProduct.SetAlignment(Element.ALIGN_LEFT)
            tblProduct.Width = 100.0F
            tblProduct.AutoFillEmptyCells = True
            tblProduct.CellsFitPage = True
            tblProduct.Alignment = Element.ALIGN_LEFT
            tblProduct.BackgroundColor = New Color(255, 255, 255)
            tblProduct.Cellspacing = 2
            tblProduct.Cellpadding = 3

            'Header Start
            p = New Paragraph(grdPOUPCCode, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            If blnFlagIn = False Then
                cell.Colspan = 2
            End If
            tblProduct.AddCell(cell)

            p = New Paragraph(POProduct, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            cell.Colspan = 2
            tblProduct.AddCell(cell)

            p = New Paragraph(POTaxGrp, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProduct.AddCell(cell)

            If blnFlagIn = True Then
                p = New Paragraph(PODiscount, FontFactory.GetFont("Verdana", 8))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                cell.BackgroundColor = New Color(214, 214, 214)
                tblProduct.AddCell(cell)
            End If

            p = New Paragraph(POQuantity, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProduct.AddCell(cell)

            p = New Paragraph(POUnitPrice, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProduct.AddCell(cell)
            p = New Paragraph(POAmount, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProduct.AddCell(cell)
            'Header End

            Dim drItems As OdbcDataReader
            drItems = objInvoiceItems.GetDataReader(objInvoiceItems.funFillGrid(strReqID))
            Dim dblSubTotal As Double = 0
            Dim dblTotal As Double = 0
            While drItems.Read
                p = New Paragraph(drItems("prdUPCCode").ToString, FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                If blnFlagIn = False Then
                    cell.Colspan = 2
                End If
                tblProduct.AddCell(cell)

                p = New Paragraph(drItems("prdName"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                cell.Colspan = 2
                tblProduct.AddCell(cell)

                Dim strGrpName As String
                If drItems("sysTaxCodeDescText").ToString <> "" Then
                    strGrpName = drItems("sysTaxCodeDescText").ToString
                Else
                    strGrpName = "--"
                End If

                p = New Paragraph(strGrpName, FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                tblProduct.AddCell(cell)

                If blnFlagIn = True Then
                    p = New Paragraph(drItems("invProductDiscount"), FontFactory.GetFont("Verdana", 7))
                    cell = New Cell(p)
                    cell.HorizontalAlignment = Element.ALIGN_LEFT
                    tblProduct.AddCell(cell)
                End If

                p = New Paragraph(drItems("invProductQty"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProduct.AddCell(cell)
                p = New Paragraph(String.Format("{0:F}", drItems("invProductUnitPrice")), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProduct.AddCell(cell)
                p = New Paragraph(objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", drItems("amount")), FontFactory.GetFont("Verdana", 7))
                dblSubTotal += CDbl(drItems("amount"))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProduct.AddCell(cell)
            End While
            drItems.Close()

            document.Add(tblProduct)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)

            Dim tblSubTotal As New Table(12)
            tblSubTotal.SetAlignment(Element.ALIGN_LEFT)
            tblSubTotal.Width = 100.0F
            tblSubTotal.AutoFillEmptyCells = True
            tblSubTotal.CellsFitPage = True
            tblSubTotal.Alignment = Element.ALIGN_LEFT
            tblSubTotal.Cellspacing = 2
            tblSubTotal.Cellpadding = 3
            tblSubTotal.BorderColor = New Color(255, 255, 255)

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 9
            tblSubTotal.AddCell(cell)

            p = New Paragraph(prnSubTotal, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderColor = New Color(255, 255, 255)
            tblSubTotal.AddCell(cell)

            p = New Paragraph(objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", dblSubTotal), FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BorderColor = New Color(255, 255, 255)
            cell.Colspan = 2
            tblSubTotal.AddCell(cell)

            document.Add(tblSubTotal)

            Dim tblProcess As New Table(8)
            tblProcess.SetAlignment(Element.ALIGN_LEFT)
            tblProcess.Width = 100.0F
            tblProcess.AutoFillEmptyCells = True
            tblProcess.CellsFitPage = True
            tblProcess.Alignment = Element.ALIGN_LEFT
            tblProcess.BackgroundColor = New Color(255, 255, 255)
            tblProcess.Cellspacing = 2
            tblProcess.Cellpadding = 3

            'Header Start
            p = New Paragraph(grdProcessDescription, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BackgroundColor = New Color(214, 214, 214)
            cell.Colspan = 2
            tblProcess.AddCell(cell)
            p = New Paragraph(grdProcessFixedCost, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProcess.AddCell(cell)
            p = New Paragraph(grdProcessCostPerHour, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProcess.AddCell(cell)
            p = New Paragraph(grdProcessCostPerUnit, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProcess.AddCell(cell)
            p = New Paragraph(prnTotalHour, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProcess.AddCell(cell)
            p = New Paragraph(prnTotalUnit, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProcess.AddCell(cell)
            p = New Paragraph(prnProcessCost, FontFactory.GetFont("Verdana", 8))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BackgroundColor = New Color(214, 214, 214)
            tblProcess.AddCell(cell)
            'Header End

            Dim drProcess As OdbcDataReader
            objInvItemProcess.Invoices_invID = strReqID
            drProcess = objInvItemProcess.GetDataReader(objInvItemProcess.funFillGridForProcess())
            Dim dblProcessTotal As Double = 0
            Dim IsProcess As Boolean = False
            While drProcess.Read
                p = New Paragraph(drProcess("ProcessDescription"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                cell.Colspan = 2
                tblProcess.AddCell(cell)
                p = New Paragraph(String.Format("{0:F}", drProcess("invItemProcFixedPrice")), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                p = New Paragraph(String.Format("{0:F}", drProcess("invItemProcPricePerHour")), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                p = New Paragraph(String.Format("{0:F}", drProcess("invItemProcPricePerUnit")), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                p = New Paragraph(drProcess("invItemProcHours"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                p = New Paragraph(drProcess("invItemProcUnits"), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                p = New Paragraph(objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", drProcess("ProcessCost")), FontFactory.GetFont("Verdana", 7))
                dblProcessTotal += CDbl(drProcess("ProcessCost"))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                tblProcess.AddCell(cell)
                IsProcess = True
            End While
            drProcess.Close()

            If IsProcess = True Then
                p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
                document.Add(p)
                document.Add(tblProcess)
            End If

            Dim tblCalc As New Table(12)
            tblCalc.SetAlignment(Element.ALIGN_LEFT)
            tblCalc.Width = 100.0F
            tblCalc.AutoFillEmptyCells = True
            tblCalc.CellsFitPage = True
            tblCalc.Alignment = Element.ALIGN_LEFT
            tblCalc.Cellspacing = 2
            tblCalc.Cellpadding = 3
            tblCalc.BorderColor = New Color(255, 255, 255)

            'Calculate Total Start
            Dim strTaxArray(,) As String
            Dim drSOItems As Data.Odbc.OdbcDataReader
            drSOItems = objInvoiceItems.GetDataReader(objInvoiceItems.funFillGrid(strReqID))
            
            While drSOItems.Read
                Dim strTax As ArrayList
                strTax = funCalcTax(drSOItems("invProductID"), drSOItems("invShpWhsCode"), drSOItems("amount"), "", objInvoice.InvCustID, objInvoice.InvCustType, drSOItems("invProductTaxGrp").ToString)
                Dim strTaxItem As String
                Dim intI As Integer = 0
                While strTax.Count - 1 >= intI
                    strTaxItem = strTax.Item(intI).ToString
                    strTaxArray = funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                    intI += 1
                End While
            End While

            Dim i As Integer = 0
            Dim j As Integer = 0
            If Not strTaxArray Is Nothing Then
                j = strTaxArray.Length / 2
            End If

            Dim objUser As New clsUser
            objUser.UserID = objInvoice.InvLastUpdateBy
            objUser.getUserInfo()

            Dim strUseName As String = objUser.UserFirstName & " " & objUser.UserLastName

            p = New Paragraph(prnAuthorisedby & " " & strUseName & Microsoft.VisualBasic.Chr(10) & Microsoft.VisualBasic.Chr(10) & prnMemo & " " & objInvoice.InvComment, FontFactory.GetFont("Verdana", 7))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 9
            cell.Rowspan = j + 1
            tblCalc.AddCell(cell)

            Dim dblTax As Double = 0

            While i < j
                p = New Paragraph(strTaxArray(0, i), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_LEFT
                cell.BorderColor = New Color(255, 255, 255)
                tblCalc.AddCell(cell)
                p = New Paragraph(objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", strTaxArray(1, i)), FontFactory.GetFont("Verdana", 7))
                cell = New Cell(p)
                cell.HorizontalAlignment = Element.ALIGN_RIGHT
                cell.BorderColor = New Color(255, 255, 255)
                cell.Colspan = 2
                tblCalc.AddCell(cell)

                dblTax += strTaxArray(1, i)
                i += 1
            End While

            p = New Paragraph(prnTotal, FontFactory.GetFont("Verdana", 7, Font.BOLD))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.BorderColor = New Color(255, 255, 255)
            tblCalc.AddCell(cell)
            dblTotal = dblSubTotal + dblProcessTotal + dblTax
            p = New Paragraph(objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", dblTotal), FontFactory.GetFont("Verdana", 7, Font.BOLD))
            cell = New Cell(p)
            cell.HorizontalAlignment = Element.ALIGN_RIGHT
            cell.BorderColor = New Color(255, 255, 255)
            cell.Colspan = 2
            tblCalc.AddCell(cell)
            'Calculate Total End

            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 3))
            document.Add(p)
            document.Add(tblCalc)


            'Generic Quotation Remarks:
            p = New Paragraph(" ", FontFactory.GetFont("Verdana", 5))
            document.Add(p)

            Dim rTable As New Table(6)
            rTable.SetAlignment(Element.ALIGN_LEFT)
            rTable.Width = 100.0F
            rTable.AutoFillEmptyCells = True
            rTable.CellsFitPage = True
            rTable.BorderWidth = 0.0F
            rTable.BorderColor = New Color(255, 255, 255)
            rTable.Alignment = Element.ALIGN_LEFT
            rTable.BackgroundColor = New Color(255, 255, 255)
            cell = New Cell(p)
            'p = New Paragraph(lblGenQuotationRemarks, FontFactory.GetFont("Tahoma", 7, Font.BOLD))
            'cell.Add(p)

            Dim strGenericInvoiceRemarks As String = IIf(objCompany.CompanyGenInvoiceRemarks <> "", objCompany.CompanyGenInvoiceRemarks, "")
            p = New Paragraph(strGenericInvoiceRemarks & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Verdana", 7))
            cell.Add(p)

            cell.HorizontalAlignment = Element.ALIGN_LEFT
            cell.Colspan = 6
            cell.BorderWidth = 0.0F
            rTable.AddCell(cell)

            document.Add(rTable)

            document.Close()
            objInvoice.CloseDatabaseConnection()
            objInvoice = Nothing
            objInvoiceItems.CloseDatabaseConnection()
            objInvoiceItems = Nothing
            objInvItemProcess.CloseDatabaseConnection()
            objInvItemProcess = Nothing
        Else
            Dim header As HeaderFooter = New HeaderFooter(New Phrase(" "), False)
            header.Border = Rectangle.NO_BORDER
            header.Alignment = 0
            document.Header = header

            document.Open()

            objCompany.CompanyName = objCompany.funGetFirstCompanyName
            objCompany.getCompanyInfo()

            document.Add(New Paragraph(objCompany.CompanyName & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 10)))

            Dim strAddress As String = objCompany.CompanyAddressLine1 & IIf(objCompany.CompanyAddressLine2 <> "", ", " & objCompany.CompanyAddressLine2, "") & IIf(objCompany.CompanyCity <> "", ", " & objCompany.CompanyCity, "") & IIf(objCompany.CompanyState <> "", ", " & objCompany.CompanyState, "") & IIf(objCompany.CompanyCountry <> "", ", " & objCompany.CompanyCountry, "") & IIf(objCompany.CompanyPostalCode <> "", ", " & objCompany.CompanyPostalCode, "")
            strAddress = IIf(strAddress <> "", strAddress, "-")
            document.Add(New Paragraph(strAddress & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            Dim strTelFax As String = lblPhone & " " & IIf(objCompany.CompanyPOPhone <> "", objCompany.CompanyPOPhone, "-") & " | " & lblFax & " " & IIf(objCompany.CompanyFax <> "", objCompany.CompanyFax, "-")
            document.Add(New Paragraph(strTelFax & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            document.Add(New Paragraph(lblEmail & " " & IIf(objCompany.CompanyEmail <> "", objCompany.CompanyEmail, "-") & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            Dim cb As PdfContentByte = writer.DirectContent
            cb.MoveTo(30.0F, document.PageSize.Height - 120.0F)
            cb.LineTo(document.PageSize.Width - 30.0F, document.PageSize.Height - 120.0F)
            cb.Stroke()

            document.Add(New Paragraph(prnNoRecordFound & " ", FontFactory.GetFont("Tahoma", 16)))

            document.Close()
        End If
        Return strPath.Substring(strPath.LastIndexOf("\") + 1)
    End Function

    Public Function funAddTax(ByVal strTaxArr(,) As String, ByVal TaxCode As String, ByVal Tax As Decimal) As Array
        Dim i As Integer = 0
        Dim j As Integer = 0
        If Not strTaxArr Is Nothing Then
            j = strTaxArr.Length / 2
        End If

        While i < j
            If strTaxArr(0, i) = TaxCode Then
                strTaxArr(1, i) = Convert.ToString(strTaxArr(1, i) + Tax)
                Return strTaxArr
            End If
            i += 1
        End While

        ReDim Preserve strTaxArr(1, j)
        strTaxArr(0, j) = TaxCode
        strTaxArr(1, j) = Tax
        Return strTaxArr
    End Function

    Public Overloads Overrides Sub onStartPage(ByVal writer As PdfWriter, ByVal doc As Document)
        Try
            If Current.Session("QO") = "" Then
                Dim cb As PdfContentByte = writer.DirectContent
                cb.MoveTo(30.0F, 30.0F)
                cb.LineTo(30.0F, doc.PageSize.Height - 50.0F)
                cb.Stroke()
                cb.MoveTo(30.0F, doc.PageSize.Height - 50.0F)
                cb.LineTo(doc.PageSize.Width - 30.0F, doc.PageSize.Height - 50.0F)
                cb.Stroke()
                cb.MoveTo(doc.PageSize.Width - 30.0F, doc.PageSize.Height - 50.0F)
                cb.LineTo(doc.PageSize.Width - 30.0F, 30.0F)
                cb.Stroke()
                cb.MoveTo(doc.PageSize.Width - 30.0F, 30.0F)
                cb.LineTo(30.0F, 30.0F)
                cb.Stroke()
            End If
        Catch de As DocumentException
        Catch ioe As IOException
        End Try
    End Sub

    Public Overloads Overrides Sub OnOpenDocument(ByVal writer As PdfWriter, ByVal doc As Document)
        Try
            bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
            cb = writer.DirectContent
            template = cb.CreateTemplate(50, 50)
        Catch de As DocumentException
        Catch ioe As IOException
        End Try
    End Sub

    Public Overloads Overrides Sub OnEndPage(ByVal writer As PdfWriter, ByVal doc As Document)
        Dim pageN As Integer = writer.PageNumber
        Dim text As String = prnPage & " " & pageN & " / "
        Dim len As Single = bf.GetWidthPoint(text, 8)
        cb.BeginText()
        cb.SetFontAndSize(bf, 8)
        cb.SetTextMatrix(520, 15)
        cb.ShowText(text)
        cb.EndText()
        cb.AddTemplate(template, 520 + len, 15)
        cb.BeginText()
        cb.SetFontAndSize(bf, 8)
        cb.SetTextMatrix(280, 820)
        cb.EndText()
    End Sub

    Public Overloads Overrides Sub OnCloseDocument(ByVal writer As PdfWriter, ByVal doc As Document)
        template.BeginText()
        template.SetFontAndSize(bf, 8)
        template.ShowText((writer.PageNumber - 1).ToString)
        template.EndText()
    End Sub

    'Request Details
    Private Function subPDFSalesGen(ByVal strReqID As String) As String
        Dim document As Document = New Document
        Dim strPath As String = ""
        strPath = Current.Server.MapPath("~") & "\Pdf\SO-" & strReqID & Now.ToString("yyyyMMddhhmmss") & ".pdf"
        If strReqID <> "" Then

            Dim writer As PdfWriter = PdfWriter.GetInstance(document, New FileStream(strPath, FileMode.Create))
            Dim footer As HeaderFooter = New HeaderFooter(New Phrase(prnPage & ": "), True)
            footer.Border = Rectangle.NO_BORDER
            footer.Alignment = 2
            'document.Footer = footer
            Dim header As HeaderFooter = New HeaderFooter(New Phrase(" "), False)
            header.Border = Rectangle.NO_BORDER
            header.Alignment = 0
            document.Header = header
            document.Open()
            
            document.Add(New Paragraph(prnCompanyTitle & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 10)))
            document.Add(New Paragraph(prnAddress & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))
            document.Add(New Paragraph(prnTelFax & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))
            document.Add(New Paragraph(contactUsEmailCol & " " & contactUsCompanyEmail & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            Dim cb As PdfContentByte = writer.DirectContent
            cb.MoveTo(30.0F, document.PageSize.Height - 120.0F)
            cb.LineTo(document.PageSize.Width - 30.0F, document.PageSize.Height - 120.0F)
            cb.Stroke()

            document.Add(New Paragraph("Sales Order Number" & ":" & Microsoft.VisualBasic.Space(25) & strReqID, FontFactory.GetFont("Tahoma", 12)))

            document.Close()
        Else
            Dim writer As PdfWriter = PdfWriter.GetInstance(document, New FileStream(strPath, FileMode.Create))
            Dim footer As HeaderFooter = New HeaderFooter(New Phrase(prnPage & ": "), True)
            footer.Border = Rectangle.NO_BORDER
            footer.Alignment = 2
            document.Footer = footer
            Dim header As HeaderFooter = New HeaderFooter(New Phrase(" "), False)
            header.Border = Rectangle.NO_BORDER
            header.Alignment = 0
            document.Header = header
            document.Open()
            
            document.Add(New Paragraph(prnCompanyTitle & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 10)))
            document.Add(New Paragraph(prnAddress & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))
            document.Add(New Paragraph(prnTelFax & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))
            document.Add(New Paragraph(contactUsEmailCol & " " & contactUsCompanyEmail & Microsoft.VisualBasic.Chr(10) & "" & Microsoft.VisualBasic.Chr(10) & "", FontFactory.GetFont("Tahoma", 8)))

            Dim cb As PdfContentByte = writer.DirectContent
            cb.MoveTo(30.0F, document.PageSize.Height - 120.0F)
            cb.LineTo(document.PageSize.Width - 30.0F, document.PageSize.Height - 120.0F)
            cb.Stroke()

            document.Add(New Paragraph(prnNoRecordFound & " ", FontFactory.GetFont("Tahoma", 16)))
        End If
        Return strPath.Substring(strPath.LastIndexOf("\") + 1)
    End Function

    'Calculate Tax
    Public Function funCalcTax(ByVal strPrdID As String, ByVal strWhsCode As String, ByVal Price As Double, Optional ByVal strPOS As String = "", Optional ByVal strCustomerID As String = "", Optional ByVal strCustomerType As String = "", Optional ByVal strTaxApplyBySO As String = "", Optional ByVal strRegCodeForPOS As String = "") As ArrayList
        Dim dblTotal As Double = 0
        Dim strTaxCode As String = ""
        Dim objDC As New clsDataClass
        Dim objDR As Data.Odbc.OdbcDataReader
        Dim objTaxCode As Object
        Dim strSQL As String = ""
        Dim strArr As New ArrayList
        Try
            If strTaxApplyBySO = "" Then
                strSQL = "SELECT prdTaxCode FROM prdquantity where prdID='" & strPrdID & "' and prdWhsCode='" & strWhsCode & "' "
                objTaxCode = Convert.ToString(objDC.GetScalarData(strSQL))
            ElseIf strTaxApplyBySO = 0 Then
                objTaxCode = ""
            Else
                objTaxCode = strTaxApplyBySO
            End If

            If objTaxCode.ToString = "" Or objTaxCode.ToString = "0" Then
                strSQL = ""
                If strPOS = "POS" Then
                    If strRegCodeForPOS <> "" Then 'Using for POS service
                        strSQL = "SELECT sysRegTaxCode FROM sysregister where sysRegWhsCode='" & strWhsCode & "' and sysRegCode='" & strRegCodeForPOS & "'"
                    Else
                        strSQL = "SELECT sysRegTaxCode FROM sysregister where sysRegWhsCode='" & strWhsCode & "' and sysRegCode='" & Current.Session("RegCode") & "'"
                    End If
                    objTaxCode = Convert.ToString(objDC.GetScalarData(strSQL))
                Else
                    If strCustomerID <> "" And (strCustomerType = "R" Or strCustomerType = "D" Or strCustomerType = "E") Then
                        strSQL = "SELECT PartnerTaxCode FROM partners where PartnerID ='" & strCustomerID & "' "
                        objTaxCode = Convert.ToString(objDC.GetScalarData(strSQL))
                    Else
                        objTaxCode = ""
                    End If
                End If
                If objTaxCode.ToString = "" Or objTaxCode.ToString = "0" Then
                    strSQL = "SELECT WarehouseRegTaxCode FROM syswarehouses where WarehouseCode='" & strWhsCode & "' "
                    objTaxCode = Convert.ToString(objDC.GetScalarData(strSQL))
                    If objTaxCode.ToString = "" Or objTaxCode.ToString = "0" Then
                        Return strArr
                    Else
                        strTaxCode = objTaxCode.ToString
                    End If
                Else
                    strTaxCode = objTaxCode.ToString
                End If
            Else
                strTaxCode = objTaxCode.ToString
            End If
            strSQL = ""
            strSQL = "select sysTaxCode.* from sysTaxCode Inner join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=sysTaxCode.systaxcode where sysTaxCode = '" & strTaxCode & "' order by sysTaxSequence"
            objDR = objDC.GetDataReader(strSQL)
            Dim nIdx As Integer = 0
            While objDR.Read
                'If objDR("sysTaxOnTotal").ToString = "0" Then
                '    dblTotal += Math.Round(Price * (CDbl(objDR("sysTaxPercentage") / 100)), 2)
                '    strArr.Add(objDR("sysTaxdesc").ToString & "@,@" & Math.Round(Price * (CDbl(objDR("sysTaxPercentage") / 100)), 2))
                'ElseIf objDR("sysTaxOnTotal").ToString = "1" Then
                '    dblTotal = Math.Round((Price + dblTotal) * (CDbl(objDR("sysTaxPercentage") / 100)), 2)
                '    strArr.Add(objDR("sysTaxdesc").ToString & "@,@" & dblTotal.ToString)
                'End If

                If objDR("sysTaxOnTotal").ToString = "0" Then
                    dblTotal += Price * (CDbl(objDR("sysTaxPercentage") / 100))
                    strArr.Add(objDR("sysTaxdesc").ToString & "@,@" & Math.Round(Price * (CDbl(objDR("sysTaxPercentage") / 100)), 2))
                ElseIf objDR("sysTaxOnTotal").ToString = "1" Then
                    dblTotal = Math.Round((Price + dblTotal) * (CDbl(objDR("sysTaxPercentage") / 100)), 2)
                    strArr.Add(objDR("sysTaxdesc").ToString & "@,@" & dblTotal.ToString)
                End If
                nIdx += 1
            End While
            objDR.Close()
            objDC.CloseDatabaseConnection()
            objDR = Nothing
            objDC = Nothing
        Catch ex As Exception
        End Try
        Return strArr
    End Function
    'Populate Address
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)
        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + Microsoft.VisualBasic.Chr(10)
            Else
                strAddress += objAdr.AddressState + Microsoft.VisualBasic.Chr(10)
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + Microsoft.VisualBasic.Chr(10)
            Else
                strAddress += objAdr.AddressCountry + Microsoft.VisualBasic.Chr(10)
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function

    'Populate Address
    Private Function funPopulateQOAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)
        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + ", "
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + ", "
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + ", "
            Else
                strAddress += objAdr.AddressState + ", "
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + Microsoft.VisualBasic.Chr(10)
            Else
                strAddress += objAdr.AddressCountry + Microsoft.VisualBasic.Chr(10)
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function

    'Get Warehouse Address
    Protected Function funWarehouseAddress(ByVal strWhsCode As String)
        Dim strData As String = ""
        If strWhsCode <> "" Then
            Dim objWhs As New clsWarehouses
            objWhs.WarehouseCode = strWhsCode
            objWhs.getWarehouseInfo()
            If objWhs.WarehouseDescription <> "" Then
                strData += objWhs.WarehouseDescription + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseAddressLine1 <> "" Then
                strData += objWhs.WarehouseAddressLine1 + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseAddressLine2 <> "" Then
                strData += objWhs.WarehouseAddressLine2 + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseCity <> "" Then
                strData += objWhs.WarehouseCity + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseState <> "" Then
                strData += objWhs.WarehouseState + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseCountry <> "" Then
                strData += objWhs.WarehouseCountry + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehousePostalCode <> "" Then
                strData += objWhs.WarehousePostalCode
            End If
            objWhs = Nothing
        End If
        Return strData
    End Function
    'Calculate Process Tax on warehouse
    Public Function funCalcProcessTax(ByVal strWhsCode As String, ByVal Price As Double) As ArrayList
        Dim dblTotal As Double = 0
        Dim strTaxCode As String = ""
        Dim objDC As New clsDataClass
        Dim objDR As Data.Odbc.OdbcDataReader
        Dim objTaxCode As Object
        Dim strSQL As String = ""
        Dim strArr As New ArrayList
        Try

            strSQL = "SELECT WarehouseRegTaxCode FROM syswarehouses where WarehouseCode='" & strWhsCode & "' "
            objTaxCode = Convert.ToString(objDC.GetScalarData(strSQL))
            If objTaxCode.ToString = "" Or objTaxCode.ToString = "0" Then
                Return strArr
            Else
                strTaxCode = objTaxCode.ToString
            End If


            strSQL = ""
            strSQL = "select sysTaxCode.* from sysTaxCode Inner join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=sysTaxCode.systaxcode where sysTaxCode = '" & strTaxCode & "' order by sysTaxSequence"
            objDR = objDC.GetDataReader(strSQL)
            Dim nIdx As Integer = 0
            While objDR.Read
                If objDR("sysTaxOnTotal").ToString = "0" Then
                    dblTotal += Math.Round(Price * (CDbl(objDR("sysTaxPercentage") / 100)), 2)
                    strArr.Add(objDR("sysTaxdesc").ToString & "@,@" & Math.Round(Price * (CDbl(objDR("sysTaxPercentage") / 100)), 2))
                ElseIf objDR("sysTaxOnTotal").ToString = "1" Then
                    dblTotal = Math.Round((Price + dblTotal) * (CDbl(objDR("sysTaxPercentage") / 100)), 2)
                    strArr.Add(objDR("sysTaxdesc").ToString & "@,@" & dblTotal.ToString)
                End If
                nIdx += 1
            End While
            objDR.Close()
            objDC.CloseDatabaseConnection()
            objDR = Nothing
            objDC = Nothing
        Catch ex As Exception
        End Try
        Return strArr
    End Function
    Public Function funTotalProcessTax(ByVal strTotalProcessAmt As Double, ByVal strWhs As String, ByVal strTaxArr(,) As String) As String(,)
        Dim strProcessTax As ArrayList
        Dim strTaxItem As String
        If strTotalProcessAmt > 0 Then
            Dim intJ As Integer = 0
            strProcessTax = funCalcProcessTax(strWhs, strTotalProcessAmt)

            While strProcessTax.Count - 1 >= intJ
                strTaxItem = strProcessTax.Item(intJ).ToString
                strTaxArr = funAddTax(strTaxArr, strTaxItem.Substring(0, strProcessTax.Item(intJ).IndexOf("@,@")), CDbl(strTaxItem.Substring(strProcessTax.Item(intJ).IndexOf("@,@") + 3)))
                intJ += 1
            End While
        End If
        Return strTaxArr
    End Function
End Class

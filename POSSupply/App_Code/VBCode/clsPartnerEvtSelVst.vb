Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsPartnerEvtSelVst
	Inherits clsDataClass
	Private _PartnerEvtSelVstId, _PartnerEvtSvcLogID, _PartnerEventVistorTxtID, _PartnerEvtVstVal As String
	Public Property PartnerEvtSelVstId() As String
		Get
			Return _PartnerEvtSelVstId
		End Get
		Set(ByVal value As String)
            _PartnerEvtSelVstId = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerEvtSvcLogID() As String
		Get
			Return _PartnerEvtSvcLogID
		End Get
		Set(ByVal value As String)
            _PartnerEvtSvcLogID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerEventVistorTxtID() As String
		Get
			Return _PartnerEventVistorTxtID
		End Get
		Set(ByVal value As String)
            _PartnerEventVistorTxtID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerEvtVstVal() As String
		Get
			Return _PartnerEvtVstVal
		End Get
		Set(ByVal value As String)
            _PartnerEvtVstVal = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert PartnerEvtSelVst
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertPartnerEvtSelVst() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO partnerevtselvst( PartnerEvtSvcLogID, PartnerEventVistorTxtID, PartnerEvtVstVal) VALUES('"
		strSQL += _PartnerEvtSvcLogID + "','"
		strSQL += _PartnerEventVistorTxtID + "','"
		strSQL += _PartnerEvtVstVal + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    '''  Update PartnerEvtSelVst
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updatePartnerEvtSelVst() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE partnerevtselvst set "
		strSQL += "PartnerEvtSvcLogID='" + _PartnerEvtSvcLogID + "', "
		strSQL += "PartnerEventVistorTxtID='" + _PartnerEventVistorTxtID + "', "
		strSQL += "PartnerEvtVstVal='" + _PartnerEvtVstVal + "'"
		strSQL += " where PartnerEvtSelVstId='" + _PartnerEvtSelVstId + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
    End Function
    ''' <summary>
    ''' Update PartnerEvtSelVst
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updatePartnerEvtSelVstWRTLogID() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE partnerevtselvst set "
        strSQL += "PartnerEvtVstVal='" + _PartnerEvtVstVal + "'"
        strSQL += " where PartnerEvtSvcLogID='" + _PartnerEvtSvcLogID + "' "
        strSQL += " and PartnerEventVistorTxtID='" + _PartnerEventVistorTxtID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    '''  Delete PartnerEvtSelVst
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function deletePartnerEvtSelVst() As Boolean
        Dim strSQL As String
        strSQL = "DELETE from partnerevtselvst"
        strSQL += " where PartnerEvtSvcLogID='" + _PartnerEvtSvcLogID + "' "
        strSQL += " and PartnerEventVistorTxtID='" + _PartnerEventVistorTxtID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    '''  Populate objects of PartnerEvtSelVst
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPartnerEvtSelVstInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  PartnerEvtSvcLogID, PartnerEventVistorTxtID, PartnerEvtVstVal FROM partnerevtselvst where PartnerEvtSelVstId='" + _PartnerEvtSelVstId + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_PartnerEvtSvcLogID = drObj.Item("PartnerEvtSvcLogID").ToString
			_PartnerEventVistorTxtID = drObj.Item("PartnerEventVistorTxtID").ToString
			_PartnerEvtVstVal = drObj.Item("PartnerEvtVstVal").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill Grid of PartnerEvtSelVst
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String = ""
        strSQL = "SELECT PartnerEvtSelVstId, PartnerEvtSvcLogID, PartnerEventVistorTxtID, PartnerEvtVstVal FROM partnerevtselvst where PartnerEvtSvcLogID='" + _PartnerEvtSvcLogID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Check Records
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckRecord() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM partnerevtselvst where PartnerEvtSvcLogID='" & _PartnerEvtSvcLogID & "' and PartnerEventVistorTxtID='" & _PartnerEventVistorTxtID & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
End Class


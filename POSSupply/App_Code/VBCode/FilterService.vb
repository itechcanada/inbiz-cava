Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<ScriptService()> _
Public Class FilterService
    Inherits System.Web.Services.WebService

    <WebMethod()> _
        Public Function GetPartner(ByVal prefixText As String, ByVal count As Integer) As String()
        Dim strError As New ArrayList(1)
        Try
            count = 10
            Dim i As Int16 = 0
            Dim items As New ArrayList(10)
            Dim strSql As String
            prefixText = clsCommon.funRemove(prefixText)
            strSql = "SELECT PartnerLongName FROM partners WHERE PartnerLongName Like '" & prefixText & "%'  and PartnerActive='1' Order BY PartnerLongName"
            Dim Dr As System.Data.Odbc.OdbcDataReader

            Dim obj As New clsDataClass
            Dr = obj.GetDataReader(strSql)
            While Dr.Read And i < 10
                items.Add(Dr.Item(0).ToString)
                i += 1
            End While
            Return (items.ToArray(GetType(String)))
        Catch ex As Exception
            strError.Add("Error in Execution")
            Return strError.ToArray(GetType(String))
        End Try
    End Function

    <WebMethod()> _
        Public Function GetTime(ByVal prefixText As String, ByVal count As Integer) As String()
        Dim strError As New ArrayList(1)
        Try
            count = 10
            Dim i As Int16 = 0
            Dim j As Int16 = 0
            Dim items As New ArrayList(10)
            Dim strData As String = ""
            Dim strHr As String = ""
            Dim strMi As String = ""
            prefixText = clsCommon.funRemove(prefixText)
            If IsNumeric(prefixText) Then
                If CInt(prefixText) >= 0 And CInt(prefixText) <= 23 Then
                    i = CInt(prefixText)
                End If
            End If
            While i < 24
                If i < 10 Then
                    strHr = "0" & i
                Else
                    strHr = i
                End If
                While j < 59
                    If j < 10 Then
                        strMi = "0" & j
                    Else
                        strMi = j
                    End If
                    strData = strHr & ":" & strMi & ":00"
                    items.Add(strData)
                    j += 10
                End While
                j = 0
                i += 1
            End While
            Return (items.ToArray(GetType(String)))
        Catch ex As Exception
            strError.Add("Error in Execution")
            Return strError.ToArray(GetType(String))
        End Try
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports System.Threading
Imports System.Globalization
'Imports System.Web.HttpContext
'Imports clsCommon
'Imports Resources.Resource
Public Class clsCurrencies
    Inherits clsDataClass
    Private _CurrencyCode, _CurrencyRelativePrice, _CurrencyActive As String
    Public Property CurrencyCode() As String
        Get
            Return _CurrencyCode
        End Get
        Set(ByVal value As String)
            _CurrencyCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CurrencyRelativePrice() As String
        Get
            Return _CurrencyRelativePrice
        End Get
        Set(ByVal value As String)
            _CurrencyRelativePrice = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CurrencyActive() As String
        Get
            Return _CurrencyActive
        End Get
        Set(ByVal value As String)
            _CurrencyActive = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    '''  Insert Currency
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertCurrency() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO syscurrencies(CurrencyCode, CurrencyRelativePrice, CurrencyActive) VALUES('"
        strSQL += _CurrencyCode + "','"
        strSQL += _CurrencyRelativePrice + "','"
        strSQL += _CurrencyActive + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update Currency
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateCurrency() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE syscurrencies set "
        strSQL += "CurrencyRelativePrice='" + _CurrencyRelativePrice + "', "
        strSQL += "CurrencyActive='" + _CurrencyActive + "'"
        strSQL += " where CurrencyCode='" + _CurrencyCode + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of Currency
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getCurrencyInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  CurrencyRelativePrice, CurrencyActive FROM syscurrencies where CurrencyCode='" + _CurrencyCode + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _CurrencyRelativePrice = drObj.Item("CurrencyRelativePrice").ToString
            _CurrencyActive = drObj.Item("CurrencyActive").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Check Duplicate Currency Code
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateCurrencyCode() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM syscurrencies where CurrencyCode='" & _CurrencyCode & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Delete Currency
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteCurrency(ByVal DeleteID As String) As String
        Dim strSQL As String
        strSQL = "DELETE FROM syscurrencies WHERE CurrencyCode='" + DeleteID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        'strSQL = "SELECT CurrencyCode, CurrencyRelativePrice FROM syscurrencies "
        strSQL = "SELECT Distinct CurrencyCode, CurrencyRelativePrice, CompanyBasCur FROM syscurrencies, syscompanyinfo "
        strSQL += "order by CurrencyCode "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid of Currency with search criteria
    ''' </summary>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal SearchData As String) As String
        Dim strSQL As String
        SearchData = clsCommon.funRemove(SearchData)
        'strSQL = "SELECT CurrencyCode, CurrencyRelativePrice FROM syscurrencies "
        strSQL = "SELECT Distinct CurrencyCode, CurrencyRelativePrice, CompanyBasCur FROM syscurrencies, syscompanyinfo "
        If SearchData <> "" Then
            strSQL += " where "
            strSQL += " ("
            strSQL += " CurrencyCode like '%" + SearchData + "%' or "
            strSQL += " CurrencyRelativePrice like '%" + SearchData + "%' "
            strSQL += " )"
        End If
        strSQL += " order by CurrencyCode "
        Return strSQL
    End Function
    ''' <summary>
    ''' Populate Currency
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub PopulateCurrency(ByVal ddl As DropDownList)
        Dim strSQL As String
        strSQL = "SELECT CurrencyCode FROM syscurrencies WHERE CurrencyActive='1' order by CurrencyCode "
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "CurrencyCode"
        ddl.DataValueField = "CurrencyCode"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.lstSelectCurrency
        ddl.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Get Relative Price
    ''' </summary>
    ''' <param name="CurCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getRelativePrice(ByVal CurCode As String) As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT CurrencyRelativePrice FROM syscurrencies where CurrencyCode='" + CurCode + "' "
        Try
            strData = GetScalarData(strSQL).ToString
        Catch ex As NullReferenceException
            strData = "0"
        Catch ex As Exception
            strData = "0"
        End Try
        Return strData
    End Function
    ''' <summary>
    ''' For converting Base Currency
    ''' </summary>
    ''' <param name="OrdExcRate"></param>
    ''' <param name="Amount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funBaseCurConversion(ByVal OrdExcRate As String, ByVal Amount As String) As String
        Dim obj As New clsCompanyInfo
        Dim str As String = ""
        If OrdExcRate <> "" Or Amount <> "" Then
            str = "(" & obj.funGetCompanyCurrency() & " " & String.Format("{0:F}", CDbl(Amount) / CDbl(OrdExcRate)) & ")"
        End If
        Return str
    End Function
End Class

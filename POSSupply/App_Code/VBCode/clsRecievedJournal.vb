Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.Odbc
'Imports clsCommon
'Imports System.Web.HttpContext
Public Class clsRecievedJournal

    Inherits clsDataClass
    Private _tagid, _whenRecieved, _poid As String
    Public Property Tagid() As String
        Get
            Return _tagid
        End Get
        Set(ByVal value As String)
            _tagid = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Property WhenRecieved() As String
        Get
            Return _whenRecieved
        End Get
        Set(ByVal value As String)
            _whenRecieved = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Poid() As String
        Get
            Return _poid
        End Get
        Set(ByVal value As String)
            _poid = clsCommon.funRemove(value, True)
        End Set
    End Property
    ''' <summary>
    ''' Insert RecievedJournal
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function InsertRecievedJournal() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO recievedjournal(tagid,whenRecieved,poid ) VALUES('"
        strSQL += _tagid + "','"
        strSQL += _whenRecieved + "','"
        strSQL += _poid + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
End Class

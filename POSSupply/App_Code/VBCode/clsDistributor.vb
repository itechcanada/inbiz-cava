Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
''Imports Resources.Resource
Public Class clsDistributor
	Inherits clsDataClass
    Private _distID, _distName, _distPhone, _distFax, _distEmail, _distType, _distActive, _distValidated, _distTaxCode, _distCommissionCode, _distDiscount, _distCurrencyCode, _distInvoiceNetTerms, _distShipBlankPref, _distCourierCode, _distLang, _distCustomerID, _distStatus, _distCompanyID As String
    Public Property DistID() As String
        Get
            Return _distID
        End Get
        Set(ByVal value As String)
            _distID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistName() As String
        Get
            Return _distName
        End Get
        Set(ByVal value As String)
            _distName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistPhone() As String
        Get
            Return _distPhone
        End Get
        Set(ByVal value As String)
            _distPhone = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistFax() As String
        Get
            Return _distFax
        End Get
        Set(ByVal value As String)
            _distFax = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistEmail() As String
        Get
            Return _distEmail
        End Get
        Set(ByVal value As String)
            _distEmail = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistType() As String
        Get
            Return _distType
        End Get
        Set(ByVal value As String)
            _distType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistActive() As String
        Get
            Return _distActive
        End Get
        Set(ByVal value As String)
            _distActive = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistValidated() As String
        Get
            Return _distValidated
        End Get
        Set(ByVal value As String)
            _distValidated = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistTaxCode() As String
        Get
            Return _distTaxCode
        End Get
        Set(ByVal value As String)
            _distTaxCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistCommissionCode() As String
        Get
            Return _distCommissionCode
        End Get
        Set(ByVal value As String)
            _distCommissionCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistDiscount() As String
        Get
            Return _distDiscount
        End Get
        Set(ByVal value As String)
            _distDiscount = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistCurrencyCode() As String
        Get
            Return _distCurrencyCode
        End Get
        Set(ByVal value As String)
            _distCurrencyCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistInvoiceNetTerms() As String
        Get
            Return _distInvoiceNetTerms
        End Get
        Set(ByVal value As String)
            _distInvoiceNetTerms = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistShipBlankPref() As String
        Get
            Return _distShipBlankPref
        End Get
        Set(ByVal value As String)
            _distShipBlankPref = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistCourierCode() As String
        Get
            Return _distCourierCode
        End Get
        Set(ByVal value As String)
            _distCourierCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistLanguage() As String
        Get
            Return _distLang
        End Get
        Set(ByVal value As String)
            _distLang = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistCustomerID() As String
        Get
            Return _distCustomerID
        End Get
        Set(ByVal value As String)
            _distCustomerID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DistStatus() As String
        Get
            Return _distStatus
        End Get
        Set(ByVal value As String)
            _distStatus = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property distCompanyID() As String
        Get
            Return _distCompanyID
        End Get
        Set(ByVal value As String)
            _distCompanyID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Insert Distributor
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertDistributor() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO distributor( distName, distPhone, distFax, distEmail, distType, distActive, distValidated, distTaxCode, distCommissionCode, distDiscount, distCurrencyCode, distInvoiceNetTerms, distShipBlankPref, distCourierCode, distLang, distCustomerID, distStatus,distCompanyID) VALUES('"
        strSQL += _distName + "','"
        strSQL += _distPhone + "','"
        strSQL += _distFax + "','"
        strSQL += _distEmail + "','"
        strSQL += _distType + "','"
        strSQL += _distActive + "','"
        strSQL += _distValidated + "','"
        strSQL += _distTaxCode + "','"
        strSQL += _distCommissionCode + "','"
        strSQL += _distDiscount + "','"
        strSQL += _distCurrencyCode + "','"
        strSQL += _distInvoiceNetTerms + "','"
        strSQL += _distShipBlankPref + "','"
        strSQL += _distCourierCode + "','"
        strSQL += _distLang + "','"
        strSQL += _distCustomerID + "','"
        strSQL += _distStatus + "','"
        strSQL += _distCompanyID + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update Distributor
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateDistributor() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE distributor set "
        strSQL += "distName='" + _distName + "', "
        strSQL += "distPhone='" + _distPhone + "', "
        strSQL += "distFax='" + _distFax + "', "
        strSQL += "distEmail='" + _distEmail + "', "
        strSQL += "distType='" + _distType + "', "
        strSQL += "distActive='" + _distActive + "', "
        strSQL += "distValidated='" + _distValidated + "', "
        strSQL += "distTaxCode='" + _distTaxCode + "', "
        strSQL += "distCommissionCode='" + _distCommissionCode + "', "
        strSQL += "distDiscount='" + _distDiscount + "', "
        strSQL += "distCurrencyCode='" + _distCurrencyCode + "', "
        strSQL += "distInvoiceNetTerms='" + _distInvoiceNetTerms + "', "
        strSQL += "distShipBlankPref='" + _distShipBlankPref + "', "
        strSQL += "distCourierCode='" + _distCourierCode + "', "
        strSQL += "distLang='" + _distLang + "', "
        strSQL += "distCustomerID='" + _distCustomerID + "', "
        strSQL += "distCompanyID='" + _distCompanyID + "', "
        strSQL += "distStatus='" + _distStatus + "'"
        strSQL += " where distID='" + _distID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of Distributor
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getDistributorInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  distName, distPhone, distFax, distEmail, distType, distActive, distValidated, distTaxCode, distCommissionCode, distDiscount, distCurrencyCode, distInvoiceNetTerms, distShipBlankPref, distCourierCode, distLang, distCustomerID, distStatus,distCompanyID FROM distributor where distID='" + _distID + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _distName = drObj.Item("distName").ToString
            _distPhone = drObj.Item("distPhone").ToString
            _distFax = drObj.Item("distFax").ToString
            _distEmail = drObj.Item("distEmail").ToString
            _distType = drObj.Item("distType").ToString
            _distActive = drObj.Item("distActive").ToString
            _distValidated = drObj.Item("distValidated").ToString
            _distTaxCode = drObj.Item("distTaxCode").ToString
            _distCommissionCode = drObj.Item("distCommissionCode").ToString
            _distDiscount = drObj.Item("distDiscount").ToString
            _distCurrencyCode = drObj.Item("distCurrencyCode").ToString
            _distInvoiceNetTerms = drObj.Item("distInvoiceNetTerms").ToString
            _distShipBlankPref = drObj.Item("distShipBlankPref").ToString
            _distCourierCode = drObj.Item("distCourierCode").ToString
            _distLang = drObj.Item("distLang").ToString
            _distCustomerID = drObj.Item("distCustomerID").ToString
            _distStatus = drObj.Item("distStatus").ToString
            _distCompanyID = drObj.Item("distCompanyID").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Return Distributor ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetDistributorID() As String
        Dim strSQL As String
        strSQL = "SELECT distID FROM distributor where distName='" & _distName & "' and distEmail='" & _distEmail & "' "
        Dim strData As String = ""
        strData = GetScalarData(strSQL)
        Return strData
    End Function
    ''' <summary>
    ''' Check Duplicate Distributor
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateDistributor() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM distributor where distName='" & _distName & "' and distEmail='" & _distEmail & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    '''  Delete Distributor
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteDistributor(ByVal DeleteID As String) As String
        Dim strSQL As String
        'strSQL = "DELETE FROM distributor WHERE distID='" + DeleteID + "' "
        strSQL = "UPDATE distributor set distActive=0 WHERE distID='" + DeleteID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill grid 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distID, distName, distPhone, distFax, distEmail FROM distributor where distActive=1 order by distID, distName "
        Else
            strSQL = "SELECT distID, distName, distPhone, distFax, distEmail FROM distributor inner join salesrepcustomer s on s.CustomerID=distID and s.CustomerType='" & iTECH.InbizERP.BusinessLogic.AddressReference.DISTRIBUTER & "' and s.UserID='" & HttpContext.Current.Session("UserID") & "' where distActive=1 order by distID, distName "
        End If

        Return strSQL
    End Function
    ''' <summary>
    ''' Fill grid with search criteria
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal StatusData As String, ByVal SearchData As String) As String
        Dim strSQL As String
        SearchData = clsCommon.funRemove(SearchData)
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distID, distName, distPhone, distFax, distEmail FROM distributor where distActive=1 "
        Else
            strSQL = "SELECT distID, distName, distPhone, distFax, distEmail FROM distributor left join salesrepcustomer s on s.CustomerID=distID and s.CustomerType='" & iTECH.InbizERP.BusinessLogic.AddressReference.DISTRIBUTER & "' and s.UserID='" & HttpContext.Current.Session("UserID") & "' where distActive=1 "
        End If

        If StatusData <> "" And SearchData <> "" Then
            strSQL += " and distStatus='" + StatusData + "' and "
            strSQL += " ("
            strSQL += " distName like '%" + SearchData + "%' or "
            strSQL += " distPhone like '%" + SearchData + "%' or "
            strSQL += " distEmail like '%" + SearchData + "%' "
            strSQL += " )"
        ElseIf StatusData <> "" And SearchData = "" Then
            strSQL += " and distStatus='" + StatusData + "' and "
            strSQL += " ("
            strSQL += " distName like '%" + SearchData + "%' or "
            strSQL += " distPhone like '%" + SearchData + "%' or "
            strSQL += " distEmail like '%" + SearchData + "%' "
            strSQL += " )"
        ElseIf StatusData = "" Then
            strSQL += " and "
            strSQL += " ("
            strSQL += " distName like '%" + SearchData + "%' or "
            strSQL += " distPhone like '%" + SearchData + "%' or "
            strSQL += " distEmail like '%" + SearchData + "%' "
            strSQL += " )"
        End If
        strSQL += " order by distID, distName "
        Return strSQL
    End Function
    ''' <summary>
    ''' Populate Distributor
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub PopulateDistributor(ByVal ddl As DropDownList)
        Dim strSQL As String
        strSQL = "SELECT distID, distName FROM distributor WHERE distActive='1' order by distName "
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "distName"
        ddl.DataValueField = "distID"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.SelectDistributor
        ddl.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Insert Customer ID In Webshop
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertWebshop() As Boolean
        Dim strSQL As String = ""
        Dim strConn As String = ""
        strConn = ConfigurationManager.ConnectionStrings("ConnectionStringW").ConnectionString
        Dim obj As New clsDataClass(strConn)
        strSQL = "INSERT INTO customer(cust_divid, cust_customerid, cust_billtoconame, cust_phoneno, cust_faxno, cust_emailid) VALUES('"
        strSQL += "2" + "','"
        strSQL += DistCustomerID + "','" + _distName + "','" + _distPhone + "','" + _distFax + "','" + _distEmail + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim intDataCustomer As Int16 = 0
        intDataCustomer = obj.PopulateData(strSQL)
        strSQL = "INSERT INTO websiterequest(req_divid, req_customerid, req_date, req_hoursofoperation, req_typeofoperation) VALUES('"
        strSQL += "2" + "','"
        strSQL += DistCustomerID + "','" + Now.ToString("yyyy-MM-dd hh:mm:ss") + "','+','+')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim intDataWebsite As Int16 = 0
        intDataWebsite = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intDataWebsite = 0 And intDataCustomer = 0 Then
            Return False
        ElseIf intDataWebsite > 0 And intDataCustomer > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Populate SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <remarks></remarks>
    Public Sub PopulateSR(ByVal chklst As CheckBoxList)
        Dim strSQL As String
        strSQL = "SELECT userID, concat(userFirstName, ' ',userLastName) as Name FROM users WHERE userActive='1' and userModules like '%SR%' order by Name "
        chklst.Items.Clear()
        chklst.DataSource = GetDataReader(strSQL)
        chklst.DataTextField = "Name"
        chklst.DataValueField = "userID"
        chklst.DataBind()
        CloseDatabaseConnection()
    End Sub

    ''' <summary>
    ''' Populate User's SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <param name="DistID"></param>
    ''' <remarks></remarks>
    Public Sub PopulateUserSR(ByVal chklst As CheckBoxList, ByVal DistID As String)
        Dim strSQL As String
        strSQL = "SELECT u.userID, concat(userFirstName, ' ',userLastName) as Name, 'A' as seq FROM users u inner join salesrepcustomer s on s.UserID=u.userID and CustomerID='" & DistID & "' and CustomerType='D' WHERE userActive='1' and userModules like '%SR%' Union SELECT u.userID, concat(userFirstName, ' ',userLastName) as Name, 'B' as seq FROM users u WHERE userActive='1' and userModules like '%SR%' and u.userID not in (Select UserID from salesrepcustomer where CustomerID='" & DistID & "' and CustomerType='D') order by seq, Name"
        chklst.Items.Clear()
        chklst.DataSource = GetDataReader(strSQL)
        chklst.DataTextField = "Name"
        chklst.DataValueField = "userID"
        chklst.DataBind()
        CloseDatabaseConnection()

        Dim SR As String = GetUserSR(DistID)
        For i As Int16 = 0 To chklst.Items.Count - 1
            If SR.Contains("~" & chklst.Items(i).Value & "~") Then
                chklst.Items(i).Selected = True
            End If
        Next
    End Sub

    ''' <summary>
    ''' Get User's SR
    ''' </summary>
    ''' <param name="DistID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetUserSR(ByVal DistID As String) As String
        Dim strSQL As String
        strSQL = "SELECT u.userID FROM users u inner join salesrepcustomer s on s.UserID=u.userID and CustomerID='" & DistID & "' and CustomerType='D' WHERE userActive='1' and userModules like '%SR%' "
        Dim SR As String = "~"
        Dim dr As OdbcDataReader = GetDataReader(strSQL)
        While dr.Read
            SR += dr.Item(0) & "~"
        End While
        CloseDatabaseConnection()
        Return SR
    End Function

    ''' <summary>
    ''' Insert SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <param name="DistID"></param>
    ''' <remarks></remarks>
    Public Sub InsertSR(ByVal chklst As CheckBoxList, ByVal DistID As String)
        Dim objSR As New clsSalesRepCustomer
        Dim i As Integer
        For i = 0 To chklst.Items.Count - 1
            If chklst.Items(i).Selected = True Then
                objSR.UserID = chklst.Items(i).Value
                objSR.CustID = DistID
                objSR.CustType = iTECH.InbizERP.BusinessLogic.AddressReference.DISTRIBUTER
                objSR.insertSalesRepCustomer()
            End If
        Next
    End Sub

    ''' <summary>
    ''' Update SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <param name="DistID"></param>
    ''' <remarks></remarks>
    Public Sub UpdateSR(ByVal chklst As CheckBoxList, ByVal DistID As String)
        Dim objSR As New clsSalesRepCustomer
        objSR.CustID = DistID
        objSR.CustType = iTECH.InbizERP.BusinessLogic.AddressReference.DISTRIBUTER
        objSR.DeleteSalesRepCustomer()

        For i As Int16 = 0 To chklst.Items.Count - 1
            If chklst.Items(i).Selected = True Then
                objSR.UserID = chklst.Items(i).Value
                objSR.CustID = DistID
                objSR.CustType = iTECH.InbizERP.BusinessLogic.AddressReference.DISTRIBUTER
                objSR.insertSalesRepCustomer()
            End If
        Next
    End Sub
    ''' <summary>
    ''' Return New Distributor ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetNewDistributorID() As String
        Dim strSQL As String
        strSQL = "SELECT Max(distID) FROM distributor "
        Dim strData As String = ""
        strData = GetScalarData(strSQL)
        Return strData
    End Function
End Class

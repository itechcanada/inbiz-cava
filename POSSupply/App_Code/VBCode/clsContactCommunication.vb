Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsContactCommunication
	Inherits clsDataClass
	Private _ComID, _ComContactID, _ComType, _ComMotive, _ComDateTime, _ComServiceLog, _ComEmailMessage, _ComCreatedBy As String
	Public Property ComID() As String
		Get
			Return _ComID
		End Get
		Set(ByVal value As String)
            _ComID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ComContactID() As String
		Get
			Return _ComContactID
		End Get
		Set(ByVal value As String)
            _ComContactID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ComType() As String
		Get
			Return _ComType
		End Get
		Set(ByVal value As String)
            _ComType = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ComMotive() As String
		Get
			Return _ComMotive
		End Get
		Set(ByVal value As String)
            _ComMotive = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ComDateTime() As String
		Get
			Return _ComDateTime
		End Get
		Set(ByVal value As String)
            _ComDateTime = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ComServiceLog() As String
		Get
			Return _ComServiceLog
		End Get
		Set(ByVal value As String)
            _ComServiceLog = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ComEmailMessage() As String
		Get
			Return _ComEmailMessage
		End Get
		Set(ByVal value As String)
            _ComEmailMessage = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ComCreatedBy() As String
		Get
			Return _ComCreatedBy
		End Get
		Set(ByVal value As String)
            _ComCreatedBy = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    '''  Insert ContactCommunication
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertContactCommunication() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO contactcommunication( ComContactID, ComType, ComMotive, ComDateTime, ComServiceLog, ComEmailMessage, ComCreatedBy) VALUES('"
		strSQL += _ComContactID + "','"
		strSQL += _ComType + "','"
		strSQL += _ComMotive + "','"
		strSQL += _ComDateTime + "','"
		strSQL += _ComServiceLog + "','"
		strSQL += _ComEmailMessage + "','"
		strSQL += _ComCreatedBy + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update ContactCommunication
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateContactCommunication() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE contactcommunication set "
		strSQL += "ComContactID='" + _ComContactID + "', "
		strSQL += "ComType='" + _ComType + "', "
		strSQL += "ComMotive='" + _ComMotive + "', "
		strSQL += "ComDateTime='" + _ComDateTime + "', "
		strSQL += "ComServiceLog='" + _ComServiceLog + "', "
		strSQL += "ComEmailMessage='" + _ComEmailMessage + "', "
		strSQL += "ComCreatedBy='" + _ComCreatedBy + "'"
		strSQL += " where ComID='" + _ComID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of ContactCommunication
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getContactCommunicationInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  ComContactID, ComType, ComMotive, ComDateTime, ComServiceLog, ComEmailMessage, ComCreatedBy FROM contactcommunication where ComID='" + _ComID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_ComContactID = drObj.Item("ComContactID").ToString
			_ComType = drObj.Item("ComType").ToString
			_ComMotive = drObj.Item("ComMotive").ToString
			_ComDateTime = drObj.Item("ComDateTime").ToString
			_ComServiceLog = drObj.Item("ComServiceLog").ToString
			_ComEmailMessage = drObj.Item("ComEmailMessage").ToString
			_ComCreatedBy = drObj.Item("ComCreatedBy").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill Grid of Contact Communication
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String = ""
        strSQL = "SELECT ComID, ComContactID, ComType, ComMotive, ComDateTime, ComServiceLog, ComEmailMessage, ComCreatedBy, Concat(ContactLastName,' ',ifnull(ContactFirstName,'')) as ContactName, PartnerID, PartnerLongName FROM contactcommunication cc inner join partnercontacts pc on pc.ContactID=cc.ComContactID inner join partners p on p.PartnerID=pc.ContactPartnerID "
        strSQL += " where ComContactID='" & _ComContactID & "' "
        strSQL += " order by ComDateTime desc "
        Return strSQL
    End Function
End Class

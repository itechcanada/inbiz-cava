Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsPartnerContacts
	Inherits clsDataClass
    Private _ContactID, _ContactPartnerID, _ContactSalutation, _ContactLastName, _ContactFirstName, _ContactTitleFr, _ContactTitleEn, _ContactPhone, _ContactPhoneExt, _ContactEmail, _ContactSecEmail, _ContactFax, _ContactDOB, _ContactSex, _ContactLangPref, _ContactHomePhone, _ContactCellPhone, _ContactPagerNo, _ContactCreatedOn, _ContactCreatedBy, _ContactLastUpdatedOn, _ContactLastUpdatedBy, _ContactActive, _ContactPrimary, _ContactSelSpecialization, _ContactNote, _IsSubscribeEmail As String
	Public Property ContactID() As String
		Get
			Return _ContactID
		End Get
		Set(ByVal value As String)
            _ContactID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactPartnerID() As String
		Get
			Return _ContactPartnerID
		End Get
		Set(ByVal value As String)
            _ContactPartnerID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactSalutation() As String
		Get
			Return _ContactSalutation
		End Get
		Set(ByVal value As String)
            _ContactSalutation = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactLastName() As String
		Get
			Return _ContactLastName
		End Get
		Set(ByVal value As String)
            _ContactLastName = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactFirstName() As String
		Get
			Return _ContactFirstName
		End Get
		Set(ByVal value As String)
            _ContactFirstName = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactTitleFr() As String
		Get
			Return _ContactTitleFr
		End Get
		Set(ByVal value As String)
            _ContactTitleFr = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactTitleEn() As String
		Get
			Return _ContactTitleEn
		End Get
		Set(ByVal value As String)
            _ContactTitleEn = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactPhone() As String
		Get
			Return _ContactPhone
		End Get
		Set(ByVal value As String)
            _ContactPhone = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactPhoneExt() As String
		Get
			Return _ContactPhoneExt
		End Get
		Set(ByVal value As String)
            _ContactPhoneExt = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactEmail() As String
		Get
			Return _ContactEmail
		End Get
		Set(ByVal value As String)
            _ContactEmail = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactSecEmail() As String
		Get
			Return _ContactSecEmail
		End Get
		Set(ByVal value As String)
            _ContactSecEmail = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactFax() As String
		Get
			Return _ContactFax
		End Get
		Set(ByVal value As String)
            _ContactFax = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactDOB() As String
		Get
			Return _ContactDOB
		End Get
		Set(ByVal value As String)
            _ContactDOB = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactSex() As String
		Get
			Return _ContactSex
		End Get
		Set(ByVal value As String)
            _ContactSex = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactLangPref() As String
		Get
			Return _ContactLangPref
		End Get
		Set(ByVal value As String)
            _ContactLangPref = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactHomePhone() As String
		Get
			Return _ContactHomePhone
		End Get
		Set(ByVal value As String)
            _ContactHomePhone = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactCellPhone() As String
		Get
			Return _ContactCellPhone
		End Get
		Set(ByVal value As String)
            _ContactCellPhone = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactPagerNo() As String
		Get
			Return _ContactPagerNo
		End Get
		Set(ByVal value As String)
            _ContactPagerNo = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactCreatedOn() As String
		Get
			Return _ContactCreatedOn
		End Get
		Set(ByVal value As String)
            _ContactCreatedOn = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactCreatedBy() As String
		Get
			Return _ContactCreatedBy
		End Get
		Set(ByVal value As String)
            _ContactCreatedBy = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactLastUpdatedOn() As String
		Get
			Return _ContactLastUpdatedOn
		End Get
		Set(ByVal value As String)
            _ContactLastUpdatedOn = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactLastUpdatedBy() As String
		Get
			Return _ContactLastUpdatedBy
		End Get
		Set(ByVal value As String)
            _ContactLastUpdatedBy = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactActive() As String
		Get
			Return _ContactActive
		End Get
		Set(ByVal value As String)
            _ContactActive = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactPrimary() As String
		Get
			Return _ContactPrimary
		End Get
		Set(ByVal value As String)
            _ContactPrimary = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ContactSelSpecialization() As String
		Get
			Return _ContactSelSpecialization
		End Get
		Set(ByVal value As String)
            _ContactSelSpecialization = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property ContactNote() As String
        Get
            Return _ContactNote
        End Get
        Set(ByVal value As String)
            _ContactNote = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property IsSubscribeEmail() As String
        Get
            Return _IsSubscribeEmail
        End Get
        Set(ByVal value As String)
            _IsSubscribeEmail = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    '''  Insert PartnerContacts
    ''' </summary>
    ''' <param name="sID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertPartnerContacts(ByRef sID As String) As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO partnercontacts( ContactPartnerID, ContactSalutation, ContactLastName, ContactFirstName, ContactTitleFr, ContactTitleEn, ContactPhone, ContactPhoneExt, ContactEmail, ContactSecEmail, ContactFax, ContactDOB, ContactSex, ContactLangPref, ContactHomePhone, ContactCellPhone, ContactPagerNo, ContactCreatedOn, ContactCreatedBy, ContactLastUpdatedOn, ContactLastUpdatedBy, ContactActive, ContactPrimary, ContactSelSpecialization, ContactNote,IsSubscribeEmail) VALUES('"
        strSQL += _ContactPartnerID + "','"
        strSQL += _ContactSalutation + "','"
        strSQL += _ContactLastName + "','"
        strSQL += _ContactFirstName + "','"
        strSQL += _ContactTitleFr + "','"
        strSQL += _ContactTitleEn + "','"
        strSQL += _ContactPhone + "','"
        strSQL += _ContactPhoneExt + "','"
        strSQL += _ContactEmail + "','"
        strSQL += _ContactSecEmail + "','"
        strSQL += _ContactFax + "','"
        strSQL += _ContactDOB + "','"
        strSQL += _ContactSex + "','"
        strSQL += _ContactLangPref + "','"
        strSQL += _ContactHomePhone + "','"
        strSQL += _ContactCellPhone + "','"
        strSQL += _ContactPagerNo + "','"
        strSQL += _ContactCreatedOn + "','"
        strSQL += _ContactCreatedBy + "','"
        strSQL += _ContactLastUpdatedOn + "','"
        strSQL += _ContactLastUpdatedBy + "','"
        strSQL += _ContactActive + "','"
        strSQL += _ContactPrimary + "','"
        strSQL += _ContactSelSpecialization + "','"
        strSQL += _ContactNote + "','"
        strSQL += _IsSubscribeEmail + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        sID = obj.GetScalarData("Select LAST_INSERT_ID()")
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    '''  Update PartnerContacts
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updatePartnerContacts() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE partnercontacts set "
		strSQL += "ContactPartnerID='" + _ContactPartnerID + "', "
		strSQL += "ContactSalutation='" + _ContactSalutation + "', "
		strSQL += "ContactLastName='" + _ContactLastName + "', "
		strSQL += "ContactFirstName='" + _ContactFirstName + "', "
		strSQL += "ContactTitleFr='" + _ContactTitleFr + "', "
		strSQL += "ContactTitleEn='" + _ContactTitleEn + "', "
		strSQL += "ContactPhone='" + _ContactPhone + "', "
		strSQL += "ContactPhoneExt='" + _ContactPhoneExt + "', "
		strSQL += "ContactEmail='" + _ContactEmail + "', "
		strSQL += "ContactSecEmail='" + _ContactSecEmail + "', "
		strSQL += "ContactFax='" + _ContactFax + "', "
		strSQL += "ContactDOB='" + _ContactDOB + "', "
		strSQL += "ContactSex='" + _ContactSex + "', "
		strSQL += "ContactLangPref='" + _ContactLangPref + "', "
		strSQL += "ContactHomePhone='" + _ContactHomePhone + "', "
		strSQL += "ContactCellPhone='" + _ContactCellPhone + "', "
		strSQL += "ContactPagerNo='" + _ContactPagerNo + "', "
        'strSQL += "ContactCreatedOn='" + _ContactCreatedOn + "', "
        'strSQL += "ContactCreatedBy='" + _ContactCreatedBy + "', "
		strSQL += "ContactLastUpdatedOn='" + _ContactLastUpdatedOn + "', "
		strSQL += "ContactLastUpdatedBy='" + _ContactLastUpdatedBy + "', "
		strSQL += "ContactActive='" + _ContactActive + "', "
		strSQL += "ContactPrimary='" + _ContactPrimary + "', "
        strSQL += "ContactSelSpecialization='" + _ContactSelSpecialization + "', "
        strSQL += "ContactNote='" + _ContactNote + "'"
        ' strSQL += "IsSubscribeEmail='" + _IsSubscribeEmail + "'"
		strSQL += " where ContactID='" + _ContactID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of PartnerContacts
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPartnerContactsInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
        strSQL = "SELECT  ContactPartnerID, ContactSalutation, ContactLastName, ContactFirstName, ContactTitleFr, ContactTitleEn, ContactPhone, ContactPhoneExt, ContactEmail, ContactSecEmail, ContactFax, ContactDOB, ContactSex, ContactLangPref, ContactHomePhone, ContactCellPhone, ContactPagerNo, ContactCreatedOn, ContactCreatedBy, ContactLastUpdatedOn, ContactLastUpdatedBy, ContactActive, ContactPrimary, ContactSelSpecialization, ContactNote,IsSubscribeEmail FROM partnercontacts where ContactID='" + _ContactID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_ContactPartnerID = drObj.Item("ContactPartnerID").ToString
			_ContactSalutation = drObj.Item("ContactSalutation").ToString
			_ContactLastName = drObj.Item("ContactLastName").ToString
			_ContactFirstName = drObj.Item("ContactFirstName").ToString
			_ContactTitleFr = drObj.Item("ContactTitleFr").ToString
			_ContactTitleEn = drObj.Item("ContactTitleEn").ToString
			_ContactPhone = drObj.Item("ContactPhone").ToString
			_ContactPhoneExt = drObj.Item("ContactPhoneExt").ToString
			_ContactEmail = drObj.Item("ContactEmail").ToString
			_ContactSecEmail = drObj.Item("ContactSecEmail").ToString
			_ContactFax = drObj.Item("ContactFax").ToString
			_ContactDOB = drObj.Item("ContactDOB").ToString
			_ContactSex = drObj.Item("ContactSex").ToString
			_ContactLangPref = drObj.Item("ContactLangPref").ToString
			_ContactHomePhone = drObj.Item("ContactHomePhone").ToString
			_ContactCellPhone = drObj.Item("ContactCellPhone").ToString
			_ContactPagerNo = drObj.Item("ContactPagerNo").ToString
            _ContactCreatedOn = drObj.Item("ContactCreatedOn").ToString
			_ContactCreatedBy = drObj.Item("ContactCreatedBy").ToString
			_ContactLastUpdatedOn = drObj.Item("ContactLastUpdatedOn").ToString
			_ContactLastUpdatedBy = drObj.Item("ContactLastUpdatedBy").ToString
			_ContactActive = drObj.Item("ContactActive").ToString
			_ContactPrimary = drObj.Item("ContactPrimary").ToString
            _ContactSelSpecialization = drObj.Item("ContactSelSpecialization").ToString
            _ContactNote = drObj.Item("ContactNote").ToString
            _IsSubscribeEmail = drObj.Item("IsSubscribeEmail").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub

    ''' <summary>
    ''' return ContactID ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetContactID() As String
        Dim strSQL As String
        strSQL = "SELECT ContactID FROM partnercontacts where ContactPartnerID='" & _ContactPartnerID & "' "
        Return GetScalarData(strSQL)
    End Function

    ''' <summary>
    ''' Delete Contact
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteContact(ByVal DeleteID As String) As String
        Dim strSQL As String = ""
        Dim objDC As New clsDataClass
        If HttpContext.Current.Session("UserRole") = "1" Then
            'Alerts
            strSQL = "DELETE alerts.* from alerts inner join contactcommunication on ComID=AlertComID inner join partnercontacts on ContactID=ComContactID where ContactID ='" & DeleteID & "' "
            objDC.PopulateData(strSQL)
            'Communication Follow Up
            strSQL = "DELETE contactcomfollowup.* from contactcomfollowup inner join contactcommunication on ComID=ComFollowUpComID inner join partnercontacts on ContactID=ComContactID where ContactID ='" & DeleteID & "' "
            objDC.PopulateData(strSQL)
            'Communication
            strSQL = "DELETE contactcommunication.* from contactcommunication inner join partnercontacts on ContactID=ComContactID where ContactID ='" & DeleteID & "' "
            objDC.PopulateData(strSQL)
            'Contact Address
            strSQL = "DELETE from addresses where addressRef='C' and addressSourceID ='" & DeleteID & "' "
            objDC.PopulateData(strSQL)
            'Contacts
            strSQL = "DELETE from partnercontacts where ContactID ='" & DeleteID & "' "
        Else
            strSQL = "UPDATE partnercontacts SET ContactActive=0 WHERE ContactID='" + DeleteID + "' "
        End If
        objDC = Nothing
        Return strSQL
    End Function
    ''' <summary>
    ''' Check Duplicate Contact Email ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateContactEmailID() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM partnercontacts where ContactEmail='" & _ContactEmail & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Return Contact Name
    ''' </summary>
    ''' <param name="strPartnerID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funContactPerSonName(ByVal strPartnerID As String) As String
        Dim strSQL, strContactName As String
        strSQL = "SELECT  Concat(ContactLastName,' ',ifnull(ContactFirstName,'')) as ContactName FROM partnercontacts "
        strSQL += " where ContactPartnerID='" & strPartnerID & "' and ContactActive='1' "
        strSQL += " order by ContactID Asc limit 1"
        strContactName = GetScalarData(strSQL)
        Return strContactName
    End Function

    ''' <summary>
    ''' Fill Grid of Contacts
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String = ""
        strSQL = "SELECT ContactID, ContactPartnerID, ContactSalutation, ContactLastName, ContactFirstName, Concat(ContactLastName,' ',ifnull(ContactFirstName,'')) as ContactName, ContactTitleFr, ContactTitleEn, ContactPhone, ContactPhoneExt, ContactEmail, ContactSecEmail, ContactFax, ContactDOB, ContactSex, ContactLangPref, ContactHomePhone, ContactCellPhone, ContactPagerNo, ContactCreatedOn, ContactCreatedBy, ContactLastUpdatedOn, ContactLastUpdatedBy, ContactActive, ContactPrimary, ContactSelSpecialization, ContactNote FROM partnercontacts "
        strSQL += " where ContactPartnerID='" & _ContactPartnerID & "' and ContactActive='1' "
        strSQL += " order by ContactID "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid of Contacts
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="sPartnerID"></param>
    ''' <param name="SearchCriteria"></param>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal StatusData As String, ByVal sPartnerID As String, ByVal SearchCriteria As String, ByVal SearchData As String) As String
        StatusData = clsCommon.funRemove(StatusData)
        SearchData = clsCommon.funRemove(SearchData)
        Dim strSQL As String = ""
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distinct ContactID, ContactPartnerID,PartnerAcronyme, ContactSalutation, ContactLastName, ContactFirstName, Concat(ContactLastName,' ',ifnull(ContactFirstName,'')) as ContactName, ContactTitleFr, ContactTitleEn, ContactPhone, ContactPhoneExt, ContactEmail, ContactSecEmail, ContactFax, ContactDOB, ContactSex, ContactLangPref, ContactHomePhone, ContactCellPhone, ContactPagerNo, ContactCreatedOn, ContactCreatedBy, ContactLastUpdatedOn, ContactLastUpdatedBy,ContactActive as ContactActiveCsv, CASE ContactActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as ContactActive, ContactPrimary, ContactSelSpecialization, ContactNote, PartnerLongName,IsSubscribeEmail FROM partnercontacts inner join partners p on p.PartnerID=ContactPartnerID  "
        Else
            strSQL = "SELECT distinct ContactID, ContactPartnerID,PartnerAcronyme, ContactSalutation, ContactLastName, ContactFirstName, Concat(ContactLastName,' ',ifnull(ContactFirstName,'')) as ContactName, ContactTitleFr, ContactTitleEn, ContactPhone, ContactPhoneExt, ContactEmail, ContactSecEmail, ContactFax, ContactDOB, ContactSex, ContactLangPref, ContactHomePhone, ContactCellPhone, ContactPagerNo, ContactCreatedOn, ContactCreatedBy, ContactLastUpdatedOn, ContactLastUpdatedBy,ContactActive as ContactActiveCsv, CASE ContactActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as ContactActive, ContactPrimary, ContactSelSpecialization, ContactNote, PartnerLongName,IsSubscribeEmail FROM partnercontacts inner join partners p on p.PartnerID=ContactPartnerID inner join salesrepcustomer s on s.CustomerID=ContactPartnerID  and s.UserID='" & HttpContext.Current.Session("UserID") & "'"
        End If

        If StatusData = "1" Then
            strSQL += " where ContactActive='" + StatusData + "' "
        ElseIf StatusData = "0" Then
            strSQL += " where ContactActive='" + StatusData + "' "
        ElseIf StatusData = "" Then
            strSQL += " where (1=1) "
        End If
        'For Partner ID
        If sPartnerID <> "" And sPartnerID <> "0" Then
            strSQL += " and ContactPartnerID='" + sPartnerID + "' "
        End If
        'For Search Criteria
        If SearchCriteria = "PN" Then
            strSQL += " and (PartnerLongName like '%" + SearchData + "%' or PartnerAcronyme  like '%" + SearchData + "%') "
        ElseIf SearchCriteria = "CN" Then
            strSQL += " and (Concat(ContactLastName,' ',ifnull(ContactFirstName,'')) like '%" + SearchData + "%' )"
        ElseIf SearchCriteria = "CE" Then
            strSQL += " and (ContactEmail like '%" + SearchData + "%') "
        ElseIf SearchCriteria = "CP" Then
            strSQL += " and (ContactPhone like '%" + SearchData + "%') "
        End If
        strSQL += " order by ContactPartnerID, ContactEmail "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid of Contacts for Advance search
    ''' </summary>
    ''' <param name="strPartnarName"></param>
    ''' <param name="strContTitle"></param>
    ''' <param name="strCity"></param>
    ''' <param name="strState"></param>
    ''' <param name="strCountry"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGridAdvanceSearch(ByVal strPartnarName As String, ByVal strContTitle As String, ByVal strCity As String, ByVal strState As String, ByVal strCountry As String) As String
        strPartnarName = clsCommon.funRemove(strPartnarName, False)
        strContTitle = clsCommon.funRemove(strContTitle, False)
        strCity = clsCommon.funRemove(strCity, False)
        strState = clsCommon.funRemove(strState, False)
        strCountry = clsCommon.funRemove(strCountry, False)
        Dim strSQL As String = ""
        strSQL = "SELECT distinct ContactID, ContactPartnerID,PartnerAcronyme, ContactSalutation, ContactLastName, ContactFirstName, Concat(ContactLastName,' ',ifnull(ContactFirstName,'')) as ContactName, ContactTitleFr, ContactTitleEn, ContactPhone, ContactPhoneExt, ContactEmail, ContactSecEmail, ContactFax, ContactDOB, ContactSex, ContactLangPref, ContactHomePhone, ContactCellPhone, ContactPagerNo, ContactCreatedOn, ContactCreatedBy, ContactLastUpdatedOn, ContactLastUpdatedBy,ContactActive as ContactActiveCsv, CASE ContactActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as ContactActive, ContactPrimary, ContactSelSpecialization, ContactNote, PartnerLongName,IsSubscribeEmail FROM partnercontacts inner join partners p on p.PartnerID=ContactPartnerID "
        If strPartnarName <> "" Or strContTitle <> "" Or strCity <> "" Or strState <> "" Or strCountry <> "" Then
            strSQL += " left join addresses a on (a.addressSourceID=partnercontacts.ContactID) and a.addressRef='C'"
        End If
        strSQL += " where (1=1) "
        'For Search Criteria
        If strPartnarName <> "" Then
            If strPartnarName.Contains("*") Then
                strPartnarName = strPartnarName.Replace("*", "%")
                strSQL += " and (PartnerLongName like '" + strPartnarName + "' or PartnerAcronyme like '" + strPartnarName + "') "
            Else
                strSQL += " and (PartnerLongName like '" + strPartnarName + "' or PartnerAcronyme like '" + strPartnarName + "') "
            End If
        End If
        If strContTitle <> "" Then
            If strContTitle.Contains("*") Then
                strContTitle = strContTitle.Replace("*", "%")
                strSQL += " and (ContactTitleFr like '" + strContTitle + "')"
            Else
                strSQL += " and (ContactTitleFr like '" + strContTitle + "' )"
            End If
        End If
        If strCity <> "" Then
            If strCity.Contains("*") Then
                strCity = strCity.Replace("*", "%")
                strSQL += " and (addressCity like '" + strCity + "')"
            Else
                strSQL += " and (addressCity like '" + strCity + "')"
            End If
        End If
        If strState <> "" Then
            If strState.Contains("*") Then
                strState = strState.Replace("*", "%")
                strSQL += " and (addressState like '" + strState + "')"
            Else
                strSQL += " and (addressState like '" + strState + "')"
            End If
        End If
        If strCountry <> "" Then
            If strCountry.Contains("*") Then
                strCountry = strCountry.Replace("*", "%")
                strSQL += " and (addressCountry like '" + strCountry + "')"
            Else
                strSQL += " and (addressCountry like '" + strCountry + "')"
            End If
        End If
        strSQL += " order by ContactPartnerID, ContactEmail "
        Return strSQL
    End Function
    ''' <summary>
    ''' Return Modified By
    ''' </summary>
    ''' <param name="sID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetModifiedBy(ByVal sID As String) As String
        Dim strData As String = ""
        Dim objP As New clsPartners
        objP.PartnerID = sID
        objP.getPartnersInfo()
        strData = objP.PartnerLongName
        objP = Nothing
        Return strData
    End Function
    ''' <summary>
    ''' Return Created By
    ''' </summary>
    ''' <param name="sID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetCreatedBy(ByVal sID As String) As String
        Dim strData As String = ""
        Dim objP As New clsPartners
        objP.PartnerID = sID
        objP.getPartnersInfo()
        strData = objP.PartnerLongName
        objP = Nothing
        Return strData
    End Function
    ''' <summary>
    ''' Populate Control
    ''' </summary>
    ''' <param name="chk"></param>
    ''' <param name="sPID"></param>
    ''' <remarks></remarks>
    Public Sub PopulateContacts(ByVal chk As CheckBoxList, ByVal sPID As String)
        Dim strSQL As String = ""
        strSQL = "SELECT ContactID, ContactPartnerID, Concat(ContactLastName,' ',ifnull(ContactFirstName,''),' (',ContactEmail,')') as ContactName, ContactLastName, ContactFirstName, ContactEmail FROM partnercontacts where ContactActive='1' "
        If sPID <> "" And sPID <> "0" Then
            strSQL += " and ContactPartnerID='" & sPID & "' "
        End If
        chk.Items.Clear()
        chk.DataSource = GetDataReader(strSQL)
        chk.DataTextField = "ContactName"
        chk.DataValueField = "ContactID"
        chk.DataBind()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate Control
    ''' </summary>
    ''' <param name="chk"></param>
    ''' <param name="sPartnerName"></param>
    ''' <param name="sSQL"></param>
    ''' <remarks></remarks>
    Public Sub PopulateContacts(ByVal chk As CheckBoxList, ByVal sPartnerName As String, ByVal sSQL As String)
        Dim strSQL As String = ""
        strSQL = sSQL
        chk.Items.Clear()
        chk.DataSource = GetDataReader(strSQL)
        chk.DataTextField = "ContactName"
        chk.DataValueField = "ContactID"
        chk.DataBind()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate Control
    ''' </summary>
    ''' <param name="chk"></param>
    ''' <param name="sCriteria"></param>
    ''' <remarks></remarks>
    Public Sub PopulateControl(ByVal chk As CheckBoxList, ByVal sCriteria As String)
        Dim strSQL As String = ""
        If sCriteria <> "" Then
            Select Case sCriteria
                Case "TitleFr"
                    strSQL = "SELECT Distinct ContactTitleFr as Criteria FROM partnercontacts where ContactActive='1' and ContactTitleFr <> '' "
                Case "TitleEn"
                    strSQL = "SELECT Distinct ContactTitleEn as Criteria FROM partnercontacts where ContactActive='1' and ContactTitleEn <> '' "
            End Select
            chk.Items.Clear()
            chk.DataSource = GetDataReader(strSQL)
            chk.DataTextField = "Criteria"
            chk.DataValueField = "Criteria"
            chk.DataBind()
            CloseDatabaseConnection()
        End If
    End Sub
    ''' <summary>
    ''' Check Duplicate Contact Name
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateContactName() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM partnercontacts where ContactPartnerID='" & _ContactPartnerID & "' and ContactFirstName='" & _ContactFirstName & "' and ContactLastName='" & _ContactLastName & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Populate objects of PartnerContacts
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getFirstContactWRTPartnerID()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT ContactID, ContactPartnerID, ContactSalutation, ContactLastName, ContactFirstName, ContactTitleFr, ContactTitleEn, ContactPhone, ContactPhoneExt, ContactEmail, ContactSecEmail, ContactFax, ContactDOB, ContactSex, ContactLangPref, ContactHomePhone, ContactCellPhone, ContactPagerNo, ContactCreatedOn, ContactCreatedBy, ContactLastUpdatedOn, ContactLastUpdatedBy, ContactActive, ContactPrimary, ContactSelSpecialization, ContactNote,IsSubscribeEmail FROM partnercontacts where ContactPrimary ='1' and ContactPartnerID='" + _ContactPartnerID + "' limit 1 "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _ContactID = drObj.Item("ContactID").ToString
            _ContactPartnerID = drObj.Item("ContactPartnerID").ToString
            _ContactSalutation = drObj.Item("ContactSalutation").ToString
            _ContactLastName = drObj.Item("ContactLastName").ToString
            _ContactFirstName = drObj.Item("ContactFirstName").ToString
            _ContactTitleFr = drObj.Item("ContactTitleFr").ToString
            _ContactTitleEn = drObj.Item("ContactTitleEn").ToString
            _ContactPhone = drObj.Item("ContactPhone").ToString
            _ContactPhoneExt = drObj.Item("ContactPhoneExt").ToString
            _ContactEmail = drObj.Item("ContactEmail").ToString
            _ContactSecEmail = drObj.Item("ContactSecEmail").ToString
            _ContactFax = drObj.Item("ContactFax").ToString
            _ContactDOB = drObj.Item("ContactDOB").ToString
            _ContactSex = drObj.Item("ContactSex").ToString
            _ContactLangPref = drObj.Item("ContactLangPref").ToString
            _ContactHomePhone = drObj.Item("ContactHomePhone").ToString
            _ContactCellPhone = drObj.Item("ContactCellPhone").ToString
            _ContactPagerNo = drObj.Item("ContactPagerNo").ToString
            _ContactCreatedOn = drObj.Item("ContactCreatedOn").ToString
            _ContactCreatedBy = drObj.Item("ContactCreatedBy").ToString
            _ContactLastUpdatedOn = drObj.Item("ContactLastUpdatedOn").ToString
            _ContactLastUpdatedBy = drObj.Item("ContactLastUpdatedBy").ToString
            _ContactActive = drObj.Item("ContactActive").ToString
            _ContactPrimary = drObj.Item("ContactPrimary").ToString
            _ContactSelSpecialization = drObj.Item("ContactSelSpecialization").ToString
            _ContactNote = drObj.Item("ContactNote").ToString
            _IsSubscribeEmail = drObj.Item("IsSubscribeEmail").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Update PartnerContacts
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updatePartnerContactWRTPartnerID() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE partnercontacts set "
        strSQL += "ContactFax='" + _ContactFax + "' "
        strSQL += " where ContactID='" + _ContactID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update IsSubscribeEmail
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updatePartnerIsSubscribeEmail() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE partnercontacts set "
        strSQL += "IsSubscribeEmail='0' "
        strSQL += " where ContactEmail='" + _ContactEmail + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update PartnerContacts
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateContactsforEndClient() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE partnercontacts set "
        strSQL += "ContactPartnerID='" + _ContactPartnerID + "', "
        ' strSQL += "ContactSalutation='" + _ContactSalutation + "', "
        strSQL += "ContactLastName='" + _ContactLastName + "', "
        strSQL += "ContactFirstName='" + _ContactFirstName + "', "
        'strSQL += "ContactTitleFr='" + _ContactTitleFr + "', "
        'strSQL += "ContactTitleEn='" + _ContactTitleEn + "', "
        strSQL += "ContactPhone='" + _ContactPhone + "', "
        'strSQL += "ContactPhoneExt='" + _ContactPhoneExt + "', "
        strSQL += "ContactEmail='" + _ContactEmail + "', "
        'strSQL += "ContactSecEmail='" + _ContactSecEmail + "', "
        strSQL += "ContactFax='" + _ContactFax + "', "
        ' strSQL += "ContactDOB='" + _ContactDOB + "', "
        'strSQL += "ContactSex='" + _ContactSex + "', "
        strSQL += "ContactLangPref='" + _ContactLangPref + "', "
        'strSQL += "ContactHomePhone='" + _ContactHomePhone + "', "
        'strSQL += "ContactCellPhone='" + _ContactCellPhone + "', "
        'strSQL += "ContactPagerNo='" + _ContactPagerNo + "', "
        'strSQL += "ContactCreatedOn='" + _ContactCreatedOn + "', "
        'strSQL += "ContactCreatedBy='" + _ContactCreatedBy + "', "
        strSQL += "ContactLastUpdatedOn='" + _ContactLastUpdatedOn + "', "
        strSQL += "ContactLastUpdatedBy='" + _ContactLastUpdatedBy + "', "
        strSQL += "ContactActive='" + _ContactActive + "', "
        ' strSQL += "ContactPrimary='" + _ContactPrimary + "', "
        'strSQL += "ContactSelSpecialization='" + _ContactSelSpecialization + "', "
        strSQL += "ContactNote='" + _ContactNote + "'"
        strSQL += " where ContactID='" + _ContactID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of PartnerContacts
    ''' </summary>
    ''' <param name="strPID"></param>
    ''' <remarks></remarks>
    Public Sub subPartnerContactsInfo(ByVal strPID As String)
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  Concat(ContactLastName,' ',ifnull(ContactFirstName,'')) as ContactName, ContactTitleFr, ContactTitleEn,ContactPhone FROM partnercontacts where ContactPartnerID='" + strPID + "'  order by ContactPartnerID limit 1 "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _ContactFirstName = drObj.Item("ContactName").ToString
            _ContactTitleFr = drObj.Item("ContactTitleFr").ToString
            _ContactTitleEn = drObj.Item("ContactTitleEn").ToString
            _ContactPhone = drObj.Item("ContactPhone").ToString
        End While
        CloseDatabaseConnection()
        drObj.Close()
    End Sub
End Class

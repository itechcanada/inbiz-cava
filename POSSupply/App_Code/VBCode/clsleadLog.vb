Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon

Public Class clsleadLog
    Inherits clsDataClass
    Private _LeadsLogID, _AssignLeadsID, _LeadsFollowup, _LeadsFollowupUserID, _LeadsLogText, _LeadsActivityDatetime As String
    Public Property LeadsLogID() As String
        Get
            Return _LeadsLogID
        End Get
        Set(ByVal value As String)
            _LeadsLogID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property AssignLeadsID() As String
        Get
            Return _AssignLeadsID
        End Get
        Set(ByVal value As String)
            _AssignLeadsID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property LeadsFollowup() As String
        Get
            Return _LeadsFollowup
        End Get
        Set(ByVal value As String)
            _LeadsFollowup = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property LeadsFollowupUserID() As String
        Get
            Return _LeadsFollowupUserID
        End Get
        Set(ByVal value As String)
            _LeadsFollowupUserID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property LeadsLogText() As String
        Get
            Return _LeadsLogText
        End Get
        Set(ByVal value As String)
            _LeadsLogText = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property LeadsActivityDatetime() As String
        Get
            Return _LeadsActivityDatetime
        End Get
        Set(ByVal value As String)
            _LeadsActivityDatetime = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    '''  Insert leadslog
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertLeadLog() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO leadslog( AssignLeadsID, LeadsFollowup, LeadsFollowupUserID, LeadsLogText, LeadsActivityDatetime) VALUES('"
        strSQL += _AssignLeadsID + "','"
        strSQL += _LeadsFollowup + "','"
        strSQL += _LeadsFollowupUserID + "','"
        strSQL += _LeadsLogText + "','"
        strSQL += _LeadsActivityDatetime + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update leadslog
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateLeadsLog() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE leadslog set "
        strSQL += "AssignLeadsID='" + _AssignLeadsID + "', "
        strSQL += "LeadsFollowup='" + _LeadsFollowup + "', "
        strSQL += "LeadsFollowupUserID='" + _LeadsFollowupUserID + "', "
        strSQL += "LeadsLogText='" + _LeadsLogText + "', "
        strSQL += "LeadsActivityDatetime='" + _LeadsActivityDatetime + "'"
        strSQL += " where LeadsLogID='" + _LeadsLogID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of leadslog
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetLeadsLogInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  AssignLeadsID, LeadsFollowup, LeadsFollowupUserID, LeadsLogText, LeadsActivityDatetime FROM leadslog where LeadsLogID='" + _LeadsLogID + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _AssignLeadsID = drObj.Item("AssignLeadsID").ToString
            _LeadsFollowup = drObj.Item("LeadsFollowup").ToString
            _LeadsFollowupUserID = drObj.Item("LeadsFollowupUserID").ToString
            _LeadsLogText = drObj.Item("LeadsLogText").ToString
            _LeadsActivityDatetime = drObj.Item("LeadsActivityDatetime").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill Grid 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        strSQL = "SELECT LeadsLogID,concat(userFirstName,' ',userLastName) as userName, AssignLeadsID, LeadsFollowup, LeadsFollowupUserID, LeadsLogText, LeadsActivityDatetime FROM leadslog as l "
        strSQL += " Inner join users u on u.userID=l.LeadsFollowupUserID where AssignLeadsID='" + _AssignLeadsID + "' "
        strSQL += " order by LeadsActivityDatetime desc "
        Return strSQL
    End Function

End Class

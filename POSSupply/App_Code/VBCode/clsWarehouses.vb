Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports iTECH.InbizERP.BusinessLogic
'Imports System.Web.HttpContext
'Imports clsCommon
'Imports Resources.Resource
Public Class clsWarehouses
	Inherits clsDataClass
    Private _WarehouseCode, _WarehouseDescription, _WarehouseAddressLine1, _WarehouseAddressLine2, _WarehouseCity, _WarehouseState, _WarehouseCountry, _WarehousePostalCode, _WarehousePrinterID, _WarehouseEmailID, _WarehouseFax, _WarehousePhone, _WarehouseActive, _WarehouseRegTaxCode, _WarehouseCompanyID As String
	Public Property WarehouseCode() As String
		Get
			Return _WarehouseCode
		End Get
		Set(ByVal value As String)
            _WarehouseCode = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property WarehouseDescription() As String
		Get
			Return _WarehouseDescription
		End Get
		Set(ByVal value As String)
            _WarehouseDescription = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property WarehouseAddressLine1() As String
		Get
			Return _WarehouseAddressLine1
		End Get
		Set(ByVal value As String)
            _WarehouseAddressLine1 = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property WarehouseAddressLine2() As String
		Get
			Return _WarehouseAddressLine2
		End Get
		Set(ByVal value As String)
            _WarehouseAddressLine2 = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property WarehouseCity() As String
		Get
			Return _WarehouseCity
		End Get
		Set(ByVal value As String)
            _WarehouseCity = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property WarehouseState() As String
		Get
			Return _WarehouseState
		End Get
		Set(ByVal value As String)
            _WarehouseState = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property WarehouseCountry() As String
		Get
			Return _WarehouseCountry
		End Get
		Set(ByVal value As String)
            _WarehouseCountry = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property WarehousePostalCode() As String
		Get
			Return _WarehousePostalCode
		End Get
		Set(ByVal value As String)
            _WarehousePostalCode = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property WarehousePrinterID() As String
		Get
			Return _WarehousePrinterID
		End Get
		Set(ByVal value As String)
            _WarehousePrinterID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property WarehouseEmailID() As String
		Get
			Return _WarehouseEmailID
		End Get
		Set(ByVal value As String)
            _WarehouseEmailID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property WarehouseFax() As String
		Get
			Return _WarehouseFax
		End Get
		Set(ByVal value As String)
            _WarehouseFax = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property WarehousePhone() As String
		Get
			Return _WarehousePhone
		End Get
		Set(ByVal value As String)
            _WarehousePhone = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property WarehouseActive() As String
		Get
			Return _WarehouseActive
		End Get
		Set(ByVal value As String)
            _WarehouseActive = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property WarehouseTaxCode() As String
        Get
            Return _WarehouseRegTaxCode
        End Get
        Set(ByVal value As String)
            _WarehouseRegTaxCode = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Property WarehouseCompanyID() As String
        Get
            Return _WarehouseCompanyID
        End Get
        Set(ByVal value As String)
            _WarehouseCompanyID = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert Warehouses
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertWarehouse() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO syswarehouses(WarehouseCode, WarehouseDescription, WarehouseAddressLine1, WarehouseAddressLine2, WarehouseCity, WarehouseState, WarehouseCountry, WarehousePostalCode, WarehousePrinterID, WarehouseEmailID, WarehouseFax, WarehousePhone, WarehouseActive, WarehouseRegTaxCode,WarehouseCompanyID) VALUES('"
        strSQL += _WarehouseCode + "','"
        strSQL += _WarehouseDescription + "','"
        strSQL += _WarehouseAddressLine1 + "','"
        strSQL += _WarehouseAddressLine2 + "','"
        strSQL += _WarehouseCity + "','"
        strSQL += _WarehouseState + "','"
        strSQL += _WarehouseCountry + "','"
        strSQL += _WarehousePostalCode + "','"
        strSQL += _WarehousePrinterID + "','"
        strSQL += _WarehouseEmailID + "','"
        strSQL += _WarehouseFax + "','"
        strSQL += _WarehousePhone + "','"
        strSQL += _WarehouseActive + "','"
        strSQL += _WarehouseRegTaxCode + "','"
        strSQL += _WarehouseCompanyID + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update Warehouses
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateWarehouse() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE syswarehouses set "
        strSQL += "WarehouseDescription='" + _WarehouseDescription + "', "
        strSQL += "WarehouseAddressLine1='" + _WarehouseAddressLine1 + "', "
        strSQL += "WarehouseAddressLine2='" + _WarehouseAddressLine2 + "', "
        strSQL += "WarehouseCity='" + _WarehouseCity + "', "
        strSQL += "WarehouseState='" + _WarehouseState + "', "
        strSQL += "WarehouseCountry='" + _WarehouseCountry + "', "
        strSQL += "WarehousePostalCode='" + _WarehousePostalCode + "', "
        strSQL += "WarehousePrinterID='" + _WarehousePrinterID + "', "
        strSQL += "WarehouseEmailID='" + _WarehouseEmailID + "', "
        strSQL += "WarehouseFax='" + _WarehouseFax + "', "
        strSQL += "WarehousePhone='" + _WarehousePhone + "', "
        strSQL += "WarehouseActive='" + _WarehouseActive + "', "
        strSQL += "WarehouseRegTaxCode='" + _WarehouseRegTaxCode + "', "
        strSQL += "WarehouseCompanyID='" + _WarehouseCompanyID + "'"
        strSQL += " where WarehouseCode='" + _WarehouseCode + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of Warehouses
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getWarehouseInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  WarehouseDescription, WarehouseAddressLine1, WarehouseAddressLine2, WarehouseCity, WarehouseState, WarehouseCountry, WarehousePostalCode, WarehousePrinterID, WarehouseEmailID, WarehouseFax, WarehousePhone, WarehouseActive, WarehouseRegTaxCode,WarehouseCompanyID FROM syswarehouses where WarehouseCode='" + _WarehouseCode + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _WarehouseDescription = drObj.Item("WarehouseDescription").ToString
            _WarehouseAddressLine1 = drObj.Item("WarehouseAddressLine1").ToString
            _WarehouseAddressLine2 = drObj.Item("WarehouseAddressLine2").ToString
            _WarehouseCity = drObj.Item("WarehouseCity").ToString
            _WarehouseState = drObj.Item("WarehouseState").ToString
            _WarehouseCountry = drObj.Item("WarehouseCountry").ToString
            _WarehousePostalCode = drObj.Item("WarehousePostalCode").ToString
            _WarehousePrinterID = drObj.Item("WarehousePrinterID").ToString
            _WarehouseEmailID = drObj.Item("WarehouseEmailID").ToString
            _WarehouseFax = drObj.Item("WarehouseFax").ToString
            _WarehousePhone = drObj.Item("WarehousePhone").ToString
            _WarehouseActive = drObj.Item("WarehouseActive").ToString
            _WarehouseRegTaxCode = drObj.Item("WarehouseRegTaxCode").ToString
            _WarehouseCompanyID = drObj.Item("WarehouseCompanyID").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Check Duplicate Warehouse Code
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateWarehouseCode() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM syswarehouses where WarehouseCode='" & _WarehouseCode & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Check Duplicate Warehouse Description
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateWarehouseDescription() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM syswarehouses where "
        strSQL += "WarehouseDescription='" & _WarehouseDescription & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Delete Warehouse
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteWarehouse(ByVal DeleteID As String) As String
        Dim strSQL As String
        strSQL = "DELETE FROM syswarehouses WHERE WarehouseCode='" + DeleteID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid of User
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        strSQL = "SELECT WarehouseCode, WarehouseDescription,WarehouseCompanyID,CompanyName  "
        strSQL += "  FROM syswarehouses left join syscompanyinfo on syscompanyinfo.CompanyID= syswarehouses.WarehouseCompanyID"
        strSQL += " order by WarehouseDescription "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid of Warehouse with search criteria
    ''' </summary>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal SearchData As String) As String
        Dim strSQL As String
        SearchData = clsCommon.funRemove(SearchData)
        strSQL = "SELECT WarehouseCode, WarehouseDescription,WarehouseCompanyID,CompanyName  "
        strSQL += "  FROM syswarehouses left join syscompanyinfo on syscompanyinfo.CompanyID= syswarehouses.WarehouseCompanyID"
        If SearchData <> "" Then
            strSQL += " where "
            strSQL += " ("
            strSQL += " WarehouseCode like '%" + SearchData + "%' or "
            strSQL += " WarehouseDescription like '%" + SearchData + "%' "
            strSQL += " )"
        End If
        strSQL += " order by WarehouseDescription "
        Return strSQL
    End Function
    ''' <summary>
    ''' Populate Warehouse
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <param name="strCopnayID"></param>
    ''' <remarks></remarks>//Done In Inbiz 4.0 In Bll Class
    Public Sub PopulateWarehouse(ByVal ddl As DropDownList, Optional ByVal strCopnayID As String = "")
        Dim strSQL As String
        strSQL = "SELECT WarehouseCode, WarehouseDescription, concat(upper(WarehouseCode),'::',substring(WarehouseDescription,1,15)) as WD FROM syswarehouses WHERE WarehouseActive='1'  "
        If ConfigurationManager.AppSettings("MultipleCompanies").ToLower = "false" Then
            strSQL += " and WarehouseCompanyID ='" & HttpContext.Current.Session("DefaultCompanyID") & "'"
        ElseIf strCopnayID <> "" Then
            strSQL += " and WarehouseCompanyID ='" & strCopnayID & "'"
        End If
        strSQL += " order by WarehouseDescription"
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "WD"
        ddl.DataValueField = "WarehouseCode"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = ""
        ddl.Items.Insert(0, itm)
        'If HttpContext.Current.Session("UserWarehouse") <> "" Then
        '    ddl.SelectedValue = HttpContext.Current.Session("UserWarehouse")
        'End If
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate Warehouse
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <param name="OnlyCode"></param>
    ''' <remarks></remarks>
    Public Sub PopulateWarehouse(ByVal ddl As DropDownList, ByVal OnlyCode As Boolean)
        Dim strSQL As String
        strSQL = "SELECT WarehouseCode, WarehouseDescription FROM syswarehouses WHERE WarehouseActive='1' order by WarehouseCode "
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "WarehouseCode"
        ddl.DataValueField = "WarehouseCode"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = ""
        ddl.Items.Insert(0, itm)
        If HttpContext.Current.Session("UserWarehouse") <> "" Then
            ddl.SelectedValue = HttpContext.Current.Session("UserWarehouse")
        End If
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Get Warehouse Code
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetWhsName() As String
        Dim strSQL, strWhs As String
        strSQL = "SELECT concat(WarehouseCode,': ',WarehouseDescription) FROM syswarehouses where WarehouseCompanyID='" & _WarehouseCompanyID & "' "
        strWhs = GetScalarData(strSQL)
        Return strWhs
    End Function
    ''' <summary>
    ''' Get Warehouse Code
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetWhsNameByWhscode() As String
        Dim strSQL, strWhs As String
        strSQL = "SELECT concat(WarehouseCode,': ',WarehouseDescription) FROM syswarehouses where WarehouseCode='" & _WarehouseCode & "' "
        strWhs = GetScalarData(strSQL)
        Return strWhs
    End Function
    ''' <summary>
    ''' Update Warehouses
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateWarehouseAddress() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE syswarehouses set "
        strSQL += "WarehouseDescription='" + _WarehouseDescription + "', "
        strSQL += "WarehouseAddressLine1='" + _WarehouseAddressLine1 + "', "
        strSQL += "WarehouseAddressLine2='" + _WarehouseAddressLine2 + "', "
        strSQL += "WarehouseCity='" + _WarehouseCity + "', "
        strSQL += "WarehouseState='" + _WarehouseState + "', "
        strSQL += "WarehouseCountry='" + _WarehouseCountry + "', "
        strSQL += "WarehousePostalCode='" + _WarehousePostalCode + "' "
        ' strSQL += "WarehousePrinterID='" + _WarehousePrinterID + "', "
        'strSQL += "WarehouseEmailID='" + _WarehouseEmailID + "', "
        'strSQL += "WarehouseFax='" + _WarehouseFax + "', "
        ' strSQL += "WarehousePhone='" + _WarehousePhone + "', "
        'strSQL += "WarehouseActive='" + _WarehouseActive + "', "
        'strSQL += "WarehouseRegTaxCode='" + _WarehouseRegTaxCode + "', "
        'strSQL += "WarehouseCompanyID='" + _WarehouseCompanyID + "'"
        strSQL += " where WarehouseCode='" + _WarehouseCode + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
End Class

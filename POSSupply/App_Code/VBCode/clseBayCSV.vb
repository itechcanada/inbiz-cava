Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Data.Odbc
Imports System.IO
Imports clsCommon
Imports iTECH.InbizERP.BusinessLogic


Public Class clseBayCSV
    Inherits clsDataClass
    Private strFilePhysicalPath As String = ""
    Public Function generateCSVString(ByVal itmArry As ArrayList, Optional ByVal IsAllProduct As Boolean = False, Optional ByVal filePhysicalPath As String = "") As String
        strFilePhysicalPath = filePhysicalPath
        Dim strProdId As String
        strProdId = ArrayListToDelimString(itmArry, ",")

        Dim sbCSV As New StringBuilder
        Dim strHeader As String = "Site,Format,Currency,Condition,Duration,Title,SubtitleText,Custom Label,Description,Category 1,Store Category,PicURL,Quantity,Starting Price,Reserve Price,BIN Price,SellerTags,Accept PayPal,PayPal Email Address,ShippingType,InternationalShippingType,ReturnsAccepted,Location - City/State,Location - Country,DispatchTimeMax,Specifying Shipping Costs,ShippingServiceOptions,InternationalShippingServiceOptions"
        sbCSV.AppendLine(strHeader)

        Dim newRow As New Dictionary(Of String, String)

        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT p.productId, prdUPCCode, p.prdName, prdWeight, prdWeightPkg, prdSalePricePerMinQty, prdEndUserSalesPrice,"
        strSQL += "prdHeight, prdLength, prdHeightPkg, prdLengthPkg, prdWidth, prdWidthPkg, "
        strSQL += "pd.prdSmallDesc, Concat('<p align=center><font size=5px><b><br/>Size: ', ifnull(prdWidth, ''), ' x ', ifnull(prdLength, ''), '<br/>Origin: ', ifnull(prdWeight, ''), '<br/>Material: ', ifnull(prdWeightPkg, ''), '<br/>Style: ', ifnull(p.prdName, ''), '</b></font></p>') as prdeBayDesc ,   pd.prdLargeDesc, sum(prdOhdQty) as prdOhdQty FROM products p inner join prddescriptions pd on p.productId = pd.id "
        strSQL += " Left join prdQuantity pq on pq.PrdID= p.ProductID "
        strSQL += " where(pd.descLang ='" + funGetProductLang() + "') "
        If Not (IsAllProduct) Then
            strSQL += " and p.productId in (" + strProdId + ")"
        End If
        strSQL += " GROUP BY productID "
        drObj = GetDataReader(strSQL)

        Dim index As Integer = 0
        While drObj.Read
            initNewRow(newRow)

            newRow("Site") = ConfigurationManager.AppSettings("Site").ToString
            newRow("Format") = ConfigurationManager.AppSettings("Format").ToString
            newRow("Currency") = ConfigurationManager.AppSettings("Currency").ToString
            newRow("Condition") = ConfigurationManager.AppSettings("Condition").ToString
            newRow("Duration") = ConfigurationManager.AppSettings("Duration").ToString
            newRow("Title") = getCSVFormat(drObj.Item("prdName").ToString())
            newRow("SubtitleText") = ""
            newRow("Custom Label") = getCSVFormat(drObj.Item("prdUPCCode").ToString())
            newRow("Description") = getCSVFormat(getDescriptionHTML(drObj.Item("ProductId").ToString(), drObj.Item("prdeBayDesc").ToString(), "SIZE: " & drObj.Item("prdWidth").ToString() & " x " & drObj.Item("prdLength").ToString() & "  " & drObj.Item("prdName").ToString() & " RUG"))
            newRow("Category 1") = ConfigurationManager.AppSettings("Category1").ToString
            newRow("StoreCategory") = ConfigurationManager.AppSettings("StoreCategory").ToString
            newRow("Quantity") = getCSVFormat(drObj.Item("prdOhdQty").ToString())
            newRow("Starting Price") = getCSVFormat(drObj.Item("prdEndUserSalesPrice").ToString())

            ' // Item Specifics string
            Dim strItemSpecifics As New StringBuilder
            strItemSpecifics.Append("<ItemSpecifics>")
            'strItemSpecifics.Append("<NameValueList><Name><![CDATA[Age]]></Name><Value><![CDATA[]]></Value><Source>ItemSpecific</Source></NameValueList>")
            strItemSpecifics.Append("<NameValueList><Name><![CDATA[Rug Type]]></Name><Value><![CDATA[]]></Value><Source>ItemSpecific</Source></NameValueList>")
            strItemSpecifics.Append("<NameValueList><Name><![CDATA[Shape]]></Name><Value><![CDATA[]]></Value><Source>ItemSpecific</Source></NameValueList>")
            strItemSpecifics.Append("<NameValueList><Name><![CDATA[Size]]></Name><Value><![CDATA[" + getCSVFormat(drObj.Item("prdWidth").ToString()) + " x " + getCSVFormat(drObj.Item("prdLength").ToString()) + "]]></Value><Source>ItemSpecific</Source></NameValueList>")
            strItemSpecifics.Append("<NameValueList><Name><![CDATA[Style]]></Name><Value><![CDATA[" + getCSVFormat(drObj.Item("prdName").ToString()) + "]]></Value><Source>ItemSpecific</Source></NameValueList>")
            strItemSpecifics.Append("<NameValueList><Name><![CDATA[Material]]></Name><Value><![CDATA[" + getCSVFormat(drObj.Item("prdWeightPkg").ToString()) + "]]></Value><Source>ItemSpecific</Source></NameValueList>")
            strItemSpecifics.Append("<NameValueList><Name><![CDATA[Condition]]></Name><Value><![CDATA[New]]></Value><Source>ItemSpecific</Source></NameValueList>")
            strItemSpecifics.Append("<NameValueList><Name><![CDATA[Origin]]></Name><Value><![CDATA[" + getCSVFormat(drObj.Item("prdWeight").ToString()) + "]]></Value><Source>ItemSpecific</Source></NameValueList>")
            strItemSpecifics.Append("<NameValueList><Name><![CDATA[Stock No.]]></Name><Value><![CDATA[" + getCSVFormat(drObj.Item("prdUPCCode").ToString()) + "]]></Value><Source>ItemSpecific</Source></NameValueList>")
            strItemSpecifics.Append("</ItemSpecifics>")

            newRow("SellerTags") = getCSVFormat(strItemSpecifics.ToString())
            newRow("AcceptPayPal") = ConfigurationManager.AppSettings("AcceptPayPal").ToString
            newRow("PayPalEmailAddress") = ConfigurationManager.AppSettings("PayPalEmailAddress").ToString
            newRow("ShippingType") = ConfigurationManager.AppSettings("ShippingType").ToString
            newRow("Specifying Shipping Costs") = ConfigurationManager.AppSettings("SpecifyingShippingCosts").ToString
            newRow("InternationalShippingType") = ConfigurationManager.AppSettings("InternationalShippingType").ToString
            newRow("ReturnsAccepted") = ConfigurationManager.AppSettings("ReturnsAccepted").ToString
            newRow("LocationCityState") = ConfigurationManager.AppSettings("LocationCityState").ToString
            newRow("LocationCountry") = ConfigurationManager.AppSettings("LocationCountry").ToString
            newRow("DispatchTimeMax") = ConfigurationManager.AppSettings("DispatchTimeMax").ToString
            newRow("PicURL") = getProductPictureString(drObj.Item("ProductId").ToString())

            Dim shippingCost As String = "0"
            If drObj.Item("prdHeightPkg").ToString() <> "" Then
                If (IsNumeric(drObj.Item("prdHeightPkg").ToString())) Then
                    shippingCost = drObj.Item("prdHeightPkg").ToString()
                End If
            End If
            newRow("ShippingServiceOptions") = ConfigurationManager.AppSettings("ShippingServiceOptions").ToString ' "PABT%0d%0aAGgAaQBwAHAAaQBuAGcAUwBlAHIAdgBpAGMAZQBPAHAAdABpAG8AbgBzAD4APABTAGgAaQBwAHAA%0d%0aaQBuAGcAUwBlAHIAdgBpAGMAZQBPAHAAdABpAG8AbgA+ADwAUwBoAGkAcABwAGkAbgBnAFMAZQBy%0d%0aAHYAaQBjAGUAPgAyADAAMQA8AC8AUwBoAGkAcABwAGkAbgBnAFMAZQByAHYAaQBjAGUAPgA8AFMA%0d%0aaABpAHAAcABpAG4AZwBTAGUAcgB2AGkAYwBlAFAAcgBpAG8AcgBpAHQAeQA+ADEAPAAvAFMAaABp%0d%0aAHAAcABpAG4AZwBTAGUAcgB2AGkAYwBlAFAAcgBpAG8AcgBpAHQAeQA+ADwARgByAGUAZQBTAGgA%0d%0aaQBwAHAAaQBuAGcAPgAwADwALwBGAHIAZQBlAFMAaABpAHAAcABpAG4AZwA+ADwAUwBoAGkAcABw%0d%0aAGkAbgBnAFMAZQByAHYAaQBjAGUAQwBvAHMAdAA+ADAALgAwADAAPAAvAFMAaABpAHAAcABpAG4A%0d%0aZwBTAGUAcgB2AGkAYwBlAEMAbwBzAHQAPgA8AFMAaABpAHAAcABpAG4AZwBTAGUAcgB2AGkAYwBl%0d%0aAEEAZABkAGkAdABpAG8AbgBhAGwAQwBvAHMAdAA+ADwALwBTAGgAaQBwAHAAaQBuAGcAUwBlAHIA%0d%0adgBpAGMAZQBBAGQAZABpAHQAaQBvAG4AYQBsAEMAbwBzAHQAPgA8AFMAaABpAHAAcABpAG4AZwBT%0d%0aAGUAcgB2AGkAYwBlAEEAZABkAFMAdQByAGMAaABhAHIAZwBlAD4APAAvAFMAaABpAHAAcABpAG4A%0d%0aZwBTAGUAcgB2AGkAYwBlAEEAZABkAFMAdQByAGMAaABhAHIAZwBlAD4APABTAGgAaQBwAHAAaQBu%0d%0aAGcAUwBlAHIAdgBpAGMAZQBTAHUAcgBjAGgAYQByAGcAZQBWAGEAbAA+ADwALwBTAGgAaQBwAHAA%0d%0aaQBuAGcAUwBlAHIAdgBpAGMAZQBTAHUAcgBjAGgAYQByAGcAZQBWAGEAbAA+ADwALwBTAGgAaQBw%0d%0aAHAAaQBuAGcAUwBlAHIAdgBpAGMAZQBPAHAAdABpAG8AbgA+ADwAUwBoAGkAcABwAGkAbgBnAFMA%0d%0aZQByAHYAaQBjAGUATwBwAHQAaQBvAG4APgA8AFMAaABpAHAAcABpAG4AZwBTAGUAcgB2AGkAYwBl%0d%0aAD4AMgA1ADAAPAAvAFMAaABpAHAAcABpAG4AZwBTAGUAcgB2AGkAYwBlAD4APABTAGgAaQBwAHAA%0d%0aaQBuAGcAUwBlAHIAdgBpAGMAZQBQAHIAaQBvAHIAaQB0AHkAPgAyADwALwBTAGgAaQBwAHAAaQBu%0d%0aAGcAUwBlAHIAdgBpAGMAZQBQAHIAaQBvAHIAaQB0AHkAPgA8AEYAcgBlAGUAUwBoAGkAcABwAGkA%0d%0abgBnAD4AMAA8AC8ARgByAGUAZQBTAGgAaQBwAHAAaQBuAGcAPgA8AFMAaABpAHAAcABpAG4AZwBT%0d%0aAGUAcgB2AGkAYwBlAEMAbwBzAHQAPgAwAC4AMAAwADwALwBTAGgAaQBwAHAAaQBuAGcAUwBlAHIA%0d%0adgBpAGMAZQBDAG8AcwB0AD4APABTAGgAaQBwAHAAaQBuAGcAUwBlAHIAdgBpAGMAZQBBAGQAZABp%0d%0aAHQAaQBvAG4AYQBsAEMAbwBzAHQAPgA8AC8AUwBoAGkAcABwAGkAbgBnAFMAZQByAHYAaQBjAGUA%0d%0aQQBkAGQAaQB0AGkAbwBuAGEAbABDAG8AcwB0AD4APABTAGgAaQBwAHAAaQBuAGcAUwBlAHIAdgBp%0d%0aAGMAZQBBAGQAZABTAHUAcgBjAGgAYQByAGcAZQA+ADwALwBTAGgAaQBwAHAAaQBuAGcAUwBlAHIA%0d%0adgBpAGMAZQBBAGQAZABTAHUAcgBjAGgAYQByAGcAZQA+ADwAUwBoAGkAcABwAGkAbgBnAFMAZQBy%0d%0aAHYAaQBjAGUAUwB1AHIAYwBoAGEAcgBnAGUAVgBhAGwAPgA8AC8AUwBoAGkAcABwAGkAbgBnAFMA%0d%0aZQByAHYAaQBjAGUAUwB1AHIAYwBoAGEAcgBnAGUAVgBhAGwAPgA8AC8AUwBoAGkAcABwAGkAbgBn%0d%0aAFMAZQByAHYAaQBjAGUATwBwAHQAaQBvAG4APgA8AC8AUwBoAGkAcABwAGkAbgBnAFMAZQByAHYA%0d%0aaQBjAGUATwBwAHQAaQBvAG4AcwA+ACgA"
            newRow("InternationalShippingServiceOptions") = ConfigurationManager.AppSettings("InternationalShippingServiceOptions").ToString '"PABJ%0d%0aAG4AdABlAHIAbgBhAHQAaQBvAG4AYQBsAFMAaABpAHAAcABpAG4AZwBTAGUAcgB2AGkAYwBlAE8A%0d%0acAB0AGkAbwBuAHMAPgA8AFMAaABpAHAAcABpAG4AZwBTAGUAcgB2AGkAYwBlAE8AcAB0AGkAbwBu%0d%0aAD4APABTAGgAaQBwAHAAaQBuAGcAUwBlAHIAdgBpAGMAZQA+ADUAMAAyADAAMQA8AC8AUwBoAGkA%0d%0acABwAGkAbgBnAFMAZQByAHYAaQBjAGUAPgA8AFMAaABpAHAAcABpAG4AZwBTAGUAcgB2AGkAYwBl%0d%0aAFAAcgBpAG8AcgBpAHQAeQA+ADEAPAAvAFMAaABpAHAAcABpAG4AZwBTAGUAcgB2AGkAYwBlAFAA%0d%0acgBpAG8AcgBpAHQAeQA+ADwAUwBoAGkAcABwAGkAbgBnAFMAZQByAHYAaQBjAGUAQwBvAHMAdAA+%0d%0aADAALgAwADAAPAAvAFMAaABpAHAAcABpAG4AZwBTAGUAcgB2AGkAYwBlAEMAbwBzAHQAPgA8AFMA%0d%0aaABpAHAAcABpAG4AZwBTAGUAcgB2AGkAYwBlAEEAZABkAGkAdABpAG8AbgBhAGwAQwBvAHMAdAA+%0d%0aADwALwBTAGgAaQBwAHAAaQBuAGcAUwBlAHIAdgBpAGMAZQBBAGQAZABpAHQAaQBvAG4AYQBsAEMA%0d%0abwBzAHQAPgA8AFMAaABpAHAAVABvAEwAbwBjAGEAdABpAG8AbgBzAD4APABTAGgAaQBwAFQAbwBM%0d%0aAG8AYwBhAHQAaQBvAG4APgBVAFMAPAAvAFMAaABpAHAAVABvAEwAbwBjAGEAdABpAG8AbgA+ADwA%0d%0aLwBTAGgAaQBwAFQAbwBMAG8AYwBhAHQAaQBvAG4AcwA+ADwALwBTAGgAaQBwAHAAaQBuAGcAUwBl%0d%0aAHIAdgBpAGMAZQBPAHAAdABpAG8AbgA+ADwALwBJAG4AdABlAHIAbgBhAHQAaQBvAG4AYQBsAFMA%0d%0aaABpAHAAcABpAG4AZwBTAGUAcgB2AGkAYwBlAE8AcAB0AGkAbwBuAHMAPgAfAA=="

            Dim strNewRow As String = newRow("Site") + "," + newRow("Format") + "," + newRow("Currency") + "," + newRow("Condition") + "," + newRow("Duration") + "," + newRow("Title") + "," + newRow("SubtitleText") + "," + newRow("Custom Label") + "," + newRow("Description") + "," + newRow("Category 1") + "," + newRow("StoreCategory") + "," + newRow("PicURL") + "," + newRow("Quantity") + "," + newRow("Starting Price") + "," + newRow("Reserve Price") + "," + newRow("BIN Price") + "," + newRow("SellerTags") + "," + newRow("AcceptPayPal") + "," + newRow("PayPalEmailAddress") + "," + newRow("ShippingType") + "," + newRow("InternationalShippingType") + "," + newRow("ReturnsAccepted") + "," + newRow("LocationCityState") + "," + newRow("LocationCountry") + "," + newRow("DispatchTimeMax") + "," + newRow("Specifying Shipping Costs") + "," + newRow("ShippingServiceOptions") + "," + newRow("InternationalShippingServiceOptions")
            sbCSV.AppendLine(strNewRow)

            index += 1
        End While

        drObj.Close()
        CloseDatabaseConnection()

        Return sbCSV.ToString()

        '0,9,2,DDWEDR,SubtitleText,Description,20584,,1,100,100,100,<ItemSpecifics><NameValueList><Name><![CDATA[Age]]></Name><Value><![CDATA[2000-Now]]></Value><Source>ItemSpecific</Source></NameValueList><NameValueList><Name><![CDATA[Rug Type]]></Name><Value><![CDATA[Area Rugs]]></Value><Source>ItemSpecific</Source></NameValueList><NameValueList><Name><![CDATA[Shape]]></Name><Value><![CDATA[Ractangle]]></Value><Source>ItemSpecific</Source></NameValueList><NameValueList><Name><![CDATA[Size]]></Name><Value><![CDATA[8.1 x 11.3]]></Value><Source>ItemSpecific</Source></NameValueList><NameValueList><Name><![CDATA[Style]]></Name><Value><![CDATA[Indo Tibetan]]></Value><Source>ItemSpecific</Source></NameValueList><NameValueList><Name><![CDATA[Material]]></Name><Value><![CDATA[Wool]]></Value><Source>ItemSpecific</Source></NameValueList><NameValueList><Name><![CDATA[Condition]]></Name><Value><![CDATA[New]]></Value><Source>ItemSpecific</Source></NameValueList><NameValueList><Name><![CDATA[Origin]]></Name><Value><![CDATA[India]]></Value><Source>ItemSpecific</Source></NameValueList><NameValueList><Name><![CDATA[Stock No.]]></Name><Value><![CDATA[11484]]></Value><Source>ItemSpecific</Source></NameValueList></ItemSpecifics>

    End Function

    Public Sub initNewRow(ByRef newRow As Dictionary(Of String, String))
        newRow("Site") = ""
        newRow("Format") = ""
        newRow("Currency") = ""
        newRow("Title") = ""
        newRow("SubtitleText") = ""
        newRow("Custom Label") = ""
        newRow("Description") = ""
        newRow("Category 1") = ""
        newRow("StoreCategory") = ""
        newRow("PicURL") = ""
        newRow("Quantity") = ""
        newRow("Starting Price") = ""
        newRow("Reserve Price") = ""
        newRow("BIN Price") = ""
        newRow("SellerTags") = ""
        newRow("Condition") = ""
        newRow("Duration") = ""
        newRow("AcceptPayPal") = ""
        newRow("PayPalEmailAddress") = ""
        newRow("ShippingType") = ""
        newRow("InternationalShippingType") = ""
        newRow("LocationCityState") = ""
        newRow("LocationCountry") = ""
        newRow("DispatchTimeMax") = ""
        newRow("Specifying Shipping Costs") = ""
        newRow("ShippingServiceOptions") = ""
        newRow("InternationalShippingServiceOptions") = ""

    End Sub

    Public Function getCSVFormat(ByVal strValue As String) As String
        strValue = strValue.Replace("""", """""")
        If strValue.Contains(",") Or strValue.Contains("""") Then
            strValue = """" + strValue + """"
        End If
        Return strValue
    End Function

    Private Function getProductPictureString(ByVal strProdId As String) As String
        Dim strFilePath As String = getURLPath()

        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "Select * From prdimages Where prdID =" + strProdId
        drObj = GetDataReader(strSQL)
        Dim sbPicUrl As New StringBuilder
        Dim index As Integer = 0
        While drObj.Read
            If index <> 0 And sbPicUrl.ToString() <> "" Then
                sbPicUrl.Append(";")
            End If

            If drObj.Item("prdSmallImagePath").ToString() <> "" Then
                sbPicUrl.Append(strFilePath + drObj.Item("prdSmallImagePath").ToString())
            ElseIf drObj.Item("prdLargeImagePath").ToString() <> "" Then
                sbPicUrl.Append(strFilePath + drObj.Item("prdLargeImagePath").ToString())
            Else
                sbPicUrl.Append("")
            End If
            index += 1
        End While
        drObj.Close()

        Return sbPicUrl.ToString
    End Function

    Private Function getURLPath() As String
        Dim strFilePath As String = ConfigurationManager.AppSettings("RelativePath")
        strFilePath = strFilePath.Replace("https://", "http://")
        Return strFilePath
    End Function

    Private Function getDescriptionHTML(ByVal strProdId As String, ByVal strPrdDesc As String, ByVal strTitle As String) As String
        Dim strFilePath As String = getURLPath()
        Dim streBayStoreHeader As String = getFileText("eBayStoreHeader.html")
        Dim streBayStoreFooter As String = getFileText("eBayStoreFooter.html")

        Dim sbDescription As New StringBuilder
        sbDescription.Append(streBayStoreHeader)
        sbDescription.Append(strPrdDesc)
        sbDescription.Append("<br/>")
        Dim strPicSrc As String() = getProductPictureString(strProdId).Split(";")
        Dim picSrc As String = ""
        For Each picSrc In strPicSrc
            If picSrc <> "" Then
                sbDescription.Append("<p><img src='" + picSrc + "' alt='' /></p>")
            End If
        Next
        sbDescription.Append("<br/>")
        sbDescription.Append(streBayStoreFooter)

        Return sbDescription.ToString().Replace("@@title@@", strTitle)
    End Function

    'Reads content from file.
    Protected Function getFileText(ByVal fileName As String) As String
        Dim objStreamReader As StreamReader
        Dim sbStr As New StringBuilder
        If File.Exists(strFilePhysicalPath & "/" & fileName) = True Then
            objStreamReader = File.OpenText(strFilePhysicalPath & "/" & fileName)

            While (Not objStreamReader.EndOfStream)
                sbStr.Append(objStreamReader.ReadLine())
            End While
            objStreamReader.Close()
        End If
        Return sbStr.ToString()
    End Function

    Private Function getShippingServiceString(ByVal cost As String) As String
        Dim sb As New StringBuilder
        sb.Append("<ShippingServiceOptions>")
        sb.Append("<ShippingServiceOption>")
        sb.Append("<ShippingService>201</ShippingService>")
        sb.Append("<ShippingServicePriority>1</ShippingServicePriority>")
        sb.Append("<FreeShipping>0</FreeShipping>")
        sb.Append("<ShippingServiceCost>" + cost + "</ShippingServiceCost>")
        sb.Append("<ShippingServiceAdditionalCost></ShippingServiceAdditionalCost>")
        sb.Append("<ShippingServiceAddSurcharge></ShippingServiceAddSurcharge>")
        sb.Append("<ShippingServiceSurchargeVal></ShippingServiceSurchargeVal>")
        sb.Append("</ShippingServiceOption>")
        sb.Append("<ShippingServiceOption>")
        sb.Append("<ShippingService>250</ShippingService>")
        sb.Append("<ShippingServicePriority>2</ShippingServicePriority>")
        sb.Append("<FreeShipping>0</FreeShipping>")
        sb.Append("<ShippingServiceCost>0</ShippingServiceCost>")
        sb.Append("<ShippingServiceAdditionalCost></ShippingServiceAdditionalCost>")
        sb.Append("<ShippingServiceAddSurcharge></ShippingServiceAddSurcharge>")
        sb.Append("<ShippingServiceSurchargeVal></ShippingServiceSurchargeVal>")
        sb.Append("</ShippingServiceOption>")
        sb.Append("</ShippingServiceOptions>")


        'Needs to convert into base64
        Dim shippingServiceStr As String = ""
        'Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(sb.ToString)
        'shippingServiceStr = Convert.ToBase64String(byt)

        ' replace tokens & convert to base64
        Dim encoding As New System.Text.ASCIIEncoding()
        shippingServiceStr = Convert.ToBase64String(encoding.GetBytes(xorString(convertString(sb.ToString))))

        Dim resultSB As New StringBuilder()
        resultSB.Append(shippingServiceStr.Substring(0, 4) + "\r\n")

        Dim a1 As String = shippingServiceStr.Substring(4)
        Dim a2 As String = ""
        Dim index As Integer = 0
        While (index < a1.Length)
            If (index + 76 < a1.Length) Then
                a2 += a1.Substring(index, 76) + "\r\n"
            Else
                a2 += a1.Substring(index)
            End If
            index += 76
        End While
        resultSB.Append(a2.Substring(0, a2.Length - 2))
        resultSB.Replace("\r", "%0d").Replace("\n", "%0a")

        Return resultSB.ToString
    End Function

    Private Function convertString(ByVal str As String) As String
        Dim sb As New StringBuilder
        ' convert to 16bit encoding the 'quick hack' way.
        Dim index As Integer = 0
        For index = 0 To str.Length - 1
            sb.Append(str(index) + Convert.ToChar(0))
        Next
        Return sb.ToString()
    End Function
    Private Function xorString(ByVal str As String) As String
        ' calculate 8 bit checksum & add to string as 16 bit number (MSB=0)
        ' this works because in our case all MSBs in string are 0 and xor to 0.
        Dim sb As New StringBuilder
        Dim x As Integer = 0
        Dim index As Integer = 0
        For index = 0 To str.Length - 1
            x = x Xor Convert.ToInt32(str(index))
        Next
        Return str + Convert.ToString(x) + Convert.ToChar(0).ToString()
    End Function
End Class


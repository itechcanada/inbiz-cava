Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsBatchPrint
    Inherits clsDataClass

    Private _docID, _docType, _docNo, _docCtr, _docPrintDateTime, _docPrintedBy, _docCompanyID As String

#Region "Properties"
    Public Property docID() As String
        Get
            Return _docID
        End Get
        Set(ByVal value As String)
            _docID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property docType() As String
        Get
            Return _docType
        End Get
        Set(ByVal value As String)
            _docType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property docNo() As String
        Get
            Return _docNo
        End Get
        Set(ByVal value As String)
            _docNo = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property docCtr() As String
        Get
            Return _docCtr
        End Get
        Set(ByVal value As String)
            _docCtr = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property docPrintDateTime() As String
        Get
            Return _docPrintDateTime
        End Get
        Set(ByVal value As String)
            _docPrintDateTime = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property docPrintedBy() As String
        Get
            Return _docPrintedBy
        End Get
        Set(ByVal value As String)
            _docPrintedBy = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property docCompanyID() As String
        Get
            Return _docCompanyID
        End Get
        Set(ByVal value As String)
            _docCompanyID = clsCommon.funRemove(value, True)
        End Set
    End Property

#End Region

    Public Sub New()
        MyBase.New()
    End Sub

#Region "Function"
    ''' <summary>
    ''' Insert itdocstatus
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertItdocstatus() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO itdocstatus(docType, docNo, docCtr, docPrintDateTime, docPrintedBy,docCompanyID) VALUES('"
        strSQL += _docType + "','"
        strSQL += _docNo + "','"
        strSQL += _docCtr + "','"
        strSQL += _docPrintDateTime + "','"
        strSQL += _docPrintedBy + "','"
        strSQL += _docCompanyID + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update itdocstatus
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateItdocstatus() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE itdocstatus set "
        strSQL += "docType='" + docType + "', "
        strSQL += "docNo='" + _docNo + "', "
        strSQL += "docCtr='" + _docCtr + "',"
        strSQL += "docPrintDateTime='" + _docPrintDateTime + "',"
        strSQL += "docPrintedBy='" + _docPrintedBy + "'"
        strSQL += " where docID='" + _docID + "'"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of itdocstatus
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetItdocstatus()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT docType, docNo, docCtr, docPrintDateTime, docPrintedBy,docCompanyID FROM itdocstatus where docID='" + _docID + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _docType = drObj.Item("docType").ToString
            _docCtr = drObj.Item("docCtr").ToString
            _docNo = drObj.Item("docNo").ToString
            _docPrintDateTime = drObj.Item("docPrintDateTime").ToString
            _docPrintedBy = drObj.Item("docPrintedBy").ToString
            _docCompanyID = drObj.Item("docCompanyID").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Duplicate Docno
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDuplicateItdocstatus() As Boolean
        Dim strSql As String
        strSql = "SELECT count(*) FROM itdocstatus "
        strSql += " where docNo='" + _docNo + "' and docPrintedBy='" + _docPrintedBy + "' and docCompanyID='" + _docCompanyID + "'"
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.GetScalarData(strSql)
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' update Doc Control
    ''' </summary>
    ''' <param name="strDocNo"></param>
    ''' <param name="strPrintedBy"></param>
    ''' <param name="strComID"></param>
    ''' <remarks></remarks>
    Public Sub updateDocCtl(ByVal strDocNo As String, ByVal strPrintedBy As String, ByVal strComID As String)
        Dim strSql, strDocCtr As String
        Dim obj As New clsDataClass
        strSql = "SELECT docCtr FROM itdocstatus "
        strSql += " where docNo='" + strDocNo + "' and docPrintedBy='" + strPrintedBy + "' and docCompanyID='" + strComID + "'"
        strDocCtr = obj.GetScalarData(strSql) + 1
        strSql = "UPDATE itdocstatus set "
        strSql += "docCtr='" + strDocCtr + "' "
        strSql += " where docNo='" + strDocNo + "' and docPrintedBy='" + strPrintedBy + "' and docCompanyID='" + strComID + "'"
        obj.PopulateData(strSql)
        obj.CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Duplicate Already Paid
    ''' </summary>
    ''' <param name="StrInvID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInvNotPaid(ByVal StrInvID As String) As Boolean
        Dim strSql As String
        strSql = "SELECT count(*) FROM invoices "
        strSql += " where invStatus<>'" & iTECH.InbizERP.BusinessLogic.InvoicesStatus.PAYMENT_RECEIVED.ToString() & "' and invID='" & StrInvID & " '"
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.GetScalarData(strSql)
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region

End Class

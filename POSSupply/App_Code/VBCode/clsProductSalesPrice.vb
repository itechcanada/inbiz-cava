Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsProductSalesPrice
	Inherits clsDataClass
    Private _productSalesPriceId, _productID, _fromQty, _toQty, _salesPrice, _fromRushQty, _toRushQty, _rushSalesPrice As String
    Public Property ProductSalesPriceID() As String
        Get
            Return _productSalesPriceId
        End Get
        Set(ByVal value As String)
            _productSalesPriceId = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ProductID() As String
        Get
            Return _productID
        End Get
        Set(ByVal value As String)
            _productID = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Property FromQty() As String
		Get
			Return _fromQty
		End Get
		Set(ByVal value As String)
            _fromQty = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ToQty() As String
		Get
			Return _toQty
		End Get
		Set(ByVal value As String)
            _toQty = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property SalesPrice() As String
		Get
			Return _salesPrice
		End Get
		Set(ByVal value As String)
            _salesPrice = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property FromRushQty() As String
		Get
			Return _fromRushQty
		End Get
		Set(ByVal value As String)
            _fromRushQty = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ToRushQty() As String
		Get
			Return _toRushQty
		End Get
		Set(ByVal value As String)
            _toRushQty = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property RushSalesPrice() As String
		Get
			Return _rushSalesPrice
		End Get
		Set(ByVal value As String)
            _rushSalesPrice = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    '''  Insert ProductSalesPrice
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertProductSalesPrice() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO productsalesprice( prdSalesPriceID, fromQty, toQty, salesPrice, fromRushQty, toRushQty, rushSalesPrice) VALUES("
        strSQL += "'" + _productID + "','"
        strSQL += _fromQty + "','"
        strSQL += _toQty + "','"
        strSQL += _salesPrice + "','"
        strSQL += _fromRushQty + "','"
        strSQL += _toRushQty + "','"
        strSQL += _rushSalesPrice + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim objDataClass As New clsDataClass
        Dim intData As Int16 = 0
        intData = objDataClass.PopulateData(strSQL)
        objDataClass.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    '''  Update ProductSalesPrice
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateProductSalesPrice() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE productsalesprice set "
        strSQL += "fromQty='" + _fromQty + "', "
        strSQL += "toQty='" + _toQty + "', "
        strSQL += "salesPrice='" + _salesPrice + "', "
        strSQL += "fromRushQty='" + _fromRushQty + "', "
        strSQL += "toRushQty='" + _toRushQty + "', "
        strSQL += "rushSalesPrice='" + _rushSalesPrice + "'"
        strSQL += " where id='" + _productSalesPriceId + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim objDataClass As New clsDataClass
        Dim intData As Int16 = 0
        intData = objDataClass.PopulateData(strSQL)
        objDataClass.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of ProductSalesPrice
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetProductSalesPriceInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  id, prdSalesPriceID, fromQty, toQty, salesPrice, fromRushQty, toRushQty, rushSalesPrice FROM productsalesprice where prdSalesPriceID='" + _productID + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _productSalesPriceId = drObj.Item("id").ToString
            _productID = drObj.Item("prdSalesPriceID").ToString
            _fromQty = drObj.Item("fromQty").ToString
            _toQty = drObj.Item("toQty").ToString
            _salesPrice = drObj.Item("salesPrice").ToString
            _fromRushQty = drObj.Item("fromRushQty").ToString
            _toRushQty = drObj.Item("toRushQty").ToString
            _rushSalesPrice = drObj.Item("rushSalesPrice").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Find product sales Price
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function isGivenRangeAlreadyExist() As Boolean
        Dim strSQL As String
        Dim objDataClass As New clsDataClass
        strSQL = "SELECT count(*) FROM productsalesprice where prdSalesPriceID ='" & _productID & "'"
        strSQL += " and ('" & _fromQty & "' between fromQty and toQty or '" & _toQty & "' between fromQty and toQty "
        strSQL += " or  fromQty between '" & _fromQty & "' and '" & _toQty & "' )"

        Dim intCount As Integer = objDataClass.GetScalarData(strSQL)

        objDataClass = Nothing
        If intCount = 0 Then
            Return False
        End If
        Return True
    End Function
    ''' <summary>
    ''' get Grid View Filler SQL
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getGridViewFillerSQL() As String
        Dim strSQL As String = ""
        strSQL = "SELECT  id , prdSalesPriceID, fromQty, toQty, salesPrice, fromRushQty, toRushQty, rushSalesPrice FROM productsalesprice where prdSalesPriceID='" + _productID + "' order by prdSalesPriceID, fromQty"
        Return strSQL
    End Function
    ''' <summary>
    ''' delete Product Sale Price Row SQL
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function deleteProductSalePriceRowSQL() As String
        Dim strSql As String = "delete from productsalesprice where id='" & _productSalesPriceId & "' And prdSalesPriceID ='" & _productID & "'"
        Return strSql
    End Function
    ''' <summary>
    ''' get Sale Price
    ''' </summary>
    ''' <param name="Qty"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getSalePrice(ByVal Qty As String) As String
        Dim strSQL As String = ""
        Dim price As String = "0"
        Dim drObj As OdbcDataReader
        strSQL = "SELECT distinct salesPrice  FROM productsalesprice where prdSalesPriceID='" + _productID + "' "
        strSQL += " and " + Qty + " between fromQty and toQty "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            price = drObj.Item("salesPrice").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
        Return price
    End Function
    ''' <summary>
    ''' get Sale Price Id By Qty
    ''' </summary>
    ''' <param name="Qty"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getSalePriceIdByQty(ByVal Qty As String) As String
        Dim strSQL As String = ""
        Dim id As String = ""
        Dim drObj As OdbcDataReader
        strSQL = "SELECT distinct id  FROM productsalesprice where prdSalesPriceID='" + _productID + "' "
        strSQL += " and " + Qty + " between fromQty and toQty "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            id = drObj.Item("id").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
        Return id
    End Function
    ''' <summary>
    ''' sub Get Product Sales Price Info By Id
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetProductSalesPriceInfoById()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  id, prdSalesPriceID, fromQty, toQty, salesPrice, fromRushQty, toRushQty, rushSalesPrice FROM productsalesprice where id='" + _productSalesPriceId + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _productSalesPriceId = drObj.Item("id").ToString
            _productID = drObj.Item("prdSalesPriceID").ToString
            _fromQty = drObj.Item("fromQty").ToString
            _toQty = drObj.Item("toQty").ToString
            _salesPrice = drObj.Item("salesPrice").ToString
            _fromRushQty = drObj.Item("fromRushQty").ToString
            _toRushQty = drObj.Item("toRushQty").ToString
            _rushSalesPrice = drObj.Item("rushSalesPrice").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports iTECH.InbizERP.BusinessLogic
Public Class clsAccountReceivable
    Inherits clsDataClass
    Private _AccountReceivableID, _ARInvoiceNo, _ARAmtRcvd, _ARAmtRcvdDateTime, _ARAmtRcvdVia, _ARRcvdBy, _ARContactNm, _ARContactPhone, _ARContactEmail, _ARCreditCardNo, _ARCreditCardExp, _ARCreditCardCVVCode, _ARCreditCardLastName, _ARCreditCardFirstName, _ARChequeNo, _ARNote, _ARColAsigned, _ARWriteOff As String
    Public Property AccountReceivableID() As String
        Get
            Return _AccountReceivableID
        End Get
        Set(ByVal value As String)
            _AccountReceivableID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARInvoiceNo() As String
        Get
            Return _ARInvoiceNo
        End Get
        Set(ByVal value As String)
            _ARInvoiceNo = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARAmtRcvd() As String
        Get
            Return _ARAmtRcvd
        End Get
        Set(ByVal value As String)
            _ARAmtRcvd = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARAmtRcvdDateTime() As String
        Get
            Return _ARAmtRcvdDateTime
        End Get
        Set(ByVal value As String)
            _ARAmtRcvdDateTime = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARAmtRcvdVia() As String
        Get
            Return _ARAmtRcvdVia
        End Get
        Set(ByVal value As String)
            _ARAmtRcvdVia = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARRcvdBy() As String
        Get
            Return _ARRcvdBy
        End Get
        Set(ByVal value As String)
            _ARRcvdBy = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARContactNm() As String
        Get
            Return _ARContactNm
        End Get
        Set(ByVal value As String)
            _ARContactNm = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARContactPhone() As String
        Get
            Return _ARContactPhone
        End Get
        Set(ByVal value As String)
            _ARContactPhone = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARContactEmail() As String
        Get
            Return _ARContactEmail
        End Get
        Set(ByVal value As String)
            _ARContactEmail = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARCreditCardNo() As String
        Get
            Return _ARCreditCardNo
        End Get
        Set(ByVal value As String)
            _ARCreditCardNo = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARCreditCardExp() As String
        Get
            Return _ARCreditCardExp
        End Get
        Set(ByVal value As String)
            _ARCreditCardExp = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARCreditCardCVVCode() As String
        Get
            Return _ARCreditCardCVVCode
        End Get
        Set(ByVal value As String)
            _ARCreditCardCVVCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARCreditCardLastName() As String
        Get
            Return _ARCreditCardLastName
        End Get
        Set(ByVal value As String)
            _ARCreditCardLastName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARCreditCardFirstName() As String
        Get
            Return _ARCreditCardFirstName
        End Get
        Set(ByVal value As String)
            _ARCreditCardFirstName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARChequeNo() As String
        Get
            Return _ARChequeNo
        End Get
        Set(ByVal value As String)
            _ARChequeNo = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARNote() As String
        Get
            Return _ARNote
        End Get
        Set(ByVal value As String)
            _ARNote = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARColAsigned() As String
        Get
            Return _ARColAsigned
        End Get
        Set(ByVal value As String)
            _ARColAsigned = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ARWriteOff() As String
        Get
            Return _ARWriteOff
        End Get
        Set(ByVal value As String)
            _ARWriteOff = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Account Receivable Insert Functionality
    ''' Insert the Data in accountreceivable tbale into Database 
    ''' </summary>
    ''' <returns>Its Boolean value</returns>
    ''' <remarks></remarks>
    Public Function insertAccountReceivable() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO accountreceivable( ARInvoiceNo, ARAmtRcvd, ARAmtRcvdDateTime, ARAmtRcvdVia, ARRcvdBy, ARContactNm, ARContactPhone, ARContactEmail, ARCreditCardNo, ARCreditCardExp, ARCreditCardCVVCode, ARCreditCardLastName, ARCreditCardFirstName, ARChequeNo, ARNote, ARColAsigned, ARWriteOff) VALUES('"
        strSQL += _ARInvoiceNo + "','"
        strSQL += _ARAmtRcvd + "','"
        strSQL += _ARAmtRcvdDateTime + "','"
        strSQL += _ARAmtRcvdVia + "','"
        strSQL += _ARRcvdBy + "','"
        strSQL += _ARContactNm + "','"
        strSQL += _ARContactPhone + "','"
        strSQL += _ARContactEmail + "','"
        strSQL += _ARCreditCardNo + "','"
        strSQL += _ARCreditCardExp + "','"
        strSQL += _ARCreditCardCVVCode + "','"
        strSQL += _ARCreditCardLastName + "','"
        strSQL += _ARCreditCardFirstName + "','"
        strSQL += _ARChequeNo + "','"
        strSQL += _ARNote + "','"
        strSQL += _ARColAsigned + "','"
        strSQL += _ARWriteOff + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Account Receivable Update 
    ''' Insert the Data in accountreceivable tbale into Database 
    ''' </summary>
    ''' <returns>Its Boolean value</returns>
    ''' <remarks></remarks>
    Public Function updateAccountReceivable() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE accountreceivable set "
        strSQL += "ARInvoiceNo='" + _ARInvoiceNo + "', "
        strSQL += "ARAmtRcvd='" + _ARAmtRcvd + "', "
        strSQL += "ARAmtRcvdDateTime='" + _ARAmtRcvdDateTime + "', "
        strSQL += "ARAmtRcvdVia='" + _ARAmtRcvdVia + "', "
        strSQL += "ARRcvdBy='" + _ARRcvdBy + "', "
        strSQL += "ARContactNm='" + _ARContactNm + "', "
        strSQL += "ARContactPhone='" + _ARContactPhone + "', "
        strSQL += "ARContactEmail='" + _ARContactEmail + "', "
        strSQL += "ARCreditCardNo='" + _ARCreditCardNo + "', "
        strSQL += "ARCreditCardExp='" + _ARCreditCardExp + "', "
        strSQL += "ARCreditCardCVVCode='" + _ARCreditCardCVVCode + "', "
        strSQL += "ARCreditCardLastName='" + _ARCreditCardLastName + "', "
        strSQL += "ARCreditCardFirstName='" + _ARCreditCardFirstName + "', "
        strSQL += "ARChequeNo='" + _ARChequeNo + "', "
        strSQL += "ARNote='" + _ARNote + "', "
        strSQL += "ARColAsigned='" + _ARColAsigned + "', "
        strSQL += "ARWriteOff='" + _ARWriteOff + "'"
        strSQL += " where AccountReceivableID='" + _AccountReceivableID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Get Account Receive Information by Account Receivable ID
    '''Select Data for accountreceivable table
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getAccountReceivableInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  ARInvoiceNo, ARAmtRcvd, ARAmtRcvdDateTime, ARAmtRcvdVia, ARRcvdBy, ARContactNm, ARContactPhone, ARContactEmail, ARCreditCardNo, ARCreditCardExp, ARCreditCardCVVCode, ARCreditCardLastName, ARCreditCardFirstName, ARChequeNo, ARNote, ARColAsigned, ARWriteOff FROM accountreceivable where AccountReceivableID='" + _AccountReceivableID + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _ARInvoiceNo = drObj.Item("ARInvoiceNo").ToString
            _ARAmtRcvd = drObj.Item("ARAmtRcvd").ToString
            _ARAmtRcvdDateTime = drObj.Item("ARAmtRcvdDateTime").ToString
            _ARAmtRcvdVia = drObj.Item("ARAmtRcvdVia").ToString
            _ARRcvdBy = drObj.Item("ARRcvdBy").ToString
            _ARContactNm = drObj.Item("ARContactNm").ToString
            _ARContactPhone = drObj.Item("ARContactPhone").ToString
            _ARContactEmail = drObj.Item("ARContactEmail").ToString
            _ARCreditCardNo = drObj.Item("ARCreditCardNo").ToString
            _ARCreditCardExp = drObj.Item("ARCreditCardExp").ToString
            _ARCreditCardCVVCode = drObj.Item("ARCreditCardCVVCode").ToString
            _ARCreditCardLastName = drObj.Item("ARCreditCardLastName").ToString
            _ARCreditCardFirstName = drObj.Item("ARCreditCardFirstName").ToString
            _ARChequeNo = drObj.Item("ARChequeNo").ToString
            _ARNote = drObj.Item("ARNote").ToString
            _ARColAsigned = drObj.Item("ARColAsigned").ToString
            _ARWriteOff = drObj.Item("ARWriteOff").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub

    ''' <summary>
    ''' Make the Query For Grid 
    ''' Payment Not Received or Partial Received 
    ''' </summary>
    ''' <returns>Query for Selecting data for Grid </returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String

        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distinct o.invID,o.InvRefNo,invShpWhsCode, PartnerLongName as CustomerName, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as InvAmount, if(sum(ARAmtRcvd) Is Null,0, sum(ARAmtRcvd)) as BalanceAmount, invStatus, invCustPO, invForOrderNo, ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, o.invRefType, a.* FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID left join partners on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID left join users u on u.userID=c.ColUserAssignedTo "
        Else
            strSQL = "SELECT distinct o.invID,o.InvRefNo,invShpWhsCode, PartnerLongName as CustomerName, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as InvAmount, if(sum(ARAmtRcvd) Is Null,0, sum(ARAmtRcvd)) as BalanceAmount, invStatus, invCustPO, invForOrderNo, ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, o.invRefType, a.* FROM invoices o Inner join salesrepcustomer s on s.CustomerID=o.invCustID and s.CustomerType=o.invCustType and s.UserID='" & HttpContext.Current.Session("UserID") & "' inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID left join users u on u.userID=c.ColUserAssignedTo "
        End If

        strSQL += " where o.invStatus<>'" & InvoicesStatus.PAYMENT_RECEIVED & "' "

        If _ARInvoiceNo <> "" Then
            strSQL += " and a.ARInvoiceNo='" & _ARInvoiceNo & "' "
        End If
        strSQL += " group by o.invID, a.ARInvoiceNo order by o.invCreatedOn, o.invID desc,  CustomerName "
        Return strSQL
    End Function
    ''' <summary>
    ''' Make the Query For Grid 
    '''For Payment Not Received or Partial Received based on Option 
    ''' </summary>
    ''' <param name="strOption"></param>
    ''' <returns>Query for Selecting data for Grid</returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal strOption As String) As String
        Dim strSQL As String

        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distinct o.invID,o.InvRefNo, PartnerLongName as CustomerName, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as InvAmount, if(sum(ARAmtRcvd) Is Null,0, sum(ARAmtRcvd)) as BalanceAmount, invStatus, invCustPO, invForOrderNo, ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, o.invRefType FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID left join users u on u.userID=c.ColUserAssignedTo "
        Else
            strSQL = "SELECT distinct o.invID,o.InvRefNo, PartnerLongName as CustomerName, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as InvAmount, if(sum(ARAmtRcvd) Is Null,0, sum(ARAmtRcvd)) as BalanceAmount, invStatus, invCustPO, invForOrderNo, ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, o.invRefType FROM invoices o Inner join salesrepcustomer s on s.CustomerID=o.invCustID and s.CustomerType=o.invCustType and s.UserID='" & HttpContext.Current.Session("UserID") & "' inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID left join users u on u.userID=c.ColUserAssignedTo "
        End If
        strSQL += " where o.invStatus<>'" & InvoicesStatus.PAYMENT_RECEIVED.ToString() & "' "
        If strOption = StatusCollectionSearchOption.NULL_VALUE Then
            strSQL += " and c.ColUserAssignedTo is null "
        ElseIf strOption = StatusCollectionSearchOption.NOT_NULL_VALUE Then
            strSQL += " and c.ColUserAssignedTo is not null "
        ElseIf strOption = StatusCollectionSearchOption.ALL_VALUE Then

        End If

        strSQL += " group by o.invID, a.ARInvoiceNo order by o.invCreatedOn, o.invID desc,  CustomerName "
        Return strSQL
    End Function
    ''' <summary>
    ''' Make the Query For Grid 
    '''For Payment Not Received or Partial Received based on Serach Option 
    ''' </summary>
    ''' <param name="strOption">Option like null , notnull ,all</param>
    ''' <param name="SearchDays">as String Search Day </param>
    ''' <param name="strCustName">as String Cumtomer Name</param>
    ''' <param name="strCustPhone">as String Cutomer Phone Number</param>
    ''' <param name="strInvoiceNo">as String InvoiceNo</param>
    ''' <param name="strAssignedTo">as String AssignedTo</param>
    ''' <returns>Query for Selecting data for Grid</returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal strOption As String, ByVal SearchDays As String, ByVal strCustName As String, ByVal strCustPhone As String, ByVal strInvoiceNo As String, ByVal strAssignedTo As String) As String
        Dim strSQL As String
        strCustName = clsCommon.funRemove(strCustName)
        strCustPhone = clsCommon.funRemove(strCustPhone)
        strInvoiceNo = clsCommon.funRemove(strInvoiceNo)
        strAssignedTo = clsCommon.funRemove(strAssignedTo)
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distinct o.invID,o.InvRefNo,invShpWhsCode,PartnerPhone, PartnerLongName as CustomerName,PartnerAcronyme, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as InvAmount, if(sum(ARAmtRcvd) Is Null,0, sum(ARAmtRcvd)) as BalanceAmount, invStatus, invCustPO, invForOrderNo, ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, o.invRefType FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID  "
        Else
            strSQL = "SELECT distinct o.invID,o.InvRefNo,invShpWhsCode,PartnerPhone, PartnerLongName as CustomerName,PartnerAcronyme, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as InvAmount, if(sum(ARAmtRcvd) Is Null,0, sum(ARAmtRcvd)) as BalanceAmount, invStatus, invCustPO, invForOrderNo, ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, o.invRefType FROM invoices o Inner join salesrepcustomer s on s.CustomerID=o.invCustID and s.CustomerType=o.invCustType and s.UserID='" & HttpContext.Current.Session("UserID") & "' inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID  "
        End If
        If strAssignedTo <> "" Then
            strSQL += " Inner join users u on u.userID=c.ColUserAssignedTo"
        Else
            strSQL += " left join users u on u.userID=c.ColUserAssignedTo"
        End If

        strSQL += " where o.invStatus<>'" & InvoicesStatus.PAYMENT_RECEIVED.ToString() & "' "
        If strOption = StatusCollectionSearchOption.NULL_VALUE Then
            strSQL += " and c.ColUserAssignedTo is null "
        ElseIf strOption = StatusCollectionSearchOption.NOT_NULL_VALUE Then
            strSQL += " and c.ColUserAssignedTo is not null "
        ElseIf strOption = StatusCollectionSearchOption.ALL_VALUE Then

        End If
        If strCustName <> "" Then
            strSQL += " and (PartnerLongName like '%" & strCustName & "%' or PartnerAcronyme like '%" + strCustName + "%' )"
        End If
        If strCustPhone <> "" Then
            strSQL += " and PartnerPhone like '%" & strCustPhone & "%'"
        End If
        If strInvoiceNo <> "" Then
            strSQL += " and InvRefNo like '%" & strInvoiceNo & "%'"
        End If

        If strAssignedTo <> "" Then
            strSQL += " and concat(u.userFirstName,' ',u.userLastName) like '%" & strAssignedTo & "%' "
        End If

        If SearchDays <> "" Then
            strSQL += funCreatedDateRage(SearchDays)
        End If
        strSQL += " group by o.invID, a.ARInvoiceNo order by o.invCreatedOn, o.invID desc,  CustomerName "
        Return strSQL
    End Function
    ''' <summary>
    ''' Make the Query For Grid 
    '''For Payment Not Received or Partial Received based on Serach Option 
    ''' </summary>
    ''' <param name="SearchDays">as String Search Day </param>
    ''' <param name="SearchType">As String Search Type</param>
    ''' <param name="SearchData">As String Date</param>
    ''' <param name="strValue">as String if Pass 1 then Apply for Invoice Status p other vice non</param>
    ''' <returns>Query for Selecting data for Grid</returns>
    Public Function funFillGrid(ByVal SearchType As String, ByVal SearchData As String, ByVal SearchDays As String, ByVal strValue As String) As String
        Dim strSQL As String
        SearchData = clsCommon.funRemove(SearchData)
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distinct o.invID,o.InvRefNo,invShpWhsCode,PartnerPhone,PartnerAcronyme, PartnerLongName  as CustomerName, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as InvAmount, if(sum(ARAmtRcvd) Is Null,0, sum(ARAmtRcvd)) as BalanceAmount, invStatus, invCustPO, invForOrderNo, ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, o.invRefType,o.invCustPO FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID left join users u on u.userID=c.ColUserAssignedTo "
        Else
            strSQL = "SELECT distinct o.invID,o.InvRefNo,invShpWhsCode,PartnerPhone,PartnerAcronyme, PartnerLongName  as CustomerName, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as InvAmount, if(sum(ARAmtRcvd) Is Null,0, sum(ARAmtRcvd)) as BalanceAmount, invStatus, invCustPO, invForOrderNo, ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, o.invRefType,o.invCustPO FROM invoices o inner join salesrepcustomer s on s.CustomerID=o.invCustID and s.CustomerType=o.invCustType and s.UserID='" & HttpContext.Current.Session("UserID") & "' inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID left join users u on u.userID=c.ColUserAssignedTo "
        End If
        If strValue = "1" Then
            strSQL += " where o.invStatus<>'P' "
        Else
            strSQL += " where 1=1 "
        End If
        If SearchType = OrderSearchFields.InvoiceNo And SearchData <> "" And SearchDays <> "" Then
            strSQL += " and o.InvRefNo Like '%" + SearchData + "%' "
            strSQL += funCreatedDateRage(SearchDays)
        ElseIf SearchType = OrderSearchFields.OrderNo And SearchData <> "" And SearchDays <> "" Then
            strSQL += " and o.invForOrderNo = '" + SearchData + "' "
            strSQL += funCreatedDateRage(SearchDays)
        ElseIf SearchType = CustomerSearchFields.ContactName And SearchData <> "" And SearchDays <> "" Then
            strSQL += " and (PartnerLongName like '%" + SearchData + "%' or PartnerAcronyme like '%" + SearchData + "%' ) "
            strSQL += funCreatedDateRage(SearchDays)
        ElseIf SearchType = CustomerSearchFields.CustomerPhoneNumber And SearchData <> "" And SearchDays <> "" Then
            strSQL += " and (PartnerPhone like '%" + SearchData + "%') "
            strSQL += funCreatedDateRage(SearchDays)
        ElseIf SearchType = OrderSearchFields.CustomerPO And SearchData <> "" And SearchDays <> "" Then
            strSQL += " and (o.invCustPO like '%" + SearchData + "%') "
            strSQL += funCreatedDateRage(SearchDays)
        ElseIf SearchType = CustomerSearchFields.Acronyme And SearchData <> "" And SearchDays <> "" Then
            strSQL += " and (PartnerAcronyme like '%" + SearchData + "%') "
            strSQL += funCreatedDateRage(SearchDays)

        ElseIf SearchType = OrderSearchFields.InvoiceNo And SearchData <> "" And SearchDays = "" Then
            strSQL += " and o.InvRefNo Like '%" + SearchData + "%' "
        ElseIf SearchType = OrderSearchFields.OrderNo And SearchData <> "" And SearchDays = "" Then
            strSQL += " and o.invForOrderNo = '" + SearchData + "' "
        ElseIf SearchType = CustomerSearchFields.ContactName And SearchData <> "" And SearchDays = "" Then
            strSQL += " and (PartnerLongName like '%" + SearchData + "%' or PartnerAcronyme like '%" + SearchData + "%') "
        ElseIf SearchType = CustomerSearchFields.CustomerPhoneNumber And SearchData <> "" And SearchDays = "" Then
            strSQL += " and (PartnerPhone like '%" + SearchData + "%') "
        ElseIf SearchType = OrderSearchFields.CustomerPO And SearchData <> "" And SearchDays = "" Then
            strSQL += " and (o.invCustPO like '%" + SearchData + "%') "
        ElseIf SearchType = CustomerSearchFields.Acronyme And SearchData <> "" And SearchDays = "" Then
            strSQL += " and (PartnerAcronyme like '%" + SearchData + "%') "

        ElseIf SearchType = OrderSearchFields.InvoiceNo And SearchData = "" And SearchDays <> "" Then
            strSQL += funCreatedDateRage(SearchDays)
        ElseIf SearchType = OrderSearchFields.OrderNo And SearchData = "" And SearchDays <> "" Then
            strSQL += funCreatedDateRage(SearchDays)
        ElseIf SearchType = CustomerSearchFields.ContactName And SearchData = "" And SearchDays <> "" Then
            strSQL += funCreatedDateRage(SearchDays)
        ElseIf SearchType = CustomerSearchFields.CustomerPhoneNumber And SearchData = "" And SearchDays <> "" Then
            strSQL += funCreatedDateRage(SearchDays)
        ElseIf SearchType = OrderSearchFields.CustomerPO And SearchData = "" And SearchDays <> "" Then
            strSQL += funCreatedDateRage(SearchDays)
        ElseIf SearchType = CustomerSearchFields.Acronyme And SearchData = "" And SearchDays <> "" Then
            strSQL += funCreatedDateRage(SearchDays)

        ElseIf SearchType = "" And SearchData = "" And SearchDays <> "" Then
            strSQL += funCreatedDateRage(SearchDays)
        End If
        strSQL += " group by o.invID, a.ARInvoiceNo order by o.invCreatedOn,o.invID desc,  CustomerName "
        Return strSQL
    End Function

    ''' <summary>
    ''' Get Amount For Invoice No
    ''' Using accountreceivable from Database 
    ''' </summary>
    ''' <param name="nInvNo">as String Invoice Number</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getAmountForInvoiceNo(ByVal nInvNo As String) As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT if(sum(ARAmtRcvd) Is Null,0, sum(ARAmtRcvd)) as BalanceAmount FROM accountreceivable where ARInvoiceNo='" & nInvNo & "' group by ARInvoiceNo "
        Try
            strData = GetScalarData(strSQL).ToString
        Catch ex As NullReferenceException
            strData = "0"
        Catch ex As Exception
            strData = "0"
        End Try
        Return strData
    End Function
    ''' <summary>
    ''' Get WriteOff For Invoice No
    ''' Using accountreceivable from Database 
    ''' </summary>
    ''' <param name="nInvNo">as String Invoice Number</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getWriteOffForInvoiceNo(ByVal nInvNo As String) As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT if(sum(ARWriteOff) Is Null,0, sum(ARWriteOff)) as WriteOffAmount FROM accountreceivable where ARInvoiceNo='" & nInvNo & "' group by ARInvoiceNo "
        Try
            strData = GetScalarData(strSQL).ToString
        Catch ex As NullReferenceException
            strData = "0"
        Catch ex As Exception
            strData = "0"
        End Try
        Return strData
    End Function
    ''' <summary>
    ''' Update Invoice
    ''' From Table invoices according Status
    ''' </summary>
    ''' <param name="sStatus">As String Status</param>
    ''' <returns>As Boolean </returns>
    ''' <remarks></remarks>
    Public Function funUpdateInvoice(ByVal sStatus As String) As Boolean
        Dim strSQL As String
        strSQL = "UPDATE invoices set "
        strSQL += "invLastUpdatedOn=now() ,"
        strSQL += "invStatus='" + sStatus + "'"
        strSQL += " where invID='" + _ARInvoiceNo + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Get AssignedTo For Invoice No
    ''' From table invoices with Inner Join invoiceitems , left join partners ,left join collection ,left join accountreceivable And left join users
    ''' </summary>
    ''' <param name="nInvNo">As String invoice number</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetAssignedTo(ByVal nInvNo As String) As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT concat(u.userFirstName,' ',u.userLastName) as AssignedTo FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID left join users u on u.userID=c.ColUserAssignedTo where o.invStatus<>'P' and o.invID='" & nInvNo & "' group by o.invID, a.ARInvoiceNo "
        Try
            strData = GetScalarData(strSQL).ToString
        Catch ex As NullReferenceException
            strData = ""
        Catch ex As Exception
            strData = ""
        End Try
        Return strData
    End Function
    ''' <summary>
    ''' Fill Grid For Payment Not Received or Partial Received
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Query for selecting data for grid</remarks>
    Public Function funFillGridForInvoicePaymnetDetail() As String
        Dim strSQL As String

        strSQL = "SELECT distinct o.invID,o.InvRefNo, PartnerLongName as CustomerName, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, invStatus, invCustPO, invForOrderNo, o.invCurrencyCode, o.invCurrencyExRate, o.invRefType, a.* FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID  inner join accountreceivable a on a.ARInvoiceNo=o.invID "

        'strSQL += " where o.invStatus<>'P' "
        If _ARInvoiceNo <> "" Then
            strSQL += " and o.invID='" & _ARInvoiceNo & "' "
        End If
        strSQL += " order by o.invCreatedOn, CustomerName "
        Return strSQL
    End Function
    ''' <summary>
    ''' get the InvID for Batch statement
    ''' </summary>
    ''' <param name="strCompanyID"></param>
    ''' <param name="SearchDays"></param>
    ''' <param name="strValue"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funBatchStatement(ByVal strCompanyID As String, ByVal SearchDays As String, ByVal strValue As String) As ArrayList
        Dim strSQL As String
        Dim strArry As New ArrayList
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distinct o.invID FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID left join users u on u.userID=c.ColUserAssignedTo "
        Else
            strSQL = "SELECT distinct o.invID FROM invoices o inner join salesrepcustomer s on s.CustomerID=o.invCustID and s.CustomerType=o.invCustType and s.UserID='" & HttpContext.Current.Session("UserID") & "' inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID left join users u on u.userID=c.ColUserAssignedTo "
        End If
        If strValue = "1" Then
            strSQL += " where o.invStatus<>'" & InvoicesStatus.PAYMENT_RECEIVED.ToString() & " ' "
        Else
            strSQL += " where 1=1 "
        End If
        If strCompanyID <> "" Then
            strSQL += " and invCompanyID ='" & strCompanyID & "' "
        End If
        If SearchDays <> "" Then
            strSQL += funCreatedDateRage(SearchDays)
        End If
        Dim drObj As OdbcDataReader
        drObj = GetDataReader(strSQL)
        While drObj.Read
            strArry.Add(drObj("invID").ToString())
        End While
        drObj = Nothing
        CloseDatabaseConnection()
        Return strArry
    End Function

    Public Function funBatchStatement(ByVal strCompanyID As String, ByVal strCustomerID As String, ByVal SearchDays As String, ByVal strValue As String) As ArrayList
        Dim strSQL As String
        Dim strArry As New ArrayList
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distinct o.invID FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID left join users u on u.userID=c.ColUserAssignedTo "
        Else
            strSQL = "SELECT distinct o.invID FROM invoices o inner join salesrepcustomer s on s.CustomerID=o.invCustID and s.CustomerType=o.invCustType and s.UserID='" & HttpContext.Current.Session("UserID") & "' inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID left join collection c on c.ColInvID=o.invID left join users u on u.userID=c.ColUserAssignedTo "
        End If
        If strValue = "1" Then
            strSQL += " where o.invStatus<>'" & InvoicesStatus.PAYMENT_RECEIVED.ToString() & " ' "
        Else
            strSQL += " where 1=1 "
        End If
        If strCompanyID <> "" Then
            strSQL += " and invCompanyID ='" & strCompanyID & "' "
        End If
        If strCustomerID <> "" Then
            strSQL += " and invCustID ='" & strCustomerID & "' "
        End If
        If SearchDays <> "" Then
            strSQL += funCreatedDateRage(SearchDays)
        End If
        Dim drObj As OdbcDataReader
        drObj = GetDataReader(strSQL)
        While drObj.Read
            strArry.Add(drObj("invID").ToString())
        End While
        drObj = Nothing
        CloseDatabaseConnection()
        Return strArry
    End Function

    Public Function funCreatedDateRage(ByVal strValue As String) As String
        Dim strSql As String
        strSql = " and "
        If strValue = "10" Then
            strSql += "(o.invCreatedOn >= SUBDATE(now(), 10)  and o.invCreatedOn < now())"
        ElseIf strValue = "15" Then
            strSql += "(o.invCreatedOn >= SUBDATE(now(), 15)  and o.invCreatedOn < SUBDATE(now(), 10))"
        ElseIf strValue = "30" Then
            strSql += "(o.invCreatedOn >= SUBDATE(now(), 30)  and o.invCreatedOn < SUBDATE(now(), 15))"
        ElseIf strValue = "60" Then
            strSql += "(o.invCreatedOn >= SUBDATE(now(), 60)  and o.invCreatedOn < SUBDATE(now(), 30))"
        ElseIf strValue = "90" Then
            strSql += "(o.invCreatedOn >= SUBDATE(now(), 90)  and o.invCreatedOn < SUBDATE(now(), 60))"
        ElseIf strValue = "180" Then
            strSql += "(o.invCreatedOn >= SUBDATE(now(), 180)  and o.invCreatedOn < SUBDATE(now(), 90))"
        Else
            strSql += "(o.invCreatedOn < SUBDATE(now(), 180))"
        End If
        Return strSql
    End Function

    ''' <summary>
    '''  Get Inv  in accountreceivable table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funISExistInvID() As Boolean
        Dim strSql As String
        strSql = "SELECT count(*) FROM accountreceivable "
        strSql += " where ARInvoiceNo='" + _ARInvoiceNo + "'"
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.GetScalarData(strSql)
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Delete User
    ''' </summary>
    ''' <param name="strARID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteAR(ByVal strARID As String) As Boolean
        Dim strSQL As String
        strSQL = "DELETE FROM accountreceivable WHERE AccountReceivableID='" + strARID + "' "
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function funARNotes(ByVal strPatnetID As String) As String
        Dim strSQL As String
        strSQL = "select ARNote,ARInvoiceNo,InvRefNo,invID FROM accountreceivable inner join invoices on invoices.invid=accountreceivable.ARInvoiceNo WHERE  invCustID='" + strPatnetID + "' order by invID desc "
        Return strSQL
    End Function
End Class

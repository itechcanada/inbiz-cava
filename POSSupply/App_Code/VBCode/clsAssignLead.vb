Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports System.Threading
Imports System.Globalization
Imports System.Math
'Imports System.Web.HttpContext
'Imports clsCommon
'Imports Resources.Resource
Public Class clsAssignLead
    Inherits clsDataClass
    Private _AssignLeadID, _UserID, _CustomerID, _CustomerType, _Active As String
    Public Property AssignLeadID() As String
        Get
            Return _AssignLeadID
        End Get
        Set(ByVal value As String)
            _AssignLeadID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserID() As String
        Get
            Return _UserID
        End Get
        Set(ByVal value As String)
            _UserID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustID() As String
        Get
            Return _CustomerID
        End Get
        Set(ByVal value As String)
            _CustomerID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustType() As String
        Get
            Return _CustomerType
        End Get
        Set(ByVal value As String)
            _CustomerType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Active() As String
        Get
            Return _Active
        End Get
        Set(ByVal value As String)
            _Active = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Insert AssignLead
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertAssignLead() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO assignleads(UserID, CustomerID, CustomerType, Active) VALUES('"
        strSQL += _UserID + "','"
        strSQL += _CustomerID + "','"
        strSQL += _CustomerType + "','"
        strSQL += _Active + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update AssignLead
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateAssignLead() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE assignleads set "
        strSQL += "Active='" + _Active + "'"
        strSQL += " where CustomerID='" + _CustomerID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Fill Rejected Reason in Dropdown list
    ''' </summary>
    ''' <param name="dlReason"></param>
    ''' <param name="strType"></param>
    ''' <remarks></remarks>
    Public Sub subGetReason(ByVal dlReason As DropDownList, ByVal strType As String)
        Dim strSQL As String
        strSQL = "SELECT itReasonID, itReasonText from rejectreason where itReasonPfx='" & strType & "' "
        dlReason.DataSource = GetDataReader(strSQL)
        dlReason.DataTextField = "itReasonText"
        dlReason.DataValueField = "itReasonID"
        dlReason.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.msgSelectReason
        dlReason.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Check Duplicate SR
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDuplicateLeads() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM assignleads where CustomerID='" & _CustomerID & "' " 'and Active='1' " 'and UserID='" & _UserID & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Update SalesRepCustomer
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funReassigend() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE assignleads set "
        strSQL += "UserID='" + _UserID + "', "
        strSQL += "CustomerType='" + _CustomerType + "'"
        strSQL += " where CustomerID='" + _CustomerID + "'"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
End Class

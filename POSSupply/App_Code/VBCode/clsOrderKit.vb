Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsOrderKit
    Inherits clsDataClass
    Private _ordkitID, _ordKitName, _ordKitbarcode, _ordKitPrice As String
    Private _prdKitID, _prdID, _prdIncludeID, _prdIncludeQty As String
    Public Property ordkitID() As String
        Get
            Return _ordkitID
        End Get
        Set(ByVal value As String)
            _ordkitID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ordKitName() As String
        Get
            Return _ordKitName
        End Get
        Set(ByVal value As String)
            _ordKitName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ordKitbarcode() As String
        Get
            Return _ordKitbarcode
        End Get
        Set(ByVal value As String)
            _ordKitbarcode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ordKitPrice() As String
        Get
            Return _ordKitPrice
        End Get
        Set(ByVal value As String)
            _ordKitPrice = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Property PrdKitID() As String
        Get
            Return _prdKitID
        End Get
        Set(ByVal value As String)
            _prdKitID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdID() As String
        Get
            Return _prdID
        End Get
        Set(ByVal value As String)
            _prdID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdIncludeID() As String
        Get
            Return _prdIncludeID
        End Get
        Set(ByVal value As String)
            _prdIncludeID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdIncludeQty() As String
        Get
            Return _prdIncludeQty
        End Get
        Set(ByVal value As String)
            _prdIncludeQty = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    '''  Insert Order kit
    ''' </summary>
    ''' <param name="sID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertOrderKit(ByRef sID As String) As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO orderkit(ordKitName, ordKitbarcode, ordKitPrice) VALUES('"
        strSQL += _ordKitName + "','"
        strSQL += _ordKitbarcode + "','"
        strSQL += _ordKitPrice + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        sID = obj.GetScalarData("Select LAST_INSERT_ID()")
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    '''  Insert OrderProduct  kit
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertOrdProductKits() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO ordproductkits( prdID, prdIncludeID, prdIncludeQty) VALUES("
        strSQL += "'" + _prdID + "','"
        strSQL += _prdIncludeID + "','"
        strSQL += _prdIncludeQty + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim objDataClass As New clsDataClass
        Dim intData As Int16 = 0
        intData = objDataClass.PopulateData(strSQL)
        objDataClass.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
End Class

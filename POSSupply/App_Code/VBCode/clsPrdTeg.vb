﻿Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports System.Collections.Generic
'Imports System.Web.HttpContext
'Imports clsCommon

Public Class clsPrdTag
    'Private _iD, _productID, _receivingID, _soId, _poID, _tag, _quantity, _tagCreatedDate As String

    Private _iD As Integer
    Public Property ID() As Integer
        Get
            Return _iD
        End Get
        Set(ByVal value As Integer)
            _iD = value
        End Set
    End Property

    Private _productID As Integer
    Public Property ProductID() As Integer
        Get
            Return _productID
        End Get
        Set(ByVal value As Integer)
            _productID = value
        End Set
    End Property

    Private _receivingID As Integer
    Public Property ReceivingID() As Integer
        Get
            Return _receivingID
        End Get
        Set(ByVal value As Integer)
            _receivingID = value
        End Set
    End Property

    Private _soID As Integer
    Public Property SOID() As Integer
        Get
            Return _soID
        End Get
        Set(ByVal value As Integer)
            _soID = value
        End Set
    End Property

    Private _poID As Integer
    Public Property POID() As Integer
        Get
            Return _poID
        End Get
        Set(ByVal value As Integer)
            _poID = value
        End Set
    End Property

    Private _wareHouseCode As String
    Public Property WareHouseCode() As String
        Get
            Return _wareHouseCode
        End Get
        Set(ByVal value As String)
            _wareHouseCode = clsCommon.funRemove(value, True)
        End Set
    End Property


    Private _tag As String
    Public Property Tag() As String
        Get
            Return _tag
        End Get
        Set(ByVal value As String)
            _tag = clsCommon.funRemove(value, True)
        End Set
    End Property

    Private _quantity As String
    Public Property Quantity() As String
        Get
            Return _quantity
        End Get
        Set(ByVal value As String)
            _quantity = value
        End Set
    End Property


    Private _createdDate As Date
    Public Property CreatedDate() As Date
        Get
            Return _createdDate
        End Get
        Set(ByVal value As Date)
            _createdDate = value
        End Set
    End Property

    ''' <summary>
    ''' Insert 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Insert() As Boolean
        Dim strSQL As New StringBuilder
        strSQL.Append("INSERT INTO prdtags")
        strSQL.Append("(ProductID, ReceivingID, SOID, POID, WareHouseCode, Tag, Quantity, TagCreatedDate) ")
        strSQL.Append("VALUES (@ProductID, @ReceivingID, @SOID, @POID, '@WareHouseCode', '@Tag', @Quantity, '@CreatedDate');")

        strSQL = strSQL.Replace("@ProductID", _productID.ToString)
        strSQL = strSQL.Replace("@ReceivingID", _receivingID.ToString)
        strSQL = strSQL.Replace("@SOID", _soID.ToString)
        strSQL = strSQL.Replace("@POID", _poID.ToString)
        strSQL = strSQL.Replace("@WareHouseCode", _wareHouseCode)
        strSQL = strSQL.Replace("@Tag", _tag)
        strSQL = strSQL.Replace("@Quantity", _quantity)
        strSQL = strSQL.Replace("@CreatedDate", Now.ToString("yyyy-MM-dd hh:mm:ss"))
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")

        Dim obj As New clsDataClass
        Dim objRetData As Object
        objRetData = obj.PopulateData(strSQL.ToString)
        obj.CloseDatabaseConnection()
        Return objRetData <> Nothing
    End Function


    ''' <summary>
    ''' Returns the sequence PONO-SONO-WHS
    ''' </summary>
    ''' <param name="poID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetUniqueTagWithoutProductID(ByVal poID As String) As String
        poID = clsCommon.funRemove(poID)

        Dim strQry As String = "SELECT p.poID, p.poWhsCode, p.poForSoNo FROM purchaseorders p WHERE p.poID = @poID;"
        strQry = strQry.Replace("@poID", poID)
        strQry = strQry.Replace("'sysNull'", "Null")
        strQry = strQry.Replace("sysNull", "Null")

        Dim obj As New clsDataClass
        Dim objVal As DataTable
        objVal = obj.GetDataTable(strQry)

        If objVal IsNot Nothing And objVal.Rows.Count > 0 Then
            Dim seq1 As String = objVal.Rows(0)(0).ToString()
            Dim seq2 As String = objVal.Rows(0)(2).ToString()
            Dim seq3 As String = objVal.Rows(0)(1).ToString()
            Return String.Format("{0}-{1}-{2}", seq1, IIf(String.IsNullOrEmpty(seq2), "NA", seq2), IIf(String.IsNullOrEmpty(seq3), "NA", seq3))
        Else
            Return String.Empty
        End If
    End Function
    ''' <summary>
    ''' Get Tags By Product
    ''' </summary>
    ''' <param name="productID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTagsByProduct(ByVal productID As String) As String
        productID = clsCommon.funRemove(productID)

        Dim strQry As String = "SELECT Tag FROM prdtags WHERE ProductID = @ProductID;"
        strQry = strQry.Replace("@ProductID", productID)
        strQry = strQry.Replace("'sysNull'", "Null")
        strQry = strQry.Replace("sysNull", "Null")

        Dim obj As New clsDataClass
        Dim objVal As DataTable
        objVal = obj.GetDataTable(strQry)

        Dim retList As New StringBuilder()

        For Each row As DataRow In objVal.Rows
            retList.Append(row("Tag").ToString())
            retList.Append("^")
        Next

        Return retList.ToString()
    End Function
    ''' <summary>
    ''' Get Tags
    ''' </summary>
    ''' <param name="pID"></param>
    ''' <param name="wshCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTags(ByVal pID As String, ByVal wshCode As String) As DataTable
        Dim SQL As String = "SELECT ID," + vbCrLf + _
                             "       ProductID," + vbCrLf + _
                             "       ReceivingID," + vbCrLf + _
                             "       SOID," + vbCrLf + _
                             "       POID," + vbCrLf + _
                             "       WareHouseCode," + vbCrLf + _
                             "       Tag," + vbCrLf + _
                             "       Quantity," + vbCrLf + _
                             "       TagCreatedDate" + vbCrLf + _
                             "  FROM prdtags" + vbCrLf + _
                             " WHERE ProductID = @P_ProductID"
        SQL = SQL.Replace("@P_ProductID", clsCommon.funRemove(pID))
        If Not String.IsNullOrEmpty(wshCode.Trim()) Then
            SQL = SQL & " AND WareHouseCode = '@P_WareHouseCode'"
            SQL = SQL.Replace("@P_WareHouseCode", clsCommon.funRemove(wshCode))
        End If

        Dim objData As New clsDataClass()
        Dim dt As New DataTable()
        dt = objData.GetDataTable(SQL)
        objData.CloseDatabaseConnection()
        Return dt
    End Function

End Class

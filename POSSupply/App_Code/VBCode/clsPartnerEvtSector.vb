Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsPartnerEvtSector
	Inherits clsDataClass
	Private _PartnerEvtSectorID, _PartnerEvtSvcLogID, _PartnerEvtSectorVal As String
	Public Property PartnerEvtSectorID() As String
		Get
			Return _PartnerEvtSectorID
		End Get
		Set(ByVal value As String)
            _PartnerEvtSectorID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerEvtSvcLogID() As String
		Get
			Return _PartnerEvtSvcLogID
		End Get
		Set(ByVal value As String)
            _PartnerEvtSvcLogID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerEvtSectorVal() As String
		Get
			Return _PartnerEvtSectorVal
		End Get
		Set(ByVal value As String)
            _PartnerEvtSectorVal = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert PartnerEvtSector
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertPartnerEvtSector() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO partnerevtsector( PartnerEvtSvcLogID, PartnerEvtSectorVal) VALUES('"
		strSQL += _PartnerEvtSvcLogID + "','"
		strSQL += _PartnerEvtSectorVal + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    '''  Update PartnerEvtSector
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updatePartnerEvtSector() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE partnerevtsector set "
		strSQL += "PartnerEvtSvcLogID='" + _PartnerEvtSvcLogID + "', "
		strSQL += "PartnerEvtSectorVal='" + _PartnerEvtSectorVal + "'"
		strSQL += " where PartnerEvtSectorID='" + _PartnerEvtSectorID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    '''  Populate objects of PartnerEvtSector
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPartnerEvtSectorInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  PartnerEvtSvcLogID, PartnerEvtSectorVal FROM partnerevtsector where PartnerEvtSectorID='" + _PartnerEvtSectorID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_PartnerEvtSvcLogID = drObj.Item("PartnerEvtSvcLogID").ToString
			_PartnerEvtSectorVal = drObj.Item("PartnerEvtSectorVal").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate objects of PartnerEvtSector
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getPartnerEvtSectorWithRefToLogID()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT PartnerEvtSectorID, PartnerEvtSvcLogID, PartnerEvtSectorVal FROM partnerevtsector where PartnerEvtSvcLogID='" + _PartnerEvtSvcLogID + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _PartnerEvtSectorID = drObj.Item("PartnerEvtSectorID").ToString
            _PartnerEvtSvcLogID = drObj.Item("PartnerEvtSvcLogID").ToString
            _PartnerEvtSectorVal = drObj.Item("PartnerEvtSectorVal").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Check Record
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckRecord() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM partnerevtsector where PartnerEvtSvcLogID='" & _PartnerEvtSvcLogID & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
End Class

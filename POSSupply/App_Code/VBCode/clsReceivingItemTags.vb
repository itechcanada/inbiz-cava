﻿Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Imports System.Collections.Generic

Public Class clsReceivingItemTags
    Private _productID, _recID, _uniqueTag, _tagCount As String

    Public Property ProductID() As String
        Get
            Return _productID
        End Get
        Set(ByVal value As String)
            _productID = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Property RecID() As String
        Get
            Return _recID
        End Get
        Set(ByVal value As String)
            _recID = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Property UniqueTag() As String
        Get
            Return _uniqueTag
        End Get
        Set(ByVal value As String)
            _uniqueTag = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Property TagCount() As String
        Get
            Return _tagCount
        End Get
        Set(ByVal value As String)
            _tagCount = clsCommon.funRemove(value, True)
        End Set
    End Property
    ''' <summary>
    ''' Insert for receving Item tags
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Insert() As Boolean
        Dim strSQL As String = "INSERT INTO recevieditemtags VALUES(@ProductID, @RecID, '@UniqueTag', @TagCount);"
        strSQL = strSQL.Replace("@ProductID", _productID)
        strSQL = strSQL.Replace("@RecID", _recID)
        strSQL = strSQL.Replace("@UniqueTag", _uniqueTag)
        strSQL = strSQL.Replace("@TagCount", _tagCount)
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Get Max Count
    ''' </summary>
    ''' <param name="pId"></param>
    ''' <param name="recId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetMaxcount(ByVal pId As String, ByVal recId As String) As Integer
        pId = clsCommon.funRemove(pId)
        recId = clsCommon.funRemove(recId)

        Dim strQry As String = "SELECT MAX(r.TagCount) FROM  recevieditemtags r WHERE r.ProductID = @ProductID AND r.RecID = @RecID;"
        strQry = strQry.Replace("@ProductID", pId)
        strQry = strQry.Replace("@RecID", recId)
        strQry = strQry.Replace("'sysNull'", "Null")
        strQry = strQry.Replace("sysNull", "Null")

        Dim obj As New clsDataClass
        Dim objVal As Object
        objVal = obj.GetScalarData(strQry)

        If objVal IsNot DBNull.Value Then
            Return Convert.ToInt32(objVal)
        Else
            Return 0
        End If
    End Function

    ''' <summary>
    ''' Get Unique Tag Without Product ID
    ''' </summary>
    ''' <param name="poID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetUniqueTagWithoutProductID(ByVal poID As String) As String
        poID = clsCommon.funRemove(poID)

        Dim strQry As String = "SELECT p.poID, p.poWhsCode, p.poForSoNo FROM purchaseorders p WHERE p.poID = @poID;"
        strQry = strQry.Replace("@poID", poID)
        strQry = strQry.Replace("'sysNull'", "Null")
        strQry = strQry.Replace("sysNull", "Null")

        Dim obj As New clsDataClass
        Dim objVal As DataTable
        objVal = obj.GetDataTable(strQry)

        If objVal IsNot Nothing And objVal.Rows.Count > 0 Then
            Dim seq1 As String = objVal.Rows(0)(0).ToString()
            Dim seq2 As String = objVal.Rows(0)(2).ToString()
            Dim seq3 As String = objVal.Rows(0)(1).ToString()
            Return String.Format("{0}-{1}-{2}", seq1, IIf(String.IsNullOrEmpty(seq2), "NA", seq2), seq3)
        Else
            Return String.Empty
        End If
    End Function

    ''' <summary>
    ''' Get Tags To Print
    ''' </summary>
    ''' <param name="productID"></param>
    ''' <param name="recID"></param>
    ''' <param name="startRank"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTagsToPrint(ByVal productID As String, ByVal recID As String, ByVal startRank As String) As List(Of String)
        productID = clsCommon.funRemove(productID)
        recID = clsCommon.funRemove(recID)
        startRank = clsCommon.funRemove(startRank)

        Dim strQry As String = "SELECT r.ProductID, r.RecID, r.UniqueTag, r.TagCount FROM recevieditemtags r WHERE r.ProductID = @ProductID AND r.RecID= @RecID AND r.TagCount >= @StartRank ORDER BY TagCount;"
        strQry = strQry.Replace("@ProductID", productID)
        strQry = strQry.Replace("@RecID", recID)
        strQry = strQry.Replace("@StartRank", startRank)
        strQry = strQry.Replace("'sysNull'", "Null")
        strQry = strQry.Replace("sysNull", "Null")

        Dim obj As New clsDataClass
        Dim objVal As DataTable
        objVal = obj.GetDataTable(strQry)
        Dim retList As New List(Of String)
        For Each row As DataRow In objVal.Rows
            retList.Add(row("UniqueTag").ToString())
        Next
        Return retList
    End Function
    ''' <summary>
    ''' Get Tags By Product
    ''' </summary>
    ''' <param name="productID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetTagsByProduct(ByVal productID As String) As String
        productID = clsCommon.funRemove(productID)

        Dim strQry As String = "SELECT r.ProductID, r.RecID, r.UniqueTag, r.TagCount FROM recevieditemtags r WHERE r.ProductID = @ProductID;"
        strQry = strQry.Replace("@ProductID", productID)
        strQry = strQry.Replace("'sysNull'", "Null")
        strQry = strQry.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim objVal As DataTable
        objVal = obj.GetDataTable(strQry)
        Dim retList As New StringBuilder()
        For Each row As DataRow In objVal.Rows
            retList.Append(row("UniqueTag").ToString())
            retList.Append("^")
        Next
        Return retList.ToString()
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsPurchaseOrderItems
	Inherits clsDataClass
    Private _poItems, _poID, _poItemPrdId, _poQty, _poUnitPrice, _poItemShpToWhsCode, _poItemRcvDateTime, _prodIDDesc, _poRcvdQty As String
	Public Property PoItems() As String
		Get
			Return _poItems
		End Get
		Set(ByVal value As String)
            _poItems = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PoID() As String
		Get
			Return _poID
		End Get
		Set(ByVal value As String)
            _poID = clsCommon.funRemove(value, True)
		End Set
	End Property
    Public Property PoItemPrdId() As String
        Get
            Return _poItemPrdId
        End Get
        Set(ByVal value As String)
            _poItemPrdId = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Property PoQty() As String
		Get
			Return _poQty
		End Get
		Set(ByVal value As String)
            _poQty = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PoUnitPrice() As String
		Get
			Return _poUnitPrice
		End Get
		Set(ByVal value As String)
            _poUnitPrice = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PoItemShpToWhsCode() As String
		Get
			Return _poItemShpToWhsCode
		End Get
		Set(ByVal value As String)
            _poItemShpToWhsCode = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property PoItemRcvDateTime() As String
        Get
            Return _poItemRcvDateTime
        End Get
        Set(ByVal value As String)
            _poItemRcvDateTime = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ProdIDDesc() As String
        Get
            Return _prodIDDesc
        End Get
        Set(ByVal value As String)
            _prodIDDesc = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PoRcvdQty() As String
        Get
            Return _poRcvdQty
        End Get
        Set(ByVal value As String)
            _poRcvdQty = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert Purchase Order Items
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertPurchaseOrderItems() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO purchaseorderitems( poID, poItemPrdId, poQty, poUnitPrice, poItemShpToWhsCode, prodIDDesc ) VALUES('"
        strSQL += _poID + "','"
        strSQL += _poItemPrdId + "','"
        strSQL += _poQty + "','"
        strSQL += _poUnitPrice + "','"
        strSQL += _poItemShpToWhsCode + "','"
        strSQL += _prodIDDesc + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update Purchase Order Items
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updatePurchaseOrderItems() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE purchaseorderitems set "
        strSQL += "poID='" + _poID + "', "
        strSQL += "poItemPrdId='" + _poItemPrdId + "', "
        strSQL += "poQty='" + _poQty + "', "
        strSQL += "poUnitPrice='" + _poUnitPrice + "', "
        If _poItemShpToWhsCode <> "" Then
            strSQL += "poItemShpToWhsCode='" + _poItemShpToWhsCode + "', "
        End If
        strSQL += "prodIDDesc='" + _prodIDDesc + "' "
        ' strSQL += "poRcvdQty='" + _poRcvdQty + "' "
        strSQL += " where poItems='" + _poItems + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    '''  Populate objects of PurchaseOrderItems
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPurchaseOrderItemsInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
        strSQL = "SELECT  poID, poItemPrdId, poQty, poUnitPrice, poItemShpToWhsCode,prodIDDesc, poRcvdQty FROM purchaseorderitems where poItems='" + _poItems + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _poID = drObj.Item("poID").ToString
            _poItemPrdId = drObj.Item("poItemPrdId").ToString
            _poQty = drObj.Item("poQty").ToString
            _poUnitPrice = drObj.Item("poUnitPrice").ToString
            _poItemShpToWhsCode = drObj.Item("poItemShpToWhsCode").ToString
            _prodIDDesc = drObj.Item("prodIDDesc").ToString
            _poRcvdQty = drObj.Item("poRcvdQty").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub

    ''' <summary>
    ''' Fill Grid of PO
    ''' </summary>
    ''' <param name="POID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal POID As String) As String
        Dim strSQL As String
        strSQL = "SELECT i.poItems, i.poItemPrdId,p.prdIntID, p.prdExtID, p.prdUPCCode,CASE i.prodIDDesc='' WHEN False THEN i.prodIDDesc Else p.prdName  END as prdName, poQty, (poUnitPrice * poCurrencyExRate) as poUnitPrice, (poQty * poUnitPrice * poCurrencyExRate) as amount, poItemShpToWhsCode FROM purchaseorderitems i inner join purchaseorders po on po.poID=i.poID inner join products p on i.poItemPrdId=p.productID where i.poID='" & POID & "'"
        Return strSQL
    End Function

    ''' <summary>
    '''  Delete PO Items
    ''' </summary>
    ''' <param name="DeletePoID"></param>
    ''' <param name="DeletePrdID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeletePOItems(ByVal DeletePoID As String, ByVal DeletePrdID As String) As String
        Dim strSQL As String
        strSQL = "DELETE FROM purchaseorderitems WHERE poID='" + DeletePoID + "' and poItems='" & DeletePrdID & "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Update PurchaseOrderQuantity
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updatePurchaseOrderQuantity() As String
        Dim strSQL As String
        strSQL = "UPDATE purchaseorderitems set "
        'strSQL += "poQty='" + _poQty + "', "
        strSQL += "poRcvdQty='" + _poRcvdQty + "', "
        strSQL += "poItemRcvDateTime='" + _poItemRcvDateTime + "' "
        strSQL += " where poItems='" + _poItems + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Return strSQL
    End Function
    ''' <summary>
    ''' Check Product Quantity
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckProductQuantity() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM prdquantity where prdID='" & _poItemPrdId & "' and prdWhsCode='" & _poItemShpToWhsCode & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Update Purchase Order Quantity
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateProductQuantity() As Boolean
        Dim bResult As Boolean = False
        Dim objPrdQty As clsPrdQuantity
        If funCheckProductQuantity() = True Then
            objPrdQty = New clsPrdQuantity
            objPrdQty.ProductID = _poItemPrdId
            objPrdQty.prdWhsCode = _poItemShpToWhsCode
            objPrdQty.subGetProductQuantity()
            If IsNumeric(objPrdQty.prdOhdQty) = True Then
                objPrdQty.prdOhdQty = CDbl(objPrdQty.prdOhdQty) + CDbl(_poQty)
            Else
                objPrdQty.prdOhdQty = _poQty
            End If
            bResult = objPrdQty.funUpdatePrdQuantity(True)
            objPrdQty = Nothing
        Else
            objPrdQty = New clsPrdQuantity
            objPrdQty.ProductID = _poItemPrdId
            objPrdQty.prdWhsCode = _poItemShpToWhsCode
            objPrdQty.prdOhdQty = _poQty
            objPrdQty.prdQuoteRsv = 0
            objPrdQty.prdSORsv = 0
            objPrdQty.prdDefectiveQty = 0
            objPrdQty.TaxCode = ""
            bResult = objPrdQty.funInsertPrdQuantity()
            objPrdQty = Nothing
        End If
        Return bResult
    End Function
    ''' <summary>
    ''' Calcualte GST
    ''' </summary>
    ''' <param name="strPrdID"></param>
    ''' <param name="strWhsCode"></param>
    ''' <param name="Price"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCalcGST(ByVal strPrdID As String, ByVal strWhsCode As String, ByVal Price As Double) As Double
        Dim dblTotal As Double = 0
        Dim strTaxCode As String
        Dim objDC As New clsDataClass
        Dim objDR As System.Data.Odbc.OdbcDataReader
        Dim objTaxCode As Object
        Dim strSQL As String = ""
        strSQL = "SELECT prdTaxCode FROM prdquantity where prdID='" & strPrdID & "' and prdWhsCode='" & strWhsCode & "' "
        objTaxCode = objDC.GetScalarData(strSQL)
        If objTaxCode.ToString = "" Then
            strSQL = ""
            strSQL = "SELECT WarehouseRegTaxCode FROM syswarehouses where WarehouseCode='" & strWhsCode & "' "
            objTaxCode = objDC.GetScalarData(strSQL)
            If objTaxCode.ToString = "" Then
                Return 0
            Else
                strTaxCode = objTaxCode.ToString
            End If
        Else
            strTaxCode = objTaxCode.ToString
        End If
        strSQL = ""
        strSQL = "select * from sysTaxCode where sysTaxCode = '" & strTaxCode & "' and sysTaxDesc='GST' order by sysTaxSequence"
        objDR = objDC.GetDataReader(strSQL)
        While objDR.Read
            If objDR("sysTaxOnTotal").ToString = "0" Then
                dblTotal += Price * (CDbl(objDR("sysTaxPercentage") / 100))
            ElseIf objDR("sysTaxOnTotal").ToString = "1" Then
                dblTotal = (Price + dblTotal) * (CDbl(objDR("sysTaxPercentage") / 100))
            End If
        End While
        objDR.Close()
        objDC.CloseDatabaseConnection()
        objDC = Nothing
        Return dblTotal
    End Function
    ''' <summary>
    ''' Calcualte PST
    ''' </summary>
    ''' <param name="strPrdID"></param>
    ''' <param name="strWhsCode"></param>
    ''' <param name="Price"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCalcPST(ByVal strPrdID As String, ByVal strWhsCode As String, ByVal Price As Double) As Double
        Dim dblTotal As Double = 0
        Dim strTaxCode As String
        Dim objDC As New clsDataClass
        Dim objDR As System.Data.Odbc.OdbcDataReader
        Dim objTaxCode As Object
        Dim strSQL As String = ""
        strSQL = "SELECT prdTaxCode FROM prdquantity where prdID='" & strPrdID & "' and prdWhsCode='" & strWhsCode & "' "
        objTaxCode = objDC.GetScalarData(strSQL)
        If objTaxCode.ToString = "" Then
            strSQL = ""
            strSQL = "SELECT WarehouseRegTaxCode FROM syswarehouses where WarehouseCode='" & strWhsCode & "' "
            objTaxCode = objDC.GetScalarData(strSQL)
            If objTaxCode.ToString = "" Then
                Return 0
            Else
                strTaxCode = objTaxCode.ToString
            End If
        Else
            strTaxCode = objTaxCode.ToString
        End If
        strSQL = ""
        strSQL = "select * from sysTaxCode where sysTaxCode = '" & strTaxCode & "' and sysTaxDesc='PST' order by sysTaxSequence"
        objDR = objDC.GetDataReader(strSQL)
        While objDR.Read
            If objDR("sysTaxOnTotal").ToString = "0" Then
                dblTotal += Price * (CDbl(objDR("sysTaxPercentage") / 100))
            ElseIf objDR("sysTaxOnTotal").ToString = "1" Then
                dblTotal = (Price + dblTotal) * (CDbl(objDR("sysTaxPercentage") / 100))
            End If
        End While
        objDR.Close()
        objDC.CloseDatabaseConnection()
        objDC = Nothing
        Return dblTotal
    End Function
    ''' <summary>
    ''' Calcualte QST
    ''' </summary>
    ''' <param name="strPrdID"></param>
    ''' <param name="strWhsCode"></param>
    ''' <param name="Price"></param>
    ''' <param name="GST"></param>
    ''' <param name="PST"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCalcQST(ByVal strPrdID As String, ByVal strWhsCode As String, ByVal Price As Double, ByVal GST As Double, ByVal PST As Double) As Double
        Dim dblTotal As Double = 0
        Dim strTaxCode As String
        Dim objDC As New clsDataClass
        Dim objDR As System.Data.Odbc.OdbcDataReader
        Dim objTaxCode As Object
        Dim strSQL As String = ""
        strSQL = "SELECT prdTaxCode FROM prdquantity where prdID='" & strPrdID & "' and prdWhsCode='" & strWhsCode & "' "
        objTaxCode = objDC.GetScalarData(strSQL)
        If objTaxCode.ToString = "" Then
            strSQL = ""
            strSQL = "SELECT WarehouseRegTaxCode FROM syswarehouses where WarehouseCode='" & strWhsCode & "' "
            objTaxCode = objDC.GetScalarData(strSQL)
            If objTaxCode.ToString = "" Then
                Return 0
            Else
                strTaxCode = objTaxCode.ToString
            End If
        Else
            strTaxCode = objTaxCode.ToString
        End If
        strSQL = ""
        strSQL = "select * from sysTaxCode where sysTaxCode = '" & strTaxCode & "' and sysTaxDesc='QST' order by sysTaxSequence"
        objDR = objDC.GetDataReader(strSQL)
        While objDR.Read
            If objDR("sysTaxOnTotal").ToString = "0" Then
                dblTotal += Price * (CDbl(objDR("sysTaxPercentage") / 100))
            ElseIf objDR("sysTaxOnTotal").ToString = "1" Then
                dblTotal = (Price + GST + PST) * (CDbl(objDR("sysTaxPercentage") / 100))
            End If
        End While
        objDR.Close()
        objDC.CloseDatabaseConnection()
        objDC = Nothing
        Return dblTotal
    End Function
    ''' <summary>
    '''  Check Received Qty is equal to Ordered Qty or not for given PO Iid
    ''' </summary>
    ''' <param name="POID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRemainingPOIQty(ByVal POID As String) As Double
        Dim objDR As System.Data.Odbc.OdbcDataReader
        Dim strSQL As String = ""
        strSQL = "SELECT * FROM purchaseorderitems Where poID=" & POID
        objDR = GetDataReader(strSQL)
        Dim RcvdQty As Double = 0
        Dim OrderedQty As Double = 0
        While objDR.Read
            OrderedQty += CDbl(objDR.Item("poQty"))
            RcvdQty += CDbl(objDR.Item("poRcvdQty"))
        End While

        Return OrderedQty - RcvdQty
    End Function
    ''' <summary>
    '''  Update product Qty in inventory from Reciving
    ''' </summary>
    ''' <param name="RcvdQty"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateReceivedProductQuantity(ByVal RcvdQty As Double) As Boolean
        Dim bResult As Boolean = False
        Dim objPrdQty As clsPrdQuantity
        If funCheckProductQuantity() = True Then
            objPrdQty = New clsPrdQuantity
            objPrdQty.ProductID = _poItemPrdId
            objPrdQty.prdWhsCode = _poItemShpToWhsCode
            objPrdQty.subGetProductQuantity()
            If IsNumeric(objPrdQty.prdOhdQty) = True Then
                objPrdQty.prdOhdQty = CDbl(objPrdQty.prdOhdQty) + RcvdQty
            Else
                objPrdQty.prdOhdQty = _poRcvdQty
            End If
            bResult = objPrdQty.funUpdatePrdQuantity(True)
            objPrdQty = Nothing
        Else
            objPrdQty = New clsPrdQuantity
            objPrdQty.ProductID = _poItemPrdId
            objPrdQty.prdWhsCode = _poItemShpToWhsCode
            objPrdQty.prdOhdQty = _poRcvdQty
            objPrdQty.prdQuoteRsv = 0
            objPrdQty.prdSORsv = 0
            objPrdQty.prdDefectiveQty = 0
            objPrdQty.TaxCode = ""
            bResult = objPrdQty.funInsertPrdQuantity()
            objPrdQty = Nothing
        End If
        Return bResult
    End Function
    ''' <summary>
    '''  Check Received Qty is equal to Ordered Qty or not for given POItemsID
    ''' </summary>
    ''' <param name="POitemsID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetRemainingPOItemsQty(ByVal POitemsID As String) As Double
        Dim objDR As System.Data.Odbc.OdbcDataReader
        Dim strSQL As String = ""
        strSQL = "SELECT * FROM purchaseorderitems Where poItems=" & POitemsID
        objDR = GetDataReader(strSQL)
        Dim RcvdQty As Double = 0
        Dim OrderedQty As Double = 0
        While objDR.Read
            OrderedQty = CDbl(objDR.Item("poQty"))
            RcvdQty = CDbl(objDR.Item("poRcvdQty"))
        End While
        Return OrderedQty - RcvdQty
    End Function
    ''' <summary>
    ''' Count PO Items
    ''' </summary>
    ''' <param name="DeletePoID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCountPOItems(ByVal DeletePoID As String) As Integer
        Dim strSQL As String
        strSQL = "select count(*) FROM purchaseorderitems WHERE poID='" + DeletePoID + "'"
        Return GetScalarData(strSQL)
    End Function
End Class

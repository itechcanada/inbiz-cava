Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports System.Threading
Imports System.Globalization
'Imports System.Web.HttpContext
'Imports clsCommon
'Imports Resources.Resource

Public Class clsPrdQuantity
    Inherits clsDataClass
    Private _id, _prdID, _prdWhsCode, _prdOhdQty, _prdQuoteRsv, _prdSORsv, _prdDefectiveQty, _prdTaxCode, _prdBlock, _prdFloor, _prdAisle, _prdRack, _prdBin, _tagid As String

#Region "Property"

    Public Property id() As String
        Get
            Return _id
        End Get
        Set(ByVal value As String)
            _id = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ProductID() As String
        Get
            Return _prdID
        End Get
        Set(ByVal value As String)
            _prdID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property prdWhsCode() As String
        Get
            Return _prdWhsCode
        End Get
        Set(ByVal value As String)
            _prdWhsCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property prdOhdQty() As String
        Get
            Return _prdOhdQty
        End Get
        Set(ByVal value As String)
            _prdOhdQty = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property prdQuoteRsv() As String
        Get
            Return _prdQuoteRsv
        End Get
        Set(ByVal value As String)
            _prdQuoteRsv = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property prdSORsv() As String
        Get
            Return _prdSORsv
        End Get
        Set(ByVal value As String)
            _prdSORsv = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property prdDefectiveQty() As String
        Get
            Return _prdDefectiveQty
        End Get
        Set(ByVal value As String)
            _prdDefectiveQty = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property TaxCode() As String
        Get
            Return _prdTaxCode
        End Get
        Set(ByVal value As String)
            _prdTaxCode = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Property PrdBlock() As String
        Get
            Return _prdBlock
        End Get
        Set(ByVal value As String)
            _prdBlock = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdFloor() As String
        Get
            Return _prdFloor
        End Get
        Set(ByVal value As String)
            _prdFloor = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdAisle() As String
        Get
            Return _prdAisle
        End Get
        Set(ByVal value As String)
            _prdAisle = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdRack() As String
        Get
            Return _prdRack
        End Get
        Set(ByVal value As String)
            _prdRack = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdBin() As String
        Get
            Return _prdBin
        End Get
        Set(ByVal value As String)
            _prdBin = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Property Tagid() As String
        Get
            Return _tagid
        End Get
        Set(ByVal value As String)
            _tagid = clsCommon.funRemove(value, True)
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

#Region "Function"
    ''' <summary>
    '''  Insert PrdQuantity
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertPrdQuantity() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO prdquantity(prdID, prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty, prdTaxCode, prdBlock, prdFloor, prdAisle, prdRack, prdBin ,tagid) VALUES("
        strSQL += "'" + _prdID + "','"
        strSQL += _prdWhsCode + "','"
        strSQL += _prdOhdQty + "','"
        strSQL += _prdQuoteRsv + "','"
        strSQL += _prdSORsv + "','"
        strSQL += _prdDefectiveQty + "','"
        strSQL += _prdTaxCode + "','"
        strSQL += _prdBlock + "','"
        strSQL += _prdFloor + "','"
        strSQL += _prdAisle + "','"
        strSQL += _prdRack + "','"
        strSQL += _prdBin + "','"
        strSQL += _tagid + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim objDataClass As New clsDataClass
        Dim intData As Int16 = 0
        intData = objDataClass.PopulateData(strSQL)
        objDataClass.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update PrdQuantity
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdatePrdQuantity() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE prdquantity set "
        strSQL += "prdID='" + _prdID + "', "
        strSQL += "prdWhsCode='" + _prdWhsCode + "', "
        strSQL += "prdOhdQty='" + _prdOhdQty + "', "
        strSQL += "prdQuoteRsv='" + _prdQuoteRsv + "', "
        strSQL += "prdSORsv='" + _prdSORsv + "', "
        strSQL += "prdDefectiveQty='" + _prdDefectiveQty + "',"
        strSQL += "prdTaxCode='" + _prdTaxCode + "',"
        strSQL += "prdBlock='" + _prdBlock + "', "
        strSQL += "prdFloor='" + _prdFloor + "', "
        strSQL += "prdAisle='" + _prdAisle + "', "
        strSQL += "prdRack='" + _prdRack + "', "
        strSQL += "prdBin='" + _prdBin + "'"
        strSQL += " where id='" + _id + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim intData As Int16 = 0
        Dim objDataClass As New clsDataClass
        intData = objDataClass.PopulateData(strSQL)
        objDataClass.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update PrdQuantity
    ''' </summary>
    ''' <param name="OnlyQuantity"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdatePrdQuantity(ByVal OnlyQuantity As Boolean) As Boolean
        Dim strSQL As String
        strSQL = "UPDATE prdquantity set "
        strSQL += "prdID='" + _prdID + "', "
        strSQL += "prdWhsCode='" + _prdWhsCode + "', "
        strSQL += "prdOhdQty='" + _prdOhdQty + "' "
        strSQL += " where id='" + _id + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim intData As Int16 = 0
        intData = PopulateData(strSQL)
        CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Find Duplicate Product Name
    ''' </summary>
    ''' <param name="strProdID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDuplicateWhs(Optional ByVal strProdID As String = "") As Boolean
        Dim strSQL As String
        If strProdID <> "" Then
            strSQL = "SELECT count(*) FROM prdquantity where prdWhsCode='" & _prdWhsCode & "' And prdID='" & _prdID & "' and id <>'" & strProdID & "' "
        Else
            strSQL = "SELECT count(*) FROM prdquantity where prdWhsCode='" & _prdWhsCode & "' And prdID='" & _prdID & "'"
        End If
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Populate objects of PrdQuantity
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetPrdPrdQuantity()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  id, prdID, prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty,prdTaxCode,prdBlock, prdFloor, prdAisle, prdRack, prdBin FROM prdquantity where id='" + _id + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _prdID = drObj.Item("prdID").ToString
            _prdWhsCode = drObj.Item("prdWhsCode").ToString
            _prdOhdQty = drObj.Item("prdOhdQty").ToString
            _prdQuoteRsv = drObj.Item("prdQuoteRsv").ToString
            _prdSORsv = drObj.Item("prdSORsv").ToString
            _prdDefectiveQty = drObj.Item("prdDefectiveQty").ToString
            _prdTaxCode = drObj.Item("prdTaxCode").ToString
            _prdBlock = drObj.Item("prdBlock").ToString
            _prdFloor = drObj.Item("prdFloor").ToString
            _prdAisle = drObj.Item("prdAisle").ToString
            _prdRack = drObj.Item("prdRack").ToString
            _prdBin = drObj.Item("prdBin").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' sub Fill Product Quantity
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subFillPrdQuantity() As String
        Dim strSql As String = "SELECT id,prdID, prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty ,concat(prdWhsCode,':',WarehouseDescription) as WarehouseDescription,sysTaxCodeDescText,prdDefectiveQty FROM prdquantity"
        strSql += " Left join systaxcodedesc as T on T.sysTaxCodeDescID=prdquantity.prdTaxCode"
        strSql += " inner join syswarehouses as w on w.WarehouseCode=prdquantity.prdWhsCode where prdID='" & HttpContext.Current.Request.QueryString("prdID") & "' and WarehouseActive='1'"
        Return strSql
    End Function
    ''' <summary>
    ''' sub Product Quantity Delete
    ''' </summary>
    ''' <param name="strID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subPrdQuantityDelete(ByVal strID As String) As String
        Dim strSql As String = "delete from prdquantity where Id ='" & strID & "'"
        Return strSql
    End Function
    ''' <summary>
    ''' Update Prdoduct Quantity
    ''' </summary>
    ''' <param name="QuantityType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateProductQuantity(ByVal QuantityType As String) As Boolean
        Dim strSQL As String
        strSQL = "UPDATE prdquantity set "
        strSQL += "prdID='" + _prdID + "', "
        strSQL += "prdWhsCode='" + _prdWhsCode + "' "
        If QuantityType = "OH" Then
            strSQL += ", prdOhdQty='" + _prdOhdQty + "' "
        ElseIf QuantityType = "QR" Then
            strSQL += ", prdQuoteRsv='" + _prdQuoteRsv + "' "
        ElseIf QuantityType = "SR" Then
            strSQL += ", prdSORsv='" + _prdSORsv + "' "
        ElseIf QuantityType = "DQ" Then
            strSQL += ", prdDefectiveQty='" + _prdDefectiveQty + "' "
        End If
        strSQL += " where id='" + _id + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim objDataClass As New clsDataClass
        Dim intData As Int16 = 0
        intData = objDataClass.PopulateData(strSQL)
        objDataClass.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Check Product Quantity
    ''' </summary>
    ''' <param name="strProdID"></param>
    ''' <param name="strWhsCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckProductQuantity(ByVal strProdID As String, ByVal strWhsCode As String) As Integer
        Dim strSQL, strPrdQty As String
        strSQL = "SELECT (prdohdQty - case isnull(prdDefectiveQty) when 1 then 0 else prdDefectiveQty end) FROM prdquantity where prdID='" & strProdID & "' and prdWhsCode='" & strWhsCode & "' "
        strPrdQty = GetScalarData(strSQL)
        Return strPrdQty
    End Function
    ''' <summary>
    ''' Update POS Quantity in case of Product Kit
    ''' </summary>
    ''' <param name="strPrdID"></param>
    ''' <param name="strWhsCode"></param>
    ''' <param name="strPrdQty"></param>
    ''' <param name="strTranType"></param>
    ''' <remarks></remarks>
    Public Sub subGetPrdKitOnHandQty(ByVal strPrdID As String, ByVal strWhsCode As String, ByVal strPrdQty As String, Optional ByVal strTranType As String = "")
        Dim strSQL, strPrdKitQty As String
        Dim drObjkIT As OdbcDataReader
        strSQL = "SELECT  prdIncludeID, prdIncludeQty FROM productkits as pk Left join prdQuantity on prdQuantity.PrdID=pk.prdID where pk.prdID='" + strPrdID + "' And prdWhsCode='" & strWhsCode & "' "
        drObjkIT = GetDataReader(strSQL)
        While drObjkIT.Read
            strPrdKitQty = (drObjkIT.Item("prdIncludeQty").ToString) * strPrdQty
            If updateProductQuantity(drObjkIT.Item("prdIncludeID").ToString, strWhsCode, strPrdKitQty, strTranType) Then
            End If
        End While
        CloseDatabaseConnection()
        drObjkIT.Close()
    End Sub
    ''' <summary>
    ''' Update POS Quantity
    ''' </summary>
    ''' <param name="strProdID"></param>
    ''' <param name="strWhsCode"></param>
    ''' <param name="strPrdPOSQty"></param>
    ''' <param name="strTranType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateProductQuantity(ByVal strProdID As String, ByVal strWhsCode As String, ByVal strPrdPOSQty As String, Optional ByVal strTranType As String = "") As Boolean
        Dim strSQL, strPrdQty As String
        Dim objDataClass As New clsDataClass
        strSQL = "SELECT prdohdQty FROM prdquantity where prdID='" & strProdID & "' and prdWhsCode='" & strWhsCode & "' "
        strPrdQty = objDataClass.GetScalarData(strSQL)
        _prdID = strProdID
        _prdWhsCode = strWhsCode
        subGetProductQuantity()
        If _prdTaxCode = "" Then
            _prdTaxCode = "sysNull"
        End If
        If strTranType = "" Then
            _prdOhdQty = CDbl(strPrdQty) - CDbl(strPrdPOSQty)
        Else
            _prdOhdQty = CDbl(strPrdQty) + CDbl(strPrdPOSQty)
        End If

        If _prdQuoteRsv = "" Then
            _prdQuoteRsv = "sysNull"
        End If
        If _prdSORsv = "" Then
            _prdSORsv = "sysNull"
        End If
        If _prdDefectiveQty = "" Then
            _prdDefectiveQty = "sysNull"
        End If
        funUpdatePrdQuantity()
        Return True
    End Function
    ''' <summary>
    ''' Populate objects of PrdQuantity
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetProductQuantity()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        Dim objDataClass As New clsDataClass
        strSQL = "SELECT  id, prdID, prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty,prdTaxCode,prdBlock, prdFloor, prdAisle, prdRack, prdBin FROM prdquantity where prdID='" + _prdID + "' and prdWhsCode='" & _prdWhsCode & "' "
        drObj = objDataClass.GetDataReader(strSQL)
        While drObj.Read
            _id = drObj.Item("id").ToString
            _prdID = drObj.Item("prdID").ToString
            _prdWhsCode = drObj.Item("prdWhsCode").ToString
            _prdOhdQty = drObj.Item("prdOhdQty").ToString
            _prdQuoteRsv = drObj.Item("prdQuoteRsv").ToString
            _prdSORsv = drObj.Item("prdSORsv").ToString
            _prdDefectiveQty = drObj.Item("prdDefectiveQty").ToString
            _prdTaxCode = drObj.Item("prdTaxCode").ToString
            _prdBlock = drObj.Item("prdBlock").ToString
            _prdFloor = drObj.Item("prdFloor").ToString
            _prdAisle = drObj.Item("prdAisle").ToString
            _prdRack = drObj.Item("prdRack").ToString
            _prdBin = drObj.Item("prdBin").ToString
        End While
        objDataClass.CloseDatabaseConnection()
        drObj.Close()
    End Sub
    ''' <summary>
    ''' get Product list that belong to kit
    ''' </summary>
    ''' <param name="strPrdID"></param>
    ''' <param name="strWhsValue"></param>
    ''' <param name="strTranType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetReducePrdKitQty(ByVal strPrdID As String, ByVal strWhsValue As String, ByVal strTranType As String) As Boolean
        Dim strSQL As String
        Dim drObjkIT As OdbcDataReader
        strSQL = "SELECT pk.PrdID FROM productkits as pk Left join prdQuantity on prdQuantity.PrdID=pk.prdID where pk.prdIncludeID='" + strPrdID + "' And prdWhsCode='" & strWhsValue & "' "
        drObjkIT = GetDataReader(strSQL)
        While drObjkIT.Read
            funUpdatePrdKit(drObjkIT.Item("PrdID").ToString, strWhsValue, funGetPrdKitOnHandQty(strWhsValue, drObjkIT.Item("PrdID").ToString))
        End While
        CloseDatabaseConnection()
        drObjkIT.Close()
        Return True
    End Function
    ''' <summary>
    ''' Update PrdQuantity
    ''' </summary>
    ''' <param name="strPrdID"></param>
    ''' <param name="strWhsValue"></param>
    ''' <param name="strProKitQty"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdatePrdKit(ByVal strPrdID As String, ByVal strWhsValue As String, ByVal strProKitQty As String) As Boolean
        Dim strSQL As String
        strSQL = "UPDATE prdquantity set "
        strSQL += "prdOhdQty='" + strProKitQty + "'"
        strSQL += " where "
        strSQL += "prdID='" + strPrdID + "'and "
        strSQL += "prdWhsCode='" + strWhsValue + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim intData As Int16 = 0
        Dim objDataClass As New clsDataClass
        intData = objDataClass.PopulateData(strSQL)
        objDataClass.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Get Product Kit On Hand Qty
    ''' </summary>
    ''' <param name="strWhsValue"></param>
    ''' <param name="strPrdID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetPrdKitOnHandQty(ByVal strWhsValue As String, ByVal strPrdID As String) As Double
        Dim objDC As New clsDataClass
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        If strPrdID <> "" Then
            strSQL = "SELECT  prdIncludeID, prdIncludeQty FROM productkits as pk Left join prdQuantity on prdQuantity.PrdID=pk.prdID where pk.prdID='" + strPrdID + "' And prdWhsCode='" & strWhsValue & "' "
        Else
            strSQL = "SELECT  prdIncludeID, prdIncludeQty FROM productkits as pk Left join prdQuantity on prdQuantity.PrdID=pk.prdID where prdWhsCode='" & strWhsValue & "' "
        End If

        drObj = objDC.GetDataReader(strSQL)
        Dim dblI As Double
        Dim dblOnHandQty As Double = 0
        Dim intI As Integer = 0
        While drObj.Read
            dblI = (funGetPrdQty(drObj.Item("prdIncludeID").ToString, strWhsValue)) / (drObj.Item("prdIncludeQty").ToString)
            If intI = 0 Then
                dblOnHandQty = dblI
            End If
            If dblOnHandQty > dblI Then
                dblOnHandQty = dblI
            End If
            intI += 1
        End While
        dblOnHandQty = Math.Floor(dblOnHandQty)
        objDC.CloseDatabaseConnection()
        drObj.Close()
        Return dblOnHandQty
    End Function
    ''' <summary>
    ''' Get Product Qty
    ''' </summary>
    ''' <param name="strPrdID"></param>
    ''' <param name="strWhsValue"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetPrdQty(ByVal strPrdID As String, ByVal strWhsValue As String) As String
        Dim objDC As New clsDataClass
        Dim strSQL, strQTY As String
        'strSQL = "SELECT (prdOhdQty - case isnull(prdSORsv) when 1 then 0 else prdSORsv end ) as prdOhdQty FROM prdquantity where prdID='" & strPrdID & "' And prdWhsCode='" & strWhsValue & "' "
        strSQL = "SELECT prdOhdQty FROM prdquantity where prdID='" & strPrdID & "' And prdWhsCode='" & strWhsValue & "' "
        strQTY = objDC.GetScalarData(strSQL)
        Return strQTY
    End Function
    ''' <summary>
    ''' Get ProductQID for inserting kit
    ''' </summary>
    ''' <param name="strProductID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetProQtyId(ByVal strProductID As String) As Integer
        Dim strSQL, strPQTYID As String
        Dim objDC As New clsDataClass
        strSQL = "SELECT count(*) FROM prdquantity where prdID='" & strProductID & "' "
        strPQTYID = objDC.GetScalarData(strSQL)
        Return strPQTYID
    End Function
    ''' <summary>
    ''' Update ProductQty for inserting kit
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdatePrd() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE prdquantity set "
        strSQL += "prdWhsCode='" + _prdWhsCode + "', "
        strSQL += "prdOhdQty='" + _prdOhdQty + "' "
        strSQL += " where prdID='" + _prdID + "' and prdWhsCode='" + _prdWhsCode + "'"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim intData As Int16 = 0
        intData = PopulateData(strSQL)
        CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update Product Quantity table  for Creating sales order
    ''' </summary>
    ''' <param name="strPrdID"></param>
    ''' <param name="strWhsCode"></param>
    ''' <param name="strPrdQty"></param>
    ''' <param name="strOrderCondition"></param>
    ''' <remarks></remarks>
    Public Sub subGetPrdKitForOrder(ByVal strPrdID As String, ByVal strWhsCode As String, ByVal strPrdQty As String, ByVal strOrderCondition As String)
        Dim strSQL, strPrdKitQty As String
        Dim drObjkIT As OdbcDataReader
        strSQL = "SELECT  prdIncludeID, prdIncludeQty FROM productkits as pk Left join prdQuantity on prdQuantity.PrdID=pk.prdID where pk.prdID='" + strPrdID + "' And prdWhsCode='" & strWhsCode & "' "
        drObjkIT = GetDataReader(strSQL)
        While drObjkIT.Read
            strPrdKitQty = (drObjkIT.Item("prdIncludeQty").ToString) * strPrdQty
            If funUpdateProductQtyFields(drObjkIT.Item("prdIncludeID").ToString, strWhsCode, strPrdKitQty, strOrderCondition) Then
            End If
        End While
        CloseDatabaseConnection()
        drObjkIT.Close()
    End Sub
    ''' <summary>
    ''' Update Product Quantity table for Sales order
    ''' </summary>
    ''' <param name="strProdID"></param>
    ''' <param name="strWhsCode"></param>
    ''' <param name="strPrdPOSQty"></param>
    ''' <param name="strOrderCondition"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateProductQtyFields(ByVal strProdID As String, ByVal strWhsCode As String, ByVal strPrdPOSQty As String, ByVal strOrderCondition As String) As Boolean
        Dim strSQL, strPrdOnhandQty, strQoteRsv, strSORsv As String
        strSQL = ""
        strPrdOnhandQty = ""
        strSORsv = ""
        strQoteRsv = ""
        Dim objDataClass As New clsDataClass
        strOrderCondition = strOrderCondition.ToLower
        'If strOrderCondition = "ohdqty" Then
        '    strSQL = "SELECT prdohdQty "
        'ElseIf strOrderCondition = "quotersv" Then
        '    strSQL = "SELECT prdQuoteRsv "
        'ElseIf strOrderCondition = "sorsv" Then
        '    strSQL = "SELECT prdSORsv "
        'End If

        Dim drObj As OdbcDataReader
        strSQL += "SELECT prdohdQty,prdQuoteRsv,prdSORsv FROM prdquantity where prdID='" & strProdID & "' and prdWhsCode='" & strWhsCode & "' "
        drObj = objDataClass.GetDataReader(strSQL)
        While drObj.Read
            strPrdonhandQty = drObj.Item("prdohdQty").ToString
            strQoteRsv = drObj.Item("prdQuoteRsv").ToString
            strSORsv = drObj.Item("prdSORsv").ToString
        End While
        drObj.Close()

        _prdID = strProdID
        _prdWhsCode = strWhsCode
        subGetProductQuantity()
        If _prdTaxCode = "" Then
            _prdTaxCode = "sysNull"
        End If
        If strOrderCondition = "oh" Then 'condition for onhand quantity
            _prdOhdQty = CDbl(strPrdOnhandQty) - CDbl(strPrdPOSQty)
            _prdSORsv = CDbl(strSORsv) - CDbl(strPrdPOSQty)
        ElseIf strOrderCondition = "qt" Then 'condition for Quotation reserve
            _prdQuoteRsv = CDbl(strQoteRsv) + CDbl(strPrdPOSQty)
        ElseIf strOrderCondition = "sr" Then 'condition for SO Reserve
            _prdSORsv = CDbl(strSORsv) + CDbl(strPrdPOSQty)
            _prdQuoteRsv = CDbl(strQoteRsv) - CDbl(strPrdPOSQty)
        ElseIf strOrderCondition = "canqt" Then 'condition for subtract Quotation reserve 
            _prdQuoteRsv = CDbl(strQoteRsv) - CDbl(strPrdPOSQty)
        End If

        If _prdQuoteRsv = "" Then
            _prdQuoteRsv = "sysNull"
        End If
        If _prdSORsv = "" Then
            _prdSORsv = "sysNull"
        End If
        If _prdDefectiveQty = "" Then
            _prdDefectiveQty = "sysNull"
        End If
        funUpdatePrdQuantity()
        Return True
    End Function
    ''' <summary>
    ''' Populate objects of PrdQuantity By given ProductID and warehouse
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getProductQuantityByPrdIDnWhs()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  id, prdID, prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty,prdTaxCode,prdBlock, prdFloor, prdAisle, prdRack, prdBin FROM prdquantity where prdID='" + _prdID + "' and prdWhsCode='" + _prdWhsCode + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _id = drObj.Item("id").ToString
            _prdID = drObj.Item("prdID").ToString
            _prdWhsCode = drObj.Item("prdWhsCode").ToString
            _prdOhdQty = drObj.Item("prdOhdQty").ToString
            _prdQuoteRsv = drObj.Item("prdQuoteRsv").ToString
            _prdSORsv = drObj.Item("prdSORsv").ToString
            _prdDefectiveQty = drObj.Item("prdDefectiveQty").ToString
            _prdTaxCode = drObj.Item("prdTaxCode").ToString
            _prdBlock = drObj.Item("prdBlock").ToString
            _prdFloor = drObj.Item("prdFloor").ToString
            _prdAisle = drObj.Item("prdAisle").ToString
            _prdRack = drObj.Item("prdRack").ToString
            _prdBin = drObj.Item("prdBin").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
#End Region
End Class

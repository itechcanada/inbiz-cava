Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsPosPaymentType
    Inherits clsDataClass

    Private _posPayTypeID, _posPayTypeDesc, _posPayTypeIDImagePath, _posPayActive As String

#Region "Properties"
    Public Property PayTypeID() As String
        Get
            Return _posPayTypeID
        End Get
        Set(ByVal value As String)
            _posPayTypeID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PayTypeDesc() As String
        Get
            Return _posPayTypeDesc
        End Get
        Set(ByVal value As String)
            _posPayTypeDesc = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PayTypeIDImagePath() As String
        Get
            Return _posPayTypeIDImagePath
        End Get
        Set(ByVal value As String)
            _posPayTypeIDImagePath = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PayActive() As String
        Get
            Return _posPayActive
        End Get
        Set(ByVal value As String)
            _posPayActive = clsCommon.funRemove(value, True)
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

#Region "Function"
    ''' <summary>
    '''  Insert PosPaymentType
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertPosPaymentType() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO pospaymenttype(posPayTypeDesc, posPayTypeIDImagePath, posPayActive) VALUES('"
        strSQL += _posPayTypeDesc + "','"
        strSQL += _posPayTypeIDImagePath + "','"
        strSQL += _posPayActive + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update PosPaymentType
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdatePosPaymentType() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE pospaymenttype set "
        strSQL += "posPayTypeDesc='" + _posPayTypeDesc + "', "
        strSQL += "posPayTypeIDImagePath='" + _posPayTypeIDImagePath + "', "
        strSQL += "posPayActive='" + _posPayActive + "'"
        strSQL += " where posPayTypeID='" + _posPayTypeID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of PosPaymentType
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetPosPaymentType()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT posPayTypeDesc, posPayTypeIDImagePath, posPayActive FROM pospaymenttype where posPayTypeID='" + _posPayTypeID + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _posPayTypeDesc = drObj.Item("posPayTypeDesc").ToString
            _posPayTypeIDImagePath = drObj.Item("posPayTypeIDImagePath").ToString
            _posPayActive = drObj.Item("posPayActive").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
#End Region
End Class

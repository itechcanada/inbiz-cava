Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsProcessGroup
	Inherits clsDataClass
    Private _ProcessID, _ProcessCode, _ProcessDescription, _ProcessFixedCost, _ProcessCostPerHour, _ProcessCostPerUnit As String
	Public Property ProcessID() As String
		Get
			Return _ProcessID
		End Get
		Set(ByVal value As String)
            _ProcessID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ProcessCode() As String
		Get
			Return _ProcessCode
		End Get
		Set(ByVal value As String)
            _ProcessCode = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ProcessDescription() As String
		Get
			Return _ProcessDescription
		End Get
		Set(ByVal value As String)
            _ProcessDescription = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ProcessFixedCost() As String
		Get
			Return _ProcessFixedCost
		End Get
		Set(ByVal value As String)
            _ProcessFixedCost = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ProcessCostPerHour() As String
		Get
			Return _ProcessCostPerHour
		End Get
		Set(ByVal value As String)
            _ProcessCostPerHour = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property ProcessCostPerUnit() As String
        Get
            Return _ProcessCostPerUnit
        End Get
        Set(ByVal value As String)
            _ProcessCostPerUnit = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert ProcessGroup
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertProcessGroup() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO sysprocessgroup( ProcessCode, ProcessDescription, ProcessFixedCost, ProcessCostPerHour, ProcessCostPerUnit) VALUES('"
		strSQL += _ProcessCode + "','"
		strSQL += _ProcessDescription + "','"
		strSQL += _ProcessFixedCost + "','"
        strSQL += _ProcessCostPerHour + "','"
        strSQL += _ProcessCostPerUnit + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update ProcessGroup
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateProcessGroup() As Boolean
		Dim strSQL As String
        strSQL = "UPDATE sysprocessgroup set "
		strSQL += "ProcessCode='" + _ProcessCode + "', "
		strSQL += "ProcessDescription='" + _ProcessDescription + "', "
		strSQL += "ProcessFixedCost='" + _ProcessFixedCost + "', "
        strSQL += "ProcessCostPerHour='" + _ProcessCostPerHour + "', "
        strSQL += "ProcessCostPerUnit='" + _ProcessCostPerUnit + "'"
		strSQL += " where ProcessID='" + _ProcessID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of ProcessGroup
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getProcessGroupInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
        strSQL = "SELECT  ProcessCode, ProcessDescription, ProcessFixedCost, ProcessCostPerHour, ProcessCostPerUnit FROM sysprocessgroup where ProcessID='" + _ProcessID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_ProcessCode = drObj.Item("ProcessCode").ToString
			_ProcessDescription = drObj.Item("ProcessDescription").ToString
			_ProcessFixedCost = drObj.Item("ProcessFixedCost").ToString
            _ProcessCostPerHour = drObj.Item("ProcessCostPerHour").ToString
            _ProcessCostPerUnit = drObj.Item("ProcessCostPerUnit").ToString
		End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Check Duplicate Process Code
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateProcessCode() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM sysprocessgroup where ProcessCode='" & _ProcessCode & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Check Duplicate Process Description
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateProcessDescription() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM sysprocessgroup where "
        strSQL += "ProcessDescription='" & _ProcessDescription & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    '''  Delete Process
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteProcess(ByVal DeleteID As String) As String
        Dim strSQL As String
        strSQL = "DELETE FROM sysprocessgroup WHERE ProcessID='" + DeleteID + "' "
        Return strSQL
    End Function
    ''' <summary>
    '''  Fill Grid of User
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        strSQL = "SELECT ProcessID, ProcessCode, ProcessDescription, ProcessFixedCost, ProcessCostPerHour, ProcessCostPerUnit FROM sysprocessgroup "
        strSQL += "order by ProcessDescription "
        Return strSQL
    End Function
    ''' <summary>
    '''  Fill Grid of Process with search criteria
    ''' </summary>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal SearchData As String) As String
        Dim strSQL As String
        SearchData = clsCommon.funRemove(SearchData)
        strSQL = "SELECT ProcessID, ProcessCode, ProcessDescription, ProcessFixedCost, ProcessCostPerHour, ProcessCostPerUnit FROM sysprocessgroup "
        If SearchData <> "" Then
            strSQL += " where "
            strSQL += " ("
            strSQL += " ProcessCode like '%" + SearchData + "%' or "
            strSQL += " ProcessDescription like '%" + SearchData + "%' "
            strSQL += " )"
        End If
        strSQL += " order by ProcessDescription "
        Return strSQL
    End Function
End Class

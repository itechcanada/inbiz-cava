Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data.Odbc
Imports System.Data
Imports System.IO
Imports System.Xml
Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Collections.Generic
Imports System.Collections
Imports System.Web.Script.Services
Imports iTECH.Library.Utilities
Imports iTECH.InbizERP.BusinessLogic

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class POSService
    Inherits System.Web.Services.WebService
    Dim objCommon As New clsCommon
    Private objDataClass As New clsDataClass
    Private objProduct As New clsProducts
    Public Function funAddXMLTag(ByVal strXMLData As String) As String
        Dim strData As String
        strData = "<?xml version=""1.0"" encoding=""utf-8""?>"
        strData += strXMLData
        Return strData
    End Function
    'Get Product Information
    <WebMethod(EnableSession:=True)> _
    Public Function subGetProductsArray(ByVal strTokan As String, ByVal strUserID As String, ByVal strLang As String, ByVal strWhsHouse As String, ByVal strBarcode As String, ByVal strRegcode As String, ByVal strLaywayOrderID As String) As List(Of String)
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        Dim ObjCommon As New clsCommon
        Dim objDB As New clsDataClass
        Dim strArr As New Hashtable
        Dim objRecItemsTags As New clsPrdTag()
        Dim list As New List(Of String)

        Try
            Dim strXML As New StringBuilder
            Dim strValue, StrNewToken As String
            If strBarcode <> "" Then
                strBarcode = Replace(strBarcode, "'", "''").Trim()
                strBarcode = Replace(strBarcode, "\", "\\")
            End If
            If funGetToken(strTokan, strUserID, StrNewToken) = False Then
                list.Add("POSInvalidTokenNo")
                Return list
            End If
            strSQL = "SELECT distinct products.productID,  prdUPCCode,  CASE prdIsNonStandard WHEN 1 THEN  Products.PrdName ELSE CONCAT_WS('<br/>',Products.PrdName,CONCAT_WS(' ', pcol.Color" & strLang & ",psz.Size" & strLang & ") ) END as prdname,prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty,prdEndUserSalesPrice,prdIsKit,prdSmallImagePath, prdLargeImagePath,prdTaxCode, prdIsGiftCardProduct, prdIsNonStandard FROM products "
            strSQL += " Left join prdQuantity on prdQuantity.PrdID=Products.ProductID "
            strSQL += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID "
            strSQL += " left join prdImages on prdImages.PrdID=Products.ProductID  "
            strSQL += " INNER JOIN ProductClothDesc AS pCd on pCd.ProductID = Products.productID "
            strSQL += " LEFT OUTER JOIN productcolor pcol on pcol.colorID = pCd.Color "
            strSQL += " LEFT OUTER JOIN productsize psz on psz.sizeID = pCd.Size "
            strSQL += " where descLang ='" & strLang & "'  and prdIsActive=1 " 'and prdWhsCode='" & strWhsHouse & "'
            If strBarcode <> "" Then
                strSQL += " and prdUPCCode ='" & strBarcode & "'"
                strSQL += " or products.productID IN (SELECT ProductID FROM prdtags WHERE Tag = '" & strBarcode & "')"
                strSQL += " group by products.ProductID order by Pdes.prdname Asc"
            ElseIf strBarcode = "" And BusinessUtility.GetInt(strLaywayOrderID) > 0 Then
                strSQL += " AND products.productID IN (select ordProductID from orderItems where ordID = " & BusinessUtility.GetInt(strLaywayOrderID) & ")"
                strSQL += " group by products.ProductID order by Pdes.prdname Asc limit 250" '
            ElseIf Session("SavedProductLst") IsNot Nothing Then
                strSQL += " AND products.productID IN ( " & BusinessUtility.GetString(Session("SavedProductLst")) & ")"
                strSQL += " group by products.ProductID order by Pdes.prdname Asc limit 250" '
            Else
                strSQL += " group by products.ProductID order by Pdes.prdname Asc limit 250" '
            End If
            drObj = objDB.GetDataReader(strSQL)
            While drObj.Read
                'list.Add(drObj.Item("productID").ToString & "~" & ObjCommon.replaceSpecialCh(drObj.Item("prdName").ToString) & "~" & drObj.Item("prdUPCCode").ToString & "~" & drObj.Item("prdOhdQty").ToString & "~" & drObj.Item("prdQuoteRsv").ToString & "~" & drObj.Item("prdSORsv").ToString & "~" & drObj.Item("prdEndUserSalesPrice").ToString & "~" & drObj.Item("prdIsKit").ToString & "~" & drObj.Item("prdSmallImagePath").ToString & "__taxGrp__" & funTax(drObj.Item("productID").ToString, drObj.Item("prdEndUserSalesPrice").ToString, strWhsHouse, strRegcode))

                'list.Add(drObj.Item("productID").ToString & "~" & ObjCommon.replaceSpecialCh(drObj.Item("prdName").ToString) & "~" & drObj.Item("prdUPCCode").ToString & "~" & drObj.Item("prdOhdQty").ToString & "~" & drObj.Item("prdQuoteRsv").ToString & "~" & drObj.Item("prdSORsv").ToString & "~" & drObj.Item("prdEndUserSalesPrice").ToString & "~" & drObj.Item("prdIsKit").ToString & "~" & drObj.Item("prdSmallImagePath").ToString & "__taxGrp__" & objProduct.funTaxCodeDetail(drObj.Item("prdTaxCode").ToString, strWhsHouse, strRegcode))
                Dim sb As New StringBuilder()
                sb.Append(drObj.Item("productID").ToString)
                sb.Append("~")
                sb.Append(ObjCommon.replaceSpecialCh(drObj.Item("prdName").ToString))
                sb.Append("~")
                sb.Append(drObj.Item("prdUPCCode").ToString)
                sb.Append("~")
                sb.Append(drObj.Item("prdOhdQty").ToString)
                sb.Append("~")
                sb.Append(drObj.Item("prdQuoteRsv").ToString)
                sb.Append("~")
                sb.Append(drObj.Item("prdSORsv").ToString)
                sb.Append("~")
                sb.Append(drObj.Item("prdEndUserSalesPrice").ToString)
                sb.Append("~")
                sb.Append(drObj.Item("prdIsKit").ToString)
                sb.Append("~")
                sb.Append(drObj.Item("prdSmallImagePath").ToString)
                sb.Append("~")
                sb.Append(drObj.Item("prdIsGiftCardProduct").ToString)
                sb.Append("~")
                sb.Append(drObj.Item("prdIsNonStandard").ToString)  ' 10

                'Append Tax Groups..
                sb.Append("__taxGrp__")

                If strBarcode = "" And BusinessUtility.GetInt(strLaywayOrderID) > 0 Then
                    Dim objOrdItem As New OrderItems
                    'objOrdItem.GetOrderItemList(BusinessUtility.GetInt(strLaywayOrderID), BusinessUtility.GetInt(drObj.Item("productID"))
                    Dim lItems As List(Of OrderItems) = New OrderItems().GetOrderItemList(BusinessUtility.GetInt(strLaywayOrderID), BusinessUtility.GetInt(drObj.Item("productID")))
                    If lItems.Count > 0 Then
                        sb.Append(objProduct.funTaxCodeDetail(BusinessUtility.GetString(lItems(0).OrdProductTaxGrp), strWhsHouse, strRegcode))
                    Else
                        sb.Append(objProduct.funTaxCodeDetail(drObj.Item("prdTaxCode").ToString, strWhsHouse, strRegcode))
                    End If
                ElseIf Session("SavedTaxLst") IsNot Nothing Then
                    Dim savedPTaxDetail As List(Of Product.ProductTaxDetail) = CType(Session.Item("SavedTaxLst"), List(Of Product.ProductTaxDetail))
                    Dim itemtoUpdate = savedPTaxDetail.Where(Function(i) i.ProductID = BusinessUtility.GetInt(drObj.Item("productID")))
                    Dim item As Product.ProductTaxDetail = itemtoUpdate(0)
                    If BusinessUtility.GetInt(item.ProductID) > 0 Then
                        sb.Append(objProduct.funTaxCodeDetail(BusinessUtility.GetString(item.TaxID), strWhsHouse, strRegcode))
                    Else
                        sb.Append(objProduct.funTaxCodeDetail(drObj.Item("prdTaxCode").ToString, strWhsHouse, strRegcode))
                    End If
                Else
                    sb.Append(objProduct.funTaxCodeDetail(drObj.Item("prdTaxCode").ToString, strWhsHouse, strRegcode))
                End If

                'Append Tags for product..
                sb.Append("__tags__")
                sb.Append(objRecItemsTags.GetTagsByProduct(drObj.Item("productID").ToString))

                'Add string to list
                list.Add(sb.ToString())
            End While
            If list.Count = 0 Then
                list.Add("POSNoProductfound")
            End If
            list.Add("__token__" + StrNewToken)
            drObj.Close()
            drObj = Nothing
            ObjCommon = Nothing
            objDB.CloseDatabaseConnection()
            Return list
        Catch ex As Exception
            list.Add(("__error__" & "msgBlankProductArray"))
            Return list
        Finally
            drObj = Nothing
            ObjCommon = Nothing
            objDB.CloseDatabaseConnection()
            objDB = Nothing
        End Try
    End Function

    'Get Product Information by TagID
    <WebMethod()> _
    Public Function subGetProductsArrayByTag(ByVal strTokan As String, ByVal strUserID As String, ByVal strLang As String, ByVal strWhsHouse As String, ByVal strBarcode As String, ByVal strRegcode As String) As List(Of String)
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        Dim ObjCommon As New clsCommon
        Dim objDB As New clsDataClass
        Dim strArr As New Hashtable
        Dim objRecItemsTags As New clsPrdTag()
        Dim list As New List(Of String)
        Try
            Dim strXML As New StringBuilder
            Dim strValue, StrNewToken As String
            If strBarcode <> "" Then
                strBarcode = Replace(strBarcode, "'", "''").Trim()
                strBarcode = Replace(strBarcode, "\", "\\")
            End If
            If funGetToken(strTokan, strUserID, StrNewToken) = False Then
                list.Add("POSInvalidTokenNo")
                Return list
            End If
            strSQL = "SELECT distinct productID,  prdUPCCode,  Pdes.prdname as prdname,prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty,prdEndUserSalesPrice,prdIsKit,prdSmallImagePath, prdLargeImagePath,prdTaxCode, prdIsGiftCardProduct FROM products "
            strSQL += " Left join prdQuantity on prdQuantity.PrdID=Products.ProductID "
            strSQL += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID "
            strSQL += " left join prdImages on prdImages.PrdID=Products.ProductID  "
            strSQL += " where descLang ='" & strLang & "' and prdWhsCode='" & strWhsHouse & "' and prdIsActive=1 "
            If strBarcode <> "" Then
                strSQL += " and productID IN (SELECT ProductID FROM prdtags WHERE Tag = '" & strBarcode & "')"
                strSQL += " group by ProductID order by Pdes.prdname Asc"
            Else
                strSQL += " group by ProductID order by Pdes.prdname Asc limit 250"
            End If
            drObj = objDB.GetDataReader(strSQL)
            While drObj.Read
                'list.Add(drObj.Item("productID").ToString & "~" & ObjCommon.replaceSpecialCh(drObj.Item("prdName").ToString) & "~" & drObj.Item("prdUPCCode").ToString & "~" & drObj.Item("prdOhdQty").ToString & "~" & drObj.Item("prdQuoteRsv").ToString & "~" & drObj.Item("prdSORsv").ToString & "~" & drObj.Item("prdEndUserSalesPrice").ToString & "~" & drObj.Item("prdIsKit").ToString & "~" & drObj.Item("prdSmallImagePath").ToString & "__taxGrp__" & funTax(drObj.Item("productID").ToString, drObj.Item("prdEndUserSalesPrice").ToString, strWhsHouse, strRegcode))

                'list.Add(drObj.Item("productID").ToString & "~" & ObjCommon.replaceSpecialCh(drObj.Item("prdName").ToString) & "~" & drObj.Item("prdUPCCode").ToString & "~" & drObj.Item("prdOhdQty").ToString & "~" & drObj.Item("prdQuoteRsv").ToString & "~" & drObj.Item("prdSORsv").ToString & "~" & drObj.Item("prdEndUserSalesPrice").ToString & "~" & drObj.Item("prdIsKit").ToString & "~" & drObj.Item("prdSmallImagePath").ToString & "__taxGrp__" & objProduct.funTaxCodeDetail(drObj.Item("prdTaxCode").ToString, strWhsHouse, strRegcode))
                Dim sb As New StringBuilder()
                sb.Append(drObj.Item("productID").ToString)
                sb.Append("~")
                sb.Append(ObjCommon.replaceSpecialCh(drObj.Item("prdName").ToString))
                sb.Append("~")
                sb.Append(drObj.Item("prdUPCCode").ToString)
                sb.Append("~")
                sb.Append(drObj.Item("prdOhdQty").ToString)
                sb.Append("~")
                sb.Append(drObj.Item("prdQuoteRsv").ToString)
                sb.Append("~")
                sb.Append(drObj.Item("prdSORsv").ToString)
                sb.Append("~")
                sb.Append(drObj.Item("prdEndUserSalesPrice").ToString)
                sb.Append("~")
                sb.Append(drObj.Item("prdIsKit").ToString)
                sb.Append("~")
                sb.Append(drObj.Item("prdSmallImagePath").ToString)
                sb.Append("~")
                sb.Append(drObj.Item("prdIsGiftCardProduct").ToString)
                'Append Tax Groups..
                sb.Append("__taxGrp__")
                sb.Append(objProduct.funTaxCodeDetail(drObj.Item("prdTaxCode").ToString, strWhsHouse, strRegcode))

                'Append Tags for product..
                sb.Append("__tags__")
                sb.Append(objRecItemsTags.GetTagsByProduct(drObj.Item("productID").ToString))

                'Add string to list
                list.Add(sb.ToString())
            End While
            If list.Count = 0 Then
                list.Add("POSNoProductfound")
            End If
            list.Add("__token__" + StrNewToken)
            drObj.Close()
            drObj = Nothing
            ObjCommon = Nothing
            objDB.CloseDatabaseConnection()
            Return list
        Catch ex As Exception
            list.Add(("__error__" & "msgBlankProductArray"))
            Return list
        Finally
            drObj = Nothing
            ObjCommon = Nothing
            objDB.CloseDatabaseConnection()
            objDB = Nothing
        End Try
    End Function

    'Calculate Tax
    Public Function funTax(ByVal strProductID As String, ByVal strTotalAmount As String, ByVal strWhsHouse As String, ByVal strRegcode As String) As String
        Dim strTaxArr As ArrayList
        Dim strValue As String
        Dim objPrint As New clsPrint
        ' Dim strAmount() As String = strTotalAmount.Split("~")
        Dim strTaxArray(,) As String
        strTaxArr = objPrint.funCalcTax(strProductID, strWhsHouse, strTotalAmount, "POS", , , , strRegcode)
        Dim strTaxItem As String
        Dim intI As Integer = 0
        While strTaxArr.Count - 1 >= intI
            strTaxItem = strTaxArr.Item(intI).ToString
            strTaxArray = objPrint.funAddTax(strTaxArray, strTaxItem.Substring(0, strTaxArr.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTaxArr.Item(intI).IndexOf("@,@") + 3)))
            intI += 1
        End While
        Dim i As Integer = 0
        Dim j As Integer = 0
        If Not strTaxArray Is Nothing Then
            j = strTaxArray.Length / 2
        End If
        Dim dblTax As String = ""
        While i < j
            dblTax += strTaxArray(0, i) & "~" & strTaxArray(1, i) & "^"
            i += 1
        End While
        'strValue = funAddXMLTag(dblTax)
        Return dblTax
    End Function

    'Generates Tokan 
    Public Function funGetToken(ByVal strPreTokan As String, ByVal strUserID As String, ByRef StrNewToken As String) As Boolean
        Dim strToken As String
        Dim ObjCommon As New clsCommon
        Dim strSQL As String
        Dim objDC As New clsDataClass
        strToken = Date.Now.Ticks & ObjCommon.funRandomString()
        Dim strData As String
        strSQL = "select count(*) from Postoken where tokenID='" & strPreTokan & "' and tokenUser='" & strUserID & "' and tokenStatus='1' and adddate(tokenTime,INTERVAL 20 Minute) > now()"
        strData = objDC.GetScalarData(strSQL)
        If strData = 1 Then
            strSQL = "select tokenNo from Postoken where tokenID='" & strPreTokan & "' and tokenUser='" & strUserID & "' and tokenStatus='1'"
            strData = objDC.GetScalarData(strSQL)
            strToken = strData & "-" & strToken
            strSQL = "Update Postoken set tokenId='" & strToken & "' where tokenNo='" & strData & "' "
            objDC.PopulateData(strSQL)
            StrNewToken = strToken
            objDC.CloseDatabaseConnection()
            objDC = Nothing
            Return True
        Else
            Return False
        End If
    End Function
    'Get the Kit Items
    <WebMethod()> _
    Public Function funGetKitItem(ByVal strTokan As String, ByVal strUserID As String, ByVal strLang As String, ByVal strProductID As String) As String
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        Dim ObjCommon As New clsCommon
        Dim objDB As New clsDataClass
        Dim strArr As New Hashtable
        Try
            Dim strXML As New StringBuilder
            Dim strValue, StrNewToken As String
            If funGetToken(strTokan, strUserID, StrNewToken) = False Then
                strXML.Append("POSInvalidTokenNo")
                strValue = funAddXMLTag(strXML.ToString)
                Return strValue
            End If
            strSQL = "select prdKitID,prdID,prdIncludeID,prdIncludeQty,pd.prdName from productkits "
            strSQL += " Inner Join prddescriptions as pd on pd.id=productkits.prdIncludeID where prdID ='" & strProductID & "' and descLang='" & strLang & "'"
            drObj = objDB.GetDataReader(strSQL)
            While drObj.Read
                strXML.Append(drObj.Item("prdIncludeID").ToString & "~" & ObjCommon.replaceSpecialCh(drObj.Item("prdName").ToString) & "~" & drObj.Item("prdIncludeQty").ToString & "__item__")
            End While
            Dim strItem As String = "__item__"
            If strXML.ToString <> "" Then
                strValue = strXML.ToString
                'strValue = strXML.ToString.Substring(0, strXML.ToString.Length - strItem.Length)
            Else
                strValue = POSNoProductfound
            End If
            strValue += "__token__" & StrNewToken
            drObj.Close()
            drObj = Nothing
            ObjCommon = Nothing
            objDB.CloseDatabaseConnection()
            strValue = funAddXMLTag(strValue)
            Return strValue
        Catch ex As Exception
            Return "__error__" & "POSNoProductfound"
        Finally
            drObj = Nothing
            ObjCommon = Nothing
            objDB.CloseDatabaseConnection()
            objDB = Nothing
        End Try
    End Function
    'Generates TransactionID 
    <WebMethod()> _
    Public Function funGetTransactionID(ByVal strTokan As String, ByVal strUserID As String, ByVal strRegCode As String, ByVal strTax As String, ByVal strTotalAmt As String, ByVal strTotalSubAmt As String, ByVal strTransType As String, ByVal strReturnAmt As String, ByVal strReceivedAmt As String, ByVal strPreviousTranID As String) As String
        Dim objTransaction As New clsPOSTransaction
        Dim objPosPrdNo As New clsPosProductNo
        Dim objPosTranAccHrd As New clsPosTransAccHdr
        Dim objTransDtl As New clsTransactionDtl
        Dim objProduct As New clsProducts
        Dim objPrdQty As New clsPrdQuantity
        Dim strTranID, StrNewToken, strvalue As String
        Dim strTax1(), strTax2(), strTax3(), strTax4() As String
        Try
            If funGetTokenForTransaction(strTokan, strUserID, StrNewToken) = False Then
                strvalue = "POSInvalidTokenNo"
                strvalue = funAddXMLTag(strvalue)
                Return strvalue
            End If
            objTransaction.Tax4 = 0
            objTransaction.Tax3 = 0
            objTransaction.Tax2 = 0
            objTransaction.Tax1 = 0
            Dim strTaxCal() As String = strTax.Split("^")
            If strTaxCal.Length >= 1 Then
                strTax1 = strTaxCal(0).Split("~")
                If strTax1.Length > 1 Then
                    objTransaction.Tax1 = strTax1(1)
                    objTransaction.Tax1Desc = strTax1(0)
                End If
            End If
            If strTaxCal.Length >= 2 Then
                strTax2 = strTaxCal(1).Split("~")
                If strTax2.Length > 1 Then
                    objTransaction.Tax2 = strTax2(1)
                    objTransaction.Tax2Desc = strTax2(0)
                End If
            End If
            If strTaxCal.Length >= 3 Then
                strTax3 = strTaxCal(2).Split("~")
                If strTax3.Length > 1 Then
                    objTransaction.Tax3 = strTax3(1)
                    objTransaction.Tax3Desc = strTax3(0)
                End If
            End If
            If strTaxCal.Length >= 4 Then
                strTax4 = strTaxCal(3).Split("~")
                If strTax4.Length > 1 Then
                    objTransaction.Tax4 = strTax4(1)
                    objTransaction.Tax4Desc = strTax4(0)
                End If
            End If

            objTransDtl.posTotalValue = strTotalAmt

            If strTransType = 2 Then
                objPosTranAccHrd.AcctPayType = 2
            ElseIf strTransType = 3 Then
                objPosTranAccHrd.AcctPayType = 3
            ElseIf strTransType = 4 Then
                objPosTranAccHrd.AcctPayType = 4
            ElseIf strTransType = 5 Then
                objPosTranAccHrd.AcctPayType = 5
            End If


            objPosTranAccHrd.CashReturned = 0
            objPosTranAccHrd.CasReceived = strReceivedAmt

            objTransaction.TransUserID = strUserID
            objTransaction.TransRegCode = strRegCode
            objTransaction.TransType = "S"
            objTransaction.TransDateTime = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
            objTransaction.TotalValue = strTotalAmt
            objTransaction.SubTotal = strTotalSubAmt
            objTransDtl.posTotalValue = strTotalAmt
            objTransaction.OldTransID = "sysNull"
            objTransaction.TransStatus = 0

            Dim strTranAccHrdID As String
            Dim strPosAccDtlId As String

            '***************************** Insert in Transaction Table
            objTransaction.funInsertPosTransaction(strTranID)
            objPosTranAccHrd.AcctTransID = strTranID

            '***************************** Insert in postransaccthdr Table

            objPosTranAccHrd.funInsertpostransaccthdr(strTranAccHrdID)

            '***************************** Insert in postransacctdtl Table

            If strTransType = 2 Or strTransType = 3 Or strTransType = 4 Or strTransType = 5 Then
                objTransDtl.posAcctHdrDtlId = strTranAccHrdID
                objTransDtl.posTransEndTime = "sysNull"
                objTransDtl.posTransID = "sysNull"
                objTransDtl.posTranAcctType = "sale"
                objTransDtl.posTransStartDatetime = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
                objTransDtl.posTransAcctDtlStatus = 0
                objTransDtl.funInsertPosTransaction(strPosAccDtlId)
            End If
            '**************************** Condition for Tab
            If strPreviousTranID <> "" Then
                objTransaction.TransStatus = 0
                objTransaction.funUpdateTransStaus(strPreviousTranID)
            End If
            strvalue = strTranID & "~" & strTranAccHrdID & "~" & strPosAccDtlId & "__token__" & StrNewToken
            strvalue = funAddXMLTag(strvalue)
            Return strvalue
        Catch ex As Exception
            Return "__error__" & "POSNoTransactionIDfound"
        Finally
            objTransaction = Nothing
            objPosPrdNo = Nothing
            objPosTranAccHrd = Nothing
            objTransDtl = Nothing
            objProduct = Nothing
            objPrdQty = Nothing
        End Try
    End Function
    'Cash Transaction
    <WebMethod()> _
    Public Function funCashTransaction(ByVal LangsID As String, ByVal strTokan As String, ByVal strUserID As String, ByVal strRegCode As String, ByVal strTax As String, ByVal strTotalAmt As String, ByVal strTotalSubAmt As String, ByVal strTransType As String, ByVal strReturnAmt As String, ByVal strReceivedAmt As String, ByVal strWhshouse As String, ByVal strProductDtl As String, ByVal strBtnType As String, ByVal strPreviousTranID As String, ByVal strTabNote As String, ByVal giftReason As String, ByVal authorizedBy As String) As String
        Dim objTransaction As New clsPOSTransaction
        Dim objPosPrdNo As New clsPosProductNo
        Dim objPosTranAccHrd As New clsPosTransAccHdr
        Dim objTransDtl As New clsTransactionDtl
        Dim objProduct As New clsProducts
        Dim objPrdQty As New clsPrdQuantity
        Dim strTranID, StrNewToken, strvalue As String
        strvalue = String.Empty
        Dim strTax1(), strTax2(), strTax3(), strTax4() As String
        Dim item() As String = {"__item__"}
        Dim tax() As String = {"__taxGrp__"}
        Try
            If funCloseTansactionToken(strTokan, strUserID, "1") = False Then
                strvalue = "POSInvalidTokenNo"
                strvalue = funAddXMLTag(strvalue)
                Return strvalue
            End If
            objTransaction.Tax4 = 0
            objTransaction.Tax3 = 0
            objTransaction.Tax2 = 0
            objTransaction.Tax1 = 0
            Dim strTaxCal() As String = strTax.Split("^")
            If strTaxCal.Length >= 1 Then
                strTax1 = strTaxCal(0).Split("~")
                If strTax1.Length > 1 Then
                    objTransaction.Tax1 = strTax1(1)
                    objTransaction.Tax1Desc = strTax1(0)
                End If
            End If
            If strTaxCal.Length >= 2 Then
                strTax2 = strTaxCal(1).Split("~")
                If strTax2.Length > 1 Then
                    objTransaction.Tax2 = strTax2(1)
                    objTransaction.Tax2Desc = strTax2(0)
                End If
            End If
            If strTaxCal.Length >= 3 Then
                strTax3 = strTaxCal(2).Split("~")
                If strTax3.Length > 1 Then
                    objTransaction.Tax3 = strTax3(1)
                    objTransaction.Tax3Desc = strTax3(0)
                End If
            End If
            If strTaxCal.Length >= 4 Then
                strTax4 = strTaxCal(3).Split("~")
                If strTax4.Length > 1 Then
                    objTransaction.Tax4 = strTax4(1)
                    objTransaction.Tax4Desc = strTax4(0)
                End If
            End If

            objTransaction.TransStatus = 1

            If strTransType = 1 Then
                objPosTranAccHrd.AcctPayType = 1
            ElseIf strTransType = 7 Then  'Condition for lost transaction
                objPosTranAccHrd.AcctPayType = 7
                objTransaction.TransStatus = 0
            ElseIf strTransType = 8 Then  'Condition for Gift transaction
                objPosTranAccHrd.AcctPayType = 8
                'objTransaction.TransStatus = 0
            ElseIf strTransType = 9 Then  'Condition for Staff Gift transaction
                objPosTranAccHrd.AcctPayType = 9
                'objTransaction.TransStatus = 0
            ElseIf strTransType = 6 Then
                objPosTranAccHrd.AcctPayType = 1
            End If

            If strTransType = 2 Then
                objPosTranAccHrd.AcctPayType = 2
            ElseIf strTransType = 3 Then
                objPosTranAccHrd.AcctPayType = 3
            ElseIf strTransType = 4 Then
                objPosTranAccHrd.AcctPayType = 4
            ElseIf strTransType = 5 Then
                objPosTranAccHrd.AcctPayType = 5
            End If


            '***************************** Check for Cancel/tab Button
            If strBtnType.ToLower = "cancel" Then
                objTransaction.TransStatus = 0
            ElseIf strBtnType.ToLower = "tab" Then
                objTransaction.PosTabNote = strTabNote
                objTransaction.TransStatus = 9
            End If

            objPosTranAccHrd.CashReturned = strReturnAmt
            objPosTranAccHrd.CasReceived = strReceivedAmt

            If strTransType = 8 OrElse strTransType = 9 Then
                objTransaction.PosPercentDiscount = 100.0F

                objPosTranAccHrd.CashReturned = 0
                objPosTranAccHrd.CasReceived = 0
            End If
            'objPosTranAccHrd.CashReturned = strReturnAmt
            'objPosTranAccHrd.CasReceived = strReceivedAmt

            objTransaction.TransUserID = strUserID
            objTransaction.TransRegCode = strRegCode
            objTransaction.TransType = "S"
            objTransaction.TransDateTime = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
            objTransaction.TotalValue = strTotalAmt
            objTransaction.SubTotal = strTotalSubAmt
            objTransaction.OldTransID = "sysNull"
            objTransaction.PosGiftReason = giftReason
            objTransaction.PosAuthorizedBy = authorizedBy

            '**************************** Condition for Tab
            If strPreviousTranID <> "" Then
                objTransaction.TransStatus = 1
                objTransaction.TransId = strPreviousTranID
                objTransaction.funUpdatePosTransaction()
                strTranID = strPreviousTranID
                objTransaction.funPosTransactionDelete(strPreviousTranID)
            Else
                '***************************** Insert in Transaction Table
                objTransaction.funInsertPosTransaction(strTranID)
            End If

            objPosTranAccHrd.AcctTransID = strTranID
            If strBtnType = "" Then
                '***************************** Insert in postransaccthdr Table
                Dim strTranAccHrdID As String = String.Empty
                objPosTranAccHrd.funInsertpostransaccthdr(strTranAccHrdID)
            Else
                'Added By Hitendra On 2011-12-27
                'Following statemts needs to hard code to make card transaction equivelent to Cash Trasaction only difference will be TrasactionType
                Dim strTranAccHrdID As String = String.Empty
                objPosTranAccHrd.funInsertpostransaccthdr(strTranAccHrdID)
            End If


            '***************************** Insert in posproductno
            Dim strPrd() As String = strProductDtl.Split(item, System.StringSplitOptions.None)
            Dim strTaxOnPrd() As String
            Dim strTaxValue() As String
            Dim intI = 0, intR As Integer = 0
            Dim strPrdItem() As String
            objPosPrdNo.PrdTransID = strTranID
            While strPrd.Length - 1 > intI
                strTaxValue = strPrd(intI).Split(tax, System.StringSplitOptions.None)
                strPrdItem = strTaxValue(0).Split("~")
                If strPrdItem.Length > 1 Then
                    objPosPrdNo.ProductID = strPrdItem(0)
                    objPosPrdNo.PrdQty = strPrdItem(1)
                    objPosPrdNo.PrdUnitPrice = strPrdItem(2)
                    objPosPrdNo.PrdPrice = strPrdItem(3)
                    objPosPrdNo.Tax4 = 0
                    objPosPrdNo.Tax3 = 0
                    objPosPrdNo.Tax2 = 0
                    objPosPrdNo.Tax1 = 0
                    If strTaxValue.Length > 1 Then
                        strTaxOnPrd = strTaxValue(1).Split("^")
                        While strTaxOnPrd.Length - 1 > intR
                            strTax1 = strTaxOnPrd(intR).Split("~")
                            If strTax1.Length > 1 Then
                                If intR = 0 Then
                                    objPosPrdNo.Tax1 = strTax1(1)
                                    objPosPrdNo.Tax1Desc = strTax1(0)
                                ElseIf intR = 1 Then
                                    objPosPrdNo.Tax2 = strTax1(1)
                                    objPosPrdNo.Tax2Desc = strTax1(0)
                                ElseIf intR = 2 Then
                                    objPosPrdNo.Tax3 = strTax1(1)
                                    objPosPrdNo.Tax3Desc = strTax1(0)
                                ElseIf intR = 3 Then
                                    objPosPrdNo.Tax4 = strTax1(1)
                                    objPosPrdNo.Tax4Desc = strTax1(0)
                                End If
                            End If
                            intR += 1
                        End While
                    End If
                    intR = 0
                    objPosPrdNo.PrdTaxCode = objTransaction.funPrdTax(strPrdItem(0), strWhshouse)
                    objPosPrdNo.funInsertPosProduct()
                    If strBtnType = "" Then
                        If objProduct.funGetPrdValue(objPosPrdNo.ProductID, "kit") = "1" Then
                            objPrdQty.subGetPrdKitOnHandQty(objPosPrdNo.ProductID, strWhshouse, objPosPrdNo.PrdQty) 'reduce kit's product Qty
                            objPrdQty.updateProductQuantity(objPosPrdNo.ProductID, strWhshouse, objPosPrdNo.PrdQty) ' reduce prd kit Qty
                        Else
                            objPrdQty.updateProductQuantity(objPosPrdNo.ProductID, strWhshouse, objPosPrdNo.PrdQty)
                            objPrdQty.funGetReducePrdKitQty(objPosPrdNo.ProductID, strWhshouse, "") ' reduce prd kit Qty
                        End If
                    End If
                End If
                intI += 1
            End While

            ''*************************** Return value
            If strBtnType = "" Then
                If strTransType = 7 Then
                    strvalue = msgTransaction & " " & strTranID & " has been Lost." & "^" & strTranID
                ElseIf strTransType = 8 Then 'Gift Transation Added By Hitendra
                    strvalue = msgTransaction & " " & strTranID & " has been gifted to guest." & "^" & strTranID
                ElseIf strTransType = 9 Then 'Staff Gift Transation Added By Hitendra
                    strvalue = msgTransaction & " " & strTranID & " has been gifted to staff." & "^" & strTranID
                Else
                    If LangsID = "en" Then
                        strvalue = msgTransaction & " " & strTranID & " " & msgCompletedSuccess & "[br]" & POSTotalAmount & " " & strTotalAmt & ", " & POSReceived & " " & strReceivedAmt & ", " & POSBalance & " " & strReturnAmt & "^" & strTranID
                    Else
                        strvalue = "La transaction" & " " & strTranID & " " & "est compl�ter avec succ�s" & "[br]" & "Montant Total " & " " & strTotalAmt & ", " & "Montant re�u " & " " & strReceivedAmt & ", " & POSBalance & " " & strReturnAmt & "^" & strTranID
                    End If
                End If
            ElseIf strBtnType.ToLower = "cancel" Then
                strvalue = msgTrCanceled
            ElseIf strBtnType.ToLower = "tab" Then
                strvalue = msgTrdPausedSuccessfully
            End If

            'Added By Hitendra On 2011-12-27
            'Following statemts needs to hard code to make card transaction equivelent to Cash Trasaction only difference will be TrasactionType
            Try
                If String.IsNullOrEmpty(strvalue.Trim()) Then
                    strvalue = msgTransaction & " " & strTranID & " " & msgCompletedSuccess & "[br]" & POSTotalAmount & strTotalAmt & ", " & POSReceived & strReceivedAmt & ", " & POSBalance & strReturnAmt & "^" & strTranID
                End If
            Catch ex As Exception

            End Try


            strvalue = funAddXMLTag(strvalue)
            Return strvalue
        Catch ex As Exception
            Return "__error__" & "POSCashTransactionFailed"
        Finally
            objTransaction = Nothing
            objPosPrdNo = Nothing
            objPosTranAccHrd = Nothing
            objTransDtl = Nothing
            objProduct = Nothing
            objPrdQty = Nothing
            item = Nothing
            tax = Nothing
        End Try
    End Function
    'Insert Transaction Details
    <WebMethod()> _
    Public Function funInsertTransactionDetail(ByVal strTokan As String, ByVal strUserID As String, ByVal strWhshouse As String, ByVal strTranID As String, ByVal strTotalAmt As String, ByVal strTransType As String, ByVal strProductDtl As String, ByVal strTranAccHrdID As String, ByVal strPosAccDtlId As String, ByVal strCardValue As String, ByVal strReturnAmt As String, ByVal strReceivedAmt As String, ByVal strCardLangas As String, ByVal strCardAccountType As String, ByVal strCardMsg As String, ByVal strRegCode As String) As String
        Dim objTransaction As New clsPOSTransaction
        Dim objPosPrdNo As New clsPosProductNo
        Dim objPosTranAccHrd As New clsPosTransAccHdr
        Dim objTransDtl As New clsTransactionDtl
        Dim strvalue, StrNewToken As String
        Dim objProduct As New clsProducts
        Dim objPrdQty As New clsPrdQuantity
        Dim item() As String = {"__item__"}
        Dim tax() As String = {"__taxGrp__"}
        Dim strTax1(), strTax2(), strTax3(), strTax4() As String
        Dim strTaxCal() As String
        Try
            Dim strResponse As String
            Dim strMsg As String
            If strTransType = 2 Or strTransType = 3 Or strTransType = 4 Or strTransType = 5 Then
                Dim str() As String = strCardValue.ToLower.Split("~")
                If str(0) = "1[49]" Then
                    strResponse = 1
                Else
                    strResponse = 0
                End If

                If strResponse = 1 Then
                    If funCloseTansactionToken(strTokan, strUserID) = False Then
                        strvalue = "POSInvalidTokenNo"
                        strvalue = funAddXMLTag(strvalue)
                        Return strvalue
                    End If
                Else
                    If funGetToken(strTokan, strUserID, StrNewToken) = False Then
                        strvalue = "POSInvalidTokenNo"
                        strvalue = funAddXMLTag(strvalue)
                        Return strvalue
                    End If
                End If

                strMsg = str(1)
                If str(2).ToLower = "visa" Then
                    objPosTranAccHrd.AcctPayType = 2
                    objTransDtl.posCardName = str(2)
                ElseIf str(2).ToLower = "masterCard" Then
                    objPosTranAccHrd.AcctPayType = 3
                    objTransDtl.posCardName = str(2)
                ElseIf str(2).ToLower = "american express" Then
                    objPosTranAccHrd.AcctPayType = 4
                    objTransDtl.posCardName = str(2)
                ElseIf str(2).ToLower = "interac" Then
                    objPosTranAccHrd.AcctPayType = 5
                    objTransDtl.posCardName = str(2)
                End If
                If str(3) <> "" Then
                    objTransDtl.posCardNo = str(3) 'card No
                End If
                objTransDtl.posExtReturnStatus = str(4) & str(5) 'expiry date
                objTransDtl.posTransID = str(6)
                objTransDtl.posTransEndTime = str(8) & " " & str(7)

                If strResponse = "1" Then
                    objTransDtl.posTransAcctDtlStatus = "1"
                Else
                    objTransDtl.posTransAcctDtlStatus = "9"
                End If

                If strCardLangas = "[48]" Or strCardLangas = "[0]" Then
                    objTransDtl.posCardLang = "en"
                ElseIf strCardLangas = "[49]" Then
                    objTransDtl.posCardLang = "fr"
                Else
                    objTransDtl.posCardLang = "fr"
                End If

                objTransDtl.posCardMsg = strCardMsg
                objTransDtl.posCardAccountType = strCardAccountType

                objTransDtl.funUpdateTransaction(strPosAccDtlId)
                If strResponse = "1" Then
                    objTransaction.TransStatus = 1
                    objPosTranAccHrd.CasReceived = strTotalAmt
                    objPosTranAccHrd.funUpdateRecievePostransaccthdr()
                    objTransaction.funUpdateTransStaus(strTranID)
                Else
                    objTransaction.TransStatus = 0
                    objPosTranAccHrd.CasReceived = 0
                    objPosTranAccHrd.funUpdateRecievePostransaccthdr()
                    objTransaction.funUpdateTransStaus(strTranID)
                End If
            End If

            '***************************** Insert in posproductno
            Dim strPrd() As String = strProductDtl.Split(item, System.StringSplitOptions.None)
            Dim intI = 0, intR As Integer = 0
            Dim strPrdItem() As String
            Dim strTaxOnPrd() As String
            Dim strTaxValue() As String
            objPosPrdNo.PrdTransID = strTranID
            While strPrd.Length - 1 > intI
                strTaxValue = strPrd(intI).Split(tax, System.StringSplitOptions.None)
                strPrdItem = strTaxValue(0).Split("~")
                If strPrdItem.Length > 1 Then
                    objPosPrdNo.ProductID = strPrdItem(0)
                    objPosPrdNo.PrdQty = strPrdItem(1)
                    objPosPrdNo.PrdUnitPrice = strPrdItem(2)
                    objPosPrdNo.PrdPrice = strPrdItem(3)
                    objPosPrdNo.Tax4 = 0
                    objPosPrdNo.Tax3 = 0
                    objPosPrdNo.Tax2 = 0
                    objPosPrdNo.Tax1 = 0
                    'Dim strTaxList As String
                    'strTaxList = funTax(objPosPrdNo.ProductID, objPosPrdNo.PrdPrice, strWhshouse, strRegCode)
                    If strTaxValue.Length > 1 Then
                        strTaxOnPrd = strTaxValue(1).Split("^")
                        While strTaxOnPrd.Length - 1 > intR
                            strTax1 = strTaxOnPrd(intR).Split("~")
                            If strTax1.Length > 1 Then
                                If intR = 0 Then
                                    objPosPrdNo.Tax1 = strTax1(1)
                                    objPosPrdNo.Tax1Desc = strTax1(0)
                                ElseIf intR = 1 Then
                                    objPosPrdNo.Tax2 = strTax1(1)
                                    objPosPrdNo.Tax2Desc = strTax1(0)
                                ElseIf intR = 2 Then
                                    objPosPrdNo.Tax3 = strTax1(1)
                                    objPosPrdNo.Tax3Desc = strTax1(0)
                                ElseIf intR = 3 Then
                                    objPosPrdNo.Tax4 = strTax1(1)
                                    objPosPrdNo.Tax4Desc = strTax1(0)
                                End If
                            End If
                            intR += 1
                        End While
                    End If
                    intR = 0
                    objPosPrdNo.PrdTaxCode = objTransaction.funPrdTax(strPrdItem(0), strWhshouse)
                    objPosPrdNo.funInsertPosProduct()
                    If strTransType = 1 Or strResponse = 1 Then
                        If objProduct.funGetPrdValue(objPosPrdNo.ProductID, "kit") = "1" Then
                            objPrdQty.subGetPrdKitOnHandQty(objPosPrdNo.ProductID, strWhshouse, objPosPrdNo.PrdQty) 'reduce kit's product Qty
                            objPrdQty.updateProductQuantity(objPosPrdNo.ProductID, strWhshouse, objPosPrdNo.PrdQty) ' reduce prd kit Qty
                        Else
                            objPrdQty.updateProductQuantity(objPosPrdNo.ProductID, strWhshouse, objPosPrdNo.PrdQty)
                            objPrdQty.funGetReducePrdKitQty(objPosPrdNo.ProductID, strWhshouse, "") ' reduce prd kit Qty
                        End If
                    End If
                End If
                intI += 1
            End While
            ''*************************** Return value
            If strResponse = 0 Then
                'strvalue = msgTransaction & " " & strTranID & " [" & strMsg & ".]"
                strvalue = strMsg & "__token__" & StrNewToken
            Else
                strvalue = msgTransaction & " " & strTranID & " " & msgCompletedSuccess & "<br/> " & POSTotalAmount & strTotalAmt & ", " & POSReceived & strReceivedAmt & ", " & POSBalance & strReturnAmt
            End If
            strvalue = funAddXMLTag(strvalue)
            Return strvalue
        Catch ex As Exception
            Return "__error__" & "POSProblemInInsertTransaction"
        Finally
            objTransaction = Nothing
            objPosPrdNo = Nothing
            objPosTranAccHrd = Nothing
            objTransDtl = Nothing
            objProduct = Nothing
            objPrdQty = Nothing
        End Try
    End Function
    'Generates Tokan 
    Public Function funGetTokenForTransaction(ByVal strPreTokan As String, ByVal strUserID As String, ByRef StrNewToken As String) As Boolean
        Dim strToken As String
        Dim ObjCommon As New clsCommon
        Dim strSQL As String
        Dim objDC As New clsDataClass
        Try
            strToken = Date.Now.Ticks & ObjCommon.funRandomString()
            Dim strData As String
            strSQL = "select count(*) from Postoken where tokenID='" & strPreTokan & "' and tokenUser='" & strUserID & "' and tokenStatus='1' and adddate(tokenTime,INTERVAL 20 Minute) > now()"
            strData = objDC.GetScalarData(strSQL)
            If strData = 1 Then
                strSQL = "select tokenNo from Postoken where tokenID='" & strPreTokan & "' and tokenUser='" & strUserID & "' and tokenStatus='1'"
                strData = objDC.GetScalarData(strSQL)
                strSQL = "Update Postoken set tokenStatus='0' where tokenNo='" & strData & "' "
                objDC.PopulateData(strSQL)
                strSQL = "Insert into Postoken (tokenId, tokenUser, tokenTime, tokenStatus) values ('" & strToken & "', '" & strUserID & "', now(), '1')"
                objDC.PopulateData(strSQL)
                strSQL = "select tokenNo from Postoken where tokenID='" & strToken & "' and tokenUser='" & strUserID & "' and tokenStatus='1'"
                strData = objDC.GetScalarData(strSQL)
                strToken = "T" & strData & "-" & strToken
                strSQL = "Update Postoken set tokenId='" & strToken & "' where tokenNo='" & strData & "' "
                objDC.PopulateData(strSQL)
                StrNewToken = strToken
                objDC.CloseDatabaseConnection()
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
        Finally
            objDC = Nothing
            ObjCommon = Nothing
        End Try
    End Function
    'Close Transaction Tokan 
    Public Function funCloseTansactionToken(ByVal strPreTokan As String, ByVal strUserID As String, Optional ByVal strType As String = "") As Boolean
        Dim strSQL As String
        Dim objDC As New clsDataClass
        Dim strData As String
        Try
            strSQL = "select count(*) from Postoken where tokenID='" & strPreTokan & "' and tokenUser='" & strUserID & "' and tokenStatus='1' "
            If strType = "" Then
                strSQL += " and adddate(tokenTime,INTERVAL 2 Minute) > now()"
            Else
                strSQL += " and adddate(tokenTime,INTERVAL 20 Minute) > now()"
            End If
            strData = objDC.GetScalarData(strSQL)
            If strData = 1 Then
                strSQL = "select tokenNo from Postoken where tokenID='" & strPreTokan & "' and tokenUser='" & strUserID & "' and tokenStatus='1'"
                strData = objDC.GetScalarData(strSQL)
                strSQL = "Update Postoken set tokenStatus='0' where tokenNo='" & strData & "' "
                objDC.PopulateData(strSQL)
                objDC.CloseDatabaseConnection()
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
        Finally
            objDC = Nothing
        End Try
    End Function
    'Get Data for Tab Condition
    <WebMethod()> _
    Public Function funGetPrdDtlForTab(ByVal strTokan As String, ByVal strUserID As String, ByVal strLang As String, ByVal strWhshouse As String, ByVal strTranID As String, ByVal strRegcode As String) As String
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        Dim strXML As New StringBuilder
        Dim objDataClass As New clsDataClass
        Dim strValue, StrNewToken As String
        Dim strTax As StringBuilder
        Try
            If funGetToken(strTokan, strUserID, StrNewToken) = False Then
                strXML.Append("POSInvalidTokenNo")
                strValue = funAddXMLTag(strXML.ToString)
                Return strValue
            End If
            strSQL = "SELECT posPrdTransID, posProductID,Pdes.prdname as ProductName, posPrdQty, posPrdUnitPrice,posPrdPrice, posPrdTaxCode,prdUPCCode,PosTax1, PosTax1Desc, PosTax2, PosTax2Desc, PosTax3, PosTax3Desc, PosTax4, PosTax4Desc FROM posproductno "
            strSQL += "Inner join Products on Products.ProductID=posproductno.posProductID"
            strSQL += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID where descLang ='" + strLang + "' and posPrdTransID='" + strTranID + "' "
            drObj = objDataClass.GetDataReader(strSQL)

            While drObj.Read
                strTax = New StringBuilder
                If drObj.Item("PosTax1Desc").ToString <> "" Then
                    strTax.Append(drObj.Item("PosTax1Desc").ToString & "~" & drObj.Item("PosTax1").ToString & "^")
                End If
                If drObj.Item("PosTax2Desc").ToString <> "" Then
                    strTax.Append(drObj.Item("PosTax2Desc").ToString & "~" & drObj.Item("PosTax2").ToString & "^")
                End If
                If drObj.Item("PosTax3Desc").ToString <> "" Then
                    strTax.Append(drObj.Item("PosTax3Desc").ToString & "~" & drObj.Item("PosTax3").ToString & "^")
                End If
                If drObj.Item("PosTax4Desc").ToString <> "" Then
                    strTax.Append(drObj.Item("PosTax4Desc").ToString & "~" & drObj.Item("PosTax4").ToString)
                End If
                strXML.Append(drObj.Item("posProductID").ToString & "~" & objCommon.replaceSpecialCh(drObj.Item("ProductName").ToString) & "~" & drObj.Item("prdUPCCode").ToString & "~" & drObj.Item("posPrdQty").ToString & "~1~1~" & drObj.Item("posPrdUnitPrice").ToString & "~1~1__taxGrp__" & strTax.ToString & "__item__")
            End While
            Dim strItem As String = "__item__"
            If strXML.ToString <> "" Then
                strValue = strXML.ToString
                'strValue = strXML.ToString.Substring(0, strXML.ToString.Length - strItem.Length)
            Else
                strValue = POSNoProductfound
            End If
            strValue += "__token__" & StrNewToken
            drObj.Close()
            drObj = Nothing
            objDataClass.CloseDatabaseConnection()
            strValue = funAddXMLTag(strValue)
            Return strValue
        Catch ex As Exception
            Return "__error__" & "POSNoTransactionInformationReturn"
        Finally
            drObj = Nothing
            objDataClass.CloseDatabaseConnection()
            objDataClass = Nothing
        End Try
    End Function
    Public Sub subTaxes(ByVal objTransaction As clsPOSTransaction, ByVal strProductDtl As String, ByVal strWhshouse As String, ByVal strRegCode As String, ByVal strSubtotal As String)
        Dim item() As String = {"__item__"}
        Dim strPrd() As String = strProductDtl.Split(item, System.StringSplitOptions.None)
        Dim strTaxOnPrd() As String
        Dim intI = 0, intR As Integer = 0
        Dim strPrdItem() As String
        Dim strTax1 = "0", strTax2 = "0", strTax3 = "0", strTax4 = "0", strTaxDesc1 = "", strTaxDesc2 = "", strTaxDesc3 = "", strTaxDesc4 = "", strTaxVal, strTaxDescVal As String

        objTransaction.Tax4 = 0
        objTransaction.Tax3 = 0
        objTransaction.Tax2 = 0
        objTransaction.Tax1 = 0

        While strPrd.Length - 1 > intI
            strPrdItem = strPrd(intI).Split("~")
            If strPrdItem.Length > 1 Then
                Dim strTaxList As String
                strTaxList = funTax(strPrdItem(0), strPrdItem(3), strWhshouse, strRegCode)
                strTaxOnPrd = strTaxList.Split("^")
                intR = 0
                While strTaxOnPrd.Length - 1 > intR
                    If strTaxOnPrd.Length >= 1 Then
                        strTaxDescVal = strTaxOnPrd(intR).ToString.Substring(0, strTaxOnPrd(intR).IndexOf("~"))
                        strTaxVal = strTaxOnPrd(intR).ToString.Substring(strTaxOnPrd(intR).IndexOf("~") + 1)
                        If strTaxDesc1.ToLower = strTaxDescVal.ToLower Or strTaxDesc1.ToLower = "" Then
                            strTax1 = CDbl(strTax1) + CDbl(strTaxVal)
                            strTaxDesc1 = strTaxDescVal
                            objTransaction.Tax1 = strTax1
                            objTransaction.Tax1Desc = strTaxDesc1
                        ElseIf strTaxDesc2.ToLower = strTaxDescVal.ToLower Or strTaxDesc2.ToLower = "" Then
                            strTax2 = CDbl(strTax2) + CDbl(strTaxVal)
                            strTaxDesc2 = strTaxDescVal
                            objTransaction.Tax2 = strTax2
                            objTransaction.Tax2Desc = strTaxDesc2
                        ElseIf strTaxDesc3.ToLower = strTaxDescVal.ToLower Or strTaxDesc3.ToLower = "" Then
                            strTax3 = CDbl(strTax3) + CDbl(strTaxVal)
                            strTaxDesc3 = strTaxDescVal
                            objTransaction.Tax3 = strTax3
                            objTransaction.Tax3Desc = strTaxDesc3
                        ElseIf strTaxDesc4.ToLower = strTaxDescVal.ToLower Or strTaxDesc4.ToLower = "" Then
                            strTax4 = CDbl(strTax4) + CDbl(strTaxVal)
                            strTaxDesc4 = strTaxDescVal
                            objTransaction.Tax4 = strTax4
                            objTransaction.Tax4Desc = strTaxDesc4
                        End If
                    End If
                    intR += 1
                End While
            End If
            intI += 1
        End While
        objTransaction.TotalValue = CDbl(strSubtotal) + CDbl(objTransaction.Tax1) + CDbl(objTransaction.Tax2) + CDbl(objTransaction.Tax3) + CDbl(objTransaction.Tax4)
    End Sub
    '<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    <WebMethod()> _
    Public Function Transaction(ByVal strUserID As String, ByVal strRegCode As String, ByVal strTax As String, ByVal strTotalAmt As String, ByVal strTotalSubAmt As String, ByVal strTransType As String, ByVal strReturnAmt As String, ByVal strReceivedAmt As String, ByVal strWhshouse As String, ByVal strProductDtl As String, ByVal strBtnType As String, ByVal strPreviousTranID As String, ByVal strTabNote As String) As String
        Dim objTransaction As New clsPOSTransaction
        Dim objPosPrdNo As New clsPosProductNo
        Dim objPosTranAccHrd As New clsPosTransAccHdr
        Dim objTransDtl As New clsTransactionDtl
        Dim objProduct As New clsProducts
        Dim objPrdQty As New clsPrdQuantity
        Dim strTranID, strvalue As String
        strvalue = String.Empty
        Dim strTax1(), strTax2(), strTax3(), strTax4() As String
        Dim item() As String = {"__item__"}
        Dim tax() As String = {"__taxGrp__"}
        Dim objCateProduct As New iTECH.InbizERP.BusinessLogic.CategoryWithProduct
        strTabNote = objCateProduct.GetTransitionTableNote()

        Try
            'If funCloseTansactionToken(strTokan, strUserID, "1") = False Then
            '    strvalue = "POSInvalidTokenNo"
            '    strvalue = funAddXMLTag(strvalue)
            '    Return strvalue
            'End If
            objTransaction.Tax4 = 0
            objTransaction.Tax3 = 0
            objTransaction.Tax2 = 0
            objTransaction.Tax1 = 0
            Dim strTaxCal() As String = strTax.Split("^")
            If strTaxCal.Length >= 1 Then
                strTax1 = strTaxCal(0).Split("~")
                If strTax1.Length > 1 Then
                    objTransaction.Tax1 = strTax1(1)
                    objTransaction.Tax1Desc = strTax1(0)
                End If
            End If
            If strTaxCal.Length >= 2 Then
                strTax2 = strTaxCal(1).Split("~")
                If strTax2.Length > 1 Then
                    objTransaction.Tax2 = strTax2(1)
                    objTransaction.Tax2Desc = strTax2(0)
                End If
            End If
            If strTaxCal.Length >= 3 Then
                strTax3 = strTaxCal(2).Split("~")
                If strTax3.Length > 1 Then
                    objTransaction.Tax3 = strTax3(1)
                    objTransaction.Tax3Desc = strTax3(0)
                End If
            End If
            If strTaxCal.Length >= 4 Then
                strTax4 = strTaxCal(3).Split("~")
                If strTax4.Length > 1 Then
                    objTransaction.Tax4 = strTax4(1)
                    objTransaction.Tax4Desc = strTax4(0)
                End If
            End If

            objTransaction.TransStatus = 1

            If strTransType = 1 Then
                objPosTranAccHrd.AcctPayType = 1
            ElseIf strTransType = 7 Then  'Condition for lost transaction
                objPosTranAccHrd.AcctPayType = 7
                objTransaction.TransStatus = 0
            ElseIf strTransType = 8 Then  'Condition for Gift transaction
                objPosTranAccHrd.AcctPayType = 8
                'objTransaction.TransStatus = 0
            ElseIf strTransType = 9 Then  'Condition for Staff Gift transaction
                objPosTranAccHrd.AcctPayType = 9
                'objTransaction.TransStatus = 0
            ElseIf strTransType = 6 Then
                objPosTranAccHrd.AcctPayType = 1
            End If

            If strTransType = 2 Then
                objPosTranAccHrd.AcctPayType = 2
            ElseIf strTransType = 3 Then
                objPosTranAccHrd.AcctPayType = 3
            ElseIf strTransType = 4 Then
                objPosTranAccHrd.AcctPayType = 4
            ElseIf strTransType = 5 Then
                objPosTranAccHrd.AcctPayType = 5
            End If


            '***************************** Check for Cancel/tab Button
            If strBtnType.ToLower = "cancel" Then
                objTransaction.TransStatus = 0
            ElseIf strBtnType.ToLower = "tab" Then
                objTransaction.PosTabNote = strTabNote
                objTransaction.TransStatus = 9
            End If

            objPosTranAccHrd.CashReturned = strReturnAmt
            objPosTranAccHrd.CasReceived = strReceivedAmt

            If strTransType = 8 OrElse strTransType = 9 Then
                objTransaction.PosPercentDiscount = 100.0F

                objPosTranAccHrd.CashReturned = 0
                objPosTranAccHrd.CasReceived = 0
            End If
            'objPosTranAccHrd.CashReturned = strReturnAmt
            'objPosTranAccHrd.CasReceived = strReceivedAmt

            objTransaction.TransUserID = strUserID
            objTransaction.TransRegCode = strRegCode
            objTransaction.TransType = "S"
            objTransaction.TransDateTime = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
            objTransaction.TotalValue = strTotalAmt
            objTransaction.SubTotal = strTotalSubAmt
            objTransaction.OldTransID = "sysNull"
            objTransaction.PosGiftReason = "sysNull"
            objTransaction.PosAuthorizedBy = "sysNull"

            '***************************** Insert in Transaction Table
            objTransaction.funInsertPosTransaction(strTranID)
            objPosTranAccHrd.AcctTransID = strTranID

            If strBtnType = "" Then
                '***************************** Insert in postransaccthdr Table
                Dim strTranAccHrdID As String = String.Empty
                objPosTranAccHrd.funInsertpostransaccthdr(strTranAccHrdID)
            Else
                'Added By Hitendra On 2011-12-27
                'Following statemts needs to hard code to make card transaction equivelent to Cash Trasaction only difference will be TrasactionType
                Dim strTranAccHrdID As String = String.Empty
                objPosTranAccHrd.funInsertpostransaccthdr(strTranAccHrdID)
            End If

            '***************************** Insert in posproductno
            Dim strPrd() As String = strProductDtl.Split(item, System.StringSplitOptions.None)
            Dim strTaxOnPrd() As String
            Dim strTaxValue() As String
            Dim intI = 0, intR As Integer = 0
            Dim strPrdItem() As String
            objPosPrdNo.PrdTransID = strTranID
            While strPrd.Length - 1 > intI
                strTaxValue = strPrd(intI).Split(tax, System.StringSplitOptions.None)
                strPrdItem = strTaxValue(0).Split("~")
                If strPrdItem.Length > 1 Then
                    objPosPrdNo.ProductID = strPrdItem(0)
                    objPosPrdNo.PrdQty = strPrdItem(1)
                    objPosPrdNo.PrdUnitPrice = strPrdItem(2)
                    objPosPrdNo.PrdPrice = strPrdItem(3)
                    objPosPrdNo.Tax4 = 0
                    objPosPrdNo.Tax3 = 0
                    objPosPrdNo.Tax2 = 0
                    objPosPrdNo.Tax1 = 0
                    If strTaxValue.Length > 1 Then
                        strTaxOnPrd = strTaxValue(1).Split("^")
                        While strTaxOnPrd.Length - 1 > intR
                            strTax1 = strTaxOnPrd(intR).Split("~")
                            If strTax1.Length > 1 Then
                                If intR = 0 Then
                                    objPosPrdNo.Tax1 = strTax1(1)
                                    objPosPrdNo.Tax1Desc = strTax1(0)
                                ElseIf intR = 1 Then
                                    objPosPrdNo.Tax2 = strTax1(1)
                                    objPosPrdNo.Tax2Desc = strTax1(0)
                                ElseIf intR = 2 Then
                                    objPosPrdNo.Tax3 = strTax1(1)
                                    objPosPrdNo.Tax3Desc = strTax1(0)
                                ElseIf intR = 3 Then
                                    objPosPrdNo.Tax4 = strTax1(1)
                                    objPosPrdNo.Tax4Desc = strTax1(0)
                                End If
                            End If
                            intR += 1
                        End While
                    End If
                    intR = 0
                    objPosPrdNo.PrdTaxCode = objTransaction.funPrdTax(strPrdItem(0), strWhshouse)
                    objPosPrdNo.funInsertPosProduct()
                    If strBtnType = "" Then
                        If objProduct.funGetPrdValue(objPosPrdNo.ProductID, "kit") = "1" Then
                            objPrdQty.subGetPrdKitOnHandQty(objPosPrdNo.ProductID, strWhshouse, objPosPrdNo.PrdQty) 'reduce kit's product Qty
                            objPrdQty.updateProductQuantity(objPosPrdNo.ProductID, strWhshouse, objPosPrdNo.PrdQty) ' reduce prd kit Qty
                        Else
                            objPrdQty.updateProductQuantity(objPosPrdNo.ProductID, strWhshouse, objPosPrdNo.PrdQty)
                            objPrdQty.funGetReducePrdKitQty(objPosPrdNo.ProductID, strWhshouse, "") ' reduce prd kit Qty
                        End If
                    End If
                End If
                intI += 1
            End While
            '**************************** Condition for Tab
            If strPreviousTranID <> "" Then
                objTransaction.TransStatus = 0
                objTransaction.funUpdateTransStaus(strPreviousTranID)
            End If
            ''*************************** Return value
            If strBtnType = "" Then
                If strTransType = 7 Then
                    strvalue = msgTransaction & " " & strTranID & " has been Lost." & "^" & strTranID
                ElseIf strTransType = 8 Then 'Gift Transation Added By Hitendra
                    strvalue = msgTransaction & " " & strTranID & " has been gifted to guest." & "^" & strTranID
                ElseIf strTransType = 9 Then 'Staff Gift Transation Added By Hitendra
                    strvalue = msgTransaction & " " & strTranID & " has been gifted to staff." & "^" & strTranID
                Else
                    strvalue = msgTransaction & " " & strTranID & " " & msgCompletedSuccess & "[br]" & POSTotalAmount & strTotalAmt & ", " & POSReceived & strReceivedAmt & ", " & POSBalance & strReturnAmt & "^" & strTranID
                End If
            ElseIf strBtnType.ToLower = "cancel" Then
                strvalue = msgTrCanceled
            ElseIf strBtnType.ToLower = "tab" Then
                strvalue = msgTrdPausedSuccessfully
            End If

            'Added By Hitendra On 2011-12-27
            'Following statemts needs to hard code to make card transaction equivelent to Cash Trasaction only difference will be TrasactionType
            Try
                If String.IsNullOrEmpty(strvalue.Trim()) Then
                    strvalue = msgTransaction & " " & strTranID & " " & msgCompletedSuccess & "[br]" & POSTotalAmount & strTotalAmt & ", " & POSReceived & strReceivedAmt & ", " & POSBalance & strReturnAmt & "^" & strTranID
                End If
            Catch ex As Exception

            End Try

            'strvalue = funAddXMLTag(strvalue + " Your token is " + strTabNote)
            strvalue = funAddXMLTag(strTabNote + "&transID=" + strTranID)
            Return strvalue
        Catch ex As Exception
            Return "__error__" & "POSCashTransactionFailed"
        Finally
            objTransaction = Nothing
            objPosPrdNo = Nothing
            objPosTranAccHrd = Nothing
            objTransDtl = Nothing
            objProduct = Nothing
            objPrdQty = Nothing
            item = Nothing
            tax = Nothing
        End Try
    End Function

    <WebMethod()> _
    Public Function TransactionWithCustDetails(ByVal strUserID As String, ByVal strRegCode As String, ByVal strTax As String, ByVal strTotalAmt As String, ByVal strTotalSubAmt As String, ByVal strTransType As String, ByVal strReturnAmt As String, ByVal strReceivedAmt As String, ByVal strWhshouse As String, ByVal strProductDtl As String, ByVal strBtnType As String, ByVal strPreviousTranID As String, ByVal strTabNote As String, ByVal strCustPhoneNo As String, ByVal strCustEmailID As String, ByVal strCustPickUpDate As String, ByVal strCustAdditionalRequest As String) As String
        Dim objTransaction As New clsPOSTransaction
        Dim objPosPrdNo As New clsPosProductNo
        Dim objPosTranAccHrd As New clsPosTransAccHdr
        Dim objTransDtl As New clsTransactionDtl
        Dim objProduct As New clsProducts
        Dim objPrdQty As New clsPrdQuantity
        Dim strTranID, strvalue As String
        strvalue = String.Empty
        Dim strTax1(), strTax2(), strTax3(), strTax4() As String
        Dim item() As String = {"__item__"}
        Dim tax() As String = {"__taxGrp__"}
        Dim objCateProduct As New iTECH.InbizERP.BusinessLogic.CategoryWithProduct
        strTabNote += objCateProduct.GetTransitionTableNote()

        Try
            objTransaction.Tax4 = 0
            objTransaction.Tax3 = 0
            objTransaction.Tax2 = 0
            objTransaction.Tax1 = 0
            Dim strTaxCal() As String = strTax.Split("^")
            If strTaxCal.Length >= 1 Then
                strTax1 = strTaxCal(0).Split("~")
                If strTax1.Length > 1 Then
                    objTransaction.Tax1 = strTax1(1)
                    objTransaction.Tax1Desc = strTax1(0)
                End If
            End If
            If strTaxCal.Length >= 2 Then
                strTax2 = strTaxCal(1).Split("~")
                If strTax2.Length > 1 Then
                    objTransaction.Tax2 = strTax2(1)
                    objTransaction.Tax2Desc = strTax2(0)
                End If
            End If
            If strTaxCal.Length >= 3 Then
                strTax3 = strTaxCal(2).Split("~")
                If strTax3.Length > 1 Then
                    objTransaction.Tax3 = strTax3(1)
                    objTransaction.Tax3Desc = strTax3(0)
                End If
            End If
            If strTaxCal.Length >= 4 Then
                strTax4 = strTaxCal(3).Split("~")
                If strTax4.Length > 1 Then
                    objTransaction.Tax4 = strTax4(1)
                    objTransaction.Tax4Desc = strTax4(0)
                End If
            End If

            objTransaction.TransStatus = 1

            If strTransType = 1 Then
                objPosTranAccHrd.AcctPayType = 1
            ElseIf strTransType = 7 Then  'Condition for lost transaction
                objPosTranAccHrd.AcctPayType = 7
                objTransaction.TransStatus = 0
            ElseIf strTransType = 8 Then  'Condition for Gift transaction
                objPosTranAccHrd.AcctPayType = 8
                'objTransaction.TransStatus = 0
            ElseIf strTransType = 9 Then  'Condition for Staff Gift transaction
                objPosTranAccHrd.AcctPayType = 9
                'objTransaction.TransStatus = 0
            ElseIf strTransType = 6 Then
                objPosTranAccHrd.AcctPayType = 1
            End If

            If strTransType = 2 Then
                objPosTranAccHrd.AcctPayType = 2
            ElseIf strTransType = 3 Then
                objPosTranAccHrd.AcctPayType = 3
            ElseIf strTransType = 4 Then
                objPosTranAccHrd.AcctPayType = 4
            ElseIf strTransType = 5 Then
                objPosTranAccHrd.AcctPayType = 5
            End If


            '***************************** Check for Cancel/tab Button
            If strBtnType.ToLower = "cancel" Then
                objTransaction.TransStatus = 0
            ElseIf strBtnType.ToLower = "tab" Then
                objTransaction.PosTabNote = strTabNote
                objTransaction.TransStatus = 9
            End If

            objPosTranAccHrd.CashReturned = strReturnAmt
            objPosTranAccHrd.CasReceived = strReceivedAmt

            If strTransType = 8 OrElse strTransType = 9 Then
                objTransaction.PosPercentDiscount = 100.0F

                objPosTranAccHrd.CashReturned = 0
                objPosTranAccHrd.CasReceived = 0
            End If
            'objPosTranAccHrd.CashReturned = strReturnAmt
            'objPosTranAccHrd.CasReceived = strReceivedAmt

            objTransaction.TransUserID = strUserID
            objTransaction.TransRegCode = strRegCode
            objTransaction.TransType = "S"
            objTransaction.TransDateTime = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
            objTransaction.TotalValue = strTotalAmt
            objTransaction.SubTotal = strTotalSubAmt
            objTransaction.OldTransID = "sysNull"
            objTransaction.PosGiftReason = "sysNull"
            objTransaction.PosAuthorizedBy = "sysNull"

            '***************************** Insert in Transaction Table
            objTransaction.funInsertPosTransaction(strTranID)
            objPosTranAccHrd.AcctTransID = strTranID

            If String.IsNullOrEmpty(strCustPickUpDate) Then
                strCustPickUpDate = Date.Now.AddMinutes(15).ToString("yyyy-MM-dd HH:mm")
            End If
            objTransaction.funInsertPosTransactionCutomerDetails(strTranID, strCustPhoneNo, strCustEmailID, strCustPickUpDate, strCustAdditionalRequest, strUserID)
            If strBtnType = "" Then
                '***************************** Insert in postransaccthdr Table
                Dim strTranAccHrdID As String = String.Empty
                objPosTranAccHrd.funInsertpostransaccthdr(strTranAccHrdID)
            Else
                'Added By Hitendra On 2011-12-27
                'Following statemts needs to hard code to make card transaction equivelent to Cash Trasaction only difference will be TrasactionType
                Dim strTranAccHrdID As String = String.Empty
                objPosTranAccHrd.funInsertpostransaccthdr(strTranAccHrdID)
            End If


            '***************************** Insert in posproductno
            Dim strPrd() As String = strProductDtl.Split(item, System.StringSplitOptions.None)
            Dim strTaxOnPrd() As String
            Dim strTaxValue() As String
            Dim intI = 0, intR As Integer = 0
            Dim strPrdItem() As String
            objPosPrdNo.PrdTransID = strTranID
            While strPrd.Length - 1 > intI
                strTaxValue = strPrd(intI).Split(tax, System.StringSplitOptions.None)
                strPrdItem = strTaxValue(0).Split("~")
                If strPrdItem.Length > 1 Then
                    objPosPrdNo.ProductID = strPrdItem(0)
                    objPosPrdNo.PrdQty = strPrdItem(1)
                    objPosPrdNo.PrdUnitPrice = strPrdItem(2)
                    objPosPrdNo.PrdPrice = strPrdItem(3)
                    objPosPrdNo.Tax4 = 0
                    objPosPrdNo.Tax3 = 0
                    objPosPrdNo.Tax2 = 0
                    objPosPrdNo.Tax1 = 0
                    If strTaxValue.Length > 1 Then
                        strTaxOnPrd = strTaxValue(1).Split("^")
                        While strTaxOnPrd.Length - 1 > intR
                            strTax1 = strTaxOnPrd(intR).Split("~")
                            If strTax1.Length > 1 Then
                                If intR = 0 Then
                                    objPosPrdNo.Tax1 = strTax1(1)
                                    objPosPrdNo.Tax1Desc = strTax1(0)
                                ElseIf intR = 1 Then
                                    objPosPrdNo.Tax2 = strTax1(1)
                                    objPosPrdNo.Tax2Desc = strTax1(0)
                                ElseIf intR = 2 Then
                                    objPosPrdNo.Tax3 = strTax1(1)
                                    objPosPrdNo.Tax3Desc = strTax1(0)
                                ElseIf intR = 3 Then
                                    objPosPrdNo.Tax4 = strTax1(1)
                                    objPosPrdNo.Tax4Desc = strTax1(0)
                                End If
                            End If
                            intR += 1
                        End While
                    End If
                    intR = 0
                    objPosPrdNo.PrdTaxCode = objTransaction.funPrdTax(strPrdItem(0), strWhshouse)
                    objPosPrdNo.funInsertPosProduct()
                    If strBtnType = "" Then
                        If objProduct.funGetPrdValue(objPosPrdNo.ProductID, "kit") = "1" Then
                            objPrdQty.subGetPrdKitOnHandQty(objPosPrdNo.ProductID, strWhshouse, objPosPrdNo.PrdQty) 'reduce kit's product Qty
                            objPrdQty.updateProductQuantity(objPosPrdNo.ProductID, strWhshouse, objPosPrdNo.PrdQty) ' reduce prd kit Qty
                        Else
                            objPrdQty.updateProductQuantity(objPosPrdNo.ProductID, strWhshouse, objPosPrdNo.PrdQty)
                            objPrdQty.funGetReducePrdKitQty(objPosPrdNo.ProductID, strWhshouse, "") ' reduce prd kit Qty
                        End If
                    End If
                End If
                intI += 1
            End While
            '**************************** Condition for Tab
            If strPreviousTranID <> "" Then
                objTransaction.TransStatus = 0
                objTransaction.funUpdateTransStaus(strPreviousTranID)
            End If
            ''*************************** Return value
            If strBtnType = "" Then
                If strTransType = 7 Then
                    strvalue = msgTransaction & " " & strTranID & " has been Lost." & "^" & strTranID
                ElseIf strTransType = 8 Then 'Gift Transation Added By Hitendra
                    strvalue = msgTransaction & " " & strTranID & " has been gifted to guest." & "^" & strTranID
                ElseIf strTransType = 9 Then 'Staff Gift Transation Added By Hitendra
                    strvalue = msgTransaction & " " & strTranID & " has been gifted to staff." & "^" & strTranID
                Else
                    strvalue = msgTransaction & " " & strTranID & " " & msgCompletedSuccess & "[br]" & POSTotalAmount & strTotalAmt & ", " & POSReceived & strReceivedAmt & ", " & POSBalance & strReturnAmt & "^" & strTranID
                End If
            ElseIf strBtnType.ToLower = "cancel" Then
                strvalue = msgTrCanceled
            ElseIf strBtnType.ToLower = "tab" Then
                strvalue = msgTrdPausedSuccessfully
            End If

            'Added By Hitendra On 2011-12-27
            'Following statemts needs to hard code to make card transaction equivelent to Cash Trasaction only difference will be TrasactionType
            Try
                If String.IsNullOrEmpty(strvalue.Trim()) Then
                    strvalue = msgTransaction & " " & strTranID & " " & msgCompletedSuccess & "[br]" & POSTotalAmount & strTotalAmt & ", " & POSReceived & strReceivedAmt & ", " & POSBalance & strReturnAmt & "^" & strTranID
                End If
            Catch ex As Exception

            End Try

            'strvalue = funAddXMLTag(strvalue + " Your token is " + strTabNote)
            'strvalue = funAddXMLTag(strTabNote)
            strvalue = funAddXMLTag(strTabNote + "&transID=" + strTranID)
            Return strvalue
        Catch ex As Exception
            Return "__error__" & "POSCashTransactionFailed"
        Finally
            objTransaction = Nothing
            objPosPrdNo = Nothing
            objPosTranAccHrd = Nothing
            objTransDtl = Nothing
            objProduct = Nothing
            objPrdQty = Nothing
            item = Nothing
            tax = Nothing
        End Try
    End Function

End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports System.Web.HttpContext
Imports clsCommon
Public Class clsOrderItemProcess
	Inherits clsDataClass
    Private _orderItemProcID, _ordID, _ordItemProcCode, _ordItemProcFixedPrice, _ordItemProcPricePerHour, _ordItemProcHours, _ordItemProcPricePerUnit, _ordItemProcUnits, _sysTaxCodeDescID As String
	Public Property OrderItemProcID() As String
		Get
			Return _orderItemProcID
		End Get
		Set(ByVal value As String)
			_orderItemProcID = funRemove(value, True)
		End Set
	End Property
	Public Property OrdID() As String
		Get
			Return _ordID
		End Get
		Set(ByVal value As String)
			_ordID = funRemove(value, True)
		End Set
    End Property
    Public Property sysTaxCodeDescID() As String
        Get
            Return _sysTaxCodeDescID
        End Get
        Set(ByVal value As String)
            _sysTaxCodeDescID = funRemove(value, True)
        End Set
    End Property
	Public Property OrdItemProcCode() As String
		Get
			Return _ordItemProcCode
		End Get
		Set(ByVal value As String)
			_ordItemProcCode = funRemove(value, True)
		End Set
	End Property
	Public Property OrdItemProcFixedPrice() As String
		Get
			Return _ordItemProcFixedPrice
		End Get
		Set(ByVal value As String)
			_ordItemProcFixedPrice = funRemove(value, True)
		End Set
	End Property
	Public Property OrdItemProcPricePerHour() As String
		Get
			Return _ordItemProcPricePerHour
		End Get
		Set(ByVal value As String)
			_ordItemProcPricePerHour = funRemove(value, True)
		End Set
	End Property
	Public Property OrdItemProcHours() As String
		Get
			Return _ordItemProcHours
		End Get
		Set(ByVal value As String)
			_ordItemProcHours = funRemove(value, True)
		End Set
    End Property
    Public Property OrdItemProcPricePerUnit() As String
        Get
            Return _ordItemProcPricePerUnit
        End Get
        Set(ByVal value As String)
            _ordItemProcPricePerUnit = funRemove(value, True)
        End Set
    End Property
    Public Property OrdItemProcUnits() As String
        Get
            Return _ordItemProcUnits
        End Get
        Set(ByVal value As String)
            _ordItemProcUnits = funRemove(value, True)
        End Set
    End Property
	Public Sub New()
		MyBase.New()
	End Sub
	' Insert OrderItemProcess
	Public Function insertOrderItemProcess() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO orderitemprocess( ordID, ordItemProcCode, ordItemProcFixedPrice, ordItemProcPricePerHour, ordItemProcHours, ordItemProcPricePerUnit, ordItemProcUnits,sysTaxCodeDescID) VALUES('"
		strSQL += _ordID + "','"
		strSQL += _ordItemProcCode + "','"
		strSQL += _ordItemProcFixedPrice + "','"
		strSQL += _ordItemProcPricePerHour + "','"
        strSQL += _ordItemProcHours + "','"
        strSQL += _ordItemProcPricePerUnit + "','"
        strSQL += _ordItemProcUnits + "','"
        strSQL += _sysTaxCodeDescID + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
	' Update OrderItemProcess
	Public Function updateOrderItemProcess() As Boolean
		Dim strSQL As String
        strSQL = "UPDATE orderitemprocess set "
		strSQL += "ordID='" + _ordID + "', "
		strSQL += "ordItemProcCode='" + _ordItemProcCode + "', "
		strSQL += "ordItemProcFixedPrice='" + _ordItemProcFixedPrice + "', "
		strSQL += "ordItemProcPricePerHour='" + _ordItemProcPricePerHour + "', "
        strSQL += "ordItemProcHours='" + _ordItemProcHours + "', "
        strSQL += "ordItemProcPricePerUnit='" + _ordItemProcPricePerUnit + "', "
        strSQL += "ordItemProcUnits='" + _ordItemProcUnits + "', "
        strSQL += "sysTaxCodeDescID='" + _sysTaxCodeDescID + "'"
		strSQL += " where orderItemProcID='" + _orderItemProcID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
	' Populate objects of OrderItemProcess
	Public Sub getOrderItemProcessInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
        strSQL = "SELECT ordID, ordItemProcCode, ordItemProcFixedPrice, ordItemProcPricePerHour, ordItemProcHours, ordItemProcPricePerUnit, ordItemProcUnits, sysTaxCodeDescID FROM orderitemprocess where orderItemProcID='" + _orderItemProcID + "' "
		drObj = GetDataReader(strSql)
		While drObj.Read
			_ordID = drObj.Item("ordID").ToString
			_ordItemProcCode = drObj.Item("ordItemProcCode").ToString
			_ordItemProcFixedPrice = drObj.Item("ordItemProcFixedPrice").ToString
			_ordItemProcPricePerHour = drObj.Item("ordItemProcPricePerHour").ToString
            _ordItemProcHours = drObj.Item("ordItemProcHours").ToString
            _ordItemProcPricePerUnit = drObj.Item("ordItemProcPricePerUnit").ToString
            _ordItemProcUnits = drObj.Item("ordItemProcUnits").ToString
            _sysTaxCodeDescID = drObj.Item("sysTaxCodeDescID").ToString
		End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ' Fill grid 
    Public Function funFillGridForProcess() As String
        Dim strSQL As String
        strSQL = " SELECT orderItemProcID, i.ordID, ordItemProcCode, ProcessDescription, (ordItemProcFixedPrice*o.ordCurrencyExRate) as ordItemProcFixedPrice, (ordItemProcPricePerHour*o.ordCurrencyExRate) as ordItemProcPricePerHour,  OrdItemProcFixedPrice as InOrdItemProcFixedPrice,ordItemProcPricePerHour as InOrdItemProcPricePerHour,"
        strSQL += " ordItemProcPricePerUnit as InOrdItemProcPricePerUnit, ordItemProcHours, (ordItemProcPricePerUnit*o.ordCurrencyExRate) as ordItemProcPricePerUnit, ordItemProcUnits, o.ordCurrencyExRate*(ordItemProcFixedPrice+(ordItemProcPricePerHour*ordItemProcHours)+(ordItemProcPricePerUnit*ordItemProcUnits)) as ProcessCost , IFNULL(systaxcodedesc.sysTaxCodeDescID,0) as sysTaxCodeDescID, IFNULL(systaxcodedesc.sysTaxCodeDescText, ""Warehous Tax"") as sysTaxCodeDescText"
        strSQL += " FROM orderitemprocess i inner join orders o on o.ordID=i.ordID inner join sysprocessgroup on ProcessCode=ordItemProcCode"
        strSQL += " Left outer  join systaxcodedesc on i.sysTaxCodeDescID = systaxcodedesc.sysTaxCodeDescID where i.ordID='" & _ordID & "' "
        strSQL += " order by ProcessDescription"

        'strSQL = "SELECT orderItemProcID, i.ordID, ordItemProcCode, ProcessDescription, (ordItemProcFixedPrice*o.ordCurrencyExRate) as ordItemProcFixedPrice, (ordItemProcPricePerHour*o.ordCurrencyExRate) as ordItemProcPricePerHour, "
        'strSQL += " OrdItemProcFixedPrice as InOrdItemProcFixedPrice,ordItemProcPricePerHour as InOrdItemProcPricePerHour,ordItemProcPricePerUnit as InOrdItemProcPricePerUnit,"
        'strSQL += " ordItemProcHours, (ordItemProcPricePerUnit*o.ordCurrencyExRate) as ordItemProcPricePerUnit, ordItemProcUnits, o.ordCurrencyExRate*(ordItemProcFixedPrice+(ordItemProcPricePerHour*ordItemProcHours)+(ordItemProcPricePerUnit*ordItemProcUnits)) as ProcessCost "
        'strSQL += " FROM orderitemprocess i inner join orders o on o.ordID=i.ordID inner join sysprocessgroup on ProcessCode=ordItemProcCode where i.ordID='" & _ordID & "' "
        'strSQL += " order by ProcessDescription "
        Return strSQL
    End Function
    'Get Process Cost For Order No
    Public Function getProcessCostForOrderNo() As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT sum(ordItemProcFixedPrice+(ordItemProcPricePerHour*ordItemProcHours)+(ordItemProcPricePerUnit*ordItemProcUnits))*o.ordCurrencyExRate as ProcessCost FROM orderitemprocess i inner join orders o on o.ordID=i.ordID where i.ordID='" & _ordID & "' group by i.ordID "
        Try
            strData = GetScalarData(strSQL).ToString
        Catch ex As NullReferenceException
            strData = "0"
        Catch ex As Exception
            strData = "0"
        End Try
        Return strData
    End Function
    ' Delete Sales Order Item Process
    Public Function funDeleteSOItemProcess() As String
        Dim strSQL As String
        strSQL = "DELETE FROM orderitemprocess WHERE orderItemProcID='" + _orderItemProcID + "' "
        Return strSQL
    End Function

    ' Duplicate Docno
    Public Function funDuplicateProcess() As Boolean
        Dim strSql As String
        strSql = "SELECT count(*) FROM orderitemprocess "
        strSql += " where ordItemProcCode='" + _ordItemProcCode + "' and ordID='" + _ordID + "' "
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.GetScalarData(strSql)
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Created By :: Ankit Choure
    ''' Created Date :: 19-4-2011
    ''' </summary>
    ''' <param name="Taxcode"> Tax Code for Deatils </param>
    ''' <returns>its Return the Datatable with Tax Code details </returns>
    ''' <remarks></remarks>
    Public Function funGetTaxDetailByTaxCode(ByVal Taxcode As Integer) As DataTable
        Dim strSql As String
        strSql = "SELECT s.sysTaxCode, s.sysTaxDesc, s.sysTaxPercentage, s.sysTaxSequence, s.sysTaxOnTotal FROM inbizdemo.systaxcode s"
        strSql += "  Where sysTaxCode='" + Taxcode.ToString() + "' "
        Dim obj As New clsDataClass
        Dim dtTaxCodeDeatils As DataTable
        dtTaxCodeDeatils = obj.GetDataTable(strSql)
        Return dtTaxCodeDeatils
    End Function
End Class

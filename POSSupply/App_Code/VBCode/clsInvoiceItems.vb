Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsInvoiceItems
	Inherits clsDataClass
    Private _invItemID, _invoices_invID, _invProductID, _invProductQty, _invProductUnitPrice, _invProductDiscount, _invProductTaxGrp, _invProdIDDesc, _invProductDiscountType As String
    Public Property InvItemID() As String
        Get
            Return _invItemID
        End Get
        Set(ByVal value As String)
            _invItemID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Invoices_invID() As String
        Get
            Return _invoices_invID
        End Get
        Set(ByVal value As String)
            _invoices_invID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvProductID() As String
        Get
            Return _invProductID
        End Get
        Set(ByVal value As String)
            _invProductID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvProductQty() As String
        Get
            Return _invProductQty
        End Get
        Set(ByVal value As String)
            _invProductQty = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvProductUnitPrice() As String
        Get
            Return _invProductUnitPrice
        End Get
        Set(ByVal value As String)
            _invProductUnitPrice = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property invProductDiscount() As String
        Get
            Return _invProductDiscount
        End Get
        Set(ByVal value As String)
            _invProductDiscount = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property invProductTaxGrp() As String
        Get
            Return _invProductTaxGrp
        End Get
        Set(ByVal value As String)
            _invProductTaxGrp = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvProdIDDesc() As String
        Get
            Return _invProdIDDesc
        End Get
        Set(ByVal value As String)
            _invProdIDDesc = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property invProductDiscountType() As String
        Get
            Return _invProductDiscountType
        End Get
        Set(ByVal value As String)
            _invProductDiscountType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Insert InvoiceItems
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertInvoiceItems() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO invoiceitems(invoices_invID, invProductID, invProductQty, invProductUnitPrice,invProductDiscount,invProductTaxGrp, invProdIDDesc,invProductDiscountType) VALUES('"
        strSQL += _invoices_invID + "','"
        strSQL += _invProductID + "','"
        strSQL += _invProductQty + "','"
        strSQL += _invProductUnitPrice + "','"
        strSQL += _invProductDiscount + "','"
        strSQL += _invProductTaxGrp + "','"
        strSQL += _invProdIDDesc + "','"
        strSQL += _invProductDiscountType + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update InvoiceItems
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateInvoiceItems() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE invoiceitems set "
        strSQL += "invoices_invID='" + _invoices_invID + "', "
        strSQL += "invProductID='" + _invProductID + "', "
        strSQL += "invProductQty='" + _invProductQty + "', "
        strSQL += "invProductUnitPrice='" + _invProductUnitPrice + "',"
        strSQL += "invProductDiscount='" + _invProductDiscount + "', "
        strSQL += "invProductTaxGrp='" + _invProductTaxGrp + "', "
        strSQL += "invProdIDDesc='" + _invProdIDDesc + "', "
        strSQL += "invProductDiscountType='" + _invProductDiscountType + "' "
        strSQL += " where invItemID='" + _invItemID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of InvoiceItems
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getInvoiceItemsInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  invoices_invID, invProductID, invProductQty, invProductUnitPrice,invProductDiscount,invProductTaxGrp, invProdIDDesc,invProductDiscountType FROM invoiceitems where invItemID='" + _invItemID + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _invoices_invID = drObj.Item("invoices_invID").ToString
            _invProductID = drObj.Item("invProductID").ToString
            _invProductQty = drObj.Item("invProductQty").ToString
            _invProductUnitPrice = drObj.Item("invProductUnitPrice").ToString
            _invProductDiscount = drObj.Item("invProductDiscount").ToString
            _invProductTaxGrp = drObj.Item("invProductTaxGrp").ToString
            _invProdIDDesc = drObj.Item("invProdIDDesc").ToString
            _invProductDiscountType = drObj.Item("invProductDiscountType").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate Invoice Item Object and Insert
    ''' </summary>
    ''' <param name="SOID"></param>
    ''' <param name="sInvID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funPopulateInvoiceItem(ByVal SOID As String, ByVal sInvID As String) As Boolean
        Dim bFlag As Boolean = False
        Dim strSQL As String = ""
        Dim drObj As OdbcDataReader
        Dim objSOItems As New clsOrderItems
        strSQL = objSOItems.funFillGrid(SOID)  'fill Order Items
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _invoices_invID = sInvID
            _invProductID = drObj.Item("ordProductID").ToString
            _invProductQty = drObj.Item("ordProductQty").ToString
            _invProductUnitPrice = drObj.Item("InOrdProductUnitPrice").ToString
            _invProductDiscount = drObj.Item("ordProductDiscount").ToString
            invProductTaxGrp = drObj.Item("ordProductTaxGrp").ToString
            InvProdIDDesc = drObj.Item("prdName")
            _invProductDiscountType = drObj.Item("ordProductDiscountType").ToString
            bFlag = insertInvoiceItems()
        End While
        drObj.Close()
        CloseDatabaseConnection()
        Return bFlag
    End Function
    ''' <summary>
    ''' Fill Grid of Invoice Items
    ''' </summary>
    ''' <param name="INID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal INID As String) As String
        Dim strSQL As String
        strSQL = "SELECT i.invItemID, i.invProductID,i.invProductDiscount,i.invProductTaxGrp,sysTaxCodeDescText, p.prdIntID, p.prdExtID, p.prdUPCCode, CASE i.invProdIDDesc='' WHEN False THEN i.invProdIDDesc Else p.prdName END as prdName, invProductQty, (invProductUnitPrice * invCurrencyExRate) as invProductUnitPrice, invCurrencyCode, "
        strSQL += " case i.invProductDiscountType when 'A' then (invProductQty * invProductUnitPrice * invCurrencyExRate) - i.invProductDiscount else  CASE i.invProductDiscount WHEN 0 THEN (invProductQty * invProductUnitPrice * invCurrencyExRate) ELSE (invProductQty * invProductUnitPrice * invCurrencyExRate) - ((invProductQty * invProductUnitPrice * invCurrencyExRate) * i.invProductDiscount/100) END End  as amount, "
        strSQL += " invShpWhsCode, invCustID, invCustType,i.invProductDiscountType FROM invoiceitems i inner join invoices o on o.invID=i.invoices_invID inner join products p on i.invProductID=p.productID left join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=i.invProductTaxGrp where i.invoices_invID='" & INID & "'"
        Return strSQL
    End Function
    ''' <summary>
    '''  Get the dicount amount
    ''' </summary>
    ''' <param name="INID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetDiscount(ByVal INID As String) As Boolean
        Dim strSQL As String
        strSQL = "SELECT invProductDiscount FROM invoiceitems where invoices_invID='" & INID & "'"
        Dim drObj As OdbcDataReader
        drObj = GetDataReader(strSQL)
        While drObj.Read
            If drObj.Item("invProductDiscount").ToString <> "0" Then
                drObj.Close()
                CloseDatabaseConnection()
                Return True
            End If
        End While
        drObj.Close()
        CloseDatabaseConnection()
        Return False
    End Function
    ''' <summary>
    ''' Delete Invoice Order Items
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteInvItems() As String
        Dim strSQL As String
        strSQL = "DELETE FROM invoiceitems WHERE invItemID='" + _invItemID + "' "
        GetScalarData(strSQL)
        Return strSQL
    End Function
    ''' <summary>
    ''' Count Invoice Order Items
    ''' </summary>
    ''' <param name="invID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCountSOItems(ByVal invID As String) As Integer
        Dim strSQL As String
        strSQL = "select count(*) FROM invoiceitems WHERE invoices_invID='" + invID + "' "
        Return GetScalarData(strSQL)
    End Function
End Class

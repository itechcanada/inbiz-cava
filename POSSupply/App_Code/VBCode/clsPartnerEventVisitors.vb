Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsPartnerEventVisitors
	Inherits clsDataClass
	Private _EventVistorID, _VisitorDescLang, _VisitorDesc, _EventVistorActive As String
	Public Property EventVistorID() As String
		Get
			Return _EventVistorID
		End Get
		Set(ByVal value As String)
            _EventVistorID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property VisitorDescLang() As String
		Get
			Return _VisitorDescLang
		End Get
		Set(ByVal value As String)
            _VisitorDescLang = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property VisitorDesc() As String
		Get
			Return _VisitorDesc
		End Get
		Set(ByVal value As String)
            _VisitorDesc = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property EventVistorActive() As String
		Get
			Return _EventVistorActive
		End Get
		Set(ByVal value As String)
            _EventVistorActive = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert PartnerEventVisitors
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertPartnerEventVisitors() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO partnereventvisitors( VisitorDescLang, VisitorDesc, EventVistorActive) VALUES('"
		strSQL += _VisitorDescLang + "','"
		strSQL += _VisitorDesc + "','"
		strSQL += _EventVistorActive + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    '''  Update PartnerEventVisitors
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updatePartnerEventVisitors() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE partnereventvisitors set "
		strSQL += "VisitorDescLang='" + _VisitorDescLang + "', "
		strSQL += "VisitorDesc='" + _VisitorDesc + "', "
		strSQL += "EventVistorActive='" + _EventVistorActive + "'"
		strSQL += " where EventVistorID='" + _EventVistorID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of PartnerEventVisitors
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPartnerEventVisitorsInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  VisitorDescLang, VisitorDesc, EventVistorActive FROM partnereventvisitors where EventVistorID='" + _EventVistorID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_VisitorDescLang = drObj.Item("VisitorDescLang").ToString
			_VisitorDesc = drObj.Item("VisitorDesc").ToString
			_EventVistorActive = drObj.Item("EventVistorActive").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill Grid of EventVisitors
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String = ""
        Dim strLang As String = clsCommon.funGetProductLang() 
        strSQL = "SELECT EventVistorID, VisitorDescLang, VisitorDesc, EventVistorActive FROM partnereventvisitors where VisitorDescLang='" & strLang & "' "
        strSQL += " order by EventVistorID "
        Return strSQL
    End Function
End Class

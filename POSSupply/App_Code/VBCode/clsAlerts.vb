Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsAlerts
	Inherits clsDataClass
    Private _AlertID, _AlertPartnerContactID, _AlertComID, _AlertDateTime, _AlertUserID, _AlertNote, _AlertRefType As String
	Public Property AlertID() As String
		Get
			Return _AlertID
		End Get
		Set(ByVal value As String)
            _AlertID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property AlertPartnerContactID() As String
		Get
			Return _AlertPartnerContactID
		End Get
		Set(ByVal value As String)
            _AlertPartnerContactID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property AlertComID() As String
		Get
			Return _AlertComID
		End Get
		Set(ByVal value As String)
            _AlertComID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property AlertDateTime() As String
		Get
			Return _AlertDateTime
		End Get
		Set(ByVal value As String)
            _AlertDateTime = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property AlertUserID() As String
		Get
			Return _AlertUserID
		End Get
		Set(ByVal value As String)
            _AlertUserID = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property AlertNote() As String
        Get
            Return _AlertNote
        End Get
        Set(ByVal value As String)
            _AlertNote = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property AlertRefType() As String
        Get
            Return _AlertRefType
        End Get
        Set(ByVal value As String)
            _AlertRefType = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    '''  Insert Alerts
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertAlerts() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO alerts( AlertPartnerContactID, AlertComID, AlertDateTime, AlertUserID, AlertNote, AlertRefType) VALUES('"
		strSQL += _AlertPartnerContactID + "','"
		strSQL += _AlertComID + "','"
        strSQL += _AlertDateTime + "','"
        strSQL += _AlertUserID + "','"
        strSQL += _AlertNote + "','"
        strSQL += _AlertRefType + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    '''  Update Alerts
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateAlerts() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE alerts set "
		strSQL += "AlertPartnerContactID='" + _AlertPartnerContactID + "', "
		strSQL += "AlertComID='" + _AlertComID + "', "
        strSQL += "AlertDateTime='" + _AlertDateTime + "', "
        strSQL += "AlertUserID='" + _AlertUserID + "', "
        strSQL += "AlertNote='" + _AlertNote + "'"
        strSQL += "AlertRefType='" + _AlertRefType + "'"
		strSQL += " where AlertID='" + _AlertID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of Alerts
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getAlertsInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
        strSQL = "SELECT  AlertPartnerContactID, AlertComID, AlertDateTime, AlertUserID, AlertNote, AlertRefType FROM alerts where AlertID='" + _AlertID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_AlertPartnerContactID = drObj.Item("AlertPartnerContactID").ToString
			_AlertComID = drObj.Item("AlertComID").ToString
			_AlertDateTime = drObj.Item("AlertDateTime").ToString
            _AlertUserID = drObj.Item("AlertUserID").ToString
            _AlertNote = drObj.Item("AlertNote").ToString
            _AlertRefType = drObj.Item("AlertRefType").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Delete Alert
    ''' </summary>
    ''' <param name="strDeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteAlert(ByVal strDeleteID As String) As String
        Dim strSql As String = ""
        strSql = "DELETE from alerts where AlertID ='" & strDeleteID & "' "
        Return strSql
    End Function
    ''' <summary>
    '''  Fill Grid of Alert
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String = ""
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT Distinct AlertID, AlertPartnerContactID, AlertComID, AlertDateTime, AlertUserID, AlertNote, Concat(ContactLastName,' ',ifnull(ContactFirstName,'')) as ContactName,AlertRefType FROM alerts left join contactcommunication cc on cc.ComContactID=AlertPartnerContactID left join partnercontacts pc on pc.ContactID=AlertPartnerContactID "
        Else
            strSQL = "SELECT Distinct AlertID, AlertPartnerContactID, AlertComID, AlertDateTime, AlertUserID, AlertNote, Concat(ContactLastName,' ',ifnull(ContactFirstName,'')) as ContactName,AlertRefType FROM alerts left join contactcommunication cc on cc.ComContactID=AlertPartnerContactID left join partnercontacts pc on pc.ContactID=AlertPartnerContactID inner join salesrepcustomer s on s.CustomerID=pc.ContactPartnerID  and s.UserID='" & HttpContext.Current.Session("UserID") & "' "
        End If

        strSQL += " order by AlertDateTime desc "
        Return strSQL
    End Function
    ''' <summary>
    '''  Fill Grid of Alert for Comm ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGridForCommID() As String
        Dim strSQL As String = ""
        strSQL = "SELECT Distinct AlertID, AlertPartnerContactID, AlertComID, AlertDateTime, AlertUserID, AlertNote, Concat(ContactLastName,' ',ifnull(ContactFirstName,'')) as ContactName FROM alerts left join contactcommunication cc on cc.ComContactID=AlertPartnerContactID inner join partnercontacts pc on pc.ContactID=AlertPartnerContactID "
        strSQL += " where AlertComID='" & _AlertComID & "' "
        strSQL += " order by AlertDateTime desc "
        Return strSQL
    End Function
    ''' <summary>
    ''' Get Alert DateTime for Comm ID
    ''' </summary>
    ''' <param name="sCommID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetAlertDateTime(ByVal sCommID As String) As String
        Dim strSQL As String = ""
        strSQL = "SELECT AlertDateTime FROM alerts "
        strSQL += " where AlertComID='" & sCommID & "' "
        strSQL += " order by AlertDateTime desc limit 1 "
        Return GetScalarData(strSQL)
    End Function
End Class

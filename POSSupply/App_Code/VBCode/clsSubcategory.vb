Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.Odbc
'Imports clsCommon
'Imports System.Web.HttpContext

Public Class clsSubcategory
    Inherits clsDataClass

#Region "Private variables"
    Private intSubcatId As String, intSubcatCatId As String, strSubcatName As String
    Private strSubcatImagePath As String, intSubcatIsActive As String, intSubcatUpdatedBy As String
    Private dateSubcatUpdatedOn As String
    Private intcatWebSeq As String, tintcatStatus As String, _subcatdescLang As String, _eBayCatgNo As String
#End Region

#Region "Properties"
    Public Property SubcategoryID() As String
        Get
            Return intSubcatId
        End Get
        Set(ByVal value As String)
            intSubcatId = value
        End Set
    End Property
    Public Property subcatdescLang() As String
        Get
            Return _subcatdescLang
        End Get
        Set(ByVal value As String)
            _subcatdescLang = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property SubcategoryCatID() As String
        Get
            Return intSubcatCatId
        End Get
        Set(ByVal value As String)
            intSubcatCatId = value
        End Set
    End Property
    Public Property SubcategoryName() As String
        Get
            Return strSubcatName
        End Get
        Set(ByVal value As String)
            strSubcatName = value
        End Set
    End Property
    Public Property SubcategoryImagepath() As String
        Get
            Return strSubcatImagePath
        End Get
        Set(ByVal value As String)
            strSubcatImagePath = value
        End Set
    End Property
    Public Property SubcategoryIsActive() As String
        Get
            Return intSubcatIsActive
        End Get
        Set(ByVal value As String)
            intSubcatIsActive = value
        End Set
    End Property
    Public Property SubcategoryUpdatedby() As String
        Get
            Return intSubcatUpdatedBy
        End Get
        Set(ByVal value As String)
            intSubcatUpdatedBy = value
        End Set
    End Property
    Public Property SubcategoryUpdatedon() As String
        Get
            Return dateSubcatUpdatedOn
        End Get
        Set(ByVal value As String)
            dateSubcatUpdatedOn = value
        End Set
    End Property
    Public Property SubcategoryWebSequence() As String
        Get
            Return intcatWebSeq
        End Get
        Set(ByVal value As String)
            intcatWebSeq = value
        End Set
    End Property
    Public Property SubcategoryStatus() As String
        Get
            Return tintcatStatus
        End Get
        Set(ByVal value As String)
            tintcatStatus = value
        End Set
    End Property
    Public Property eBayCatgNo() As String
        Get
            Return _eBayCatgNo
        End Get
        Set(ByVal value As String)
            _eBayCatgNo = value
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub

#Region "Functions"
    ''' <summary>
    ''' Get Sub-Category Information
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetSubCategoryInfo()
        Dim objDataClass As New clsDataClass
        Dim dbreader As OdbcDataReader
        Dim strSql As String
        objDataClass.OpenDatabaseConnection()
        strSql = "SELECT * FROM subcategory where subcatId= '" & intSubcatId & "'"
        dbreader = objDataClass.GetDataReader(strSql)
        While dbreader.Read
            intcatWebSeq = dbreader.Item("subcatWebSeq").ToString
        End While
    End Sub
    ''' <summary>
    ''' Find sub-Category Name
    ''' </summary>
    ''' <param name="strSubID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDuplicateSubCategory(Optional ByVal strSubID As String = "") As Boolean
        Dim strSQL As String
        Dim objDataClass As New clsDataClass
        If strSubID <> "" Then
            strSQL = "SELECT count(*) FROM subcategory where subcatName='" & strSubcatName & "' And subcatStatus='1' And subcatCatId='" & intSubcatCatId & "' and subcatId <>'" & strSubID & "' and subcatdescLang = '" + clsCommon.funGetProductLang() + "'"
        Else
            strSQL = "SELECT count(*) FROM subcategory where subcatName='" & strSubcatName & "' And subcatCatId='" & intSubcatCatId & "' And subcatStatus='1' and subcatdescLang = '" + clsCommon.funGetProductLang() + "'"
        End If
        If objDataClass.GetScalarData(strSQL) <> 0 Then
            objDataClass.OpenDatabaseConnection()
            objDataClass.CloseDatabaseConnection()
            objDataClass = Nothing
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Return Pending subCategory Request 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funPendingSubCategoryReq() As String
        Dim objDataClass As New clsDataClass
        Dim strSql, strSubCatCount As String
        objDataClass.OpenDatabaseConnection()
        strSql = "SELECT Count(*) FROM subcategory inner join category on subcategory.subcatCatid=category.catid where subcatIsActive='2' and subcatStatus='1' And catStatus='1' "
        strSubCatCount = objDataClass.GetScalarData(strSql)
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return strSubCatCount
    End Function
    ''' <summary>
    ''' fun Insert Sub Category
    ''' </summary>
    ''' <param name="objDataClass"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertSubCategory(ByVal objDataClass As clsDataClass) As Boolean
        Dim sql As String = "INSERT INTO subcategory(subcatDescLang,subcatCatId,subcatName,subcatImagePath," & _
                    "subcatIsActive,subcatUpdatedBy,subcatUpdatedOn,subcatWebSeq,subcatStatus, eBayCatgNo)VALUES( 'en'," & _
                    SubcategoryCatID & ",'" & SubcategoryName & "','" & _
                    SubcategoryImagepath & "'," & SubcategoryIsActive & "," & SubcategoryUpdatedby & ",'" & _
                    SubcategoryUpdatedon & "'," & _
                    SubcategoryWebSequence & ",'1', '" & eBayCatgNo & "' )"
        sql = clsCommon.replaceSysNull(sql)
        Dim intCount As Int32 = objDataClass.SetData(sql)
        If intCount > 0 Then
            Dim intSubCatID As Int32 = funGetMaxSubCategoryID()
            sql = "INSERT INTO subcategory(subcatID, subcatDescLang,subcatCatId,subcatName,subcatImagePath," & _
                        "subcatIsActive,subcatUpdatedBy,subcatUpdatedOn,subcatWebSeq,subcatStatus, eBayCatgNo)VALUES(" & intSubCatID & ",'fr'," & _
                        SubcategoryCatID & ",'" & SubcategoryName & "','" & _
                        SubcategoryImagepath & "'," & SubcategoryIsActive & "," & SubcategoryUpdatedby & ",'" & _
                        SubcategoryUpdatedon & "'," & _
                        SubcategoryWebSequence & ",'1', '" & eBayCatgNo & "')"
            sql = clsCommon.replaceSysNull(sql)
            objDataClass.SetData(sql)
            sql = "INSERT INTO subcategory(subcatID, subcatDescLang,subcatCatId,subcatName,subcatImagePath," & _
                     "subcatIsActive,subcatUpdatedBy,subcatUpdatedOn,subcatWebSeq,subcatStatus, eBayCatgNo)VALUES( " & intSubCatID & ", 'sp'," & _
                     SubcategoryCatID & ",'" & SubcategoryName & "','" & _
                     SubcategoryImagepath & "'," & SubcategoryIsActive & "," & SubcategoryUpdatedby & ",'" & _
                     SubcategoryUpdatedon & "'," & _
                     SubcategoryWebSequence & ",'1', '" & eBayCatgNo & "')"
            sql = clsCommon.replaceSysNull(sql)
            objDataClass.SetData(sql)
        End If
        Return True
    End Function
    ''' <summary>
    ''' For Fill GridView
    ''' </summary>
    ''' <param name="strApprove"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(Optional ByVal strApprove As String = "") As String
        Dim strSql As String
        strSql = "Select subcatID,catname,subcatName,concat(userFirstName,' ',UserLastName) as UserName, CASE subcatIsActive WHEN 1 THEN 'green.gif' WHEN 2 THEN 'orange.gif' ELSE 'red.gif' END as subcatIsActive ,subcatUpdatedOn,subcatWebSeq,  " & _
                 " case subcatImagePath when '' then '../images/nopic.jpg' else subcatImagePath end as subcatImagePath, subcategory.eBayCatgNo  from subcategory " & _
                 " inner join  users on  users.userid=subcategory.subcatUpdatedBy inner join category on subcategory.subcatCatid=category.catid where subcatStatus='1' And catStatus='1' and catdescLang = subcatdescLang and subcatdescLang ='" + clsCommon.funGetProductLang() + "'"
        If strApprove <> "" Then
            strSql += "And subcatIsActive='2' "
        Else
            If HttpContext.Current.Session("UserRole") = "0" Then
                strSql += "And subcatIsActive<>'2' "
            End If
        End If
        strSql += "  ORDER BY subcatWebSeq"

        Return strSql
    End Function
    ''' <summary>
    '''For Delete User(we Update Only User Status)
    ''' </summary>
    ''' <param name="strUserID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteSubcategory(ByVal strUserID As String) As String
        Dim strSql As String
        strSql = "Update subcategory set subcatStatus='0' Where subcatId='" & strUserID & "'" ' and subcatdescLang ='" + _subcatdescLang + "'"
        Return strSql
    End Function
    ''' <summary>
    '''Fill sub-Category Name in Dropdown list According to Category
    ''' </summary>
    ''' <param name="dlSubCategoryName"></param>
    ''' <param name="CategoryID"></param>
    ''' <remarks></remarks>
    Public Sub subGetSubCategoryName(ByVal dlSubCategoryName As DropDownList, ByVal CategoryID As String)
        Dim objDataclass As New clsDataClass
        objDataclass.OpenDatabaseConnection()
        Dim strSQL As String
        strSQL = "SELECT subcatId,subcatName from subcategory where subcatStatus='1' and subcatIsActive='1' and subcatCatId='" & CategoryID & "' and subcatdescLang ='" + clsCommon.funGetProductLang() + "'"
        dlSubCategoryName.DataSource = objDataclass.GetDataReader(strSQL)
        dlSubCategoryName.DataTextField = "subcatName"
        dlSubCategoryName.DataValueField = "subcatId"
        dlSubCategoryName.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = "----Select Sub-Category----"
        dlSubCategoryName.Items.Insert(0, itm)
        objDataclass.CloseDatabaseConnection()
        objDataclass = Nothing
    End Sub
    ''' <summary>
    ''' fun Get Max Sub Category ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetMaxSubCategoryID() As Integer
        Dim objDataClass As New clsDataClass
        Dim strSql, strMaxSubCatId As String
        objDataClass.OpenDatabaseConnection()
        strSql = "SELECT MAX(subcatId) FROM subcategory"
        strMaxSubCatId = Convert.ToString(objDataClass.GetScalarData(strSql))
        strMaxSubCatId = IIf(strMaxSubCatId = "", 0, strMaxSubCatId)
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return strMaxSubCatId
    End Function
    ''' <summary>
    ''' Reject Sub-Category
    ''' </summary>
    ''' <param name="strSID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funRejectSubCategory(Optional ByVal strSID As String = "") As Boolean
        Dim strSQL As String
        Dim objDataClass As New clsDataClass
        strSQL = "Update subcategory set subcatStatus='0',subcatIsActive='0' Where subcatId='" & strSID & "'"
        objDataClass.SetData(strSQL)
        objDataClass.CloseDatabaseConnection()
        Return True
    End Function

    ''' <summary>
    ''' Returns Category name
    ''' </summary>
    ''' <param name="CategoryId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetCategoryName(ByVal CategoryId As String) As String
        Dim strData As String = ""
        Dim objDataClass As New clsDataClass
        objDataClass.OpenDatabaseConnection()
        Dim strSQL As String = "SELECT catName FROM category WHERE catid='" & CategoryId & "' And catIsActive='1' And catdescLang ='" + clsCommon.funGetProductLang() + "' Order By catname "
        strData = objDataClass.GetScalarData(strSQL)
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return strData
    End Function

    ''' <summary>
    ''' Populate Drop Down List
    ''' </summary>
    ''' <param name="dlSubCategory"></param>
    ''' <param name="CategoryId"></param>
    ''' <remarks></remarks>
    Public Sub GetSubCategoryName(ByVal dlSubCategory As DropDownList, ByVal CategoryId As String)
        Dim objDataClass As New clsDataClass
        objDataClass.OpenDatabaseConnection()
        Dim strSQL As String = "SELECT subcatId, subcatName FROM subcategory WHERE subcatcatid='" & CategoryId & "' And subcatIsActive='1' and subcatStatus=1 and subcatdescLang ='" + clsCommon.funGetProductLang() + "' Order By subcatname "
        dlSubCategory.Items.Clear()
        dlSubCategory.DataSource = objDataClass.GetDataReader(strSQL)
        dlSubCategory.DataTextField = "subcatName"
        dlSubCategory.DataValueField = "subcatId"
        dlSubCategory.DataBind()
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
    End Sub
#End Region

End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports System.Threading
Imports System.Globalization
Imports System.Math
Imports iTECH.InbizERP.BusinessLogic
Imports iTECH.Library.Utilities
'Imports System.Web.HttpContext
'Imports clsCommon
'Imports Resources.Resource

Public Class clsProducts
    Inherits clsDataClass
    'Dim objPrint As New clsPrint
    Private _productID, _prdIntID, _prdExtID, _prdUPCCode, _prdName, _prdIsKit, _prdMinQtyPerSO, _prdWeight, _prdWeightPkg, _prdInoclTerms, _prdCreatedUserID, _prdCreatedOn, _prdLastUpdatedByUserID, _prdLastUpdatedOn, _prdSalePricePerMinQty, _prdEndUserSalesPrice, _prdIsSpecial, _prdDiscount, _prdMinQtyPOTrig, _prdAutoPO, _prdPOQty, _prdIsActive, _prdIsWeb, _prdComissionCode, _isPOSMenu, _prdWebSalesPrice, _prdDiscountType, _prdHeight, _prdLength, _prdHeightPkg, _prdLengthPkg, _prdWidth, _prdWidthPkg As String
    Public _CustID As Integer

#Region "Property"
    Public Property ProductID() As String
        Get
            Return _productID
        End Get
        Set(ByVal value As String)
            _productID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdIntID() As String
        Get
            Return _prdIntID
        End Get
        Set(ByVal value As String)
            _prdIntID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdExtID() As String
        Get
            Return _prdExtID
        End Get
        Set(ByVal value As String)
            _prdExtID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdUPCCode() As String
        Get
            Return _prdUPCCode
        End Get
        Set(ByVal value As String)
            _prdUPCCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdName() As String
        Get
            Return _prdName
        End Get
        Set(ByVal value As String)
            _prdName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdIsKit() As String
        Get
            Return _prdIsKit
        End Get
        Set(ByVal value As String)
            _prdIsKit = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdMinQtyPerSO() As String
        Get
            Return _prdMinQtyPerSO
        End Get
        Set(ByVal value As String)
            _prdMinQtyPerSO = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdWeight() As String
        Get
            Return _prdWeight
        End Get
        Set(ByVal value As String)
            _prdWeight = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdWeightPkg() As String
        Get
            Return _prdWeightPkg
        End Get
        Set(ByVal value As String)
            _prdWeightPkg = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdInoclTerms() As String
        Get
            Return _prdInoclTerms
        End Get
        Set(ByVal value As String)
            _prdInoclTerms = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdCreatedUserID() As String
        Get
            Return _prdCreatedUserID
        End Get
        Set(ByVal value As String)
            _prdCreatedUserID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdCreatedOn() As String
        Get
            Return _prdCreatedOn
        End Get
        Set(ByVal value As String)
            _prdCreatedOn = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdLastUpdatedByUserID() As String
        Get
            Return _prdLastUpdatedByUserID
        End Get
        Set(ByVal value As String)
            _prdLastUpdatedByUserID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdLastUpdatedOn() As String
        Get
            Return _prdLastUpdatedOn
        End Get
        Set(ByVal value As String)
            _prdLastUpdatedOn = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdSalePricePerMinQty() As String
        Get
            Return _prdSalePricePerMinQty
        End Get
        Set(ByVal value As String)
            _prdSalePricePerMinQty = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdEndUserSalesPrice() As String
        Get
            Return _prdEndUserSalesPrice
        End Get
        Set(ByVal value As String)
            _prdEndUserSalesPrice = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdIsSpecial() As String
        Get
            Return _prdIsSpecial
        End Get
        Set(ByVal value As String)
            _prdIsSpecial = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdDiscount() As String
        Get
            Return _prdDiscount
        End Get
        Set(ByVal value As String)
            _prdDiscount = clsCommon.funRemove(value, True)
        End Set
    End Property
    
    Public Property PrdMinQtyPOTrig() As String
        Get
            Return _prdMinQtyPOTrig
        End Get
        Set(ByVal value As String)
            _prdMinQtyPOTrig = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdAutoPO() As String
        Get
            Return _prdAutoPO
        End Get
        Set(ByVal value As String)
            _prdAutoPO = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdPOQty() As String
        Get
            Return _prdPOQty
        End Get
        Set(ByVal value As String)
            _prdPOQty = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdIsActive() As String
        Get
            Return _prdIsActive
        End Get
        Set(ByVal value As String)
            _prdIsActive = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdIsWeb() As String
        Get
            Return _prdIsWeb
        End Get
        Set(ByVal value As String)
            _prdIsWeb = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdComissionCode() As String
        Get
            Return _prdComissionCode
        End Get
        Set(ByVal value As String)
            _prdComissionCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property POSMenu() As String
        Get
            Return _isPOSMenu
        End Get
        Set(ByVal value As String)
            _isPOSMenu = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdWebSalesPrice() As String
        Get
            Return _prdWebSalesPrice
        End Get
        Set(ByVal value As String)
            _prdWebSalesPrice = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property prdDiscountType() As String
        Get
            Return _prdDiscountType
        End Get
        Set(ByVal value As String)
            _prdDiscountType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property prdHeight() As String
        Get
            Return _prdHeight
        End Get
        Set(ByVal value As String)
            _prdHeight = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property prdLength() As String
        Get
            Return _prdLength
        End Get
        Set(ByVal value As String)
            _prdLength = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property prdHeightPkg() As String
        Get
            Return _prdHeightPkg
        End Get
        Set(ByVal value As String)
            _prdHeightPkg = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property prdLengthPkg() As String
        Get
            Return _prdLengthPkg
        End Get
        Set(ByVal value As String)
            _prdLengthPkg = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property prdWidth() As String
        Get
            Return _prdWidth
        End Get
        Set(ByVal value As String)
            _prdWidth = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property prdWidthPkg() As String
        Get
            Return _prdWidthPkg
        End Get
        Set(ByVal value As String)
            _prdWidthPkg = clsCommon.funRemove(value, True)
        End Set
    End Property

#End Region

    Public Sub New()
        MyBase.New()
    End Sub

#Region "Function"
    ''' <summary>
    ''' Insert Products
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertProducts() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO products( prdIntID, prdExtID, prdUPCCode, prdName, prdIsKit, prdMinQtyPerSO,  prdWeight, prdWeightPkg, prdInoclTerms, prdCreatedUserID, prdCreatedOn, prdLastUpdatedByUserID, prdLastUpdatedOn, prdSalePricePerMinQty, prdEndUserSalesPrice, prdIsSpecial, prdDiscount,prdMinQtyPOTrig, prdAutoPO, prdPOQty, prdIsActive, prdIsWeb, prdComissionCode,isPOSMenu,prdWebSalesPrice,prdDiscountType,prdHeight, prdLength, prdHeightPkg, prdLengthPkg,prdWidth, prdWidthPkg) VALUES("
        strSQL += "'" + _prdIntID + "','"
        strSQL += _prdExtID + "','"
        strSQL += _prdUPCCode + "','"
        strSQL += _prdName + "','"
        strSQL += _prdIsKit + "','"
        strSQL += _prdMinQtyPerSO + "','"
        strSQL += _prdWeight + "','"
        strSQL += _prdWeightPkg + "','"
        strSQL += _prdInoclTerms + "','"
        strSQL += _prdCreatedUserID + "','"
        strSQL += Format(Date.Parse(Now()), "yyyy-MM-dd") + "','"
        strSQL += _prdLastUpdatedByUserID + "','"
        strSQL += Format(Date.Parse(Now()), "yyyy-MM-dd") + "','"
        strSQL += _prdSalePricePerMinQty + "','"
        strSQL += _prdEndUserSalesPrice + "','"
        strSQL += _prdIsSpecial + "','"
        strSQL += _prdDiscount + "','"
        strSQL += _prdMinQtyPOTrig + "','"
        strSQL += _prdAutoPO + "','"
        strSQL += _prdPOQty + "','"
        strSQL += _prdIsActive + "','"
        strSQL += _prdIsWeb + "','"
        strSQL += _prdComissionCode + "','"
        strSQL += _isPOSMenu + "','"
        strSQL += _prdWebSalesPrice + "','"
        strSQL += _prdDiscountType + "','"
        strSQL += _prdHeight + "','"
        strSQL += _prdLength + "','"
        strSQL += _prdHeightPkg + "','"
        strSQL += _prdLengthPkg + "','"
        strSQL += _prdWidth + "','"
        strSQL += _prdWidthPkg + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        'Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = PopulateData(strSQL)
        CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    '''  Update Products
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateProducts() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE products set "
        strSQL += "prdIntID='" + _prdIntID + "', "
        strSQL += "prdExtID='" + _prdExtID + "', "
        strSQL += "prdUPCCode='" + _prdUPCCode + "', "
        strSQL += "prdName='" + _prdName + "', "
        strSQL += "prdIsKit='" + _prdIsKit + "', "
        strSQL += "prdMinQtyPerSO='" + _prdMinQtyPerSO + "', "
        strSQL += "prdWeight='" + _prdWeight + "', "
        strSQL += "prdWeightPkg='" + _prdWeightPkg + "', "
        strSQL += "prdInoclTerms='" + _prdInoclTerms + "', "
        strSQL += "prdLastUpdatedByUserID='" + HttpContext.Current.Session("UserID").ToString() + "',"
        strSQL += "prdLastUpdatedOn= '" + Format(Date.Parse(Now()), "yyyy-MM-dd") + "',"
        strSQL += "prdSalePricePerMinQty='" + _prdSalePricePerMinQty + "', "
        strSQL += "prdEndUserSalesPrice='" + _prdEndUserSalesPrice + "', "
        strSQL += "prdIsSpecial='" + _prdIsSpecial + "', "
        strSQL += "prdDiscount='" + _prdDiscount + "', "
        strSQL += "prdMinQtyPOTrig='" + _prdMinQtyPOTrig + "', "
        strSQL += "prdAutoPO='" + _prdAutoPO + "', "
        strSQL += "prdPOQty='" + _prdPOQty + "', "
        strSQL += "prdIsActive='" + _prdIsActive + "', "
        strSQL += "prdIsWeb='" + _prdIsWeb + "', "
        strSQL += "prdComissionCode='" + _prdComissionCode + "', "
        strSQL += "isPOSMenu='" + _isPOSMenu + "', "
        strSQL += "prdWebSalesPrice='" + _prdWebSalesPrice + "', "
        strSQL += "prdDiscountType='" + _prdDiscountType + "', "
        strSQL += "prdHeight='" + _prdHeight + "', "
        strSQL += "prdLength='" + _prdLength + "', "
        strSQL += "prdHeightPkg='" + _prdHeightPkg + "', "
        strSQL += "prdLengthPkg='" + _prdLengthPkg + "', "
        strSQL += "prdWidth='" + _prdWidth + "', "
        strSQL += "prdWidthPkg='" + _prdWidthPkg + "'"
        strSQL += " where productID='" + _productID + "'"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update Products
    ''' </summary>
    ''' <param name="strPrdLst"></param>
    ''' <param name="strCatID"></param>
    ''' <param name="strSubCatID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funAssignPrd(ByVal strPrdLst As String, ByVal strCatID As String, ByVal strSubCatID As String) As Boolean
        Dim strSQL, strPrd() As String
        Dim obj As New clsDataClass
        strPrd = strPrdLst.Split("-")

        'This code removes perticular old category and subcategory referece from products.
        strSQL = "Update products p set "
        strSQL += "p.prdCategory=0 , "
        strSQL += "p.prdSubcategory=0 "
        strSQL += "where  p.prdCategory ='" + strCatID + " ' "
        strSQL += "and p.prdSubcategory ='" + strSubCatID + " ' "
        obj.PopulateData(strSQL)

        Dim intData As Int16
        If strPrd.Length >= 1 Then
            For intI As Integer = 0 To strPrd.Length - 2
                strSQL = "UPDATE products set "
                strSQL += "prdCategory='" + strCatID + "',"
                strSQL += "prdSubcategory='" + strSubCatID + "'"
                strSQL += " where productID='" + strPrd(intI) + "'"
                intData = 0
                intData = obj.PopulateData(strSQL)
            Next
        End If
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of Products
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetProductsInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  prdIntID, prdExtID, prdUPCCode, prdName, prdIsKit, prdMinQtyPerSO,  prdWeight,  prdWeightPkg, prdInoclTerms, prdCreatedUserID, prdCreatedOn, prdLastUpdatedByUserID, prdLastUpdatedOn, prdSalePricePerMinQty, prdEndUserSalesPrice, prdIsSpecial, prdDiscount, prdMinQtyPOTrig, prdAutoPO, prdPOQty, prdIsActive, prdIsWeb, prdComissionCode,isPOSMenu,prdWebSalesPrice,prdDiscountType,prdHeight, prdLength, prdHeightPkg, prdLengthPkg,prdWidth, prdWidthPkg FROM products where productID='" + _productID + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _prdIntID = drObj.Item("prdIntID").ToString
            _prdExtID = drObj.Item("prdExtID").ToString
            _prdUPCCode = drObj.Item("prdUPCCode").ToString
            _prdName = drObj.Item("prdName").ToString
            _prdIsKit = drObj.Item("prdIsKit").ToString
            _prdMinQtyPerSO = drObj.Item("prdMinQtyPerSO").ToString
            _prdWeight = drObj.Item("prdWeight").ToString
            _prdWeightPkg = drObj.Item("prdWeightPkg").ToString
            _prdInoclTerms = drObj.Item("prdInoclTerms").ToString
            _prdCreatedUserID = drObj.Item("prdCreatedUserID").ToString
            _prdCreatedOn = drObj.Item("prdCreatedOn").ToString
            _prdLastUpdatedByUserID = drObj.Item("prdLastUpdatedByUserID").ToString
            _prdLastUpdatedOn = drObj.Item("prdLastUpdatedOn").ToString
            _prdSalePricePerMinQty = drObj.Item("prdSalePricePerMinQty").ToString
            _prdEndUserSalesPrice = drObj.Item("prdEndUserSalesPrice").ToString
            _prdIsSpecial = drObj.Item("prdIsSpecial").ToString
            _prdDiscount = drObj.Item("prdDiscount").ToString
            _prdMinQtyPOTrig = drObj.Item("prdMinQtyPOTrig").ToString
            _prdAutoPO = drObj.Item("prdAutoPO").ToString
            _prdPOQty = drObj.Item("prdPOQty").ToString
            _prdIsActive = drObj.Item("prdIsActive").ToString
            _prdIsWeb = drObj.Item("prdIsWeb").ToString
            _prdComissionCode = drObj.Item("prdComissionCode").ToString
            _isPOSMenu = drObj.Item("isPOSMenu").ToString
            _prdWebSalesPrice = drObj.Item("prdWebSalesPrice").ToString
            _prdDiscountType = drObj.Item("prdDiscountType").ToString
            _prdHeight = drObj.Item("prdHeight").ToString
            _prdLength = drObj.Item("prdLength").ToString
            _prdHeightPkg = drObj.Item("prdHeightPkg").ToString
            _prdLengthPkg = drObj.Item("prdLengthPkg").ToString
            _prdWidth = drObj.Item("prdWidth").ToString
            _prdWidthPkg = drObj.Item("prdWidthPkg").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    '''  sub Fill Product
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <param name="strVendorID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subFillProduct(ByVal StatusData As String, ByVal SearchData As String, ByVal strVendorID As String) As String

        Dim strSql As String = "select Pdes.prdname,prdInoclTerms,productID,prdIsKit as kit,sum(prdOhdQty) as prdOhdQty,Sum(prdQuoteRsv) as prdQuoteRsv,prdUPCCode,sum(prdSORsv) as prdSORsv,prdIntID,prdExtID,prdUPCCode,prdSmallDesc,prdLargeDesc,prdDefectiveQty,  "
        'strSql += " CASE prdIsKit WHEN 1 THEN 'Blue.jpg' ELSE 'Yellow.jpg' END as prdIsKit " & _
        strSql += " CASE prdIsActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as prdIsActive"
        strSql += "  from Products"
        If strVendorID <> "" Then
            strSql += " Inner join prdassociatevendor on prdassociatevendor.prdId=Products.ProductID "
        End If
        strSql += " Left join prdQuantity on prdQuantity.PrdID=Products.ProductID "
        strSql += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID where  descLang ='" + clsCommon.funGetProductLang() + "'"

        If StatusData = ProductSearchFields.ProductName And SearchData <> "sysNull" Then
            strSql += " And Pdes.prdName like '%" + SearchData + "%' "
        ElseIf StatusData = ProductSearchFields.ProductInternalID And SearchData <> "sysNull" Then
            strSql += " And prdIntID = '" + SearchData + "' "
        ElseIf StatusData = ProductSearchFields.ProductExternalID And SearchData <> "sysNull" Then
            strSql += " And prdExtID = '" + SearchData + "' "
        ElseIf StatusData = ProductSearchFields.ProductBarCode And SearchData <> "sysNull" Then
            strSql += " And prdUPCCode = '" + SearchData + "' "
            'ElseIf StatusData = "TI" And SearchData <> "sysNull" Then
            '    strSql += " And prdQuantity.tagid = '" + SearchData + "' "
        ElseIf StatusData = "TI" And SearchData <> "sysNull" Then
            strSql += " AND Products.productID IN (SELECT ProductID FROM prdtags WHERE Tag = '" + SearchData + "') "
        ElseIf StatusData = ProductSearchFields.ProductDescription And SearchData <> "sysNull" Then
            strSql += " And "
            strSql += " ("
            strSql += " prdSmallDesc like '%" + SearchData + "%' or "
            strSql += " prdLargeDesc like '%" + SearchData + "%' "
            strSql += " )"
        End If
        If strVendorID <> "" Then
            strSql += " And prdVendorID = '" + strVendorID + "' "
        End If
        strSql += " GROUP BY productID "
        Return strSql
    End Function

    Public Function SearchProductAdvance(ByVal alpha As String) As String

        Dim strSql As String = "select Pdes.prdname,prdInoclTerms,productID,prdIsKit as kit,sum(prdOhdQty) as prdOhdQty,Sum(prdQuoteRsv) as prdQuoteRsv,prdUPCCode,sum(prdSORsv) as prdSORsv,prdIntID,prdExtID,prdUPCCode,prdSmallDesc,prdLargeDesc,prdDefectiveQty,  "
        'strSql += " CASE prdIsKit WHEN 1 THEN 'Blue.jpg' ELSE 'Yellow.jpg' END as prdIsKit " & _
        strSql += " CASE prdIsActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as prdIsActive"
        strSql += "  from Products"
        
        strSql += " Left join prdQuantity on prdQuantity.PrdID=Products.ProductID "
        strSql += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID where  descLang ='" + clsCommon.funGetProductLang() + "'"

        If alpha.Length = 1 Then
            strSql += " And Pdes.prdName like '" + alpha + "%' "
        ElseIf alpha = "CF" Then 'Customer Fav
            strSql += " And productID IN (SELECT ordProductID FROM orderitems)"
        ElseIf alpha = "MF" Then 'My Fav
            strSql += " And prdIsSpecial = 1 "
        
        End If
        strSql += " GROUP BY productID "
        Return strSql
    End Function


    ''' <summary>
    ''' Get the vendor from Product
    ''' </summary>
    ''' <param name="strPrdID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetVendorCount(ByVal strPrdID As String) As Integer
        Dim objDataClass As New clsDataClass
        Dim strSql, strVendor As String
        strSql = "select count(prdassociatevendor.prdId) as a,productID from Products left join prdassociatevendor on prdassociatevendor.prdId=Products.ProductID where productID='" & strPrdID & "' GROUP BY productID"
        strVendor = Convert.ToString(objDataClass.GetScalarData(strSql))
        strVendor = IIf(strVendor = "", 0, strVendor)
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return strVendor
    End Function
    ''' <summary>
    ''' Product Delete
    ''' </summary>
    ''' <param name="strProdID"></param>
    ''' <param name="strMultipleDete"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funProductDelete(ByVal strProdID As String, Optional ByVal strMultipleDete As String = "") As String
        Dim objDataClass As New clsDataClass
        Dim strSql As String = "delete from prdavailablewhs where prdWhsID ='" & strProdID & "'"
        objDataClass.SetData(strSql)
        strSql = "delete from prdassociatevendor where prdID='" & strProdID & "'"
        objDataClass.SetData(strSql)
        strSql = "delete from prdassociations where id='" & strProdID & "'"
        objDataClass.SetData(strSql)
        strSql = "delete from prddescriptions where id='" & strProdID & "'"
        objDataClass.SetData(strSql)
        strSql = "delete from prdimages where prdID='" & strProdID & "'"
        objDataClass.SetData(strSql)
        strSql = "delete from productsalesprice where prdSalesPriceID='" & strProdID & "'"
        objDataClass.SetData(strSql)
        strSql = "delete from productkits where prdID='" & strProdID & "'"
        objDataClass.SetData(strSql)
        strSql = "delete from prdcolor where prdID='" & strProdID & "'"
        objDataClass.SetData(strSql)
        strSql = "delete from prdquantity where prdID='" & strProdID & "'"
        objDataClass.SetData(strSql)
        strSql = "delete from Products where ProductID='" & strProdID & "'"
        If strMultipleDete = "yes" Then
            objDataClass.SetData(strSql)
            objDataClass = Nothing
            Return Resources.Resource.msgPrdDeleted
        End If
        objDataClass = Nothing
        Return strSql
    End Function
    ''' <summary>
    ''' Get the Max ProductID from Product
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetMaxProductID() As Integer
        Dim objDataClass As New clsDataClass
        Dim strSql, strMaxProdId As String
        strSql = "SELECT MAX(ProductID) FROM Products"
        strMaxProdId = Convert.ToString(objDataClass.GetScalarData(strSql))
        strMaxProdId = IIf(strMaxProdId = "", 0, strMaxProdId)
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return strMaxProdId
    End Function
    ''' <summary>
    ''' Get Product Value
    ''' </summary>
    ''' <param name="strProdID"></param>
    ''' <param name="strType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetPrdValue(ByVal strProdID As String, Optional ByVal strType As String = "") As String
        Dim strSQL, strPrdName As String
        If strType = "" Then
            strSQL = "SELECT prdName from products where ProductID='" & strProdID & "'" ' not used
        Else
            strSQL = "SELECT prdIsKit from products where ProductID='" & strProdID & "'"
        End If
        Dim objDataClass As New clsDataClass
        strPrdName = objDataClass.GetScalarData(strSQL)
        objDataClass = Nothing
        Return strPrdName
    End Function
    ''' <summary>
    ''' Fill Product Name in Dropdown list
    ''' </summary>
    ''' <param name="dlProduct"></param>
    ''' <param name="strIntProdID"></param>
    ''' <remarks></remarks>
    Public Sub subGetProduct(ByVal dlProduct As DropDownList, Optional ByVal strIntProdID As String = "")
        Dim strSQL As String
        strSQL = "SELECT Pdes.prdName as ProductName,ProductID, concat(ifnull(prdIntID,''),' ',Pdes.prdName) as ProductForKit from products  "
        strSQL += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID where  descLang ='" + clsCommon.funGetProductLang() + "' and prdISActive='1'  and ProductID!='" & HttpContext.Current.Request.QueryString("PrdID") & "' order by ProductID"
        dlProduct.DataSource = GetDataReader(strSQL)
        If strIntProdID = "1" Then
            dlProduct.DataTextField = "ProductForKit"
        Else
            dlProduct.DataTextField = "ProductName"
        End If

        dlProduct.DataValueField = "ProductID"
        dlProduct.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.msgSelectProduct
        dlProduct.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub

    'Fill Product Name in List box list
    Public Sub subGetProductForList(ByVal lstProduct As ListBox, ByVal strCatID As String, ByVal strSubCatID As String, ByVal strListType As String)
        Dim strSQL As String
        If strListType = "From" Then
            strSQL = "SELECT prdName,ProductID from products  where prdISActive='1' and prdIsWeb='1' and (prdCategory!='" & strCatID & "' or prdSubCategory!='" & strSubCatID & "')"
        Else
            strSQL = "SELECT prdName,ProductID from products  where prdISActive='1' and prdIsWeb='1' and prdCategory='" & strCatID & "' And prdSubCategory='" & strSubCatID & "'"
        End If
        lstProduct.DataSource = GetDataReader(strSQL)
        lstProduct.DataTextField = "prdName"
        lstProduct.DataValueField = "ProductID"
        lstProduct.DataBind()
        'Dim itm As New ListItem
        'itm.Value = 0
        'itm.Text = msgSelectProduct
        'lstProduct.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub

    ''' <summary>
    ''' Find Duplicate Product Name
    ''' </summary>
    ''' <param name="strProdID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDuplicateProduct(Optional ByVal strProdID As String = "") As Boolean
        Dim strSQL As String
        If strProdID <> "" Then
            strSQL = "SELECT count(*) FROM products where prdName='" & _prdName & "' And prdISActive='1' and ProductID <>'" & strProdID & "' "
        Else
            strSQL = "SELECT count(*) FROM products where prdName='" & _prdName & "' And prdISActive='1'"
        End If
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Fill Product For POS
    ''' </summary>
    ''' <param name="strType"></param>
    ''' <param name="strProduct"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subFillProductForPOS(ByVal strType As String, ByVal strProduct As String) As String
        Dim strSql As String = "select ProductID,Pdes.prdname as ProductName,productID,prdUPCCode,prdDiscount,"
        strSql += " case prdDiscounttype when 'A' then prdEndUserSalesPrice - prdDiscount else (prdEndUserSalesPrice - (prdEndUserSalesPrice * prdDiscount/100)) end  as prdEndUserSalesPrice,prdEndUserSalesPrice,(prdOhdQty - case isnull(prdDefectiveQty) when 1 then 0 else prdDefectiveQty end) as prdOhdQty  from Products "
        strSql += " Left join prdQuantity on prdQuantity.PrdID=Products.ProductID "
        strSql += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID where prdWhsCode='" & HttpContext.Current.Session("UserWarehouse") & "' and descLang ='" + clsCommon.funGetProductLang() + "' and prdIsActive=1 " '(prdOhdQty - case isnull(prdSORsv) when 1 then 0 else prdSORsv end) > 0"
        If strType = ProductSearchFields.ProductName Then
            strSql += " And Pdes.prdname like '%" & strProduct & "%'"
        ElseIf strType = "TI" Then
            strSql += " AND Products.productID IN (SELECT ProductID FROM prdtags WHERE Tag = '" + strProduct + "') "
        ElseIf strType = ProductSearchFields.ProductBarCode Then
            strSql += " And prdUPCCode = '" & strProduct & "'"
        ElseIf strType = "POSMENU" Then
            strSql += " And productID = '" & strProduct & "' "
        End If
        strSql += " GROUP BY productID order by Pdes.prdname Asc"
        Return strSql
    End Function
    ''' <summary>
    ''' sub Fill PrdWhs For View
    ''' </summary>
    ''' <param name="strPrdID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subFillPrdWhsForView(ByVal strPrdID As String) As String
        Dim strSql As String = "SELECT id,prdID, prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty ,concat(prdWhsCode,':',WarehouseDescription) as WarehouseDescription,sysTaxCodeDescText FROM prdquantity"
        strSql += " Left join systaxcodedesc as T on T.sysTaxCodeDescID=prdquantity.prdTaxCode"
        strSql += " inner join syswarehouses as w on w.WarehouseCode=prdquantity.prdWhsCode where prdID='" & strPrdID & "' and WarehouseActive='1'"
        Return strSql
    End Function
    ''' <summary>
    ''' Get Product list for POS Menu
    ''' </summary>
    ''' <param name="strPrdCount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subFillPosMenu(Optional ByVal strPrdCount As String = "") As String
        Dim strSql As String
        If strPrdCount = "" Then
            strSql = "select ProductID, Pdes.prdname as prdname ,isPosMenu, "
            strSql += " prdSmallImagePath, prdLargeImagePath, pimgIsDefault"
        Else
            strSql = "select Count(*)"
        End If
        strSql += " from Products"
        strSql += " Left join prdQuantity on prdQuantity.PrdID=Products.ProductID "
        strSql += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID "
        strSql += " left join prdImages on prdImages.PrdID=Products.ProductID  where descLang ='" + clsCommon.funGetProductLang() + "'  and isPosMenu='1' and prdWhsCode='" & HttpContext.Current.Session("UserWarehouse") & "'"
        strSql += " and (prdSmallImagePath !='_low_1.' or prdSmallImagePath is null) and prdIsActive=1 group by ProductID order by Pdes.prdname Asc limit 25 " ' and pimgIsDefault='1' and (prdOhdQty - case isnull(prdSORsv) when 1 then 0 else prdSORsv end) > 0 
        If strPrdCount <> "" Then
            strSql = GetScalarData(strSql)
            Return strSql
        End If
        Return strSql
    End Function
    ''' <summary>
    ''' Find Duplicate Product Bar Code
    ''' </summary>
    ''' <param name="strProdID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDuplicateProductUPCCode(Optional ByVal strProdID As String = "") As Boolean
        Dim strSQL As String
        If strProdID <> "" Then
            strSQL = "SELECT count(*) FROM products where prdUPCCode='" & _prdUPCCode & "' And prdISActive='1' and ProductID <>'" & strProdID & "' "
        Else
            strSQL = "SELECT count(*) FROM products where prdUPCCode='" & _prdUPCCode & "' And prdISActive='1'"
        End If
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Find ProductName by UPCCode.
    ''' </summary>
    ''' <param name="strUPCCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funProductName(ByVal strUPCCode As String) As String
        Dim strSQL As String
        Dim strResult As String = ""
        Dim drObj As OdbcDataReader
        If strUPCCode <> "" Then
            strSQL = "SELECT prdName FROM products where prdUPCCode='" & strUPCCode & "' "
        Else
            strSQL = "SELECT prdName) FROM products where prdUPCCode='" & strUPCCode & "' "
        End If
        drObj = GetDataReader(strSQL)
        While drObj.Read
            strResult = drObj.Item("prdName").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
        Return strResult
    End Function
    ''' <summary>
    ''' Get Product list for POS Menu
    ''' </summary>
    ''' <param name="strPrdCatgId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function fillPosMenuForCatg(ByVal strPrdCatgId As String) As String
        Dim strSql As String
        strSql = "select products.ProductID, Pdes.prdname as prdname ,isPosMenu, prdIsKit, "
        strSql += " prdSmallImagePath, prdLargeImagePath, pimgIsDefault, ppc.prdPOSCatgID "
        strSql += " from Products"
        strSql += " Left join prdQuantity on prdQuantity.PrdID=Products.ProductID "
        strSql += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID "
        strSql += " left join prdImages on prdImages.PrdID=Products.ProductID  "
        strSql += "  Inner Join prdposcatgsel ppc ON products.productID = ppc.productID and ppc.prdPOSCatgID = '" & strPrdCatgId & "' "
        strSql += "  where (prdSmallImagePath !='_low_1.' or prdSmallImagePath is null) and descLang ='" + clsCommon.funGetProductLang() + "' and prdIsActive=1 and isPosMenu='1' and prdWhsCode='" & HttpContext.Current.Session("UserWarehouse") & "' group by ProductID order by Pdes.prdname Asc"
        ' strSql += " and prdSmallImagePath !='_low_1.' and pimgIsDefault='1' and (prdOhdQty - case isnull(prdSORsv) when 1 then 0 else prdSORsv end) > 0  limit 25 "
        Return strSql
    End Function
    ''' <summary>
    ''' Product Kit Contents Info
    ''' </summary>
    ''' <param name="strProductId"></param>
    ''' <param name="strPrdCount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function fillProductKitContent(ByVal strProductId As String, Optional ByVal strPrdCount As String = "") As String
        Dim strSql As String
        If strPrdCount = "" Then
            strSql = " SELECT pk.prdKitId,p.prdUPCCode, p.prdName, pk.prdIncludeQty "
        Else
            strSql = "select Count(*) "
        End If
        strSql += " FROM products p INNER JOIN productkits pk ON pk.prdIncludeID = p.productId "
        strSql += " where pk.prdId =" & strProductId
        If strPrdCount <> "" Then
            strSql = GetScalarData(strSql)
            Return strSql
        End If
        Return strSql
    End Function
    ''' <summary>
    ''' Populate objects of Products
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subGetProductsArray() As Hashtable
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        Dim objCommon As New clsCommon
        Dim strArr As New Hashtable

        ' Get Default Customer TaxID And DisCount
        Dim iCUstID = _CustID
        Dim iTaxID As Integer
        Dim iDisc As Integer
        Dim sTaxName As String = ""
        Dim dblTaxValue As Double = 0.0

        Dim objPartners As New Partners
        objPartners.PopulateObject(iCUstID)
        If (BusinessUtility.GetInt(objPartners.PartnerTaxCode) > 0) Then
            iTaxID = BusinessUtility.GetInt(objPartners.PartnerTaxCode)
        Else
            iTaxID = 5
        End If

        If (iTaxID > 0) Then
            Dim objTaxGroup As New SysTaxCodeDesc
            objTaxGroup.PopulateObject(iTaxID)
            sTaxName = BusinessUtility.GetString(objTaxGroup.SysTaxCodeDescText)

            Dim objTaxCode As New SysTaxCode
            objTaxCode.PopulateObject(BusinessUtility.GetInt(iTaxID), sTaxName)
            dblTaxValue = objTaxCode.sysTaxPercentage
        End If

        If (BusinessUtility.GetInt(objPartners.PartnerDiscount) > 0) Then
            iDisc = BusinessUtility.GetInt(objPartners.PartnerDiscount)
        Else
            iDisc = 0
        End If


        strSQL = "SELECT  distinct productID,prdTaxCode,  prdUPCCode,  Pdes.prdname as prdname,prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty,prdEndUserSalesPrice,prdIsKit,prdSmallImagePath, prdLargeImagePath FROM products "
        strSQL += " Left join prdQuantity on prdQuantity.PrdID=Products.ProductID "
        strSQL += " Inner join prddescriptions as Pdes on Pdes.ID=Products.ProductID "
        strSQL += " left join prdImages on prdImages.PrdID=Products.ProductID  "
        strSQL += " where (prdSmallImagePath !='_low_1.' or prdSmallImagePath is null) and descLang ='" + clsCommon.funGetProductLang() + "' and prdIsActive=1 and isPosMenu='1' and prdWhsCode='" & HttpContext.Current.Session("UserWarehouse") & "' group by ProductID order by Pdes.prdname Asc"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            'strArr.Add(drObj.Item("productID").ToString, drObj.Item("productID").ToString & "~" & objCommon.replaceSpecialCh(drObj.Item("prdName").ToString) & "~" & drObj.Item("prdUPCCode").ToString & "~" & drObj.Item("prdOhdQty").ToString & "~" & drObj.Item("prdQuoteRsv").ToString & "~" & drObj.Item("prdSORsv").ToString & "~" & drObj.Item("prdEndUserSalesPrice").ToString & "~" & drObj.Item("prdIsKit").ToString & "~" & drObj.Item("prdSmallImagePath").ToString & "__taxGrp__" & funTaxCodeDetail(drObj.Item("prdTaxCode").ToString, Current.Session("UserWarehouse"), Current.Session("RegCode")))

            Dim sb As New StringBuilder()
            sb.Append(drObj.Item("productID").ToString)
            sb.Append("~")
            sb.Append(objCommon.replaceSpecialCh(drObj.Item("prdName").ToString))
            sb.Append("~")
            sb.Append(drObj.Item("prdUPCCode").ToString)
            sb.Append("~")
            sb.Append(drObj.Item("prdOhdQty").ToString)
            sb.Append("~")
            sb.Append(drObj.Item("prdQuoteRsv").ToString)
            sb.Append("~")
            sb.Append(drObj.Item("prdSORsv").ToString)
            sb.Append("~")

            Dim dblPrdCost As Double = (BusinessUtility.GetDouble(drObj.Item("prdEndUserSalesPrice")) * 1)
            Dim dblPrdDisc = BusinessUtility.GetDouble(dblPrdCost * iDisc / 100)
            Dim dblPrdAftDisc = BusinessUtility.GetDouble(dblPrdCost - dblPrdDisc)

            Dim dblTaxAmount As Double = BusinessUtility.GetDouble(dblPrdAftDisc * dblTaxValue / 100)
            Dim dblPrdNetCost As Double = BusinessUtility.GetDouble(dblPrdAftDisc + dblTaxAmount)

            'sb.Append(BusinessUtility.GetString(dblPrdNetCost))
            sb.Append(drObj.Item("prdEndUserSalesPrice").ToString)
            sb.Append("~")
            sb.Append(drObj.Item("prdIsKit").ToString)
            sb.Append("~")
            sb.Append(drObj.Item("prdSmallImagePath").ToString)
            sb.Append("~")
            'sb.Append(drObj.Item("prdSmallImagePath").ToString)
            sb.Append(BusinessUtility.GetString(iDisc))
            sb.Append("~")
            'sb.Append(BusinessUtility.GetString(iTaxID))
            sb.Append(BusinessUtility.GetString(drObj.Item("prdTaxCode").ToString))
            sb.Append("~")
            sb.Append(sTaxName)
            sb.Append("~")
            sb.Append(BusinessUtility.GetString(dblPrdNetCost))
            sb.Append("~")
            sb.Append(BusinessUtility.GetString(dblTaxValue))
            'Append Tax Groups..
            sb.Append("__taxGrp__")
            sb.Append(funTaxCodeDetail(drObj.Item("prdTaxCode").ToString, HttpContext.Current.Session("UserWarehouse"), HttpContext.Current.Session("RegCode")))
            'Append Tags for product..
            sb.Append("__tags__")
            Dim objRecItemsTags As New clsPrdTag()
            sb.Append(objRecItemsTags.GetTagsByProduct(drObj.Item("productID").ToString))

            'add string to hashtable
            strArr.Add(drObj.Item("productID").ToString, sb.ToString())
        End While
        drObj.Close()
        CloseDatabaseConnection()
        Return strArr
    End Function
    ''' <summary>
    ''' Tax Detail
    ''' </summary>
    ''' <param name="strTaxCode"></param>
    ''' <param name="strWhscode"></param>
    ''' <param name="strRegister"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funTaxCodeDetail(ByVal strTaxCode As String, ByVal strWhscode As String, ByVal strRegister As String) As String
        Dim strSQL, strTaxValue As String
        Dim drObj As OdbcDataReader
        Dim strTax As New StringBuilder
        If strTaxCode = "" Or strTaxCode = "0" Then
            strSQL = "SELECT sysRegTaxCode FROM sysregister where sysRegWhsCode='" & strWhscode & "' and sysRegCode='" & strRegister & "'"
            strTaxCode = Convert.ToString(GetScalarDataWithoutConClose(strSQL))
        End If
        strSQL = "SELECT  sysTaxCode, sysTaxDesc, sysTaxPercentage, sysTaxSequence, sysTaxOnTotal FROM systaxcode where sysTaxCode='" + strTaxCode + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            strTax.Append(drObj.Item("sysTaxDesc").ToString & "~" & drObj.Item("sysTaxPercentage").ToString & "~" & drObj.Item("sysTaxSequence").ToString & "~" & drObj.Item("sysTaxOnTotal").ToString & "~" & drObj.Item("sysTaxCode").ToString & "^")
        End While
        drObj.Close()
        strTaxValue = strTax.ToString
        Return strTaxValue
    End Function
    ''' <summary>
    ''' for Tax
    ''' </summary>
    ''' <param name="strProductID"></param>
    ''' <param name="strValue"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funTax(ByVal strProductID As String, ByVal strValue As String) As String
        Dim strTaxArr As ArrayList

        Dim str() As String = strValue.Split("~")
        Dim strTaxArray(,) As String
        'strTaxArr = objPrint.funCalcTax(strProductID, HttpContext.Current.Session("UserWarehouse"), str(6), "POS")
        Dim strTaxItem As String
        Dim intI As Integer = 0
        While strTaxArr.Count - 1 >= intI
            strTaxItem = strTaxArr.Item(intI).ToString
            'strTaxArray = objPrint.funAddTax(strTaxArray, strTaxItem.Substring(0, strTaxArr.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTaxArr.Item(intI).IndexOf("@,@") + 3)))
            intI += 1
        End While
        Dim i As Integer = 0
        Dim j As Integer = 0
        If Not strTaxArray Is Nothing Then
            j = strTaxArray.Length / 2
        End If
        Dim dblTax As String = ""
        While i < j
            dblTax += strTaxArray(0, i) & "~" & strTaxArray(1, i) & "^"
            i += 1
        End While
        Return dblTax
    End Function
    ''' <summary>
    ''' Get CategoryName related to Product
    ''' </summary>
    ''' <param name="strProductID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetCategoryName(ByVal strProductID As String) As String
        Dim strSql, strCat As String
        strCat = ""
        strSql = ""
        Dim drObjCat As OdbcDataReader
        strSql += "select prdPOSCatgID, prdPOSCatgDesc, prdPOSCatgImgPath, prdPOSCatgActive, prdPOSCatgSeq  FROM prdposcatg as pc"
        strSql += " WHERE prdposcatgActive =1 and prdPOSCatgId IN (SELECT distinct prdPOSCatgId FROM prdposcatgsel as p inner join products on products.productID =p.productid where   isPOSMenu=1 "
        strSql += " and p.productid=" & strProductID & ")"
        drObjCat = GetDataReader(strSql)
        While drObjCat.Read
            strCat += "~" & drObjCat.Item("prdPOSCatgID").ToString
        End While
        If strCat <> "" Then
            strCat = strCat.TrimStart("~")
        End If
        drObjCat = Nothing
        CloseDatabaseConnection()
        Return strCat
    End Function
    ''' <summary>
    ''' Get Token
    ''' </summary>
    ''' <param name="strUserID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetToken(ByVal strUserID As String) As String       
        Dim strToken As String
        Dim ObjCommon As New clsCommon
        Dim strSQL As String
        Dim objDC As New clsDataClass
        strToken = Date.Now.Ticks & clsCommon.funRandomString()
        strSQL = "Insert into Postoken (tokenId, tokenUser, tokenTime, tokenStatus) values ('" & strToken & "', '" & strUserID & "', now(), '1')"
        objDC.PopulateData(strSQL)
        Dim strData As String
        strSQL = "select tokenNo from Postoken where tokenID='" & strToken & "' and tokenUser='" & strUserID & "' and tokenStatus='1'"
        strData = objDC.GetScalarData(strSQL)
        strToken = strData & "-" & strToken
        strSQL = "Update Postoken set tokenId='" & strToken & "' where tokenNo='" & strData & "' "
        objDC.PopulateData(strSQL)
        objDC.CloseDatabaseConnection()
        objDC = Nothing
        Return strToken
    End Function
#End Region
End Class

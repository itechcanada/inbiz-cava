Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports Resources.Resource
''Imports clsCommon
Public Class clsOrdInvFrequency
    Inherits clsDataClass
    Private _orderID, _dayofTheMonth1, _dayofTheMonth2, _dayofTheMonth3, _dayofTheMonth4, _dayofTheMonth5 As String
    Private _endDateTime, _lastInvoiceCreatedOnDT As String

    Public Property OrderID() As String
        Get
            Return _orderID
        End Get
        Set(ByVal value As String)
            _orderID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DayofTheMonth1() As String
        Get
            Return _dayofTheMonth1
        End Get
        Set(ByVal value As String)
            _dayofTheMonth1 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DayofTheMonth2() As String
        Get
            Return _dayofTheMonth2
        End Get
        Set(ByVal value As String)
            _dayofTheMonth2 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DayofTheMonth3() As String
        Get
            Return _dayofTheMonth3
        End Get
        Set(ByVal value As String)
            _dayofTheMonth3 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DayofTheMonth4() As String
        Get
            Return _dayofTheMonth4
        End Get
        Set(ByVal value As String)
            _dayofTheMonth4 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property DayofTheMonth5() As String
        Get
            Return _dayofTheMonth5
        End Get
        Set(ByVal value As String)
            _dayofTheMonth5 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property EndDateTime() As String
        Get
            Return _endDateTime
        End Get
        Set(ByVal value As String)
            _endDateTime = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property LastInvoiceCreatedOnDT() As String
        Get
            Return _lastInvoiceCreatedOnDT
        End Get
        Set(ByVal value As String)
            _lastInvoiceCreatedOnDT = clsCommon.funRemove(value, True)
        End Set
    End Property


    ''' <summary>
    '''  Insert Subcription
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertSubcription() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO OrdInvFrequency(orderID,dayofTheMonth1,dayofTheMonth2,dayofTheMonth3,dayofTheMonth4,dayofTheMonth5,endDateTime,lastInvoiceCreatedOnDT) VALUES('"
        strSQL += _orderID + "','"
        strSQL += _dayofTheMonth1 + "','"
        strSQL += _dayofTheMonth2 + "','"
        strSQL += _dayofTheMonth3 + "','"
        strSQL += _dayofTheMonth4 + "','"
        strSQL += _dayofTheMonth5 + "','"
        strSQL += _endDateTime + "','"
        strSQL += _lastInvoiceCreatedOnDT + "')"

        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' check entry of orderId
    ''' </summary>
    ''' <param name="orderid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CheckSubcription(ByVal orderid As String) As String

        Dim strSQL As String
        strSQL = "Select count(orderID) from  OrdInvFrequency where orderid=" & orderid & ""

        Return GetScalarData(strSQL)
    End Function

    ''' <summary>
    ''' Validate Subscription invoice Frequency
    ''' </summary>
    ''' <param name="ordID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidateSubscriptionInvoice(ByVal ordID As String) As Boolean
        ordID = clsCommon.funRemove(ordID)
        Dim sb As New StringBuilder()
        sb.Append("SELECT * FROM ordinvfrequency WHERE orderID = @orderID")
        sb.Replace("@orderID", ordID)

        Dim dt As New DataTable()
        Dim objDataAccess As New clsDataClass()
        dt = objDataAccess.GetDataTable(sb.ToString())

        If dt.Rows.Count = 0 Then
            Return False
        End If

        Dim day1, day2, day3, day4, day5 As Integer
        Dim endDay As DateTime
        day1 = CType(dt.Rows(0)(1), Integer)
        day2 = CType(dt.Rows(0)(2), Integer)
        day3 = CType(dt.Rows(0)(3), Integer)
        day4 = CType(dt.Rows(0)(4), Integer)
        day5 = CType(dt.Rows(0)(5), Integer)
        endDay = CType(dt.Rows(0)(6), DateTime)

        'Logic to validate current date with subscription end date. 
        Return (endDay.Month = Now.Month) And (day1 = Now.Day Or day2 = Now.Day Or day3 = Now.Day Or day4 = Now.Day Or day5 = Now.Day Or endDay.Day = Now.Day)
    End Function
    ''' <summary>
    ''' Update Last Crated Invoice Date
    ''' </summary>
    ''' <param name="ordID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function UpdateLastCratedInvoiceDate(ByVal ordID As String) As Boolean
        ordID = clsCommon.funRemove(ordID)
        Dim sb As New StringBuilder()
        sb.Append("UPDATE ordinvfrequency SET lastInvoiceCreatedOnDT = now() WHERE orderID = @orderID")
        sb.Replace("@orderID", ordID)

        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(sb.ToString())
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If

    End Function
End Class

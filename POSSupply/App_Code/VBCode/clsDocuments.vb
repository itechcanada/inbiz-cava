Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsDocuments
	Inherits clsDataClass
	Private _sysDocId, _sysDocType, _sysDocDistType, _sysDocDistDateTime, _sysDocPath, _sysDocEmailTo, _sysDocFaxToPhone, _sysDocFaxToAttention, _sysDocBody, _sysDocSubject As String
	Public Property SysDocId() As String
		Get
			Return _sysDocId
		End Get
		Set(ByVal value As String)
            _sysDocId = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property SysDocType() As String
		Get
			Return _sysDocType
		End Get
		Set(ByVal value As String)
            _sysDocType = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property SysDocDistType() As String
		Get
			Return _sysDocDistType
		End Get
		Set(ByVal value As String)
            _sysDocDistType = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property SysDocDistDateTime() As String
		Get
			Return _sysDocDistDateTime
		End Get
		Set(ByVal value As String)
            _sysDocDistDateTime = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property SysDocPath() As String
		Get
			Return _sysDocPath
		End Get
		Set(ByVal value As String)
            _sysDocPath = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property SysDocEmailTo() As String
		Get
			Return _sysDocEmailTo
		End Get
		Set(ByVal value As String)
            _sysDocEmailTo = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property SysDocFaxToPhone() As String
		Get
			Return _sysDocFaxToPhone
		End Get
		Set(ByVal value As String)
            _sysDocFaxToPhone = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property SysDocFaxToAttention() As String
		Get
			Return _sysDocFaxToAttention
		End Get
		Set(ByVal value As String)
            _sysDocFaxToAttention = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property SysDocBody() As String
		Get
			Return _sysDocBody
		End Get
		Set(ByVal value As String)
            _sysDocBody = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property SysDocSubject() As String
		Get
			Return _sysDocSubject
		End Get
		Set(ByVal value As String)
            _sysDocSubject = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert Documents
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertDocuments() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO sysDocuments(sysDocType, sysDocDistType, sysDocDistDateTime, sysDocPath, sysDocEmailTo, sysDocFaxToPhone, sysDocFaxToAttention, sysDocBody, sysDocSubject) VALUES('"
        strSQL += _sysDocType + "','"
		strSQL += _sysDocDistType + "','"
		strSQL += _sysDocDistDateTime + "','"
		strSQL += _sysDocPath + "','"
		strSQL += _sysDocEmailTo + "','"
		strSQL += _sysDocFaxToPhone + "','"
		strSQL += _sysDocFaxToAttention + "','"
		strSQL += _sysDocBody + "','"
		strSQL += _sysDocSubject + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update Documents
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateDocuments() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE sysDocuments set "
		strSQL += "sysDocType='" + _sysDocType + "', "
		strSQL += "sysDocDistType='" + _sysDocDistType + "', "
		strSQL += "sysDocDistDateTime='" + _sysDocDistDateTime + "', "
		strSQL += "sysDocPath='" + _sysDocPath + "', "
		strSQL += "sysDocEmailTo='" + _sysDocEmailTo + "', "
		strSQL += "sysDocFaxToPhone='" + _sysDocFaxToPhone + "', "
		strSQL += "sysDocFaxToAttention='" + _sysDocFaxToAttention + "', "
		strSQL += "sysDocBody='" + _sysDocBody + "', "
		strSQL += "sysDocSubject='" + _sysDocSubject + "'"
		strSQL += " where sysDocId='" + _sysDocId + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of Documents
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getDocumentsInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  sysDocType, sysDocDistType, sysDocDistDateTime, sysDocPath, sysDocEmailTo, sysDocFaxToPhone, sysDocFaxToAttention, sysDocBody, sysDocSubject FROM sysDocuments where sysDocId='" + _sysDocId + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_sysDocType = drObj.Item("sysDocType").ToString
			_sysDocDistType = drObj.Item("sysDocDistType").ToString
			_sysDocDistDateTime = drObj.Item("sysDocDistDateTime").ToString
			_sysDocPath = drObj.Item("sysDocPath").ToString
			_sysDocEmailTo = drObj.Item("sysDocEmailTo").ToString
			_sysDocFaxToPhone = drObj.Item("sysDocFaxToPhone").ToString
			_sysDocFaxToAttention = drObj.Item("sysDocFaxToAttention").ToString
			_sysDocBody = drObj.Item("sysDocBody").ToString
			_sysDocSubject = drObj.Item("sysDocSubject").ToString
		End While
        drObj.Close()
        CloseDatabaseConnection()
	End Sub
End Class

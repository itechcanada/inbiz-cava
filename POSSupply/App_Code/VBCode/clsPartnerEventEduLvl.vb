Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsPartnerEventEduLvl
	Inherits clsDataClass
	Private _PartnerEvenEduLvlID, _EventEduLvlDescLang, _EventEduLvlDesc, _EventEduLvlActive As String
	Public Property PartnerEvenEduLvlID() As String
		Get
			Return _PartnerEvenEduLvlID
		End Get
		Set(ByVal value As String)
            _PartnerEvenEduLvlID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property EventEduLvlDescLang() As String
		Get
			Return _EventEduLvlDescLang
		End Get
		Set(ByVal value As String)
            _EventEduLvlDescLang = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property EventEduLvlDesc() As String
		Get
			Return _EventEduLvlDesc
		End Get
		Set(ByVal value As String)
            _EventEduLvlDesc = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property EventEduLvlActive() As String
		Get
			Return _EventEduLvlActive
		End Get
		Set(ByVal value As String)
            _EventEduLvlActive = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    '''  Insert PartnerEventEduLvl
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertPartnerEventEduLvl() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO partnereventedulvl( EventEduLvlDescLang, EventEduLvlDesc, EventEduLvlActive) VALUES('"
		strSQL += _EventEduLvlDescLang + "','"
		strSQL += _EventEduLvlDesc + "','"
		strSQL += _EventEduLvlActive + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update PartnerEventEduLvl
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updatePartnerEventEduLvl() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE partnereventedulvl set "
		strSQL += "EventEduLvlDescLang='" + _EventEduLvlDescLang + "', "
		strSQL += "EventEduLvlDesc='" + _EventEduLvlDesc + "', "
		strSQL += "EventEduLvlActive='" + _EventEduLvlActive + "'"
		strSQL += " where PartnerEvenEduLvlID='" + _PartnerEvenEduLvlID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of PartnerEventEduLvl
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPartnerEventEduLvlInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  EventEduLvlDescLang, EventEduLvlDesc, EventEduLvlActive FROM partnereventedulvl where PartnerEvenEduLvlID='" + _PartnerEvenEduLvlID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_EventEduLvlDescLang = drObj.Item("EventEduLvlDescLang").ToString
			_EventEduLvlDesc = drObj.Item("EventEduLvlDesc").ToString
			_EventEduLvlActive = drObj.Item("EventEduLvlActive").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill Grid of EventVisitors
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String = ""
        Dim strLang As String = clsCommon.funGetProductLang()

        strSQL = "SELECT PartnerEvenEduLvlID, EventEduLvlDescLang, EventEduLvlDesc, EventEduLvlActive FROM partnereventedulvl where EventEduLvlDescLang='" & strLang & "' "
        strSQL += " order by PartnerEvenEduLvlID "
        Return strSQL
    End Function
End Class

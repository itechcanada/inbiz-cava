Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.Odbc
Imports System.Web.HttpContext
Imports iTECH.InbizERP.BusinessLogic
Imports iTECH.Library.Utilities


Public Class clsImport

    Public Function ImportDistributor(ByVal CSVFilePath As String) As Integer
        Dim strSql As String
        'strSql = "load data local infile '" & CSVFilePath.Replace("\", "\\") & "' into table distributor fields terminated by ',' enclosed by '""' lines terminated by '\n' ignore 1 lines (distName, distPhone, distFax, distEmail, distType, distTaxCode, distCommissionCode, distDiscount, distCurrencyCode, distInvoiceNetTerms, distShipBlankPref, distCourierCode, distLang, distCustomerID, distValidated, distActive)"
        strSql = "load data local infile '" & CSVFilePath.Replace("\", "\\") & "' into table partners fields terminated by ',' enclosed by '""' lines terminated by '\n' ignore 1 lines (PartnerLongName, PartnerPhone, PartnerFax, PartnerEmail, PartnerWebsite, PartnerActive, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, PartnerAcronyme, PartnerNote, PartnerValidated, PartnerTaxCode, PartnerCommissionCode, PartnerDiscount, PartnerCurrencyCode, PartnerInvoiceNetTerms, PartnerShipBlankPref, PartnerCourierCode, PartnerLang, PartnerStatus, PartnerValue)"
        Dim objDb As clsDataClass = New clsDataClass
        Return objDb.SetData(strSql)
    End Function

    Public Function ImportReseller(ByVal CSVFilePath As String) As Integer
        Dim strSql As String
        strSql = "load data local infile '" & CSVFilePath.Replace("\", "\\") & "' into table partners fields terminated by ',' enclosed by '""' lines terminated by '\n' ignore 1 lines (PartnerLongName, PartnerPhone, PartnerFax, PartnerEmail, PartnerWebsite, PartnerActive, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, PartnerAcronyme, PartnerNote, PartnerValidated, PartnerTaxCode, PartnerCommissionCode, PartnerDiscount, PartnerCurrencyCode, PartnerInvoiceNetTerms, PartnerShipBlankPref, PartnerCourierCode, PartnerLang, PartnerStatus, PartnerValue)"

        Dim objDb As clsDataClass = New clsDataClass
        Return objDb.SetData(strSql)
    End Function

    Public Function ImportEndClient(ByVal CSVFilePath As String) As Integer
        Dim strSql As String
        strSql = "load data local infile '" & CSVFilePath.Replace("\", "\\") & "' into table partners fields terminated by ',' enclosed by '""' lines terminated by '\n' ignore 1 lines (PartnerLongName, PartnerPhone, PartnerFax, PartnerEmail, PartnerWebsite, PartnerActive, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, PartnerAcronyme, PartnerNote, PartnerValidated, PartnerTaxCode, PartnerCommissionCode, PartnerDiscount, PartnerCurrencyCode, PartnerInvoiceNetTerms, PartnerShipBlankPref, PartnerCourierCode, PartnerLang, PartnerStatus, PartnerValue)"

        Dim objDb As clsDataClass = New clsDataClass
        Return objDb.SetData(strSql)
    End Function
    'Public Function ImportPartner(ByVal CSVFilePath As String) As Integer
    '    Dim strSql As String
    '    strSql = "load data local infile '" & CSVFilePath.Replace("\", "\\") & "' into table partners fields terminated by ',' enclosed by '""' lines terminated by '\n' ignore 1 lines (PartnerLongName, PartnerPhone, PartnerFax, PartnerEmail, PartnerWebsite, PartnerActive, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, PartnerAcronyme, PartnerNote, PartnerValidated, PartnerTaxCode, PartnerCommissionCode, PartnerDiscount, PartnerCurrencyCode, PartnerInvoiceNetTerms, PartnerShipBlankPref, PartnerCourierCode, PartnerLang, PartnerStatus, PartnerValue)"

    '    Dim objDb As clsDataClass = New clsDataClass
    '    Return objDb.SetData(strSql)
    'End Function

    Public Function ImportVendor(ByVal CSVFilePath As String) As Integer
        Dim strSql As String
        strSql = "load data local infile '" & CSVFilePath.Replace("\", "\\") & "' into table vendor fields terminated by ',' enclosed by '""' lines terminated by '\n' ignore 1 lines (vendorName, vendorEmailID, vendorFax, vendorPhone, vendorPOPref, vendorCurrency, vendorStatus, vendorValidated, vendorActive)"

        Dim objDb As clsDataClass = New clsDataClass
        Return objDb.SetData(strSql)
    End Function

    Public Function ImportProduct(ByVal CSVFolderPath As String, ByVal CSVFile As String) As Integer
        Dim conn As OdbcConnection
        Dim ds As DataSet = New DataSet
        Dim da As OdbcDataAdapter
        Dim connectionString As String

        connectionString = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + CSVFolderPath + ";"
        conn = New Odbc.OdbcConnection(connectionString)

        da = New OdbcDataAdapter("select * from [" + CSVFile + "]", conn)
        da.Fill(ds)

        Dim intRecords As Integer = 0
        Dim i As Integer
        Dim ProductId As Integer

        Dim strSql As String
        Dim objDb As clsDataClass = New clsDataClass

        'Open Database Connection
        objDb.OpenDatabaseConnection()
        Dim strProductType As String = String.Empty
        Dim prdType As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Try
                strProductType = GetValue(ds, i, "prdType")
                If Not Integer.TryParse(strProductType, prdType) Then
                    prdType = 1
                End If

                strSql = "insert into products (prdExtID, prdUPCCode, prdName, prdMinQtyPerSO, prdSalePricePerMinQty, prdEndUserSalesPrice, prdWebSalesPrice, prdIsGlutenFree, prdSpicyLevel, prdIsContainsNuts, prdIsVegetarian, " 'Edited by mukesh 20130722' To add columns'
                strSql += "prdCreatedUserID, prdCreatedOn, prdMinQtyPOTrig, prdAutoPO, prdPOQty, prdIsActive, prdIsWeb, isPOSMenu, prdLength, prdLengthPkg, prdWidth,"
                strSql += "prdWidthPkg, prdIsKit, prdDiscount, prdIsSpecial, prdType,prdWeight) values ('" & GetValue(ds, i, "prdExtID") & "', '" & GetValue(ds, i, "prdUPCCode") & "', '" & GetValue(ds, i, "prdName") & "', "
                strSql += GetValue(ds, i, "prdMinQtyPerSO") & ", " & GetValue(ds, i, "prdSalePricePerMinQty") & ", " & GetValue(ds, i, "prdEndUserSalesPrice") & ", "
                strSql += GetValue(ds, i, "prdWebSalesPrice") & ", " & GetValue(ds, i, "prdIsGlutenFree") & ", " & GetValue(ds, i, "prdSpicyLevel") & ", " & GetValue(ds, i, "prdIsContainsNuts") & ", " & GetValue(ds, i, "prdIsVegetarian") & ", 1, now(), 1, 0, 0, 1, 1, 0, '" & GetValue(ds, i, "prdLength") & "', '" 'Edited by mukesh 20130722' To add columns'
                strSql += GetValue(ds, i, "prdLengthPkg") & "', '" & GetValue(ds, i, "prdWidth") & "', '" & GetValue(ds, i, "prdWidthPkg") & "', 0, 0, 0, '" & prdType & "','" & GetValue(ds, i, "prdWeight") & "') "
                objDb.ExecuteNnQuery(strSql)

                intRecords += 1 'consider as a record inserted wether following statments will execute or not

                strSql = "Select max(productID) from products;"
                ProductId = objDb.GetScalarDataByQuery(strSql)

                strSql = "insert into prddescriptions (id, descLang, prdName, prdSmallDesc, prdLargeDesc) values ('" & ProductId & "', 'en'" & ", '"
                strSql += GetValue(ds, i, "prdName") & "', '" & GetValue(ds, i, "prdSmallDesc") & "', '" & GetValue(ds, i, "prdLargeDesc") & "') "
                objDb.ExecuteNnQuery(strSql)

                strSql = "insert into prddescriptions (id, descLang, prdName, prdSmallDesc, prdLargeDesc) values ('" & ProductId & "', 'fr'" & ", '"
                strSql += GetValue(ds, i, "prdName") & "', '" & GetValue(ds, i, "prdSmallDesc") & "', '" & GetValue(ds, i, "prdLargeDesc") & "') "
                objDb.ExecuteNnQuery(strSql)

                strSql = "insert into prddescriptions (id, descLang, prdName, prdSmallDesc, prdLargeDesc) values ('" & ProductId & "', 'es'" & ", '"
                strSql += GetValue(ds, i, "prdName") & "', '" & GetValue(ds, i, "prdSmallDesc") & "', '" & GetValue(ds, i, "prdLargeDesc") & "') "
                objDb.ExecuteNnQuery(strSql)

                strSql = "insert into prdquantity (prdID, prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty) values ('" & ProductId & "', '"
                strSql += GetValue(ds, i, "prdWhsCode") & "', '" & GetValue(ds, i, "prdOhdQty") & "', 0, 0, 0) "
                objDb.ExecuteNnQuery(strSql)

                Try
                    strSql = "INSERT INTO prdassociatevendor(prdID, prdVendorID, prdCostPrice) VALUES('" & ProductId & "', '" & GetValue(ds, i, "vendorID") & "', '"
                    strSql += GetValue(ds, i, "prdCostPrice") & "')"
                    objDb.ExecuteNnQuery(strSql)
                Catch ex As Exception

                End Try

            Catch ex As Exception
                'Record Failed
                Throw
            End Try
        Next
        'Close Database Connection
        objDb.CloseDatabaseConnection()
        objDb = Nothing

        Return intRecords
    End Function

    Public Function ImportProductStyle(ByVal ds As DataSet) As String
        Dim _prd As Product = New Product()
        Dim intRecords As Integer = 0
        Dim i As Integer
        Dim ProductId As Integer
        Dim strColorID As String = String.Empty
        Dim prdName As String = String.Empty
        Dim prdDesFr As String = String.Empty
        Dim prdDesEn As String = String.Empty
        Dim styleName As String = String.Empty
        Dim collID As String = String.Empty
        Dim matID As String = String.Empty
        Dim gaugeID As String = String.Empty
        Dim textureID As String = String.Empty
        Dim webSiteID As String = String.Empty
        Dim webSiteSubCatgID As String = String.Empty
        Dim necklineID As String = String.Empty
        Dim extraCatgID As String = String.Empty
        Dim sleeveID As String = String.Empty
        Dim silhouetteID As String = String.Empty
        Dim trendID As String = String.Empty
        Dim keywordID As String = String.Empty
        Dim lengthSize As String = String.Empty
        Dim lengthInc As String = String.Empty
        Dim chest As String = String.Empty
        Dim chestInc As String = String.Empty
        Dim shoulder As String = String.Empty
        Dim shoulderInc As String = String.Empty
        Dim biceps As String = String.Empty
        Dim bicepsInc As String = String.Empty
        Dim FOB As String = String.Empty
        Dim Landed As String = String.Empty
        Dim WSPrice As String = String.Empty
        Dim RPrice As String = String.Empty
        Dim WebPrice As String = String.Empty
        Dim ProdWgt As String = String.Empty
        Dim ProdWgtIncr As String = String.Empty
        Dim PackageWgt As String = String.Empty
        Dim PackageWidth As String = String.Empty
        Dim PackageLength As String = String.Empty
        Dim IsWebActive As String = String.Empty
        Dim IsPOSMenu As String = String.Empty
        Dim SizeID As String = String.Empty
        Dim prdQty As String = String.Empty
        Dim lngSize As String = String.Empty
        Dim chstSize As String = String.Empty
        Dim sldSize As String = String.Empty
        Dim bcpSize As String = String.Empty
        Dim prdWgtSize As String = String.Empty
        Dim incCounter As Integer = 0
        Dim dublicateUPCCode As String = String.Empty
        Dim msgErrorCode As String = String.Empty
        Dim currentUPCCode As String = String.Empty
        msgErrorCode = "<tr><td colspan='2' style='font-weight:bold; font-size:14px' >" + Resources.Resource.lblStyleImportErrorMsg + "</td></tr><tr><td>SKU #</td><td>" + Resources.Resource.lblError + "</td></tr>"
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Try
                strColorID = GetValue(ds, i, "ColorID")
                If strColorID = "" Then
                    prdName = GetValue(ds, i, "Name")
                    WSPrice = GetValue(ds, i, "WholeSalePrice")
                    RPrice = GetValue(ds, i, "RetailPrice")
                    WebPrice = GetValue(ds, i, "WebPrice")
                    FOB = GetValue(ds, i, "FOB").Trim()
                    Landed = GetValue(ds, i, "Landed")
                    collID = GetValue(ds, i, "Season/Col_ID")
                    styleName = GetValue(ds, i, "STYLE")
                    matID = GetValue(ds, i, "Mat-ID")
                    ProdWgt = GetValue(ds, i, "ProdWgt")
                    PackageWgt = GetValue(ds, i, "PackageWgt")
                    PackageWidth = GetValue(ds, i, "PackageWidth")
                    PackageLength = GetValue(ds, i, "PackageLength")
                    IsWebActive = GetValue(ds, i, "IsWebActive")
                    IsPOSMenu = GetValue(ds, i, "IsPOSMenu")

                    prdDesFr = GetValue(ds, i, "Description Fr")
                    prdDesEn = GetValue(ds, i, "Description En")

                    textureID = GetValue(ds, i, "Texture-ID")
                    webSiteID = GetValue(ds, i, "Website Category-ID")
                    webSiteSubCatgID = GetValue(ds, i, "Website Sub Catg-ID")
                    necklineID = GetValue(ds, i, "Neckline-ID")
                    extraCatgID = GetValue(ds, i, "Extra Catg-ID")
                    sleeveID = GetValue(ds, i, "Sleeve-ID")
                    silhouetteID = GetValue(ds, i, "Silhouette-ID")
                    trendID = GetValue(ds, i, "Trend-ID")
                    keywordID = GetValue(ds, i, "Keywords-ID")

                    gaugeID = GetValue(ds, i, "Gauge-ID")
                    lengthSize = GetValue(ds, i, "LengthSize")
                    lengthInc = GetValue(ds, i, "Length Increment")
                    chest = GetValue(ds, i, "Chest")
                    chestInc = GetValue(ds, i, "Chest Inc")
                    shoulder = GetValue(ds, i, "Shoulder")
                    shoulderInc = GetValue(ds, i, "Shoulder Inc")
                    biceps = GetValue(ds, i, "Biceps")
                    bicepsInc = GetValue(ds, i, "Biceps Inc")
                    ProdWgtIncr = GetValue(ds, i, "ProdWgtIncr")
                Else

                End If
                If String.IsNullOrEmpty(strColorID) = False Then

                    For j = 37 To ds.Tables(0).Columns.Count - 1
                        Try
                            SizeID = ds.Tables(0).Columns(j).ColumnName
                            prdQty = ds.Tables(0).Rows(i)(j + 1).ToString()
                            If String.IsNullOrEmpty(prdQty) Then
                                prdQty = "0"
                            End If
                            _prd.PrdUPCCode = ds.Tables(0).Rows(i)(j).ToString()
                            currentUPCCode = _prd.PrdUPCCode
                            If j = 37 Then
                                incCounter = 0
                            End If
                            j += 1
                            incCounter += 1
                            If (_prd.IsProductUPCCodeDuplicate() = False And (String.IsNullOrEmpty(_prd.PrdUPCCode) = False)) Then
                                lngSize = (BusinessUtility.GetDouble(lengthSize) + BusinessUtility.GetDouble(lengthInc) * incCounter).ToString()
                                chstSize = (BusinessUtility.GetDouble(chest) + BusinessUtility.GetDouble(chestInc) * incCounter).ToString()
                                sldSize = (BusinessUtility.GetDouble(shoulder) + BusinessUtility.GetDouble(shoulderInc) * incCounter).ToString()
                                bcpSize = (BusinessUtility.GetDouble(biceps) + BusinessUtility.GetDouble(bicepsInc) * incCounter).ToString()
                                prdWgtSize = (BusinessUtility.GetDouble(ProdWgt) + BusinessUtility.GetDouble(ProdWgtIncr) * incCounter).ToString()
                                ProductId = ImportStyle(_prd.PrdUPCCode, prdName, WSPrice, RPrice, WebPrice, FOB, Landed, collID, styleName, matID, prdWgtSize, PackageWgt, PackageWidth, PackageLength, IsWebActive, IsPOSMenu, prdDesEn, prdDesFr, prdQty, lngSize, chstSize, sldSize, bcpSize, strColorID, SizeID)
                                If ProductId > 0 Then
                                    ImportCatgDtls(ProductId, webSiteID, webSiteSubCatgID, necklineID, extraCatgID, sleeveID, silhouetteID, trendID, keywordID, gaugeID)
                                    intRecords += 1
                                    Dim textureIDs As String() = textureID.Split(",")
                                    For Each s As String In textureIDs
                                        If s.Trim() <> "" Then
                                            ImportTexture(ProductId, s)
                                        End If
                                    Next s
                                End If
                            ElseIf String.IsNullOrEmpty(_prd.PrdUPCCode) = False Then
                                msgErrorCode += "<tr><td> " + _prd.PrdUPCCode + "</td><td>402</td></tr>"
                                dublicateUPCCode += _prd.PrdUPCCode + ","
                            End If

                            'sLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sLength) + (BusinessUtility.GetDouble(LengthIncrement) * iRowCnt));


                        Catch ex As Exception
                            msgErrorCode += "<tr><td> " + currentUPCCode + "</td><td>401</td></tr>"
                        End Try
                    Next 'end cell reading of one row

                End If
            Catch ex As Exception
                'error
                Throw
            End Try
        Next
        Return BusinessUtility.GetString(intRecords) + "," + msgErrorCode
    End Function

    Public Function ImportStyle(ByVal SKU_XS As String, ByVal prdName As String, ByVal WSPrice As String, ByVal RPrice As String, ByVal WebPrice As String, ByVal FOB As String, ByVal Landed As String, ByVal collID As String, ByVal styleName As String, ByVal matID As String, ByVal ProdWgt As String, ByVal PackageWgt As String, ByVal PackageWidth As String, ByVal PackageLength As String, ByVal IsWebActive As String, ByVal IsPOSMenu As String, ByVal prdDesEn As String, ByVal prdDesFr As String, ByVal qty As String, ByVal leng As String, ByVal chest As String, ByVal shoulder As String, ByVal biceps As String, ByVal colorID As String, ByVal sizeID As String) As Integer
        Dim ProductId As Integer
        Dim strSql As String
        Dim objDb As clsDataClass = New clsDataClass
        'Open Database Connection
        'Try
        objDb.OpenDatabaseConnection()
        strSql = "insert into products (prdUPCCode, prdName, prdSalePricePerMinQty, prdEndUserSalesPrice, prdWebSalesPrice, prdFOBPrice, prdLandedPrice, Style, "
        strSql += "MatID, prdWeight, prdWeightPkg, prdWidthPkg, prdLengthPkg, prdIsWeb, IsPOSMenu,prdCreatedUserID, prdCreatedOn,prdIsActive,prdType) "
        strSql += " values ('" & SKU_XS & "', '" & prdName & "', '" & BusinessUtility.GetDouble(WSPrice) & "','" & BusinessUtility.GetDouble(RPrice) & "', '" & BusinessUtility.GetDouble(WebPrice) & "', '" & BusinessUtility.GetDouble(FOB) & "','" & BusinessUtility.GetDouble(Landed) & "','" & styleName & "', '" & BusinessUtility.GetInt(matID) & "', '" & ProdWgt & "','" & PackageWgt & "','" & PackageWidth & "','" & PackageLength & "','" & BusinessUtility.GetInt(IsWebActive) & "','" & BusinessUtility.GetInt(IsPOSMenu) & "', '" & CurrentUser.UserID & "',now(),1,1) "
        objDb.ExecuteNnQuery(strSql)

        strSql = "Select max(productID) from products;"
        ProductId = objDb.GetScalarDataByQuery(strSql)

        strSql = "insert into prddescriptions (id, descLang, prdName, prdSmallDesc, prdLargeDesc) values ('" & ProductId & "', 'en'" & ", '"
        strSql += prdName & "', '" & prdDesEn & "', '" & prdDesEn & "') "
        objDb.ExecuteNnQuery(strSql)

        strSql = "insert into prddescriptions (id, descLang, prdName, prdSmallDesc, prdLargeDesc) values ('" & ProductId & "', 'fr'" & ", '"
        strSql += prdName & "', '" & prdDesFr & "', '" & prdDesFr & "') "
        objDb.ExecuteNnQuery(strSql)

        strSql = "insert into prdquantity (prdID, prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty) values ('" & ProductId & "', '"
        strSql += CurrentUser.UserDefaultWarehouse & "', '" & BusinessUtility.GetDouble(qty) & "', 0, 0, 0) "
        objDb.ExecuteNnQuery(strSql)

        strSql = "INSERT INTO ProductClothDesc (productID, style,  size, longueurLength, epauleShoulder, busteChest, bicep, material, collection, color) values ('" & ProductId & "', '"
        strSql += "" & styleName & "', '" & BusinessUtility.GetInt(sizeID) & "','" & leng & "','" & shoulder & "','" & chest & "','" & biceps & "','" & BusinessUtility.GetInt(matID) & "','" & BusinessUtility.GetInt(collID) & "','" & BusinessUtility.GetInt(colorID) & "') "
        objDb.ExecuteNnQuery(strSql)

        'Catch ex As Exception
        'End Try


        'Close Database Connection
        objDb.CloseDatabaseConnection()
        objDb = Nothing

        Return ProductId
    End Function

    Public Function ImportTexture(ByVal prdID As String, ByVal textureID As String) As Boolean
        Dim strSql As String
        Dim objDb As clsDataClass = New clsDataClass
        'Open Database Connection
        'Try
        objDb.OpenDatabaseConnection()
        strSql = "INSERT INTO productAssociateTexture (ProductID, TextureID) "
        strSql += " values ('" & prdID & "', '" & BusinessUtility.GetInt(textureID) & "') "
        objDb.ExecuteNnQuery(strSql)

        'Catch ex As Exception
        'End Try

        'Close Database Connection
        objDb.CloseDatabaseConnection()
        objDb = Nothing

        Return True
    End Function

    Public Function ImportWebsite(ByVal prdID As String, ByVal catgDtlID As String) As Boolean
        Dim strSql As String
        Dim objDb As clsDataClass = New clsDataClass
        'Open Database Connection
        'Try
        objDb.OpenDatabaseConnection()
        strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  "
        strSql += " values ('" & prdID & "', '" & BusinessUtility.GetInt(catgDtlID) & "', 1) "
        objDb.ExecuteNnQuery(strSql)

        'Catch ex As Exception
        'End Try

        'Close Database Connection
        objDb.CloseDatabaseConnection()
        objDb = Nothing

        Return True
    End Function

    Public Function ImportWebsiteSubCatg(ByVal prdID As String, ByVal catgDtlID As String) As Boolean
        Dim strSql As String
        Dim objDb As clsDataClass = New clsDataClass
        'Open Database Connection
        'Try
        objDb.OpenDatabaseConnection()
        strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  "
        strSql += " values ('" & prdID & "', '" & BusinessUtility.GetInt(catgDtlID) & "', 8) "
        objDb.ExecuteNnQuery(strSql)

        'Catch ex As Exception
        'End Try

        'Close Database Connection
        objDb.CloseDatabaseConnection()
        objDb = Nothing

        Return True
    End Function

    Public Function ImportNeckline(ByVal prdID As String, ByVal catgDtlID As String) As Boolean
        Dim strSql As String
        Dim objDb As clsDataClass = New clsDataClass
        'Open Database Connection
        'Try
        objDb.OpenDatabaseConnection()
        strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  "
        strSql += " values ('" & prdID & "', '" & BusinessUtility.GetInt(catgDtlID) & "', 2) "
        objDb.ExecuteNnQuery(strSql)

        'Catch ex As Exception
        'End Try

        'Close Database Connection
        objDb.CloseDatabaseConnection()
        objDb = Nothing

        Return True
    End Function

    Public Function ImportExtraCatg(ByVal prdID As String, ByVal catgDtlID As String) As Boolean
        Dim strSql As String
        Dim objDb As clsDataClass = New clsDataClass
        'Open Database Connection
        'Try
        objDb.OpenDatabaseConnection()
        strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  "
        strSql += " values ('" & prdID & "', '" & BusinessUtility.GetInt(catgDtlID) & "', 3) "
        objDb.ExecuteNnQuery(strSql)

        'Catch ex As Exception
        'End Try

        'Close Database Connection
        objDb.CloseDatabaseConnection()
        objDb = Nothing

        Return True
    End Function

    Public Function ImportSleeve(ByVal prdID As String, ByVal catgDtlID As String) As Boolean
        Dim strSql As String
        Dim objDb As clsDataClass = New clsDataClass
        'Open Database Connection
        'Try
        objDb.OpenDatabaseConnection()
        strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  "
        strSql += " values ('" & prdID & "', '" & BusinessUtility.GetInt(catgDtlID) & "', 4) "
        objDb.ExecuteNnQuery(strSql)

        'Catch ex As Exception
        'End Try

        'Close Database Connection
        objDb.CloseDatabaseConnection()
        objDb = Nothing

        Return True
    End Function

    Public Function ImportCatgDtls(ByVal prdID As String, ByVal webSiteID As String, ByVal webSiteSubCatgID As String, ByVal necklineID As String, ByVal extraCatgID As String, ByVal sleeveID As String, ByVal silhouetteIDs As String, ByVal trendIDs As String, ByVal keywordIDs As String, ByVal gaugeIDs As String) As Boolean
        Dim strSql As String
        Dim objDb As clsDataClass = New clsDataClass

        'Try


        Dim websiteIDs As String() = webSiteID.Split(",")
        For Each s As String In websiteIDs
            If s.Trim() <> "" Then
                ImportWebsite(prdID, s)
            End If
        Next s

        Dim websiteSubCgIDs As String() = webSiteSubCatgID.Split(",")
        For Each s As String In websiteSubCgIDs
            If s.Trim() <> "" Then
                ImportWebsiteSubCatg(prdID, s)
            End If
        Next s

        Dim necklineIDs As String() = necklineID.Split(",")
        For Each s As String In necklineIDs
            If s.Trim() <> "" Then
                ImportNeckline(prdID, s)
            End If
        Next s

        Dim extraCatgIDs As String() = extraCatgID.Split(",")
        For Each s As String In extraCatgIDs
            If s.Trim() <> "" Then
                ImportExtraCatg(prdID, s)
            End If
        Next s

        Dim sleeveIDs As String() = sleeveID.Split(",")
        For Each s As String In sleeveIDs
            If s.Trim() <> "" Then
                ImportSleeve(prdID, s)
            End If
        Next s

        'Open Database Connection
        objDb.OpenDatabaseConnection()
        Dim silIDs As String() = silhouetteIDs.Split(",")
        For Each s As String In silIDs
            If s.Trim() <> "" Then
                strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  "
                strSql += " values ('" & prdID & "', '" & BusinessUtility.GetInt(s) & "', 5) "
                objDb.ExecuteNnQuery(strSql)
            End If
        Next s

        Dim treIDs As String() = trendIDs.Split(",")
        For Each s As String In treIDs
            If s.Trim() <> "" Then
                strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  "
                strSql += " values ('" & prdID & "', '" & BusinessUtility.GetInt(s) & "', 6) "
                objDb.ExecuteNnQuery(strSql)
            End If
        Next s

        Dim keyIDs As String() = keywordIDs.Split(",")
        For Each s As String In keyIDs
            If s.Trim() <> "" Then
                strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  "
                strSql += " values ('" & prdID & "', '" & BusinessUtility.GetInt(s) & "', 7) "
                objDb.ExecuteNnQuery(strSql)
            End If
        Next s

        Dim guagIDs As String() = gaugeIDs.Split(",")
        For Each s As String In guagIDs
            If s.Trim() <> "" Then
                strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  "
                strSql += " values ('" & prdID & "', '" & BusinessUtility.GetInt(s) & "', 9) "
                objDb.ExecuteNnQuery(strSql)
            End If
        Next s

        'Catch ex As Exception
        'End Try

        'Close Database Connection
        objDb.CloseDatabaseConnection()
        objDb = Nothing

        Return True
    End Function

    Public Function ImportCustomers(ByVal CSVFolderPath As String, ByVal CSVFile As String, ByVal strAutoAssignSR As String) As Integer
        Dim conn As OdbcConnection

        Dim ds As DataSet = New DataSet
        Dim da As OdbcDataAdapter
        Dim connectionString As String

        connectionString = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + CSVFolderPath + ";"
        conn = New Odbc.OdbcConnection(connectionString)

        da = New OdbcDataAdapter("select * from [" + CSVFile + "]", conn)
        da.Fill(ds)

        Dim intRecords As Integer = 0
        Dim i As Integer
        Dim CustomerId As Integer
        Dim ContactId As Integer
        Dim CustomerType As StringBuilder = New StringBuilder
        Dim Name() As String
        Dim ContactName As StringBuilder = New StringBuilder
        Dim FirstName As StringBuilder = New StringBuilder
        Dim LastName As StringBuilder = New StringBuilder
        Dim ContactPhone As StringBuilder = New StringBuilder
        Dim ContactEmail As StringBuilder = New StringBuilder
        Dim ContactFax As StringBuilder = New StringBuilder
        Dim ContactTitle As StringBuilder = New StringBuilder

        Dim strSql As StringBuilder
        Dim objDb As clsDataClass = New clsDataClass
        'Open Database Connection
        objDb.OpenDatabaseConnection()
        Dim strSR() As String
        Dim intI, intJ As Integer
        If strAutoAssignSR = "Y" Then
            strSR = funGetSRForImport(objDb).Split("~")
            intI = strSR.Length - 1
        End If
        Dim txCode, partEmail As String
        Dim dtO As Date = Date.Now
        Dim strLog As String = String.Empty
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Try
                txCode = GetValue(ds, i, "PartnerTaxCode").Trim("'").Trim()
                partEmail = GetValue(ds, i, "PartnerEmail")
                If String.IsNullOrEmpty(txCode) OrElse Not IsNumeric(txCode) Then
                    txCode = "NULL"
                End If
                If String.IsNullOrEmpty(partEmail) Then
                    partEmail = String.Format("{0}@noemail.com", dtO.ToString("yyyyMMddHHmmssffff"))
                End If
                dtO = dtO.AddSeconds(1)
                strSql = New StringBuilder
                strSql.Append("insert into partners (PartnerLongName, PartnerPhone, PartnerFax, PartnerEmail, PartnerWebsite, PartnerAcronyme, PartnerNote, PartnerTaxCode, ")
                strSql.Append("PartnerCurrencyCode, PartnerLang, PartnerActive, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerValue, PartnerInvoiceNetTerms,PartnerDiscount, PartnerCreditAvailable, PartnerType) ")
                strSql.Append("values ('" & GetValue(ds, i, "PartnerLongName") & "', '" & GetValue(ds, i, "PartnerPhone") & "', '" & GetValue(ds, i, "PartnerFax") & "', '")
                strSql.Append(partEmail & "', '" & GetValue(ds, i, "PartnerWebsite") & "', '" & GetValue(ds, i, "PartnerAcronyme") & "', '")
                strSql.Append(GetValue(ds, i, "PartnerNote") & "', " & txCode & ", '" & GetValue(ds, i, "PartnerCurrencyCode") & "', '")
                strSql.Append(GetValue(ds, i, "PartnerLang") & "', 1, now(), '" & Current.Session("userID") & "', now(), '" & Current.Session("userID") & "', '" & GetValue(ds, i, "PartnerTaxCode") & "'").Append(",")
                strSql.Append("'").Append(GetValue(ds, i, "PartnerInvoiceNetTerms")).Append("'").Append(",")
                strSql.Append("'").Append(BusinessUtility.GetInt(GetValue(ds, i, "PartnerDiscount"))).Append("'").Append(",")
                strSql.Append("'").Append(BusinessUtility.GetDouble(GetValue(ds, i, "PartnerCreditAvailable"))).Append("'").Append(",")
                strSql.Append("'").Append(BusinessUtility.GetInt(GetValue(ds, i, "PartnerType"))).Append("'").Append(")")
                objDb.ExecuteNnQuery(strSql.ToString)

                strSql = New StringBuilder
                strSql.Append("Select max(PartnerID) from partners;")
                CustomerId = objDb.GetScalarDataByQuery(strSql.ToString)

                ContactName = New StringBuilder
                FirstName = New StringBuilder
                LastName = New StringBuilder
                If GetValue(ds, i, "ContactName") <> "" Then
                    ContactName.Append(GetValue(ds, i, "ContactName"))
                Else
                    ContactName.Append(GetValue(ds, i, "PartnerLongName"))
                End If
                Name = ContactName.ToString.Split(" ")
                If Name.Length > 1 Then
                    LastName.Append(Name(Name.Length - 1))
                    FirstName.Append(ContactName.ToString.Replace(" " & LastName.ToString, ""))
                Else
                    LastName.Append(Name(0))
                End If

                ContactPhone = New StringBuilder
                If GetValue(ds, i, "ContactPhone") <> "" Then
                    ContactPhone.Append(GetValue(ds, i, "ContactPhone"))
                Else
                    ContactPhone.Append(GetValue(ds, i, "PartnerPhone"))
                End If

                ContactEmail = New StringBuilder
                If GetValue(ds, i, "ContactEmail") <> "" Then
                    ContactEmail.Append(GetValue(ds, i, "ContactEmail"))
                Else
                    ContactEmail.Append(GetValue(ds, i, "PartnerEmail"))
                End If

                ContactFax = New StringBuilder
                If GetValue(ds, i, "ContactFax") <> "" Then
                    ContactFax.Append(GetValue(ds, i, "ContactFax"))
                Else
                    ContactFax.Append(GetValue(ds, i, "PartnerFax"))
                End If

                ContactTitle = New StringBuilder
                If GetValue(ds, i, "PartnerLang") = "fr" Then
                    ContactTitle.Append("ContactTitleFr")
                Else
                    ContactTitle.Append("ContactTitleEn")
                End If

                strSql = New StringBuilder
                strSql.Append("insert into partnercontacts (ContactPartnerID, ContactLastName, ContactFirstName, " & ContactTitle.ToString & ", ContactPhone, ContactPhoneExt, ")
                strSql.Append("ContactEmail, ContactFax, ContactLangPref, ContactCreatedOn, ContactCreatedBy, ContactLastUpdatedOn, ContactLastUpdatedBy, ")
                strSql.Append("ContactActive, ContactPrimary, ContactSelSpecialization, IsSubscribeEmail) values ")
                strSql.Append("('" & CustomerId & "', '" & LastName.ToString & "', '" & FirstName.ToString & "', '" & GetValue(ds, i, "ContactTitle") & "', '" & ContactPhone.ToString & "', ")
                strSql.Append("'" & GetValue(ds, i, "ContactPhoneExt") & "', '" & ContactEmail.ToString & "', '" & ContactFax.ToString & "', '" & GetValue(ds, i, "PartnerLang") & "', ")
                strSql.Append("now(), '" & Current.Session("userID") & "', now(), '" & Current.Session("userID") & "', 1, 1, 0, 1) ")
                objDb.ExecuteNnQuery(strSql.ToString)

                strSql = New StringBuilder
                strSql.Append("Select max(ContactID) from partnercontacts;")
                ContactId = objDb.GetScalarDataByQuery(strSql.ToString)

                strSql = New StringBuilder
                strSql.Append("insert into partnerseltype (PartnerSelTypeID, PartnerSelPartID) values ('" & GetValue(ds, i, "PartnerType") & "', '" & CustomerId & "') ")
                objDb.ExecuteNnQuery(strSql.ToString)

                CustomerType = New StringBuilder
                If GetValue(ds, i, "PartnerType") = "1" Then
                    CustomerType.Append("D")
                ElseIf GetValue(ds, i, "PartnerType") = "2" Then
                    CustomerType.Append("E")
                ElseIf GetValue(ds, i, "PartnerType") = "3" Then
                    CustomerType.Append("R")
                ElseIf GetValue(ds, i, "PartnerType") = "4" Then
                    CustomerType.Append("L")
                End If

                If strAutoAssignSR = "Y" And CustomerType.ToString = "L" Then
                    If intI < intJ Then
                        intJ = 0
                    End If
                    If intI >= intJ Then
                        strSql = New StringBuilder
                        strSql.Append("INSERT INTO assignleads(UserID, CustomerID, CustomerType, Active) VALUES('" & strSR(intJ) & "','" & CustomerId & "','L','1')")
                        objDb.ExecuteNnQuery(strSql.ToString)
                        intJ += 1
                    End If
                End If

                strSql = New StringBuilder
                strSql.Append("insert into addresses (addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode) ")
                strSql.Append("values ('" & CustomerType.ToString & "', 'H', '" & CustomerId & "', '" & GetValue(ds, i, "addressLine1") & "', '" & GetValue(ds, i, "addressLine2") & "', '")
                strSql.Append(GetValue(ds, i, "addressLine3") & "', '" & GetValue(ds, i, "addressCity") & "', '" & GetValue(ds, i, "addressState") & "', '")
                strSql.Append(GetValue(ds, i, "addressCountry") & "', '" & GetValue(ds, i, "addressPostalCode") & "') ")
                objDb.ExecuteNnQuery(strSql.ToString)

                strSql = New StringBuilder
                strSql.Append("insert into addresses (addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode) ")
                strSql.Append("values ('" & CustomerType.ToString & "', 'B', '" & CustomerId & "', '" & GetValue(ds, i, "addressLine1") & "', '" & GetValue(ds, i, "addressLine2") & "', '")
                strSql.Append(GetValue(ds, i, "addressLine3") & "', '" & GetValue(ds, i, "addressCity") & "', '" & GetValue(ds, i, "addressState") & "', '")
                strSql.Append(GetValue(ds, i, "addressCountry") & "', '" & GetValue(ds, i, "addressPostalCode") & "') ")
                objDb.ExecuteNnQuery(strSql.ToString)

                strSql = New StringBuilder
                strSql.Append("insert into addresses (addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode) ")
                strSql.Append("values ('" & CustomerType.ToString & "', 'S', '" & CustomerId & "', '" & GetValue(ds, i, "addressLine1") & "', '" & GetValue(ds, i, "addressLine2") & "', '")
                strSql.Append(GetValue(ds, i, "addressLine3") & "', '" & GetValue(ds, i, "addressCity") & "', '" & GetValue(ds, i, "addressState") & "', '")
                strSql.Append(GetValue(ds, i, "addressCountry") & "', '" & GetValue(ds, i, "addressPostalCode") & "') ")
                objDb.ExecuteNnQuery(strSql.ToString)

                strSql = New StringBuilder
                strSql.Append("insert into addresses (addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode) ")
                strSql.Append("values ('C', 'B', '" & ContactId.ToString & "', '" & GetValue(ds, i, "addressLine1") & "', '" & GetValue(ds, i, "addressLine2") & "', '")
                strSql.Append(GetValue(ds, i, "addressLine3") & "', '" & GetValue(ds, i, "addressCity") & "', '" & GetValue(ds, i, "addressState") & "', '")
                strSql.Append(GetValue(ds, i, "addressCountry") & "', '" & GetValue(ds, i, "addressPostalCode") & "') ")
                objDb.ExecuteNnQuery(strSql.ToString)

                intRecords += 1
            Catch ex As Exception
                'Record Failed
                'If Not strSql Is Nothing Then
                '    strLog += Environment.NewLine + strSql.ToString()
                'End If
                strLog += Environment.NewLine + ex.Message
            End Try
        Next
        'Close Database Connection
        objDb.CloseDatabaseConnection()
        objDb = Nothing
        Dim tt As String = strLog
        Return intRecords
    End Function

    Private Function GetValue(ByVal ds As DataSet, ByVal i As Integer, ByVal Name As String) As String
        Dim strValue As String = ds.Tables(0).Rows(i).Item(Name).ToString
        strValue = strValue.Replace("'", "''")
        Return strValue
    End Function
    Public Function funGetSRForImport(ByVal objDb As clsDataClass) As String
        Dim strSQL As String
        strSQL = "SELECT UserId FROM users WHERE UserActive='1' and userModules like '%SR%'  "
        Dim strSR As String = ""
        Dim dr As OdbcDataReader = objDb.GetDataReader(strSQL)
        While dr.Read
            strSR += dr.Item(0) & "~"
        End While
        If strSR <> "" Then
            strSR = strSR.TrimEnd("~")
        End If
        Return strSR
    End Function

    Public Function ImportSO(ByVal CSVFolderPath As String, ByVal CSVFile As String) As Integer
        Dim conn As OdbcConnection
        Dim ds As DataSet = New DataSet
        Dim da As OdbcDataAdapter
        Dim connectionString As String

        connectionString = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + CSVFolderPath + ";"
        conn = New Odbc.OdbcConnection(connectionString)

        da = New OdbcDataAdapter("select * from [" + CSVFile + "]", conn)
        da.Fill(ds)

        Dim intRecords As Integer = 0
        Dim i As Integer

        Dim strSql As String
        Dim objDb As clsDataClass = New clsDataClass

        'Open Database Connection
        objDb.OpenDatabaseConnection()

        For i = 0 To ds.Tables(0).Rows.Count - 1
            Try

                ' Insert Customer
                strSql = "INSERT INTO partners( PartnerLongName, PartnerActive, PartnerCreatedOn, PartnerType, PartnerLastUpdatedOn, PartnerValidated, PartnerShipBlankPref, PartnerCurrencyCode, PartnerLang) "
                strSql += " Values ('" + GetValue(ds, i, "Buyer full name") + "', 1, NOW(), 2, NOW(), 0, 0, 'CAD', 'en')"
                objDb.ExecuteNnQuery(strSql)

                Dim custId As Integer
                strSql = "Select max(PartnerId) from partners;"
                custId = objDb.GetScalarDataByQuery(strSql)

                ' Insert CustomerType
                strSql = "INSERT INTO partnerseltype(PartnerSelTypeID, PartnerSelPartID) VALUES( "
                strSql += " 2, " + custId.ToString + ")"
                objDb.ExecuteNnQuery(strSql)

                ' Insert Customer Shipping Address
                strSql = "INSERT INTO addresses( addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressCity, addressState, addressCountry, addressPostalCode) VALUES('"
                strSql += "E', 'S', " + custId.ToString + ", '" + GetValue(ds, i, "Buyer address 1") + "', '" + GetValue(ds, i, "Buyer address 2") + "', '" + GetValue(ds, i, "Buyer city") + "', '" + GetValue(ds, i, "Buyer state") + "', '" + GetValue(ds, i, "Buyer country") + "', '" + GetValue(ds, i, "Buyer ZIP/Postal code") + "')"
                objDb.ExecuteNnQuery(strSql)

                ' Insert Customer Billing Address
                strSql = "INSERT INTO addresses( addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressCity, addressState, addressCountry, addressPostalCode) VALUES('"
                strSql += "E', 'B', " + custId.ToString + ", '" + GetValue(ds, i, "Buyer address 1") + "', '" + GetValue(ds, i, "Buyer address 2") + "', '" + GetValue(ds, i, "Buyer city") + "', '" + GetValue(ds, i, "Buyer state") + "', '" + GetValue(ds, i, "Buyer country") + "', '" + GetValue(ds, i, "Buyer ZIP/Postal code") + "')"
                objDb.ExecuteNnQuery(strSql)

                '///////////
                ' Get WarehouseCode, Company ID
                Dim objUsr As New clsUser
                objUsr.UserID = Current.Session("UserID")
                objUsr.getUserInfo()
                Dim usrWhs As String = objUsr.UserWarehouse
                objUsr = Nothing

                Dim objComp As New clsCompanyInfo
                Dim companyID As String = objComp.funGetCompanyID(usrWhs)
                objComp = Nothing

                ' Insert Order
                strSql = "INSERT INTO orders( ordType, ordCustType, ordCustID, ordCreatedOn, ordStatus, ordVerified, ordSalesRepID, ordSaleWeb, ordShpDate, ordShpBlankPref, ordShpWhsCode, ordLastUpdatedOn, ordCurrencyCode, ordCurrencyExRate, qutExpDate,ordCompanyID,orderTypeCommission) "
                strSql += " Values( 'SO', 'E', " + custId.ToString + ", '" + getDateFormat(GetValue(ds, i, "Sale date")) + "', 'P', 0, 1, 1, '" + getDateFormat(GetValue(ds, i, "Shipped on date")) + "', 0, '-', NOW(), 'CAD', 1, NOW(), " + companyID + ", 0) "
                objDb.ExecuteNnQuery(strSql)

                Dim ordId As Integer
                strSql = "Select max(ordId) from orders;"
                ordId = objDb.GetScalarDataByQuery(strSql)

                '///////// TO DO 
                ' Method to get product id from given Item Number
                ' Item Number from ebay --> new table map barcode --> get product Id
                ' Dim objeBayItemBarcodeMap As New clseBayItemBarcodeMap
                ' objeBayItemBarcodeMap.eBayItemNumber = GetValue(ds, i, "Item Number")
                Dim prdID As String = "" ' objeBayItemBarcodeMap.getProductIDByeBayItemNumber()
                ' objeBayItemBarcodeMap = Nothing

                ' Insert Order Items
                strSql = "INSERT INTO orderitems(ordID, ordProductID, ordProductQty, ordProductUnitPrice, orderItemDesc ) VALUES('"
                strSql += ordId.ToString + "', " + prdID.ToString + ", " + GetValue(ds, i, "Quantity") + ", " + GetValue(ds, i, "Sale price") + ", '" + GetValue(ds, i, "Item title") + "')"
                objDb.ExecuteNnQuery(strSql)

                ' Insert Invoice
                strSql = "INSERT INTO invoices( invCustType, invCustID, invCreatedOn, invStatus, invLastUpdatedOn, invCurrencyCode, invCurrencyExRate, invShpWhsCode, invForOrderNo,invCompanyID,InvRefNo,invRefType, invTypeCommision) VALUES('"
                strSql += "E', " + custId.ToString + ", NOW(), 'P', NOW(), 'CAD', 1, '-', " + ordId.ToString + ", " + companyID + ", " + ordId.ToString + ", 'IV', 0)"
                objDb.ExecuteNnQuery(strSql)

                Dim invId As Integer
                strSql = "Select max(invId) from invoices;"
                invId = objDb.GetScalarDataByQuery(strSql)

                ' Insert Invoice Items
                strSql = "INSERT INTO invoiceitems(invoices_invID, invProductID, invProductQty, invProductUnitPrice, invProdIDDesc) VALUES('"
                strSql += invId.ToString + "', " + prdID.ToString + ", " + GetValue(ds, i, "Quantity") + ", " + GetValue(ds, i, "Sale price") + ", '" + GetValue(ds, i, "Item title") + "')"
                objDb.ExecuteNnQuery(strSql)

                ' Insert Payment
                strSql = "INSERT INTO postransaction(posTransType, posTransDateTime, posTransUserID, posTransStatus, posSubTotal, posTotalValue) VALUES('"
                strSql += "E', '" + getDateFormat(GetValue(ds, i, "Paid on date")) + "', " + GetValue(ds, i, "User ID") + ", 1, " + GetValue(ds, i, "Total price") + ", " + GetValue(ds, i, "Total price") + ")"
                objDb.ExecuteNnQuery(strSql)

                Dim transactionId As Integer
                strSql = "Select max(posTransId) from postransaction;"
                transactionId = objDb.GetScalarDataByQuery(strSql)

                '//// TO DO
                'PayType
                strSql = "INSERT INTO postransaccthdr(posAcctTransID, posAcctPayType, posCasReceived, posCashReturned) VALUES('"
                strSql += transactionId.ToString + "', 6," + GetValue(ds, i, "Total price") + ", 0)"
                objDb.ExecuteNnQuery(strSql)

                Dim accHeaderID As Integer
                strSql = "Select max(posAcctHdrId) from postransaccthdr;"
                accHeaderID = objDb.GetScalarDataByQuery(strSql)

                strSql = "INSERT INTO postransacctdtl(posAcctHdrDtlId, posTranAcctType, posTransStartDatetime,  posTotalValue, posTransAcctDtlStatus, posTransID, posTransEndTime, posCardNo ) VALUES('"
                strSql += accHeaderID.ToString + "', 'sale', '" + getDateFormat(GetValue(ds, i, "Paid on date")) + "', " + GetValue(ds, i, "Total price") + ", 0, 0, '" + getDateFormat(GetValue(ds, i, "Paid on date")) + "', '') "
                objDb.ExecuteNnQuery(strSql)

                intRecords += 1
            Catch ex As Exception
                'Record Failed
            End Try
        Next
        'Close Database Connection
        objDb.CloseDatabaseConnection()
        objDb = Nothing

        Return intRecords

    End Function
    Public Function getDateFormat(ByVal strDate As String) As String
        If strDate <> "" Then
            Dim dtDate As New DateTime
            dtDate = CType(strDate, DateTime)
            strDate = dtDate.ToString("yyyy-MM-dd")
        End If

        Return strDate
    End Function
    Private Function GetInt(ByVal ds As DataSet, ByVal i As Integer, ByVal Name As String) As String
        Dim strValue As String = ds.Tables(0).Rows(i).Item(Name).ToString
        If strValue = "" Or IsNumeric(strValue) = False Then
            strValue = 0
        End If
        Return strValue
    End Function
    Public Function UpdateProduct(ByVal CSVFolderPath As String, ByVal CSVFile As String) As Integer
        Dim conn As OdbcConnection
        Dim ds As DataSet = New DataSet
        Dim da As OdbcDataAdapter
        Dim connectionString As String

        connectionString = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + CSVFolderPath + ";"
        conn = New Odbc.OdbcConnection(connectionString)

        da = New OdbcDataAdapter("select * from [" + CSVFile + "]", conn)
        da.Fill(ds)

        Dim intRecords As Integer = 0
        Dim i As Integer
        Dim count As Integer

        Dim strSql As String
        Dim objDb As clsDataClass = New clsDataClass

        'Open Database Connection
        objDb.OpenDatabaseConnection()

        For i = 0 To ds.Tables(0).Rows.Count - 1
            Try
                'Added by mukesh 20130730
                For k = 0 To ds.Tables(0).Columns.Count - 1
                    ds.Tables(0).Rows(i)(k) = ds.Tables(0).Rows(i).ItemArray(k).ToString().Replace("'", "")
                    ds.Tables(0).AcceptChanges()
                Next
                'Added end
                strSql = "update products set prdUPCCode='" & GetValue(ds, i, "UPC_Code") & "', prdName='" & GetValue(ds, i, "Product_Name") & "', "
                strSql += "prdIntID='" & GetValue(ds, i, "Internal_ID") & "', prdExtID='" & GetValue(ds, i, "External_ID") & "', "  'Edited By Mukesh 20130718'
                strSql += "prdIsGlutenFree='" & GetValue(ds, i, "Gluten_Free") & "', prdSpicyLevel='" & GetValue(ds, i, "Spicy") & "', "    'Added By Mukesh 20130718'
                strSql += "prdIsContainsNuts='" & GetValue(ds, i, "Contains_Nuts") & "', prdIsVegetarian='" & GetValue(ds, i, "Vegetarian") & "'"    'Added By Mukesh 20130718'

                If ds.Tables(0).Columns.Contains("Sales_Price") Then
                    strSql += ", prdSalePricePerMinQty='" & GetInt(ds, i, "Sales_Price") & "'"
                End If
                If ds.Tables(0).Columns.Contains("End_User_Price") Then
                    strSql += ", prdEndUserSalesPrice='" & GetInt(ds, i, "End_User_Price") & "'"
                End If
                If ds.Tables(0).Columns.Contains("Web_Price") Then
                    strSql += ", prdWebSalesPrice='" & GetInt(ds, i, "Web_Price") & "'"
                End If

                strSql += " where productID='" & GetValue(ds, i, "ID") & "';"
                objDb.ExecuteNnQuery(strSql)

                strSql = "Select count(*) from prdquantity where prdID='" & GetValue(ds, i, "ID") & "' and prdWhsCode='" & GetValue(ds, i, "Warehouse") & "';"
                count = objDb.GetScalarDataByQuery(strSql)

                If count = 0 Then
                    strSql = "insert into prdquantity (prdID, prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty, prdBlock, prdFloor, prdAisle, "
                    strSql += "prdRack, prdBin) values ('" & GetValue(ds, i, "ID") & "', '" & GetValue(ds, i, "Warehouse") & "', '" & GetInt(ds, i, "In_Hand_Quantity") & "', "
                    strSql += "0, 0, '" & GetInt(ds, i, "Defective_Quantity") & "', '" & GetValue(ds, i, "Block") & "', '" & GetValue(ds, i, "Floor") & "', "
                    strSql += "'" & GetValue(ds, i, "Aisle") & "', '" & GetValue(ds, i, "Rack") & "', '" & GetValue(ds, i, "Bin") & "');"
                Else
                    strSql = "update prdquantity set prdOhdQty='" & GetInt(ds, i, "In_Hand_Quantity") & "', prdDefectiveQty='" & GetInt(ds, i, "Defective_Quantity") & "', "
                    strSql += "prdBlock='" & GetValue(ds, i, "Block") & "', prdFloor='" & GetValue(ds, i, "Floor") & "', prdAisle='" & GetValue(ds, i, "Aisle") & "', "
                    strSql += "prdRack='" & GetValue(ds, i, "Rack") & "', prdBin='" & GetValue(ds, i, "Bin") & "'"
                    strSql += "where prdID='" & GetValue(ds, i, "ID") & "' and prdWhsCode='" & GetValue(ds, i, "Warehouse") & "';"
                End If
                objDb.ExecuteNnQuery(strSql)

                intRecords += 1
            Catch ex As Exception
                'Record Failed
            End Try
        Next
        'Close Database Connection
        objDb.CloseDatabaseConnection()
        objDb = Nothing
        Return intRecords
    End Function
End Class



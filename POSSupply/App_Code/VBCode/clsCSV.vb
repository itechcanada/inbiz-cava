Imports System.IO
Imports System.Web.HttpContext
Imports Microsoft.VisualBasic

Public Class clsCSV

    Public Shared Pg As Web.UI.Page
    Public Shared CSVName As String
    Public Shared Grid As GridView
    Public Shared Skip() As Integer
    Public Shared blnFullUrl As Boolean

    Public Shared Sub ExportCSV(ByVal Page As Web.UI.Page, ByVal Name As String, ByVal GridView As GridView, ByVal SkipColumns() As Integer, ByVal CSVBtn As ImageButton, Optional ByVal FullUrl As Boolean = False)
        Pg = Page
        CSVName = Name
        Grid = GridView
        Skip = SkipColumns
        blnFullUrl = FullUrl
        AddEvents(CSVBtn)
        If Current.Session("CSVRedirect") = "True" Then
            Current.Session("CSVRedirect") = Nothing
            GridView.AllowPaging = False
            DownloadCSV()
            GridView.AllowPaging = True
        End If
    End Sub

    Public Shared Sub AddEvents(ByVal CSVBtn As ImageButton)
        AddHandler CSVBtn.Click, AddressOf Redirect
    End Sub

    Public Shared Sub Redirect(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Current.Session("CSVRedirect") = "True"
        Dim strPath As String = Current.Request.Url.AbsoluteUri.ToString
        If blnFullUrl = False Then
            strPath = strPath.Split("?")(0)
        End If
        Current.Response.Redirect(strPath)
    End Sub

    Public Shared Sub DownloadCSV()
        Dim CSVPath As String = CSVName & "_" & Date.Now.Year & Date.Now.Month & Date.Now.Day & Date.Now.Hour & Date.Now.Minute & Date.Now.Second & ".csv"
        Dim sw As StreamWriter = New StreamWriter(Current.Request.PhysicalApplicationPath & "\Upload\Export\" & CSVPath)
        Dim data As String
        Dim i As Integer
        Dim j As Integer
        Dim k As Integer
        Dim columns As Integer = Grid.Columns.Count
        Dim rows As Integer = Grid.Rows.Count
        Dim last As Integer = columns - 1
        While Array.BinarySearch(Skip, last) > -1
            last = last - 1
        End While

        For i = 0 To columns - 1
            If Array.BinarySearch(Skip, i) > -1 Then
                Continue For
            End If
            data = Grid.Columns(i).HeaderText.Trim
            data = FormaColumn(data)
            sw.Write(data)
            If i <> last Then
                sw.Write(",")
            End If
        Next
        sw.Write(vbCrLf)

        For j = 0 To rows - 1
            For k = 0 To columns - 1
                If Array.BinarySearch(Skip, k) > -1 Then
                    Continue For
                End If
                data = FetchLinkText(Grid.Rows(j).Cells(k))
                If data = "" Then
                    data = Grid.Rows(j).Cells(k).Text.Trim
                    data = FormatData(data)
                End If
                sw.Write(data)
                If k <> last Then
                    sw.Write(",")
                End If
            Next
            If j <> rows - 1 Then
                sw.Write(vbCrLf)
            End If
        Next

        sw.Close()

        Current.Response.Redirect("~/Upload/Export/" & CSVPath)
    End Sub

    Private Shared Function FetchLinkText(ByVal cell As TableCell) As String
        Dim data As String = ""
        If cell.HasControls Then
            If (cell.Controls(1).GetType.ToString = "System.Web.UI.WebControls.LinkButton") Then
                data = CType(cell.Controls(1), LinkButton).Text
                data = FormatData(data)
            End If
        End If
        Return data
    End Function

    Private Shared Function FormaColumn(ByVal data As String) As String
        data = data.Trim
        data = Current.Server.HtmlDecode(data)
        data = data.Replace(" ", "_")
        data = data.Replace("<b>", "")
        data = data.Replace("</b>", "")
        data = data.Replace("""", """""")
        data = """" & data & """"
        Return data
    End Function

    Private Shared Function FormatData(ByVal data As String) As String
        data = data.Trim
        data = Current.Server.HtmlDecode(data)
        data = data.Replace("<b>", "")
        data = data.Replace("</b>", "")
        data = data.Replace("""", """""")
        data = """" & data & """"
        Return data
    End Function
End Class

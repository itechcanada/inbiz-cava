Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Web.HttpContext
Imports System.Data.Odbc
Imports System.Net.Mail
Imports System.Threading
Imports System.Net.Mail.Attachment
Imports System.Data
Imports System.IO.File
Imports System.Net
Imports System.Text
Imports System.Resources
Imports Resources.Resource
Imports System.Globalization
Imports iTECH.InbizERP.BusinessLogic
Imports clsCommon
Public Class clsMassEmail
    Inherits clsDataClass
    ' Send Mail
    Public nmVal As System.Collections.Specialized.NameValueCollection = System.Configuration.ConfigurationManager.AppSettings
    Private Function SendMail(ByVal strAttachFile As String, ByVal strMail As String, ByVal strSub As String, ByVal strMsg As String) As Boolean
        Try
            Dim EmailFrom As String = nmVal.Get("Sender")
            Dim EmailTo As String = strMail
            Dim EmailSub As String = strSub
            Dim EmailMsg As String = strMsg

            Dim msg As MailMessage = New MailMessage(EmailFrom, EmailTo, EmailSub, EmailMsg)
            'msg.Attachments.Add(New Mail.Attachment(strAttachFile))
            msg.IsBodyHtml = True

            Dim server As SmtpClient = New SmtpClient
            Dim serverName As String = nmVal.Get("SMTPServer")
            server.Host = serverName
            server.UseDefaultCredentials = False
            server.Port = nmVal.Get("SMTPPort")
            server.Credentials = New System.Net.NetworkCredential(nmVal.Get("SMTPUserEmail"), nmVal.Get("SMTPPassword"))
            server.Send(msg)
            msg.Dispose()
            server = Nothing
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    ' Send Mail
    Private Function SendMail(ByVal strAttachFile1 As String, ByVal strAttachFile2 As String, ByVal strMail As String, ByVal strSub As String, ByVal strMsg As String) As Boolean
        Try
            Dim EmailFrom As String = nmVal.Get("Sender")
            Dim EmailTo As String = strMail
            Dim EmailSub As String = strSub
            Dim EmailMsg As String = strMsg

            Dim msg As MailMessage = New MailMessage(EmailFrom, EmailTo, EmailSub, EmailMsg)
            msg.Attachments.Add(New Mail.Attachment(strAttachFile1))
            msg.Attachments.Add(New Mail.Attachment(strAttachFile2))
            msg.IsBodyHtml = True

            Dim server As SmtpClient = New SmtpClient
            Dim serverName As String = nmVal.Get("SMTPServer")
            server.Host = serverName
            server.UseDefaultCredentials = False
            server.Port = nmVal.Get("SMTPPort")
            server.Credentials = New System.Net.NetworkCredential(nmVal.Get("SMTPUserEmail"), nmVal.Get("SMTPPassword"))

            server.Send(msg)
            msg.Dispose()
            server = Nothing
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    ' Send Customized Mail
    Public Function funCustomizedMail(ByVal strSub As String, ByVal strMsg As String, ByVal StrMailTo As String, ByVal strFrom As String, ByVal strName As String) As Boolean

        Dim bStatus As Boolean
        Dim strMailSubject As String = strSub
        Dim strMailMessage As String = ""
        Dim strUrl As String
        Dim strText As String = ""


        Dim strMailfrom As String = strFrom
        Dim strDataFromFile As StringBuilder
        strDataFromFile = New StringBuilder(File.ReadAllText(Current.Request.PhysicalApplicationPath & "MailFormat\mail.htm"))
        strUrl = ConfigurationManager.AppSettings("unsubscribeEmailURL")

        strDataFromFile.Replace("strEmail", StrMailTo)
        strDataFromFile.Replace("strLink", strUrl)
        strText = msgStopEmailing
        strDataFromFile.Replace("strText", strText)

        strDataFromFile.Replace("strName", strName)
        strDataFromFile.Replace("strMessage", strMsg)

        strMailMessage = strDataFromFile.ToString

        Dim objCommon As New clsCommon

        bStatus = objCommon.SendMail(strMailSubject, strMailMessage, StrMailTo, strMailfrom)
        objCommon = Nothing
        Return bStatus
    End Function

    ' Send Customized Mail
    Public Function funCustomizedMail(ByVal strSub As String, ByVal strMsg As String, ByVal StrMailTo As String, ByVal strFrom As String, ByVal strFirstName As String, ByVal strLastName As String, ByVal AttachFile As String) As Boolean

        Dim bStatus As Boolean
        Dim strMailSubject As String = strSub
        Dim strMailMessage As String = ""
        Dim strUrl As String
        Dim strText As String = ""
        Dim strMailfrom As String = strFrom
        Dim strDataFromFile As StringBuilder
        strDataFromFile = New StringBuilder(File.ReadAllText(Current.Request.PhysicalApplicationPath & "MailFormat\mailFL.htm"))
        strUrl = ConfigurationManager.AppSettings("unsubscribeEmailURL")

        strMsg = strMsg.Replace("&lt;FIRST NAME&gt;", strFirstName)
        strMsg = strMsg.Replace("&lt;LAST NAME&gt;", strLastName)

        strDataFromFile.Replace("strMessage", strMsg)
        strDataFromFile.Replace("strEmail", StrMailTo)
        strDataFromFile.Replace("strLink", strUrl)
        strText = msgStopEmailing
        strDataFromFile.Replace("strText", strText)

        strMailMessage = strDataFromFile.ToString

        Dim objCommon As New clsCommon

        If AttachFile <> "" Then
            bStatus = objCommon.SendMail(strMailSubject, strMailMessage, StrMailTo, strMailfrom, AttachFile)
        Else
            bStatus = objCommon.SendMail(strMailSubject, strMailMessage, StrMailTo, strMailfrom)
        End If

        objCommon = Nothing
        Return bStatus
    End Function

    'Insert Mass Email Header
    Public Function funInsertMEHeader(ByVal strSub As String, ByVal strMsg As String, ByVal strFrom As String, ByVal strFirstName As String, ByVal strLastName As String, ByVal strEmail As String, ByVal AttachFile As String) As String
        Dim objCommon As New clsCommon
        Dim strHeaderID As String
        Dim strMailSubject As String = strSub
        Dim strMailMessage As String = ""

        Dim strMailfrom As String = strFrom
        Dim strDataFromFile As StringBuilder
        strDataFromFile = New StringBuilder(File.ReadAllText(Current.Request.PhysicalApplicationPath & "MailFormat\mailFL.htm"))

        strDataFromFile.Replace("strMessage", strMsg)
        strMailMessage = strDataFromFile.ToString
        strMailMessage = clsCommon.funRemove(strMailMessage)
        AttachFile = clsCommon.funRemove(AttachFile)

        Dim strSMTPServer As String = ConfigurationManager.AppSettings("SMTPServer")
        Dim strSMTPPort As String = ConfigurationManager.AppSettings("SMTPPort")
        Dim strSMTPUser As String = ConfigurationManager.AppSettings("IsAsynchronousMail")
        Dim strSMTPPassword As String = ConfigurationManager.AppSettings("SMTPPassword")
        Dim strReplStr1 As String = strFirstName
        Dim strReplStr2 As String = strLastName
        Dim strReplStr3 As String = strEmail

        Dim strSql As String = ""
        strSql += "Insert into massemailhdr (MeSMTPSvr, MeSMTPPort, MeSMTPUsr, MeSMTPPwd, MeEmlFrEmlIID, MeEmlReplyToUsrID, MeEmlSubj, MeEmlBody, MeExeStartDtTm, MeReplStr1, MeReplStr2, MeReplStr3, MeEmlAttachment) "
        strSql += "values ('" & strSMTPServer & "','" & strSMTPPort & "','" & strSMTPUser & "','" & strSMTPPassword & "','" & strFrom & "','" & strFrom & "','" & clsCommon.funRemove(strMailSubject) & "', "
        strSql += "'" & strMailMessage & "',now(),'" & strReplStr1 & "','" & strReplStr2 & "','" & strReplStr3 & "', " & IIf(AttachFile <> "", "'" & AttachFile & "'", "null") & ") "

        Dim objDB As New clsDataClass
        objDB.PopulateData(strSql)
        strHeaderID = objDB.GetScalarData("Select LAST_INSERT_ID()")
        objDB.CloseDatabaseConnection()
        Return strHeaderID
    End Function

    'Insert Mass Email Detail
    Public Function funInsertMEDetail(ByVal strHeaderId As String, ByVal StrMailToName As String, ByVal StrMailTo As String, ByVal strFirstName As String, ByVal strLastName As String, ByVal strEmail As String) As String
        Dim intCount As String
        Dim strSql As String = "Insert into massemaildtl (MeEmlDtlHdrID, MeEmlToNm, MeEmlToID, MeReplVal1, MeReplVal2, MeReplVal3) "
        strSql += "values ('" & strHeaderId & "','" & clsCommon.funRemove(StrMailToName) & "','" & StrMailTo & "','" & clsCommon.funRemove(strFirstName) & "','" & clsCommon.funRemove(strLastName) & "','" & clsCommon.funRemove(strEmail) & "') "

        Dim objDB As New clsDataClass
        intCount = objDB.SetData(strSql)

        Return intCount
    End Function
End Class

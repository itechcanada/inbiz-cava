Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports iTECH.InbizERP.BusinessLogic
'Imports System.Web.HttpContext
'Imports Resources.Resource
'Imports clsCommon
Public Class clsAddress
	Inherits clsDataClass
    Private _addressID, _addressRef, _addressType, _addressSourceID, _addressLine1, _addressLine2, _addressLine3, _addressCity, _addressState, _addressCountry, _addressPostalCode As String
	Public Property AddressID() As String
		Get
			Return _addressID
		End Get
		Set(ByVal value As String)
            _addressID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property AddressRef() As String
		Get
			Return _addressRef
		End Get
		Set(ByVal value As String)
            _addressRef = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property AddressType() As String
		Get
			Return _addressType
		End Get
		Set(ByVal value As String)
            _addressType = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property AddressSourceID() As String
		Get
			Return _addressSourceID
		End Get
		Set(ByVal value As String)
            _addressSourceID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property AddressLine1() As String
		Get
			Return _addressLine1
		End Get
		Set(ByVal value As String)
            _addressLine1 = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property AddressLine2() As String
		Get
			Return _addressLine2
		End Get
		Set(ByVal value As String)
            _addressLine2 = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property AddressLine3() As String
		Get
			Return _addressLine3
		End Get
		Set(ByVal value As String)
            _addressLine3 = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property AddressCity() As String
		Get
			Return _addressCity
		End Get
		Set(ByVal value As String)
            _addressCity = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property AddressState() As String
		Get
			Return _addressState
		End Get
		Set(ByVal value As String)
            _addressState = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property AddressCountry() As String
		Get
			Return _addressCountry
		End Get
		Set(ByVal value As String)
            _addressCountry = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property AddressPostalCode() As String
		Get
			Return _addressPostalCode
		End Get
		Set(ByVal value As String)
            _addressPostalCode = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert Address
    ''' into table addresss
    ''' </summary>
    ''' <returns>as boolean</returns>
    ''' <remarks></remarks>
	Public Function insertAddress() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO addresses( addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode) VALUES('"
		strSQL += _addressRef + "','"
		strSQL += _addressType + "','"
		strSQL += _addressSourceID + "','"
		strSQL += _addressLine1 + "','"
		strSQL += _addressLine2 + "','"
		strSQL += _addressLine3 + "','"
		strSQL += _addressCity + "','"
		strSQL += _addressState + "','"
		strSQL += _addressCountry + "','"
		strSQL += _addressPostalCode + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    '''  Update Address
    ''' into table addresss
    ''' </summary>
    ''' <returns>as boolean</returns>
    ''' <remarks></remarks>
	Public Function updateAddress() As Boolean
		Dim strSQL As String
        strSQL = "UPDATE addresses set "
		strSQL += "addressRef='" + _addressRef + "', "
		strSQL += "addressType='" + _addressType + "', "
		strSQL += "addressSourceID='" + _addressSourceID + "', "
		strSQL += "addressLine1='" + _addressLine1 + "', "
		strSQL += "addressLine2='" + _addressLine2 + "', "
		strSQL += "addressLine3='" + _addressLine3 + "', "
		strSQL += "addressCity='" + _addressCity + "', "
		strSQL += "addressState='" + _addressState + "', "
		strSQL += "addressCountry='" + _addressCountry + "', "
		strSQL += "addressPostalCode='" + _addressPostalCode + "'"
		strSQL += " where addressID='" + _addressID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    '''  Populate objects of Address
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getAddressInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode FROM addresses where addressID='" + _addressID + "' "
        drObj = GetDataReader(strSQL)
		While drObj.Read
			_addressRef = drObj.Item("addressRef").ToString
			_addressType = drObj.Item("addressType").ToString
			_addressSourceID = drObj.Item("addressSourceID").ToString
			_addressLine1 = drObj.Item("addressLine1").ToString
			_addressLine2 = drObj.Item("addressLine2").ToString
			_addressLine3 = drObj.Item("addressLine3").ToString
			_addressCity = drObj.Item("addressCity").ToString
			_addressState = drObj.Item("addressState").ToString
			_addressCountry = drObj.Item("addressCountry").ToString
			_addressPostalCode = drObj.Item("addressPostalCode").ToString
		End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate objects of Address
    ''' </summary>
    ''' <param name="sRef">As String</param>
    ''' <param name="sType">As String</param>
    ''' <param name="sSource">As String</param>
    ''' <remarks></remarks>
    Public Sub getAddressInfo(ByVal sRef As String, ByVal sType As String, ByVal sSource As String)
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT addressID, addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode FROM addresses "
        If sRef <> "" And sType <> "" And sSource <> "" Then
            strSQL += " where "
            strSQL += " ("
            strSQL += " addressRef = '" + sRef + "' and "
            strSQL += " addressType = '" + sType + "' and "
            strSQL += " addressSourceID = '" + sSource + "' "
            strSQL += " )"
        End If
        strSQL += " order by addressID "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _addressRef = drObj.Item("addressRef").ToString
            _addressType = drObj.Item("addressType").ToString
            _addressSourceID = drObj.Item("addressSourceID").ToString
            _addressLine1 = drObj.Item("addressLine1").ToString
            _addressLine2 = drObj.Item("addressLine2").ToString
            _addressLine3 = drObj.Item("addressLine3").ToString
            _addressCity = drObj.Item("addressCity").ToString
            _addressState = drObj.Item("addressState").ToString
            _addressCountry = drObj.Item("addressCountry").ToString
            _addressPostalCode = drObj.Item("addressPostalCode").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Return Address ID
    ''' </summary>
    ''' <returns>As String</returns>
    ''' <remarks></remarks>
    Public Function funGetAddressID() As String
        Dim strSQL As String
        strSQL = "SELECT addressID FROM addresses "
        strSQL += " where "
        strSQL += " ("
        strSQL += " addressRef = '" + _addressRef + "' and "
        strSQL += " addressType = '" + _addressType + "' and "
        strSQL += " addressSourceID = '" + _addressSourceID + "' "
        strSQL += " )"
        Dim strData As String = ""
        strData = GetScalarData(strSQL)
        Return strData
    End Function
    ''' <summary>
    ''' Check Duplicate Address
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>As Boolean</remarks>
    Public Function funCheckDuplicateAddress() As Boolean
        Dim strSQL As String
        Dim strPostalCode As String
        If clsCommon.funRemove(_addressPostalCode) = "" Then
            strPostalCode = "addressPostalCode is Null Or addressPostalCode=''"
        Else
            strPostalCode = "addressPostalCode= " & clsCommon.funRemove(_addressPostalCode)
        End If
        strSQL = "SELECT count(*) FROM addresses where addressRef='" & clsCommon.funRemove(_addressRef) & "' and addressType='" & clsCommon.funRemove(_addressType) & "' and addressSourceID='" & clsCommon.funRemove(_addressSourceID) & "'" 'and (" & strPostalCode & ")"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Delete Address
    ''' </summary>
    ''' <param name="DeleteID">As String addressId</param>
    ''' <returns>As String</returns>
    ''' <remarks></remarks>
    Public Function funDeleteAddress(ByVal DeleteID As String) As String
        Dim strSQL As String
        strSQL = "DELETE FROM addresses WHERE addressID='" + DeleteID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid virw
    ''' </summary>
    ''' <returns>As String </returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        strSQL = "SELECT addressID, addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode FROM addresses order by addressID "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill grid with search criteria
    ''' </summary>
    ''' <param name="sRef">As String ref</param>
    ''' <param name="sType">As String type</param>
    ''' <param name="sSource">As String source</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strSQL As String
        strSQL = "SELECT addressID, addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode FROM addresses "
        If sRef <> "" And sType <> "" And sSource <> "" Then
            strSQL += " where "
            strSQL += " ("
            strSQL += " addressRef = '" + sRef + "' and "
            strSQL += " addressType = '" + sType + "' and "
            strSQL += " addressSourceID = '" + sSource + "' "
            strSQL += " )"
        End If
        strSQL += " order by addressID "
        Return strSQL
    End Function
    ''' <summary>
    ''' Get Distributor Name
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getDistributorName() As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT distName FROM distributor where distID='" & _addressSourceID & "' "
        strData = GetScalarData(strSQL).ToString
        Return strData
    End Function
    ''' <summary>
    ''' Get Distributor CustomerID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getDistributorCustomerID() As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT distCustomerID FROM distributor where distID='" & _addressSourceID & "' "
        strData = GetScalarData(strSQL).ToString
        Return strData
    End Function
    ''' <summary>
    ''' Get Reseller Name
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getResellerName() As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT resellerName FROM reseller where resellerID='" & _addressSourceID & "' "
        strData = GetScalarData(strSQL).ToString
        Return strData
    End Function
    ''' <summary>
    ''' Get EndClient Name
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getEndClientName() As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT endclientName FROM endclient where endclientID='" & _addressSourceID & "' "
        strData = GetScalarData(strSQL).ToString
        Return strData
    End Function
    ''' <summary>
    ''' Get Vendor Name
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getVendorName() As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT vendorName FROM vendor where vendorID='" & _addressSourceID & "' "
        strData = GetScalarData(strSQL).ToString
        Return strData
    End Function
    ' Insert Customer ID In Webshop
    'Public Function updateWebshopCustomer(ByVal strDistCustomerID As String) As Boolean
    '    Dim strConn As String = ""
    '    strConn = ConfigurationManager.ConnectionStrings("ConnectionStringW").ConnectionString
    '    Dim obj As New clsDataClass(strConn)
    '    Dim objDist As New clsPartners
    '    objDist.PartnerID = _addressSourceID
    '    objDist.getPartnersInfo() 'Get Distributor Info
    '    Dim strCust As String = ""
    '    strCust = "UPDATE Customer set "
    '    strCust += "  cust_billtocustomerno='" & "sysNull" & "' "
    '    strCust += ", cust_billtoconame='" & objDist.PartnerLongName & "' "
    '    strCust += ", cust_billtoaddress='" & _addressLine1 & " " & _addressLine2 & " " & _addressLine3 & "' "
    '    strCust += ", cust_billtocity='" & _addressCity & "' "
    '    strCust += ", cust_billtozipcode='" & _addressPostalCode & "' "
    '    'strCust += ", cust_billtopobox='" & strBillToPoBox & "' "
    '    'strCust += ", cust_categoryid=" & strCategoryID & " "
    '    'strCust += ", cust_subcategoryid=" & strSubCategoryID & " "
    '    strCust += ", cust_phoneno='" & objDist.DistPhone & "' "
    '    strCust += ", cust_faxno='" & objDist.DistFax & "' "
    '    strCust += ", cust_emailid='" & objDist.DistEmail & "' "
    '    'strCust += ", cust_tollfree='" & strTollFree & "' "
    '    strCust += ", cust_stateid=" & _addressState & " "
    '    strCust += ", cust_countryid=" & _addressCountry & " "
    '    strCust += " where cust_customerid='" & strDistCustomerID & "' "
    '    strCust = strCust.Replace("'sysNull'", "null")
    '    strCust = strCust.Replace("sysNull", "null")
    '    Dim intDataCustomer As Int16 = 0
    '    intDataCustomer = obj.PopulateData(strCust)
    '    objDist = Nothing
    '    obj.CloseDatabaseConnection()
    '    If intDataCustomer = 0 Then
    '        Return False
    '    ElseIf intDataCustomer > 0 Then
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function
    'Get State Name
    ''' <summary>
    ''' Get State Name
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getStateName() As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT StateName FROM state where StateID='" & _addressState & "' "
        Try
            strData = GetScalarData(strSQL).ToString
        Catch ex As Exception
            strData = _addressState
        End Try
        Return strData
    End Function
    ''' <summary>
    ''' Get Country Name
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getCountryName() As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT CountryName FROM country where CountryID='" & _addressCountry & "' "
        Try
            strData = GetScalarData(strSQL).ToString
        Catch ex As Exception
            strData = _addressCountry
        End Try
        Return strData
    End Function
    ''' <summary>
    ''' Delete Address
    ''' </summary>
    ''' <param name="RefType"></param>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteAddress(ByVal RefType As String, ByVal DeleteID As String) As Boolean
        Dim strSQL As String = ""
        If RefType = AddressReference.DISTRIBUTER And DeleteID <> "" Then
            strSQL = "DELETE FROM addresses WHERE addressRef='" & AddressReference.DISTRIBUTER & "' and addressSourceID='" + DeleteID + "' "
        ElseIf RefType = AddressReference.RESELLER And DeleteID <> "" Then
            strSQL = "DELETE FROM addresses WHERE addressRef='" & AddressReference.RESELLER & "' and addressSourceID='" + DeleteID + "' "
        ElseIf RefType = AddressReference.END_CLIENT And DeleteID <> "" Then
            strSQL = "DELETE FROM addresses WHERE addressRef='" & AddressReference.END_CLIENT & "' and addressSourceID='" + DeleteID + "' "
        ElseIf RefType = AddressReference.VENDOR And DeleteID <> "" Then
            strSQL = "DELETE FROM addresses WHERE addressRef='" & AddressReference.VENDOR & "' and addressSourceID='" + DeleteID + "' "
        End If
        Dim nData As Integer
        nData = PopulateData(strSQL)
        CloseDatabaseConnection()
        If nData > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    ''' <summary>
    ''' Populate Country List
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub subPopulateCountry(ByRef ddl As DropDownList)
        Dim strSQL As String = "SELECT CountryID,CountryName FROM Country Order By CountryName"
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "CountryName"
        ddl.DataValueField = "CountryID"
        ddl.DataBind()
        CloseDatabaseConnection()
        Dim itm As New WebControls.ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.SelectCountry
        ddl.Items.Insert(0, itm)
    End Sub
    ''' <summary>
    ''' Populate State List
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <param name="dlCuntry"></param>
    ''' <remarks></remarks>
    Public Sub subPopulateState(ByRef ddl As DropDownList, ByRef dlCuntry As DropDownList)
        Dim intCountry As Integer = 0
        If dlCuntry.Items.Count > 1 Then
            intCountry = dlCuntry.SelectedValue
            Dim strSQL As String = "SELECT StateID,StateName FROM state where StateCountryID=" & intCountry & " Order By StateName"
            ddl.Items.Clear()
            ddl.DataSource = GetDataReader(strSQL)
            ddl.DataTextField = "StateName"
            ddl.DataValueField = "StateID"
            ddl.DataBind()
            CloseDatabaseConnection()
            Dim itm As New WebControls.ListItem
            itm.Value = 0
            itm.Text = Resources.Resource.SelectState
            ddl.Items.Insert(0, itm)
        End If
    End Sub
    ''' <summary>
    ''' Populate Control
    ''' </summary>
    ''' <param name="chk"></param>
    ''' <param name="sCriteria"></param>
    ''' <param name="RefType"></param>
    ''' <remarks></remarks>
    Public Sub PopulateControl(ByVal chk As CheckBoxList, ByVal sCriteria As String, ByVal RefType As String)
        Dim strSQL As String = "" ', , 
        If sCriteria <> "" Then
            Select Case sCriteria
                Case "City"
                    strSQL = "SELECT Distinct addressCity as Criteria FROM addresses where addressCity <> '' "
                Case "District"
                    strSQL = "SELECT Distinct addressLine3 as Criteria FROM addresses where addressLine3 <> '' "
                Case "State"
                    strSQL = "SELECT Distinct addressState as Criteria FROM addresses where addressState <> '' "
                Case "Country"
                    strSQL = "SELECT Distinct addressCountry as Criteria FROM addresses where addressCountry <> '' "
            End Select
            Select Case RefType
                Case InvoicesStatus.PAYMENT_RECEIVED.ToString()
                    strSQL += " and (addressRef = '" & AddressReference.END_CLIENT & "' OR addressRef = '" & AddressReference.RESELLER & "' OR addressRef = '" & AddressReference.DISTRIBUTER & "')"
                Case AddressReference.CUSTOMER_CONTACT
                    strSQL += " and addressRef = '" + RefType + "'  "
            End Select

            chk.Items.Clear()
            chk.DataSource = GetDataReader(strSQL)
            chk.DataTextField = "Criteria"
            chk.DataValueField = "Criteria"
            chk.DataBind()
            CloseDatabaseConnection()
        End If
    End Sub
    ''' <summary>
    ''' Populate Control
    ''' </summary>
    ''' <param name="dlst"></param>
    ''' <param name="sCriteria"></param>
    ''' <param name="RefType"></param>
    ''' <remarks></remarks>
    Public Sub PopulateControl(ByVal dlst As DropDownList, ByVal sCriteria As String, ByVal RefType As String)
        Dim strSQL As String = "" ', , 
        If sCriteria <> "" Then
            Select Case sCriteria
                Case "City"
                    strSQL = "SELECT Distinct addressCity as Criteria FROM addresses where addressCity <> '' "
                Case "District"
                    strSQL = "SELECT Distinct addressLine3 as Criteria FROM addresses where addressLine3 <> '' "
                Case "State"
                    strSQL = "SELECT Distinct addressState as Criteria FROM addresses where addressState <> '' "
                Case "Country"
                    strSQL = "SELECT Distinct addressCountry as Criteria FROM addresses where addressCountry <> '' "
            End Select
            Select Case RefType
                Case InvoicesStatus.PAYMENT_RECEIVED.ToString()
                    strSQL += " and (addressRef = '" & AddressReference.END_CLIENT & "' OR addressRef = '" & AddressReference.RESELLER & "' OR addressRef = '" & AddressReference.DISTRIBUTER & "')"
                Case AddressReference.CUSTOMER_CONTACT
                    strSQL += " and addressRef = '" + RefType + "'  "
            End Select
            Select Case sCriteria
                Case "City"
                    strSQL += " order by addressCity "
                Case "District"
                    strSQL += " order by addressLine3 "
                Case "State"
                    strSQL += " order by addressState "
                Case "Country"
                    strSQL += " order by addressCountry "
            End Select

            dlst.Items.Clear()
            dlst.DataSource = GetDataReader(strSQL)
            dlst.DataTextField = "Criteria"
            dlst.DataValueField = "Criteria"
            dlst.DataBind()

            Dim itm As New ListItem
            itm.Value = 0

            Select Case sCriteria
                Case "State"
                    itm.Text = Resources.Resource.msgSelectaddressState
                Case "Country"
                    itm.Text = Resources.Resource.msgSelectaddressCountry
            End Select
            dlst.Items.Insert(0, itm)
            CloseDatabaseConnection()
        End If
    End Sub
    ''' <summary>
    ''' Populate Address
    ''' </summary>
    ''' <param name="sRef"></param>
    ''' <param name="sType"></param>
    ''' <param name="sSource"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)
        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + vbNewLine
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + vbNewLine
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + vbNewLine
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + vbNewLine
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + vbNewLine
            Else
                strAddress += objAdr.AddressState + vbNewLine
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + vbNewLine
            Else
                strAddress += objAdr.AddressCountry + vbNewLine
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += Resources.Resource.lblPostalCode & " " + objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
'Imports Resources.Resource
Imports iTECH.InbizERP.BusinessLogic
Public Class clsPartners
    Inherits clsDataClass
    Private _PartnerID, _PartnerLongName, _PartnerPhone, _PartnerAdditionPhone, _PartnerFax, _PartnerEmail, _PartnerWebsite, _PartnerActive, _PartnerCreatedOn, _PartnerCreatedBy, _PartnerLastUpdatedOn, _PartnerLastUpdatedBy, _PartnerType, _PartnerAcronyme, _PartnerNote, _PartnerInvoicePreference As String
    Private _PartnerValidated, _PartnerTaxCode, _PartnerCommissionCode, _PartnerDiscount, _PartnerCurrencyCode, _PartnerInvoiceNetTerms, _PartnerShipBlankPref, _PartnerCourierCode, _PartnerLang, _PartnerStatus, _PartnerValue As String
    Private _PartnerPhone2, _PartnerDesc1, _PartnerDesc2, _PartnerCreditAvailable, _InActiveReason As String
   
#Region "Properties"
    Public Property PartnerID() As String
        Get
            Return _PartnerID
        End Get
        Set(ByVal value As String)
            _PartnerID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerLongName() As String
        Get
            Return _PartnerLongName
        End Get
        Set(ByVal value As String)
            _PartnerLongName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerAdditionPhone() As String
        Get
            Return _PartnerAdditionPhone
        End Get
        Set(ByVal value As String)
            _PartnerAdditionPhone = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerPhone() As String
        Get
            Return _PartnerPhone
        End Get
        Set(ByVal value As String)
            _PartnerPhone = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerFax() As String
        Get
            Return _PartnerFax
        End Get
        Set(ByVal value As String)
            _PartnerFax = clsCommon.funRemove(value, True)
        End Set
    End Property


    Public Property PartnerEmail() As String
        Get
            Return _PartnerEmail
        End Get
        Set(ByVal value As String)
            _PartnerEmail = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerWebsite() As String
        Get
            Return _PartnerWebsite
        End Get
        Set(ByVal value As String)
            _PartnerWebsite = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerActive() As String
        Get
            Return _PartnerActive
        End Get
        Set(ByVal value As String)
            _PartnerActive = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerCreatedOn() As String
        Get
            Return _PartnerCreatedOn
        End Get
        Set(ByVal value As String)
            _PartnerCreatedOn = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerCreatedBy() As String
        Get
            Return _PartnerCreatedBy
        End Get
        Set(ByVal value As String)
            _PartnerCreatedBy = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerLastUpdatedOn() As String
        Get
            Return _PartnerLastUpdatedOn
        End Get
        Set(ByVal value As String)
            _PartnerLastUpdatedOn = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerLastUpdatedBy() As String
        Get
            Return _PartnerLastUpdatedBy
        End Get
        Set(ByVal value As String)
            _PartnerLastUpdatedBy = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerType() As String
        Get
            Return _PartnerType
        End Get
        Set(ByVal value As String)
            _PartnerType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerAcronyme() As String
        Get
            Return _PartnerAcronyme
        End Get
        Set(ByVal value As String)
            _PartnerAcronyme = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerNote() As String
        Get
            Return _PartnerNote
        End Get
        Set(ByVal value As String)
            _PartnerNote = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerInvoicePreference() As String
        Get
            Return _PartnerInvoicePreference
        End Get
        Set(ByVal value As String)
            _PartnerInvoicePreference = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerValidated() As String
        Get
            Return _PartnerValidated
        End Get
        Set(ByVal value As String)
            _PartnerValidated = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerTaxCode() As String
        Get
            Return _PartnerTaxCode
        End Get
        Set(ByVal value As String)
            _PartnerTaxCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerCommissionCode() As String
        Get
            Return _PartnerCommissionCode
        End Get
        Set(ByVal value As String)
            _PartnerCommissionCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerDiscount() As String
        Get
            Return _PartnerDiscount
        End Get
        Set(ByVal value As String)
            _PartnerDiscount = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerCurrencyCode() As String
        Get
            Return _PartnerCurrencyCode
        End Get
        Set(ByVal value As String)
            _PartnerCurrencyCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerInvoiceNetTerms() As String
        Get
            Return _PartnerInvoiceNetTerms
        End Get
        Set(ByVal value As String)
            _PartnerInvoiceNetTerms = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerShipBlankPref() As String
        Get
            Return _PartnerShipBlankPref
        End Get
        Set(ByVal value As String)
            _PartnerShipBlankPref = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerCourierCode() As String
        Get
            Return _PartnerCourierCode
        End Get
        Set(ByVal value As String)
            _PartnerCourierCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerLang() As String
        Get
            Return _PartnerLang
        End Get
        Set(ByVal value As String)
            _PartnerLang = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerStatus() As String
        Get
            Return _PartnerStatus
        End Get
        Set(ByVal value As String)
            _PartnerStatus = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerValue() As String
        Get
            Return _PartnerValue
        End Get
        Set(ByVal value As String)
            _PartnerValue = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Phone2() As String
        Get
            Return _PartnerPhone2
        End Get
        Set(ByVal value As String)
            _PartnerPhone2 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Desc1() As String
        Get
            Return _PartnerDesc1
        End Get
        Set(ByVal value As String)
            _PartnerDesc1 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Desc2() As String
        Get
            Return _PartnerDesc2
        End Get
        Set(ByVal value As String)
            _PartnerDesc2 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerCreditAvailable() As String
        Get
            Return _PartnerCreditAvailable
        End Get
        Set(ByVal value As String)
            _PartnerCreditAvailable = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property INActiveReason() As String
        Get
            Return _InActiveReason
        End Get
        Set(ByVal value As String)
            _InActiveReason = clsCommon.funRemove(value, True)
        End Set
    End Property

#End Region

    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Insert Partners
    ''' </summary>
    ''' <param name="sID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertPartners(ByRef sID As String) As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO partners( PartnerLongName, PartnerPhone, PartnerAdditionPhone, PartnerFax, PartnerEmail, PartnerWebsite, PartnerActive,PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, PartnerAcronyme, PartnerNote,PartnerInvoicePreference,PartnerValidated, PartnerTaxCode, PartnerCommissionCode, PartnerDiscount, PartnerCurrencyCode, PartnerInvoiceNetTerms, PartnerShipBlankPref, PartnerCourierCode, PartnerLang, PartnerStatus, PartnerValue,PartnerPhone2, PartnerDesc1, PartnerDesc2,PartnerCreditAvailable,InActiveReason) VALUES('"
        strSQL += _PartnerLongName + "','"
        strSQL += _PartnerPhone + "','"
        strSQL += _PartnerAdditionPhone + "','"
        strSQL += _PartnerFax + "','"
        strSQL += _PartnerEmail + "','"
        strSQL += _PartnerWebsite + "','"
        strSQL += _PartnerActive + "','"
        strSQL += _PartnerCreatedOn + "','"
        strSQL += _PartnerCreatedBy + "','"
        strSQL += _PartnerLastUpdatedOn + "','"
        strSQL += _PartnerLastUpdatedBy + "','"
        strSQL += _PartnerType + "','"
        strSQL += _PartnerAcronyme + "','"
        strSQL += _PartnerNote + "','"
        strSQL += _PartnerInvoicePreference + "','"
        strSQL += _PartnerValidated + "','"
        strSQL += _PartnerTaxCode + "','"
        strSQL += _PartnerCommissionCode + "','"
        strSQL += _PartnerDiscount + "','"
        strSQL += _PartnerCurrencyCode + "','"
        strSQL += _PartnerInvoiceNetTerms + "','"
        strSQL += _PartnerShipBlankPref + "','"
        strSQL += _PartnerCourierCode + "','"
        strSQL += _PartnerLang + "','"
        strSQL += _PartnerStatus + "','"
        strSQL += _PartnerValue + "','"
        strSQL += _PartnerPhone2 + "','"
        strSQL += _PartnerDesc1 + "','"
        strSQL += _PartnerDesc2 + "','"
        strSQL += _PartnerCreditAvailable + "','"
        strSQL += _InActiveReason + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        sID = obj.GetScalarData("Select LAST_INSERT_ID()")
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update Partners
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updatePartners() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE partners set "
        strSQL += "PartnerLongName='" + _PartnerLongName + "', "
        strSQL += "PartnerPhone='" + _PartnerPhone + "', "
        strSQL += "PartnerAdditionPhone='" + _PartnerAdditionPhone + "',"
        strSQL += "PartnerFax='" + _PartnerFax + "', "
        strSQL += "PartnerEmail='" + _PartnerEmail + "', "
        strSQL += "PartnerWebsite='" + _PartnerWebsite + "', "
        strSQL += "PartnerActive='" + _PartnerActive + "', "
        strSQL += "PartnerLastUpdatedOn='" + _PartnerLastUpdatedOn + "', "
        strSQL += "PartnerLastUpdatedBy='" + _PartnerLastUpdatedBy + "', "
        strSQL += "PartnerType='" + _PartnerType + "', "
        strSQL += "PartnerAcronyme='" + _PartnerAcronyme + "', "
        strSQL += "PartnerNote='" + _PartnerNote + "', "
        strSQL += "PartnerInvoicePreference='" + _PartnerInvoicePreference + "', "
        strSQL += "PartnerValidated='" + _PartnerValidated + "', "
        strSQL += "PartnerTaxCode='" + _PartnerTaxCode + "', "
        strSQL += "PartnerCommissionCode='" + _PartnerCommissionCode + "', "
        strSQL += "PartnerDiscount='" + _PartnerDiscount + "', "
        strSQL += "PartnerCurrencyCode='" + _PartnerCurrencyCode + "', "
        strSQL += "PartnerInvoiceNetTerms='" + _PartnerInvoiceNetTerms + "', "
        strSQL += "PartnerShipBlankPref='" + _PartnerShipBlankPref + "', "
        strSQL += "PartnerCourierCode='" + _PartnerCourierCode + "', "
        strSQL += "PartnerLang='" + _PartnerLang + "', "
        strSQL += "PartnerStatus='" + _PartnerStatus + "', "
        strSQL += "PartnerValue='" + _PartnerValue + "', "
        strSQL += "PartnerPhone2='" + _PartnerPhone2 + "', "
        strSQL += "PartnerDesc1='" + _PartnerDesc1 + "', "
        strSQL += "PartnerDesc2='" + _PartnerDesc2 + "', "
        strSQL += "PartnerCreditAvailable='" + _PartnerCreditAvailable + "'"
        strSQL += " where PartnerID='" + _PartnerID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of Partners
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getPartnersInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT * FROM partners where PartnerID='" + _PartnerID + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _PartnerLongName = drObj.Item("PartnerLongName").ToString
            _PartnerPhone = drObj.Item("PartnerPhone").ToString
            _PartnerAdditionPhone = drObj.Item("PartnerAdditionPhone").ToString
            _PartnerFax = drObj.Item("PartnerFax").ToString
            _PartnerEmail = drObj.Item("PartnerEmail").ToString
            _PartnerWebsite = drObj.Item("PartnerWebsite").ToString
            _PartnerActive = drObj.Item("PartnerActive").ToString
            _PartnerCreatedOn = drObj.Item("PartnerCreatedOn").ToString
            _PartnerCreatedBy = drObj.Item("PartnerCreatedBy").ToString
            _PartnerLastUpdatedOn = drObj.Item("PartnerLastUpdatedOn").ToString
            _PartnerLastUpdatedBy = drObj.Item("PartnerLastUpdatedBy").ToString
            _PartnerType = drObj.Item("PartnerType").ToString
            _PartnerAcronyme = drObj.Item("PartnerAcronyme").ToString
            _PartnerNote = drObj.Item("PartnerNote").ToString
            _PartnerInvoicePreference = drObj.Item("PartnerInvoicePreference").ToString
            _PartnerValidated = drObj.Item("PartnerValidated").ToString
            _PartnerTaxCode = drObj.Item("PartnerTaxCode").ToString
            _PartnerCommissionCode = drObj.Item("PartnerCommissionCode").ToString
            _PartnerDiscount = drObj.Item("PartnerDiscount").ToString
            _PartnerCurrencyCode = drObj.Item("PartnerCurrencyCode").ToString
            _PartnerInvoiceNetTerms = drObj.Item("PartnerInvoiceNetTerms").ToString
            _PartnerShipBlankPref = drObj.Item("PartnerShipBlankPref").ToString
            _PartnerCourierCode = drObj.Item("PartnerCourierCode").ToString
            _PartnerLang = drObj.Item("PartnerLang").ToString
            _PartnerStatus = drObj.Item("PartnerStatus").ToString
            _PartnerValue = drObj.Item("PartnerValue").ToString
            _PartnerPhone2 = drObj.Item("PartnerPhone2").ToString
            _PartnerDesc1 = drObj.Item("PartnerDesc1").ToString
            _PartnerDesc2 = drObj.Item("PartnerDesc2").ToString
            _PartnerCreditAvailable = drObj.Item("PartnerCreditAvailable").ToString
            _InActiveReason = drObj.Item("InActiveReason").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill Grid of Partners
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <param name="strCustType"></param>
    ''' <param name="SearchCatg"></param>
    ''' <param name="strPartnerID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal StatusData As String, ByVal SearchData As String, ByVal strCustType As String, ByVal SearchCatg As String, Optional ByVal strPartnerID As String = "") As String
        '        StatusData = clsCommon.funRemove(StatusData, False)
        SearchData = clsCommon.funRemove(SearchData)
        If SearchData = "sysNull" Then
            SearchData = ""
        End If
        Dim strSQL As String = ""
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distinct PartnerID,PartnerAcronyme, PartnerLongName, PartnerPhone, PartnerPhone2, PartnerFax, PartnerEmail, PartnerWebsite,PartnerActive as PartnerActiveForCsv, CASE PartnerActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as PartnerActive, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, case PartnerSelTypeID when 4 then 'blue.jpg' end as custType, PartnerNote FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID "
        Else
            strSQL = "SELECT distinct PartnerID,PartnerAcronyme, PartnerLongName, PartnerPhone, PartnerPhone2, PartnerFax, PartnerEmail, PartnerWebsite,PartnerActive as PartnerActiveForCsv, CASE PartnerActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as PartnerActive, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, case PartnerSelTypeID when 4 then 'blue.jpg' end as custType, PartnerNote FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID inner join salesrepcustomer s on s.CustomerID=partners.PartnerID  and s.UserID='" & HttpContext.Current.Session("UserID") & "'"
        End If

        If SearchCatg = OrderSearchFields.InvoiceNo Then
            strSQL += " INNER JOIN invoices i on  i.invCustID = partners.PartnerID "
        End If
        strSQL += " where 1=1 "
        If StatusData <> "" Then
            strSQL += " and PartnerActive='" + StatusData + "'  "
        End If
        If SearchCatg = CustomerSearchFields.ContactName Then
            strSQL += " and (PartnerLongName like '%" + SearchData + "%' or PartnerAcronyme like '%" + SearchData + "%') "
        ElseIf SearchCatg = OrderSearchFields.InvoiceNo Then
            strSQL += " and i.InvRefNo like '%" + SearchData + "%'"
        ElseIf SearchCatg = CustomerSearchFields.PartnerPhone Then
            strSQL += " and PartnerPhone like '%" + SearchData + "%'"
        ElseIf SearchCatg = CustomerSearchFields.Acronyme Then
            strSQL += " and PartnerAcronyme like '%" + SearchData + "%'"
        ElseIf SearchCatg = "PC" Then
            strSQL += " and PartnerID IN (SELECT DISTINCT addressSourceID FROM addresses WHERE addressRef = 'D' AND addressType = 'H' AND addressPostalCode = '" + SearchData + "')"
        ElseIf SearchCatg = "CT" Then
            strSQL += " and PartnerID IN (SELECT DISTINCT addressSourceID FROM addresses WHERE addressRef = 'D' AND addressType = 'H' AND addressCity LIKE '%" + SearchData + "%')"
        ElseIf SearchCatg = "ST" Then
            strSQL += " and PartnerID IN (SELECT DISTINCT addressSourceID FROM addresses WHERE addressRef = 'D' AND addressType = 'H' AND addressState LIKE '%" + SearchData + "%')"
        ElseIf SearchCatg = "CO" Then
            strSQL += " and PartnerID IN (SELECT DISTINCT addressSourceID FROM addresses WHERE addressRef = 'D' AND addressType = 'H' AND addressCountry LIKE '%" + SearchData + "%')"
        ElseIf SearchCatg = "CA" Then
            strSQL += " AND PartnerID IN (SELECT DISTINCT cdl_customercategories.CustomerID  FROM cdl_customercategories " _
              & "INNER JOIN cdl_syscategories ON cdl_customercategories.CategoryID = cdl_syscategories.CategoryID  " _
              & "INNER JOIN cdl_textdescriptor ON cdl_syscategories.CategoryName = cdl_textdescriptor.DescriptionID " _
              & " WHERE (cdl_textdescriptor.TextDescription LIKE '%" + SearchData + "%'))"
        End If
        If strPartnerID <> "" Then
            strSQL += " and PartnerID ='" + strPartnerID + "'  "
        End If
        'If strCustType <> "" Then
        '    strSQL += " and PartnerSelTypeID='" & strCustType & "' "
        'Else
        '    strSQL += " and PartnerSelTypeID!='2' "
        'End If
        strSQL += "  order by PartnerLongName "
        Return strSQL
    End Function

    ''' <summary>
    ''' Fill Grid of Partners
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <param name="strCustType"></param>
    ''' <param name="SearchCatg"></param>
    ''' <param name="createdOnStart"></param>
    ''' <param name="createdOnEnd"></param>
    ''' <param name="modifiedOnStart"></param>
    ''' <param name="modifiedOnEnd"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal StatusData As String, ByVal SearchData As String, ByVal strCustType As String, ByVal SearchCatg As String, ByVal createdOnStart As String, ByVal createdOnEnd As String, ByVal modifiedOnStart As String, ByVal modifiedOnEnd As String) As String
        '        StatusData = clsCommon.funRemove(StatusData, False)
        SearchData = clsCommon.funRemove(SearchData)
        If SearchData = "sysNull" Then
            SearchData = ""
        End If
        Dim strSQL As String = ""
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distinct PartnerID,PartnerAcronyme, PartnerLongName, PartnerPhone, PartnerPhone2, PartnerFax, PartnerEmail, PartnerWebsite,PartnerActive as PartnerActiveForCsv, CASE PartnerActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as PartnerActive, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, case PartnerSelTypeID when 4 then 'blue.jpg' end as custType, PartnerNote FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID "
        Else
            strSQL = "SELECT distinct PartnerID,PartnerAcronyme, PartnerLongName, PartnerPhone, PartnerPhone2, PartnerFax, PartnerEmail, PartnerWebsite,PartnerActive as PartnerActiveForCsv, CASE PartnerActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as PartnerActive, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, case PartnerSelTypeID when 4 then 'blue.jpg' end as custType, PartnerNote FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID inner join salesrepcustomer s on s.CustomerID=partners.PartnerID  and s.UserID='" & HttpContext.Current.Session("UserID") & "'"
        End If

        If SearchCatg = OrderSearchFields.InvoiceNo Then
            strSQL += " INNER JOIN invoices i on  i.invCustID = partners.PartnerID "
        End If
        strSQL += " where 1=1 "
        If StatusData <> "" Then
            strSQL += " and PartnerActive='" + StatusData + "'  "
        End If
        If SearchCatg = CustomerSearchFields.ContactName Then
            strSQL += " and (PartnerLongName like '%" + SearchData + "%' or PartnerAcronyme like '%" + SearchData + "%') "
        ElseIf SearchCatg = OrderSearchFields.InvoiceNo Then
            strSQL += " and i.InvRefNo like '%" + SearchData + "%'"
        ElseIf SearchCatg = CustomerSearchFields.PartnerPhone Then
            strSQL += " and PartnerPhone like '%" + SearchData + "%'"
        ElseIf SearchCatg = CustomerSearchFields.Acronyme Then
            strSQL += " and PartnerAcronyme like '%" + SearchData + "%'"
        ElseIf SearchCatg = "PC" Then
            strSQL += " and PartnerID IN (SELECT DISTINCT addressSourceID FROM addresses WHERE addressRef = 'D' AND addressType = 'H' AND addressPostalCode = '" + SearchData + "')"
        ElseIf SearchCatg = "CT" Then
            strSQL += " and PartnerID IN (SELECT DISTINCT addressSourceID FROM addresses WHERE addressRef = 'D' AND addressType = 'H' AND addressCity LIKE '%" + SearchData + "%')"
        ElseIf SearchCatg = "ST" Then
            strSQL += " and PartnerID IN (SELECT DISTINCT addressSourceID FROM addresses WHERE addressRef = 'D' AND addressType = 'H' AND addressState LIKE '%" + SearchData + "%')"
        ElseIf SearchCatg = "CO" Then
            strSQL += " and PartnerID IN (SELECT DISTINCT addressSourceID FROM addresses WHERE addressRef = 'D' AND addressType = 'H' AND addressCountry LIKE '%" + SearchData + "%')"
        ElseIf SearchCatg = "CA" Then
            strSQL += " AND PartnerID IN (SELECT DISTINCT cdl_customercategories.CustomerID  FROM cdl_customercategories " _
              & "INNER JOIN cdl_syscategories ON cdl_customercategories.CategoryID = cdl_syscategories.CategoryID  " _
              & "INNER JOIN cdl_textdescriptor ON cdl_syscategories.CategoryName = cdl_textdescriptor.DescriptionID " _
              & " WHERE (cdl_textdescriptor.TextDescription LIKE '%" + SearchData + "%'))"
        End If

        If Not String.IsNullOrEmpty(createdOnStart) Then
            If Not String.IsNullOrEmpty(createdOnEnd) Or createdOnStart <> createdOnEnd Then
                strSQL += " AND PartnerCreatedOn BETWEEN '" + createdOnStart + "' AND '" + createdOnEnd + "'"
            Else
                Dim dtStart As DateTime = Convert.ToDateTime(createdOnStart)
                dtStart = dtStart.AddDays(1)
                strSQL += " AND PartnerCreatedOn BETWEEN '" + createdOnStart + "' AND '" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "'"
            End If
        End If

        If Not String.IsNullOrEmpty(modifiedOnStart) Then
            If Not String.IsNullOrEmpty(modifiedOnEnd) Or modifiedOnStart <> modifiedOnEnd Then
                strSQL += " AND PartnerLastUpdatedOn BETWEEN '" + modifiedOnStart + "' AND '" + modifiedOnEnd + "'"
            Else
                Dim dtStart As DateTime = Convert.ToDateTime(modifiedOnStart)
                dtStart = dtStart.AddDays(1)
                strSQL += " AND PartnerLastUpdatedOn BETWEEN '" + modifiedOnStart + "' AND '" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "'"
            End If
        End If



        'If strCustType <> "" Then
        '    strSQL += " and PartnerSelTypeID='" & strCustType & "' "
        'Else
        '    strSQL += " and PartnerSelTypeID!='2' "
        'End If
        strSQL += "  order by PartnerLongName "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill ActivityCustomers Grid
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <param name="strCustType"></param>
    ''' <param name="SearchCatg"></param>
    ''' <param name="strPartnerID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillActivityCustomersGrid(ByVal StatusData As String, ByVal SearchData As String, ByVal strCustType As String, ByVal SearchCatg As String, Optional ByVal strPartnerID As String = "") As String
        '        StatusData = clsCommon.funRemove(StatusData, False)
        SearchData = clsCommon.funRemove(SearchData)
        If SearchData = "sysNull" Then
            SearchData = ""
        End If
        Dim strSQL As String = ""
        strSQL = "SELECT distinct p.PartnerID,p.PartnerAcronyme, p.PartnerLongName, p.PartnerPhone, p.PartnerPhone2, p.PartnerFax, p.PartnerEmail, p.PartnerWebsite,p.PartnerActive as PartnerActiveForCsv, CASE p.PartnerActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as PartnerActive, p.PartnerLocked, p.PartnerCreatedOn, p.PartnerCreatedBy, p.PartnerLastUpdatedOn, p.PartnerLastUpdatedBy, p.PartnerType, case ps.PartnerSelTypeID when 4 then 'blue.jpg' end as custType, p.PartnerNote, (SELECT p1.FollowUpDate FROM partneractivitylog p1 WHERE p1.PartnerId = p.PartnerID  ORDER BY p1.FollowUpDate DESC LIMIT 1) AS LastFollowUpDate, (SELECT p2.ActivityCreatedDate FROM partneractivitylog p2 WHERE p2.PartnerId = p.PartnerID  ORDER BY p2.ActivityCreatedDate DESC LIMIT 1) AS LastActivityDate FROM partners p Inner Join partnerseltype ps on ps.PartnerSelPartID=p.PartnerID INNER JOIN partneractivitylog pal ON pal.PartnerId = p.PartnerID "
        If SearchCatg = OrderSearchFields.InvoiceNo Then
            strSQL += " INNER JOIN invoices i on  i.invCustID = p.PartnerID "
        End If
        strSQL += " where 1=1 "
        If StatusData <> "" Then
            strSQL += " and p.PartnerActive='" + StatusData + "'  "
        End If
        If SearchCatg = CustomerSearchFields.ContactName Then
            strSQL += " and (p.PartnerLongName like '%" + SearchData + "%' or p.PartnerAcronyme like '%" + SearchData + "%') "
        ElseIf SearchCatg = OrderSearchFields.InvoiceNo Then
            strSQL += " and i.InvRefNo like '%" + SearchData + "%'"
        ElseIf SearchCatg = CustomerSearchFields.PartnerPhone Then
            strSQL += " and p.PartnerPhone like '%" + SearchData + "%'"
        ElseIf SearchCatg = CustomerSearchFields.Acronyme Then
            strSQL += " and p.PartnerAcronyme like '%" + SearchData + "%'"
        End If
        If strPartnerID <> "" Then
            strSQL += " and p.PartnerID ='" + strPartnerID + "'  "
        End If
        'If strCustType <> "" Then
        '    strSQL += " and PartnerSelTypeID='" & strCustType & "' "
        'Else
        '    strSQL += " and PartnerSelTypeID!='2' "
        'End If
        strSQL += "  order by p.PartnerLongName "
        Return strSQL
    End Function
    ''' <summary>
    '''  Fill Grid of Partners
    ''' </summary>
    ''' <param name="strClassification"></param>
    ''' <param name="strSpecialization"></param>
    ''' <param name="strState"></param>
    ''' <param name="strCountry"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGridByOtherField(ByVal strClassification As String, ByVal strSpecialization As String, ByVal strState As String, ByVal strCountry As String) As String
        Dim strSQL As String = ""
        strSQL = "SELECT distinct PartnerID, PartnerLongName, PartnerPhone, PartnerFax, PartnerEmail, PartnerWebsite,PartnerActive as PartnerActiveForCsv, CASE PartnerActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as PartnerActive, PartnerLocked, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, PartnerNote,PartnerAcronyme FROM partners as p  "
        If strClassification <> "0" Or strSpecialization <> "0" Or strState <> "0" Or strCountry <> "0" Then
            strSQL += " left join addresses a on a.addressSourceID=p.PartnerID and a.addressRef='P' "
            strSQL += " left join partnerselclass psc on psc.PartnerSelPartID=p.PartnerID "
            strSQL += " left join partnerselspecialization pss on pss.PartnerSelPartID=p.PartnerID "
            strSQL += " where 1=1 "
        End If
        If strClassification <> "0" Then
            strSQL += " and psc.PartnerSelClassID = '" + strClassification + "' "
        End If
        If strSpecialization <> "0" Then
            strSQL += " and pss.PartnerSelSplID ='" + strSpecialization + "' "
        End If
        If strState <> "0" Then
            strSQL += " and a.addressState ='" + strState + "'  "
        End If
        If strCountry <> "0" Then
            strSQL += " and a.addressCountry ='" + strCountry + "'  "
        End If
        strSQL += " order by PartnerLongName "
        Return strSQL
    End Function
    ''' <summary>
    '''  Delete Partner
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeletePartner(ByVal DeleteID As String) As String
        Dim strSQL As String
        strSQL = "UPDATE partners SET PartnerActive=0 WHERE PartnerID='" + DeleteID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Check Duplicate Partner Email ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicatePartnerEmailID() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM partners where PartnerEmail='" & _PartnerEmail & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Populate objects of Partner With Ref To Name and Email
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getPartnerInfoWithRefToNameEmail()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT * FROM partners where PartnerLongName='" & _PartnerLongName & "' and PartnerEmail='" & _PartnerEmail & "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _PartnerID = drObj.Item("PartnerID").ToString
            _PartnerLongName = drObj.Item("PartnerLongName").ToString
            _PartnerPhone = drObj.Item("PartnerPhone").ToString
            _PartnerFax = drObj.Item("PartnerFax").ToString
            _PartnerEmail = drObj.Item("PartnerEmail").ToString
            _PartnerWebsite = drObj.Item("PartnerWebsite").ToString
            _PartnerActive = drObj.Item("PartnerActive").ToString
            _PartnerCreatedOn = drObj.Item("PartnerCreatedOn").ToString
            _PartnerCreatedBy = drObj.Item("PartnerCreatedBy").ToString
            _PartnerLastUpdatedOn = drObj.Item("PartnerLastUpdatedOn").ToString
            _PartnerLastUpdatedBy = drObj.Item("PartnerLastUpdatedBy").ToString
            _PartnerType = drObj.Item("PartnerType").ToString
            _PartnerAcronyme = drObj.Item("PartnerAcronyme").ToString
            _PartnerNote = drObj.Item("PartnerNote").ToString
            _PartnerValidated = drObj.Item("PartnerValidated").ToString
            _PartnerTaxCode = drObj.Item("PartnerTaxCode").ToString
            _PartnerCommissionCode = drObj.Item("PartnerCommissionCode").ToString
            _PartnerDiscount = drObj.Item("PartnerDiscount").ToString
            _PartnerCurrencyCode = drObj.Item("PartnerCurrencyCode").ToString
            _PartnerInvoiceNetTerms = drObj.Item("PartnerInvoiceNetTerms").ToString
            _PartnerShipBlankPref = drObj.Item("PartnerShipBlankPref").ToString
            _PartnerCourierCode = drObj.Item("PartnerCourierCode").ToString
            _PartnerLang = drObj.Item("PartnerLang").ToString
            _PartnerStatus = drObj.Item("PartnerStatus").ToString
            _PartnerValue = drObj.Item("PartnerValue").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Return Modified By
    ''' </summary>
    ''' <param name="sID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetModifiedBy(ByVal sID As String) As String
        Dim strData As String = ""
        Dim objSU As New clsUser
        objSU.UserID = sID
        objSU.getUserInfo()
        strData = objSU.UserLoginId
        objSU = Nothing
        Return strData
    End Function
    ''' <summary>
    ''' Return Created By
    ''' </summary>
    ''' <param name="sID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetCreatedBy(ByVal sID As String) As String
        Dim strData As String = ""
        Dim objSU As New clsUser
        objSU.UserID = sID
        objSU.getUserInfo()
        strData = objSU.UserLoginId
        objSU = Nothing
        Return strData
    End Function
    ''' <summary>
    ''' Populate Control
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <param name="sPID"></param>
    ''' <param name="strCustType"></param>
    ''' <remarks></remarks>
    Public Sub PopulatePartner(ByVal ddl As DropDownList, Optional ByVal sPID As String = "", Optional ByVal strCustType As String = "")
        Dim strSQL As String = ""
        strSQL = "SELECT PartnerID, PartnerLongName, PartnerEmail FROM partners  "

        If strCustType <> "" Then
            strSQL += " Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID "
        End If
        strSQL += " where PartnerActive='1'"
        If sPID <> "" Then
            strSQL += " and PartnerID='" & sPID & "' "
        End If
        If strCustType <> "" Then
            strSQL += " and PartnerSelTypeID='" & strCustType & "' "
        End If
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "PartnerLongName"
        ddl.DataValueField = "PartnerID"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.liCMSelectPartner
        ddl.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Check Partner Name
    ''' </summary>
    ''' <param name="sName"></param>
    ''' <param name="sPartnerID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckPartnerName(ByVal sName As String, ByRef sPartnerID As String) As Boolean
        sName = clsCommon.funRemove(sName, False)
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM partners where PartnerLongName='" & sName & "' and PartnerActive='1' "
        If GetScalarData(strSQL) <> 0 Then
            strSQL = "SELECT PartnerID FROM partners where PartnerLongName='" & sName & "' and PartnerActive='1' "
            sPartnerID = GetScalarData(strSQL)
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Populate SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <remarks></remarks>
    Public Sub PopulateSR(ByVal chklst As CheckBoxList)
        Dim strSQL As String
        strSQL = "SELECT userID, concat(userFirstName, ' ',userLastName) as Name FROM users WHERE userActive='1' and userModules like '%SR%' order by Name "
        chklst.Items.Clear()
        chklst.DataSource = GetDataReader(strSQL)
        chklst.DataTextField = "Name"
        chklst.DataValueField = "userID"
        chklst.DataBind()
        CloseDatabaseConnection()

        Dim SR As String = GetCurrentSRUser()
        For i As Int16 = 0 To chklst.Items.Count - 1
            If SR.Contains("~" & chklst.Items(i).Value & "~") Then
                If chklst.Items(i).Value = HttpContext.Current.Session("UserID") Then
                    chklst.Items(i).Selected = True
                End If
            End If
        Next
    End Sub
    ''' <summary>
    ''' Current SR User
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCurrentSRUser() As String
        Dim strSQL As String
        strSQL = "SELECT u.userID FROM users u WHERE userActive='1' and userModules like '%SR%' "
        Dim SR As String = "~"
        Dim dr As OdbcDataReader = GetDataReader(strSQL)
        While dr.Read
            SR += dr.Item(0) & "~"
        End While
        CloseDatabaseConnection()
        Return SR
    End Function
    ''' <summary>
    ''' Populate User's SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <param name="DistID"></param>
    ''' <param name="strCustType"></param>
    ''' <param name="strTM"></param>
    ''' <remarks></remarks>
    Public Sub PopulateUserSR(ByVal chklst As CheckBoxList, ByVal DistID As String, ByVal strCustType As String, Optional ByVal strTM As String = "")
        Dim strSQL As String
        strSQL = "SELECT u.userID, concat(userFirstName, ' ',userLastName) as Name, 'A' as seq FROM users u inner join salesrepcustomer s on s.UserID=u.userID and CustomerID='" & DistID & "' and CustomerType='" & strCustType & "' WHERE userActive='1' and userModules like '%SR%' Union SELECT u.userID, concat(userFirstName, ' ',userLastName) as Name, 'B' as seq FROM users u WHERE userActive='1' and userModules like '%SR%' and u.userID not in (Select UserID from salesrepcustomer where CustomerID='" & DistID & "' and CustomerType='" & strCustType & "') order by seq, Name"
        chklst.Items.Clear()
        chklst.DataSource = GetDataReader(strSQL)
        chklst.DataTextField = "Name"
        chklst.DataValueField = "userID"
        chklst.DataBind()
        CloseDatabaseConnection()

        Dim SR As String
        SR = ""
        If strTM = "" Then
            SR = GetUserSR(DistID, strCustType)
        ElseIf strTM = "TM" Then 'Tele Marketing
            SR = GetUserSRForTM(DistID, strCustType)
        End If

        For i As Int16 = 0 To chklst.Items.Count - 1
            If SR.Contains("~" & chklst.Items(i).Value & "~") Then
                chklst.Items(i).Selected = True
            End If
        Next
    End Sub
    ''' <summary>
    ''' Get User's SR
    ''' </summary>
    ''' <param name="DistID"></param>
    ''' <param name="strCustType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetUserSR(ByVal DistID As String, ByVal strCustType As String) As String
        Dim strSQL As String
        strSQL = "SELECT u.userID FROM users u inner join salesrepcustomer s on s.UserID=u.userID and CustomerID='" & DistID & "' and CustomerType='" & strCustType & "' WHERE userActive='1' and userModules like '%SR%' "
        Dim SR As String = "~"
        Dim dr As OdbcDataReader = GetDataReader(strSQL)
        While dr.Read
            SR += dr.Item(0) & "~"
        End While
        CloseDatabaseConnection()
        Return SR
    End Function
    ''' <summary>
    ''' Get User's TM
    ''' </summary>
    ''' <param name="DistID"></param>
    ''' <param name="strCustType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetUserSRForTM(ByVal DistID As String, ByVal strCustType As String) As String
        Dim strSQL As String
        strSQL = "SELECT u.userID FROM users u inner join assignleads s on s.UserID=u.userID and CustomerID='" & DistID & "' and CustomerType='" & strCustType & "' WHERE userActive='1' and userModules like '%SR%' "
        Dim SR As String = "~"
        Dim dr As OdbcDataReader = GetDataReader(strSQL)
        While dr.Read
            SR += dr.Item(0) & "~"
        End While
        CloseDatabaseConnection()
        Return SR
    End Function
    ''' <summary>
    ''' Insert SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <param name="DistID"></param>
    ''' <param name="strCustType"></param>
    ''' <remarks></remarks>
    Public Sub InsertSR(ByVal chklst As CheckBoxList, ByVal DistID As String, ByVal strCustType As String)
        Dim objSR As New clsSalesRepCustomer
        Dim i As Integer
        For i = 0 To chklst.Items.Count - 1
            If chklst.Items(i).Selected = True Then
                objSR.UserID = chklst.Items(i).Value
                objSR.CustID = DistID
                objSR.CustType = strCustType
                objSR.insertSalesRepCustomer()
            End If
        Next
    End Sub
    ''' <summary>
    ''' Update SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <param name="DistID"></param>
    ''' <param name="strCustType"></param>
    ''' <param name="strLeads"></param>
    ''' <remarks></remarks>
    Public Sub UpdateSR(ByVal chklst As CheckBoxList, ByVal DistID As String, ByVal strCustType As String, Optional ByVal strLeads As String = "")
        Dim objSR As New clsSalesRepCustomer
        objSR.CustID = DistID
        'If strLeads <> "" Then
        '    objSR.CustType = "L"
        'Else
        objSR.CustType = strLeads
        'End If
        objSR.DeleteSalesRepCustomer()

        For i As Int16 = 0 To chklst.Items.Count - 1
            If chklst.Items(i).Selected = True Then
                objSR.UserID = chklst.Items(i).Value
                objSR.CustID = DistID
                objSR.CustType = strCustType
                objSR.insertSalesRepCustomer()
            End If
        Next
    End Sub
    ''' <summary>
    ''' Return New Distributor ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetNewDistributorID() As String
        Dim strSQL As String
        strSQL = "SELECT Max(PartnerID) FROM partners "
        Dim strData As String = ""
        strData = GetScalarData(strSQL)
        Return strData
    End Function
    ''' <summary>
    ''' Populate SR 
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub subGetSR(ByVal ddl As DropDownList)
        Dim strSQL As String
        strSQL = "SELECT UserId, concat(userFirstName,' ',userLastName) as UserName FROM users WHERE UserActive='1' and userModules like '%SR%' order by UserName "
        'If Current.Session("UserModules").ToString.Contains("ADM") = False Then
        '    strSQL += " UserID='" & Current.Session("UserID") & "' "
        'End If
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "UserName"
        ddl.DataValueField = "UserId"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.liSelectSR
        ddl.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill Grid of Partners
    ''' </summary>
    ''' <param name="strOption"></param>
    ''' <param name="StrCustName"></param>
    ''' <param name="strCustPhoneNo"></param>
    ''' <param name="strAssignedTo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGridForAssignSR(ByVal strOption As String, ByVal StrCustName As String, ByVal strCustPhoneNo As String, ByVal strAssignedTo As String) As String
        StrCustName = clsCommon.funRemove(StrCustName)
        strCustPhoneNo = clsCommon.funRemove(strCustPhoneNo)
        StrCustName = clsCommon.funRemove(StrCustName)
        strAssignedTo = clsCommon.funRemove(strAssignedTo)
        Dim strSQL As String = ""

        strSQL = "SELECT distinct PartnerID,PartnerAcronyme, PartnerLongName, PartnerPhone, PartnerFax,s.UserID as AssignedTo, PartnerEmail FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID "

        If strOption = "NA" Or strOption = "AL" Then
            strSQL += " left join salesrepcustomer s on s.CustomerID=partners.PartnerID "
        ElseIf strOption = "AS" Then
            strSQL += " inner join salesrepcustomer s on s.CustomerID=partners.PartnerID  " 'and s.UserID='" & Current.Session("UserID") & "' "
        End If

        'strSQL += " left join users u on u.userID=s.UserID" 'concat(u.userFirstName,' ',u.userLastName)

        strSQL += " where 1=1 "
        strSQL += " and PartnerActive='1'  "

        If strOption = "NA" Then
            strSQL += " and PartnerID not in (select CustomerID from salesrepcustomer) "
        ElseIf strOption = "AS" Then
            strSQL += " and PartnerID in (select CustomerID from salesrepcustomer) "
        ElseIf strOption = "AL" Then
        End If

        If StrCustName <> "" Then
            strSQL += " and (PartnerLongName like '%" + StrCustName + "%' or PartnerAcronyme like '%" + StrCustName + "%') "
        End If
        If strCustPhoneNo <> "" Then
            strSQL += " and PartnerPhone like '%" + strCustPhoneNo + "%'"
        End If

        If strAssignedTo <> "" Then
            strSQL += " and concat(u.userFirstName,' ',u.userLastName) like '%" & strAssignedTo & "%' "
        End If

        If strOption = "NA" Or strOption = "AL" Then
            strSQL += " and ps.PartnerSelTypeID='4'"
        End If

        strSQL += "  order by PartnerLongName " 'and ps.PartnerSelTypeID='4'
        Return strSQL
    End Function
    ''' <summary>
    ''' Update note and inactive status
    ''' </summary>
    ''' <param name="strDeleteID"></param>
    ''' <param name="strReason"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateNote(ByVal strDeleteID As String, ByVal strReason As String) As Boolean
        Dim strSQL As String
        strSQL = "UPDATE partners SET PartnerActive=0 ,INActiveReason='" & strReason & "' WHERE PartnerID='" + strDeleteID + "' "
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If

    End Function
    ''' <summary>
    ''' Populate SR 
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub subGetJobsPanningUser(ByVal ddl As DropDownList)
        Dim strSQL As String
        strSQL = "SELECT UserId, concat(userFirstName,' ',userLastName) as UserName FROM users WHERE UserActive='1' and userModules like '%JOBP%' order by UserName "
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "UserName"
        ddl.DataValueField = "UserId"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.lblselect
        ddl.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
End Class

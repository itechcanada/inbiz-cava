Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsPartnerSelType
	Inherits clsDataClass
	Private _PartnerSelTypeID, _PartnerSelPartID As String
	Public Property PartnerSelTypeID() As String
		Get
			Return _PartnerSelTypeID
		End Get
		Set(ByVal value As String)
            _PartnerSelTypeID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerSelPartID() As String
		Get
			Return _PartnerSelPartID
		End Get
		Set(ByVal value As String)
            _PartnerSelPartID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert PartnerSelType
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertPartnerSelType() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO partnerseltype(PartnerSelTypeID, PartnerSelPartID) VALUES('"
		strSQL += _PartnerSelTypeID + "','"
		strSQL += _PartnerSelPartID + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update PartnerSelType
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updatePartnerSelType() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE partnerseltype set "
        strSQL += "PartnerSelTypeID='" + _PartnerSelTypeID + "'"
        strSQL += " where PartnerSelPartID='" + _PartnerSelPartID + "' "
        strSQL += " and PartnerSelTypeID='" + _PartnerSelTypeID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
    End Function
    ''' <summary>
    ''' Delete PartnerSelType
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function deletePartnerSelType() As Boolean
        Dim strSQL As String
        strSQL = "Delete from partnerseltype "
        strSQL += " where PartnerSelPartID='" + _PartnerSelPartID + "' "
        strSQL += " and PartnerSelTypeID='" + _PartnerSelTypeID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of PartnerSelType
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPartnerSelTypeInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
        strSQL = "SELECT PartnerSelPartID, PartnerSelTypeID FROM partnerseltype where PartnerSelPartID='" + _PartnerSelPartID + "' "
		drObj = GetDataReader(strSQL)
        While drObj.Read
            _PartnerSelPartID = drObj.Item("PartnerSelPartID").ToString
            _PartnerSelTypeID = drObj.Item("PartnerSelTypeID").ToString
        End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill Grid of Partners Sel Types
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String = ""
        strSQL = "SELECT PartnerSelPartID, PartnerSelTypeID FROM partnerseltype where PartnerSelPartID='" + _PartnerSelPartID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Check Record for Partners
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckRecord() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM partnerseltype where PartnerSelPartID='" & _PartnerSelPartID & "' and PartnerSelTypeID='" & _PartnerSelTypeID & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
End Class

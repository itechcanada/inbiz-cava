Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsPartnerSelSpecialization
	Inherits clsDataClass
	Private _PartnerSelSplID, _PartnerSelPartID As String
	Public Property PartnerSelSplID() As String
		Get
			Return _PartnerSelSplID
		End Get
		Set(ByVal value As String)
            _PartnerSelSplID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerSelPartID() As String
		Get
			Return _PartnerSelPartID
		End Get
		Set(ByVal value As String)
            _PartnerSelPartID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert PartnerSelSpecialization
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertPartnerSelSpecialization() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO partnerselspecialization(PartnerSelSplID, PartnerSelPartID) VALUES('"
		strSQL += _PartnerSelSplID + "','"
		strSQL += _PartnerSelPartID + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update PartnerSelSpecialization
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updatePartnerSelSpecialization() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE partnerselspecialization set "
        strSQL += "PartnerSelSplID='" + _PartnerSelSplID + "'"
        strSQL += " where PartnerSelSplID='" + _PartnerSelSplID + "' "
        strSQL += " and PartnerSelPartID='" + _PartnerSelPartID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
    End Function
    ''' <summary>
    '''  Delete PartnerSelSpecialization
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function deletePartnerSelSpecialization() As Boolean
        Dim strSQL As String
        strSQL = "Delete from partnerselspecialization "
        strSQL += " where PartnerSelSplID='" + _PartnerSelSplID + "' "
        strSQL += " and PartnerSelPartID='" + _PartnerSelPartID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of PartnerSelSpecialization
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPartnerSelSpecializationInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
        strSQL = "SELECT PartnerSelSplID, PartnerSelPartID FROM partnerselspecialization where PartnerSelPartID='" + _PartnerSelPartID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
            _PartnerSelPartID = drObj.Item("PartnerSelPartID").ToString
            _PartnerSelSplID = drObj.Item("PartnerSelSplID").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill Grid of Partners Sel Specialization
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String = ""
        strSQL = "SELECT PartnerSelSplID, PartnerSelPartID FROM partnerselspecialization where PartnerSelPartID='" + _PartnerSelPartID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Check Record for Partner
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckRecord() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM partnerselspecialization where PartnerSelPartID='" & _PartnerSelPartID & "' and PartnerSelSplID='" & _PartnerSelSplID & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
End Class

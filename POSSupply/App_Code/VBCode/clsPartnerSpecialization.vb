Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports System.Web.HttpContext
''Imports clsCommon
''Imports Resources.Resource
Public Class clsPartnerSpecialization
	Inherits clsDataClass
	Private _PartnerSpecializationID, _PartnerSplDescLang, _PartnerSplDesc, _PartnerSplActive As String
	Public Property PartnerSpecializationID() As String
		Get
			Return _PartnerSpecializationID
		End Get
		Set(ByVal value As String)
            _PartnerSpecializationID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerSplDescLang() As String
		Get
			Return _PartnerSplDescLang
		End Get
		Set(ByVal value As String)
            _PartnerSplDescLang = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerSplDesc() As String
		Get
			Return _PartnerSplDesc
		End Get
		Set(ByVal value As String)
            _PartnerSplDesc = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerSplActive() As String
		Get
			Return _PartnerSplActive
		End Get
		Set(ByVal value As String)
            _PartnerSplActive = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    '''  Insert PartnerSpecialization
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertPartnerSpecialization() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO partnerspecialization( PartnerSplDescLang, PartnerSplDesc, PartnerSplActive) VALUES('"
		strSQL += _PartnerSplDescLang + "','"
		strSQL += _PartnerSplDesc + "','"
		strSQL += _PartnerSplActive + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    '''  Update PartnerSpecialization
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updatePartnerSpecialization() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE partnerspecialization set "
		strSQL += "PartnerSplDescLang='" + _PartnerSplDescLang + "', "
		strSQL += "PartnerSplDesc='" + _PartnerSplDesc + "', "
		strSQL += "PartnerSplActive='" + _PartnerSplActive + "'"
		strSQL += " where PartnerSpecializationID='" + _PartnerSpecializationID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of PartnerSpecialization
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPartnerSpecializationInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  PartnerSplDescLang, PartnerSplDesc, PartnerSplActive FROM partnerspecialization where PartnerSpecializationID='" + _PartnerSpecializationID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_PartnerSplDescLang = drObj.Item("PartnerSplDescLang").ToString
			_PartnerSplDesc = drObj.Item("PartnerSplDesc").ToString
			_PartnerSplActive = drObj.Item("PartnerSplActive").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate Specialization
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <remarks></remarks>
    Public Sub PopulateSpecialization(ByVal chklst As CheckBoxList)
        Dim strSQL As String = ""
        Dim strLang As String = clsCommon.funGetProductLang()
        strSQL = "SELECT PartnerSpecializationID, PartnerSplDesc, PartnerSplActive FROM partnerspecialization where PartnerSplActive='1' and PartnerSplDescLang='" & strLang & "' "
        chklst.Items.Clear()
        chklst.DataSource = GetDataReader(strSQL)
        chklst.DataTextField = "PartnerSplDesc"
        chklst.DataValueField = "PartnerSpecializationID"
        chklst.DataBind()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate Specialization
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub PopulateSpecialization(ByVal ddl As DropDownList)
        Dim strSQL As String = ""
        Dim strLang As String = clsCommon.funGetProductLang()
        strSQL = "SELECT PartnerSpecializationID, PartnerSplDesc, PartnerSplActive FROM partnerspecialization where PartnerSplActive='1' and PartnerSplDescLang='" & strLang & "' "
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "PartnerSplDesc"
        ddl.DataValueField = "PartnerSpecializationID"
        ddl.DataBind()
        CloseDatabaseConnection()
        Dim itm As New WebControls.ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.liCMSelectContactSpecialization
        ddl.Items.Insert(0, itm)
    End Sub
End Class

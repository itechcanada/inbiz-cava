Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsPartnerEventLogs
	Inherits clsDataClass
	Private _PartnerServiceLogID, _PartnerID, _PartnerSvcDateTime, _PartnerEventType, _PartnerSvcLog, _PartnerEventShortName, _PartnerEvenLongName, _PartnerNumberOfVisitors As String
	Public Property PartnerServiceLogID() As String
		Get
			Return _PartnerServiceLogID
		End Get
		Set(ByVal value As String)
            _PartnerServiceLogID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerID() As String
		Get
			Return _PartnerID
		End Get
		Set(ByVal value As String)
            _PartnerID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerSvcDateTime() As String
		Get
			Return _PartnerSvcDateTime
		End Get
		Set(ByVal value As String)
            _PartnerSvcDateTime = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerEventType() As String
		Get
			Return _PartnerEventType
		End Get
		Set(ByVal value As String)
            _PartnerEventType = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerSvcLog() As String
		Get
			Return _PartnerSvcLog
		End Get
		Set(ByVal value As String)
            _PartnerSvcLog = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerEventShortName() As String
		Get
			Return _PartnerEventShortName
		End Get
		Set(ByVal value As String)
            _PartnerEventShortName = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerEvenLongName() As String
		Get
			Return _PartnerEvenLongName
		End Get
		Set(ByVal value As String)
            _PartnerEvenLongName = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerNumberOfVisitors() As String
		Get
			Return _PartnerNumberOfVisitors
		End Get
		Set(ByVal value As String)
            _PartnerNumberOfVisitors = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert PartnerEventLogs
    ''' </summary>
    ''' <param name="strLogID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertPartnerEventLogs(ByRef strLogID As String) As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO partnereventlogs( PartnerID, PartnerSvcDateTime, PartnerEventType, PartnerSvcLog, PartnerEventShortName, PartnerEvenLongName, PartnerNumberOfVisitors) VALUES('"
        strSQL += _PartnerID + "','"
        strSQL += _PartnerSvcDateTime + "','"
        strSQL += _PartnerEventType + "','"
        strSQL += _PartnerSvcLog + "','"
        strSQL += _PartnerEventShortName + "','"
        strSQL += _PartnerEvenLongName + "','"
        strSQL += _PartnerNumberOfVisitors + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        strLogID = obj.GetScalarData("Select LAST_INSERT_ID()")
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update PartnerEventLogs
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updatePartnerEventLogs() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE partnereventlogs set "
		strSQL += "PartnerID='" + _PartnerID + "', "
		strSQL += "PartnerSvcDateTime='" + _PartnerSvcDateTime + "', "
		strSQL += "PartnerEventType='" + _PartnerEventType + "', "
		strSQL += "PartnerSvcLog='" + _PartnerSvcLog + "', "
		strSQL += "PartnerEventShortName='" + _PartnerEventShortName + "', "
		strSQL += "PartnerEvenLongName='" + _PartnerEvenLongName + "', "
		strSQL += "PartnerNumberOfVisitors='" + _PartnerNumberOfVisitors + "'"
		strSQL += " where PartnerServiceLogID='" + _PartnerServiceLogID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of PartnerEventLogs
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPartnerEventLogsInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  PartnerID, PartnerSvcDateTime, PartnerEventType, PartnerSvcLog, PartnerEventShortName, PartnerEvenLongName, PartnerNumberOfVisitors FROM partnereventlogs where PartnerServiceLogID='" + _PartnerServiceLogID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_PartnerID = drObj.Item("PartnerID").ToString
			_PartnerSvcDateTime = drObj.Item("PartnerSvcDateTime").ToString
			_PartnerEventType = drObj.Item("PartnerEventType").ToString
			_PartnerSvcLog = drObj.Item("PartnerSvcLog").ToString
			_PartnerEventShortName = drObj.Item("PartnerEventShortName").ToString
			_PartnerEvenLongName = drObj.Item("PartnerEvenLongName").ToString
			_PartnerNumberOfVisitors = drObj.Item("PartnerNumberOfVisitors").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill Grid of Events
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String = ""
        strSQL = "SELECT PartnerServiceLogID, PartnerID, PartnerSvcDateTime, PartnerEventType, PartnerSvcLog, PartnerEventShortName, PartnerEvenLongName, PartnerNumberOfVisitors FROM partnereventlogs "
        strSQL += " where PartnerID='" & _PartnerID & "' "
        strSQL += " order by PartnerServiceLogID "
        Return strSQL
    End Function
    ''' <summary>
    ''' Delete Event
    ''' </summary>
    ''' <param name="strLogID"></param>
    ''' <param name="strPartnerID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteEvent(ByVal strLogID As String, ByVal strPartnerID As String) As String
        Dim objDataClass As New clsDataClass
        Dim strSql As String = "DELETE from partnerevtselvst where PartnerEvtSvcLogID ='" & strLogID & "' "
        objDataClass.SetData(strSql)
        strSql = "DELETE from partnerevtseledu where PartnerEvtSvcLogID ='" & strLogID & "' "
        objDataClass.SetData(strSql)
        strSql = "DELETE from partnerevtsector where PartnerEvtSvcLogID ='" & strLogID & "' "
        objDataClass.SetData(strSql)
        strSql = "DELETE from partnereventlogs where PartnerServiceLogID ='" & strLogID & "' "
        strSql += "and PartnerID='" & strPartnerID & "' "
        objDataClass = Nothing
        Return strSql
    End Function
End Class

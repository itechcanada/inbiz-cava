Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsTaxCode
	Inherits clsDataClass
	Private _sysTaxCode, _sysTaxDesc, _sysTaxPercentage, _sysTaxSequence, _sysTaxOnTotal As String
	Public Property SysTaxCode() As String
		Get
			Return _sysTaxCode
		End Get
		Set(ByVal value As String)
            _sysTaxCode = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property SysTaxDesc() As String
		Get
			Return _sysTaxDesc
		End Get
		Set(ByVal value As String)
            _sysTaxDesc = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property SysTaxPercentage() As String
		Get
			Return _sysTaxPercentage
		End Get
		Set(ByVal value As String)
            _sysTaxPercentage = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property SysTaxSequence() As String
		Get
			Return _sysTaxSequence
		End Get
		Set(ByVal value As String)
            _sysTaxSequence = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property SysTaxOnTotal() As String
		Get
			Return _sysTaxOnTotal
		End Get
		Set(ByVal value As String)
            _sysTaxOnTotal = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert TaxCodes
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertTaxCode() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO systaxcode(sysTaxCode, sysTaxDesc, sysTaxPercentage, sysTaxSequence, sysTaxOnTotal) VALUES('"
		strSQL += _sysTaxCode + "','"
		strSQL += _sysTaxDesc + "','"
		strSQL += _sysTaxPercentage + "','"
		strSQL += _sysTaxSequence + "','"
		strSQL += _sysTaxOnTotal + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update TaxCode
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateTaxCode() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE systaxcode set "
		strSQL += "sysTaxDesc='" + _sysTaxDesc + "', "
		strSQL += "sysTaxPercentage='" + _sysTaxPercentage + "', "
		strSQL += "sysTaxSequence='" + _sysTaxSequence + "', "
		strSQL += "sysTaxOnTotal='" + _sysTaxOnTotal + "'"
		strSQL += " where sysTaxCode='" + _sysTaxCode + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of TaxCode
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getTaxCodeInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  sysTaxDesc, sysTaxPercentage, sysTaxSequence, sysTaxOnTotal FROM systaxcode where sysTaxCode='" + _sysTaxCode + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_sysTaxDesc = drObj.Item("sysTaxDesc").ToString
			_sysTaxPercentage = drObj.Item("sysTaxPercentage").ToString
			_sysTaxSequence = drObj.Item("sysTaxSequence").ToString
			_sysTaxOnTotal = drObj.Item("sysTaxOnTotal").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate objects of TaxCode
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getTaxCodeSpecific()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  sysTaxDesc, sysTaxPercentage, sysTaxSequence, sysTaxOnTotal FROM systaxcode where sysTaxCode='" + _sysTaxCode + "' and sysTaxDesc='" & _sysTaxDesc & "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _sysTaxDesc = drObj.Item("sysTaxDesc").ToString
            _sysTaxPercentage = drObj.Item("sysTaxPercentage").ToString
            _sysTaxSequence = drObj.Item("sysTaxSequence").ToString
            _sysTaxOnTotal = drObj.Item("sysTaxOnTotal").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Update TaxCode
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateTaxCodeSpecific() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE systaxcode set "
        strSQL += "sysTaxDesc='" + _sysTaxDesc + "', "
        strSQL += "sysTaxPercentage='" + _sysTaxPercentage + "', "
        strSQL += "sysTaxSequence='" + _sysTaxSequence + "', "
        strSQL += "sysTaxOnTotal='" + _sysTaxOnTotal + "'"
        strSQL += " where sysTaxCode='" + _sysTaxCode + "' and sysTaxDesc='" + _sysTaxDesc + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Check Duplicate Tax Description
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateTaxDescription() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM systaxcode where "
        strSQL += "sysTaxDesc='" & _sysTaxDesc & "' and sysTaxCode='" & _sysTaxCode & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    '''  Fill Grid for Taxes
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        strSQL = "SELECT sysTaxCode, sysTaxCodeDescText, sysTaxDesc, sysTaxPercentage, sysTaxSequence, sysTaxOnTotal FROM systaxcode inner join systaxcodedesc on sysTaxCodeDescID=sysTaxCode "
        strSQL += " order by sysTaxCode, sysTaxDesc, sysTaxSequence "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid for Taxes
    ''' </summary>
    ''' <param name="TaxID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal TaxID As String) As String
        Dim strSQL As String
        strSQL = "SELECT sysTaxCode, sysTaxCodeDescText, sysTaxDesc, sysTaxPercentage, sysTaxSequence, sysTaxOnTotal FROM systaxcode inner join systaxcodedesc on sysTaxCodeDescID=sysTaxCode "
        If TaxID <> "" Then
            strSQL += " where sysTaxCode='" & TaxID & "' "
        End If
        strSQL += " order by sysTaxCode, sysTaxDesc, sysTaxSequence "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid for Taxes
    ''' </summary>
    ''' <param name="TaxGroup"></param>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal TaxGroup As String, ByVal SearchData As String) As String
        Dim strSQL As String
        strSQL = "SELECT sysTaxCode, sysTaxCodeDescText, sysTaxDesc, sysTaxPercentage, sysTaxSequence, sysTaxOnTotal FROM systaxcode inner join systaxcodedesc on sysTaxCodeDescID=sysTaxCode "
        If TaxGroup <> "" And SearchData <> "" Then
            strSQL += " where sysTaxCodeDescText='" & TaxGroup & "' and "
            strSQL += " (sysTaxCode='" & SearchData & "' or sysTaxDesc like '%" & SearchData & "%') "
        ElseIf TaxGroup <> "" And SearchData = "" Then
            strSQL += " where sysTaxCodeDescText='" & TaxGroup & "' "
        ElseIf TaxGroup = "" And SearchData <> "" Then
            strSQL += " where (sysTaxCode='" & SearchData & "' or sysTaxDesc like '%" & SearchData & "%') "
        End If
        strSQL += " order by sysTaxCode, sysTaxDesc, sysTaxSequence "
        Return strSQL
    End Function
    ''' <summary>
    ''' Delete Taxs
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteTax() As String
        Dim strSQL As String
        strSQL = "DELETE FROM systaxcode WHERE sysTaxCode='" + _sysTaxCode + "' and sysTaxDesc='" & _sysTaxDesc & "' "
        Return strSQL
    End Function
End Class

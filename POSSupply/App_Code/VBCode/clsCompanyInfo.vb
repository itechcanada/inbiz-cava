Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
''Imports Resources.Resource
Public Class clsCompanyInfo
    Inherits clsDataClass
    Private _CompanyID, _CompanyName, _CompanyAddressLine1, _CompanyAddressLine2, _CompanyCity, _CompanyState, _CompanyCountry, _CompanyPostalCode, _CompanyBillToAddr1, _CompanyBillToAddr2, _CompanyBillToCity, _CompanyBillToState, _CompanyBillToCountry, _CompanyBillToPostalCode, _CompanyShpToTerms, _CompanyFax, _CompanyBasCur, _CompanyEmail, _CompanyInternetFax, _CompanyPOTerms, _CompanyPOName, _CompanyPOPhone, _CompanyPOEmail, _CompanySalesRepRestricted, _CompanyLogoPath, _CompanyCourierCode, _CompanyGenQuotationRemarks, _CompanyGenInvoiceRemarks, _CompanyPhoneno As String
    Public Property CompanyName() As String
        Get
            Return _CompanyName
        End Get
        Set(ByVal value As String)
            _CompanyName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyAddressLine1() As String
        Get
            Return _CompanyAddressLine1
        End Get
        Set(ByVal value As String)
            _CompanyAddressLine1 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyAddressLine2() As String
        Get
            Return _CompanyAddressLine2
        End Get
        Set(ByVal value As String)
            _CompanyAddressLine2 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyCity() As String
        Get
            Return _CompanyCity
        End Get
        Set(ByVal value As String)
            _CompanyCity = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyState() As String
        Get
            Return _CompanyState
        End Get
        Set(ByVal value As String)
            _CompanyState = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyCountry() As String
        Get
            Return _CompanyCountry
        End Get
        Set(ByVal value As String)
            _CompanyCountry = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyPostalCode() As String
        Get
            Return _CompanyPostalCode
        End Get
        Set(ByVal value As String)
            _CompanyPostalCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyBillToAddr1() As String
        Get
            Return _CompanyBillToAddr1
        End Get
        Set(ByVal value As String)
            _CompanyBillToAddr1 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyBillToAddr2() As String
        Get
            Return _CompanyBillToAddr2
        End Get
        Set(ByVal value As String)
            _CompanyBillToAddr2 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyBillToCity() As String
        Get
            Return _CompanyBillToCity
        End Get
        Set(ByVal value As String)
            _CompanyBillToCity = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyBillToState() As String
        Get
            Return _CompanyBillToState
        End Get
        Set(ByVal value As String)
            _CompanyBillToState = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyBillToCountry() As String
        Get
            Return _CompanyBillToCountry
        End Get
        Set(ByVal value As String)
            _CompanyBillToCountry = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyBillToPostalCode() As String
        Get
            Return _CompanyBillToPostalCode
        End Get
        Set(ByVal value As String)
            _CompanyBillToPostalCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyShpToTerms() As String
        Get
            Return _CompanyShpToTerms
        End Get
        Set(ByVal value As String)
            _CompanyShpToTerms = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyFax() As String
        Get
            Return _CompanyFax
        End Get
        Set(ByVal value As String)
            _CompanyFax = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyBasCur() As String
        Get
            Return _CompanyBasCur
        End Get
        Set(ByVal value As String)
            _CompanyBasCur = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyEmail() As String
        Get
            Return _CompanyEmail
        End Get
        Set(ByVal value As String)
            _CompanyEmail = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyInternetFax() As String
        Get
            Return _CompanyInternetFax
        End Get
        Set(ByVal value As String)
            _CompanyInternetFax = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyPOTerms() As String
        Get
            Return _CompanyPOTerms
        End Get
        Set(ByVal value As String)
            _CompanyPOTerms = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyPOName() As String
        Get
            Return _CompanyPOName
        End Get
        Set(ByVal value As String)
            _CompanyPOName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyPOPhone() As String
        Get
            Return _CompanyPOPhone
        End Get
        Set(ByVal value As String)
            _CompanyPOPhone = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyPOEmail() As String
        Get
            Return _CompanyPOEmail
        End Get
        Set(ByVal value As String)
            _CompanyPOEmail = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanySalesRepRestricted() As String
        Get
            Return _CompanySalesRepRestricted
        End Get
        Set(ByVal value As String)
            _CompanySalesRepRestricted = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyLogoPath() As String
        Get
            Return _CompanyLogoPath
        End Get
        Set(ByVal value As String)
            _CompanyLogoPath = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyID() As String
        Get
            Return _CompanyID
        End Get
        Set(ByVal value As String)
            _CompanyID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CourierCode() As String
        Get
            Return _CompanyCourierCode
        End Get
        Set(ByVal value As String)
            _CompanyCourierCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyGenQuotationRemarks() As String
        Get
            Return _CompanyGenQuotationRemarks
        End Get
        Set(ByVal value As String)
            _CompanyGenQuotationRemarks = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyGenInvoiceRemarks() As String
        Get
            Return _CompanyGenInvoiceRemarks
        End Get
        Set(ByVal value As String)
            _CompanyGenInvoiceRemarks = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CompanyPhoneno() As String
        Get
            Return _CompanyPhoneno
        End Get
        Set(ByVal value As String)
            _CompanyPhoneno = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Insert CompanyInfo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertCompanyInfo() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO syscompanyinfo(CompanyName, CompanyAddressLine1, CompanyAddressLine2, CompanyCity, CompanyState, CompanyCountry, CompanyPostalCode, CompanyBillToAddr1, CompanyBillToAddr2, CompanyBillToCity, CompanyBillToState, CompanyBillToCountry, CompanyBillToPostalCode, CompanyShpToTerms, CompanyFax, CompanyBasCur, CompanyEmail, CompanyInternetFax, CompanyPOTerms, CompanyPOName, CompanyPOPhone, CompanyPOEmail, CompanySalesRepRestricted, CompanyLogoPath,CompanyCourierCode,CompanyGenQuotationRemarks,CompanyGenInvoiceRemarks,CompanyPhoneno) VALUES('"
        strSQL += _CompanyName + "','"
        strSQL += _CompanyAddressLine1 + "','"
        strSQL += _CompanyAddressLine2 + "','"
        strSQL += _CompanyCity + "','"
        strSQL += _CompanyState + "','"
        strSQL += _CompanyCountry + "','"
        strSQL += _CompanyPostalCode + "','"
        strSQL += _CompanyBillToAddr1 + "','"
        strSQL += _CompanyBillToAddr2 + "','"
        strSQL += _CompanyBillToCity + "','"
        strSQL += _CompanyBillToState + "','"
        strSQL += _CompanyBillToCountry + "','"
        strSQL += _CompanyBillToPostalCode + "','"
        strSQL += _CompanyShpToTerms + "','"
        strSQL += _CompanyFax + "','"
        strSQL += _CompanyBasCur + "','"
        strSQL += _CompanyEmail + "','"
        strSQL += _CompanyInternetFax + "','"
        strSQL += _CompanyPOTerms + "','"
        strSQL += _CompanyPOName + "','"
        strSQL += _CompanyPOPhone + "','"
        strSQL += _CompanyPOEmail + "','"
        strSQL += _CompanySalesRepRestricted + "','"
        strSQL += _CompanyLogoPath + "','"
        strSQL += _CompanyCourierCode + "','"
        strSQL += _CompanyGenQuotationRemarks + "','"
        strSQL += _CompanyGenInvoiceRemarks + "','"
        strSQL += _CompanyPhoneno + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update CompanyInfo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateCompanyInfo() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE syscompanyinfo set "
        strSQL += "CompanyName='" + _CompanyName + "', "
        strSQL += "CompanyAddressLine1='" + _CompanyAddressLine1 + "', "
        strSQL += "CompanyAddressLine2='" + _CompanyAddressLine2 + "', "
        strSQL += "CompanyCity='" + _CompanyCity + "', "
        strSQL += "CompanyState='" + _CompanyState + "', "
        strSQL += "CompanyCountry='" + _CompanyCountry + "', "
        strSQL += "CompanyPostalCode='" + _CompanyPostalCode + "', "
        strSQL += "CompanyBillToAddr1='" + _CompanyBillToAddr1 + "', "
        strSQL += "CompanyBillToAddr2='" + _CompanyBillToAddr2 + "', "
        strSQL += "CompanyBillToCity='" + _CompanyBillToCity + "', "
        strSQL += "CompanyBillToState='" + _CompanyBillToState + "', "
        strSQL += "CompanyBillToCountry='" + _CompanyBillToCountry + "', "
        strSQL += "CompanyBillToPostalCode='" + _CompanyBillToPostalCode + "', "
        strSQL += "CompanyShpToTerms='" + _CompanyShpToTerms + "', "
        strSQL += "CompanyFax='" + _CompanyFax + "', "
        strSQL += "CompanyBasCur='" + _CompanyBasCur + "', "
        strSQL += "CompanyEmail='" + _CompanyEmail + "', "
        strSQL += "CompanyInternetFax='" + _CompanyInternetFax + "', "
        strSQL += "CompanyPOTerms='" + _CompanyPOTerms + "', "
        strSQL += "CompanyPOName='" + _CompanyPOName + "', "
        strSQL += "CompanyPOPhone='" + _CompanyPOPhone + "', "
        strSQL += "CompanyPOEmail='" + _CompanyPOEmail + "', "
        strSQL += "CompanySalesRepRestricted='" + _CompanySalesRepRestricted + "', "
        strSQL += "CompanyLogoPath='" + _CompanyLogoPath + "', "
        strSQL += "CompanyGenQuotationRemarks='" + _CompanyGenQuotationRemarks + "', "
        strSQL += "CompanyGenInvoiceRemarks='" + _CompanyGenInvoiceRemarks + "', "
        strSQL += "CompanyCourierCode='" + _CompanyCourierCode + "', "
        strSQL += "CompanyPhoneno='" + _CompanyPhoneno + "'"
        strSQL += " where CompanyID='" + _CompanyID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of CompanyInfo
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getCompanyInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  CompanyID,CompanyName,CompanyAddressLine1, CompanyAddressLine2, CompanyCity, CompanyState, CompanyCountry, CompanyPostalCode, CompanyBillToAddr1, CompanyBillToAddr2, CompanyBillToCity, CompanyBillToState, CompanyBillToCountry, CompanyBillToPostalCode, CompanyShpToTerms, CompanyFax, CompanyBasCur, CompanyEmail, CompanyInternetFax, CompanyPOTerms, CompanyPOName, CompanyPOPhone, CompanyPOEmail, CompanySalesRepRestricted,CompanyLogoPath,CompanyCourierCode,CompanyGenQuotationRemarks,CompanyGenInvoiceRemarks,CompanyPhoneno FROM syscompanyinfo where CompanyID='" + _CompanyID + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _CompanyID = drObj.Item("CompanyID").ToString
            _CompanyName = drObj.Item("CompanyName").ToString
            _CompanyAddressLine1 = drObj.Item("CompanyAddressLine1").ToString
            _CompanyAddressLine2 = drObj.Item("CompanyAddressLine2").ToString
            _CompanyCity = drObj.Item("CompanyCity").ToString
            _CompanyState = drObj.Item("CompanyState").ToString
            _CompanyCountry = drObj.Item("CompanyCountry").ToString
            _CompanyPostalCode = drObj.Item("CompanyPostalCode").ToString
            _CompanyBillToAddr1 = drObj.Item("CompanyBillToAddr1").ToString
            _CompanyBillToAddr2 = drObj.Item("CompanyBillToAddr2").ToString
            _CompanyBillToCity = drObj.Item("CompanyBillToCity").ToString
            _CompanyBillToState = drObj.Item("CompanyBillToState").ToString
            _CompanyBillToCountry = drObj.Item("CompanyBillToCountry").ToString
            _CompanyBillToPostalCode = drObj.Item("CompanyBillToPostalCode").ToString
            _CompanyShpToTerms = drObj.Item("CompanyShpToTerms").ToString
            _CompanyFax = drObj.Item("CompanyFax").ToString
            _CompanyBasCur = drObj.Item("CompanyBasCur").ToString
            _CompanyEmail = drObj.Item("CompanyEmail").ToString
            _CompanyInternetFax = drObj.Item("CompanyInternetFax").ToString
            _CompanyPOTerms = drObj.Item("CompanyPOTerms").ToString
            _CompanyPOName = drObj.Item("CompanyPOName").ToString
            _CompanyPOPhone = drObj.Item("CompanyPOPhone").ToString
            _CompanyPOEmail = drObj.Item("CompanyPOEmail").ToString
            _CompanySalesRepRestricted = drObj.Item("CompanySalesRepRestricted").ToString
            _CompanyLogoPath = drObj.Item("CompanyLogoPath").ToString
            _CompanyCourierCode = drObj.Item("CompanyCourierCode").ToString
            _CompanyGenQuotationRemarks = drObj.Item("CompanyGenQuotationRemarks").ToString
            _CompanyGenInvoiceRemarks = drObj.Item("CompanyGenInvoiceRemarks").ToString
            _CompanyPhoneno = drObj.Item("CompanyPhoneno").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Check Duplicate Company Code
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateCompanyName() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM syscompanyinfo where CompanyID='" & _CompanyID & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Check Company Count
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckCompanyCount() As Boolean
        Dim strSQL As String
        Dim intCount As Int16
        strSQL = "SELECT count(*) FROM syscompanyinfo "
        intCount = GetScalarData(strSQL)
        If intCount = 1 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Delete Company
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteCompany(ByVal DeleteID As String) As String
        Dim strSQL As String
        strSQL = "DELETE FROM syscompanyinfo WHERE CompanyID='" + DeleteID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grids
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        strSQL = "SELECT CompanyID,CompanyName, CompanyBasCur, CompanyEmail FROM syscompanyinfo "
        strSQL += "order by CompanyName "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid of Company with search criteria
    ''' </summary>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal SearchData As String) As String
        Dim strSQL As String
        SearchData = clsCommon.funRemove(SearchData)
        strSQL = "SELECT CompanyID,CompanyName,CompanyBasCur, CompanyEmail FROM syscompanyinfo "
        If SearchData <> "" Then
            strSQL += " where "
            strSQL += " ("
            strSQL += " CompanyName like '%" + SearchData + "%' "
            strSQL += " )"
        End If
        strSQL += " order by CompanyName "
        Return strSQL
    End Function
    ''' <summary>
    ''' Get First Company Code
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetFirstCompanyName() As String
        Dim strSQL, strWComID, strComName As String
        strSQL = "SELECT WarehouseCompanyID FROM syswarehouses where WarehouseCode='" & HttpContext.Current.Session("UserWarehouse") & "' "
        strWComID = GetScalarData(strSQL)
        strSQL = "SELECT CompanyName FROM syscompanyinfo where CompanyID='" & strWComID & "' "
        strComName = GetScalarData(strSQL)
        Return strComName
    End Function
    ''' <summary>
    ''' Get First Company Fax Info
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetFirstCompanyFaxInfo() As String
        Dim strSQL As String
        strSQL = "SELECT CompanyInternetFax FROM syscompanyinfo where companyID='" + _CompanyID + "'"
        Return GetScalarData(strSQL).ToString
    End Function
    ''' <summary>
    ''' Return Fax Number Without hyphen and brackets
    ''' </summary>
    ''' <param name="strInput"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFaxNumbers(ByVal strInput As String) As String
        Dim strOutput As String
        strOutput = strInput.ToLower
        strOutput = strOutput.Replace(" ", "")
        strOutput = strOutput.Replace(vbCrLf, "")
        strOutput = strOutput.Replace(vbLf, "")
        strOutput = strOutput.Replace(",", "")
        strOutput = strOutput.Replace("$", "")
        strOutput = strOutput.Replace("(", "")
        strOutput = strOutput.Replace(")", "")
        strOutput = strOutput.Replace("-", "")
        Dim intCount As Integer
        Dim strTemp As String = ""
        For intCount = 1 To Len(strOutput) 'Step through each character
            If IsNumeric(Mid(strOutput, intCount, 1)) Then 'if a number
                strTemp = strTemp & Mid(strOutput, intCount, 1) 'add to temp
            End If
        Next intCount
        strOutput = strTemp
        Return strOutput
    End Function
    ''' <summary>
    ''' Populate objects of CompanyInfo
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetCompanyInfo()
        Dim strSQL, strWComID As String
        strSQL = "SELECT WarehouseCompanyID FROM syswarehouses where WarehouseCode='" & HttpContext.Current.Session("UserWarehouse") & "' "
        strWComID = GetScalarData(strSQL).ToString()
        Dim drObj As OdbcDataReader
        strSQL = "SELECT CompanyName, CompanyPOPhone FROM syscompanyinfo where CompanyID='" & strWComID & "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _CompanyName = drObj.Item("CompanyName").ToString
            _CompanyPOPhone = drObj.Item("CompanyPOPhone").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Get Company Base Currency Code
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetCompanyCurrency() As String
        Dim strSQL, strWComID As String
        strSQL = "SELECT WarehouseCompanyID FROM syswarehouses where WarehouseCode='" & HttpContext.Current.Session("UserWarehouse") & "' "
        strWComID = GetScalarData(strSQL).ToString()
        strSQL = "SELECT CompanyBasCur FROM syscompanyinfo where CompanyID='" & strWComID & "'"
        Return GetScalarData(strSQL).ToString
    End Function
    ''' <summary>
    ''' Get max Company ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funMaxCompanyID() As String
        Dim strSQL, strMaxID As String
        strSQL = "SELECT case isnull(max(CompanyID)+1) when 0 then max(CompanyID)+1 when 1 then 1 end  FROM syscompanyinfo"
        strMaxID = GetScalarData(strSQL)
        Return strMaxID
    End Function
    ''' <summary>
    ''' Populate Company Name
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub subPopulateCompany(ByVal ddl As DropDownList)
        Dim strSQL As String
        strSQL = "SELECT companyID,companyName FROM sysCompanyInfo "
        strSQL += "order by companyName asc "
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "companyName"
        ddl.DataValueField = "companyID"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.litComapnyName
        ddl.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Update Base Currency 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateBaseCurrency() As Boolean
        Dim strSQL As String
        strSQL = "Update syscompanyinfo set CompanyBasCur='" & _CompanyBasCur & "'"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Get base Currency Count
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetBaseCurrencyCount() As String
        Dim strSQL, strCurrency As String
        strSQL = "SELECT count(CompanyBasCur) FROM syscompanyinfo"
        strCurrency = GetScalarData(strSQL).ToString
        Return strCurrency
    End Function
    ''' <summary>
    ''' Get base Currency 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetBaseCurrency() As String
        Dim strSQL, strCurrency As String
        strSQL = "SELECT CompanyBasCur FROM syscompanyinfo"
        strCurrency = GetScalarData(strSQL)
        Return strCurrency
    End Function
    ''' <summary>
    ''' Get First Company Code
    ''' </summary>
    ''' <param name="strWhsCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetCompanyID(ByVal strWhsCode As String) As String
        Dim strSQL, strComID As String
        strSQL = "SELECT WarehouseCompanyID FROM syswarehouses "
        If strWhsCode <> "" Then
            strSQL += " where WarehouseCode='" & strWhsCode & "'"
        Else
            strSQL += " where WarehouseCode='" & HttpContext.Current.Session("UserWarehouse") & "'"
        End If
        strComID = GetScalarData(strSQL).ToString()
        Return strComID
    End Function
    ''' <summary>
    ''' Get Company Logo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetCompanyLogo() As String
        Dim strSQL, strWComID, strLogo As String
        strSQL = "SELECT WarehouseCompanyID FROM syswarehouses where WarehouseCode='" & HttpContext.Current.Session("UserWarehouse") & "' "
        strWComID = GetScalarData(strSQL)
        strSQL = "SELECT CompanyLogoPath FROM syscompanyinfo where CompanyID='" & strWComID & "' "
        strLogo = Convert.ToString(GetScalarData(strSQL))
        Return strLogo
    End Function
    Public Sub GetCompanyAddress()
        Dim strSQL, strWComID As String
        strSQL = "SELECT WarehouseCompanyID FROM syswarehouses where WarehouseCode='" & HttpContext.Current.Session("UserWarehouse") & "' "
        strWComID = GetScalarData(strSQL).ToString()
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  CompanyID,CompanyName,CompanyAddressLine1, CompanyAddressLine2, CompanyCity, CompanyState, CompanyCountry, CompanyPostalCode,CompanySalesRepRestricted,CompanyPOPhone FROM syscompanyinfo where CompanyID='" + strWComID + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _CompanyID = drObj.Item("CompanyID").ToString
            _CompanyName = drObj.Item("CompanyName").ToString
            _CompanyAddressLine1 = drObj.Item("CompanyAddressLine1").ToString
            _CompanyAddressLine2 = drObj.Item("CompanyAddressLine2").ToString
            _CompanyCity = drObj.Item("CompanyCity").ToString
            _CompanyState = drObj.Item("CompanyState").ToString
            _CompanyCountry = drObj.Item("CompanyCountry").ToString
            _CompanyPostalCode = drObj.Item("CompanyPostalCode").ToString
            _CompanySalesRepRestricted = drObj.Item("CompanySalesRepRestricted").ToString
            _CompanyPOPhone = drObj.Item("CompanyPOPhone").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Get First Company ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetSingleCompID() As String
        Dim strSQL, strWComID As String
        strSQL = "SELECT WarehouseCompanyID FROM syswarehouses where WarehouseCode='" & HttpContext.Current.Session("UserWarehouse") & "' "
        strWComID = GetScalarData(strSQL)
        'strSQL = "SELECT CompanyName FROM syscompanyinfo where CompanyID='" & strWComID & "' "
        'strComName = GetScalarData(strSQL)
        Return strWComID
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports System.Web.HttpContext
Imports clsCommon
Public Class clsReceiving
	Inherits clsDataClass
	Private _recID, _recDateTime, _recPOID, _recByUserID, _recAtWhsCode As String
	Public Property RecID() As String
		Get
			Return _recID
		End Get
		Set(ByVal value As String)
            _recID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property RecDateTime() As String
		Get
			Return _recDateTime
		End Get
		Set(ByVal value As String)
            _recDateTime = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property RecPOID() As String
		Get
			Return _recPOID
		End Get
		Set(ByVal value As String)
            _recPOID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property RecByUserID() As String
		Get
			Return _recByUserID
		End Get
		Set(ByVal value As String)
            _recByUserID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property RecAtWhsCode() As String
		Get
			Return _recAtWhsCode
		End Get
		Set(ByVal value As String)
            _recAtWhsCode = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
	' Insert Receiving
	Public Function insertReceiving() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO receiving( recDateTime, recPOID, recByUserID, recAtWhsCode) VALUES('"
		strSQL += _recDateTime + "','"
		strSQL += _recPOID + "','"
		strSQL += _recByUserID + "','"
		strSQL += _recAtWhsCode + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
	' Update Receiving
	Public Function updateReceiving() As Boolean
		Dim strSQL As String
        strSQL = "UPDATE receiving set "
		strSQL += "recDateTime='" + _recDateTime + "', "
		strSQL += "recPOID='" + _recPOID + "', "
		strSQL += "recByUserID='" + _recByUserID + "', "
		strSQL += "recAtWhsCode='" + _recAtWhsCode + "'"
		strSQL += " where recID='" + _recID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
	' Populate objects of Receiving
	Public Sub getReceivingInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  recDateTime, recPOID, recByUserID, recAtWhsCode FROM receiving where recID='" + _recID + "' "
		drObj = GetDataReader(strSql)
		While drObj.Read
			_recDateTime = drObj.Item("recDateTime").ToString
			_recPOID = drObj.Item("recPOID").ToString
			_recByUserID = drObj.Item("recByUserID").ToString
			_recAtWhsCode = drObj.Item("recAtWhsCode").ToString
		End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ' Fill Grid 
    Public Function funFillGrid(ByVal StatusData As String, ByVal WhsCode As String) As String
        Dim strSQL As String
        strSQL = "SELECT distinct recID, recPOID, v.vendorID, v.vendorName, poStatus, DATE_FORMAT(recDateTime,'%Y-%m-%d') as recDateTime FROM receiving r inner join purchaseorders po on po.poID=r.recPOID inner join vendor v on po.poVendorID=v.vendorID inner join purchaseorderitems i on po.poID = i.poID and i.poItemShpToWhsCode=r.recAtWhsCode inner join products p on p.productID=i.poItemPrdId where poStatus='R' "
        If WhsCode <> "ALL" Then
            strSQL += " and recAtWhsCode like '" + WhsCode + "%' "
        End If
        '
        Dim nDOW As Int16
        nDOW = Now.DayOfWeek
        Dim nFirst As Int16
        Dim nLast As Int16

        nFirst = -nDOW
        nLast = nFirst + 6
        If StatusData = "T" Then
            strSQL += " and recDateTime like '" & Now.ToString("yyyy-MM-dd") & "%' "
        ElseIf StatusData = "C" Then
            strSQL += " and (recDateTime >= '" & Now.AddDays(nFirst).ToString("yyyy-MM-dd") & " 00:00:00' "
            strSQL += " and recDateTime <= '" & Now.AddDays(nLast).ToString("yyyy-MM-dd") & " 23:59:59') "
        ElseIf StatusData = "N" Then
            If nDOW = 0 Then
                nFirst = nFirst + 7
            Else
                nFirst = nFirst + 7
            End If
            nLast = nFirst + 6
            strSQL += " and (recDateTime >= '" & Now.AddDays(nFirst).ToString("yyyy-MM-dd") & " 00:00:00' "
            strSQL += " and recDateTime <= '" & Now.AddDays(nLast).ToString("yyyy-MM-dd") & " 23:59:59') "
        End If
        strSQL += " order by recID desc "
        Return strSQL
    End Function
    ' Fill Grid 
    Public Function funFillGrid(ByVal StatusData As String, ByVal SearchData As String, ByVal WhsCode As String) As String
        Dim strSQL As String
        SearchData = clsCommon.funRemove(SearchData)
        strSQL = "SELECT distinct recID, recPOID, v.vendorID, v.vendorName, poStatus, DATE_FORMAT(recDateTime,'%Y-%m-%d') as recDateTime  FROM receiving r inner join purchaseorders po on po.poID=r.recPOID inner join vendor v on po.poVendorID=v.vendorID inner join purchaseorderitems i on po.poID = i.poID and i.poItemShpToWhsCode=r.recAtWhsCode inner join products p on p.productID=i.poItemPrdId where poStatus='R' "
        If WhsCode <> "ALL" Then
            strSQL += " and recAtWhsCode like '" + WhsCode + "%' "
        End If
        '
        Dim nDOW As Int16
        nDOW = Now.DayOfWeek
        Dim nFirst As Int16
        Dim nLast As Int16

        nFirst = -nDOW
        nLast = nFirst + 6
        If StatusData = "PO" And SearchData <> "" Then
            strSQL += " and po.poID = '" + SearchData + "' "
        ElseIf StatusData = "RN" And SearchData <> "" Then
            strSQL += " and recID = '" + SearchData + "' "
        ElseIf StatusData = "VI" And SearchData <> "" Then
            strSQL += " and v.vendorID = '" + SearchData + "' "
        ElseIf StatusData = "UC" And SearchData <> "" Then
            strSQL += " and prdUPCCode = '" + SearchData + "' "
        End If
        If StatusData = "T" Then
            strSQL += " and recDateTime like '" & Now.ToString("yyyy-MM-dd") & "%' "
        ElseIf StatusData = "C" Then
            strSQL += " and recDateTime >= '" & Now.AddDays(nFirst).ToString("yyyy-MM-dd") & " 00:00:00' "
            strSQL += " and recDateTime <= '" & Now.AddDays(nLast).ToString("yyyy-MM-dd") & " 23:59:59' "
        ElseIf StatusData = "N" Then
            If nDOW = 0 Then
                nFirst = nFirst + 7
            Else
                nFirst = nFirst + 6
            End If
            nLast = nFirst + 6
            strSQL += " and recDateTime >= '" & Now.AddDays(nFirst).ToString("yyyy-MM-dd") & " 00:00:00' "
            strSQL += " and recDateTime <= '" & Now.AddDays(nLast).ToString("yyyy-MM-dd") & " 23:59:59' "
        End If

        strSQL += " order by recDateTime asc "
        Return strSQL
    End Function
    ' Search by PO
    Public Function funCheckRecevingForPO() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM receiving r inner join purchaseorders p on p.poID=r.recPOID inner join purchaseorderitems i on p.poID = i.poID and i.poItemShpToWhsCode=r.recAtWhsCode where recPOID='" & _recPOID & "' and poStatus='R' "
        If _recAtWhsCode <> "ALL" Then
            strSQL += " and recAtWhsCode like '" + _recAtWhsCode + "%' "
        End If
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ' Search by Receipt No
    Public Function funCheckRecevingForReceiptNo() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM receiving r inner join purchaseorders p on p.poID=r.recPOID inner join purchaseorderitems i on p.poID = i.poID and i.poItemShpToWhsCode=r.recAtWhsCode where recID='" & _recID & "' and poStatus='R' "
        If _recAtWhsCode <> "ALL" Then
            strSQL += " and recAtWhsCode like '" + _recAtWhsCode + "%' "
        End If
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ' Search by Vendor
    Public Function funCheckRecevingForVendor(ByVal SearchData As String) As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM receiving r inner join purchaseorders p on p.poID=r.recPOID inner join purchaseorderitems i on p.poID = i.poID and i.poItemShpToWhsCode=r.recAtWhsCode where p.poVendorID='" & SearchData & "' and poStatus='R' "
        If _recAtWhsCode <> "ALL" Then
            strSQL += " and recAtWhsCode like '" + _recAtWhsCode + "%' "
        End If
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ' Search by UPC Code
    Public Function funCheckRecevingForUPCCode(ByVal SearchData As String) As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM receiving r inner join purchaseorders po on po.poID=r.recPOID inner join purchaseorderitems i on po.poID = i.poID and i.poItemShpToWhsCode=r.recAtWhsCode inner join products p on p.productID=i.poItemPrdId where p.prdUPCCode='" & SearchData & "' and poStatus='R' "
        If _recAtWhsCode <> "ALL" Then
            strSQL += " and recAtWhsCode like '" + _recAtWhsCode + "%' "
        End If
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ' Search by Today/This/Next Week/All
    Public Function funCheckReceving(ByVal SearchData As String) As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM receiving r inner join purchaseorders po on po.poID=r.recPOID inner join purchaseorderitems i on po.poID = i.poID and i.poItemShpToWhsCode=r.recAtWhsCode inner join products p on p.productID=i.poItemPrdId where poStatus='R' "
        If _recAtWhsCode <> "ALL" Then
            strSQL += " and recAtWhsCode like '" + _recAtWhsCode + "%' "
        End If
        Dim nDOW As Int16
        nDOW = Now.DayOfWeek
        Dim nFirst As Int16
        Dim nLast As Int16

        nFirst = -nDOW
        nLast = nFirst + 6
        If SearchData = "T" Then
            strSQL += " and recDateTime like '" & Now.ToString("yyyy-MM-dd") & "%' "
        ElseIf SearchData = "C" Then
            strSQL += " and (recDateTime >= '" & Now.AddDays(nFirst).ToString("yyyy-MM-dd") & " 00:00:00' "
            strSQL += " and recDateTime <= '" & Now.AddDays(nLast).ToString("yyyy-MM-dd") & " 23:59:59') "
        ElseIf SearchData = "N" Then
            If nDOW = 0 Then
                nFirst = nFirst + 7
            Else
                nFirst = nFirst + 7
            End If
            nLast = nFirst + 6
            strSQL += " and (recDateTime >= '" & Now.AddDays(nFirst).ToString("yyyy-MM-dd") & " 00:00:00' "
            strSQL += " and recDateTime <= '" & Now.AddDays(nLast).ToString("yyyy-MM-dd") & " 23:59:59') "
        End If
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ' Fill Grid 
    Public Function funFillGridForPOItems() As String
        Dim strSQL As String
        strSQL = "SELECT i.poItems, i.poID, i.poItemPrdId, i.poQty, i.poRcvdQty, i.poUnitPrice, i.poItemShpToWhsCode, i.poItemRcvDateTime, i.prodIDDesc,  p.prdIntID, p.prdExtID, p.prdUPCCode, prdName "
        strSQL += " FROM purchaseorderitems i inner join purchaseorders po on po.poID=i.poID inner join products p on i.poItemPrdId=p.productID inner join receiving r on r.recPOID=po.poID and r.recAtWhsCode=i.poItemShpToWhsCode where i.poItemShpToWhsCode='" & _recAtWhsCode & "' and r.recPOID='" & _recPOID & "' and r.recid='" & _recID & "' order by r.recID desc "
        Return strSQL
    End Function
    ' Get Receiving Date
    Public Function getReceivingDate(ByVal POID As String) As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT recDateTime FROM receiving where recPOID='" & POID & "' "
        strData = GetScalarData(strSQL)
        Return strData
    End Function
    ' Get Receiving Date
    Public Function getReceivingDate(ByVal POID As String, ByVal WHS As String) As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT recDateTime FROM receiving where recPOID='" & POID & "' and recAtWhsCode='" & WHS & "' "
        strData = GetScalarData(strSQL)
        Return strData
    End Function
    'Check Quantity Update Status in purchaseorderitems table
    Public Function funCheckQuantityUpdateStatus() As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT count(*) FROM purchaseorderitems i inner join purchaseorders po on po.poID=i.poID inner join products p on i.poItemPrdId=p.productID inner join receiving r on r.recPOID=po.poID and r.recAtWhsCode=i.poItemShpToWhsCode where i.poItemRcvDateTime is not null and  r.recid='" & _recID & "' order by r.recID desc "
        strData = GetScalarData(strSQL)
        Return strData
    End Function
    ' Check Receving For PO Exist
    Public Function funCheckRecevingForPOExist(ByVal sPoID As String) As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM receiving where recPOID='" & sPoID & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ' Get Receving ID For PO
    Public Function funGetRecevingIDForPO(ByVal sPoID As String) As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT recID FROM receiving where recPOID='" & sPoID & "' "
        strData = GetScalarData(strSQL)
        Return strData
    End Function
    Public Function funInTransit(ByVal strprdiD As String, Optional ByVal strWhr As String = "") As String
        Dim objDataClass As New clsDataClass
        Dim strSql, strSqlRcvdQty, strVendor As String
        strSql = "select sum(poQty) as poQty from purchaseorderitems as pi "
        strSql += " Inner join purchaseorders on purchaseorders.poID=pi.poID "
        strSql += " where poItemPrdId='" & strprdiD & "' and poStatus='R'"
        If strWhr <> "" Then
            strSql += " and poItemShpToWhsCode='" & strWhr & "'"
        End If
        strSql += " Group by pi.poItemPrdId"

        strSqlRcvdQty = "select sum(poRcvdQty) as poRcvdQty from purchaseorderitems as pi "
        strSqlRcvdQty += " Inner join purchaseorders on purchaseorders.poID=pi.poID "
        strSqlRcvdQty += " where poItemPrdId='" & strprdiD & "' and poStatus='R'"
        If strWhr <> "" Then
            strSqlRcvdQty += " and poItemShpToWhsCode='" & strWhr & "'"
        End If
        strSqlRcvdQty += " Group by pi.poItemPrdId"

        strVendor = Convert.ToString(objDataClass.GetScalarData(strSql) - objDataClass.GetScalarData(strSqlRcvdQty))
        strVendor = IIf(strVendor = "", 0, strVendor)
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return strVendor
    End Function
    Public Function funInvFillGrid(ByVal strPrdID As String, Optional ByVal strWhr As String = "") As String
        Dim strSQL As String
        strSQL = "select pi.poID,max(poQty) as poQty,poUnitPrice,DATE_FORMAT(recDateTime,'%Y-%m-%d') as recDateTime from purchaseorderitems as pi"
        strSQL += " Inner join purchaseorders on purchaseorders.poID=pi.poID "
        strSQL += " Inner join receiving on receiving.recPOID=pi.poID "
        strSQL += " where poItemPrdId='" & strPrdID & "' and poStatus='R'  "
        If strWhr <> "" Then
            strSQL += " and poItemShpToWhsCode='" & strWhr & "'"
        End If
        strSQL += " Group by pi.poID"
        Return strSQL
    End Function
    ' Count Receving For PO Exist
    Public Function funCountRecevingForPOExist(ByVal sPoID As String) As Int16
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM receiving where recPOID='" & sPoID & "' "
        Dim intCount As Int16 = 0
        intCount = GetScalarData(strSQL)
        Return intCount
    End Function
End Class


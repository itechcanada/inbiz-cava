Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon

Public Class clsPartnerActivityLog
    Private _PartnerActivityId, _PartnerId, _FollowUpDate, _LogText As String
    Private _ActivityCreatedDate, _ActivityCreatedBy, _ActivityLastModified, _ActivityLastModifyBy, _ActivityStatus As String

    Public Property PartnerActivityId() As String
        Get
            Return _PartnerActivityId
        End Get
        Set(ByVal value As String)
            _PartnerActivityId = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerId() As String
        Get
            Return _PartnerId
        End Get
        Set(ByVal value As String)
            _PartnerId = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property FollowUpDate() As String
        Get
            Return _FollowUpDate
        End Get
        Set(ByVal value As String)
            _FollowUpDate = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property LogText() As String
        Get
            Return _LogText
        End Get
        Set(ByVal value As String)
            _LogText = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ActivityCreatedDate() As String
        Get
            Return _ActivityCreatedDate
        End Get
        Set(ByVal value As String)
            _ActivityCreatedDate = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ActivityCreatedBy() As String
        Get
            Return _ActivityCreatedBy
        End Get
        Set(ByVal value As String)
            _ActivityCreatedBy = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ActivityLastModified() As String
        Get
            Return _ActivityLastModified
        End Get
        Set(ByVal value As String)
            _ActivityLastModified = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ActivityLastModifyBy() As String
        Get
            Return _ActivityLastModifyBy
        End Get
        Set(ByVal value As String)
            _ActivityLastModifyBy = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ActivityStatus() As String
        Get
            Return _ActivityStatus
        End Get
        Set(ByVal value As String)
            _ActivityStatus = clsCommon.funRemove(value, True)
        End Set
    End Property
    ''' <summary>
    ''' Insert Log
    ''' </summary>
    ''' <param name="activityId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function InsertLog(ByRef activityId As String) As Boolean
        Dim objSb As New StringBuilder()
        objSb.Append("INSERT INTO partneractivitylog (PartnerId, FollowupDate, LogText, ActivityCreatedDate, ActivityCreatedBy, ActivityLastModified, ActivityLastModifyBy, ActivityStatus)  VALUES(@PartnerId, '@FollowupDate', '@LogText', '@ActivityCreatedDate', @ActivityCreatedBy, '@ActivityLastModified', @ActivityLastModifyBy, '@ActivityStatus')")
        objSb.Replace("@PartnerId", Me._PartnerId)
        objSb.Replace("@FollowupDate", Me._FollowUpDate)
        objSb.Replace("@LogText", Me._LogText)
        objSb.Replace("@ActivityCreatedDate", Me._ActivityCreatedDate)
        objSb.Replace("@ActivityCreatedBy", Me._ActivityCreatedBy)
        objSb.Replace("@ActivityLastModified", Me._ActivityLastModified)
        objSb.Replace("@ActivityLastModifyBy", Me._ActivityLastModifyBy)
        objSb.Replace("@ActivityStatus", Me._ActivityStatus)
        objSb.Replace("'sysNull'", "Null")
        objSb.Replace("sysNull", "Null")

        Dim intData As Int16 = 0
        Dim objDataAccess As New clsDataClass

        intData = objDataAccess.PopulateData(objSb.ToString)
        activityId = objDataAccess.GetScalarData("Select LAST_INSERT_ID()")
        objDataAccess.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Insert Log
    ''' </summary>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function InsertLog() As Integer
        Dim objSb As New StringBuilder()
        objSb.Append("INSERT INTO partneractivitylog (PartnerId, FollowupDate, LogText, ActivityCreatedDate, ActivityCreatedBy, ActivityLastModified, ActivityLastModifyBy, ActivityStatus)  VALUES(@PartnerId, '@FollowupDate', '@LogText', '@ActivityCreatedDate', @ActivityCreatedBy, '@ActivityLastModified', @ActivityLastModifyBy, '@ActivityStatus')")
        objSb.Replace("@PartnerId", Me._PartnerId)
        objSb.Replace("@FollowupDate", Me._FollowUpDate)
        objSb.Replace("@LogText", Me._LogText)
        objSb.Replace("@ActivityCreatedDate", Me._ActivityCreatedDate)
        objSb.Replace("@ActivityCreatedBy", Me._ActivityCreatedBy)
        objSb.Replace("@ActivityLastModified", Me._ActivityLastModified)
        objSb.Replace("@ActivityLastModifyBy", Me._ActivityLastModifyBy)
        objSb.Replace("@ActivityStatus", Me._ActivityStatus)
        objSb.Replace("'sysNull'", "Null")
        objSb.Replace("sysNull", "Null")

        Dim intData As Int16 = 0
        Dim objDataAccess As New clsDataClass

        intData = objDataAccess.PopulateData(objSb.ToString)
        Dim activityId As Object = objDataAccess.GetScalarData("Select LAST_INSERT_ID()")
        objDataAccess.CloseDatabaseConnection()
        Return iTECH.Library.Utilities.BusinessUtility.GetInt(activityId)
    End Function

    ''' <summary>
    ''' Update Log
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function UpdateLog() As Boolean
        Dim objSb As New StringBuilder()
        objSb.Append("UPDATE partneractivitylog SET FollowupDate = '@FollowupDate', LogText = '@LogText', ActivityLastModifyBy = '@ActivityLastModifyBy', ActivityModifyBy = @ActivityModifyBy, ActivityStatus = '@ActivityStatus' WHERE PartnerActivityId = @PartnerActivityId;")
        objSb.Replace("@FollowupDate", _FollowUpDate)
        objSb.Replace("@LogText", _LogText)
        objSb.Replace("@ActivityLastModified", _ActivityLastModified)
        objSb.Replace("@ActivityLastModifyBy", _ActivityLastModifyBy)
        objSb.Replace("@ActivityStatus", _ActivityStatus)
        objSb.Replace("@PartnerActivityId", _PartnerActivityId)
        objSb.Replace("'sysNull'", "Null")
        objSb.Replace("sysNull", "Null")

        Dim intData As Int16 = 0
        Dim objDataAccess As New clsDataClass

        intData = objDataAccess.PopulateData(objSb.ToString)

        objDataAccess.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Delete Log
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DeleteLog() As Boolean
        Dim objSb As New StringBuilder()
        objSb.Append("DELETE FROM partneractivitylog WHERE PartnerActivityId = @PartnerActivityId;")
        objSb.Replace("@PartnerActivityId", Me._PartnerActivityId)
        objSb.Replace("'sysNull'", "Null")
        objSb.Replace("sysNull", "Null")

        Dim intData As Int16 = 0
        Dim objDataAccess As New clsDataClass

        intData = objDataAccess.PopulateData(objSb.ToString)

        objDataAccess.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Fill Grid All Activity By Partner 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetFillGridAllActivityByPartnerIdCommand() As String
        Dim objSb As New StringBuilder()
        objSb.Append("SELECT p.PartnerActivityId, p.PartnerId, p.FollowUpDate, p.LogText, p.ActivityCreatedDate, p.ActivityCreatedBy, p.ActivityLastModified, p.ActivityLastModifyBy, p.ActivityStatus, CONCAT(u.userFirstName, ' ', u.userLastName) AS UserName FROM partneractivitylog p INNER JOIN users u ON p.ActivityCreatedBy = u.UserID WHERE p.PartnerId = @PartnerId ORDER BY p.FollowUpDate DESC;")
        objSb.Replace("@PartnerId", _PartnerId)
        Return objSb.ToString
    End Function
    ''' <summary>
    ''' Activities Count By Partner
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetActivitiesCountByPartnerId() As Integer
        Dim objSb As New StringBuilder()
        objSb.Append("SELECT COUNT(*) FROM partneractivitylog p WHERE p.PartnerId = @PartnerId;")
        objSb.Replace("@PartnerId", _PartnerId)

        Dim objDataAccess As New clsDataClass

        Dim obj As Object
        obj = objDataAccess.GetScalarData(objSb.ToString)
        objDataAccess.CloseDatabaseConnection()

        If Not obj = Nothing Then
            Return Convert.ToInt32(obj)
        Else
            Return 0
        End If
    End Function
    ''' <summary>
    ''' Assing Sales Representative
    ''' </summary>
    ''' <param name="partActivityLogId"></param>
    ''' <param name="userId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AssingSalesRepresentative(ByVal partActivityLogId As String, ByVal userId As String) As Boolean
        Dim objSb As New StringBuilder()
        objSb.Append("INSERT INTO assignpartnerfollowupstosr VALUES(@PartnerActivityLogId, @UserId);")
        objSb.Replace("@PartnerActivityLogId", partActivityLogId)
        objSb.Replace("@UserId", userId)
        objSb.Replace("'sysNull'", "Null")
        objSb.Replace("sysNull", "Null")

        Dim intData As Int16 = 0
        Dim objDataAccess As New clsDataClass

        intData = objDataAccess.PopulateData(objSb.ToString)

        objDataAccess.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' All Assigned Representatives
    ''' </summary>
    ''' <param name="activityId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetAllAssignedRepresentatives(ByVal activityId As String) As String
        Dim objSb As New StringBuilder()
        objSb.Append("SELECT a.PartnerActivityLogId, a.UserId, CONCAT(u.userFirstName, ' ', u.userLastName) AS UserName FROM assignpartnerfollowupstosr a INNER JOIN users u ON a.UserId = u.UserID WHERE a.PartnerActivityLogId = @PartnerActivityLogId;")
        objSb.Replace("@PartnerActivityLogId", activityId)
        Dim objDataClass As New clsDataClass()
        Dim retVal As String = String.Empty
        Dim dr As OdbcDataReader = objDataClass.GetDataReader(objSb.ToString)
        While dr.Read()
            retVal = retVal + dr.GetString(2) + ", "
        End While
        Return retVal.Trim(","c, " "c)
    End Function

End Class

Imports Microsoft.VisualBasic
Imports System.Xml

Public Class clsXML

    'Get Node Text
    Public Shared Function GetNodeText(ByVal XML As String, ByVal Node As String) As String
        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(XML)

        Dim xmlNode As XmlNode
        xmlNode = xmlDoc.SelectSingleNode(Node)

        Dim Value As String = xmlNode.InnerText
        Return Value
    End Function

    'Get Node List
    Public Shared Function GetNodeList(ByVal XML As String, ByVal Node As String) As String()
        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(XML)

        Dim xmlNodeList As XmlNodeList
        xmlNodeList = xmlDoc.SelectNodes(Node)
        Dim xmlNode As XmlNode
        Dim i As Integer = 0

        Dim Value(xmlNodeList.Count - 1) As String
        For Each xmlNode In xmlNodeList
            Value(i) = xmlNode.ChildNodes.Item(0).InnerText
            i += 1
        Next
        Return Value
    End Function

    'Get Node Count
    Public Shared Function GetNodeCount(ByVal XML As String, ByVal Node As String) As Integer
        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(XML)

        Dim xmlNodeList As XmlNodeList
        xmlNodeList = xmlDoc.SelectNodes(Node)

        Dim Count As Integer = xmlNodeList.Count
        Return Count
    End Function

    'Get Node Attribute
    Public Shared Function GetNodeAttribute(ByVal XML As String, ByVal Node As String, ByVal Item As Integer, ByVal Attribute As String) As String
        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(XML)

        Dim xmlNodeList As XmlNodeList
        xmlNodeList = xmlDoc.SelectNodes(Node)

        Dim xmlNode As XmlNode = xmlNodeList.Item(Item)
        If Not xmlNode.Attributes(Attribute) Is Nothing Then
            Dim Value As String = xmlNode.Attributes(Attribute).Value
            Return Value
        End If
        Return ""
    End Function

    'Get Node Child
    Public Shared Function GetNodeChild(ByVal XML As String, ByVal Node As String, ByVal Item As Integer, ByVal Child As String) As String
        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(XML)

        Dim xmlNodeList As XmlNodeList
        xmlNodeList = xmlDoc.SelectNodes(Node)

        Dim xmlNode As XmlNode = xmlNodeList.Item(Item)
        If Not xmlNode.Item(Child) Is Nothing Then
            Dim Value As String = xmlNode.Item(Child).InnerText
            Return Value
        End If
        Return ""
    End Function
End Class

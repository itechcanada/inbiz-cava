Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports iTECH.InbizERP.BusinessLogic

Public Class clsOrders
	Inherits clsDataClass
    Private _ordID, _ordType, _ordCustType, _ordCustID, _ordCustName, _ordCustFax, _ordCreatedOn, _ordCreatedFromIP, _ordStatus, _ordVerified, _ordVerifiedBy, _ordSalesRepID, _ordSaleWeb, _ordComment, _ordShpDate, _ordShpBlankPref, _ordShpWhsCode, _ordLastUpdatedOn, _ordLastUpdateBy, _ordShpTrackNo, _ordShpCost, _ordShpCode, _ordCurrencyCode, _ordCurrencyExRate, _ordCustPO, _qutExpDate, _ordCompanyID, _ordShippingTerms, _ordNetTerms, _ordCreatedBy, _orderTypeCommission, _PartnerLang, _custAcronym, _orderRejectReason, _InvRefNo, _invDate As String
    Private _Amount As String
	Public Property OrdID() As String
		Get
			Return _ordID
		End Get
		Set(ByVal value As String)
            _ordID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdType() As String
		Get
			Return _ordType
		End Get
		Set(ByVal value As String)
            _ordType = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdCustType() As String
		Get
			Return _ordCustType
		End Get
		Set(ByVal value As String)
            _ordCustType = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdCustID() As String
		Get
			Return _ordCustID
		End Get
		Set(ByVal value As String)
            _ordCustID = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property OrdCustName() As String
        Get
            Return _ordCustName
        End Get
        Set(ByVal value As String)
            _ordCustName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property OrdCustFax() As String
        Get
            Return _ordCustFax
        End Get
        Set(ByVal value As String)
            _ordCustFax = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Property OrdCreatedOn() As String
		Get
			Return _ordCreatedOn
		End Get
		Set(ByVal value As String)
            _ordCreatedOn = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdCreatedFromIP() As String
		Get
			Return _ordCreatedFromIP
		End Get
		Set(ByVal value As String)
            _ordCreatedFromIP = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdStatus() As String
		Get
			Return _ordStatus
		End Get
		Set(ByVal value As String)
            _ordStatus = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdVerified() As String
		Get
			Return _ordVerified
		End Get
		Set(ByVal value As String)
            _ordVerified = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdVerifiedBy() As String
		Get
			Return _ordVerifiedBy
		End Get
		Set(ByVal value As String)
            _ordVerifiedBy = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdSalesRepID() As String
		Get
			Return _ordSalesRepID
		End Get
		Set(ByVal value As String)
            _ordSalesRepID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdSaleWeb() As String
		Get
			Return _ordSaleWeb
		End Get
		Set(ByVal value As String)
            _ordSaleWeb = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdComment() As String
		Get
			Return _ordComment
		End Get
		Set(ByVal value As String)
            _ordComment = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdShpDate() As String
		Get
			Return _ordShpDate
		End Get
		Set(ByVal value As String)
            _ordShpDate = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdShpBlankPref() As String
		Get
			Return _ordShpBlankPref
		End Get
		Set(ByVal value As String)
            _ordShpBlankPref = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdShpWhsCode() As String
		Get
			Return _ordShpWhsCode
		End Get
		Set(ByVal value As String)
            _ordShpWhsCode = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdLastUpdatedOn() As String
		Get
			Return _ordLastUpdatedOn
		End Get
		Set(ByVal value As String)
            _ordLastUpdatedOn = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdLastUpdateBy() As String
		Get
			Return _ordLastUpdateBy
		End Get
		Set(ByVal value As String)
            _ordLastUpdateBy = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdShpTrackNo() As String
		Get
			Return _ordShpTrackNo
		End Get
		Set(ByVal value As String)
            _ordShpTrackNo = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdShpCost() As String
		Get
			Return _ordShpCost
		End Get
		Set(ByVal value As String)
            _ordShpCost = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdShpCode() As String
		Get
			Return _ordShpCode
		End Get
		Set(ByVal value As String)
            _ordShpCode = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdCurrencyCode() As String
		Get
			Return _ordCurrencyCode
		End Get
		Set(ByVal value As String)
            _ordCurrencyCode = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property OrdCurrencyExRate() As String
		Get
			Return _ordCurrencyExRate
		End Get
		Set(ByVal value As String)
            _ordCurrencyExRate = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property OrdCustPO() As String
        Get
            Return _ordCustPO
        End Get
        Set(ByVal value As String)
            _ordCustPO = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Amount() As String
        Get
            Return _Amount
        End Get
        Set(ByVal value As String)
            _Amount = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property QutExpDate() As String
        Get
            Return _qutExpDate
        End Get
        Set(ByVal value As String)
            _qutExpDate = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ordCompanyID() As String
        Get
            Return _ordCompanyID
        End Get
        Set(ByVal value As String)
            _ordCompanyID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ordShippingTerms() As String
        Get
            Return _ordShippingTerms
        End Get
        Set(ByVal value As String)
            _ordShippingTerms = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ordNetTerms() As String
        Get
            Return _ordNetTerms
        End Get
        Set(ByVal value As String)
            _ordNetTerms = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ordCreatedBy() As String
        Get
            Return _ordCreatedBy
        End Get
        Set(ByVal value As String)
            _ordCreatedBy = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ordercommission() As String
        Get
            Return _orderTypeCommission
        End Get
        Set(ByVal value As String)
            _orderTypeCommission = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustomerLang() As String
        Get
            Return _PartnerLang
        End Get
        Set(ByVal value As String)
            _PartnerLang = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustAcronym() As String
        Get
            Return _custAcronym
        End Get
        Set(ByVal value As String)
            _custAcronym = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property orderRejectReason() As String
        Get
            Return _orderRejectReason
        End Get
        Set(ByVal value As String)
            _orderRejectReason = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvRefNo() As String
        Get
            Return _InvRefNo
        End Get
        Set(ByVal value As String)
            _InvRefNo = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property invDate() As String
        Get
            Return _invDate
        End Get
        Set(ByVal value As String)
            _invDate = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Insert Orders
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertOrders() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO orders( ordType, ordCustType, ordCustID, ordCreatedOn, ordCreatedFromIP, ordStatus, ordVerified, ordVerifiedBy, ordSalesRepID, ordSaleWeb, ordComment, ordShpDate, ordShpBlankPref, ordShpWhsCode, ordLastUpdatedOn, ordLastUpdateBy, ordShpTrackNo, ordShpCost, ordShpCode, ordCurrencyCode, ordCurrencyExRate, ordCustPO, qutExpDate,ordCompanyID,ordShippingTerms,ordNetTerms,ordCreatedBy,orderTypeCommission,orderRejectReason,InvRefNo, invDate) VALUES('"
		strSQL += _ordType + "','"
		strSQL += _ordCustType + "','"
		strSQL += _ordCustID + "','"
		strSQL += _ordCreatedOn + "','"
		strSQL += _ordCreatedFromIP + "','"
		strSQL += _ordStatus + "','"
		strSQL += _ordVerified + "','"
		strSQL += _ordVerifiedBy + "','"
		strSQL += _ordSalesRepID + "','"
		strSQL += _ordSaleWeb + "','"
		strSQL += _ordComment + "','"
		strSQL += _ordShpDate + "','"
		strSQL += _ordShpBlankPref + "','"
		strSQL += _ordShpWhsCode + "','"
		strSQL += _ordLastUpdatedOn + "','"
		strSQL += _ordLastUpdateBy + "','"
		strSQL += _ordShpTrackNo + "','"
		strSQL += _ordShpCost + "','"
		strSQL += _ordShpCode + "','"
        strSQL += _ordCurrencyCode + "','"
        strSQL += _ordCurrencyExRate + "','"
        strSQL += _ordCustPO + "','"
        strSQL += _qutExpDate + "','"
        strSQL += _ordCompanyID + "','"
        strSQL += _ordShippingTerms + "','"
        strSQL += _ordNetTerms + "','"
        strSQL += _ordCreatedBy + "','"
        strSQL += _orderTypeCommission + "','"
        strSQL += _orderRejectReason + "','"
        strSQL += _InvRefNo + "','"
        strSQL += _invDate + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
    End Function
    ''' <summary>
    ''' Update Orders
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateOrders() As Boolean
		Dim strSQL As String
        strSQL = "UPDATE orders set "
		strSQL += "ordType='" + _ordType + "', "
		strSQL += "ordCustType='" + _ordCustType + "', "
		strSQL += "ordCustID='" + _ordCustID + "', "
		strSQL += "ordCreatedFromIP='" + _ordCreatedFromIP + "', "
		strSQL += "ordStatus='" + _ordStatus + "', "
		strSQL += "ordSaleWeb='" + _ordSaleWeb + "', "
		strSQL += "ordComment='" + _ordComment + "', "
		strSQL += "ordShpDate='" + _ordShpDate + "', "
		strSQL += "ordShpBlankPref='" + _ordShpBlankPref + "', "
		strSQL += "ordShpWhsCode='" + _ordShpWhsCode + "', "
		strSQL += "ordLastUpdatedOn='" + _ordLastUpdatedOn + "', "
		strSQL += "ordLastUpdateBy='" + _ordLastUpdateBy + "', "
		strSQL += "ordShpTrackNo='" + _ordShpTrackNo + "', "
		strSQL += "ordShpCost='" + _ordShpCost + "', "
		strSQL += "ordShpCode='" + _ordShpCode + "', "
        strSQL += "ordCurrencyCode='" + _ordCurrencyCode + "', "
        strSQL += "ordCurrencyExRate='" + _ordCurrencyExRate + "', "
        strSQL += "ordCustPO='" + _ordCustPO + "', "
        strSQL += "qutExpDate='" + _qutExpDate + "', "
        strSQL += "ordCompanyID='" + _ordCompanyID + "',"
        strSQL += "ordShippingTerms='" + _ordShippingTerms + "', "
        strSQL += "ordNetTerms='" + _ordNetTerms + "', "
        strSQL += "orderTypeCommission='" + _orderTypeCommission + "', "
        strSQL += "orderRejectReason='" + _orderRejectReason + "',"
        strSQL += "InvRefNo='" + _InvRefNo + "', "
        strSQL += "invDate='" + _invDate + "' "
        strSQL += " where ordID='" + _ordID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
    End Function
    ''' <summary>
    ''' Populate objects of Orders
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getOrdersInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Or HttpContext.Current.Session("UserModules").ToString.Contains("JOBP") = True Or HttpContext.Current.Session("UserModules").ToString.Contains("JBPM") = True Then
            ' strSQL = "SELECT  CASE ordCustType WHEN 'R' THEN r.resellerName WHEN 'D' THEN d.distName WHEN 'E' THEN e.endclientName END as CustomerName, ordType, ordCustType, ordCustID, ordCreatedOn, ordCreatedFromIP, ordStatus, ordVerified, ordVerifiedBy, ordSalesRepID, ordSaleWeb, ordComment, ordShpDate, ordShpBlankPref, ordShpWhsCode, ordLastUpdatedOn, ordLastUpdateBy, ordShpTrackNo, ordShpCost, ordShpCode, ordCurrencyCode, ordCurrencyExRate, ordCustPO, qutExpDate,ordCompanyID FROM orders o left join reseller r on r.resellerID=o.ordCustID and o.ordCustType='R' left join distributor d on d.distID=o.ordCustID and o.ordCustType='D' left join endclient e on e.endclientID=o.ordCustID and o.ordCustType='E' where ordID='" + _ordID + "' "
            strSQL = "SELECT PartnerAcronyme,InvRefNo,invDate,orderRejectReason, PartnerLongName as CustomerName,PartnerLang, ordType,orderTypeCommission, ordCustType, ordCustID, ordCreatedOn, ordCreatedFromIP, ordStatus, ordVerified, ordVerifiedBy, ordSalesRepID, ordSaleWeb, ordComment, ordShpDate, ordShpBlankPref, ordShpWhsCode, ordLastUpdatedOn, ordLastUpdateBy, ordShpTrackNo, ordShpCost, ordShpCode, ordCurrencyCode, ordCurrencyExRate, ordCustPO, qutExpDate,ordCompanyID,ordShippingTerms,ordNetTerms,ordCreatedBy FROM orders o left join partners  on partners. PartnerID =o.ordCustID where ordID='" + _ordID + "'"
        Else
            strSQL = "SELECT PartnerAcronyme,InvRefNo,invDate,orderRejectReason, PartnerLongName as CustomerName,PartnerLang, ordType,orderTypeCommission, ordCustType, ordCustID, ordCreatedOn, ordCreatedFromIP, ordStatus, ordVerified, ordVerifiedBy, ordSalesRepID, ordSaleWeb, ordComment, ordShpDate, ordShpBlankPref, ordShpWhsCode, ordLastUpdatedOn, ordLastUpdateBy, ordShpTrackNo, ordShpCost, ordShpCode, ordCurrencyCode, ordCurrencyExRate, ordCustPO, qutExpDate,ordCompanyID,ordShippingTerms,ordNetTerms,ordCreatedBy FROM orders o Inner join salesrepcustomer s on s.CustomerID=o.ordCustID and s.CustomerType=o.ordCustType and s.UserID='" & HttpContext.Current.Session("UserID") & "' left join partners  on partners. PartnerID =o.ordCustID where ordID='" + _ordID + "' "
        End If

        drObj = GetDataReader(strSQL)
		While drObj.Read
			_ordType = drObj.Item("ordType").ToString
			_ordCustType = drObj.Item("ordCustType").ToString
            _ordCustID = drObj.Item("ordCustID").ToString
            _ordCustName = drObj.Item("CustomerName").ToString
			_ordCreatedOn = drObj.Item("ordCreatedOn").ToString
			_ordCreatedFromIP = drObj.Item("ordCreatedFromIP").ToString
			_ordStatus = drObj.Item("ordStatus").ToString
			_ordVerified = drObj.Item("ordVerified").ToString
			_ordVerifiedBy = drObj.Item("ordVerifiedBy").ToString
			_ordSalesRepID = drObj.Item("ordSalesRepID").ToString
			_ordSaleWeb = drObj.Item("ordSaleWeb").ToString
			_ordComment = drObj.Item("ordComment").ToString
			_ordShpDate = drObj.Item("ordShpDate").ToString
			_ordShpBlankPref = drObj.Item("ordShpBlankPref").ToString
			_ordShpWhsCode = drObj.Item("ordShpWhsCode").ToString
			_ordLastUpdatedOn = drObj.Item("ordLastUpdatedOn").ToString
			_ordLastUpdateBy = drObj.Item("ordLastUpdateBy").ToString
			_ordShpTrackNo = drObj.Item("ordShpTrackNo").ToString
			_ordShpCost = drObj.Item("ordShpCost").ToString
			_ordShpCode = drObj.Item("ordShpCode").ToString
			_ordCurrencyCode = drObj.Item("ordCurrencyCode").ToString
            _ordCurrencyExRate = drObj.Item("ordCurrencyExRate").ToString
            _ordCustPO = drObj.Item("ordCustPO").ToString
            _qutExpDate = drObj.Item("qutExpDate").ToString
            _ordCompanyID = drObj.Item("ordCompanyID").ToString
            _ordShippingTerms = drObj.Item("ordShippingTerms").ToString
            _ordNetTerms = drObj.Item("ordNetTerms").ToString
            _ordCreatedBy = drObj.Item("ordCreatedBy").ToString
            _orderTypeCommission = drObj.Item("orderTypeCommission").ToString
            _PartnerLang = drObj.Item("PartnerLang").ToString
            _custAcronym = drObj.Item("PartnerAcronyme").ToString
            _orderRejectReason = drObj.Item("orderRejectReason").ToString
            _InvRefNo = drObj.Item("InvRefNo").ToString
            _invDate = drObj.Item("invDate").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate objects of Sales Orders
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getOrders()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT o.*, sum(ordProductQty * ordProductUnitPrice) as Amount,ordCompanyID FROM orders o inner join orderitems i on i.ordID=o.ordID where o.ordID='" + _ordID + "' group by o.ordID "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _ordType = drObj.Item("ordType").ToString
            _ordCustType = drObj.Item("ordCustType").ToString
            _ordCustID = drObj.Item("ordCustID").ToString
            _ordCreatedOn = drObj.Item("ordCreatedOn").ToString
            _ordCreatedFromIP = drObj.Item("ordCreatedFromIP").ToString
            _ordStatus = drObj.Item("ordStatus").ToString
            _ordVerified = drObj.Item("ordVerified").ToString
            _ordVerifiedBy = drObj.Item("ordVerifiedBy").ToString
            _ordSalesRepID = drObj.Item("ordSalesRepID").ToString
            _ordSaleWeb = drObj.Item("ordSaleWeb").ToString
            _ordComment = drObj.Item("ordComment").ToString
            _ordShpDate = drObj.Item("ordShpDate").ToString
            _ordShpBlankPref = drObj.Item("ordShpBlankPref").ToString
            _ordShpWhsCode = drObj.Item("ordShpWhsCode").ToString
            _ordLastUpdatedOn = drObj.Item("ordLastUpdatedOn").ToString
            _ordLastUpdateBy = drObj.Item("ordLastUpdateBy").ToString
            _ordShpTrackNo = drObj.Item("ordShpTrackNo").ToString
            _ordShpCost = drObj.Item("ordShpCost").ToString
            _ordShpCode = drObj.Item("ordShpCode").ToString
            _ordCurrencyCode = drObj.Item("ordCurrencyCode").ToString
            _ordCurrencyExRate = drObj.Item("ordCurrencyExRate").ToString
            _ordCustPO = drObj.Item("ordCustPO").ToString
            _qutExpDate = drObj.Item("qutExpDate").ToString
            _Amount = drObj.Item("Amount").ToString
            _ordCompanyID = drObj.Item("ordCompanyID").ToString
            _ordShippingTerms = drObj.Item("ordShippingTerms").ToString
            _ordNetTerms = drObj.Item("ordNetTerms").ToString
            _ordCreatedBy = drObj.Item("ordCreatedBy").ToString
            _orderTypeCommission = drObj.Item("orderTypeCommission").ToString
            _orderRejectReason = drObj.Item("orderRejectReason").ToString
            _InvRefNo = drObj.Item("InvRefNo").ToString
            _invDate = drObj.Item("invDate").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill grid with search criteria
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <param name="strCompanyID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGridForQuotation(ByVal StatusData As String, ByVal SearchData As String, ByVal strCompanyID As String) As String
        Dim strSQL As String
        Dim strLang As String = "en"
        strLang = HttpContext.Current.Session("Language")
        If strLang = "en-CA" Then
            strLang = "en"
        ElseIf strLang = "fr-CA" Then
            strLang = "fr"
        ElseIf strLang = "es-MX" Then
            strLang = "sp"
        Else
            strLang = "en"
        End If
        SearchData = clsCommon.funRemove(SearchData)
        strSQL = "SELECT p.productID,pq.prdWhsCode, pd.prdName, p.prdIntID, p.prdExtID, p.prdUPCCode, prdSmallDesc, prdLargeDesc, sum(pq.prdOhdQty) as prdOhdQty, p.prdEndUserSalesPrice as Price,prdTaxCode,prdDiscount,p.prdDiscountType FROM products as p inner join prddescriptions as pd on pd.id=p.productID and pd.descLang='" & strLang & "' inner join prdquantity pq on pq.prdID=p.productID "

        If strCompanyID <> "" Then
            strSQL += "and pq.prdWhsCode In (SELECT WarehouseCode FROM syswarehouses where WarehouseCompanyID= '" & strCompanyID & "') and pq.prdOhdQty!='0'"
        End If

        If StatusData = CustomerSearchFields.PartnerId And SearchData <> "" Then
            strSQL += " where p.productID = '" + SearchData + "' "
        ElseIf StatusData = CustomerSearchFields.PartnerName And SearchData <> "" Then
            strSQL += " where p.prdName like '%" + SearchData + "%' "
        ElseIf StatusData = OrderSearchFields.InvoiceNo And SearchData <> "" Then
            strSQL += " where p.prdIntID = '" + SearchData + "' "
        ElseIf StatusData = ProductSearchFields.ProductExternalID And SearchData <> "" Then
            strSQL += " where p.prdExtID = '" + SearchData + "' "
        ElseIf StatusData = ProductSearchFields.ProductBarCode And SearchData <> "" Then
            strSQL += " where p.prdUPCCode = '" + SearchData + "' "
        ElseIf StatusData = ProductSearchFields.ProductDescription And SearchData <> "" Then
            strSQL += " where "
            strSQL += " ("
            strSQL += " prdSmallDesc like '%" + SearchData + "%' or "
            strSQL += " prdLargeDesc like '%" + SearchData + "%' "
            strSQL += " )"
        End If
        strSQL += " group by p.productID order by p.prdName "
        Return strSQL
    End Function

    Public Function SearchProductAdvanceForQuotation(ByVal alpha As String, ByVal strCompanyID As String, ByVal strCustID As Integer) As String
        Dim strSQL As String
        Dim strLang As String = "en"
        strLang = HttpContext.Current.Session("Language")
        If strLang = "en-CA" Then
            strLang = "en"
        ElseIf strLang = "fr-CA" Then
            strLang = "fr"
        ElseIf strLang = "es-MX" Then
            strLang = "sp"
        Else
            strLang = "en"
        End If

        strSQL = "SELECT p.productID,pq.prdWhsCode, pd.prdName, p.prdIntID, p.prdExtID, p.prdUPCCode, prdSmallDesc, prdLargeDesc, sum(pq.prdOhdQty) as prdOhdQty, p.prdEndUserSalesPrice as Price,prdTaxCode,prdDiscount,p.prdDiscountType FROM products as p inner join prddescriptions as pd on pd.id=p.productID and pd.descLang='" & strLang & "' inner join prdquantity pq on pq.prdID=p.productID "

        If strCompanyID <> "" Then
            strSQL += "and pq.prdWhsCode In (SELECT WarehouseCode FROM syswarehouses where WarehouseCompanyID= '" & strCompanyID & "') and pq.prdOhdQty!='0'"
        End If

        Dim cusID As Integer = 0
        Integer.TryParse(strCustID, cusID)

        If alpha.Length = 1 Then
            strSQL += " And p.prdName like '" + alpha + "%' "
        ElseIf alpha = "CF" Then 'Customer Fav
            strSQL += " And p.productID IN (SELECT oi.ordProductID FROM orderitems oi INNER JOIN orders o ON o.ordID = oi.ordID AND o.ordCustID = " + cusID.ToString() + ")"
        ElseIf alpha = "MF" Then 'My Fav
            strSQL += " And p.prdIsSpecial = 1 "

        End If
        strSQL += " group by p.productID order by p.prdName "
        Return strSQL
    End Function

    ''' <summary>
    ''' Fill grid with search criteria
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGridForQuotationApproval(ByVal StatusData As String, ByVal SearchData As String) As String
        Dim strSQL As String

        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distinct o.ordID,PartnerAcronyme, PartnerLongName as CustomerName,orderTypeCommission,orderTypeDesc, sum(ordProductQty * (ordProductUnitPrice*o.ordCurrencyExRate)) as amount, DATE_FORMAT(ordCreatedOn,'%m-%d-%Y') as ordDate, ordStatus,o.ordCustPO FROM orders o left join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID "
            strSQL += " inner join products as pro on i.ordProductID = pro.productID "
            strSQL += " left join ordertype on ordertype.orderTypeID=o.orderTypeCommission"
            strSQL += " left join ordertypedtl on ordertypedtl.orderTypeID=o.orderTypeCommission"
            strSQL += " where ordStatus='N' "
        Else
            strSQL = "SELECT distinct o.ordID,PartnerAcronyme, PartnerLongName as CustomerName,orderTypeCommission,orderTypeDesc, sum(ordProductQty * (ordProductUnitPrice*o.ordCurrencyExRate)) as amount, DATE_FORMAT(ordCreatedOn,'%m-%d-%Y') as ordDate, ordStatus,o.ordCustPO FROM orders o Inner join salesrepcustomer s on s.CustomerID=o.ordCustID and s.CustomerType=o.ordCustType and s.UserID='" & HttpContext.Current.Session("UserID") & "' inner join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID"
            strSQL += " inner join products as pro on i.ordProductID = pro.productID "
            strSQL += " left join ordertype on ordertype.orderTypeID=o.orderTypeCommission"
            strSQL += " left join ordertypedtl on ordertypedtl.orderTypeID=o.orderTypeCommission"
            strSQL += " where ordStatus='N' "
        End If

        If StatusData = ProductSearchFields.ProductBarCode And SearchData <> "" Then
            strSQL += " and pro.prdUPCCode = '" + SearchData + "' "
        ElseIf StatusData = CustomerSearchFields.PartnerName And SearchData <> "" Then
            strSQL += " and pro.prdName like '%" + SearchData + "%' "
        ElseIf StatusData = OrderSearchFields.OrderNo And SearchData <> "" Then
            strSQL += " and o.ordID = '" + SearchData + "' "
        ElseIf StatusData = CustomerSearchFields.ContactName And SearchData <> "" Then
            strSQL += " and (PartnerLongName like '%" + SearchData + "%' OR PartnerAcronyme like '%" + SearchData + "%' ) "
        ElseIf StatusData = OrderSearchFields.CustomerPO And SearchData <> "" Then
            strSQL += " and o.ordCustPO like '%" + SearchData + "%' "
        ElseIf StatusData = CustomerSearchFields.Acronyme And SearchData <> "" Then
            strSQL += " and PartnerAcronyme like '%" + SearchData + "%' "
        End If
        strSQL += " group by o.ordID order by o.ordID desc, CustomerName "
        Return strSQL
    End Function

    Public Function IsValidSO(ByVal soNo As String) As DataTable
        Dim dAccess As New clsDataClass()
        Dim dt As DataTable = dAccess.GetDataTable("SELECT o.ordCustID, p.PartnerLongName AS CustName FROM orders o inner join partners p  on p.PartnerID =o.ordCustID  WHERE o.ordStatus = 'N' AND o.ordID = " & clsCommon.funRemove(soNo) & ";")
        Return dt
    End Function

    ''' <summary>
    ''' Fill Grid of All Orders
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <param name="sPoStatus"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGridForAllOrders(ByVal StatusData As String, ByVal SearchData As String, ByVal sPoStatus As String) As String
        Dim strSQL As String
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distinct o.ordID,orderRejectReason,PartnerID,PartnerAcronyme, PartnerLongName as CustomerName,orderTypeCommission,orderTypeDesc, sum(ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) as amount, DATE_FORMAT(ordCreatedOn,'%m-%d-%Y') as ordDate, ordStatus,o.ordCustPO,o.ordComment FROM orders o inner join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID "
            strSQL += " inner join products as pro on i.ordProductID = pro.productID "
        Else
            strSQL = "SELECT distinct o.ordID,orderRejectReason,PartnerID,PartnerAcronyme, PartnerLongName as CustomerName,orderTypeCommission,orderTypeDesc, sum(ordProductQty * ordProductUnitPrice * o.ordCurrencyExRate) as amount, DATE_FORMAT(ordCreatedOn,'%m-%d-%Y') as ordDate, ordStatus,o.ordCustPO,o.ordComment FROM orders o inner join salesrepcustomer s on s.CustomerID=o.ordCustID and s.CustomerType=o.ordCustType and s.UserID='" & HttpContext.Current.Session("UserID") & "' inner join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID "
            strSQL += " inner join products as pro on i.ordProductID = pro.productID "
        End If
        strSQL += " left join ordertype on ordertype.orderTypeID=o.orderTypeCommission"
        strSQL += " left join ordertypedtl on ordertypedtl.orderTypeID=o.orderTypeCommission"
        strSQL += " where 1=1"
        If HttpContext.Current.Request.QueryString("custid") <> "" Then
            strSQL += " And (PartnerID='" & HttpContext.Current.Request.QueryString("custid") & "' "
            strSQL += " OR o.ordCustID = " & HttpContext.Current.Request.QueryString("custid") & ") "
        End If
        If StatusData = ProductSearchFields.ProductBarCode And SearchData <> "" And sPoStatus <> "" Then
            strSQL += " and pro.prdUPCCode = '" + SearchData + "' and o.ordStatus='" & sPoStatus & "' "
        ElseIf StatusData = ProductSearchFields.ProductBarCode And SearchData <> "" And sPoStatus = "" Then
            strSQL += " and pro.prdUPCCode = '" + SearchData + "' "
        ElseIf StatusData = ProductSearchFields.ProductName And SearchData <> "" And sPoStatus <> "" Then
            strSQL += " and pro.prdName like '%" + SearchData + "%' and o.ordStatus='" & sPoStatus & "' "
        ElseIf StatusData = ProductSearchFields.ProductName And SearchData <> "" And sPoStatus = "" Then
            strSQL += " and pro.prdName like '%" + SearchData + "%' "
        ElseIf StatusData = OrderSearchFields.OrderNo And SearchData <> "" And sPoStatus <> "" Then
            strSQL += " and o.ordID = '" + SearchData + "' and o.ordStatus='" & sPoStatus & "' "
        ElseIf StatusData = CustomerSearchFields.ContactName And SearchData <> "" And sPoStatus <> "" Then
            strSQL += " and (PartnerLongName like '%" + SearchData + "%' OR PartnerAcronyme like '%" + SearchData + "%') and o.ordStatus='" & sPoStatus & "' "
        ElseIf StatusData = OrderSearchFields.OrderNo And SearchData <> "" And sPoStatus = "" Then
            strSQL += " and o.ordID = '" + SearchData + "' "
        ElseIf StatusData = CustomerSearchFields.ContactName And SearchData <> "" And sPoStatus = "" Then
            strSQL += " and (PartnerLongName like '%" + SearchData + "%' OR PartnerAcronyme like '%" + SearchData + "%') "
        ElseIf StatusData = OrderSearchFields.OrderNo And SearchData = "" And sPoStatus <> "" Then
            strSQL += " and o.ordStatus='" & sPoStatus & "' "
        ElseIf StatusData = CustomerSearchFields.ContactName And SearchData = "" And sPoStatus <> "" Then
            strSQL += " and o.ordStatus='" & sPoStatus & "' "
        ElseIf StatusData = "" And SearchData = "" And sPoStatus <> "" Then
            strSQL += " and o.ordStatus='" & sPoStatus & "' "
        ElseIf StatusData = OrderSearchFields.CustomerPO And SearchData <> "" And sPoStatus <> "" Then
            strSQL += " and o.ordCustPO like '%" + SearchData + "%' and o.ordStatus='" & sPoStatus & "' "
        ElseIf StatusData = OrderSearchFields.CustomerPO And SearchData <> "" And sPoStatus = "" Then
            strSQL += " and o.ordCustPO like '%" + SearchData + "%' "
        ElseIf StatusData = CustomerSearchFields.Acronyme And SearchData <> "" And sPoStatus <> "" Then
            strSQL += " and PartnerAcronyme like '%" + SearchData + "%' and o.ordStatus='" & sPoStatus & "' "
        ElseIf StatusData = CustomerSearchFields.Acronyme And SearchData <> "" And sPoStatus = "" Then
            strSQL += " and PartnerAcronyme like '%" + SearchData + "%' "

        End If
        strSQL += " group by o.ordID order by o.ordID desc, CustomerName "
        Return strSQL
    End Function
    ''' <summary>
    ''' Update Sales Order
    ''' </summary>
    ''' <param name="strReject"></param>
    ''' <remarks></remarks>
    Public Sub funUpdateOrder(Optional ByVal strReject As String = "")
        Dim strSQL As String
        strSQL = "Update orders set ordStatus='" & _ordStatus & "' "
        If strReject <> "" Then
            strSQL += " ,orderRejectReason='" & _orderRejectReason & "' "
        End If
        strSQL += " WHERE ordID='" + _ordID + "' "
        Dim obj As New clsDataClass
        obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Approve Sales Order
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub funApproveOrder()
        Dim strSQL As String
        strSQL = "Update orders set ordStatus='" & _ordStatus & "', ordCustPO='" & _ordCustPO & "',ordShippingTerms='" & _ordShippingTerms & "',ordNetTerms='" & _ordNetTerms & "' WHERE ordID='" + _ordID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Delete Sales Order
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteOrder() As String
        Dim strSQL As String
        strSQL = "DELETE FROM orders WHERE ordID='" + _ordID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Delete Sales Order Items
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub funDeleteOrderItems()
        Dim strSQL As String
        strSQL = "DELETE FROM orderitems WHERE ordID='" + _ordID + "' "
        Dim obj As New clsDataClass
        obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Delete Sales Order Item Process
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub funDeleteOrderItemProcess()
        Dim strSQL As String
        strSQL = "DELETE FROM orderitemprocess WHERE ordID='" + _ordID + "' "
        Dim obj As New clsDataClass
        obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
    End Sub

    ''' <summary>
    ''' Fill Grid for Shipping Process
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <param name="WhsCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGridForShippingProcess(ByVal StatusData As String, ByVal SearchData As String, ByVal WhsCode As String) As String
        Dim strSQL As String
        strSQL = "SELECT distinct o.ordID,PartnerAcronyme,  PartnerLongName as CustomerName, concat(userFirstName,' ',userLastName) as AgentName, ordShpTrackNo, CASE ordVerified WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as ordVerified, ordStatus, DATE_FORMAT(ordShpDate,'%m-%d-%Y') as ordShpDate FROM orders o inner join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID left join users u on u.userID=o.ordSalesRepID where (ordStatus='R' or ordStatus='Z') "
        If WhsCode <> "" And WhsCode <> "ALL" Then
            strSQL += " and o.ordShpWhsCode = '" + WhsCode + "' "
        End If
        If StatusData = OrderSearchFields.OrderNo And SearchData <> "" Then
            strSQL += " and o.ordID = '" + SearchData + "' "
        ElseIf StatusData = CustomerSearchFields.ContactName And SearchData <> "" Then
            strSQL += " and (PartnerLongName like '%" + SearchData + "%' or PartnerAcronyme like '%" + SearchData + "%'  ) "
        ElseIf StatusData = "AN" And SearchData <> "" Then
            strSQL += " and concat(userFirstName,' ',userLastName) like '%" + SearchData + "%' "
        ElseIf StatusData = "TN" And SearchData <> "" Then
            strSQL += " and ordShpTrackNo = '" + SearchData + "' "
        End If

        Dim nDOW As Int16
        nDOW = Now.DayOfWeek
        Dim nFirst As Int16
        Dim nLast As Int16

        nFirst = -nDOW
        nLast = nFirst + 6
        If StatusData = "T" Then
            strSQL += " and ordShpDate like '" & Now.ToString("yyyy-MM-dd") & "%' "
        ElseIf StatusData = "TM" Then
            strSQL += " and ordShpDate like '" & Now.AddDays(1).ToString("yyyy-MM-dd") & "%' "
        ElseIf StatusData = "TW" Then
            strSQL += " and (ordShpDate >= '" & Now.AddDays(nFirst).ToString("yyyy-MM-dd") & " 12:00:00' "
            strSQL += " and ordShpDate <= '" & Now.AddDays(nLast).ToString("yyyy-MM-dd") & " 12:00:00') "
        ElseIf StatusData = "NW" Then
            If nDOW = 0 Then
                nFirst = nFirst + 7
            Else
                nFirst = nFirst + 7
            End If
            nLast = nFirst + 6
            strSQL += " and (ordShpDate >= '" & Now.AddDays(nFirst).ToString("yyyy-MM-dd") & " 12:00:00' "
            strSQL += " and ordShpDate <= '" & Now.AddDays(nLast).ToString("yyyy-MM-dd") & " 12:00:00') "
        End If

        strSQL += " group by o.ordID order by o.ordID, CustomerName "
        Return strSQL
    End Function
    ''' <summary>
    '''Fill Grid Job Planning
    ''' </summary>
    ''' <param name="strCustName"></param>
    ''' <param name="strCustPhoneNo"></param>
    ''' <param name="strOrdNO"></param>
    ''' <param name="strAssignedTo"></param>
    ''' <param name="strFromDate"></param>
    ''' <param name="strTodate"></param>
    ''' <param name="strStatus"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGridJobPlanning(ByVal strCustName As String, ByVal strCustPhoneNo As String, ByVal strOrdNO As String, ByVal strAssignedTo As String, ByVal strFromDate As String, ByVal strTodate As String, Optional ByVal strStatus As String = "") As String
        Dim strSQL As String
        strCustName = clsCommon.funRemove(strCustName)
        strCustPhoneNo = clsCommon.funRemove(strCustPhoneNo)
        strOrdNO = clsCommon.funRemove(strOrdNO)
        strAssignedTo = clsCommon.funRemove(strAssignedTo)

        strSQL = "SELECT distinct o.ordID,PartnerAcronyme,Jobduedate,partnerPhone,o.ordCustID,o.ordCustType, PartnerLongName as CustomerName, o.ordID as AssignedTo, ordShpTrackNo, ordStatus, DATE_FORMAT(ordShpDate,'%m-%d-%Y') as ordShpDate FROM orders o "
        strSQL += " inner join orderitems i on i.ordID=o.ordID left join partners on partners.PartnerID =o.ordCustID "
        strSQL += " left join jobplanning J on J.orderID=o.ordID "
        strSQL += " left join users u on u.userID=J.userID "

        strSQL += " where (ordStatus='P') "

        If HttpContext.Current.Session("UserModules").ToString.Contains("JOBP") = True And (HttpContext.Current.Session("UserModules").ToString.Contains("ADM") = False And HttpContext.Current.Session("UserModules").ToString.Contains("JBPM") = False) Then
            strSQL += " and u.UserID='" & HttpContext.Current.Session("UserID") & "'"
        End If

        If strCustName <> "" Then
            strSQL += " and (PartnerLongName like '%" + strCustName + "%' or PartnerAcronyme like '%" + strCustName + "%'  ) "
        End If
        If strCustPhoneNo <> "" Then
            strSQL += " and partnerPhone= '" + strCustPhoneNo + "' "
        End If
        If strOrdNO <> "" Then
            strSQL += " and o.ordID = '" + strOrdNO + "' "
        End If
        If strAssignedTo <> "" Then
            strSQL += " and concat(u.userFirstName,' ',u.userLastName) like '%" & strAssignedTo & "%' "
        End If
        If strStatus <> "" Then
            strSQL += " and o.ordStatus!='" & strStatus & "'"
        End If
        If strTodate <> "" And strFromDate <> "" Then
            strSQL += " and J.Jobduedate >='" & CDate(strFromDate).ToString("yyyy-MM-dd") & "' and J.Jobduedate <='" & CDate(strTodate).ToString("yyyy-MM-dd") & "'"
        ElseIf strTodate <> "" Then
            strSQL += " and J.Jobduedate <='" & CDate(strTodate).ToString("yyyy-MM-dd") & "'"
        ElseIf strFromDate <> "" Then
            strSQL += " and J.Jobduedate >='" & CDate(strFromDate).ToString("yyyy-MM-dd") & "'"
        End If
        strSQL += " group by o.ordID order by o.ordID, CustomerName "
        Return strSQL
    End Function
    ''' <summary>
    ''' Search by Order No
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <param name="WhsCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckShippingForOrder(ByVal StatusData As String, ByVal SearchData As String, ByVal WhsCode As String) As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM orders o inner join orderitems i on i.ordID=o.ordID left join partners  on partners.PartnerID =o.ordCustID left join users u on u.userID=o.ordSalesRepID where (ordStatus='R' or ordStatus='Z') "
        If WhsCode <> "" And WhsCode <> "ALL" Then
            strSQL += " and o.ordShpWhsCode = '" + WhsCode + "' "
        End If
        If StatusData = OrderSearchFields.OrderNo And SearchData <> "" Then
            strSQL += " and o.ordID = '" + SearchData + "' "
        ElseIf StatusData = CustomerSearchFields.ContactName And SearchData <> "" Then
            strSQL += " and (PartnerLongName like '%" + SearchData + "%' ) "
        ElseIf StatusData = "AN" And SearchData <> "" Then
            strSQL += " and concat(userFirstName,' ',userLastName) like '%" + SearchData + "%' "
        ElseIf StatusData = "TN" And SearchData <> "" Then
            strSQL += " and ordShpTrackNo = '" + SearchData + "' "
        End If
        Dim nDOW As Int16
        nDOW = Now.DayOfWeek
        Dim nFirst As Int16
        Dim nLast As Int16

        nFirst = -nDOW
        nLast = nFirst + 6
        If StatusData = "T" Then
            strSQL += " and ordShpDate like '" & Now.ToString("yyyy-MM-dd") & "%' "
        ElseIf StatusData = "TW" Then
            strSQL += " and (ordShpDate >= '" & Now.AddDays(nFirst).ToString("yyyy-MM-dd") & " 12:00:00' "
            strSQL += " and ordShpDate <= '" & Now.AddDays(nLast).ToString("yyyy-MM-dd") & " 12:00:00') "
        ElseIf StatusData = "NW" Then
            If nDOW = 0 Then
                nFirst = nFirst + 7
            Else
                nFirst = nFirst + 7
            End If
            nLast = nFirst + 6
            strSQL += " and (ordShpDate >= '" & Now.AddDays(nFirst).ToString("yyyy-MM-dd") & " 12:00:00' "
            strSQL += " and ordShpDate <= '" & Now.AddDays(nLast).ToString("yyyy-MM-dd") & " 12:00:00') "
        End If

        strSQL += " group by o.ordID order by o.ordID "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Update Quantity On Process
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateProductQuantityOnProcess() As Boolean
        Dim bResult As Boolean = False
        Dim objSOI As New clsOrderItems
        objSOI.OrdID = _ordID
        Dim objPrdQty As clsPrdQuantity
        Dim objDC As New clsDataClass
        Dim objDR As Data.Odbc.OdbcDataReader
        objDR = objDC.GetDataReader(objSOI.funFillGrid(_ordID))
        While objDR.Read
            objSOI.OrdProductID = objDR("ordProductID").ToString
            objSOI.OrdWarehouse = objDR("ordShpWhsCode").ToString
            objSOI.OrdProductQty = objDR("ordProductQty").ToString
            If objSOI.funCheckProductQuantity() = True Then
                objPrdQty = New clsPrdQuantity
                objPrdQty.ProductID = objSOI.OrdProductID
                objPrdQty.prdWhsCode = objSOI.OrdWarehouse
                objPrdQty.subGetProductQuantity()
                If IsNumeric(objPrdQty.prdQuoteRsv) = True Then
                    objPrdQty.prdQuoteRsv = CDbl(objPrdQty.prdQuoteRsv) - CDbl(objSOI.OrdProductQty)
                Else
                    'objPrdQty.prdQuoteRsv = "-" & objSOI.OrdProductQty
                End If
                Try
                    bResult = objPrdQty.funUpdateProductQuantity("QR")
                Catch ex As Exception
                    bResult = False
                End Try
                If IsNumeric(objPrdQty.prdSORsv) = True Then
                    objPrdQty.prdSORsv = CDbl(objPrdQty.prdSORsv) + CDbl(objSOI.OrdProductQty)
                Else
                    objPrdQty.prdSORsv = objSOI.OrdProductQty
                End If
                Try
                    bResult = objPrdQty.funUpdateProductQuantity("SR")
                Catch ex As Exception
                    bResult = False
                End Try

                objPrdQty = Nothing
            End If
        End While
        objDR.Close()
        objDC.CloseDatabaseConnection()
        objDR = Nothing
        objDC = Nothing
        Return bResult
    End Function
    ''' <summary>
    ''' Update Quantity On Shipping
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateProductQuantityOnShipping() As Boolean
        Dim bResult As Boolean = False
        Dim objSOI As New clsOrderItems
        objSOI.OrdID = _ordID
        Dim objPrdQty As clsPrdQuantity
        Dim objDC As New clsDataClass
        Dim objDR As Data.Odbc.OdbcDataReader
        objDR = objDC.GetDataReader(objSOI.funFillGrid(_ordID))
        While objDR.Read
            objSOI.OrdProductID = objDR("ordProductID").ToString
            objSOI.OrdWarehouse = objDR("ordShpWhsCode").ToString
            objSOI.OrdProductQty = objDR("ordProductQty").ToString
            If objSOI.funCheckProductQuantity() = True Then
                objPrdQty = New clsPrdQuantity
                objPrdQty.ProductID = objSOI.OrdProductID
                objPrdQty.prdWhsCode = objSOI.OrdWarehouse
                objPrdQty.subGetProductQuantity()
                If IsNumeric(objPrdQty.prdSORsv) = True Then
                    objPrdQty.prdSORsv = CDbl(objPrdQty.prdSORsv) - CDbl(objSOI.OrdProductQty)
                Else
                    'objPrdQty.prdSORsv = "-" & objSOI.OrdProductQty
                End If
                Try
                    bResult = objPrdQty.funUpdateProductQuantity("SR")
                Catch ex As Exception
                    bResult = False
                End Try
                If IsNumeric(objPrdQty.prdOhdQty) = True Then
                    objPrdQty.prdOhdQty = CDbl(objPrdQty.prdOhdQty) - CDbl(objSOI.OrdProductQty)
                Else
                    'objPrdQty.prdOhdQty = "-" & objSOI.OrdProductQty
                End If
                Try
                    bResult = objPrdQty.funUpdateProductQuantity("OH")
                Catch ex As Exception
                    bResult = False
                End Try
                objPrdQty = Nothing
            End If
        End While
        objDR.Close()
        objDC.CloseDatabaseConnection()
        objDR = Nothing
        objDC = Nothing
        Return bResult
    End Function
    ''' <summary>
    ''' Order Sales Order
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub funUpdateOrderforGInvoice()
        Dim strSQL As String
        strSQL = "Update orders set ordStatus='" & _ordStatus & "',ordLastUpdatedOn='" & _ordLastUpdatedOn & "' WHERE ordID='" + _ordID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Get SO for Batch print
    ''' </summary>
    ''' <param name="strFromDate"></param>
    ''' <param name="strToDate"></param>
    ''' <param name="strCompanyID"></param>
    ''' <param name="strStatus"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetSOForBatchPrint(ByVal strFromDate As String, ByVal strToDate As String, ByVal strCompanyID As String, ByVal strStatus As String) As ArrayList
        Dim strSQL As String
        Dim strArry As New ArrayList
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distinct ordID, ordStatus FROM orders where 1=1"
        Else
            strSQL = "SELECT distinct ordID FROM orders inner join salesrepcustomer s on s.CustomerID=orders.ordCustID and s.CustomerType=orders.ordCustType and s.UserID='" & HttpContext.Current.Session("UserID") & "' where 1=1 "
        End If
        If strFromDate <> "" And strToDate <> "" Then
            strSQL += " and (ordCreatedOn >='" & strFromDate & " 00:00:00' and ordCreatedOn <='" & strToDate & " 23:59:59')"
        End If
        If strCompanyID <> "" Then
            strSQL += " and ordCompanyID ='" & strCompanyID & "' "
        End If
        If strStatus <> "0" Then
            strSQL += " and ordStatus='" & strStatus & "'"
        ElseIf strStatus = "0" Then
            strSQL += " and (ordStatus='A' OR ordStatus='P' OR ordStatus='D' OR ordStatus='R')"
        End If
        Dim drObj As OdbcDataReader
        drObj = GetDataReader(strSQL)
        While drObj.Read
            strArry.Add(drObj("ordID").ToString())
        End While
        drObj = Nothing
        CloseDatabaseConnection()
        Return strArry
    End Function
    ''' <summary>
    ''' Get SO for Batch Reprint
    ''' </summary>
    ''' <param name="strFromDate"></param>
    ''' <param name="strToDate"></param>
    ''' <param name="strCompanyID"></param>
    ''' <param name="strStatus"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetSOForBatchReprint(ByVal strFromDate As String, ByVal strToDate As String, ByVal strCompanyID As String, ByVal strStatus As String) As ArrayList
        Dim strSQL, strSQLMain As String
        Dim strArry As New ArrayList
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distinct ordID FROM orders where 1=1"
        Else
            strSQL = "SELECT distinct ordID FROM orders inner join salesrepcustomer s on s.CustomerID=orders.ordCustID and s.CustomerType=orders.ordCustType and s.UserID='" & HttpContext.Current.Session("UserID") & "' where 1=1 "
        End If
        If strFromDate <> "" And strToDate <> "" Then
            strSQL += " and (ordCreatedOn >='" & strFromDate & " 00:00:00' and ordCreatedOn <='" & strToDate & " 23:59:59')"
        End If
        If strCompanyID <> "" Then
            strSQL += " and ordCompanyID ='" & strCompanyID & "' "
        End If
        If strStatus <> "0" Then
            strSQL += " and ordStatus='" & strStatus & "'"
        ElseIf strStatus = "0" Then
            strSQL += " and (ordStatus='A' OR ordStatus='P' OR ordStatus='D')"
        End If

        strSQLMain = "Select InvRefNo from invoices where invForOrderNo in (" & strSQL & ")"

        Dim drObj As OdbcDataReader
        drObj = GetDataReader(strSQLMain)
        While drObj.Read
            strArry.Add(drObj("InvRefNo").ToString())
        End While
        drObj = Nothing
        CloseDatabaseConnection()
        Return strArry
    End Function
    ''' <summary>
    ''' ordSalesRep ID
    ''' </summary>
    ''' <param name="strOrdID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetOrdSalesRepID(ByVal strOrdID As String) As String
        Dim strSQL, strSalesRepID As String
        strSQL = "Select ordSalesRepID from orders WHERE ordID='" + strOrdID + "' "
        strSalesRepID = GetScalarData(strSQL)
        Return strSalesRepID
    End Function
    ''' <summary>
    ''' Populate objects of Customer Language
    ''' </summary>
    ''' <param name="strOderID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetCustomerLanguage(ByVal strOderID As String) As String
        Dim strSQL, strPartnerLang As String
        strSQL = "SELECT  PartnerLang FROM orders o left join partners  on partners. PartnerID =o.ordCustID where ordID='" + strOderID + "'"
        strPartnerLang = GetScalarData(strSQL)
        If strPartnerLang = "fr" Then
            strPartnerLang = "fr-CA"
        ElseIf strPartnerLang = "sp" Then
            strPartnerLang = "es-MX"
        Else
            strPartnerLang = "en-CA"
        End If
        Return strPartnerLang
    End Function
    ''' <summary>
    ''' Updated Verified field when orser Approve/reject
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subUpdatedVerified()
        Dim strSQL As String
        strSQL = "Update orders set ordVerified='" & _ordVerified & "',ordVerifiedBy='" & _ordVerifiedBy & "' WHERE ordID='" + _ordID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Find Duplicate Invoice Ref no Name
    ''' </summary>
    ''' <param name="strInvRefType"></param>
    ''' <param name="strInvCompanyID"></param>
    ''' <param name="strInvID"></param>
    ''' <param name="strSOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDuplicateInvoice(ByVal strInvRefType As String, ByVal strInvCompanyID As String, ByVal strInvID As String, ByVal strSOID As String) As Boolean
        Dim strSQL As String
        If strSOID <> "" Then
            strSQL = "SELECT count(*) FROM invoices where invRefType='" & strInvRefType & "' And invCompanyID='" & strInvCompanyID & "' and InvRefNo ='" & strInvID & "' and invForOrderNo<>'" & strSOID & "' "
        Else
            strSQL = "SELECT count(*) FROM invoices where invRefType='" & strInvRefType & "' And invCompanyID='" & strInvCompanyID & "' and InvRefNo ='" & strInvID & "' "
        End If
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
End Class

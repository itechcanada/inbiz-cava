Imports Microsoft.VisualBasic
Imports System.Web.HttpContext
Imports System.Reflection

Public Class clsGrid

    Public Shared gv As GridView
    Public Shared Cnt As Control

    Public Shared Sub MaintainParams(ByVal Page As Web.UI.Page, ByVal Grid As GridView, ByVal Container As Control, ByVal Lbl As Label, ByVal SearchBtn As ImageButton)
        gv = Grid
        Cnt = Container

        If Not Page.IsPostBack Then
            AddEvents(Grid)
            If Not Current.Request.QueryString("Default") = "1" Then
                SetSearchState(Grid, Container)

                Dim t As Type = GetType(ImageButton)
                Dim p() As Object = New Object(0) {}
                p(0) = New System.Web.UI.ImageClickEventArgs(0, 0)
                Dim m As MethodInfo = t.GetMethod("OnClick", (BindingFlags.NonPublic Or BindingFlags.Instance))
                m.Invoke(SearchBtn, p)

                Lbl.Text = ""
            End If
        Else
            GetSearchState(New Object, New EventArgs)
        End If
    End Sub

    Public Shared Sub MaintainParams(ByVal Page As Web.UI.Page, ByVal Grid As GridView, ByVal Container As Control, ByVal Lbl As Label, ByVal SearchBtn As Button)
        gv = Grid
        Cnt = Container

        If Not Page.IsPostBack Then
            AddEvents(Grid)
            If Not Current.Request.QueryString("Default") = "1" Then
                SetSearchState(Grid, Container)

                Dim t As Type = GetType(Button)
                Dim p() As Object = New Object(0) {}
                p(0) = New System.EventArgs()
                Dim m As MethodInfo = t.GetMethod("OnClick", (BindingFlags.NonPublic Or BindingFlags.Instance))
                m.Invoke(SearchBtn, p)

                Lbl.Text = ""
            End If
        Else
            GetSearchState(New Object, New EventArgs)
        End If
    End Sub

    Public Shared Sub AddEvents(ByVal Grid As GridView)
        AddHandler Grid.Sorted, AddressOf GetSearchState
        AddHandler Grid.PageIndexChanged, AddressOf GetSearchState
    End Sub

    Public Shared Sub GetSearchState(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim SearchHash As New Hashtable
        GetControlValues(Cnt, SearchHash)
        SearchHash.Add("Path", Current.Request.Path)
        SearchHash.Add("SortExpression", gv.SortExpression)
        SearchHash.Add("SortDirection", gv.SortDirection)
        SearchHash.Add("PageIndex", gv.PageIndex)
        Current.Session("SearchHash") = SearchHash
    End Sub

    Public Shared Function GetControlValues(ByVal Container As Control, ByVal SearchHash As Hashtable) As Hashtable
        For Each ctl As Control In Container.Controls
            If TypeOf ctl Is TextBox Then
                SearchHash.Add(ctl.ID, CType(ctl, TextBox).Text)
            End If
            If TypeOf ctl Is DropDownList Then
                SearchHash.Add(ctl.ID, CType(ctl, DropDownList).SelectedItem.Value)
            End If
            If TypeOf ctl Is CheckBox Then
                SearchHash.Add(ctl.ID, CType(ctl, CheckBox).Checked)
            End If
            If TypeOf ctl Is RadioButtonList Then
                SearchHash.Add(ctl.ID, CType(ctl, RadioButtonList).SelectedItem.Value)
            End If
            GetControlValues(ctl, SearchHash)
        Next
        Return SearchHash
    End Function

    Public Shared Sub SetSearchState(ByVal Grid As GridView, ByVal Container As Control)
        Dim SearchHash As Hashtable = Current.Session("SearchHash")
        If Not SearchHash Is Nothing Then
            If SearchHash.Item("Path").ToString = Current.Request.Path Then
                For Each obj As Object In SearchHash.Keys
                    SetControlValues(Container, obj.ToString, SearchHash.Item(obj).ToString)
                Next
                Grid.Sort(SearchHash.Item("SortExpression"), SearchHash.Item("SortDirection"))
                Grid.PageIndex = SearchHash.Item("PageIndex")
            End If
        End If
    End Sub

    Public Shared Sub SetControlValues(ByVal Container As Control, ByVal Name As String, ByVal Value As String)
        Dim ctl As Control = Container.FindControl(Name)
        If Not ctl Is Nothing Then
            If TypeOf ctl Is TextBox Then
                CType(ctl, TextBox).Text = Value
            End If
            If TypeOf ctl Is DropDownList Then
                CType(ctl, DropDownList).SelectedValue = Value
            End If
            If TypeOf ctl Is CheckBox Then
                CType(ctl, CheckBox).Checked = Value
            End If
            If TypeOf ctl Is RadioButtonList Then
                CType(ctl, RadioButtonList).SelectedValue = Value
            End If
        End If
    End Sub
End Class

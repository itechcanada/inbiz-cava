Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsContactComFollowUp
	Inherits clsDataClass
	Private _ContactComFollowUpID, _ComFollowUpComID, _ComFollowUpText, _ComFollowUpEmail, _ComFollowUpDateTime, _ComFollowUpUserID As String
	Public Property ContactComFollowUpID() As String
		Get
			Return _ContactComFollowUpID
		End Get
		Set(ByVal value As String)
            _ContactComFollowUpID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ComFollowUpComID() As String
		Get
			Return _ComFollowUpComID
		End Get
		Set(ByVal value As String)
            _ComFollowUpComID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ComFollowUpText() As String
		Get
			Return _ComFollowUpText
		End Get
		Set(ByVal value As String)
            _ComFollowUpText = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ComFollowUpEmail() As String
		Get
			Return _ComFollowUpEmail
		End Get
		Set(ByVal value As String)
            _ComFollowUpEmail = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ComFollowUpDateTime() As String
		Get
			Return _ComFollowUpDateTime
		End Get
		Set(ByVal value As String)
            _ComFollowUpDateTime = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ComFollowUpUserID() As String
		Get
			Return _ComFollowUpUserID
		End Get
		Set(ByVal value As String)
            _ComFollowUpUserID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert ContactComFollowUp
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertContactComFollowUp() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO contactcomfollowup( ComFollowUpComID, ComFollowUpText, ComFollowUpEmail, ComFollowUpDateTime, ComFollowUpUserID) VALUES('"
		strSQL += _ComFollowUpComID + "','"
		strSQL += _ComFollowUpText + "','"
		strSQL += _ComFollowUpEmail + "','"
		strSQL += _ComFollowUpDateTime + "','"
		strSQL += _ComFollowUpUserID + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update ContactComFollowUp
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateContactComFollowUp() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE contactcomfollowup set "
		strSQL += "ComFollowUpComID='" + _ComFollowUpComID + "', "
		strSQL += "ComFollowUpText='" + _ComFollowUpText + "', "
		strSQL += "ComFollowUpEmail='" + _ComFollowUpEmail + "', "
		strSQL += "ComFollowUpDateTime='" + _ComFollowUpDateTime + "', "
		strSQL += "ComFollowUpUserID='" + _ComFollowUpUserID + "'"
		strSQL += " where ContactComFollowUpID='" + _ContactComFollowUpID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of ContactComFollowUp
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getContactComFollowUpInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  ComFollowUpComID, ComFollowUpText, ComFollowUpEmail, ComFollowUpDateTime, ComFollowUpUserID FROM contactcomfollowup where ContactComFollowUpID='" + _ContactComFollowUpID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_ComFollowUpComID = drObj.Item("ComFollowUpComID").ToString
			_ComFollowUpText = drObj.Item("ComFollowUpText").ToString
			_ComFollowUpEmail = drObj.Item("ComFollowUpEmail").ToString
			_ComFollowUpDateTime = drObj.Item("ComFollowUpDateTime").ToString
			_ComFollowUpUserID = drObj.Item("ComFollowUpUserID").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill Grid of FollowUp
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGridByCommID() As String
        Dim strSQL As String = ""
        strSQL = "SELECT Distinct ContactComFollowUpID, ComFollowUpComID, ComFollowUpText, ComFollowUpEmail, ComFollowUpDateTime, ComFollowUpUserID, ComID, ComContactID, ComType, ComMotive, ComDateTime, ComServiceLog, ComEmailMessage, ComCreatedBy, Concat(ContactLastName,' ',ifnull(ContactFirstName,'')) as ContactName, PartnerID, PartnerLongName FROM contactcomfollowup f inner join contactcommunication cc on cc.ComID = f.ComFollowUpComID inner join partnercontacts pc on pc.ContactID=cc.ComContactID inner join partners p on p.PartnerID=pc.ContactPartnerID "
        strSQL += " where ComFollowUpComID='" & _ComFollowUpComID & "' "
        strSQL += " order by ComFollowUpDateTime desc "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid of FollowUp
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGridByFollowUpID() As String
        Dim strSQL As String = ""
        strSQL = "SELECT Distinct ContactComFollowUpID, ComFollowUpComID, ComFollowUpText, ComFollowUpEmail, ComFollowUpDateTime, ComFollowUpUserID, ComID, ComContactID, ComType, ComMotive, ComDateTime, ComServiceLog, ComEmailMessage, ComCreatedBy, Concat(ContactLastName,' ',ifnull(ContactFirstName,'')) as ContactName, PartnerID, PartnerLongName FROM contactcomfollowup f inner join contactcommunication cc on cc.ComID = f.ComFollowUpComID inner join partnercontacts pc on pc.ContactID=cc.ComContactID inner join partners p on p.PartnerID=pc.ContactPartnerID "
        strSQL += " where ContactComFollowUpID='" & _ContactComFollowUpID & "' "
        strSQL += " order by ComFollowUpDateTime desc "
        Return strSQL
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports Resources.Resource
'Imports clsCommon
Public Class clsSysStatus
    Inherits clsDataClass
    Public Sub New()
        MyBase.New()
    End Sub
    'Fill Sales Order in Dropdown list
    Public Sub subGetStatus(ByVal dlStatus As DropDownList, ByVal sysAppPfx As String, ByVal sysAppCode As String, Optional ByVal strType As String = "")
        Dim strSQL As String
        strSQL = "SELECT sysAppDesc,sysAppLogicCode FROM sysstatus where "
        strSQL += " sysAppCodeLang ='" + clsCommon.funGetProductLang() + "' and sysAppCodeActive='1' and sysAppPfx='" & sysAppPfx & "' and sysAppCode='" & sysAppCode & "' order by sysAppCodeSeq"
        dlStatus.DataSource = GetDataReader(strSQL)

        dlStatus.DataTextField = "sysAppDesc"
        dlStatus.DataValueField = "sysAppLogicCode"
        dlStatus.DataBind()
        If strType <> "No" Then
            Dim itm As New ListItem
            itm.Value = ""
            If sysAppPfx = "SO" Or sysAppPfx = "PO" Or sysAppPfx = "SH" Or sysAppPfx = "RE" Then
                itm.Text = Resources.Resource.liSelectOrderStatus
            ElseIf sysAppPfx = "IN" And strType = "IN" Then
                itm.Text = Resources.Resource.liSelect
            ElseIf sysAppPfx = "IN" Then
                itm.Text = Resources.Resource.liINSelStatus
            ElseIf sysAppPfx = "AR" And strType = "AR" Then
                itm.Text = Resources.Resource.liARSelectCreatedDays
            ElseIf sysAppPfx = "AR" Then
                itm.Text = Resources.Resource.liARRcvdVia
            End If

            If sysAppPfx <> "CU" And sysAppPfx <> "VN" And sysAppPfx <> "US" And sysAppPfx <> "CO" And sysAppPfx <> "PD" And sysAppPfx <> "SA" Then
                dlStatus.Items.Insert(0, itm)
            End If
        End If
        CloseDatabaseConnection()
    End Sub
  
End Class

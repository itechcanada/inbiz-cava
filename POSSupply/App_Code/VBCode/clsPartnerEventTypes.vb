Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
''Imports Resources.Resource
Public Class clsPartnerEventTypes
	Inherits clsDataClass
	Private _EventTypeID, _EventType As String
	Public Property EventTypeID() As String
		Get
			Return _EventTypeID
		End Get
		Set(ByVal value As String)
            _EventTypeID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property EventType() As String
		Get
			Return _EventType
		End Get
		Set(ByVal value As String)
            _EventType = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert PartnerEventTypes
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertPartnerEventTypes() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO partnereventtypes( EventType) VALUES('"
		strSQL += _EventType + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update PartnerEventTypes
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updatePartnerEventTypes() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE partnereventtypes set "
		strSQL += "EventType='" + _EventType + "'"
		strSQL += " where EventTypeID='" + _EventTypeID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of PartnerEventTypes
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPartnerEventTypesInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  EventType FROM partnereventtypes where EventTypeID='" + _EventTypeID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_EventType = drObj.Item("EventType").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate EventTypes
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub PopulateEventTypes(ByVal ddl As DropDownList)
        Dim strSQL As String = ""
        strSQL = "SELECT EventTypeID, EventType FROM partnereventtypes "
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "EventType"
        ddl.DataValueField = "EventTypeID"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.liCMSelectEventType
        ddl.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports iTECH.InbizERP.BusinessLogic
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsInvoices
    Inherits clsDataClass
    Private _invID, _invCustType, _invCustID, _invCreatedOn, _invStatus, _invComment, _invLastUpdatedOn, _invLastUpdateBy, _invShpCost, _invCurrencyCode, _invCurrencyExRate, _invCustPO, _invShpWhsCode, _invForOrderNo, _invRefType, _invCompanyID, _InvRefNo, _invTypeCommission As String
    Public Property InvID() As String
        Get
            Return _invID
        End Get
        Set(ByVal value As String)
            _invID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvCustType() As String
        Get
            Return _invCustType
        End Get
        Set(ByVal value As String)
            _invCustType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvCustID() As String
        Get
            Return _invCustID
        End Get
        Set(ByVal value As String)
            _invCustID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvCreatedOn() As String
        Get
            Return _invCreatedOn
        End Get
        Set(ByVal value As String)
            _invCreatedOn = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvStatus() As String
        Get
            Return _invStatus
        End Get
        Set(ByVal value As String)
            _invStatus = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvComment() As String
        Get
            Return _invComment
        End Get
        Set(ByVal value As String)
            _invComment = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvLastUpdatedOn() As String
        Get
            Return _invLastUpdatedOn
        End Get
        Set(ByVal value As String)
            _invLastUpdatedOn = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvLastUpdateBy() As String
        Get
            Return _invLastUpdateBy
        End Get
        Set(ByVal value As String)
            _invLastUpdateBy = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvShpCost() As String
        Get
            Return _invShpCost
        End Get
        Set(ByVal value As String)
            _invShpCost = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvCurrencyCode() As String
        Get
            Return _invCurrencyCode
        End Get
        Set(ByVal value As String)
            _invCurrencyCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvCurrencyExRate() As String
        Get
            Return _invCurrencyExRate
        End Get
        Set(ByVal value As String)
            _invCurrencyExRate = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvCustPO() As String
        Get
            Return _invCustPO
        End Get
        Set(ByVal value As String)
            _invCustPO = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvShpWhsCode() As String
        Get
            Return _invShpWhsCode
        End Get
        Set(ByVal value As String)
            _invShpWhsCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvForOrderNo() As String
        Get
            Return _invForOrderNo
        End Get
        Set(ByVal value As String)
            _invForOrderNo = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvRefType() As String
        Get
            Return _invRefType
        End Get
        Set(ByVal value As String)
            _invRefType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvCompanyID() As String
        Get
            Return _invCompanyID
        End Get
        Set(ByVal value As String)
            _invCompanyID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvRefNo() As String
        Get
            Return _InvRefNo
        End Get
        Set(ByVal value As String)
            _InvRefNo = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property InvCommission() As String
        Get
            Return _invTypeCommission
        End Get
        Set(ByVal value As String)
            _invTypeCommission = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Insert Invoices
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertInvoices() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO invoices( invCustType, invCustID, invCreatedOn, invStatus, invComment, invLastUpdatedOn, invLastUpdateBy, invShpCost, invCurrencyCode, invCurrencyExRate, invCustPO, invShpWhsCode, invForOrderNo,invCompanyID,InvRefNo,invRefType,invTypeCommission) VALUES('"
        strSQL += _invCustType + "','"
        strSQL += _invCustID + "','"
        strSQL += _invCreatedOn + "','"
        strSQL += _invStatus + "','"
        strSQL += _invComment + "','"
        strSQL += _invLastUpdatedOn + "','"
        strSQL += _invLastUpdateBy + "','"
        strSQL += _invShpCost + "','"
        strSQL += _invCurrencyCode + "','"
        strSQL += _invCurrencyExRate + "','"
        strSQL += _invCustPO + "','"
        strSQL += _invShpWhsCode + "','"
        strSQL += _invForOrderNo + "','"
        strSQL += _invCompanyID + "','"
        strSQL += _InvRefNo + "','"
        strSQL += _invRefType + "','"
        strSQL += _invTypeCommission + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Update Invoices
    ''' </summary>
    ''' <param name="strShip"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateInvoices(Optional ByVal strShip As String = "") As Boolean
        Dim strSQL As String
        strSQL = "UPDATE invoices set "
        strSQL += "invCustType='" + _invCustType + "', "
        strSQL += "invCustID='" + _invCustID + "', "
        strSQL += "invStatus='" + _invStatus + "', "
        strSQL += "invComment='" + _invComment + "', "
        strSQL += "invLastUpdatedOn='" + _invLastUpdatedOn + "', "
        strSQL += "invLastUpdateBy='" + _invLastUpdateBy + "', "
        strSQL += "invShpCost='" + _invShpCost + "', "
        strSQL += "invCurrencyCode='" + _invCurrencyCode + "', "
        strSQL += "invCurrencyExRate='" + _invCurrencyExRate + "', "
        strSQL += "invCustPO='" + _invCustPO + "', "
        strSQL += "invShpWhsCode='" + _invShpWhsCode + "', "
        If strShip = "" Then
            strSQL += "invCreatedOn='" + _invCreatedOn + "', "
            strSQL += "InvRefNo= '" + _InvRefNo + "', "
            strSQL += "invForOrderNo='" + _invForOrderNo + "', "
        End If
        strSQL += "invCompanyID= '" + _invCompanyID + "',"
        strSQL += "invTypeCommission= '" + _invTypeCommission + "'"
        If strShip = "" Then
            strSQL += " where invID='" + _invID + "' "
        Else
            strSQL += " where invForOrderNo='" + _invForOrderNo + "' and invRefType=" & OrderSearchFields.InvoiceNo  ''IN' "
        End If
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of Invoices
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getInvoicesInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  invCustType, invCustID, invCreatedOn, invStatus, invComment, invLastUpdatedOn, invLastUpdateBy, invShpCost, invCurrencyCode, invCurrencyExRate, invCustPO, invShpWhsCode, invForOrderNo, invRefType,invCompanyID,InvRefNo,invTypeCommission FROM invoices where invID='" + _invID + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _invCustType = drObj.Item("invCustType").ToString
            _invCustID = drObj.Item("invCustID").ToString
            _invCreatedOn = drObj.Item("invCreatedOn").ToString
            _invStatus = drObj.Item("invStatus").ToString
            _invComment = drObj.Item("invComment").ToString
            _invLastUpdatedOn = drObj.Item("invLastUpdatedOn").ToString
            _invLastUpdateBy = drObj.Item("invLastUpdateBy").ToString
            _invShpCost = drObj.Item("invShpCost").ToString
            _invCurrencyCode = drObj.Item("invCurrencyCode").ToString
            _invCurrencyExRate = drObj.Item("invCurrencyExRate").ToString
            _invCustPO = drObj.Item("invCustPO").ToString
            _invShpWhsCode = drObj.Item("invShpWhsCode").ToString
            _invForOrderNo = drObj.Item("invForOrderNo").ToString
            _invRefType = drObj.Item("invRefType").ToString
            _invCompanyID = drObj.Item("invCompanyID").ToString
            _InvRefNo = drObj.Item("InvRefNo").ToString
            _invTypeCommission = drObj.Item("invTypeCommission").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Return Invoice ID 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetInvoiceID() As String
        Dim objDataClass As New clsDataClass
        Dim strSql, strData As String
        objDataClass.OpenDatabaseConnection()
        strSql = "SELECT Max(invID) FROM invoices where invForOrderNo='" + _invForOrderNo + "' "
        strData = objDataClass.GetScalarData(strSql)
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return strData
    End Function

    ''' <summary>
    ''' Fill Grid of All Orders
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <param name="sPoStatus"></param>
    ''' <param name="sRefType"></param>
    ''' <param name="strCustID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGridForAllOrders(ByVal StatusData As String, ByVal SearchData As String, ByVal sPoStatus As String, ByVal sRefType As String, Optional ByVal strCustID As String = "") As String
        Dim strSQL As String

        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT distinct o.invID,PartnerAcronyme,o.invShpWhsCode, w.WarehouseCode, o.InvRefNo,invCreatedOn, PartnerLongName  as CustomerName,orderTypeDesc, sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as amount, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, invStatus, invCustPO, invForOrderNo, invRefType, invCurrencyCode, invCurrencyExRate FROM invoices o left join syswarehouses w on w.WarehouseCompanyID=o.invcompanyid inner join invoiceitems i on i.invoices_invID=o.invID  left join partners  on partners.PartnerID =o.invCustID "
        Else
            strSQL = "SELECT distinct  o.invID,PartnerAcronyme,o.invShpWhsCode,  w.WarehouseCode, o.InvRefNo,invCreatedOn, PartnerLongName as CustomerName,orderTypeDesc, sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as amount, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, invStatus, invCustPO, invForOrderNo, invRefType, invCurrencyCode, invCurrencyExRate FROM invoices o left join syswarehouses w on w.WarehouseCompanyID=o.invcompanyid Inner join salesrepcustomer s on s.CustomerID=o.invCustID and s.CustomerType=o.invCustType and s.UserID='" & HttpContext.Current.Session("UserID") & "' inner join invoiceitems i on i.invoices_invID=o.invID left join partners  on partners.PartnerID =o.invCustID "
        End If
        strSQL += " left join ordertype on ordertype.orderTypeID=o.InvTypeCommission"
        strSQL += " left join ordertypedtl on ordertypedtl.orderTypeID=o.InvTypeCommission"

        If StatusData = OrderSearchFields.InvoiceNo And SearchData <> "" And sPoStatus <> "" Then
            strSQL += " where o.InvRefNo like '%" + SearchData + "%' and o.invStatus='" & sPoStatus & "' "
        ElseIf StatusData = OrderSearchFields.OrderNo And SearchData <> "" And sPoStatus <> "" Then
            strSQL += " where o.invForOrderNo = '" + SearchData + "' and o.invStatus='" & sPoStatus & "' "
        ElseIf StatusData = CustomerSearchFields.ContactName And SearchData <> "" And sPoStatus <> "" Then
            strSQL += " where (PartnerLongName like '%" + SearchData + "%' OR PartnerAcronyme like '%" + SearchData + "%' ) and o.invStatus='" & sPoStatus & "' "
        ElseIf StatusData = OrderSearchFields.CustomerPO And SearchData <> "" And sPoStatus <> "" Then
            strSQL += " where o.invCustPO = '" + SearchData + "' and o.invStatus='" & sPoStatus & "' "
        ElseIf StatusData = OrderSearchFields.InvoiceNo And SearchData <> "" And sPoStatus = "" Then
            strSQL += " where o.InvRefNo like '%" + SearchData + "%' "
        ElseIf StatusData = OrderSearchFields.OrderNo And SearchData <> "" And sPoStatus = "" Then
            strSQL += " where o.invForOrderNo = '" + SearchData + "' "
        ElseIf StatusData = CustomerSearchFields.Acronyme And SearchData <> "" And sPoStatus = "" Then
            strSQL += " where PartnerAcronyme like '%" + SearchData + "%' "
        ElseIf StatusData = CustomerSearchFields.Acronyme And SearchData <> "" And sPoStatus <> "" Then
            strSQL += " where PartnerAcronyme like '%" + SearchData + "%' and o.invStatus='" & sPoStatus & "' "
        ElseIf StatusData = CustomerSearchFields.Acronyme And SearchData = "" And sPoStatus <> "" Then
            strSQL += " where o.invStatus='" & sPoStatus & "' "

        ElseIf StatusData = CustomerSearchFields.ContactName And SearchData <> "" And sPoStatus = "" Then
            strSQL += " where (PartnerLongName like '%" + SearchData + "%' OR PartnerAcronyme like '%" + SearchData + "%' ) "
        ElseIf StatusData = OrderSearchFields.CustomerPO And SearchData <> "" And sPoStatus = "" Then
            strSQL += " where o.invCustPO = '" + SearchData + "' "
        ElseIf StatusData = OrderSearchFields.InvoiceNo And SearchData = "" And sPoStatus <> "" Then
            strSQL += " where o.invStatus='" & sPoStatus & "' "
        ElseIf StatusData = OrderSearchFields.OrderNo And SearchData = "" And sPoStatus <> "" Then
            strSQL += " where o.invStatus='" & sPoStatus & "' "
        ElseIf StatusData = CustomerSearchFields.ContactName And SearchData = "" And sPoStatus <> "" Then
            strSQL += " where o.invStatus='" & sPoStatus & "' "
        ElseIf StatusData = OrderSearchFields.CustomerPO And SearchData = "" And sPoStatus <> "" Then
            strSQL += " where o.invStatus='" & sPoStatus & "' "
        ElseIf StatusData = "" And SearchData = "" And sPoStatus <> "" Then
            strSQL += " where o.invStatus='" & sPoStatus & "' "
        End If
        If sRefType <> "" Then
            If strSQL.Contains("where") = True Then
                strSQL += " and o.invRefType='" & sRefType & "' "
            Else
                strSQL += " where o.invRefType='" & sRefType & "' "
            End If
        End If
        If strCustID <> "" Then
            If strSQL.Contains("where") = True Then
                strSQL += " and ( o.invCustID='" & strCustID & "' OR i.invGuestID = " & strCustID & " ) "
            Else
                strSQL += " where (o.invCustID='" & strCustID & "' OR i.invGuestID = " & strCustID & " ) "
            End If
        End If
        strSQL += " group by o.invID order by invCreatedOn, o.invID desc, CustomerName "
        Return strSQL
    End Function
    ''' <summary>
    ''' Delete Invoice
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteInvoice() As String
        Dim strSQL As String
        strSQL = "DELETE FROM invoices WHERE invID='" + _invID + "' "
        Return strSQL
    End Function
    ''' <summary>
    '''  Delete Sales Invoice Items
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub funDeleteInvoiceItems()
        Dim strSQL As String
        strSQL = "DELETE FROM invoiceitems WHERE invoices_invID='" + _invID + "' "
        Dim obj As New clsDataClass
        obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Delete Sales Invoice Item Process
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub funDeleteInvoiceItemProcess()
        Dim strSQL As String
        strSQL = "DELETE FROM invitemprocess WHERE invoices_invID='" + _invID + "' "
        Dim obj As New clsDataClass
        obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Update Invoice Comment
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateInvoiceComment() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE invoices set "
        'strSQL += "invStatus='" + _invStatus + "', "
        strSQL += "invComment='" + _invComment + "', "
        strSQL += "invLastUpdatedOn='" + _invLastUpdatedOn + "', "
        strSQL += "invLastUpdateBy='" + _invLastUpdateBy + "'"
        strSQL += " where invForOrderNo='" + _invForOrderNo + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Check Invoice For Order No
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckInvoiceForOrderNo() As Boolean
        Dim strSql As String
        strSql = "SELECT count(*) FROM invoices "
        strSql += " where invForOrderNo='" + _invForOrderNo + "' and invRefType='" + _invRefType + "' "
        strSql = strSql.Replace("'sysNull'", "Null")
        strSql = strSql.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.GetScalarData(strSql)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Update Invoice For Credit Note
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateInvoiceForCreditNote() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE invoices set "
        strSQL += "invRefType='" + _invRefType + "'"
        strSQL += " where invForOrderNo='" + _invForOrderNo + "' and "
        strSQL += " invID='" + _invID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update Invoice refrence NO
    ''' </summary>
    ''' <param name="strInvRefType"></param>
    ''' <param name="strInvCompanyID"></param>
    ''' <param name="strInvID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateInvRefNo(ByVal strInvRefType As String, ByVal strInvCompanyID As String, ByVal strInvID As String) As Boolean
        Dim strSQL, strMaxID As String
        strSQL = "SELECT case isnull(max(InvRefNo)+1) when 0 then max(InvRefNo)+1 when 1 then 1 end  FROM invoices where invRefType='" & strInvRefType & "' "
        If ConfigurationManager.AppSettings("SeparateInvID").ToLower = "yes" Then
            strSQL += " and invCompanyID='" & strInvCompanyID & "'"
        End If
       
        strMaxID = GetScalarData(strSQL)

        If strInvRefType.ToLower = "cn" Then
            strMaxID = 0
        End If

        strSQL = "UPDATE invoices set "
        strSQL += "InvRefNo='" + strMaxID + "'"
        strSQL += " where invForOrderNo='" + _invForOrderNo + "' and "
        strSQL += " invID='" + strInvID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Check Invoice For Invoice
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckInvoiceExits() As Boolean
        Dim strSql As String
        strSql = "SELECT count(*) FROM invoices "
        strSql += " where invForOrderNo='" + _invForOrderNo + "' and invRefType=" & "'IN' "
        strSql = strSql.Replace("'sysNull'", "Null")
        strSql = strSql.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.GetScalarData(strSql)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Return Invoice reff ID 
    ''' </summary>
    ''' <param name="strID"></param>
    ''' <param name="strType"></param>
    ''' <param name="strComID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetInvRefID(ByVal strID As String, ByVal strType As String, Optional ByVal strComID As String = "") As String
        Dim objDataClass As New clsDataClass
        Dim strSql, strData As String
        strSql = ""
        objDataClass.OpenDatabaseConnection()
        If strType = "ID" Then
            strSql = "SELECT InvID FROM invoices where invForOrderNo='" + strID + "'"
        ElseIf strType = "Ref" Then
            strSql = "SELECT InvRefNo FROM invoices where invForOrderNo='" + strID + "'"
        ElseIf strType = "InvID" Then
            strSql = "SELECT InvID FROM invoices where InvRefNo='" + strID + "' and invCompanyID='" & strComID & "'"
        End If
        strData = objDataClass.GetScalarData(strSql)
        objDataClass = Nothing
        Return strData
    End Function
End Class

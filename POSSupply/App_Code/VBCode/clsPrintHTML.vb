Imports Microsoft.VisualBasic
Imports System
Imports System.IO
Imports System.Data.Odbc
Imports Resources.Resource
Imports System.Web.HttpContext
Imports System.Threading
Imports System.Globalization

Public Class clsPrintHTML
    Private strPOID As String
    Private strPath As String = Current.Server.MapPath("~") & "/Docs/"
    Dim objPrint As New clsPrint
    Public Sub New(ByVal ID As String)
        strPOID = ID
    End Sub
    Public Function PrintPurchaseOrder(ByVal strPOID As String) As String

        If strPOID <> "" Then
            Dim objPO As New clsPurchaseOrders
            objPO.PoID = strPOID
            objPO.getPurchaseOrdersInfo()

            Dim objPrt As New clsPartners
            objPrt.PartnerID = objPO.PoVendorID
            objPrt.getPartnersInfo()

            Dim strPrintLang As String = objPrt.PartnerLang
            setPageLanguage(strPrintLang)
        End If

        Dim strData As String
        strData = generatePOHTML(strPOID)
        Return strData
    End Function
    Public Sub setPageLanguage(ByVal custLang As String)
        Dim lang As String = ""
        If custLang = "fr" Then
            lang = "fr-CA"
        ElseIf custLang = "Sp" Then
            lang = "es-MX"
        Else
            lang = "en-CA"
        End If
        Thread.CurrentThread.CurrentUICulture = New CultureInfo(lang)
    End Sub

    Public Function PrintRequestDetail(ByVal strReqID As String, ByVal strReqType As String, Optional ByVal strDuplicateInv As String = "") As String
        Current.Session("QO") = ""
        Dim strData As String = ""
        If strReqType = "QO" Then
            Dim objOrder As New clsOrders
            objOrder.OrdID = strReqID
            objOrder.getOrdersInfo()

            Dim objPrt As New clsPartners
            objPrt.PartnerID = objOrder.OrdCustID

            Dim strPrintLang As String = objPrt.PartnerLang
            setPageLanguage(strPrintLang)

            If ConfigurationManager.AppSettings("QOFormat").ToLower = "a" Then
                Current.Session("QO") = "1"
                strData = generateQOHTML(strReqID, strReqType)
            Else
                'strData = subPDFGeneration(strReqID, strReqType)
            End If
        ElseIf strReqType = "SO" Then
            Dim objOrder As New clsOrders
            objOrder.OrdID = strReqID
            objOrder.getOrdersInfo()

            Dim objPrt As New clsPartners
            objPrt.PartnerID = objOrder.OrdCustID

            Dim strPrintLang As String = objPrt.PartnerLang
            setPageLanguage(strPrintLang)

            strData = generateQOHTML(strReqID, strReqType)
        ElseIf strReqType = "IN" Then
            Dim objInv As New clsInvoices
            objInv.InvID = strReqID

            Dim objPrt As New clsPartners
            objPrt.PartnerID = objInv.InvCustID

            Dim strPrintLang As String = objPrt.PartnerLang
            setPageLanguage(strPrintLang)

            strData = generateInvoiceHTML(strReqID, strReqType, strDuplicateInv)
        ElseIf strReqType = "PL" Then
            'strData = subItemListGeneration(strReqID, strReqType)
        ElseIf strReqType = "BL" Then
            'strData = subItemListGeneration(strReqID, strReqType)
        End If
        Return strData
    End Function
    ' Function for read content of file
    Public Function funReadFileContent(ByVal fileName As String) As String
        Dim strContent As String = ""
        Dim stream As StreamReader = New StreamReader(strPath & "/" & fileName)
        strContent = stream.ReadToEnd
        stream.Close()
        Return strContent
    End Function
    ' Generate page contianing PageHeader Company Info HTML
    Private Function funGetCompanyInfoHTML(ByVal strID As String, ByVal strTitle As String) As String

        Dim strPageHeaderHTML As String = funReadFileContent("PageHeaderTemplate.html")

        Dim objCompany As New clsCompanyInfo
        objCompany.CompanyID = strID

        objCompany.getCompanyInfo()

        Dim logo As String = ""
        Dim strCompanyInfo As String = ""
        If objCompany.CompanyLogoPath <> "" Then
            logo = "<img src= " & ConfigurationManager.AppSettings("CompanyLogoPath").ToString() & objCompany.CompanyLogoPath & " height=80px width=120px alt= " & objCompany.CompanyLogoPath & " style=max-height:100px;max-width:120px/> "
        Else
            logo = ""
        End If
        strPageHeaderHTML = strPageHeaderHTML.Replace("<!--~Company-Logo~-->", logo)
        strPageHeaderHTML = strPageHeaderHTML.Replace("<!--~Company-Name~-->", objCompany.CompanyName)

        'Add sivananda Logo logic by Hitendra
        logo = "<img src= " & ConfigurationManager.AppSettings("SivanandaLogoPath").ToString() & " height=228px width=750px /> "
        strPageHeaderHTML = strPageHeaderHTML.Replace("<!--~Sivananda-Logo~-->", logo)

        Dim strAddress As String = objCompany.CompanyAddressLine1 & IIf(objCompany.CompanyAddressLine2 <> "", ", " & objCompany.CompanyAddressLine2, "") & IIf(objCompany.CompanyCity <> "", ", " & objCompany.CompanyCity, "") & IIf(objCompany.CompanyState <> "", ", " & objCompany.CompanyState, "") & IIf(objCompany.CompanyCountry <> "", ", " & objCompany.CompanyCountry, "") & IIf(objCompany.CompanyPostalCode <> "", ", " & objCompany.CompanyPostalCode, "")
        strAddress = IIf(strAddress <> "", strAddress, "-")
        Dim strTelFax As String = lblPhone & " " & IIf(objCompany.CompanyPOPhone <> "", objCompany.CompanyPOPhone, "-") & " | " & lblFax & " " & IIf(objCompany.CompanyFax <> "", objCompany.CompanyFax, "-")
        Dim strEmail As String = lblEmail & " " & IIf(objCompany.CompanyEmail <> "", objCompany.CompanyEmail, "-")

        strPageHeaderHTML = strPageHeaderHTML.Replace("<!--~Company-Address~-->", strAddress & "<br/>" & strTelFax & "<br/>" & strEmail)
        strPageHeaderHTML = strPageHeaderHTML.Replace("<!--~Doc-Type~-->", strTitle)


        Return strPageHeaderHTML
    End Function
    ' Generates Document header
    Private Function funGetDocHeaderHTML(ByVal DocType As String, ByVal strDate As String, ByVal strNumber As String, ByVal strLeftAddress As String, ByVal strRightAddress As String) As String

        Dim strDocHeaderHTML As String = funReadFileContent("DocHeaderTemplate.html")
        ' Document Hedear Info
        Dim strLeftAddressTitle As String = ""
        Dim strRightAddressTitle As String = prnShipTo
        strDate = prnDate + ": " + IIf(strDate <> "", CDate(strDate).ToString("MMM-dd yyyy"), "--")
        If DocType = "PO" Then
            strLeftAddressTitle = prnBillTo
            strRightAddressTitle = prnShipTo
            strNumber = prnPONo + " " + strNumber
        ElseIf DocType = "QO" Then
            strLeftAddressTitle = prnSOBillTo
            strRightAddressTitle = prnShipTo
            strNumber = prnQuotationNo + " " + strNumber
        ElseIf DocType = "SO" Then
            strLeftAddressTitle = prnSOBillTo
            strRightAddressTitle = prnShipTo
            strNumber = prnSONo + " " + strNumber
        ElseIf DocType = "IN" Then
            strLeftAddressTitle = prnBillTo
            strRightAddressTitle = prnShipTo
            strNumber = prnInvoiceNo + " " + strNumber
        End If

        strDocHeaderHTML = strDocHeaderHTML.Replace("<!--~Date~-->", strDate)
        strDocHeaderHTML = strDocHeaderHTML.Replace("<!--~Doc-No~-->", strNumber)
        strDocHeaderHTML = strDocHeaderHTML.Replace("<!--~Bill-To~-->", strLeftAddressTitle)
        strDocHeaderHTML = strDocHeaderHTML.Replace("<!--~Ship-To~-->", strRightAddressTitle)
        ' Bill To
        strDocHeaderHTML = strDocHeaderHTML.Replace("<!--~BillTo-Address~-->", strLeftAddress)

        ' Shipping
        strDocHeaderHTML = strDocHeaderHTML.Replace("<!--~ShipTo-Address~-->", strRightAddress)

        ' Document Hedear Info End

        Return strDocHeaderHTML
    End Function

    ' Function for replacing PO Items Content HTML
    Private Function funPOItemsTableContent(ByVal strPOFrontPageHTML As String, Optional ByVal check As Boolean = False) As String
        If check Then
            strPOFrontPageHTML = strPOFrontPageHTML.Replace("<!--~External-ID~-->", POExternalID)
            strPOFrontPageHTML = strPOFrontPageHTML.Replace("<!--~Bar-Code~-->", grdPOUPCCode)
            strPOFrontPageHTML = strPOFrontPageHTML.Replace("<!--~Product-Name~-->", POProduct)
            strPOFrontPageHTML = strPOFrontPageHTML.Replace("<!--~Quantity~-->", POQuantity)
            strPOFrontPageHTML = strPOFrontPageHTML.Replace("<!--~Unit-Price~-->", POUnitPrice)
            strPOFrontPageHTML = strPOFrontPageHTML.Replace("<!--~Amount~-->", POAmount)
        End If
        Return strPOFrontPageHTML
    End Function
    ' Generate HTML for PO
    Private Function generatePOHTML(ByVal strPOID As String)

        Dim strHTML As String = ""
        Dim strNewPage As String = ""
        Dim strNewPageHeader As String = ""
        Dim strPageHeaderHTML As String = ""
        Dim strPageFooterHTML As String = ""
        Dim strDocHeaderHTML As String = ""
        Dim strPOFrontPageHTML As String = funReadFileContent("POFrontPageDetailTemplate.html")
        Dim strPOItemsDetailHTML As String = funReadFileContent("POItemsDetailTemplate.html")

        Dim intTop As Integer = 0

        Dim objPO As New clsPurchaseOrders
        Dim objPOItems As New clsPurchaseOrderItems

        If strPOID <> "" Then
            objPO.PoID = strPOID
            objPO.getPurchaseOrders()

            ' Page Header Info
            strNewPageHeader = funGetCompanyInfoHTML(objPO.poCompanyID, prnPO)
            ' Set Top position  
            intTop += 30
            strPageHeaderHTML = strNewPageHeader.Replace("<!--~top-Position~-->", intTop)
            ' Page Header Info End

            ' Document Hedear Info
            Dim strCurrency As String = ""
            Dim strVendorInfo As String = ""
            strCurrency = objPO.VendorCurrency

            ' Bill To
            strVendorInfo += objPO.VendorName & "<br />"
            strVendorInfo += funPopulateAddress("V", "B", objPO.PoVendorID)

            ' Shipping
            Dim strShippingInfo As String = ""
            Dim strWHS As String
            strWHS = objPO.getPOShippingWarehouseCode(strPOID).Split(",")(0)
            strShippingInfo += funWarehouseAddress(strWHS)

            strDocHeaderHTML = funGetDocHeaderHTML("PO", objPO.PoDate, objPO.PoID, strVendorInfo, strShippingInfo)
            strPageHeaderHTML = strPageHeaderHTML.Replace("<!--Document-Header-->", strDocHeaderHTML)
            ' Document Hedear Info End

            ' PO Front Page Detail
            ' Shipping info content
            Dim strShipTable As String = ""
            strShipTable += "<tr>"
            strShipTable += "<td><font style=font:Verdana;font-size:12px;>" & IIf(objPO.PoShpTerms <> "", objPO.PoShpTerms, "--") & "</font></td>"
            strShipTable += "<td><font style=font:Verdana;font-size:12px;>" & IIf(objPO.PoShipVia <> "", objPO.PoShipVia, "--") & "</font></td>"
            strShipTable += "<td><font style=font:Verdana;font-size:12px;>" & IIf(objPO.PoFOBLoc <> "", objPO.PoFOBLoc, "--") & "</font></td>"
            strShipTable += "<td><font style=font:Verdana;font-size:12px;>" & IIf(objPO.PoNotes <> "", objPO.PoNotes, "--") & "</font></td>"
            strShipTable += "</tr>"
            'strPOFrontPageHTML = strPOFrontPageHTML.Replace("<!--~Shipping-Info~-->", strShipTable)
            'strPageHeaderHTML = strPageHeaderHTML.Replace("<!--~Shipping-Info~-->", strShipTable)
            ' PO Items
            ' PO Items
            Dim drPOItems As OdbcDataReader
            drPOItems = objPOItems.GetDataReader(objPOItems.funFillGrid(strPOID))
            Dim dblSubTotal As Double = 0
            Dim strItemContent As String = ""
            Dim strNewPageItemContent As String = ""
            Dim intCount As Integer = 0
            Dim strItemTemp As String = ""
            While drPOItems.Read

                strItemTemp += "<tr>"
                strItemTemp += "<td style=width:15%;text-align:left;><font style=font:Verdana;font-size:12px;>" & IIf(drPOItems("prdExtID").ToString <> "", drPOItems("prdExtID").ToString, "--") & "</font></td>"
                strItemTemp += "<td style=width:15%;text-align:left;><font style=font:Verdana;font-size:12px;>" & IIf(drPOItems("prdUPCCode").ToString <> "", drPOItems("prdUPCCode").ToString, "--") & "</td>"
                strItemTemp += "<td style=width:25%;text-align:left;><font style=font:Verdana;font-size:12px;>" & IIf(drPOItems("prdName") <> "", drPOItems("prdName"), "--") & "</td>"
                strItemTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & drPOItems("poQty") & "</td>"
                strItemTemp += "<td style=width:15%;text-align:right;><font style=font:Verdana;font-size:12px;>" & String.Format("{0:F}", drPOItems("poUnitPrice")) & "</td>"
                strItemTemp += "<td style=width:15%;text-align:right;><font style=font:Verdana;font-size:12px;>" & objPO.PoCurrencyCode & " " & String.Format("{0:F}", drPOItems("amount")) & "</td>"
                strItemTemp += "</tr>"

                'If intCount < 10 Then
                '    strItemContent = strItemTemp
                'Else
                '    If intCount = 10 Then
                '        strItemTemp = ""
                '        ' Set Top position
                '        intTop += 1120
                '        strNewPageItemContent = strNewPageHeader.Replace("<!--~top-Position~-->", intTop)
                '        strNewPageItemContent = strNewPageItemContent.Replace("<!--Document-Detail-->", funPOItemsTableContent(strPOItemsDetailHTML, True))
                '    End If
                'End If
                'intCount += 1
                dblSubTotal += CDbl(drPOItems("amount"))
            End While
            'If intCount > 10 Then
            '    strPOFrontPageHTML = strPOFrontPageHTML.Replace("<!--~Authorized-By~-->", "").Replace("<!--~Sub-Total~-->", "")
            '    strHTML += strNewPageItemContent.Replace("<!--~Items-Detail~-->", strItemTemp)
            'End If
            drPOItems.Close()
            'strPOFrontPageHTML = strPOFrontPageHTML.Replace("<!--~Items-Detail~-->", strItemContent)
            strPOFrontPageHTML = strPOFrontPageHTML.Replace("<!--~Items-Detail~-->", strItemTemp)

            'Calculate Sub Total Start

            ' Authorised By
            Dim objUser As New clsUser
            objUser.UserID = objPO.PoAuthroisedByUserID
            objUser.getUserInfo()
            Dim authorName As String = objUser.UserFirstName & " " & objUser.UserLastName
            strPageFooterHTML = strPageFooterHTML.Replace("<!--~Authorized-By~-->", prnAuthorisedby & " " & authorName)
            strPageFooterHTML = strPageFooterHTML.Replace("<!--~Sub-Total~-->", prnTotal & " <b>" & objPO.PoCurrencyCode & " " & String.Format("{0:F}", dblSubTotal) & "</b>")
            'Calculate Sub Total End


            'strPageHeaderHTML = strPageHeaderHTML.Replace("<!--Document-Header-->", strDocHeaderHTML)
            'strPageHeaderHTML = strPageHeaderHTML.Replace("<!--Document-Detail-->", strPOFrontPageHTML)
            strPageHeaderHTML = strPageHeaderHTML.Replace("<!--Document-Header-->", strDocHeaderHTML)
            strPageHeaderHTML = strPageHeaderHTML.Replace("<!--Document-Detail-->", strPOFrontPageHTML)
            strPageHeaderHTML = strPageHeaderHTML.Replace("<!--~Shipping-Info~-->", strShipTable)
            'strPageHeaderHTML = strPageHeaderHTML.Replace("<!--Page-Footer-->", strPageFooterHTML)
            '   strHTML += strPageHeaderHTML

            'strHTML = strHTML.Replace("<!--~Authorized-By~-->", prnAuthorisedby & " " & authorName)
            'strHTML = strHTML.Replace("<!--~Sub-Total~-->", prnTotal & " <b>" & objPO.PoCurrencyCode & " " & String.Format("{0:F}", dblSubTotal) & "</b>")

            strPageHeaderHTML = strPageHeaderHTML.Replace("<!--~Authorized-By~-->", prnAuthorisedby & " " & authorName)
            strPageHeaderHTML = strPageHeaderHTML.Replace("<!--~Sub-Total~-->", prnTotal & " <b>" & objPO.PoCurrencyCode & " " & String.Format("{0:F}", dblSubTotal) & "</b>")
            strHTML = funReplaceHeaderTitle(strPageHeaderHTML)
            objPO.CloseDatabaseConnection()
            objPO = Nothing
            objPOItems.CloseDatabaseConnection()
            objPOItems = Nothing
        End If
        Return strHTML
    End Function
    ' Function for replacing Titles HTML
    Private Function funReplaceHeaderTitle(ByVal strHTML As String) As String
        strHTML = strHTML.Replace("<!--~External-ID~-->", POExternalID)
        strHTML = strHTML.Replace("<!--~Bar-Code~-->", grdPOUPCCode)
        strHTML = strHTML.Replace("<!--~Product-Name~-->", POProduct)
        strHTML = strHTML.Replace("<!--~Quantity~-->", POQuantity)
        strHTML = strHTML.Replace("<!--~Unit-Price~-->", POUnitPrice)
        strHTML = strHTML.Replace("<!--~Amount~-->", POAmount)
        strHTML = strHTML.Replace("<!--~Shipping-Terms~-->", prnShippingTerms)
        strHTML = strHTML.Replace("<!--~Ship-Via~-->", prnShipVia)
        strHTML = strHTML.Replace("<!--~Ship-FOB-Location~-->", prnFOBLocation)
        strHTML = strHTML.Replace("<!--~Shipping-Notes~-->", prnNote)
        Return strHTML
    End Function
    'Calculate Tax
    Public Function funCalcTax(ByVal strPrdID As String, ByVal strWhsCode As String, ByVal Price As Double, Optional ByVal strPOS As String = "", Optional ByVal strCustomerID As String = "", Optional ByVal strCustomerType As String = "", Optional ByVal strTaxApplyBySO As String = "") As ArrayList
        Dim dblTotal As Double = 0
        Dim strTaxCode As String = ""
        Dim objDC As New clsDataClass
        Dim objDR As Data.Odbc.OdbcDataReader
        Dim objTaxCode As Object
        Dim strSQL As String = ""
        Dim strArr As New ArrayList
        Try
            If strTaxApplyBySO = "" Then
                strSQL = "SELECT prdTaxCode FROM prdquantity where prdID='" & strPrdID & "' and prdWhsCode='" & strWhsCode & "' "
                objTaxCode = Convert.ToString(objDC.GetScalarData(strSQL))
            ElseIf strTaxApplyBySO = 0 Then
                objTaxCode = ""
            Else
                objTaxCode = strTaxApplyBySO
            End If

            If objTaxCode.ToString = "" Or objTaxCode.ToString = "0" Then
                strSQL = ""
                If strPOS = "POS" Then
                    strSQL = "SELECT sysRegTaxCode FROM sysregister where sysRegWhsCode='" & strWhsCode & "' and sysRegCode='" & Current.Session("RegCode") & "'"
                    objTaxCode = Convert.ToString(objDC.GetScalarData(strSQL))
                Else
                    If strCustomerID <> "" And (strCustomerType = "R" Or strCustomerType = "D" Or strCustomerType = "E") Then
                        strSQL = "SELECT PartnerTaxCode FROM partners where PartnerID ='" & strCustomerID & "' "
                        objTaxCode = Convert.ToString(objDC.GetScalarData(strSQL))
                    Else
                        objTaxCode = ""
                    End If
                End If
                If objTaxCode.ToString = "" Or objTaxCode.ToString = "0" Then
                    strSQL = "SELECT WarehouseRegTaxCode FROM syswarehouses where WarehouseCode='" & strWhsCode & "' "
                    objTaxCode = Convert.ToString(objDC.GetScalarData(strSQL))
                    If objTaxCode.ToString = "" Or objTaxCode.ToString = "0" Then
                        Return strArr
                    Else
                        strTaxCode = objTaxCode.ToString
                    End If
                Else
                    strTaxCode = objTaxCode.ToString
                End If
            Else
                strTaxCode = objTaxCode.ToString
            End If
            strSQL = ""
            strSQL = "select sysTaxCode.* from sysTaxCode Inner join systaxcodedesc on systaxcodedesc.sysTaxCodeDescID=sysTaxCode.systaxcode where sysTaxCode = '" & strTaxCode & "' order by sysTaxSequence"
            objDR = objDC.GetDataReader(strSQL)
            Dim nIdx As Integer = 0
            While objDR.Read
                If objDR("sysTaxOnTotal").ToString = "0" Then
                    dblTotal += Math.Round(Price * (CDbl(objDR("sysTaxPercentage") / 100)), 2)
                    strArr.Add(objDR("sysTaxdesc").ToString & "@,@" & Math.Round(Price * (CDbl(objDR("sysTaxPercentage") / 100)), 2))
                ElseIf objDR("sysTaxOnTotal").ToString = "1" Then
                    dblTotal = Math.Round((Price + dblTotal) * (CDbl(objDR("sysTaxPercentage") / 100)), 2)
                    strArr.Add(objDR("sysTaxdesc").ToString & "@,@" & dblTotal.ToString)
                End If
                nIdx += 1
            End While
            objDR.Close()
            objDC.CloseDatabaseConnection()
            objDR = Nothing
            objDC = Nothing
        Catch ex As Exception
        End Try
        Return strArr
    End Function
    'Populate Address
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)
        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + "<br/>" 'Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + "<br/>" ' Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + "<br/>" 'Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + "<br/>" ' Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + "<br/>" ' Microsoft.VisualBasic.Chr(10)
            Else
                strAddress += objAdr.AddressState + "<br/>" ' Microsoft.VisualBasic.Chr(10)
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + "<br/>" ' Microsoft.VisualBasic.Chr(10)
            Else
                strAddress += objAdr.AddressCountry + "<br/>" 'Microsoft.VisualBasic.Chr(10)
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function
    'Populate Address
    Private Function funPopulateQOAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)
        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + ", "
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + ", "
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + ", "
            Else
                strAddress += objAdr.AddressState + ", "
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + Microsoft.VisualBasic.Chr(10)
            Else
                strAddress += objAdr.AddressCountry + Microsoft.VisualBasic.Chr(10)
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function
    'Get Warehouse Address
    Protected Function funWarehouseAddress(ByVal strWhsCode As String)
        Dim strData As String = ""
        If strWhsCode <> "" Then
            Dim objWhs As New clsWarehouses
            objWhs.WarehouseCode = strWhsCode
            objWhs.getWarehouseInfo()
            If objWhs.WarehouseDescription <> "" Then
                strData += objWhs.WarehouseDescription + "<br/>" 'Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseAddressLine1 <> "" Then
                strData += objWhs.WarehouseAddressLine1 + "<br/>" 'Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseAddressLine2 <> "" Then
                strData += objWhs.WarehouseAddressLine2 + "<br/>" ' Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseCity <> "" Then
                strData += objWhs.WarehouseCity + "<br/>" ' Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseState <> "" Then
                strData += objWhs.WarehouseState + "<br/>" 'Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseCountry <> "" Then
                strData += objWhs.WarehouseCountry + "<br/>" 'Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehousePostalCode <> "" Then
                strData += objWhs.WarehousePostalCode
            End If
            objWhs = Nothing
        End If
        Return strData
    End Function

    'Request Details
    Public Function generateQOHTML(ByVal strReqID As String, ByVal strReqType As String) As String

        Dim strHTML As String = ""
        Dim strNewPageHeader As String = ""
        Dim strFrontPageHeader As String = ""
        Dim strDocHeaderHTML As String = ""

        Dim strFrontPageItemsDetailHTML As String = funReadFileContent("SOFrontPageDetailTemplate.html")
        Dim strItemsDetailHTML As String = funReadFileContent("SOItemsDetailTemplate.html")
        Dim strProcessHTML As String = funReadFileContent("SOProcessTemplate.html")

        Dim intTop As Integer = 0
        Dim intItemsInFirstPage As Integer = 10
        Dim objCompany As New clsCompanyInfo

        Dim objOrder As New clsOrders
        Dim objOrderItems As New clsOrderItems
        Dim objOrderItemProcess As New clsOrderItemProcess

        If strReqID <> "" And strReqType <> "" Then

            objOrder.OrdID = strReqID
            objOrder.getOrdersInfo()
            objCompany.CompanyID = objOrder.ordCompanyID
            objCompany.getCompanyInfo()

            ' Page Header Info
            Dim strTitle As String = ""
            If strReqType = "QO" Then
                strTitle = prnQuotation
            ElseIf strReqType = "SO" Then
                strTitle = prnSalesOrder
            End If
            strNewPageHeader = funGetCompanyInfoHTML(objOrder.ordCompanyID, strTitle)
            strFrontPageHeader = strNewPageHeader.Replace("<!--~top-Position~-->", 30)

            ' -----------------------Document Hedear Info--------------------------
            Dim strCurrency As String = ""
            Dim strVendorInfo As String = ""
            strCurrency = objOrder.OrdCurrencyCode

            ' Bill To
            strVendorInfo += objOrder.OrdCustName
            strVendorInfo += "</br>"
            strVendorInfo += funPopulateAddress(objOrder.OrdCustType, "B", objOrder.OrdCustID)

            ' Shipping
            Dim strShippingInfo As String = ""
            strShippingInfo += objOrder.OrdCustName
            strShippingInfo += "</br>"
            strShippingInfo += funPopulateAddress(objOrder.OrdCustType, "S", objOrder.OrdCustID)

            strDocHeaderHTML = funGetDocHeaderHTML(strReqType, objOrder.OrdCreatedOn, objOrder.OrdID, strVendorInfo, strShippingInfo)
            strDocHeaderHTML = strDocHeaderHTML.Replace("<!--~Cust-Acronym~-->", prnOrdAcronym + " #: " + objOrder.CustAcronym)
            strFrontPageHeader = strFrontPageHeader.Replace("<!--Document-Header-->", strDocHeaderHTML)

            '-------------------Document Hedear Info End--------------------------


            '--------------------Order Items Detail Start-------------------------
            Dim drItems As OdbcDataReader
            drItems = objOrderItems.GetDataReader(objOrderItems.funFillGrid(strReqID))
            Dim dblSubTotal As Double = 0
            Dim dblTotal As Double = 0

            Dim strItemContent As String = ""
            Dim strNewPageItemContent As String = ""
            Dim intCount As Integer = 0

            Dim isDiscountGTZero As Boolean = False
            Dim strItemTemp As String = ""
            Dim strItemTempDiscount As String = ""

            While drItems.Read
                If (drItems("ordProductDiscount").ToString <> "") Then
                    If (Convert.ToInt32(drItems("ordProductDiscount")) <> 0) Then
                        isDiscountGTZero = True
                    End If
                End If

                strItemTemp += "<tr>"
                strItemTemp += "<td style=width:15%;text-align:left;display:none;><font style=font:Verdana;font-size:12px;>" & IIf(drItems("prdUPCCode").ToString <> "", drItems("prdUPCCode").ToString, "--") & "</td>"
                strItemTemp += "<td style=width:20%;text-align:left;><font style=font:Verdana;font-size:12px;>" & IIf(drItems("prdName") <> "", drItems("prdName").ToString().Replace(Environment.NewLine, "<br>"), "--") & "</td>"
                strItemTemp += "<td style=width:15%;text-align:left;><font style=font:Verdana;font-size:12px;>" & IIf(drItems("sysTaxCodeDescText").ToString <> "", drItems("sysTaxCodeDescText").ToString, "--") & "</td>"
                strItemTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & IIf(drItems("ordProductDiscount").ToString <> "", drItems("ordProductDiscount").ToString, "--") & "</td>"
                strItemTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & drItems("ordProductQty") & "</td>"
                strItemTemp += "<td style=width:15%;text-align:right;><font style=font:Verdana;font-size:12px;>" & objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", drItems("ordProductUnitPrice")) & "</td>"
                strItemTemp += "<td style=width:15%;text-align:right;><font style=font:Verdana;font-size:12px;>" & objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", drItems("amount")) & "</td>"
                strItemTemp += "</tr>"

                strItemTempDiscount += "<tr>"
                strItemTempDiscount += "<td style=width:15%;text-align:left;display:none;><font style=font:Verdana;font-size:12px;>" & IIf(drItems("prdUPCCode").ToString <> "", drItems("prdUPCCode").ToString, "--") & "</td>"
                strItemTempDiscount += "<td style=width:20%;text-align:left;><font style=font:Verdana;font-size:12px;>" & IIf(drItems("prdName") <> "", drItems("prdName").ToString().Replace(Environment.NewLine, "<br>"), "--") & "</td>"
                strItemTempDiscount += "<td style=width:15%;text-align:left;><font style=font:Verdana;font-size:12px;>" & IIf(drItems("sysTaxCodeDescText").ToString <> "", drItems("sysTaxCodeDescText").ToString, "--") & "</td>"
                strItemTempDiscount += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & drItems("ordProductQty") & "</td>"
                strItemTempDiscount += "<td style=width:15%;text-align:right;><font style=font:Verdana;font-size:12px;>" & objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", drItems("ordProductUnitPrice")) & "</td>"
                strItemTempDiscount += "<td style=width:15%;text-align:right;><font style=font:Verdana;font-size:12px;>" & objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", drItems("amount")) & "</td>"
                strItemTempDiscount += "</tr>"

                dblSubTotal += CDbl(drItems("amount"))

            End While


            If isDiscountGTZero Then
                Dim head As String = "<th style=""width:10%;text-align:right;""><font style=""font:Verdana;font-size:14px;"" ><!--~Discount~--></font></th>"
                strFrontPageItemsDetailHTML = strFrontPageItemsDetailHTML.Replace("<!--~IsDiscountTrue~-->", head)
                strFrontPageItemsDetailHTML = strFrontPageItemsDetailHTML.Replace("<!--~Items-Detail~-->", strItemTemp)
            Else
                strFrontPageItemsDetailHTML = strFrontPageItemsDetailHTML.Replace("<!--~Items-Detail~-->", strItemTempDiscount)
            End If

            strHTML = strFrontPageHeader.Replace("<!--Document-Detail-->", strFrontPageItemsDetailHTML)
            drItems.Close()

            '--------------------Order Items Detail End-------------------------


            '-------------------- Process Start---------------------------------
            Dim drProcess As OdbcDataReader
            objOrderItemProcess.OrdID = strReqID
            drProcess = objOrderItemProcess.GetDataReader(objOrderItemProcess.funFillGridForProcess())
            Dim dblProcessTotal As Double = 0
            Dim IsProcess As Boolean = False
            Dim strProcessTemp As String = ""
            While drProcess.Read
                strProcessTemp += "<tr>"
                strProcessTemp += "<td style=width:30%;text-align:left;><font style=font:Verdana;font-size:12px;>" & drProcess("ProcessDescription") & "</td>"
                strProcessTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;> " & String.Format("{0:F}", drProcess("ordItemProcFixedPrice")) & "</td>"
                strProcessTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & String.Format("{0:F}", drProcess("ordItemProcPricePerHour")) & "</td>"
                strProcessTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & String.Format("{0:F}", drProcess("ordItemProcPricePerUnit")) & "</td>"
                strProcessTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & drProcess("ordItemProcHours") & "</td>"
                strProcessTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & drProcess("ordItemProcUnits") & "</td>"
                strProcessTemp += "<td style=width:20%;text-align:right;><font style=font:Verdana;font-size:12px;>" & objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", drProcess("ProcessCost")) & "</td>"
                strProcessTemp += "</tr>"
                dblProcessTotal += CDbl(drProcess("ProcessCost"))
                IsProcess = True
            End While
            drProcess.Close()
            strProcessHTML = strProcessHTML.Replace("<!--~Process-Detail~-->", strProcessTemp)
            '-------------------- Process End ---------------------------------


            '-------------------Calculate Total Start--------------------------
            Dim strTaxArray(,) As String
            Dim drSOItems As Data.Odbc.OdbcDataReader
            drSOItems = objOrderItems.GetDataReader(objOrderItems.funFillGrid(strReqID))

            While drSOItems.Read
                Dim strTax As ArrayList
                Dim strTaxItem As String
                strTax = funCalcTax(drSOItems("ordProductID"), drSOItems("ordShpWhsCode"), drSOItems("amount"), "", objOrder.OrdCustID, objOrder.OrdCustType, drSOItems("ordProductTaxGrp").ToString)
                Dim intI As Integer = 0
                While strTax.Count - 1 >= intI
                    strTaxItem = strTax.Item(intI).ToString
                    strTaxArray = funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                    intI += 1
                End While
            End While

            Dim i As Integer = 0
            Dim j As Integer = 0

            strTaxArray = objPrint.funTotalProcessTax(dblProcessTotal, objOrder.OrdShpWhsCode, strTaxArray)

            If Not strTaxArray Is Nothing Then
                j = strTaxArray.Length / 2
            End If

            Dim strAmountTable As String = ""
            strAmountTable += "<table width=100% cellpadding=5px cellspacing=0px border=0 > "

            Dim dblTax As Double = 0
            While i < j
                'Commentd by hitendra On 17th Feb 2012 for sivanada
                'strAmountTable += "<tr>"
                'strAmountTable += "<td style=text-align:right;width:70%;><font style=font:Verdana;font-size:12px;font-weight:200;>&nbsp;</td>"
                'strAmountTable += "<td style=text-align:right;width:30%;><font style=font:Verdana;font-size:12px;font-weight:200; >" & strTaxArray(0, i) & ":  <b>" & objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", strTaxArray(1, i)) & "</font></td>"
                'strAmountTable += "</tr>"
                dblTax += strTaxArray(1, i)
                i += 1
            End While

            dblTotal = dblSubTotal + dblProcessTotal + dblTax
            'Commentd by hitendra On 17th Feb 2012 for sivanada
            'strAmountTable += "<tr>"
            'strAmountTable += "<td style=text-align:right;width:70%;><font style=font:Verdana;font-size:14px;font-weight:200;>&nbsp;</td>"
            'strAmountTable += "<td style=text-align:right;width:30%;><font style=font:Verdana;font-size:14px;font-weight:300; >" & prnTotal & " <b> " & objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", dblTotal) & "</b></font></td>"
            'strAmountTable += "</tr>"

            'Balance Amout
            'Dim invId As Integer = iTECH.InbizERP.BusinessLogic.ProcessInvoice.GetInvoiceID(iTECH.InbizERP.BusinessLogic.InvoiceReferenceType.INVOICE, strReqID)
            'If invId > 0 Then
            '    Dim rcvdAmt As Double = iTECH.InbizERP.BusinessLogic.ProcessInvoice.GetInvoiceReceivedAmout(invId)
            '    Dim rcVdAmtBy As String = iTECH.InbizERP.BusinessLogic.ProcessInvoice.AmountReceivedBy(invId)
            '    rcVdAmtBy = IIf(Not String.IsNullOrEmpty(rcVdAmtBy), String.Format(" ({0})", rcVdAmtBy), "")
            '    strAmountTable += "<tr>"
            '    strAmountTable += "<td style=text-align:right;width:70%;><font style=font:Verdana;font-size:14px;font-weight:200;>&nbsp;</td>"
            '    strAmountTable += "<td style=text-align:right;width:30%;><font style=font:Verdana;font-size:14px;font-weight:300; >Partial Payment <b> " & objOrder.OrdCurrencyCode & " " & String.Format("{0:F}{1}", rcvdAmt, rcVdAmtBy) & "</b></font></td>"
            '    strAmountTable += "</tr>"
            '    strAmountTable += "<tr>"
            '    strAmountTable += "<td style=text-align:right;width:70%;><font style=font:Verdana;font-size:14px;font-weight:200;>&nbsp;</td>"
            '    strAmountTable += "<td style=text-align:right;width:30%;><font style=font:Verdana;font-size:14px;font-weight:300; >Balance Amount <b> " & objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", dblTotal - rcvdAmt) & "</b></font></td>"
            '    strAmountTable += "</tr>"
            'Else
            '    strAmountTable += "<tr>"
            '    strAmountTable += "<td style=text-align:right;width:70%;><font style=font:Verdana;font-size:14px;font-weight:200;>&nbsp;</td>"
            '    strAmountTable += "<td style=text-align:right;width:30%;><font style=font:Verdana;font-size:14px;font-weight:300; >Balance Amount <b> " & objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", 0.0) & "</b></font></td>"
            '    strAmountTable += "</tr>"
            '    strAmountTable += "<tr>"
            '    strAmountTable += "<td style=text-align:right;width:70%;><font style=font:Verdana;font-size:14px;font-weight:200;>&nbsp;</td>"
            '    strAmountTable += "<td style=text-align:right;width:30%;><font style=font:Verdana;font-size:14px;font-weight:300; >Balance Amount <b> " & objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", dblTotal) & "</b></font></td>"
            '    strAmountTable += "</tr>"
            'End If
            'New Calculated Total Summary Logic 
            strAmountTable += "<tr>"
            strAmountTable += String.Format("<td colspan='2' style=font:Verdana;font-size:12px;font-weight:200;>{0}</td>", iTECH.InbizERP.BusinessLogic.CalculationHelper.GetOrderTotalSummaryToPrint(iTECH.Library.Utilities.BusinessUtility.GetInt(objOrder.OrdID)))
            strAmountTable += "</tr>"
            '----------------------- Calculate Total End------------------------

            ' Authorized By
            Dim objUser As New clsUser
            objUser.UserID = objOrder.OrdLastUpdateBy
            objUser.getUserInfo()
            Dim strUseName As String = objUser.UserFirstName & " " & objUser.UserLastName
            'Commnetd On 17th Feb By Hitendra for sivanadas 
            'strAmountTable += "<tr>"
            'strAmountTable += "<td style=text-align:left;><font style=font:Verdana;font-size:12px;font-weight:200;>" & prnAuthorisedby & " " & strUseName & "<br />" & prnMemo & " " & objOrder.OrdComment & "</td>"
            'strAmountTable += "<td style=text-align:right;><font style=font:Verdana;font-size:12px;font-weight:200; >&nbsp;</font></td>"
            'strAmountTable += "</tr>"

            ' Generic Quotation Remarks: Commnetd On 17th Feb By Hitendra for sivanadas 
            'Dim strGenericQuotationRemarks As String = IIf(objCompany.CompanyGenQuotationRemarks <> "", objCompany.CompanyGenQuotationRemarks, "")
            'strAmountTable += "<tr>"
            'strAmountTable += "<td style=text-align:left;><font style=font:Verdana;font-size:12px;font-weight:200;>" & strGenericQuotationRemarks & "</td>"
            'strAmountTable += "<td style=text-align:right;><font style=font:Verdana;font-size:12px;font-weight:200; >&nbsp;</font></td>"
            'strAmountTable += "</tr>"
            'strAmountTable += "</table>"

            If IsProcess = True Then
                strHTML = strHTML.Replace("<!--~Process-Table~-->", strProcessHTML)
            End If
            strHTML = strHTML.Replace("<!--~Sub-Total~-->", prnSubTotal & " <b>" & objOrder.OrdCurrencyCode & " " & String.Format("{0:F}", dblSubTotal) & "</b>")
            strHTML = strHTML.Replace("<!--~Amount-Table~-->", strAmountTable)

            strHTML = funReplaceSOHeader(strHTML)
        End If
        Return strHTML
    End Function

    Private Function funReplaceSOHeader(ByVal strHTML As String)

        ' Process Cost Header
        strHTML = strHTML.Replace("<!--~Process-Description~-->", grdProcessDescription)
        strHTML = strHTML.Replace("<!--~Fixed-Cost~-->", grdProcessFixedCost)
        strHTML = strHTML.Replace("<!--~Cost-Hour~-->", grdProcessCostPerHour)
        strHTML = strHTML.Replace("<!--~Cost-Unit~-->", grdProcessCostPerUnit)
        strHTML = strHTML.Replace("<!--~Total-Hour~-->", prnTotalHour)
        strHTML = strHTML.Replace("<!--~Total-Unit~-->", prnTotalUnit)
        strHTML = strHTML.Replace("<!--~Process-Cost~-->", prnProcessCost)

        strHTML = strHTML.Replace("<!--~Bar-Code~-->", grdPOUPCCode)
        strHTML = strHTML.Replace("<!--~Product-Name~-->", POProduct)
        strHTML = strHTML.Replace("<!--~Tax-Group~-->", POTaxGrp)
        strHTML = strHTML.Replace("<!--~Discount~-->", PODiscount)
        strHTML = strHTML.Replace("<!--~Quantity~-->", POQuantity)
        strHTML = strHTML.Replace("<!--~Unit-Price~-->", POUnitPrice)
        strHTML = strHTML.Replace("<!--~Amount~-->", POAmount)
        Return strHTML
    End Function

    Public Function funAddTax(ByVal strTaxArr(,) As String, ByVal TaxCode As String, ByVal Tax As Decimal) As Array
        Dim i As Integer = 0
        Dim j As Integer = 0
        If Not strTaxArr Is Nothing Then
            j = strTaxArr.Length / 2
        End If

        While i < j
            If strTaxArr(0, i) = TaxCode Then
                strTaxArr(1, i) = Convert.ToString(strTaxArr(1, i) + Tax)
                Return strTaxArr
            End If
            i += 1
        End While

        ReDim Preserve strTaxArr(1, j)
        strTaxArr(0, j) = TaxCode
        strTaxArr(1, j) = Tax
        Return strTaxArr
    End Function

    Private Function funGetInvoiceHeaderHTML(ByRef objInvoice As clsInvoices, Optional ByVal salesRepUsrName As String = "") As String
        Dim strHTML As String = ""

        strHTML = funReadFileContent("InvoiceHeaderTemplate.html")

        Dim objCust As New clsExtUser
        objCust.CustID = objInvoice.InvCustID
        objCust.CustType = objInvoice.InvCustType
        objCust.getCustomerInfoByType()

        Dim strCustName As String = objCust.CustName
        objCust = Nothing

        strHTML = strHTML.Replace("<!--~Date~-->", prnDate + ": " + IIf(objInvoice.InvCreatedOn <> "", CDate(objInvoice.InvCreatedOn).ToString("MMM-dd yyyy"), "--"))
        strHTML = strHTML.Replace("<!--~Doc-No~-->", prnInvoiceNo + ": " + objInvoice.InvRefNo)
        strHTML = strHTML.Replace("<!--~Order-No~-->", prnInvOrderNo + ": " + objInvoice.InvForOrderNo)
        strHTML = strHTML.Replace("<!--~Customer-Name~-->", prnInvCustomer + ": " + strCustName)
        If (salesRepUsrName = "") Then
            strHTML = strHTML.Replace("<!--~Invoice-No~-->", prnCustPONo + ": " + objInvoice.InvCustPO)
        Else
            strHTML = strHTML.Replace("<!--~Invoice-No~-->", prnCustPONo + ": " + objInvoice.InvCustPO + "<br/>" & prnSalesRep & ": " + salesRepUsrName)
        End If


        strHTML = strHTML.Replace("<!--~Bill-To~-->", prnInvBillTo)
        strHTML = strHTML.Replace("<!--~Ship-To~-->", prnShipTo)

        Dim strCurrency As String = ""
        Dim strVendorInfo As String = ""
        strCurrency = objInvoice.InvCurrencyCode

       

        ' Bill To
        strVendorInfo += strCustName
        strVendorInfo += "<br/>"
        strVendorInfo += funPopulateAddress(objInvoice.InvCustType, "B", objInvoice.InvCustID)
        strHTML = strHTML.Replace("<!--~BillTo-Address~-->", strVendorInfo)


        ' Shipping
        Dim strShippingInfo As String = ""
        strShippingInfo += strCustName
        strShippingInfo += "<br/>"
        strShippingInfo += funPopulateAddress(objInvoice.InvCustType, "S", objInvoice.InvCustID)
        strHTML = strHTML.Replace("<!--~ShipTo-Address~-->", strShippingInfo)

        Return strHTML
    End Function
    'Request Details
    Private Function generateInvoiceHTML(ByVal strReqID As String, ByVal strReqType As String, ByVal strDuplicateInv As String) As String
        Dim strHTML As String = ""
        Dim strNewPageHeader As String = ""
        Dim strFrontPageHeader As String = ""
        Dim strDocHeaderHTML As String = ""

        Dim strFrontPageItemsDetailHTML As String = funReadFileContent("InvoiceFrontPageDetailTemplate.html")
        Dim strItemsDetailHTML As String = funReadFileContent("SOItemsDetailTemplate.html")
        Dim strProcessHTML As String = funReadFileContent("SOProcessTemplate.html")

        Dim intTop As Integer = 0
        Dim intItemsInFirstPage As Integer = 10

        Dim objCompany As New clsCompanyInfo

        Dim objInvoice As New clsInvoices
        Dim objInvoiceItems As New clsInvoiceItems
        Dim objInvItemProcess As New clsInvItemProcess

        Dim objOrd As New clsOrders

        If strReqID <> "" And strReqType <> "" Then

            objInvoice.InvID = strReqID
            objInvoice.getInvoicesInfo()
            objCompany.CompanyID = objInvoice.InvCompanyID
            ' objCompany.CompanyName = objCompany.funGetFirstCompanyName
            objCompany.getCompanyInfo()

            ' Page Header Info
            Dim strTitle As String = ""
            If objInvoice.InvRefType = "CN" Then
                strTitle = prnCreditNote
            Else
                strTitle = prnInvoice
            End If
            strNewPageHeader = funGetCompanyInfoHTML(objInvoice.InvCompanyID, strTitle)
            strFrontPageHeader = strNewPageHeader.Replace("<!--~top-Position~-->", 30)


            ' -----------------------Document Hedear Info--------------------------
            Dim strCurrency As String = ""
            Dim strVendorInfo As String = ""
            strCurrency = objInvoice.InvCurrencyCode

            objOrd.OrdID = objInvoice.InvForOrderNo
            objOrd.getOrdersInfo()

            Dim objUser As New clsUser
            Dim salesRep As String = objUser.funUserName(objOrd.OrdSalesRepID)

            strDocHeaderHTML = funGetInvoiceHeaderHTML(objInvoice, salesRep)
            strFrontPageHeader = strFrontPageHeader.Replace("<!--Document-Header-->", strDocHeaderHTML)
            '-------------------Document Hedear Info End--------------------------


            '--------------------Invoice Items Detail Start-------------------------
            Dim drItems As OdbcDataReader
            drItems = objInvoiceItems.GetDataReader(objInvoiceItems.funFillGrid(strReqID))
            Dim dblSubTotal As Double = 0
            Dim dblTotal As Double = 0

            Dim strItemContent As String = ""
            Dim strNewPageItemContent As String = ""
            Dim intCount As Integer = 0

            Dim isDiscountGTZero As Boolean = False
            Dim strItemTemp As String = ""
            Dim strItemTempDiscount As String = ""
            While drItems.Read

                If (drItems("invProductDiscount").ToString <> "") Then
                    If (Convert.ToInt32(drItems("invProductDiscount")) <> 0) Then
                        isDiscountGTZero = True
                    End If
                End If
                strItemTemp += "<tr>"
                strItemTemp += "<td style=width:15%;text-align:left;display:none;><font style=font:Verdana;font-size:12px;>" & IIf(drItems("prdUPCCode").ToString <> "", drItems("prdUPCCode").ToString, "--") & "</td>"
                strItemTemp += "<td style=width:20%;text-align:left;><font style=font:Verdana;font-size:12px;>" & IIf(drItems("prdName") <> "", drItems("prdName").ToString().Replace(Environment.NewLine, "<br>"), "--") & "</td>"
                'strItemTemp += "<td style=width:15%;text-align:left;><font style=font:Verdana;font-size:12px;>" & IIf(drItems("sysTaxCodeDescText").ToString <> "", drItems("sysTaxCodeDescText").ToString, "--") & "</td>"
                strItemTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & IIf(drItems("invProductDiscount").ToString <> "", drItems("invProductDiscount").ToString, "--") & "</td>"
                strItemTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & drItems("invProductQty") & "</td>"
                strItemTemp += "<td style=width:15%;text-align:right;><font style=font:Verdana;font-size:12px;>" & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", drItems("invProductUnitPrice")) & "</td>"
                strItemTemp += "<td style=width:15%;text-align:right;><font style=font:Verdana;font-size:12px;>" & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", drItems("amount")) & "</td>"
                strItemTemp += "</tr>"

                strItemTempDiscount += "<tr>"
                strItemTempDiscount += "<td style=width:15%;text-align:left;display:none;><font style=font:Verdana;font-size:12px;>" & IIf(drItems("prdUPCCode").ToString <> "", drItems("prdUPCCode").ToString, "--") & "</td>"
                strItemTempDiscount += "<td style=width:20%;text-align:left;><font style=font:Verdana;font-size:12px;>" & IIf(drItems("prdName") <> "", drItems("prdName").ToString().Replace(Environment.NewLine, "<br>"), "--") & "</td>"
                strItemTempDiscount += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & drItems("invProductQty") & "</td>"
                strItemTempDiscount += "<td style=width:15%;text-align:right;><font style=font:Verdana;font-size:12px;>" & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", drItems("invProductUnitPrice")) & "</td>"
                strItemTempDiscount += "<td style=width:15%;text-align:right;><font style=font:Verdana;font-size:12px;>" & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", drItems("amount")) & "</td>"
                strItemTempDiscount += "</tr>"

                dblSubTotal += CDbl(drItems("amount"))
            End While

            If isDiscountGTZero Then
                Dim head As String = "<th style=""width:10%;text-align:right;""><font style=""font:Verdana;font-size:14px;"" ><!--~Discount~--></font></th>"
                strFrontPageItemsDetailHTML = strFrontPageItemsDetailHTML.Replace("<!--~IsDiscountTrue~-->", head)
                strFrontPageItemsDetailHTML = strFrontPageItemsDetailHTML.Replace("<!--~Items-Detail~-->", strItemTemp)
            Else
                strFrontPageItemsDetailHTML = strFrontPageItemsDetailHTML.Replace("<!--~Items-Detail~-->", strItemTempDiscount)
            End If

            strHTML = strFrontPageHeader.Replace("<!--Document-Detail-->", strFrontPageItemsDetailHTML)
            drItems.Close()

            '--------------------Invoice Items Detail End-------------------------


            '-------------------- Process Start---------------------------------
            Dim drProcess As OdbcDataReader
            objInvItemProcess.Invoices_invID = strReqID
            drProcess = objInvItemProcess.GetDataReader(objInvItemProcess.funFillGridForProcess())
            Dim dblProcessTotal As Double = 0
            Dim IsProcess As Boolean = False
            Dim strProcessTemp As String = ""
            While drProcess.Read
                strProcessTemp += "<tr>"
                strProcessTemp += "<td style=width:30%;text-align:left;><font style=font:Verdana;font-size:12px;>" & drProcess("ProcessDescription") & "</td>"
                strProcessTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;> " & String.Format("{0:F}", drProcess("invItemProcFixedPrice")) & "</td>"
                strProcessTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & String.Format("{0:F}", drProcess("invItemProcPricePerHour")) & "</td>"
                strProcessTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & String.Format("{0:F}", drProcess("invItemProcPricePerUnit")) & "</td>"
                strProcessTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & drProcess("invItemProcHours") & "</td>"
                strProcessTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & drProcess("invItemProcUnits") & "</td>"
                strProcessTemp += "<td style=width:20%;text-align:right;><font style=font:Verdana;font-size:12px;>" & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", drProcess("ProcessCost")) & "</td>"
                strProcessTemp += "</tr>"
                dblProcessTotal += CDbl(drProcess("ProcessCost"))
                IsProcess = True
            End While
            drProcess.Close()
            strProcessHTML = strProcessHTML.Replace("<!--~Process-Detail~-->", strProcessTemp)
            '-------------------- Process End ---------------------------------

            '-------------------Calculate Total Start--------------------------
            Dim strTaxArray(,) As String
            Dim drSOItems As Data.Odbc.OdbcDataReader
            drSOItems = objInvoiceItems.GetDataReader(objInvoiceItems.funFillGrid(strReqID))


            While drSOItems.Read
                Dim strTax As ArrayList
                strTax = funCalcTax(drSOItems("invProductID"), drSOItems("invShpWhsCode"), drSOItems("amount"), "", objInvoice.InvCustID, objInvoice.InvCustType, drSOItems("invProductTaxGrp").ToString)
                Dim strTaxItem As String
                Dim intI As Integer = 0
                While strTax.Count - 1 >= intI
                    strTaxItem = strTax.Item(intI).ToString
                    strTaxArray = funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                    intI += 1
                End While
            End While

            Dim i As Integer = 0
            Dim j As Integer = 0

            '///////////////// WhsTax
            strTaxArray = objPrint.funTotalProcessTax(dblProcessTotal, objInvoice.InvShpWhsCode, strTaxArray)

            If Not strTaxArray Is Nothing Then
                j = strTaxArray.Length / 2
            End If

            Dim strAmountTable As String = ""
            strAmountTable += "<table width=100% cellpadding=5px cellspacing=0px border=0 > "


            ' Dim strWhsTaxHTML As String = getWhsTaxHTML(objInvoiceItems.GetDataReader(objInvoiceItems.funFillGrid(strReqID)), objInvoice.InvShpWhsCode, objInvoice.InvCurrencyCode, dblProcessTotal)
            'strAmountTable += strWhsTaxHTML

            Dim dblTax As Double = 0
            While i < j
                'Commented by Hitendra on 17th Feb 12012
                'strAmountTable += "<tr>"
                'strAmountTable += "<td style=text-align:right;><font style=font:Verdana;font-size:12px;font-weight:200;>&nbsp;</td>"
                'strAmountTable += "<td style=text-align:right;><font style=font:Verdana;font-size:12px;font-weight:200; >" & strTaxArray(0, i) & ":  <b>" & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", strTaxArray(1, i)) & "</font></td>"
                'strAmountTable += "</tr>"
                dblTax += strTaxArray(1, i)
                i += 1
            End While

            dblTotal = dblSubTotal + dblProcessTotal + dblTax
            'Commented by Hitendra on 17th Feb 12012
            'strAmountTable += "<tr>"
            'strAmountTable += "<td style=text-align:right;><font style=font:Verdana;font-size:12px;font-weight:200;>&nbsp;</td>"
            'strAmountTable += "<td style=text-align:right;><font style=font:Verdana;font-size:14px;font-weight:300; > " & prnTotal & "<b> " & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", dblTotal) & "</b></font></td>"
            'strAmountTable += "</tr>"

            'Balance Amout
            'Dim invId As Integer = strReqID
            'If invId > 0 Then
            '    Dim rcvdAmt As Double = iTECH.InbizERP.BusinessLogic.ProcessInvoice.GetInvoiceReceivedAmout(invId)
            '    Dim rcVdAmtBy As String = iTECH.InbizERP.BusinessLogic.ProcessInvoice.AmountReceivedBy(invId)
            '    rcVdAmtBy = IIf(Not String.IsNullOrEmpty(rcVdAmtBy), String.Format(" ({0})", rcVdAmtBy), "")
            '    strAmountTable += "<tr>"
            '    strAmountTable += "<td style=text-align:right;width:70%;><font style=font:Verdana;font-size:14px;font-weight:200;>&nbsp;</td>"
            '    strAmountTable += "<td style=text-align:right;width:30%;><font style=font:Verdana;font-size:14px;font-weight:300; >Partial Payment <b> " & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}{1}", rcvdAmt, rcVdAmtBy) & "</b></font></td>"
            '    strAmountTable += "</tr>"
            '    strAmountTable += "<tr>"
            '    strAmountTable += "<td style=text-align:right;width:70%;><font style=font:Verdana;font-size:14px;font-weight:200;>&nbsp;</td>"
            '    strAmountTable += "<td style=text-align:right;width:30%;><font style=font:Verdana;font-size:14px;font-weight:300; >Balance Amount <b> " & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", dblTotal - rcvdAmt) & "</b></font></td>"
            '    strAmountTable += "</tr>"
            'Else
            '    strAmountTable += "<tr>"
            '    strAmountTable += "<td style=text-align:right;width:70%;><font style=font:Verdana;font-size:14px;font-weight:200;>&nbsp;</td>"
            '    strAmountTable += "<td style=text-align:right;width:30%;><font style=font:Verdana;font-size:14px;font-weight:300; >Balance Amount <b> " & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", 0.0) & "</b></font></td>"
            '    strAmountTable += "</tr>"
            '    strAmountTable += "<tr>"
            '    strAmountTable += "<td style=text-align:right;width:70%;><font style=font:Verdana;font-size:14px;font-weight:200;>&nbsp;</td>"
            '    strAmountTable += "<td style=text-align:right;width:30%;><font style=font:Verdana;font-size:14px;font-weight:300; >Balance Amount <b> " & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", dblTotal) & "</b></font></td>"
            '    strAmountTable += "</tr>"
            'End If
            'New Calculated Total Summary Logic 
            strAmountTable += "<tr>"
            strAmountTable += String.Format("<td colspan='2' style=font:Verdana;font-size:12px;font-weight:200;>{0}</td>", iTECH.InbizERP.BusinessLogic.CalculationHelper.GetInvoiceTotalSummaryToPrint(iTECH.Library.Utilities.BusinessUtility.GetInt(objInvoice.InvID)))
            strAmountTable += "</tr>"
            '----------------------- Calculate Total End------------------------

            ' Authorized By

            objUser.UserID = objInvoice.InvLastUpdateBy
            objUser.getUserInfo()
            Dim strUseName As String = objUser.UserFirstName & " " & objUser.UserLastName
            'Commentd by Hitendra On 17th Feb 2012 for sivanada
            'strAmountTable += "<tr>"            
            'strAmountTable += "<td style=text-align:left;><font style=font:Verdana;font-size:12px;font-weight:200;> " & prnShippingTerms & ": " & objOrd.ordShippingTerms & "<br/>" & prnMemo & " " & objInvoice.InvComment & "</td>"
            'strAmountTable += "<td style=text-align:right;><font style=font:Verdana;font-size:12px;font-weight:200; >&nbsp;</font></td>"
            'strAmountTable += "</tr>"

            ' Generic Quotation Remarks: Commentd by Hitendra On 17th Feb 2012 for sivanada
            'Dim strGenericInvoiceRemarks As String = IIf(objCompany.CompanyGenInvoiceRemarks <> "", objCompany.CompanyGenInvoiceRemarks, "")
            'strAmountTable += "<tr>"
            'strAmountTable += "<td style=text-align:left;><font style=font:Verdana;font-size:12px;font-weight:200;>" & strGenericInvoiceRemarks & "</td>"
            'strAmountTable += "<td style=text-align:right;><font style=font:Verdana;font-size:12px;font-weight:200; >&nbsp;</font></td>"
            'strAmountTable += "</tr>"

            Dim objBP As New clsBatchPrint
            objBP.docNo = objInvoice.InvRefNo
            objBP.docPrintedBy = Current.Session("UserID")
            objBP.docCompanyID = objInvoice.InvCompanyID

            If objBP.funDuplicateItdocstatus Then
                strAmountTable += "<tr>"
                strAmountTable += "<td style=text-align:center; colspan=2><font style=font:Verdana;font-size:12px;font-weight:200;>" & lblDuplicate & "</td>"
                strAmountTable += "</tr>"
            End If
            strAmountTable += "</table>"

            If IsProcess = True Then
                strHTML = strHTML.Replace("<!--~Process-Table~-->", strProcessHTML)
            End If

            strHTML = strHTML.Replace("<!--~Sub-Total~-->", prnSubTotal & " <b> " & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", dblSubTotal) & "</b>")
            strHTML = strHTML.Replace("<!--~Amount-Table~-->", strAmountTable)

            strHTML = funReplaceSOHeader(strHTML)

        End If

        Return strHTML

    End Function
    ' Generates Invoice HTML for NAB Server
    Public Function generateInvoiceHTMLForNAB(ByVal strReqID As String, ByVal strReqType As String, ByVal strDuplicateInv As String)
        Dim htInvoiceData As New System.Collections.Hashtable
        htInvoiceData = getInvoiceDataHashTable(strReqID, strReqType, strDuplicateInv)

        Dim strHTML As String = funReadFileContent("NABInvoiceHTMLTemplateNew.html")
        strHTML = replaceHashtableString(strHTML, htInvoiceData)
        Return strHTML
    End Function
    Private Sub initializeHashtable(ByRef hashTable As Hashtable)
        hashTable.Add("Company_Logo", "")
        hashTable.Add("Doc_Type", "")
        hashTable.Add("Company_Address_Line1", "")
        hashTable.Add("Company_Address_Line2", "")
        hashTable.Add("Company_Address_Line3", "")
        hashTable.Add("Company_Phone", "")
        hashTable.Add("Company_Fax", "")
        hashTable.Add("Company_URL", "")

        hashTable.Add("Doc_Date", "")
        hashTable.Add("Doc_Type_Number", "")
        hashTable.Add("Doc_Ref_Number", "")
        hashTable.Add("Doc_Terms", "")

        hashTable.Add("Label_Bill_To", "")
        hashTable.Add("BillTo_Address", "")

        hashTable.Add("Label_Ship_To", "")
        hashTable.Add("ShipTo_Address", "")

        hashTable.Add("Label_Item_Description", "")
        hashTable.Add("Label_Item_Purchaser", "")
        hashTable.Add("Label_Item_Amount", "")
        hashTable.Add("Items_Detail_Rows", "")

        hashTable.Add("Label_Terms", "")
        hashTable.Add("Terms", "")

        hashTable.Add("Label_Subtotal", "")
        hashTable.Add("Subtotal_Amount", "")

        hashTable.Add("Label_Interest", "")
        hashTable.Add("Interest_Amount", "")

        hashTable.Add("Label_Shipping", "")
        hashTable.Add("Shipping_Amount", "")

        hashTable.Add("Label_Total", "")
        hashTable.Add("Total_Amount", "")

        hashTable.Add("Label_Msg_ThankYouForYourBusiness", "")

        hashTable.Add("Label_Msg_MakeAllCecksPayble", "")

        hashTable.Add("Label_Payment_Form", "")
        hashTable.Add("Label_Visa", "")
        hashTable.Add("Label_Mastercard", "")
        hashTable.Add("Label_Check", "")
        hashTable.Add("Label_MoneyOrder", "")

        hashTable.Add("Label_Card_Number", "")
        hashTable.Add("Label_Expiry_Date", "")
        hashTable.Add("Label_Zip_Code", "")
        hashTable.Add("Label_Signature", "")

        hashTable.Add("Label_Doc_Date", "")
        hashTable.Add("Label_Doc_Number", "")
        hashTable.Add("Label_Doc_Ref_Number", "")
        hashTable.Add("Label_Doc_Terms", "")
        hashTable.Add("Tax_Details", "")
        hashTable.Add("Label_Duplicate", "")

        hashTable.Add("PO_NO", "")
        hashTable.Add("Ship_Terms", "")
        hashTable.Add("REP", "")
        hashTable.Add("Ship_Date", "")
        hashTable.Add("Ship_Via", "")
        hashTable.Add("Dept_No", "")

        hashTable.Add("Label_PO_NO", "")
        hashTable.Add("Label_Ship_Terms", "")
        hashTable.Add("Label_Rep", "")
        hashTable.Add("Label_Ship_Date", "")
        hashTable.Add("Label_Ship_Via", "")
        hashTable.Add("Label_Dept_No", "")
        hashTable.Add("Label_Item_Qty", "")
        hashTable.Add("Label_Item_Rate", "")

    End Sub
    ' set data from invoice to hashtable
    Public Function getInvoiceDataHashTable(ByVal strReqID As String, ByVal strReqType As String, ByVal strDuplicateInv As String) As Hashtable
        Dim invoiceData As New System.Collections.Hashtable
        initializeHashtable(invoiceData)

        Dim objCompany As New clsCompanyInfo
        Dim objInvoice As New clsInvoices
        Dim objInvoiceItems As New clsInvoiceItems
        Dim objInvItemProcess As New clsInvItemProcess

        If strReqID <> "" And strReqType <> "" Then

            objInvoice.InvID = strReqID
            objInvoice.getInvoicesInfo()
            objCompany.CompanyID = objInvoice.InvCompanyID
            objCompany.getCompanyInfo()

            Dim logo As String = ""
            Dim strCompanyInfo As String = ""
            If objCompany.CompanyLogoPath <> "" Then
                logo = "<img src= " & ConfigurationManager.AppSettings("CompanyLogoPath").ToString() & objCompany.CompanyLogoPath & " height=80px width=120px alt= " & objCompany.CompanyLogoPath & " style=max-height:100px;max-width:120px/> "
            Else
                logo = ""
            End If
            invoiceData("Company_Logo") = logo
            invoiceData("Company_Address_Line1") = objCompany.CompanyAddressLine1 & IIf(objCompany.CompanyAddressLine2 <> "", ", " & objCompany.CompanyAddressLine2, "")
            invoiceData("Company_Address_Line2") = IIf(objCompany.CompanyCity <> "", objCompany.CompanyCity, "") & IIf(objCompany.CompanyState <> "", ", " & objCompany.CompanyState, "")
            invoiceData("Company_Address_Line3") = IIf(objCompany.CompanyCountry <> "", objCompany.CompanyCountry, "") & IIf(objCompany.CompanyPostalCode <> "", ", " & objCompany.CompanyPostalCode, "")
            invoiceData("Company_Phone") = lblPhone & ": " & IIf(objCompany.CompanyPOPhone <> "", objCompany.CompanyPOPhone, "-")
            invoiceData("Company_Fax") = lblFax & ": " & IIf(objCompany.CompanyFax <> "", objCompany.CompanyFax, "-")
            invoiceData("Company_URL") = "http://www.northamericanbusinessindex.com/" 'IIf(objCompany.CompanyEmail <> "", objCompany.CompanyEmail, "-")

            invoiceData("Terms") = objCompany.CompanyGenInvoiceRemarks

            Dim strTitle As String = ""
            If objInvoice.InvRefType = "CN" Then
                strTitle = prnCreditNote
            Else
                strTitle = prnInvoice
            End If
            invoiceData("Doc_Type") = strTitle

            Dim strCurrency As String = ""
            Dim strVendorInfo As String = ""
            strCurrency = objInvoice.InvCurrencyCode

            invoiceData("Doc_Date") = IIf(objInvoice.InvCreatedOn <> "", CDate(objInvoice.InvCreatedOn).ToString("MM/dd/yyyy"), "--")
            invoiceData("Doc_Type_Number") = objInvoice.InvRefNo
            invoiceData("Doc_Ref_Number") = objInvoice.InvCustPO


            invoiceData("Label_Doc_Date") = prnYSRDate
            invoiceData("Label_Doc_Number") = prnYSRInvoiceNo
            invoiceData("Label_Doc_Ref_Number") = prnCustPONo
            invoiceData("Label_Bill_To") = prnYSRBillTo
            invoiceData("Label_Ship_To") = lblYSListingAddress


            '//////////////////////////////// New code for NAB

            Dim objOrder As New clsOrders
            objOrder.OrdID = objInvoice.InvForOrderNo
            objOrder.getOrdersInfo()

            invoiceData("Ship_Terms") = IIf(objOrder.ordNetTerms <> "", objOrder.ordNetTerms, "--")
            invoiceData("Ship_Date") = IIf(objOrder.OrdShpDate <> "", CDate(objOrder.OrdShpDate).ToString("MM/dd/yyyy"), "--")
            invoiceData("Ship_Via") = IIf(objOrder.OrdShpCode <> "", objOrder.OrdShpCode, "--")

            objOrder = Nothing
            '////////////////////////////////

            Dim objCust As New clsExtUser
            objCust.CustID = objInvoice.InvCustID
            objCust.CustType = objInvoice.InvCustType
            objCust.getCustomerInfoByType()
            Dim strCustName As String = objCust.CustName
            Dim objContect As New clsPartnerContacts
            objContect.ContactPartnerID = objInvoice.InvCustID
            objContect.getFirstContactWRTPartnerID()
            Dim strContectName As String = objContect.ContactLastName + " " + objContect.ContactFirstName

            invoiceData("Doc_Terms") = objCust.CustNetTerms

            objCust = Nothing

            ' Bill To
            strVendorInfo += strCustName
            strVendorInfo += "<br/>"
            strVendorInfo += funPopulateAddress(objInvoice.InvCustType, "B", objInvoice.InvCustID)
            invoiceData("BillTo_Address") = strVendorInfo

            ' Shipping
            Dim strShippingInfo As String = ""
            strShippingInfo += strCustName
            strShippingInfo += "<br/>"
            strShippingInfo += funPopulateAddress(objInvoice.InvCustType, "S", objInvoice.InvCustID)
            invoiceData("ShipTo_Address") = strShippingInfo

            '--------------------Invoice Items Detail Start-------------------------
            Dim drItems As OdbcDataReader
            drItems = objInvoiceItems.GetDataReader(objInvoiceItems.funFillGrid(strReqID))
            Dim dblSubTotal As Double = 0
            Dim dblTotal As Double = 0

            Dim strItemTemp As String = ""
            While drItems.Read
                strItemTemp += "<tr style=text-align:top;>"
                strItemTemp += "<td style= border-left:thin;border-left-style:solid;min-height:20px;text-align:left;padding-left:5px;><font style=font:Verdana;font-size:14px;>" & IIf(drItems("prdName") <> "", drItems("prdName"), "--") & "</td>"
                strItemTemp += "<td style= border-left:thin;border-left-style:solid;min-height:20px;text-align:left;padding-left:5px;><font style=font:Verdana;font-size:14px;>" & IIf(strContectName <> "", strContectName, "--") & "</td>"
                strItemTemp += "<td style= border-left:thin;border-left-style:solid;min-height:20px;text-align:center;><font style=font:Verdana;font-size:14px;>" & drItems("invProductQty") & "</td>"
                strItemTemp += "<td style= border-left:thin;border-left-style:solid;min-height:20px;text-align:right;text-align:top;padding-right:5px;><font style=font:Verdana;font-size:14px;>" & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", drItems("amount") / drItems("invProductQty")) & "</td>"
                strItemTemp += "<td style= border-left:thin;border-left-style:solid;min-height:20px;text-align:right;text-align:top;padding-right:5px;border-right:thin;border-right-style:solid;><font style=font:Verdana;font-size:14px;>" & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", drItems("amount")) & "</td>"
                strItemTemp += "</tr>"

                dblSubTotal += CDbl(drItems("amount"))
            End While
            drItems.Close()

            invoiceData("Items_Detail_Rows") = strItemTemp
            invoiceData("Label_Subtotal") = prnYSRSubTotal
            invoiceData("Subtotal_Amount") = objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", dblSubTotal)
            '--------------------Invoice Items Detail End-------------------------

            '-------------------- Process Start---------------------------------
            'Dim drProcess As OdbcDataReader
            'objInvItemProcess.Invoices_invID = strReqID
            'drProcess = objInvItemProcess.GetDataReader(objInvItemProcess.funFillGridForProcess())
            'Dim dblProcessTotal As Double = 0
            'Dim IsProcess As Boolean = False
            'Dim strProcessTemp As String = ""
            'While drProcess.Read
            '    strProcessTemp += "<tr>"

            '    strProcessTemp += "<td style=width:30%;text-align:left;><font style=font:Verdana;font-size:12px;>" & drProcess("ProcessDescription") & "</td>"
            '    strProcessTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;> " & String.Format("{0:F}", drProcess("invItemProcFixedPrice")) & "</td>"
            '    strProcessTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & String.Format("{0:F}", drProcess("invItemProcPricePerHour")) & "</td>"
            '    strProcessTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & String.Format("{0:F}", drProcess("invItemProcPricePerUnit")) & "</td>"
            '    strProcessTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & drProcess("invItemProcHours") & "</td>"
            '    strProcessTemp += "<td style=width:10%;text-align:right;><font style=font:Verdana;font-size:12px;>" & drProcess("invItemProcUnits") & "</td>"
            '    strProcessTemp += "<td style=width:20%;text-align:right;><font style=font:Verdana;font-size:12px;>" & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", drProcess("ProcessCost")) & "</td>"
            '    strProcessTemp += "</tr>"
            '    IsProcess = True
            'End While
            'drProcess.Close()

            '-------------------- Process End ---------------------------------

            '-------------------Calculate Total Start--------------------------
            Dim strTaxArray(,) As String
            Dim drSOItems As Data.Odbc.OdbcDataReader
            drSOItems = objInvoiceItems.GetDataReader(objInvoiceItems.funFillGrid(strReqID))

            While drSOItems.Read
                Dim strTax As ArrayList
                strTax = funCalcTax(drSOItems("invProductID"), drSOItems("invShpWhsCode"), drSOItems("amount"), "", objInvoice.InvCustID, objInvoice.InvCustType, drSOItems("invProductTaxGrp").ToString)
                Dim strTaxItem As String
                Dim intI As Integer = 0
                While strTax.Count - 1 >= intI
                    strTaxItem = strTax.Item(intI).ToString
                    strTaxArray = funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                    intI += 1
                End While
            End While

            Dim i As Integer = 0
            Dim j As Integer = 0
            If Not strTaxArray Is Nothing Then
                j = strTaxArray.Length / 2
            End If

            Dim dblTax As Double = 0
            Dim strUSTax As String = ""
            While i < j
                strUSTax += "<tr>"
                strUSTax += "<td style=text-align:left;padding-right:8px;border-left:thin;border-left-style:solid;border-bottom:thin;border-bottom-style:solid;>&nbsp;" & strTaxArray(0, i) & "</td>"
                strUSTax += "<td style=text-align:right;border-right:thin;border-right-style:solid;border-bottom:thin;border-bottom-style:solid;padding-right:5px;>&nbsp;" & objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", strTaxArray(1, i)) & "</td>"
                strUSTax += "</tr>"
                dblTax += strTaxArray(1, i)
                i += 1
            End While

            Dim dblShpAmt As Double = 0
            If objInvoice.InvShpCost <> "" Then
                dblShpAmt = objInvoice.InvShpCost
            End If

            invoiceData("Shipping_Amount") = dblShpAmt
            invoiceData("Interest_Amount") = "0.00"

            ' ////////////////////////////////////////////
            ' If Company Country is canada then Tax added else shipping amount added 
            '-----------------------------------------------------------------------
            Dim strTaxDetail As String = ""
            If objCompany.CompanyCountry.ToLower = "canada" Then
                invoiceData("Tax_Details") = strUSTax
                dblTotal = dblSubTotal + dblTax
            Else
                strTaxDetail += "<tr>"
                strTaxDetail += "<td style=text-align:left;padding-right:8px;border-left:thin;border-left-style:solid;border-bottom:thin;border-bottom-style:solid;>&nbsp;" & lblYSShipping & "</td>"
                strTaxDetail += "<td style=text-align:right;border-right:thin;border-right-style:solid;border-bottom:thin;border-bottom-style:solid;padding-right:5px;>&nbsp;" & invoiceData("Shipping_Amount") & "</td></tr>"
                invoiceData("Tax_Details") = strTaxDetail
                dblTotal = dblSubTotal + dblShpAmt



            End If
            ' ////////////////////////////////////////////

            invoiceData("Label_Total") = prnYSRTotal
            invoiceData("Total_Amount") = objInvoice.InvCurrencyCode & " " & String.Format("{0:F}", dblTotal)
            '----------------------- Calculate Total End------------------------

            ' Authorized By
            'Dim objUser As New clsUser
            'objUser.UserID = objInvoice.InvLastUpdateBy
            'objUser.getUserInfo()
            'Dim strUseName As String = objUser.UserFirstName & " " & objUser.UserLastName
            'strAmountTable += "<tr>"
            'strAmountTable += "<td style=text-align:left;><font style=font:Verdana;font-size:12px;font-weight:200;>" & prnAuthorisedby & " " & strUseName & "<br />" & prnMemo & " " & objInvoice.InvComment & "</td>"
            'strAmountTable += "<td style=text-align:right;><font style=font:Verdana;font-size:12px;font-weight:200; >&nbsp;</font></td>"
            'strAmountTable += "</tr>"

            ' Generic Quotation Remarks:
            'Dim strGenericInvoiceRemarks As String = IIf(objCompany.CompanyGenInvoiceRemarks <> "", objCompany.CompanyGenInvoiceRemarks, "")
            'strAmountTable += "<tr>"
            'strAmountTable += "<td style=text-align:left;><font style=font:Verdana;font-size:12px;font-weight:200;>" & strGenericInvoiceRemarks & "</td>"
            'strAmountTable += "<td style=text-align:right;><font style=font:Verdana;font-size:12px;font-weight:200; >&nbsp;</font></td>"
            'strAmountTable += "</tr>"
            'strAmountTable += "</table>"
            If strDuplicateInv = "1" Then
                invoiceData("Label_Duplicate") = lblDuplicate
            End If
            createInvoiceHashtableLabels(invoiceData)
        End If
            Return invoiceData
    End Function
    ' Creates label from Resources
    Private Sub createInvoiceHashtableLabels(ByRef htInvoiceData As Hashtable)
        htInvoiceData("Label_Item_Description") = prnYSRDescription
        htInvoiceData("Label_Item_Purchaser") = lblYSPurchaser
        htInvoiceData("Label_Item_Amount") = prnYSRAmount

        htInvoiceData("Label_Interest") = lblYSInterest
        htInvoiceData("Label_Shipping") = lblYSShipping
        htInvoiceData("Label_Terms") = lblTerms

        htInvoiceData("Label_Msg_ThankYouForYourBusiness") = msgThankYouForYourBusiness

        htInvoiceData("Label_Msg_MakeAllCecksPayble") = msgMakeAllCecksPayble

        htInvoiceData("Label_Payment_Form") = lblPaymentForm
        htInvoiceData("Label_Visa") = lblVisa
        htInvoiceData("Label_Mastercard") = lblMasterCard
        htInvoiceData("Label_Check") = lblCheck
        htInvoiceData("Label_MoneyOrder") = lblMoneyorder

        htInvoiceData("Label_Card_Number") = lblCardNumber
        htInvoiceData("Label_Expiry_Date") = lblExpiryDate
        htInvoiceData("Label_Zip_Code") = lblZipCode
        htInvoiceData("Label_Signature") = lblSignature

        htInvoiceData("Label_Doc_Terms") = lblTerms

        htInvoiceData("Label_PO_NO") = prnYSRPONO
        htInvoiceData("Label_Ship_Terms") = prnYSRShpTerm
        htInvoiceData("Label_Rep") = prnYSRREP
        htInvoiceData("Label_Ship_Via") = prnjYSRShipVia
        htInvoiceData("Label_Dept_No") = prnYSRDeptNo
        htInvoiceData("Label_Item_Qty") = prnYSRQty
        htInvoiceData("Label_Item_Rate") = prnYSRRate
        htInvoiceData("Label_Ship_Date") = prnYSRShipDate

    End Sub
    ' Replaces hashtable values in given string
    Private Function replaceHashtableString(ByVal strHTML As String, ByRef htInvoiceData As Hashtable) As String
        strHTML = strHTML.Replace("<!--~Company_Logo~-->", htInvoiceData("Company_Logo"))
        strHTML = strHTML.Replace("<!--~Doc_Type~-->", htInvoiceData("Doc_Type"))
        strHTML = strHTML.Replace("<!--~Company_Address_Line1~-->", htInvoiceData("Company_Address_Line1"))
        strHTML = strHTML.Replace("<!--~Company_Address_Line2~-->", htInvoiceData("Company_Address_Line2"))
        strHTML = strHTML.Replace("<!--~Company_Address_Line3~-->", htInvoiceData("Company_Address_Line3"))
        strHTML = strHTML.Replace("<!--~Company_Phone~-->", htInvoiceData("Company_Phone"))
        strHTML = strHTML.Replace("<!--~Company_Fax~-->", htInvoiceData("Company_Fax"))
        strHTML = strHTML.Replace("<!--~Company_URL~-->", htInvoiceData("Company_URL"))

        strHTML = strHTML.Replace("<!--~Doc_Date~-->", htInvoiceData("Doc_Date"))
        strHTML = strHTML.Replace("<!--~Doc_Type_Number~-->", htInvoiceData("Doc_Type_Number"))
        strHTML = strHTML.Replace("<!--~Doc_Ref_Number~-->", htInvoiceData("Doc_Ref_Number"))
        strHTML = strHTML.Replace("<!--~Doc_Terms~-->", htInvoiceData("Doc_Terms"))

        strHTML = strHTML.Replace("<!--~Label_Bill_To~-->", htInvoiceData("Label_Bill_To"))
        strHTML = strHTML.Replace("<!--~BillTo_Address~-->", htInvoiceData("BillTo_Address"))

        strHTML = strHTML.Replace("<!--~Label_Ship_To~-->", htInvoiceData("Label_Ship_To"))
        strHTML = strHTML.Replace("<!--~ShipTo_Address~-->", htInvoiceData("ShipTo_Address"))

        strHTML = strHTML.Replace("<!--~Label_Item_Description~-->", htInvoiceData("Label_Item_Description"))
        strHTML = strHTML.Replace("<!--~Label_Item_Purchaser~-->", htInvoiceData("Label_Item_Purchaser"))
        strHTML = strHTML.Replace("<!--~Label_Item_Amount~-->", htInvoiceData("Label_Item_Amount"))
        strHTML = strHTML.Replace("<!--~Items_Detail_Rows~-->", htInvoiceData("Items_Detail_Rows"))

        strHTML = strHTML.Replace("<!--~Label_Terms~-->", htInvoiceData("Label_Terms"))
        strHTML = strHTML.Replace("<!--~Terms~-->", htInvoiceData("Terms"))

        strHTML = strHTML.Replace("<!--~Label_Subtotal~-->", htInvoiceData("Label_Subtotal"))
        strHTML = strHTML.Replace("<!--~Subtotal_Amount~-->", htInvoiceData("Subtotal_Amount"))

        strHTML = strHTML.Replace("<!--~Label_Interest~-->", htInvoiceData("Label_Interest"))
        strHTML = strHTML.Replace("<!--~Interest_Amount~-->", htInvoiceData("Interest_Amount"))

        strHTML = strHTML.Replace("<!--~Label_Shipping~-->", htInvoiceData("Label_Shipping"))
        strHTML = strHTML.Replace("<!--~Shipping_Amount~-->", htInvoiceData("Shipping_Amount"))

        strHTML = strHTML.Replace("<!--~Label_Total~-->", htInvoiceData("Label_Total"))
        strHTML = strHTML.Replace("<!--~Total_Amount~-->", htInvoiceData("Total_Amount"))

        strHTML = strHTML.Replace("<!--~Label_Msg_ThankYouForYourBusiness~-->", htInvoiceData("Label_Msg_ThankYouForYourBusiness"))

        strHTML = strHTML.Replace("<!--~Label_Msg_MakeAllCecksPayble~-->", htInvoiceData("Label_Msg_MakeAllCecksPayble"))

        strHTML = strHTML.Replace("<!--~Label_Payment_Form~-->", htInvoiceData("Label_Payment_Form"))
        strHTML = strHTML.Replace("<!--~Label_Visa~-->", htInvoiceData("Label_Visa"))
        strHTML = strHTML.Replace("<!--~Label_Mastercard~-->", htInvoiceData("Label_Mastercard"))
        strHTML = strHTML.Replace("<!--~Label_Check~-->", htInvoiceData("Label_Check"))
        strHTML = strHTML.Replace("<!--~Label_MoneyOrder~-->", htInvoiceData("Label_MoneyOrder"))

        strHTML = strHTML.Replace("<!--~Label_Card_Number~-->", htInvoiceData("Label_Card_Number"))
        strHTML = strHTML.Replace("<!--~Label_Expiry_Date~-->", htInvoiceData("Label_Expiry_Date"))
        strHTML = strHTML.Replace("<!--~Label_Zip_Code~-->", htInvoiceData("Label_Zip_Code"))
        strHTML = strHTML.Replace("<!--~Label_Signature~-->", htInvoiceData("Label_Signature"))

        strHTML = strHTML.Replace("<!--~Label_Doc_Date~-->", htInvoiceData("Label_Doc_Date"))
        strHTML = strHTML.Replace("<!--~Label_Doc_Number~-->", htInvoiceData("Label_Doc_Number"))
        strHTML = strHTML.Replace("<!--~Label_Doc_Ref_Number~-->", htInvoiceData("Label_Doc_Ref_Number"))
        strHTML = strHTML.Replace("<!--~Label_Doc_Terms~-->", htInvoiceData("Label_Doc_Terms"))
        strHTML = strHTML.Replace("<!--~Tax_Details~-->", htInvoiceData("Tax_Details"))
        strHTML = strHTML.Replace("<!--~Label_Duplicate~-->", htInvoiceData("Label_Duplicate"))






        '////////////
        strHTML = strHTML.Replace("<!--~Ship_Terms~-->", htInvoiceData("Ship_Terms"))
        strHTML = strHTML.Replace("<!--~Ship_Date~-->", htInvoiceData("Ship_Date"))
        strHTML = strHTML.Replace("<!--~Ship_Via~-->", htInvoiceData("Ship_Via"))

        strHTML = strHTML.Replace("<!--~Label_PO_NO~-->", htInvoiceData("Label_PO_NO"))
        strHTML = strHTML.Replace("<!--~Label_Ship_Terms~-->", htInvoiceData("Label_Ship_Terms"))
        strHTML = strHTML.Replace("<!--~Label_Rep~-->", htInvoiceData("Label_Rep"))
        strHTML = strHTML.Replace("<!--~Label_Ship_Via~-->", htInvoiceData("Label_Ship_Via"))
        strHTML = strHTML.Replace("<!--~Label_Dept_No~-->", htInvoiceData("Label_Dept_No"))
        strHTML = strHTML.Replace("<!--~Label_Ship_Date~-->", htInvoiceData("Label_Ship_Date"))

        strHTML = strHTML.Replace("<!--~Label_Item_Rate~-->", htInvoiceData("Label_Item_Rate"))
        strHTML = strHTML.Replace("<!--~Label_Item_Qty~-->", htInvoiceData("Label_Item_Qty"))
        Return strHTML
    End Function

    Private Function getWhsTaxHTML(ByVal drSOItems As Data.Odbc.OdbcDataReader, ByVal whsCode As String, ByVal currencyCode As String, ByVal Price As Double) As String
        Dim strTaxArray(,) As String


        While drSOItems.Read
            Dim strTax As ArrayList
            strTax = objPrint.funCalcProcessTax(whsCode, Price)
            ' strTax = funCalcTax(drSOItems("invProductID"), drSOItems("invShpWhsCode"), drSOItems("amount"), "", objInvoice.InvCustID, objInvoice.InvCustType, drSOItems("invProductTaxGrp").ToString)
            Dim strTaxItem As String
            Dim intI As Integer = 0
            While strTax.Count - 1 >= intI
                strTaxItem = strTax.Item(intI).ToString
                strTaxArray = funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                intI += 1
            End While
        End While

        Dim i As Integer = 0
        Dim j As Integer = 0
        If Not strTaxArray Is Nothing Then
            j = strTaxArray.Length / 2
        End If

        Dim strAmountTable As String = ""
        Dim dblTax As Double = 0
        While i < j
            strAmountTable += "<tr>"
            strAmountTable += "<td style=text-align:right;><font style=font:Verdana;font-size:12px;font-weight:200;>" & strTaxArray(0, i) & "</td>"
            strAmountTable += "<td style=text-align:right;><font style=font:Verdana;font-size:12px;font-weight:200; >" & currencyCode & " " & String.Format("{0:F}", strTaxArray(1, i)) & "</font></td>"
            strAmountTable += "</tr>"
            dblTax += strTaxArray(1, i)
            i += 1
        End While
        Return strAmountTable
    End Function
    
   
End Class

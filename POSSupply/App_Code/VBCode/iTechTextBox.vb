Imports Microsoft.VisualBasic
Imports System.Web
Imports System.Security.Permissions
Public NotInheritable Class iTechTextBox 'Use to support UTF-8 Charset in TextBox
    Inherits System.Web.UI.WebControls.TextBox
    Protected Overloads Property [ReadOnly]() As Boolean
        Get
            [ReadOnly] = MyBase.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            MyBase.ReadOnly = value
        End Set
    End Property
    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim stringWriter As New System.IO.StringWriter
        Dim source As New HtmlTextWriter(stringWriter)
        MyBase.Render(source)
        writer.Write(stringWriter.ToString().Replace("&amp;#", "&#").Replace("ReadOnly=""false""", "").Replace("ReadOnly=""False""", ""))
    End Sub
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports Resources.Resource
'Imports clsCommon
Imports System.Text


Public Class clsRoleGroup

    Private _roleGroupID, _roleGroupName, _localResourceKey, _roleGroupDescription, _active As String

    Public Property RoleGroupID() As String
        Get
            Return _roleGroupID
        End Get
        Set(ByVal value As String)
            _roleGroupID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RoleGroupName() As String
        Get
            Return _roleGroupName
        End Get
        Set(ByVal value As String)
            _roleGroupName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property LocalResourceKey() As String
        Get
            Return _localResourceKey
        End Get
        Set(ByVal value As String)
            _localResourceKey = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RoleGroupDescription() As String
        Get
            Return _roleGroupDescription
        End Get
        Set(ByVal value As String)
            _roleGroupDescription = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Active() As String
        Get
            Return _active
        End Get
        Set(ByVal value As String)
            _active = clsCommon.funRemove(value, True)
        End Set
    End Property
    ''' <summary>
    ''' Check DuplicateRole Group
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateRoleGroup() As Boolean
        Dim sb As New StringBuilder()
        sb.Append("SELECT COUNT(*) FROM sysrolegroups s WHERE s.RoleGroupName = '@RoleGroupName';")
        sb.Replace("@RoleGroupName", _roleGroupName)

        Dim obj As New clsDataClass
        Dim intData As Object
        intData = obj.GetScalarData(sb.ToString)
        obj.CloseDatabaseConnection()
        If Not intData <> Nothing Then
            Return Convert.ToInt32(intData) > 0
        Else
            Return False
        End If
    End Function
    ''' <summary>
    '''  Fill Grid
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim sb As New StringBuilder()
        sb.Append("SELECT s.RoleGroupID, s.RoleGroupName, s.LocalResourceKey, s.RoleGroupDescription, CASE s.Active WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as Active FROM sysrolegroups s;")

        Return sb.ToString
    End Function
    ''' <summary>
    ''' Add 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Add() As Boolean
        Dim sb As New StringBuilder()
        sb.Append("INSERT INTO sysrolegroups VALUES('@RoleGroupName', '@LocalResourceKey','@RoleGroupDescription',@Active);")
        sb.Replace("@RoleGroupName", _roleGroupName)
        sb.Replace("@LocalResourceKey", _localResourceKey)
        sb.Replace("@RoleGroupDescription", _roleGroupDescription)
        sb.Replace("@Active", _active)
        sb.Replace("'sysNull'", "Null")
        sb.Replace("'sysNull'", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(sb.ToString)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update role Group
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Update() As Boolean
        Dim sb As New StringBuilder()
        sb.Append("UPDATE sysrolegroups s SET s.RoleGroupName = '@RoleGroupName', s.LocalResourceKey = '@LocalResourceKey', s.RoleGroupDescription = '@RoleGroupDescription',s.Active = @Active WHERE (s.RoleGroupID = @RoleGroupID);")
        sb.Replace("@RoleGroupName", _roleGroupName)
        sb.Replace("@LocalResourceKey", _localResourceKey)
        sb.Replace("@RoleGroupDescription", _roleGroupDescription)
        sb.Replace("@Active", _active)
        sb.Replace("@RoleGroupID", _roleGroupID)
        sb.Replace("'sysNull'", "Null")
        sb.Replace("'sysNull'", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(sb.ToString)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Delete role Group
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Delete(ByVal id As String) As String
        Dim sb As New StringBuilder()
        sb.Append("UPDATE sysrolegroups s SET s.Active = 0 WHERE (s.RoleGroupID = @RoleGroupID);")
        sb.Replace("@RoleGroupID", id)
        sb.Replace("'sysNull'", "Null")
        sb.Replace("'sysNull'", "Null")
        Return sb.ToString
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
'Imports Resources.Resource
Public Class clsVendor
	Inherits clsDataClass
    Private _vendorID, _vendorName, _vendorEmailID, _vendorFax, _vendorPhone, _vendorPOPref, _vendorStatus, _vendorCurrency, _vendorActive, _vendorValidated, _vendorCreatedOn, _vendorLastUpdatedOn, _vendorContactPersonName, _vendorLang, _vendorContactPersonPhone, _vendorContactPersonEmailID As String
	Public Property VendorID() As String
		Get
			Return _vendorID
		End Get
		Set(ByVal value As String)
            _vendorID = clsCommon.funRemove(value, True)
		End Set
    End Property
    Private _value As String

    Public Property value() As String
        Get
            Return _value
        End Get
        Set(ByVal value As String)
            _value = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Property VendorName() As String
		Get
			Return _vendorName
		End Get
		Set(ByVal value As String)
            _vendorName = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property VendorEmailID() As String
		Get
			Return _vendorEmailID
		End Get
		Set(ByVal value As String)
            _vendorEmailID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property VendorFax() As String
		Get
			Return _vendorFax
		End Get
		Set(ByVal value As String)
            _vendorFax = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property VendorPhone() As String
		Get
			Return _vendorPhone
		End Get
		Set(ByVal value As String)
            _vendorPhone = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property VendorPOPref() As String
		Get
			Return _vendorPOPref
		End Get
		Set(ByVal value As String)
            _vendorPOPref = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property VendorStatus() As String
		Get
			Return _vendorStatus
		End Get
		Set(ByVal value As String)
            _vendorStatus = clsCommon.funRemove(value, True)
		End Set
	End Property
    Public Property VendorCurrency() As String
        Get
            Return _vendorCurrency
        End Get
        Set(ByVal value As String)
            _vendorCurrency = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Property VendorActive() As String
		Get
			Return _vendorActive
		End Get
		Set(ByVal value As String)
            _vendorActive = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property VendorValidated() As String
		Get
			Return _vendorValidated
		End Get
		Set(ByVal value As String)
            _vendorValidated = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property VendorCreatedOn() As String
		Get
			Return _vendorCreatedOn
		End Get
		Set(ByVal value As String)
            _vendorCreatedOn = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property VendorLastUpdatedOn() As String
		Get
			Return _vendorLastUpdatedOn
		End Get
		Set(ByVal value As String)
            _vendorLastUpdatedOn = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property VendorContactPersonName() As String
        Get
            Return _vendorContactPersonName
        End Get
        Set(ByVal value As String)
            _vendorContactPersonName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property VendorLang() As String
        Get
            Return _vendorLang
        End Get
        Set(ByVal value As String)
            _vendorLang = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Property vendorContactPersonPhone() As String
        Get
            Return _vendorContactPersonPhone
        End Get
        Set(ByVal value As String)
            _vendorContactPersonPhone = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property vendorContactPersonEmailID() As String
        Get
            Return _vendorContactPersonEmailID
        End Get
        Set(ByVal value As String)
            _vendorContactPersonEmailID = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    '''  Insert Vendor
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertVendor() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO vendor( vendorName, vendorEmailID, vendorFax, vendorPhone, vendorPOPref, vendorStatus, vendorCurrency, vendorActive, vendorValidated, vendorCreatedOn, vendorLastUpdatedOn, vendorContactPersonName,vendorLang,vendorContactPersonPhone, vendorContactPersonEmailID) VALUES('"
		strSQL += _vendorName + "','"
		strSQL += _vendorEmailID + "','"
		strSQL += _vendorFax + "','"
		strSQL += _vendorPhone + "','"
		strSQL += _vendorPOPref + "','"
		strSQL += _vendorStatus + "','"
        strSQL += _vendorCurrency + "','"
		strSQL += _vendorActive + "','"
		strSQL += _vendorValidated + "','"
		strSQL += _vendorCreatedOn + "','"
        strSQL += _vendorLastUpdatedOn + "','"
        strSQL += _vendorContactPersonName + "','"
        strSQL += _vendorLang + "','"
        strSQL += _vendorContactPersonPhone + "','"
        strSQL += _vendorContactPersonEmailID + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update Vendor
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateVendor() As Boolean
		Dim strSQL As String
        strSQL = "UPDATE vendor set "
		strSQL += "vendorName='" + _vendorName + "', "
		strSQL += "vendorEmailID='" + _vendorEmailID + "', "
		strSQL += "vendorFax='" + _vendorFax + "', "
		strSQL += "vendorPhone='" + _vendorPhone + "', "
		strSQL += "vendorPOPref='" + _vendorPOPref + "', "
		strSQL += "vendorStatus='" + _vendorStatus + "', "
        strSQL += "vendorCurrency='" + _vendorCurrency + "', "
		strSQL += "vendorActive='" + _vendorActive + "', "
		strSQL += "vendorValidated='" + _vendorValidated + "', "
        strSQL += "vendorCreatedOn='" + _vendorCreatedOn + "', "
        strSQL += "vendorContactPersonName='" + _vendorContactPersonName + "', "
        strSQL += "vendorLastUpdatedOn='" + _vendorLastUpdatedOn + "', "
        strSQL += "vendorLang='" + _vendorLang + "', "
        strSQL += "vendorContactPersonPhone='" + _vendorContactPersonPhone + "', "
        strSQL += "vendorContactPersonEmailID='" + _vendorContactPersonEmailID + "'"
        strSQL += " where vendorID='" + _vendorID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of Vendor
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getVendorInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
        strSQL = "SELECT  vendorName, vendorEmailID, vendorFax, vendorPhone, vendorPOPref, vendorStatus, vendorCurrency, vendorActive, vendorValidated, vendorCreatedOn, vendorLastUpdatedOn,vendorContactPersonName,vendorLang,vendorContactPersonPhone,vendorContactPersonEmailID FROM vendor where vendorID='" + _vendorID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_vendorName = drObj.Item("vendorName").ToString
			_vendorEmailID = drObj.Item("vendorEmailID").ToString
			_vendorFax = drObj.Item("vendorFax").ToString
			_vendorPhone = drObj.Item("vendorPhone").ToString
			_vendorPOPref = drObj.Item("vendorPOPref").ToString
			_vendorStatus = drObj.Item("vendorStatus").ToString
            _vendorCurrency = drObj.Item("vendorCurrency").ToString
			_vendorActive = drObj.Item("vendorActive").ToString
			_vendorValidated = drObj.Item("vendorValidated").ToString
			_vendorCreatedOn = drObj.Item("vendorCreatedOn").ToString
            _vendorLastUpdatedOn = drObj.Item("vendorLastUpdatedOn").ToString
            _vendorContactPersonName = drObj.Item("vendorContactPersonName").ToString
            _vendorLang = drObj.Item("vendorLang").ToString
            _vendorContactPersonPhone = drObj.Item("vendorContactPersonPhone").ToString
            _vendorContactPersonEmailID = drObj.Item("vendorContactPersonEmailID").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Delete Vendor
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteVendor(ByVal DeleteID As String) As String
        Dim strSQL As String
        'strSQL = "DELETE FROM vendor WHERE vendorId='" + DeleteID + "' "
        strSQL = "UPDATE vendor set vendorActive='0' WHERE vendorId='" + DeleteID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Check Duplicate Vendor
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateVendor() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM Vendor where vendorName='" & _vendorName & "' and vendorEmailID='" & _vendorEmailID & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Return Vendor ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetVendorID() As String
        Dim strSQL As String
        strSQL = "SELECT vendorId FROM Vendor where vendorName='" & _vendorName & "' and vendorEmailID='" & _vendorEmailID & "' "
        Dim strData As String = ""
        strData = GetScalarData(strSQL)
        Return strData
    End Function
    ''' <summary>
    ''' Fill grid 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        strSQL = "SELECT vendorId, vendorName, vendorPhone, vendorEmailID, vendorFax FROM vendor where vendorActive='1' "
        strSQL += " order by vendorName "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill grid with search criteria
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal StatusData As String, ByVal SearchData As String) As String
        Dim strSQL As String
        SearchData = clsCommon.funRemove(SearchData)
        strSQL = "SELECT vendorId, vendorName, vendorEmailID, vendorFax, vendorPhone FROM vendor "
        If StatusData <> "" And SearchData <> "" Then
            strSQL += " where vendorStatus='" + StatusData + "' and "
            strSQL += " ("
            strSQL += " vendorEmailID like '%" + SearchData + "%' or "
            strSQL += " vendorPhone like '%" + SearchData + "%' or "
            strSQL += " vendorName like '%" + SearchData + "%' "
            strSQL += " )"
        ElseIf StatusData <> "" And SearchData = "" Then
            strSQL += " where vendorStatus='" + StatusData + "' and "
            strSQL += " ("
            strSQL += " vendorEmailID like '%" + SearchData + "%' or "
            strSQL += " vendorPhone like '%" + SearchData + "%' or "
            strSQL += " vendorName like '%" + SearchData + "%' "
            strSQL += " )"
        ElseIf StatusData = "" Then
            strSQL += " where "
            strSQL += " ("
            strSQL += " vendorEmailID like '%" + SearchData + "%' or "
            strSQL += " vendorPhone like '%" + SearchData + "%' or "
            strSQL += " vendorName like '%" + SearchData + "%' "
            strSQL += " )"
        End If
        strSQL += " order by vendorName "
        Return strSQL
    End Function
    ''' <summary>
    ''' Populate Vendor
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub PopulateVendor(ByVal ddl As DropDownList)
        Dim strSQL As String
        strSQL = "SELECT vendorID, vendorName FROM vendor WHERE vendorActive='1' order by vendorName "
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "vendorName"
        ddl.DataValueField = "vendorID"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.SelectVendor
        ddl.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Return New Vendor ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetNewVendorID() As String
        Dim strSQL As String
        strSQL = "SELECT Max(vendorId) FROM Vendor"
        Dim strData As String = ""
        strData = GetScalarData(strSQL)
        Return strData
    End Function
    ''' <summary>
    '''  Fill grid
    ''' </summary>
    ''' <param name="searchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funfillGridVendorDetail(ByVal searchData As String) As String
        Dim strSQL As String
        strSQL = "SELECT vendorId, vendorName, (SELECT Count(*) FROM purchaseorders i where poVendorid=vendorId) as TotalPO, vendorPhone, vendorEmailID, vendorFax FROM vendor where vendorActive='1' "
        strSQL += " and vendorName like '%" & searchData & "%'"
        strSQL += " order by vendorName "
        Return strSQL
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsProductKits
	Inherits clsDataClass
	Private _prdKitID, _prdID, _prdIncludeID, _prdIncludeQty As String
	Public Property PrdKitID() As String
		Get
			Return _prdKitID
		End Get
		Set(ByVal value As String)
            _prdKitID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PrdID() As String
		Get
			Return _prdID
		End Get
		Set(ByVal value As String)
            _prdID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PrdIncludeID() As String
		Get
			Return _prdIncludeID
		End Get
		Set(ByVal value As String)
            _prdIncludeID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PrdIncludeQty() As String
		Get
			Return _prdIncludeQty
		End Get
		Set(ByVal value As String)
            _prdIncludeQty = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert ProductKits
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertProductKits() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO productkits( prdID, prdIncludeID, prdIncludeQty) VALUES("
        strSQL += "'" + _prdID + "','"
        strSQL += _prdIncludeID + "','"
        strSQL += _prdIncludeQty + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim objDataClass As New clsDataClass
        Dim intData As Int16 = 0
        intData = objDataClass.PopulateData(strSQL)
        objDataClass.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update Product Kits
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateProductKits() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE productkits set "
        strSQL += "prdID='" + _prdID + "', "
        strSQL += "prdIncludeID='" + _prdIncludeID + "', "
        strSQL += "prdIncludeQty='" + _prdIncludeQty + "'"
        strSQL += " where prdKitID='" + _prdKitID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim objDataClass As New clsDataClass
        Dim intData As Int16 = 0
        intData = objDataClass.PopulateData(strSQL)
        objDataClass.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of ProductKits
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetProductKitsInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  prdID, prdIncludeID, prdIncludeQty FROM productkits where prdKitID='" + _prdKitID + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _prdID = drObj.Item("prdID").ToString
            _prdIncludeID = drObj.Item("prdIncludeID").ToString
            _prdIncludeQty = drObj.Item("prdIncludeQty").ToString
        End While
        CloseDatabaseConnection()
        drObj.Close()
    End Sub
    ''' <summary>
    ''' sub Fill ProductKit
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subFillProductKit() As String
        Dim strSql As String = "select prdKitID,prdID,prdIncludeID,prdIncludeQty,pd.prdName,prdIntID from productkits "
        strSql += " Inner Join prddescriptions as pd on pd.id=productkits.prdIncludeID Inner Join products as p on p.productID=productkits.prdIncludeID where prdID ='" & HttpContext.Current.Request.QueryString("PrdID") & "' and descLang='en'"
        'where descLang='" + Current.Session("Lang") + "' "
        Return strSql
    End Function
    ''' <summary>
    ''' sub Product Kit Delete
    ''' </summary>
    ''' <param name="strPrdKitID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subPrdKitDelete(ByVal strPrdKitID As String) As String
        Dim strSql As String = "delete from productkits where prdKitID='" & strPrdKitID & "' "
        Return strSql
    End Function
    ''' <summary>
    ''' Find duplicate product kit Name
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDuplicateProductKit() As Boolean
        Dim strSQL As String
        Dim objDataClass As New clsDataClass
        strSQL = "SELECT count(*) FROM productkits where prdID='" & _prdID & "' And prdIncludeID ='" & _prdIncludeID & "'"
        If objDataClass.GetScalarData(strSQL) <> 0 Then
            objDataClass = Nothing
            Return True
        Else
            objDataClass = Nothing
            Return False
        End If
    End Function
    ''' <summary>
    ''' ProductKits for given ProductId
    ''' </summary>
    ''' <param name="PrdId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetProductKitsInfoByPrdId(ByVal PrdId As String) As OdbcDataReader
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT * FROM productkits where prdID='" + PrdId + "' "
        drObj = GetDataReader(strSQL)
        Return drObj
    End Function
End Class

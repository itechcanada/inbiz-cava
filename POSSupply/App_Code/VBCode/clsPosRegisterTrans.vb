Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon

Public Class clsPosRegisterTrans
    Private _RegTransID, _RegCode, _RegStartDateTime, _RegCloseDateTime, _RegStartAmount, _RegCloseAmount, _RegMgr, _RegPOSUser As String, _RegCalulatedCash As String, _RegisterCode As String, _Status As String

#Region "Properties"
    Public Property RegTransID() As String
        Get
            Return _RegTransID
        End Get
        Set(ByVal value As String)
            _RegTransID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegCode() As String
        Get
            Return _RegCode
        End Get
        Set(ByVal value As String)
            _RegCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegStartDateTime() As String
        Get
            Return _RegStartDateTime
        End Get
        Set(ByVal value As String)
            _RegStartDateTime = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegCloseDateTime() As String
        Get
            Return _RegCloseDateTime
        End Get
        Set(ByVal value As String)
            _RegCloseDateTime = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegStartAmount() As String
        Get
            Return _RegStartAmount
        End Get
        Set(ByVal value As String)
            _RegStartAmount = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegCloseAmount() As String
        Get
            Return _RegCloseAmount
        End Get
        Set(ByVal value As String)
            _RegCloseAmount = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegMgr() As String
        Get
            Return _RegMgr
        End Get
        Set(ByVal value As String)
            _RegMgr = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegPOSUser() As String
        Get
            Return _RegPOSUser
        End Get
        Set(ByVal value As String)
            _RegPOSUser = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Property RegCalculatedCash() As String
        Get
            Return _RegCalulatedCash
        End Get
        Set(ByVal value As String)
            _RegCalulatedCash = clsCommon.funRemove(value, True)
        End Set
    End Property


    Public Property Status() As String
        Get
            Return _Status
        End Get
        Set(ByVal value As String)
            _Status = clsCommon.funRemove(value, True)
        End Set
    End Property


    Public Property RegisterCode() As String
        Get
            Return _RegisterCode
        End Get
        Set(ByVal value As String)
            _RegisterCode = clsCommon.funRemove(value, True)
        End Set
    End Property

#End Region

    Public Sub New()
        MyBase.New()
    End Sub
#Region "Function"
    ''' <summary>
    '''  Insert posregistertrans
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertRegiTran() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO posregistertrans(RegCode, RegStartDateTime, RegCloseDateTime, RegStartAmount, RegCloseAmount, RegMgr, RegPOSUser, lastUpdatedDateTime, lastUpdatedbyUser) VALUES('"
        strSQL += _RegCode + "','"
        strSQL += _RegStartDateTime + "','"
        strSQL += _RegCloseDateTime + "','"
        strSQL += _RegStartAmount + "','"
        strSQL += _RegCloseAmount + "','"
        strSQL += _RegMgr + "','"
        strSQL += _RegPOSUser + "','"
        strSQL += Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss") + "','"
        strSQL += _RegMgr + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        RegTransID = obj.GetScalarData("Select LAST_INSERT_ID()")
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update posregistertrans
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateRegiTran() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE posregistertrans set "
        ' strSQL += "RegCode='" + _RegCode + "', "
        ' strSQL += "RegStartDateTime='" + _RegStartDateTime + "', "
        strSQL += "RegCloseDateTime='" + _RegCloseDateTime + "',"
        strSQL += "RegStartAmount='" + _RegStartAmount + "',"
        strSQL += "RegCloseAmount='" + _RegCloseAmount + "',"
        strSQL += "RegMgr='" + _RegMgr + "', "
        strSQL += "lastUpdatedDateTime='" + Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss") + "', "
        strSQL += "lastUpdatedbyUser='" + _RegMgr + "'"
        ' strSQL += "RegPOSUser='" + _RegPOSUser + "'"
        strSQL += " where RegTransID='" + _RegTransID + "'"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of posregistertrans
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetRegiTran()
        Dim strSQL As String
        Dim obj As New clsDataClass
        Dim drObj As OdbcDataReader
        strSQL = "SELECT *, funOpenRegisterCalcAmount(RegTransID) AS CalculatedCash FROM posregistertrans where RegTransID='" + _RegTransID + "'"
        drObj = obj.GetDataReader(strSQL)
        While drObj.Read
            _RegCode = drObj.Item("RegCode").ToString
            _RegStartDateTime = drObj.Item("RegStartDateTime").ToString
            _RegCloseDateTime = drObj.Item("RegCloseDateTime").ToString
            _RegStartAmount = drObj.Item("RegStartAmount").ToString
            _RegCloseAmount = drObj.Item("RegCloseAmount").ToString
            _RegMgr = drObj.Item("RegMgr").ToString
            _RegPOSUser = drObj.Item("RegPOSUser").ToString
            _RegCalulatedCash = drObj.Item("CalculatedCash").ToString
        End While
        drObj.Close()
        obj.CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' fill Grid Sql
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String 'ByVal strStatus As String, ByVal strRegCode As String
        Dim strSql As String = "select RegTransID,RegCode,sysRegDesc, concat(users.userFirstName,' ',users.userLastName) as UserName, RegStartDateTime, RegCloseDateTime, IFNULL(RegStartAmount,0) AS RegStartAmount, IFNULL(RegCloseAmount,0) AS RegCloseAmount, RegMgr, RegPOSUser, lastUpdatedDateTime, concat(usrUpdate.userFirstName,' ',usrUpdate.userLastName) as lastUpdatedbyUser, funOpenRegisterCalcAmount(RegTransID) AS CalculatedCash "
        strSql += " FROM posregistertrans "
        strSql += " Left join sysregister on sysregister.sysRegCode=posregistertrans.RegCode "
        strSql += " Left join users on users.UserID=posregistertrans.RegMgr "
        strSql += " Left join users AS usrUpdate on usrUpdate.UserID=posregistertrans.lastUpdatedbyUser "
        strSql += " WHERE 1=1  "
        If (Status = "O") Then
            'strSql += " AND RegCloseDateTime IS NULL  "
            strSql += " AND IFNULL( RegCloseDateTime,'') ='' "
        End If
        If (Not String.IsNullOrEmpty(RegisterCode) And RegisterCode <> "sysNull") Then
            strSql += " AND RegCode= '" + RegisterCode + "' "
        End If
        strSql += " order by RegStartDateTime desc"
        Return strSql
    End Function
    ''' <summary>
    ''' Get close amount
    ''' </summary>
    ''' <param name="strStartAmt"></param>
    ''' <param name="strRegiTranID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCalCloseAmt(ByVal strStartAmt As String, ByVal strRegiTranID As String) As String
        Dim strSQL, strCloseAmout, strTotalAmout As String
        strTotalAmout = ""
        Dim objDataClass As New clsDataClass
        strSQL = "SELECT sum(posTotalValue) FROM posregistertrans   "
        strSQL += " inner Join postransaction on postransaction.posTransRegCode=posregistertrans.RegCode "
        strSQL += " inner Join postransaccthdr on postransaccthdr.posAcctTransID=postransaction.posTransId "
        strSQL += " where RegTransID='" & strRegiTranID & "' and (posAcctPayType='1' Or posAcctPayType='6') and posTransRegCode ='" & HttpContext.Current.Session("RegCode") & "' and posTransStatus='1' and "
        strSQL += " posTransDateTime >=RegStartDateTime and posTransDateTime <='" & Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss") & "'  "
        strCloseAmout = objDataClass.GetScalarData(strSQL).ToString
        If strCloseAmout <> "" Then
            strTotalAmout = CDbl(strCloseAmout) + CDbl(strStartAmt)
        End If
        Return strTotalAmout
    End Function
    ''' <summary>
    '''Get check close amount
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckCloseAmt() As Boolean
        Dim strSQL, strCloseAmout As String
        Dim objDataClass As New clsDataClass
        strSQL = "SELECT count(*) FROM posregistertrans   "
        strSQL += " where  RegCode ='" & HttpContext.Current.Session("RegCode") & "' AND IFNULL( RegCloseDateTime,'') =''  order by RegTransID desc limit 1 " 'and (RegCloseAmount is null or RegCloseAmount='')
        strCloseAmout = objDataClass.GetScalarData(strSQL)
        If strCloseAmout >= 1 Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region
End Class

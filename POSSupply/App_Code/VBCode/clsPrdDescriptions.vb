Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsPrdDescriptions
	Inherits clsDataClass
    Private _id, _descLang, _prdSmallDesc, _prdLargeDesc, _prdName As String
	Public Property Id() As String
		Get
			Return _id
		End Get
		Set(ByVal value As String)
            _id = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property DescLang() As String
		Get
			Return _descLang
		End Get
		Set(ByVal value As String)
            _descLang = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PrdSmallDesc() As String
		Get
			Return _prdSmallDesc
		End Get
		Set(ByVal value As String)
            _prdSmallDesc = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PrdLargeDesc() As String
		Get
			Return _prdLargeDesc
		End Get
		Set(ByVal value As String)
            _prdLargeDesc = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property PrdName() As String
        Get
            Return _prdName
        End Get
        Set(ByVal value As String)
            _prdName = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert PrdDescriptions
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertPrdDescriptions() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO prddescriptions(id, descLang,PrdName, prdSmallDesc, prdLargeDesc) VALUES("
        strSQL += "'" + _id + "','"
        strSQL += _descLang + "','"
        strSQL += _prdName + "','"
        strSQL += _prdSmallDesc + "','"
        strSQL += _prdLargeDesc + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update PrdDescriptions
    ''' </summary>
    ''' <param name="strPageType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdatePrdDescriptions(Optional ByVal strPageType As String = "") As Boolean
        Dim strSQL As String
        Dim obj As New clsDataClass
        strSQL = "select count(*) from prddescriptions where id='" + _id + "' and descLang ='" + _descLang + "'"
        If obj.GetScalarData(strSQL) = 0 Then
            funInsertPrdDescriptions()
            Exit Function
        End If
        strSQL = "UPDATE prddescriptions set "
        strSQL += "descLang='" + _descLang + "',"
        If strPageType = "" Then
            strSQL += " prdSmallDesc='" + _prdSmallDesc + "', "
            strSQL += "prdLargeDesc='" + _prdLargeDesc + "',"
        End If
        strSQL += "PrdName='" + _prdName + "'"
        strSQL += " where id='" + _id + "' and descLang ='" + _descLang + "'"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of PrdDescriptions
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetPrdDescriptionsInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  descLang,PrdName, prdSmallDesc, prdLargeDesc FROM prddescriptions where id='" + _id + "' and descLang ='" + _descLang + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _descLang = drObj.Item("descLang").ToString
            _prdName = drObj.Item("PrdName").ToString
            _prdSmallDesc = drObj.Item("prdSmallDesc").ToString
            _prdLargeDesc = drObj.Item("prdLargeDesc").ToString
        End While
        CloseDatabaseConnection()
        drObj.Close()
    End Sub
    ''' <summary>
    ''' get Product Name
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funProductName() As String
        Dim strSQL, strPrdName As String
        strSQL = "SELECT PrdName FROM prddescriptions where id='" + HttpContext.Current.Request.QueryString("PrdID") + "' and descLang ='" + clsCommon.funGetProductLang() + "' "
        Dim objDataClass As New clsDataClass
        strPrdName = objDataClass.GetScalarData(strSQL)
        objDataClass = Nothing
        If strPrdName <> "" Then
            Return strPrdName & " /"
        End If
        Return strPrdName
    End Function
End Class

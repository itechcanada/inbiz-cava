Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsInvItemProcess
	Inherits clsDataClass
	Private _invItemProcID, _invoices_invID, _invItemProcCode, _invItemProcFixedPrice, _invItemProcPricePerHour, _invItemProcHours, _invItemProcPricePerUnit, _invItemProcUnits As String
	Public Property InvItemProcID() As String
		Get
			Return _invItemProcID
		End Get
		Set(ByVal value As String)
            _invItemProcID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property Invoices_invID() As String
		Get
			Return _invoices_invID
		End Get
		Set(ByVal value As String)
            _invoices_invID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property InvItemProcCode() As String
		Get
			Return _invItemProcCode
		End Get
		Set(ByVal value As String)
            _invItemProcCode = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property InvItemProcFixedPrice() As String
		Get
			Return _invItemProcFixedPrice
		End Get
		Set(ByVal value As String)
            _invItemProcFixedPrice = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property InvItemProcPricePerHour() As String
		Get
			Return _invItemProcPricePerHour
		End Get
		Set(ByVal value As String)
            _invItemProcPricePerHour = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property InvItemProcHours() As String
		Get
			Return _invItemProcHours
		End Get
		Set(ByVal value As String)
            _invItemProcHours = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property InvItemProcPricePerUnit() As String
		Get
			Return _invItemProcPricePerUnit
		End Get
		Set(ByVal value As String)
            _invItemProcPricePerUnit = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property InvItemProcUnits() As String
		Get
			Return _invItemProcUnits
		End Get
		Set(ByVal value As String)
            _invItemProcUnits = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    '''  Insert InvItemProcess
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertInvItemProcess() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO invitemprocess( invoices_invID, invItemProcCode, invItemProcFixedPrice, invItemProcPricePerHour, invItemProcHours, invItemProcPricePerUnit, invItemProcUnits) VALUES('"
		strSQL += _invoices_invID + "','"
		strSQL += _invItemProcCode + "','"
		strSQL += _invItemProcFixedPrice + "','"
		strSQL += _invItemProcPricePerHour + "','"
		strSQL += _invItemProcHours + "','"
		strSQL += _invItemProcPricePerUnit + "','"
		strSQL += _invItemProcUnits + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update InvItemProcess
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateInvItemProcess() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE invitemprocess set "
		strSQL += "invoices_invID='" + _invoices_invID + "', "
		strSQL += "invItemProcCode='" + _invItemProcCode + "', "
		strSQL += "invItemProcFixedPrice='" + _invItemProcFixedPrice + "', "
		strSQL += "invItemProcPricePerHour='" + _invItemProcPricePerHour + "', "
		strSQL += "invItemProcHours='" + _invItemProcHours + "', "
		strSQL += "invItemProcPricePerUnit='" + _invItemProcPricePerUnit + "', "
		strSQL += "invItemProcUnits='" + _invItemProcUnits + "'"
		strSQL += " where invItemProcID='" + _invItemProcID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of InvItemProcess
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getInvItemProcessInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  invoices_invID, invItemProcCode, invItemProcFixedPrice, invItemProcPricePerHour, invItemProcHours, invItemProcPricePerUnit, invItemProcUnits FROM invitemprocess where invItemProcID='" + _invItemProcID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_invoices_invID = drObj.Item("invoices_invID").ToString
			_invItemProcCode = drObj.Item("invItemProcCode").ToString
			_invItemProcFixedPrice = drObj.Item("invItemProcFixedPrice").ToString
			_invItemProcPricePerHour = drObj.Item("invItemProcPricePerHour").ToString
			_invItemProcHours = drObj.Item("invItemProcHours").ToString
			_invItemProcPricePerUnit = drObj.Item("invItemProcPricePerUnit").ToString
			_invItemProcUnits = drObj.Item("invItemProcUnits").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate Invoice Item Process Object and Insert
    ''' </summary>
    ''' <param name="SOID"></param>
    ''' <param name="sInvID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funPopulateInvoiceItemProcess(ByVal SOID As String, ByVal sInvID As String) As Boolean
        Dim bFlag As Boolean = False
        Dim strSQL As String = ""
        Dim drObj As OdbcDataReader
        Dim objSOIP As New clsOrderItemProcess
        objSOIP.OrdID = SOID
        strSQL = objSOIP.funFillGridForProcess 'fill Order Items Process
        drObj = GetDataReader(strSQL)
        If drObj.HasRows = False Then
            bFlag = True
        End If
        While drObj.Read
            _invoices_invID = sInvID
            _invItemProcCode = drObj.Item("ordItemProcCode").ToString
            _invItemProcFixedPrice = drObj.Item("InOrdItemProcFixedPrice").ToString
            _invItemProcPricePerHour = drObj.Item("InOrdItemProcPricePerHour").ToString
            _invItemProcHours = drObj.Item("ordItemProcHours").ToString
            _invItemProcPricePerUnit = drObj.Item("InOrdItemProcPricePerUnit").ToString
            _invItemProcUnits = drObj.Item("ordItemProcUnits").ToString
            bFlag = insertInvItemProcess()
        End While
        drObj.Close()
        CloseDatabaseConnection()
        Return bFlag
    End Function

    ''' <summary>
    ''' Fill grid 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGridForProcess() As String
        Dim strSQL As String
        strSQL = "SELECT invItemProcID, invoices_invID, invItemProcCode, ProcessDescription, (invItemProcFixedPrice*invCurrencyExRate) as invItemProcFixedPrice, (invItemProcPricePerHour*invCurrencyExRate) as invItemProcPricePerHour,(invItemProcPricePerUnit*invCurrencyExRate) as invItemProcPricePerUnit,"
        ' strSQL += " invItemProcFixedPrice as PdfinvItemProcFixedPrice,invItemProcPricePerHour as PdfinvItemProcPricePerHour,invItemProcPricePerUnit as PdfinvItemProcPricePerUnit,"
        strSQL += " invItemProcHours,  invItemProcUnits, (invItemProcFixedPrice+(invItemProcPricePerHour*invItemProcHours)+(invItemProcPricePerUnit*invItemProcUnits))*invCurrencyExRate as ProcessCost FROM invitemprocess inner join invoices on invID=invoices_invID inner join sysprocessgroup on ProcessCode=invItemProcCode where invoices_invID='" & _invoices_invID & "' "
        strSQL += " order by ProcessDescription "
        Return strSQL
    End Function
    ''' <summary>
    ''' Get Process Cost For Invoice No
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getProcessCostForInvoiceNo() As String
        Dim strSQL As String
        Dim strData As String = ""
        strSQL = "SELECT sum(invItemProcFixedPrice+(invItemProcPricePerHour*invItemProcHours)+(invItemProcPricePerUnit*invItemProcUnits))*invCurrencyExRate as ProcessCost FROM invitemprocess inner join invoices on invID=invoices_invID where invoices_invID='" & _invoices_invID & "' group by invoices_invID "
        Try
            strData = GetScalarData(strSQL).ToString
        Catch ex As NullReferenceException
            strData = "0"
        Catch ex As Exception
            strData = "0"
        End Try
        Return strData
    End Function
    ''' <summary>
    ''' Delete invoice Item Process
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteInvItemProcess() As String
        Dim strSQL As String
        strSQL = "DELETE FROM invitemprocess WHERE invItemProcID='" + _invItemProcID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Duplicate Docno
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDuplicateProcess() As Boolean
        Dim strSql As String
        strSql = "SELECT count(*) FROM invitemprocess "
        strSql += " where invItemProcCode='" + _invItemProcCode + "' and invoices_invID='" + _invoices_invID + "' "
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.GetScalarData(strSql)
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
End Class

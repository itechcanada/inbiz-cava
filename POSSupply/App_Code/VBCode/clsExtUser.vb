Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports iTECH.InbizERP.BusinessLogic
''Imports System.Web.HttpContext
''Imports Resources.Resource
''Imports clsCommon
Public Class clsExtUser
	Inherits clsDataClass
    Private _extUserID, _userLoginId, _userPassword, _userSalutation, _userFirstName, _userLastName, _userEmail, _userDesignation, _userDepartment, _userPhone, _userPhoneExt, _userFax, _userCellPhone, _userRoleID, _userType, _userOrgID, _userModules, _userStatus, _userLastUpdatedAt, _userActive, _userValidated, _userLang As String
    Private _custID, _custName, _custType, _custNetTerms, _custCurrency, _custEmail, _custFax, _custPhone, _custTaxCode, _custAcronyme, _PartnerNote As String
	Public Property ExtUserID() As String
		Get
			Return _extUserID
		End Get
		Set(ByVal value As String)
            _extUserID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserLoginId() As String
        Get
            Return _userLoginId
        End Get
        Set(ByVal value As String)
            _userLoginId = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserPassword() As String
        Get
            Return _userPassword
        End Get
        Set(ByVal value As String)
            _userPassword = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserSalutation() As String
        Get
            Return _userSalutation
        End Get
        Set(ByVal value As String)
            _userSalutation = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserFirstName() As String
        Get
            Return _userFirstName
        End Get
        Set(ByVal value As String)
            _userFirstName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserLastName() As String
        Get
            Return _userLastName
        End Get
        Set(ByVal value As String)
            _userLastName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserEmail() As String
        Get
            Return _userEmail
        End Get
        Set(ByVal value As String)
            _userEmail = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserDesignation() As String
        Get
            Return _userDesignation
        End Get
        Set(ByVal value As String)
            _userDesignation = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserDepartment() As String
        Get
            Return _userDepartment
        End Get
        Set(ByVal value As String)
            _userDepartment = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserPhone() As String
        Get
            Return _userPhone
        End Get
        Set(ByVal value As String)
            _userPhone = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserPhoneExt() As String
        Get
            Return _userPhoneExt
        End Get
        Set(ByVal value As String)
            _userPhoneExt = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserFax() As String
        Get
            Return _userFax
        End Get
        Set(ByVal value As String)
            _userFax = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserCellPhone() As String
        Get
            Return _userCellPhone
        End Get
        Set(ByVal value As String)
            _userCellPhone = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserRoleID() As String
        Get
            Return _userRoleID
        End Get
        Set(ByVal value As String)
            _userRoleID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserType() As String
        Get
            Return _userType
        End Get
        Set(ByVal value As String)
            _userType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserOrgID() As String
        Get
            Return _userOrgID
        End Get
        Set(ByVal value As String)
            _userOrgID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserModules() As String
        Get
            Return _userModules
        End Get
        Set(ByVal value As String)
            _userModules = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserStatus() As String
        Get
            Return _userStatus
        End Get
        Set(ByVal value As String)
            _userStatus = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserLastUpdatedAt() As String
        Get
            Return _userLastUpdatedAt
        End Get
        Set(ByVal value As String)
            _userLastUpdatedAt = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserActive() As String
        Get
            Return _userActive
        End Get
        Set(ByVal value As String)
            _userActive = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserValidated() As String
        Get
            Return _userValidated
        End Get
        Set(ByVal value As String)
            _userValidated = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserLang() As String
        Get
            Return _userLang
        End Get
        Set(ByVal value As String)
            _userLang = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustID() As String
        Get
            Return _custID
        End Get
        Set(ByVal value As String)
            _custID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustName() As String
        Get
            Return _custName
        End Get
        Set(ByVal value As String)
            _custName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustType() As String
        Get
            Return _custType
        End Get
        Set(ByVal value As String)
            _custType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustNetTerms() As String
        Get
            Return _custNetTerms
        End Get
        Set(ByVal value As String)
            _custNetTerms = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustCurrency() As String
        Get
            Return _custCurrency
        End Get
        Set(ByVal value As String)
            _custCurrency = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustEmail() As String
        Get
            Return _custEmail
        End Get
        Set(ByVal value As String)
            _custEmail = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustFax() As String
        Get
            Return _custFax
        End Get
        Set(ByVal value As String)
            _custFax = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustPhone() As String
        Get
            Return _custPhone
        End Get
        Set(ByVal value As String)
            _custPhone = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustTaxCode() As String
        Get
            Return _custTaxCode
        End Get
        Set(ByVal value As String)
            _custTaxCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustAcronyme() As String
        Get
            Return _custAcronyme
        End Get
        Set(ByVal value As String)
            _custAcronyme = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PartnerNote() As String
        Get
            Return _PartnerNote
        End Get
        Set(ByVal value As String)
            _PartnerNote = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Insert ExtUser
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertExtUser() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO extusers( userLoginId, userPassword, userSalutation, userFirstName, userLastName, userEmail, userDesignation, userDepartment, userPhone, userPhoneExt, userFax, userCellPhone, userRoleID, userType, userOrgID, userModules, userStatus, userLastUpdatedAt, userActive, userValidated, userLang) VALUES('"
        strSQL += _userLoginId + "',password('"
        strSQL += _userPassword + "'),'"
        strSQL += _userSalutation + "','"
        strSQL += _userFirstName + "','"
        strSQL += _userLastName + "','"
        strSQL += _userEmail + "','"
        strSQL += _userDesignation + "','"
        strSQL += _userDepartment + "','"
        strSQL += _userPhone + "','"
        strSQL += _userPhoneExt + "','"
        strSQL += _userFax + "','"
        strSQL += _userCellPhone + "','"
        strSQL += _userRoleID + "','"
        strSQL += _userType + "','"
        strSQL += _userOrgID + "','"
        strSQL += _userModules + "','"
        strSQL += _userStatus + "','"
        strSQL += _userLastUpdatedAt + "','"
        strSQL += _userActive + "','"
        strSQL += _userValidated + "','"
        strSQL += _userLang + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update ExtUser
    ''' </summary>
    ''' <param name="nUpdatePassword"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateExtUser(ByVal nUpdatePassword As Int16) As Boolean
        Dim strSQL As String
        strSQL = "UPDATE extusers set "
        strSQL += "userLoginId='" + _userLoginId + "', "
        If nUpdatePassword = 1 Then
            strSQL += "userPassword=password('" + _userPassword + "'), "
        End If
        strSQL += "userSalutation='" + _userSalutation + "', "
        strSQL += "userFirstName='" + _userFirstName + "', "
        strSQL += "userLastName='" + _userLastName + "', "
        strSQL += "userEmail='" + _userEmail + "', "
        strSQL += "userDesignation='" + _userDesignation + "', "
        strSQL += "userDepartment='" + _userDepartment + "', "
        strSQL += "userPhone='" + _userPhone + "', "
        strSQL += "userPhoneExt='" + _userPhoneExt + "', "
        strSQL += "userFax='" + _userFax + "', "
        strSQL += "userCellPhone='" + _userCellPhone + "', "
        strSQL += "userRoleID='" + _userRoleID + "', "
        strSQL += "userType='" + _userType + "', "
        strSQL += "userOrgID='" + _userOrgID + "', "
        strSQL += "userModules='" + _userModules + "', "
        strSQL += "userStatus='" + _userStatus + "', "
        strSQL += "userLastUpdatedAt='" + _userLastUpdatedAt + "', "
        strSQL += "userActive='" + _userActive + "', "
        strSQL += "userValidated='" + _userValidated + "', "
        strSQL += "userLang='" + _userLang + "' "
        strSQL += " where extUserID='" + _extUserID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of ExtUser
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getExtUserInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  userLoginId, userPassword, userSalutation, userFirstName, userLastName, userEmail, userDesignation, userDepartment, userPhone, userPhoneExt, userFax, userCellPhone, userRoleID, userType, userOrgID, userModules, userStatus, userLastUpdatedAt, userActive, userValidated, userLang FROM extusers where extUserID='" + _extUserID + "' "
        drObj = GetDataReader(strSql)
        While drObj.Read
            _userLoginId = drObj.Item("userLoginId").ToString
            _userPassword = drObj.Item("userPassword").ToString
            _userSalutation = drObj.Item("userSalutation").ToString
            _userFirstName = drObj.Item("userFirstName").ToString
            _userLastName = drObj.Item("userLastName").ToString
            _userEmail = drObj.Item("userEmail").ToString
            _userDesignation = drObj.Item("userDesignation").ToString
            _userDepartment = drObj.Item("userDepartment").ToString
            _userPhone = drObj.Item("userPhone").ToString
            _userPhoneExt = drObj.Item("userPhoneExt").ToString
            _userFax = drObj.Item("userFax").ToString
            _userCellPhone = drObj.Item("userCellPhone").ToString
            _userRoleID = drObj.Item("userRoleID").ToString
            _userType = drObj.Item("userType").ToString
            _userOrgID = drObj.Item("userOrgID").ToString
            _userModules = drObj.Item("userModules").ToString
            _userStatus = drObj.Item("userStatus").ToString
            _userLastUpdatedAt = drObj.Item("userLastUpdatedAt").ToString
            _userActive = drObj.Item("userActive").ToString
            _userValidated = drObj.Item("userValidated").ToString
            _userLang = drObj.Item("userLang").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Validate User
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidateUser() As Boolean
        Dim strSQL As String
        strSQL = "SELECT Count(*) FROM extusers WHERE userEmail='" & _userEmail & "' "
        strSQL &= "AND userPassword=password('" & _userPassword & "') And userActive=1 "
        'Check Valid User
        If (Convert.ToUInt16(GetScalarData(strSQL)) > 0) Then
            Dim drObj As System.Data.Odbc.OdbcDataReader
            drObj = GetDataReader("SELECT extUserID,userLoginID,userEmail,userModules FROM extusers WHERE userEmail='" & _userEmail & "' AND userPassword=password('" & _userPassword & "')")
            While drObj.Read
                HttpContext.Current.Session("UserID") = drObj.Item("extUserID").ToString
                HttpContext.Current.Session("LoginID") = drObj.Item("userLoginID").ToString
                HttpContext.Current.Session("EmailID") = drObj.Item("userEmail").ToString
                HttpContext.Current.Session("UserModules") = drObj.Item("userModules").ToString
            End While
            drObj.Close()
            CloseDatabaseConnection()
            Return True
        Else
            CloseDatabaseConnection()
            Return False
        End If
    End Function
    ''' <summary>
    ''' Check Duplicate User Email ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateUserEmailID() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM extusers where userEmail='" & _userEmail & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Delete User
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteUser(ByVal DeleteID As String) As String
        Dim strSQL As String
        strSQL = "DELETE FROM extusers WHERE extUserId='" + DeleteID + "' "
        Return strSql
    End Function
    ''' <summary>
    ''' Fill Grid of User
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        strSQL = "SELECT extUserId, userLoginId, userEmail, userModules, userType, concat(userFirstName,' ',userLastName) as userName, CASE userActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as userActive FROM extusers order by userEmail "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid of User with search criteria
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal StatusData As String, ByVal SearchData As String) As String
        Dim strSQL As String
        SearchData = clsCommon.funRemove(SearchData)
        strSQL = "SELECT extUserId, userLoginId, userEmail, userModules, userType, concat(userFirstName,' ',userLastName) as userName, CASE userActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as userActive FROM extusers "
        If StatusData <> "" And SearchData <> "" Then
            strSQL += " where userStatus='" + StatusData + "' and "
            strSQL += " ("
            strSQL += " userEmail like '%" + SearchData + "%' or "
            strSQL += " concat(userFirstName,' ',userLastName) like '%" + SearchData + "%' or "
            strSQL += " userPhone like '%" + SearchData + "%' or "
            strSQL += " userOrgID like '%" + SearchData + "%' "
            strSQL += " )"
        ElseIf StatusData = "" Then
            strSQL += " where "
            strSQL += " ("
            strSQL += " userEmail like '%" + SearchData + "%' or "
            strSQL += " concat(userFirstName,' ',userLastName) like '%" + SearchData + "%' or "
            strSQL += " userPhone like '%" + SearchData + "%' or "
            strSQL += " userOrgID like '%" + SearchData + "%' "
            strSQL += " )"
        End If
        strSQL += " order by userEmail "
        Return strSQL
    End Function
    ''' <summary>
    ''' Populate External User
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub PopulateUser(ByVal ddl As DropDownList)
        Dim strSQL As String
        strSQL = "SELECT extUserId, concat(userFirstName,' ',userLastName) as userName FROM extusers WHERE userActive='1' order by userName "
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "userName"
        ddl.DataValueField = "extUserId"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = "----Select External User----"
        ddl.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate Distributor, Reseller and EndClient
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub PopulateCustomer(ByVal ddl As DropDownList)
        Dim strSQL As String
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = " SELECT PartnerID as ID, PartnerLongName as CustomerName, PartnerEmail FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID where PartnerActive='1'  "
        Else
            strSQL = " SELECT PartnerID as ID, PartnerLongName as CustomerName, PartnerEmail FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID inner join salesrepcustomer s on s.CustomerID=partners.PartnerID  and s.UserID='" & HttpContext.Current.Session("UserID") & "' where PartnerActive='1' "
        End If
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "CustomerName"
        ddl.DataValueField = "ID"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.liSOSelectCustomer
        ddl.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate objects of Customer
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getCustomerInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = " SELECT PartnerID as ID, PartnerLongName as CustomerName, PartnerEmail as Email, PartnerInvoiceNetTerms as NetTerms, PartnerSelTypeID as CustType,  PartnerCurrencyCode as Currency, PartnerFax as Fax,PartnerTaxCode,PartnerNote FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID where PartnerActive='1' and PartnerID='" & _custID & "'" 'and PartnerLongName='" & _custName & "'
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _custID = drObj.Item("ID").ToString
            _custName = drObj.Item("CustomerName").ToString
            If drObj.Item("CustType").ToString = "1" Then
                _custType = AddressReference.DISTRIBUTER
            ElseIf drObj.Item("CustType").ToString = "2" Then
                _custType = AddressReference.END_CLIENT
            ElseIf drObj.Item("CustType").ToString = "3" Then
                _custType = AddressReference.RESELLER
            ElseIf drObj.Item("CustType").ToString = "4" Then
                _custType = AddressReference.LEADS
            End If
            _custNetTerms = drObj.Item("NetTerms").ToString
            _custCurrency = drObj.Item("Currency").ToString
            _custEmail = drObj.Item("Email").ToString
            _custFax = drObj.Item("Fax").ToString
            _custTaxCode = drObj.Item("PartnerTaxCode").ToString
            _PartnerNote = drObj.Item("PartnerNote").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' return Customer Name
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getCustomerInfoByType()
        Dim strSQL As String = ""
        Dim drObj As OdbcDataReader
        strSQL = " SELECT PartnerID as ID, PartnerLongName as CustomerName, PartnerEmail as Email, PartnerInvoiceNetTerms as NetTerms, PartnerSelTypeID as CustType,  PartnerCurrencyCode as Currency, PartnerFax as Fax,PartnerPhone as Phone,PartnerTaxCode, partnerAcronyme As Acronyme FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID where PartnerID='" & _custID & "'" 'PartnerActive='1' and
        If _custType = AddressReference.DISTRIBUTER Or _custType = "1" Then
            strSQL += " and PartnerSelTypeID ='1'"
        ElseIf _custType = AddressReference.RESELLER Or _custType = "3" Then
            strSQL += " and PartnerSelTypeID ='3'"
        ElseIf _custType = AddressReference.END_CLIENT Or _custType = "2" Then
            strSQL += " and PartnerSelTypeID ='2'"
        ElseIf _custType = AddressReference.LEADS Or _custType = "4" Then
            strSQL += " and PartnerSelTypeID ='4'"
        End If

        drObj = GetDataReader(strSQL)
        While drObj.Read
            _custID = drObj.Item("ID").ToString
            _custName = drObj.Item("CustomerName").ToString
            If drObj.Item("CustType").ToString = "1" Then
                _custType = AddressReference.DISTRIBUTER
            ElseIf drObj.Item("CustType").ToString = "2" Then
                _custType = AddressReference.END_CLIENT
            ElseIf drObj.Item("CustType").ToString = "3" Then
                _custType = AddressReference.RESELLER
            End If
            _custNetTerms = drObj.Item("NetTerms").ToString
            _custCurrency = drObj.Item("Currency").ToString
            _custEmail = drObj.Item("Email").ToString
            _custPhone = drObj.Item("Phone").ToString
            _custFax = drObj.Item("Fax").ToString
            _custTaxCode = drObj.Item("PartnerTaxCode").ToString
            _custAcronyme = drObj.Item("Acronyme").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Search Customer Detail
    ''' </summary>
    ''' <param name="searchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funfillGridCustomerDetail(ByVal searchData As String) As String
        Dim strSQL As String = ""
        searchData = clsCommon.funRemove(searchData)
        strSQL = "SELECT PartnerID, PartnerAcronyme, PartnerLongName,(SELECT Count(*) FROM orders o where ordCustID =PartnerID) as TotalOrders, "
        strSQL += " (SELECT Count(*) FROM invoices i where invCustID=PartnerID) as TotalInvoices, "
        strSQL += " (SELECT Sum(ARAmtRcvd) FROM accountreceivable a inner join invoices on (invID = ARInvoiceNo) where invCustId =PartnerID ) as TotalAmtRcvd, "
        strSQL += " (SELECT  DATE_FORMAT(Max(ARAmtRcvdDateTime),'%m-%d-%Y') FROM accountreceivable a inner join invoices on (invID = ARInvoiceNo) where invCustId =PartnerID ) as  LastAmtRcvd "
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL += " FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID "
        Else
            strSQL += " FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID Inner join salesrepcustomer s on s.CustomerID=partners.PartnerID  and s.UserID='" & HttpContext.Current.Session("UserID") & "' "
        End If
        strSQL += " where PartnerActive='1' and  (PartnerLongName like '%" & searchData & "%' or PartnerAcronyme like '%" & searchData & "%')"
        Return strSQL
    End Function
    ''' <summary>
    ''' Search SR Detail
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <param name="SearchCatg"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funfillGridSR(ByVal StatusData As String, ByVal SearchData As String, ByVal SearchCatg As String) As String
        Dim strSQL As String = ""
        strSQL = " SELECT distinct PartnerID, PartnerAcronyme, PartnerLongName,PartnerPhone2,PartnerPhone,PartnerID as AssignedTo, "
        strSQL += " (SELECT LeadsFollowup FROM leadslog l where AssignLeadsID=PartnerID order by LeadsFollowup desc limit 1) as LeadsFollowup "
        strSQL += " FROM partners Inner Join partnerseltype as ps on ps.PartnerSelPartID=partners.PartnerID"
        strSQL += " Inner join assignleads s on s.CustomerID=partners.PartnerID "
        strSQL += " Left join partnercontacts on partnercontacts.ContactPartnerID=partners.PartnerID"
        strSQL += " where PartnerActive='1' And active='1' "
        strSQL += " And PartnerID not in (select distinct AssignLeadsID from leadslog where LeadsFollowup >'" & CDate(Date.Now).ToString("yyyy-MM-dd HH:mm:ss") & "')"
        If StatusData <> "" Then
            strSQL += " and PartnerActive='" + StatusData + "'  "
        End If
        If SearchCatg = CustomerSearchFields.ContactName Then
            strSQL += " and (PartnerLongName like '%" + SearchData + "%' or PartnerAcronyme like '%" + SearchData + "%') "
        ElseIf SearchCatg = CustomerSearchFields.CustomerFullName Then
            strSQL += " and Concat(ContactLastName,' ',ifnull(ContactFirstName,'')) like '%" + SearchData + "%'"
        ElseIf SearchCatg = CustomerSearchFields.PartnerPhone Then
            strSQL += " and PartnerPhone like '%" + SearchData + "%'"
        ElseIf SearchCatg = CustomerSearchFields.Acronyme Then
            strSQL += " and PartnerAcronyme like '%" + SearchData + "%'"
        End If

        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = True And (HttpContext.Current.Session("UserModules").ToString.Contains("ADM") = False And HttpContext.Current.Session("UserModules").ToString.Contains("LMA") = False) Then
            strSQL += " and s.UserID='" & HttpContext.Current.Session("UserID") & "'"
        End If
        'ps.PartnerSelTypeID='4' and
        Return strSQL
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Imports iTECH.InbizERP.BusinessLogic
Public Class clsFaxInbox
	Inherits clsDataClass
    Private _faxID, _faxExternalUniqueID, _faxRecDateTime, _faxFromPDFName, _faxFrom, _faxFromOrgID, _faxFromPhone, _faxNew As String
	Public Property FaxID() As String
		Get
			Return _faxID
		End Get
		Set(ByVal value As String)
            _faxID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property FaxExternalUniqueIDe() As String
        Get
            Return _faxExternalUniqueID
        End Get
        Set(ByVal value As String)
            _faxExternalUniqueID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property FaxRecDateTime() As String
        Get
            Return _faxRecDateTime
        End Get
        Set(ByVal value As String)
            _faxRecDateTime = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property FaxFromPDFName() As String
        Get
            Return _faxFromPDFName
        End Get
        Set(ByVal value As String)
            _faxFromPDFName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property FaxFrom() As String
        Get
            Return _faxFrom
        End Get
        Set(ByVal value As String)
            _faxFrom = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property FaxFromOrgID() As String
        Get
            Return _faxFromOrgID
        End Get
        Set(ByVal value As String)
            _faxFromOrgID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property FaxFromPhone() As String
        Get
            Return _faxFromPhone
        End Get
        Set(ByVal value As String)
            _faxFromPhone = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property FaxNew() As String
        Get
            Return _faxNew
        End Get
        Set(ByVal value As String)
            _faxNew = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert FaxInbox
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertFaxInbox() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO sysfaxrequests( faxExternalUniqueID, faxReceivedDateTime, faxFromPDFName, faxFrom, faxFromOrgID, faxFromPhone, faxNew) VALUES('"
        strSQL += _faxExternalUniqueID + "','"
        strSQL += _faxRecDateTime + "','"
        strSQL += _faxFromPDFName + "','"
        strSQL += _faxFrom + "','"
        strSQL += _faxFromOrgID + "','"
        strSQL += _faxFromPhone + "','"
        strSQL += _faxNew + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update FaxInbox
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateFaxInbox() As Boolean
		Dim strSQL As String
        strSQL = "UPDATE sysfaxrequests set "
        strSQL += "faxExternalUniqueID='" + _faxExternalUniqueID + "', "
        strSQL += "faxReceivedDateTime='" + _faxRecDateTime + "', "
        strSQL += "faxFromPDFName='" + _faxFromPDFName + "', "
        strSQL += "faxFrom='" + _faxFrom + "', "
        strSQL += "faxFromOrgID='" + _faxFromOrgID + "', "
        strSQL += "faxFromPhone='" + _faxFromPhone + "', "
        strSQL += "faxNew='" + _faxNew + "'"
        strSQL += " where sysFaxID='" + _faxID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    '''  Populate objects of FaxInbox
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getFaxInboxInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
        strSQL = "SELECT  faxExternalUniqueID, faxReceivedDateTime, faxFromPDFName, faxFrom, faxFromOrgID, faxFromPhone, faxNew FROM sysfaxrequests where sysFaxID='" + _faxID + "' "
		drObj = GetDataReader(strSql)
		While drObj.Read
            _faxExternalUniqueID = drObj.Item("faxExternalUniqueID").ToString
            _faxRecDateTime = drObj.Item("faxReceivedDateTime").ToString
            _faxFromPDFName = drObj.Item("faxFromPDFName").ToString
            _faxFrom = IIf(drObj.Item("faxFrom").ToString <> "", drObj.Item("faxFrom").ToString, "-")
            _faxFromOrgID = IIf(drObj.Item("faxFromOrgID").ToString <> "", drObj.Item("faxFromOrgID").ToString, "-")
            _faxFromPhone = drObj.Item("faxFromPhone").ToString
            _faxNew = drObj.Item("faxNew").ToString
		End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub

    ''' <summary>
    ''' Return Sql for Grid
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSql As String
        'strSql = "SELECT sysFaxID, faxExternalUniqueID, DATE_FORMAT(faxReceivedDateTime,'%m-%d-%Y %h:%i %p') as faxReceivedDateTime, faxFromPDFName, Case faxFrom WHEN 'D' THEN 'Distributor' WHEN 'R' THEN 'Reseller' WHEN 'E' THEN 'End Client' WHEN 'V' THEN 'Vendor' END as faxFrom, Case faxFrom WHEN 'D' THEN d.distName WHEN 'R' THEN r.resellerName WHEN 'E' THEN e.endclientName WHEN 'V' THEN v.vendorName END as faxFromOrgID, CASE faxNew WHEN 1 THEN 'Unread.gif' ELSE 'Read.gif' END as faxNew FROM sysfaxrequests s left join distributor d on s.faxFrom='D' and s.faxFromOrgID=d.distID left join reseller r on s.faxFrom='R' and s.faxFromOrgID=r.resellerID left join endclient e on s.faxFrom='E' and s.faxFromOrgID=e.endclientID left join vendor v on s.faxFrom='V' and s.faxFromOrgID=v.vendorID order by sysFaxID desc "
        strSql = "SELECT sysFaxID, faxExternalUniqueID, DATE_FORMAT(faxReceivedDateTime,'%m-%d-%Y %h:%i %p') as faxReceivedDateTime, faxFromPDFName, Case faxFrom WHEN '" & AddressReference.DISTRIBUTER & " ' THEN 'Distributor' WHEN '" & AddressReference.RESELLER & "' THEN 'Reseller' WHEN '" & AddressReference.END_CLIENT & "' THEN 'End Client' WHEN '" & AddressReference.VENDOR & "' THEN 'Vendor' END as faxFrom, Case faxFrom WHEN '" & AddressReference.DISTRIBUTER & "' THEN PartnerLongName WHEN '" & AddressReference.RESELLER & "' THEN PartnerLongName WHEN '" & AddressReference.END_CLIENT & "' THEN PartnerLongName WHEN '" & AddressReference.VENDOR & "' THEN v.vendorName END as faxFromOrgID, CASE faxNew WHEN 1 THEN 'Unread.gif' ELSE 'Read.gif' END as faxNew FROM sysfaxrequests s left join partners p on p.PartnerID= s.faxFromOrgID left join vendor v on s.faxFrom='" & AddressReference.VENDOR & "' and s.faxFromOrgID=v.vendorID order by sysFaxID desc"
        Return strSql
    End Function

    ''' <summary>
    ''' Return Sql for Deleting Fax
    ''' </summary>
    ''' <param name="strFaxID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteFax(ByVal strFaxID As String) As String
        Dim strSql As String
        strSql = "DELETE FROM sysfaxrequests where sysFaxID='" & strFaxID & "'"
        Return strSql
    End Function

    ''' <summary>
    ''' Return Sql for Search Grid
    ''' </summary>
    ''' <param name="strParam"></param>
    ''' <param name="strValue"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funSearchFillGrid(ByVal strParam As String, ByVal strValue As String) As String
        strValue = strValue.Replace("'", "''")

        Dim strCondition As String = ""
        If strParam = "F" Then
            strCondition += "where s.sysFaxID='" & strValue & "'"
        ElseIf strParam = AddressReference.VENDOR Then
            strCondition += "where v.vendorName like '%" & strValue & "%'"
        ElseIf strParam = AddressReference.DISTRIBUTER Then
            strCondition += "where PartnerLongName like '%" & strValue & "%'"
        ElseIf strParam = AddressReference.RESELLER Then
            strCondition += "where PartnerLongName like '%" & strValue & "%'"
        ElseIf strParam = AddressReference.END_CLIENT Then
            strCondition += "where PartnerLongName like '%" & strValue & "%'"
        End If

        Dim strSql As String
        strSql = "SELECT sysFaxID, faxExternalUniqueID, DATE_FORMAT(faxReceivedDateTime,'%m-%d-%Y %h:%i %p') as faxReceivedDateTime, faxFromPDFName, Case faxFrom WHEN '" & AddressReference.DISTRIBUTER & "' THEN 'Distributor' WHEN '" & AddressReference.RESELLER & "' THEN 'Reseller' WHEN '" & AddressReference.END_CLIENT & "' THEN 'End Client' WHEN '" & AddressReference.VENDOR & "' THEN 'Vendor' END as faxFrom, Case faxFrom WHEN '" & AddressReference.DISTRIBUTER & "' THEN PartnerLongName WHEN '" & AddressReference.RESELLER & "' THEN PartnerLongName WHEN '" & AddressReference.END_CLIENT & "' THEN PartnerLongName WHEN 'V' THEN v.vendorName END as faxFromOrgID, CASE faxNew WHEN 1 THEN 'Unread.gif' ELSE 'Read.gif' END as faxNew FROM sysfaxrequests s left join partners p on p.PartnerID= s.faxFromOrgID left join vendor v on s.faxFrom='" & AddressReference.VENDOR & "' and s.faxFromOrgID=v.vendorID " & strCondition & " order by sysFaxID desc "
        Return strSql
    End Function

    ''' <summary>
    ''' Update Status of Fax
    ''' </summary>
    ''' <param name="strFaxID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateStatus(ByVal strFaxID As String) As Boolean
        Dim strSql As String
        strSql = "Update sysfaxrequests set faxNew='0' where sysFaxID='" & strFaxID & "'"

        Dim obj As New clsDataClass
        obj.PopulateData(strSql)
        obj.CloseDatabaseConnection()

        Return True
    End Function

    ''' <summary>
    ''' Update Status of Fax
    ''' </summary>
    ''' <param name="strFaxID"></param>
    ''' <param name="strFromType"></param>
    ''' <param name="strFromOrg"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FunSaveOrganization(ByVal strFaxID As String, ByVal strFromType As String, ByVal strFromOrg As String) As Boolean
        Dim strSql As String
        strSql = "Update sysfaxrequests set faxFrom='" & strFromType & "', faxFromOrgID='" & strFromOrg & "' where sysFaxID='" & strFaxID & "'"

        Dim obj As New clsDataClass
        obj.PopulateData(strSql)
        obj.CloseDatabaseConnection()

        Return True
    End Function

    ''' <summary>
    ''' Get Fax Count
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FunGetFaxCount() As Integer
        Dim strSql As String
        strSql = "select count(*) from sysfaxrequests "

        Dim FaxCount As Integer = 0

        Dim obj As New clsDataClass
        FaxCount = obj.GetScalarData(strSql)
        obj.CloseDatabaseConnection()

        Return FaxCount
    End Function
End Class

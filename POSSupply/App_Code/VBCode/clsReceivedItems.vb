Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon

Public Class clsReceivedItems
    Inherits clsDataClass
    Private _poItems, _rcvdQty, _poItemRcvDateTime, _recId As String
#Region "properties"

    Public Property PoItems() As String
        Get
            Return _poItems
        End Get
        Set(ByVal value As String)
            _poItems = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RcvdQty() As String
        Get
            Return _rcvdQty
        End Get
        Set(ByVal value As String)
            _rcvdQty = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PoItemRcvDateTime() As String
        Get
            Return _poItemRcvDateTime
        End Get
        Set(ByVal value As String)
            _poItemRcvDateTime = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RecId() As String
        Get
            Return _recId
        End Get
        Set(ByVal value As String)
            _recId = clsCommon.funRemove(value, True)
        End Set
    End Property
#End Region

    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    '''  Insert PurchaseOrderItems
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertRcvdItemQty() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO receivedItems(poitems, rcvdQty, recId, poItemRcvDateTime) VALUES('"
        strSQL += _poItems + "','"
        strSQL += _rcvdQty + "','"
        strSQL += _recId + "','"
        strSQL += _poItemRcvDateTime + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Check Receving ID Exist
    ''' </summary>
    ''' <param name="recID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckReceiptExist(ByVal recID As String) As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM receivedItems where recID='" & recID & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
End Class

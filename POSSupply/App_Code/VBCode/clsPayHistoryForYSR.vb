Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports System.Web.HttpContext
Imports Resources.Resource
Imports clsCommon

Public Class clsPayHistoryForYSR
    Inherits clsDataClass

    Public Function funFillGridForPaymentHistory(ByVal strCompanyName As String) As String
        Dim strSQL As String = "select * from yp_paymenthistory where CompanyNm='" & strCompanyName & "' order by payDateTm desc"
        Return strSQL
    End Function

    Public Function funFillGridForStatus(ByVal strCompanyName As String) As String
        Dim strSQL As String = "select * from yp_status where CompanyNm='" & strCompanyName & "' order by statusdttm desc"
        Return strSQL
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
'Imports Resources.Resource
Imports iTECH.InbizERP.BusinessLogic
Public Class clsReSeller
	Inherits clsDataClass
    Private _resellerID, _resellerName, _resellerPhone, _resellerFax, _resellerEmail, _resellerType, _resellerActive, _resellerValidated, _resellerTaxCode, _resellerCommissionCode, _resellerDiscount, _resellerCurrencyCode, _resellerInvoiceNetTerms, _resellerShipBlankPref, _resellerCourierCode, _resellerDistId, _resellerLang, _resellerStatus, _resellerCompanyID As String
    Public Property ResellerID() As String
        Get
            Return _resellerID
        End Get
        Set(ByVal value As String)
            _resellerID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerName() As String
        Get
            Return _resellerName
        End Get
        Set(ByVal value As String)
            _resellerName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerPhone() As String
        Get
            Return _resellerPhone
        End Get
        Set(ByVal value As String)
            _resellerPhone = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerFax() As String
        Get
            Return _resellerFax
        End Get
        Set(ByVal value As String)
            _resellerFax = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerEmail() As String
        Get
            Return _resellerEmail
        End Get
        Set(ByVal value As String)
            _resellerEmail = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerType() As String
        Get
            Return _resellerType
        End Get
        Set(ByVal value As String)
            _resellerType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerActive() As String
        Get
            Return _resellerActive
        End Get
        Set(ByVal value As String)
            _resellerActive = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerValidated() As String
        Get
            Return _resellerValidated
        End Get
        Set(ByVal value As String)
            _resellerValidated = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerTaxCode() As String
        Get
            Return _resellerTaxCode
        End Get
        Set(ByVal value As String)
            _resellerTaxCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerCommissionCode() As String
        Get
            Return _resellerCommissionCode
        End Get
        Set(ByVal value As String)
            _resellerCommissionCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerDiscount() As String
        Get
            Return _resellerDiscount
        End Get
        Set(ByVal value As String)
            _resellerDiscount = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerCurrencyCode() As String
        Get
            Return _resellerCurrencyCode
        End Get
        Set(ByVal value As String)
            _resellerCurrencyCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerInvoiceNetTerms() As String
        Get
            Return _resellerInvoiceNetTerms
        End Get
        Set(ByVal value As String)
            _resellerInvoiceNetTerms = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerShipBlankPref() As String
        Get
            Return _resellerShipBlankPref
        End Get
        Set(ByVal value As String)
            _resellerShipBlankPref = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerCourierCode() As String
        Get
            Return _resellerCourierCode
        End Get
        Set(ByVal value As String)
            _resellerCourierCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerDistId() As String
        Get
            Return _resellerDistId
        End Get
        Set(ByVal value As String)
            _resellerDistId = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerLanguage() As String
        Get
            Return _resellerLang
        End Get
        Set(ByVal value As String)
            _resellerLang = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ResellerStatus() As String
        Get
            Return _resellerStatus
        End Get
        Set(ByVal value As String)
            _resellerStatus = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property resellerCompanyID() As String
        Get
            Return _resellerCompanyID
        End Get
        Set(ByVal value As String)
            _resellerCompanyID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Insert Reseller
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function insertReseller() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO reseller( resellerName, resellerPhone, resellerFax, resellerEmail, resellerType, resellerActive, resellerValidated, resellerTaxCode, resellerCommissionCode, resellerDiscount, resellerCurrencyCode, resellerInvoiceNetTerms, resellerShipBlankPref, resellerCourierCode, resellerDistId, resellerLang, resellerStatus,resellerCompanyID) VALUES('"
        strSQL += _resellerName + "','"
        strSQL += _resellerPhone + "','"
        strSQL += _resellerFax + "','"
        strSQL += _resellerEmail + "','"
        strSQL += _resellerType + "','"
        strSQL += _resellerActive + "','"
        strSQL += _resellerValidated + "','"
        strSQL += _resellerTaxCode + "','"
        strSQL += _resellerCommissionCode + "','"
        strSQL += _resellerDiscount + "','"
        strSQL += _resellerCurrencyCode + "','"
        strSQL += _resellerInvoiceNetTerms + "','"
        strSQL += _resellerShipBlankPref + "','"
        strSQL += _resellerCourierCode + "','"
        strSQL += _resellerDistId + "','"
        strSQL += _resellerLang + "','"
        strSQL += _resellerStatus + "','"
        strSQL += _resellerCompanyID + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update Reseller
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateReseller() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE reseller set "
        strSQL += "resellerName='" + _resellerName + "', "
        strSQL += "resellerPhone='" + _resellerPhone + "', "
        strSQL += "resellerFax='" + _resellerFax + "', "
        strSQL += "resellerEmail='" + _resellerEmail + "', "
        strSQL += "resellerType='" + _resellerType + "', "
        strSQL += "resellerActive='" + _resellerActive + "', "
        strSQL += "resellerValidated='" + _resellerValidated + "', "
        strSQL += "resellerTaxCode='" + _resellerTaxCode + "', "
        strSQL += "resellerCommissionCode='" + _resellerCommissionCode + "', "
        strSQL += "resellerDiscount='" + _resellerDiscount + "', "
        strSQL += "resellerCurrencyCode='" + _resellerCurrencyCode + "', "
        strSQL += "resellerInvoiceNetTerms='" + _resellerInvoiceNetTerms + "', "
        strSQL += "resellerShipBlankPref='" + _resellerShipBlankPref + "', "
        strSQL += "resellerCourierCode='" + _resellerCourierCode + "', "
        strSQL += "resellerDistId='" + _resellerDistId + "', "
        strSQL += "resellerLang='" + _resellerLang + "', "
        strSQL += "resellerCompanyID='" + _resellerCompanyID + "', "
        strSQL += "resellerStatus='" + _resellerStatus + "'"
        strSQL += " where resellerID='" + _resellerID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of Reseller
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub getResellerInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  resellerName, resellerPhone, resellerFax, resellerEmail, resellerType, resellerActive, resellerValidated, resellerTaxCode, resellerCommissionCode, resellerDiscount, resellerCurrencyCode, resellerInvoiceNetTerms, resellerShipBlankPref, resellerCourierCode, resellerDistId, resellerLang, resellerStatus,resellerCompanyID FROM reseller where resellerID='" + _resellerID + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _resellerName = drObj.Item("resellerName").ToString
            _resellerPhone = drObj.Item("resellerPhone").ToString
            _resellerFax = drObj.Item("resellerFax").ToString
            _resellerEmail = drObj.Item("resellerEmail").ToString
            _resellerType = drObj.Item("resellerType").ToString
            _resellerActive = drObj.Item("resellerActive").ToString
            _resellerValidated = drObj.Item("resellerValidated").ToString
            _resellerTaxCode = drObj.Item("resellerTaxCode").ToString
            _resellerCommissionCode = drObj.Item("resellerCommissionCode").ToString
            _resellerDiscount = drObj.Item("resellerDiscount").ToString
            _resellerCurrencyCode = drObj.Item("resellerCurrencyCode").ToString
            _resellerInvoiceNetTerms = drObj.Item("resellerInvoiceNetTerms").ToString
            _resellerShipBlankPref = drObj.Item("resellerShipBlankPref").ToString
            _resellerCourierCode = drObj.Item("resellerCourierCode").ToString
            _resellerDistId = drObj.Item("resellerDistId").ToString
            _resellerLang = drObj.Item("resellerLang").ToString
            _resellerStatus = drObj.Item("resellerStatus").ToString
            _resellerCompanyID = drObj.Item("resellerCompanyID").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Return Reseller ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetResellerID() As String
        Dim strSQL As String
        strSQL = "SELECT resellerID FROM reseller where resellerName='" & _resellerName & "' and resellerEmail='" & _resellerEmail & "' "
        Dim strData As String = ""
        strData = GetScalarData(strSQL)
        Return strData
    End Function
    ''' <summary>
    ''' Check Duplicate Reseller
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateReseller() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM reseller where resellerName='" & _resellerName & "' and resellerEmail='" & _resellerEmail & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Delete Reseller
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteReseller(ByVal DeleteID As String) As String
        Dim strSQL As String
        'strSQL = "DELETE FROM reseller WHERE resellerID='" + DeleteID + "' "
        strSQL = "UPDATE reseller set resellerActive='0' WHERE resellerID='" + DeleteID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill grid
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT resellerID, resellerName, resellerPhone, resellerFax, resellerEmail FROM reseller where resellerActive='1' order by resellerName "
        Else
            strSQL = "SELECT resellerID, resellerName, resellerPhone, resellerFax, resellerEmail FROM reseller inner join salesrepcustomer s on s.CustomerID=resellerID and s.CustomerType='R' and s.UserID='" & HttpContext.Current.Session("UserID") & "' where resellerActive='1' order by resellerName "
        End If
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill grid with search criteria
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal StatusData As String, ByVal SearchData As String) As String
        Dim strSQL As String
        SearchData = clsCommon.funRemove(SearchData)
        If HttpContext.Current.Session("UserModules").ToString.Contains("SR") = False Or HttpContext.Current.Session("SalesRepRestricted") = "0" Then
            strSQL = "SELECT resellerID, resellerName, resellerPhone, resellerFax, resellerEmail FROM reseller where resellerActive='1' "
        Else
            strSQL = "SELECT resellerID, resellerName, resellerPhone, resellerFax, resellerEmail FROM reseller left join salesrepcustomer s on s.CustomerID=resellerID and s.CustomerType='R' and s.UserID='" & HttpContext.Current.Session("UserID") & "' where resellerActive='1' "
        End If

        If StatusData <> "" And SearchData <> "" Then
            strSQL += " and resellerStatus='" + StatusData + "' and "
            strSQL += " ("
            strSQL += " resellerName like '%" + SearchData + "%' or "
            strSQL += " resellerPhone like '%" + SearchData + "%' or "
            strSQL += " resellerEmail like '%" + SearchData + "%' "
            strSQL += " )"
        ElseIf StatusData <> "" And SearchData = "" Then
            strSQL += " and resellerStatus='" + StatusData + "' and "
            strSQL += " ("
            strSQL += " resellerName like '%" + SearchData + "%' or "
            strSQL += " resellerPhone like '%" + SearchData + "%' or "
            strSQL += " resellerEmail like '%" + SearchData + "%' "
            strSQL += " )"
        ElseIf StatusData = "" Then
            strSQL += " and "
            strSQL += " ("
            strSQL += " resellerName like '%" + SearchData + "%' or "
            strSQL += " resellerPhone like '%" + SearchData + "%' or "
            strSQL += " resellerEmail like '%" + SearchData + "%' "
            strSQL += " )"
        End If
        strSQL += " order by resellerName "
        Return strSQL
    End Function
    ''' <summary>
    ''' Populate Reseller
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub PopulateReseller(ByVal ddl As DropDownList)
        Dim strSQL As String
        strSQL = "SELECT resellerID, resellerName FROM reseller WHERE resellerActive='1' order by resellerName "
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "resellerName"
        ddl.DataValueField = "resellerID"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.SelectReseller
        ddl.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub

    ''' <summary>
    ''' Populate SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <remarks></remarks>
    Public Sub PopulateSR(ByVal chklst As CheckBoxList)
        Dim strSQL As String
        strSQL = "SELECT userID, concat(userFirstName, ' ',userLastName) as Name FROM users WHERE userActive='1' and userModules like '%SR%' order by Name "
        chklst.Items.Clear()
        chklst.DataSource = GetDataReader(strSQL)
        chklst.DataTextField = "Name"
        chklst.DataValueField = "userID"
        chklst.DataBind()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate User's SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <param name="ReselerID"></param>
    ''' <remarks></remarks>
    Public Sub PopulateUserSR(ByVal chklst As CheckBoxList, ByVal ReselerID As String)
        Dim strSQL As String
        strSQL = "SELECT u.userID, concat(userFirstName, ' ',userLastName) as Name, 'A' as seq FROM users u inner join salesrepcustomer s on s.UserID=u.userID and CustomerID='" & ResellerID & "' and CustomerType='R' WHERE userActive='1' and userModules like '%SR%' Union SELECT u.userID, concat(userFirstName, ' ',userLastName) as Name, 'B' as seq FROM users u WHERE userActive='1' and userModules like '%SR%' and u.userID not in (Select UserID from salesrepcustomer where CustomerID='" & ResellerID & "' and CustomerType='R') order by seq, Name"
        chklst.Items.Clear()
        chklst.DataSource = GetDataReader(strSQL)
        chklst.DataTextField = "Name"
        chklst.DataValueField = "userID"
        chklst.DataBind()
        CloseDatabaseConnection()
        Dim SR As String = GetUserSR(ReselerID)
        For i As Int16 = 0 To chklst.Items.Count - 1
            If SR.Contains("~" & chklst.Items(i).Value & "~") Then
                chklst.Items(i).Selected = True
            End If
        Next
    End Sub
    ''' <summary>
    ''' Get User's SR
    ''' </summary>
    ''' <param name="ResellerID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetUserSR(ByVal ResellerID As String) As String
        Dim strSQL As String
        strSQL = "SELECT u.userID FROM users u inner join salesrepcustomer s on s.UserID=u.userID and CustomerID='" & ResellerID & "' and CustomerType='R' WHERE userActive='1' and userModules like '%SR%' "
        Dim SR As String = "~"
        Dim dr As OdbcDataReader = GetDataReader(strSQL)
        While dr.Read
            SR += dr.Item(0) & "~"
        End While
        CloseDatabaseConnection()
        Return SR
    End Function
    ''' <summary>
    ''' Insert SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <param name="ResellerID"></param>
    ''' <remarks></remarks>
    Public Sub InsertSR(ByVal chklst As CheckBoxList, ByVal ResellerID As String)
        Dim objSR As New clsSalesRepCustomer
        Dim i As Integer
        For i = 0 To chklst.Items.Count - 1
            If chklst.Items(i).Selected = True Then
                objSR.UserID = chklst.Items(i).Value
                objSR.CustID = ResellerID
                objSR.CustType = AddressReference.RESELLER
                objSR.insertSalesRepCustomer()
            End If
        Next
    End Sub
    ''' <summary>
    ''' Update SR
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <param name="ResellerID"></param>
    ''' <remarks></remarks>
    Public Sub UpdateSR(ByVal chklst As CheckBoxList, ByVal ResellerID As String)
        Dim objSR As New clsSalesRepCustomer
        objSR.CustID = ResellerID
        objSR.CustType = AddressReference.RESELLER
        objSR.DeleteSalesRepCustomer()

        For i As Int16 = 0 To chklst.Items.Count - 1
            If chklst.Items(i).Selected = True Then
                objSR.UserID = chklst.Items(i).Value
                objSR.CustID = ResellerID
                objSR.CustType = AddressReference.RESELLER
                objSR.insertSalesRepCustomer()
            End If
        Next
    End Sub
    ''' <summary>
    ''' Return New Reseller ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetNewResellerID() As String
        Dim strSQL As String
        strSQL = "SELECT Max(resellerID) FROM reseller "
        Dim strData As String = ""
        strData = GetScalarData(strSQL)
        Return strData
    End Function
End Class

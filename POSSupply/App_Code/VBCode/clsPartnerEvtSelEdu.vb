Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsPartnerEvtSelEdu
	Inherits clsDataClass
	Private _PartnerEvtSelEduID, _PartnerEvenEduLvlTxtID, _PartnerEvtSvcLogID, _PartnerEvtEduVal As String
	Public Property PartnerEvtSelEduID() As String
		Get
			Return _PartnerEvtSelEduID
		End Get
		Set(ByVal value As String)
            _PartnerEvtSelEduID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerEvenEduLvlTxtID() As String
		Get
			Return _PartnerEvenEduLvlTxtID
		End Get
		Set(ByVal value As String)
            _PartnerEvenEduLvlTxtID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerEvtSvcLogID() As String
		Get
			Return _PartnerEvtSvcLogID
		End Get
		Set(ByVal value As String)
            _PartnerEvtSvcLogID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerEvtEduVal() As String
		Get
			Return _PartnerEvtEduVal
		End Get
		Set(ByVal value As String)
            _PartnerEvtEduVal = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert PartnerEvtSelEdu
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertPartnerEvtSelEdu() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO partnerevtseledu( PartnerEvenEduLvlTxtID, PartnerEvtSvcLogID, PartnerEvtEduVal) VALUES('"
		strSQL += _PartnerEvenEduLvlTxtID + "','"
		strSQL += _PartnerEvtSvcLogID + "','"
		strSQL += _PartnerEvtEduVal + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    '''  Update PartnerEvtSelEdu
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updatePartnerEvtSelEdu() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE partnerevtseledu set "
		strSQL += "PartnerEvenEduLvlTxtID='" + _PartnerEvenEduLvlTxtID + "', "
		strSQL += "PartnerEvtSvcLogID='" + _PartnerEvtSvcLogID + "', "
		strSQL += "PartnerEvtEduVal='" + _PartnerEvtEduVal + "'"
		strSQL += " where PartnerEvtSelEduID='" + _PartnerEvtSelEduID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
    End Function
    ''' <summary>
    '''  Update PartnerEvtSelEdu
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updatePartnerEvtSelEduWRTLogID() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE partnerevtseledu set "
        strSQL += "PartnerEvtEduVal='" + _PartnerEvtEduVal + "'"
        strSQL += " where PartnerEvtSvcLogID='" + _PartnerEvtSvcLogID + "' "
        strSQL += " and PartnerEvenEduLvlTxtID='" + _PartnerEvenEduLvlTxtID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    '''  Delete PartnerEvtSelEdu
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function deletePartnerEvtSelEdu() As Boolean
        Dim strSQL As String
        strSQL = "DELETE from partnerevtseledu "
        strSQL += " where PartnerEvtSvcLogID='" + _PartnerEvtSvcLogID + "' "
        strSQL += " and PartnerEvenEduLvlTxtID='" + _PartnerEvenEduLvlTxtID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    '''  Populate objects of PartnerEvtSelEdu
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPartnerEvtSelEduInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  PartnerEvenEduLvlTxtID, PartnerEvtSvcLogID, PartnerEvtEduVal FROM partnerevtseledu where PartnerEvtSelEduID='" + _PartnerEvtSelEduID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_PartnerEvenEduLvlTxtID = drObj.Item("PartnerEvenEduLvlTxtID").ToString
			_PartnerEvtSvcLogID = drObj.Item("PartnerEvtSvcLogID").ToString
			_PartnerEvtEduVal = drObj.Item("PartnerEvtEduVal").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    '''  Fill Grid of PartnerEvtSelEdu
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String = ""
        strSQL = "SELECT PartnerEvtSelEduID, PartnerEvenEduLvlTxtID, PartnerEvtSvcLogID, PartnerEvtEduVal FROM partnerevtseledu where PartnerEvtSvcLogID='" + _PartnerEvtSvcLogID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Check Record
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckRecord() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM partnerevtseledu where PartnerEvtSvcLogID='" & _PartnerEvtSvcLogID & "' and PartnerEvenEduLvlTxtID='" & _PartnerEvenEduLvlTxtID & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsCollectionLog
	Inherits clsDataClass
	Private _CollectionLogID, _ColHeaderID, _ColFollowup, _ColFollowupUserID, _ColLogText, _ColActivityDatetime As String
	Public Property CollectionLogID() As String
		Get
			Return _CollectionLogID
		End Get
		Set(ByVal value As String)
            _CollectionLogID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ColHeaderID() As String
		Get
			Return _ColHeaderID
		End Get
		Set(ByVal value As String)
            _ColHeaderID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ColFollowup() As String
		Get
			Return _ColFollowup
		End Get
		Set(ByVal value As String)
            _ColFollowup = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ColFollowupUserID() As String
		Get
			Return _ColFollowupUserID
		End Get
		Set(ByVal value As String)
            _ColFollowupUserID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ColLogText() As String
		Get
			Return _ColLogText
		End Get
		Set(ByVal value As String)
            _ColLogText = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ColActivityDatetime() As String
		Get
			Return _ColActivityDatetime
		End Get
		Set(ByVal value As String)
            _ColActivityDatetime = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert CollectionLog
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertCollectionLog() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO collectionlog( ColHeaderID, ColFollowup, ColFollowupUserID, ColLogText, ColActivityDatetime) VALUES('"
		strSQL += _ColHeaderID + "','"
		strSQL += _ColFollowup + "','"
		strSQL += _ColFollowupUserID + "','"
		strSQL += _ColLogText + "','"
		strSQL += _ColActivityDatetime + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update CollectionLog
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateCollectionLog() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE collectionlog set "
		strSQL += "ColHeaderID='" + _ColHeaderID + "', "
		strSQL += "ColFollowup='" + _ColFollowup + "', "
		strSQL += "ColFollowupUserID='" + _ColFollowupUserID + "', "
		strSQL += "ColLogText='" + _ColLogText + "', "
		strSQL += "ColActivityDatetime='" + _ColActivityDatetime + "'"
		strSQL += " where CollectionLogID='" + _CollectionLogID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of CollectionLog
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getCollectionLogInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  ColHeaderID, ColFollowup, ColFollowupUserID, ColLogText, ColActivityDatetime FROM collectionlog where CollectionLogID='" + _CollectionLogID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_ColHeaderID = drObj.Item("ColHeaderID").ToString
			_ColFollowup = drObj.Item("ColFollowup").ToString
			_ColFollowupUserID = drObj.Item("ColFollowupUserID").ToString
			_ColLogText = drObj.Item("ColLogText").ToString
			_ColActivityDatetime = drObj.Item("ColActivityDatetime").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill Grid 
    ''' </summary>
    ''' <param name="strPartnerID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(Optional ByVal strPartnerID As String = "") As String
        Dim strSQL As String
        strSQL = "SELECT  o.InvRefNo, CollectionLogID, ColHeaderID, ColFollowup, ColFollowupUserID, ColLogText, ColActivityDatetime FROM collectionlog  left join invoices o on o.invID=collectionlog.ColHeaderID "
        If strPartnerID = "" Then
            strSQL += " where ColHeaderID='" + _ColHeaderID + "' "
            strSQL += " order by ColActivityDatetime desc "
        Else
            strSQL += " where invCustID='" + strPartnerID + "' "
            strSQL += " order by o.invID desc "
        End If
        'If Current.Session("UserModules").ToString.Contains("CUA") = True Then
        '    strSQL += " and ColFollowupUserID='" & Current.Session("UserID").ToString & "' "
        'End If
        Return strSQL
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsPartnerSelClass
	Inherits clsDataClass
	Private _PartnerSelClassID, _PartnerSelPartID As String
	Public Property PartnerSelClassID() As String
		Get
			Return _PartnerSelClassID
		End Get
		Set(ByVal value As String)
            _PartnerSelClassID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerSelPartID() As String
		Get
			Return _PartnerSelPartID
		End Get
		Set(ByVal value As String)
            _PartnerSelPartID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert PartnerSelClass
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertPartnerSelClass() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO partnerselclass(PartnerSelClassID, PartnerSelPartID) VALUES('"
		strSQL += _PartnerSelClassID + "','"
		strSQL += _PartnerSelPartID + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    '''  Update PartnerSelClass
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updatePartnerSelClass() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE partnerselclass set "
        strSQL += "PartnerSelClassID='" + _PartnerSelClassID + "'"
        strSQL += " where PartnerSelClassID='" + _PartnerSelClassID + "' "
        strSQL += " and PartnerSelPartID='" + _PartnerSelPartID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
    End Function
    ''' <summary>
    ''' Delete PartnerSelClass
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function deletePartnerSelClass() As Boolean
        Dim strSQL As String
        strSQL = "Delete from partnerselclass "
        strSQL += " where PartnerSelClassID='" + _PartnerSelClassID + "' "
        strSQL += " and PartnerSelPartID='" + _PartnerSelPartID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of PartnerSelClasss
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPartnerSelClassInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
        strSQL = "SELECT PartnerSelClassID, PartnerSelPartID FROM partnerselclass where PartnerSelPartID='" + _PartnerSelPartID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
            _PartnerSelPartID = drObj.Item("PartnerSelPartID").ToString
            _PartnerSelClassID = drObj.Item("PartnerSelClassID").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    '''  Fill Grid of Partners Sel Classification
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String = ""
        strSQL = "SELECT PartnerSelClassID, PartnerSelPartID FROM partnerselclass where PartnerSelPartID='" + _PartnerSelPartID + "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Check Record
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckRecord() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM partnerselclass where PartnerSelPartID='" & _PartnerSelPartID & "' and PartnerSelClassID='" & _PartnerSelClassID & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsOrderType
    Inherits clsDataClass
    Private _orderTypeDtlID, _orderTypeID, _userID, _orderCommission As String
    Public Property orderTypeDtlID() As String
        Get
            Return _orderTypeDtlID
        End Get
        Set(ByVal value As String)
            _orderTypeDtlID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property orderTypeID() As String
        Get
            Return _orderTypeID
        End Get
        Set(ByVal value As String)
            _orderTypeID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property userID() As String
        Get
            Return _userID
        End Get
        Set(ByVal value As String)
            _userID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property ordercommission() As String
        Get
            Return _ordercommission
        End Get
        Set(ByVal value As String)
            _orderCommission = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Insert Order Type
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>//Done In New Inbiz4.0
    Public Function funInsertOrdertype() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO ordertypedtl(orderTypeID, userID, ordercommission) VALUES('"
        strSQL += _orderTypeID + "','"
        strSQL += _userID + "','"
        strSQL += _orderCommission + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update Order Type
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateOrdertype() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE ordertypedtl set "
        strSQL += "orderTypeID='" + _orderTypeID + "', "
        strSQL += "userID='" + _userID + "', "
        strSQL += "ordercommission='" + _orderCommission + "'"
        strSQL += " where orderTypeDtlID='" + _orderTypeDtlID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of Order Type
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub funGetOrdertypeInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT * FROM ordertypedtl "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _orderTypeID = drObj.Item("orderTypeID").ToString
            _userID = drObj.Item("userID").ToString
            _orderCommission = drObj.Item("ordercommission").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fill Grid of OrderType for Comm ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>//Done In New in InBiz 4.0 Bll
    Public Function funFillGrid() As String
        Dim strSQL As String = ""
        strSQL = "SELECT orderTypeDtlID,orderTypeDesc, userID, ordercommission FROM ordertypedtl Inner join ordertype on ordertype.orderTypeID=ordertypedtl.orderTypeID where userID='" & HttpContext.Current.Request.QueryString("UserID") & "' and orderTypeLang='" & clsCommon.funGetProductLang() & "'"
      
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid of OrderType for Comm ID
    ''' </summary>
    ''' <param name="strOrderTypeID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteOderType(ByVal strOrderTypeID As String) As String
        Dim strSQL As String = ""
        strSQL = "Delete from ordertypedtl where orderTypeDtlID ='" & strOrderTypeID & "'"
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill oderType Name in Dropdown list
    ''' </summary>
    ''' <param name="dlOrderType"></param>
    ''' <remarks></remarks>
    Public Sub subGetOrderType(ByVal dlOrderType As DropDownList)
        Dim strSQL As String
        strSQL = "SELECT ordertypeID,orderTypeDesc from ordertype  where orderTypeLang='" & clsCommon.funGetProductLang() & "'"
        dlOrderType.DataSource = GetDataReader(strSQL)
        dlOrderType.DataTextField = "orderTypeDesc"
        dlOrderType.DataValueField = "ordertypeID"
        dlOrderType.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.lblSelectOrderType
        dlOrderType.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Find Duplicate Order Type
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>//Done In Inbiz 4.0 Bll 
    Public Function funDuplicateOderType() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM ordertypedtl where orderTypeID='" & _orderTypeID & "' and userID='" & _userID & "'"
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Find Order Type Commission
    ''' </summary>
    ''' <param name="strOrdTypeID"></param>
    ''' <param name="strUserID"></param>
    ''' <param name="strAmount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funOderCommission(ByVal strOrdTypeID As String, ByVal strUserID As String, ByVal strAmount As String) As String
        Dim strSQL, strCommisSionAmt As String
        strSQL = "SELECT ordercommission FROM ordertypedtl where orderTypeID='" & strOrdTypeID & "' and userID='" & strUserID & "'"
        strCommisSionAmt = GetScalarData(strSQL)
        If strCommisSionAmt <> 0 Then
            strCommisSionAmt = (strAmount * strCommisSionAmt) / 100
        Else
            strCommisSionAmt = "0"
        End If
        Return strCommisSionAmt
    End Function

End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsProductMaterial
	Inherits clsDataClass
	Private _matCode, _matCodeDesc As String
	Public Property MatCode() As String
		Get
			Return _matCode
		End Get
		Set(ByVal value As String)
            _matCode = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property MatCodeDesc() As String
		Get
			Return _matCodeDesc
		End Get
		Set(ByVal value As String)
            _matCodeDesc = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert ProductMaterial
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertProductMaterial() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO sysproductmaterial(matCode, matCodeDesc) VALUES('"
		strSQL += _matCode + "','"
		strSQL += _matCodeDesc + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update ProductMaterial
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateProductMaterial() As Boolean
		Dim strSQL As String
        strSQL = "UPDATE sysproductmaterial set "
		strSQL += "matCodeDesc='" + _matCodeDesc + "'"
		strSQL += " where matCode='" + _matCode + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of ProductMaterial
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getProductMaterialInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  matCodeDesc FROM sysproductmaterial where matCode='" + _matCode + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_matCodeDesc = drObj.Item("matCodeDesc").ToString
		End While
        drObj.Close()
        CloseDatabaseConnection()
	End Sub
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data

Public Class clsDataClass
    Protected dbConn As New OdbcConnection
    Protected dbConnectionString As String
    Public Sub New()
        dbConnectionString = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
        dbConn.ConnectionString = dbConnectionString
    End Sub
    Public Sub New(ByVal ConnectionParameter As String)
        dbConnectionString = ConnectionParameter
        dbConn.ConnectionString = dbConnectionString
    End Sub
    ''' <summary>
    ''' Open the database connection
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub OpenDatabaseConnection()
        If (dbConn.State = Data.ConnectionState.Closed) Then
            dbConn.Open()
        End If
    End Sub
    ''' <summary>
    ''' Close the connection
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CloseDatabaseConnection()
        If (dbConn.State = Data.ConnectionState.Open) Then
            dbConn.Close()
        End If
    End Sub
    ''' <summary>
    ''' To get the Data Reader 
    ''' </summary>
    ''' <param name="strSql"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDataReader(ByVal strSql As String) As OdbcDataReader
        If (dbConn.State = Data.ConnectionState.Closed) Then
            dbConn.Open()
        End If
        Dim dbCommand As OdbcCommand = New OdbcCommand(strSql)
        dbCommand.Connection = dbConn
        Dim dbReader As OdbcDataReader = dbCommand.ExecuteReader()
        Return dbReader
    End Function
    ''' <summary>
    ''' To get the single value record
    ''' </summary>
    ''' <param name="strSql"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetScalarData(ByVal strSql As String) As Object
        If (dbConn.State = Data.ConnectionState.Closed) Then
            dbConn.Open()
        End If
        Dim dbCommand As OdbcCommand = New OdbcCommand(strSql)
        dbCommand.Connection = dbConn
        Dim strData As Object = dbCommand.ExecuteScalar
        If (dbConn.State = Data.ConnectionState.Open) Then
            dbConn.Close()
        End If
        dbCommand.Dispose()
        Return strData
    End Function

    ''' <summary>
    ''' To Execute the query such  Update , Insert and  Delete
    ''' </summary>
    ''' <param name="strSql"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SetData(ByVal strSql As String) As Int32
        Dim dbCommand As OdbcCommand = New OdbcCommand(strSql)
        If (dbConn.State = Data.ConnectionState.Closed) Then
            dbConn.Open()
        End If

        dbCommand.Connection = dbConn
        Dim intCount As Int32 = dbCommand.ExecuteNonQuery()
        If (dbConn.State = Data.ConnectionState.Open) Then
            dbConn.Close()
        End If
        dbCommand.Dispose()
        Return (intCount)
    End Function
    ''' <summary>
    ''' To Execute the query such as Update,Insert And Delete   
    ''' </summary>
    ''' <param name="strSql"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function PopulateData(ByVal strSql As String) As Int32
        Dim dbCommand As OdbcCommand = New OdbcCommand(strSql)
        If (dbConn.State = Data.ConnectionState.Closed) Then
            dbConn.Open()
        End If
        dbCommand.Connection = dbConn
        Dim intCount As Int32 = dbCommand.ExecuteNonQuery()
        dbCommand.Dispose()
        Return (intCount)
    End Function
    Public Function SetDataMail(ByVal strSql As String) As Int32 'for Mailmessage
        Dim dbCommand As OdbcCommand = New OdbcCommand(strSql)
        If (dbConn.State = Data.ConnectionState.Closed) Then
            dbConn.Open()
        End If
        dbCommand.Connection = dbConn
        Dim intCount As Int32 = dbCommand.ExecuteNonQuery()
        dbCommand.Dispose()
        Return (intCount)
    End Function
    Public Function GetDataset(ByVal strSql As String) As DataSet
        If (dbConn.State = Data.ConnectionState.Closed) Then
            dbConn.Open()
        End If
        Dim dbCommand As OdbcCommand = New OdbcCommand(strSql)
        dbCommand.Connection = dbConn
        Dim dbAdapter As OdbcDataAdapter = New OdbcDataAdapter(strSql, dbConn)
        Dim ds As New DataSet
        dbAdapter.Fill(ds)
        Return ds
    End Function
    'Returns Data Table
    Public Function GetDataTable(ByVal strSql As String) As DataTable
        If (dbConn.State = Data.ConnectionState.Closed) Then
            dbConn.Open()
        End If
        Dim dbCommand As OdbcCommand = New OdbcCommand(strSql)
        dbCommand.Connection = dbConn
        Dim ds As New DataSet
        Dim da As New OdbcDataAdapter
        da.SelectCommand = dbCommand
        da.Fill(ds)
        da.Dispose()
        If (dbConn.State = Data.ConnectionState.Open) Then
            dbConn.Close()
        End If
        Return ds.Tables(0)
    End Function
    ''' <summary>
    ''' To get the single value record without connection close
    ''' </summary>
    ''' <param name="strSql"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetScalarDataWithoutConClose(ByVal strSql As String) As Object
        If (dbConn.State = Data.ConnectionState.Closed) Then
            dbConn.Open()
        End If
        Dim dbCommand As OdbcCommand = New OdbcCommand(strSql)
        dbCommand.Connection = dbConn

        Dim strData As Object = dbCommand.ExecuteScalar
        dbCommand.Dispose()
        Return strData
    End Function

    ''' <summary>
    ''' Execute Non Query without opening and closing the connection
    ''' </summary>
    ''' <param name="strSql"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExecuteNnQuery(ByVal strSql As String) As Int32
        Dim dbCommand As OdbcCommand = New OdbcCommand(strSql)
        dbCommand.Connection = dbConn
        Dim intCount As Int32 = dbCommand.ExecuteNonQuery()
        dbCommand.Dispose()
        Return (intCount)
    End Function

    ''' <summary>
    ''' To get the single value record without opening and closing the connections
    ''' </summary>
    ''' <param name="strSql"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetScalarDataByQuery(ByVal strSql As String) As Object
        Dim dbCommand As OdbcCommand = New OdbcCommand(strSql)
        dbCommand.Connection = dbConn
        Dim strData As Object = dbCommand.ExecuteScalar
        dbCommand.Dispose()
        Return strData
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports System.Text
Imports iTECH.InbizERP.BusinessLogic

Public Class clsUser
	Inherits clsDataClass
    Private _userID, _userLoginId, _userPassword, _userSalutation, _userFirstName, _userLastName, _userEmail, _userDesignation, _userDepartment, _userPhone, _userPhoneExt, _userFax, _userCellPhone, _userRoleID, _userType, _userOrgID, _userModules, _userStatus, _userLastUpdatedAt, _userActive, _userValidated, _userLang, _userDefaultWhs As String
	Public Property UserID() As String
		Get
			Return _userID
		End Get
		Set(ByVal value As String)
            _userID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserLoginId() As String
		Get
			Return _userLoginId
		End Get
		Set(ByVal value As String)
            _userLoginId = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserPassword() As String
		Get
			Return _userPassword
		End Get
		Set(ByVal value As String)
            _userPassword = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserSalutation() As String
		Get
			Return _userSalutation
		End Get
		Set(ByVal value As String)
            _userSalutation = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserFirstName() As String
		Get
			Return _userFirstName
		End Get
		Set(ByVal value As String)
            _userFirstName = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserLastName() As String
		Get
			Return _userLastName
		End Get
		Set(ByVal value As String)
            _userLastName = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserEmail() As String
		Get
			Return _userEmail
		End Get
		Set(ByVal value As String)
            _userEmail = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property UserDesignation() As String
        Get
            Return _userDesignation
        End Get
        Set(ByVal value As String)
            _userDesignation = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserDepartment() As String
        Get
            Return _userDepartment
        End Get
        Set(ByVal value As String)
            _userDepartment = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Property UserPhone() As String
		Get
			Return _userPhone
		End Get
		Set(ByVal value As String)
            _userPhone = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserPhoneExt() As String
		Get
			Return _userPhoneExt
		End Get
		Set(ByVal value As String)
            _userPhoneExt = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserFax() As String
		Get
			Return _userFax
		End Get
		Set(ByVal value As String)
            _userFax = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserCellPhone() As String
		Get
			Return _userCellPhone
		End Get
		Set(ByVal value As String)
            _userCellPhone = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserRoleID() As String
		Get
			Return _userRoleID
		End Get
		Set(ByVal value As String)
            _userRoleID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserType() As String
		Get
			Return _userType
		End Get
		Set(ByVal value As String)
            _userType = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserOrgID() As String
		Get
			Return _userOrgID
		End Get
		Set(ByVal value As String)
            _userOrgID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserModules() As String
		Get
			Return _userModules
		End Get
		Set(ByVal value As String)
            _userModules = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property UserStatus() As String
        Get
            Return _userStatus
        End Get
        Set(ByVal value As String)
            _userStatus = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Property UserLastUpdatedAt() As String
		Get
			Return _userLastUpdatedAt
		End Get
		Set(ByVal value As String)
            _userLastUpdatedAt = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserActive() As String
		Get
			Return _userActive
		End Get
		Set(ByVal value As String)
            _userActive = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserValidated() As String
		Get
			Return _userValidated
		End Get
		Set(ByVal value As String)
            _userValidated = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property UserLang() As String
		Get
			Return _userLang
		End Get
		Set(ByVal value As String)
            _userLang = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property UserWarehouse() As String
        Get
            Return _userDefaultWhs
        End Get
        Set(ByVal value As String)
            _userDefaultWhs = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert User
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertUser() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO users( userLoginId, userPassword, userSalutation, userFirstName, userLastName, userEmail, userDesignation, userDepartment, userPhone, userPhoneExt, userFax, userCellPhone, userRoleID, userType, userModules, userStatus, userLastUpdatedAt, userActive, userValidated, userLang, userDefaultWhs) VALUES('" 'userOrgID, 
        strSQL += _userLoginId + "',password('"
        strSQL += _userPassword + "'),'"
		strSQL += _userSalutation + "','"
		strSQL += _userFirstName + "','"
		strSQL += _userLastName + "','"
        strSQL += _userEmail + "','"
        strSQL += _userDesignation + "','"
        strSQL += _userDepartment + "','"
		strSQL += _userPhone + "','"
		strSQL += _userPhoneExt + "','"
		strSQL += _userFax + "','"
		strSQL += _userCellPhone + "','"
		strSQL += _userRoleID + "','"
		strSQL += _userType + "','"
        'strSQL += _userOrgID + "','"
        strSQL += _userModules + "','"
        strSQL += _userStatus + "','"
		strSQL += _userLastUpdatedAt + "','"
		strSQL += _userActive + "','"
        strSQL += _userValidated + "','"
        strSQL += _userLang + "','"
        strSQL += _userDefaultWhs + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update User
    ''' </summary>
    ''' <param name="nUpdatePassword"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function updateUser(ByVal nUpdatePassword As Int16) As Boolean
        Dim strSQL As String
        strSQL = "UPDATE users set "
        strSQL += "userLoginId='" + _userLoginId + "', "
        If nUpdatePassword = 1 Then
            strSQL += "userPassword=password('" + _userPassword + "'), "
        End If
        strSQL += "userSalutation='" + _userSalutation + "', "
        strSQL += "userFirstName='" + _userFirstName + "', "
        strSQL += "userLastName='" + _userLastName + "', "
        strSQL += "userEmail='" + _userEmail + "', "
        strSQL += "userDesignation='" + _userDesignation + "', "
        strSQL += "userDepartment='" + _userDepartment + "', "
        strSQL += "userPhone='" + _userPhone + "', "
        strSQL += "userPhoneExt='" + _userPhoneExt + "', "
        strSQL += "userFax='" + _userFax + "', "
        strSQL += "userCellPhone='" + _userCellPhone + "', "
        strSQL += "userRoleID='" + _userRoleID + "', "
        strSQL += "userType='" + _userType + "', "
        'strSQL += "userOrgID='" + _userOrgID + "', "
        'strSQL += "userModules='" + _userModules + "', "
        strSQL += "userStatus='" + _userStatus + "', "
        strSQL += "userLastUpdatedAt='" + _userLastUpdatedAt + "', "
        strSQL += "userActive='" + _userActive + "', "
        strSQL += "userValidated='" + _userValidated + "', "
        strSQL += "userLang='" + _userLang + "', "
        strSQL += "userDefaultWhs='" + _userDefaultWhs + "'"
        strSQL += " where userID='" + _userID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    '''  Populate objects of User
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getUserInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
        strSQL = "SELECT  userLoginId, userPassword, userSalutation, userFirstName, userLastName, userEmail, userDesignation, userDepartment, userPhone, userPhoneExt, userFax, userCellPhone, userRoleID, userType, userModules, userStatus, userLastUpdatedAt, userActive, userValidated, userLang, userDefaultWhs FROM users where userID='" + _userID + "' " 'userOrgID, 
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_userLoginId = drObj.Item("userLoginId").ToString
			_userPassword = drObj.Item("userPassword").ToString
			_userSalutation = drObj.Item("userSalutation").ToString
			_userFirstName = drObj.Item("userFirstName").ToString
			_userLastName = drObj.Item("userLastName").ToString
            _userEmail = drObj.Item("userEmail").ToString
            _userDesignation = drObj.Item("userDesignation").ToString
            _userDepartment = drObj.Item("userDepartment").ToString
			_userPhone = drObj.Item("userPhone").ToString
			_userPhoneExt = drObj.Item("userPhoneExt").ToString
			_userFax = drObj.Item("userFax").ToString
			_userCellPhone = drObj.Item("userCellPhone").ToString
			_userRoleID = drObj.Item("userRoleID").ToString
			_userType = drObj.Item("userType").ToString
            '_userOrgID = drObj.Item("userOrgID").ToString
            _userModules = drObj.Item("userModules").ToString
            _userStatus = drObj.Item("userStatus").ToString
			_userLastUpdatedAt = drObj.Item("userLastUpdatedAt").ToString
			_userActive = drObj.Item("userActive").ToString
			_userValidated = drObj.Item("userValidated").ToString
            _userLang = drObj.Item("userLang").ToString
            _userDefaultWhs = drObj.Item("userDefaultWhs").ToString
		End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Validate User
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ValidateUserOld() As Boolean
        Dim strSQL As String
        strSQL = "SELECT Count(*) FROM users WHERE userLoginId='" & _userLoginId & "' "
        strSQL &= "AND userPassword=password('" & _userPassword & "') And userActive=1 "
        'Check Valid User
        If (Convert.ToUInt16(GetScalarData(strSQL)) > 0) Then
            Dim drObj As System.Data.Odbc.OdbcDataReader
            drObj = GetDataReader("SELECT userID,userLoginID,userEmail,userModules,userDefaultWhs FROM users WHERE userLoginId='" & _userLoginId & "' AND userPassword=password('" & _userPassword & "')")
            While drObj.Read
                HttpContext.Current.Session("UserID") = drObj.Item("userID").ToString
                HttpContext.Current.Session("LoginID") = drObj.Item("userLoginID").ToString
                HttpContext.Current.Session("EmailID") = drObj.Item("userEmail").ToString
                HttpContext.Current.Session("UserModules") = drObj.Item("userModules").ToString
                HttpContext.Current.Session("UserWarehouse") = drObj.Item("userDefaultWhs").ToString
            End While
            drObj.Close()
            CloseDatabaseConnection()
            Return True
        Else
            Return False
        End If
    End Function
    ''' <summary>
    ''' Validation for User
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ValidateUser() As Boolean
        Dim strSQL As String
        strSQL = "SELECT Count(*) FROM users WHERE userLoginId='" & _userLoginId & "' "
        strSQL &= "AND userPassword=password('" & _userPassword & "') And userActive=1 "
        'Check Valid User
        If (Convert.ToUInt16(GetScalarData(strSQL)) > 0) Then
            Dim drObj As System.Data.Odbc.OdbcDataReader
            drObj = GetDataReader("SELECT userID,userLoginID, userSalutation, userFirstName, userLastName, userEmail,userModules,userDefaultWhs,WarehouseCompanyID,CompanyName, userType FROM users left join syswarehouses on syswarehouses.WarehouseCode=users.userDefaultWhs left join syscompanyinfo on syscompanyinfo.CompanyID=syswarehouses.WarehouseCompanyID WHERE userLoginId='" & _userLoginId & "' AND userPassword=password('" & _userPassword & "')")
            While drObj.Read                
                CurrentUser.SetCurrentUserSession(drObj.Item("userID").ToString, drObj.Item("userLoginID").ToString, drObj.Item("userEmail").ToString, drObj.Item("userDefaultWhs").ToString, drObj.Item("WarehouseCompanyID").ToString, drObj.Item("CompanyName").ToString, drObj.Item("userSalutation").ToString & " " & drObj.Item("userFirstName").ToString & " " & drObj.Item("userLastName").ToString, drObj.Item("userModules").ToString())
                'Current.Session("UserID") = drObj.Item("userID").ToString
                'Current.Session("LoginID") = drObj.Item("userLoginID").ToString
                'Current.Session("EmailID") = drObj.Item("userEmail").ToString
                'HttpContext.Current.Session("UserModules") = drObj.Item("userModules").ToString()
                'Current.Session("UserWarehouse") = drObj.Item("userDefaultWhs").ToString
                'Current.Session("DefaultCompanyID") = drObj.Item("WarehouseCompanyID").ToString
                'Current.Session("CompanyName") = drObj.Item("CompanyName").ToString
                'Current.Session("UserName") = drObj.Item("userSalutation").ToString & " " & drObj.Item("userFirstName").ToString & " " & drObj.Item("userLastName").ToString
                HttpContext.Current.Session("CurrentUserType") = drObj.Item("userType").ToString()
            End While
            drObj.Close()
            strSQL = "SELECT CompanySalesRepRestricted FROM syscompanyinfo limit 1"
            HttpContext.Current.Session("SalesRepRestricted") = GetScalarData(strSQL)
            CloseDatabaseConnection()
            Return True
        Else
            Return False
        End If
    End Function
    ''' <summary>
    ''' Check Duplicate User Email ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateUserEmailID() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM users where userEmail='" & _userEmail & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Check Duplicate User Login ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateUserLoginId() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM users where userLoginId='" & _userLoginId & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Delete User
    ''' </summary>
    ''' <param name="DeleteID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteUser(ByVal DeleteID As String) As String
        Dim strSQL As String
        strSQL = "DELETE FROM users WHERE UserId='" + DeleteID + "' "
        Return strSql
    End Function
    ''' <summary>
    ''' Fill Grid of User
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        strSQL = "SELECT userId, userLoginId, userEmail, userModules, userType, concat(userFirstName,' ',userLastName) as userName, CASE userActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as userActive FROM users order by userLoginId "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid of User with search criteria
    ''' </summary>
    ''' <param name="StatusData"></param>
    ''' <param name="SearchData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal StatusData As String, ByVal SearchData As String) As String
        Dim strSQL As String
        SearchData = clsCommon.funRemove(SearchData)
        strSQL = "SELECT UserId, userLoginId, userEmail, userModules, userType, concat(userFirstName,' ',userLastName) as userName, CASE userActive WHEN 1 THEN 'green.gif' ELSE 'red.gif' END as userActive FROM users "
        If StatusData <> "" And SearchData <> "" Then
            strSQL += " where userStatus='" + StatusData + "' and "
            strSQL += " ("
            strSQL += " userEmail like '%" + SearchData + "%' or "
            strSQL += " concat(userFirstName,' ',userLastName) like '%" + SearchData + "%' or "
            strSQL += " userPhone like '%" + SearchData + "%' or "
            strSQL += " userLoginId like '%" + SearchData + "%' "
            strSQL += " )"
        ElseIf StatusData <> "" And SearchData = "" Then
            strSQL += " where userStatus='" + StatusData + "' and "
            strSQL += " ("
            strSQL += " userEmail like '%" + SearchData + "%' or "
            strSQL += " concat(userFirstName,' ',userLastName) like '%" + SearchData + "%' or "
            strSQL += " userPhone like '%" + SearchData + "%' or "
            strSQL += " userLoginId like '%" + SearchData + "%' "
            strSQL += " )"
        ElseIf StatusData = "" Then
            strSQL += " where "
            strSQL += " ("
            strSQL += " userEmail like '%" + SearchData + "%' or "
            strSQL += " concat(userFirstName,' ',userLastName) like '%" + SearchData + "%' or "
            strSQL += " userPhone like '%" + SearchData + "%' or "
            strSQL += " userLoginId like '%" + SearchData + "%' "
            strSQL += " )"
        End If
        strSQL += " order by userLoginId "
        Return strSQL
    End Function
    ''' <summary>
    ''' Populate Collection Agent
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub PopulateCollectionAgent(ByVal ddl As DropDownList)
        Dim strSQL As String
        strSQL = "SELECT UserId, concat(userFirstName,' ',userLastName) as UserName FROM users WHERE UserActive='1' and userModules like '%CUA%' order by UserName "
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "UserName"
        ddl.DataValueField = "UserId"
        ddl.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.liSelectCollectionAgent
        ddl.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Return userIDs seperated by comma as string for given userModule
    ''' </summary>
    ''' <param name="userModule"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetUsersByUserModule(ByVal userModule As String) As String
        Dim strSQL As String
        Dim strUserId As String = ""
        Dim drObj As OdbcDataReader
        strSQL = "SELECT * FROM users  "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            Dim strUserModules As String = drObj.Item("userModules").ToString
            If (strUserModules.Contains("ADM") Or strUserModules.Contains(userModule)) Then
                If strUserId <> "" Then
                    strUserId += ", "
                End If
                strUserId += drObj.Item("UserId").ToString
            End If
        End While
        Return strUserId
    End Function
    ''' <summary>
    ''' Check Max User ID
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funMaxUserId() As String
        Dim strSQL As String
        strSQL = "SELECT Max(userID) FROM users "
        Return GetScalarData(strSQL)
    End Function
    ''' <summary>
    ''' Check User Name
    ''' </summary>
    ''' <param name="strUserId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUserName(ByVal strUserId As String) As String
        Dim strSQL, strUserName As String
        strSQL = "SELECT concat(userFirstName,' ',userLastName) as UserName FROM users where UserId='" & strUserId & "'"
        strUserName = GetScalarData(strSQL)
        Return strUserName
    End Function
    ''' <summary>
    ''' Check password
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funPassword() As Boolean
        Dim objComm As New clsCommon
        Dim strSql, strPwd As String
        strPwd = objComm.RandomString()
        strSql = "SELECT count(*) FROM users where userLoginId='" & _userLoginId & "' and userEmail='" & _userEmail & "' and userActive='1' "
        If GetScalarData(strSql) = 0 Then
            Return False
        Else
            'Password Reset
            strSql = "Update Users Set userPassword=password('" & strPwd & "') where userEmail='" & _userEmail & "' and userLoginId='" & _userLoginId & "'"
            SetData(strSql)
            Dim strMailfrom As String = ConfigurationManager.AppSettings("ErrorEmailFrom")
            Dim strMailMessage As String = ""
            strMailMessage = Resources.Resource.lblResetPwdMsg & " " & strPwd
            Dim objCommon As New clsCommon
            'Sending Password Mail
            Dim strMailSubject As String
            strMailSubject = Resources.Resource.PasswordReset
            objCommon.SendMail(strMailSubject, strMailMessage, _userEmail, strMailfrom)
            objCommon = Nothing
            Return True
        End If
    End Function

    ''' <summary>
    ''' Function to get all userTypemodules
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>//done in Inbiz 4.0 
    Public Function funGetAllUserTypeModules() As DataTable
        Dim objDataAccess As New clsDataClass
        Return objDataAccess.GetDataTable("SELECT s.UserTypeID, s.ModuleID, s1.UserTypeName, s1.LocalResourceKey, sm.ModuleName, sm.ModuleAbbreviation, sm.LocalResourceKey FROM sysusertypemodules s INNER JOIN sysusertype s1 ON s1.UserTypeID = s.UserTypeID INNER JOIN sysmodules sm ON sm.ModuleID = s.ModuleID ORDER BY s1.UserTypeName, sm.ModuleName;")
    End Function

    ''' <summary>
    ''' Function to get all usermodules
    ''' </summary>
    ''' <param name="uID"></param>
    ''' <param name="uTypeId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckUserTypeIsAssociated(ByVal uID As String, ByVal uTypeId As String) As Boolean
        Dim objDataAccess As New clsDataClass
        Dim strQry As String
        strQry = "SELECT COUNT(*) AS UserTypeCount FROM sysusermodules s WHERE s.UserID = @UserID AND s.UserTypeID = @UserTypeID;"
        strQry = strQry.Replace("@UserID", uID).Replace("@UserTypeID", clsCommon.funRemove(uTypeId))
        Dim dt As DataTable = objDataAccess.GetDataTable(strQry)
        Return IIf(CType(dt.Rows(0)("UserTypeCount"), Integer) > 0, True, False)
    End Function

    ''' <summary>
    ''' Function to get all usermodules
    ''' </summary>
    ''' <param name="uID"></param>
    ''' <param name="uTypeId"></param>
    ''' <param name="moduleId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetAssociatedModule(ByVal uID As String, ByVal uTypeId As String, ByVal moduleId As String) As DataTable
        Dim objDataAccess As New clsDataClass
        Dim strQry As String
        strQry = "SELECT s.UserID, s.UserTypeID, s.ModuleID, s.AccessType FROM sysusermodules s WHERE s.UserID = @UserID AND s.UserTypeID = @UserTypeID AND s.ModuleID = @ModuleID"
        strQry = strQry.Replace("@UserID", clsCommon.funRemove(uID)).Replace("@UserTypeID", clsCommon.funRemove(uTypeId)).Replace("@ModuleID", clsCommon.funRemove(moduleId))
        Return objDataAccess.GetDataTable(strQry)
    End Function
    ''' <summary>
    ''' Delete User Modules
    ''' </summary>
    ''' <param name="uID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteUserModules(ByVal uID As String) As Integer
        Dim objDataAccess As New clsDataClass
        Dim strQry As String = "DELETE FROM sysusermodules  WHERE UserID = @UserID;"
        strQry = strQry.Replace("@UserID", clsCommon.funRemove(uID))
        Return objDataAccess.SetData(strQry)
    End Function

    'Function To Save UserModule
    Public Function funSaveUserModule(ByVal uID As String, ByVal uTypeId As String, ByVal moduleId As String, ByVal accessType As String) As Integer
        Dim objDataAccess As New clsDataClass
        Dim strQry As String = "INSERT INTO sysusermodules VALUES(@UserID, @UserTypeID, @ModuleID, '@AccessType');"
        strQry = strQry.Replace("@UserID", clsCommon.funRemove(uID)).Replace("@UserTypeID", clsCommon.funRemove(uTypeId)).Replace("@ModuleID", clsCommon.funRemove(moduleId)).Replace("@AccessType", clsCommon.funRemove(accessType))
        Return objDataAccess.SetData(strQry)
    End Function
    ''' <summary>
    '''  Update User Module
    ''' </summary>
    ''' <param name="uID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateUserModule(ByVal uID As String) As Boolean
        Dim moduleSQL As String = "SELECT DISTINCT sm.ModuleAbbreviation FROM sysusermodules s INNER JOIN sysmodules sm ON s.ModuleID = sm.ModuleID WHERE s.UserID = @UserID;"
        Dim objDataAccess As New clsDataClass
        Dim moduleValues As New DataTable
        Dim modules As String = String.Empty
        moduleValues = objDataAccess.GetDataTable(moduleSQL.Replace("@UserID", uID))

        For Each m As DataRow In moduleValues.Rows
            modules = modules + m(0).ToString() + ","
        Next
        modules = modules.TrimEnd(",")
        Dim strSql As String
        strSql = "UPDATE users SET"
        strSql += " userModules='" + modules
        strSql += "' WHERE userID=" + clsCommon.funRemove(uID)

        If objDataAccess.SetData(strSql) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
End Class

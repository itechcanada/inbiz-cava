Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsPrdAvailableWhs
	Inherits clsDataClass
	Private _id, _prdWhsID, _prdWhsCode, _prdBlock, _prdFloor, _prdAisle, _prdRack, _prdBin As String
	Public Property Id() As String
		Get
			Return _id
		End Get
		Set(ByVal value As String)
            _id = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PrdWhsID() As String
		Get
			Return _prdWhsID
		End Get
		Set(ByVal value As String)
            _prdWhsID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PrdWhsCode() As String
		Get
			Return _prdWhsCode
		End Get
		Set(ByVal value As String)
            _prdWhsCode = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PrdBlock() As String
		Get
			Return _prdBlock
		End Get
		Set(ByVal value As String)
            _prdBlock = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PrdFloor() As String
		Get
			Return _prdFloor
		End Get
		Set(ByVal value As String)
            _prdFloor = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PrdAisle() As String
		Get
			Return _prdAisle
		End Get
		Set(ByVal value As String)
            _prdAisle = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PrdRack() As String
		Get
			Return _prdRack
		End Get
		Set(ByVal value As String)
            _prdRack = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PrdBin() As String
		Get
			Return _prdBin
		End Get
		Set(ByVal value As String)
            _prdBin = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    '''  Insert PrdAvailableWhs
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertPrdAvailableWhs() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO prdavailablewhs(prdWhsID, prdWhsCode, prdBlock, prdFloor, prdAisle, prdRack, prdBin) VALUES("
        strSQL += "'" + _prdWhsID + "','"
        strSQL += _prdWhsCode + "','"
        strSQL += _prdBlock + "','"
        strSQL += _prdFloor + "','"
        strSQL += _prdAisle + "','"
        strSQL += _prdRack + "','"
        strSQL += _prdBin + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim objDataClass As New clsDataClass
        Dim intData As Int16 = 0
        intData = objDataClass.PopulateData(strSQL)
        objDataClass.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update PrdAvailableWhs
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdatePrdAvailableWhs() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE prdavailablewhs set "
        strSQL += "prdWhsID='" + _prdWhsID + "', "
        strSQL += "prdWhsCode='" + _prdWhsCode + "', "
        strSQL += "prdBlock='" + _prdBlock + "', "
        strSQL += "prdFloor='" + _prdFloor + "', "
        strSQL += "prdAisle='" + _prdAisle + "', "
        strSQL += "prdRack='" + _prdRack + "', "
        strSQL += "prdBin='" + _prdBin + "'"
        strSQL += " where id='" + _id + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim objDataClass As New clsDataClass
        Dim intData As Int16 = 0
        intData = objDataClass.PopulateData(strSQL)
        objDataClass.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of PrdAvailableWhs
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetPrdAvailableWhsInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  prdWhsID, prdWhsCode, prdBlock, prdFloor, prdAisle, prdRack, prdBin FROM prdavailablewhs where id='" + _id + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _prdWhsID = drObj.Item("prdWhsID").ToString
            _prdWhsCode = drObj.Item("prdWhsCode").ToString
            _prdBlock = drObj.Item("prdBlock").ToString
            _prdFloor = drObj.Item("prdFloor").ToString
            _prdAisle = drObj.Item("prdAisle").ToString
            _prdRack = drObj.Item("prdRack").ToString
            _prdBin = drObj.Item("prdBin").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' sub Fill Avail Whs
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subFillAvailWhs() As String
        Dim strSql As String = "SELECT id,prdBlock,prdFloor,WarehouseDescription FROM prdavailablewhs"
        strSql += " inner join syswarehouses as w on w.WarehouseCode=prdavailablewhs.prdWhsCode where prdWhsID='" & HttpContext.Current.Request.QueryString("prdID") & "' and WarehouseActive='1'"
        Return strSql
    End Function
    ''' <summary>
    ''' sub Avail Whsd Delete
    ''' </summary>
    ''' <param name="strID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subAvailWhsdDelete(ByVal strID As String) As String
        Dim strSql As String = "delete from prdavailablewhs where Id ='" & strID & "'"
        Return strSql
    End Function
    ''' <summary>
    ''' Fill Warehouse Name in Dropdown list
    ''' </summary>
    ''' <param name="dlWh"></param>
    ''' <remarks></remarks>
    Public Sub subGetWarehouse(ByVal dlWh As DropDownList)
        Dim objDataclass As New clsDataClass
        Dim strSQL As String
        strSQL = "SELECT WarehouseDescription,WarehouseCode from syswarehouses where WarehouseActive='1'"
        dlWh.DataSource = objDataclass.GetDataReader(strSQL)
        dlWh.DataTextField = "WarehouseDescription"
        dlWh.DataValueField = "WarehouseCode"
        dlWh.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = "----Select Warehouse----"
        dlWh.Items.Insert(0, itm)
        objDataclass.CloseDatabaseConnection()
        objDataclass = Nothing
    End Sub
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports iTECH.InbizERP.BusinessLogic
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsCollection
	Inherits clsDataClass
	Private _CollectionID, _ColInvID, _ColUserAssignedTo, _ColUserAssignedDatetime, _ColUserAssignedByManager, _ColLastActivity, _ColNotes, _ColStatus As String
	Public Property CollectionID() As String
		Get
			Return _CollectionID
		End Get
		Set(ByVal value As String)
            _CollectionID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ColInvID() As String
		Get
			Return _ColInvID
		End Get
		Set(ByVal value As String)
            _ColInvID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ColUserAssignedTo() As String
		Get
			Return _ColUserAssignedTo
		End Get
		Set(ByVal value As String)
            _ColUserAssignedTo = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ColUserAssignedDatetime() As String
		Get
			Return _ColUserAssignedDatetime
		End Get
		Set(ByVal value As String)
            _ColUserAssignedDatetime = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ColUserAssignedByManager() As String
		Get
			Return _ColUserAssignedByManager
		End Get
		Set(ByVal value As String)
            _ColUserAssignedByManager = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ColLastActivity() As String
		Get
			Return _ColLastActivity
		End Get
		Set(ByVal value As String)
            _ColLastActivity = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ColNotes() As String
		Get
			Return _ColNotes
		End Get
		Set(ByVal value As String)
            _ColNotes = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ColStatus() As String
		Get
			Return _ColStatus
		End Get
		Set(ByVal value As String)
            _ColStatus = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert Collection
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertCollection() As Boolean
		Dim strSQL As String
        strSQL = "INSERT INTO collection( ColInvID, ColUserAssignedTo, ColUserAssignedDatetime, ColUserAssignedByManager, ColLastActivity, ColNotes, ColStatus) VALUES('"
		strSQL += _ColInvID + "','"
		strSQL += _ColUserAssignedTo + "','"
		strSQL += _ColUserAssignedDatetime + "','"
		strSQL += _ColUserAssignedByManager + "','"
		strSQL += _ColLastActivity + "','"
		strSQL += _ColNotes + "','"
		strSQL += _ColStatus + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update Collection
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updateCollection() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE collection set "
		strSQL += "ColInvID='" + _ColInvID + "', "
		strSQL += "ColUserAssignedTo='" + _ColUserAssignedTo + "', "
		strSQL += "ColUserAssignedDatetime='" + _ColUserAssignedDatetime + "', "
		strSQL += "ColUserAssignedByManager='" + _ColUserAssignedByManager + "', "
		strSQL += "ColLastActivity='" + _ColLastActivity + "', "
		strSQL += "ColNotes='" + _ColNotes + "', "
		strSQL += "ColStatus='" + _ColStatus + "'"
		strSQL += " where CollectionID='" + _CollectionID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of Collection
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getCollectionInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  ColInvID, ColUserAssignedTo, ColUserAssignedDatetime, ColUserAssignedByManager, ColLastActivity, ColNotes, ColStatus FROM collection where CollectionID='" + _CollectionID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_ColInvID = drObj.Item("ColInvID").ToString
			_ColUserAssignedTo = drObj.Item("ColUserAssignedTo").ToString
			_ColUserAssignedDatetime = drObj.Item("ColUserAssignedDatetime").ToString
			_ColUserAssignedByManager = drObj.Item("ColUserAssignedByManager").ToString
			_ColLastActivity = drObj.Item("ColLastActivity").ToString
			_ColNotes = drObj.Item("ColNotes").ToString
			_ColStatus = drObj.Item("ColStatus").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Check Invoice In Collection
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckInvoiceInCollection() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM collection where ColInvID='" + _ColInvID + "' "
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.GetScalarData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Fill Grid For Payment Not Received or Partial Received
    ''' </summary>
    ''' <param name="strOption"></param>
    ''' <param name="SearchDays"></param>
    ''' <param name="strCustName"></param>
    ''' <param name="strCustPhone"></param>
    ''' <param name="strInvoiceNo"></param>
    ''' <param name="strAssignedTo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal strOption As String, ByVal SearchDays As String, ByVal strCustName As String, ByVal strCustPhone As String, ByVal strInvoiceNo As String, ByVal strAssignedTo As String) As String
        Dim objAR As New clsAccountReceivable
        Dim strSQL As String
        strCustName = clsCommon.funRemove(strCustName)
        strCustPhone = clsCommon.funRemove(strCustPhone)
        strInvoiceNo = clsCommon.funRemove(strInvoiceNo)
        strAssignedTo = clsCommon.funRemove(strAssignedTo)
        If HttpContext.Current.Session("UserModules").ToString.Contains("COL") = True Or HttpContext.Current.Session("UserModules").ToString.Contains("ADM") = True Then
            strSQL = "SELECT distinct o.invID,o.InvRefNo,invShpWhsCode,PartnerPhone,PartnerAcronyme, PartnerLongName as CustomerName, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as InvAmount, if(sum(ARAmtRcvd) Is Null,0, sum(ARAmtRcvd)) as BalanceAmount, invStatus, invCustPO, invForOrderNo, ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, o.invRefType FROM invoices o inner join invoiceitems i on i.invoices_invID=o.invID left join partners on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID inner join collection c on c.ColInvID=o.invID  "
        Else
            strSQL = "SELECT distinct o.invID,o.InvRefNo,invShpWhsCode,PartnerPhone,PartnerAcronyme, PartnerLongName as CustomerName, DATE_FORMAT(invCreatedOn,'%m-%d-%Y') as invDate, PartnerInvoiceNetTerms as NetTerms, sum(invProductQty * invProductUnitPrice*invCurrencyExRate) as InvAmount, if(sum(ARAmtRcvd) Is Null,0, sum(ARAmtRcvd)) as BalanceAmount, invStatus, invCustPO, invForOrderNo, ColUserAssignedTo, concat(u.userFirstName,' ',u.userLastName) as AssignedTo, o.invCurrencyCode, o.invCurrencyExRate, o.invRefType FROM invoices o inner join collection c on c.ColInvID=o.invID and c.ColUserAssignedTo='" & HttpContext.Current.Session("UserID") & "' inner join invoiceitems i on i.invoices_invID=o.invID left join partners on partners.PartnerID =o.invCustID left join accountreceivable a on a.ARInvoiceNo=o.invID  "
        End If
        If strAssignedTo <> "" Then
            strSQL += " Inner join users u on u.userID=c.ColUserAssignedTo"
        Else
            strSQL += " left join users u on u.userID=c.ColUserAssignedTo"
        End If

        strSQL += " where o.invStatus<>'" & InvoicesStatus.PAYMENT_RECEIVED.ToString() & "' "

        If strOption = StatusCollectionSearchOption.NULL_VALUE Then
            strSQL += " and c.ColUserAssignedTo is null "
        ElseIf strOption = StatusCollectionSearchOption.NOT_NULL_VALUE Then
            strSQL += " and c.ColUserAssignedTo is not null "
        ElseIf strOption = StatusCollectionSearchOption.ALL_VALUE Then

        End If
        If strCustName <> "" Then
            strSQL += " and (PartnerLongName like '%" & strCustName & "%' OR PartnerAcronyme like '%" + strCustName + "%' )"
        End If
        If strCustPhone <> "" Then
            strSQL += " and PardtnerPhone like '%" & strCustPhone & "%'"
        End If
        If strInvoiceNo <> "" Then
            strSQL += " and InvRefNo like '%" & strInvoiceNo & "%'"
        End If

        If strAssignedTo <> "" Then
            strSQL += " and concat(u.userFirstName,' ',u.userLastName) like '%" & strAssignedTo & "%'"
        End If

        If SearchDays <> "" Then
            'If SearchDays = "180p" Then
            '    strSQL += " and (o.invCreatedOn<now())"
            'Else
            '    strSQL += " and (o.invCreatedOn < SUBDATE(now(), " & SearchDays & "))"
            'End If
            strSQL += objAR.funCreatedDateRage(SearchDays)
        End If

        strSQL += " group by o.invID, a.ARInvoiceNo order by o.invCreatedOn,o.invID desc, CustomerName "
        Return strSQL
    End Function
End Class

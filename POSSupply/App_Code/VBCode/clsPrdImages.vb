Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsPrdImages
	Inherits clsDataClass
    Private _prdImageID, _prdID, _prdSmallImagePath, _prdLargeImagePath, _pimgIsDefault As String
    Private _PrdLowImg1, _PrdLowImg2, _PrdLowImg3, _PrdLowImg4 As String
    Private _PrdHighImg1, _PrdHighImg2, _PrdHighImg3, _PrdHighImg4 As String
    Private _prdImageID1, _prdImageID2, _prdImageID3, _prdImageID4 As String
    Public Property PrdImageID() As String
        Get
            Return _prdImageID
        End Get
        Set(ByVal value As String)
            _prdImageID = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Property PrdID() As String
		Get
			Return _prdID
		End Get
		Set(ByVal value As String)
            _prdID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PrdSmallImagePath() As String
		Get
			Return _prdSmallImagePath
		End Get
		Set(ByVal value As String)
            _prdSmallImagePath = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PrdLargeImagePath() As String
		Get
			Return _prdLargeImagePath
		End Get
		Set(ByVal value As String)
            _prdLargeImagePath = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property PimgIsDefault() As String
        Get
            Return _pimgIsDefault
        End Get
        Set(ByVal value As String)
            _pimgIsDefault = clsCommon.funRemove(value, True)
        End Set
    End Property

#Region "Image Property"
    Public Property PrdLowImg1() As String
        Get
            Return _PrdLowImg1
        End Get
        Set(ByVal value As String)
            _PrdLowImg1 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdLowImg2() As String
        Get
            Return _PrdLowImg2
        End Get
        Set(ByVal value As String)
            _PrdLowImg2 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdLowImg3() As String
        Get
            Return _PrdLowImg3
        End Get
        Set(ByVal value As String)
            _PrdLowImg3 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdLowImg4() As String
        Get
            Return _PrdLowImg4
        End Get
        Set(ByVal value As String)
            _PrdLowImg4 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdHighImg1() As String
        Get
            Return _PrdHighImg1
        End Get
        Set(ByVal value As String)
            _PrdHighImg1 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdHighImg2() As String
        Get
            Return _PrdHighImg2
        End Get
        Set(ByVal value As String)
            _PrdHighImg2 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdHighImg3() As String
        Get
            Return _PrdHighImg3
        End Get
        Set(ByVal value As String)
            _PrdHighImg3 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdHighImg4() As String
        Get
            Return _PrdHighImg4
        End Get
        Set(ByVal value As String)
            _PrdHighImg4 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdImageID1() As String
        Get
            Return _prdImageID1
        End Get
        Set(ByVal value As String)
            _prdImageID1 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdImageID2() As String
        Get
            Return _prdImageID2
        End Get
        Set(ByVal value As String)
            _prdImageID2 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdImageID3() As String
        Get
            Return _prdImageID3
        End Get
        Set(ByVal value As String)
            _prdImageID3 = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdImageID4() As String
        Get
            Return _prdImageID4
        End Get
        Set(ByVal value As String)
            _prdImageID4 = clsCommon.funRemove(value, True)
        End Set
    End Property
#End Region

	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert PrdImages
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertPrdImages() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO prdimages(prdID, prdSmallImagePath, prdLargeImagePath,pimgIsDefault) VALUES("
        strSQL += "'" + _prdID + "','"
        strSQL += _prdSmallImagePath + "','"
        strSQL += _prdLargeImagePath + "','"
        strSQL += _pimgIsDefault + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update PrdImages
    ''' </summary>
    ''' <param name="strImageID"></param>
    ''' <param name="strImgType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdatePrdImages(ByVal strImageID As String, ByVal strImgType As String) As Boolean
        Dim strSQL As String
        strSQL = "UPDATE prdimages set "
        If strImgType = "low" Then
            strSQL += "prdSmallImagePath='" + _prdSmallImagePath + "'"
        Else
            strSQL += "prdLargeImagePath='" + _prdLargeImagePath + "'"
        End If
        strSQL += " where prdImageID='" + strImageID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of PrdImages
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetPrdImagesInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  prdImageID, prdSmallImagePath, prdLargeImagePath,pimgIsDefault FROM prdimages where prdID ='" + _prdID + "' "
        drObj = GetDataReader(strSQL)
        Dim intCount As Integer = 1

        While drObj.Read

            If intCount = 1 Then ' drObj.Item("prdSmallImagePath").ToString.Contains("_low_1") Or drObj.Item("prdLargeImagePath").ToString.Contains("_high_1") Then
                _prdImageID1 = drObj.Item("prdImageID").ToString
                _PrdLowImg1 = drObj.Item("prdSmallImagePath").ToString
                _PrdHighImg1 = drObj.Item("prdLargeImagePath").ToString
            ElseIf intCount = 2 Then  'drObj.Item("prdSmallImagePath").ToString.Contains("_low_2") Or drObj.Item("prdLargeImagePath").ToString.Contains("_high_2") Then
                _prdImageID2 = drObj.Item("prdImageID").ToString
                _PrdLowImg2 = drObj.Item("prdSmallImagePath").ToString
                _PrdHighImg2 = drObj.Item("prdLargeImagePath").ToString
            ElseIf intCount = 3 Then 'drObj.Item("prdSmallImagePath").ToString.Contains("_low_3") Or drObj.Item("prdLargeImagePath").ToString.Contains("_high_3") Then
                _prdImageID3 = drObj.Item("prdImageID").ToString
                _PrdLowImg3 = drObj.Item("prdSmallImagePath").ToString
                _PrdHighImg3 = drObj.Item("prdLargeImagePath").ToString
            ElseIf intCount = 4 Then 'drObj.Item("prdSmallImagePath").ToString.Contains("_low_4") Or drObj.Item("prdLargeImagePath").ToString.Contains("_high_4") Then
                _prdImageID4 = drObj.Item("prdImageID").ToString
                _PrdLowImg4 = drObj.Item("prdSmallImagePath").ToString
                _PrdHighImg4 = drObj.Item("prdLargeImagePath").ToString
            End If
            _prdImageID = drObj.Item("prdImageID").ToString
            '_prdSmallImagePath = drObj.Item("prdSmallImagePath").ToString
            '_prdLargeImagePath = drObj.Item("prdLargeImagePath").ToString
            _pimgIsDefault = drObj.Item("pimgIsDefault").ToString

            intCount += 1
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Get the Max PrdIDImage from Product
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funGetMaxPrdImageID() As Integer
        Dim objDataClass As New clsDataClass
        Dim strSql, strMaxPrdImgId As String
        strSql = "SELECT MAX(prdImageID) FROM prdimages"
        strMaxPrdImgId = Convert.ToString(objDataClass.GetScalarData(strSql))
        strMaxPrdImgId = IIf(strMaxPrdImgId = "", 0, strMaxPrdImgId)
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return strMaxPrdImgId
    End Function
    ''' <summary>
    ''' Delete Image Info
    ''' </summary>
    ''' <param name="strImageID"></param>
    ''' <param name="strImgType"></param>
    ''' <param name="strImagePath"></param>
    ''' <remarks></remarks>
    Public Sub SubDeleteImage(ByVal strImageID As String, ByVal strImgType As String, ByVal strImagePath As String)
        Dim strSQL As String
        strSQL = "Update prdimages set "
        If strImgType = "low" Then
            strSQL += "  prdSmallImagePath='" & strImagePath & "' "
        Else
            strSQL += "  prdLargeImagePath='" & strImagePath & "' "
        End If
        strSQL += " Where prdImageID='" & strImageID & "'"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        Dim objDataClass As New clsDataClass
        objDataClass.SetData(strSQL)
        objDataClass.CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Is Exist Image
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funIsExistImage() As Integer
        Dim strSQL As String
        Dim intI As Integer
        Dim objDataClass As New clsDataClass
        strSQL = "SELECT count(*) FROM prdimages where prdID='" & _prdID & "'"
        intI = objDataClass.GetScalarData(strSQL)
        objDataClass = Nothing
        Return intI
    End Function
End Class

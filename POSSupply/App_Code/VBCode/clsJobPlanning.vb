Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports System.Threading
Imports System.Globalization
Imports System.Math
''Imports System.Web.HttpContext
''Imports clsCommon
''Imports Resources.Resource

Public Class clsJobPlanning
    Inherits clsDataClass
    Private _JobPlanningID, _UserID, _OrderID, _Active, _CustomerID, _CustomerType, _JobDueDate, _jobNote As String
    Public Property JobPlanningID() As String
        Get
            Return _JobPlanningID
        End Get
        Set(ByVal value As String)
            _JobPlanningID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property UserID() As String
        Get
            Return _UserID
        End Get
        Set(ByVal value As String)
            _UserID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property OrdID() As String
        Get
            Return _OrderID
        End Get
        Set(ByVal value As String)
            _OrderID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property Active() As String
        Get
            Return _Active
        End Get
        Set(ByVal value As String)
            _Active = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustID() As String
        Get
            Return _CustomerID
        End Get
        Set(ByVal value As String)
            _CustomerID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property CustType() As String
        Get
            Return _CustomerType
        End Get
        Set(ByVal value As String)
            _CustomerType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property JobDueDate() As String
        Get
            Return _JobDueDate
        End Get
        Set(ByVal value As String)
            _JobDueDate = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property jobNote() As String
        Get
            Return _jobNote
        End Get
        Set(ByVal value As String)
            _jobNote = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Sub New()
        MyBase.New()
    End Sub
    ''' <summary>
    ''' Insert JobPlanning
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertJobPlanning() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO jobplanning(UserID, OrderID, Active,CustomerID, CustomerType,JobDueDate, jobNote) VALUES('"
        strSQL += _UserID + "','"
        strSQL += _OrderID + "','"
        strSQL += _Active + "','"
        strSQL += _CustomerID + "','"
        strSQL += _CustomerType + "','"
        strSQL += _JobDueDate + "','"
        strSQL += _jobNote + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Check Duplicate JobPlanning
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDuplicateJobPlanning() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM jobplanning where OrderID='" & _OrderID & "' and Active='1' and UserID='" & _UserID & "'"
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    '''  Update JobPlanning
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateJobPlanning() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE jobplanning set "
        strSQL += " Active='" + _Active + "'"
        strSQL += " where OrderID='" & _OrderID & "' and UserID='" & _UserID & "'"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Fill Grid of JopPanning
    ''' </summary>
    ''' <param name="strOID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid(ByVal strOID As String) As String
        Dim strSQL As String = ""
        strSQL = "SELECT JobPlanningID,JobDueDate, jobNote,concat(userFirstName,' ',userLastName) as userName FROM jobplanning as j "
        strSQL += " Inner join users u on u.userID=J.userID "
        strSQL += " where OrderID='" & strOID & "'"
        If HttpContext.Current.Session("UserModules").ToString.Contains("JOBP") = True And (HttpContext.Current.Session("UserModules").ToString.Contains("ADM") = False And HttpContext.Current.Session("UserModules").ToString.Contains("JBPM") = False) Then
            strSQL += " and u.UserID='" & HttpContext.Current.Session("UserID") & "'"
        End If
        Return strSQL
    End Function
End Class

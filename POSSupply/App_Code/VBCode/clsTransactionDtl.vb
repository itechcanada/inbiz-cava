Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
Public Class clsTransactionDtl
    Inherits clsDataClass
    Private _posAcctId, _posAcctHdrDtlId, _posTranAcctType, _posTransStartDatetime, _posCardNo, _posCardExpDate, _posCardName, _posCVVCode, _posConfirmatioNo, _posTotalValue, _posTransAcctDtlStatus, _posTransID, _posTransEndTime, _posExtReturnStatus, _posFirstName, _posLastName, _posCardLang, _posCardAccountType, _posCardMsg As String


#Region "Properties"
    Public Property posAcctId() As String
        Get
            Return _posAcctId
        End Get
        Set(ByVal value As String)
            _posAcctId = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posAcctHdrDtlId() As String
        Get
            Return _posAcctHdrDtlId
        End Get
        Set(ByVal value As String)
            _posAcctHdrDtlId = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posTranAcctType() As String
        Get
            Return _posTranAcctType
        End Get
        Set(ByVal value As String)
            _posTranAcctType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posTransStartDatetime() As String
        Get
            Return _posTransStartDatetime
        End Get
        Set(ByVal value As String)
            _posTransStartDatetime = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posCardNo() As String
        Get
            Return _posCardNo
        End Get
        Set(ByVal value As String)
            _posCardNo = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posCardExpDate() As String
        Get
            Return _posCardExpDate
        End Get
        Set(ByVal value As String)
            _posCardExpDate = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posCardName() As String
        Get
            Return _posCardName
        End Get
        Set(ByVal value As String)
            _posCardName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posCVVCode() As String
        Get
            Return _posCVVCode
        End Get
        Set(ByVal value As String)
            _posCVVCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posConfirmatioNo() As String
        Get
            Return _posConfirmatioNo
        End Get
        Set(ByVal value As String)
            _posConfirmatioNo = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posTotalValue() As String
        Get
            Return _posTotalValue
        End Get
        Set(ByVal value As String)
            _posTotalValue = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posTransAcctDtlStatus() As String
        Get
            Return _posTransAcctDtlStatus
        End Get
        Set(ByVal value As String)
            _posTransAcctDtlStatus = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posTransID() As String
        Get
            Return _posTransID
        End Get
        Set(ByVal value As String)
            _posTransID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posTransEndTime() As String
        Get
            Return _posTransEndTime
        End Get
        Set(ByVal value As String)
            _posTransEndTime = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posExtReturnStatus() As String
        Get
            Return _posExtReturnStatus
        End Get
        Set(ByVal value As String)
            _posExtReturnStatus = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posFirstName() As String
        Get
            Return _posFirstName
        End Get
        Set(ByVal value As String)
            _posFirstName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posLastName() As String
        Get
            Return _posLastName
        End Get
        Set(ByVal value As String)
            _posLastName = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posCardLang() As String
        Get
            Return _posCardLang
        End Get
        Set(ByVal value As String)
            _posCardLang = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posCardAccountType() As String
        Get
            Return _posCardAccountType
        End Get
        Set(ByVal value As String)
            _posCardAccountType = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property posCardMsg() As String
        Get
            Return _posCardMsg
        End Get
        Set(ByVal value As String)
            _posCardMsg = clsCommon.funRemove(value, True)
        End Set
    End Property

#End Region

    Public Sub New()
        MyBase.New()
    End Sub

#Region "Function"
    ''' <summary>
    ''' Insert Transaction
    ''' </summary>
    ''' <param name="sID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertPosTransaction(ByRef sID As String) As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO postransacctdtl(posAcctHdrDtlId, posTranAcctType, posTransStartDatetime, posCardNo, posCardExpDate, posCardName, posCVVCode, posConfirmatioNo, posTotalValue, posTransAcctDtlStatus, posTransID, posTransEndTime, posExtReturnStatus,posFirstName,posLastName,posCardLang, posCardAccountType, posCardMsg) VALUES('"
        strSQL += _posAcctHdrDtlId + "','"
        strSQL += _posTranAcctType + "','"
        strSQL += _posTransStartDatetime + "','"
        strSQL += _posCardNo + "','"
        strSQL += _posCardExpDate + "','"
        strSQL += _posCardName + "','"
        strSQL += _posCVVCode + "','"
        strSQL += _posConfirmatioNo + "','"
        strSQL += _posTotalValue + "','"
        strSQL += _posTransAcctDtlStatus + "','"
        strSQL += _posTransID + "','"
        strSQL += _posTransEndTime + "','"
        strSQL += _posExtReturnStatus + "','"
        strSQL += _posFirstName + "','"
        strSQL += _posLastName + "','"
        strSQL += _posCardLang + "','"
        strSQL += _posCardAccountType + "','"
        strSQL += _posCardMsg + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        sID = obj.GetScalarData("Select LAST_INSERT_ID()")
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update Invoices
    ''' </summary>
    ''' <param name="strPosTranAcctID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateTransaction(ByVal strPosTranAcctID As String) As Boolean
        Dim strSQL As String
        strSQL = "UPDATE postransacctdtl set "
        ' strSQL += "posAcctHdrDtlId='" + _posAcctHdrDtlId + "', "
        'strSQL += "posTranAcctType='" + _posTranAcctType + "', "
        strSQL += "posCardNo='" + _posCardNo + "', "
        strSQL += "posCardExpDate='" + _posCardExpDate + "', "
        strSQL += "posCardName='" + _posCardName + "', "
        strSQL += "posCVVCode='" + _posCVVCode + "', "
        strSQL += "posConfirmatioNo='" + _posConfirmatioNo + "', "
        'strSQL += "posTotalValue='" + _posTotalValue + "', "
        strSQL += "posTransAcctDtlStatus='" + _posTransAcctDtlStatus + "', "
        strSQL += "posTransID='" + _posTransID + "', "
        strSQL += "posTransEndTime='" + _posTransEndTime + "', "
        'strSQL += "posTranAcctType='" + _posTranAcctType + "', "
        strSQL += "posExtReturnStatus='" + _posExtReturnStatus + "', "
        strSQL += "posCardLang='" + _posCardLang + "', "
        strSQL += "posCardAccountType='" + _posCardAccountType + "', "
        strSQL += "posCardMsg='" + _posCardMsg + "'"
        strSQL += " where posAcctId='" + strPosTranAcctID + "'"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of Transaction
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetTransactionInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT posAcctHdrDtlId, posTranAcctType, posTransStartDatetime, posCardNo, posCardExpDate, posCardName, posCVVCode, posConfirmatioNo, posTotalValue, posTransAcctDtlStatus, posTransID, posTransEndTime, posExtReturnStatus,posCardLang, posCardAccountType, posCardMsg FROM postransacctdtl where posAcctId='" + _posAcctId + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _posAcctHdrDtlId = drObj.Item("posAcctHdrDtlId").ToString
            _posTranAcctType = drObj.Item("posTranAcctType").ToString
            _posTransStartDatetime = drObj.Item("posTransStartDatetime").ToString
            _posCardNo = drObj.Item("posCardNo").ToString
            _posCardExpDate = drObj.Item("posCardExpDate").ToString
            _posCardName = drObj.Item("posCardName").ToString
            _posCVVCode = drObj.Item("posCVVCode").ToString
            _posConfirmatioNo = drObj.Item("posConfirmatioNo").ToString
            _posTotalValue = drObj.Item("posTotalValue").ToString
            _posTransAcctDtlStatus = drObj.Item("posTransAcctDtlStatus").ToString
            _posTransID = drObj.Item("posTransID").ToString
            _posTransEndTime = drObj.Item("posTransEndTime").ToString
            _posExtReturnStatus = drObj.Item("posExtReturnStatus").ToString
            _posCardLang = drObj.Item("posCardLang").ToString
            _posCardAccountType = drObj.Item("posCardAccountType").ToString
            _posCardMsg = drObj.Item("posCardMsg").ToString
        End While
        CloseDatabaseConnection()
        drObj.Close()
    End Sub
    ''' <summary>
    ''' Get Account Hdrs
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetAccountHdrID()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT posAcctHdrDtlId FROM postransacctdtl where posAcctId='" + _posAcctId + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _posAcctHdrDtlId = drObj.Item("posAcctHdrDtlId").ToString
        End While
        CloseDatabaseConnection()
        drObj.Close()
    End Sub
    ''' <summary>
    ''' Get Data For Refund
    ''' </summary>
    ''' <param name="TransID"></param>
    ''' <remarks></remarks>
    Public Sub subGetDataForRefund(ByVal TransID As String)
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT posCardNo,posCardExpDate,posTransID FROM postransacctdtl where posTransID='" + TransID + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _posCardNo = drObj.Item("posCardNo").ToString
            _posCardExpDate = drObj.Item("posCardExpDate").ToString
        End While
        CloseDatabaseConnection()
        drObj.Close()
    End Sub
#End Region
End Class

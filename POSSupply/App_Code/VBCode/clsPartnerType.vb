Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
Public Class clsPartnerType
	Inherits clsDataClass
	Private _ParterTypeID, _ParterTypeLang, _PartnerTypeDesc, _PartnerTypeActive As String
	Public Property ParterTypeID() As String
		Get
			Return _ParterTypeID
		End Get
		Set(ByVal value As String)
            _ParterTypeID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property ParterTypeLang() As String
		Get
			Return _ParterTypeLang
		End Get
		Set(ByVal value As String)
            _ParterTypeLang = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerTypeDesc() As String
		Get
			Return _PartnerTypeDesc
		End Get
		Set(ByVal value As String)
            _PartnerTypeDesc = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerTypeActive() As String
		Get
			Return _PartnerTypeActive
		End Get
		Set(ByVal value As String)
            _PartnerTypeActive = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert PartnerType
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertPartnerType() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO partnertype( ParterTypeLang, PartnerTypeDesc, PartnerTypeActive) VALUES('"
		strSQL += _ParterTypeLang + "','"
		strSQL += _PartnerTypeDesc + "','"
		strSQL += _PartnerTypeActive + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    '''  Update PartnerType
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updatePartnerType() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE partnertype set "
		strSQL += "ParterTypeLang='" + _ParterTypeLang + "', "
		strSQL += "PartnerTypeDesc='" + _PartnerTypeDesc + "', "
		strSQL += "PartnerTypeActive='" + _PartnerTypeActive + "'"
		strSQL += " where ParterTypeID='" + _ParterTypeID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    '''  Populate objects of PartnerType
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPartnerTypeInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  ParterTypeLang, PartnerTypeDesc, PartnerTypeActive FROM partnertype where ParterTypeID='" + _ParterTypeID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_ParterTypeLang = drObj.Item("ParterTypeLang").ToString
			_PartnerTypeDesc = drObj.Item("PartnerTypeDesc").ToString
			_PartnerTypeActive = drObj.Item("PartnerTypeActive").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate Partner Type
    ''' </summary>
    ''' <param name="chklst"></param>
    ''' <param name="strCustType"></param>
    ''' <remarks></remarks>
    Public Sub PopulatePartnerType(ByVal chklst As RadioButtonList, Optional ByVal strCustType As String = "")
        Dim strSQL As String = ""
        Dim strLang As String = clsCommon.funGetProductLang()
        strSQL = "SELECT  ParterTypeID, PartnerTypeDesc, PartnerTypeActive FROM partnertype where PartnerTypeActive='1' and ParterTypeLang='" & strLang & "' "
        'If strCustType = "" Then
        '    strSQL += " And ParterTypeID!='2'"
        'ElseIf strCustType = "E" Then
        '    strSQL += " And ParterTypeID='2'"
        'End If
        chklst.Items.Clear()
        chklst.DataSource = GetDataReader(strSQL)
        chklst.DataTextField = "PartnerTypeDesc"
        chklst.DataValueField = "ParterTypeID"
        chklst.DataBind()
        CloseDatabaseConnection()
    End Sub
End Class

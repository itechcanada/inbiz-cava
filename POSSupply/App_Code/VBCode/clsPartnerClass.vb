Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
''Imports System.Web.HttpContext
''Imports clsCommon
''Imports Resources.Resource
Public Class clsPartnerClass
	Inherits clsDataClass
	Private _PartnerClassID, _PartnerClassLang, _PartnerClassDesc, _PartnerClassActive As String
	Public Property PartnerClassID() As String
		Get
			Return _PartnerClassID
		End Get
		Set(ByVal value As String)
            _PartnerClassID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerClassLang() As String
		Get
			Return _PartnerClassLang
		End Get
		Set(ByVal value As String)
            _PartnerClassLang = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerClassDesc() As String
		Get
			Return _PartnerClassDesc
		End Get
		Set(ByVal value As String)
            _PartnerClassDesc = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PartnerClassActive() As String
		Get
			Return _PartnerClassActive
		End Get
		Set(ByVal value As String)
            _PartnerClassActive = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert PartnerClass
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function insertPartnerClass() As Boolean
		Dim strSQL As String
		strSQL = "INSERT INTO partnerclass( PartnerClassLang, PartnerClassDesc, PartnerClassActive) VALUES('"
		strSQL += _PartnerClassLang + "','"
		strSQL += _PartnerClassDesc + "','"
		strSQL += _PartnerClassActive + "')"
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Update PartnerClass
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function updatePartnerClass() As Boolean
		Dim strSQL As String
		strSQL = "UPDATE partnerclass set "
		strSQL += "PartnerClassLang='" + _PartnerClassLang + "', "
		strSQL += "PartnerClassDesc='" + _PartnerClassDesc + "', "
		strSQL += "PartnerClassActive='" + _PartnerClassActive + "'"
		strSQL += " where PartnerClassID='" + _PartnerClassID + "' "
		strSQL = strSQL.Replace("'sysNull'", "Null")
		strSQL = strSQL.Replace("sysNull", "Null")
		Dim obj As New clsDataClass
		Dim intData As Int16 = 0
		intData = obj.PopulateData(strSQL)
		obj.CloseDatabaseConnection()
		If intData = 0 Then
			Return False
		Else
			Return True
		End If
	End Function
    ''' <summary>
    ''' Populate objects of PartnerClass
    ''' </summary>
    ''' <remarks></remarks>
	Public Sub getPartnerClassInfo()
		Dim strSQL As String
		Dim drObj As OdbcDataReader
		strSQL = "SELECT  PartnerClassLang, PartnerClassDesc, PartnerClassActive FROM partnerclass where PartnerClassID='" + _PartnerClassID + "' "
		drObj = GetDataReader(strSQL)
		While drObj.Read
			_PartnerClassLang = drObj.Item("PartnerClassLang").ToString
			_PartnerClassDesc = drObj.Item("PartnerClassDesc").ToString
			_PartnerClassActive = drObj.Item("PartnerClassActive").ToString
		End While
		drObj.Close()
		CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate Classification
    ''' </summary>
    ''' <param name="rbl"></param>
    ''' <remarks></remarks>
    Public Sub PopulateClassification(ByVal rbl As RadioButtonList)
        Dim strSQL As String = ""
        Dim strLang As String = clsCommon.funGetProductLang()
        strSQL = "SELECT  PartnerClassID, PartnerClassDesc, PartnerClassActive FROM partnerclass where PartnerClassActive='1' and PartnerClassLang='" & strLang & "' "
        rbl.Items.Clear()
        rbl.DataSource = GetDataReader(strSQL)
        rbl.DataTextField = "PartnerClassDesc"
        rbl.DataValueField = "PartnerClassID"
        rbl.DataBind()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Populate Classification
    ''' </summary>
    ''' <param name="dlCla"></param>
    ''' <remarks></remarks>
    Public Sub subGetClassification(ByVal dlCla As DropDownList)
        Dim strSQL As String
        Dim strLang As String = clsCommon.funGetProductLang()
        strSQL = "SELECT  PartnerClassID, PartnerClassDesc, PartnerClassActive FROM partnerclass where PartnerClassActive='1' and PartnerClassLang='" & strLang & "' order by PartnerClassSequence "
        dlCla.DataSource = GetDataReader(strSQL)
        dlCla.DataTextField = "PartnerClassDesc"
        dlCla.DataValueField = "PartnerClassID"
        dlCla.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.msgSelectClassification
        dlCla.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
End Class

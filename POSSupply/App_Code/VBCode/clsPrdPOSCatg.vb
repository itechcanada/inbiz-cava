Imports Microsoft.VisualBasic
''Imports clsCommon

Public Class clsPrdPOSCatg
    Inherits clsDataClass
    Private _prdPOSCatgID, _prdPOSCatgDesc, _prdPOSCatgImgPath, _prdPOSCatgActive, _prdPOSCatgSeq As String
#Region "Properties"
    Public Property PrdPOSCatgID() As String
        Get
            Return _prdPOSCatgID
        End Get
        Set(ByVal value As String)
            _prdPOSCatgID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdPOSCatgDesc() As String
        Get
            Return _prdPOSCatgDesc
        End Get
        Set(ByVal value As String)
            _prdPOSCatgDesc = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdPOSCatgImgPath() As String
        Get
            Return _prdPOSCatgImgPath
        End Get
        Set(ByVal value As String)
            _prdPOSCatgImgPath = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdPOSCatgActive() As String
        Get
            Return _prdPOSCatgActive
        End Get
        Set(ByVal value As String)
            _prdPOSCatgActive = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property PrdPOSCatgSeq() As String
        Get
            Return _prdPOSCatgSeq
        End Get
        Set(ByVal value As String)
            _prdPOSCatgSeq = clsCommon.funRemove(value, True)
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Populate Product POS Categories
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Public Sub PopulatePrdPOSCategories(ByVal ddl As ListBox)
        Dim strSQL As String
        strSQL = "SELECT prdPOSCatgID, prdPOSCatgDesc FROM prdposcatg p WHERE prdPOSCatgActive = '1' "
        ddl.Items.Clear()
        ddl.DataSource = GetDataReader(strSQL)
        ddl.DataTextField = "prdPOSCatgDesc"
        ddl.DataValueField = "prdPOSCatgID"
        ddl.DataBind()
        'Dim itm As New ListItem
        'itm.Value = 0
        'itm.Text = "--Select Categories--"
        'ddl.Items.Insert(0, itm)
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Fills the POS Category Menu
    ''' </summary>
    ''' <param name="strCatgCount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function fillPOSCatgMenu(Optional ByVal strCatgCount As String = "") As String
        Dim strSQL As String = ""
        If strCatgCount = "" Then
            strSQL = " Select * "
        Else
            strSQL = "select Count(*) "
        End If
        strSQL += " FROM prdposcatg WHERE prdposcatgActive =1 and prdPOSCatgId IN (SELECT distinct prdPOSCatgId FROM prdposcatgsel as p inner join products on products.productID =p.productid where isPOSMenu=1 )"
        strSQL += " Order by prdPOSCatgSeq Asc"
        If strCatgCount <> "" Then
            strSQL = GetScalarData(strSQL)
            Return strSQL
        End If
        Return strSQL
    End Function

    ''' <summary>
    ''' Fill Category for list
    ''' </summary>
    ''' <param name="lstCatg"></param>
    ''' <param name="strPrdID"></param>
    ''' <param name="strListType"></param>
    ''' <remarks></remarks>
    Public Sub subGetCatgForList(ByVal lstCatg As ListBox, ByVal strPrdID As String, ByVal strListType As String)
        Dim strSQL As String
        If strListType = "From" Then
            strSQL = "SELECT pc.prdPOSCatgID, pc.prdPOSCatgDesc FROM prdPOSCatg pc WHERE pc.prdPOSCatgActive ='1' and pc.prdPOSCatgID Not IN (SELECT ps.prdPOSCatgID FROM prdPOSCatgSel ps WHERE ps.productID = '" & strPrdID & "')"
        Else
            strSQL = "SELECT p.prdPOSCatgID, p.prdPOSCatgDesc FROM prdposcatg p INNER JOIN prdposcatgsel ps ON p.prdPOSCatgID = ps.prdPOSCatgID WHERE prdPOSCatgActive = '1' and ps.productID = '" & strPrdID & "' "
        End If
        lstCatg.Items.Clear()
        lstCatg.DataSource = GetDataReader(strSQL)
        lstCatg.DataTextField = "prdPOSCatgDesc"
        lstCatg.DataValueField = "prdPOSCatgID"
        lstCatg.DataBind()
    End Sub
    ''' <summary>
    ''' POS Catg Menu For Paging
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function fillPOSCatgMenuForPaging() As String
        Dim strSQL As String = ""
        strSQL = "SELECT distinct pc.prdposcatgID FROM prdposcatg as pc inner join prdposcatgsel as pcs on pcs.prdPOSCatgID=pc.prdposcatgID WHERE prdposcatgActive ='1'"
        Return strSQL
    End Function
End Class

Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
'Imports System.Web.HttpContext
'Imports clsCommon
'Imports Resources.Resource
Imports System.Threading
Imports System.Globalization

Public Class clsRegister
    Inherits clsDataClass
    Private _RegisterCode, _RegisterDescription, _RegisterAddress, _RegisterCity, _RegisterState, _RegisterPostalCode, _RegisterEmailID, _RegisterFax, _RegisterPhone, _RegisterActive, _RegisterRegTaxCode, _RegWhsCode, _RegMerchantId, _RegHost, _RegHostPort, _regTerminalID, _RegLogFilePath, _sysRegMessage, _regPrintMerchantCopy As String
#Region "Properties"

    Public Property RegisterCode() As String
        Get
            Return _RegisterCode
        End Get
        Set(ByVal value As String)
            _RegisterCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegDescription() As String
        Get
            Return _RegisterDescription
        End Get
        Set(ByVal value As String)
            _RegisterDescription = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegAddress() As String
        Get
            Return _RegisterAddress
        End Get
        Set(ByVal value As String)
            _RegisterAddress = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegCity() As String
        Get
            Return _RegisterCity
        End Get
        Set(ByVal value As String)
            _RegisterCity = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegState() As String
        Get
            Return _RegisterState
        End Get
        Set(ByVal value As String)
            _RegisterState = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Property RegPostalCode() As String
        Get
            Return _RegisterPostalCode
        End Get
        Set(ByVal value As String)
            _RegisterPostalCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegEmailID() As String
        Get
            Return _RegisterEmailID
        End Get
        Set(ByVal value As String)
            _RegisterEmailID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegFax() As String
        Get
            Return _RegisterFax
        End Get
        Set(ByVal value As String)
            _RegisterFax = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegPhone() As String
        Get
            Return _RegisterPhone
        End Get
        Set(ByVal value As String)
            _RegisterPhone = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegActive() As String
        Get
            Return _RegisterActive
        End Get
        Set(ByVal value As String)
            _RegisterActive = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegTaxCode() As String
        Get
            Return _RegisterRegTaxCode
        End Get
        Set(ByVal value As String)
            _RegisterRegTaxCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegWhsCode() As String
        Get
            Return _RegWhsCode
        End Get
        Set(ByVal value As String)
            _RegWhsCode = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegMerchantId() As String
        Get
            Return _RegMerchantId
        End Get
        Set(ByVal value As String)
            _RegMerchantId = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Property RegHost() As String
        Get
            Return _RegHost
        End Get
        Set(ByVal value As String)
            _RegHost = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegHostPort() As String
        Get
            Return _RegHostPort
        End Get
        Set(ByVal value As String)
            _RegHostPort = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegTerminalID() As String
        Get
            Return _regTerminalID
        End Get
        Set(ByVal value As String)
            _regTerminalID = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegLogFilePath() As String
        Get
            Return _RegLogFilePath
        End Get
        Set(ByVal value As String)
            _RegLogFilePath = clsCommon.funRemove(value, True)
        End Set
    End Property
    Public Property RegMessage() As String
        Get
            Return _sysRegMessage
        End Get
        Set(ByVal value As String)
            _sysRegMessage = clsCommon.funRemove(value, True)
        End Set
    End Property

    Public Property RegPrintMerchantCopy() As String
        Get
            Return _regPrintMerchantCopy
        End Get
        Set(ByVal value As String)
            _regPrintMerchantCopy = clsCommon.funRemove(value, True)
        End Set
    End Property


#End Region
    Public Sub New()
        MyBase.New()
    End Sub
#Region "Function"
    ''' <summary>
    '''  Insert Register
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FunInsertRegister() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO sysRegister(sysRegCode, sysRegDesc, sysRegAddress, sysRegCity, sysRegState, sysRegPostalCode, sysRegEmailID, sysRegPhone, sysRegFax, sysRegActive, sysRegTaxCode, sysRegWhsCode,sysRegMerchantId,sysRegHost,sysRegHostPort,sysRegTerminalID,sysRegLogFilePath,sysRegMessage,sysRegPrintMerchantCopy) VALUES('"
        strSQL += _RegisterCode + "','"
        strSQL += _RegisterDescription + "','"
        strSQL += _RegisterAddress + "','"
        strSQL += _RegisterCity + "','"
        strSQL += _RegisterState + "','"
        strSQL += _RegisterPostalCode + "','"
        strSQL += _RegisterEmailID + "','"
        strSQL += _RegisterFax + "','"
        strSQL += _RegisterPhone + "','"
        strSQL += _RegisterActive + "','"
        strSQL += _RegisterRegTaxCode + "','"
        strSQL += _RegWhsCode + "','"
        strSQL += _RegMerchantId + "','"
        strSQL += _RegHost + "','"
        strSQL += _RegHostPort + "','"
        strSQL += _regTerminalID + "','"
        strSQL += _RegLogFilePath + "','"
        strSQL += _sysRegMessage + "',"
        strSQL += _regPrintMerchantCopy + ")"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    '''  Update Register
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdateRegister() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE sysRegister set "
        strSQL += "sysRegDesc='" + _RegisterDescription + "', "
        strSQL += "sysRegAddress='" + _RegisterAddress + "', "
        strSQL += "sysRegCity='" + _RegisterCity + "', "
        strSQL += "sysRegState='" + _RegisterState + "', "
        strSQL += "sysRegPostalCode='" + _RegisterPostalCode + "', "
        strSQL += "sysRegEmailID='" + _RegisterEmailID + "', "
        strSQL += "sysRegPhone='" + _RegisterPhone + "', "
        strSQL += "sysRegFax='" + _RegisterFax + "', "
        strSQL += "sysRegActive='" + _RegisterActive + "', "
        strSQL += "sysRegTaxCode='" + _RegisterRegTaxCode + "', "
        strSQL += "sysRegWhsCode='" + _RegWhsCode + "',"
        strSQL += "sysRegMerchantId='" + _RegMerchantId + "',"
        strSQL += "sysRegHost='" + _RegHost + "', "
        strSQL += "sysRegHostPort='" + _RegHostPort + "',"
        strSQL += "sysRegTerminalID='" + _regTerminalID + "',"
        strSQL += "sysRegLogFilePath='" + _RegLogFilePath + "',"
        strSQL += "sysRegMessage='" + _sysRegMessage + "',"
        strSQL += "sysRegPrintMerchantCopy=" + _regPrintMerchantCopy
        strSQL += " where sysRegCode='" + _RegisterCode + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim obj As New clsDataClass
        Dim intData As Int16 = 0
        intData = obj.PopulateData(strSQL)
        obj.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of Register
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub subGetRegisterInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT * FROM sysRegister where sysRegCode='" + _RegisterCode + "'"
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _RegisterDescription = drObj.Item("sysRegDesc").ToString
            _RegisterAddress = drObj.Item("sysRegAddress").ToString
            _RegisterCity = drObj.Item("sysRegCity").ToString
            _RegisterState = drObj.Item("sysRegState").ToString
            _RegisterPostalCode = drObj.Item("sysRegPostalCode").ToString
            _RegisterEmailID = drObj.Item("sysRegEmailID").ToString
            _RegisterFax = drObj.Item("sysRegFax").ToString
            _RegisterPhone = drObj.Item("sysRegPhone").ToString
            _RegisterActive = drObj.Item("sysRegActive").ToString
            _RegisterRegTaxCode = drObj.Item("sysRegTaxCode").ToString
            _RegWhsCode = drObj.Item("sysRegWhsCode").ToString
            _RegMerchantId = drObj.Item("sysRegMerchantId").ToString
            _RegHost = drObj.Item("sysRegHost").ToString
            _RegHostPort = drObj.Item("sysRegHostPort").ToString
            _regTerminalID = drObj.Item("sysRegTerminalID").ToString
            _RegLogFilePath = drObj.Item("sysRegLogFilePath").ToString
            _sysRegMessage = drObj.Item("sysRegMessage").ToString
            _regPrintMerchantCopy = drObj.Item("sysRegPrintMerchantCopy").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Check Duplicate Register Code
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funCheckDuplicateRegisterCode() As Boolean
        Dim strSQL As String
        strSQL = "SELECT count(*) FROM sysregister where sysRegCode='" & _RegisterCode & "' "
        If GetScalarData(strSQL) <> 0 Then
            Return True
        End If
        Return False
    End Function
    ''' <summary>
    ''' Delete Register
    ''' </summary>
    ''' <param name="strRegCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDeleteRegister(ByVal strRegCode As String) As String
        Dim strSQL As String
        strSQL = "DELETE FROM sysregister WHERE sysRegCode='" & strRegCode & "' "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Grid of Register
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funFillGrid() As String
        Dim strSQL As String
        strSQL = "SELECT sysRegCode, sysRegDesc FROM sysregister "
        strSQL += "order by sysRegCode "
        Return strSQL
    End Function
    ''' <summary>
    ''' Fill Register code in Dropdown lists
    ''' </summary>
    ''' <param name="dlRegister"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subGetCode(ByVal dlRegister As DropDownList) As String
        Dim objDataclass As New clsDataClass
        Dim strSQL, strCount As String
        strSQL = "SELECT count(*) from sysregister where sysRegWhsCode='" & HttpContext.Current.Session("UserWarehouse") & "' and sysRegActive='1'"
        strCount = GetScalarData(strSQL)
        If strCount = "1" Then
            strSQL = "SELECT sysRegCode from sysregister where sysRegWhsCode='" & HttpContext.Current.Session("UserWarehouse") & "' and sysRegActive='1'"
            HttpContext.Current.Session("RegCode") = GetScalarData(strSQL)
            Return 1
        Else
            strSQL = "SELECT sysRegCode, sysRegDesc from sysregister where sysRegWhsCode='" & HttpContext.Current.Session("UserWarehouse") & "' and sysRegActive='1'"
            dlRegister.DataSource = objDataclass.GetDataReader(strSQL)
            dlRegister.DataTextField = "sysRegDesc"
            dlRegister.DataValueField = "sysRegCode"
            dlRegister.DataBind()
            Dim itm As New ListItem
            itm.Value = 0
            itm.Text = Resources.Resource.msgSelectRegister
            dlRegister.Items.Insert(0, itm)
            objDataclass.CloseDatabaseConnection()
            objDataclass = Nothing
            Return 0
        End If
    End Function
    ''' <summary>
    ''' Is Print Merchant Copry
    ''' </summary>
    ''' <param name="regCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function IsPrintMerchantCopry(ByVal regCode As String) As String
        Dim SQL As String = "SELECT sr.sysRegPrintMerchantCopy From sysregister sr WHERE sr.sysRegCode = '{0}'"
        SQL = String.Format(SQL, clsCommon.funRemove(regCode))
        Dim objDataAccess As New clsDataClass()

        Dim objData As Object = objDataAccess.GetScalarData(SQL)
        objDataAccess.CloseDatabaseConnection()
        If Not objData Is Nothing Then
            If Convert.ToInt32(objData) = 0 Then
                Return "false"
            ElseIf Convert.ToInt32(objData) = 1 Then
                Return "true"
            End If
        End If
        Return "false"
    End Function
#End Region
End Class

Imports Microsoft.VisualBasic
Imports System.Web.HttpContext
Imports System.Net.Mail
Imports System.Threading
Imports System.Net.Mail.Attachment
Public Class clsCommon
    Private Mail As MailMessage
   
    Public Function SendErrorMail(ByVal Message As String) As Boolean
        'Sends the error mail
        Try
            Dim serverName As String = ConfigurationManager.AppSettings("SMTPServer")
            Dim EmailFrom As String = ConfigurationManager.AppSettings("ErrorEmailFrom")
            Dim EmailTo As String = ConfigurationManager.AppSettings("ErrorEmailTo")
            Dim Subject As String = ConfigurationManager.AppSettings("ErrorSubject")

            Mail = New MailMessage(EmailFrom, EmailTo, Subject, Message)
            Mail.IsBodyHtml = True
            If (ConfigurationManager.AppSettings("IsAsynchronousMail").ToUpper = "YES") Then
                Dim thread As Thread = New Thread(New ThreadStart(AddressOf AsyncMail))
                thread.Start()
            Else
                AsyncMail()
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    'Send mail 
    Public Function SendMail(ByVal subject As String, ByVal message As String, ByVal EmailTo As String, ByVal EmailFrom As String) As Boolean
        Try
            Mail = New MailMessage(EmailFrom, EmailTo, subject, message)
            Mail.IsBodyHtml = True

            If (ConfigurationManager.AppSettings("IsAsynchronousMail").ToUpper = "YES") Then
                Dim thread As Thread = New Thread(New ThreadStart(AddressOf AsyncMail))
                thread.Start()
            Else
                AsyncMail()
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    'Send mail with attachment
    Public Function SendMail(ByVal subject As String, ByVal message As String, ByVal EmailTo As String, ByVal EmailFrom As String, ByVal FileAttachments As String) As Boolean
        Try
            Mail = New MailMessage(EmailFrom, EmailTo, subject, message)
            Mail.IsBodyHtml = True
            'Mail.Attachments
            Mail.Attachments.Add(New Attachment(FileAttachments))
            If (ConfigurationManager.AppSettings("IsAsynchronousMail").ToUpper = "YES") Then
                Dim thread As Thread = New Thread(New ThreadStart(AddressOf AsyncMail))
                thread.Start()
            Else
                AsyncMail()
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Sub AsyncMail()
        'Send mail 
        Try
            Dim serverName As String = ConfigurationManager.AppSettings("SMTPServer")
            Dim emailClient As New SmtpClient(serverName)

            emailClient.Host = serverName
            emailClient.UseDefaultCredentials = False
            emailClient.Port = ConfigurationManager.AppSettings("SMTPPort")
            emailClient.Credentials = New System.Net.NetworkCredential(ConfigurationManager.AppSettings("SMTPUserEmail"), ConfigurationManager.AppSettings("SMTPPassword"))
            emailClient.Send(Mail)
        Catch ex As Exception
        End Try
    End Sub
    'Send Fax with attachment
    Public Function SendFax(ByVal subject As String, ByVal message As String, ByVal EmailTo As String, ByVal EmailFrom As String, ByVal FileAttachments As String) As Boolean
        Try
            Mail = New MailMessage(EmailFrom, EmailTo, subject, message)
            Mail.IsBodyHtml = False
            'Mail.Attachments
            Mail.Attachments.Add(New Attachment(FileAttachments))
            If (ConfigurationManager.AppSettings("IsAsynchronousMail").ToUpper = "YES") Then
                Dim thread As Thread = New Thread(New ThreadStart(AddressOf AsyncMail))
                thread.Start()
            Else
                AsyncMail()
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function GetFormatDate(ByVal DtTime As DateTime, ByVal mode As Int16) As String
        'Gets the Date and Return to the form yyyy/mmm/dd and time to HH:mm:ss (24 hour format)
        Dim strDate As String = "NA"
        If mode = 1 Then
            strDate = Format(DtTime, "yyyy/MMM/dd")
        ElseIf mode = 2 Then
            strDate = Format(DtTime, "yyyy/MMM/dd HH:mm:ss")
        ElseIf mode = 3 Then
            strDate = Format(DtTime, "yyyy/MM")
        ElseIf mode = 4 Then
            strDate = Format(DtTime, "yyyy/MM/dd")
        ElseIf mode = 5 Then
            strDate = Format(DtTime, "MM/dd/yyyy HH:mm")
        ElseIf mode = 6 Then
            strDate = Format(DtTime, "MM-dd-yyyy HH:mm")
        ElseIf mode = 7 Then
            strDate = Format(DtTime, "dd-MMM")
        End If
        Return (strDate)
    End Function
    'Generates random string
    Public Shared Function funRandomString() As String
        Dim rand As Random = New Random
        Dim strResult As String = ""
        Dim i As Int32
        For i = 0 To 15
            strResult = strResult & Convert.ToChar((rand.Next(1, 26) + 64))
        Next i
        Return strResult
    End Function

    'To generate random number
    Public Shared Function GetRandomNumber() As String
        Dim rand As Random = New Random
        Return rand.Next(100000, 1000000).ToString()
    End Function

    ' Replace Special Characters
    Public Shared Function funRemove(ByVal strValue As String) As String
        If strValue <> "" Then
            strValue = Replace(strValue, "'", "''").Trim()
            strValue = Replace(strValue, "\", "\\")
        End If
        Return strValue
    End Function
    ' Replace Special Characters
    Public Shared Function funRemove(ByVal strValue As String, ByVal bRtn As Boolean) As String
        If strValue <> "" Then
            strValue = Replace(strValue, "'", "''").Trim()
            strValue = Replace(strValue, "\", "\\")
        ElseIf strValue = "" Then
            strValue = "sysNull"
        End If
        Return strValue
    End Function
    ' Replace URL
    Public Shared Function funModifyURL(ByVal strValue As String, ByVal bRtn As Boolean) As String
        If strValue <> "" Then
            If strValue.ToLower().Contains("http") = False Then
                strValue = "http://" & strValue.Trim()
                strValue = funRemove(strValue, True)
            Else
                strValue = funRemove(strValue, True)
            End If
        ElseIf strValue = "" Then
            strValue = "sysNull"
        End If
        Return strValue
    End Function
    'Get Product Language
    Public Shared Function funGetProductLang() As String
        Dim strPrdLang As String = ""
        If Current.Session("Language") = "en-CA" Then
            strPrdLang = "en"
        ElseIf Current.Session("Language") = "fr-CA" Then
            strPrdLang = "fr"
        ElseIf Current.Session("Language") = "es-MX" Then
            strPrdLang = "sp"
        End If
        Return strPrdLang
    End Function

    Public Function funImageSize(ByVal strPath As String, ByVal strArgWidth As String, ByVal strArgHeight As String, ByVal strType As String, Optional ByVal strReturnValue As String = "") As String
        Dim sytImagesize As String
        Dim objImage As System.Drawing.Image
        If strPath = "" Then
            If strReturnValue <> "" Then
                strReturnValue = "0/0"
                Return strReturnValue
            Else
                sytImagesize = "width='0' Height='0'"
                Return sytImagesize
            End If
        End If
        strPath = strPath.Remove(0, strPath.LastIndexOf("/") + 1)
        If strType = "P" Then
            strPath = "upload/Product/" & strPath
        ElseIf strType = "C" Then
            strPath = "upload/Category/" & strPath
        ElseIf strType = "SC" Then
            strPath = "upload/Subcategory/" & strPath
        ElseIf strType = "Comp" Then
            strPath = "upload/CompanyLogo/" & strPath
        End If

        Dim strLogoPath As String = Current.Server.MapPath("../" & strPath)
        If System.IO.File.Exists(strLogoPath) Then
            objImage = System.Drawing.Image.FromFile(strLogoPath)
            Dim Height As Integer = objImage.Height
            Dim Width As Integer = objImage.Width
            Dim Ly As Integer = strArgHeight ' max height for thumbnails
            Dim Lx As Integer = strArgWidth ' max width for thumbnails
            Dim newWidth, newHeight As Integer
            Dim l2 ' temp variable used when calculating new size
            If (Width / Lx) > (Width / Ly) Then
                l2 = Width
                newWidth = Lx
                newHeight = Height * (Lx / l2)
                If newHeight > Ly Then
                    newWidth = newWidth * (Ly / newHeight)
                    newHeight = Ly
                End If
            Else
                l2 = Height
                newHeight = Ly
                newWidth = Width * (Ly / l2)
                If newWidth > Lx Then
                    newHeight = newHeight * (Lx / newWidth)
                    newWidth = Lx
                End If
            End If
            If strReturnValue <> "" Then
                strReturnValue = newWidth & "/" & newHeight
                Return strReturnValue
            Else
                sytImagesize = "width='" & newWidth & "px' Height='" & newHeight & "px'"
                Return sytImagesize
            End If
        Else
            If strReturnValue <> "" Then
                strReturnValue = "0/0"
                Return strReturnValue
            Else
                sytImagesize = "width='0' Height='0'"
                Return sytImagesize
            End If
        End If
    End Function
    'User In Partner Module
    Public Sub funRemoveSession()
        Current.Session.Remove("constate")
        Current.Session.Remove("conpartnerName")
        Current.Session.Remove("ContactTitle")
        Current.Session.Remove("conCity")
        Current.Session.Remove("Country")
        Current.Session.Remove("ContStatus")
        Current.Session.Remove("ContSearch")
        Current.Session.Remove("ContSearchtxt")
        Current.Session.Remove("Searchtxt")
        Current.Session.Remove("Status")
        Current.Session.Remove("Classification")
        Current.Session.Remove("Specialization")
        Current.Session.Remove("State")
        Current.Session.Remove("Country")
    End Sub


    '* Generic function to encode/decode the String.
    '* @param sinput The String in operation.
    '* @param ocs   The Old character sequence.
    '* @param ncs   The New character sequence.
    '* @param en_de boolean true/false i.e. whether encoding/decoding is required.
    '* @return String encoded/decoded.
    Public Function replaceSpecialCh(ByVal val As String) As String
        If val Is Nothing Then
            val = ""
        End If
        val = replaceSpecialChar(val, ",", "__COMMA__", True)
        val = replaceSpecialChar(val, "$", "__DOLLAR__", True)
        val = replaceSpecialChar(val, "&", "__AND__", True)
        val = replaceSpecialChar(val, "%", "__PERCENT__", True)
        val = replaceSpecialChar(val, "'", "__QUOTE__", True)
        val = replaceSpecialChar(val, """", "__DQUOTE__", True)
        val = replaceSpecialChar(val, "\", "__BSLASH__", True)
        val = replaceSpecialChar(val, vbLf, "__NEWLINE__N__", True)
        val = replaceSpecialChar(val, vbCr, "__NEWLINE__R__", True)
        Return val
    End Function
    '* Generic function to encode/decode the String.
    '* @param sinput The String in operation.
    '* @param ocs   The Old character sequence.
    '* @param ncs   The New character sequence.
    '* @param en_de boolean true/false i.e. whether encoding/decoding is required.
    '* @return String encoded/decoded.
    Public Function replaceSpecialChar(ByVal sinput As String, ByVal ocs As String, ByVal ncs As String, ByVal en_de As Boolean) As String
        If sinput Is Nothing Then
            sinput = ""
        End If
        If en_de = True Then
            If ocs.Equals("$") Then
                ocs = "\" & ocs
            ElseIf ocs.Equals("\") Then
                ocs = "\" & ocs
            End If
        Else
            If ncs.Equals("$") Then
                ncs = "\" & ncs
            ElseIf ncs.Equals("\") Then
                ncs = "\" & ncs
            End If
        End If
        Return sinput.Replace(ocs, ncs)
    End Function
    Public Shared Function replaceSysNull(ByVal str As String) As String
        str = str.Replace("'sysNull'", "Null")
        str = str.Replace("sysNull", "Null")
        Return str
    End Function
    Public Function funPrintLang(ByVal strTID As String) As String
        Dim strsql, strlang As String
        Dim objDataClass As New clsDataClass
        strsql = " SELECT  posCardlang FROM posproductno "
        strsql += " Inner join postransaccthdr on postransaccthdr.posAcctTransID=posproductno.posPrdTransID "
        strsql += " Inner join postransacctdtl on postransacctdtl.posAcctHdrDtlId=postransaccthdr.posAcctHdrID "
        strsql += " Inner join posTransaction as pt on pt.posTransId=posproductno.posPrdTransID"
        strsql += " Inner join Products on Products.ProductID=posproductno.posProductID where posPrdTransID='" & strTID & "'"
        strlang = objDataClass.GetScalarData(strsql)
        If strlang = "fr" Then
            strlang = "fr-CA"
        ElseIf strlang = "sp" Then
            strlang = "es-MX"
        Else
            strlang = "en-CA"
        End If
        Return strlang
    End Function

    Public Shared Function getProductSalePrice(ByVal productID As String, ByVal qty As String) As String
        Dim price As String = "0"
        Dim objPrdSalePrice As New clsProductSalesPrice
        objPrdSalePrice.ProductID = productID

        price = objPrdSalePrice.getSalePrice(qty)
        objPrdSalePrice = Nothing

        If price = "0" Then
            Dim objProducts As New clsProducts
            objProducts.ProductID = productID
            objProducts.subGetProductsInfo()

            price = objProducts.PrdEndUserSalesPrice
        End If
        Return String.Format("{0:F}", CDbl(price))
    End Function

    Public Shared Function ArrayListToDelimString(ByVal arrayList As ArrayList, ByVal separator As String) As String

        Dim stringArrayList As New ArrayList

        For Each o As Object In arrayList
            stringArrayList.Add(o.ToString())
        Next

        Return String.Join(separator, stringArrayList.ToArray(GetType(String)))

    End Function

    'Generate 8 Char Random String
    Public Function RandomString() As String
        Dim rand As Random = New Random
        Dim strResult As String = ""
        Dim i As Int32
        For i = 0 To 7
            strResult = strResult & Convert.ToChar((rand.Next(1, 26) + 64))
        Next i
        Return strResult
    End Function
    Public Shared Function fundate(ByVal intHour As String, ByVal strType As String) As String
        Dim intValue As String
        If strType = "PM" Then
            If intHour <= 11 Then
                intValue = intHour + 12
            Else
                intValue = intHour
            End If
        ElseIf strType = "AM" Then
            If intHour = "12" Then
                intValue = "00"
            ElseIf intHour = "11" Then
                intValue = intHour
            End If
            If intHour <= 9 Then
                intValue = "0" & intHour
            End If
        End If
        Return intValue
    End Function
    Public Shared Function funMin(ByVal intMin As String) As String
        Dim intValue As String
        If intMin <= 9 Then
            If intMin = "00" Then
                intValue = intMin
            Else
                intValue = "0" & intMin
            End If
        Else
            intValue = intMin
        End If
        Return intValue
    End Function

    Public Shared Function funDateformat(ByVal strValue As String, ByVal strTime As String, Optional ByVal strDateFormat As String = "") As String
        Dim dtDate As DateTime
        If strDateFormat = "" Then
            dtDate = DateTime.ParseExact(strValue, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
        Else
            dtDate = DateTime.ParseExact(strValue, strDateFormat, Nothing).ToString("yyyy-MM-dd hh:mm:ss")
        End If
        Return dtDate
    End Function

    Public Shared Function funGenerateAlert(ByVal strAlertPartnerId As String, ByVal strAlertType As String, ByVal strCreateUserId As String, ByVal strAlerUserId As String, ByVal strAlerNote As String, ByVal strAlertDateTime As String) As Boolean
        Dim objAlert As New clsAlerts
        objAlert.AlertPartnerContactID = strAlertPartnerId ' Invoice ID
        objAlert.AlertComID = strCreateUserId
        objAlert.AlertDateTime = strAlertDateTime 'DateTime.ParseExact(txtFollowupDateTime.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
        objAlert.AlertUserID = strCreateUserId 'Followup userID
        objAlert.AlertNote = strAlerNote
        objAlert.AlertRefType = strAlertType
        Return objAlert.insertAlerts()
    End Function

    Public Shared Function funBuildAlertNote(ByVal strUserId As String, ByVal strLine1 As String, ByVal strLine2 As String, ByVal strLine3 As String) As String
        Dim strNote As String = ""
        strNote += strLine1 & "<br><br>" 'You have a Collection Followup Activity.

        If Not String.IsNullOrEmpty(strLine2) Then
            strNote += strLine2 & "<br>" 'Invoice No. 
        End If
        If Not String.IsNullOrEmpty(strLine3) Then
            strNote += strLine3 & "<br>" 'Customer:
        End If

        Dim objUsr As New clsUser
        objUsr.UserID = strUserId
        objUsr.getUserInfo()
        strNote += Resources.Resource.alrtlblCreatedBy & " " & objUsr.UserSalutation & " " & objUsr.UserFirstName & " " & objUsr.UserLastName 'Created by:
        objUsr = Nothing
        Return strNote
    End Function

    Public Shared Sub RegisterThickBoxScript(ByVal pg As Page)
        'Dim script As String = String.Format("<script type=""text/javascript"" src=""{0}""></script>", pg.ResolveUrl("~/scripts/ thickbox.js"))
        Dim lnkTag As New HtmlLink
        lnkTag.Attributes.Add("rel", "stylesheet")
        lnkTag.Attributes.Add("type", "text/css")
        lnkTag.Attributes.Add("media", "screen")
        lnkTag.Href = pg.ResolveUrl("~/Css/thickbox.css")
        lnkTag.ID = "cssLnkThickBox"
        Dim bHasLnk As Boolean = False
        For Each ct As Control In pg.Header.Controls
            If ct.GetType().Equals(GetType(HtmlLink)) AndAlso ct.ID = "cssLnkThickBox" Then
                bHasLnk = True
                Exit For
            End If
        Next

        'Add Link css to page header
        If Not bHasLnk Then
            pg.Header.Controls.Add(lnkTag)
        End If

        'Add Init Script on page.
        Dim sb As New StringBuilder()
        sb.Append("<script language=""javascript"" type=""text/javascript"">")
        sb.Append(String.Format("var tb_pathToImage = ""{0}"";", pg.ResolveUrl("~/images/loadingAnimation.gif")))
        sb.Append("</script>")

        Dim cs As ClientScriptManager = pg.ClientScript
        If Not cs.IsClientScriptIncludeRegistered(pg.GetType(), "thickBoxScript") Then
            cs.RegisterClientScriptInclude(pg.GetType(), "thickBoxScript", pg.ResolveUrl("~/scripts/thickbox.js"))
        End If
        If Not cs.IsClientScriptBlockRegistered(pg.GetType(), "thickBoxInitScript") Then
            cs.RegisterClientScriptBlock(pg.GetType(), "thickBoxInitScript", sb.ToString())
        End If
    End Sub

End Class

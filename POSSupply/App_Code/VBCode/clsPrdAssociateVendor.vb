Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Data
Imports System.Threading
Imports System.Globalization
'Imports System.Web.HttpContext
'Imports clsCommon
'Imports Resources.Resource
Public Class clsPrdAssociateVendor
	Inherits clsDataClass
    Private _prdID, _prdVendorID, _prdCostPrice As String
	Public Property PrdID() As String
		Get
			Return _prdID
		End Get
		Set(ByVal value As String)
            _prdID = clsCommon.funRemove(value, True)
		End Set
	End Property
	Public Property PrdVendorID() As String
		Get
			Return _prdVendorID
		End Get
		Set(ByVal value As String)
            _prdVendorID = clsCommon.funRemove(value, True)
		End Set
    End Property
    Public Property PrdCostPrice() As String
        Get
            Return _prdCostPrice
        End Get
        Set(ByVal value As String)
            _prdCostPrice = clsCommon.funRemove(value, True)
        End Set
    End Property
	Public Sub New()
		MyBase.New()
	End Sub
    ''' <summary>
    ''' Insert PrdAssociateVendor
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funInsertPrdAssociateVendor() As Boolean
        Dim strSQL As String
        strSQL = "INSERT INTO prdassociatevendor(prdID, prdVendorID, prdCostPrice) VALUES("
        strSQL += "'" + _prdID + "','"
        strSQL += _prdVendorID + "','"
        strSQL += _prdCostPrice + "')"
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim objDataClass As New clsDataClass
        Dim intData As Int16 = 0
        intData = objDataClass.PopulateData(strSQL)
        objDataClass.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Update PrdAssociateVendor
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funUpdatePrdAssociateVendor() As Boolean
        Dim strSQL As String
        strSQL = "UPDATE prdassociatevendor set "
        strSQL += "prdVendorID='" + _prdVendorID + "',"
        strSQL += "prdCostPrice='" + _prdCostPrice + "'"
        strSQL += " where prdID='" + _prdID + "' "
        strSQL = strSQL.Replace("'sysNull'", "Null")
        strSQL = strSQL.Replace("sysNull", "Null")
        Dim objDataClass As New clsDataClass
        Dim intData As Int16 = 0
        intData = objDataClass.PopulateData(strSQL)
        objDataClass.CloseDatabaseConnection()
        If intData = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    ''' <summary>
    ''' Populate objects of PrdAssociateVendor
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub funGetPrdAssociateVendorInfo()
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  prdVendorID,prdCostPrice FROM prdassociatevendor where prdID='" + _prdID + "' "
        drObj = GetDataReader(strSQL)
        While drObj.Read
            _prdVendorID = drObj.Item("prdVendorID").ToString
            _prdCostPrice = drObj.Item("prdCostPrice").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
    End Sub
    ''' <summary>
    ''' Find Association Vendor Name
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function funDuplicateAssVendor() As Boolean
        Dim strSQL As String
        Dim objDataClass As New clsDataClass
        strSQL = "SELECT count(*) FROM prdassociatevendor where prdID='" + _prdID + "' And prdVendorID ='" & _prdVendorID & "'"
        If objDataClass.GetScalarData(strSQL) <> 0 Then
            objDataClass = Nothing
            Return True
        Else
            objDataClass = Nothing
            Return False
        End If
    End Function
    ''' <summary>
    ''' Fill Ass Vendor
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subFillAssVendor() As String
        Dim strSql As String = "select prdVendorID,prdID,vendorName,prdCostPrice from prdassociatevendor as pv "
        strSql += " Inner Join vendor as v on v.vendorID=pv.prdVendorID where pv.prdID ='" & HttpContext.Current.Request.QueryString("PrdID") & "'"
        'where descLang='" + Current.Session("Lang") + "' "
        Return strSql
    End Function
    ''' <summary>
    ''' sub Ass vendor Delete
    ''' </summary>
    ''' <param name="strVendorID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function subAssvendorDelete(ByVal strVendorID As String) As String
        Dim strSql As String = "delete from prdassociatevendor where prdVendorID='" & strVendorID & "' And prdID ='" & HttpContext.Current.Request.QueryString("PrdID") & "'"
        Return strSql
    End Function
    ''' <summary>
    ''' Fill Vendore Name in Dropdown list
    ''' </summary>
    ''' <param name="dlAssVendor"></param>
    ''' <remarks></remarks>
    Public Sub subGetVendor(ByVal dlAssVendor As DropDownList)
        Dim objDataclass As New clsDataClass
        Dim strSQL As String
        strSQL = "SELECT vendorName,vendorID from vendor where vendorActive='1'"
        dlAssVendor.DataSource = objDataclass.GetDataReader(strSQL)
        dlAssVendor.DataTextField = "vendorName"
        dlAssVendor.DataValueField = "vendorID"
        dlAssVendor.DataBind()
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.msgSelectVendor
        dlAssVendor.Items.Insert(0, itm)
        objDataclass.CloseDatabaseConnection()
        objDataclass = Nothing
    End Sub
    ''' <summary>
    ''' Get Vendor Names for Given Product
    ''' </summary>
    ''' <param name="prdId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetVendorNames(ByVal prdId As String) As String
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        strSQL = "select prdVendorID,prdID,vendorName,prdCostPrice from prdassociatevendor as pv "
        strSQL += " Inner Join vendor as v on v.vendorID=pv.prdVendorID where pv.prdID ='" & prdId & "'"
        drObj = GetDataReader(strSQL)
        Dim strVendors As String = ""
        While drObj.Read
            If strVendors <> "" Then
                strVendors += ", "
            End If
            strVendors += drObj("vendorName").ToString
        End While
        drObj.Close()
        CloseDatabaseConnection()
        If strVendors = "" Then
            strVendors = "--"
        End If
        Return strVendors
    End Function
End Class

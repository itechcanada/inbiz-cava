﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.IO;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using System.Resources;
using System.Text;
public class CommonPrint
{
    //Order Common Print SO/QO
    public static string GetOrderPrintHtmlSnipt(Page pg, string docType, int soID)
    {
        //Check application specific layout
        string appName = System.Configuration.ConfigurationManager.AppSettings["ApplicationName"];
        if (appName == "possupply")
        {
            //Continuew with default layout
        }
        else if (appName == "sivananda")
        {
            //To Do to make separate layout from default layout & return html
        }

        //Continue with Default layout
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            //Set templates html to litrals   
            string printTemplet = File.ReadAllText(HttpContext.Current.Server.MapPath("~/PrintTemplates/Order.htm"));
            string itemRowTemplet = string.Empty;
            string processItemRowTemplet = string.Empty;
            int sIdx, eIdx;
            sIdx = printTemplet.IndexOf("<!--ITEM_ROW_TEMPLET_START--><!--");
            eIdx = printTemplet.IndexOf("--><!--ITEM_ROW_TEMPLET_END-->");
            itemRowTemplet = printTemplet.Substring(sIdx, eIdx - sIdx).Replace("<!--ITEM_ROW_TEMPLET_START--><!--", "");

            sIdx = printTemplet.IndexOf("<!--PROCESS_ITEM_ROW_TEMPLET_START--><!--");
            eIdx = printTemplet.IndexOf("--><!--PROCESS_ITEM_ROW_TEMPLET_END-->");

            processItemRowTemplet = printTemplet.Substring(sIdx, eIdx - sIdx).Replace("<!--PROCESS_ITEM_ROW_TEMPLET_START--><!--", "");

            //Init Requested Order
            Orders ord = new Orders();
            ord.PopulateObject(dbHelp, soID);

            //Init Customer 
            Partners part = new Partners();
            part.PopulateObject(dbHelp, ord.OrdCustID);

            //Init CompanyInfo
            SysCompanyInfo cinfo = new SysCompanyInfo();
            cinfo.PopulateObject(ord.OrdCompanyID, dbHelp);

            //TO DO TO SET Company Info & Logo on Header also need to set Html in Header
            if (!string.IsNullOrEmpty(cinfo.CompanyLogoPath))
            {
                //printTemplet = printTemplet.Replace("#COM_LOGO#", pg.ResolveUrl("~/Upload/companylogo/") + cinfo.CompanyLogoPath);
                string logoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/Upload/companylogo/") + cinfo.CompanyLogoPath;
                printTemplet = printTemplet.Replace("#COM_LOGO#", logoUrl);
            }
            else
            {
                printTemplet = printTemplet.Replace("#COM_LOGO#", "");
            }
            if (ConfigurationManager.AppSettings["DocumentHeaderInfo"] == "WHS")
            {
                printTemplet = printTemplet.Replace("#COM_INFO#", CommonPrint.WarehouseInfoHtml(dbHelp, ord.OrdShpWhsCode));
            }
            else
            {
                printTemplet = printTemplet.Replace("#COM_INFO#", CommonPrint.GetCompanyInfoHtml(cinfo));
            }

            if (docType.ToUpper().Trim() == "QO")
            {
                printTemplet = printTemplet.Replace("#DOCUMENT_TITLE#", CommonResource.ResourceValue("prnQuotation"));
            }
            else if (docType.ToUpper().Trim() == "SO")
            {
                printTemplet = printTemplet.Replace("#DOCUMENT_TITLE#", CommonResource.ResourceValue("prnSalesOrder"));
            }

            string docInfo = "";
            docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("prnDate"), ord.OrdCreatedOn.ToString("MMM-dd yyyy"));

            if (docType.ToUpper().Trim() == "QO")
            {
                docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("prnQuotationNo"), ord.OrdID);
            }
            else if (docType.ToUpper().Trim() == "SO")
            {
                docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("prnSONo"), ord.OrdID);
            }

            docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("lblCustomerName"), part.PartnerLongName);
            printTemplet = printTemplet.Replace("#DOC_INFO#", docInfo);
            printTemplet = printTemplet.Replace("#TRACKINGURL#", "");


            //Set for shivananda
            printTemplet = CommonPrint.GetDocumentHeaderSiavanadaSOHtml(ord, part, docType, printTemplet);

            //Set Bill To Address
            //Addresses billToAddress = new Addresses();
            //billToAddress.PopulateObject(dbHelp, ord.OrdCustID, ord.OrdCustType, AddressType.BILL_TO_ADDRESS);

            Addresses billToAddress = new Addresses();
            billToAddress.GetOrderAddress(ord.OrdID, AddressType.BILL_TO_ADDRESS);
            printTemplet = printTemplet.Replace("#BILL_TO_ADDRESS_TITLE#", CommonResource.ResourceValue("BillTo"));
            printTemplet = printTemplet.Replace("#BILL_TO_ADDRESS#", GetAddress(billToAddress));


            //Set Ship To Address
            //Addresses shipToAddress = new Addresses();
            //shipToAddress.PopulateObject(dbHelp, ord.OrdCustID, ord.OrdCustType, AddressType.SHIP_TO_ADDRESS);
            Addresses shipToAddress = new Addresses();
            shipToAddress.GetOrderAddress(ord.OrdID, AddressType.SHIP_TO_ADDRESS);
            printTemplet = printTemplet.Replace("#SHIP_TO_ADDRESS_TITLE#", CommonResource.ResourceValue("ShipTo"));
            printTemplet = printTemplet.Replace("#SHIP_TO_ADDRESS#", GetAddress(shipToAddress));

            printTemplet = printTemplet.Replace("#ITEM_LIST_TITLE#", CommonResource.ResourceValue("lblItems"));

            //Build Item Header      
            printTemplet = printTemplet.Replace("#ITM_HDR_DESCRIPTION#", CommonResource.ResourceValue("lblDescription"));
            printTemplet = printTemplet.Replace("#ITM_HDR_UNITS#", CommonResource.ResourceValue("lblUnits"));
            printTemplet = printTemplet.Replace("#ITM_HDR_COST_PER_UNIT#", CommonResource.ResourceValue("lblCostPerUnit"));
            printTemplet = printTemplet.Replace("#ITM_HDR_DISCOUNT#", CommonResource.ResourceValue("grdSaleDiscount"));
            printTemplet = printTemplet.Replace("#ITM_HDR_AMOUNT#", CommonResource.ResourceValue("lblAmount"));

            OrderItems oi = new OrderItems();
            //IDataReader itemReader = null;
            DataTable itemReader = null;
            string itemsToPrint = string.Empty;
            try
            {
                itemReader = oi.GetOrderItemsTable(dbHelp, ord.OrdID);
                //itemReader = oi.GetOrderItemsReader(dbHelp, ord.OrdID);
                //string itemRow = string.Empty;
                //while (itemReader.Read())
                //{
                //    itemRow = itemRowTemplet;
                //    itemRow = itemRow.Replace("#ITM_DESCRIPTION#", BusinessUtility.GetString(itemReader["prdName"]).Replace(Environment.NewLine, "<br>"));
                //    itemRow = itemRow.Replace("#ITM_UNITS#", BusinessUtility.GetString(itemReader["ordProductQty"]));
                //    itemRow = itemRow.Replace("#ITM_COST_PER_UNIT#", string.Format("{0} {1:F}", ord.OrdCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(itemReader["ordProductUnitPrice"]), Globals.CurrentCultureName)));
                //    itemRow = itemRow.Replace("#ITM_DISCOUNT#", string.Format("{0} {1:F}", ord.OrdCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(itemReader["ordProductDiscount"]), Globals.CurrentCultureName)));
                //    itemRow = itemRow.Replace("#ITM_AMOUNT#", string.Format("{0} {1:F}", ord.OrdCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(itemReader["amount"]), Globals.CurrentCultureName)));

                //    itemsToPrint += itemRow;
                //}

                #region PrintLayout

                sIdx = printTemplet.IndexOf("<!--ITEM_PROD_HDG_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_PROD_HDG_END-->");
                string itemProdHdg = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdHdg = itemProdHdg.Replace("<!--ITEM_PROD_HDG_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_HDG_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_HDG_END-->");
                string itemProdDescHdg = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdDescHdg = itemProdDescHdg.Replace("<!--ITEM_DESC_HDG_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_HDG_VAL_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_HDG_VAL_END-->");
                string itemProdDescVal = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdDescVal = itemProdDescVal.Replace("<!--ITEM_DESC_HDG_VAL_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_SIZE_HDG_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_SIZE_HDG_END-->");
                string itemProdSizeHdg = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdSizeHdg = itemProdSizeHdg.Replace("<!--ITEM_DESC_SIZE_HDG_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_CNT_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_CNT_END-->");
                string itemProdDesc = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdDesc = itemProdDesc.Replace("<!--ITEM_DESC_CNT_START--><!--", "");

                var vProdGroup = from row in itemReader.AsEnumerable()
                                 group row by row.Field<string>("prdName") into grp
                                 select new
                                 {
                                     prdName = grp.Key,
                                     MemberCount = grp.Count()
                                 };


                var vProdSizeGroup = from row in itemReader.AsEnumerable()
                                     group row by row.Field<string>("Size") into grp
                                     select new
                                     {
                                         Size = grp.Key,
                                         MemberCount = grp.Count()
                                     };


                foreach (var item in vProdGroup)
                {


                    itemsToPrint += itemProdHdg.Replace("#PNAME#", item.prdName);
                    string sItemDescHdgRow = "";
                    string sSizeHdg = "";
                    int iTotalSizeCount = 0;
                    foreach (var size in vProdSizeGroup)
                    {
                        iTotalSizeCount += 1;
                        sSizeHdg += itemProdSizeHdg.Replace("#SIZE#", size.Size);
                    }
                    sItemDescHdgRow += itemProdDescHdg.Replace("#HDG_COLOR#", CommonResource.ResourceValue("lblColor")).Replace("#HDG_SIZE#", sSizeHdg).Replace("#HDG_COST_PER_UNIT#", CommonResource.ResourceValue("grdvProcessCostPerUnit")).Replace("#HDG_AMOUNT#", CommonResource.ResourceValue("lblAmmount")).Replace("#HDG_DISCOUNT#", CommonResource.ResourceValue("lblDiscount2"));

                    DataView datavw = new DataView();
                    datavw = itemReader.DefaultView;
                    datavw.RowFilter = "prdName='" + BusinessUtility.GetString(item.prdName) + "'";
                    DataTable dtProduct = datavw.ToTable();

                    var vProdColorGroup = from row in dtProduct.AsEnumerable()
                                          group row by row.Field<string>("Color") into grp
                                          select new
                                          {
                                              Color = grp.Key,
                                              MemberCount = grp.Count()
                                          };
                    foreach (var color in vProdColorGroup)
                    {
                        string sSizeQty = "";
                        int qty = 0;
                        double pPrice = 0.0;
                        double pDiscount = 0.0;
                        double pAmount = 0.0;
                        foreach (var size in vProdSizeGroup)
                        {
                            DataView dvNewPRow = new DataView();
                            dvNewPRow = itemReader.DefaultView;
                            dvNewPRow.RowFilter = "prdName='" + BusinessUtility.GetString(item.prdName) + "' AND color='" + color.Color + "' AND Size ='" + size.Size + "' ";
                            DataTable dtProductSize = datavw.ToTable();

                            if (dtProductSize.Rows.Count > 0)
                            {
                                sSizeQty += itemProdSizeHdg.Replace("#SIZE#", BusinessUtility.GetString(dtProductSize.Rows[0]["ordProductQty"]));
                                qty += BusinessUtility.GetInt(dtProductSize.Rows[0]["ordProductQty"]);
                                pPrice = BusinessUtility.GetDouble(dtProductSize.Rows[0]["ordProductUnitPrice"]);
                                pDiscount = BusinessUtility.GetDouble(dtProductSize.Rows[0]["ordProductDiscount"]);
                                pAmount += BusinessUtility.GetDouble(dtProductSize.Rows[0]["amount"]);
                            }
                            else
                            {
                                sSizeQty += itemProdSizeHdg.Replace("#SIZE#", BusinessUtility.GetString("0"));
                                qty += BusinessUtility.GetInt(0);
                            }
                        }
                        //sItemDescHdgRow += itemProdDescVal.Replace("#HDG_COLOR#", color.Color).Replace("#HDG_SIZE#", sSizeQty).Replace("#HDG_COST_PER_UNIT#", CurrencyFormat.GetCompanyCurrencyFormat(pPrice,ord.OrdCurrencyCode)).Replace("#HDG_AMOUNT#", BusinessUtility.GetCurrencyString(pAmount, Globals.CurrentCultureName)).Replace("#HDG_DISCOUNT#", BusinessUtility.GetString(pDiscount));
                        sItemDescHdgRow += itemProdDescVal.Replace("#HDG_COLOR#", color.Color).Replace("#HDG_SIZE#", sSizeQty).Replace("#HDG_COST_PER_UNIT#", CurrencyFormat.GetCompanyCurrencyFormat(pPrice, ord.OrdCurrencyCode)).Replace("#HDG_AMOUNT#", CurrencyFormat.GetCompanyCurrencyFormat(pAmount, ord.OrdCurrencyCode)).Replace("#HDG_DISCOUNT#", BusinessUtility.GetString(pDiscount));
                    }
                    itemsToPrint += itemProdDesc.Replace("#ITEM_GROUP_DESC#", sItemDescHdgRow);
                }
                #endregion
            }
            finally
            {
                //if (itemReader != null && !itemReader.IsClosed)
                //{
                //    itemReader.Close();
                //    itemReader.Dispose();
                //}
            }

            printTemplet = printTemplet.Replace("<!--ITEM_ROW_LIST_PLACEHOLDER-->", itemsToPrint);


            printTemplet = printTemplet.Replace("#PROCESS_ITEM_LIST_TITLE#", CommonResource.ResourceValue("lblServices"));

            //Build Process Header
            printTemplet = printTemplet.Replace("#PITEM_HDR_PROCESS_CODE#", CommonResource.ResourceValue("grdProcessCode"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_DESCRIPTION#", CommonResource.ResourceValue("lblDescription"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_FIXED_COST#", CommonResource.ResourceValue("lblProcessFixedCost"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_COST_PER_HOUR#", CommonResource.ResourceValue("lblCostPerHour"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_COST_PER_UNIT#", CommonResource.ResourceValue("lblCostPerUnit"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_TOTAL_HOURS#", CommonResource.ResourceValue("lblSOTotalHour"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_TOTAL_UNITS#", CommonResource.ResourceValue("lblSOTotalUnit"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_PROCESS_COST#", CommonResource.ResourceValue("prnProcessCost"));


            OrderItemProcess oip = new OrderItemProcess();
            IDataReader processReader = null;
            string processToPrint = string.Empty;
            try
            {
                processReader = oip.GetOrderProcessItemsReader(dbHelp, ord.OrdID);
                bool showProcess = false;
                string processItemRow = string.Empty;
                while (processReader.Read())
                {
                    processItemRow = processItemRowTemplet;
                    if (!showProcess)
                    {
                        showProcess = true;
                    }
                    processItemRow = processItemRow.Replace("#PITEM_PROCESS_CODE#", BusinessUtility.GetString(processReader["ordItemProcCode"]));
                    processItemRow = processItemRow.Replace("#PITEM_DESCRIPTION#", BusinessUtility.GetString(processReader["ProcessDescription"]).Replace(Environment.NewLine, "<br>"));
                    //processItemRow = processItemRow.Replace("#PITEM_FIXED_COST#", string.Format("{0} {1:F}", ord.OrdCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(processReader["ordItemProcFixedPrice"]), Globals.CurrentCultureName)));
                    //processItemRow = processItemRow.Replace("#PITEM_COST_PER_HOUR#", string.Format("{0} {1:F}", ord.OrdCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(processReader["ordItemProcPricePerHour"]), Globals.CurrentCultureName)));
                    //processItemRow = processItemRow.Replace("#PITEM_COST_PER_UNIT#", string.Format("{0} {1:F}", ord.OrdCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(processReader["ordItemProcPricePerUnit"]), Globals.CurrentCultureName)));
                    processItemRow = processItemRow.Replace("#PITEM_FIXED_COST#", CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(processReader["ordItemProcFixedPrice"]), ord.OrdCurrencyCode));
                    processItemRow = processItemRow.Replace("#PITEM_COST_PER_HOUR#", CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(processReader["ordItemProcPricePerHour"]), ord.OrdCurrencyCode));
                    processItemRow = processItemRow.Replace("#PITEM_COST_PER_UNIT#", CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(processReader["ordItemProcPricePerUnit"]), ord.OrdCurrencyCode));
                    processItemRow = processItemRow.Replace("#PITEM_TOTAL_HOURS#", string.Format("{0}", processReader["ordItemProcHours"]));
                    processItemRow = processItemRow.Replace("#PITEM_TOTAL_UNITS#", string.Format("{0}", processReader["ordItemProcUnits"]));
                    //processItemRow = processItemRow.Replace("#PITEM_PROCESS_COST#", string.Format("{0} {1:F}", ord.OrdCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(processReader["ProcessCost"]), Globals.CurrentCultureName)));
                    processItemRow = processItemRow.Replace("#PITEM_PROCESS_COST#", CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(processReader["ProcessCost"]), ord.OrdCurrencyCode));

                    processToPrint += processItemRow;
                }

                printTemplet = printTemplet.Replace("<!--PROCESS_ITEM_ROW_LIST_PLACEHOLDER-->", processToPrint);
                if (showProcess)
                {
                    printTemplet = printTemplet.Replace("#SERVICE_DISPLAY#", "");
                }
                else
                {
                    printTemplet = printTemplet.Replace("#SERVICE_DISPLAY#", "display:none;");
                }
            }
            finally
            {
                if (processReader != null && !processReader.IsClosed)
                {
                    processReader.Close();
                    processReader.Dispose();
                }
            }

            printTemplet = printTemplet.Replace(processItemRowTemplet, processToPrint);

            TotalSummary lTotal = CalculationHelper.GetOrderTotal(dbHelp, ord.OrdID);
            //Set Lables for html to be generated
            //lTotal.SetLabel(TotalSummary.LabelKey.BalanceAmount, Resources.Resource.lblBalanceAmount);
            //lTotal.SetLabel(TotalSummary.LabelKey.GrandSubTotal, Resources.Resource.lblSubTotalOrder);
            //lTotal.SetLabel(TotalSummary.LabelKey.GrandTotal, Resources.Resource.lblOrderTotal);
            //lTotal.SetLabel(TotalSummary.LabelKey.ReceivedAmount, Resources.Resource.lblReceivedAmount);
            //lTotal.SetLabel(TotalSummary.LabelKey.SubTotalItems, Resources.Resource.lblSubTotalItems);
            //lTotal.SetLabel(TotalSummary.LabelKey.SubTotalServices, Resources.Resource.lblSubTotalServices);
            //lTotal.SetLabel(TotalSummary.LabelKey.TotalDiscount, Resources.Resource.lblDiscount);
            //lTotal.SetLabel(TotalSummary.LabelKey.TotalTax, Resources.Resource.lblTotalTax);

            printTemplet = printTemplet.Replace("#ITEM_SUB_TOTAL_TABLE#", CommonHtml.GetItemTotalHtml(lTotal, ord.OrdCustID, ord.OrdID));
            printTemplet = printTemplet.Replace("#PROCRESS_ITEM_SUB_TOTAL_TABLE#", CommonHtml.GetProcessTotalHtml(lTotal));
            printTemplet = printTemplet.Replace("#GRAND_TOTAL#", CommonHtml.GetGrandTotalHtml(lTotal).Replace("#HISTORY_LINK#", ""));

            if (!string.IsNullOrEmpty(appName) && appName.ToLower() == "sivananda")
            {
                //Hide default document header
                printTemplet = printTemplet.Replace("<!--SHIVANANDA_HEADER_START--><!--", "");
                printTemplet = printTemplet.Replace("--><!--SHIVANANDA_HEADER_END-->", "");
                printTemplet = printTemplet.Replace("#DEFAULT_HEADER_DISPLAY#", "display:none;");
                printTemplet = printTemplet.Replace("#SHIVANANDA_DISPLAY#", "");
            }
            else
            {
                //show default header
                printTemplet = printTemplet.Replace("#DEFAULT_HEADER_DISPLAY#", "");
                printTemplet = printTemplet.Replace("#SHIVANANDA_DISPLAY#", "display:none");
            }

            //Update Print Counter
            if (docType == "SO")
            {
                ord.UpdateSalesOrderPrintCounter(dbHelp, ord.OrdID);
            }
            else
            {
                ord.UpdateQuotePrintCounter(dbHelp, ord.OrdID);
            }

            //Apply Barcode prefix & postfix
            printTemplet = printTemplet.Replace("#BARCODE_PREFIX#", AppConfiguration.AppSettings[AppSettingKey.BarcodePrefix]);
            printTemplet = printTemplet.Replace("#BARCODE_POSTFIX#", AppConfiguration.AppSettings[AppSettingKey.BarcodePrefix]);

            return printTemplet;
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    //Invoice Common Print
    public static string GetInvoicePrintHtmlSnipt(Page pg, int invoceID)
    {
        //Check application specific layout
        string appName = System.Configuration.ConfigurationManager.AppSettings["ApplicationName"];
        if (appName == "possupply")
        {
            return CommonPrint.GetInvoicePrintHtmlSniptPosSupply(pg, invoceID);
        }
        else if (appName == "sivananda")
        {
            //To Do to make separate layout from default layout & return html
        }

        //Continue with default setup for Inbiz
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            //Set templates html to litrals   
            string printTemplet = File.ReadAllText(HttpContext.Current.Server.MapPath("~/PrintTemplates/Order.htm"));
            string itemRowTemplet = string.Empty;
            string processItemRowTemplet = string.Empty;
            int sIdx, eIdx;
            sIdx = printTemplet.IndexOf("<!--ITEM_ROW_TEMPLET_START--><!--");
            eIdx = printTemplet.IndexOf("--><!--ITEM_ROW_TEMPLET_END-->");
            itemRowTemplet = printTemplet.Substring(sIdx, eIdx - sIdx).Replace("<!--ITEM_ROW_TEMPLET_START--><!--", "");

            sIdx = printTemplet.IndexOf("<!--PROCESS_ITEM_ROW_TEMPLET_START--><!--");
            eIdx = printTemplet.IndexOf("--><!--PROCESS_ITEM_ROW_TEMPLET_END-->");

            processItemRowTemplet = printTemplet.Substring(sIdx, eIdx - sIdx).Replace("<!--PROCESS_ITEM_ROW_TEMPLET_START--><!--", "");

            //Init Requested Order
            Invoice ord = new Invoice();
            ord.PopulateObject(dbHelp, invoceID);

            //Init Customer 
            Partners part = new Partners();
            part.PopulateObject(dbHelp, ord.InvCustID);

            //Init CompanyInfo
            SysCompanyInfo cinfo = new SysCompanyInfo();
            cinfo.PopulateObject(ord.InvCompanyID, dbHelp);

            //TO DO TO SET Company Info & Logo on Header also need to set Html in Header
            if (!string.IsNullOrEmpty(cinfo.CompanyLogoPath))
            {
                //printTemplet = printTemplet.Replace("#COM_LOGO#", pg.ResolveUrl("~/Upload/companylogo/") + cinfo.CompanyLogoPath);
                string logoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/Upload/companylogo/") + cinfo.CompanyLogoPath;
                printTemplet = printTemplet.Replace("#COM_LOGO#", logoUrl);
            }
            else
            {
                printTemplet = printTemplet.Replace("#COM_LOGO#", "");
            }
            if (ConfigurationManager.AppSettings["DocumentHeaderInfo"] == "WHS")
            {
                printTemplet = printTemplet.Replace("#COM_INFO#", CommonPrint.WarehouseInfoHtml(dbHelp, ord.InvShpWhsCode));
            }
            else
            {
                printTemplet = printTemplet.Replace("#COM_INFO#", CommonPrint.GetCompanyInfoHtml(cinfo));
            }
            printTemplet = printTemplet.Replace("#DOCUMENT_TITLE#", CommonResource.ResourceValue("prnInvoice"));

            Orders _ord = new Orders();
            _ord.PopulateObject(dbHelp, ord.InvForOrderNo);
            string[] sShipingCompany = _ord.OrdShpCode.Split('-');
            string sTrackingUrl = "";
            if (sShipingCompany.Length > 0)
            {
                SysShippingCompany objShippingCompny = new SysShippingCompany();
                var sSHippingCompany = objShippingCompny.GetShippingCompany(null, 0, BusinessUtility.GetString(sShipingCompany[0]));
                if (sSHippingCompany.Count > 0)
                {
                    sTrackingUrl = (sSHippingCompany[0].ShipCompanyUrl + BusinessUtility.GetString(_ord.OrdShpCode)).Replace(BusinessUtility.GetString(sSHippingCompany[0].ShipCompany) + "-", "").Trim();
                    if (_ord.OrdShpCode != "")
                    {
                        string sShippingDtl = "Tracking:" + _ord.OrdShpCode + "</br>" + sTrackingUrl;
                        printTemplet = printTemplet.Replace("#TRACKINGURL#", sShippingDtl);
                    }
                    else
                    {
                        printTemplet = printTemplet.Replace("#TRACKINGURL#", "");
                    }
                }
                else
                {
                    printTemplet = printTemplet.Replace("#TRACKINGURL#", "");
                }
            }
            else
            {
                printTemplet = printTemplet.Replace("#TRACKINGURL#", "");
            }




            string docInfo = "";
            docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("prnDate"), ord.InvCreatedOn.ToString("MMM-dd yyyy"));
            docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("prnInvoiceNo"), ord.InvRefNo);
            docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("prnSONo"), ord.InvForOrderNo);

            docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("lblCustomerName"), part.PartnerLongName);
            Orders o = new Orders();
            o.PopulateObject(dbHelp, ord.InvForOrderNo);
            //printTemplet = printTemplet.Replace("#Net Terms#", o.OrdNetTerms);

            docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("lblSONetTerms"), o.OrdNetTerms);
            printTemplet = printTemplet.Replace("#DOC_INFO#", docInfo);


            //Set for shivananda
            printTemplet = CommonPrint.GetDocumentHeaderSivanandaInvHtml(ord, part, printTemplet);

            //Set Bill To Address
            Addresses billToAddress = new Addresses();
            //billToAddress.PopulateObject(dbHelp, ord.InvCustID, ord.InvCustType, AddressType.BILL_TO_ADDRESS);
            billToAddress.GetOrderAddress(ord.InvForOrderNo, AddressType.BILL_TO_ADDRESS);
            printTemplet = printTemplet.Replace("#BILL_TO_ADDRESS_TITLE#", CommonResource.ResourceValue("BillTo"));
            printTemplet = printTemplet.Replace("#BILL_TO_ADDRESS#", GetAddress(billToAddress));


            //Set Ship To Address
            Addresses shipToAddress = new Addresses();
            //shipToAddress.PopulateObject(dbHelp, ord.InvCustID, ord.InvCustType, AddressType.SHIP_TO_ADDRESS);
            shipToAddress.GetOrderAddress(ord.InvForOrderNo, AddressType.SHIP_TO_ADDRESS);
            printTemplet = printTemplet.Replace("#SHIP_TO_ADDRESS_TITLE#", CommonResource.ResourceValue("ShipTo"));
            printTemplet = printTemplet.Replace("#SHIP_TO_ADDRESS#", GetAddress(shipToAddress));

            printTemplet = printTemplet.Replace("#ITEM_LIST_TITLE#", CommonResource.ResourceValue("lblItems"));

            //Build Item Header      
            printTemplet = printTemplet.Replace("#ITM_HDR_DESCRIPTION#", CommonResource.ResourceValue("lblDescription"));
            printTemplet = printTemplet.Replace("#ITM_HDR_UNITS#", CommonResource.ResourceValue("lblUnits"));
            printTemplet = printTemplet.Replace("#ITM_HDR_COST_PER_UNIT#", CommonResource.ResourceValue("lblCostPerUnit"));
            printTemplet = printTemplet.Replace("#ITM_HDR_DISCOUNT#", CommonResource.ResourceValue("grdSaleDiscount"));
            printTemplet = printTemplet.Replace("#ITM_HDR_AMOUNT#", CommonResource.ResourceValue("lblAmount"));

            InvoiceItems oi = new InvoiceItems();
            //IDataReader itemReader = null;
            DataTable itemReader = null;
            string itemsToPrint = string.Empty;
            try
            {
                itemReader = oi.GetInvoiceItemsDataTable(dbHelp, ord.InvID);
                //itemReader = oi.GetInvoiceItemsReader(dbHelp, ord.InvID);
                //string itemRow = string.Empty;
                //while (itemReader.Read())
                //{
                //    itemRow = itemRowTemplet;
                //    itemRow = itemRow.Replace("#ITM_DESCRIPTION#", BusinessUtility.GetString(itemReader["prdName"]).Replace(Environment.NewLine, "<br>"));
                //    itemRow = itemRow.Replace("#ITM_UNITS#", BusinessUtility.GetString(itemReader["invProductQty"]));
                //    itemRow = itemRow.Replace("#ITM_COST_PER_UNIT#", string.Format("{0} {1:F}", ord.InvCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(itemReader["invProductUnitPrice"]), Globals.CurrentCultureName)));
                //    itemRow = itemRow.Replace("#ITM_DISCOUNT#", string.Format("{0} {1:F}", ord.InvCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(itemReader["invProductDiscount"]), Globals.CurrentCultureName)));
                //    itemRow = itemRow.Replace("#ITM_AMOUNT#", string.Format("{0} {1:F}", ord.InvCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(itemReader["amount"]), Globals.CurrentCultureName)));

                //    itemsToPrint += itemRow;
                //}

                #region PrintLayout

                sIdx = printTemplet.IndexOf("<!--ITEM_PROD_HDG_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_PROD_HDG_END-->");
                string itemProdHdg = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdHdg = itemProdHdg.Replace("<!--ITEM_PROD_HDG_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_HDG_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_HDG_END-->");
                string itemProdDescHdg = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdDescHdg = itemProdDescHdg.Replace("<!--ITEM_DESC_HDG_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_HDG_VAL_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_HDG_VAL_END-->");
                string itemProdDescVal = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdDescVal = itemProdDescVal.Replace("<!--ITEM_DESC_HDG_VAL_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_SIZE_HDG_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_SIZE_HDG_END-->");
                string itemProdSizeHdg = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdSizeHdg = itemProdSizeHdg.Replace("<!--ITEM_DESC_SIZE_HDG_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_CNT_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_CNT_END-->");
                string itemProdDesc = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdDesc = itemProdDesc.Replace("<!--ITEM_DESC_CNT_START--><!--", "");

                var vProdGroup = from row in itemReader.AsEnumerable()
                                 group row by row.Field<string>("prdName") into grp
                                 select new
                                 {
                                     prdName = grp.Key,
                                     MemberCount = grp.Count()
                                 };


                var vProdSizeGroup = from row in itemReader.AsEnumerable()
                                     group row by row.Field<string>("Size") into grp
                                     select new
                                     {
                                         Size = grp.Key,
                                         MemberCount = grp.Count()
                                     };


                foreach (var item in vProdGroup)
                {
                    itemsToPrint += itemProdHdg.Replace("#PNAME#", item.prdName);
                    string sItemDescHdgRow = "";
                    string sSizeHdg = "";
                    foreach (var size in vProdSizeGroup)
                    {
                        sSizeHdg += itemProdSizeHdg.Replace("#SIZE#", size.Size);
                    }
                    sItemDescHdgRow += itemProdDescHdg.Replace("#HDG_COLOR#", CommonResource.ResourceValue("lblColor")).Replace("#HDG_SIZE#", sSizeHdg).Replace("#HDG_COST_PER_UNIT#", CommonResource.ResourceValue("grdvProcessCostPerUnit")).Replace("#HDG_AMOUNT#", CommonResource.ResourceValue("lblAmmount")).Replace("#HDG_DISCOUNT#", CommonResource.ResourceValue("lblDiscount2"));

                    DataView datavw = new DataView();
                    datavw = itemReader.DefaultView;
                    datavw.RowFilter = "prdName='" + BusinessUtility.GetString(item.prdName) + "'";
                    DataTable dtProduct = datavw.ToTable();

                    var vProdColorGroup = from row in dtProduct.AsEnumerable()
                                          group row by row.Field<string>("Color") into grp
                                          select new
                                          {
                                              Color = grp.Key,
                                              MemberCount = grp.Count()
                                          };
                    foreach (var color in vProdColorGroup)
                    {
                        string sSizeQty = "";
                        int qty = 0;
                        double pPrice = 0.0;
                        double pDiscount = 0.0;
                        double pAmount = 0.0;
                        string sDiscountType = "P";
                        string sDiscountFormat = "";
                        foreach (var size in vProdSizeGroup)
                        {
                            DataView dvNewPRow = new DataView();
                            dvNewPRow = itemReader.DefaultView;
                            dvNewPRow.RowFilter = "prdName='" + BusinessUtility.GetString(item.prdName) + "' AND color='" + color.Color + "' AND Size ='" + size.Size + "' ";
                            DataTable dtProductSize = datavw.ToTable();

                            if (dtProductSize.Rows.Count > 0)
                            {
                                sSizeQty += itemProdSizeHdg.Replace("#SIZE#", BusinessUtility.GetString(dtProductSize.Rows[0]["invProductQty"]));
                                qty += BusinessUtility.GetInt(dtProductSize.Rows[0]["invProductQty"]);
                                pPrice = BusinessUtility.GetDouble(dtProductSize.Rows[0]["invProductUnitPrice"]);
                                pDiscount = BusinessUtility.GetDouble(dtProductSize.Rows[0]["invProductDiscount"]);
                                pAmount += BusinessUtility.GetDouble(dtProductSize.Rows[0]["amount"]);
                                sDiscountType = BusinessUtility.GetString(dtProductSize.Rows[0]["invProductDiscountType"]);
                            }
                            else
                            {
                                sSizeQty += itemProdSizeHdg.Replace("#SIZE#", BusinessUtility.GetString("0"));
                                qty += BusinessUtility.GetInt(0);
                            }

                            //foreach (DataRow drSize in dtProductSize.Rows)
                            //{
                            //    sSizeQty += itemProdSizeHdg.Replace("#SIZE#", BusinessUtility.GetString(drSize["invProductQty"]));
                            //    qty += BusinessUtility.GetInt(drSize["invProductQty"]);
                            //    pPrice = BusinessUtility.GetDouble(drSize["invProductUnitPrice"]);
                            //    pDiscount = BusinessUtility.GetDouble(drSize["invProductDiscount"]);
                            //    pAmount = BusinessUtility.GetDouble(drSize["amount"]);

                            //}
                        }
                        //double pAmount = pPrice * qty;
                        //total += pAmount;
                        //sItemDescHdgRow += itemProdDescVal.Replace("#HDG_COLOR#", color.Color).Replace("#HDG_SIZE#", sSizeQty).Replace("#HDG_COST_PER_UNIT#", BusinessUtility.GetCurrencyString(pPrice, Globals.CurrentCultureName)).Replace("#HDG_AMOUNT#", BusinessUtility.GetCurrencyString(pAmount, Globals.CurrentCultureName)).Replace("#HDG_DISCOUNT#", BusinessUtility.GetString(pDiscount));

                        string sDiscVal = "";
                        if (sDiscountType.ToUpper() == "P")
                        {
                            sDiscountFormat = "%";
                            sDiscVal = BusinessUtility.GetString(pDiscount) + sDiscountFormat;
                        }
                        else
                        {
                            sDiscVal = CurrencyFormat.GetAmountInUSCulture(pDiscount);
                            sDiscountFormat = "";
                        }

                        sItemDescHdgRow += itemProdDescVal.Replace("#HDG_COLOR#", color.Color).Replace("#HDG_SIZE#", sSizeQty).Replace("#HDG_COST_PER_UNIT#", BusinessUtility.GetCurrencyString(pPrice, Globals.CurrentCultureName)).Replace("#HDG_AMOUNT#", CurrencyFormat.GetCompanyCurrencyFormat(pAmount, ord.InvCurrencyCode, Globals.CurrentCultureName)).Replace("#HDG_DISCOUNT#", sDiscVal);
                    }
                    itemsToPrint += itemProdDesc.Replace("#ITEM_GROUP_DESC#", sItemDescHdgRow);
                }
                #endregion
            }
            finally
            {
                //if (itemReader != null && !itemReader.IsClosed)
                //{
                //    itemReader.Close();
                //    itemReader.Dispose();
                //}
            }

            printTemplet = printTemplet.Replace("<!--ITEM_ROW_LIST_PLACEHOLDER-->", itemsToPrint);


            printTemplet = printTemplet.Replace("#PROCESS_ITEM_LIST_TITLE#", CommonResource.ResourceValue("lblServices"));

            //Build Process Header
            printTemplet = printTemplet.Replace("#PITEM_HDR_PROCESS_CODE#", CommonResource.ResourceValue("grdProcessCode"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_DESCRIPTION#", CommonResource.ResourceValue("lblDescription"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_FIXED_COST#", CommonResource.ResourceValue("lblProcessFixedCost"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_COST_PER_HOUR#", CommonResource.ResourceValue("lblCostPerHour"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_COST_PER_UNIT#", CommonResource.ResourceValue("lblCostPerUnit"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_TOTAL_HOURS#", CommonResource.ResourceValue("lblSOTotalHour"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_TOTAL_UNITS#", CommonResource.ResourceValue("lblSOTotalUnit"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_PROCESS_COST#", CommonResource.ResourceValue("prnProcessCost"));


            InvoiceItemProcess oip = new InvoiceItemProcess();
            IDataReader processReader = null;
            string processToPrint = string.Empty;
            try
            {
                processReader = oip.GetProcessItemsReader(dbHelp, ord.InvID);
                bool showProcess = false;
                string processItemRow = string.Empty;
                while (processReader.Read())
                {
                    processItemRow = processItemRowTemplet;
                    if (!showProcess)
                    {
                        showProcess = true;
                    }
                    processItemRow = processItemRow.Replace("#PITEM_PROCESS_CODE#", BusinessUtility.GetString(processReader["invItemProcCode"]));
                    processItemRow = processItemRow.Replace("#PITEM_DESCRIPTION#", BusinessUtility.GetString(processReader["ProcessDescription"]).Replace(Environment.NewLine, "<br>"));
                    //processItemRow = processItemRow.Replace("#PITEM_FIXED_COST#", string.Format("{0} {1:F}", ord.InvCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(processReader["invItemProcFixedPrice"]), Globals.CurrentCultureName)));
                    //processItemRow = processItemRow.Replace("#PITEM_COST_PER_HOUR#", string.Format("{0} {1:F}", ord.InvCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(processReader["invItemProcPricePerHour"]), Globals.CurrentCultureName)));
                    //processItemRow = processItemRow.Replace("#PITEM_COST_PER_UNIT#", string.Format("{0} {1:F}", ord.InvCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(processReader["invItemProcPricePerUnit"]), Globals.CurrentCultureName)));
                    processItemRow = processItemRow.Replace("#PITEM_FIXED_COST#", string.Format("{0} {1:F}", ord.InvCurrencyCode, CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(processReader["invItemProcFixedPrice"]), Globals.CurrentCultureName))));
                    processItemRow = processItemRow.Replace("#PITEM_COST_PER_HOUR#", string.Format("{0} {1:F}", ord.InvCurrencyCode, CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(processReader["invItemProcPricePerHour"]), Globals.CurrentCultureName))));
                    processItemRow = processItemRow.Replace("#PITEM_COST_PER_UNIT#", string.Format("{0} {1:F}", ord.InvCurrencyCode, CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(processReader["invItemProcPricePerUnit"]), Globals.CurrentCultureName))));
                    processItemRow = processItemRow.Replace("#PITEM_TOTAL_HOURS#", string.Format("{0}", processReader["invItemProcHours"]));
                    processItemRow = processItemRow.Replace("#PITEM_TOTAL_UNITS#", string.Format("{0}", processReader["invItemProcUnits"]));
                    //processItemRow = processItemRow.Replace("#PITEM_PROCESS_COST#", string.Format("{0} {1:F}", ord.InvCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(processReader["ProcessCost"]), Globals.CurrentCultureName)));
                    processItemRow = processItemRow.Replace("#PITEM_PROCESS_COST#", string.Format("{0} {1:F}", ord.InvCurrencyCode, CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(processReader["ProcessCost"]), Globals.CurrentCultureName))));

                    processToPrint += processItemRow;
                }

                printTemplet = printTemplet.Replace("<!--PROCESS_ITEM_ROW_LIST_PLACEHOLDER-->", processToPrint);
                if (showProcess)
                {
                    printTemplet = printTemplet.Replace("#SERVICE_DISPLAY#", "");
                }
                else
                {
                    printTemplet = printTemplet.Replace("#SERVICE_DISPLAY#", "display:none;");
                }
            }
            finally
            {
                if (processReader != null && !processReader.IsClosed)
                {
                    processReader.Close();
                    processReader.Dispose();
                }
            }

            printTemplet = printTemplet.Replace(processItemRowTemplet, processToPrint);

            TotalSummary lTotal = CalculationHelper.GetInvoiceTotal(dbHelp, ord.InvID);
            //Set Lables for html to be generated
            //lTotal.SetLabel(TotalSummary.LabelKey.BalanceAmount, Resources.Resource.lblBalanceAmount);
            //lTotal.SetLabel(TotalSummary.LabelKey.GrandSubTotal, Resources.Resource.lblSubTotalOrder);
            //lTotal.SetLabel(TotalSummary.LabelKey.GrandTotal, Resources.Resource.lblOrderTotal);
            //lTotal.SetLabel(TotalSummary.LabelKey.ReceivedAmount, Resources.Resource.lblReceivedAmount);
            //lTotal.SetLabel(TotalSummary.LabelKey.SubTotalItems, Resources.Resource.lblSubTotalItems);
            //lTotal.SetLabel(TotalSummary.LabelKey.SubTotalServices, Resources.Resource.lblSubTotalServices);
            //lTotal.SetLabel(TotalSummary.LabelKey.TotalDiscount, Resources.Resource.lblDiscount);
            //lTotal.SetLabel(TotalSummary.LabelKey.TotalTax, Resources.Resource.lblTotalTax);

            printTemplet = printTemplet.Replace("#ITEM_SUB_TOTAL_TABLE#", CommonHtml.GetItemTotalHtml(lTotal, ord.InvCustID, ord.InvForOrderNo));
            printTemplet = printTemplet.Replace("#PROCRESS_ITEM_SUB_TOTAL_TABLE#", CommonHtml.GetProcessTotalHtml(lTotal));
            printTemplet = printTemplet.Replace("#GRAND_TOTAL#", CommonHtml.GetGrandTotalHtml(lTotal).Replace("#HISTORY_LINK#", ""));

            if (!string.IsNullOrEmpty(appName) && appName.ToLower() == "sivananda")
            {
                //Hide default document header
                printTemplet = printTemplet.Replace("<!--SHIVANANDA_HEADER_START--><!--", "");
                printTemplet = printTemplet.Replace("--><!--SHIVANANDA_HEADER_END-->", "");
                printTemplet = printTemplet.Replace("#DEFAULT_HEADER_DISPLAY#", "display:none;");
                printTemplet = printTemplet.Replace("#SHIVANANDA_DISPLAY#", "");
            }
            else
            {
                //show default header
                printTemplet = printTemplet.Replace("#DEFAULT_HEADER_DISPLAY#", "");
                printTemplet = printTemplet.Replace("#SHIVANANDA_DISPLAY#", "display:none");
            }

            //Apply Barcode prefix & postfix
            printTemplet = printTemplet.Replace("#BARCODE_PREFIX#", AppConfiguration.AppSettings[AppSettingKey.BarcodePrefix]);
            printTemplet = printTemplet.Replace("#BARCODE_POSTFIX#", AppConfiguration.AppSettings[AppSettingKey.BarcodePrefix]);

            //Update Print Counter
            ord.UpdateInvoicePrintCounter(dbHelp, ord.InvID);

            return printTemplet;
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    //Packing List Common Print
    public static string GetPackagePrintHtmlSnipt(Page pg, int soID)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            //Set templates html to litrals   
            string printTemplet = File.ReadAllText(HttpContext.Current.Server.MapPath("~/PrintTemplates/PackageList.htm"));
            string itemRowTemplet = string.Empty;
            int sIdx, eIdx;
            //sIdx = printTemplet.IndexOf("<!--ITEM_TEMPLATE_START--><!--");
            //eIdx = printTemplet.IndexOf("--><!--ITEM_TEMPLATE_END-->");
            //itemRowTemplet = printTemplet.Substring(sIdx, eIdx - sIdx).Replace("<!--ITEM_TEMPLATE_START--><!--", "");

            //Init Requested Order
            Orders ord = new Orders();
            ord.PopulateObject(dbHelp, soID);

            //Init Customer 
            Partners part = new Partners();
            part.PopulateObject(dbHelp, ord.OrdCustID);

            //Init CompanyInfo
            SysCompanyInfo cinfo = new SysCompanyInfo();
            cinfo.PopulateObject(ord.OrdCompanyID, dbHelp);

            //Init Shipping Warehous 
            SysWarehouses whs = new SysWarehouses();
            whs.PopulateObject(ord.OrdShpWhsCode, dbHelp);

            //TO DO TO SET Company Info & Logo on Header also need to set Html in Header
            if (!string.IsNullOrEmpty(cinfo.CompanyLogoPath))
            {
                //printTemplet = printTemplet.Replace("#COM_LOGO#", pg.ResolveUrl("~/Upload/companylogo/") + cinfo.CompanyLogoPath);
                string logoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/Upload/companylogo/") + cinfo.CompanyLogoPath;
                printTemplet = printTemplet.Replace("#COMPANY_LOGO_URL#", logoUrl);
            }
            else
            {
                printTemplet = printTemplet.Replace("#COM_LOGO#", "");
            }

            printTemplet = printTemplet.Replace("#ITM_HDR_DESCRIPTION#", CommonResource.ResourceValue("lblDescription"));
            printTemplet = printTemplet.Replace("#COMPANY_NAME#", whs.WarehouseDescription);
            printTemplet = printTemplet.Replace("#COMPANY_EMAIL#", whs.WarehouseEmailID);
            printTemplet = printTemplet.Replace("#CONTACT_NO#", part.ExtendedProperties.EmergencyContactName);
            printTemplet = printTemplet.Replace("#PHONE_NO#", part.PartnerPhone);

            printTemplet = printTemplet.Replace("#COMPANY_ADDRESS#", GetAddress(whs));
            string phone = string.Empty;
            if (!string.IsNullOrEmpty(whs.WarehousePhone))
            {
                phone = string.Format("Tel: {0}", whs.WarehousePhone);
            }
            if (!string.IsNullOrEmpty(whs.WarehouseFax))
            {
                phone += string.Format("<br> Fax: {0}", whs.WarehouseFax);
            }
            //Add tol free no & fax
            printTemplet = printTemplet.Replace("#COMPANY_FHONE_FAX#", phone);
            printTemplet = printTemplet.Replace("#ORDER_NO#", soID.ToString());
            printTemplet = printTemplet.Replace("#CUST_ID#", ord.OrdCustID.ToString());
            printTemplet = printTemplet.Replace("#BUYER_NAME#", part.ExtendedProperties.EmergencyContactName);
            printTemplet = printTemplet.Replace("#CUSTOMER_NAME#", part.PartnerLongName);
            printTemplet = printTemplet.Replace("#SHIP_VIA#", "");
            printTemplet = printTemplet.Replace("#ORDER_DATE#", ord.OrdCreatedOn.ToString("MM/dd/yyyy"));
            printTemplet = printTemplet.Replace("#NET_TERMS#", ord.OrdNetTerms);
            printTemplet = printTemplet.Replace("#ORDER_NOTES#", ord.OrdComment);
            printTemplet = printTemplet.Replace("#TRACKINGURL#", "");

            printTemplet = printTemplet.Replace("#PKGWEIGHT#", "");
            printTemplet = printTemplet.Replace("#PKGPKG#", "");
            printTemplet = printTemplet.Replace("#SHIP_DATE#", ord.OrdShpDate.ToString("MM/dd/yyyy"));

            //if (ord.ChkISParentID(ord.ParentOrdID) == true)
            //{
            if (ord.HasOrderToShiftQty(ord.ParentOrdID))
            {
                printTemplet = printTemplet.Replace("#REMANING_QTY_MSG#", "Solde à venir / Balance to come");
            }
            else
            {
                printTemplet = printTemplet.Replace("#REMANING_QTY_MSG#", "Pas de balance / No Balance");
            }
            //}
            //else
            //{
            //    printTemplet = printTemplet.Replace("#REMANING_QTY_MSG#", "No Balance / No Balance");
            //}

            Invoice objInvoice = new Invoice();
            int iInvNo = objInvoice.GetInvoiceID(dbHelp, ord.OrdID);
            if (iInvNo > 0)
            {
                printTemplet = printTemplet.Replace("#INVOICENO#", "N ° de facture / Invoice # : " + BusinessUtility.GetString(iInvNo));
            }
            else
            {
                printTemplet = printTemplet.Replace("#INVOICENO#", "");
            }


            //Set Bill To Address
            Addresses billToAddress = new Addresses();
            //billToAddress.PopulateObject(dbHelp, ord.OrdCustID, ord.OrdCustType, AddressType.BILL_TO_ADDRESS);
            billToAddress.GetOrderAddress(ord.OrdID, AddressType.BILL_TO_ADDRESS);
            printTemplet = printTemplet.Replace("#BILL_TO_ADDRESS_TITLE#", CommonResource.ResourceValue("BillTo"));
            printTemplet = printTemplet.Replace("#BILL_TO_ADDRESS#", GetAddress(billToAddress));


            //Set Ship To Address
            Addresses shipToAddress = new Addresses();
            //shipToAddress.PopulateObject(dbHelp, ord.OrdCustID, ord.OrdCustType, AddressType.SHIP_TO_ADDRESS);
            shipToAddress.GetOrderAddress(ord.OrdID, AddressType.SHIP_TO_ADDRESS);
            printTemplet = printTemplet.Replace("#SHIP_TO_ADDRESS_TITLE#", CommonResource.ResourceValue("ShipTo"));
            if (ord.OrderTypeCommission == 2)
            {
                SysWarehouses whsToship = new SysWarehouses();
                whsToship.PopulateObject(ord.OrdShpToWhsCode, dbHelp);
                printTemplet = printTemplet.Replace("#SHIP_TO_ADDRESS#", GetAddress(whsToship));
            }
            else
            {
                printTemplet = printTemplet.Replace("#SHIP_TO_ADDRESS#", GetAddress(shipToAddress));
            }


            //Build Item Header      
            //printTemplet = printTemplet.Replace("#ITM_HDR_DESCRIPTION#", "Description");
            //printTemplet = printTemplet.Replace("#ITM_HDR_UNITS#", "Units");
            //printTemplet = printTemplet.Replace("#ITM_HDR_COST_PER_UNIT#", "Cost Per Unit");
            //printTemplet = printTemplet.Replace("#ITM_HDR_DISCOUNT#", "Discount");
            //printTemplet = printTemplet.Replace("#ITM_HDR_AMOUNT#", "Amount");

            OrderItems oi = new OrderItems();
            //IDataReader itemReader = null;
            DataTable itemReader = null;
            string itemsToPrint = string.Empty;
            try
            {
                itemReader = oi.GetOrderItemsTable(dbHelp, ord.OrdID);
                //itemReader = oi.GetOrderItemsReader(dbHelp, ord.OrdID);
                //string itemRow = string.Empty;
                //while (itemReader.Read())
                //{
                //    itemRow = itemRowTemplet;
                //    itemRow = itemRow.Replace("#ITM_DESCRIPTION#", BusinessUtility.GetString(itemReader["prdName"]).Replace(Environment.NewLine, "<br>"));
                //    itemRow = itemRow.Replace("#ITM_UNITS#", BusinessUtility.GetString(itemReader["ordProductQty"]));
                //    itemRow = itemRow.Replace("#ITM_COST_PER_UNIT#", string.Format("{0} {1:F}", ord.OrdCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(itemReader["ordProductUnitPrice"]), Globals.CurrentCultureName)));
                //    itemRow = itemRow.Replace("#ITM_DISCOUNT#", string.Format("{0} {1:F}", ord.OrdCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(itemReader["ordProductDiscount"]), Globals.CurrentCultureName)));
                //    itemRow = itemRow.Replace("#ITM_AMOUNT#", string.Format("{0} {1:F}", ord.OrdCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(itemReader["amount"]), Globals.CurrentCultureName)));

                //    itemsToPrint += itemRow;
                //}

                #region PrintLayout

                sIdx = printTemplet.IndexOf("<!--ITEM_PROD_HDG_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_PROD_HDG_END-->");
                string itemProdHdg = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdHdg = itemProdHdg.Replace("<!--ITEM_PROD_HDG_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_HDG_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_HDG_END-->");
                string itemProdDescHdg = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdDescHdg = itemProdDescHdg.Replace("<!--ITEM_DESC_HDG_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_HDG_VAL_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_HDG_VAL_END-->");
                string itemProdDescVal = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdDescVal = itemProdDescVal.Replace("<!--ITEM_DESC_HDG_VAL_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_SIZE_HDG_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_SIZE_HDG_END-->");
                string itemProdSizeHdg = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdSizeHdg = itemProdSizeHdg.Replace("<!--ITEM_DESC_SIZE_HDG_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_CNT_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_CNT_END-->");
                string itemProdDesc = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdDesc = itemProdDesc.Replace("<!--ITEM_DESC_CNT_START--><!--", "");

                var vProdGroup = from row in itemReader.AsEnumerable()
                                 group row by row.Field<string>("prdName") into grp
                                 select new
                                 {
                                     prdName = grp.Key,
                                     MemberCount = grp.Count()
                                 };


                var vProdSizeGroup = from row in itemReader.AsEnumerable()
                                     group row by row.Field<string>("Size") into grp
                                     select new
                                     {
                                         Size = grp.Key,
                                         MemberCount = grp.Count()
                                     };


                foreach (var item in vProdGroup)
                {


                    itemsToPrint += itemProdHdg.Replace("#PNAME#", item.prdName);
                    string sItemDescHdgRow = "";
                    string sSizeHdg = "";
                    int iTotalSizeCount = 0;
                    foreach (var size in vProdSizeGroup)
                    {
                        iTotalSizeCount += 1;
                        sSizeHdg += itemProdSizeHdg.Replace("#SIZE#", size.Size);
                    }
                    sItemDescHdgRow += itemProdDescHdg.Replace("#HDG_COLOR#", CommonResource.ResourceValue("lblColor")).Replace("#HDG_SIZE#", sSizeHdg).Replace("#HDG_COST_PER_UNIT#", "Coût/unité / Cost/Unit ").Replace("#HDG_AMOUNT#", "Montant / Amount").Replace("#HDG_DISCOUNT#", "Réduction / Discount");

                    DataView datavw = new DataView();
                    datavw = itemReader.DefaultView;
                    datavw.RowFilter = "prdName='" + BusinessUtility.GetString(item.prdName) + "'";
                    DataTable dtProduct = datavw.ToTable();

                    var vProdColorGroup = from row in dtProduct.AsEnumerable()
                                          group row by row.Field<string>("Color") into grp
                                          select new
                                          {
                                              Color = grp.Key,
                                              MemberCount = grp.Count()
                                          };
                    foreach (var color in vProdColorGroup)
                    {
                        string sSizeQty = "";
                        int qty = 0;
                        double pPrice = 0.0;
                        double pDiscount = 0.0;
                        double pAmount = 0.0;
                        foreach (var size in vProdSizeGroup)
                        {
                            DataView dvNewPRow = new DataView();
                            dvNewPRow = itemReader.DefaultView;
                            dvNewPRow.RowFilter = "prdName='" + BusinessUtility.GetString(item.prdName) + "' AND color='" + color.Color + "' AND Size ='" + size.Size + "' ";
                            DataTable dtProductSize = datavw.ToTable();

                            if (dtProductSize.Rows.Count > 0)
                            {
                                sSizeQty += itemProdSizeHdg.Replace("#SIZE#", BusinessUtility.GetString(dtProductSize.Rows[0]["ordProductQty"]));
                                qty += BusinessUtility.GetInt(dtProductSize.Rows[0]["ordProductQty"]);
                                pPrice = BusinessUtility.GetDouble(dtProductSize.Rows[0]["ordProductUnitPrice"]);
                                pDiscount = BusinessUtility.GetDouble(dtProductSize.Rows[0]["ordProductDiscount"]);
                                pAmount += BusinessUtility.GetDouble(dtProductSize.Rows[0]["amount"]);
                            }
                            else
                            {
                                sSizeQty += itemProdSizeHdg.Replace("#SIZE#", BusinessUtility.GetString("0"));
                                qty += BusinessUtility.GetInt(0);
                            }
                        }
                        //sItemDescHdgRow += itemProdDescVal.Replace("#HDG_COLOR#", color.Color).Replace("#HDG_SIZE#", sSizeQty).Replace("#HDG_COST_PER_UNIT#", CurrencyFormat.GetCompanyCurrencyFormat(pPrice,ord.OrdCurrencyCode)).Replace("#HDG_AMOUNT#", BusinessUtility.GetCurrencyString(pAmount, Globals.CurrentCultureName)).Replace("#HDG_DISCOUNT#", BusinessUtility.GetString(pDiscount));
                        sItemDescHdgRow += itemProdDescVal.Replace("#HDG_COLOR#", color.Color).Replace("#HDG_SIZE#", sSizeQty).Replace("#HDG_COST_PER_UNIT#", CurrencyFormat.GetCompanyCurrencyFormat(pPrice, ord.OrdCurrencyCode)).Replace("#HDG_AMOUNT#", CurrencyFormat.GetCompanyCurrencyFormat(pAmount, ord.OrdCurrencyCode)).Replace("#HDG_DISCOUNT#", BusinessUtility.GetString(pDiscount));
                    }
                    itemsToPrint += itemProdDesc.Replace("#ITEM_GROUP_DESC#", sItemDescHdgRow);
                }
                #endregion
            }
            finally
            {
                //if (itemReader != null && !itemReader.IsClosed)
                //{
                //    itemReader.Close();
                //    itemReader.Dispose();
                //}
            }

            printTemplet = printTemplet.Replace("<!--ITEM_ROW_LIST_PLACEHOLDER-->", itemsToPrint);

            //Update Print Counter
            ord.UpdatePackingListPrintCounter(dbHelp, ord.OrdID);

            //Apply Barcode prefix & postfix
            printTemplet = printTemplet.Replace("#BARCODE_PREFIX#", AppConfiguration.AppSettings[AppSettingKey.BarcodePrefix]);
            printTemplet = printTemplet.Replace("#BARCODE_POSTFIX#", AppConfiguration.AppSettings[AppSettingKey.BarcodePrefix]);

            return printTemplet;
        }
        catch
        {
            throw;
        }

        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    //Invoice Print for Possupply
    private static string GetInvoicePrintHtmlSniptPosSupply(Page pd, int invID)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            //Set templates html to litrals   
            string printTemplet = File.ReadAllText(HttpContext.Current.Server.MapPath("~/PrintTemplates/PosSupplyInvoice.htm"));
            string itemRowTemplet = string.Empty;
            string processItemRowTemplet = string.Empty;
            string totalTemplet = string.Empty;
            int sIdx, eIdx;
            sIdx = printTemplet.IndexOf("<!--ITEM_TEMPLATE_START--><!--");
            eIdx = printTemplet.IndexOf("--><!--ITEM_TEMPLATE_END-->");
            if (sIdx == -1 || eIdx == -1)
            {
                return printTemplet;
            }
            itemRowTemplet = printTemplet.Substring(sIdx, eIdx - sIdx).Replace("<!--ITEM_TEMPLATE_START--><!--", "");

            sIdx = printTemplet.IndexOf("<!--PROCESS_ITEM_TEMPLATE_START--><!--");
            eIdx = printTemplet.IndexOf("--><!--PROCESS_ITEM_TEMPLATE_END-->");
            if (sIdx == -1 || eIdx == -1)
            {
                return printTemplet;
            }
            processItemRowTemplet = printTemplet.Substring(sIdx, eIdx - sIdx).Replace("<!--PROCESS_ITEM_TEMPLATE_START--><!--", "");

            sIdx = printTemplet.IndexOf("@TOTAL_ITEM_START@");
            eIdx = printTemplet.IndexOf("@TOTAL_ITEM_END@");
            totalTemplet = printTemplet.Substring(sIdx, eIdx - sIdx).Replace("@TOTAL_ITEM_START@", "");

            //Init Requested Order
            Invoice ord = new Invoice();
            ord.PopulateObject(dbHelp, invID);

            //Init Customer 
            Partners part = new Partners();
            part.PopulateObject(dbHelp, ord.InvCustID);

            //Init CompanyInfo
            SysCompanyInfo cinfo = new SysCompanyInfo();
            cinfo.PopulateObject(ord.InvCompanyID, dbHelp);

            //Populate shipping ware shouse
            SysWarehouses whs = new SysWarehouses();
            whs.PopulateObject(ord.InvShpWhsCode, dbHelp);

            //TO DO TO SET Company Info & Logo on Header also need to set Html in Header
            if (!string.IsNullOrEmpty(cinfo.CompanyLogoPath))
            {
                //printTemplet = printTemplet.Replace("#COM_LOGO#", pg.ResolveUrl("~/Upload/companylogo/") + cinfo.CompanyLogoPath);
                string logoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/Upload/companylogo/") + cinfo.CompanyLogoPath;
                printTemplet = printTemplet.Replace("#COMPANY_LOGO_URL#", logoUrl);
            }
            else
            {
                printTemplet = printTemplet.Replace("#COM_LOGO#", "");
            }
            printTemplet = printTemplet.Replace("#TRACKINGURL#", "");
            printTemplet = printTemplet.Replace("#COMPANY_NAME#", whs.WarehouseDescription);
            printTemplet = printTemplet.Replace("#COMPANY_EMAIL#", whs.WarehouseEmailID);
            printTemplet = printTemplet.Replace("#COMPANY_ADDRESS#", GetAddress(whs));
            string phone = string.Empty;
            if (!string.IsNullOrEmpty(whs.WarehousePhone))
            {
                phone = string.Format("Tel: {0}", whs.WarehousePhone);
            }
            if (!string.IsNullOrEmpty(whs.WarehouseFax))
            {
                phone += string.Format("<br> Fax: {0}", whs.WarehouseFax);
            }
            printTemplet = printTemplet.Replace("#COMPANY_FHONE_FAX#", phone);
            printTemplet = printTemplet.Replace("#INVOICE_NO#", invID.ToString());
            printTemplet = printTemplet.Replace("#CUSTOMER_ID#", ord.InvCustID.ToString());
            printTemplet = printTemplet.Replace("#BUYER_NAME#", part.ExtendedProperties.EmergencyContactName);
            printTemplet = printTemplet.Replace("#CUSTOMER_NAME#", part.PartnerLongName);
            printTemplet = printTemplet.Replace("#SHIP_VIA#", "");
            printTemplet = printTemplet.Replace("#INVOICE_DATE#", ord.InvCreatedOn.ToString("MM/dd/yyyy"));

            Orders o = new Orders();
            o.PopulateObject(dbHelp, ord.InvForOrderNo);
            printTemplet = printTemplet.Replace("#Net Terms#", o.OrdNetTerms);

            //Set Bill To Address
            Addresses billToAddress = new Addresses();
            billToAddress.PopulateObject(dbHelp, ord.InvCustID, ord.InvCustType, AddressType.BILL_TO_ADDRESS);
            printTemplet = printTemplet.Replace("#BILL_TO_ADDRESS_TITLE#", CommonResource.ResourceValue("BillTo"));
            printTemplet = printTemplet.Replace("#BILL_TO_ADDRESS#", GetAddress(billToAddress));


            //Set Ship To Address
            Addresses shipToAddress = new Addresses();
            shipToAddress.PopulateObject(dbHelp, ord.InvCustID, ord.InvCustType, AddressType.SHIP_TO_ADDRESS);
            printTemplet = printTemplet.Replace("#SHIP_TO_ADDRESS_TITLE#", CommonResource.ResourceValue("ShipTo"));
            printTemplet = printTemplet.Replace("#SHIP_TO_ADDRESS#", GetAddress(shipToAddress));

            printTemplet = printTemplet.Replace("#ITEM_LIST_TITLE#", CommonResource.ResourceValue("lblItems"));

            //Build Item Header      
            printTemplet = printTemplet.Replace("#ITM_HDR_DESCRIPTION#", CommonResource.ResourceValue("lblDescription"));
            printTemplet = printTemplet.Replace("#ITM_HDR_UNITS#", CommonResource.ResourceValue("lblUnits"));
            printTemplet = printTemplet.Replace("#ITM_HDR_COST_PER_UNIT#", CommonResource.ResourceValue("lblCostPerUnit"));
            printTemplet = printTemplet.Replace("#ITM_HDR_DISCOUNT#", CommonResource.ResourceValue("grdSaleDiscount"));
            printTemplet = printTemplet.Replace("#ITM_HDR_AMOUNT#", CommonResource.ResourceValue("lblAmount"));

            InvoiceItems oi = new InvoiceItems();
            //IDataReader itemReader = null;
            DataTable itemReader = null;
            string itemsToPrint = string.Empty;
            try
            {
                itemReader = oi.GetInvoiceItemsDataTable(dbHelp, ord.InvID);
                //itemReader = oi.GetInvoiceItemsReader(dbHelp, ord.InvID);
                //string itemRow = string.Empty;
                //while (itemReader.Read())
                //{
                //    itemRow = itemRowTemplet;
                //    itemRow = itemRow.Replace("#ITM_DESCRIPTION#", BusinessUtility.GetString(itemReader["prdName"]).Replace(Environment.NewLine, "<br>"));
                //    itemRow = itemRow.Replace("#ITM_UNITS#", BusinessUtility.GetString(itemReader["invProductQty"]));
                //    itemRow = itemRow.Replace("#ITM_COST_PER_UNIT#", string.Format("{0} {1:F}", ord.InvCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(itemReader["invProductUnitPrice"]), Globals.CurrentCultureName)));
                //    itemRow = itemRow.Replace("#ITM_DISCOUNT#", string.Format("{0} {1:F}", ord.InvCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(itemReader["invProductDiscount"]), Globals.CurrentCultureName)));
                //    itemRow = itemRow.Replace("#ITM_AMOUNT#", string.Format("{0} {1:F}", ord.InvCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(itemReader["amount"]), Globals.CurrentCultureName)));

                //    itemsToPrint += itemRow;
                //}

                #region PrintLayout

                sIdx = printTemplet.IndexOf("<!--ITEM_PROD_HDG_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_PROD_HDG_END-->");
                string itemProdHdg = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdHdg = itemProdHdg.Replace("<!--ITEM_PROD_HDG_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_HDG_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_HDG_END-->");
                string itemProdDescHdg = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdDescHdg = itemProdDescHdg.Replace("<!--ITEM_DESC_HDG_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_HDG_VAL_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_HDG_VAL_END-->");
                string itemProdDescVal = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdDescVal = itemProdDescVal.Replace("<!--ITEM_DESC_HDG_VAL_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_SIZE_HDG_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_SIZE_HDG_END-->");
                string itemProdSizeHdg = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdSizeHdg = itemProdSizeHdg.Replace("<!--ITEM_DESC_SIZE_HDG_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_CNT_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_CNT_END-->");
                string itemProdDesc = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdDesc = itemProdDesc.Replace("<!--ITEM_DESC_CNT_START--><!--", "");

                var vProdGroup = from row in itemReader.AsEnumerable()
                                 group row by row.Field<string>("prdName") into grp
                                 select new
                                 {
                                     prdName = grp.Key,
                                     MemberCount = grp.Count()
                                 };


                var vProdSizeGroup = from row in itemReader.AsEnumerable()
                                     group row by row.Field<string>("Size") into grp
                                     select new
                                     {
                                         Size = grp.Key,
                                         MemberCount = grp.Count()
                                     };


                foreach (var item in vProdGroup)
                {
                    itemsToPrint += itemProdHdg.Replace("#PNAME#", item.prdName);
                    string sItemDescHdgRow = "";
                    string sSizeHdg = "";
                    foreach (var size in vProdSizeGroup)
                    {
                        sSizeHdg += itemProdSizeHdg.Replace("#SIZE#", size.Size);
                    }
                    sItemDescHdgRow += itemProdDescHdg.Replace("#HDG_COLOR#", CommonResource.ResourceValue("lblColor")).Replace("#HDG_SIZE#", sSizeHdg).Replace("#HDG_COST_PER_UNIT#", CommonResource.ResourceValue("grdvProcessCostPerUnit")).Replace("#HDG_AMOUNT#", CommonResource.ResourceValue("lblAmmount")).Replace("#HDG_DISCOUNT#", CommonResource.ResourceValue("lblDiscount"));

                    DataView datavw = new DataView();
                    datavw = itemReader.DefaultView;
                    datavw.RowFilter = "prdName='" + BusinessUtility.GetString(item.prdName) + "'";
                    DataTable dtProduct = datavw.ToTable();

                    var vProdColorGroup = from row in dtProduct.AsEnumerable()
                                          group row by row.Field<string>("Color") into grp
                                          select new
                                          {
                                              Color = grp.Key,
                                              MemberCount = grp.Count()
                                          };
                    foreach (var color in vProdColorGroup)
                    {
                        string sSizeQty = "";
                        int qty = 0;
                        double pPrice = 0.0;
                        double pDiscount = 0.0;
                        double pAmount = 0.0;
                        foreach (var size in vProdSizeGroup)
                        {
                            DataView dvNewPRow = new DataView();
                            dvNewPRow = itemReader.DefaultView;
                            dvNewPRow.RowFilter = "prdName='" + BusinessUtility.GetString(item.prdName) + "' AND color='" + color.Color + "' AND Size ='" + size.Size + "' ";
                            DataTable dtProductSize = datavw.ToTable();
                            if (dtProductSize.Rows.Count > 0)
                            {
                                sSizeQty += itemProdSizeHdg.Replace("#SIZE#", BusinessUtility.GetString(dtProductSize.Rows[0]["invProductQty"]));
                                qty += BusinessUtility.GetInt(dtProductSize.Rows[0]["invProductQty"]);
                                pPrice = BusinessUtility.GetDouble(dtProductSize.Rows[0]["invProductUnitPrice"]);
                                pDiscount = BusinessUtility.GetDouble(dtProductSize.Rows[0]["invProductDiscount"]);
                                pAmount = BusinessUtility.GetDouble(dtProductSize.Rows[0]["amount"]);
                            }
                            else
                            {
                                sSizeQty += itemProdSizeHdg.Replace("#SIZE#", BusinessUtility.GetString("0"));
                                qty += BusinessUtility.GetInt(0);
                            }
                            //foreach (DataRow drSize in dtProductSize.Rows)
                            //{
                            //    sSizeQty += itemProdSizeHdg.Replace("#SIZE#", BusinessUtility.GetString(drSize["invProductQty"]));
                            //    qty += BusinessUtility.GetInt(drSize["invProductQty"]);
                            //    pPrice = BusinessUtility.GetDouble(drSize["invProductUnitPrice"]);
                            //    pDiscount = BusinessUtility.GetDouble(drSize["invProductDiscount"]);
                            //    pAmount = BusinessUtility.GetDouble(drSize["amount"]);

                            //}
                        }
                        //double pAmount = pPrice * qty;
                        //total += pAmount;
                        sItemDescHdgRow += itemProdDescVal.Replace("#HDG_COLOR#", color.Color).Replace("#HDG_SIZE#", sSizeQty).Replace("#HDG_COST_PER_UNIT#", BusinessUtility.GetCurrencyString(pPrice, Globals.CurrentCultureName)).Replace("#HDG_AMOUNT#", BusinessUtility.GetCurrencyString(pAmount, Globals.CurrentCultureName)).Replace("#HDG_DISCOUNT#", BusinessUtility.GetString(qty));
                    }
                    itemsToPrint += itemProdDesc.Replace("#ITEM_GROUP_DESC#", sItemDescHdgRow);
                }
                #endregion
            }
            finally
            {
                //if (itemReader != null && !itemReader.IsClosed)
                //{
                //    itemReader.Close();
                //    itemReader.Dispose();
                //}
            }

            printTemplet = printTemplet.Replace("<!--ITEM_LIST_PLACEHOLDER-->", itemsToPrint);


            printTemplet = printTemplet.Replace("#PROCESS_ITEM_LIST_TITLE#", CommonResource.ResourceValue("lblServices"));

            //Build Process Header
            printTemplet = printTemplet.Replace("#PITEM_HDR_PROCESS_CODE#", CommonResource.ResourceValue("grdProcessCode"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_DESCRIPTION#", CommonResource.ResourceValue("lblDescription"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_FIXED_COST#", CommonResource.ResourceValue("lblProcessFixedCost"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_COST_PER_HOUR#", CommonResource.ResourceValue("lblCostPerHour"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_COST_PER_UNIT#", CommonResource.ResourceValue("lblCostPerUnit"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_TOTAL_HOURS#", CommonResource.ResourceValue("lblSOTotalHour"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_TOTAL_UNITS#", CommonResource.ResourceValue("lblSOTotalUnit"));
            printTemplet = printTemplet.Replace("#PITEM_HDR_PROCESS_COST#", CommonResource.ResourceValue("prnProcessCost"));


            InvoiceItemProcess oip = new InvoiceItemProcess();
            IDataReader processReader = null;
            string processToPrint = string.Empty;
            try
            {
                processReader = oip.GetProcessItemsReader(dbHelp, ord.InvID);
                bool showProcess = false;
                string processItemRow = string.Empty;
                while (processReader.Read())
                {
                    processItemRow = processItemRowTemplet;
                    if (!showProcess)
                    {
                        showProcess = true;
                    }
                    processItemRow = processItemRow.Replace("#PROCESS_ITM_CODE#", BusinessUtility.GetString(processReader["invItemProcCode"]));
                    processItemRow = processItemRow.Replace("#PROCESS_ITM_DESCRIPTION#", BusinessUtility.GetString(processReader["ProcessDescription"]).Replace(Environment.NewLine, "<br>"));
                    processItemRow = processItemRow.Replace("#PROCESS_ITM_FIXED_COST#", string.Format("{0} {1:F}", ord.InvCurrencyCode, processReader["invItemProcFixedPrice"]));
                    processItemRow = processItemRow.Replace("#PROCESS_ITM_HOUR_COST#", string.Format("{0} {1:F}", ord.InvCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(processReader["invItemProcPricePerHour"]), Globals.CurrentCultureName)));
                    processItemRow = processItemRow.Replace("#PROCESS_ITM_UNIT_COST#", string.Format("{0} {1:F}", ord.InvCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(processReader["invItemProcPricePerUnit"]), Globals.CurrentCultureName)));
                    processItemRow = processItemRow.Replace("#PROCESS_ITM_TOTAL_HOUR#", string.Format("{0}", processReader["invItemProcHours"]));
                    processItemRow = processItemRow.Replace("#PROCESS_ITM_TOTAL_UNIT#", string.Format("{0}", processReader["invItemProcUnits"]));
                    processItemRow = processItemRow.Replace("#PROCESS_ITM_AMOUNT#", string.Format("{0} {1:F}", ord.InvCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(processReader["ProcessCost"]), Globals.CurrentCultureName)));

                    processToPrint += processItemRow;
                }

                printTemplet = printTemplet.Replace("<!--PROCESS_ITEM_LIST_PLACEHOLDER-->", processToPrint);
                if (showProcess)
                {
                    printTemplet = printTemplet.Replace("#PROCESS_ITEM_DISPLAY#", "");
                }
                else
                {
                    printTemplet = printTemplet.Replace("#PROCESS_ITEM_DISPLAY#", "display:none;");
                }
            }
            finally
            {
                if (processReader != null && !processReader.IsClosed)
                {
                    processReader.Close();
                    processReader.Dispose();
                }
            }

            //printTemplet = printTemplet.Replace(processItemRowTemplet, processToPrint);

            TotalSummary lTotal = CalculationHelper.GetInvoiceTotal(dbHelp, ord.InvID);
            //Set Lables for html to be generated
            //lTotal.SetLabel(TotalSummary.LabelKey.BalanceAmount, Resources.Resource.lblBalanceAmount);
            //lTotal.SetLabel(TotalSummary.LabelKey.GrandSubTotal, Resources.Resource.lblSubTotalOrder);
            //lTotal.SetLabel(TotalSummary.LabelKey.GrandTotal, Resources.Resource.lblOrderTotal);
            //lTotal.SetLabel(TotalSummary.LabelKey.ReceivedAmount, Resources.Resource.lblReceivedAmount);
            //lTotal.SetLabel(TotalSummary.LabelKey.SubTotalItems, Resources.Resource.lblSubTotalItems);
            //lTotal.SetLabel(TotalSummary.LabelKey.SubTotalServices, Resources.Resource.lblSubTotalServices);
            //lTotal.SetLabel(TotalSummary.LabelKey.TotalDiscount, Resources.Resource.lblDiscount);
            //lTotal.SetLabel(TotalSummary.LabelKey.TotalTax, Resources.Resource.lblTotalTax);

            printTemplet = printTemplet.Replace("#ITEM_SUB_TOTAL#", GetItemsSubTotalSnipt(totalTemplet, lTotal));
            printTemplet = printTemplet.Replace("#PROCESS_ITEM_SUB_TOTAL#", GetProcessItemsSubTotalSnipt(totalTemplet, lTotal));
            //printTemplet = printTemplet.Replace("#GRAND_TOTAL#", CommonHtml.GetGrandTotalHtml(lTotal).Replace("#HISTORY_LINK#", ""));
            printTemplet = printTemplet.Replace("#GRAND_TOTAL#", string.Format("{0} {1:F}", lTotal.CurrencyCode, BusinessUtility.GetCurrencyString(lTotal.GrandTotal, Globals.CurrentCultureName)));
            //string totalHtml = totalTemplet.Replace("#TOTAL_TITLE#", "Sub Total:").Replace("#TOTAL_AMOUNT#", string.Format("{0} {1:F}", lTotal.CurrencyCode, lTotal.ItemSubTotal + lTotal.ProcessSubTotal));
            //var lstTaxes = lTotal.GetCombiendTaxList();            
            //foreach (var item in lstTaxes)
            //{
            //    totalHtml += totalTemplet.Replace("#TOTAL_TITLE#", string.Format("{0}:", item.TaxCode)).Replace("#TOTAL_AMOUNT#", string.Format("{0} {1:F}", lTotal.CurrencyCode, item.CalculatedPrice));
            //}
            //totalHtml += totalTemplet.Replace("#TOTAL_TITLE#", "Total").Replace("#TOTAL_AMOUNT#", string.Format("{0} {1:F}", lTotal.CurrencyCode, lTotal.ItemSubTotal + lTotal.ProcessSubTotal + lTotal.TotalItemTax + lTotal.TotalProcessTax));
            //totalHtml += totalTemplet.Replace("#TOTAL_TITLE#", "Discount").Replace("#TOTAL_AMOUNT#", string.Format("{0} {1:F}", lTotal.CurrencyCode, lTotal.AdditionalDiscount));
            printTemplet = printTemplet.Replace("<!--#TOTAL_ITEMS#-->", GetTotalSummaryHtml(totalTemplet, lTotal));

            string appName = ConfigurationManager.AppSettings["ApplicationName"];
            if (!string.IsNullOrEmpty(appName) && appName.ToLower() == "sivananda")
            {
                //Hide default document header
                printTemplet = printTemplet.Replace("<!--SHIVANANDA_HEADER_START--><!--", "");
                printTemplet = printTemplet.Replace("--><!--SHIVANANDA_HEADER_END-->", "");
                printTemplet = printTemplet.Replace("#DEFAULT_HEADER_DISPLAY#", "display:none;");
            }
            else
            {
                //show default header
                printTemplet = printTemplet.Replace("#DEFAULT_HEADER_DISPLAY#", "");
            }

            //Apply Barcode prefix & postfix
            printTemplet = printTemplet.Replace("#BARCODE_PREFIX#", AppConfiguration.AppSettings[AppSettingKey.BarcodePrefix]);
            printTemplet = printTemplet.Replace("#BARCODE_POSTFIX#", AppConfiguration.AppSettings[AppSettingKey.BarcodePrefix]);

            //Update Print Counter
            ord.UpdateInvoicePrintCounter(dbHelp, ord.InvID);

            return printTemplet;
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private static string GetItemsSubTotalSnipt(string template, TotalSummary ts)
    {
        //string format = @"<b style=""font-size:13px;"">{0}:  </b>";
        //format += @"{1} {2:F}";

        string totalData = string.Empty;

        //totalData += string.Format(format, "Sub Total",  ts.CurrencyCode, ts.ItemSubTotal);
        //totalData += "<br>";

        //foreach (var item in ts.ItemTaxList)
        //{
        //    totalData += string.Format(format, item.TaxCode, ts.CurrencyCode, item.CalculatedPrice);
        //    totalData += "<br>";
        //}
        //if (ts.ItemTaxList.Count <= 0)
        //{
        //    totalData += string.Format(format, "Taxes", ts.CurrencyCode, 0.0D);
        //    totalData += "<br>";
        //}

        //totalData += string.Format(format, "Total", ts.CurrencyCode, ts.ItemSubTotal + ts.TotalItemTax);

        totalData += template.Replace("#TOTAL_TITLE#", CommonResource.ResourceValue("lblSubTotal")).Replace("#TOTAL_AMOUNT#", string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.ItemSubTotal, Globals.CurrentCultureName)));
        foreach (var item in ts.ItemTaxList)
        {
            totalData += template.Replace("#TOTAL_TITLE#", item.TaxCode).Replace("#TOTAL_AMOUNT#", string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(item.CalculatedPrice, Globals.CurrentCultureName)));
        }
        totalData += template.Replace("#TOTAL_TITLE#", CommonResource.ResourceValue("lblTotal")).Replace("#TOTAL_AMOUNT#", string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.ItemSubTotal + ts.TotalItemTax, Globals.CurrentCultureName)));
        return totalData;
    }

    private static string GetProcessItemsSubTotalSnipt(string template, TotalSummary ts)
    {
        //string format = @"<b style=""font-size:13px;"">{0}:  </b>";
        //format += @"{1} {2:F}";

        string totalData = string.Empty;

        //totalData += string.Format(format, "Sub Total", ts.CurrencyCode, ts.ProcessSubTotal);
        //totalData += "<br>";

        //foreach (var item in ts.ProcessTaxList)
        //{
        //    totalData += string.Format(format, item.TaxCode, ts.CurrencyCode, item.CalculatedPrice);
        //    totalData += "<br>";
        //}
        //if (ts.ItemTaxList.Count <= 0)
        //{
        //    totalData += string.Format(format, "Taxes", ts.CurrencyCode, 0.0D);
        //    totalData += "<br>";
        //}

        //totalData += string.Format(format, "Total", ts.CurrencyCode, ts.ProcessSubTotal + ts.TotalProcessTax);

        totalData += template.Replace("#TOTAL_TITLE#", CommonResource.ResourceValue("lblSubTotal")).Replace("#TOTAL_AMOUNT#", string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.ProcessSubTotal, Globals.CurrentCultureName)));
        foreach (var item in ts.ProcessTaxList)
        {
            totalData += template.Replace("#TOTAL_TITLE#", item.TaxCode).Replace("#TOTAL_AMOUNT#", string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(item.CalculatedPrice, Globals.CurrentCultureName)));
        }
        totalData += template.Replace("#TOTAL_TITLE#", CommonResource.ResourceValue("lblTotal")).Replace("#TOTAL_AMOUNT#", string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.ProcessSubTotal + ts.TotalProcessTax, Globals.CurrentCultureName)));
        return totalData;
    }

    private static string GetTotalSummaryHtml(string template, TotalSummary ts)
    {
        string totalHtml = template.Replace("#TOTAL_TITLE#", CommonResource.ResourceValue("lblSubTotal") + ":").Replace("#TOTAL_AMOUNT#", string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.ItemSubTotal + ts.ProcessSubTotal, Globals.CurrentCultureName)));
        var lstTaxes = ts.GetCombiendTaxList();
        foreach (var item in lstTaxes)
        {
            totalHtml += template.Replace("#TOTAL_TITLE#", string.Format("{0}:", item.TaxCode)).Replace("#TOTAL_AMOUNT#", string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(item.CalculatedPrice, Globals.CurrentCultureName)));
        }
        totalHtml += template.Replace("#TOTAL_TITLE#", CommonResource.ResourceValue("lblTotal")).Replace("#TOTAL_AMOUNT#", string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.ItemSubTotal + ts.ProcessSubTotal + ts.TotalItemTax + ts.TotalProcessTax, Globals.CurrentCultureName)));
        totalHtml += template.Replace("#TOTAL_TITLE#", CommonResource.ResourceValue("grdSaleDiscount")).Replace("#TOTAL_AMOUNT#", string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.AdditionalDiscount, Globals.CurrentCultureName)));

        return totalHtml;
    }

    private static string WarehouseInfoHtml(DbHelper dbHelp, string whsCode)
    {
        SysWarehouses whs = new SysWarehouses();
        whs.PopulateObject(whsCode, dbHelp);

        string comAddr = string.Format(@"<b style=""font-size:13px;"">{0}</b><br />", whs.WarehouseDescription);
        if (!string.IsNullOrEmpty(whs.WarehouseAddressLine1))
        {
            comAddr += whs.WarehouseAddressLine1;
        }
        if (!string.IsNullOrEmpty(whs.WarehouseAddressLine2))
        {
            comAddr += ", " + whs.WarehouseAddressLine2;
        }
        if (!string.IsNullOrEmpty(whs.WarehouseCity))
        {
            comAddr += ", " + whs.WarehouseCity;
        }
        if (!string.IsNullOrEmpty(whs.WarehouseState))
        {
            comAddr += ", " + whs.WarehouseState;
        }
        if (!string.IsNullOrEmpty(whs.WarehouseCountry))
        {
            comAddr += ", " + whs.WarehouseCountry;
        }
        if (!string.IsNullOrEmpty(whs.WarehousePostalCode))
        {
            comAddr += ", " + whs.WarehousePostalCode;
        }
        comAddr += string.Format("<br />{0}: {1} | {2}: {3}", CommonResource.ResourceValue("lblPhone"), whs.WarehousePhone, CommonResource.ResourceValue("Fax"), whs.WarehouseFax);
        comAddr += string.Format("<br />{0}: {1}", CommonResource.ResourceValue("lblEmail"), whs.WarehouseEmailID);
        return comAddr;
    }

    private static string GetCompanyInfoHtml(SysCompanyInfo ci)
    {
        //string comAddr = string.Format(@"<b style=""font-size:13px;"">{0}</b><br />", ci.CompanyName);
        string comAddr = string.Format(@"<b style=""font-size:13px;"">{0}</b>", "");
        if (!string.IsNullOrEmpty(ci.CompanyAddressLine1))
        {
            comAddr += ci.CompanyAddressLine1;
        }
        if (!string.IsNullOrEmpty(ci.CompanyAddressLine2))
        {
            comAddr += ", " + ci.CompanyAddressLine2;
        }
        if (!string.IsNullOrEmpty(ci.CompanyCity))
        {
            comAddr += ", " + ci.CompanyCity;
        }
        if (!string.IsNullOrEmpty(ci.CompanyState))
        {
            comAddr += ", " + ci.CompanyState;
        }
        if (!string.IsNullOrEmpty(ci.CompanyCountry))
        {
            comAddr += ", " + ci.CompanyCountry;
        }
        if (!string.IsNullOrEmpty(ci.CompanyPostalCode))
        {
            comAddr += ", " + ci.CompanyPostalCode;
        }
        comAddr += string.Format("<br />{0}: {1} | {2}: {3}", CommonResource.ResourceValue("lblPhone"), ci.CompanyPhoneno, CommonResource.ResourceValue("Fax"), ci.CompanyFax);
        comAddr += string.Format("<br />{0}: {1}", CommonResource.ResourceValue("lblEmail"), ci.CompanyEmail);
        return comAddr;
    }

    public static string GetAddress(string addrLine1, string addrLine2, string addrLine3, string city, string state, string country, string postalCode)
    {
        string addr = string.Empty;
        if (!string.IsNullOrEmpty(addrLine1))
        {
            addr += addrLine1;
        }
        if (!string.IsNullOrEmpty(addrLine2))
        {
            addr += addr.Length > 0 ? "<br>" + addrLine2 : addrLine2;
        }
        if (!string.IsNullOrEmpty(addrLine3))
        {
            addr += addr.Length > 0 ? "<br>" + addrLine3 : addrLine3;
        }
        if (!string.IsNullOrEmpty(city))
        {
            addr += addr.Length > 0 ? "<br>" + city : city;
        }
        if (!string.IsNullOrEmpty(state))
        {
            addr += addr.Length > 0 ? ", " + state : state;
        }
        if (!string.IsNullOrEmpty(postalCode))
        {
            addr += addr.Length > 0 ? "<br>" + postalCode : postalCode;
        }
        if (!string.IsNullOrEmpty(country))
        {
            addr += addr.Length > 0 ? ", " + country : country;
        }

        return addr.Trim().TrimEnd(',');
    }
    public static string GetAddress(Addresses addr)
    {
        return GetAddress(addr.AddressLine1, addr.AddressLine2, addr.AddressLine3, addr.AddressCity, addr.AddressState, addr.AddressCountry, addr.AddressPostalCode);
    }
    public static string GetAddress(SysCompanyInfo cinfo)
    {
        return GetAddress(cinfo.CompanyAddressLine1, cinfo.CompanyAddressLine2, string.Empty, cinfo.CompanyCity, cinfo.CompanyState, cinfo.CompanyCountry, cinfo.CompanyPostalCode);
    }
    public static string GetAddress(SysWarehouses whs)
    {
        return GetAddress(whs.WarehouseAddressLine1, whs.WarehouseAddressLine2, string.Empty, whs.WarehouseCity, whs.WarehouseState, whs.WarehouseCountry, whs.WarehousePostalCode);
    }

    public static string GetPOSResceiptHtmlSnipt(int invID, string sRegCode, string sWhrCode, string lngID)
    {
        DbHelper dbHelp = new DbHelper(true);
        int custID = 0;
        try
        {
            string cultureName = "en-CA";
            string sCurrencyCode = "";

            if (lngID == "fr")
            {
                cultureName = "fr-CA";

            }
            else
            {
                cultureName = "en-CA";
            }

            string printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/PrintTemplates/POSReceipt.htm"));
            //string printTemplet = File.ReadAllText(ServerPath + "PrintTemplates/POSReceipt.htm");
            printTemplet = printTemplet.Replace("#PRINTDATE#", CommonResource.ResourceValue("POSDate", cultureName) + ": " + DateTime.Now.ToString("MM/dd/yyyy"));
            printTemplet = printTemplet.Replace("#PRINTTIME#", CommonResource.ResourceValue("POSTime", cultureName) + ": " + DateTime.Now.ToString("HH:mm:ss"));

            SysCompanyInfo objCompany = new SysCompanyInfo();
            SysWarehouses objWhrHouse = new SysWarehouses();
            objWhrHouse.PopulateObject(sWhrCode);
            objCompany.PopulateObject(BusinessUtility.GetInt(objWhrHouse.WarehouseCompanyID));
            printTemplet = printTemplet.Replace("#COMPNAME#", objCompany.CompanyName);
            //printTemplet = printTemplet.Replace("#COMPPHONE#", objCompany.CompanyPOPhone);
            printTemplet = printTemplet.Replace("#COMPPHONE#", "");
            SysRegister objRegister = new SysRegister();
            objRegister.PopulateObject(BusinessUtility.GetString(sRegCode));

            string sCompAddr = "";
            sCompAddr = objRegister.SysRegAddress + "<BR/>" + objRegister.SysRegCity + " " + objRegister.SysRegState + "<BR/>" + objCompany.CompanyCountry + " " + objRegister.SysRegPostalCode;
            if (BusinessUtility.GetString(objRegister.SysRegPhone) != "")
            {
                sCompAddr = sCompAddr + "</br>PH.- " + BusinessUtility.GetString(objRegister.SysRegPhone);
            }
            printTemplet = printTemplet.Replace("#COMPADD#", sCompAddr);

            Invoice objInvoice = new Invoice();
            objInvoice.GetInvoiceCustSaleRepName(BusinessUtility.GetInt(invID));
            printTemplet = printTemplet.Replace("#CUSTNAME#", CommonResource.ResourceValue("lblCustName", cultureName) + ": " + BusinessUtility.GetString(objInvoice.InvCustName));
            printTemplet = printTemplet.Replace("#SALEREP#", CommonResource.ResourceValue("lblPrintSalesRep", cultureName) + ": " + BusinessUtility.GetString(objInvoice.InvSaleRep));
            printTemplet = printTemplet.Replace("#TRANSACTIONID#", BusinessUtility.GetString(invID.ToString("D6")));
            string logoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/lib/image/") + "Logoprint.png";
            printTemplet = printTemplet.Replace("#COM_LOGO#", logoUrl);

            DataTable dt = objInvoice.GetPOSReceiptDetail(invID);
            #region AddInvoiceProductRows

            StringBuilder sbPrdRows = new StringBuilder();
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.HorizontalAlign = HorizontalAlign.Right;

            for (int nM = 0; nM != 5; nM++)
            {
                TableHeaderCell headerTableCell = new TableHeaderCell();
                headerTableCell.CssClass = "lblPOSPrint";
                headerTableCell.Font.Size = 7;
                if (nM == 0)
                {
                    headerTableCell.Text = CommonResource.ResourceValue("msgPrd", cultureName);
                    headerTableCell.HorizontalAlign = HorizontalAlign.Left;
                    headerTableCell.Width = 85;
                }
                else if (nM == 1)
                {
                    headerTableCell.Text = CommonResource.ResourceValue("msgQty", cultureName);
                    headerTableCell.Width = 32;
                }
                else if (nM == 2)
                {
                    headerTableCell.Text = CommonResource.ResourceValue("msgPrice", cultureName);
                    headerTableCell.Width = 43;
                }
                else if (nM == 3)
                {
                    headerTableCell.Text = CommonResource.ResourceValue("lblPosGrdDisc", cultureName);
                    headerTableCell.Width = 70;
                }
                else if (nM == 4)
                {
                    headerTableCell.Text = CommonResource.ResourceValue("msgTotalPrice", cultureName);
                    headerTableCell.Width = 70;
                }
                headerRow.Cells.Add(headerTableCell);
            }

            StringWriter sw = new StringWriter();
            headerRow.RenderControl(new HtmlTextWriter(sw));
            sbPrdRows.Append(sw.ToString());
            double dblSubTot = 0.0;

            string strTotal = "";
            string strSubTotal = "";
            string strReceived = "";
            string strReturn = "";
            string strDiscount = "0";

            StringBuilder sbPrdDtl = new StringBuilder();
            foreach (DataRow drObj in dt.Rows)
            {
                custID = BusinessUtility.GetInt(drObj["invCustID"]);
                printTemplet = printTemplet.Replace("#REGMESSAGE#", BusinessUtility.GetString(drObj["sysRegMessage"]));
                TableRow tempRow = new TableRow();
                for (int nM = 0; nM != 5; nM++)
                {
                    TableCell tempCell = new TableCell();
                    tempCell.CssClass = "lblPOSPrint";
                    tempCell.Font.Size = 7;

                    if (nM == 0)
                    {

                        string sPName = "";
                        if (BusinessUtility.GetString(drObj["prdIsNonStandard"]) == "1")
                        {
                            sPName = BusinessUtility.GetString(drObj["ProductName"]) + "<Br />" + BusinessUtility.GetString(drObj["prdUPCCode"]);
                        }
                        else
                        {
                            sPName = BusinessUtility.GetString(drObj["ProductName"]) + "<Br />" + BusinessUtility.GetString(drObj["prdUPCCode"]);
                        }
                        //tempCell.Text = BusinessUtility.GetString(drObj["ProductName"]) + " (" + BusinessUtility.GetString(drObj["Color"]) + ", " + BusinessUtility.GetString(drObj["Size"]) + ") " + "<Br />" + BusinessUtility.GetString(drObj["prdUPCCode"]);
                        tempCell.Text = sPName;
                        tempCell.HorizontalAlign = HorizontalAlign.Left;
                        tempCell.Width = 85;
                    }
                    else if (nM == 1)
                    {
                        tempCell.Text = BusinessUtility.GetString(drObj["posPrdQty"]);
                        tempCell.HorizontalAlign = HorizontalAlign.Right;
                        tempCell.Width = 32;
                    }
                    else if (nM == 2)
                    {
                        tempCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(drObj["posPrdUnitPrice"]), "", cultureName);
                        tempCell.HorizontalAlign = HorizontalAlign.Right;
                        tempCell.Width = 43;
                    }
                    else if (nM == 3)
                    {
                        tempCell.Text = BusinessUtility.GetString(BusinessUtility.GetInt(drObj["posPercentDiscount"]));
                        tempCell.HorizontalAlign = HorizontalAlign.Right;
                        tempCell.Width = 43;
                    }
                    else if (nM == 4)
                    {
                        double pDisc = BusinessUtility.GetDouble(drObj["posPercentDiscount"]);
                        double pAmt = BusinessUtility.GetDouble(drObj["posPrdPrice"]);
                        pAmt = pAmt - (pAmt * pDisc / 100);
                        dblSubTot += pAmt;
                        tempCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(pAmt), "", cultureName);
                        tempCell.HorizontalAlign = HorizontalAlign.Right;
                        tempCell.Width = 70;
                    }
                    tempRow.Cells.Add(tempCell);


                    TotalSummary objCalcHlpr = new TotalSummary();
                    objCalcHlpr.InitInvoiceTotal(BusinessUtility.GetInt(invID));

                    double taxTotal = 0.0;
                    foreach (TaxItem taxDtl in objCalcHlpr.ItemTaxList)
                    {
                        taxTotal += BusinessUtility.GetDouble(taxDtl.CalculatedPrice);
                    }

                    strSubTotal = string.Format("{0:F}", Convert.ToDouble(dblSubTot));
                    strTotal = string.Format("{0:F}", Convert.ToDouble((BusinessUtility.GetDouble(strSubTotal) + BusinessUtility.GetDouble(taxTotal)).ToString()));
                    if (!string.IsNullOrEmpty(Convert.ToString(drObj["posCasReceived"])))
                    {
                        strReceived = string.Format("{0:F}", Convert.ToDouble(drObj["posCasReceived"].ToString()));
                    }
                    else
                    {
                        strReceived = "0.00";
                    }

                    strReturn = string.Format("{0:F}", Convert.ToDouble((BusinessUtility.GetDouble(strTotal) - BusinessUtility.GetDouble(strReceived)).ToString()));
                    //strReturn = CurrencyFormat.GetReplaceCurrencySymbol( CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble((BusinessUtility.GetDouble(strTotal) - BusinessUtility.GetDouble(strReceived)).ToString())));
                    strSubTotal = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(strSubTotal), "", cultureName));
                    strDiscount = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(strDiscount), "", cultureName));
                    strTotal = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(strTotal), "", cultureName));
                    strReceived = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(strReceived), "", cultureName));
                    strReturn = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(strReturn), "", cultureName));
                    sCurrencyCode = BusinessUtility.GetString(drObj["invCurrencyCode"]);
                }

                StringWriter sProwDtl = new StringWriter();
                tempRow.RenderControl(new HtmlTextWriter(sProwDtl));
                sbPrdDtl.Append(sProwDtl.ToString());
            }

            sbPrdRows.Append(sbPrdDtl.ToString());
            printTemplet = printTemplet.Replace("#ITEMROWS#", sbPrdRows.ToString());
            #endregion

            #region SetTaxes
            TotalSummary objCalcHlprNew = new TotalSummary();
            objCalcHlprNew.InitInvoiceTotal(BusinessUtility.GetInt(invID));
            StringBuilder sbTaxRows = new StringBuilder();
            TableRow RowTax = new TableRow();
            bool IsRow = false;
            foreach (TaxItem taxDtl in objCalcHlprNew.ItemTaxList)
            {
                IsRow = true;
                if ((taxDtl.CalculatedPrice > 0))
                {
                    RowTax = new TableRow();
                    TableCell tempTaxDescCell = new TableCell();
                    tempTaxDescCell.CssClass = "lblPOSPrint";
                    tempTaxDescCell.Font.Size = 7;
                    tempTaxDescCell.Text = taxDtl.TaxCode + "  --";
                    tempTaxDescCell.HorizontalAlign = HorizontalAlign.Right;
                    tempTaxDescCell.Font.Bold = true;
                    tempTaxDescCell.Width = 158;
                    RowTax.Cells.Add(tempTaxDescCell);

                    TableCell tempTaxValueCell = new TableCell();
                    tempTaxValueCell.CssClass = "lblPOSPrint";
                    tempTaxValueCell.Font.Size = 7;
                    tempTaxValueCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(taxDtl.CalculatedPrice), "", cultureName);
                    tempTaxValueCell.HorizontalAlign = HorizontalAlign.Right;
                    RowTax.Cells.Add(tempTaxValueCell);

                    //tblTax.Rows.Add(RowTax);

                    StringWriter sTaxRows = new StringWriter();
                    RowTax.RenderControl(new HtmlTextWriter(sTaxRows));
                    sbTaxRows.Append(sTaxRows.ToString());
                }
            }
            printTemplet = printTemplet.Replace("#TAXROWS#", sbTaxRows.ToString());
            #endregion

            #region SetInvoiceTotal
            string strTransType = "S";
            StringBuilder sbInvTotalRows = new StringBuilder();

            AccountReceivable objAccountReceivable = new AccountReceivable();
            DataTable dtAcountReceivable = new DataTable();
            dtAcountReceivable = objAccountReceivable.GetInvoicePartialPaymentList(BusinessUtility.GetInt(invID), lngID);
            bool IsAddPartialDesc = false;
            CustomerCredit objCustCrdt = new CustomerCredit();
            double dblAvlCrdt = BusinessUtility.GetDouble(objCustCrdt.GetAvailableCredit(custID));

            for (int nR = 0; nR <= 4; nR++)
            {
                TableRow RowAmount = new TableRow();
                for (int nM = 0; nM <= 1; nM++)
                {
                    TableCell tempCell = new TableCell();
                    tempCell.CssClass = "lblPOSPrint";
                    tempCell.Font.Size = 7;
                    if (nR == 0)
                    {
                        if (nM == 0)
                        {
                            tempCell.Text = CommonResource.ResourceValue("msgTotal", cultureName) + "  --";
                            tempCell.HorizontalAlign = HorizontalAlign.Right;
                            tempCell.Width = 165;
                            tempCell.Font.Bold = true;
                        }
                        else if (nM == 1)
                        {
                            tempCell.Text = strTotal;
                            tempCell.HorizontalAlign = HorizontalAlign.Right;
                            tempCell.Width = 65;
                        }
                    }
                    else if (nR == 2)
                    {
                        if (((dtAcountReceivable.Rows.Count > 0) & IsAddPartialDesc == false))
                        {
                            IsAddPartialDesc = true;
                            foreach (DataRow dRow in dtAcountReceivable.Rows)
                            {
                                if (!string.IsNullOrEmpty(BusinessUtility.GetString(dRow["Mode"])))
                                {
                                    TableRow dProw = new TableRow();
                                    TableCell tempCellp = new TableCell();
                                    tempCellp.CssClass = "lblPOSPrint";
                                    tempCellp.Font.Size = 7;
                                    if (Convert.ToDouble(dRow["ARAmtRcvd"]) < 0)
                                    {
                                        tempCellp.Text = Resources.Resource.lblPORefund + " " + BusinessUtility.GetString(dRow["Mode"]) + "  --";
                                    }
                                    else
                                    {
                                        tempCellp.Text = BusinessUtility.GetString(dRow["Mode"]) + "  --";
                                    }
                                    tempCellp.HorizontalAlign = HorizontalAlign.Right;
                                    tempCellp.Width = 165;
                                    tempCellp.Font.Bold = true;

                                    TableCell tempCellv = new TableCell();
                                    tempCellv.CssClass = "lblPOSPrint";
                                    tempCellv.Font.Size = 7;
                                    tempCellv.Text = string.Format("{0:F}", Convert.ToDouble(dRow["ARAmtRcvd"].ToString()));
                                    tempCellv.HorizontalAlign = HorizontalAlign.Right;
                                    tempCellv.Width = 65;
                                    dProw.Cells.Add(tempCellp);
                                    dProw.Cells.Add(tempCellv);
                                    StringWriter sInvPartialPayment = new StringWriter();
                                    dProw.RenderControl(new HtmlTextWriter(sInvPartialPayment));
                                    sbInvTotalRows.Append(sInvPartialPayment.ToString());
                                }
                            }
                        }

                    }
                    else if (nR == 3)
                    {
                        if (nM == 0)
                        {
                            if (strTransType == "S")
                            {
                                tempCell.Text = CommonResource.ResourceValue("msgBalance", cultureName) + "  --";
                            }
                            else
                            {
                                tempCell.Text = CommonResource.ResourceValue("lblPOSRefund", cultureName) + "  --";
                            }
                            tempCell.HorizontalAlign = HorizontalAlign.Right;
                            tempCell.Font.Bold = true;
                        }
                        else if (nM == 1)
                        {
                            tempCell.Text = strReturn;
                            tempCell.HorizontalAlign = HorizontalAlign.Right;
                        }
                    }
                    else if (nR == 4)
                    {
                        if (BusinessUtility.GetDouble(dblAvlCrdt) > 0)
                        {
                            if (nM == 0)
                            {
                                if (strTransType == "S")
                                {
                                    tempCell.Text = CommonResource.ResourceValue("lblInstoreCreditAvailable", cultureName) + "  --";
                                }
                                tempCell.HorizontalAlign = HorizontalAlign.Right;
                                tempCell.Font.Bold = true;
                            }
                            else if (nM == 1)
                            {
                                tempCell.Text = string.Format("{0:F}", dblAvlCrdt);
                                tempCell.HorizontalAlign = HorizontalAlign.Right;
                            }
                        }
                    }
                    else if (nR == 1)
                    {
                        if (BusinessUtility.GetDouble(strDiscount) > 0)
                        {
                            if (nM == 0)
                            {
                                if (strTransType == "S")
                                {
                                    tempCell.Text = "Discount(%)" + "  --";
                                }
                                tempCell.HorizontalAlign = HorizontalAlign.Right;
                                tempCell.Font.Bold = true;
                            }
                            else if (nM == 1)
                            {
                                tempCell.Text = strDiscount;
                                tempCell.HorizontalAlign = HorizontalAlign.Right;
                            }
                        }
                    }
                    RowAmount.Cells.Add(tempCell);
                }
                StringWriter sInvTotal = new StringWriter();
                RowAmount.RenderControl(new HtmlTextWriter(sInvTotal));
                sbInvTotalRows.Append(sInvTotal.ToString());
                //tblAmount.Rows.Add(RowAmount);
            }

            printTemplet = printTemplet.Replace("#AMOUNTROWS#", sbInvTotalRows.ToString());
            #endregion

            #region SetSubTotal
            StringBuilder sbInvSubTotal = new StringBuilder();
            TableRow RowSub = new TableRow();
            for (int nM = 0; nM <= 1; nM++)
            {
                TableCell tempCell = new TableCell();
                tempCell.CssClass = "lblPOSPrint";
                tempCell.Font.Size = 7;
                if (nM == 0)
                {
                    tempCell.Text = CommonResource.ResourceValue("msgSubTotal", cultureName) + "  --";
                    tempCell.HorizontalAlign = HorizontalAlign.Right;
                    tempCell.Width = 165;
                    tempCell.Font.Bold = true;
                }
                else if (nM == 1)
                {
                    tempCell.Text = strSubTotal;
                    tempCell.HorizontalAlign = HorizontalAlign.Right;
                    tempCell.Width = 65;
                }
                RowSub.Cells.Add(tempCell);
            }
            //tblsubTotal.Rows.Add(RowSub);
            StringWriter sInvSubTotal = new StringWriter();
            RowSub.RenderControl(new HtmlTextWriter(sInvSubTotal));
            sbInvSubTotal.Append(sInvSubTotal.ToString());
            printTemplet = printTemplet.Replace("#TOTALSUBTOTALROWS#", sInvSubTotal.ToString());
            #endregion

            return printTemplet;
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    public static string GetPOSGiftResceiptHtmlSnipt(int invID, string sRegCode, string sWhrCode, string lngID)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            string cultureName = "en-CA";
            if (lngID == "fr")
            {
                cultureName = "fr-CA";

            }
            else
            {
                cultureName = "en-CA";
            }

            string printTemplet = File.ReadAllText(HttpContext.Current.Server.MapPath("~/PrintTemplates/POSGiftReceipt.htm"));
            printTemplet = printTemplet.Replace("#PRINTDATE#", CommonResource.ResourceValue("POSDate", cultureName) + ": " + DateTime.Now.ToString("MM/dd/yyyy"));
            printTemplet = printTemplet.Replace("#PRINTTIME#", CommonResource.ResourceValue("POSTime", cultureName) + ": " + DateTime.Now.ToString("HH:mm:ss"));

            SysCompanyInfo objCompany = new SysCompanyInfo();
            SysWarehouses objWhrHouse = new SysWarehouses();
            objWhrHouse.PopulateObject(sWhrCode);
            objCompany.PopulateObject(BusinessUtility.GetInt(objWhrHouse.WarehouseCompanyID));
            printTemplet = printTemplet.Replace("#COMPNAME#", objCompany.CompanyName);
            //printTemplet = printTemplet.Replace("#COMPPHONE#", objCompany.CompanyPOPhone);
            printTemplet = printTemplet.Replace("#COMPPHONE#", "");
            SysRegister objRegister = new SysRegister();
            objRegister.PopulateObject(BusinessUtility.GetString(sRegCode));
            //printTemplet = printTemplet.Replace("#COMPADD#", objRegister.SysRegAddress + "<BR/>" + objRegister.SysRegCity + " " + objRegister.SysRegState + "<BR/>" + objCompany.CompanyCountry + " " + objRegister.SysRegPostalCode);
            string sCompAddr = "";
            sCompAddr = objRegister.SysRegAddress + "<BR/>" + objRegister.SysRegCity + " " + objRegister.SysRegState + "<BR/>" + objCompany.CompanyCountry + " " + objRegister.SysRegPostalCode;
            if (BusinessUtility.GetString(objRegister.SysRegPhone) != "")
            {
                sCompAddr = sCompAddr + "</br>PH.- " + BusinessUtility.GetString(objRegister.SysRegPhone);
            }
            printTemplet = printTemplet.Replace("#COMPADD#", sCompAddr);

            Invoice objInvoice = new Invoice();
            objInvoice.GetInvoiceCustSaleRepName(BusinessUtility.GetInt(invID));
            printTemplet = printTemplet.Replace("#CUSTNAME#", CommonResource.ResourceValue("lblCustName", cultureName) + ": " + BusinessUtility.GetString(objInvoice.InvCustName));
            printTemplet = printTemplet.Replace("#SALEREP#", CommonResource.ResourceValue("lblPrintSalesRep", cultureName) + ": " + BusinessUtility.GetString(objInvoice.InvSaleRep));
            printTemplet = printTemplet.Replace("#TRANSACTIONID#", BusinessUtility.GetString(invID.ToString("D6")));
            string logoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/lib/image/") + "Logoprint.png";
            printTemplet = printTemplet.Replace("#COM_LOGO#", logoUrl);

            DataTable dt = objInvoice.GetPOSReceiptDetail(invID);
            #region AddInvoiceProductRows

            StringBuilder sbPrdRows = new StringBuilder();
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.HorizontalAlign = HorizontalAlign.Right;

            for (int nM = 0; nM != 2; nM++)
            {
                TableHeaderCell headerTableCell = new TableHeaderCell();
                headerTableCell.CssClass = "lblPOSPrint";
                headerTableCell.Font.Size = 7;
                if (nM == 0)
                {
                    headerTableCell.Text = CommonResource.ResourceValue("msgPrd", cultureName);
                    headerTableCell.HorizontalAlign = HorizontalAlign.Left;
                    headerTableCell.Width = 85;
                }
                else if (nM == 1)
                {
                    headerTableCell.Text = CommonResource.ResourceValue("msgQty", cultureName);
                    headerTableCell.Width = 32;
                }
                headerRow.Cells.Add(headerTableCell);
            }

            StringWriter sw = new StringWriter();
            headerRow.RenderControl(new HtmlTextWriter(sw));
            sbPrdRows.Append(sw.ToString());

            StringBuilder sbPrdDtl = new StringBuilder();
            foreach (DataRow drObj in dt.Rows)
            {
                printTemplet = printTemplet.Replace("#REGMESSAGE#", BusinessUtility.GetString(drObj["sysRegMessage"]));
                TableRow tempRow = new TableRow();
                for (int nM = 0; nM != 2; nM++)
                {
                    TableCell tempCell = new TableCell();
                    tempCell.CssClass = "lblPOSPrint";
                    tempCell.Font.Size = 7;

                    if (nM == 0)
                    {

                        tempCell.Text = BusinessUtility.GetString(drObj["ProductName"]) + "<Br />" + BusinessUtility.GetString(drObj["prdUPCCode"]);
                        tempCell.HorizontalAlign = HorizontalAlign.Left;
                        tempCell.Width = 85;
                    }
                    else if (nM == 1)
                    {
                        tempCell.Text = BusinessUtility.GetString(drObj["posPrdQty"]);
                        tempCell.HorizontalAlign = HorizontalAlign.Right;
                        tempCell.Width = 32;
                    }
                    tempRow.Cells.Add(tempCell);
                }

                StringWriter sProwDtl = new StringWriter();
                tempRow.RenderControl(new HtmlTextWriter(sProwDtl));
                sbPrdDtl.Append(sProwDtl.ToString());
            }

            sbPrdRows.Append(sbPrdDtl.ToString());
            printTemplet = printTemplet.Replace("#ITEMROWS#", sbPrdRows.ToString());
            #endregion
            return printTemplet;
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    public static string GetReturnReceiptHtmlSnipt(string lngID) // , List<ReturnCart> returnCart
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            string cultureName = "en-CA";
            string sCurrencyCode = "";
            string printTemplet = "";
            if (lngID == "fr")
            {
                cultureName = "fr-CA";
            }
            else
            {
                cultureName = "en-CA";
            }

            if (HttpContext.Current.Session["RETURN_CART"] != null)
            {
                printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/PrintTemplates/ReturnReceipt.htm"));
                List<ReturnCart> returnCart = (List<ReturnCart>)HttpContext.Current.Session["RETURN_CART"];
                //dynamic itemtoUpdate = returnCart.Where(i => i.ProductToReturnQty > 0);
                var result = returnCart.OfType<ReturnCart>().Where(s => s.ProductToReturnQty > 0);
                var itemlist = result.Cast<ReturnCart>().ToList();

                if (itemlist.Count > 0)
                {
                    var item = itemlist[0];
                    //ReturnCart item = result[0];

                    // Get Cust Name And Sales Rep Name
                    if (BusinessUtility.GetInt(item.InvoiceID) > 0)
                    {
                        Invoice objOrd = new Invoice();
                        objOrd.PopulateObject(BusinessUtility.GetInt(item.InvoiceID));

                        printTemplet = printTemplet.Replace("#PRINTDATE#", CommonResource.ResourceValue("POSDate", cultureName) + ": " + DateTime.Now.ToString("MM/dd/yyyy"));
                        printTemplet = printTemplet.Replace("#PRINTTIME#", CommonResource.ResourceValue("POSTime", cultureName) + ": " + DateTime.Now.ToString("HH:mm:ss"));

                        SysCompanyInfo objCompany = new SysCompanyInfo();
                        SysWarehouses objWhrHouse = new SysWarehouses();
                        objWhrHouse.PopulateObject(objOrd.InvShpWhsCode);
                        objCompany.PopulateObject(BusinessUtility.GetInt(objWhrHouse.WarehouseCompanyID));
                        printTemplet = printTemplet.Replace("#COMPNAME#", objCompany.CompanyName);
                        //printTemplet = printTemplet.Replace("#COMPPHONE#", objCompany.CompanyPOPhone);
                        printTemplet = printTemplet.Replace("#COMPPHONE#", "");

                        SysRegister objRegister = new SysRegister();
                        objRegister.PopulateObject(BusinessUtility.GetString(objOrd.InvRegCode));
                        //printTemplet = printTemplet.Replace("#COMPADD#", objRegister.SysRegAddress + "<BR/>" + objRegister.SysRegCity + " " + objRegister.SysRegState + "<BR/>" + objCompany.CompanyCountry + " " + objRegister.SysRegPostalCode);
                        string sCompAddr = "";
                        sCompAddr = objRegister.SysRegAddress + "<BR/>" + objRegister.SysRegCity + " " + objRegister.SysRegState + "<BR/>" + objCompany.CompanyCountry + " " + objRegister.SysRegPostalCode;
                        if (BusinessUtility.GetString(objRegister.SysRegPhone) != "")
                        {
                            sCompAddr = sCompAddr + "</br>PH.- " + BusinessUtility.GetString(objRegister.SysRegPhone);
                        }
                        printTemplet = printTemplet.Replace("#COMPADD#", sCompAddr);


                        Invoice objInvoice = new Invoice();
                        objInvoice.GetInvoiceCustSaleRepName(BusinessUtility.GetInt(item.InvoiceID));
                        printTemplet = printTemplet.Replace("#CUSTNAME#", CommonResource.ResourceValue("lblCustName", cultureName) + ": " + BusinessUtility.GetString(objInvoice.InvCustName));
                        printTemplet = printTemplet.Replace("#SALEREP#", CommonResource.ResourceValue("lblPrintSalesRep", cultureName) + ": " + BusinessUtility.GetString(objInvoice.InvSaleRep));

                        //   printTemplet = printTemplet.Replace("#TRANSACTIONID#", BusinessUtility.GetString(item.InvoiceID.ToString("D6")));
                        if (BusinessUtility.GetString(HttpContext.Current.Session["AccountReceivableID"]) != "")
                        {
                            printTemplet = printTemplet.Replace("#TRANSACTIONID#", BusinessUtility.GetString(BusinessUtility.GetInt(HttpContext.Current.Session["AccountReceivableID"]).ToString("D6")));
                        }
                        else
                        {
                            printTemplet = printTemplet.Replace("#TRANSACTIONID#", BusinessUtility.GetString(item.InvoiceID.ToString("D6")));
                        }
                        string logoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/lib/image/") + "Logoprint.png";
                        printTemplet = printTemplet.Replace("#COM_LOGO#", logoUrl);
                    }
                    else
                    {
                        Orders objOrd = new Orders();
                        objOrd.PopulateObject(BusinessUtility.GetInt(item.OrderID));

                        printTemplet = printTemplet.Replace("#PRINTDATE#", CommonResource.ResourceValue("POSDate", cultureName) + ": " + DateTime.Now.ToString("MM/dd/yyyy"));
                        printTemplet = printTemplet.Replace("#PRINTTIME#", CommonResource.ResourceValue("POSTime", cultureName) + ": " + DateTime.Now.ToString("HH:mm:ss"));

                        SysCompanyInfo objCompany = new SysCompanyInfo();
                        SysWarehouses objWhrHouse = new SysWarehouses();
                        objWhrHouse.PopulateObject(objOrd.OrdShpWhsCode);
                        objCompany.PopulateObject(BusinessUtility.GetInt(objWhrHouse.WarehouseCompanyID));
                        printTemplet = printTemplet.Replace("#COMPNAME#", objCompany.CompanyName);
                        //printTemplet = printTemplet.Replace("#COMPPHONE#", objCompany.CompanyPOPhone);
                        printTemplet = printTemplet.Replace("#COMPPHONE#", "");

                        SysRegister objRegister = new SysRegister();
                        objRegister.PopulateObject(BusinessUtility.GetString(objOrd.OrdRegCode));
                        //printTemplet = printTemplet.Replace("#COMPADD#", objRegister.SysRegAddress + "<BR/>" + objRegister.SysRegCity + " " + objRegister.SysRegState + "<BR/>" + objCompany.CompanyCountry + " " + objRegister.SysRegPostalCode);
                        string sCompAddr = "";
                        sCompAddr = objRegister.SysRegAddress + "<BR/>" + objRegister.SysRegCity + " " + objRegister.SysRegState + "<BR/>" + objCompany.CompanyCountry + " " + objRegister.SysRegPostalCode;
                        if (BusinessUtility.GetString(objRegister.SysRegPhone) != "")
                        {
                            sCompAddr = sCompAddr + "</br>PH.- " + BusinessUtility.GetString(objRegister.SysRegPhone);
                        }
                        printTemplet = printTemplet.Replace("#COMPADD#", sCompAddr);

                        Orders objInvoice = new Orders();
                        objInvoice.GetOrderCustSaleRepName(BusinessUtility.GetInt(item.OrderID));
                        printTemplet = printTemplet.Replace("#CUSTNAME#", CommonResource.ResourceValue("lblCustName", cultureName) + ": " + BusinessUtility.GetString(objInvoice.OrdCustName));
                        printTemplet = printTemplet.Replace("#SALEREP#", CommonResource.ResourceValue("lblPrintSalesRep", cultureName) + ": " + BusinessUtility.GetString(objInvoice.OrdCustSaleName));
                        //printTemplet = printTemplet.Replace("#TRANSACTIONID#", BusinessUtility.GetString(item.InvoiceID.ToString("D6")));

                        if (BusinessUtility.GetString(HttpContext.Current.Session["AccountReceivableID"]) != "")
                        {
                            printTemplet = printTemplet.Replace("#TRANSACTIONID#", BusinessUtility.GetString(BusinessUtility.GetInt(HttpContext.Current.Session["AccountReceivableID"]).ToString("D6")));
                        }
                        else
                        {
                            printTemplet = printTemplet.Replace("#TRANSACTIONID#", BusinessUtility.GetString(item.InvoiceID.ToString("D6")));
                        }
                        string logoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/lib/image/") + "Logoprint.png";
                        printTemplet = printTemplet.Replace("#COM_LOGO#", logoUrl);
                    }

                    //DataTable dt = objInvoice.GetPOSReceiptDetail(invID);
                    #region AddInvoiceProductRows

                    StringBuilder sbPrdRows = new StringBuilder();
                    TableHeaderRow headerRow = new TableHeaderRow();
                    headerRow.HorizontalAlign = HorizontalAlign.Right;

                    for (int nM = 0; nM != 4; nM++)
                    {
                        TableHeaderCell headerTableCell = new TableHeaderCell();
                        headerTableCell.CssClass = "lblPOSPrint";
                        headerTableCell.Font.Size = 7;
                        if (nM == 0)
                        {
                            headerTableCell.Text = CommonResource.ResourceValue("msgPrd", cultureName);
                            headerTableCell.HorizontalAlign = HorizontalAlign.Left;
                            headerTableCell.Width = 85;
                        }
                        else if (nM == 1)
                        {
                            headerTableCell.Text = CommonResource.ResourceValue("msgQty", cultureName);
                            headerTableCell.Width = 32;
                        }
                        else if (nM == 2)
                        {
                            headerTableCell.Text = CommonResource.ResourceValue("msgPrice", cultureName);
                            headerTableCell.Width = 43;
                        }
                        else if (nM == 3)
                        {
                            headerTableCell.Text = CommonResource.ResourceValue("msgTotalPrice", cultureName);
                            headerTableCell.Width = 70;
                        }
                        headerRow.Cells.Add(headerTableCell);
                    }

                    StringWriter sw = new StringWriter();
                    headerRow.RenderControl(new HtmlTextWriter(sw));
                    sbPrdRows.Append(sw.ToString());
                    double dblSubTot = 0.0;
                    string strSubTotal = "";

                    StringBuilder sbPrdDtl = new StringBuilder();
                    //foreach (DataRow drObj in dt.Rows)
                    foreach (ReturnCart vITem in itemlist)
                    {
                        printTemplet = printTemplet.Replace("#REGMESSAGE#", "");
                        TableRow tempRow = new TableRow();
                        for (int nM = 0; nM != 4; nM++)
                        {
                            TableCell tempCell = new TableCell();
                            tempCell.CssClass = "lblPOSPrint";
                            tempCell.Font.Size = 7;

                            if (nM == 0)
                            {

                                tempCell.Text = BusinessUtility.GetString(vITem.ProductName) + "<Br />" + BusinessUtility.GetString(vITem.UPCCode);
                                tempCell.HorizontalAlign = HorizontalAlign.Left;
                                tempCell.Width = 85;
                            }
                            else if (nM == 1)
                            {
                                tempCell.Text = BusinessUtility.GetString(vITem.ProductToReturnQty);
                                tempCell.HorizontalAlign = HorizontalAlign.Right;
                                tempCell.Width = 32;
                            }
                            else if (nM == 2)
                            {
                                tempCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(vITem.ProductUnitPrice), "", cultureName);
                                tempCell.HorizontalAlign = HorizontalAlign.Right;
                                tempCell.Width = 43;
                            }
                            else if (nM == 3)
                            {
                                double pQty = BusinessUtility.GetDouble(vITem.ProductToReturnQty);
                                double pAmt = BusinessUtility.GetDouble(vITem.ProductUnitPrice);
                                pAmt = (pAmt * pQty);
                                dblSubTot += pAmt;
                                tempCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(pAmt), "", cultureName);
                                tempCell.HorizontalAlign = HorizontalAlign.Right;
                                tempCell.Width = 70;
                            }
                            tempRow.Cells.Add(tempCell);
                            strSubTotal = string.Format("{0:F}", Convert.ToDouble(dblSubTot));
                            strSubTotal = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(strSubTotal), "", cultureName));
                        }

                        StringWriter sProwDtl = new StringWriter();
                        tempRow.RenderControl(new HtmlTextWriter(sProwDtl));
                        sbPrdDtl.Append(sProwDtl.ToString());
                    }

                    sbPrdRows.Append(sbPrdDtl.ToString());
                    printTemplet = printTemplet.Replace("#ITEMROWS#", sbPrdRows.ToString());
                    #endregion

                    #region SetTaxes
                    TotalSummary objCalcHlprNew = new TotalSummary();
                    printTemplet = printTemplet.Replace("#TAXROWS#", "");
                    #endregion

                    #region SetInvoiceTotal
                    printTemplet = printTemplet.Replace("#AMOUNTROWS#", "");
                    #endregion

                    #region SetSubTotal
                    StringBuilder sbInvSubTotal = new StringBuilder();
                    TableRow RowSub = new TableRow();
                    if (HttpContext.Current.Session["ReceiptRefundAmount"] != null)
                    {
                        for (int nM = 0; nM <= 1; nM++)
                        {
                            TableCell tempCell = new TableCell();
                            tempCell.CssClass = "lblPOSPrint";
                            tempCell.Font.Size = 7;
                            if (nM == 0)
                            {
                                tempCell.Text = CommonResource.ResourceValue("msgRefund", cultureName) + "  --";
                                tempCell.HorizontalAlign = HorizontalAlign.Right;
                                tempCell.Width = 165;
                                tempCell.Font.Bold = true;
                            }
                            else if (nM == 1)
                            {
                                strSubTotal = string.Format("{0:F}", (BusinessUtility.GetDouble(HttpContext.Current.Session["ReceiptRefundAmount"])).ToString());
                                strSubTotal = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(strSubTotal), "", cultureName));

                                tempCell.Text = strSubTotal;
                                tempCell.HorizontalAlign = HorizontalAlign.Right;
                                tempCell.Width = 65;
                            }
                            RowSub.Cells.Add(tempCell);
                        }
                        HttpContext.Current.Session["ReceiptRefundAmount"] = null;
                    }
                    else
                    {
                        for (int nM = 0; nM <= 1; nM++)
                        {
                            TableCell tempCell = new TableCell();
                            tempCell.CssClass = "lblPOSPrint";
                            tempCell.Font.Size = 7;
                            if (nM == 0)
                            {
                                tempCell.Text = CommonResource.ResourceValue("msgSubTotal", cultureName) + "  --";
                                tempCell.HorizontalAlign = HorizontalAlign.Right;
                                tempCell.Width = 165;
                                tempCell.Font.Bold = true;
                            }
                            else if (nM == 1)
                            {
                                tempCell.Text = strSubTotal;
                                tempCell.HorizontalAlign = HorizontalAlign.Right;
                                tempCell.Width = 65;
                            }
                            RowSub.Cells.Add(tempCell);
                        }
                    }
                    //tblsubTotal.Rows.Add(RowSub);
                    StringWriter sInvSubTotal = new StringWriter();
                    RowSub.RenderControl(new HtmlTextWriter(sInvSubTotal));
                    sbInvSubTotal.Append(sInvSubTotal.ToString());
                    printTemplet = printTemplet.Replace("#TOTALSUBTOTALROWS#", sInvSubTotal.ToString());
                    #endregion

                    HttpContext.Current.Session["RETURN_CART"] = null;
                }
                else
                {
                    printTemplet = "";
                }
            }
            else
            {
                printTemplet = "";
            }
            return printTemplet;
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    public static string GetPOSLaywayHtmlSnipt(int orderID, string lngID)
    {
        DbHelper dbHelp = new DbHelper(true);
        int custID = 0;
        try
        {
            Orders objOrd = new Orders();
            objOrd.PopulateObject(BusinessUtility.GetInt(orderID));

            string cultureName = "en-CA";
            string sCurrencyCode = "";

            if (lngID == "fr")
            {
                cultureName = "fr-CA";

            }
            else
            {
                cultureName = "en-CA";
            }

            //string printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/PrintTemplates/POSReceipt.htm"));
            string printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/PrintTemplates/LaywayReceipt.htm"));
            //string printTemplet = File.ReadAllText(ServerPath + "PrintTemplates/POSReceipt.htm");
            printTemplet = printTemplet.Replace("#PRINTDATE#", CommonResource.ResourceValue("POSDate", cultureName) + ": " + DateTime.Now.ToString("MM/dd/yyyy"));
            printTemplet = printTemplet.Replace("#PRINTTIME#", CommonResource.ResourceValue("POSTime", cultureName) + ": " + DateTime.Now.ToString("HH:mm:ss"));

            SysCompanyInfo objCompany = new SysCompanyInfo();
            SysWarehouses objWhrHouse = new SysWarehouses();
            objWhrHouse.PopulateObject(objOrd.OrdShpWhsCode);
            objCompany.PopulateObject(BusinessUtility.GetInt(objWhrHouse.WarehouseCompanyID));
            printTemplet = printTemplet.Replace("#COMPNAME#", objCompany.CompanyName);
            //printTemplet = printTemplet.Replace("#COMPPHONE#", objCompany.CompanyPOPhone);
            printTemplet = printTemplet.Replace("#COMPPHONE#", "");
            SysRegister objRegister = new SysRegister();
            objRegister.PopulateObject(BusinessUtility.GetString(objOrd.OrdRegCode));

            string sCompAddr = "";
            sCompAddr = objRegister.SysRegAddress + "<BR/>" + objRegister.SysRegCity + " " + objRegister.SysRegState + "<BR/>" + objCompany.CompanyCountry + " " + objRegister.SysRegPostalCode;
            if (BusinessUtility.GetString(objRegister.SysRegPhone) != "")
            {
                sCompAddr = sCompAddr + "</br>PH.- " + BusinessUtility.GetString(objRegister.SysRegPhone);
            }
            printTemplet = printTemplet.Replace("#COMPADD#", sCompAddr);

            Orders objInvoice = new Orders();
            objInvoice.GetOrderCustSaleRepName(BusinessUtility.GetInt(orderID));
            printTemplet = printTemplet.Replace("#CUSTNAME#", CommonResource.ResourceValue("lblCustName", cultureName) + ": " + BusinessUtility.GetString(objInvoice.OrdCustName));
            printTemplet = printTemplet.Replace("#SALEREP#", CommonResource.ResourceValue("lblPrintSalesRep", cultureName) + ": " + BusinessUtility.GetString(objInvoice.OrdCustSaleName));
            printTemplet = printTemplet.Replace("#CUSTPHONE#", CommonResource.ResourceValue("lblCoPhoneNo", cultureName) + ": " + BusinessUtility.GetString(objInvoice.OrdCustPhoneNo));
            printTemplet = printTemplet.Replace("#TRANSACTIONID#", BusinessUtility.GetString(orderID.ToString("D6")));
            string logoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/lib/image/") + "Logoprint.png";
            printTemplet = printTemplet.Replace("#COM_LOGO#", logoUrl);

            DataTable dt = objInvoice.GetPOSReceiptDetail(orderID, Globals.CurrentAppLanguageCode);
            #region AddInvoiceProductRows

            StringBuilder sbPrdRows = new StringBuilder();
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.HorizontalAlign = HorizontalAlign.Right;

            for (int nM = 0; nM != 5; nM++)
            {
                TableHeaderCell headerTableCell = new TableHeaderCell();
                headerTableCell.CssClass = "lblPOSPrint";
                headerTableCell.Font.Size = 7;
                if (nM == 0)
                {
                    headerTableCell.Text = CommonResource.ResourceValue("msgPrd", cultureName);
                    headerTableCell.HorizontalAlign = HorizontalAlign.Left;
                    headerTableCell.Width = 85;
                }
                else if (nM == 1)
                {
                    headerTableCell.Text = CommonResource.ResourceValue("msgQty", cultureName);
                    headerTableCell.Width = 32;
                }
                else if (nM == 2)
                {
                    headerTableCell.Text = CommonResource.ResourceValue("msgPrice", cultureName);
                    headerTableCell.Width = 43;
                }
                else if (nM == 3)
                {
                    headerTableCell.Text = CommonResource.ResourceValue("lblPosGrdDisc", cultureName);
                    headerTableCell.Width = 70;
                }
                else if (nM == 4)
                {
                    headerTableCell.Text = CommonResource.ResourceValue("msgTotalPrice", cultureName);
                    headerTableCell.Width = 70;
                }
                headerRow.Cells.Add(headerTableCell);
            }

            StringWriter sw = new StringWriter();
            headerRow.RenderControl(new HtmlTextWriter(sw));
            sbPrdRows.Append(sw.ToString());
            double dblSubTot = 0.0;

            string strTotal = "";
            string strSubTotal = "";
            string strReceived = "";
            string strReturn = "";
            string strDiscount = "0";

            StringBuilder sbPrdDtl = new StringBuilder();
            foreach (DataRow drObj in dt.Rows)
            {
                custID = BusinessUtility.GetInt(drObj["invCustID"]);
                printTemplet = printTemplet.Replace("#REGMESSAGE#", BusinessUtility.GetString(drObj["sysRegMessage"]));
                TableRow tempRow = new TableRow();
                for (int nM = 0; nM != 5; nM++)
                {
                    TableCell tempCell = new TableCell();
                    tempCell.CssClass = "lblPOSPrint";
                    tempCell.Font.Size = 7;

                    if (nM == 0)
                    {

                        string sPName = "";
                        if (BusinessUtility.GetString(drObj["prdIsNonStandard"]) == "1")
                        {
                            sPName = BusinessUtility.GetString(drObj["ProductName"]) + "<Br />" + BusinessUtility.GetString(drObj["prdUPCCode"]);
                        }
                        else
                        {
                            sPName = BusinessUtility.GetString(drObj["ProductName"]) + "<Br />" + BusinessUtility.GetString(drObj["prdUPCCode"]);
                        }
                        //tempCell.Text = BusinessUtility.GetString(drObj["ProductName"]) + " (" + BusinessUtility.GetString(drObj["Color"]) + ", " + BusinessUtility.GetString(drObj["Size"]) + ") " + "<Br />" + BusinessUtility.GetString(drObj["prdUPCCode"]);
                        tempCell.Text = sPName;
                        tempCell.HorizontalAlign = HorizontalAlign.Left;
                        tempCell.Width = 85;
                    }
                    else if (nM == 1)
                    {
                        tempCell.Text = BusinessUtility.GetString(drObj["posPrdQty"]);
                        tempCell.HorizontalAlign = HorizontalAlign.Right;
                        tempCell.Width = 32;
                    }
                    else if (nM == 2)
                    {
                        tempCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(drObj["posPrdUnitPrice"]), "", cultureName);
                        tempCell.HorizontalAlign = HorizontalAlign.Right;
                        tempCell.Width = 43;
                    }
                    else if (nM == 3)
                    {
                        tempCell.Text = BusinessUtility.GetString(BusinessUtility.GetInt(drObj["posPercentDiscount"]));
                        tempCell.HorizontalAlign = HorizontalAlign.Right;
                        tempCell.Width = 43;
                    }
                    else if (nM == 4)
                    {
                        double pDisc = BusinessUtility.GetDouble(drObj["posPercentDiscount"]);
                        double pAmt = BusinessUtility.GetDouble(drObj["posPrdPrice"]);
                        pAmt = pAmt - (pAmt * pDisc / 100);
                        dblSubTot += pAmt;
                        tempCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(pAmt), "", cultureName);
                        tempCell.HorizontalAlign = HorizontalAlign.Right;
                        tempCell.Width = 70;
                    }
                    tempRow.Cells.Add(tempCell);


                    TotalSummary objCalcHlpr = new TotalSummary();
                    objCalcHlpr.InitOrderTotal(BusinessUtility.GetInt(orderID));

                    double taxTotal = 0.0;
                    foreach (TaxItem taxDtl in objCalcHlpr.ItemTaxList)
                    {
                        taxTotal += BusinessUtility.GetDouble(taxDtl.CalculatedPrice);
                    }

                    strSubTotal = string.Format("{0:F}", Convert.ToDouble(dblSubTot));
                    strTotal = string.Format("{0:F}", Convert.ToDouble((BusinessUtility.GetDouble(strSubTotal) + BusinessUtility.GetDouble(taxTotal)).ToString()));
                    if (!string.IsNullOrEmpty(Convert.ToString(drObj["posCasReceived"])))
                    {
                        strReceived = string.Format("{0:F}", Convert.ToDouble(drObj["posCasReceived"].ToString()));
                    }
                    else
                    {
                        strReceived = "0.00";
                    }

                    strReturn = string.Format("{0:F}", Convert.ToDouble((BusinessUtility.GetDouble(strTotal) - BusinessUtility.GetDouble(strReceived)).ToString()));
                    //strReturn = CurrencyFormat.GetReplaceCurrencySymbol( CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble((BusinessUtility.GetDouble(strTotal) - BusinessUtility.GetDouble(strReceived)).ToString())));
                    strSubTotal = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(strSubTotal), "", cultureName));
                    strDiscount = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(strDiscount), "", cultureName));
                    strTotal = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(strTotal), "", cultureName));
                    strReceived = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(strReceived), "", cultureName));
                    strReturn = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(strReturn), "", cultureName));
                    sCurrencyCode = BusinessUtility.GetString(drObj["invCurrencyCode"]);
                }

                StringWriter sProwDtl = new StringWriter();
                tempRow.RenderControl(new HtmlTextWriter(sProwDtl));
                sbPrdDtl.Append(sProwDtl.ToString());
            }

            sbPrdRows.Append(sbPrdDtl.ToString());
            printTemplet = printTemplet.Replace("#ITEMROWS#", sbPrdRows.ToString());
            #endregion

            #region SetTaxes
            TotalSummary objCalcHlprNew = new TotalSummary();
            //objCalcHlprNew.InitInvoiceTotal(BusinessUtility.GetInt(invID));
            objCalcHlprNew.InitOrderTotal(BusinessUtility.GetInt(orderID));
            StringBuilder sbTaxRows = new StringBuilder();
            TableRow RowTax = new TableRow();
            bool IsRow = false;
            foreach (TaxItem taxDtl in objCalcHlprNew.ItemTaxList)
            {
                IsRow = true;
                if ((taxDtl.CalculatedPrice > 0))
                {
                    RowTax = new TableRow();
                    TableCell tempTaxDescCell = new TableCell();
                    tempTaxDescCell.CssClass = "lblPOSPrint";
                    tempTaxDescCell.Font.Size = 7;
                    tempTaxDescCell.Text = taxDtl.TaxCode + "  --";
                    tempTaxDescCell.HorizontalAlign = HorizontalAlign.Right;
                    tempTaxDescCell.Font.Bold = true;
                    tempTaxDescCell.Width = 158;
                    RowTax.Cells.Add(tempTaxDescCell);

                    TableCell tempTaxValueCell = new TableCell();
                    tempTaxValueCell.CssClass = "lblPOSPrint";
                    tempTaxValueCell.Font.Size = 7;
                    tempTaxValueCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(taxDtl.CalculatedPrice), "", cultureName);
                    tempTaxValueCell.HorizontalAlign = HorizontalAlign.Right;
                    RowTax.Cells.Add(tempTaxValueCell);

                    //tblTax.Rows.Add(RowTax);

                    StringWriter sTaxRows = new StringWriter();
                    RowTax.RenderControl(new HtmlTextWriter(sTaxRows));
                    sbTaxRows.Append(sTaxRows.ToString());
                }
            }
            printTemplet = printTemplet.Replace("#TAXROWS#", sbTaxRows.ToString());
            #endregion

            #region SetInvoiceTotal
            string strTransType = "S";
            StringBuilder sbInvTotalRows = new StringBuilder();

            AccountReceivable objAccountReceivable = new AccountReceivable();
            DataTable dtAcountReceivable = new DataTable();
            dtAcountReceivable = objAccountReceivable.GetOrderPartialPaymentList(BusinessUtility.GetInt(orderID), lngID);
            bool IsAddPartialDesc = false;
            CustomerCredit objCustCrdt = new CustomerCredit();
            double dblAvlCrdt = BusinessUtility.GetDouble(objCustCrdt.GetAvailableCredit(custID));

            for (int nR = 0; nR <= 4; nR++)
            {
                TableRow RowAmount = new TableRow();
                for (int nM = 0; nM <= 1; nM++)
                {
                    TableCell tempCell = new TableCell();
                    tempCell.CssClass = "lblPOSPrint";
                    tempCell.Font.Size = 7;
                    if (nR == 0)
                    {
                        if (nM == 0)
                        {
                            tempCell.Text = CommonResource.ResourceValue("msgTotal", cultureName) + "  --";
                            tempCell.HorizontalAlign = HorizontalAlign.Right;
                            tempCell.Width = 165;
                            tempCell.Font.Bold = true;
                        }
                        else if (nM == 1)
                        {
                            tempCell.Text = strTotal;
                            tempCell.HorizontalAlign = HorizontalAlign.Right;
                            tempCell.Width = 65;
                        }
                    }
                    else if (nR == 2)
                    {
                        if (((dtAcountReceivable.Rows.Count > 0) & IsAddPartialDesc == false))
                        {
                            IsAddPartialDesc = true;
                            foreach (DataRow dRow in dtAcountReceivable.Rows)
                            {
                                if (!string.IsNullOrEmpty(BusinessUtility.GetString(dRow["Mode"])))
                                {
                                    if (Convert.ToDouble(dRow["AmountDeposit"]) > 0)
                                    {
                                        TableRow dProw = new TableRow();
                                        TableCell tempCellp = new TableCell();
                                        tempCellp.CssClass = "lblPOSPrint";
                                        tempCellp.Font.Size = 7;
                                        tempCellp.Text = BusinessUtility.GetString(dRow["Mode"]) + "  --";
                                        tempCellp.HorizontalAlign = HorizontalAlign.Right;
                                        tempCellp.Width = 165;
                                        tempCellp.Font.Bold = true;

                                        TableCell tempCellv = new TableCell();
                                        tempCellv.CssClass = "lblPOSPrint";
                                        tempCellv.Font.Size = 7;
                                        //tempCellv.Text = string.Format("{0:F}", Convert.ToDouble(dRow["ARAmtRcvd"].ToString()));
                                        tempCellv.Text = string.Format("{0:F}", Convert.ToDouble(dRow["AmountDeposit"].ToString()));
                                        tempCellv.HorizontalAlign = HorizontalAlign.Right;
                                        tempCellv.Width = 65;
                                        dProw.Cells.Add(tempCellp);
                                        dProw.Cells.Add(tempCellv);
                                        StringWriter sInvPartialPayment = new StringWriter();
                                        dProw.RenderControl(new HtmlTextWriter(sInvPartialPayment));
                                        sbInvTotalRows.Append(sInvPartialPayment.ToString());
                                    }
                                }
                            }
                        }

                    }
                    else if (nR == 3)
                    {
                        if (nM == 0)
                        {
                            if (strTransType == "S")
                            {
                                tempCell.Text = CommonResource.ResourceValue("msgBalance", cultureName) + "  --";
                            }
                            else
                            {
                                tempCell.Text = CommonResource.ResourceValue("lblPOSRefund", cultureName) + "  --";
                            }
                            tempCell.HorizontalAlign = HorizontalAlign.Right;
                            tempCell.Font.Bold = true;
                        }
                        else if (nM == 1)
                        {
                            tempCell.Text = strReturn;
                            tempCell.HorizontalAlign = HorizontalAlign.Right;
                        }
                    }
                    else if (nR == 4)
                    {
                        if (BusinessUtility.GetDouble(dblAvlCrdt) > 0)
                        {
                            if (nM == 0)
                            {
                                if (strTransType == "S")
                                {
                                    tempCell.Text = CommonResource.ResourceValue("lblInstoreCreditAvailable", cultureName) + "  --";
                                }
                                tempCell.HorizontalAlign = HorizontalAlign.Right;
                                tempCell.Font.Bold = true;
                            }
                            else if (nM == 1)
                            {
                                tempCell.Text = dblAvlCrdt.ToString();
                                tempCell.HorizontalAlign = HorizontalAlign.Right;
                            }
                        }
                    }
                    else if (nR == 1)
                    {
                        if (BusinessUtility.GetDouble(strDiscount) > 0)
                        {
                            if (nM == 0)
                            {
                                if (strTransType == "S")
                                {
                                    tempCell.Text = "Discount(%)" + "  --";
                                }
                                tempCell.HorizontalAlign = HorizontalAlign.Right;
                                tempCell.Font.Bold = true;
                            }
                            else if (nM == 1)
                            {
                                tempCell.Text = strDiscount;
                                tempCell.HorizontalAlign = HorizontalAlign.Right;
                            }
                        }
                    }
                    RowAmount.Cells.Add(tempCell);
                }
                StringWriter sInvTotal = new StringWriter();
                RowAmount.RenderControl(new HtmlTextWriter(sInvTotal));
                sbInvTotalRows.Append(sInvTotal.ToString());
                //tblAmount.Rows.Add(RowAmount);
            }

            printTemplet = printTemplet.Replace("#AMOUNTROWS#", sbInvTotalRows.ToString());
            #endregion

            #region SetSubTotal
            StringBuilder sbInvSubTotal = new StringBuilder();
            TableRow RowSub = new TableRow();
            for (int nM = 0; nM <= 1; nM++)
            {
                TableCell tempCell = new TableCell();
                tempCell.CssClass = "lblPOSPrint";
                tempCell.Font.Size = 7;
                if (nM == 0)
                {
                    tempCell.Text = CommonResource.ResourceValue("msgSubTotal", cultureName) + "  --";
                    tempCell.HorizontalAlign = HorizontalAlign.Right;
                    tempCell.Width = 165;
                    tempCell.Font.Bold = true;
                }
                else if (nM == 1)
                {
                    tempCell.Text = strSubTotal;
                    tempCell.HorizontalAlign = HorizontalAlign.Right;
                    tempCell.Width = 65;
                }
                RowSub.Cells.Add(tempCell);
            }
            //tblsubTotal.Rows.Add(RowSub);
            StringWriter sInvSubTotal = new StringWriter();
            RowSub.RenderControl(new HtmlTextWriter(sInvSubTotal));
            sbInvSubTotal.Append(sInvSubTotal.ToString());
            printTemplet = printTemplet.Replace("#TOTALSUBTOTALROWS#", sInvSubTotal.ToString());
            #endregion

            return printTemplet;
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    public static string GetTicketPrintHtmlSnipt(Page pg, string oIDs, string lngID)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            string cultureName = "en-CA";
            if (lngID == "fr")
            {
                cultureName = "fr-CA";

            }
            else
            {
                cultureName = "en-CA";
            }

            string printTemplet = File.ReadAllText(HttpContext.Current.Server.MapPath("~/PrintTemplates/TicketTemplate.htm"));
            printTemplet = printTemplet.Replace("#PRINTDATE#", CommonResource.ResourceValue("POSDate", cultureName) + ": " + DateTime.Now.ToString("MM/dd/yyyy"));
            printTemplet = printTemplet.Replace("#PRINTTIME#", CommonResource.ResourceValue("POSTime", cultureName) + ": " + DateTime.Now.ToString("HH:mm:ss"));
            string[] oids = oIDs.Split(',');
            int orderIDs = 0;
            if (oids.Length > 0)
            {
                orderIDs = BusinessUtility.GetInt(oids[0]);
            }
            Orders ord = new Orders();
            ord.PopulateObject(orderIDs);
            SysWarehouses sWhs = new SysWarehouses();
            sWhs.PopulateObject(ord.OrdShpToWhsCode, dbHelp);
            SysWarehouses sWhsFrom = new SysWarehouses();
            sWhsFrom.PopulateObject(ord.OrdShpWhsCode, dbHelp);
            if (ord.OrderTypeCommission == 2)
            {
                printTemplet = printTemplet.Replace("#SHIPTOWHS#", CommonResource.ResourceValue("lblShpToWarehouse", cultureName) + " : " + ord.OrdShpToWhsCode);
                printTemplet = printTemplet.Replace("#WHSTOADD#", sWhs.WarehouseAddressLine1 + " " + sWhs.WarehouseAddressLine2 + "</br>" + sWhs.WarehouseCity + " " + sWhs.WarehouseState + " " + sWhs.WarehouseCountry + "</br>" + sWhs.WarehousePostalCode + " " + sWhs.WarehouseFax + " " + sWhs.WarehousePhone);
                printTemplet = printTemplet.Replace("#SHIPFROMWHS#", CommonResource.ResourceValue("lblShipFromWhs", cultureName) + " : " + ord.OrdShpWhsCode);
                printTemplet = printTemplet.Replace("#WHSFROMADD#", sWhsFrom.WarehouseAddressLine1 + " " + sWhsFrom.WarehouseAddressLine2 + "</br>" + sWhsFrom.WarehouseCity + " " + sWhsFrom.WarehouseState + " " + sWhsFrom.WarehouseCountry + "</br>" + sWhsFrom.WarehousePostalCode + " " + sWhsFrom.WarehouseFax + " " + sWhsFrom.WarehousePhone);
            }
            else
            {
                printTemplet = printTemplet.Replace("#SHIPTOWHS#", CommonResource.ResourceValue("titleShippingAddress", cultureName));
                Addresses shipToAddress = new Addresses();
                shipToAddress.GetOrderAddress(ord.OrdID, AddressType.SHIP_TO_ADDRESS);
                printTemplet = printTemplet.Replace("#WHSTOADD#", shipToAddress.ToHtml());
                printTemplet = printTemplet.Replace("#SHIPFROMWHS#", CommonResource.ResourceValue("lblShipFromWhs", cultureName) + " : " + ord.OrdShpWhsCode);
                printTemplet = printTemplet.Replace("#WHSFROMADD#", sWhsFrom.WarehouseAddressLine1 + " " + sWhsFrom.WarehouseAddressLine2 + "</br>" + sWhsFrom.WarehouseCity + " " + sWhsFrom.WarehouseState + " " + sWhsFrom.WarehouseCountry + "</br>" + sWhsFrom.WarehousePostalCode + " " + sWhsFrom.WarehouseFax + " " + sWhsFrom.WarehousePhone);
            }

            SysCompanyInfo objCompany = new SysCompanyInfo();
            objCompany.PopulateObject(BusinessUtility.GetInt(sWhs.WarehouseCompanyID));
            printTemplet = printTemplet.Replace("#COMPNAME#", objCompany.CompanyName);
            //printTemplet = printTemplet.Replace("#COMPPHONE#", objCompany.CompanyPhoneno);
            //string sCompAddr = "";
            //sCompAddr = objCompany.CompanyAddressLine1 + "<BR/>" + objCompany.CompanyAddressLine2 + "<BR/>" + objCompany.CompanyCity + " " + objCompany.CompanyState + "<BR/>" + objCompany.CompanyCountry + " " + objCompany.CompanyPostalCode;
            //printTemplet = printTemplet.Replace("#COMPADD#", sCompAddr);
            ord.GetOrderCustSaleRepName(ord.OrdID);
            printTemplet = printTemplet.Replace("#CUSTNAME#", CommonResource.ResourceValue("lblCustName", cultureName) + ": " + BusinessUtility.GetString(ord.OrdCustName));
            string logoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/lib/image/") + "Logoprint.png";
            printTemplet = printTemplet.Replace("#COM_LOGO#", logoUrl);


            DataTable dt = ord.GetTicketItemsList(dbHelp, oIDs, lngID);

            StringBuilder sbPrdRows = new StringBuilder();
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.HorizontalAlign = HorizontalAlign.Right;

            for (int nM = 0; nM != 3; nM++)
            {
                TableHeaderCell headerTableCell = new TableHeaderCell();
                headerTableCell.CssClass = "lblPOSPrint";
                headerTableCell.Font.Size = 8;
                if (nM == 1)
                {
                    headerTableCell.Text = CommonResource.ResourceValue("msgPrd", cultureName);
                    headerTableCell.HorizontalAlign = HorizontalAlign.Center;
                    headerTableCell.Width = 85;
                }
                else if (nM == 2)
                {
                    headerTableCell.Text = CommonResource.ResourceValue("msgQty", cultureName);
                    headerTableCell.Width = 32;
                }
                else if (nM == 0)
                {
                    headerTableCell.Text = CommonResource.ResourceValue("cmdCssOrderNo", cultureName);
                    headerTableCell.HorizontalAlign = HorizontalAlign.Left;
                    headerTableCell.Width = 32;
                }

                headerRow.Cells.Add(headerTableCell);
            }

            StringWriter sw = new StringWriter();
            headerRow.RenderControl(new HtmlTextWriter(sw));
            sbPrdRows.Append(sw.ToString());

            StringBuilder sbPrdDtl = new StringBuilder();
            foreach (DataRow drObj in dt.Rows)
            {
                TableRow tempRow = new TableRow();
                for (int nM = 0; nM != 3; nM++)
                {
                    TableCell tempCell = new TableCell();
                    tempCell.CssClass = "lblPOSPrint";
                    tempCell.Font.Size = 8;

                    if (nM == 1)
                    {
                        if (BusinessUtility.GetString(drObj["color"]) != "" && BusinessUtility.GetString(drObj["size"]) != "")
                        {
                            tempCell.Text = BusinessUtility.GetString(drObj["ProductName"]) + "<Br />" + BusinessUtility.GetString(drObj["color"]) + " " + BusinessUtility.GetString(drObj["size"]) + "<Br />" + BusinessUtility.GetString(drObj["prdUPCCode"]);

                        }
                        else
                        {
                            tempCell.Text = BusinessUtility.GetString(drObj["ProductName"]) + "<Br />" + BusinessUtility.GetString(drObj["prdUPCCode"]);
                        }
                        tempCell.HorizontalAlign = HorizontalAlign.Center;
                        tempCell.Width = 85;
                    }
                    else if (nM == 2)
                    {
                        tempCell.Text = BusinessUtility.GetString(drObj["ordProductQty"]);
                        tempCell.HorizontalAlign = HorizontalAlign.Right;
                        tempCell.Width = 32;
                    }
                    else if (nM == 0)
                    {
                        tempCell.Text = BusinessUtility.GetString(drObj["ordID"]);
                        tempCell.HorizontalAlign = HorizontalAlign.Left;
                        tempCell.Width = 32;
                    }
                    tempRow.Cells.Add(tempCell);
                }

                StringWriter sProwDtl = new StringWriter();
                tempRow.RenderControl(new HtmlTextWriter(sProwDtl));
                sbPrdDtl.Append(sProwDtl.ToString());
            }

            sbPrdRows.Append(sbPrdDtl.ToString());
            printTemplet = printTemplet.Replace("#ITEMROWS#", sbPrdRows.ToString());
            //#endregion
            return printTemplet;
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    #region Sivananda Header Methods

    private static string GetDocumentHeaderSiavanadaSOHtml(Orders ord, Partners part, string docType, string printTemplet)
    {
        printTemplet = printTemplet.Replace("#SHIVANANDA_DOC_LOGO#", string.Format(@"<img src= ""{0}"" height=""228px"" width=""750px"" />", ConfigurationManager.AppSettings["SivanandaLogoPath"]));

        string docInfo = "";
        docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("prnDate"), ord.OrdCreatedOn.ToString("MMM-dd yyyy"));

        if (docType.ToUpper().Trim() == "QO")
        {
            docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("prnQuotationNo"), ord.OrdID);
        }
        else if (docType.ToUpper().Trim() == "SO")
        {
            docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("prnSONo"), ord.OrdID);
        }

        docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("lblCustomerName"), part.PartnerLongName);

        printTemplet = printTemplet.Replace("#SHIVANANDA_DOC_INFO#", docInfo);

        return printTemplet;
    }

    private static string GetDocumentHeaderSivanandaInvHtml(Invoice inv, Partners part, string printTemplet)
    {
        printTemplet = printTemplet.Replace("#SHIVANANDA_DOC_LOGO#", string.Format(@"<img src= ""{0}"" height=""228px"" width=""750px"" />", ConfigurationManager.AppSettings["SivanandaLogoPath"]));

        string docInfo = "";
        docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("prnDate"), inv.InvCreatedOn.ToString("MMM-dd yyyy"));
        docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("prnInvoiceNo"), inv.InvID);
        docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("prnSONo"), inv.InvForOrderNo);

        docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("lblCustomerName"), part.PartnerLongName);

        printTemplet = printTemplet.Replace("#SHIVANANDA_DOC_INFO#", docInfo);

        return printTemplet;
    }
    #endregion

    #region Purchase Order Print Functions
    public static string GetPOPrintHtmlSnipt(Page pg, int poID)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            //Set templates html to litrals   
            string printTemplet = File.ReadAllText(HttpContext.Current.Server.MapPath("~/PrintTemplates/PurchaseOrder.htm"));
            string itemRowTemplet = string.Empty;
            int sIdx, eIdx;
            sIdx = printTemplet.IndexOf("<!--ITEM_ROW_TEMPLET_START--><!--");
            eIdx = printTemplet.IndexOf("--><!--ITEM_ROW_TEMPLET_END-->");
            itemRowTemplet = printTemplet.Substring(sIdx, eIdx - sIdx).Replace("<!--ITEM_ROW_TEMPLET_START--><!--", "");


            //Init Requested Order
            PurchaseOrders ord = new PurchaseOrders();
            ord.PopulateObject(dbHelp, poID);

            //Init Customer 
            Vendor part = new Vendor();
            part.PopulateObject(dbHelp, ord.PoVendorID);

            //Init CompanyInfo
            SysCompanyInfo cinfo = new SysCompanyInfo();
            cinfo.PopulateObject(ord.PoCompanyID, dbHelp);

            //TO DO TO SET Company Info & Logo on Header also need to set Html in Header
            if (!string.IsNullOrEmpty(cinfo.CompanyLogoPath))
            {
                //printTemplet = printTemplet.Replace("#COM_LOGO#", pg.ResolveUrl("~/Upload/companylogo/") + cinfo.CompanyLogoPath);
                string logoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/Upload/companylogo/") + cinfo.CompanyLogoPath;
                printTemplet = printTemplet.Replace("#COM_LOGO#", logoUrl);
            }
            else
            {
                printTemplet = printTemplet.Replace("#COM_LOGO#", "");
            }
            if (ConfigurationManager.AppSettings["DocumentHeaderInfo"] == "WHS")
            {
                printTemplet = printTemplet.Replace("#COM_INFO#", CommonPrint.WarehouseInfoHtml(dbHelp, ord.PoWhsCode));
            }
            else
            {
                printTemplet = printTemplet.Replace("#COM_INFO#", CommonPrint.GetCompanyInfoHtml(cinfo));
            }

            printTemplet = printTemplet.Replace("#DOCUMENT_TITLE#", CommonResource.ResourceValue("prnPO"));

            string docInfo = "";
            docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("prnDate"), ord.PoDate.ToString("MMM-dd yyyy"));
            docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("prnPONo"), ord.PoID);
            docInfo += string.Format("<b>{0}</b> : {1} <br>", CommonResource.ResourceValue("grdVendorName"), part.VendorName);
            printTemplet = printTemplet.Replace("#DOC_INFO#", docInfo);



            //Set Bill To Address
            Addresses billToAddress = new Addresses();
            billToAddress.PopulateObject(dbHelp, ord.PoVendorID, "V", AddressType.BILL_TO_ADDRESS);
            printTemplet = printTemplet.Replace("#BILL_TO_ADDRESS_TITLE#", CommonResource.ResourceValue("BillTo"));
            printTemplet = printTemplet.Replace("#BILL_TO_ADDRESS#", GetAddress(billToAddress));


            //Set Ship To Address
            Addresses shipToAddress = new Addresses();
            shipToAddress.PopulateObject(dbHelp, ord.PoVendorID, "V", AddressType.SHIP_TO_ADDRESS);
            printTemplet = printTemplet.Replace("#SHIP_TO_ADDRESS_TITLE#", CommonResource.ResourceValue("ShipTo"));
            printTemplet = printTemplet.Replace("#SHIP_TO_ADDRESS#", GetAddress(shipToAddress));

            printTemplet = printTemplet.Replace("#ITEM_LIST_TITLE#", CommonResource.ResourceValue("lblItems"));

            //Build Item Header      
            printTemplet = printTemplet.Replace("#ITM_HDR_DESCRIPTION#", CommonResource.ResourceValue("lblDescription"));
            printTemplet = printTemplet.Replace("#ITM_HDR_UNITS#", CommonResource.ResourceValue("lblUnits"));
            printTemplet = printTemplet.Replace("#ITM_HDR_COST_PER_UNIT#", CommonResource.ResourceValue("lblCostPerUnit"));
            printTemplet = printTemplet.Replace("#ITM_HDR_DISCOUNT#", CommonResource.ResourceValue("grdSaleDiscount"));
            printTemplet = printTemplet.Replace("#ITM_HDR_AMOUNT#", CommonResource.ResourceValue("lblAmount"));

            PurchaseOrderItems oi = new PurchaseOrderItems();
            string itemsToPrint = string.Empty;
            double total = 0.00D;
            SysCurrencies cur = new SysCurrencies();
            cur.PopulateObject(ord.PoCurrencyCode);
            try
            {
                var itemReader = oi.GetPurchaseOrderItems(dbHelp, ord.PoID);
                //string itemRow = string.Empty;
                //double units, unitPrice;
                //foreach (DataRow item in itemReader.Rows)
                //{
                //    itemRow = itemRowTemplet;
                //    itemRow = itemRow.Replace("#ITM_DESCRIPTION#", BusinessUtility.GetString(item["prdName"]).Replace(Environment.NewLine, "<br>"));
                //    itemRow = itemRow.Replace("#ITM_UNITS#", BusinessUtility.GetString(item["poQty"]));
                //    itemRow = itemRow.Replace("#ITM_COST_PER_UNIT#", string.Format("{0} {1:F}", ord.PoCurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(item["poUnitPrice"]), Globals.CurrentCultureName)));
                //    //itemRow = itemRow.Replace("#ITM_DISCOUNT#", string.Format("{0} {1:F}", ord.PoCurrencyCode, item["ordProductDiscount"]));
                //    units = BusinessUtility.GetDouble(item["poQty"]);
                //    unitPrice = BusinessUtility.GetDouble(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(item["poUnitPrice"]), Globals.CurrentCultureName));
                //    itemRow = itemRow.Replace("#ITM_AMOUNT#", string.Format("{0} {1:F}", ord.PoCurrencyCode, BusinessUtility.GetCurrencyString(units * unitPrice, Globals.CurrentCultureName)));
                //    total += units * unitPrice;
                //    // itemsToPrint += itemRow;
                //}

                #region PrintLayout

                sIdx = printTemplet.IndexOf("<!--ITEM_PROD_HDG_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_PROD_HDG_END-->");
                string itemProdHdg = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdHdg = itemProdHdg.Replace("<!--ITEM_PROD_HDG_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_HDG_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_HDG_END-->");
                string itemProdDescHdg = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdDescHdg = itemProdDescHdg.Replace("<!--ITEM_DESC_HDG_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_HDG_VAL_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_HDG_VAL_END-->");
                string itemProdDescVal = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdDescVal = itemProdDescVal.Replace("<!--ITEM_DESC_HDG_VAL_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_SIZE_HDG_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_SIZE_HDG_END-->");
                string itemProdSizeHdg = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdSizeHdg = itemProdSizeHdg.Replace("<!--ITEM_DESC_SIZE_HDG_START--><!--", "");

                sIdx = printTemplet.IndexOf("<!--ITEM_DESC_CNT_START--><!--");
                eIdx = printTemplet.IndexOf("--><!--ITEM_DESC_CNT_END-->");
                string itemProdDesc = printTemplet.Substring(sIdx, eIdx - sIdx);
                itemProdDesc = itemProdDesc.Replace("<!--ITEM_DESC_CNT_START--><!--", "");

                var vProdGroup = from row in itemReader.AsEnumerable()
                                 group row by row.Field<string>("prdName") into grp
                                 select new
                                 {
                                     prdName = grp.Key,
                                     MemberCount = grp.Count()
                                 };


                var vProdSizeGroup = from row in itemReader.AsEnumerable()
                                     group row by row.Field<string>("Size") into grp
                                     select new
                                     {
                                         Size = grp.Key,
                                         MemberCount = grp.Count()
                                     };


                foreach (var item in vProdGroup)
                {
                    itemsToPrint += itemProdHdg.Replace("#PNAME#", item.prdName);
                    string sItemDescHdgRow = "";
                    string sSizeHdg = "";
                    foreach (var size in vProdSizeGroup)
                    {
                        sSizeHdg += itemProdSizeHdg.Replace("#SIZE#", size.Size);
                    }
                    sItemDescHdgRow += itemProdDescHdg.Replace("#HDG_COLOR#", CommonResource.ResourceValue("lblColor")).Replace("#HDG_SIZE#", sSizeHdg).Replace("#HDG_COST_PER_UNIT#", CommonResource.ResourceValue("grdvProcessCostPerUnit")).Replace("#HDG_AMOUNT#", CommonResource.ResourceValue("lblAmmount")).Replace("#HDG_TOTAL_QTY#", CommonResource.ResourceValue("lblTotalQty"));

                    DataView datavw = new DataView();
                    datavw = itemReader.DefaultView;
                    datavw.RowFilter = "prdName='" + BusinessUtility.GetString(item.prdName) + "'";
                    DataTable dtProduct = datavw.ToTable();

                    var vProdColorGroup = from row in dtProduct.AsEnumerable()
                                          group row by row.Field<string>("Color") into grp
                                          select new
                                          {
                                              Color = grp.Key,
                                              MemberCount = grp.Count()
                                          };
                    foreach (var color in vProdColorGroup)
                    {
                        string sSizeQty = "";
                        int qty = 0;
                        double pPrice = 0.0;
                        foreach (var size in vProdSizeGroup)
                        {
                            DataView dvNewPRow = new DataView();
                            dvNewPRow = itemReader.DefaultView;
                            dvNewPRow.RowFilter = "prdName='" + BusinessUtility.GetString(item.prdName) + "' AND color='" + color.Color + "' AND Size ='" + size.Size + "' ";
                            DataTable dtProductSize = datavw.ToTable();

                            if (dtProductSize.Rows.Count > 0)
                            {
                                sSizeQty += itemProdSizeHdg.Replace("#SIZE#", BusinessUtility.GetString(dtProductSize.Rows[0]["poQty"]));
                                qty += BusinessUtility.GetInt(dtProductSize.Rows[0]["poQty"]);
                                pPrice = BusinessUtility.GetDouble(dtProductSize.Rows[0]["poUnitPrice"]);
                            }
                            else
                            {
                                sSizeQty += itemProdSizeHdg.Replace("#SIZE#", BusinessUtility.GetString("0"));
                                qty += BusinessUtility.GetInt(0);
                            }

                            //foreach (DataRow drSize in dtProductSize.Rows)
                            //{
                            //    sSizeQty += itemProdSizeHdg.Replace("#SIZE#", BusinessUtility.GetString(drSize["poQty"]));
                            //    qty += BusinessUtility.GetInt(drSize["poQty"]);
                            //    pPrice = BusinessUtility.GetDouble(drSize["poUnitPrice"]);
                            //}
                        }
                        double pAmount = pPrice * qty;
                        total += pAmount;
                        sItemDescHdgRow += itemProdDescVal.Replace("#HDG_COLOR#", color.Color).Replace("#HDG_SIZE#", sSizeQty).Replace("#HDG_COST_PER_UNIT#", BusinessUtility.GetCurrencyString(pPrice, Globals.CurrentCultureName)).Replace("#HDG_AMOUNT#", string.Format("{0} {1:F}", ord.PoCurrencyCode, CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(pAmount * cur.CurrencyRelativePrice, Globals.CurrentCultureName)))).Replace("#HDG_TOTAL_QTY#", BusinessUtility.GetString(qty));
                    }
                    itemsToPrint += itemProdDesc.Replace("#ITEM_GROUP_DESC#", sItemDescHdgRow);
                }
                #endregion
            }
            finally
            {

            }



            printTemplet = printTemplet.Replace("<!--ITEM_ROW_LIST_PLACEHOLDER-->", itemsToPrint);
            printTemplet = printTemplet.Replace("#ITEM_SUB_TOTAL_TABLE#", string.Format("{0} {1:F}", ord.PoCurrencyCode, CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString((total * cur.CurrencyRelativePrice), Globals.CurrentCultureName))));

            //Update Print Counter
            //if (docType == "SO")
            //{
            //    ord.UpdateSalesOrderPrintCounter(dbHelp, ord.OrdID);
            //}
            //else
            //{
            //    ord.UpdateQuotePrintCounter(dbHelp, ord.OrdID);
            //}

            //Apply Barcode prefix & postfix
            printTemplet = printTemplet.Replace("#BARCODE_PREFIX#", AppConfiguration.AppSettings[AppSettingKey.BarcodePrefix]);
            printTemplet = printTemplet.Replace("#BARCODE_POSTFIX#", AppConfiguration.AppSettings[AppSettingKey.BarcodePrefix]);

            return printTemplet;
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }
    #endregion

    #region Statement Printing
    public static string GetStatementPrintHtml(Page pg, int comID, string daysRange, int customerID)
    {
        //Set templates html to litrals   
        string printTemplet = File.ReadAllText(HttpContext.Current.Server.MapPath("~/PrintTemplates/Statement.htm"));
        string itemRowTemplet = string.Empty;
        string processItemRowTemplet = string.Empty;
        int sIdx, eIdx;
        sIdx = printTemplet.IndexOf("@ITEM_ROW_START@");
        eIdx = printTemplet.IndexOf("@ITEM_ROW_END@");
        itemRowTemplet = printTemplet.Substring(sIdx, eIdx - sIdx).Replace("@ITEM_ROW_START@", "");

        DbHelper dbHelp = new DbHelper(true);
        try
        {
            dbHelp.OpenDatabaseConnection();
            SysCompanyInfo cinfo = new SysCompanyInfo();
            cinfo.PopulateObject(comID, dbHelp);

            if (!string.IsNullOrEmpty(cinfo.CompanyLogoPath))
            {
                string logoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/Upload/companylogo/") + cinfo.CompanyLogoPath;
                printTemplet = printTemplet.Replace("#COM_LOGO#", logoUrl);
            }

            printTemplet = printTemplet.Replace("#COM_INFO#", CommonPrint.GetCompanyInfoHtml(cinfo));
            printTemplet = printTemplet.Replace("#STATEMENT_DATE#", DateTime.Now.ToString("MM/dd/yyyy"));

            Invoice inv = new Invoice();
            var statements = inv.GetStatements(dbHelp, comID, daysRange, customerID);
            string itmHtml = string.Empty;
            string itmsHtml = string.Empty;
            TotalSummary ts;
            double totalPaidAmount = 0.00D, totalBalAmount = 0.00D;
            foreach (DataRow item in statements.Rows)
            {
                itmHtml = itemRowTemplet;
                ts = CalculationHelper.GetInvoiceTotal(dbHelp, BusinessUtility.GetInt(item["invID"]));
                itmHtml = itmHtml.Replace("#INVOICE_NO#", BusinessUtility.GetString(item["InvoiceNo"]));
                itmHtml = itmHtml.Replace("#INVOICE_DATE#", BusinessUtility.GetString(item["InvDate"]));
                itmHtml = itmHtml.Replace("#INVOICE_AMOUNT#", string.Format("{0:C}", BusinessUtility.GetCurrencyString(ts.GrandTotal, Globals.CurrentCultureName)));
                itmHtml = itmHtml.Replace("#INVOICE_PAID_AMOUNT#", string.Format("{0:C}", BusinessUtility.GetCurrencyString(ts.TotalAmountReceived, Globals.CurrentCultureName)));
                itmHtml = itmHtml.Replace("#INVOICE_BALANCE_AMOUNT#", string.Format("{0:C}", BusinessUtility.GetCurrencyString(ts.OutstandingAmount, Globals.CurrentCultureName)));
                totalPaidAmount += ts.TotalAmountReceived;
                totalBalAmount += ts.OutstandingAmount;
                itmsHtml += itmHtml;
            }

            printTemplet = printTemplet.Replace("<!--ROW_ITEMS-->", itmsHtml);
            printTemplet = printTemplet.Replace("#TOTAL_PAID_AMOUNT#", string.Format("{0:C}", BusinessUtility.GetCurrencyString(totalPaidAmount, Globals.CurrentCultureName)));
            printTemplet = printTemplet.Replace("#TOTAL_BALANCE_AMOUNT#", string.Format("{0:C}", BusinessUtility.GetCurrencyString(totalBalAmount, Globals.CurrentCultureName)));

            return printTemplet;
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }

    }
    #endregion

    public static string GetReturnReceiptHtmlSnipt(string lngID, int ordID, int accRcblID) // , List<ReturnCart> returnCart
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            string cultureName = "en-CA";
            string sCurrencyCode = "";
            string printTemplet = "";
            if (lngID == "fr")
            {
                cultureName = "fr-CA";
            }
            else
            {
                cultureName = "en-CA";
            }

            if (ordID > 0 && accRcblID > 0)
            {
                printTemplet = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/PrintTemplates/ReturnReceiptNew.htm"));

                int invoiceID = 0;
                Invoice inv = new Invoice();
                invoiceID = inv.GetInvoiceID(dbHelp, ordID);
                // Get Cust Name And Sales Rep Name
                if (invoiceID > 0)
                {
                    Invoice objOrd = new Invoice();
                    objOrd.PopulateObject(invoiceID);

                    printTemplet = printTemplet.Replace("#PRINTDATE#", CommonResource.ResourceValue("POSDate", cultureName) + ": " + DateTime.Now.ToString("MM/dd/yyyy"));
                    printTemplet = printTemplet.Replace("#PRINTTIME#", CommonResource.ResourceValue("POSTime", cultureName) + ": " + DateTime.Now.ToString("HH:mm:ss"));

                    SysCompanyInfo objCompany = new SysCompanyInfo();
                    SysWarehouses objWhrHouse = new SysWarehouses();
                    objWhrHouse.PopulateObject(objOrd.InvShpWhsCode);
                    objCompany.PopulateObject(BusinessUtility.GetInt(objWhrHouse.WarehouseCompanyID));
                    printTemplet = printTemplet.Replace("#COMPNAME#", objCompany.CompanyName);
                    //printTemplet = printTemplet.Replace("#COMPPHONE#", objCompany.CompanyPOPhone);
                    printTemplet = printTemplet.Replace("#COMPPHONE#", "");

                    SysRegister objRegister = new SysRegister();
                    objRegister.PopulateObject(BusinessUtility.GetString(objOrd.InvRegCode));
                    string sCompAddr = "";
                    sCompAddr = objRegister.SysRegAddress + "<BR/>" + objRegister.SysRegCity + " " + objRegister.SysRegState + "<BR/>" + objCompany.CompanyCountry + " " + objRegister.SysRegPostalCode;
                    if (BusinessUtility.GetString(objRegister.SysRegPhone) != "")
                    {
                        sCompAddr = sCompAddr + "</br>PH.- " + BusinessUtility.GetString(objRegister.SysRegPhone);
                    }
                    printTemplet = printTemplet.Replace("#COMPADD#", sCompAddr);


                    Invoice objInvoice = new Invoice();
                    objInvoice.GetInvoiceCustSaleRepName(invoiceID);
                    printTemplet = printTemplet.Replace("#CUSTNAME#", CommonResource.ResourceValue("lblCustName", cultureName) + ": " + BusinessUtility.GetString(objInvoice.InvCustName));
                    printTemplet = printTemplet.Replace("#SALEREP#", CommonResource.ResourceValue("lblPrintSalesRep", cultureName) + ": " + BusinessUtility.GetString(objInvoice.InvSaleRep));

                    printTemplet = printTemplet.Replace("#TRANSACTIONID#", BusinessUtility.GetString(accRcblID.ToString("D6")));

                    string logoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/lib/image/") + "Logoprint.png";
                    printTemplet = printTemplet.Replace("#COM_LOGO#", logoUrl);
                }
                else
                {
                    Orders objOrd = new Orders();
                    objOrd.PopulateObject(ordID);

                    printTemplet = printTemplet.Replace("#PRINTDATE#", CommonResource.ResourceValue("POSDate", cultureName) + ": " + DateTime.Now.ToString("MM/dd/yyyy"));
                    printTemplet = printTemplet.Replace("#PRINTTIME#", CommonResource.ResourceValue("POSTime", cultureName) + ": " + DateTime.Now.ToString("HH:mm:ss"));

                    SysCompanyInfo objCompany = new SysCompanyInfo();
                    SysWarehouses objWhrHouse = new SysWarehouses();
                    objWhrHouse.PopulateObject(objOrd.OrdShpWhsCode);
                    objCompany.PopulateObject(BusinessUtility.GetInt(objWhrHouse.WarehouseCompanyID));
                    printTemplet = printTemplet.Replace("#COMPNAME#", objCompany.CompanyName);
                    //printTemplet = printTemplet.Replace("#COMPPHONE#", objCompany.CompanyPOPhone);
                    printTemplet = printTemplet.Replace("#COMPPHONE#", "");

                    SysRegister objRegister = new SysRegister();
                    objRegister.PopulateObject(BusinessUtility.GetString(objOrd.OrdRegCode));
                    //printTemplet = printTemplet.Replace("#COMPADD#", objRegister.SysRegAddress + "<BR/>" + objRegister.SysRegCity + " " + objRegister.SysRegState + "<BR/>" + objCompany.CompanyCountry + " " + objRegister.SysRegPostalCode);
                    string sCompAddr = "";
                    sCompAddr = objRegister.SysRegAddress + "<BR/>" + objRegister.SysRegCity + " " + objRegister.SysRegState + "<BR/>" + objCompany.CompanyCountry + " " + objRegister.SysRegPostalCode;
                    if (BusinessUtility.GetString(objRegister.SysRegPhone) != "")
                    {
                        sCompAddr = sCompAddr + "</br>PH.- " + BusinessUtility.GetString(objRegister.SysRegPhone);
                    }
                    printTemplet = printTemplet.Replace("#COMPADD#", sCompAddr);

                    Orders objInvoice = new Orders();
                    objInvoice.GetOrderCustSaleRepName(ordID);
                    printTemplet = printTemplet.Replace("#CUSTNAME#", CommonResource.ResourceValue("lblCustName", cultureName) + ": " + BusinessUtility.GetString(objInvoice.OrdCustName));
                    printTemplet = printTemplet.Replace("#SALEREP#", CommonResource.ResourceValue("lblPrintSalesRep", cultureName) + ": " + BusinessUtility.GetString(objInvoice.OrdCustSaleName));
                    printTemplet = printTemplet.Replace("#TRANSACTIONID#", BusinessUtility.GetString(accRcblID.ToString("D6")));

                    string logoUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/lib/image/") + "Logoprint.png";
                    printTemplet = printTemplet.Replace("#COM_LOGO#", logoUrl);
                }

                //DataTable dt = objInvoice.GetPOSReceiptDetail(invID);
                #region AddInvoiceProductRows

                StringBuilder sbPrdRows = new StringBuilder();
                TableHeaderRow headerRow = new TableHeaderRow();
                headerRow.HorizontalAlign = HorizontalAlign.Right;

                for (int nM = 0; nM != 4; nM++)
                {
                    TableHeaderCell headerTableCell = new TableHeaderCell();
                    headerTableCell.CssClass = "lblPOSPrint";
                    headerTableCell.Font.Size = 7;
                    if (nM == 0)
                    {
                        headerTableCell.Text = CommonResource.ResourceValue("msgPrd", cultureName);
                        headerTableCell.HorizontalAlign = HorizontalAlign.Left;
                        headerTableCell.Width = 85;
                    }
                    else if (nM == 1)
                    {
                        headerTableCell.Text = CommonResource.ResourceValue("msgQty", cultureName);
                        headerTableCell.Width = 32;
                    }
                    else if (nM == 2)
                    {
                        headerTableCell.Text = CommonResource.ResourceValue("msgPrice", cultureName);
                        headerTableCell.Width = 43;
                    }
                    else if (nM == 3)
                    {
                        headerTableCell.Text = CommonResource.ResourceValue("msgTotalPrice", cultureName);
                        headerTableCell.Width = 70;
                    }
                    headerRow.Cells.Add(headerTableCell);
                }

                StringWriter sw = new StringWriter();
                headerRow.RenderControl(new HtmlTextWriter(sw));
                sbPrdRows.Append(sw.ToString());
                double dblSubTot = 0.0;
                double returnAmt = 0.0;
                string strSubTotal = "";

                StringBuilder sbPrdDtl = new StringBuilder();
                OrderReturn rtn = new OrderReturn();
                DataTable dt = rtn.GetReturnHistoryReceipt(null, ordID, invoiceID,accRcblID);
                //foreach (DataRow drObj in dt.Rows)
                foreach (DataRow vITem in dt.Rows)
                {
                    printTemplet = printTemplet.Replace("#REGMESSAGE#", "");
                    TableRow tempRow = new TableRow();
                    for (int nM = 0; nM != 4; nM++)
                    {
                        TableCell tempCell = new TableCell();
                        tempCell.CssClass = "lblPOSPrint";
                        tempCell.Font.Size = 7;

                        if (nM == 0)
                        {

                            tempCell.Text = BusinessUtility.GetString(vITem[6]) + "<Br />" + BusinessUtility.GetString(vITem[0]);
                            tempCell.HorizontalAlign = HorizontalAlign.Left;
                            tempCell.Width = 85;
                        }
                        else if (nM == 1)
                        {
                            tempCell.Text = BusinessUtility.GetString(vITem[2]);
                            tempCell.HorizontalAlign = HorizontalAlign.Right;
                            tempCell.Width = 32;
                        }
                        else if (nM == 2)
                        {
                            tempCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(vITem[3]), "", cultureName);
                            tempCell.HorizontalAlign = HorizontalAlign.Right;
                            tempCell.Width = 43;
                        }
                        else if (nM == 3)
                        {
                            double pQty = BusinessUtility.GetDouble(vITem[2]);
                            double pAmt = BusinessUtility.GetDouble(vITem[3]);
                            pAmt = (pAmt * pQty);
                            returnAmt += BusinessUtility.GetDouble(vITem[4]);
                            dblSubTot += pAmt;
                            tempCell.Text = CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(pAmt), "", cultureName);
                            tempCell.HorizontalAlign = HorizontalAlign.Right;
                            tempCell.Width = 70;
                        }
                        
                        tempRow.Cells.Add(tempCell);
                        strSubTotal = string.Format("{0:F}", Convert.ToDouble(dblSubTot));
                        strSubTotal = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(strSubTotal), "", cultureName));
                    }

                    StringWriter sProwDtl = new StringWriter();
                    tempRow.RenderControl(new HtmlTextWriter(sProwDtl));
                    sbPrdDtl.Append(sProwDtl.ToString());
                }

                sbPrdRows.Append(sbPrdDtl.ToString());
                printTemplet = printTemplet.Replace("#ITEMROWS#", sbPrdRows.ToString());
                #endregion

                #region SetTaxes
                TotalSummary objCalcHlprNew = new TotalSummary();
                printTemplet = printTemplet.Replace("#TAXROWS#", "");
                #endregion

                #region SetInvoiceTotal
                printTemplet = printTemplet.Replace("#AMOUNTROWS#", "");
                #endregion

                #region SetSubTotal
                StringBuilder sbInvSubTotal = new StringBuilder();
                TableRow RowSub = new TableRow();
                if (returnAmt>0)
                {
                    for (int nM = 0; nM <= 1; nM++)
                    {
                        TableCell tempCell = new TableCell();
                        tempCell.CssClass = "lblPOSPrint";
                        tempCell.Font.Size = 7;
                        if (nM == 0)
                        {
                            tempCell.Text = CommonResource.ResourceValue("msgRefund", cultureName) + "  --";
                            tempCell.HorizontalAlign = HorizontalAlign.Right;
                            tempCell.Width = 165;
                            tempCell.Font.Bold = true;
                        }
                        else if (nM == 1)
                        {
                            strSubTotal = string.Format("{0:F}", (returnAmt).ToString());
                            strSubTotal = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(Convert.ToDouble(strSubTotal), "", cultureName));

                            tempCell.Text = strSubTotal;
                            tempCell.HorizontalAlign = HorizontalAlign.Right;
                            tempCell.Width = 65;
                        }
                        RowSub.Cells.Add(tempCell);
                    }
                }
                else
                {
                    for (int nM = 0; nM <= 1; nM++)
                    {
                        TableCell tempCell = new TableCell();
                        tempCell.CssClass = "lblPOSPrint";
                        tempCell.Font.Size = 7;
                        if (nM == 0)
                        {
                            tempCell.Text = CommonResource.ResourceValue("msgSubTotal", cultureName) + "  --";
                            tempCell.HorizontalAlign = HorizontalAlign.Right;
                            tempCell.Width = 165;
                            tempCell.Font.Bold = true;
                        }
                        else if (nM == 1)
                        {
                            tempCell.Text = strSubTotal;
                            tempCell.HorizontalAlign = HorizontalAlign.Right;
                            tempCell.Width = 65;
                        }
                        RowSub.Cells.Add(tempCell);
                    }
                }
                //tblsubTotal.Rows.Add(RowSub);
                StringWriter sInvSubTotal = new StringWriter();
                RowSub.RenderControl(new HtmlTextWriter(sInvSubTotal));
                sbInvSubTotal.Append(sInvSubTotal.ToString());
                printTemplet = printTemplet.Replace("#TOTALSUBTOTALROWS#", sInvSubTotal.ToString());
                #endregion
            }
            else
            {
                printTemplet = "";
            }
            return printTemplet;
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }
}

public class CommonHtml
{

    public const string ITEM_TOTAL_HTML_FORMAT = "<tr><td valign='top' >{0}:</td><td valign='top' style='width: 150px;'>{1}</td></tr>";
    public const string ITEM_Exchange_HTML_FORMAT = "<tr><td valign='top' >{0}:</td><td valign='top' style='width: 150px;'><input  type='text' id='txtExchangeRate' class='SetExchangeRate'  style='width:100px;text-align:right;' value='{1}' /></td></tr>"; //onkeyup='SetExchangeRate()'

    public static string GetItemTotalHtml(TotalSummary ts, int customerID, int ordID)
    {
        string strData = string.Empty;
        //Items
        //BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName)
        strData += string.Format(ITEM_TOTAL_HTML_FORMAT, CommonResource.ResourceValue("lblSubTotalItems"), string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.ItemSubTotal, Globals.CurrentCultureName)));

        foreach (var item in ts.ItemTaxList)
        {
            strData += string.Format(ITEM_TOTAL_HTML_FORMAT, item.TaxCode, string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(item.CalculatedPrice, Globals.CurrentCultureName)));
        }
        if (ts.ItemTaxList.Count <= 0)
        {
            strData += string.Format(ITEM_TOTAL_HTML_FORMAT, CommonResource.ResourceValue("lblTaxes"), string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(0.0D, Globals.CurrentCultureName)));
        }
        strData += string.Format("<tr><td valign='top' style='border-top:1px solid #ccc;' >{0}:</td><td valign='top' style='width: 90px;border-top:1px solid #ccc;'>{1}</td></tr>", CommonResource.ResourceValue("lblTotal"), string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.ItemSubTotal + ts.TotalItemTax, Globals.CurrentCultureName)));

        if (ordID > 0)
        {
            Orders _ord = new Orders();
            DbHelper dbHelp = new DbHelper(true);
            _ord.PopulateObject(dbHelp, ordID);

            if (BusinessUtility.GetString(_ord.OrderTypeCommission) != "2")
            {
                strData += string.Format(ITEM_Exchange_HTML_FORMAT, CommonResource.ResourceValue("ExchangeRates"), string.Format("{0} {1:F}", null, BusinessUtility.GetCurrencyString(_ord.OrdCurrencyExRate, Globals.CurrentCultureName))).Replace(" $", " ").Replace(" €", " ");
            }
        }
        else
        {
            SysCurrencies cur = new SysCurrencies();
            cur.PopulateObject(ts.CurrencyCode);
            strData += string.Format(ITEM_Exchange_HTML_FORMAT, CommonResource.ResourceValue("ExchangeRates"), string.Format("{0} {1:F}", null, BusinessUtility.GetCurrencyString(cur.CurrencyRelativePrice, Globals.CurrentCultureName))).Replace(" $", " ").Replace(" €", " ");
        }

        return CurrencyFormat.GetReplaceCurrencySymbol(strData);
    }


    public static string GetPoItemTotalHtml(TotalSummary ts, int poID)
    {

        string strData = string.Empty;
        SysCompanyInfo ci = new SysCompanyInfo();
        ci.PopulateObject(CurrentUser.DefaultCompanyID);


        SysCurrencies cur = new SysCurrencies();
        cur.PopulateObject(ts.CurrencyCode);
        if (ts.ItemSubTotal > 0)
        {

            double currencyRate = 0.00;
            if (poID > 0)
            {
                PurchaseOrders pOrder = new PurchaseOrders();
                DbHelper dbHelp = new DbHelper(true);
                pOrder.PopulateObject(dbHelp, poID);
                //strData += string.Format(ITEM_Exchange_HTML_FORMAT, CommonResource.ResourceValue("ExchangeRates"), string.Format("{0} {1:F}", null, BusinessUtility.GetCurrencyString(pOrder.PoCurrencyExRate, Globals.CurrentCultureName))).Replace(" $", " ").Replace(" €", " ");
                currencyRate = pOrder.PoCurrencyExRate;
            }
            else
            {
                currencyRate = cur.CurrencyRelativePrice;
            }


            if (ts.CurrencyCode != ci.CompanyBasCur)
            {
                //if ((BusinessUtility.GetDouble(ts.ItemSubTotal) * BusinessUtility.GetDouble(cur.CurrencyRelativePrice) > 0))
                //{
                //    strData += string.Format(ITEM_TOTAL_HTML_FORMAT, CommonResource.ResourceValue("lblTotalAmountVendorCurrency"), string.Format("<font size='2px'><b>{0} {1:F}</b></font>", ts.CurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(ts.ItemSubTotal) * BusinessUtility.GetDouble(cur.CurrencyRelativePrice), Globals.CurrentCultureName))).Replace(" $", " ").Replace(" €", " ");
                //}
                if ((BusinessUtility.GetDouble(ts.ItemSubTotal) * BusinessUtility.GetDouble(currencyRate) > 0))
                {
                    strData += string.Format(ITEM_TOTAL_HTML_FORMAT, CommonResource.ResourceValue("lblTotalAmountVendorCurrency"), string.Format("<font size='2px'><b>{0} {1:F}</b></font>", ts.CurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(ts.ItemSubTotal) * BusinessUtility.GetDouble(currencyRate), Globals.CurrentCultureName))).Replace(" $", " ").Replace(" €", " ");
                }
            }
            strData += string.Format(ITEM_TOTAL_HTML_FORMAT, CommonResource.ResourceValue("lblTotalAmount") + " " + ci.CompanyBasCur, string.Format("{0} {1:F}", ci.CompanyBasCur, BusinessUtility.GetCurrencyString(ts.ItemSubTotal, Globals.CurrentCultureName))).Replace(" $", " ").Replace(" €", " ");

            if (poID > 0)
            {
                //PurchaseOrders pOrder = new PurchaseOrders();
                //DbHelper dbHelp = new DbHelper(true);
                //pOrder.PopulateObject(dbHelp, poID);
                //strData += string.Format(ITEM_Exchange_HTML_FORMAT, CommonResource.ResourceValue("ExchangeRates"), string.Format("{0} {1:F}", null, BusinessUtility.GetCurrencyString(pOrder.PoCurrencyExRate, Globals.CurrentCultureName))).Replace(" $", " ").Replace(" €", " ");
                //strData += string.Format(ITEM_Exchange_HTML_FORMAT, CommonResource.ResourceValue("ExchangeRates"), string.Format("{0} {1:F}", null, BusinessUtility.GetCurrencyString(currencyRate, Globals.CurrentCultureName))).Replace(" $", " ").Replace(" €", " ");
                strData += string.Format(ITEM_Exchange_HTML_FORMAT, CommonResource.ResourceValue("ExchangeRates"), string.Format("{0} {1:F}", null, BusinessUtility.GetString(currencyRate))).Replace(" $", " ").Replace(" €", " ");
            }
            else
            {
                if (ts.CurrencyCode != ci.CompanyBasCur)
                {
                    if (cur.CurrencyRelativePrice > 0)
                    {
                        //strData += string.Format(ITEM_Exchange_HTML_FORMAT, CommonResource.ResourceValue("ExchangeRates"), string.Format("{0} {1:F}", null, BusinessUtility.GetCurrencyString(cur.CurrencyRelativePrice, Globals.CurrentCultureName))).Replace(" $", " ").Replace(" €", " ");
                        strData += string.Format(ITEM_Exchange_HTML_FORMAT, CommonResource.ResourceValue("ExchangeRates"), string.Format("{0} {1:F}", null, BusinessUtility.GetString(currencyRate))).Replace(" $", " ").Replace(" €", " ");
                    }
                }
            }
        }
        return CurrencyFormat.GetReplaceCurrencySymbol(strData);
    }


    public static string GetProcessTotalHtml(TotalSummary ts)
    {
        string strData = string.Empty;
        //Items
        strData += string.Format(ITEM_TOTAL_HTML_FORMAT, CommonResource.ResourceValue("lblSubTotalServices"), string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.ProcessSubTotal, Globals.CurrentCultureName)));

        foreach (var item in ts.ProcessTaxList)
        {
            strData += string.Format(ITEM_TOTAL_HTML_FORMAT, item.TaxCode, string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(item.CalculatedPrice, Globals.CurrentCultureName)));
        }

        if (ts.ProcessTaxList.Count <= 0)
        {
            strData += string.Format(ITEM_TOTAL_HTML_FORMAT, CommonResource.ResourceValue("lblTaxes"), string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(0.0D, Globals.CurrentCultureName)));
        }

        strData += string.Format("<tr><td valign='top' style='border-top:1px solid #ccc;' >{0}:</td><td valign='top' style='width: 90px;border-top:1px solid #ccc;'>{1}</td></tr>", CommonResource.ResourceValue("lblTotal"), string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.ProcessSubTotal + ts.TotalProcessTax, Globals.CurrentCultureName)));

        return CurrencyFormat.GetReplaceCurrencySymbol(strData);
    }

    public static string GetGrandTotalHtml(TotalSummary ts)
    {
        string strData = string.Empty;
        //Items
        strData += string.Format(ITEM_TOTAL_HTML_FORMAT, CommonResource.ResourceValue("lblSubTotalOrder"), string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.ItemSubTotal + ts.ProcessSubTotal + ts.TotalItemTax + ts.TotalProcessTax, Globals.CurrentCultureName)));
        //strData += string.Format(rowTemplate, this.Labels[LabelKey.TotalTax], string.Format("{0} {1:F}", this.CurrencyCode, this.TotalItemTax + this.TotalProcessTax));
        if (ts.AdditionalDiscount > 0)
        {
            strData += string.Format(ITEM_TOTAL_HTML_FORMAT, CommonResource.ResourceValue("lblDiscount"), string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.AdditionalDiscount, Globals.CurrentCultureName)));
        }
        strData += string.Format(ITEM_TOTAL_HTML_FORMAT, CommonResource.ResourceValue("lblReceivedAmount"), string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.TotalAmountReceived, Globals.CurrentCultureName)));
        if (ts.TotalReturnedAmount > 0)
        {
            strData += string.Format(ITEM_TOTAL_HTML_FORMAT, CommonResource.ResourceValue("lblReturnedAmount"), string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.TotalReturnedAmount, Globals.CurrentCultureName)));
        }
        strData += string.Format("<tr><td valign='top' style='border-top:1px solid #ccc;' >{0}:</td><td valign='top' style='width: 90px;border-top:1px solid #ccc;'>{1}</td></tr>", CommonResource.ResourceValue("lblInvoiceBalance"), string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(ts.OutstandingAmount, Globals.CurrentCultureName)));
        //strData += string.Format(rowTemplate, this.Labels[LabelKey.GrandTotal], string.Format("{0} {1:F}", this.CurrencyCode, this.GrandTotal));
        //strData += string.Format(rowTemplate, this.Labels[LabelKey.ReceivedAmount], string.Format("{0} {1:F}", this.CurrencyCode, this.TotalAmountReceived));
        //strData += string.Format(rowTemplate, this.Labels[LabelKey.BalanceAmount], string.Format("{0} {1:F}", this.CurrencyCode, this.OutstandingAmount));          

        return CurrencyFormat.GetReplaceCurrencySymbol(strData);
    }
}


public class CurrencyFormat
{
    public static string GetCompanyCurrencyFormat(double Amount)
    {
        //strData += string.Format(ITEM_TOTAL_HTML_FORMAT, Resources.Resource.lblTotalAmountVendorCurrency, string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(ts.ItemSubTotal) * BusinessUtility.GetDouble(cur.CurrencyRelativePrice), Globals.CurrentCultureName))).Replace(" $", " ").Replace(" €", " ");
        string strData = string.Empty;
        SysCompanyInfo ci = new SysCompanyInfo();
        ci.PopulateObject(CurrentUser.DefaultCompanyID);
        return strData += string.Format(string.Format("{0} {1:F}", ci.CompanyBasCur, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(Amount), Globals.CurrentCultureName))).Replace(" $", " ").Replace(" €", " ");
    }


    public static string GetCompanyCurrencyFormat(double Amount, string CurrencyCode)
    {
        //strData += string.Format(ITEM_TOTAL_HTML_FORMAT, Resources.Resource.lblTotalAmountVendorCurrency, string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(ts.ItemSubTotal) * BusinessUtility.GetDouble(cur.CurrencyRelativePrice), Globals.CurrentCultureName))).Replace(" $", " ").Replace(" €", " ");
        string strData = string.Empty;
        //SysCompanyInfo ci = new SysCompanyInfo();
        //ci.PopulateObject(CurrentUser.DefaultCompanyID);
        return strData += string.Format(string.Format("{0} {1:F}", CurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(Amount), Globals.CurrentCultureName))).Replace(" $", " ").Replace(" €", " ");
    }

    public static string GetCompanyCurrencyFormat(double Amount, string CurrencyCode, string CultureName)
    {
        //strData += string.Format(ITEM_TOTAL_HTML_FORMAT, Resources.Resource.lblTotalAmountVendorCurrency, string.Format("{0} {1:F}", ts.CurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(ts.ItemSubTotal) * BusinessUtility.GetDouble(cur.CurrencyRelativePrice), Globals.CurrentCultureName))).Replace(" $", " ").Replace(" €", " ");
        string strData = string.Empty;
        //SysCompanyInfo ci = new SysCompanyInfo();
        //ci.PopulateObject(CurrentUser.DefaultCompanyID);
        return strData += string.Format(string.Format("{0} {1:F}", CurrencyCode, BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(Amount), CultureName))).Replace(" $", " ").Replace(" €", " ");
    }


    public static string GetReplaceCurrencySymbol(string Amount)
    {
        return Amount.Replace(" $", "").Replace(" €", "").Replace("$", "").Replace("€", "");
    }

    public static string GetAmountInUSCulture(double Amount)
    {
        System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
        //return Amount.ToString(culture);
        return Amount.ToString("N2", culture);
    }
}


public class CommonResource
{
    #region ReadResourceFile
    public static string ResourceValue(string key)
    {
        string resourceValue = string.Empty;
        try
        {
            string resxFile = System.AppDomain.CurrentDomain.BaseDirectory.ToString() + "App_GlobalResources\\" + ResourseFileName();
            //using (ResXResourceReader resxReader = new ResXResourceReader(resxFile))
            using (ResXResourceSet resxSet = new ResXResourceSet(resxFile))
            {
                resourceValue = resxSet.GetString(key);
                //foreach (DictionaryEntry entry in resxReader)
                //{
                //    //if (((string)entry.Key).StartsWith("EarlyAuto"))
                //    //    autos.Add((Automobile)entry.Value);
                //    //else if (((string)entry.Key).StartsWith("Header"))
                //    //    headers.Add((string)entry.Key, (string)entry.Value);
                //}
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            resourceValue = string.Empty;
        }
        return resourceValue;
    }

    public static string ResourceValue(string key, string culterName)
    {
        string resourceValue = string.Empty;
        try
        {
            string resxFile = System.AppDomain.CurrentDomain.BaseDirectory.ToString() + "App_GlobalResources\\" + ResourseFileName(culterName);
            //using (ResXResourceReader resxReader = new ResXResourceReader(resxFile))
            using (ResXResourceSet resxSet = new ResXResourceSet(resxFile))
            {
                resourceValue = resxSet.GetString(key);
                //foreach (DictionaryEntry entry in resxReader)
                //{
                //    //if (((string)entry.Key).StartsWith("EarlyAuto"))
                //    //    autos.Add((Automobile)entry.Value);
                //    //else if (((string)entry.Key).StartsWith("Header"))
                //    //    headers.Add((string)entry.Key, (string)entry.Value);
                //}
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            resourceValue = string.Empty;
        }
        return resourceValue;
    }

    private static string ResourseFileName()
    {
        if (Globals.CurrentCultureName == "fr-CA")
        {
            return "Resource.fr-CA.resx";
        }
        else
        {
            return "Resource.resx";
        }
    }

    private static string ResourseFileName(string cultureName)
    {
        if (cultureName == "fr-CA")
        {
            return "Resource.fr-CA.resx";
        }
        else
        {
            return "Resource.resx";
        }
    }
    #endregion
}
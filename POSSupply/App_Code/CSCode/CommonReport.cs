﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;

using iTECH.Library.Utilities;

/// <summary>
/// Summary description for CommonReport
/// </summary>
public class CommonReport
{
    public static string GetReportConfigurationXmlPath(ReportKey key) {
        switch (key)
        {            
            case ReportKey.SalesByRegister:
                return "~/NewReport/xml/SalesByRegister.xml";
            case ReportKey.TaxCollectedByRegister:
                return "";
            case ReportKey.SalesByProduct:
                return "";
            case ReportKey.WasteReport:
                return "";
            case ReportKey.OpenOrderFullfillment:
                return "~/NewReport/xml/OpenOrderFulfillment.xml";
            case ReportKey.InventoryByProduct:
                return "~/NewReport/xml/ShortageInventory.xml";
            case ReportKey.VendorPOHistory:
                return "~/NewReport/xml/VendorPOHistory.xml";
            case ReportKey.PreviousWeekCount:
                return "~/NewReport/xml/PreviousWeekCount.xml";
            case ReportKey.MealCountReport:
                return "~/NewReport/xml/MealCountReport.xml";
            case ReportKey.SpecialGuestList:
                return "~/NewReport/xml/SpecialGuestList.xml";
            case ReportKey.Housekeeping:
                return "~/NewReport/xml/Housekeeping.xml";
            case ReportKey.MealCountReport_PM:
                return "~/NewReport/xml/MealCountReport_PM.xml";
            case ReportKey.StaffList:
                return "~/NewReport/xml/StaffList.xml";
            case ReportKey.SalesByProductForAssociatedVendor:
                return "~/NewReport/xml/SalesByProductForVendor.xml";
            case ReportKey.ShuttleServiceReport:
                return "~/NewReport/xml/ShuttleServiceReport.xml";
            case ReportKey.GuestList:
            default:
                return "~/NewReport/xml/GuestList.xml";
        }
    }

    public static void SendReport(string emailIds, string sub, string msg, string attachment)
    {
        try
        {
            string[] emails = emailIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            string strMailFrom = System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"];
            string emailPtternToMatch = "";
            foreach (var item in emails)
            {
                if (IsRegexPatternValid(item, emailPtternToMatch))
                {
                    EmailHelper.SendEmail(strMailFrom, item, msg, sub, attachment);
                }
            }
        }
        catch
        {
            throw;
        }        
    }

    public static bool IsRegexPatternValid(string input, string pattern)
    {
        try
        {
            return System.Text.RegularExpressions.Regex.IsMatch(input, pattern);
        }
        catch { }
        return false;
    }

    public static bool CreateReportWorkBook(DataTable dt, string filename)
    {
        try
        {
            if (dt == null)
                return false;

            string sTableStart = @"<HTML><BODY><TABLE Border=1>";
            string sTableEnd = @"</TABLE></BODY></HTML>";
            string sTHead = "<TR>";
            StringBuilder sTableData = new StringBuilder();
            foreach (DataColumn col in dt.Columns)
            {
                sTHead += @"<TH>" + col.ColumnName + @"</TH>";
            }
            sTHead += @"</TR>";
            foreach (DataRow row in dt.Rows)
            {
                sTableData.Append(@"<TR>");
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    sTableData.Append(@"<TD>" + row[i].ToString() + @"</TD>");
                }
                sTableData.Append(@"</TR>");
            }
            string sTable = sTableStart + sTHead + sTableData.ToString() + sTableEnd;
            using (System.IO.StreamWriter oExcelWriter = System.IO.File.CreateText(filename))
            {
                oExcelWriter.WriteLine(sTable);
                oExcelWriter.Close();
                oExcelWriter.Dispose();
            }

            return true;
        }
        catch
        {
            throw;
        }
    }
}

public enum ReportKey
{
    Default = 12,
    SalesByRegister = 1,
    TaxCollectedByRegister = 2,
    SalesByProduct = 3,
    WasteReport = 4,
    OpenOrderFullfillment = 5,
    InventoryByProduct = 6,
    VendorPOHistory = 7,
    PreviousWeekCount = 8,
    MealCountReport = 9,
    SpecialGuestList = 10,
    Housekeeping = 11,
    GuestList = 12,
    MealCountReport_PM = 13,
    StaffList = 14,
    SalesByProductForAssociatedVendor = 15,
    ShuttleServiceReport = 16
}
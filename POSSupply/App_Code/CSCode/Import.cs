﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using System.Data;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using System.Text;
using MySql.Data.MySqlClient;

/// <summary>
/// Summary description for Import
/// </summary>
public class Import
{
    public Import()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string ImportErrorMessage { get; set; }
    public int ImportErrorCount { get; set; }
    public int ImportSuccessCount { get; set; }


    public int UpdateStyleProduct(string CSVFolderPath, string CSVFile)
    {
        string ProductID = "";
        string ColorID = "";
        string Name = "";
        string Description_Fr = "";
        string Description_En = "";
        string Col_ID = "";
        string Style = "";
        string MatID = "";
        string Gauge = "";
        string TextureID = "";
        string WebSite_CatgeoryID = "";
        string Website_SubCatg_ID = "";
        string Neckline_ID = "";
        string Extra_Catg_ID = "";
        string Sleeve_ID = "";
        string Silhouette_ID = "";
        string Trend_ID = "";
        string Keywords_ID = "";
        string LengthSize = "";
        string LengthIncrement = "";
        string Chest = "";
        string Chest_Increment = "";
        string Shoulder = "";
        string ShoulderIncrement = "";
        string Biceps = "";
        string BicepsIncrement = "";
        string FOBPrice = "";
        string LandedPrice = "";
        string WholeSalePrice = "";
        string RetailPrice = "";
        string WebPrice = "";
        string ProdWgt = "";
        string ProdWgtIncrement = "";
        string PackageWidth = "";
        string PackageLength = "";
        string IsWebActive = "";
        string IsPOSMenu = "";
        string prdUPCCode = "";
        string Size = "";
        string WareHouse = "";



        bool isFirstRowHeader = true;
        string header = isFirstRowHeader ? "Yes" : "No";
        string pathOnly = CSVFolderPath;
        string fileName = CSVFile;
        string sql = @"SELECT * FROM [" + fileName + "]";
        DataTable dataTable = new DataTable();
        //using (OleDbConnection connection = new OleDbConnection(
        //          @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathOnly +
        //          ";Extended Properties=\"Text;HDR=" + header + "\""))

        //using (OleDbCommand command = new OleDbCommand(sql, connection))
        //using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
        //{
        //    adapter.Fill(dataTable);
        //}
        pathOnly = pathOnly + CSVFile;
        dataTable = ExcelHelper.exceldata(pathOnly);
        //return 0;

        int iRow = 0;
        foreach (DataRow dRowPDetail in dataTable.Rows)
        {
            int iCol = 0;
            foreach (DataColumn dc in dataTable.Columns)
            {
                dataTable.Rows[iRow][iCol] = BusinessUtility.GetString(dataTable.Rows[iRow][iCol]).Replace("'", "");
                dataTable.AcceptChanges();
                iCol += 1;
            }
            iRow += 1;
        }
        int iProductUpdate = 0;
        List<ErrorDetails> lstErrDtl = new List<ErrorDetails>();
        foreach (DataRow dRowPDetail in dataTable.Rows)
        {
            //ProductID = BusinessUtility.GetString(dRowPDetail["ColorID"]);
            ColorID = BusinessUtility.GetString(dRowPDetail["ColorID"]);
            if (ColorID == "")
            {
                Name = BusinessUtility.GetString(dRowPDetail["Name"]);
                Description_Fr = BusinessUtility.GetString(dRowPDetail["Description_Fr"]);
                Description_En = BusinessUtility.GetString(dRowPDetail["Description_En"]);
                Col_ID = BusinessUtility.GetString(dRowPDetail["Col_ID"]);
                Style = BusinessUtility.GetString(dRowPDetail["Style"]);
                MatID = BusinessUtility.GetString(dRowPDetail["MatID"]);
                Gauge = BusinessUtility.GetString(dRowPDetail["Gauge"]);
                TextureID = BusinessUtility.GetString(dRowPDetail["TextureID"]);
                WebSite_CatgeoryID = BusinessUtility.GetString(dRowPDetail["WebSite_CatgeoryID"]);
                Website_SubCatg_ID = BusinessUtility.GetString(dRowPDetail["Website_SubCatg_ID"]);
                Neckline_ID = BusinessUtility.GetString(dRowPDetail["Neckline_ID"]);
                Extra_Catg_ID = BusinessUtility.GetString(dRowPDetail["Extra_Catg_ID"]);
                Sleeve_ID = BusinessUtility.GetString(dRowPDetail["Sleeve_ID"]);
                Silhouette_ID = BusinessUtility.GetString(dRowPDetail["Silhouette_ID"]);
                Trend_ID = BusinessUtility.GetString(dRowPDetail["Trend_ID"]);
                Keywords_ID = BusinessUtility.GetString(dRowPDetail["Keywords_ID"]);
                LengthSize = BusinessUtility.GetString(dRowPDetail["LengthSize"]);
                Chest = BusinessUtility.GetString(dRowPDetail["Chest"]);
                Shoulder = BusinessUtility.GetString(dRowPDetail["Shoulder"]);
                Biceps = BusinessUtility.GetString(dRowPDetail["Biceps"]);
                LengthIncrement = BusinessUtility.GetString(dRowPDetail["LengthIncrement"]);
                Chest_Increment = BusinessUtility.GetString(dRowPDetail["Chest_Increment"]);
                ShoulderIncrement = BusinessUtility.GetString(dRowPDetail["ShoulderIncrement"]);
                BicepsIncrement = BusinessUtility.GetString(dRowPDetail["BicepsIncrement"]);
                FOBPrice = BusinessUtility.GetString(dRowPDetail["FOBPrice"]);
                LandedPrice = BusinessUtility.GetString(dRowPDetail["LandedPrice"]);
                WholeSalePrice = BusinessUtility.GetString(dRowPDetail["WholeSalePrice"]);
                RetailPrice = BusinessUtility.GetString(dRowPDetail["RetailPrice"]);
                WebPrice = BusinessUtility.GetString(dRowPDetail["WebPrice"]);
                ProdWgt = BusinessUtility.GetString(dRowPDetail["ProdWgt"]);
                ProdWgtIncrement = BusinessUtility.GetString(dRowPDetail["ProdWgtIncrement"]);
                PackageWidth = BusinessUtility.GetString(dRowPDetail["PackageWidth"]);
                PackageLength = BusinessUtility.GetString(dRowPDetail["PackageLength"]);
                IsWebActive = BusinessUtility.GetString(dRowPDetail["IsWebActive"]);
                IsPOSMenu = BusinessUtility.GetString(dRowPDetail["IsPOSMenu"]);
                WareHouse = BusinessUtility.GetString(dRowPDetail["Warehouse"]);
                //prdUPCCode = BusinessUtility.GetString(dRowPDetail["prdUPCCode"]);
                //Size = BusinessUtility.GetString(dRowPDetail["ColorID"]);
            }
            else if (ColorID != "")
            {
                int iProdColDetal = 38;
                string sLength = "";
                string sChestLength = "";
                string sSoldLength = "";
                string sBicepLength = "";
                string sProdWeight = "";
                sLength = BusinessUtility.GetString(BusinessUtility.GetDouble(LengthSize));
                sChestLength = BusinessUtility.GetString(BusinessUtility.GetDouble(Chest));
                sSoldLength = BusinessUtility.GetString(BusinessUtility.GetDouble(Shoulder));
                sBicepLength = BusinessUtility.GetString(BusinessUtility.GetDouble(Biceps));
                sProdWeight = BusinessUtility.GetString(BusinessUtility.GetDouble(ProdWgt));
                int iRowCnt = 0;

                for (iProdColDetal = 38; iProdColDetal != dataTable.Columns.Count; iProdColDetal++)
                {
                    DataColumn dColumn = dataTable.Columns[iProdColDetal];
                    //prdUPCCode = BusinessUtility.GetString(dRowPDetail["prdUPCCode"]);
                    string sPQty = "";
                    if (dColumn.ColumnName.Contains("SKU"))
                    {
                        prdUPCCode = BusinessUtility.GetString(dRowPDetail[BusinessUtility.GetString(dColumn.ColumnName)]);
                        sLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sLength) + (BusinessUtility.GetDouble(LengthIncrement) * iRowCnt));
                        sChestLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sChestLength) + (BusinessUtility.GetDouble(Chest_Increment) * iRowCnt));
                        sSoldLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sSoldLength) + (BusinessUtility.GetDouble(ShoulderIncrement) * iRowCnt));
                        sBicepLength = BusinessUtility.GetString(BusinessUtility.GetDouble(sBicepLength) + (BusinessUtility.GetDouble(BicepsIncrement) * iRowCnt));
                        sProdWeight = BusinessUtility.GetString(BusinessUtility.GetDouble(sProdWeight) + (BusinessUtility.GetDouble(ProdWgtIncrement) * iRowCnt));
                        iRowCnt = +1;
                    }
                    if (dColumn.ColumnName.Contains("Qty"))
                    {
                        sPQty = BusinessUtility.GetString(dRowPDetail[BusinessUtility.GetString(dColumn.ColumnName)]);
                        Product objProduct = new Product();
                        if (prdUPCCode != "")
                        {
                            objProduct.PrdUPCCode = prdUPCCode;
                            int iProductID = objProduct.GetProductIDOnUPCCode();
                            if (iProductID > 0)
                            {
                                try
                                {
                                    objProduct.PopulateObject(iProductID);
                                    #region ProdcutUpdate
                                    Product.ProductClothDesc objProductClothDesc = new Product.ProductClothDesc();
                                    Product.ProductClothDesc objGetProductClothDesc = new Product.ProductClothDesc();
                                    objGetProductClothDesc.ProductID = iProductID;
                                    objGetProductClothDesc.getClothDesc(null, 0);

                                    objProduct.ProductID = iProductID;
                                    objProduct.PrdName = Name;
                                    objProduct.PrdFOBPrice = BusinessUtility.GetDouble(FOBPrice);
                                    objProduct.PrdLandedPrice = BusinessUtility.GetDouble(LandedPrice);
                                    objProduct.PrdSalePricePerMinQty = BusinessUtility.GetDouble(WholeSalePrice);
                                    objProduct.PrdEndUserSalesPrice = BusinessUtility.GetDouble(RetailPrice);
                                    objProduct.PrdWebSalesPrice = BusinessUtility.GetDouble(WebPrice);
                                    //objProduct.PrdWeight = BusinessUtility.GetString(ProdWgt);
                                    objProduct.PrdWeight = BusinessUtility.GetString(sProdWeight);
                                    objProduct.PrdWidthPkg = BusinessUtility.GetString(PackageWidth);
                                    objProduct.PrdLengthPkg = BusinessUtility.GetString(PackageLength);
                                    objProduct.PrdHeight = objProduct.PrdHeight;
                                    objProduct.PrdHeightPkg = objProduct.PrdHeightPkg;
                                    objProduct.PrdLength = sLength;
                                    objProduct.PrdWidth = objProduct.PrdWidth;
                                    objProduct.PrdWeightPkg = objProduct.PrdWeightPkg;
                                    if (BusinessUtility.GetString(IsWebActive) == "1") { objProduct.PrdIsWeb = true; }
                                    else { objProduct.PrdIsWeb = true; }

                                    if (BusinessUtility.GetString(IsPOSMenu) == "1") { objProduct.IsPOSMenu = true; }
                                    else { objProduct.IsPOSMenu = true; }



                                    objProduct.PrdIntID = objProduct.PrdIntID;
                                    objProduct.PrdExtID = objProduct.PrdExtID;
                                    objProduct.PrdType = objProduct.PrdType;
                                    objProduct.PrdUPCCode = objProduct.PrdUPCCode;
                                    objProduct.PrdIsKit = objProduct.PrdIsKit;
                                    objProduct.PrdMinQtyPerSO = objProduct.PrdMinQtyPerSO;
                                    objProduct.PrdInoclTerms = objProduct.PrdInoclTerms;
                                    objProduct.PrdIsSpecial = objProduct.PrdIsSpecial;
                                    objProduct.PrdDiscount = objProduct.PrdDiscount;
                                    objProduct.PrdMinQtyPOTrig = objProduct.PrdMinQtyPOTrig;
                                    objProduct.PrdPOQty = objProduct.PrdPOQty;
                                    objProduct.PrdIsActive = objProduct.PrdIsActive;
                                    objProduct.PrdComissionCode = objProduct.PrdComissionCode;
                                    objProduct.PrdCategory = objProduct.PrdCategory;
                                    objProduct.PrdSubcategory = objProduct.PrdSubcategory;
                                    objProduct.PrdDiscountType = objProduct.PrdDiscountType;
                                    objProduct.PrdAccommodationType = objProduct.PrdAccommodationType;
                                    objProduct.PrdExtendedCategory = objProduct.PrdExtendedCategory;
                                    objProduct.PrdIsGlutenFree = objProduct.PrdIsGlutenFree;
                                    objProduct.PrdIsVegetarian = objProduct.PrdIsVegetarian;
                                    objProduct.PrdIsContainsNuts = objProduct.PrdIsContainsNuts;
                                    objProduct.PrdIsCookedSushi = objProduct.PrdIsCookedSushi;
                                    objProduct.PrdSpicyLevel = objProduct.PrdSpicyLevel;


                                    objProductClothDesc.ProductID = iProductID;
                                    objProductClothDesc.MasterID = BusinessUtility.GetInt(objProduct.MasterID);
                                    objProductClothDesc.CollectionID = BusinessUtility.GetInt(Col_ID);
                                    //objProductClothDesc.Gauge = BusinessUtility.GetInt(Gauge);
                                    objProductClothDesc.Gauge = BusinessUtility.GetString(Gauge);
                                    objProductClothDesc.Color = BusinessUtility.GetInt(ColorID);
                                    objProductClothDesc.Style = BusinessUtility.GetString(Style);
                                    objProductClothDesc.Size = BusinessUtility.GetInt(objGetProductClothDesc.Size);
                                    objProductClothDesc.LongueurLength = BusinessUtility.GetString(sLength);
                                    objProductClothDesc.EpauleShoulder = BusinessUtility.GetString(sSoldLength);
                                    objProductClothDesc.BusteChest = BusinessUtility.GetString(sChestLength);
                                    objProductClothDesc.Bicep = BusinessUtility.GetString(sBicepLength);
                                    objProductClothDesc.Material = BusinessUtility.GetInt(MatID);
                                    objProductClothDesc.Neckline = Neckline_ID;
                                    objProductClothDesc.Texture = TextureID;
                                    objProductClothDesc.ExtraCatg = Extra_Catg_ID;
                                    objProductClothDesc.Sleeve = Sleeve_ID;
                                    objProductClothDesc.Silhouette = Silhouette_ID;
                                    objProductClothDesc.Trend = Trend_ID;
                                    objProductClothDesc.KeyWord = Keywords_ID;
                                    objProductClothDesc.WebSiteCatg = WebSite_CatgeoryID;
                                    objProductClothDesc.WebSiteSubCatg = Website_SubCatg_ID;
                                    objProductClothDesc.WebAssociatedStyle = "";
                                    objProductClothDesc.PrdName = BusinessUtility.GetString(objProduct.PrdName);
                                    objProductClothDesc.PrdEnDesc = BusinessUtility.GetString(Description_En);
                                    objProductClothDesc.PrdFrDesc = BusinessUtility.GetString(Description_Fr);


                                    objProduct.Update(CurrentUser.UserID);
                                    objProduct.UpdatePrdOnHandQty(null, iProductID, BusinessUtility.GetInt(sPQty), WareHouse);
                                    InventoryMovment objIM = new InventoryMovment();
                                    objIM.AddMovment(WareHouse, CurrentUser.UserID, iProductID, BusinessUtility.GetString(InvMovmentSrc.SUEXL), BusinessUtility.GetInt(sPQty), BusinessUtility.GetString(InvMovmentUpdateType.ORWT));
                                    objProductClothDesc.InsertUpdateClothDesc();
                                    objProductClothDesc.UpdateProduceDesc();
                                    iProductUpdate += 1;
                                    #endregion
                                    this.ImportSuccessCount += 1;
                                }
                                catch
                                {
                                    lstErrDtl.Add(new ErrorDetails { UPCCode = prdUPCCode, ErrorCode = (int)ImportProductErrorCode.UnkownError });
                                    this.ImportErrorCount += 1;
                                }
                            }
                            else
                            {
                                lstErrDtl.Add(new ErrorDetails { UPCCode = prdUPCCode, ErrorCode = (int)ImportProductErrorCode.SKUCodeNotFound });
                                this.ImportErrorCount += 1;
                            }
                        }
                        else if (prdUPCCode == "" && ColorID != "")
                        {
                            //lstErrDtl.Add(new ErrorDetails { UPCCode = prdUPCCode, ErrorCode = (int)ImportProductErrorCode.InValidSKUCode });
                            //this.ImportErrorCount += 1;
                        }
                    }
                }
            }
        }
        if (this.ImportErrorCount > 0)
        {
            this.ImportErrorMessage = GetErrorMessage(lstErrDtl);
        }
        return iProductUpdate;
    }

    public int UpdateProduct(string CSVFolderPath, string CSVFile, string sUpdateType, string sWhsCode)
    {
        bool isFirstRowHeader = true;
        string header = isFirstRowHeader ? "Yes" : "No";
        string pathOnly = CSVFolderPath;
        string fileName = CSVFile;
        string sql = @"SELECT * FROM [" + fileName + "]";
        DataTable dataTable = new DataTable();
        pathOnly = pathOnly + CSVFile;
        dataTable = ExcelHelper.exceldata(pathOnly);
        int iProductUpdate = 0;
        DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
        dbTransactionHelper.BeginTransaction();

        List<ErrorDetails> lstErrDtl = new List<ErrorDetails>();

        try
        {

            if (sUpdateType.ToUpper() == "RSU")
            {
                string strSql = "update prdquantity set prdOhdQty='0' ";
                strSql += "where  prdWhsCode='" + BusinessUtility.GetString(sWhsCode) + "'"; //prdID='" + iProductID + "' and
                dbTransactionHelper.ExecuteNonQuery(strSql, CommandType.Text, null);
            }

            foreach (DataRow dRow in dataTable.Rows)
            {
                if (BusinessUtility.GetString(dRow["prdUPCCode"]) != "")
                {
                    string strSql = "";
                    Int64 iProductID = 0;
                    if (sUpdateType.ToUpper() == "ALL")
                    {
                        Product objProduct = new Product();
                        try
                        {
                            iProductID = objProduct.GetProductID(null, BusinessUtility.GetString(dRow["prdUPCCode"]));
                        }
                        catch
                        {
                            iProductID = 0;
                        }
                        string sUPCCode = BusinessUtility.GetString(dRow["prdUPCCode"]);
                        if (iProductID > 0)
                        {
                            strSql = "update products set prdUPCCode='" + BusinessUtility.GetString(dRow["prdUPCCode"]) + "', prdName='" + BusinessUtility.GetString(dRow["prdName"]) + "', ";
                            strSql += "prdIntID='" + BusinessUtility.GetString(dRow["prdIntID"]) + "', prdExtID='" + BusinessUtility.GetString(dRow["prdExtID"]) + "', ";
                            strSql += "prdIsGlutenFree='" + BusinessUtility.GetString(dRow["prdIsGlutenFree"]) + "', prdSpicyLevel='" + BusinessUtility.GetString(dRow["prdSpicyLevel"]) + "', ";
                            strSql += "prdIsContainsNuts='" + BusinessUtility.GetString(dRow["prdIsContainsNuts"]) + "', prdIsVegetarian='" + BusinessUtility.GetString(dRow["prdIsVegetarian"]) + "'";

                            if (dataTable.Columns.Contains("Sales_Price"))
                            {
                                strSql += ", prdSalePricePerMinQty='" + BusinessUtility.GetDouble(dRow["Sales_Price"]) + "'";
                            }

                            if (dataTable.Columns.Contains("End_User_Price"))
                            {
                                strSql += ", prdEndUserSalesPrice='" + BusinessUtility.GetDouble(dRow["End_User_Price"]) + "'";
                            }

                            if (dataTable.Columns.Contains("Web_Price"))
                            {
                                strSql += ", prdWebSalesPrice='" + BusinessUtility.GetDouble(dRow["Web_Price"]) + "'";
                            }

                            strSql += " where productID='" + BusinessUtility.GetInt(dRow["productID"]) + "'";
                            dbTransactionHelper.ExecuteNonQuery(strSql, CommandType.Text, null);

                            strSql = "Select count(*) from prdquantity where prdID='" + BusinessUtility.GetInt(dRow["productID"]) + "' and prdWhsCode='" + BusinessUtility.GetString(dRow["Warehouse"]) + "'";
                            //DataTable dt = dbTransactionHelper.GetDataTable(strSql, CommandType.Text, null);
                            //iProductID = BusinessUtility.GetInt(dRow["productID"]);
                            //sWhsCode = BusinessUtility.GetString(dRow["Warehouse"]);

                            object rValue = dbTransactionHelper.GetValue(strSql, CommandType.Text, null);
                            if (BusinessUtility.GetInt(rValue) == 0)
                            {
                                strSql = "insert into prdquantity (prdID, prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty, prdBlock, prdFloor, prdAisle, ";
                                strSql += "prdRack, prdBin) values ('" + BusinessUtility.GetInt(dRow["productID"]) + "', '" + BusinessUtility.GetString(dRow["Warehouse"]) + "', '" + BusinessUtility.GetInt(dRow["prdOhdQty"]) + "', ";
                                strSql += "0, 0, '" + BusinessUtility.GetInt(dRow["prdDefectiveQty"]) + "', '" + BusinessUtility.GetString(dRow["prdBlock"]) + "', '" + BusinessUtility.GetString(dRow["prdFloor"]) + "', ";
                                strSql += "'" + BusinessUtility.GetString(dRow["prdAisle"]) + "', '" + BusinessUtility.GetString(dRow["prdRack"]) + "', '" + BusinessUtility.GetString(dRow["prdBin"]) + "')";
                            }
                            else
                            {
                                strSql = "update prdquantity set prdOhdQty='" + BusinessUtility.GetInt(dRow["prdOhdQty"]) + "', prdDefectiveQty='" + BusinessUtility.GetInt(dRow["prdDefectiveQty"]) + "', ";
                                strSql += "prdBlock='" + BusinessUtility.GetString(dRow["prdBlock"]) + "', prdFloor='" + BusinessUtility.GetString(dRow["prdFloor"]) + "', prdAisle='" + BusinessUtility.GetString(dRow["prdAisle"]) + "', ";
                                strSql += "prdRack='" + BusinessUtility.GetString(dRow["prdRack"]) + "', prdBin='" + BusinessUtility.GetString(dRow["prdBin"]) + "'";
                                strSql += "where prdID='" + BusinessUtility.GetInt(dRow["productID"]) + "' and prdWhsCode='" + BusinessUtility.GetString(dRow["Warehouse"]) + "'";
                            }
                            dbTransactionHelper.ExecuteNonQuery(strSql, CommandType.Text, null);

                            strSql = " INSERT INTO inventorymovement (WhsCode, UserID, CreationDateTime, ProductID, TransactionSourceID, Qty, UpdateType ) ";
                            strSql += " VALUES ( @WhsCode, @UserID, @CreationDateTime, @ProductID, @TransactionSourceID, @Qty, @UpdateType ) ";
                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("WhsCode", BusinessUtility.GetString(dRow["Warehouse"]), MyDbType.String),
                    DbUtility.GetParameter("UserID", CurrentUser.UserID, MyDbType.Int),
                    DbUtility.GetParameter("CreationDateTime", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ProductID", BusinessUtility.GetInt(dRow["productID"]), MyDbType.Int),
                    DbUtility.GetParameter("TransactionSourceID", BusinessUtility.GetString(InvMovmentSrc.SUEXL), MyDbType.String),
                    DbUtility.GetParameter("Qty", BusinessUtility.GetInt(dRow["prdOhdQty"]), MyDbType.Int),
                    DbUtility.GetParameter("UpdateType",BusinessUtility.GetString(InvMovmentUpdateType.ORWT), MyDbType.String)
                    });
                            iProductUpdate += 1;

                        }
                        else
                        {
                            // AddMissProduct
                            lstErrDtl.Add(new ErrorDetails { UPCCode = sUPCCode, ErrorCode = (int)ImportProductErrorCode.UnkownError });
                            this.ImportErrorCount += 1;
                        }
                    }
                    else if ((sUpdateType.ToUpper() == "QTY") || (sUpdateType.ToUpper() == "RSU"))
                    {
                        Product objProduct = new Product();
                        try
                        {
                            iProductID = objProduct.GetProductID(null, BusinessUtility.GetString(dRow["prdUPCCode"]));
                        }
                        catch
                        {
                            iProductID = 0;
                        }
                        string sUPCCode = BusinessUtility.GetString(dRow["prdUPCCode"]);
                        Int64 iQty = BusinessUtility.GetInt(dRow["prdOhdQty"]);

                        if (iProductID > 0)
                        {
                            if (iQty > 0)
                            {
                                strSql = "Select count(*) from prdquantity where prdID='" + iProductID + "' and prdWhsCode='" + BusinessUtility.GetString(sWhsCode) + "'";
                                object rValue = dbTransactionHelper.GetValue(strSql, CommandType.Text, null);
                                if (BusinessUtility.GetInt(rValue) == 0)
                                {
                                    strSql = "insert into prdquantity (prdID, prdWhsCode, prdOhdQty";
                                    strSql += ") values ('" + iProductID + "', '" + BusinessUtility.GetString(sWhsCode) + "', '" + BusinessUtility.GetLong(iQty) + "') ";

                                }
                                else
                                {
                                    strSql = "update prdquantity set prdOhdQty='" + BusinessUtility.GetLong(iQty) + "' ";
                                    strSql += "where prdID='" + iProductID + "' and prdWhsCode='" + BusinessUtility.GetString(sWhsCode) + "'";
                                }
                                dbTransactionHelper.ExecuteNonQuery(strSql, CommandType.Text, null);

                                strSql = " INSERT INTO inventorymovement (WhsCode, UserID, CreationDateTime, ProductID, TransactionSourceID, Qty, UpdateType ) ";
                                strSql += " VALUES ( @WhsCode, @UserID, @CreationDateTime, @ProductID, @TransactionSourceID, @Qty, @UpdateType ) ";
                                dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, new MySqlParameter[]{
                            DbUtility.GetParameter("WhsCode", BusinessUtility.GetString(sWhsCode), MyDbType.String),
                            DbUtility.GetParameter("UserID", CurrentUser.UserID, MyDbType.Int),
                            DbUtility.GetParameter("CreationDateTime", DateTime.Now, MyDbType.DateTime),
                            DbUtility.GetParameter("ProductID", iProductID, MyDbType.Int),
                            DbUtility.GetParameter("TransactionSourceID", BusinessUtility.GetString(InvMovmentSrc.SUEXL), MyDbType.String),
                            DbUtility.GetParameter("Qty", BusinessUtility.GetInt(iQty), MyDbType.Int),
                            DbUtility.GetParameter("UpdateType",BusinessUtility.GetString(InvMovmentUpdateType.ORWT), MyDbType.String)
                            });
                                iProductUpdate += 1;
                            }
                        }
                        else
                        {
                            // AddMissProduct
                            lstErrDtl.Add(new ErrorDetails { UPCCode = sUPCCode, ErrorCode = (int)ImportProductErrorCode.UnkownError });
                            this.ImportErrorCount += 1;
                        }
                    }
                    else if ((sUpdateType.ToUpper() == "INC"))
                    {
                        Product objProduct = new Product();
                        try
                        {
                            iProductID = objProduct.GetProductID(null, BusinessUtility.GetString(dRow["prdUPCCode"]));
                        }
                        catch
                        {
                            iProductID = 0;
                        }
                        string sUPCCode = BusinessUtility.GetString(dRow["prdUPCCode"]);
                        Int64 iQty = BusinessUtility.GetInt(dRow["prdOhdQty"]);
                        if (iProductID > 0)
                        {
                            strSql = "Select count(*) from prdquantity where prdID='" + iProductID + "' and prdWhsCode='" + BusinessUtility.GetString(sWhsCode) + "'";
                            object rValue = dbTransactionHelper.GetValue(strSql, CommandType.Text, null);
                            if (BusinessUtility.GetInt(rValue) == 0)
                            {
                                strSql = "insert into prdquantity (prdID, prdWhsCode, prdOhdQty";
                                strSql += ") values ('" + iProductID + "', '" + BusinessUtility.GetString(sWhsCode) + "', '" + BusinessUtility.GetLong(iQty) + "') ";
                            }
                            else
                            {
                                strSql = "update prdquantity set prdOhdQty= prdOhdQty+ '" + BusinessUtility.GetLong(iQty) + "' ";
                                strSql += "where prdID='" + iProductID + "' and prdWhsCode='" + BusinessUtility.GetString(sWhsCode) + "'";
                            }
                            dbTransactionHelper.ExecuteNonQuery(strSql, CommandType.Text, null);

                            strSql = " INSERT INTO inventorymovement (WhsCode, UserID, CreationDateTime, ProductID, TransactionSourceID, Qty, UpdateType ) ";
                            strSql += " VALUES ( @WhsCode, @UserID, @CreationDateTime, @ProductID, @TransactionSourceID, @Qty, @UpdateType ) ";
                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, new MySqlParameter[]{
                        DbUtility.GetParameter("WhsCode", BusinessUtility.GetString(sWhsCode), MyDbType.String),
                        DbUtility.GetParameter("UserID", CurrentUser.UserID, MyDbType.Int),
                        DbUtility.GetParameter("CreationDateTime", DateTime.Now, MyDbType.DateTime),
                        DbUtility.GetParameter("ProductID", iProductID, MyDbType.Int),
                        DbUtility.GetParameter("TransactionSourceID", BusinessUtility.GetString(InvMovmentSrc.SUEXL), MyDbType.String),
                        DbUtility.GetParameter("Qty", BusinessUtility.GetInt(iQty), MyDbType.Int),
                        DbUtility.GetParameter("UpdateType",BusinessUtility.GetString(InvMovmentUpdateType.ORWT), MyDbType.String)
                        });
                            iProductUpdate += 1;
                        }
                        else
                        {
                            // AddMissProduct
                            lstErrDtl.Add(new ErrorDetails { UPCCode = sUPCCode, ErrorCode = (int)ImportProductErrorCode.UnkownError });
                            this.ImportErrorCount += 1;
                        }
                    }
                }

            }


            dbTransactionHelper.CommitTransaction();
            if (this.ImportErrorCount > 0)
            {
                this.ImportErrorMessage = GetErrorMessage(lstErrDtl);
            }
            return iProductUpdate;
        }
        catch
        {
            dbTransactionHelper.RollBackTransaction();
            throw;
        }
        finally
        {
            dbTransactionHelper.CloseDatabaseConnection();
        }


    }

    private class ErrorDetails
    {
        public string UPCCode { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }

    public string ImportProductStyle(DataSet ds)
    {
        Product _prd = new Product();
        ProductColor prdColor = new ProductColor();
        int intRecords = 0;
        int i = 0;
        bool excutionStatus = false;
        string strColorID = string.Empty;
        string prdName = string.Empty;
        string prdDesFr = string.Empty;
        string prdDesEn = string.Empty;
        string styleName = string.Empty;
        string collID = string.Empty;
        string matID = string.Empty;
        string gaugeID = string.Empty;
        string textureID = string.Empty;
        string webSiteID = string.Empty;
        string webSiteSubCatgID = string.Empty;
        string necklineID = string.Empty;
        string extraCatgID = string.Empty;
        string sleeveID = string.Empty;
        string silhouetteID = string.Empty;
        string trendID = string.Empty;
        string keywordID = string.Empty;
        string lengthSize = string.Empty;
        string lengthInc = string.Empty;
        string chest = string.Empty;
        string chestInc = string.Empty;
        string shoulder = string.Empty;
        string shoulderInc = string.Empty;
        string biceps = string.Empty;
        string bicepsInc = string.Empty;
        string FOB = string.Empty;
        string Landed = string.Empty;
        string WSPrice = string.Empty;
        string RPrice = string.Empty;
        string WebPrice = string.Empty;
        string ProdWgt = string.Empty;
        string ProdWgtIncr = string.Empty;
        string PackageWgt = string.Empty;
        string PackageWidth = string.Empty;
        string PackageLength = string.Empty;
        string IsWebActive = string.Empty;
        string IsPOSMenu = string.Empty;
        string SizeID = string.Empty;
        string prdQty = string.Empty;
        string lngSize = string.Empty;
        string chstSize = string.Empty;
        string sldSize = string.Empty;
        string bcpSize = string.Empty;
        string prdWgtSize = string.Empty;
        int incCounter = 0;
        string dublicateUPCCode = string.Empty;
        string msgErrorCode = string.Empty;
        string currentUPCCode = string.Empty;
        msgErrorCode = "<tr><td colspan='2' style='font-weight:bold; font-size:14px' >" + CommonResource.ResourceValue("lblStyleImportErrorMsg") + "</td></tr><tr><td>SKU #</td><td>" + CommonResource.ResourceValue("lblError") + "</td></tr>";
        List<ErrorDetails> lstErrDtl = new List<ErrorDetails>();
        for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
        {
            try
            {
                strColorID = GetValue(ds, i, "ColorID");
                //if (string.IsNullOrEmpty(strColorID))
                if (BusinessUtility.GetString(strColorID).Trim().ToUpper() == "XXXX" || BusinessUtility.GetString(strColorID).Trim() == "")
                {


                    prdName = GetValue(ds, i, "Name");
                    if (BusinessUtility.GetString(prdName) != "")
                    {
                        WSPrice = GetValue(ds, i, "WholeSalePrice");
                        RPrice = GetValue(ds, i, "RetailPrice");
                        WebPrice = GetValue(ds, i, "WebPrice");
                        FOB = GetValue(ds, i, "FOB").Trim();
                        Landed = GetValue(ds, i, "Landed");
                        collID = GetValue(ds, i, "Season/Col_ID");
                        styleName = GetValue(ds, i, "STYLE");
                        matID = GetValue(ds, i, "Mat-ID");
                        ProdWgt = GetValue(ds, i, "ProdWgt");
                        PackageWgt = GetValue(ds, i, "PackageWgt");
                        PackageWidth = GetValue(ds, i, "PackageWidth");
                        PackageLength = GetValue(ds, i, "PackageLength");
                        IsWebActive = GetValue(ds, i, "IsWebActive");
                        IsPOSMenu = GetValue(ds, i, "IsPOSMenu");

                        prdDesFr = GetValue(ds, i, "Description Fr");
                        prdDesEn = GetValue(ds, i, "Description En");

                        textureID = GetValue(ds, i, "Texture-ID");
                        webSiteID = GetValue(ds, i, "Website Category-ID");
                        webSiteSubCatgID = GetValue(ds, i, "Website Sub Catg-ID");
                        necklineID = GetValue(ds, i, "Neckline-ID");
                        extraCatgID = GetValue(ds, i, "Extra Catg-ID");
                        sleeveID = GetValue(ds, i, "Sleeve-ID");
                        silhouetteID = GetValue(ds, i, "Silhouette-ID");
                        trendID = GetValue(ds, i, "Trend-ID");
                        keywordID = GetValue(ds, i, "Keywords-ID");

                        gaugeID = GetValue(ds, i, "Gauge-ID");
                        lengthSize = GetValue(ds, i, "LengthSize");
                        lengthInc = GetValue(ds, i, "Length Increment");
                        chest = GetValue(ds, i, "Chest");
                        chestInc = GetValue(ds, i, "Chest Inc");
                        shoulder = GetValue(ds, i, "Shoulder");
                        shoulderInc = GetValue(ds, i, "Shoulder Inc");
                        biceps = GetValue(ds, i, "Biceps");
                        bicepsInc = GetValue(ds, i, "Biceps Inc");
                        ProdWgtIncr = GetValue(ds, i, "ProdWgtIncr");
                    }
                }

                //if (string.IsNullOrEmpty(strColorID) == false)
                if (BusinessUtility.GetString(strColorID).Trim().ToUpper() != "XXXX" && BusinessUtility.GetInt(strColorID) > 0)
                {
                    if (!prdColor.ChkColorExists(null, styleName, BusinessUtility.GetInt(collID), BusinessUtility.GetInt(strColorID)))
                    {
                        for (int j = 37; j <= ds.Tables[0].Columns.Count - 1; j++)
                        {
                            try
                            {
                                SizeID = ds.Tables[0].Columns[j].ColumnName;
                                prdQty = ds.Tables[0].Rows[i][j + 1].ToString();
                                if (string.IsNullOrEmpty(prdQty))
                                {
                                    prdQty = "0";
                                }
                                _prd.PrdUPCCode = ds.Tables[0].Rows[i][j].ToString();
                                currentUPCCode = _prd.PrdUPCCode;
                                if (j == 37)
                                {
                                    incCounter = 0;
                                }
                                else
                                {
                                    incCounter += 1;
                                }
                                j += 1;

                                if ((_prd.IsProductUPCCodeDuplicate() == false & (string.IsNullOrEmpty(_prd.PrdUPCCode) == false)))
                                {
                                    lngSize = (BusinessUtility.GetDouble(lengthSize) + BusinessUtility.GetDouble(lengthInc) * incCounter).ToString();
                                    chstSize = (BusinessUtility.GetDouble(chest) + BusinessUtility.GetDouble(chestInc) * incCounter).ToString();
                                    sldSize = (BusinessUtility.GetDouble(shoulder) + BusinessUtility.GetDouble(shoulderInc) * incCounter).ToString();
                                    bcpSize = (BusinessUtility.GetDouble(biceps) + BusinessUtility.GetDouble(bicepsInc) * incCounter).ToString();
                                    prdWgtSize = (BusinessUtility.GetDouble(ProdWgt) + BusinessUtility.GetDouble(ProdWgtIncr) * incCounter).ToString();
                                    excutionStatus = ImportStyle(_prd.PrdUPCCode, prdName, WSPrice, RPrice, WebPrice, FOB, Landed, collID, styleName, matID,
                                    prdWgtSize, PackageWgt, PackageWidth, PackageLength, IsWebActive, IsPOSMenu, prdDesEn, prdDesFr, prdQty, lngSize,
                                    chstSize, sldSize, bcpSize, strColorID, SizeID, webSiteID, webSiteSubCatgID, necklineID, extraCatgID, sleeveID,
                                    silhouetteID, trendID, keywordID, gaugeID, textureID);

                                    if (!excutionStatus)
                                    {
                                        msgErrorCode += "<tr><td> " + currentUPCCode + "</td><td>401</td></tr>";
                                        lstErrDtl.Add(new ErrorDetails { UPCCode = currentUPCCode, ErrorCode = (int)ImportProductErrorCode.UnkownError, ErrorMessage = BusinessUtility.GetString(this.ImportErrorMessage) });
                                    }
                                    else
                                    {
                                        intRecords += 1;

                                    }
                                }
                                else if (string.IsNullOrEmpty(_prd.PrdUPCCode) == false)
                                {
                                    msgErrorCode += "<tr><td> " + _prd.PrdUPCCode + "</td><td>402</td></tr>";
                                    dublicateUPCCode += _prd.PrdUPCCode + ",";
                                    lstErrDtl.Add(new ErrorDetails { UPCCode = currentUPCCode, ErrorCode = (int)ImportProductErrorCode.DUPLICATE, ErrorMessage = BusinessUtility.GetString(this.ImportErrorMessage) });
                                }

                            }
                            catch (Exception ex)
                            {
                                if (!msgErrorCode.Contains(currentUPCCode))
                                    msgErrorCode += "<tr><td> " + currentUPCCode + "</td><td>401</td></tr>";
                                lstErrDtl.Add(new ErrorDetails { UPCCode = currentUPCCode, ErrorCode = (int)ImportProductErrorCode.UnkownError, ErrorMessage = BusinessUtility.GetString(ex.Message) });
                            }
                        }
                        //end cell reading of one row
                    }
                    else
                    {
                        for (int j = 37; j <= ds.Tables[0].Columns.Count - 1; j++)
                        {
                            try
                            {
                                _prd.PrdUPCCode = ds.Tables[0].Rows[i][j].ToString();
                                currentUPCCode = _prd.PrdUPCCode;

                                if ((_prd.IsProductUPCCodeDuplicate() == false) && (string.IsNullOrEmpty(_prd.PrdUPCCode) == false))
                                {
                                    if (!string.IsNullOrEmpty(currentUPCCode))
                                    {
                                        msgErrorCode += "<tr><td> " + currentUPCCode + "</td><td>401</td></tr>";
                                        lstErrDtl.Add(new ErrorDetails { UPCCode = currentUPCCode, ErrorCode = (int)ImportProductErrorCode.COLORCODEREPEATEDINGORUP, ErrorMessage = BusinessUtility.GetString(this.ImportErrorMessage) });
                                    }
                                }
                                else if (string.IsNullOrEmpty(_prd.PrdUPCCode) == false)
                                {
                                    msgErrorCode += "<tr><td> " + _prd.PrdUPCCode + "</td><td>402</td></tr>";
                                    dublicateUPCCode += _prd.PrdUPCCode + ",";
                                    lstErrDtl.Add(new ErrorDetails { UPCCode = currentUPCCode, ErrorCode = (int)ImportProductErrorCode.DUPLICATE, ErrorMessage = BusinessUtility.GetString(this.ImportErrorMessage) });
                                }
                                j += 1;
                            }
                            catch (Exception ex)
                            {
                                if (!msgErrorCode.Contains(currentUPCCode))
                                    msgErrorCode += "<tr><td> " + currentUPCCode + "</td><td>401</td></tr>";
                                lstErrDtl.Add(new ErrorDetails { UPCCode = currentUPCCode, ErrorCode = (int)ImportProductErrorCode.UnkownError, ErrorMessage = BusinessUtility.GetString(ex.Message) });
                            }
                        }
                    }
                }
                else if ((BusinessUtility.GetString(prdName) == "") && BusinessUtility.GetString(strColorID).Trim() == "")
                {
                    for (int j = 37; j <= ds.Tables[0].Columns.Count - 1; j++)
                    {
                        try
                        {
                            SizeID = ds.Tables[0].Columns[j].ColumnName;
                            prdQty = ds.Tables[0].Rows[i][j + 1].ToString();
                            if (string.IsNullOrEmpty(prdQty))
                            {
                                prdQty = "0";
                            }
                            _prd.PrdUPCCode = ds.Tables[0].Rows[i][j].ToString();
                            msgErrorCode += "<tr><td> " + _prd.PrdUPCCode + "</td><td>" + (int)ImportProductErrorCode.COLORNOTFOUND + "</td></tr>"; //Return Err Code 403 for ColorID Not defined for for Bar Code
                            lstErrDtl.Add(new ErrorDetails { UPCCode = _prd.PrdUPCCode, ErrorCode = (int)ImportProductErrorCode.PRODUCTANDCOLORNOTFOUND, ErrorMessage = BusinessUtility.GetString(this.ImportErrorMessage) });
                            j += 1;
                        }
                        catch (Exception ex)
                        {
                            //if (!msgErrorCode.Contains(currentUPCCode))
                            //    msgErrorCode += "<tr><td> " + currentUPCCode + "</td><td>401</td></tr>";
                            //lstErrDtl.Add(new ErrorDetails { UPCCode = currentUPCCode, ErrorCode = (int)ImportProductErrorCode.UnkownError, ErrorMessage = BusinessUtility.GetString(this.ImportErrorMessage) });
                            //lstErrDtl.Add(new ErrorDetails { UPCCode = currentUPCCode, ErrorCode = (int)ImportProductErrorCode.UnkownError, ErrorMessage = BusinessUtility.GetString(ex.Message) });
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        if (lstErrDtl.Count > 0)
        {
            this.ImportErrorMessage = GetErrorMessage(lstErrDtl);
        }
        return BusinessUtility.GetString(intRecords) + "," + msgErrorCode;

    }
    private string GetValue(DataSet ds, int i, string Name)
    {
        string strValue = ds.Tables[0].Rows[i][Name].ToString();
        strValue = strValue.Replace("'", "''");
        return strValue;
    }
    public bool ImportStyle(string SKU_XS, string prdName, string WSPrice, string RPrice, string WebPrice, string FOB, string Landed, string collID, string styleName, string matID,
                            string ProdWgt, string PackageWgt, string PackageWidth, string PackageLength, string IsWebActive, string IsPOSMenu, string prdDesEn, string prdDesFr, string qty, string leng,
                            string chest, string shoulder, string biceps, string colorID, string sizeID, string webSiteID, string webSiteSubCatgID, string necklineID, string extraCatgID, string sleeveID,
                             string silhouetteID, string trendID, string keywordID, string gaugeID, string textureID)
    {
        int ProductId = 0;
        string strSql = null;
        DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
        dbTransactionHelper.BeginTransaction();
        try
        {
            strSql = "insert into products (prdUPCCode, prdName, prdSalePricePerMinQty, prdEndUserSalesPrice, prdWebSalesPrice, prdFOBPrice, prdLandedPrice, Style, ";
            strSql += "MatID, prdWeight, prdWeightPkg, prdWidthPkg, prdLengthPkg, prdIsWeb, IsPOSMenu,prdCreatedUserID, prdCreatedOn,prdIsActive,prdType) ";
            strSql += " values (@SKU_XS, @prdName, @WSPrice,@RPrice,@WebPrice,@FOB,@Landed,@styleName,@matID,@ProdWgt,@PackageWgt,@PackageWidth,@PackageLength,@IsWebActive,@IsPOSMenu,@UserID,@CurrentDate,@prdIsActive,@prdType) ";
            MySqlParameter[] pInsert = {                                            
                                        DbUtility.GetParameter("SKU_XS", SKU_XS, MyDbType.String),
                                        DbUtility.GetParameter("prdName", prdName, MyDbType.String),
                                        DbUtility.GetParameter("WSPrice", BusinessUtility.GetDouble(WSPrice), MyDbType.Double),
                                        DbUtility.GetParameter("RPrice", BusinessUtility.GetDouble(RPrice), MyDbType.Double),
                                        DbUtility.GetParameter("WebPrice", BusinessUtility.GetDouble(WebPrice), MyDbType.Double),
                                        DbUtility.GetParameter("FOB", BusinessUtility.GetDouble(FOB), MyDbType.Double),
                                        DbUtility.GetParameter("Landed", BusinessUtility.GetDouble(Landed), MyDbType.Double),
                                        DbUtility.GetParameter("styleName", styleName, MyDbType.String),
                                        DbUtility.GetParameter("matID", BusinessUtility.GetInt(matID), MyDbType.Int),
                                        DbUtility.GetParameter("ProdWgt", ProdWgt, MyDbType.String),
                                        DbUtility.GetParameter("PackageWgt", PackageWgt, MyDbType.String),
                                        DbUtility.GetParameter("PackageWidth", PackageWidth, MyDbType.String),
                                        DbUtility.GetParameter("PackageLength", PackageLength, MyDbType.String),
                                        DbUtility.GetParameter("IsWebActive", BusinessUtility.GetInt(IsWebActive), MyDbType.Int),
                                        DbUtility.GetParameter("IsPOSMenu", BusinessUtility.GetInt(IsPOSMenu), MyDbType.Int),
                                        DbUtility.GetParameter("UserID", CurrentUser.UserID, MyDbType.Int),
                                        DbUtility.GetParameter("CurrentDate", DateTime.Now, MyDbType.DateTime),
                                        DbUtility.GetParameter("prdIsActive", 1, MyDbType.Int),
                                        DbUtility.GetParameter("prdType", 1, MyDbType.Int)
                                        };
            dbTransactionHelper.ExecuteNonQuery(strSql, System.Data.CommandType.Text, pInsert);
            ProductId = dbTransactionHelper.GetLastInsertID();

            strSql = "insert into prddescriptions (id, descLang, prdName, prdSmallDesc, prdLargeDesc) values (@ProductId,@lang, ";
            strSql += "@prdName,@prdDesEn,@prdDesLargeEn) ";
            MySqlParameter[] pInsertprdDesc = {                                            
                                        DbUtility.GetParameter("ProductId", ProductId, MyDbType.Int),
                                        DbUtility.GetParameter("lang", "en", MyDbType.String),
                                        DbUtility.GetParameter("prdName", prdName, MyDbType.String),
                                        DbUtility.GetParameter("prdDesEn", prdDesEn, MyDbType.String),
                                        DbUtility.GetParameter("prdDesLargeEn", prdDesEn, MyDbType.String),
                                        };
            dbTransactionHelper.ExecuteNonQuery(strSql, System.Data.CommandType.Text, pInsertprdDesc);

            strSql = "insert into prddescriptions (id, descLang, prdName, prdSmallDesc, prdLargeDesc) values (@ProductId,@lang, ";
            strSql += "@prdName, @prdDesFr,@prdDesLargefr) ";
            MySqlParameter[] pInsertprdDescFr = {                                            
                                        DbUtility.GetParameter("ProductId", ProductId, MyDbType.Int),
                                        DbUtility.GetParameter("lang", "fr", MyDbType.String),
                                        DbUtility.GetParameter("prdName", prdName, MyDbType.String),
                                        DbUtility.GetParameter("prdDesFr", prdDesFr, MyDbType.String),
                                        DbUtility.GetParameter("prdDesLargefr", prdDesFr, MyDbType.String),
                                        };
            dbTransactionHelper.ExecuteNonQuery(strSql, System.Data.CommandType.Text, pInsertprdDescFr);

            strSql = "insert into prdquantity (prdID, prdWhsCode, prdOhdQty, prdQuoteRsv, prdSORsv, prdDefectiveQty) values (@ProductId, ";
            strSql += "@UserDefaultWarehouse,@qty,@QRsv, @SORsv,@DFQty) ";
            MySqlParameter[] pInsertprdQty = {                                            
                                        DbUtility.GetParameter("ProductId", ProductId, MyDbType.Int),
                                        DbUtility.GetParameter("UserDefaultWarehouse", CurrentUser.UserDefaultWarehouse, MyDbType.String),
                                        DbUtility.GetParameter("qty", BusinessUtility.GetDouble(qty), MyDbType.Double),
                                        DbUtility.GetParameter("QRsv", 0, MyDbType.Double),
                                        DbUtility.GetParameter("SORsv", 0, MyDbType.Double),
                                        DbUtility.GetParameter("DFQty", 0, MyDbType.Double),
                                        };
            dbTransactionHelper.ExecuteNonQuery(strSql, System.Data.CommandType.Text, pInsertprdQty);

            strSql = " INSERT INTO inventorymovement (WhsCode, UserID, CreationDateTime, ProductID, TransactionSourceID, Qty, UpdateType ) ";
            strSql += " VALUES ( @WhsCode, @UserID, @CreationDateTime, @ProductID, @TransactionSourceID, @Qty, @UpdateType ) ";
            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, new MySqlParameter[]{
                    DbUtility.GetParameter("WhsCode", CurrentUser.UserDefaultWarehouse, MyDbType.String),
                    DbUtility.GetParameter("UserID", CurrentUser.UserID, MyDbType.Int),
                    DbUtility.GetParameter("CreationDateTime", DateTime.Now, MyDbType.DateTime),
                    DbUtility.GetParameter("ProductID", ProductId, MyDbType.Int),
                    DbUtility.GetParameter("TransactionSourceID", BusinessUtility.GetString(InvMovmentSrc.IMPT), MyDbType.String),
                    DbUtility.GetParameter("Qty", qty, MyDbType.Int),
                    DbUtility.GetParameter("UpdateType",BusinessUtility.GetString(InvMovmentUpdateType.ORWT), MyDbType.String)
                    });



            strSql = "INSERT INTO ProductClothDesc (productID, style,  size, longueurLength, epauleShoulder, busteChest, bicep, material, collection, color) values (@ProductId, ";
            strSql += "@styleName, @sizeID, @leng, @shoulder, @chest, @biceps, @matID, @collID, @colorID) ";
            MySqlParameter[] pInsertprdCDesc = {                                            
                                        DbUtility.GetParameter("ProductId", ProductId, MyDbType.Int),
                                        DbUtility.GetParameter("styleName", styleName, MyDbType.String),
                                        DbUtility.GetParameter("sizeID", BusinessUtility.GetInt(sizeID), MyDbType.Int),
                                        DbUtility.GetParameter("leng", leng, MyDbType.String),
                                        DbUtility.GetParameter("shoulder", shoulder, MyDbType.String),
                                        DbUtility.GetParameter("chest", chest, MyDbType.String),
                                        DbUtility.GetParameter("biceps", biceps, MyDbType.String),
                                        DbUtility.GetParameter("matID", BusinessUtility.GetInt(matID), MyDbType.Int),
                                        DbUtility.GetParameter("collID", BusinessUtility.GetInt(collID), MyDbType.Int),
                                        DbUtility.GetParameter("colorID", BusinessUtility.GetInt(colorID), MyDbType.Int),
                                        };
            dbTransactionHelper.ExecuteNonQuery(strSql, System.Data.CommandType.Text, pInsertprdCDesc);

            string[] websiteIDs = webSiteID.Split(',');
            foreach (string s in websiteIDs)
            {
                if (!string.IsNullOrEmpty(s.Trim()))
                {
                    strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID ) ";
                    strSql += " values (@prdID,@CtgID, @HdrID) ";
                    MySqlParameter[] pInsertWebSite = {                                            
                                        DbUtility.GetParameter("prdID", ProductId, MyDbType.Int),
                                        DbUtility.GetParameter("CtgID", BusinessUtility.GetInt(s), MyDbType.Int),
                                        DbUtility.GetParameter("HdrID", 1, MyDbType.Int),
                                        };
                    dbTransactionHelper.ExecuteNonQuery(strSql, System.Data.CommandType.Text, pInsertWebSite);
                }
            }

            string[] websiteSubCgIDs = webSiteSubCatgID.Split(',');
            foreach (string s in websiteSubCgIDs)
            {
                if (!string.IsNullOrEmpty(s.Trim()))
                {
                    strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  ";
                    strSql += " values (@prdID, @CtgID, @HdrID) ";
                    MySqlParameter[] pInsertWebSiteSubCtg = {                                            
                                        DbUtility.GetParameter("prdID", ProductId, MyDbType.Int),
                                        DbUtility.GetParameter("CtgID", BusinessUtility.GetInt(s), MyDbType.Int),
                                        DbUtility.GetParameter("HdrID", 8, MyDbType.Int),
                                        };
                    dbTransactionHelper.ExecuteNonQuery(strSql, System.Data.CommandType.Text, pInsertWebSiteSubCtg);
                }
            }

            string[] necklineIDs = necklineID.Split(',');
            foreach (string s in necklineIDs)
            {
                if (!string.IsNullOrEmpty(s.Trim()))
                {
                    strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  ";
                    strSql += " values (@prdID, @CtgID, @HdrID) ";
                    MySqlParameter[] pInsertNeckline = {                                            
                                        DbUtility.GetParameter("prdID", ProductId, MyDbType.Int),
                                        DbUtility.GetParameter("CtgID", BusinessUtility.GetInt(s), MyDbType.Int),
                                        DbUtility.GetParameter("HdrID", 2, MyDbType.Int),
                                        };
                    dbTransactionHelper.ExecuteNonQuery(strSql, System.Data.CommandType.Text, pInsertNeckline);
                }
            }

            string[] extraCatgIDs = extraCatgID.Split(',');
            foreach (string s in extraCatgIDs)
            {
                if (!string.IsNullOrEmpty(s.Trim()))
                {
                    strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  ";
                    strSql += " values (@prdID, @CtgID, @HdrID) ";
                    MySqlParameter[] pInsertExtCtg = {                                            
                                        DbUtility.GetParameter("prdID", ProductId, MyDbType.Int),
                                        DbUtility.GetParameter("CtgID", BusinessUtility.GetInt(s), MyDbType.Int),
                                        DbUtility.GetParameter("HdrID", 3, MyDbType.Int),
                                        };
                    dbTransactionHelper.ExecuteNonQuery(strSql, System.Data.CommandType.Text, pInsertExtCtg);
                }
            }

            string[] sleeveIDs = sleeveID.Split(',');
            foreach (string s in sleeveIDs)
            {
                if (!string.IsNullOrEmpty(s.Trim()))
                {
                    strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  ";
                    strSql += "  values (@prdID, @CtgID, @HdrID) ";
                    MySqlParameter[] pInsertSleev = {                                            
                                        DbUtility.GetParameter("prdID", ProductId, MyDbType.Int),
                                        DbUtility.GetParameter("CtgID", BusinessUtility.GetInt(s), MyDbType.Int),
                                        DbUtility.GetParameter("HdrID", 4, MyDbType.Int),
                                        };
                    dbTransactionHelper.ExecuteNonQuery(strSql, System.Data.CommandType.Text, pInsertSleev);
                }
            }

            string[] silIDs = silhouetteID.Split(',');
            foreach (string s in silIDs)
            {
                if (!string.IsNullOrEmpty(s.Trim()))
                {
                    strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  ";
                    strSql += "  values (@prdID, @CtgID, @HdrID) ";
                    MySqlParameter[] pInsertSilh = {                                            
                                        DbUtility.GetParameter("prdID", ProductId, MyDbType.Int),
                                        DbUtility.GetParameter("CtgID", BusinessUtility.GetInt(s), MyDbType.Int),
                                        DbUtility.GetParameter("HdrID", 5, MyDbType.Int),
                                        };
                    dbTransactionHelper.ExecuteNonQuery(strSql, System.Data.CommandType.Text, pInsertSilh);
                }
            }

            string[] treIDs = trendID.Split(',');
            foreach (string s in treIDs)
            {
                if (!string.IsNullOrEmpty(s.Trim()))
                {
                    strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  ";
                    strSql += "  values (@prdID, @CtgID, @HdrID) ";
                    MySqlParameter[] pInsertTrend = {                                            
                                        DbUtility.GetParameter("prdID", ProductId, MyDbType.Int),
                                        DbUtility.GetParameter("CtgID", BusinessUtility.GetInt(s), MyDbType.Int),
                                        DbUtility.GetParameter("HdrID", 6, MyDbType.Int),
                                        };
                    dbTransactionHelper.ExecuteNonQuery(strSql, System.Data.CommandType.Text, pInsertTrend);
                }
            }

            string[] keyIDs = keywordID.Split(',');
            foreach (string s in keyIDs)
            {
                if (!string.IsNullOrEmpty(s.Trim()))
                {
                    strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  ";
                    strSql += "  values (@prdID, @CtgID, @HdrID) ";
                    MySqlParameter[] pInsertKeyword = {                                            
                                        DbUtility.GetParameter("prdID", ProductId, MyDbType.Int),
                                        DbUtility.GetParameter("CtgID", BusinessUtility.GetInt(s), MyDbType.Int),
                                        DbUtility.GetParameter("HdrID", 7, MyDbType.Int),
                                        };
                    dbTransactionHelper.ExecuteNonQuery(strSql, System.Data.CommandType.Text, pInsertKeyword);
                }
            }

            string[] guagIDs = gaugeID.Split(',');
            foreach (string s in guagIDs)
            {
                if (!string.IsNullOrEmpty(s.Trim()))
                {
                    strSql = "INSERT INTO prodCatgSelVal (ProdID, prodSysCatgDtlID, ProdSyCatgHdrID )  ";
                    strSql += "  values (@prdID, @CtgID, @HdrID) ";
                    MySqlParameter[] pInsertKeyGauge = {                                            
                                        DbUtility.GetParameter("prdID", ProductId, MyDbType.Int),
                                        DbUtility.GetParameter("CtgID", BusinessUtility.GetInt(s), MyDbType.Int),
                                        DbUtility.GetParameter("HdrID", 9, MyDbType.Int),
                                        };
                    dbTransactionHelper.ExecuteNonQuery(strSql, System.Data.CommandType.Text, pInsertKeyGauge);
                }
            }
            string[] textureIDs = textureID.Split(',');
            foreach (string s in textureIDs)
            {
                if (!string.IsNullOrEmpty(s.Trim()))
                {
                    strSql = "INSERT INTO productAssociateTexture (ProductID, TextureID) ";
                    strSql += "  values (@prdID, @textureID) ";
                    MySqlParameter[] pInsertKeyTexture = {                                            
                                        DbUtility.GetParameter("prdID", ProductId, MyDbType.Int),
                                        DbUtility.GetParameter("textureID", BusinessUtility.GetInt(s), MyDbType.Int),
                                        };
                    dbTransactionHelper.ExecuteNonQuery(strSql, System.Data.CommandType.Text, pInsertKeyTexture);
                }
            }


            dbTransactionHelper.CommitTransaction();
            return true;
        }
        catch (Exception ex)
        {
            dbTransactionHelper.RollBackTransaction();
            this.ImportErrorMessage = ex.Message;
            throw;
        }
        finally
        {
            dbTransactionHelper.CloseDatabaseConnection();
        }
    }

    public static bool IsNumeric(string s)
    {
        int output;
        return int.TryParse(s, out output);
    }

    public string ImportCustomer(DataSet ds, string strAutoAssignSR)
    {
        Product _prd = new Product();
        int intRecords = 0;
        int i = 0;
        string msgErrorCode = string.Empty;
        string currentUPCCode = string.Empty;
        msgErrorCode = "<tr><td colspan='2' style='font-weight:bold; font-size:14px' >" + CommonResource.ResourceValue("lblStyleImportErrorMsg") + "</td></tr><tr><td>SKU #</td><td>" + CommonResource.ResourceValue("lblError") + "</td></tr>";
        msgErrorCode = "";
        List<ErrorDetails> lstErrDtl = new List<ErrorDetails>();


        int CustomerId = 0;
        int ContactId = 0;
        StringBuilder CustomerType = new StringBuilder();
        string[] Name = null;
        StringBuilder ContactName = new StringBuilder();
        StringBuilder FirstName = new StringBuilder();
        StringBuilder LastName = new StringBuilder();
        StringBuilder ContactPhone = new StringBuilder();
        StringBuilder ContactEmail = new StringBuilder();
        StringBuilder ContactFax = new StringBuilder();
        StringBuilder ContactTitle = new StringBuilder();
        StringBuilder strSql = default(StringBuilder);
        string[] strSR = null;
        int intI = 0;
        int intJ = 0;
        if (strAutoAssignSR == "Y")
        {
            strSR = funGetSRForImport(null).Split('~');
            intI = strSR.Length - 1;
        }
        string txCode = null;
        string partEmail = null;
        System.DateTime dtO = System.DateTime.Now;
        string strLog = string.Empty;

        for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
        {
            DbTransactionHelper dbTransactionHelper = new DbTransactionHelper();
            dbTransactionHelper.BeginTransaction();

            try
            {
                if (GetValue(ds, i, "PartnerType") == "1")
                {
                    FirstName = new StringBuilder();
                    LastName = new StringBuilder();
                    partEmail = GetValue(ds, i, "Email");
                    FirstName.Append(GetValue(ds, i, "FirstName"));
                    LastName.Append(GetValue(ds, i, "LastName"));
                    if (string.IsNullOrEmpty(txCode) || !IsNumeric(txCode))
                    {
                        txCode = "NULL";
                    }
                    if (String.IsNullOrEmpty(partEmail.Trim()))
                    {
                        partEmail = String.Format("{0}@noemail.com", dtO.ToString("yyyyMMddHHmmssffff"));
                    }

                    //DateTime dtPartnerSince = Convert.ToDateTime(GetValue(ds, i, "VIPSince"));
                    DateTime dtPartnerSince = BusinessUtility.GetDateTime(GetValue(ds, i, "VIPSince"),DateFormat.MMddyyyy);

                    dtO = dtO.AddSeconds(1);
                    strSql = new StringBuilder();
                    strSql.Append("insert into partners (PartnerLongName, PartnerPhone, PartnerEmail, PartnerEmail2, ");
                    strSql.Append(" PartnerActive, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerType, legacyPartnerID, PartnerSince, PartnerTaxCode, PartnerCurrencyCode, PartnerLang) ");//6, CAD,  fr //PartnerTaxCode, PartnerCurrencyCode, PartnerLang
                    strSql.Append("values ('" + FirstName + " " + LastName + "','" + GetValue(ds, i, "PartnerPhone").Trim() + "', '");
                    strSql.Append(partEmail + "', '" + GetValue(ds, i, "PersonalEmail") + "', ");

                    strSql.Append(" '1', now(), '" + HttpContext.Current.Session["userID"] + "', now(), '" + HttpContext.Current.Session["userID"] + "'").Append(",");
                    strSql.Append(BusinessUtility.GetInt(GetValue(ds, i, "PartnerType"))).Append(",");
                    strSql.Append("'" + BusinessUtility.GetString(GetValue(ds, i, "VIPCode")) + "'").Append(",");
                    strSql.Append("'" + dtPartnerSince.ToString("yyyy/MM/dd hh:mm:ss") + "'");
                    strSql.Append(",6,'CAD', 'fr'  ");
                    //strSql.Append("'").Append(BusinessUtility.GetInt(GetValue(ds, i, "PartnerType"))).Append("'").Append(",");
                    //strSql.Append(strSql.Append("'").Append(BusinessUtility.GetString(GetValue(ds, i, "VIPCode"))).Append("'"));
                    //strSql.Append(",'" + dtPartnerSince + "'");
                    strSql.Append(")");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);
                    CustomerId = dbTransactionHelper.GetLastInsertID();

                    strSql = new StringBuilder();
                    strSql.Append("INSERT INTO z_customer_extended (PartnerID, FirstName, LastName) ");
                    strSql.Append(" VALUES ('" + CustomerId + "', '" + FirstName + "', '" + LastName + "') ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);


                    strSql = new StringBuilder();
                    strSql.Append("INSERT INTO z_loyal_partner_history (LoyalPartnerID, LoyalCategoryID, Points, PointsAddedOn, PointsAddedBy, PointAuto) ");
                    strSql.Append(" VALUES ('" + CustomerId + "', 1 , '" + BusinessUtility.GetDouble(GetValue(ds, i, "VIPPOINTS")) + "', now(), '" + HttpContext.Current.Session["userID"] + "', 0) ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);

                    ContactTitle = new StringBuilder();
                    ContactTitle.Append("ContactTitleEn");

                    strSql = new StringBuilder();
                    strSql.Append("insert into partnercontacts (ContactPartnerID, ContactLastName, ContactFirstName,  "); //ContactPhone, ContactPhoneExt,  /" + BusinessUtility.GetString(ContactTitle) + ",
                    strSql.Append(" ContactLangPref, ContactCreatedOn, ContactCreatedBy, ContactLastUpdatedOn, ContactLastUpdatedBy, "); //ContactEmail, ContactFax,
                    strSql.Append("ContactActive, ContactPrimary, ContactSelSpecialization, IsSubscribeEmail) values ");
                    strSql.Append("('" + CustomerId + "', '" + BusinessUtility.GetString(LastName) + "', '" + BusinessUtility.GetString(FirstName) + "',  ");  //'" + BusinessUtility.GetString(GetValue(ds, i, "ContactTitle")) + "',
                    strSql.Append(" '" + "en" + "', ");
                    strSql.Append("now(), '" + HttpContext.Current.Session["userID"] + "', now(), '" + HttpContext.Current.Session["userID"] + "', 1, 1, 0, 1) ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);

                    strSql = new StringBuilder();
                    //strSql.Append("Select max(ContactID) from partnercontacts;");
                    //ContactId = objDb.GetScalarDataByQuery(strSql.ToString);
                    ContactId = dbTransactionHelper.GetLastInsertID();

                    strSql = new StringBuilder();
                    strSql.Append("insert into partnerseltype (PartnerSelTypeID, PartnerSelPartID) values ('" + GetValue(ds, i, "PartnerType") + "', '" + CustomerId + "') ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);

                    //strSql = new StringBuilder();
                    //strSql.Append(" INSERT INTO cdl_customercategories ( CategoryID, CustomerID)  values ('" + GetValue(ds, i, "CustomerType") + "', '" + CustomerId + "') ");
                    //dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);

                    CustomerType = new StringBuilder();
                    if (GetValue(ds, i, "PartnerType") == "1")
                    {
                        CustomerType.Append("D");
                    }
                    else if (GetValue(ds, i, "PartnerType") == "2")
                    {
                        CustomerType.Append("E");
                    }
                    else if (GetValue(ds, i, "PartnerType") == "3")
                    {
                        CustomerType.Append("R");
                    }
                    else if (GetValue(ds, i, "PartnerType") == "4")
                    {
                        CustomerType.Append("L");
                    }

                    strSql = new StringBuilder();
                    strSql.Append("insert into addresses (addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode) ");
                    strSql.Append("values ('" + BusinessUtility.GetString(CustomerType) + "', 'H', '" + CustomerId + "', '" + GetValue(ds, i, "Address") + "', '" + "" + "', '");
                    strSql.Append("" + "', '" + GetValue(ds, i, "City") + "', '" + GetValue(ds, i, "Province") + "', '");
                    strSql.Append(GetValue(ds, i, "Country") + "', '" + GetValue(ds, i, "ZipCode") + "') ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);

                    strSql = new StringBuilder();
                    strSql.Append("insert into addresses (addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode) ");
                    strSql.Append("values ('" + BusinessUtility.GetString(CustomerType) + "', 'B', '" + CustomerId + "', '" + GetValue(ds, i, "Address") + "', '" + "" + "', '");
                    strSql.Append("" + "', '" + GetValue(ds, i, "City") + "', '" + GetValue(ds, i, "Province") + "', '");
                    strSql.Append(GetValue(ds, i, "Country") + "', '" + GetValue(ds, i, "ZipCode") + "') ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);

                    strSql = new StringBuilder();
                    strSql.Append("insert into addresses (addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode) ");
                    strSql.Append("values ('" + BusinessUtility.GetString(CustomerType) + "', 'S', '" + CustomerId + "', '" + GetValue(ds, i, "Address") + "', '" + "" + "', '");
                    strSql.Append("" + "', '" + GetValue(ds, i, "City") + "', '" + GetValue(ds, i, "Province") + "', '");
                    strSql.Append(GetValue(ds, i, "Country") + "', '" + GetValue(ds, i, "ZipCode") + "') ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);

                    strSql = new StringBuilder();
                    strSql.Append("insert into addresses (addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode) ");
                    strSql.Append("values ('C', 'B', '" + BusinessUtility.GetString(ContactId) + "', '" + GetValue(ds, i, "Address") + "', '" + "" + "', '");
                    strSql.Append("" + "', '" + GetValue(ds, i, "City") + "', '" + GetValue(ds, i, "Province") + "', '");
                    strSql.Append(GetValue(ds, i, "Country") + "', '" + GetValue(ds, i, "ZipCode") + "') ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);
                    dbTransactionHelper.CommitTransaction();
                    intRecords += 1;

                }
                else
                {
                    txCode = GetValue(ds, i, "PartnerTaxCode").Trim();
                    partEmail = GetValue(ds, i, "PartnerEmail");
                    Double dblBal = BusinessUtility.GetDouble(GetValue(ds, i, "Balance"));
                    Double dblTotal = BusinessUtility.GetDouble(GetValue(ds, i, "Total"));
                    if (string.IsNullOrEmpty(txCode) || !IsNumeric(txCode))
                    {
                        txCode = "NULL";
                    }
                    if (String.IsNullOrEmpty(partEmail.Trim()))
                    {
                        partEmail = String.Format("{0}@noemail.com", dtO.ToString("yyyyMMddHHmmssffff"));
                    }

                    dtO = dtO.AddSeconds(1);
                    strSql = new StringBuilder();
                    strSql.Append("insert into partners (PartnerLongName, PartnerPhone, PartnerFax, PartnerEmail, PartnerWebsite, PartnerAcronyme, PartnerNote, PartnerTaxCode, ");
                    strSql.Append("PartnerCurrencyCode, PartnerLang, PartnerActive, PartnerCreatedOn, PartnerCreatedBy, PartnerLastUpdatedOn, PartnerLastUpdatedBy, PartnerValue, PartnerInvoiceNetTerms,PartnerDiscount, PartnerCreditAvailable, PartnerType, legacyOpenAR, legacyTotalSales) ");
                    strSql.Append("values ('" + GetValue(ds, i, "PartnerLongName") + "', '" + GetValue(ds, i, "PartnerPhone") + "', '" + GetValue(ds, i, "PartnerFax") + "', '");
                    strSql.Append(partEmail + "', '" + GetValue(ds, i, "PartnerWebsite") + "', '" + GetValue(ds, i, "PartnerAcronyme") + "', '");
                    strSql.Append(GetValue(ds, i, "PartnerNote") + "', " + txCode + ", '" + GetValue(ds, i, "PartnerCurrencyCode") + "', '");
                    strSql.Append(GetValue(ds, i, "PartnerLang") + "', 1, now(), '" + HttpContext.Current.Session["userID"] + "', now(), '" + HttpContext.Current.Session["userID"] + "', '" + GetValue(ds, i, "PartnerTaxCode") + "'").Append(",");
                    strSql.Append("'").Append(GetValue(ds, i, "PartnerInvoiceNetTerms")).Append("'").Append(",");
                    strSql.Append("'").Append(BusinessUtility.GetInt(GetValue(ds, i, "PartnerDiscount"))).Append("'").Append(",");
                    strSql.Append("'").Append(BusinessUtility.GetDouble(GetValue(ds, i, "PartnerCreditAvailable"))).Append("'").Append(",");
                    strSql.Append("'").Append(BusinessUtility.GetInt(GetValue(ds, i, "PartnerType"))).Append("'").Append(",");
                    strSql.Append("'").Append(dblBal).Append("'").Append(",");
                    strSql.Append("'").Append(dblTotal).Append("'").Append(")");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);

                    CustomerId = dbTransactionHelper.GetLastInsertID();

                    ContactName = new StringBuilder();
                    FirstName = new StringBuilder();
                    LastName = new StringBuilder();
                    if (!string.IsNullOrEmpty(GetValue(ds, i, "ContactName")))
                    {
                        ContactName.Append(GetValue(ds, i, "ContactName"));
                    }
                    else
                    {
                        ContactName.Append(GetValue(ds, i, "PartnerLongName"));
                    }
                    Name = BusinessUtility.GetString(ContactName).Split(' ');
                    if (Name.Length > 1)
                    {
                        LastName.Append(Name[Name.Length - 1]);
                        FirstName.Append(BusinessUtility.GetString(ContactName).Replace(" " + BusinessUtility.GetString(LastName), ""));
                    }
                    else
                    {
                        LastName.Append(Name[0]);
                    }

                    ContactPhone = new StringBuilder();
                    if (!string.IsNullOrEmpty(GetValue(ds, i, "ContactPhone")))
                    {
                        ContactPhone.Append(GetValue(ds, i, "ContactPhone"));
                    }
                    else
                    {
                        ContactPhone.Append(GetValue(ds, i, "PartnerPhone"));
                    }

                    ContactEmail = new StringBuilder();
                    if (!string.IsNullOrEmpty(GetValue(ds, i, "ContactEmail")))
                    {
                        ContactEmail.Append(GetValue(ds, i, "ContactEmail"));
                    }
                    else
                    {
                        ContactEmail.Append(GetValue(ds, i, "PartnerEmail"));
                    }

                    ContactFax = new StringBuilder();
                    if (!string.IsNullOrEmpty(GetValue(ds, i, "ContactFax")))
                    {
                        ContactFax.Append(GetValue(ds, i, "ContactFax"));
                    }
                    else
                    {
                        ContactFax.Append(GetValue(ds, i, "PartnerFax"));
                    }

                    ContactTitle = new StringBuilder();
                    if (GetValue(ds, i, "PartnerLang") == "fr")
                    {
                        ContactTitle.Append("ContactTitleFr");
                    }
                    else
                    {
                        ContactTitle.Append("ContactTitleEn");
                    }

                    strSql = new StringBuilder();
                    strSql.Append(" INSERT INTO cdl_customercategories ( CategoryID, CustomerID)  values ('" + GetValue(ds, i, "CustomerType") + "', '" + CustomerId + "') ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);

                    strSql = new StringBuilder();
                    strSql.Append("insert into partnercontacts (ContactPartnerID, ContactLastName, ContactFirstName, " + BusinessUtility.GetString(ContactTitle) + ", ContactPhone, ContactPhoneExt, ");
                    strSql.Append("ContactEmail, ContactFax, ContactLangPref, ContactCreatedOn, ContactCreatedBy, ContactLastUpdatedOn, ContactLastUpdatedBy, ");
                    strSql.Append("ContactActive, ContactPrimary, ContactSelSpecialization, IsSubscribeEmail) values ");
                    strSql.Append("('" + CustomerId + "', '" + BusinessUtility.GetString(LastName) + "', '" + BusinessUtility.GetString(FirstName) + "', '" + BusinessUtility.GetString(GetValue(ds, i, "ContactTitle")) + "', '" + BusinessUtility.GetString(ContactPhone) + "', ");
                    strSql.Append("'" + GetValue(ds, i, "ContactPhoneExt") + "', '" + BusinessUtility.GetString(ContactEmail) + "', '" + BusinessUtility.GetString(ContactFax) + "', '" + GetValue(ds, i, "PartnerLang") + "', ");
                    strSql.Append("now(), '" + HttpContext.Current.Session["userID"] + "', now(), '" + HttpContext.Current.Session["userID"] + "', 1, 1, 0, 1) ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);

                    strSql = new StringBuilder();
                    //strSql.Append("Select max(ContactID) from partnercontacts;");
                    //ContactId = objDb.GetScalarDataByQuery(strSql.ToString);
                    ContactId = dbTransactionHelper.GetLastInsertID();

                    strSql = new StringBuilder();
                    strSql.Append("insert into partnerseltype (PartnerSelTypeID, PartnerSelPartID) values ('" + GetValue(ds, i, "PartnerType") + "', '" + CustomerId + "') ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);

                    CustomerType = new StringBuilder();
                    if (GetValue(ds, i, "PartnerType") == "1")
                    {
                        CustomerType.Append("D");
                    }
                    else if (GetValue(ds, i, "PartnerType") == "2")
                    {
                        CustomerType.Append("E");
                    }
                    else if (GetValue(ds, i, "PartnerType") == "3")
                    {
                        CustomerType.Append("R");
                    }
                    else if (GetValue(ds, i, "PartnerType") == "4")
                    {
                        CustomerType.Append("L");
                    }

                    if (strAutoAssignSR == "Y" & BusinessUtility.GetString(CustomerType) == "L")
                    {
                        if (intI < intJ)
                        {
                            intJ = 0;
                        }
                        if (intI >= intJ)
                        {
                            strSql = new StringBuilder();
                            strSql.Append("INSERT INTO assignleads(UserID, CustomerID, CustomerType, Active) VALUES('" + strSR[intJ] + "','" + CustomerId + "','L','1')");
                            dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);
                            intJ += 1;
                        }
                    }

                    strSql = new StringBuilder();
                    strSql.Append("insert into addresses (addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode) ");
                    strSql.Append("values ('" + BusinessUtility.GetString(CustomerType) + "', 'H', '" + CustomerId + "', '" + GetValue(ds, i, "addressLine1") + "', '" + GetValue(ds, i, "addressLine2") + "', '");
                    strSql.Append(GetValue(ds, i, "addressLine3") + "', '" + GetValue(ds, i, "addressCity") + "', '" + GetValue(ds, i, "addressState") + "', '");
                    strSql.Append(GetValue(ds, i, "addressCountry") + "', '" + GetValue(ds, i, "addressPostalCode") + "') ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);

                    strSql = new StringBuilder();
                    strSql.Append("insert into addresses (addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode) ");
                    strSql.Append("values ('" + BusinessUtility.GetString(CustomerType) + "', 'B', '" + CustomerId + "', '" + GetValue(ds, i, "addressLine1") + "', '" + GetValue(ds, i, "addressLine2") + "', '");
                    strSql.Append(GetValue(ds, i, "addressLine3") + "', '" + GetValue(ds, i, "addressCity") + "', '" + GetValue(ds, i, "addressState") + "', '");
                    strSql.Append(GetValue(ds, i, "addressCountry") + "', '" + GetValue(ds, i, "addressPostalCode") + "') ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);

                    strSql = new StringBuilder();
                    strSql.Append("insert into addresses (addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode) ");
                    strSql.Append("values ('" + BusinessUtility.GetString(CustomerType) + "', 'S', '" + CustomerId + "', '" + GetValue(ds, i, "addressLine1") + "', '" + GetValue(ds, i, "addressLine2") + "', '");
                    strSql.Append(GetValue(ds, i, "addressLine3") + "', '" + GetValue(ds, i, "addressCity") + "', '" + GetValue(ds, i, "addressState") + "', '");
                    strSql.Append(GetValue(ds, i, "addressCountry") + "', '" + GetValue(ds, i, "addressPostalCode") + "') ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);

                    strSql = new StringBuilder();
                    strSql.Append("insert into addresses (addressRef, addressType, addressSourceID, addressLine1, addressLine2, addressLine3, addressCity, addressState, addressCountry, addressPostalCode) ");
                    strSql.Append("values ('C', 'B', '" + BusinessUtility.GetString(ContactId) + "', '" + GetValue(ds, i, "addressLine1") + "', '" + GetValue(ds, i, "addressLine2") + "', '");
                    strSql.Append(GetValue(ds, i, "addressLine3") + "', '" + GetValue(ds, i, "addressCity") + "', '" + GetValue(ds, i, "addressState") + "', '");
                    strSql.Append(GetValue(ds, i, "addressCountry") + "', '" + GetValue(ds, i, "addressPostalCode") + "') ");
                    dbTransactionHelper.ExecuteNonQuery(BusinessUtility.GetString(strSql), CommandType.Text, null);
                    dbTransactionHelper.CommitTransaction();
                    intRecords += 1;
                    //return true;
                }
            }
            catch (Exception ex)
            {
                dbTransactionHelper.RollBackTransaction();
                this.ImportErrorMessage = ex.Message;
                throw;
            }
            finally
            {
                dbTransactionHelper.CloseDatabaseConnection();
            }
        }


        return BusinessUtility.GetString(intRecords) + "," + msgErrorCode;

    }

    private string funGetSRForImport(DbHelper dbHelp)
    {
        bool mustClose = false;
        if (dbHelp == null)
        {
            mustClose = true;
            dbHelp = new DbHelper(true);
        }
        try
        {
            string strSQL = null;
            strSQL = "SELECT UserId FROM users WHERE UserActive='1' and userModules like '%SR%'  ";
            string strSR = "";
            DataTable dt = dbHelp.GetDataTable(BusinessUtility.GetString(strSQL), CommandType.Text, null);
            //while (idr.Read)
            foreach (DataRow dRow in dt.Rows)
            {
                strSR += dRow[0] + "~";
            }
            if (!string.IsNullOrEmpty(strSR))
            {
                //strSR = strSR. TrimEnd("~")
                strSR = strSR.Substring(0, strSR.LastIndexOf("~") - 1);//  . TrimEnd("~")
            }
            return strSR;
        }
        catch
        {
            throw;
        }
        finally
        {
            if (mustClose) dbHelp.CloseDatabaseConnection();
        }
    }

    private string GetErrorMessage(List<ErrorDetails> lstErroDtl)
    {
        StringBuilder sbHtml = new StringBuilder();
        sbHtml.Append("<table>");
        sbHtml.Append("<tr>");
        sbHtml.Append("<td colspan='2' style='font-weight:bold; font-size:14px'>");
        sbHtml.Append(CommonResource.ResourceValue("lblStyleImportErrorMsg"));
        sbHtml.Append("</td>");
        sbHtml.Append("</tr>");
        sbHtml.Append("<tr>");
        sbHtml.Append("<td style='font-size:10px;font-weight:bold; '>");
        sbHtml.Append(CommonResource.ResourceValue("lblSrchSKU"));
        sbHtml.Append("</td>");
        sbHtml.Append("<td style='font-size:10px;font-weight:bold; '>");
        sbHtml.Append(CommonResource.ResourceValue("lblError"));
        sbHtml.Append("</td>");
        sbHtml.Append("</tr>");
        foreach (var item in lstErroDtl)
        {
            sbHtml.Append("<tr>");
            sbHtml.Append("<td>");
            sbHtml.Append(BusinessUtility.GetString(item.UPCCode));
            sbHtml.Append("</td>");
            sbHtml.Append("<td>");
            sbHtml.Append(BusinessUtility.GetString(item.ErrorCode));
            sbHtml.Append("</td>");
            sbHtml.Append("<td>");
            sbHtml.Append(BusinessUtility.GetString(item.ErrorMessage));
            sbHtml.Append("</td>");
            sbHtml.Append("</tr>");
        }
        sbHtml.Append("</table>");
        return BusinessUtility.GetString(sbHtml);
    }
}
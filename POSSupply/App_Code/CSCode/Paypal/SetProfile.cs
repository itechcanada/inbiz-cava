﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using com.paypal.soap.api;
using com.paypal.sdk.services;
using com.paypal.sdk.profiles;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for SetProfile
/// </summary>
public class SetProfile
{
    public static Boolean is3token = true;
    public static readonly IAPIProfile DefaultProfile = SetProfile.CreateAPIProfile(PaypalConstants.API_USERNAME, PaypalConstants.API_PASSWORD, PaypalConstants.API_SIGNATURE, "", "", PaypalConstants.CERTIFICATE, PaypalConstants.PRIVATE_KEY_PASSWORD);    

    //IAPIProfile profile = ProfileFactory.CreateAPIProfile();
    public static IAPIProfile CreateAPIProfile(string apiUsername, string apiPassword, string signature, string CertificateFile_Sig, string APISignature_Sig, string CertificateFile_Cer, string PrivateKeyPassword_Cer)    
    {
        if (is3token == true)
        {
            IAPIProfile profile = ProfileFactory.createSignatureAPIProfile();
            profile.APIUsername = apiUsername;
            profile.APIPassword = apiPassword;
            profile.Environment = PaypalConstants.ENVIRONMENT;

            profile.Subject = String.Empty;
            profile.APISignature = signature;
            return profile;
        }
        else
        {
            IAPIProfile profile = ProfileFactory.createSSLAPIProfile();
            profile.APIUsername = apiUsername;
            profile.APIPassword = apiPassword;
            profile.Environment = PaypalConstants.ENVIRONMENT;

            profile.CertificateFile = CertificateFile_Cer;
            profile.PrivateKeyPassword = PrivateKeyPassword_Cer;
            profile.Subject = signature;
            //profile.Subject = string.Empty;
            //profile.APISignature = signature;
            return profile;
        }
    }


    public static void SetDefaultProfile()
    {
        SetProfile.SessionProfile = SetProfile.DefaultProfile;
    }

    public static IAPIProfile SessionProfile
    {
        get
        {
            return (IAPIProfile)(HttpContext.Current.Session[PaypalConstants.PROFILE_KEY]);
        }
        set
        {
            HttpContext.Current.Session[PaypalConstants.PROFILE_KEY] = value;
        }
    }
}
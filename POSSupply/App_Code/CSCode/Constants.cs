﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

/// <summary>
/// Summary description for Constants
/// </summary>
public class Constants
{
    public static String API_USERNAME = ConfigurationManager.AppSettings["PP_API_USERNAME"].ToString();
    public static String API_PASSWORD = ConfigurationManager.AppSettings["PP_API_PASSWORD"].ToString();
    public static String API_SIGNATURE = ConfigurationManager.AppSettings["PP_API_SIGNATURE"].ToString();
    public static String API_VERSION = "72.0";
    public static String PAYMENTACTION_TYPE = "Sale";
    public static String API_RETURNURL = ConfigurationManager.AppSettings["PP_API_RETURNURL"].ToString();
    public static String API_CANCELURL = ConfigurationManager.AppSettings["PP_API_CANCELURL"].ToString();
    public static String API_CURRENCYCODE = ConfigurationManager.AppSettings["PP_API_CURRENCYCODE"].ToString();




    //public static String CERTIFICATE = ConfigurationManager.AppSettings["PP_CERTIFICATE"].ToString();
    //public static String PRIVATE_KEY_PASSWORD = ConfigurationManager.AppSettings["PP_PRIVATE_KEY_PASSWORD"].ToString();
    //public static String SUBJECT = ConfigurationManager.AppSettings["PP_MAIL_SUBJECT"].ToString();
    //public static String ENVIRONMENT = ConfigurationManager.AppSettings["PP_ENVIRONMENT"].ToString();
    //public const String ECURLLOGIN = "https://developer.paypal.com";

    public const String GET_TRANSACTION_DETAILS_SESSION_KEY = "GetTransactionDetailsResponseType";
    public const String TRANSACTION_SEARCH_SESSION_KEY = "TransactionSearchResponseType";
    public const String RESPONSE_SESSION_KEY = "payPalResponse";
    public const String PROFILE_KEY = "Profile";
    public const String PAYMENTACTIONTYPE_SESSION_KEY = "PaymentActionType";
    public const String TRANSACTIONID_PARAM = "trxID";
    public const String REFUND_TYPE_PARAM = "refundType";
    public const String PAYMENT_AMOUNT_PARAM = "amount";
    public const String PAYMENT_CURRENCY_PARAM = "currency";
    public const String BUYER_LAST_NAME_PARAM = "buyerLastName";
    public const String BUYER_FIRST_NAME_PARAM = "buyerFirstName";
    public const String BUYER_ADDRESS1_PARAM = "buyerAddress1";
    public const String BUYER_ADDRESS2_PARAM = "buyerAddress2";
    public const String BUYER_CITY_PARAM = "buyerCity";
    public const String BUYER_STATE_PARAM = "buyerState";
    public const String BUYER_ZIPCODE_PARAM = "buyerZipCode";
    public const String CREDIT_CARD_TYPE_PARAM = "creditCardType";
    public const String CREDIT_CARD_NUMBER_PARAM = "creditCardNumber";
    public const String CVV2_PARAM = "cvv2";
    public const String EXP_MONTH_PARAM = "expMonth";
    public const String EXP_YEAR_PARAM = "expYear";
    public const String TOKEN_PARAM = "token";
    public const String PAYERID_PARAM = "payerID";
    public const String AUTHORIZATIONID_PARAM = "authorizationID";
    public const String PAYMENT_ACTION_PARAM = "paymentAction";

    public const String REGULAR_RATE = "a3";
    public const String REGULAR_BILLING_CYCLE = "p3";
    public const String REGULAR_BILLING_CYCLE_UNITS = "t3";
    public const String RECURRING_PAYMENTS = "src";
    public const String MODIFICATION_BEHAVIOR = "modify";
    public const String RECURRING_TIMES = "srt";
    public const String REATTEMPT_ON_FAILURE = "sra";
    public const String BUSINESS = "business";
    public const String CURRENCY_CODE = "currency_code";
    public const String ITEM_NAME = "item_name";
    public const String INVOICE = "invoice";
}
public class ACKTyp
{
    public const String SUCCESS = "Success";
    public const String ERROR = "Failure";
}
public class ResponseAPIData
{
    public const String VERSION = "VERSION";
    public const String TIMESTAMP = "TIMESTAMP";
    public const String CORRELATIONID = "CORRELATIONID";
    public const String ACK = "ACK";
    public const String BUILD = "BUILD";
    public const String TOKEN = "TOKEN";
    public const String PAYERID = "PAYERID";
    public const String RANSACTIONID = "PAYMENTREQUEST_0_TRANSACTIONID";
    public const String AMOUNT = "PAYMENTREQUEST_0_AMT";
    public const String CURRENCYCODE="PAYMENTREQUEST_0_CURRENCYCODE";
    public const String PAYMENTSTATUS = "PAYMENTREQUEST_0_PAYMENTSTATUS";

    public const String SEVERITY_CODE = "L_SEVERITYCODE0";
    public const String ERROR_LONG_MESSAGE = "L_LONGMESSAGE0";
    public const String ERROR_SHORT_MESSAGE = "L_SHORTMESSAGE0";
    public const String ERROR_CODE = "L_ERRORCODE0";    
}
public class APIMETHOD
{
    public const String SETDETAILS = "SetExpressCheckout";
    public const String GETDETAILS = "GetExpressCheckoutDetails";
    public const String PAYMET = "DoExpressCheckoutPayment";
}
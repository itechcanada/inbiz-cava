﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using System.Xml;
using iTECH.WebService.Utility;
using iTECH.Library.DataAccess.MySql;
using iTECH.InbizERP.BusinessLogic.MobileReservation;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
[System.Web.Script.Services.ScriptService]

public class Reservation_Service : System.Web.Services.WebService
{
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string Authentication(string emailID, string password, string deviceID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        MReservation mobileReservation = new MReservation();
        int userID = -1;
        try
        {
            userID = mobileReservation.GetUser(emailID, password);
            if (userID > 0)
            {
                Tokens objtoken = new Tokens();
                objtoken.UserID = userID;
                objtoken.ExpirePreviousToken();
                objtoken.TokenData = SingleSignOn.CreateToken(AuthTokenUser(emailID, deviceID));
                objtoken.CheckInType = 1;//By Deafult Set
                AuthToken objAuthToken = new AuthToken(AuthTokenUser(emailID, deviceID));
                objtoken.DeviceID = deviceID;
                objtoken.IsActive = true;
                objtoken.TokenNo = objtoken.Insert();
                dictResult.OkStatus(ResponseCode.SUCCESS);
                dictResult.Add("TokenNo", objtoken.TokenData);
                dictResult.Add("EmailID", emailID);
                dictResult.Add("UserID", userID);
            }
            else
                throw new Exception(ExceptionMessage.INVALIDUSER);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "Authentication", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDUSER ? ResponseCode.NOTREGISTER : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ReservationDetails(string emailID, string tokenNo, string deviceID, string checkInDate, string checkOutDate)
    {
        ResultDictionary dictResult = new ResultDictionary();
        MReservation mobileReservation = new MReservation();
        try
        {
            if (Authorization(emailID, tokenNo, deviceID))
            {
                dictResult.OkStatus(ResponseCode.SUCCESS);
                dictResult.Add("Reservation", mobileReservation.ReservationDetails(null, BusinessUtility.GetDateTime(checkInDate), BusinessUtility.GetDateTime(checkOutDate)));
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "ReservationDetails", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string Reservation(string emailID, string tokenNo, string deviceID, string reservationID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        MReservation mobileReservation = new MReservation();
        try
        {
            if (Authorization(emailID, tokenNo, deviceID))
            {
                dictResult.OkStatus(ResponseCode.SUCCESS);
                dictResult.Add("ReservationFor", mobileReservation.ReservationDetails(null, BusinessUtility.GetInt(reservationID)));
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "ReservationDetails", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string CreateReservation(string emailID, string tokenNo, string deviceID, string reservationNote, string guestName, string noOfGuest, string bedID, string checkInDate, string checkOutDate, string userID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        MReservation mobileReservation = new MReservation();
        try
        {
            if (Authorization(emailID, tokenNo, deviceID))
            {
                bool isSuccess = mobileReservation.MakeReservation(null,  guestName, BusinessUtility.GetInt(noOfGuest), BusinessUtility.GetInt(bedID), BusinessUtility.GetDateTime(checkInDate), BusinessUtility.GetDateTime(checkOutDate), BusinessUtility.GetInt(ConfigurationManager.AppSettings["ReservationPartnerID"]), BusinessUtility.GetInt(userID));
                dictResult.OkStatus(isSuccess?ResponseCode.SUCCESS:ResponseCode.UNSUCCESS);
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "CreateReservation", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }


    #region Private Methods
    private bool Authorization(string emailID, string tokenNo, string deviceID)
    {
        bool check1 = new SingleSignOn().Authenticate(AuthTokenUser(emailID, deviceID), tokenNo);
        bool check2 = new Tokens().IsValidToken(emailID, deviceID);
        return check1 && check2;
    }
    private string AuthTokenUser(string emailID, string deviceID)
    {
        return string.Format("{0}{1}", emailID, deviceID);
    }
    #endregion
}
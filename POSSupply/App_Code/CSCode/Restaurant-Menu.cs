﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using System.Xml;
using iTECH.WebService.Utility;
using iTECH.Library.DataAccess.MySql;
using System.Net.Mail;
using System.Resources;
using System.Globalization;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
[System.Web.Script.Services.ScriptService]
public class Restaurant_Menu : System.Web.Services.WebService
{
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string Authentication(string emailID, string password, string deviceID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        int userID = -1;
        try
        {
            userID = new Partners().ValidatePartner(emailID, password);
            if (userID > 0)
            {
                Tokens objtoken = new Tokens();
                objtoken.UserID = userID;
                objtoken.ExpirePreviousToken();
                objtoken.TokenData = SingleSignOn.CreateToken(AuthTokenUser(emailID, deviceID));
                objtoken.CheckInType = 1;//By Deafult Set
                AuthToken objAuthToken = new AuthToken(AuthTokenUser(emailID, deviceID));
                objtoken.DeviceID = deviceID;
                objtoken.IsActive = true;
                objtoken.TokenNo = objtoken.Insert();
                dictResult.OkStatus(ResponseCode.SUCCESS);
                dictResult.Add("TokenNo", objtoken.TokenData);
                dictResult.Add("EmailID", emailID);
                dictResult.Add("UserID", userID);
            }
            else
                throw new Exception(ExceptionMessage.INVALIDUSER);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "Authentication", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDUSER ? ResponseCode.NOTREGISTER : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string CategoryANDProduct(string lang, string logInID, string deviceID, string token)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            if (Authorization(logInID, token, deviceID))
            {
                dictResult.OkStatus(ResponseCode.SUCCESS);
                dictResult.Add("Category", new CategoryWithProduct().CategoryAndProducts(null, !string.IsNullOrEmpty(lang) ? lang : "en"));
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "CategoryANDProduct", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string CountStatus(string statusID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            int statusType = BusinessUtility.GetInt(statusID);
            dictResult.OkStatus(ResponseCode.SUCCESS);
            dictResult.Add("Count", new CategoryWithProduct().GetCountStatus(null, statusType));
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "CountStatus", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string AllStatusCount(string confCode, string IsOnHold)
    {
        ResultDictionary dictResult = new ResultDictionary();
        DbHelper dbHelper = new DbHelper();
        try
        {
            dictResult.OkStatus(ResponseCode.SUCCESS);
            dictResult.Add("CountACCEPT", new CategoryWithProduct().GetCountStatus(dbHelper, (int)RESTAURANTMENU_ORDERSTATUS.ACCEPT));
            dictResult.Add("CountHELD", new CategoryWithProduct().GetCountStatus(dbHelper, (int)RESTAURANTMENU_ORDERSTATUS.HELD));
            dictResult.Add("CountNEW", new CategoryWithProduct().GetCountStatus(dbHelper, (int)RESTAURANTMENU_ORDERSTATUS.NEW));
            dictResult.Add("CountREADY", new CategoryWithProduct().GetCountStatus(dbHelper, (int)RESTAURANTMENU_ORDERSTATUS.READY));

            if (!string.IsNullOrEmpty(confCode))
            {
                dictResult.Add("Transition", new CategoryWithProduct().GetTransition(dbHelper, confCode));
            }
            else
            {
                if (BusinessUtility.GetBool(IsOnHold))
                {

                    dictResult.Add("Transition", new CategoryWithProduct().GetTransition(dbHelper, BusinessUtility.GetBool(IsOnHold)));
                }
                else
                {
                    dictResult.Add("Transition", new CategoryWithProduct().GetTransition(dbHelper, 0, 0));
                }
            }

        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "AllStatusCount", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        finally { dbHelper.CloseDatabaseConnection(); }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ChangesStatus(string statusTypeID, string transID, string userID, string timeInMin, string IsOnHold)
    {
        ResultDictionary dictResult = new ResultDictionary();
        DbHelper dbHelper = new DbHelper();
        try
        {
            dictResult.OkStatus(ResponseCode.SUCCESS);
            if ((!string.IsNullOrEmpty(transID)) && (!string.IsNullOrEmpty(statusTypeID)))
            {
                new CategoryWithProduct().SetStatus(dbHelper, BusinessUtility.GetInt(statusTypeID), BusinessUtility.GetInt(transID), BusinessUtility.GetInt(userID), timeInMin, IsOnHold);
            }
            bool chkLPAllreadyAdded = new CategoryWithProduct().CheckPointAddedExist(transID, dbHelper);
            if (statusTypeID == "13" && chkLPAllreadyAdded == false)
            {
                int calculatedPoint = 0;
                SysWarehouses _whs = new SysWarehouses();
                double amtPerPoint = _whs.GetAmtPerPoint(System.Configuration.ConfigurationManager.AppSettings["WebSaleWhsCode"]);
                List<object> lstResult = new List<object>();
                lstResult = new CategoryWithProduct().TotalAmt(dbHelper, BusinessUtility.GetInt(transID));
                if (amtPerPoint > 0)
                {
                    calculatedPoint = BusinessUtility.GetInt(BusinessUtility.GetDouble(lstResult[0]) / amtPerPoint);
                }
                Partners part = new Partners();
                part.PopulateObject(BusinessUtility.GetInt(lstResult[1]));
                LoyalPartnerHistory lph = new LoyalPartnerHistory();
                lph.LoyalCategoryID = part.PartnerLoyalCategoryID;
                lph.LoyalPartnerID = part.PartnerID;
                lph.PointAuto = false;
                lph.Points = calculatedPoint;
                lph.PointsAddedBy = BusinessUtility.GetInt(userID);
                lph.PointsAddedOn = DateTime.Now;
                lph.PosTransactionID = BusinessUtility.GetInt(transID);
                lph.Insert(dbHelper);
            }
            if (statusTypeID == "13" && chkLPAllreadyAdded == true)
            {
                int calculatedPoint = 0;
                SysWarehouses _whs = new SysWarehouses();
                double amtPerPoint = _whs.GetAmtPerPoint(System.Configuration.ConfigurationManager.AppSettings["WebSaleWhsCode"]);
                List<object> lstResult = new List<object>();
                lstResult = new CategoryWithProduct().TotalAmt(dbHelper, BusinessUtility.GetInt(transID));
                double unPaidAmt = new CategoryWithProduct().UnPaidPartialAmt(BusinessUtility.GetInt(transID), dbHelper);
                if (unPaidAmt > 0)
                {
                    if (amtPerPoint > 0)
                    {
                        calculatedPoint = BusinessUtility.GetInt(BusinessUtility.GetDouble(unPaidAmt) / amtPerPoint);
                    }
                    Partners part = new Partners();
                    part.PopulateObject(BusinessUtility.GetInt(lstResult[1]));
                    LoyalPartnerHistory lph = new LoyalPartnerHistory();
                    lph.LoyalCategoryID = part.PartnerLoyalCategoryID;
                    lph.LoyalPartnerID = part.PartnerID;
                    lph.PointAuto = false;
                    lph.Points = calculatedPoint;
                    lph.PointsAddedBy = BusinessUtility.GetInt(userID);
                    lph.PointsAddedOn = DateTime.Now;
                    lph.PosTransactionID = BusinessUtility.GetInt(transID);
                    lph.Insert(dbHelper);
                }
            }
            dictResult.Add("CountACCEPT", new CategoryWithProduct().GetCountStatus(dbHelper, (int)RESTAURANTMENU_ORDERSTATUS.ACCEPT));
            dictResult.Add("CountHELD", new CategoryWithProduct().GetCountStatus(dbHelper, (int)RESTAURANTMENU_ORDERSTATUS.HELD));
            dictResult.Add("CountNEW", new CategoryWithProduct().GetCountStatus(dbHelper, (int)RESTAURANTMENU_ORDERSTATUS.NEW));
            dictResult.Add("CountREADY", new CategoryWithProduct().GetCountStatus(dbHelper, (int)RESTAURANTMENU_ORDERSTATUS.READY));

            if (BusinessUtility.GetBool(IsOnHold))//if (!string.IsNullOrEmpty(confCode))
            {
                dictResult.Add("Transition", new CategoryWithProduct().GetTransition(dbHelper, BusinessUtility.GetBool(IsOnHold)));
            }
            else
            {
                dictResult.Add("Transition", new CategoryWithProduct().GetTransition(dbHelper, 0, 0));
            }
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "ChangesStatus", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        finally { dbHelper.CloseDatabaseConnection(); }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string Transaction(string transID, string logInID, string deviceID, string token)
    {
        ResultDictionary dictResult = new ResultDictionary();
        DbHelper dbHelper = new DbHelper();
        try
        {
            if (Authorization(logInID, token, deviceID))
            {
                int TransID = !string.IsNullOrEmpty(transID) ? BusinessUtility.GetInt(transID) : 0;
                dictResult.OkStatus(ResponseCode.SUCCESS);
                dictResult.Add("Transition", new CategoryWithProduct().GetTransition(dbHelper, TransID, 0));
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "Transition", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        finally { dbHelper.CloseDatabaseConnection(); }
        return dictResult.ResultInInJSONFormat;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ShopTime()
    {
        ResultDictionary dictResult = new ResultDictionary();
        DbHelper dbHelper = new DbHelper();
        try
        {
            dictResult.OkStatus(ResponseCode.SUCCESS);
            dictResult.Add("ShopTime", new CategoryWithProduct().ShopTime(dbHelper));
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "Transition", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        finally { dbHelper.CloseDatabaseConnection(); }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string TransactionWithCustDetails(string userID, string regCode, string totalAmount, string totalSubAmount, string whsCode,
      string productDetails, string tableNote, string custPhoneNo, string custPickUpDateTime, string custAdditionalRequest, string custEmailID, string deviceID, string token)
    {
        ResultDictionary dictResult = new ResultDictionary();
        clsPOSTransaction objTransaction = new clsPOSTransaction();
        clsPosProductNo objPosPrdNo = new clsPosProductNo();
        clsPosTransAccHdr objPosTranAccHrd = new clsPosTransAccHdr();
        clsProducts objProduct = new clsProducts();
        clsPrdQuantity objPrdQty = new clsPrdQuantity();
        string strTranID = null;
        string strvalue = null;
        string strBtnType = "tab";
        strvalue = string.Empty;
        string strTransType = "9"; ;
        string strReturnAmt = "0.00";
        string strReceivedAmt = "0.0";
        string[] item = { "__item__" };
        string[] tax = { "__taxGrp__" };

        try
        {
            // Get Table Note from Database
            //iTECH.InbizERP.BusinessLogic.CategoryWithProduct objCateProduct = new iTECH.InbizERP.BusinessLogic.CategoryWithProduct();
            //tableNote += objCateProduct.GetTransitionTableNote();
            if (Authorization(custEmailID, token, deviceID))
            {

                tableNote = BusinessUtility.GetString(BusinessUtility.GenerateRandomNumber(5));
                objTransaction.Tax4 = "0";
                objTransaction.Tax3 = "0";
                objTransaction.Tax2 = "0";
                objTransaction.Tax1 = "0";

                objTransaction.TransStatus = "1";

                switch (strTransType)
                {
                    case "1":
                        objPosTranAccHrd.AcctPayType = "1";//Condition for lost transaction
                        break;
                    case "2":
                        objPosTranAccHrd.AcctPayType = "2";
                        break;
                    case "3":
                        objPosTranAccHrd.AcctPayType = "3";
                        break;
                    case "4":
                        objPosTranAccHrd.AcctPayType = "4";
                        break;
                    case "5":
                        objPosTranAccHrd.AcctPayType = "5";
                        break;
                    case "6":
                        objPosTranAccHrd.AcctPayType = "1";
                        break;
                    case "7":
                        objPosTranAccHrd.AcctPayType = "7"; //Condition for Gift transaction
                        objTransaction.TransStatus = "0";
                        break;
                    case "8":
                        objPosTranAccHrd.AcctPayType = "8"; //Condition for Staff Gift transaction
                        objTransaction.PosPercentDiscount = "100";
                        objPosTranAccHrd.CashReturned = "0";
                        objPosTranAccHrd.CasReceived = "0";
                        break;
                    case "9":
                        objPosTranAccHrd.AcctPayType = "9";
                        objTransaction.PosPercentDiscount = "100";
                        objPosTranAccHrd.CashReturned = "0";
                        objPosTranAccHrd.CasReceived = "0";
                        break;
                }

                objTransaction.PosTabNote = tableNote;
                objTransaction.TransStatus = "9";
                objPosTranAccHrd.CashReturned = strReturnAmt;
                objPosTranAccHrd.CasReceived = strReceivedAmt;
                objTransaction.TransUserID = userID;
                objTransaction.TransRegCode = regCode;
                objTransaction.TransType = "S";
                objTransaction.TransDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                objTransaction.TotalValue = totalAmount;
                objTransaction.SubTotal = totalSubAmount;
                objTransaction.OldTransID = "sysNull";
                objTransaction.PosGiftReason = "sysNull";
                objTransaction.PosAuthorizedBy = "sysNull";

                //***************************** Insert in Transaction Table
                objTransaction.funInsertPosTransaction(ref strTranID);
                objPosTranAccHrd.AcctTransID = strTranID;

                tableNote = string.Format("{0}{1}", strTranID, tableNote);
                objTransaction.funUpdatePosTransactionTableNote(tableNote, strTranID);
                if (string.IsNullOrEmpty(custPickUpDateTime))
                    custPickUpDateTime = System.DateTime.Now.AddMinutes(15).ToString("yyyy-MM-dd HH:mm");
                objTransaction.funInsertPosTransactionCutomerDetails(ref strTranID, ref custPhoneNo, ref custEmailID, ref custPickUpDateTime, ref custAdditionalRequest, ref userID);

                string strTranAccHrdID = string.Empty;
                objPosTranAccHrd.funInsertpostransaccthdr(ref strTranAccHrdID);


                //***************************** Insert in posproductno
                string[] strPrd = productDetails.Split(item, System.StringSplitOptions.None);
                string[] strTaxOnPrd = null;
                string[] strTaxValue = null;
                int intI = 0;
                int intR = 0;
                string[] strPrdItem = null;
                objPosPrdNo.PrdTransID = strTranID;
                bool checkPrdDescExec = false;
                while (strPrd.Length - 1 > intI)
                {
                    strTaxValue = strPrd[intI].Split(tax, System.StringSplitOptions.None);
                    strPrdItem = strTaxValue[0].Split('~');
                    if (strPrdItem.Length > 1)
                    {
                        objPosPrdNo.ProductID = strPrdItem[0];
                        objPosPrdNo.PrdQty = strPrdItem[1];
                        objPosPrdNo.PrdUnitPrice = strPrdItem[2];
                        objPosPrdNo.PrdPrice = strPrdItem[3];
                        objPosPrdNo.Tax4 = "0";
                        objPosPrdNo.Tax3 = "0";
                        objPosPrdNo.Tax2 = "0";
                        objPosPrdNo.Tax1 = "0";
                        if (strTaxValue.Length > 1)
                        {
                            strTaxOnPrd = strTaxValue[1].Split('^');
                            while (strTaxOnPrd.Length - 1 > intR)
                            {
                                string[] taxDetails = strTaxOnPrd[intR].Split('~');
                                if (taxDetails.Length > 1)
                                {
                                    switch (intR)
                                    {
                                        case 0:
                                            objPosPrdNo.Tax1 = taxDetails[1];
                                            objPosPrdNo.Tax1Desc = taxDetails[0];
                                            break;
                                        case 1:
                                            objPosPrdNo.Tax2 = taxDetails[1];
                                            objPosPrdNo.Tax2Desc = taxDetails[0];
                                            break;
                                        case 2:
                                            objPosPrdNo.Tax3 = taxDetails[1];
                                            objPosPrdNo.Tax3Desc = taxDetails[0];
                                            break;
                                        case 3:
                                            objPosPrdNo.Tax4 = taxDetails[1];
                                            objPosPrdNo.Tax4Desc = taxDetails[0];
                                            break;
                                    }
                                }
                                intR += 1;
                            }
                        }
                        intR = 0;
                        objPosPrdNo.PrdTaxCode = objTransaction.funPrdTax(strPrdItem[0], whsCode).ToString();
                        objPosPrdNo.funInsertPosProduct();
                        checkPrdDescExec = true;
                        if (string.IsNullOrEmpty(strBtnType))
                        {
                            if (objProduct.funGetPrdValue(objPosPrdNo.ProductID, "kit") == "1")
                                objPrdQty.subGetPrdKitOnHandQty(objPosPrdNo.ProductID, whsCode, objPosPrdNo.PrdQty, "");
                            else
                                objPrdQty.funGetReducePrdKitQty(objPosPrdNo.ProductID, whsCode, "");
                            objPrdQty.updateProductQuantity(objPosPrdNo.ProductID, whsCode, objPosPrdNo.PrdQty, "");
                        }
                    }
                    intI += 1;
                }
                if (checkPrdDescExec)
                {
                    dictResult.OkStatus(ResponseCode.SUCCESS);
                    dictResult.Add("TableNote", tableNote);
                    dictResult.Add("TransitionID", strTranID);
                    checkPrdDescExec = false;
                }
                else
                {
                    dictResult.ErrorStatus(ResponseCode.UNSUCCESS);
                }
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "TransactionWithCustDetails", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string SignUp(string lang, string name, string logInID, string password, string phone, string address, string city, string postalCode, string state, string country)
    {
        ResultDictionary dictResult = new ResultDictionary();
        DbHelper dbHelper = new DbHelper();
        try
        {
            lang = (!string.IsNullOrEmpty(lang)) ? lang : "en";
            Partners partner = new Partners();
            if (partner.CheckEmailIDExists(null, logInID))
                throw new Exception(CustomExceptionCodes.DUPLICATE_KEY);
            List<Addresses> lstAddress = new List<Addresses>();
            partner.PartnerActive = true;
            partner.PartnerCreatedBy = 1;
            partner.PartnerCreatedOn = DateTime.Now;
            partner.PartnerEmail = logInID;
            partner.PartnerLongName = name;
            partner.PartnerPassword = password;
            partner.PartnerPhone = phone;
            partner.PartnerLang = lang;
            partner.PartnerType = (int)PartnerTypeIDs.EndClient;
            Addresses addressPartner = new Addresses();
            addressPartner.AddressCity = city;
            addressPartner.AddressCountry = country;
            addressPartner.AddressLine1 = address;
            addressPartner.AddressPostalCode = postalCode;
            addressPartner.AddressState = state;
            addressPartner.AddressType = AddressType.SHIP_TO_ADDRESS;
            addressPartner.AddressRef = AddressReference.CUSTOMER_CONTACT;
            lstAddress.Add(addressPartner);
            partner.Insert(null, 1, lstAddress);
            dictResult.OkStatus(ResponseCode.SUCCESS);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "SignUp", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ex.Message == CustomExceptionCodes.DUPLICATE_KEY ? ResponseCode.UNSUCCESS : ResponseCode.EXCEPTION);
        }
        finally { dbHelper.CloseDatabaseConnection(); }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string PartnerDetails(string partnerID, string logInID, string deviceID, string token)
    {
        ResultDictionary dictResult = new ResultDictionary();
        DbHelper dbHelper = new DbHelper();
        try
        {
            if (Authorization(logInID, token, deviceID))
            {
                Partners partner = new Partners();
                partner.PopulateObject(BusinessUtility.GetInt(partnerID));
                Addresses add = new Addresses();
                add.PopulateObject(partner.PartnerID, AddressReference.CUSTOMER_CONTACT, AddressType.SHIP_TO_ADDRESS);
                dictResult.OkStatus(ResponseCode.SUCCESS);
                dictResult.Add("Partners", partner);
                dictResult.Add("Address", add);
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "PartnerDetails", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ex.Message == CustomExceptionCodes.DUPLICATE_KEY ? ResponseCode.UNSUCCESS : ResponseCode.EXCEPTION);
        }
        finally { dbHelper.CloseDatabaseConnection(); }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string UpdatePartner(string partnerID, string name, string logInID, string phone, string address, string city, string postalCode, string state, string country, string deviceID, string token)
    {
        ResultDictionary dictResult = new ResultDictionary();
        DbHelper dbHelper = new DbHelper();
        try
        {
            if (Authorization(logInID, token, deviceID))
            {
                Partners partner = new Partners();
                partner.PartnerID = BusinessUtility.GetInt(partnerID);
                partner.PopulateObject(BusinessUtility.GetInt(partnerID));

                List<Addresses> lstAddress = new List<Addresses>();
                partner.PartnerActive = true;
                partner.PartnerLongName = name;
                partner.PartnerPhone = phone;

                Addresses addressPartner = new Addresses();
                addressPartner.AddressCity = city;
                addressPartner.AddressCountry = country;
                addressPartner.AddressLine1 = address;
                addressPartner.AddressPostalCode = postalCode;
                addressPartner.AddressState = state;
                addressPartner.AddressType = AddressType.SHIP_TO_ADDRESS;
                addressPartner.AddressRef = AddressReference.CUSTOMER_CONTACT;
                lstAddress.Add(addressPartner);
                partner.Update(null, 1, lstAddress);
                dictResult.OkStatus(ResponseCode.SUCCESS);
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "UpdatePartner", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ex.Message == CustomExceptionCodes.DUPLICATE_KEY ? ResponseCode.UNSUCCESS : ResponseCode.EXCEPTION);
        }
        finally { dbHelper.CloseDatabaseConnection(); }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ChangePassword(string partnerID, string oldPassword, string newPassword, string logInID, string deviceID, string token)
    {
        ResultDictionary dictResult = new ResultDictionary();
        DbHelper dbHelper = new DbHelper();
        try
        {
            if (Authorization(logInID, token, deviceID))
            {
                Partners partner = new Partners();
                partner.PartnerID = BusinessUtility.GetInt(partnerID);
                partner.ChangePassowrd(oldPassword, newPassword);
                dictResult.OkStatus(ResponseCode.SUCCESS);
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "ChangePassword", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ex.Message == "PASSWORD-NOT-MATCH" ? ResponseCode.UNSUCCESS : ResponseCode.EXCEPTION);
        }
        finally { dbHelper.CloseDatabaseConnection(); }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string PartnerForgetPassword(string lang, string loginID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        DbHelper dbHelper = new DbHelper();
        try
        {
            Partners partner = new Partners();
            string password = partner.RestPassword(loginID);
            dictResult.OkStatus(ResponseCode.SUCCESS);
            try
            {
                SysCompanyInfo _company = new SysCompanyInfo();
                _company.CompanyID = 5;
                _company.PopulateObject(_company.CompanyID, dbHelper);

                string emailSentTo = ConfigurationManager.AppSettings["NoReplyEmail"];
                string email = loginID;
                string subject = "";
                string messageBody = "";
                lang = (!string.IsNullOrEmpty(lang)) ? lang : "en";
                if (lang == "en")
                {
                    subject = _company.CompanyName + " Password Reset";
                    messageBody = "Hello, <br/> Your password has been changed successfully.<br/> Your new Password is " + " " + password;
                }
                else
                {
                    subject = _company.CompanyName + " " + "réinitialiser le mot de passe";
                    messageBody = "Bonjour, <br/> Votre mot des passe est modifié. <br/> Voici votre nouveau mot de passe " + " " + password;
                }
                email = email.Replace(string.Format("{0}", "\n"), " ");
                subject = subject.Replace(string.Format("{0}", "\n"), " ");
                messageBody = messageBody.Replace(string.Format("{0}", "\n"), " ");
                iTECH.Library.Utilities.EmailHelper.SendEmail(emailSentTo, email, messageBody, subject, true);
            }
            catch (SmtpException e) { }
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "PartnerForgetPassword", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ex.Message == CustomExceptionCodes.INVALID_EMAIL_ID ? ResponseCode.UNSUCCESS : ResponseCode.EXCEPTION);
        }
        finally { dbHelper.CloseDatabaseConnection(); }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string OrderStatus(int orderID, string logInID, string deviceID, string token)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            if (Authorization(logInID, token, deviceID))
            {
                dictResult.OkStatus(ResponseCode.SUCCESS);
                dictResult.Add("OrderStatus", GetStatusIntToString(new CategoryWithProduct().OrderStatus(orderID)));
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "OrderStatus", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string OrderList(string loginID, string deviceID, string token)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            if (Authorization(loginID, token, deviceID))
            {
                dictResult.OkStatus(ResponseCode.SUCCESS);
                List<object> lstResult = new List<object>();
                List<object> newListResult = new List<object>();
                lstResult = new CategoryWithProduct().OrderList(loginID);
                foreach (Dictionary<object, object> dResult in lstResult)
                {
                    dResult["Status"] = GetStatusIntToString(BusinessUtility.GetInt(dResult["Status"]));
                    newListResult.Add(dResult);
                }
                dictResult.Add("OrderList", (newListResult));
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "OrderList", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string OrderDetails(int orderID, string logInID, string deviceID, string token)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            if (Authorization(logInID, token, deviceID))
            {
                dictResult.OkStatus(ResponseCode.SUCCESS);
                dictResult.Add("OrderDetails", new CategoryWithProduct().OrderDetails(null, orderID));
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "OrderDetails", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string AvailablePoints(string location, string userID, string logInID, string deviceID, string token)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            if (Authorization(logInID, token, deviceID))
            {
                dictResult.OkStatus(ResponseCode.SUCCESS);
                if (string.IsNullOrEmpty(location))
                    location = System.Configuration.ConfigurationManager.AppSettings["WebSaleWhsCode"];
                DbHelper dbHelper = new DbHelper();
                LoyalPartnerHistory plh = new LoyalPartnerHistory();
                //double points = plh.GetPartnerLoyaltyPoints(dbHelper, BusinessUtility.GetInt(userID));
                dictResult.Add("Points", plh.GetPartnerLoyaltyPoints(dbHelper, BusinessUtility.GetInt(userID)));
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "AvailablePoints", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string CompanyInfo(string RegCode)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            string sSiteVirtualDirectory = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + VirtualPathUtility.ToAbsolute("~");

            SysRegister sr = new SysRegister();
            SysCompanyInfo _company = new SysCompanyInfo();
            DbHelper dbHelper = new DbHelper();
            _company.CompanyID = 5;
            _company.PopulateObject(_company.CompanyID, dbHelper);
            sr.PopulateObject(null, RegCode); // Populate object information by regCode    
            dictResult.OkStatus(ResponseCode.SUCCESS);
            dictResult.Add("Name", sr.SysRegDesc);
            string address = "";
            if (!string.IsNullOrEmpty(sr.SysRegAddress))
            {
                address = sr.SysRegAddress;
            }
            if (!string.IsNullOrEmpty(sr.SysRegCity))
            {
                if (!string.IsNullOrEmpty(sr.SysRegAddress))
                {
                    address += "," + sr.SysRegCity;
                }
                else
                {
                    address += sr.SysRegCity;
                }
            }
            if (!string.IsNullOrEmpty(sr.SysRegState))
            {
                if (!string.IsNullOrEmpty(sr.SysRegCity))
                {
                    address += "," + sr.SysRegState;
                }
                else
                {
                    address += sr.SysRegState;
                }
            }
            if (!string.IsNullOrEmpty(sr.SysRegPostalCode))
            {
                if (!string.IsNullOrEmpty(sr.SysRegState))
                {
                    address += "," + sr.SysRegPostalCode;
                }
                else
                {
                    address += sr.SysRegPostalCode;
                }
            }
            //dictResult.Add("Address", sr.SysRegAddress + "," + sr.SysRegCity + "," + sr.SysRegState + "," + sr.SysRegPostalCode);
            dictResult.Add("Address", address);
            dictResult.Add("Message", sr.SysRegMessage);
            dictResult.Add("Tel", sr.SysRegPhone);
            dictResult.Add("Email", sr.SysRegEmailID);
            dictResult.Add("Logo path", sSiteVirtualDirectory + "/Upload/companylogo/" + _company.CompanyLogoPath);

        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "CompanyInfo", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string Logout(string userID, string logInID, string deviceID, string token)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            if (Authorization(logInID, token, deviceID))
            {
                Tokens tk = new Tokens();
                tk.UserID = BusinessUtility.GetInt(userID);
                tk.ExpirePreviousToken();
                dictResult.OkStatus(ResponseCode.SUCCESS);
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "Logout", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string Reorder(int orderID, string logInID, string deviceID, string token)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            if (Authorization(logInID, token, deviceID))
            {
                dictResult.OkStatus(ResponseCode.SUCCESS);
                dictResult.Add("ReOrderDetails", new CategoryWithProduct().ROrderDetails(null, orderID));
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "ReOrderDetails", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string AddFavourateProduct(string partnerID, string productID, string logInID, string deviceID, string token)
    {
        ResultDictionary dictResult = new ResultDictionary();
        DbHelper dbHelper = new DbHelper();
        try
        {
            if (Authorization(logInID, token, deviceID))
            {
                CategoryWithProduct objProducts = new CategoryWithProduct();
                objProducts.InsertPOSFavProducts(dbHelper, BusinessUtility.GetInt(productID), BusinessUtility.GetInt(partnerID));
                dictResult.OkStatus(ResponseCode.SUCCESS);
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "UpdatePartner", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ex.Message == CustomExceptionCodes.DUPLICATE_KEY ? ResponseCode.UNSUCCESS : ResponseCode.EXCEPTION);
        }
        finally { dbHelper.CloseDatabaseConnection(); }
        return dictResult.ResultInInJSONFormat;
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string RemoveFavourateProduct(string partnerID, string productID, string logInID, string deviceID, string token)
    {
        ResultDictionary dictResult = new ResultDictionary();
        DbHelper dbHelper = new DbHelper();
        try
        {
            if (Authorization(logInID, token, deviceID))
            {
                CategoryWithProduct objProducts = new CategoryWithProduct();
                objProducts.DeletePOSFavProducts(dbHelper, BusinessUtility.GetInt(productID), BusinessUtility.GetInt(partnerID));
                dictResult.OkStatus(ResponseCode.SUCCESS);
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "UpdatePartner", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ex.Message == CustomExceptionCodes.DUPLICATE_KEY ? ResponseCode.UNSUCCESS : ResponseCode.EXCEPTION);
        }
        finally { dbHelper.CloseDatabaseConnection(); }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string FavourateProduct(string partnerID, string lang, string logInID, string deviceID, string token)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            if (Authorization(logInID, token, deviceID))
            {
                dictResult.OkStatus(ResponseCode.SUCCESS);
                dictResult.Add("FavourateProduct", new CategoryWithProduct().ProductsUserFavourate(null, BusinessUtility.GetInt(partnerID), !string.IsNullOrEmpty(lang) ? lang : "en"));
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "CategoryANDProduct", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.INVAILDTOKEN : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }

    #region Private Methods
    private bool Authorization(string emailID, string tokenNo, string deviceID)
    {
        bool check1 = new SingleSignOn().Authenticate(AuthTokenUser(emailID, deviceID), tokenNo);
        bool check2 = new Tokens().IsValidTokenForPartner(emailID, deviceID);
        return check1 && check2;
    }
    private string AuthTokenUser(string emailID, string deviceID)
    {
        return string.Format("{0}{1}", emailID, deviceID);
    }
    private string GetStatusIntToString(int statusCode)
    {
        string statusType = "";
        switch (statusCode)
        {
            case (int)RESTAURANTMENU_ORDERSTATUS.NEW:
                statusType = Resources.Resource.lblNew;
                break;
            case (int)RESTAURANTMENU_ORDERSTATUS.ACCEPT:
                statusType = Resources.Resource.lblAccept;
                break;
            case (int)RESTAURANTMENU_ORDERSTATUS.HELD:
                statusType = Resources.Resource.lblHeld;
                break;
            case (int)RESTAURANTMENU_ORDERSTATUS.READY:
                statusType = Resources.Resource.lblReady;
                break;
            case (int)RESTAURANTMENU_ORDERSTATUS.PICKEDUP:
                statusType = Resources.Resource.lblPickedUp;
                break;
        }
        return statusType;
    }
    #endregion


}
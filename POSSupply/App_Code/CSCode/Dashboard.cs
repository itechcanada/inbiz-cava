﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;

using Newtonsoft.Json;

using iTECH.InbizERP.BusinessLogic;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

public class DashboardSettings
{
    public List<string> WhsCodsToFilter { get; set; }
    public DashboardSettings()
    {
        this.WhsCodsToFilter = new List<string>();
    }

    private static void SetSeetingMeta(string key, string meta)
    {
        string sqlDelte = "DELETE FROM z_dashboard_settings WHERE SettingKey=@SettingKey";
        string sqlInsert = "INSERT INTO z_dashboard_settings(SettingKey,SettingMeta) VALUES(@SettingKey,@SettingMeta)";
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            dbHelp.ExecuteNonQuery(sqlDelte, CommandType.Text, new MySqlParameter[] { 
                DbUtility.GetParameter("SettingKey", key, MyDbType.String)
            });
            dbHelp.ExecuteNonQuery(sqlInsert, CommandType.Text, new MySqlParameter[] { 
                DbUtility.GetParameter("SettingKey", key, MyDbType.String),
                DbUtility.GetParameter("SettingMeta", meta, MyDbType.String)
            });
        }
        catch
        {

            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    public static void SetWhsMeta(List<string> lstWhs)
    {
        string strMeta = JsonConvert.SerializeObject(lstWhs);
        SetSeetingMeta(DashboardSettingKey.Warehouses.ToString(), strMeta);
    }

    public static void FillWhsCods(ListControl lCtrl, ListItem defaultRootItem)
    {
        string sql = "SELECT SettingMeta FROM z_dashboard_settings WHERE SettingKey=@SettingKey";
        DbHelper dbHelp = new DbHelper();
        try
        {
            object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                DbUtility.GetParameter("SettingKey", DashboardSettingKey.Warehouses.ToString(), MyDbType.String)
            });
            //List<string> lResult = JsonConvert.DeserializeObject<List<string>>(BusinessUtility.GetString(val)); 
            List<string> lResult = JsonConvert.DeserializeObject<List<string>>(BusinessUtility.GetString(val).Replace("VMN", CurrentUser.UserDefaultWarehouse )); // ["VMN"]
            lCtrl.DataSource = lResult;
            lCtrl.DataBind();
            if (defaultRootItem != null)
            {
                //lCtrl.Items.Insert(0, defaultRootItem);
                lCtrl.Items.Insert(0, defaultRootItem);
                lCtrl.Items[0].Selected = true;
            }
        }
        catch
        {

            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    public static void FillAllWhsCode(ListControl lCtrl, string userWhs, ListItem defaultRootItem)
    {
        string strSQL = null;
        List<MySqlParameter> p = new List<MySqlParameter>();
        strSQL = "SELECT WarehouseCode, concat(upper(WarehouseCode),'::',substring(WarehouseDescription,1,15)) as WD FROM syswarehouses WHERE WarehouseActive='1'  ";
        strSQL += " order by WarehouseCode";
        string sqlAssigned = "SELECT SettingMeta FROM z_dashboard_settings WHERE SettingKey=@SettingKey";
        DbHelper dbHelper = new DbHelper(true);
        try
        {
            lCtrl.Items.Clear();
            lCtrl.DataSource = dbHelper.GetDataTable(strSQL, System.Data.CommandType.Text, p.ToArray());
            lCtrl.DataTextField = "WD";
            lCtrl.DataValueField = "WarehouseCode";
            lCtrl.DataBind();
            if (defaultRootItem != null)
            {
                lCtrl.Items.Insert(0, defaultRootItem);                
            }
            object val = dbHelper.GetValue(sqlAssigned, CommandType.Text, new MySqlParameter[] { 
                DbUtility.GetParameter("SettingKey", DashboardSettingKey.Warehouses.ToString(), MyDbType.String)
            });
            List<string> lResult = JsonConvert.DeserializeObject<List<string>>(BusinessUtility.GetString(val));
            if (lResult != null)
            {
                foreach (var item in lResult)
                {
                    ListItem li = lCtrl.Items.FindByValue(item);
                    if (li != null)
                    {
                        li.Selected = true;
                    }
                }
            }            
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelper.CloseDatabaseConnection();
        }
    }

    /// <summary>
    /// Temporary for now need to move in businesslogic
    /// </summary>
    /// <param name="lCtrl"></param>
    /// <param name="rootItem"></param>
    /// <param name="lang"></param>
    public static void FillAuthorizeBy(ListControl lCtrl, ListItem rootItem, string lang)
    {
        string sql = "SELECT AuthorizeID, AuthorizeByName FROM z_pos_authorizeby_list WHERE IsActive=1 ";
        sql += " ORDER BY AuthorizeByName";

        DbHelper dbHelp = new DbHelper();
        try
        {
            lCtrl.DataSource = dbHelp.GetDataTable(sql, CommandType.Text, null);
            lCtrl.DataTextField = "AuthorizeByName";
            lCtrl.DataValueField = "AuthorizeID";
            lCtrl.DataBind();

            if (rootItem != null)
            {
                lCtrl.Items.Insert(0, rootItem);
            }
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

   
}

public class BarChartSettings
{
    public WijmoHeader Header { get; set; }
    public List<BarChartData> SeriesList { get; set; }
    public object Axis { get; set; }
    public string ChartLabelFormatString { get; set; }

    public BarChartSettings()
    {
        this.Header = new WijmoHeader { text = string.Empty };
        SeriesList = new List<BarChartData>();
        this.Axis = null;
        this.ChartLabelFormatString = string.Empty;
    }
}

public class PieChartSettings
{
    public WijmoHeader Header { get; set; }
    public List<PieChartData> SeriesList { get; set; }

    public PieChartSettings()
    {
        this.Header = new WijmoHeader { text = string.Empty };
        SeriesList = new List<PieChartData>();
    }
}

[Serializable]
public class WijmoHeader
{
    public string text { get; set; }
}


[Serializable]
public class BarChartData
{
    public string label { get; set; }
    public bool legendEntry { get; set; }
    public AxisData data { get; set; }

    public BarChartData()
    {
        label = string.Empty;        
        data = new AxisData();
    }
}

//[Serializable]
//public class Axis {
//    public object x { get; set; }
//    public object y { get; set; }
//    public Axis() {
//        //this.x = new Dictionary<string, object>();
//        //this.y = new Dictionary<string, object>();
//    }
//}


[Serializable]
public class AxisData
{
    public List<object> x { get; set; }
    public List<object> y { get; set; }
    public AxisData()
    {
        x = new List<object>();
        y = new List<object>();
    }
}

[Serializable]
public class PieChartData
{
    public string label { get; set; }
    public bool legendEntry { get; set; }
    public int offset { get; set; }
    public object data { get; set; }
}

public class TopEvents
{
    public int ID { get; set; }
    public DateTime Date { get; set; }
    public int TotalGuestCheckIn { get; set; }
    public int TotalGuestCheckOut { get; set; }
}


public class TopActivities
{
    public int ID { get; set; }
    public string ActivityType { get; set; }
    public string ActivityCurrency { get; set; }
    public double ActivityTotal { get; set; }
    public string ActivityStatus { get; set; }    
    public DateTime ActivityDate { get; set; }
    public int ActivityBy { get; set; }
    public string ActivityByName { get; set; }
}


public enum DashboardSettingKey
{
    Warehouses = 1
}

public class DashboardDataAPI
{
    //Cache Helper
    private static string MD5(string str)
    {
        MD5 md5 = new MD5CryptoServiceProvider();
        byte[] result = md5.ComputeHash(Encoding.ASCII.GetBytes(str));
        str = BitConverter.ToString(result);

        return str.Replace("-", "");
    }

    //Top Events
    #region Top Events Methods
    public static List<TopEvents> GetEvents()
    {
        string sqlCheckIn = "SELECT COUNT(oi.ordGuestID) FROM (orderitems oi INNER JOIN z_reservation_items ri ON (ri.ReservationItemID = oi.ordReservationItemID))";
        sqlCheckIn += " INNER JOIN z_reservation r ON r.ReservationID=ri.ReservationID";
        sqlCheckIn += " WHERE ri.CheckInDate = @Date AND ri.IsCanceled != 1 AND r.ReservationStatus=2";
        string sqlCheckOut = "SELECT COUNT(oi.ordGuestID) FROM (orderitems oi INNER JOIN z_reservation_items ri ON (ri.ReservationItemID = oi.ordReservationItemID))";
        sqlCheckOut += " INNER JOIN z_reservation r ON r.ReservationID=ri.ReservationID";
        sqlCheckOut += " WHERE ri.CheckOutDate = @Date AND ri.IsCanceled != 1 AND r.ReservationStatus=2";
        DbHelper dbHelp = new DbHelper(true);
        IDataReader r = null;
        List<TopEvents> lResult = new List<TopEvents>();
        try
        {
            DateTime dt = DateTime.Today;
            for (int i = 0; i < 20; i++)
            {
                TopEvents e = new TopEvents();
                e.ID = i + 1;
                e.Date = dt.AddDays(i);
                e.TotalGuestCheckIn = BusinessUtility.GetInt(dbHelp.GetValue(sqlCheckIn, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("Date", dt.AddDays(i), MyDbType.DateTime)
                }));
                e.TotalGuestCheckOut = BusinessUtility.GetInt(dbHelp.GetValue(sqlCheckOut, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("Date", dt.AddDays(i), MyDbType.DateTime)
                }));

                lResult.Add(e);
            }
            return lResult;
        }
        catch
        {

            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection(r);
        }
    }

    public static string GetguestListSql(ParameterCollection pCol, DateTime ondate, bool isCheckIn)
    {
        pCol.Clear();
        string sql = "SELECT oi.orderItemID, oi.orderItemDesc, p.PartnerLongName, ri.TotalNights";
        sql += " FROM (orderitems oi INNER JOIN partners p ON (oi.ordGuestID = p.PartnerID))";
        sql += " INNER JOIN z_reservation_items ri ON (ri.ReservationItemID = oi.ordReservationItemID)";
        sql += " INNER JOIN z_reservation r ON r.ReservationID=ri.ReservationID";
        sql += " WHERE ri.IsCanceled != 1 AND r.ReservationStatus=2";
        if (isCheckIn)
        {
            sql += " AND ri.CheckInDate = @Date";
        }
        else
        {
            sql += " AND ri.CheckOutDate = @Date";
        }
        pCol.Add("@Date", iTECH.Library.Utilities.BusinessUtility.GetDateTimeString(ondate, "yyyy-MM-dd HH:mm:ss"));
        return sql;
    } 
    #endregion

    //Top Activities
    #region Top Activities
    public static IOrderedEnumerable<TopActivities> GetActivities()
    {
        string sqlSO = "SELECT o.ordID, o.ordCreatedOn, o.ordCreatedBy, o.ordCustID, o.ordCurrencyCode, o.ordStatus, o.orderTypeCommission,";
        sqlSO += "funGetOrderTotal(o.ordID) AS OrderTotal, funGetUserName(o.ordCreatedBy) AS UserName";
        sqlSO += " FROM orders o ORDER BY o.ordCreatedOn DESC LIMIT 20";        
        DbHelper dbHelp = new DbHelper(true);
        List<TopActivities> lResult = new List<TopActivities>();
        try
        {
            using (IDataReader r = dbHelp.GetDataReader(sqlSO, CommandType.Text, new MySqlParameter[] { 
                DbUtility.GetParameter("OrdType", (int)OrderCommission.Sales, MyDbType.Int)
            }))
            {
                while (r.Read())
                {
                    TopActivities a = new TopActivities();
                    a.ActivityType = BusinessUtility.GetInt(r["orderTypeCommission"]) == (int)OrderCommission.Reservation ? "RSV" : "SALE";
                    a.ActivityCurrency = BusinessUtility.GetString(r["ordCurrencyCode"]);
                    a.ActivityDate = BusinessUtility.GetDateTime(r["ordCreatedOn"]);
                    a.ActivityStatus = BusinessUtility.GetString(r["ordStatus"]);
                    a.ID = BusinessUtility.GetInt(r["ordID"]);
                    a.ActivityBy = BusinessUtility.GetInt(r["ordCreatedBy"]);
                    a.ActivityTotal = BusinessUtility.GetDouble(r["OrderTotal"]);
                    a.ActivityByName = BusinessUtility.GetString(r["UserName"]);
                    lResult.Add(a);
                }
            }
            
            return from enumerable in lResult orderby enumerable.ActivityDate descending select enumerable;
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    } 
    #endregion

    //Chart Api functions Start Here
    #region Sales Chart Methods
    public static BarChartSettings GetSalesCompairDataByMonths(DbHelper dbHelp, string monthsCount, string whs, string salesBy)
    {
        string cacheKey = string.Format("DS_GetSalesCompairDataByMonths{0}{1}{2}", monthsCount, whs, salesBy);
        cacheKey = MD5(cacheKey);
        if (HttpContext.Current.Cache[cacheKey] != null)
        {
            return (BarChartSettings)HttpContext.Current.Cache[cacheKey]; 
        }

        //int startYear = by == "6m" ? DateTime.Today.AddMonths(-6).Year : DateTime.Today.AddMonths(-12).Year;
        //int endYear = by == "6m" ? DateTime.Today.AddMonths(-6).Year : DateTime.Today.AddMonths(-12).Year; 
        int lastMonthCount = 0;
        int.TryParse(monthsCount, out lastMonthCount);
        string sTitle = lastMonthCount == 6 ? "in Last 6 Months" : "in Last 12 Months";
        DateTime sDate = DateTime.Today.AddMonths(-1 * (lastMonthCount)).AddMonths(1);
        DateTime eDate = DateTime.Now;
        BarChartSettings settings = new BarChartSettings();
        settings.Header = new WijmoHeader { text = string.Format("Sales {0}", sTitle) };
        //IDataReader r = null;
        Dictionary<string, double> lData = new Dictionary<string, double>(); //Year-Month, amount
        try
        {
            string sqlTotal = "SELECT DISTINCT o.ordID AS OrderID, o.ordCreatedOn AS DateFlag, funGetOrderTotal(o.ordID) AS OrderTotal FROM orders o INNER JOIN orderitems oi ON oi.ordID=o.ordID INNER JOIN products p ON p.productID=oi.ordProductID WHERE o.ordStatus IN('I', 'P', 'S', 'Z', 'R') AND (o.ordCreatedOn BETWEEN @FDate AND @EDate)";
            string sqlPosSale = "SELECT SUM(p.posSubTotal) AS Amount, p.posTransDateTime AS DateFlag FROM postransaction p ";
            sqlPosSale += " INNER JOIN postransaccthdr pt ON pt.posAcctTransID=p.posTransId";
            sqlPosSale += " WHERE (p.posTransDateTime BETWEEN @FDate AND @EDate) AND (pt.posAcctPayType IN (1,2,3,4,5,6)) GROUP BY p.posTransId";
            //sqlTotal += " AND o.ordSaleWeb=1";

            int prdType = (int)StatusProductType.Default;
            //TotalSummary ts;
            DataTable dtResult;
            DataTable dtResult2;
            DateTime flagDate;
            switch (salesBy)
            {
                case "POS":
                    dtResult2 = dbHelp.GetDataTable(sqlPosSale, CommandType.Text, new MySqlParameter[] {                         
                        DbUtility.GetParameter("FDate", sDate, MyDbType.DateTime),
                        DbUtility.GetParameter("EDate", eDate, MyDbType.DateTime)});
                    foreach (DataRow r in dtResult2.Rows)
                    {
                        flagDate = BusinessUtility.GetDateTime(r["DateFlag"]);
                        if (lData.ContainsKey(string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)))
                        {
                            lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] += BusinessUtility.GetDouble(r["Amount"]);
                        }
                        else
                        {
                            lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] = BusinessUtility.GetDouble(r["Amount"]);
                        }
                    }
                    settings.SeriesList.Add(GetChartData(lastMonthCount, lData));
                    break;
                case "SALE":
                    prdType = (int)StatusProductType.Default;
                    sqlTotal += " AND p.prdType=@prdType AND o.ordSaleWeb=0";
                    dtResult = dbHelp.GetDataTable(sqlTotal, CommandType.Text, new MySqlParameter[] {                         
                        DbUtility.GetParameter("FDate", sDate, MyDbType.DateTime),
                        DbUtility.GetParameter("EDate", eDate, MyDbType.DateTime),
                        DbUtility.GetParameter("prdType", prdType, MyDbType.Int)
                    });
                    foreach (DataRow r in dtResult.Rows)
                    {
                        flagDate = BusinessUtility.GetDateTime(r["DateFlag"]);
                        //ts = CalculationHelper.GetOrderTotal(dbHelp, BusinessUtility.GetInt(r["OrderID"]));
                        if (lData.ContainsKey(string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)))
                        {
                            //lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] += ts.GrandTotal;
                            lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] += BusinessUtility.GetDouble(r["OrderTotal"]);
                        }
                        else
                        {
                            //lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] = ts.GrandTotal;
                            lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] = BusinessUtility.GetDouble(r["OrderTotal"]);
                        }
                    }
                    settings.SeriesList.Add(GetChartData(lastMonthCount, lData));
                    break;
                case "ONLINE":
                    prdType = (int)StatusProductType.Default;
                    sqlTotal += " AND p.prdType=@prdType AND o.ordSaleWeb=1";
                    dtResult = dbHelp.GetDataTable(sqlTotal, CommandType.Text, new MySqlParameter[] {                         
                        DbUtility.GetParameter("FDate", sDate, MyDbType.DateTime),
                        DbUtility.GetParameter("EDate", eDate, MyDbType.DateTime),
                        DbUtility.GetParameter("prdType", prdType, MyDbType.Int)
                    });
                    foreach (DataRow r in dtResult.Rows)
                    {
                        flagDate = BusinessUtility.GetDateTime(r["DateFlag"]);
                        //ts = CalculationHelper.GetOrderTotal(dbHelp, BusinessUtility.GetInt(r["OrderID"]));
                        if (lData.ContainsKey(string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)))
                        {
                            //lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] += ts.GrandTotal;
                            lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] += BusinessUtility.GetDouble(r["OrderTotal"]);
                        }
                        else
                        {
                            //lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] = ts.GrandTotal;
                            lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] = BusinessUtility.GetDouble(r["OrderTotal"]);
                        }
                    }
                    settings.SeriesList.Add(GetChartData(lastMonthCount, lData));
                    break;
                case "ATOTAL":
                    prdType = (int)StatusProductType.Accommodation;
                    sqlTotal += " AND p.prdType=@prdType";
                    dtResult = dbHelp.GetDataTable(sqlTotal, CommandType.Text, new MySqlParameter[] {                         
                        DbUtility.GetParameter("FDate", sDate, MyDbType.DateTime),
                        DbUtility.GetParameter("EDate", eDate, MyDbType.DateTime),
                        DbUtility.GetParameter("prdType", prdType, MyDbType.Int)
                    });
                    foreach (DataRow r in dtResult.Rows)
                    {
                        flagDate = BusinessUtility.GetDateTime(r["DateFlag"]);
                        //ts = CalculationHelper.GetOrderTotal(dbHelp, BusinessUtility.GetInt(r["OrderID"]));
                        if (lData.ContainsKey(string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)))
                        {
                            //lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] += ts.GrandTotal;
                            lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] += BusinessUtility.GetDouble(r["OrderTotal"]);
                        }
                        else
                        {
                            //lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] = ts.GrandTotal;
                            lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] = BusinessUtility.GetDouble(r["OrderTotal"]);
                        }
                    }
                    settings.SeriesList.Add(GetChartData(lastMonthCount, lData));
                    break;
                case "AONLINE":
                    prdType = (int)StatusProductType.Accommodation;
                    sqlTotal += " AND p.prdType=@prdType AND o.ordSaleWeb=1";
                    dtResult = dbHelp.GetDataTable(sqlTotal, CommandType.Text, new MySqlParameter[] {                         
                        DbUtility.GetParameter("FDate", sDate, MyDbType.DateTime),
                        DbUtility.GetParameter("EDate", eDate, MyDbType.DateTime),
                        DbUtility.GetParameter("prdType", prdType, MyDbType.Int)
                    });
                    foreach (DataRow r in dtResult.Rows)
                    {
                        flagDate = BusinessUtility.GetDateTime(r["DateFlag"]);
                        //ts = CalculationHelper.GetOrderTotal(dbHelp, BusinessUtility.GetInt(r["OrderID"]));
                        if (lData.ContainsKey(string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)))
                        {
                            //lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] += ts.GrandTotal;
                            lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] += BusinessUtility.GetDouble(r["OrderTotal"]);
                        }
                        else
                        {
                            //lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] = ts.GrandTotal;
                            lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] = BusinessUtility.GetDouble(r["OrderTotal"]);
                        }
                    }
                    settings.SeriesList.Add(GetChartData(lastMonthCount, lData));
                    break;
                default:
                    dtResult = dbHelp.GetDataTable(sqlTotal, CommandType.Text, new MySqlParameter[] {                         
                        DbUtility.GetParameter("FDate", sDate, MyDbType.DateTime),
                        DbUtility.GetParameter("EDate", eDate, MyDbType.DateTime)});
                    foreach (DataRow r in dtResult.Rows)
                    {
                        flagDate = BusinessUtility.GetDateTime(r["DateFlag"]);
                        //ts = CalculationHelper.GetOrderTotal(dbHelp, BusinessUtility.GetInt(r["OrderID"]));
                        if (lData.ContainsKey(string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)))
                        {
                            //lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] += ts.GrandTotal;
                            lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] += BusinessUtility.GetDouble(r["OrderTotal"]);
                        }
                        else
                        {
                            //lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] = ts.GrandTotal;
                            lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] = BusinessUtility.GetDouble(r["OrderTotal"]);
                        }
                    }
                    dtResult2 = dbHelp.GetDataTable(sqlPosSale, CommandType.Text, new MySqlParameter[] {                         
                        DbUtility.GetParameter("FDate", sDate, MyDbType.DateTime),
                        DbUtility.GetParameter("EDate", eDate, MyDbType.DateTime)});
                    foreach (DataRow r in dtResult2.Rows)
                    {
                        flagDate = BusinessUtility.GetDateTime(r["DateFlag"]);
                        if (lData.ContainsKey(string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)))
                        {
                            lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] += BusinessUtility.GetDouble(r["Amount"]);
                        }
                        else
                        {
                            lData[string.Format("{0:0000}-{1:00}", flagDate.Year, flagDate.Month)] = BusinessUtility.GetDouble(r["Amount"]);
                        }
                    }
                    settings.SeriesList.Add(GetChartData(lastMonthCount, lData));
                    break;
            }

            HttpContext.Current.Cache.Insert(cacheKey, settings, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromHours(24));
            return settings;
        }
        catch
        {
            throw;
        }
    }

    private static BarChartData GetChartData(int lastMonthsCount, Dictionary<string, double> data)
    {
        BarChartData it = new BarChartData();
        it.legendEntry = false;
        it.label = string.Empty;
        double totalSaleInMonth = 0.0D;

        DateTime sDate = DateTime.Today.AddMonths(-1 * (lastMonthsCount)).AddMonths(1);
        DateTime itDate = sDate;

        for (int i = 0; i < lastMonthsCount; i++)
        {
            totalSaleInMonth = 0.0D;
            itDate = sDate.AddMonths(i);
            it.data.x.Add(itDate.ToString("MMM-yy"));

            if (data.ContainsKey(string.Format("{0:0000}-{1:00}", itDate.Year, itDate.Month)))
            {
                totalSaleInMonth = data[string.Format("{0:0000}-{1:00}", itDate.Year, itDate.Month)];
            }

            it.data.y.Add(totalSaleInMonth);
        }

        return it;
    }

    private static List<BarChartData> GetChartData(int startYear, int endYear, Dictionary<string, double> data)
    {
        List<BarChartData> lResult = new List<BarChartData>();
        for (int i = startYear; i <= endYear; i++)
        {
            BarChartData it = new BarChartData();
            it.legendEntry = true;
            it.label = i.ToString();

            DateTime dt = new DateTime(i, 1, 1);
            double totalSaleInMonth = 0.0D;
            DateTime itDate = dt;
            for (int j = 0; j < 12; j++)
            {
                totalSaleInMonth = 0.0D;
                itDate = dt.AddMonths(j);
                it.data.x.Add(itDate.ToString("MMM"));

                if (data.ContainsKey(string.Format("{0:0000}-{1:00}", itDate.Year, itDate.Month)))
                {
                    totalSaleInMonth = data[string.Format("{0:0000}-{1:00}", itDate.Year, itDate.Month)];
                }

                it.data.y.Add(totalSaleInMonth);
            }
            lResult.Add(it);
        }
        return lResult;
    } 
    #endregion

    //Top Customer's data
    public static BarChartSettings GetTopCustomerData(DbHelper dbHelp, string yearRange, string whs)
    {
        string cacheKey = string.Format("DS_GetTopCustomerData{0}{1}", yearRange, whs);
        cacheKey = MD5(cacheKey);
        if (HttpContext.Current.Cache[cacheKey] != null)
        {
            return (BarChartSettings)HttpContext.Current.Cache[cacheKey];
        }

        int latMonthCount = BusinessUtility.GetInt(yearRange);
        BarChartSettings settings = new BarChartSettings();
        settings.Header = new WijmoHeader { text = "Top Customers" };        
        IDataReader r = null;
        try
        {
            string sql = string.Empty;
            sql = "SELECT SUM(CASE ii.invProductDiscountType ";
            sql += " WHEN 'A' THEN (ii.invProductQty * ii.invProductUnitPrice * i.invCurrencyExRate) - ii.invProductDiscount ELSE  ";
            sql += " CASE ii.invProductDiscount ";
            sql += " WHEN 0 THEN (ii.invProductQty * ii.invProductUnitPrice * i.invCurrencyExRate) ";
            sql += " ELSE (ii.invProductQty * ii.invProductUnitPrice * i.invCurrencyExRate) - ((ii.invProductQty * ii.invProductUnitPrice * i.invCurrencyExRate) * ii.invProductDiscount/100) END ";
            sql += " END)  AS Amount, c.PartnerLongName ";
            sql += " FROM invoiceitems ii INNER JOIN invoices i ON i.invID=ii.invoices_invID";
            sql += " INNER JOIN products p ON p.ProductID=ii.invProductID INNER JOIN orders o ON o.ordID=i.invForOrderNo";
            sql += "  INNER JOIN partners c ON c.PartnerID=i.invCustID";
            sql += " WHERE";
            sql += " (i.invCreatedOn BETWEEN @FDate AND @EDate)";
            if (!string.IsNullOrEmpty(whs))
            {
                sql += " AND i.invShpWhsCode=@Whs";
            }
            sql += " AND c.PartnerActive=1";
            sql += " GROUP BY i.invCustID ORDER BY 1 DESC LIMIT 10;";
            //string sqlSalesByMonth = "SELECT SUM(a.ArAmtRcvd) AS Amount, p.PartnerLongName  FROM accountreceivable a INNER JOIN invoices i ON i.invID = a.ARInvoiceNo";
            //sqlSalesByMonth += " INNER JOIN partners p ON p.PartnerID=i.invCustID";
            //sqlSalesByMonth += " WHERE (a.ARAmtRcvdDateTime BETWEEN @FDate AND @EDate) ";
            //if (!string.IsNullOrEmpty(whs))
            //{
            //    sqlSalesByMonth += " AND i.invShpWhsCode=@Whs";
            //}
            //sqlSalesByMonth += " GROUP BY i.invCustID ORDER BY 1 DESC LIMIT 10;";

            BarChartData it = new BarChartData();
            it.legendEntry = false;
            it.label = "";

            DateTime dtStart = DateTime.Today.AddMonths(-1 * latMonthCount);
            DateTime dtEnd = DateTime.Now;
            r = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("Whs", whs, MyDbType.String),
                        DbUtility.GetParameter("FDate", dtStart, MyDbType.DateTime),
                        DbUtility.GetParameter("EDate", dtEnd, MyDbType.DateTime)
                    });
            string pName = string.Empty;
            while (r.Read())
            {
                pName = BusinessUtility.GetString(r["PartnerLongName"]);
                it.data.x.Add(pName.Length > 8 ? pName.Substring(0, 8) : pName);
                it.data.y.Add(BusinessUtility.GetDouble(r["Amount"]));
            }
            if (r != null && !r.IsClosed)
            {
                r.Close();
                r.Dispose();
            }
            settings.SeriesList.Add(it);

            foreach (var item in settings.SeriesList)
            {
                if (item.data.x.Count <= 0)
                {
                    item.data.x.Add("Data not found");
                    item.data.y.Add(0.0D);
                }
            }
            HttpContext.Current.Cache.Insert(cacheKey, settings, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromHours(24));
            return settings;
        }
        catch
        {
            throw;
        }
        finally
        {
            if (r != null && !r.IsClosed)
            {
                r.Close();
                r.Dispose();
            }
        }
    }

    //Top Products Data
    public static BarChartSettings GetTopProductsData(DbHelper dbHelp, string yearRange, string by, string whs, string salesBy)
    {
        string cacheKey = string.Format("DS_GetTopProductsData{0}{1}{2}{3}", yearRange, whs, by, salesBy);
        cacheKey = MD5(cacheKey);
        if (HttpContext.Current.Cache[cacheKey] != null)
        {
            return (BarChartSettings)HttpContext.Current.Cache[cacheKey];
        }

        int latMonthCount = BusinessUtility.GetInt(yearRange);
        BarChartSettings chartSetttings = new BarChartSettings();        
        IDataReader r = null;
        try
        {
            string sql = string.Empty;
            sql = "SELECT SUM(ii.ordProductQty) AS Qty, ";
            sql += " SUM(CASE ii.ordProductDiscountType ";
            sql += " WHEN 'A' THEN (ii.ordProductQty * ii.ordProductUnitPrice * i.ordCurrencyExRate) - ii.ordProductDiscount ELSE  ";
            sql += " CASE ii.ordProductDiscount ";
            sql += " WHEN 0 THEN (ii.ordProductQty * ii.ordProductUnitPrice * i.ordCurrencyExRate) ";
            sql += " ELSE (ii.ordProductQty * ii.ordProductUnitPrice * i.ordCurrencyExRate) - ((ii.ordProductQty * ii.ordProductUnitPrice * i.ordCurrencyExRate) * ii.ordProductDiscount/100) END ";
            sql += " END)  AS Amount,p.prdUPCCode AS prdName";
            //sql += " CASE IFNULL(b.RoomType, 0) WHEN 1 THEN CONCAT('Private-',p.prdName)  WHEN 2 THEN CONCAT('Shared-',p.prdName) ELSE p.prdName END AS prdName";
            sql += " FROM orderitems ii INNER JOIN orders i ON i.ordID=ii.ordID";
            sql += " INNER JOIN products p ON p.ProductID=ii.ordProductID";
            sql += "  INNER JOIN partners c ON c.PartnerID=i.ordCustID";
            sql += " LEFT OUTER JOIN vw_beds b ON b.BedID = p.ProductID";
            sql += " WHERE p.prdType=@prdType AND ii.ordProductUnitPrice > 0";
            sql += " AND (i.ordCreatedOn BETWEEN @FDate AND @EDate)";
            if (!string.IsNullOrEmpty(whs))
            {
                sql += " AND i.ordShpWhsCode=@Whs";
            }
            sql += " AND c.PartnerActive=1";
            sql += " AND i.ordStatus IN('I', 'P', 'S', 'Z', 'R')";
            if (by == "qty")
            {
                chartSetttings.Header = new WijmoHeader { text = "Top Products By Unit Sold" };
            }
            else
            {
                chartSetttings.Header = new WijmoHeader { text = "Top Products By Sales($)" };
            }
            int prdType = (int)StatusProductType.Default;
            switch (salesBy)
            {
                case "PTOTAL":
                    prdType = (int)StatusProductType.Default;
                    goto default;
                case "ATOTAL":
                    prdType = (int)StatusProductType.Accommodation;
                    goto default;
                case "AONLINE":
                    prdType = (int)StatusProductType.Accommodation;
                    sql += " AND i.ordSaleWeb=1";
                    goto default;
                case "PONLINE":
                    prdType = (int)StatusProductType.Default;
                    sql += " AND i.ordSaleWeb=1";
                    goto default;
                default:
                    sql += " GROUP BY ii.ordProductID ORDER BY 1 DESC LIMIT 10;";
                    break;
            }
            var xAxis = new { 
                text = string.Empty, 
                labels = new { 
                    style = new { 
                        rotation = -60 
                    },
                    textAlign = "near"
                } 
            };
            var yAxis = new { text = string.Empty };            
            chartSetttings.Axis = new { x = xAxis, y = yAxis };
            chartSetttings.ChartLabelFormatString = string.Empty;
            if (salesBy == "AVGGP")
            {
                //sql = "SELECT slsProductName AS prdName, SUM(slsTotalUnits) AS Qty, SUM(slsTotalUnits * (slsUnitPrice - slsUnitCostPriceAvg)) AS Amount  ";
                //sql = "SELECT slsPrdUPCCode AS prdName, SUM(slsTotalUnits) AS Qty, SUM(slsUnitCostPriceAvg) * 100/SUM(slsTotalPrice) AS Amount  ";
                sql = "SELECT t1.prdName, t1.Qty, (t1.Amount/t1.RCount) AS Amount FROM(SELECT an.slsPrdUPCCode AS prdName,";
                sql += " SUM(an.slsTotalUnits) AS Qty,SUM((an.slsTotalUnits * (an.slsUnitPrice - an.slsUnitCostPriceAvg)) / (an.slsTotalPrice)) AS Amount,";
                sql += " COUNT(an.slsProductID) AS RCount";
                sql += "  FROM z_sls_analysis an WHERE an.slsRecordDateTime BETWEEN @FDate AND @EDate GROUP BY an.slsProductID) AS t1 LIMIT 8";

                chartSetttings.ChartLabelFormatString = "p2";
                yAxis = new { text = "(%)" };
                chartSetttings.Axis = new { x = xAxis, y = yAxis };         
                chartSetttings.Header = new WijmoHeader { text = "Top Products By AVG GP" };
            }

            BarChartData it = new BarChartData();
            it.legendEntry = false;
            it.label = "";

            DateTime dtStart = DateTime.Today.AddMonths(-1 * latMonthCount);
            DateTime dtEnd = DateTime.Now;
            r = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("Whs", whs, MyDbType.String),
                        DbUtility.GetParameter("prdType", prdType, MyDbType.Int),
                        DbUtility.GetParameter("FDate", dtStart, MyDbType.DateTime),
                        DbUtility.GetParameter("EDate", dtEnd, MyDbType.DateTime)
                    });
            string xL = string.Empty;
            while (r.Read())
            {
                xL = BusinessUtility.GetString(r["prdName"]);
                it.data.x.Add(xL.Length > 12 ? xL.Substring(0, 12) : xL);
                if (by == "qty")
                {
                    it.data.y.Add(BusinessUtility.GetDouble(r["Qty"]));
                }
                else
                {
                    it.data.y.Add(BusinessUtility.GetDouble(r["Amount"]));
                }
            }
            if (r != null && !r.IsClosed)
            {
                r.Close();
                r.Dispose();
            }
            chartSetttings.SeriesList.Add(it);

            foreach (var item in chartSetttings.SeriesList)
            {
                if (item.data.x.Count <= 0)
                {
                    item.data.x.Add("Data not found");
                    item.data.y.Add(0.0D);
                }
            }
            HttpContext.Current.Cache.Insert(cacheKey, chartSetttings, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromHours(24));
            return chartSetttings;
        }
        catch
        {
            throw;
        }
        finally
        {
            if (r != null && !r.IsClosed)
            {
                r.Close();
                r.Dispose();
            }
        }
    }

    //Account Rcv
    public static PieChartSettings GetAccountRcvPieData(DbHelper dbHelp, string whs)
    {
        string cacheKey = string.Format("DS_GetAccountRcvPieData{0}", whs);
        cacheKey = MD5(cacheKey);
        if (HttpContext.Current.Cache[cacheKey] != null)
        {
            return (PieChartSettings)HttpContext.Current.Cache[cacheKey];
        }

        PieChartSettings settings = new PieChartSettings();
        settings.Header = new WijmoHeader { text = "Account Receivable" };
        IDataReader r = null;
        try
        {
            string sqlSalesByMonth = "SELECT a.ArAmtRcvd AS Amount FROM accountreceivable a INNER JOIN invoices i ON i.invID = a.ARInvoiceNo ";
            //sqlSalesByMonth += " WHERE (a.ArAmtRcvd BETWEEN @AmtFrom AND @AmtTo) ";
            if (!string.IsNullOrEmpty(whs))
            {
                sqlSalesByMonth += " AND i.invShpWhsCode=@Whs";
            }
            sqlSalesByMonth += " ORDER BY a.ArAmtRcvd";


            r = dbHelp.GetDataReader(sqlSalesByMonth, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("Whs", whs, MyDbType.String)
                    });
            Dictionary<string, double> dicData = new Dictionary<string, double>();
            dicData["1-30"] = 0;
            dicData["31-60"] = 0;
            dicData["61-90"] = 0;
            dicData["> 90"] = 0;
            dicData["Disputed"] = 0;

            double totalRecords = 0;
            while (r.Read())
            {
                totalRecords++;
                double amt = BusinessUtility.GetDouble(r["Amount"]);

                if (amt >= 1 && amt <= 30)
                {
                    dicData["1-30"]++;
                }
                else if (amt > 30 && amt <= 60)
                {
                    dicData["31-60"]++;
                }
                else if (amt > 60 && amt <= 90)
                {
                    dicData["61-90"]++;
                }
                else if (amt > 90)
                {
                    dicData["> 90"]++;
                }
                else
                {
                    //dicData["Disputed"]++;
                }
            }
            if (r != null && !r.IsClosed)
            {
                r.Close();
                r.Dispose();
            }

            if (totalRecords > 0)
            {
                foreach (var item in dicData.Keys)
                {
                    PieChartData it = new PieChartData();
                    it.legendEntry = true;
                    it.label = item;
                    it.data = Math.Round(dicData[item] / totalRecords, 2);
                    settings.SeriesList.Add(it);
                }
            }

            if (settings.SeriesList.Count <= 0)
            {
                PieChartData it = new PieChartData();
                it.legendEntry = true;
                it.label = "Data not found!";
                it.data = 1.0D;
                settings.SeriesList.Add(it);
            }
            HttpContext.Current.Cache.Insert(cacheKey, settings, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromHours(24));
            return settings;
        }
        //catch {
        //    throw;
        //}
        finally
        {
            if (r != null && !r.IsClosed)
            {
                r.Close();
                r.Dispose();
            }
        }
    }
}
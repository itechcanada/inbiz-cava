﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;

using iTECH.PaymentProvider;

/// <summary>
/// Summary description for SivanandaNotifications
/// </summary>
public class Notifications
{
    #region Shivananda Notifications
    public static bool SentEmailToGuest(int orderID, string transactionID)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            Orders ord = new Orders();
            ord.PopulateObject(dbHelp, orderID);

            TotalSummary ts = CalculationHelper.GetOrderTotal(dbHelp, orderID);

            string webWhs = ConfigurationManager.AppSettings["WebSaleWhsCode"];
            SysWarehouses whs = new SysWarehouses();
            whs.PopulateObject(webWhs, dbHelp);

            SysCompanyInfo ci = new SysCompanyInfo();
            ci.PopulateObject(whs.WarehouseCompanyID, dbHelp);

            Partners part = new Partners();
            part.PopulateObject(dbHelp, ord.OrdCustID);

            Reservation rsv = new Reservation();
            ReservationItems ri = rsv.GetFirstReservationItem(dbHelp, ord.OrdID);

            Bed b = new Bed();
            b.BedID = ri.BedID;
            b.PopulateBedInfo(dbHelp, b, Globals.CurrentAppLanguageCode);

            string sub = string.Format("Reservation Confirmation {0} ({1}, {2})", ci.CompanyName, ci.CompanyCity, ci.CompanyState);
            string msg = File.ReadAllText(HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplates/ToGuest_{0}.htm", Globals.CurrentAppLanguageCode.ToLower())));

            string sqlGuest = "SELECT v.BedTitle, p.FirstName, p.LastName, p.SpirtualName, ri.PricePerDay FROM z_reservation_items ri INNER JOIN vw_beds v ON v.BedID=ri.BedID INNER JOIN z_customer_extended p ON p.PartnerID=ri.GuestID WHERE ri.ReservationID = @ReservationID";
            string bedGuestList = string.Empty;
            int totalBeds = 0;
            double toCalculatePricePerDay = 0.0D;
            double totalPriceCalculated = 0.0D;
            string spName = string.Empty;
            try
            {
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlGuest, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ReservationID", ri.ReservationID, MyDbType.Int)
                    }))
                {
                    int iCount = 0;
                    while (dr.Read())
                    {
                        iCount++;
                        bedGuestList += string.Format("{3}-{0} : {1} {2}<br>", iCount, dr["LastName"], dr["FirstName"], Resources.Resource.lblBedCaps);
                        totalPriceCalculated += BusinessUtility.GetDouble(dr["PricePerDay"]);
                        spName = BusinessUtility.GetString(dr["SpirtualName"]);
                        totalBeds++;
                    }
                }
            }
            finally
            {

            }

            toCalculatePricePerDay = (totalPriceCalculated) / (double)(totalBeds == 0 ? 1 : totalBeds);

            //OrderItemProcess oip = new OrderItemProcess();
            //string services = string.Empty;
            //try
            //{
            //    using (IDataReader dr = oip.GetOrderProcessItemsReader(dbHelp, ord.OrdID))
            //    {                    
            //        while (dr.Read())
            //        {                 
            //            services += string.Format("<tr><td>{0}</td><td>{1} {2:F}</td></tr>", dr["ProcessDescription"], ts.CurrencyCode, dr["ProcessCost"]);
            //        }
            //    }
            //}
            //finally
            //{

            //}
            string services = string.Empty;
            SysProcessGroup serviceProducts = new SysProcessGroup();
            double serviceSubTotal = 0.0D;
            try
            {
                using (IDataReader dr = serviceProducts.GetReserverdServicesForWeb(dbHelp, orderID, ts.CurrencyCode))
                {
                    while (dr.Read())
                    {
                        double subTotal = BusinessUtility.GetDouble(dr["ordProductUnitPrice"]) * BusinessUtility.GetDouble(dr["ordProductQty"]);
                        services += string.Format("<tr><td>{0}</td><td>{1} <b>x</b> {2:F} ={3} {4:F}</td></tr>", dr["prdName"], dr["ordProductQty"], dr["ordProductUnitPrice"], ts.CurrencyCode, subTotal);
                        serviceSubTotal += subTotal;
                    }
                    dr.Close();
                    dr.Dispose();
                }
            }
            finally
            {
            }

            services += string.Format("<tr><td>{0}</td><td>{1} {2:F}</td></tr>", "Service Sub Total", ts.CurrencyCode, serviceSubTotal);

            msg = msg.Replace("#GUEST_NAME#", string.Format("{0} {1}", part.ExtendedProperties.FirstName, part.ExtendedProperties.LastName))
                .Replace("#COMPANY_NAME#", ci.CompanyName)
                .Replace("#COMPANY_CITY#", ci.CompanyCity)
                .Replace("#COMPANY_STATE#", ci.CompanyState)
                .Replace("#CONFIRMATION_NO#", string.Format("{0}{1}", part.ExtendedProperties.LastName, ord.OrdID))
                .Replace("#CHECKIN_DATE#", ri.CheckInDate.ToString("dddd MMMM dd, yyyy"))
                .Replace("#CHECKOUT_DATE#", ri.CheckOutDate.ToString("dddd MMMM dd, yyyy"))
                .Replace("#TOTAL_BEDS#", totalBeds.ToString())
                .Replace("#ROOM_TYPE#", UIHelper.GetRoomType(b.RoomType))
                .Replace("#BED_GUEST_LIST#", bedGuestList)
                .Replace("#BEDCOST_PER_NIGHT#", string.Format("{0} {1:F}", ts.CurrencyCode, toCalculatePricePerDay))
                .Replace("#NO_OF_NIGHTS#", ri.TotalNights.ToString())
                .Replace("#BED_SUB_TOTAL#", string.Format("{0} {1:F}", ts.CurrencyCode, totalPriceCalculated))
                .Replace("#SERVICES#", services)
                .Replace("#REMAINING_BALANCE#", string.Format("{0} {1:F}", ts.CurrencyCode, ts.OutstandingAmount))
                .Replace("#GRAND_TOTAL#", string.Format("{0} {1:F}", ts.CurrencyCode, ts.GrandTotal))
                .Replace("#TRANSACTION_ID#", transactionID)
                .Replace("#SPIRITUAL_NAME#", !string.IsNullOrEmpty(spName) ? string.Format("({0})", spName) : string.Empty);

            if (totalBeds > 1)
            {
                msg = msg.Replace("#BED_RESOURCE#", Resources.Resource.lblBedPlural);
            }
            else
            {
                msg = msg.Replace("#BED_RESOURCE#", Resources.Resource.lblBedSingular);
            }

            //Set Reservation Detailsnipt
            EmailNotifications sniptRsv = new EmailNotifications();
            sniptRsv.EmailBody = GetReservationDetailSnipt(dbHelp, orderID);
            sniptRsv.NotificationSourcePk = orderID;
            sniptRsv.NotificationTypeRef = NotificationType.ReservationDetailsGuestSnipt;            
            sniptRsv.Insert(dbHelp);

            //Set notification into database
            EmailNotifications notification = new EmailNotifications();
            notification.EmailBody = msg;
            notification.EmailSubject = sub;
            notification.NotificationSourcePk = orderID;
            notification.NotificationTypeRef = NotificationType.ReservationOrderSuccessToGuest;
            notification.ToEmailAddresses = part.PartnerEmail;
            notification.Insert(dbHelp);
            //EmailHelper.SendEmail(ConfigurationManager.AppSettings["NoReplyEmail"], part.PartnerEmail, msg, sub, true);
            EmailHelper.SendAppointment(ConfigurationManager.AppSettings["NoReplyEmail"], part.PartnerEmail, msg, sub, ci.CompanyCity, ri.CheckInDate.AddHours(15).ToUniversalTime(), ri.CheckOutDate.AddHours(11).ToUniversalTime());
            return true;
        }
        catch
        {
            return false;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    public static bool SentEmailToAshram(int orderID, string transactionID)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            Orders ord = new Orders();
            ord.PopulateObject(dbHelp, orderID);

            TotalSummary ts = CalculationHelper.GetOrderTotal(dbHelp, orderID);

            string webWhs = ConfigurationManager.AppSettings["WebSaleWhsCode"];
            SysWarehouses whs = new SysWarehouses();
            whs.PopulateObject(webWhs, dbHelp);

            SysCompanyInfo ci = new SysCompanyInfo();
            ci.PopulateObject(whs.WarehouseCompanyID, dbHelp);

            Partners part = new Partners();
            part.PopulateObject(dbHelp, ord.OrdCustID);

            Reservation rsv = new Reservation();
            ReservationItems ri = rsv.GetFirstReservationItem(dbHelp, ord.OrdID);

            Bed b = new Bed();
            b.BedID = ri.BedID;
            b.PopulateBedInfo(dbHelp, b, Globals.CurrentAppLanguageCode);

            string sub = string.Format("A New Reservation – {0} {1} #SPIRITUAL_NAME#", part.ExtendedProperties.FirstName, part.ExtendedProperties.LastName);
            string msg = File.ReadAllText(HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplates/ToAshram_{0}.htm", Globals.CurrentAppLanguageCode.ToLower())));

            string sqlGuest = "SELECT v.BedTitle, p.FirstName, p.LastName, p.SpirtualName, ri.IsCouple, ri.Sex, ri.PricePerDay FROM z_reservation_items ri INNER JOIN vw_beds v ON v.BedID=ri.BedID INNER JOIN z_customer_extended p ON p.PartnerID=ri.GuestID WHERE ri.ReservationID = @ReservationID ORDER BY ri.ReservationItemID";
            string bedGuestList = string.Empty;
            List<string> lstCouple1 = new List<string>();
            List<string> lstCouple2 = new List<string>();
            int totalBeds = 0;
            double toCalculatePricePerDay = 0.0D;
            double totalPriceCalculated = 0.0D;
            string spName = string.Empty;
            try
            {
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlGuest, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ReservationID", ri.ReservationID, MyDbType.Int)
                    }))
                {
                    int iCount = 0;
                    while (dr.Read())
                    {
                        iCount++;
                        if (BusinessUtility.GetBool(dr["IsCouple"]))
                        {
                            if (lstCouple1.Count < 2)
                            {
                                lstCouple1.Add(string.Format("{4}-{0} ({1}): {2} {3} - {5}<br>", iCount, dr["BedTitle"], dr["LastName"], dr["FirstName"], Resources.Resource.lblBedCaps, BusinessUtility.GetString(dr["Sex"]) == "M" ? Resources.Resource.lblMale : Resources.Resource.lblFemale));
                            }
                            else if (lstCouple2.Count < 2)
                            {
                                lstCouple2.Add(string.Format("{4}-{0} ({1}): {2} {3} - {5}<br>", iCount, dr["BedTitle"], dr["LastName"], dr["FirstName"], Resources.Resource.lblBedCaps, BusinessUtility.GetString(dr["Sex"]) == "M" ? Resources.Resource.lblMale : Resources.Resource.lblFemale));
                            }
                        }
                        else
                        {
                            bedGuestList += string.Format("{4}-{0} ({1}): {2} {3} - {5}<br>", iCount, dr["BedTitle"], dr["LastName"], dr["FirstName"], Resources.Resource.lblBedCaps, BusinessUtility.GetString(dr["Sex"]) == "M" ? Resources.Resource.lblMale : Resources.Resource.lblFemale);
                        }
                        totalPriceCalculated += BusinessUtility.GetDouble(dr["PricePerDay"]);
                        spName = BusinessUtility.GetString(dr["SpirtualName"]);
                        totalBeds++;
                    }
                }
                if (lstCouple1.Count > 0 && lstCouple2.Count > 0)
                {
                    string coupleInfo = string.Format(@"<span style=""text-decoration: underline;font-weight:bold"">{0}</span><br>", Resources.Resource.lblThereAre2Couples);
                    coupleInfo += string.Format(@"<span style=""text-decoration: underline;font-weight:bold"">{0} 1</span><br>", Resources.Resource.lblCouple);
                    foreach (var item in lstCouple1)
                    {
                        coupleInfo += item;
                    }
                    coupleInfo += "<br>";

                    coupleInfo += string.Format(@"<span style=""text-decoration: underline;font-weight:bold"">{0} 2</span><br>", Resources.Resource.lblCouple);
                    foreach (var item in lstCouple2)
                    {
                        coupleInfo += item;
                    }
                    coupleInfo += "<br>";

                    bedGuestList = coupleInfo + bedGuestList;
                }
                else if (lstCouple1.Count > 0)
                {
                    string coupleInfo = string.Format(@"<span style=""text-decoration: underline;font-weight:bold"">{0}</span><br>", Resources.Resource.lblThereAre1Couple);
                    coupleInfo += string.Format(@"<span style=""text-decoration: underline;font-weight:bold"">{0} 1</span><br>", Resources.Resource.lblCouple);
                    foreach (var item in lstCouple1)
                    {
                        coupleInfo += item;
                    }
                    coupleInfo += "<br>";

                    bedGuestList = coupleInfo + bedGuestList;
                }
            }
            finally
            {

            }

            toCalculatePricePerDay = (totalPriceCalculated) / (double)(totalBeds == 0 ? 1 : totalBeds);

            //OrderItemProcess oip = new OrderItemProcess();
            //string services = string.Empty;
            //try
            //{
            //    using (IDataReader dr = oip.GetOrderProcessItemsReader(dbHelp, ord.OrdID))
            //    {
            //        while (dr.Read())
            //        {
            //            services += string.Format("<tr><td>{0}</td><td>{1} {2:F}</td></tr>", dr["ProcessDescription"], ts.CurrencyCode, dr["ProcessCost"]);
            //        }
            //    }
            //}
            //finally
            //{

            //}
            string services = string.Empty;
            SysProcessGroup serviceProducts = new SysProcessGroup();
            double serviceSubTotal = 0.0D;
            try
            {
                using (IDataReader dr = serviceProducts.GetReserverdServicesForWeb(dbHelp, orderID, ts.CurrencyCode))
                {
                    while (dr.Read())
                    {
                        double subTotal = BusinessUtility.GetDouble(dr["ordProductUnitPrice"]) * BusinessUtility.GetDouble(dr["ordProductQty"]);
                        services += string.Format("<tr><td>{0}</td><td>{1} <b>x</b> {2:F} ={3} {4:F}</td></tr>", dr["prdName"], dr["ordProductQty"], dr["ordProductUnitPrice"], ts.CurrencyCode, subTotal);
                        serviceSubTotal += subTotal;
                    }
                    dr.Close();
                    dr.Dispose();
                }
            }
            finally
            {
            }
            services += string.Format("<tr><td>{0}</td><td>{1} {2:F}</td></tr>", "Service Sub Total", ts.CurrencyCode, serviceSubTotal);

            msg = msg.Replace("#GUEST_NAME#", string.Format("{0} {1}", part.ExtendedProperties.FirstName, part.ExtendedProperties.LastName))
                .Replace("#COMPANY_NAME#", ci.CompanyName)
                .Replace("#COMPANY_CITY#", ci.CompanyCity)
                .Replace("#COMPANY_STATE#", ci.CompanyState)
                .Replace("#CONFIRMATION_NO#", string.Format("{0}{1}", part.ExtendedProperties.LastName, ord.OrdID))
                .Replace("#CHECKIN_DATE#", ri.CheckInDate.ToString("dddd MMMM dd, yyyy"))
                .Replace("#CHECKOUT_DATE#", ri.CheckOutDate.ToString("dddd MMMM dd, yyyy"))
                .Replace("#TOTAL_BEDS#", totalBeds.ToString())
                .Replace("#ROOM_TYPE#", UIHelper.GetRoomType(b.RoomType))
                .Replace("#BED_GUEST_LIST#", bedGuestList)
                .Replace("#BEDCOST_PER_NIGHT#", string.Format("{0} {1:F}", ts.CurrencyCode, toCalculatePricePerDay))
                .Replace("#NO_OF_NIGHTS#", ri.TotalNights.ToString())
                .Replace("#BED_SUB_TOTAL#", string.Format("{0} {1:F}", ts.CurrencyCode, totalPriceCalculated))
                .Replace("#SERVICES#", services)
                .Replace("#REMAINING_BALANCE#", string.Format("{0} {1:F}", ts.CurrencyCode, ts.OutstandingAmount))
                .Replace("#GUEST_EMAIL#", part.PartnerEmail)
                .Replace("#RESERVATION_NOTES#", HttpContext.Current.Session["CURRENT_GUEST_RESERVATION_NOTES"] != null ? (string)HttpContext.Current.Session["CURRENT_GUEST_RESERVATION_NOTES"] : string.Empty)
                .Replace("#CUSTOMER_NOTES#", part.PartnerNote)
                .Replace("#GRAND_TOTAL#", string.Format("{0} {1:F}", ts.CurrencyCode, ts.GrandTotal))
                .Replace("#TRANSACTION_ID#", transactionID)
                .Replace("#SPIRITUAL_NAME#", !string.IsNullOrEmpty(spName) ? string.Format("({0})", spName) : string.Empty);

            if (totalBeds > 1)
            {
                msg = msg.Replace("#BED_RESOURCE#", Resources.Resource.lblBedPlural);
            }
            else
            {
                msg = msg.Replace("#BED_RESOURCE#", Resources.Resource.lblBedSingular);
            }
            sub = sub.Replace("#SPIRITUAL_NAME#", !string.IsNullOrEmpty(spName) ? string.Format("({0})", spName) : string.Empty);


            //Set notification into database
            EmailNotifications notification = new EmailNotifications();
            notification.EmailBody = msg;
            notification.EmailSubject = sub;
            notification.NotificationSourcePk = orderID;
            notification.NotificationTypeRef = NotificationType.ReservationOrderSuccessToAshram;
            notification.ToEmailAddresses = part.PartnerEmail;
            notification.Insert(dbHelp);

            //EmailHelper.SendEmail(ConfigurationManager.AppSettings["NoReplyEmail"], ci.CompanyEmail, msg, sub, true);
            EmailHelper.SendAppointment(ConfigurationManager.AppSettings["NoReplyEmail"], ci.CompanyEmail, msg, sub, ci.CompanyCity, ri.CheckInDate.AddHours(15).ToUniversalTime(), ri.CheckOutDate.AddHours(11).ToUniversalTime());
            return true;
        }
        catch
        {
            return false;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    public static bool SendRefundNotification(int orderID, string transactionID, double amount, string guestName, string guestEmail)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            Orders ord = new Orders();
            ord.PopulateObject(dbHelp, orderID);

            Product adminFee = new Product();
            adminFee.PopulateAdminFeeProduct(dbHelp);

            TotalSummary ts = CalculationHelper.GetOrderTotal(dbHelp, orderID);

            EmailNotifications notification = new EmailNotifications();

            string sub = "Refund Acknowledgement";
            string msg = File.ReadAllText(HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplates/Refund.htm", Globals.CurrentAppLanguageCode.ToLower())));
            string oldDetails = notification.GetMessage(dbHelp, orderID, NotificationType.ReservationDetailsGuestSnipt);
            msg = msg.Replace("#GUEST_NAME#", guestName).Replace("#ORDER_NO#", orderID.ToString())
                .Replace("#TRANSACTION_ID#", transactionID)
                .Replace("#AMOUNT_PAID#", string.Format("<b>{0}</b> {1:F}", ts.CurrencyCode, amount + adminFee.PrdEndUserSalesPrice))
                .Replace("#AMOUNT_TO_REFUND#", string.Format("<b>{0}</b> {1:F}", ts.CurrencyCode, amount))
                .Replace("#ADMIN_FEE#", string.Format("<b>{0}</b> {1:F}", ts.CurrencyCode, adminFee.PrdEndUserSalesPrice))
                .Replace("#RESERVATION_DETAILS#", oldDetails);
                                                                    
            notification.EmailBody = msg;
            notification.EmailSubject = sub;
            notification.NotificationSourcePk = orderID;
            notification.NotificationTypeRef = NotificationType.ReservationOrderCancelRefundToGuest;
            notification.ToEmailAddresses = guestEmail;
            notification.Insert(dbHelp);

            return EmailHelper.SendEmail(ConfigurationManager.AppSettings["NoReplyEmail"], guestEmail, msg, sub, true);
        }
        catch
        {
            return false;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    public static string GetReservationDetailSnipt(DbHelper dbHelp, int orderID)
    {
        Orders ord = new Orders();
        ord.PopulateObject(dbHelp, orderID);

        TotalSummary ts = CalculationHelper.GetOrderTotal(dbHelp, orderID);        

        Partners part = new Partners();
        part.PopulateObject(dbHelp, ord.OrdCustID);

        Reservation rsv = new Reservation();
        ReservationItems ri = rsv.GetFirstReservationItem(dbHelp, ord.OrdID);

        Bed b = new Bed();
        b.BedID = ri.BedID;
        b.PopulateBedInfo(dbHelp, b, Globals.CurrentAppLanguageCode);

        string sqlGuest = "SELECT v.BedTitle, p.FirstName, p.LastName, p.SpirtualName, ri.PricePerDay FROM z_reservation_items ri INNER JOIN vw_beds v ON v.BedID=ri.BedID INNER JOIN z_customer_extended p ON p.PartnerID=ri.GuestID WHERE ri.ReservationID = @ReservationID";
        string bedGuestList = string.Empty;
        int totalBeds = 0;
        double toCalculatePricePerDay = 0.0D;
        double totalPriceCalculated = 0.0D;
        string spName = string.Empty;
        try
        {
            using (MySqlDataReader dr = dbHelp.GetDataReader(sqlGuest, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ReservationID", ri.ReservationID, MyDbType.Int)
                    }))
            {
                int iCount = 0;
                while (dr.Read())
                {
                    iCount++;
                    bedGuestList += string.Format("{3}-{0} : {1} {2}<br>", iCount, dr["LastName"], dr["FirstName"], Resources.Resource.lblBedCaps);
                    totalPriceCalculated += BusinessUtility.GetDouble(dr["PricePerDay"]);
                    spName = BusinessUtility.GetString(dr["SpirtualName"]);
                    totalBeds++;
                }
            }
        }
        finally
        {

        }

        toCalculatePricePerDay = (totalPriceCalculated) / (double)(totalBeds == 0 ? 1 : totalBeds);


        string services = string.Empty;
        SysProcessGroup serviceProducts = new SysProcessGroup();
        double serviceSubTotal = 0.0D;
        try
        {
            using (IDataReader dr = serviceProducts.GetReserverdServicesForWeb(dbHelp, orderID, ts.CurrencyCode))
            {
                while (dr.Read())
                {
                    double subTotal = BusinessUtility.GetDouble(dr["ordProductUnitPrice"]) * BusinessUtility.GetDouble(dr["ordProductQty"]);
                    services += string.Format("{0} :{1} <b>x</b> {2} ={3} {4:F}<br>", dr["prdName"], dr["ordProductQty"], dr["ordProductUnitPrice"], ts.CurrencyCode, subTotal);
                    serviceSubTotal += subTotal;
                }
                dr.Close();
                dr.Dispose();
            }
        }
        finally
        {
        }

        services += string.Format("{0} :{1} {2:F} </ br>", "Service Sub Total", ts.CurrencyCode, serviceSubTotal);

        string msg = File.ReadAllText(HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplates/ReservationDetails_en.htm", Globals.CurrentAppLanguageCode.ToLower())));
        msg = msg.Replace("#CONFIRMATION_NO#", string.Format("{0}{1}", part.ExtendedProperties.LastName, ord.OrdID))
                .Replace("#CHECKIN_DATE#", ri.CheckInDate.ToString("dddd MMMM dd, yyyy"))
                .Replace("#CHECKOUT_DATE#", ri.CheckOutDate.ToString("dddd MMMM dd, yyyy"))
                .Replace("#TOTAL_BEDS#", totalBeds.ToString())
                .Replace("#ROOM_TYPE#", UIHelper.GetRoomType(b.RoomType))
                .Replace("#BED_GUEST_LIST#", bedGuestList)
                .Replace("#BEDCOST_PER_NIGHT#", string.Format("{0} {1:F}", ts.CurrencyCode, toCalculatePricePerDay))
                .Replace("#NO_OF_NIGHTS#", ri.TotalNights.ToString())
                .Replace("#BED_SUB_TOTAL#", string.Format("{0} {1:F}", ts.CurrencyCode, totalPriceCalculated))
                .Replace("#SERVICES#", services)
                .Replace("#REMAINING_BALANCE#", string.Format("{0} {1:F}", ts.CurrencyCode, ts.OutstandingAmount))
                .Replace("#GRAND_TOTAL#", string.Format("{0} {1:F}", ts.CurrencyCode, ts.GrandTotal))
                .Replace("#TRANSACTION_ID#", new OrderTransaction().GetSaleTransactionID(dbHelp, orderID));

        if (totalBeds > 1)
        {
            msg = msg.Replace("#BED_RESOURCE#", Resources.Resource.lblBedPlural);
        }
        else
        {
            msg = msg.Replace("#BED_RESOURCE#", Resources.Resource.lblBedSingular);
        }

        return msg;
    }
    #endregion
}

public class EmailNotifications
{
    public int NotificationID { get; set; }
    public int NotificationSourcePk { get; set; }
    public NotificationType NotificationTypeRef { get; set; }
    public string ToEmailAddresses { get; set; }
    public string EmailSubject { get; set; }
    public string EmailBody { get; set; }
    public string SentOn { get; set; }

    public void Insert(DbHelper dbHelp)
    {
        string sql = "INSERT INTO z_email_notifications (NotificationSourcePk,NotificationTypeRef,ToEmailAddresses,EmailSubject,EmailBody,SentOn)";
        sql += " VALUES(@NotificationSourcePk,@NotificationTypeRef,@ToEmailAddresses,@EmailSubject,@EmailBody,@SentOn)";
        try
        {
            dbHelp.ExecuteNonQuery(sql, CommandType.Text, new MySqlParameter[] { 
                DbUtility.GetParameter("NotificationSourcePk", this.NotificationSourcePk, MyDbType.Int),
                DbUtility.GetParameter("NotificationTypeRef", (int)this.NotificationTypeRef, MyDbType.Int),
                DbUtility.GetParameter("ToEmailAddresses", this.ToEmailAddresses, MyDbType.String),
                DbUtility.GetParameter("EmailSubject", this.EmailSubject, MyDbType.String),
                DbUtility.GetParameter("EmailBody", this.EmailBody, MyDbType.String),
                DbUtility.GetParameter("SentOn", DateTime.Now, MyDbType.DateTime)
            });
        }
        catch
        {
            throw;
        }        
    }

    public string GetMessage(int sourcePk, NotificationType nType) {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            return this.GetMessage(dbHelp, sourcePk, nType);
        }
        catch
        {

            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    public string GetMessage(DbHelper dbHelp, int sourcePk, NotificationType nType)
    {
        string sql = "SELECT EmailBody FROM z_email_notifications WHERE NotificationSourcePk=@NotificationSourcePk AND NotificationTypeRef=@NotificationTypeRef ";
        sql += " ORDER BY NotificationID LIMIT 1";        
        try
        {
            object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                DbUtility.GetParameter("NotificationSourcePk", sourcePk, MyDbType.Int),
                DbUtility.GetParameter("NotificationTypeRef", (int)nType, MyDbType.Int)
            });
            return BusinessUtility.GetString(val);
        }
        catch
        {
            throw;
        }       
    }
}

public enum NotificationType
{ 
    ReservationOrderSuccessToGuest = 1,
    ReservationOrderSuccessToAshram = 2,
    ReservationOrderCancelRefundToGuest = 3,
    ReservationDetailsGuestSnipt = 4
}


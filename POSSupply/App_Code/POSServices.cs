﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using System.Xml;
using iTECH.WebService.Utility;
using iTECH.Library.DataAccess.MySql;
using System.Net.Mail;
using System.Resources;
using System.Globalization;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using System.Threading;

/// <summary>
/// Summary description for POSServices
/// </summary/// >
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
[System.Web.Script.Services.ScriptService]
public class POSServices : System.Web.Services.WebService
{

    public POSServices()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string funCashTransaction(string lngID, string strTokan, string strUserID, string strRegCode, string strTax, string strTotalAmt, string strTotalSubAmt, string strTransType, string strReturnAmt, string strReceivedAmt, string strWhshouse, string strProductDtl, string strBtnType, string strPreviousTranID, string strTabNote, string giftReason, string authorizedBy, string ipAddress, string customerID, string custCurrencyCode, string custType, string custInvoiceNetTerms, string salesRepID, string isPartialPayment, string totalAmountBeforePartial, string partialPaymentDtl, string isPrint, string sMailID, string isGiftReceipt, string isLayawayOrder, string laywayOrderID, string sVGiftCardNo, string sVRefundedVia)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            if (strTransType != "0")
            {
                string[] item = { "__item__" };
                string[] tax = { "__taxGrp__" };
                string strvalue = "";
                string msgSuccess = "";

                #region Chk_GiftCardNo_Allready_Saved_OR_Not
                string[] strDPrdGiftCard = strProductDtl.Split(item, System.StringSplitOptions.None);
                string[] strDPrd = strProductDtl.Split(item, System.StringSplitOptions.None);
                string[] strDTaxOnPrd = null;
                string[] strDTaxValue = null;
                int intDI = 0;
                string[] strDPrdItem = null;
                while (strDPrdGiftCard.Length - 1 > intDI)
                {
                    strDTaxValue = strDPrd[intDI].Split(tax, System.StringSplitOptions.None);
                    strDPrdItem = strDTaxValue[0].Split('~');
                    if (strDPrdItem.Length > 1)
                    {
                        if (BusinessUtility.GetString(strDPrdItem[7]) != "")
                        {
                            string sCardNo = BusinessUtility.GetString(strDPrdItem[7]);
                            GiftCard objGiftCard = new GiftCard();

                            if (sCardNo.ToUpper() != "undefined".ToUpper() && sCardNo != "")
                            {
                                if (objGiftCard.IsExistsGiftCardNo(null, sCardNo) == true)
                                {
                                    if (lngID == "fr")
                                    {
                                        dictResult.Add("CustNotFound", CommonResource.ResourceValue("lblGiftCardNoAllreadySaved", "fr-CA").Replace("#GIFTNO#", sCardNo));
                                    }
                                    else
                                    {
                                        dictResult.Add("CustNotFound", CommonResource.ResourceValue("lblGiftCardNoAllreadySaved", "en-CA").Replace("#GIFTNO#", sCardNo));
                                    }

                                    dictResult.OkStatus(ResponseCode.UNSUCCESS);
                                    return dictResult.ResultInInJSONFormat;
                                }
                            }

                        }
                    }
                    intDI += 1;
                }

                #endregion


                List<PartialPaymentDetail> lstPartialPaymentDtl = new List<PartialPaymentDetail>();
                #region PartialPaymentDetialList
                if (partialPaymentDtl != "")
                {
                    string[] partialPayment = partialPaymentDtl.Split('~');
                    foreach (string sItem in partialPayment)
                    {
                        string[] paymentDtl = sItem.Split(':');
                        string sPaymentType = BusinessUtility.GetString(paymentDtl[0]);
                        double sAmountPaid = BusinessUtility.GetDouble(paymentDtl[1]);
                        string sGiftCardNo = BusinessUtility.GetString(paymentDtl[2]);
                        int iPaymentType = 0;
                        if (BusinessUtility.GetString(sPaymentType).ToLower() == "Cash".ToLower())
                        { iPaymentType = (int)StatusAmountReceivedVia.Cash; }
                        else if (BusinessUtility.GetString(sPaymentType).ToLower() == "Credit".ToLower())
                        {
                            iPaymentType = (int)StatusAmountReceivedVia.CreditCard;
                        }
                        else if (BusinessUtility.GetString(sPaymentType).ToLower() == "Debit".ToLower())
                        {
                            iPaymentType = (int)StatusAmountReceivedVia.DebitCard;
                        }
                        else if (BusinessUtility.GetString(sPaymentType).ToLower() == "ExactCash".ToLower())
                        {
                            iPaymentType = (int)StatusAmountReceivedVia.Cash;
                        }
                        else if (BusinessUtility.GetString(sPaymentType).ToLower() == "Waste".ToLower())
                        {
                            // For Lost
                            iPaymentType = (int)StatusAmountReceivedVia.GiftFree;
                        }
                        else if (BusinessUtility.GetString(sPaymentType).ToLower() == "Gifts".ToLower())
                        {
                            // For Gift
                            //iPaymentType = (int)StatusAmountReceivedVia.GiftCard;
                            iPaymentType = (int)StatusAmountReceivedVia.GiftFree;
                        }
                        else if (BusinessUtility.GetString(sPaymentType).ToLower() == "GiftCards".ToLower())
                        {
                            // For Gift Cards
                            iPaymentType = (int)StatusAmountReceivedVia.GiftCard;
                        }
                        else if (BusinessUtility.GetString(sPaymentType).ToLower() == "InStoreCredit".ToLower())
                        {
                            // For InStore Credit
                            iPaymentType = (int)StatusAmountReceivedVia.InStoreCredit;
                        }
                        else if (BusinessUtility.GetString(sPaymentType).ToLower() == "Visa".ToLower())
                        {
                            // For Visa
                            iPaymentType = (int)StatusAmountReceivedVia.Visa;
                        }
                        else if (BusinessUtility.GetString(sPaymentType).ToLower() == "MC".ToLower())
                        {
                            // For MC
                            iPaymentType = (int)StatusAmountReceivedVia.MC;
                        }
                        else if (BusinessUtility.GetString(sPaymentType).ToLower() == "AmEx".ToLower())
                        {
                            // For AmEx
                            iPaymentType = (int)StatusAmountReceivedVia.AmEx;
                        }
                        else if (BusinessUtility.GetString(sPaymentType).ToLower() == "OGL".ToLower())
                        {
                            // For OGL
                            iPaymentType = (int)StatusAmountReceivedVia.OGL;
                        }



                        if (isLayawayOrder == "1" && iPaymentType == (int)StatusAmountReceivedVia.InStoreCredit)
                        {

                        }
                        else
                        {
                            if (sAmountPaid > 0)
                            {
                                lstPartialPaymentDtl.Add(new PartialPaymentDetail() { Amount = sAmountPaid, PaymentType = iPaymentType, GiftCardNo = sGiftCardNo });
                            }
                        }
                    }
                }
                #endregion


                SysCompanyInfo compInfo = new SysCompanyInfo();
                int companyID = BusinessUtility.GetInt(compInfo.WarehouseCompanyID(strWhshouse));
                compInfo.PopulateObject(companyID);
                SysCurrencies cur = new SysCurrencies();
                cur.PopulateObject(compInfo.CompanyBasCur);
                int userID = BusinessUtility.GetInt(strUserID);
                int custID = 0;
                if (string.IsNullOrEmpty(customerID))
                {
                    SysRegister sysReg = new SysRegister();
                    sysReg.PopulateObject(strRegCode);
                    custID = sysReg.SysDefaultCustomerID;
                    if (custID == 0)
                    {
                        if (lngID == "fr")
                        {
                            dictResult.Add("CustNotFound", CommonResource.ResourceValue("lblCustomerNotFound", "fr-CA"));
                        }
                        else
                        {
                            dictResult.Add("CustNotFound", CommonResource.ResourceValue("lblCustomerNotFound", "en-CA"));
                        }

                        dictResult.OkStatus(ResponseCode.UNSUCCESS);
                        return dictResult.ResultInInJSONFormat;
                    }
                }
                else if (isPartialPayment != "1" && isPartialPayment != "0")
                {
                    dictResult.Add("CustNotFound", "Payment Mode Not Found");//CommonResource.ResourceValue("lblCustomerNotFound", "en-CA")
                    dictResult.OkStatus(ResponseCode.UNSUCCESS);
                    return dictResult.ResultInInJSONFormat;
                }
                else
                {
                    custID = BusinessUtility.GetInt(customerID);
                }
                Orders ord = new Orders();
                ord.InvDate = DateTime.Now;
                ord.InvRefNo = 0;
                ord.OrdComment = string.Empty;
                ord.OrdCompanyID = companyID; //Get Company ID
                ord.OrdCreatedBy = userID; //Get Creator User            
                ord.OrdCreatedFromIP = HttpContext.Current.Request.UserHostAddress;//ipAddress; //Request.ServerVariables["REMOTE_ADDR"];
                ord.OrdCreatedOn = DateTime.Now; //BusinessUtility.GetDateTime(dateString, "yyyy-MM-ddTHH:mm:sszzz");
                ord.OrdCurrencyCode = compInfo.CompanyBasCur;
                ord.OrdCurrencyExRate = cur.CurrencyRelativePrice;
                ord.OrdCustID = custID;//Search customer By long name & email address & phone no
                ord.OrdCustType = Globals.GetPartnerType(BusinessUtility.GetInt(custType));
                ord.OrdDiscount = 0.00D;
                ord.OrdDiscountType = "P";
                ord.OrderRejectReason = string.Empty;
                ord.OrderTypeCommission = (int)OrderCommission.POS;
                ord.OrdLastUpdateBy = userID;
                ord.OrdLastUpdatedOn = DateTime.Now;
                ord.OrdNetTerms = custInvoiceNetTerms;
                //ord.OrdSalesRepID = userID;
                ord.OrdSalesRepID = BusinessUtility.GetInt(salesRepID);
                ord.OrdSaleWeb = false;
                ord.OrdShippingTerms = string.Empty;
                ord.OrdShpBlankPref = false;
                ord.OrdShpCode = string.Empty;
                ord.OrdShpCost = 0.00D;
                ord.OrdShpDate = DateTime.Now;
                ord.OrdShpTrackNo = string.Empty;
                ord.OrdShpWhsCode = strWhshouse;
                ord.OrdStatus = SOStatus.READY_FOR_SHIPPING;
                ord.OrdType = SOType.QUOTATION;
                ord.OrdVerified = true;
                ord.OrdVerifiedBy = userID;
                ord.QutExpDate = DateTime.MinValue;
                ord.OrdCustPO = string.Empty;
                ord.OrdRegCode = strRegCode;
                ////Save Order

                if ((isLayawayOrder == "0" || isLayawayOrder == "1") && BusinessUtility.GetInt(laywayOrderID) > 0)
                {
                    ord.OrdID = BusinessUtility.GetInt(laywayOrderID);
                    ord.Update(null, userID);
                    Addresses _billToAddress = new Addresses();
                    Addresses _shipToAddress = new Addresses();
                    // Save Order Shippment and Billing Address
                    _billToAddress.GetCustomerAddress(ord.OrdCustID, AddressType.BILL_TO_ADDRESS);
                    ord.SaveOrderAddresse(ord.OrdID, _billToAddress.AddressLine1, _billToAddress.AddressLine2, _billToAddress.AddressLine3, _billToAddress.AddressCity, _billToAddress.AddressState, _billToAddress.AddressCountry, _billToAddress.AddressPostalCode, AddressType.BILL_TO_ADDRESS);

                    //Addresses shipToAddress = new Addresses();
                    _shipToAddress.GetCustomerAddress(ord.OrdCustID, AddressType.SHIP_TO_ADDRESS);
                    ord.SaveOrderAddresse(ord.OrdID, _shipToAddress.AddressLine1, _shipToAddress.AddressLine2, _shipToAddress.AddressLine3, _shipToAddress.AddressCity, _shipToAddress.AddressState, _shipToAddress.AddressCountry, _shipToAddress.AddressPostalCode, AddressType.SHIP_TO_ADDRESS);

                    // Set Sales Order Rep
                    List<int> srUsers = new List<int>();
                    srUsers.Add(BusinessUtility.GetInt(salesRepID));
                    if (srUsers.Count > 0)
                    {
                        ord.SetOrderSalesRepUsers(null, ord.OrdID, srUsers.ToArray());
                    }

                    #region InsertUpdateOrderItem
                    List<OrderItems> ordItems = new List<OrderItems>();
                    string[] strPrd = strProductDtl.Split(item, System.StringSplitOptions.None);
                    string[] strTaxOnPrd = null;
                    string[] strTaxValue = null;
                    int intI = 0;
                    int intR = 0;
                    string[] strPrdItem = null;
                    //objPosPrdNo.PrdTransID = strTranID;
                    //bool checkPrdDescExec = false;
                    SysTaxCodeDesc stcd = new SysTaxCodeDesc();
                    DataTable dt = new DataTable();
                    #region DeleteLayWayOrderItem
                    OrderItems oi = new OrderItems();
                    if (BusinessUtility.GetInt(laywayOrderID) > 0)
                    {
                        oi.DeleteOrderItems(BusinessUtility.GetInt(laywayOrderID));
                    }
                    #endregion

                    while (strPrd.Length - 1 > intI)
                    {
                        strTaxValue = strPrd[intI].Split(tax, System.StringSplitOptions.None);
                        strPrdItem = strTaxValue[0].Split('~');
                        if (strPrdItem.Length > 1)
                        {
                            oi = new OrderItems();

                            int iOrdItemID = oi.GetOrderItemID(null, BusinessUtility.GetInt(laywayOrderID), BusinessUtility.GetInt(strPrdItem[0]));
                            if (iOrdItemID <= 0)
                            {
                                oi.OrderItemDesc = strPrdItem[2];
                                oi.OrdID = ord.OrdID;
                                //oi.OrdProductDiscount = 0;
                                oi.OrdProductDiscount = BusinessUtility.GetInt(strPrdItem[5]);
                                oi.OrdProductDiscountType = "P";
                                oi.OrdProductID = BusinessUtility.GetInt(strPrdItem[0]);
                                oi.OrdProductQty = BusinessUtility.GetDouble(strPrdItem[1]);
                                oi.OrdProductTaxGrp = BusinessUtility.GetInt(strPrdItem[6]);

                                oi.OrdProductUnitPrice = BusinessUtility.GetDouble(strPrdItem[3]);
                                oi.OrdGuestID = custID;
                                oi.Insert(null);

                                //ordItems.Add(oi);
                            }
                            else
                            {
                                oi.OrderItemDesc = strPrdItem[2];
                                oi.OrdID = ord.OrdID;
                                //oi.OrdProductDiscount = 0;
                                oi.OrdProductDiscount = BusinessUtility.GetInt(strPrdItem[5]);
                                oi.OrdProductDiscountType = "P";
                                oi.OrdProductID = BusinessUtility.GetInt(strPrdItem[0]);
                                oi.OrdProductQty = BusinessUtility.GetDouble(strPrdItem[1]);
                                oi.OrdProductTaxGrp = BusinessUtility.GetInt(strPrdItem[6]);

                                oi.OrdProductUnitPrice = BusinessUtility.GetDouble(strPrdItem[3]);
                                oi.OrdGuestID = custID;
                                oi.Update(null);
                            }
                        }
                        intI += 1;
                    }
                    #endregion


                    #region OrderPaymentReceive
                    //Amt received
                    //Partners part = new Partners();
                    //Addresses shpAddr = part.GetShipToAddress(null, custID, BusinessUtility.GetInt(custType));
                    DbHelper dbHelp = new DbHelper();
                    // Receive Payment If Not Partial Payment
                    if (isPartialPayment == "0")
                    {
                        #region NotParitalPayment
                        PreAccountRcv arc = new PreAccountRcv();
                        arc.AmountDeposit = BusinessUtility.GetDouble(strReceivedAmt);
                        arc.CustomerID = custID;
                        arc.DateReceived = DateTime.Now;
                        arc.OrderID = ord.OrdID;
                        if (BusinessUtility.GetInt(strTransType) == 1)
                        { arc.PaymentMethod = (int)StatusAmountReceivedVia.Cash; }
                        else if (BusinessUtility.GetInt(strTransType) == 2)
                        {
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.CreditCard;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 5)
                        {
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.DebitCard;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 6)
                        {
                            //arc.PaymentMethod = (int)StatusAmountReceivedVia.WriteOff; 
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.Cash;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 12)
                        {
                            // For Lost
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.GiftFree;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 11)
                        {
                            // For Gift
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.GiftCard;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 13)
                        {
                            // For Gift
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.InStoreCredit;

                            CustomerCredit cr = new CustomerCredit();
                            cr.CreditAmount = (-1.0D) * BusinessUtility.GetDouble(strReceivedAmt);
                            cr.CreditedBy = userID;
                            cr.CreditedOn = DateTime.Now;
                            cr.CreditVia = (int)StatusAmountReceivedVia.Credit;
                            cr.CustomerID = custID;
                            cr.RefundType = "C";
                            cr.WhsCode = ord.OrdShpWhsCode;
                            cr.RegCode = ord.OrdRegCode;
                            cr.Insert(dbHelp, userID);

                        }
                        else if (BusinessUtility.GetInt(strTransType) == 14)
                        {
                            // For Visa
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.Visa;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 15)
                        {
                            // For MC
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.MC;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 16)
                        {
                            // For AmEx
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.AmEx;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 17)
                        {
                            // For OGL
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.OGL;
                        }

                        arc.Notes = strBtnType;

                        int iRcvID = arc.GetRcvID(null);
                        if (iRcvID > 0)
                        {
                            arc.RcvID = iRcvID;
                            arc.Update(dbHelp);
                        }
                        else
                        {
                            arc.Insert(dbHelp);
                        }
                        #endregion
                    }
                    else if (isPartialPayment == "1")
                    {
                        strReceivedAmt = "0.00";
                        double dblAmountRecived = 0.00;
                        foreach (var vPaymentDetail in lstPartialPaymentDtl)
                        {
                            dblAmountRecived += BusinessUtility.GetDouble(vPaymentDetail.Amount);
                            PreAccountRcv objArc = new PreAccountRcv();
                            objArc.AmountDeposit = BusinessUtility.GetDouble(vPaymentDetail.Amount);
                            objArc.CustomerID = custID;
                            objArc.DateReceived = DateTime.Now;
                            objArc.OrderID = ord.OrdID;
                            objArc.PaymentMethod = BusinessUtility.GetInt(vPaymentDetail.PaymentType);
                            objArc.Notes = strBtnType;
                            //objArc.Insert(dbHelp);
                            int iRcvID = objArc.GetRcvID(null);
                            if (iRcvID > 0)
                            {
                                objArc.RcvID = iRcvID;
                                objArc.Update(dbHelp);
                            }
                            else
                            {
                                objArc.Insert(dbHelp);
                            }


                            if (BusinessUtility.GetInt(vPaymentDetail.PaymentType) == (int)StatusAmountReceivedVia.InStoreCredit)
                            {
                                CustomerCredit cr = new CustomerCredit();
                                cr.CreditAmount = (-1.0D) * BusinessUtility.GetDouble(vPaymentDetail.Amount);
                                cr.CreditedBy = userID;
                                cr.CreditedOn = DateTime.Now;
                                cr.CreditVia = (int)StatusAmountReceivedVia.Credit;
                                cr.CustomerID = custID;
                                cr.RefundType = "C";
                                cr.WhsCode = ord.OrdShpWhsCode;
                                cr.RegCode = ord.OrdRegCode;
                                cr.Insert(dbHelp, userID);
                            }
                        }
                        strReceivedAmt = BusinessUtility.GetString(dblAmountRecived);
                    }
                    #endregion

                    if ((isLayawayOrder == "0"))
                    {
                        #region Code_Not_Excute_when_Creating_Layway_Order

                        //Shipping process
                        ord.OrdShpDate = DateTime.Now;
                        ord.OrdShpCode = string.Empty;//string.Format("{0}-{1}", "POS", strTabNote);
                        ord.OrdStatus = SOStatus.SHIPPED;
                        ord.Update(null, userID);
                        //Add shipping process item to the order
                        SysProcessGroup pg = new SysProcessGroup();
                        pg.PopulateObject(null, BusinessUtility.GetInt(AppConfiguration.AppSettings[AppSettingKey.ShippingProcessID]));

                        //Populate Order Customer Info to set tax code for shipping process


                        //OrderItemProcess process = new OrderItemProcess();
                        SysWarehouses sWhs = new SysWarehouses();
                        sWhs.PopulateObject(ord.OrdShpWhsCode, null);

                        ord.UpdateInventoryOnShipping(null, ord.OrdID, ord.OrdShpWhsCode, BusinessUtility.GetString(InvMovmentSrc.POS), userID);

                        #region GenerateInvoice
                        //Generate Invoice
                        Invoice objInv = new Invoice();
                        bool isDistinctInvoice = ConfigurationManager.AppSettings["SeparateInvID"].ToLower() == "yes";
                        objInv.PopulateObjectByOrder(null, ord.OrdID, InvoicesStatus.SUBMITTED, InvoiceReferenceType.INVOICE, userID);
                        if (BusinessUtility.GetInt(laywayOrderID) > 0)
                        {
                            objInv.InvCreatedOn = DateTime.Now;
                        }
                        objInv.Insert(null, userID);
                        if (objInv.InvID > 0)
                        {
                            if (objInv.InvRefNo == 0)
                            {
                                objInv.UpdateInvoiceRefNo(null, objInv.InvID, objInv.InvCompanyID, objInv.InvRefType, isDistinctInvoice);
                            }
                            InvoiceItems invItems = new InvoiceItems();
                            invItems.AddInvoiceItemsFromOrder(null, ord.OrdID, objInv.InvID);



                            double dblGiftCardAmt = 0;

                            #region AddGiftCardProduct
                            string[] strPrdGiftCard = strProductDtl.Split(item, System.StringSplitOptions.None);
                            intI = 0;
                            while (strPrdGiftCard.Length - 1 > intI)
                            {
                                strTaxValue = strPrd[intI].Split(tax, System.StringSplitOptions.None);
                                strPrdItem = strTaxValue[0].Split('~');
                                if (strPrdItem.Length > 1)
                                {
                                    if (BusinessUtility.GetString(strPrdItem[7]) != "")
                                    {
                                        GiftCard objGiftCard = new GiftCard();
                                        objGiftCard.GiftCardNo = BusinessUtility.GetString(strPrdItem[7]);
                                        objGiftCard.TransactionType = GiftCardTransType.Buy;
                                        objGiftCard.Amount = BusinessUtility.GetDouble(strPrdItem[3]);
                                        objGiftCard.POSUser = userID;
                                        objGiftCard.PartnerID = objInv.InvCustID;
                                        objGiftCard.InvoiceID = objInv.InvID;
                                        objGiftCard.Save(null);

                                        dblGiftCardAmt += BusinessUtility.GetDouble(strPrdItem[4]);
                                    }
                                }
                                intI += 1;
                            }
                            #endregion

                            #region LoyaltyPointEntry
                            //Entry of LoyalPoint
                            Partners part = new Partners();
                            double amtPerPoint = sWhs.GetAmtPerPoint(strWhshouse);
                            int calculatedPoint = 0;
                            if (amtPerPoint > 0)
                            {
                                calculatedPoint = BusinessUtility.GetInt((BusinessUtility.GetDouble(strTotalSubAmt) - dblGiftCardAmt) / amtPerPoint);
                            }
                            part.PopulateObject(custID);
                            LoyalPartnerHistory lph = new LoyalPartnerHistory();
                            lph.LoyalCategoryID = part.PartnerLoyalCategoryID;
                            lph.LoyalPartnerID = part.PartnerID;
                            lph.PointAuto = false;
                            lph.Points = calculatedPoint;
                            lph.PointsAddedBy = userID;
                            lph.PointsAddedOn = DateTime.Now;
                            lph.PosTransactionID = objInv.InvID;
                            lph.InvoiceNo = objInv.InvID;
                            lph.OrderNo = ord.OrdID;
                            lph.Insert(null);
                            #endregion

                            //Update account receivable
                            PreAccountRcv acrc = new PreAccountRcv();
                            acrc.MoveToActualAR(null, userID, ord.OrdID, objInv.InvID);

                            //Once Invoice generated successfully just make a entry in sls Analysis table 
                            new SlsAnalysis().RecordOnInvoiceGeneration(null, objInv.InvID);

                            //Refund entry on Accountreceiable table
                            //Make amout -ve so that it can be added in same received ammount field
                            double refundAmt = BusinessUtility.GetDouble(strReturnAmt);
                            if (refundAmt > 0) refundAmt = refundAmt * (-1.00D);
                            //if (refundAmt > 0)
                            {
                                ////Add Account receivable Entry
                                AccountReceivable arc = new AccountReceivable();
                                arc.ARAmtRcvd = refundAmt;
                                arc.ARAmtRcvdDateTime = DateTime.Now;

                                if (sVRefundedVia == "")
                                {
                                    arc.ARAmtRcvdVia = (int)StatusAmountReceivedVia.Refund;
                                }
                                else
                                {
                                    arc.ARAmtRcvdVia = BusinessUtility.GetInt(sVRefundedVia);
                                }
                                //arc.ARAmtRcvdVia = (int)StatusAmountReceivedVia.Refund;
                                arc.ARChequeNo = "";
                                arc.ARColAsigned = false;
                                arc.ARContactEmail = "";
                                arc.ARContactNm = "";
                                arc.ARContactPhone = "";
                                arc.ARCreditCardCVVCode = "";
                                arc.ARCreditCardExp = string.Empty;
                                arc.ARCreditCardFirstName = "";
                                arc.ARCreditCardLastName = "";
                                arc.ARCreditCardNo = "";
                                arc.ARInvoiceNo = objInv.InvID;
                                arc.ARNote = "";
                                arc.ARRcvdBy = userID;
                                arc.ARWriteOff = 0.00;
                                arc.ARReceiptNo = "";
                                arc.Reason = "";
                                arc.Insert(null, userID);


                                // To Add Amount In-store A/c of User
                                if (BusinessUtility.GetInt(sVRefundedVia) == (int)StatusAmountReceivedVia.InStoreCredit)
                                {
                                    CustomerCredit cc = new CustomerCredit();
                                    cc.RefundType = "C";
                                    cc.CreditVia = cc.RefundType == "C" ? (int)StatusAmountReceivedVia.Credit : (int)StatusAmountReceivedVia.Refund;
                                    cc.CreditedBy = userID;
                                    cc.CustomerID = objInv.InvCustID;
                                    cc.ReservationItemID = 0;
                                    cc.Notes = "";
                                    cc.WhsCode = objInv.InvShpWhsCode;
                                    cc.RegCode = objInv.InvRegCode;
                                    if (refundAmt * (-1.00D) > 0)
                                    {
                                        cc.CreditAmount = refundAmt * (-1.00D);
                                        cc.Insert(null, userID); //Insert Record to Track Customer refunds
                                    }
                                }
                            }
                        }
                        #endregion

                        //i.ToString("D4")
                        dictResult.Add("InvoiceID", objInv.InvID.ToString("D6"));
                        if (lngID == "fr")
                        {
                            string cultureName = "fr-CA";
                            msgSuccess = CommonResource.ResourceValue("msgTransaction", cultureName) + " " + objInv.InvID + " " + CommonResource.ResourceValue("msgCompletedSuccess", cultureName) + "<br>" + CommonResource.ResourceValue("POSTotalAmount", cultureName) + " " + CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(strTotalAmt), "", cultureName) + ", " + CommonResource.ResourceValue("POSReceived", cultureName) + " " + CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(strReceivedAmt), "", cultureName) + ", " + CommonResource.ResourceValue("POSBalance", cultureName) + " " + CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(strReturnAmt), "", cultureName));
                        }
                        else
                        {
                            string cultureName = "en-CA";
                            msgSuccess = CommonResource.ResourceValue("msgTransaction", cultureName) + " " + objInv.InvID + " " + CommonResource.ResourceValue("msgCompletedSuccess", cultureName) + "<br>" + CommonResource.ResourceValue("POSTotalAmount", cultureName) + " " + CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(strTotalAmt), "", cultureName) + ", " + CommonResource.ResourceValue("POSReceived", cultureName) + " " + CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(strReceivedAmt), "", cultureName) + ", " + CommonResource.ResourceValue("POSBalance", cultureName) + " " + CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(strReturnAmt), "", cultureName));
                        }
                        #endregion
                    }
                }
                else
                {

                    #region OrderInsert
                    if (ord.Insert(null, userID))
                    {
                        Addresses _billToAddress = new Addresses();
                        Addresses _shipToAddress = new Addresses();
                        // Save Order Shippment and Billing Address
                        _billToAddress.GetCustomerAddress(ord.OrdCustID, AddressType.BILL_TO_ADDRESS);
                        ord.SaveOrderAddresse(ord.OrdID, _billToAddress.AddressLine1, _billToAddress.AddressLine2, _billToAddress.AddressLine3, _billToAddress.AddressCity, _billToAddress.AddressState, _billToAddress.AddressCountry, _billToAddress.AddressPostalCode, AddressType.BILL_TO_ADDRESS);

                        //Addresses shipToAddress = new Addresses();
                        _shipToAddress.GetCustomerAddress(ord.OrdCustID, AddressType.SHIP_TO_ADDRESS);
                        ord.SaveOrderAddresse(ord.OrdID, _shipToAddress.AddressLine1, _shipToAddress.AddressLine2, _shipToAddress.AddressLine3, _shipToAddress.AddressCity, _shipToAddress.AddressState, _shipToAddress.AddressCountry, _shipToAddress.AddressPostalCode, AddressType.SHIP_TO_ADDRESS);

                        // Set Sales Order Rep
                        List<int> srUsers = new List<int>();
                        srUsers.Add(BusinessUtility.GetInt(salesRepID));
                        if (srUsers.Count > 0)
                        {
                            ord.SetOrderSalesRepUsers(null, ord.OrdID, srUsers.ToArray());
                        }
                        List<OrderItems> ordItems = new List<OrderItems>();
                        string[] strPrd = strProductDtl.Split(item, System.StringSplitOptions.None);
                        string[] strTaxOnPrd = null;
                        string[] strTaxValue = null;
                        int intI = 0;
                        int intR = 0;
                        string[] strPrdItem = null;
                        //objPosPrdNo.PrdTransID = strTranID;
                        //bool checkPrdDescExec = false;
                        SysTaxCodeDesc stcd = new SysTaxCodeDesc();
                        DataTable dt = new DataTable();
                        while (strPrd.Length - 1 > intI)
                        {
                            strTaxValue = strPrd[intI].Split(tax, System.StringSplitOptions.None);
                            strPrdItem = strTaxValue[0].Split('~');
                            if (strPrdItem.Length > 1)
                            {
                                OrderItems oi = new OrderItems();
                                oi.OrderItemDesc = strPrdItem[2];
                                oi.OrdID = ord.OrdID;
                                //oi.OrdProductDiscount = 0;
                                oi.OrdProductDiscount = BusinessUtility.GetInt(strPrdItem[5]);
                                oi.OrdProductDiscountType = "P";
                                oi.OrdProductID = BusinessUtility.GetInt(strPrdItem[0]);
                                oi.OrdProductQty = BusinessUtility.GetDouble(strPrdItem[1]);
                                oi.OrdProductTaxGrp = BusinessUtility.GetInt(strPrdItem[6]);

                                oi.OrdProductUnitPrice = BusinessUtility.GetDouble(strPrdItem[3]);
                                oi.OrdGuestID = custID;
                                ordItems.Add(oi);
                            }
                            intI += 1;
                        }

                        OrderItems ordItem = new OrderItems();
                        ordItem.AddOrderItems(null, ordItems);
                    }
                    #endregion

                    #region OrderPaymentReceive
                    //Amt received
                    Partners part = new Partners();
                    Addresses shpAddr = part.GetShipToAddress(null, custID, BusinessUtility.GetInt(custType));
                    DbHelper dbHelp = new DbHelper();
                    // Receive Payment If Not Partial Payment
                    if (isPartialPayment == "1")
                    {
                        strReceivedAmt = "0.00";
                        double dblAmountRecived = 0.00;
                        foreach (var vPaymentDetail in lstPartialPaymentDtl)
                        {
                            dblAmountRecived += BusinessUtility.GetDouble(vPaymentDetail.Amount);
                            PreAccountRcv objArc = new PreAccountRcv();
                            objArc.AmountDeposit = BusinessUtility.GetDouble(vPaymentDetail.Amount);
                            objArc.CustomerID = custID;
                            objArc.DateReceived = DateTime.Now;
                            objArc.OrderID = ord.OrdID;
                            objArc.PaymentMethod = BusinessUtility.GetInt(vPaymentDetail.PaymentType);
                            objArc.Notes = strBtnType;
                            objArc.Insert(dbHelp);

                            if (BusinessUtility.GetInt(vPaymentDetail.PaymentType) == (int)StatusAmountReceivedVia.InStoreCredit)
                            {
                                CustomerCredit cr = new CustomerCredit();
                                cr.CreditAmount = (-1.0D) * BusinessUtility.GetDouble(vPaymentDetail.Amount);
                                cr.CreditedBy = userID;
                                cr.CreditedOn = DateTime.Now;
                                cr.CreditVia = (int)StatusAmountReceivedVia.Credit;
                                cr.CustomerID = custID;
                                cr.RefundType = "C";
                                cr.WhsCode = ord.OrdShpWhsCode;
                                cr.RegCode = ord.OrdRegCode;
                                cr.Insert(dbHelp, userID);
                            }
                        }
                        strReceivedAmt = BusinessUtility.GetString(dblAmountRecived);
                    }
                    else //if (isPartialPayment == "1")
                    {
                        #region NotParitalPayment
                        PreAccountRcv arc = new PreAccountRcv();
                        arc.AmountDeposit = BusinessUtility.GetDouble(strReceivedAmt);
                        arc.CustomerID = custID;
                        arc.DateReceived = DateTime.Now;
                        arc.OrderID = ord.OrdID;
                        if (BusinessUtility.GetInt(strTransType) == 1)
                        { arc.PaymentMethod = (int)StatusAmountReceivedVia.Cash; }
                        else if (BusinessUtility.GetInt(strTransType) == 2)
                        {
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.CreditCard;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 5)
                        {
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.DebitCard;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 6)
                        {
                            //arc.PaymentMethod = (int)StatusAmountReceivedVia.WriteOff; 
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.Cash;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 12)
                        {
                            // For Lost
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.GiftFree;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 11)
                        {
                            // For Gift
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.GiftCard;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 13)
                        {
                            // For Gift
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.InStoreCredit;

                            CustomerCredit cr = new CustomerCredit();
                            cr.CreditAmount = (-1.0D) * BusinessUtility.GetDouble(strReceivedAmt);
                            cr.CreditedBy = userID;
                            cr.CreditedOn = DateTime.Now;
                            cr.CreditVia = (int)StatusAmountReceivedVia.Credit;
                            cr.CustomerID = custID;
                            cr.RefundType = "C";
                            cr.WhsCode = ord.OrdShpWhsCode;
                            cr.RegCode = ord.OrdRegCode;
                            cr.Insert(dbHelp, userID);
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 14)
                        {
                            // For Visa
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.Visa;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 15)
                        {
                            // For MC
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.MC;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 16)
                        {
                            // For AmEx
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.AmEx;
                        }
                        else if (BusinessUtility.GetInt(strTransType) == 17)
                        {
                            // For OGL
                            arc.PaymentMethod = (int)StatusAmountReceivedVia.OGL;
                        }

                        arc.Notes = strBtnType;
                        arc.Insert(dbHelp);
                        #endregion
                    }
                    #endregion
                }

                if (isLayawayOrder == "0" && BusinessUtility.GetInt(laywayOrderID) == 0)
                {
                    #region Code_Not_Excute_when_Creating_Layway_Order

                    //Shipping process
                    ord.OrdShpDate = DateTime.Now;
                    ord.OrdShpCode = string.Empty;//string.Format("{0}-{1}", "POS", strTabNote);
                    ord.OrdStatus = SOStatus.SHIPPED;
                    ord.Update(null, userID);
                    //Add shipping process item to the order
                    SysProcessGroup pg = new SysProcessGroup();
                    pg.PopulateObject(null, BusinessUtility.GetInt(AppConfiguration.AppSettings[AppSettingKey.ShippingProcessID]));

                    //Populate Order Customer Info to set tax code for shipping process


                    //OrderItemProcess process = new OrderItemProcess();
                    SysWarehouses sWhs = new SysWarehouses();
                    sWhs.PopulateObject(ord.OrdShpWhsCode, null);

                    ord.UpdateInventoryOnShipping(null, ord.OrdID, ord.OrdShpWhsCode, BusinessUtility.GetString(InvMovmentSrc.POS), userID);

                    #region GenerateInvoice
                    //Generate Invoice
                    Invoice objInv = new Invoice();
                    bool isDistinctInvoice = ConfigurationManager.AppSettings["SeparateInvID"].ToLower() == "yes";
                    objInv.PopulateObjectByOrder(null, ord.OrdID, InvoicesStatus.SUBMITTED, InvoiceReferenceType.INVOICE, userID);
                    objInv.Insert(null, userID);
                    if (objInv.InvID > 0)
                    {
                        if (objInv.InvRefNo == 0)
                        {
                            objInv.UpdateInvoiceRefNo(null, objInv.InvID, objInv.InvCompanyID, objInv.InvRefType, isDistinctInvoice);
                        }
                        InvoiceItems invItems = new InvoiceItems();
                        invItems.AddInvoiceItemsFromOrder(null, ord.OrdID, objInv.InvID);

                        double dblGiftCardAmt = 0;

                        #region AddGiftCardProduct
                        string[] strPrdGiftCard = strProductDtl.Split(item, System.StringSplitOptions.None);
                        string[] strPrd = strProductDtl.Split(item, System.StringSplitOptions.None);
                        string[] strTaxOnPrd = null;
                        string[] strTaxValue = null;
                        int intI = 0;
                        string[] strPrdItem = null;
                        while (strPrdGiftCard.Length - 1 > intI)
                        {
                            strTaxValue = strPrd[intI].Split(tax, System.StringSplitOptions.None);
                            strPrdItem = strTaxValue[0].Split('~');
                            if (strPrdItem.Length > 1)
                            {
                                if (BusinessUtility.GetString(strPrdItem[7]) != "")
                                {
                                    GiftCard objGiftCard = new GiftCard();
                                    objGiftCard.GiftCardNo = BusinessUtility.GetString(strPrdItem[7]);
                                    objGiftCard.TransactionType = GiftCardTransType.Buy;
                                    objGiftCard.Amount = BusinessUtility.GetDouble(strPrdItem[3]);
                                    objGiftCard.POSUser = userID;
                                    objGiftCard.PartnerID = objInv.InvCustID;
                                    objGiftCard.InvoiceID = objInv.InvID;
                                    objGiftCard.Save(null);
                                    dblGiftCardAmt += BusinessUtility.GetDouble(strPrdItem[4]);
                                }
                            }
                            intI += 1;
                        }



                        if (isPartialPayment == "0")
                        {
                            #region NotParitalPayment
                            if (BusinessUtility.GetInt(strTransType) == 11)
                            {
                                GiftCard objGiftCard = new GiftCard();
                                objGiftCard.GiftCardNo = BusinessUtility.GetString(sVGiftCardNo).Trim();
                                objGiftCard.TransactionType = GiftCardTransType.Sale;
                                objGiftCard.Amount = BusinessUtility.GetDouble(strReceivedAmt);
                                objGiftCard.POSUser = userID;
                                objGiftCard.PartnerID = objInv.InvCustID;
                                objGiftCard.InvoiceID = objInv.InvID;
                                objGiftCard.Save(null);
                                // For Gift
                                //arc.PaymentMethod = (int)StatusAmountReceivedVia.GiftCard;
                            }
                            #endregion
                        }
                        else if (isPartialPayment == "1")
                        {
                            strReceivedAmt = "0.00";
                            double dblAmountRecived = 0.00;
                            foreach (var vPaymentDetail in lstPartialPaymentDtl)
                            {
                                if (BusinessUtility.GetInt(vPaymentDetail.PaymentType) == (int)StatusAmountReceivedVia.GiftCard)
                                {
                                    GiftCard objGiftCard = new GiftCard();
                                    objGiftCard.GiftCardNo = BusinessUtility.GetString(vPaymentDetail.GiftCardNo).Trim();
                                    objGiftCard.TransactionType = GiftCardTransType.Sale;
                                    objGiftCard.Amount = BusinessUtility.GetDouble(vPaymentDetail.Amount);
                                    objGiftCard.POSUser = userID;
                                    objGiftCard.PartnerID = objInv.InvCustID;
                                    objGiftCard.InvoiceID = objInv.InvID;
                                    objGiftCard.Save(null);
                                }
                            }
                            strReceivedAmt = BusinessUtility.GetString(dblAmountRecived);
                        }


                        #endregion

                        #region LoyaltyPointEntry
                        //Entry of LoyalPoint
                        Partners part = new Partners();
                        double amtPerPoint = sWhs.GetAmtPerPoint(strWhshouse);
                        int calculatedPoint = 0;
                        if (amtPerPoint > 0)
                        {
                            calculatedPoint = BusinessUtility.GetInt((BusinessUtility.GetDouble(strTotalSubAmt) - dblGiftCardAmt) / amtPerPoint);
                        }
                        part.PopulateObject(custID);
                        LoyalPartnerHistory lph = new LoyalPartnerHistory();
                        lph.LoyalCategoryID = part.PartnerLoyalCategoryID;
                        lph.LoyalPartnerID = part.PartnerID;
                        lph.PointAuto = false;
                        lph.Points = calculatedPoint;
                        lph.PointsAddedBy = userID;
                        lph.PointsAddedOn = DateTime.Now;
                        lph.PosTransactionID = objInv.InvID;
                        lph.InvoiceNo = objInv.InvID;
                        lph.OrderNo = ord.OrdID;
                        lph.Insert(null);
                        #endregion
                        //Update account receivable
                        PreAccountRcv acrc = new PreAccountRcv();
                        acrc.MoveToActualAR(null, userID, ord.OrdID, objInv.InvID);

                        //Once Invoice generated successfully just make a entry in sls Analysis table 
                        new SlsAnalysis().RecordOnInvoiceGeneration(null, objInv.InvID);

                        //Refund entry on Accountreceiable table
                        //Make amout -ve so that it can be added in same received ammount field
                        double refundAmt = BusinessUtility.GetDouble(strReturnAmt);
                        if (refundAmt > 0) refundAmt = refundAmt * (-1.00D);
                        //if (refundAmt > 0)
                        {
                            ////Add Account receivable Entry
                            AccountReceivable arc = new AccountReceivable();
                            arc.ARAmtRcvd = refundAmt;
                            arc.ARAmtRcvdDateTime = DateTime.Now;
                            if (sVRefundedVia == "")
                            {
                                arc.ARAmtRcvdVia = (int)StatusAmountReceivedVia.Refund;
                            }
                            else
                            {
                                arc.ARAmtRcvdVia = BusinessUtility.GetInt(sVRefundedVia);
                            }
                            //arc.ARAmtRcvdVia = (int)StatusAmountReceivedVia.Refund;
                            arc.ARChequeNo = "";
                            arc.ARColAsigned = false;
                            arc.ARContactEmail = "";
                            arc.ARContactNm = "";
                            arc.ARContactPhone = "";
                            arc.ARCreditCardCVVCode = "";
                            arc.ARCreditCardExp = string.Empty;
                            arc.ARCreditCardFirstName = "";
                            arc.ARCreditCardLastName = "";
                            arc.ARCreditCardNo = "";
                            arc.ARInvoiceNo = objInv.InvID;
                            arc.ARNote = "";
                            arc.ARRcvdBy = userID;
                            arc.ARWriteOff = 0.00;
                            arc.ARReceiptNo = "";
                            arc.Reason = "";
                            arc.Insert(null, userID);

                            // To Add Amount In-store A/c of User
                            if (BusinessUtility.GetInt(sVRefundedVia) == (int)StatusAmountReceivedVia.InStoreCredit)
                            {
                                CustomerCredit cc = new CustomerCredit();
                                cc.RefundType = "C";
                                cc.CreditVia = cc.RefundType == "C" ? (int)StatusAmountReceivedVia.Credit : (int)StatusAmountReceivedVia.Refund;
                                cc.CreditedBy = userID;
                                cc.CustomerID = objInv.InvCustID;
                                cc.ReservationItemID = 0;
                                cc.Notes = "";
                                cc.WhsCode = objInv.InvShpWhsCode;
                                cc.RegCode = objInv.InvRegCode;
                                if (refundAmt * (-1.00D) > 0)
                                {
                                    cc.CreditAmount = refundAmt * (-1.00D);
                                    cc.Insert(null, userID); //Insert Record to Track Customer refunds
                                }
                            }
                        }
                    }
                    #endregion

                    //i.ToString("D4")
                    dictResult.Add("InvoiceID", objInv.InvID.ToString("D6"));

                    if (lngID == "fr")
                    {
                        string cultureName = "fr-CA";
                        msgSuccess = CommonResource.ResourceValue("msgTransaction", cultureName) + " " + objInv.InvID + " " + CommonResource.ResourceValue("msgCompletedSuccess", cultureName) + "<br>" + CommonResource.ResourceValue("POSTotalAmount", cultureName) + " " + CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(strTotalAmt), "", cultureName) + ", " + CommonResource.ResourceValue("POSReceived", cultureName) + " " + CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(strReceivedAmt), "", cultureName) + ", " + CommonResource.ResourceValue("POSBalance", cultureName) + " " + CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(strReturnAmt), "", cultureName));
                    }
                    else
                    {
                        string cultureName = "en-CA";
                        msgSuccess = CommonResource.ResourceValue("msgTransaction", cultureName) + " " + objInv.InvID + " " + CommonResource.ResourceValue("msgCompletedSuccess", cultureName) + "<br>" + CommonResource.ResourceValue("POSTotalAmount", cultureName) + " " + CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(strTotalAmt), "", cultureName) + ", " + CommonResource.ResourceValue("POSReceived", cultureName) + " " + CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(strReceivedAmt), "", cultureName) + ", " + CommonResource.ResourceValue("POSBalance", cultureName) + " " + CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(strReturnAmt), "", cultureName));
                    }

                    //if (isPrint.ToUpper() == "FALSE")
                    //{
                    //    //string sMailContent= GetHtmlContent(BusinessUtility.GetInt(objInv.InvID), BusinessUtility.GetString(strRegCode), BusinessUtility.GetString(sMailID), strWhshouse, lngID, isGiftReceipt);
                    //    //dictResult.Add("mailContentToSend", sMailContent);

                    //    if (CreateHtmlContent(BusinessUtility.GetInt(objInv.InvID), BusinessUtility.GetString(strRegCode), BusinessUtility.GetString(sMailID), strWhshouse, lngID, isGiftReceipt) == true)
                    //    {
                    //        //dictResult.OkStatus(ResponseCode.SUCCESS);
                    //    }
                    //    ////Thread thrdSendMail = new Thread(() => SendMail(BusinessUtility.GetInt(objInv.InvID), BusinessUtility.GetString(strRegCode), BusinessUtility.GetString(sMailID), strWhshouse, lngID, isGiftReceipt));
                    //    ////thrdSendMail.Start();
                    //    //////thrdSendMail.Abort();
                    //}

                    #endregion
                }
                else if (isLayawayOrder == "0" && BusinessUtility.GetInt(laywayOrderID) > 0)
                {
                }
                else
                {
                    //Shipping process
                    ord.OrdShpDate = DateTime.Now;
                    ord.OrdShpCode = string.Empty;//string.Format("{0}-{1}", "POS", strTabNote);
                    ord.OrdStatus = SOStatus.APPROVED;
                    ord.Update(null, userID);
                    if (lngID == "fr")
                    {
                        string cultureName = "fr-CA";
                        msgSuccess = CommonResource.ResourceValue("MsgLaywayOrderSaved", cultureName).Replace("#ORDERID#", BusinessUtility.GetString(ord.OrdID)); //+ " " + ord.OrdID +
                    }
                    else
                    {
                        string cultureName = "en-CA";
                        msgSuccess = CommonResource.ResourceValue("MsgLaywayOrderSaved", cultureName).Replace("#ORDERID#", BusinessUtility.GetString(ord.OrdID)); //+ " " + ord.OrdID +
                    }
                }
                
                //for checking Print Marchent Copy is true or false
                SysRegister sysRegMC = new SysRegister();
                sysRegMC.PopulateObject(strRegCode);
                dictResult.Add("printMerchantCopy", sysRegMC.SysRegPrintMerchantCopy);
                
                dictResult.Add("msgSuccess", msgSuccess);
                dictResult.Add("refundAmount", strReturnAmt);//

                dictResult.OkStatus(ResponseCode.SUCCESS);
                return dictResult.ResultInInJSONFormat;
            }
            else
            {

                if ((isLayawayOrder == "0" || isLayawayOrder == "1") && BusinessUtility.GetInt(laywayOrderID) > 0)
                {
                    Orders objOrd = new Orders();
                    objOrd.UpdateOrderStatus(null, BusinessUtility.GetInt(laywayOrderID), "C");
                }

                if (lngID == "fr")
                {
                    dictResult.Add("msgSuccess", CommonResource.ResourceValue("POSTransactionCancel", "fr-CA"));
                }
                else
                {
                    dictResult.Add("msgSuccess", CommonResource.ResourceValue("POSTransactionCancel", "en-CA"));
                }
                dictResult.OkStatus(ResponseCode.SUCCESS);
                return dictResult.ResultInInJSONFormat;
            }
        }
        catch (Exception ex)
        {

            if (lngID == "fr")
            {
                dictResult.Add("catchError", CommonResource.ResourceValue("POSCashTransactionFailed", "fr-CA"));
            }
            else
            {
                dictResult.Add("catchError", CommonResource.ResourceValue("POSCashTransactionFailed", "en-CA"));
            }
            return dictResult.ResultInInJSONFormat;
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetTaxValue(string taxCode, string taxDesc, string lngID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            SysTaxCode objTaxCode = new SysTaxCode();
            StringBuilder strTax = new StringBuilder();
            var lstTax = objTaxCode.GetAllTaxes(null, BusinessUtility.GetInt(taxCode));
            foreach (var item in lstTax)
            {
                //strTax.Append(drObj.Item("sysTaxDesc").ToString & "~" & drObj.Item("sysTaxPercentage").ToString & "~" & drObj.Item("sysTaxSequence").ToString & "~" & drObj.Item("sysTaxOnTotal").ToString & "~" & drObj.Item("sysTaxCode").ToString & "^")
                strTax.Append(BusinessUtility.GetString(item.sysTaxDesc) + "~" + BusinessUtility.GetString(item.sysTaxPercentage) + "~" + BusinessUtility.GetString(item.sysTaxSequence) + "~" + BusinessUtility.GetString(item.sysTaxOnTotal) + "~" + BusinessUtility.GetString(item.sysTaxCode) + "^");
                //sysTaxDesc").ToString & "~" & drObj.Item("sysTaxPercentage").ToString & "~" & drObj.Item("sysTaxSequence").ToString & "~" & drObj.Item("sysTaxOnTotal").ToString & "~" & drObj.Item("sysTaxCode").ToString & "^")
            }
            //objTaxCode.PopulateObject(null, BusinessUtility.GetInt(taxCode), BusinessUtility.GetString(taxDesc));
            dictResult.Add("taxValue", BusinessUtility.GetString(strTax));
            dictResult.OkStatus(ResponseCode.SUCCESS);
            return dictResult.ResultInInJSONFormat;
        }
        catch (Exception ex)
        {

            if (lngID == "fr")
            {
                dictResult.Add("catchError", CommonResource.ResourceValue("POSCashTransactionFailed", "fr-CA"));
            }
            else
            {
                dictResult.Add("catchError", CommonResource.ResourceValue("POSCashTransactionFailed", "en-CA"));
            }
            return dictResult.ResultInInJSONFormat;
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string SendReceiptMail(string lngID, string invID, string strRegCode, string sMailID, string sWhrCode, string isGiftReceipt)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            if (CreateHtmlContent(BusinessUtility.GetInt(invID), BusinessUtility.GetString(strRegCode), BusinessUtility.GetString(sMailID), sWhrCode, lngID, isGiftReceipt) == true)
            {
                dictResult.OkStatus(ResponseCode.SUCCESS);
            }
            else
            {
                dictResult.OkStatus(ResponseCode.UNSUCCESS);
            }
            return dictResult.ResultInInJSONFormat;
        }
        catch (Exception ex)
        {

            if (lngID == "fr")
            {
                dictResult.Add("catchError", CommonResource.ResourceValue("POSCashTransactionFailed", "fr-CA"));
            }
            else
            {
                dictResult.Add("catchError", CommonResource.ResourceValue("POSCashTransactionFailed", "en-CA"));
            }
            return dictResult.ResultInInJSONFormat;
        }
    }

    //public void SendMail(int invID, string regCode, string mailID, string whrHouse, string langID, string isGiftReceipt)
    //{
    //    //if (CreateHtmlContent(BusinessUtility.GetInt(objInv.InvID), BusinessUtility.GetString(strRegCode), BusinessUtility.GetString(sMailID), strWhshouse, lngID, isGiftReceipt) == true)
    //    if (CreateHtmlContent(invID, regCode, mailID, whrHouse, langID, isGiftReceipt) == true)
    //    {
    //        //dictResult.OkStatus(ResponseCode.SUCCESS);
    //    }
    //}

    private bool CreateHtmlContent(int invID, string sRegCode, string sEmailID, string sWhrCode, string lngID, string isGiftReceipt)
    {
        string htmlSnnipt = @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">";
        //htmlSnnipt += @"<html xmlns=""http://www.w3.org/1999/xhtml""><head><title>Print</title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><link href='../Css/style.css' rel='stylesheet' type='text/css' /></head><body>#DATA_TO_WRITE#</body></html>";
        htmlSnnipt += @"<html xmlns=""http://www.w3.org/1999/xhtml""><head><title>Print</title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><link href='#CSSURL#' rel='stylesheet' type='text/css' /></head><body>#DATA_TO_WRITE#</body></html>";
        //string cssUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/CSS/") + "style.css";
        string cssUrl = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/CSS/") + "style.css";
        htmlSnnipt = htmlSnnipt.Replace("#CSSURL#", cssUrl);

        string htmlToMail = string.Empty;
        htmlToMail = CommonPrint.GetPOSResceiptHtmlSnipt(invID, sRegCode, sWhrCode, lngID);

        if (isGiftReceipt == "1")
        {
            htmlToMail += "<div style='page-break-after: always;'></div>" + CommonPrint.GetPOSGiftResceiptHtmlSnipt(invID, sRegCode, sWhrCode, lngID);
        }
        string strContent = htmlSnnipt.Replace("#DATA_TO_WRITE#", htmlToMail);
        string strFileName = "Receipt" + "_" + invID + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".html";
        string strFullName = Server.MapPath("~") + "/pdf/" + strFileName;
        //File.WriteAllText(strFullName, strContent);

        if (sEmailID != "")
        {
            string strMailFrom = System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"];
            string strMailTo = sEmailID;
            string strSub = "Payment Receipt";
            string strMsg = "";
            strMsg = "";
            strMsg = htmlToMail;

            //EmailHelper.SendEmail(strMailFrom,strMailTo,strMsg,strSub,true);
            //  return true;

            //if (EmailHelper.SendEmail(strMailFrom, strMailTo, strMsg, strSub, strFullName))
            if (EmailHelper.SendEmail(strMailFrom, strMailTo, strMsg, strSub, true))
            {
                return true;
            }
        }

        return false;
    }

    private string GetHtmlContent(int invID, string sRegCode, string sEmailID, string sWhrCode, string lngID, string isGiftReceipt)
    {
        string htmlSnnipt = @"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">";
        //htmlSnnipt += @"<html xmlns=""http://www.w3.org/1999/xhtml""><head><title>Print</title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><link href='../Css/style.css' rel='stylesheet' type='text/css' /></head><body>#DATA_TO_WRITE#</body></html>";
        htmlSnnipt += @"<html xmlns=""http://www.w3.org/1999/xhtml""><head><title>Print</title><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><link href='#CSSURL#' rel='stylesheet' type='text/css' /></head><body>#DATA_TO_WRITE#</body></html>";
        //string cssUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/CSS/") + "style.css";
        string cssUrl = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + System.Web.VirtualPathUtility.ToAbsolute("~/CSS/") + "style.css";
        htmlSnnipt = htmlSnnipt.Replace("#CSSURL#", cssUrl);

        string htmlToMail = string.Empty;
        htmlToMail = CommonPrint.GetPOSResceiptHtmlSnipt(invID, sRegCode, sWhrCode, lngID);

        if (isGiftReceipt == "1")
        {
            htmlToMail += "<div style='page-break-after: always;'></div>" + CommonPrint.GetPOSGiftResceiptHtmlSnipt(invID, sRegCode, sWhrCode, lngID);
        }
        string strContent = htmlSnnipt.Replace("#DATA_TO_WRITE#", htmlToMail);
        string strFileName = "Receipt" + "_" + invID + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".html";
        string strFullName = Server.MapPath("~") + "/pdf/" + strFileName;
        //File.WriteAllText(strFullName, strContent);

        //if (sEmailID != "")
        //{
        //    string strMailFrom = System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"];
        //    string strMailTo = sEmailID;
        //    string strSub = "Payment Receipt";
        //    string strMsg = "";
        //    strMsg = "";
        //    strMsg = htmlToMail;

        //    //if (EmailHelper.SendEmail(strMailFrom, strMailTo, strMsg, strSub, strFullName))
        //    if (EmailHelper.SendEmail(strMailFrom, strMailTo, strMsg, strSub, true))
        //    {
        //        return true;
        //    }
        //}

        //return false;
        return htmlToMail;
    }


    private static string ServerPath
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["ServerRootPath"]);
        }
    }


    //[System.Web.Services.WebMethod]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public string  SetSessionValue(string key, string value)
    //{
    //    ResultDictionary dictResult = new ResultDictionary();
    //    try
    //    {

    //        //HttpContext.Current.Session[key] = value;
    //        HttpContext.Current.Session["SavedData"] = value;
    //        dictResult.OkStatus(ResponseCode.SUCCESS);

    //    }
    //    catch (Exception e)
    //    {
    //        dictResult.OkStatus(ResponseCode.UNSUCCESS);
    //    }
    //    return dictResult.ResultInInJSONFormat;
    //}

    //[WebMethod]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public string GetSessionValue(string key)
    //{
    //    ResultDictionary dictResult = new ResultDictionary();
    //    try
    //    {

    //        if (HttpContext.Current.Session[key] != null)
    //        {
    //            dictResult.Add("returnValue", BusinessUtility.GetString(HttpContext.Current.Session[key].ToString()));
    //            return dictResult.ResultInInJSONFormat;
    //        }
    //        else
    //        {
    //            dictResult.OkStatus(ResponseCode.UNSUCCESS);

    //        }
    //    }
    //    catch (Exception e)
    //    {

    //    }
    //    return dictResult.ResultInInJSONFormat;
    //}

    //[System.Web.Services.WebMethod]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public string RemoveSessionValue(string key)
    //{
    //    ResultDictionary dictResult = new ResultDictionary();
    //    HttpContext.Current.Session[key] = null;
    //    dictResult.OkStatus(ResponseCode.SUCCESS);
    //    return dictResult.ResultInInJSONFormat;
    //}

    public class PartialPaymentDetail
    {
        public int PaymentType { get; set; }
        public Double Amount { get; set; }
        public string GiftCardNo { get; set; }
    }
}

Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Imports System.IO
Partial Class AdminMaster
    Inherits System.Web.UI.MasterPage
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Commented after adding image map for choose language option on 10jan 2011
        'If Session("Language") = "fr-CA" Then
        '    lnkFrench.CssClass = "current"
        '    'ElseIf Session("Language") = "es-MX" Then
        '    '    lnkSpenish.CssClass = "current"
        'Else
        '    Session("Language") = "en-CA"
        '    lnkEnglish.CssClass = "current"
        'End If


        Me.imgCmdHelp.Attributes.Add("OnClick", "return openPDF('../Help.aspx')")
        If Not IsPostBack Then
            Dim currentURL As String = Request.RawUrl
            If Request.QueryString("lang") = "logout" Then
                If Session("Label") = "1" Then
                    subCopyResource() 'copy resource file
                End If
                Session.Abandon()
                Response.Redirect("../AdminLogin.aspx?lang=logout")
            End If

            Dim objCompany As New clsCompanyInfo
            Dim objCommon As New clsCommon
            Dim strPic() As String
            lblComName.Text = objCompany.funGetFirstCompanyName
            Dim strLogo As String = objCompany.funGetCompanyLogo
            'imgHeader.Src = ConfigurationManager.AppSettings("CompanyLogoPath") & strLogo
            'strPic = objCommon.funImageSize(strLogo, "220", "78", "Comp", "Yes").Split("/")
            'imgHeader.Width = strPic(0)
            'imgHeader.Height = strPic(1)
        End If
    End Sub
    Public Sub subCopyResource()
        Dim sourcePath As String = Server.MapPath("../translator/Label/")
        Dim DestinationPath As String = Server.MapPath("../App_globalResources/")
        If (Directory.Exists(sourcePath)) Then
            For Each fName As String In Directory.GetFiles(sourcePath)
                If File.Exists(fName) Then
                    Dim dFile As String = String.Empty
                    dFile = Path.GetFileName(fName)
                    Dim dFilePath As String = String.Empty
                    dFilePath = DestinationPath + dFile
                    'If Not System.IO.File.Exists(dFilePath) Then
                    File.Copy(fName, dFilePath, True)
                    'End If
                End If
            Next
        End If
    End Sub
    'Protected Sub imgEnglish_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEnglish.Click

    'End Sub

    'Protected Sub imgFrench_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgFrench.Click

    'End Sub

    'Protected Sub imgSpanish_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSpanish.Click

    'End Sub

    Protected Sub imgCmdHelp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCmdHelp.Click
        Response.Write("<script>window.open('../help.aspx', 'Help','height=500,width=700,right=150,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');</script>")
    End Sub

    'Commented after adding image map for choose language option on 10jan 2011
    'Protected Sub lnkEnglish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEnglish.Click
    '    Session("Language") = "en-CA"
    '    Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
    '    Response.Redirect(Request.RawUrl)
    'End Sub

    'Protected Sub lnkFrench_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFrench.Click
    '    Session("Language") = "fr-CA"
    '    Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
    '    Response.Redirect(Request.RawUrl)
    'End Sub

    'Protected Sub lnkSpenish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSpenish.Click
    '    Session("Language") = "es-MX"
    '    Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
    '    Response.Redirect(Request.RawUrl)
    'End Sub

    Protected Sub imgMap1_Click(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ImageMapEventArgs) Handles imgMap1.Click
        If e.PostBackValue = "fr" Then
            Session("Language") = "fr-CA"
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
            Response.Redirect(Request.RawUrl)
        Else
            Session("Language") = "en-CA"
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
            Response.Redirect(Request.RawUrl)            
        End If
    End Sub
End Class


Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Data.Odbc

Partial Class _Service
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        NoCache()
        If Request.QueryString("AlertID") <> "" Then
            RemoveAlert(Request.QueryString("AlertID"))
        End If
        If Request.QueryString("seq") <> "" Then
            GenerateAlert(Request.QueryString("seq"))
        End If
    End Sub
    Public Sub NoCache()
        Response.Cache.SetExpires(DateTime.Now.AddMonths(-1))
        Response.Expires = 0
        Response.ExpiresAbsolute = DateTime.Now.AddMonths(-1)
        Response.AddHeader("pragma", "no-cache")
        Response.AddHeader("cache-control", "private")
        Response.CacheControl = "no-cache"
        Response.Expires = -1000
    End Sub
    Public Sub GenerateAlert(ByVal seq As String)
        Dim strSql As String = "SELECT 'COM' as Type, AlertID, concat(ContactLastName, ' ', ifnull(ContactFirstName,'')) as Name, Left(AlertNote,55) as Note, AlertDateTime, null as InvID FROM alerts "
        strSql += " inner join partnercontacts on ContactID=AlertPartnerContactID where AlertDateTime < now() and AlertViewCount is null and AlertRefType is null and AlertUserID='" & Convert.ToString(Session("UserID")) & "' "

        strSql += " union SELECT 'RCV' as Type, AlertID, null as Name, AlertNote as Note, AlertDateTime, null as InvID FROM alerts "
        strSql += " where AlertDateTime < now() and AlertViewCount is null and AlertRefType='RCV' and AlertUserID='" & Convert.ToString(Session("UserID")) & "' "

        strSql += " union SELECT 'COL' as Type, AlertID, null as Name, AlertNote as Note, AlertDateTime,AlertPartnerContactID as InvID FROM alerts "
        strSql += " where AlertDateTime < now() and AlertViewCount is null and AlertRefType='COL' and AlertUserID='" & Convert.ToString(Session("UserID")) & "' "

        strSql += " union SELECT 'STM' as Type, AlertID, null as Name, AlertNote as Note, AlertDateTime,AlertPartnerContactID as InvID FROM alerts "
        strSql += " where AlertDateTime < now() and AlertViewCount is null and AlertRefType='STM' and AlertUserID='" & Convert.ToString(Session("UserID")) & "' "

        strSql += " union SELECT 'ALE' as Type, AlertID, null as Name, AlertNote as Note, AlertDateTime,AlertPartnerContactID as InvID FROM alerts "
        strSql += " where AlertDateTime < now() and AlertViewCount is null and AlertRefType='ALE' and AlertUserID='" & Convert.ToString(Session("UserID")) & "' "

        strSql += " union SELECT 'JOB' as Type, AlertID, null as Name, AlertNote as Note, AlertDateTime,AlertPartnerContactID as InvID FROM alerts "
        strSql += " where AlertDateTime < now() and AlertViewCount is null and AlertRefType='JOB' and AlertUserID='" & Convert.ToString(Session("UserID")) & "' "

        strSql += " union SELECT 'SO' as Type, AlertID, null as Name, AlertNote as Note, AlertDateTime,AlertPartnerContactID as InvID FROM alerts "
        strSql += " where AlertDateTime < now() and AlertViewCount is null and AlertRefType='SO' and AlertUserID='" & Convert.ToString(Session("UserID")) & "' "

        strSql += " union SELECT 'PO' as Type, AlertID, null as Name, AlertNote as Note, AlertDateTime,AlertPartnerContactID as InvID FROM alerts "
        strSql += " where AlertDateTime < now() and AlertViewCount is null and AlertRefType='PO' and AlertUserID='" & Convert.ToString(Session("UserID")) & "' "

        strSql += " union SELECT 'POA' as Type, AlertID, null as Name, AlertNote as Note, AlertDateTime,AlertPartnerContactID as InvID FROM alerts "
        strSql += " where AlertDateTime < now() and AlertViewCount is null and AlertRefType='POA' and AlertUserID='" & Convert.ToString(Session("UserID")) & "' "

        strSql += " union SELECT 'SOA' as Type, AlertID, null as Name, AlertNote as Note, AlertDateTime,AlertPartnerContactID as InvID FROM alerts "
        strSql += " where AlertDateTime < now() and AlertViewCount is null and AlertRefType='SOA' and AlertUserID='" & Convert.ToString(Session("UserID")) & "' "

        strSql += " union SELECT 'CFL' as Type, AlertID, null as Name, AlertNote as Note, AlertDateTime,AlertPartnerContactID as InvID FROM alerts "
        strSql += " where AlertDateTime < now() and AlertViewCount is null and AlertRefType='CFL' and AlertUserID='" & Convert.ToString(Session("UserID")) & "' "

        strSql += " order by AlertDateTime asc, AlertID asc limit " & seq & ",1 "
        Dim strData As String = ""

        Dim objDB As New clsDataClass
        Dim dr As OdbcDataReader = objDB.GetDataReader(strSql)

        Dim strContact As String = "", strType As String = ""
        While dr.Read
            strType = dr.Item("Type").ToString
            If strType = "COM" Then
                strContact = dr.Item("Name").ToString
                If strContact.Length > 11 Then
                    strContact = dr.Item("Name").ToString.Substring(0, 11) & ".."
                End If
                strData = "<!-- <hTitle>" & strContact & ": " & dr.Item("AlertDateTime").ToString & "</hTitle>-->"
                strData += "<!-- <AlertID>" & dr.Item("AlertID").ToString & "</AlertID>-->"
                strData += "<br>" & lblAlertContactName & " " & dr.Item("Name").ToString & "<br><br>" & lblAlertNote2 & " " & dr.Item("Note").ToString
                strData += "...<br><br>" & lblAlertTime & " " & dr.Item("AlertDateTime").ToString & "<br>"
                strData += "<a href='../Partner/ReadAlert.aspx?id=" & dr.Item("AlertID").ToString & "'><img border=0 style='padding-top:7px;' src=../images/ViewDetails.png /></a><br>"
            ElseIf strType = "RCV" Then
                strData = "<!-- <hTitle>" & lblPOQtyAlert & ": " & dr.Item("AlertDateTime").ToString & "</hTitle>-->"
                strData += "<!-- <AlertID>" & dr.Item("AlertID").ToString & "</AlertID>-->"
                strData += "<br>" & dr.Item("Note").ToString
            ElseIf strType = "COL" Then
                strData = "<!-- <hTitle>" & lblCollActivity & ": " & dr.Item("AlertDateTime").ToString & "</hTitle>-->"
                strData += "<!-- <AlertID>" & dr.Item("AlertID").ToString & "</AlertID>-->"
                strData += "<br>" & dr.Item("Note").ToString & "<br>"
                strData += "<a href='../CollectionAgent/ViewFollowUpLog.aspx?SOID=" & dr.Item("InvID").ToString & "'><img border=0 style='padding-top:7px;' src=../images/ViewDetails.png /></a><br>"
            ElseIf strType = "STM" Then ' Tele Marketing
                strData = "<!-- <hTitle>" & lblTeleMarketing & ": " & dr.Item("AlertDateTime").ToString & "</hTitle>-->"
                strData += "<!-- <AlertID>" & dr.Item("AlertID").ToString & "</AlertID>-->"
                strData += "<br>" & dr.Item("Note").ToString & "<br>"
            ElseIf strType = "ALE" Then ' lead is assigned to the user.
                strData = "<!-- <hTitle>" & lblAssignLeads & ": " & dr.Item("AlertDateTime").ToString & "</hTitle>-->"
                strData += "<!-- <AlertID>" & dr.Item("AlertID").ToString & "</AlertID>-->"
                strData += "<br>" & dr.Item("Note").ToString & "<br>"
            ElseIf strType = "JOB" Then ' Job Planning
                strData = "<!-- <hTitle>" & liJOBP & ": " & dr.Item("AlertDateTime").ToString & "</hTitle>-->"
                strData += "<!-- <AlertID>" & dr.Item("AlertID").ToString & "</AlertID>-->"
                strData += "<br>" & dr.Item("Note").ToString & "<br>"
            ElseIf strType = "SO" Then
                strData = "<!-- <hTitle>" & lblSOAlert & ": " & dr.Item("AlertDateTime").ToString & "</hTitle>-->"
                strData += "<!-- <AlertID>" & dr.Item("AlertID").ToString & "</AlertID>-->"
                strData += "<br>" & dr.Item("Note").ToString & "<br>"
                strData += "<a href='../Sales/Approval.aspx?SOID=" & dr.Item("InvID").ToString & "&Aprov=y'><img border=0 style='padding-top:7px;' src=../images/ViewDetails.png /></a><br>"
            ElseIf strType = "PO" Then
                strData = "<!-- <hTitle>" & lblPOAlert & ": " & dr.Item("AlertDateTime").ToString & "</hTitle>-->"
                strData += "<!-- <AlertID>" & dr.Item("AlertID").ToString & "</AlertID>-->"
                strData += "<br>" & dr.Item("Note").ToString & "<br>"
                strData += "<a href='../Procurement/Approval.aspx?POID=" & dr.Item("InvID").ToString & "&Arov=y'><img border=0 style='padding-top:7px;' src=../images/ViewDetails.png /></a><br>"
            ElseIf strType = "POA" Then
                strData = "<!-- <hTitle>" & lblPOAlert & ": " & dr.Item("AlertDateTime").ToString & "</hTitle>-->"
                strData += "<!-- <AlertID>" & dr.Item("AlertID").ToString & "</AlertID>-->"
                strData += "<br>" & dr.Item("Note").ToString & "<br>"
            ElseIf strType = "SOA" Then
                strData = "<!-- <hTitle>" & lblSOAlert & ": " & dr.Item("AlertDateTime").ToString & "</hTitle>-->"
                strData += "<!-- <AlertID>" & dr.Item("AlertID").ToString & "</AlertID>-->"
                strData += "<br>" & dr.Item("Note").ToString & "<br>"
            ElseIf strType = "CFL" Then
                strData = "<!-- <hTitle>" & alertTitleCMFollowup & ": " & dr.Item("AlertDateTime").ToString & "</hTitle>-->"
                strData += "<!-- <AlertID>" & dr.Item("AlertID").ToString & "</AlertID>-->"
                strData += "<br>" & dr.Item("Note").ToString & "<br>"
            End If
        End While

        dr.Close()
        objDB.CloseDatabaseConnection()

        If Convert.ToString(strData) <> "" Then
            Response.Write(strData)
        Else
            Response.Flush()
            Response.Close()
        End If
    End Sub
    Public Sub RemoveAlert(ByVal AlertID As String)
        Dim strSql As String = ""
        Dim objDB As New clsDataClass

        If Convert.ToString(AlertID) <> "" Then
            strSql = "Update alerts set AlertViewCount=1, AlertLastViewedOn=now() where AlertID=" & AlertID & " "
            objDB.SetData(strSql)
        End If
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
End Class

<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="ERPGraphs.aspx.vb" Inherits="Report_ERPGraphs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="Resources.resource" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divModuleAlert').corner();
        $('#divAlertContent').corner();
        $('#divMainContainerTitle').corner();
    </script>
    <div id="divModuleAlert" class="divSectionTitle">
        <h2>
            <%=Resources.Resource.lblSearchOptions%>
        </h2>
    </div>
    <div id="divAlertContent" class="divSectionContent">
    </div>
</asp:Content>

<asp:Content ID="cntRptParam" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle" Text="Graph Parameter Selection Page"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td align="right" style="width: 150px;" >                    
                </td>                
            </tr>
        </table>               
    </div>
    <div align="center">
        <table width="98%" class="table">           
            <tr id="Tr1" runat="server">
                <td class="titleBgColor" align="left" colspan="2">
                    <iframe runat="server" id="ifrmTGRW" height="635" width="100%" src="ERPGraph.html"
                        scrolling="no" frameborder="0"></iframe>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

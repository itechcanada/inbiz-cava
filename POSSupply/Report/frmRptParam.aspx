<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="frmRptParam.aspx.vb" Inherits="Report_frmRptParam" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="Resources.resource" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divModuleAlert').corner();
        $('#divAlertContent').corner();
        $('#divMainContainerTitle').corner();
    </script>
    <div id="divModuleAlert" class="divSectionTitle">
        <h2>
            <%=Resources.Resource.lblSearchOptions%>
        </h2>
    </div>
    <div id="divAlertContent" class="divSectionContent">
    </div>
</asp:Content>

<asp:Content ID="cntRptParam" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle" Text="<%$ Resources:Resource, lblRptTitle %>"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td align="right" style="width: 150px;" >                    
                </td>                
            </tr>
        </table>               
    </div>

    <div class="divMainContent">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr>
                <td class="tdAlign">
                    <asp:Label ID="lblSelReport" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSelReport %>"> </asp:Label>
                </td>
                <td class="tdAlignLeft">
                    <asp:DropDownList ID="dlReport" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, liSalesByProduct %>" Value="SalesPerProduct"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, liSalesBySalesperson %>" Value="SalesPerSalesperson"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, liSalesByWarehouse %>" Value="SalesPerWarehouse"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, liSalesByRegister %>" Value="SalesPerRegister"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:CustomValidator ID="custvalReport" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, msgRptPlzSelReport%>"
                        ClientValidationFunction="funCheckReport" Display="None"> </asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="tdAlign">
                    <asp:Label ID="lblFromDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblRptFromDate %>"> </asp:Label>
                </td>
                <td class="tdAlignLeft">
                    <asp:TextBox ID="txtFromDate" Enabled="true" runat="server" Width="90px" MaxLength="10"> </asp:TextBox>
                    <asp:ImageButton ID="imgCalFromDate" CausesValidation="false" runat="server" ToolTip="Click to show calendar"
                        ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender4" TargetControlID="txtFromDate"
                        runat="server" PopupButtonID="imgCalFromDate" Format="MM/dd/yyyy">
                    </ajaxToolkit:CalendarExtender>
                    <asp:RequiredFieldValidator ID="reqvalFromDate" runat="server" ControlToValidate="txtFromDate"
                        ErrorMessage="<%$ Resources:Resource, msgRptPlzEntFromDate %>" SetFocusOnError="true"
                        Display="None"> </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tdAlign">
                    <asp:Label ID="lblToDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblRptToDate %>"> </asp:Label>
                </td>
                <td class="tdAlignLeft">
                    <asp:TextBox ID="txtToDate" Enabled="true" runat="server" Width="90px" MaxLength="10"> </asp:TextBox>
                    <asp:ImageButton ID="imgCalToDate" CausesValidation="false" runat="server" ToolTip="Click to show calendar"
                        ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender5" TargetControlID="txtToDate"
                        runat="server" PopupButtonID="imgCalToDate" Format="MM/dd/yyyy">
                    </ajaxToolkit:CalendarExtender>
                    <asp:RequiredFieldValidator ID="reqvalToDate" runat="server" ControlToValidate="txtToDate"
                        ErrorMessage="<%$ Resources:Resource, msgRptPlzEntToDate %>" SetFocusOnError="true"
                        Display="None"> </asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="compvalDate" EnableClientScript="true" SetFocusOnError="true"
                        ControlToValidate="txtFromDate" ControlToCompare="txtToDate" Operator="LessThan"
                        Type="Date" Display="None" ErrorMessage="<%$ Resources:Resource, msgRptPlzEntFromDateShouldLessThanToDate %>"
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td class="tdAlign">
                    <%--<asp:Label ID="lblOutput" runat="server" CssClass="lblBold" 
            Text="<%$ Resources:Resource, lblRptOutput %>">
            </asp:Label>--%>
                </td>
                <td class="tdAlignLeft">
                    <asp:RadioButtonList ID="rblstRPT" runat="server" RepeatDirection="horizontal">
                        <asp:ListItem Text="<%$ Resources:Resource, liRptViewReport %>" Value="0" Selected="true">
                        </asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, liRptDownloadExcelReport %>" Value="1">
                        </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr height="30">
                <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                    <asp:Button runat="server" ID="cmdSubmit" CssClass="imgSubmit" />
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsReport" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
    <script language="javascript">
        function funCheckReport(source, args) {
            if (document.getElementById('<%=dlReport.ClientID%>').selectedIndex == -1) {
                args.IsValid = false;
            }
        }
    </script>
</asp:Content>

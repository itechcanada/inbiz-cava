<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="Default.aspx.vb" Inherits="Report_Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divModuleAlert').corner();
        $('#divAlertContent').corner();
        $('#divMainContainerTitle').corner();
    </script>
    <div id="divModuleAlert" class="divSectionTitle">
        <h2>
            <%=Resources.Resource.lblSearchOptions%>
        </h2>
    </div>
    <div id="divAlertContent" class="divSectionContent">
    </div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblHeading" Text="<%$ Resources:Resource, lblReports %>">></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td align="right" style="width: 150px;" >                    
                </td>                
            </tr>
        </table>               
    </div>
    <div class="divMainContent">
        <table border="0" cellpadding="0" cellspacing="0">           
            <tr>
                <td width="250px">
                </td>
                <td align="left">
                    <table id="tblContent" runat="server" cellspacing="5">
                    </table>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

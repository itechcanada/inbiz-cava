Imports System.Data.Odbc
Imports AjaxControlToolkit

Partial Class Report_Default
    Inherits BasePage

    Dim XMLContent As String
    Dim XMLFieldNode As String
    Dim Name As String
    Dim Caption As String
    Dim Type As String
    Dim Enabled As String
    Dim Required As String
    Dim DDLItems As String
    Dim DDLSQL As String
    Dim DefaultValue As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strXMLFile As String = Request.QueryString("XMLFile")
        If Convert.ToString(strXMLFile) = "" Then
            Response.Write("Please Provide Input XML File.")
            Exit Sub
        End If

        XMLContent = My.Computer.FileSystem.ReadAllText(Request.PhysicalApplicationPath & "/Report/XML/" & strXMLFile)

        Page.Title = clsXML.GetNodeText(XMLContent, "Report/Report_Name")

        XMLFieldNode = "Report/Input_Parameters/Input_Field"
        Dim FieldCount As Integer = clsXML.GetNodeCount(XMLContent, XMLFieldNode)

        AddReportTitle()

        For i As Integer = 0 To FieldCount - 1
            Name = GetNodeChild(i, "ID")
            Type = GetNodeChild(i, "Type")
            Caption = GetNodeChild(i, "Label")
            Enabled = GetNodeChild(i, "Enabled")
            Required = GetNodeChild(i, "Required")
            DDLItems = GetNodeChild(i, "Items")
            DDLSQL = GetNodeChild(i, "SQL")
            DefaultValue = GetNodeChild(i, "Default")

            GenerateInputControl()
        Next

        AddSubmitButton()
    End Sub

    Private Sub AddReportTitle()
        Dim tr As New HtmlTableRow
        Dim td As New HtmlTableCell

        tr.Cells.Add(td)

        td = New HtmlTableCell
        Dim lbl As New Label
        lbl.ID = "lblTitle"
        lbl.Text = clsXML.GetNodeText(XMLContent, "Report/Report_Name")
        lbl.Font.Size = 14
        lbl.ForeColor = Drawing.Color.Black
        td.Controls.Add(lbl)
        tr.Cells.Add(td)

        tblContent.Controls.Add(tr)
    End Sub

    Private Function GetNodeChild(ByVal Index As Integer, ByVal Child As String) As String
        Return clsXML.GetNodeChild(XMLContent, XMLFieldNode, Index, Child)
    End Function

    Private Sub GenerateInputControl()
        Dim tr As New HtmlTableRow
        Dim td As New HtmlTableCell

        Dim lbl As New Label
        lbl.ID = "lbl" & Name
        lbl.Text = Caption
        lbl.ForeColor = Drawing.Color.Black
        td.Controls.Add(lbl)
        tr.Cells.Add(td)

        td = New HtmlTableCell
        If Type = "Text" Then
            Dim ctl As New TextBox
            ctl.ID = Name

            If DefaultValue <> "" Then
                ctl.Text = DefaultValue
            End If

            If Enabled = "False" Then
                ctl.Enabled = False
            End If

            td.Controls.Add(ctl)
        ElseIf Type = "DDL" Then
            Dim ctl As New DropDownList
            ctl.ID = Name

            If DDLItems <> "" Then
                Dim arrItems() As String = DDLItems.Split(";")
                For j As Integer = 0 To arrItems.Length - 1
                    ctl.Items.Add(arrItems(j))
                Next
            End If

            If DDLSQL <> "" Then
                Dim objDB As New clsDataClass
                Dim dr As OdbcDataReader = objDB.GetDataReader(DDLSQL)
                ctl.DataSource = dr
                ctl.DataTextField = dr.GetName(0)
                ctl.DataValueField = dr.GetName(1)
                ctl.DataBind()
                dr.Close()
                objDB.CloseDatabaseConnection()
            End If

            td.Controls.Add(ctl)
        ElseIf Type = "Date" Then

            Dim lb As New Label
            lb.Text = "&nbsp;"

            Dim btn As New ImageButton
            btn.ID = "btn" & Name
            btn.ImageUrl = "~/images/calendar.gif"
            btn.ImageAlign = ImageAlign.AbsMiddle
            btn.CausesValidation = False

            ' Create a textbox to hold the date
            Dim dateValue As New TextBox()
            dateValue.ID = Name

            If DefaultValue <> "" Then
                dateValue.Text = Format(Date.Today.AddDays(DefaultValue), "yyyy-MM-dd")
            End If

            ' Create the calendar extender
            Dim ajaxCalendar As New AjaxControlToolkit.CalendarExtender()
            ajaxCalendar.ID = "cal" & Name
            ajaxCalendar.Format = "yyyy-MM-dd"
            ajaxCalendar.TargetControlID = dateValue.ID
            ajaxCalendar.Animated = True
            ajaxCalendar.TargetControlID = Name
            ajaxCalendar.PopupButtonID = "btn" & Name

            Dim compareValidator As New CompareValidator()
            compareValidator.ID = "Val" & Name
            compareValidator.ControlToValidate = Name
            compareValidator.Operator = ValidationCompareOperator.DataTypeCheck
            compareValidator.Type = ValidationDataType.Date
            compareValidator.ErrorMessage = " Invalid Date."

            td.Controls.Add(dateValue)
            td.Controls.Add(lb)
            td.Controls.Add(btn)
            td.Controls.Add(ajaxCalendar)
            td.Controls.Add(compareValidator)
            'ElseIf Type = "Date" Then
            '    Dim ctl As New TextBox
            '    ctl.ID = Name
            '    ctl.ReadOnly = True

            '    If DefaultValue <> "" Then
            '        ctl.Text = Format(Date.Today.AddDays(DefaultValue), "yyyy-MM-dd")
            '    End If

            '    Dim btn As New ImageButton
            '    btn.ID = "btn" & Name
            '    btn.ImageAlign = ImageAlign.Top
            '    btn.ImageUrl = "~/images/calendar.gif"
            '    btn.CausesValidation = False
            '    AddHandler btn.Click, AddressOf Me.btn_Click

            '    Dim cal As New Calendar
            '    cal.ID = "cal" & Name
            '    cal.Visible = False
            '    cal.Font.Size = FontSize.Medium
            '    cal.Style.Add("z-index", "1")
            '    cal.Style.Add("position", "absolute")
            '    cal.TodaysDate = Date.Today
            '    cal.BackColor = Drawing.Color.AliceBlue
            '    AddHandler cal.SelectionChanged, AddressOf Me.cal_SelectionChanged

            '    If Enabled = "False" Then
            '        ctl.Enabled = False
            '        btn.Enabled = False
            '    End If

            '    td.Controls.Add(ctl)
            '    td.Controls.Add(btn)
            '    td.Controls.Add(cal)
        ElseIf Type = "Check" Then
            Dim ctl As New CheckBox
            ctl.ID = Name

            If Enabled = "False" Then
                ctl.Enabled = False
            End If

            td.Controls.Add(ctl)
        End If

        If Required = "True" Then
            Dim req As New RequiredFieldValidator
            req.ID = "req" & Name
            req.ErrorMessage = "*"
            req.ControlToValidate = Name

            td.Controls.Add(req)
        End If

        tr.Cells.Add(td)

        tblContent.Controls.Add(tr)
    End Sub

    Private Sub AddSubmitButton()
        Dim tr As New HtmlTableRow
        Dim td As New HtmlTableCell
        tr.Cells.Add(td)

        td = New HtmlTableCell
        Dim ctl As New Button
        ctl.ID = "cmdSubmit"
        ctl.Text = "Submit"
        AddHandler ctl.Click, AddressOf Me.cmdSubmit_Click
        td.Controls.Add(ctl)
        tr.Cells.Add(td)

        tblContent.Controls.Add(tr)
    End Sub

    Protected Sub cmdSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strQuery As String = GetControlValues(tblContent)

        Response.Redirect("Report.aspx?XMLFile=" & Request.QueryString("XMLFile") & "&" & strQuery)
    End Sub

    Private Function GetControlValues(ByVal Container As Control) As String
        Dim strQuery As String = ""
        For Each ctl As Control In Container.Controls
            If TypeOf ctl Is TextBox Then
                strQuery += ctl.ID & "=" & CType(ctl, TextBox).Text & "&"
            End If
            If TypeOf ctl Is DropDownList Then
                strQuery += ctl.ID & "=" & CType(ctl, DropDownList).SelectedValue & "&"
            End If
            If TypeOf ctl Is CheckBox Then
                strQuery += ctl.ID & "=" & IIf(CType(ctl, CheckBox).Checked, 1, 0) & "&"
            End If
            strQuery += GetControlValues(ctl)
        Next
        Return strQuery
    End Function

    Protected Sub btn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim cal As Calendar = tblContent.FindControl(CType(sender, ImageButton).ID.Replace("btn", "cal"))
        Dim txt As TextBox = tblContent.FindControl(CType(sender, ImageButton).ID.Replace("btn", ""))

        If cal.Visible = False Then
            cal.Visible = True
            If txt.Text <> "" Then
                cal.TodaysDate = Date.Parse(txt.Text)
                cal.SelectedDate = cal.TodaysDate
            End If
        Else
            cal.Visible = False
        End If
    End Sub

    Protected Sub cal_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim cal As Calendar = tblContent.FindControl(CType(sender, Calendar).ID)
        Dim txt As TextBox = tblContent.FindControl(CType(sender, Calendar).ID.Replace("cal", ""))

        txt.Text = Format(cal.SelectedDate, "yyyy-MM-dd")
        cal.Visible = False
    End Sub
End Class

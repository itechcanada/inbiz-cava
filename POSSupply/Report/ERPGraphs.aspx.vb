Imports System
Imports System.IO
Imports System.Net
Imports System.Data
Imports System.Drawing
Imports Resources.Resource
Partial Class Report_ERPGraphs
    Inherits BasePage
    Dim objUser As New clsUser

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Default.aspx")
        End If
        Dim strUserID As String
        strUserID = Session("UserID")
        Dim strToken As String = funGetToken(strUserID)

        If Not IsPostBack Then
            Dim strFlashParam As String = ""
            strFlashParam = "UserId=" + strUserID + "&token=" + strToken + "&WsWsdl=" + ConfigurationManager.AppSettings("ERPServiceUrl")
            ifrmTGRW.Attributes.Add("src", "ERPGraph.html?" & strFlashParam)
        End If
    End Sub

    Public Function funGetToken(ByVal strUserID As String) As String
        Dim strToken As String
        Dim strSQL As String
        Dim objDC As New clsDataClass
        strToken = Date.Now.Ticks & funRandomString()
        strSQL = "Insert into token (tokenId, tokenUser, tokenTime, tokenStatus) values ('" & strToken & "', '" & strUserID & "', now(), '1')"
        objDC.PopulateData(strSQL)
        Dim strData As String
        strSQL = "select tokenNo from token where tokenID='" & strToken & "' and tokenUser='" & strUserID & "' and tokenStatus='1' and adddate(tokenTime,INTERVAL 5 Minute) > now()"
        strData = objDC.GetScalarData(strSQL)
        strToken = strData & "-" & strToken
        strSQL = "Update token set tokenId='" & strToken & "' where tokenNo='" & strData & "' "
        objDC.PopulateData(strSQL)
        objDC.CloseDatabaseConnection()
        objDC = Nothing
        Return strToken
    End Function

    'Generates random string
    Public Shared Function funRandomString() As String
        Dim rand As Random = New Random
        Dim strResult As String = ""
        Dim i As Int32
        For i = 0 To 15
            strResult = strResult & Convert.ToChar((rand.Next(1, 26) + 64))
        Next i
        Return strResult
    End Function
End Class

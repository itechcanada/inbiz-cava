Imports System.Data
Imports System.Threading
Imports System.Globalization

Partial Class Report
    Inherits BasePage

    Dim XMLContent As String
    Dim HTMLContent As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strXMLFile As String = Request.QueryString("XMLFile")
        XMLContent = My.Computer.FileSystem.ReadAllText(Request.PhysicalApplicationPath & "/Report/XML/" & strXMLFile)

        Dim strHTMLFile As String = clsXML.GetNodeText(XMLContent, "Report/Template_File")
        HTMLContent = My.Computer.FileSystem.ReadAllText(Request.PhysicalApplicationPath & "/Report/Template/" & strHTMLFile)

        Dim strSQL As String = clsXML.GetNodeText(XMLContent, "Report/Report_SQL")

        Dim InputCount As Integer = Request.QueryString.Count - 1
        For Input As Integer = 1 To InputCount - 1
            strSQL = strSQL.Replace(Request.QueryString.Keys(Input), Request.QueryString.Item(Input))
        Next

        Dim objDB As New clsDataClass
        Dim dt As DataTable = objDB.GetDataTable(strSQL)

        Page.Title = clsXML.GetNodeText(XMLContent, "Report/Report_Name")

        Dim strGroup As String = clsXML.GetNodeText(XMLContent, "Report/Group_By")
        Dim strSubTotal As String = clsXML.GetNodeText(XMLContent, "Report/Sub_Total")
        Dim strTotal As String = clsXML.GetNodeText(XMLContent, "Report/Total")

        Dim GroupPos() As String = IIf(strGroup <> "", strGroup.Split(";"), Nothing)
        Dim SubTotalPos() As String = IIf(strSubTotal <> "", strSubTotal.Split(";"), Nothing)
        Dim TotalPos() As String = IIf(strTotal <> "", strTotal.Split(";"), Nothing)

        Dim GroupValue() As String
        If Not GroupPos Is Nothing Then
            ReDim GroupValue(GroupPos.Length - 1)
        End If

        Dim SubTotalValue() As Decimal
        If Not SubTotalPos Is Nothing Then
            ReDim SubTotalValue(SubTotalPos.Length - 1)
        End If

        Dim TotalValue() As Decimal
        If Not TotalPos Is Nothing Then
            ReDim TotalValue(TotalPos.Length - 1)
        End If

        Dim RowCount As Integer = dt.Rows.Count
        Dim ColumnCount As Integer = dt.Columns.Count

        Dim RowData(ColumnCount - 1) As String

        For Column As Integer = 0 To ColumnCount - 1
            RowData(Column) = dt.Columns(Column).Caption
        Next
        SetHTMLData("ITCol_Hdg", RowData)

        Dim m As Integer
        Dim p As Integer
        Dim q As Integer

        For i As Integer = 0 To RowCount - 1
            m = 0
            p = 0
            q = 0
            For j As Integer = 0 To ColumnCount - 1
                If m < GroupPos.Length Then
                    If j = GroupPos(m) - 1 Then
                        If Convert.ToString(GroupValue(m)) <> Convert.ToString(dt.Rows(i).Item(j)) Then
                            RowData(j) = dt.Rows(i).Item(j)
                            GroupValue(m) = dt.Rows(i).Item(j)
                            ClearArray(GroupValue, m + 1)
                            If m = GroupPos.Length - 1 And i > 0 Then
                                If Not SubTotalPos Is Nothing Then
                                    AddSubTotal(ColumnCount, SubTotalPos, SubTotalValue, "Sub Total")
                                    ClearArray(SubTotalValue, 0)
                                End If
                            End If
                        End If
                        m += 1
                    Else
                        RowData(j) = dt.Rows(i).Item(j)
                    End If
                Else
                    RowData(j) = dt.Rows(i).Item(j)
                End If

                If Not SubTotalPos Is Nothing Then
                    If p < SubTotalPos.Length Then
                        If j = SubTotalPos(p) - 1 Then
                            SubTotalValue(p) += dt.Rows(i).Item(j)
                            p += 1
                        End If
                    End If
                End If

                If Not TotalPos Is Nothing Then
                    If q < TotalPos.Length Then
                        If j = TotalPos(q) - 1 Then
                            TotalValue(q) += dt.Rows(i).Item(j)
                            q += 1
                        End If
                    End If
                End If

            Next
            SetHTMLData("IT_ColDetail", RowData)

            ClearArray(RowData, 0)
        Next

        If RowCount <> 0 Then
            If Not SubTotalPos Is Nothing Then
                AddSubTotal(ColumnCount, SubTotalPos, SubTotalValue, "Sub Total")
            End If

            If Not TotalPos Is Nothing Then
                AddSubTotal(ColumnCount, TotalPos, TotalValue, "Total")
            End If
        Else
            divContent.InnerHtml += "<br><div>No Data Found.<div>"
        End If
    End Sub

    Private Sub AddSubTotal(ByVal Count As Int16, ByVal Position() As String, ByVal SubTotal() As Decimal, ByVal Type As String)
        Dim RowData(Count - 1) As String

        Dim m As Integer = Position.Length
        Dim n As Integer = 0

        If m > 0 Then
            For i As Integer = 0 To Count - 1
                If n < m Then
                    If i = Position(n) - 1 Then
                        RowData(i) = SubTotal(n)
                        n += 1
                    End If
                End If
            Next

            RowData(0) = Type
            SetHTMLData("IT_ColSubTotal", RowData, True)
        End If
    End Sub

    Private Sub ClearArray(ByRef Array() As String, ByVal Start As Integer)
        For i As Integer = Start To Array.Length - 1
            Array(i) = ""
        Next
    End Sub

    Private Sub ClearArray(ByRef Array() As Decimal, ByVal Start As Integer)
        For i As Integer = Start To Array.Length - 1
            Array(i) = 0
        Next
    End Sub

    Private Sub SetHTMLData(ByVal Group As String, ByVal RowData() As String, Optional ByVal Format As Boolean = False)
        Dim intGroupPosition As Integer = HTMLContent.IndexOf(Group)

        Dim intStart As Integer = HTMLContent.Substring(0, intGroupPosition).LastIndexOf("<div")
        Dim intEnd As Integer = HTMLContent.Substring(intGroupPosition, HTMLContent.Length - intGroupPosition - 1).IndexOf("</div") + 6 + intGroupPosition

        Dim HTMLData As String = HTMLContent.Substring(intStart, intEnd - intStart)

        For i As Integer = 0 To RowData.Length - 1
            If Format And IsNumeric(RowData(i)) Then
                HTMLData = Replace(HTMLData, "@field" & i + 1, SetFormat(RowData(i)))
            Else
                HTMLData = Replace(HTMLData, "@field" & i + 1, RowData(i))
            End If
        Next

        divContent.InnerHtml += HTMLData
    End Sub

    Private Function SetFormat(ByVal Number As Decimal) As String
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        Return Number.ToString("n", New CultureInfo(Session("Language").ToString))
    End Function
End Class

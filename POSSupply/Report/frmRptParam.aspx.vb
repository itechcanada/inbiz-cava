Imports System
Imports System.IO
Imports Resources.Resource
Partial Class Report_frmRptParam
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            cmdSubmit.Attributes.Add("onclick", "funBlank()")
        End If
    End Sub
    Protected Sub cmdSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSubmit.Click
        If txtFromDate.Text = "" Then
            lblMsg.Text = msgRptPlzEntFromDate
            Exit Sub
        End If
        If txtToDate.Text = "" Then
            lblMsg.Text = msgRptPlzEntToDate
            Exit Sub
        End If
        If txtFromDate.Text <> "" And txtToDate.Text <> "" Then
            If DateTime.ParseExact(txtFromDate.Text, "MM/dd/yyyy", Nothing) > DateTime.ParseExact(txtToDate.Text, "MM/dd/yyyy", Nothing) Then
                lblMsg.Text = msgRptPlzEntFromDateShouldLessThanToDate
                Exit Sub
            End If
        End If
        Dim strInpFile As String = ""
        Dim strExecuteParam As String = ""
        strInpFile = dlReport.SelectedValue
        Dim strPath As String = ""
        strInpFile = strInpFile & "_" & Now.ToString("yyyyMMddHHmmss") & ".inp"
        Select Case dlReport.SelectedValue
            Case "SalesPerProduct"
                strPath = Server.MapPath("~") & "\Report\SalesPerProduct\INP\" & strInpFile
            Case "SalesPerSalesperson"
                strPath = Server.MapPath("~") & "\Report\SalesPerSalesPerson\INP\" & strInpFile
            Case "SalesPerWarehouse"
                strPath = Server.MapPath("~") & "\Report\SalesPerWarehouse\INP\" & strInpFile
            Case "SalesPerRegister"
                strPath = Server.MapPath("~") & "\Report\SalesPerRegister\INP\" & strInpFile
            Case Else
                lblMsg.Text = msgRptPlzSelReport
        End Select

        Dim sw As StreamWriter = New StreamWriter(strPath)
        sw.WriteLine(DateTime.ParseExact(txtFromDate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd"))
        sw.WriteLine(DateTime.ParseExact(txtToDate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd"))
        sw.Close()
        sw.Dispose()
        sw = Nothing
        If strInpFile <> "" Then
            strExecuteParam = strInpFile.Substring(0, strInpFile.IndexOf("_"))
        Else
            lblMsg.Text = msgRptPlzSelReport
            Exit Sub
        End If
        Try
            Select Case strExecuteParam
                Case "SalesPerProduct"
                    Response.Redirect("SalesPerProduct/frmSalesPerProduct.aspx?inpFL=" & strInpFile & "&CVS=" & rblstRPT.SelectedValue)
                Case "SalesPerSalesperson"
                    Response.Redirect("SalesPerSalesPerson/frmSalesPerSalesPerson.aspx?inpFL=" & strInpFile & "&CVS=" & rblstRPT.SelectedValue)
                Case "SalesPerWarehouse"
                    Response.Redirect("SalesPerWarehouse/frmSalesPerWarehouse.aspx?inpFL=" & strInpFile & "&CVS=" & rblstRPT.SelectedValue)
                Case "SalesPerRegister"
                    Response.Redirect("SalesPerRegister/frmSalesPerRegister.aspx?inpFL=" & strInpFile & "&CVS=" & rblstRPT.SelectedValue)
                Case Else
                    lblMsg.Text = msgRptPlzSelReport
            End Select
        Catch ex As Exception
        Finally

        End Try
    End Sub
End Class

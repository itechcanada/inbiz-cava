﻿Imports System.Data

Partial Class Report_GridReport
    Inherits BasePage

    Dim XMLContent As String
    Dim HTMLContent As String

    Dim gvDataSource As DataTable
    Dim testDataSource As DataTable

    Dim aListGroupPos As New ArrayList
    Dim aListSubTotalPos As New ArrayList
    Dim aListTotalPos As New ArrayList
    Dim aListColumnHead As New ArrayList
    Dim aLisNumericColoumnList As New ArrayList
    Dim colCount As Integer

    Dim gFlag As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FillReportData()
    End Sub

    Dim hasPreviousRow As Boolean = False

    Protected Sub gvReport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReport.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            If colCount = aListColumnHead.Count Then
                For index As Integer = 0 To aListColumnHead.Count - 1
                    e.Row.Cells(index).Text = aListColumnHead(index)
                    e.Row.Cells(index).Style(HtmlTextWriterStyle.Color) = "#fff"

                    Dim testVal As Integer = index + 1
                    If aLisNumericColoumnList.Contains(testVal.ToString()) Then
                        e.Row.Cells(index).Style(HtmlTextWriterStyle.TextAlign) = "right"
                    End If
                Next
            End If
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            For index As Integer = 0 To colCount - 1
                Dim testVal As Integer = index + 1
                If aLisNumericColoumnList.Contains(testVal.ToString()) Then
                    e.Row.Cells(index).Style(HtmlTextWriterStyle.TextAlign) = "right"
                End If
            Next
        End If
    End Sub

    Private Sub FillReportData()
        Dim strXMLFile As String = Request.QueryString("XMLFile")
        XMLContent = My.Computer.FileSystem.ReadAllText(Request.PhysicalApplicationPath & "/Report/XML/" & strXMLFile)

        Dim strHTMLFile As String = clsXML.GetNodeText(XMLContent, "Report/Template_File")
        HTMLContent = My.Computer.FileSystem.ReadAllText(Request.PhysicalApplicationPath & "/Report/Template/" & strHTMLFile)

        Dim strSQL As String = clsXML.GetNodeText(XMLContent, "Report/Report_SQL")

        Dim InputCount As Integer = Request.QueryString.Count - 1
        For Input As Integer = 1 To InputCount - 1
            strSQL = strSQL.Replace(Request.QueryString.Keys(Input), Request.QueryString.Item(Input))
        Next

        Dim objDB As New clsDataClass

        gvDataSource = objDB.GetDataTable(strSQL)

        Page.Title = clsXML.GetNodeText(XMLContent, "Report/Report_Name")

        Dim strGroup As String = clsXML.GetNodeText(XMLContent, "Report/Group_By")
        Dim strSubTotal As String = clsXML.GetNodeText(XMLContent, "Report/Sub_Total")
        Dim strTotal As String = clsXML.GetNodeText(XMLContent, "Report/Total")
        Dim strColHeadings As String = clsXML.GetNodeText(XMLContent, "Report/Column_Headings")
        Dim strNumericCols As String = clsXML.GetNodeText(XMLContent, "Report/NumericColumns")

        Dim arrGroupPos() As String = IIf(strGroup <> "", strGroup.Split(";"), Nothing)
        Dim arrSubTotalPos() As String = IIf(strSubTotal <> "", strSubTotal.Split(";"), Nothing)
        Dim arrTotalPos() As String = IIf(strTotal <> "", strTotal.Split(";"), Nothing)
        Dim arrColumnHeadings() As String = IIf(strColHeadings <> "", strColHeadings.Split(";"), Nothing)
        Dim arrNumericCols() As String = IIf(strNumericCols <> "", strNumericCols.Split(";"), Nothing)

        aListColumnHead.AddRange(arrColumnHeadings)
        aListGroupPos.AddRange(arrGroupPos)
        aListSubTotalPos.AddRange(arrSubTotalPos)
        aListTotalPos.AddRange(arrTotalPos)
        aLisNumericColoumnList.AddRange(arrNumericCols)

        BuildTestDataSource()

        colCount = gvDataSource.Columns.Count

        gvReport.DataSource = gvDataSource
        gvReport.DataBind()
    End Sub

    Private Sub BuildTestDataSource()

    End Sub

End Class

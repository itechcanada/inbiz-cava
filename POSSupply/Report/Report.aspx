<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="Report.aspx.vb" Inherits="Report" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divModuleAlert').corner();
        $('#divAlertContent').corner();
        $('#divMainContainerTitle').corner();
    </script>
    <div id="divModuleAlert" class="divSectionTitle">
        <h2>
            Seracrch Criteria
        </h2>
    </div>
    <div id="divAlertContent" class="divSectionContent">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    
                </td>
                <td>
                    
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        Report
                    </h2>
                </td>
                <td style="text-align: right;">
                </td>
                <td align="right" style="width: 150px;">
                </td>
            </tr>
        </table>
    </div>
    <div class="divMainContent">
        <div id="divContent" runat="server" align="center">
        </div>
    </div>
</asp:Content>

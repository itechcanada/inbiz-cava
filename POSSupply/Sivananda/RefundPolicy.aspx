﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RefundPolicy.aspx.cs" Inherits="Sivananda_RefundPolicy" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <%if (iTECH.InbizERP.BusinessLogic.Globals.CurrentAppLanguageCode == iTECH.InbizERP.BusinessLogic.AppLanguageCode.FR)
          {
        %>
        <b><span class="style1">CONDITIONS DE REMBOURSEMENT</span> </b>
        <ul>
            <li>
                <p>
                    <b>Remboursement, annulation ou modification de votre réservation:</b> 50 $ frais
                    d'administration .
                </p>
            </li>
            <li>
                <p>
                    <b>Annulation ou modification de moins de 72 heures avant la date d'arrivée:</b>Des
                    frais d'administration de 50 $ plus frais d'hébergement d'une nuit.
                </p>
            </li>
            <li>
                <p>
                    <b>Non présentation: </b>Aucun remboursement.
                </p>
            </li>
        </ul>

        
        <%
            }
          else
          {
          
        %>
        <b><span class="style1">REFUND POLICY</span></b>
        <ul>
            <li>
                <p>
                    <b>Refund, cancellation or changes to your reservation: </b>50$ administration fee
                    .
                </p>
            </li>
            <li>
                <p>
                    <b>Cancellation or changes less than 72 hours prior to arrival date:</b>50$ administration
                    fee plus 1 night accommodation fee.
                </p>
            </li>
            <li>
                <p>
                    <b>No show:</b> No refunds.
                </p>
            </li>
        </ul>
        
        <%
            }
        %>
    </div>
    </form>
</body>
</html>

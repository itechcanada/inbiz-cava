﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Sivananda_Cancel : PayPalBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {      
        Response.Redirect("Default.aspx?lang=" + Globals.CurrentAppLanguageCode);
    }

    private int OrderID
    {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["oid"], out id);
            return id;
        }
    }
}
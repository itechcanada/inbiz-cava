﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;

public partial class Sivananda_sivananda : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lnkClose.OnClientClick = string.Format("location.href = '{0}'; return false;", System.Configuration.ConfigurationManager.AppSettings["SivanandaWebUrl"]);
        }
    }

    protected void btnLogOut_Click(object sender, EventArgs e)
    {        
        Session.Abandon();
        Response.Redirect("~/AdminLogin.aspx?lang=logout");
    }

    protected void changeLanguage_Command(object sender, CommandEventArgs e)
    {
        Globals.SetCultureInfo(e.CommandArgument.ToString());
        Response.Redirect(Request.RawUrl);
    }
}

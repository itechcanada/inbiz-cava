﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WelcomeDiv.ascx.cs" Inherits="Sivananda_Controls_WelcomeDiv" %>
<div id="welcomeDiv" style="padding: 5px; text-align: right; font-weight: bold; margin-bottom: 5px;"
    runat="server">
    <%=Resources.Resource.lblWelcome %>,
    <asp:Literal ID="ltCurrentGuestName" Text="" runat="server" />
     [ <asp:LinkButton ID="lnkLogOut" CausesValidation="false" 
        Text="<%$Resources:Resource, Logout %>" runat="server" ForeColor="#FF7300" 
        onclick="lnkLogOut_Click" /> ]
</div>

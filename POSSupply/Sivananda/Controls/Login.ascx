﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Login.ascx.cs" Inherits="Sivananda_Controls_Login" %>
<table border="0" cellpadding="5" cellspacing="2" width="100%">
    <tr>
        <td style="width: 50%; background-color: #FFECB5; border: 1px solid #E5C365;">
            <asp:Label ID="Label8" Font-Bold="true" runat="server" Text="<%$Resources:Resource, lblLoginToRetreive %>">
            </asp:Label>
        </td>
        <td></td>
        <td style="width: 50%; background-color: #FFECB5; border: 1px solid #E5C365;">
            <asp:Label ID="Label9" Font-Bold="true" runat="server" Text="<%$Resources:Resource, lblRegister %>">
            </asp:Label>            
        </td>
    </tr>
    <tr>
        <td valign="top" style="border: 1px solid #E5C365;">
            <asp:Panel ID="pnlLogin" runat="server" DefaultButton="btnLogin">
                <ul class="form">
                    <li>
                        <div class="lbl">                            
                            <asp:Label ID="lblEmailID" Text="<%$Resources:Resource, lblEmailID %>" runat="server" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtEmailAddress" runat="server" TabIndex="0" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtEmailAddress"
                                SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator3Resource1"
                                ValidationGroup="login" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revalSecEmail" runat="server" ControlToValidate="txtEmailAddress"
                                ErrorMessage="<%$ Resources:Resource, revalCMSecEmail %>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                meta:resourcekey="revalSecEmailResource1" ValidationGroup="login" 
                                Display="Dynamic"></asp:RegularExpressionValidator>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">                            
                            <asp:Label ID="Label1" Text="<%$Resources:Resource, lblPassword%>" runat="server" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtPassword"
                                SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator3Resource1"
                                ValidationGroup="login" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                        </div>
                        <div class="input">                            
                            <asp:Button ID="btnLogin" Text="<%$Resources:Resource,lbllogin%>" runat="server" ValidationGroup="login" 
                                onclick="btnLogin_Click" />
                            <div>
                                <asp:HyperLink ID="hlForgotPassword" NavigateUrl="javascript:;" Text="<%$Resources:Resource, lblForgottenPassword %>"
                                    runat="server" /></div>

                            <div id="divForgotPassword" title="Forgot Password">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td colspan="2">
                                            <div class="error_theme">
                                                <div class="ui-widget" style="margin-top: 5px;">
                                                    <div style="padding: 0 .7em;" class="ui-state-highlight ui-corner-all">
                                                        <p>
                                                            <span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-info"></span>
                                                            <strong></strong>
                                                            <%=Resources.Resource.infoPasswordWillBeSendToyourEmail%>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px;">                                            
                                            <asp:Label ID="lblFEmailID" Text="<%$Resources:Resource, lblEmailID%>" runat="server" />
                                        </td>
                                        <td style="padding:5px;"> 
                                            <asp:TextBox ID="txtFEmail" runat="server" />
                                            <asp:Button ID="btnResetPassword" 
                                                Text="<%$Resources:Resource, lblResetPassword%>"  ValidationGroup="fpass" 
                                                runat="server" onclick="btnResetPassword_Click" />                                           
                                        </td>                                       
                                    </tr>
                                    <tr>                                        
                                        <td colspan="2">                                            
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ControlToValidate="txtFEmail"
                                                SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator3Resource1"
                                                ValidationGroup="fpass" Display="Dynamic"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtFEmail"
                                                ErrorMessage="<%$ Resources:Resource, revalCMSecEmail %>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                meta:resourcekey="revalSecEmailResource1" ValidationGroup="fpass" Display="Dynamic"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>                                
                            </div>
                            <script type="text/javascript">
                                $("#divForgotPassword").dialog({
                                    modal: true,
                                    autoOpen: false,
                                    resizable: false,
                                    width: 500,
                                    height: 150
                                }).parent().appendTo(jQuery("form:first"));

                                $("#<%=hlForgotPassword.ClientID %>").click(function () {
                                    if (Page_IsValid) {
                                        $("#divForgotPassword").dialog("open");
                                    }
                                    return false;
                                });  
                            </script>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                </ul>
            </asp:Panel>
        </td>
        <td valign="middle">
            <b>
                <asp:Literal ID="ltOR" Text="<%$Resources:Resource, lblOR %>" runat="server" /></b>
        </td>
        <td valign="top" style="border: 1px solid #E5C365;">
            <asp:Panel ID="pnlRegistraion" runat="server" DefaultButton="btnRegister">
                <ul class="form">
                    <li>
                        <div class="lbl">                            
                            <asp:Label ID="Label5" Text="<%$Resources:Resource,lblFName%>" runat="server" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtFirstName" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtFirstName"
                                SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator3Resource1"
                                ValidationGroup="regis" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">                            
                            <asp:Label ID="Label6" Text="<%$Resources:Resource, lblLName%>" runat="server" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtLastName" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtLastName"
                                SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator3Resource1"
                                ValidationGroup="regis" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">                            
                            <asp:Label ID="Label7" Text="<%$Resources:Resource, lblPhone%>" runat="server" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtPhone" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txtPhone"
                                SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator3Resource1"
                                ValidationGroup="regis" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">                            
                            <asp:Label ID="Label2" Text="<%$Resources:Resource, lblEmailID%>" runat="server" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtEmailIDReg" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtEmailIDReg"
                                SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator3Resource1"
                                ValidationGroup="regis" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmailIDReg"
                                ErrorMessage="<%$ Resources:Resource, revalCMSecEmail %>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ValidationGroup="regis" Display="Dynamic"></asp:RegularExpressionValidator>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">                            
                            <asp:Label ID="Label3" Text="<%$Resources:Resource,lblPassword %>" runat="server" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtPasswordReg" runat="server" TextMode="Password" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="txtPasswordReg"
                                SetFocusOnError="True" runat="server" ErrorMessage="*" 
                                ValidationGroup="regis" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">                            
                            <asp:Label ID="Label4" Text="<%$Resources:Resource, lblConfirmPassword %>" runat="server" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtConfirmPasswordReg" runat="server" TextMode="Password" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="txtConfirmPasswordReg"
                                SetFocusOnError="True" runat="server" ErrorMessage="*" 
                                ValidationGroup="regis" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" ControlToValidate="txtConfirmPasswordReg"
                                ControlToCompare="txtPasswordReg" Operator="Equal" runat="server" ErrorMessage="*"
                                SetFocusOnError="True" Display="Dynamic" ValidationGroup="regis"></asp:CompareValidator>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                        </div>
                        <div class="input">                            
                            <asp:Button ID="btnRegister" Text="<%$Resources:Resource, lblRegister %>" runat="server" 
                                ValidationGroup="regis" onclick="btnRegister_Click" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                </ul>
            </asp:Panel>
        </td>
    </tr>
</table>

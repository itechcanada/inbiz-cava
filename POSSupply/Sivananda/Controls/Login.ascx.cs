﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Sivananda_Controls_Login : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Visible = !SivanandaSession.IsAuthenticated;
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Partners part = new Partners();
            int partnerID = part.ValidatePartner(txtEmailAddress.Text, txtPassword.Text);
            
            if (partnerID > 0)
            {
                part.PopulateObject(partnerID);
                SivanandaSession.SetSession(part);
                Response.Redirect(Request.RawUrl);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidEmailIDorPassword);
            }
        }
    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                //Retreive Whs Info
                SysWarehouses whs = new SysWarehouses();
                whs.PopulateObject(ConfigurationManager.AppSettings["WebSaleWhsCode"]);

                SysCompanyInfo ci = new SysCompanyInfo();
                ci.PopulateObject(whs.WarehouseCompanyID); 

                Partners part = new Partners();
                part.PartnerActive = true;
                part.ExtendedProperties.GuestType = (int)StatusGuestType.Guest;
                part.ExtendedProperties.FirstName = txtFirstName.Text;
                part.ExtendedProperties.LastName = txtLastName.Text;
                part.PartnerEmail = txtEmailIDReg.Text;
                part.PartnerLongName = string.Format("{0} {1}", txtLastName.Text, txtFirstName.Text);
                part.PartnerTaxCode = whs.WarehouseRegTaxCode;
                part.PartnerType = (int)PartnerTypeIDs.EndClient;
                part.PartnerCurrencyCode = ci.CompanyBasCur;
                part.PartnerPhone = txtPhone.Text;

                part.RegisterGuest(txtPasswordReg.Text);

                int partnerID = part.ValidatePartner(txtEmailIDReg.Text, txtPasswordReg.Text);
                if (partnerID > 0)
                {
                    part.PopulateObject(partnerID);
                    SivanandaSession.SetSession(part);
                }

                Response.Redirect(Request.RawUrl, false);
            }
            catch(Exception ex)
            {
                if (ex.Message == CustomExceptionCodes.DUPLICATE_KEY || ex.Message == "PARTNER_ALREADY_EXISTS")
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgGuestAlreadyExists);
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
                }
            }                                 
        }
    }
    protected void btnResetPassword_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Partners part = new Partners();
            try
            {
                string newPass = part.RestPassword(txtFEmail.Text);
                if (!string.IsNullOrEmpty(newPass))
                {
                    //To do here to email password to customer
                    string msg = string.Format("Dear Guest, <p>" + "Your Login Credentials are as follows:<br>EmailID={0}<br>Password={1}<p> Thank you <br>Sivananda", txtFEmail.Text, newPass);
                    EmailHelper.SendEmail(ConfigurationManager.AppSettings["NoReplyEmail"], txtFEmail.Text, msg, "Sivananda-Account Password Reset", true);
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgPasswordHasBeenSent);
                }
                //MessageState.SetGlobalMessage(MessageType.Failure, "Password could not be reset!");
            }
            catch(Exception ex)
            {
                if (ex.Message == CustomExceptionCodes.INVALID_EMAIL_ID)
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgProvidedEmailIDDoesNotExists);
                }
            }
        }
    }
}
﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Sivananda_Controls_AddServices : System.Web.UI.UserControl
{
    AccommodationSearch _srch = new AccommodationSearch();
    List<SearchResult> _srchResult;
    int _visibleServiceCount = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        pnlScript.Visible = false;
        _srchResult = _srch.SearchedData;
        if (_srchResult.Count <= 0)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPleaseSearchAccommodation);
            Response.Redirect("Default.aspx?lang=" + Globals.CurrentAppLanguageCode);
        }

        if (!IsPostBack)
        {
            FillFormData();
        }
    }

    private void FillFormData()
    {
        SysProcessGroup pr = new SysProcessGroup();
        //chkServices.DataSource = pr.GetProcesses("$");
        //chkServices.DataValueField = "Key";
        //chkServices.DataTextField = "Value";
        //chkServices.DataBind();

        rptServicesToAdd.DataSource = pr.GetServicesForWeb("<span style='font-weight:bold; font-size:14px;'>$</span>");
        rptServicesToAdd.DataBind();

        trServiceNotAvailable.Visible = _visibleServiceCount <= 0;

        double total = 0.0D;
        foreach (SearchResult r in _srchResult)
        {
            total += r.SubTotal;
        }

        foreach (var s in _srch.Services)
        {
            total += s.ProcessSubTotal;
        }

        rptGuest.DataSource = _srchResult;
        rptGuest.DataBind();

        rptGuestInfo.DataSource = _srchResult;
        rptGuestInfo.DataBind();

        //ltTotal.Text = string.Format("<span style='font-weight:bold; font-size:14px;'>$ </span>{0:F}", total);
        this.SetTotal();
    }

    private void SetTotal()
    {
        rptServices.DataSource = _srch.Services;
        rptServices.DataBind();

        double total = 0.0D;
        foreach (SearchResult r in _srchResult)
        {
            total += r.SubTotal;
        }

        foreach (var s in _srch.Services)
        {
            total += s.ProcessSubTotal;
        }

        ltTotal.Text = string.Format("<span style='font-weight:bold; font-size:14px;'>$ </span>{0:F}", total);
    }

    protected void btnAddServices_Click(object sender, EventArgs e)
    {
        //if (Session["CURRENT_GUEST_LIST"] != null && (int)Session["CURRENT_GUEST_LIST"] > 0)
        //{
        //    Response.Redirect("evopost.aspx?lang=" + Globals.CurrentAppLanguageCode);
        //}
    }

    protected void btnSkipAndCheckout_Click(object sender, EventArgs e)
    {
        //_srch.SetEmptyServiceItemCart();
        //if (Session["CURRENT_GUEST_LIST"] != null && (int)Session["CURRENT_GUEST_LIST"] > 0)
        //{
        //    Response.Redirect("evopost.aspx?lang=" + Globals.CurrentAppLanguageCode);
        //}        
    }

    //private int PlaceOrder(int reservationID, int[] processID, Partners primaryPartner)
    //{
    //    List<Bed> lstReservedBeds = new Bed().GetListByReservationID(reservationID);

    //    //Check beds avaialability for the last time before creating reservation
    //    if (ProcessReservation.IsAccommodationAvailalbe(lstReservedBeds) == false)
    //    {
    //        return 0;
    //    }

    //    Orders ord = new Orders();
    //    SysCompanyInfo cinf = new SysCompanyInfo();
    //    cinf.PopulateObject(CurrentUser.DefaultCompanyID);

    //    ord.OrdCompanyID = CurrentUser.DefaultCompanyID;
    //    ord.OrdCreatedBy = CurrentUser.UserID;
    //    ord.OrdCreatedFromIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
    //    ord.OrdCurrencyCode = primaryPartner.PartnerCurrencyCode;
    //    ord.OrdCurrencyExRate = SysCurrencies.GetRelativePrice(ord.OrdCurrencyCode);
    //    ord.OrdCustID = primaryPartner.PartnerID;
    //    ord.OrdCustType = Globals.GetPartnerType(primaryPartner.PartnerType);
    //    ord.OrderTypeCommission = (int)OrderCommission.Reservation;
    //    ord.OrdLastUpdateBy = CurrentUser.UserID;
    //    ord.OrdSalesRepID = CurrentUser.UserID;
    //    ord.OrdSaleWeb = true;
    //    ord.OrdShippingTerms = cinf.CompanyShpToTerms;
    //    ord.OrdShpBlankPref = false;
    //    ord.OrdShpCode = string.Empty;
    //    ord.OrdShpCost = 0;
    //    ord.OrdShpDate = DateTime.Now;
    //    ord.OrdShpTrackNo = string.Empty;
    //    ord.OrdShpWhsCode = CurrentUser.UserDefaultWarehouse;
    //    ord.OrdStatus = SOStatus.IN_PROGRESS;
    //    ord.OrdType = StatusSalesOrderType.QUOTATION;
    //    ord.OrdVerified = true;
    //    ord.OrdVerifiedBy = CurrentUser.UserID;
    //    ord.QutExpDate = DateTime.Now;
    //    ord.OrdNetTerms = string.Empty;
    //    ord.OrderRejectReason = string.Empty;

    //    Reservation rsv = new Reservation();
    //    rsv.PopulateObject(reservationID);

    //    ord.OrdComment = rsv.ReservationNote;
    //    ord.Insert(CurrentUser.UserID); //Insert Order

    //    int soid = ord.OrdID;
    //    if (soid > 0)
    //    {
    //        List<OrderItems> itms = new List<OrderItems>();
    //        foreach (Bed item in lstReservedBeds)
    //        {
    //            OrderItems oitem = new OrderItems();
    //            oitem.OrderItemDesc = item.ToString();
    //            oitem.OrdID = soid;
    //            oitem.OrdProductDiscount = 0;
    //            oitem.OrdProductDiscountType = "P";
    //            oitem.OrdProductID = item.BedID;
    //            oitem.OrdProductQty = item.TotalDays;
    //            oitem.OrdProductTaxGrp = 0;//primaryPartner.PartnerTaxCode;
    //            oitem.OrdProductUnitPrice = item.BedPrice;
    //            oitem.OrdGuestID = item.GuestID;
    //            oitem.OrdReservationItemID = item.ReservationItemID;
    //            itms.Add(oitem);
    //        }

    //        OrderItems processItems = new OrderItems();
    //        processItems.AddOrderItems(itms, CurrentUser.UserDefaultWarehouse, false); //Insert Order Items

    //        //Process Items to Order
    //        OrderItemProcess proc = new OrderItemProcess();
    //        proc.AddProcessToOrder(processID, soid);

    //        ProcessReservation.UpdateReservationStatus(reservationID, soid, StatusReservation.Processed);
    //    }

    //    return soid;
    //}

    
    protected void rptServicesToAdd_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "addtocart")
        {
            TextBox txtQty = (TextBox)e.Item.FindControl("txtQty");
            _srch.AddServiceItem(BusinessUtility.GetInt(e.CommandArgument), BusinessUtility.GetInt(txtQty.Text));

            this.SetTotal();

            pnlScript.Visible = true;
        }
    }

    protected void rptServices_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            _srch.DeleteServiceItem(BusinessUtility.GetInt(e.CommandArgument));
            this.SetTotal();
            pnlScript.Visible = true;
        }
    }
    
    protected void btnEditReservation_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(SivanandaSession.SearchedHash))
        {
            Response.Redirect(SivanandaSession.SearchedHash);
        }
        else
        {
            Response.Redirect("Default.aspx?lang=" + Globals.CurrentAppLanguageCode);
        }
    }
    
    protected void rptServicesToAdd_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            int shtlProductID = BusinessUtility.GetInt(DataBinder.Eval(e.Item.DataItem, "ProcessID"));
            int fridayShtlID = 0;
            int sundayShtlID = 0;
            int.TryParse(ConfigurationManager.AppSettings["FridayShuttleProductID"], out fridayShtlID);
            int.TryParse(ConfigurationManager.AppSettings["SundayShuttleProductID"], out sundayShtlID);

            bool checkPast12Hr = (_srchResult[0].CheckInDate.Date != DateTime.Today || (_srchResult[0].CheckInDate.Date == DateTime.Today && DateTime.Now < DateTime.Today.AddHours(12)));

            if (_srchResult[0].CheckInDate.DayOfWeek == DayOfWeek.Friday && shtlProductID == fridayShtlID && checkPast12Hr)
            {
                e.Item.Visible = true;
                _visibleServiceCount++;            
            }
            else if (_srchResult[0].CheckOutDate.DayOfWeek == DayOfWeek.Sunday && shtlProductID == sundayShtlID)
            {
                e.Item.Visible = true;
                _visibleServiceCount++;
            }
            else
            {
                e.Item.Visible = false;
            }
        }
    }
}
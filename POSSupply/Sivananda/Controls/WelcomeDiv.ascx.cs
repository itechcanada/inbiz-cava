﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;

public partial class Sivananda_Controls_WelcomeDiv : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (SivanandaSession.IsAuthenticated)
        {
            ltCurrentGuestName.Text = SivanandaSession.CurrentGuest.PartnerLongName;
        }
        this.Visible = SivanandaSession.IsAuthenticated;
    }
    protected void lnkLogOut_Click(object sender, EventArgs e)
    {
        SivanandaSession.ClearSession();
        Response.Redirect("Default.aspx?lang=" + Globals.CurrentAppLanguageCode);
    }
}
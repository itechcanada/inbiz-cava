﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Net;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;

using iTECH.PaymentProvider;

public partial class Sivananda_evopost : SivaBasePage
{
    AccommodationSearch _srch = new AccommodationSearch();
    List<SearchResult> _srchResult;

    //replace normal reqest with secure connection request if ssl available on live
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Request.IsLocal && !Request.IsSecureConnection)
        {
            string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
            Response.Redirect(redirectUrl);            
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        _srchResult = _srch.SearchedData;
        if (_srchResult.Count <= 0)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPleaseSearchAccommodation);
            Response.Redirect("Default.aspx?lang=" + Globals.CurrentAppLanguageCode);
        }

        if (!IsPostBack)
        {
            FillFormData();
            txtCreditCardNo.Focus();
        }
    }

    private void FillFormData()
    {
        //Fill year ddl
        ddlYear.Items.Clear();
        for (int i = DateTime.Now.Year; i < DateTime.Now.Year + 20; i++)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString().Substring(2)));
        }

        double total = 0.0D;
        foreach (SearchResult r in _srchResult)
        {
            total += r.SubTotal;
        }

        foreach (var s in _srch.Services)
        {
            total += s.ProcessSubTotal;
        }

        rptGuest.DataSource = _srchResult;
        rptGuest.DataBind();

        rptGuestInfo.DataSource = _srchResult;
        rptGuestInfo.DataBind();

        rptServices.DataSource = _srch.Services;
        rptServices.DataBind();


        ltTotal.Text = string.Format("<span style='font-weight:bold; font-size:14px;'>$ </span>{0:F}", total);
    }

    //private int OrderID
    //{
    //    get
    //    {
    //        int id = 0;
    //        int.TryParse(Request.QueryString["oid"], out id);
    //        return id;
    //    }
    //}

    protected void btnPay_Click(object sender, EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);
        //Place order first
        int orderID = 0;

        try
        {
            orderID = this.PlaceOrder(dbHelp);
        }
        catch (Exception ex)
        {
            if (ex.Message == "GUEST_NOT_FOUND")
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgGuestInfoNotFound);
            }
            else if (ex.Message == CustomExceptionCodes.ACCOMMODATION_NOT_AVAILABLE)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgAccommodationNoLongerAvailable);
                Response.Redirect("Default.aspx?lang=" + Globals.CurrentAppLanguageCode, false);
            }
            if (!string.IsNullOrEmpty(SivanandaSession.SearchedHash))
            {
                Response.Redirect(SivanandaSession.SearchedHash, false);
            }
            else
            {
                Response.Redirect("Default.aspx?lang=" + Globals.CurrentAppLanguageCode, false);
            }
            return;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }

        if (orderID <= 0)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgErrorDuringPlacingOrder);

            if (!string.IsNullOrEmpty(SivanandaSession.SearchedHash))
            {
                Response.Redirect(SivanandaSession.SearchedHash, false);
            }
            else
            {
                Response.Redirect("Default.aspx?lang=" + Globals.CurrentAppLanguageCode, false);
            }
        }

        try
        {
            TotalSummary ts = CalculationHelper.GetOrderTotal(dbHelp, orderID);

            EvoPaymentHelper evoPayment = new EvoPaymentHelper(ConfigurationManager.AppSettings["EVO_MERCHANT_USER"], ConfigurationManager.AppSettings["EVO_MERCHANT_PASSWORD"]);
            NameValueCollection nvc = evoPayment.Sale(txtCreditCardNo.Text, ddlMonth.SelectedValue + ddlYear.SelectedValue, txtCVV.Text, orderID.ToString(), ts.GrandTotal.ToString(), Request.ServerVariables["REMOTE_ADDR"]);
            if (nvc.AllKeys.Contains("response") && BusinessUtility.GetInt(nvc["response"]) == (int)EvoResponse.TransactionApproved)
            {
                Reservation rsv = new Reservation();

                //Update Reservation status to consider as successfull payment
                rsv.UpdateReservationStatus(dbHelp, BusinessUtility.GetInt(hdnReservationID.Value), orderID, StatusReservation.Processed);

                Orders ord = new Orders();
                ord.PopulateObject(dbHelp, orderID);

                //Make entry in transaction table
                OrderTransaction trans = new OrderTransaction();
                trans.Amount = ts.GrandTotal;
                trans.CreatedBy = ord.OrdCustID;
                trans.CreatedOn = DateTime.Now;
                trans.GatewayErrors = string.Empty;
                trans.GatewayResponse = nvc["responsetext"];
                trans.OrderID = orderID;
                trans.PaymentMethod = "EVO Cananda";
                trans.TransactionDate = DateTime.Now;
                trans.TransactionID = nvc["transactionid"];
                trans.CardType = rdoVisa.Checked ? "VISA" : "MASTER";
                trans.IPAddress = Request.ServerVariables["REMOTE_ADDR"];
                trans.TransactionStatusID = TransactionStatus.Success;
                trans.TransactionTypeID = TransactionType.Sale;
                foreach (var item in nvc.AllKeys)
                {
                    trans.MetaDataCollection[item] = nvc[item];
                }
                trans.Insert(dbHelp, ord.OrdCustID);
                hdnTransactionID.Value = trans.TransactionID;
                PreAccountRcv arc = new PreAccountRcv();
                arc.OrderID = orderID;
                arc.AmountDeposit = BusinessUtility.GetDouble(ts.GrandTotal);
                arc.CustomerID = ord.OrdCustID;
                arc.DateReceived = DateTime.Now;
                arc.PaymentMethod = (int)StatusAmountReceivedVia.Paypal;
                arc.Insert(dbHelp);

                Notifications.SentEmailToGuest(orderID, hdnTransactionID.Value);
                Notifications.SentEmailToAshram(orderID, hdnTransactionID.Value);

                Session.Remove("DATA_TO_RESERVE");
            }
            else
            {
                Orders ord = new Orders();
                ord.PopulateObject(dbHelp, orderID);

                //Make entry in transaction table
                OrderTransaction trans = new OrderTransaction();
                trans.Amount = ts.GrandTotal;
                trans.CreatedBy = ord.OrdCustID;
                trans.CreatedOn = DateTime.Now;
                trans.GatewayErrors = nvc["responsetext"];
                trans.GatewayResponse = nvc["responsetext"];
                trans.OrderID = orderID;
                trans.PaymentMethod = "EVO Cananda";
                trans.TransactionDate = DateTime.Now;
                trans.TransactionID = nvc["transactionid"];
                trans.CardType = rdoVisa.Checked ? "VISA" : "MASTER";
                trans.IPAddress = Request.ServerVariables["REMOTE_ADDR"];
                trans.TransactionStatusID = TransactionStatus.Failed;
                trans.TransactionTypeID = TransactionType.Sale;
                foreach (var item in nvc.AllKeys)
                {
                    trans.MetaDataCollection[item] = nvc[item];
                }
                trans.Insert(dbHelp, ord.OrdCustID);

                ord.CancelReservationOrder(ord.OrdID, "ORDER REJECTED PAYMENT FAILED!");

                Session.Remove("DATA_TO_RESERVE");
            }
            nvc["lang"] = Globals.CurrentAppLanguageCode;
            Response.Redirect("evosuccess.aspx?" + nvc, false);
        }
        catch (Exception ex)
        {
            //ORDER REJECTED PAYMENT DECLINED
            Orders ord = new Orders();
            ord.CancelReservationOrder(orderID, "ORDER REJECTED SERVER ERROR ONLINE RESERVATION!");
            Response.Redirect("Default.aspx?lang=" + Globals.CurrentAppLanguageCode);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }
    
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //Response.Redirect("Cancel.aspx?oid=" + this.OrderID + "&lang=" + Globals.CurrentAppLanguageCode);
        if (!string.IsNullOrEmpty(SivanandaSession.SearchedHash))
        {
            Response.Redirect(SivanandaSession.SearchedHash);
        }
        else
        {
            Response.Redirect("Default.aspx?lang=" + Globals.CurrentAppLanguageCode);
        }
    }

    /*private bool SentEmailToGuest(int orderID)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            Orders ord = new Orders();
            ord.PopulateObject(dbHelp, orderID);

            TotalSummary ts = CalculationHelper.GetOrderTotal(dbHelp, orderID);

            string webWhs = ConfigurationManager.AppSettings["WebSaleWhsCode"];
            SysWarehouses whs = new SysWarehouses();
            whs.PopulateObject(webWhs, dbHelp);

            SysCompanyInfo ci = new SysCompanyInfo();
            ci.PopulateObject(whs.WarehouseCompanyID, dbHelp);

            Partners part = new Partners();
            part.PopulateObject(dbHelp, ord.OrdCustID);

            Reservation rsv = new Reservation();
            ReservationItems ri = rsv.GetFirstReservationItem(dbHelp, ord.OrdID);

            Bed b = new Bed();
            b.BedID = ri.BedID;
            b.PopulateBedInfo(dbHelp, b, Globals.CurrentAppLanguageCode);

            string sub = string.Format("Reservation Confirmation {0} ({1}, {2})", ci.CompanyName, ci.CompanyCity, ci.CompanyState);
            string msg = this.GetGuestEmailTemplate();

            string sqlGuest = "SELECT v.BedTitle, p.FirstName, p.LastName, p.SpirtualName, ri.PricePerDay FROM z_reservation_items ri INNER JOIN vw_beds v ON v.BedID=ri.BedID INNER JOIN z_customer_extended p ON p.PartnerID=ri.GuestID WHERE ri.ReservationID = @ReservationID";
            string bedGuestList = string.Empty;
            int totalBeds = 0;
            double toCalculatePricePerDay = 0.0D;
            double totalPriceCalculated = 0.0D;
            string spName = string.Empty;
            try
            {
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlGuest, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ReservationID", ri.ReservationID, MyDbType.Int)
                    }))
                {
                    int iCount = 0;
                    while (dr.Read())
                    {
                        iCount++;
                        bedGuestList += string.Format("{3}-{0} : {1} {2}<br>", iCount, dr["LastName"], dr["FirstName"], Resources.Resource.lblBedCaps);
                        totalPriceCalculated += BusinessUtility.GetDouble(dr["PricePerDay"]);
                        spName = BusinessUtility.GetString(dr["SpirtualName"]);
                        totalBeds++;
                    }
                }
            }
            finally
            {
                
            }

            toCalculatePricePerDay = (totalPriceCalculated) / (double)(totalBeds == 0 ? 1 : totalBeds);

            //OrderItemProcess oip = new OrderItemProcess();
            //string services = string.Empty;
            //try
            //{
            //    using (IDataReader dr = oip.GetOrderProcessItemsReader(dbHelp, ord.OrdID))
            //    {                    
            //        while (dr.Read())
            //        {                 
            //            services += string.Format("<tr><td>{0}</td><td>{1} {2:F}</td></tr>", dr["ProcessDescription"], ts.CurrencyCode, dr["ProcessCost"]);
            //        }
            //    }
            //}
            //finally
            //{

            //}
            string services = string.Empty;
            SysProcessGroup serviceProducts = new SysProcessGroup();
            double serviceSubTotal = 0.0D;
            try
            {
                using (IDataReader dr = serviceProducts.GetReserverdServicesForWeb(dbHelp, orderID, ts.CurrencyCode))
                {
                    while (dr.Read())
                    {
                        double subTotal = BusinessUtility.GetDouble(dr["ordProductUnitPrice"]) * BusinessUtility.GetDouble(dr["ordProductQty"]);
                        services += string.Format("<tr><td>{0}</td><td>{1} <b>x</b> {2:F} ={3} {4:F}</td></tr>", dr["prdName"], dr["ordProductQty"], dr["ordProductUnitPrice"], ts.CurrencyCode, subTotal);
                        serviceSubTotal += subTotal;
                    }
                    dr.Close();
                    dr.Dispose();
                }                               
            }
            finally
            { 
            }

            services += string.Format("<tr><td>{0}</td><td>{1} {2:F}</td></tr>", "Service Sub Total", ts.CurrencyCode, serviceSubTotal);

            msg = msg.Replace("#GUEST_NAME#", string.Format("{0} {1}", part.ExtendedProperties.FirstName, part.ExtendedProperties.LastName))
                .Replace("#COMPANY_NAME#", ci.CompanyName)
                .Replace("#COMPANY_CITY#", ci.CompanyCity)
                .Replace("#COMPANY_STATE#", ci.CompanyState)
                .Replace("#CONFIRMATION_NO#", string.Format("{0}{1}", part.ExtendedProperties.LastName, ord.OrdID))
                .Replace("#CHECKIN_DATE#", ri.CheckInDate.ToString("dddd MMMM dd, yyyy"))
                .Replace("#CHECKOUT_DATE#", ri.CheckOutDate.ToString("dddd MMMM dd, yyyy"))
                .Replace("#TOTAL_BEDS#", totalBeds.ToString())
                .Replace("#ROOM_TYPE#", UIHelper.GetRoomType(b.RoomType))
                .Replace("#BED_GUEST_LIST#", bedGuestList)
                .Replace("#BEDCOST_PER_NIGHT#", string.Format("{0} {1:F}", ts.CurrencyCode, toCalculatePricePerDay))
                .Replace("#NO_OF_NIGHTS#", ri.TotalNights.ToString())
                .Replace("#BED_SUB_TOTAL#", string.Format("{0} {1:F}", ts.CurrencyCode, totalPriceCalculated))
                .Replace("#SERVICES#", services)
                .Replace("#REMAINING_BALANCE#", string.Format("{0} {1:F}", ts.CurrencyCode, ts.OutstandingAmount))
                .Replace("#GRAND_TOTAL#", string.Format("{0} {1:F}", ts.CurrencyCode, ts.GrandTotal))
                .Replace("#TRANSACTION_ID#", hdnTransactionID.Value)
                .Replace("#SPIRITUAL_NAME#", !string.IsNullOrEmpty(spName) ? string.Format("({0})", spName) : string.Empty);

            if (totalBeds > 1)
            {
                msg = msg.Replace("#BED_RESOURCE#", Resources.Resource.lblBedPlural);
            }
            else
            {
                msg = msg.Replace("#BED_RESOURCE#", Resources.Resource.lblBedSingular);
            }

            //EmailHelper.SendEmail(ConfigurationManager.AppSettings["NoReplyEmail"], part.PartnerEmail, msg, sub, true);
            EmailHelper.SendAppointment(ConfigurationManager.AppSettings["NoReplyEmail"], part.PartnerEmail, msg, sub, ci.CompanyCity, ri.CheckInDate.AddHours(15).ToUniversalTime(), ri.CheckOutDate.AddHours(11).ToUniversalTime());
            return true;
        }
        catch
        {
            return false;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }      
    }

    private bool SentEmailToAshram(int orderID)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            Orders ord = new Orders();
            ord.PopulateObject(dbHelp, orderID);

            TotalSummary ts = CalculationHelper.GetOrderTotal(dbHelp, orderID);

            string webWhs = ConfigurationManager.AppSettings["WebSaleWhsCode"];
            SysWarehouses whs = new SysWarehouses();
            whs.PopulateObject(webWhs, dbHelp);

            SysCompanyInfo ci = new SysCompanyInfo();
            ci.PopulateObject(whs.WarehouseCompanyID, dbHelp);

            Partners part = new Partners();
            part.PopulateObject(dbHelp, ord.OrdCustID);

            Reservation rsv = new Reservation();
            ReservationItems ri = rsv.GetFirstReservationItem(dbHelp, ord.OrdID);

            Bed b = new Bed();
            b.BedID = ri.BedID;
            b.PopulateBedInfo(dbHelp, b, Globals.CurrentAppLanguageCode);

            string sub = string.Format("A New Reservation – {0} {1} #SPIRITUAL_NAME#", part.ExtendedProperties.FirstName, part.ExtendedProperties.LastName);
            string msg = this.GetAshramEmailTemplate();

            string sqlGuest = "SELECT v.BedTitle, p.FirstName, p.LastName, p.SpirtualName, ri.IsCouple, ri.Sex, ri.PricePerDay FROM z_reservation_items ri INNER JOIN vw_beds v ON v.BedID=ri.BedID INNER JOIN z_customer_extended p ON p.PartnerID=ri.GuestID WHERE ri.ReservationID = @ReservationID ORDER BY ri.ReservationItemID";
            string bedGuestList = string.Empty;
            List<string> lstCouple1 = new List<string>();
            List<string> lstCouple2 = new List<string>();
            int totalBeds = 0;
            double toCalculatePricePerDay = 0.0D;
            double totalPriceCalculated = 0.0D;
            string spName = string.Empty;
            try
            {                
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlGuest, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ReservationID", ri.ReservationID, MyDbType.Int)
                    }))
                {
                    int iCount = 0;
                    while (dr.Read())
                    {
                        iCount++;
                        if (BusinessUtility.GetBool(dr["IsCouple"]))
                        {
                            if (lstCouple1.Count < 2)
                            {
                                lstCouple1.Add(string.Format("{4}-{0} ({1}): {2} {3} - {5}<br>", iCount, dr["BedTitle"], dr["LastName"], dr["FirstName"], Resources.Resource.lblBedCaps, BusinessUtility.GetString(dr["Sex"]) == "M" ? Resources.Resource.lblMale : Resources.Resource.lblFemale));
                            }
                            else if(lstCouple2.Count < 2)
                            {
                                lstCouple2.Add(string.Format("{4}-{0} ({1}): {2} {3} - {5}<br>", iCount, dr["BedTitle"], dr["LastName"], dr["FirstName"], Resources.Resource.lblBedCaps, BusinessUtility.GetString(dr["Sex"]) == "M" ? Resources.Resource.lblMale : Resources.Resource.lblFemale));
                            }
                        }
                        else
                        {
                            bedGuestList += string.Format("{4}-{0} ({1}): {2} {3} - {5}<br>", iCount, dr["BedTitle"], dr["LastName"], dr["FirstName"], Resources.Resource.lblBedCaps, BusinessUtility.GetString(dr["Sex"]) == "M" ? Resources.Resource.lblMale : Resources.Resource.lblFemale);
                        }
                        totalPriceCalculated += BusinessUtility.GetDouble(dr["PricePerDay"]);
                        spName = BusinessUtility.GetString(dr["SpirtualName"]);
                        totalBeds++;
                    }
                }
                if (lstCouple1.Count > 0 && lstCouple2.Count > 0)
                {
                    string coupleInfo = string.Format(@"<span style=""text-decoration: underline;font-weight:bold"">{0}</span><br>", Resources.Resource.lblThereAre2Couples);
                    coupleInfo += string.Format(@"<span style=""text-decoration: underline;font-weight:bold"">{0} 1</span><br>", Resources.Resource.lblCouple);
                    foreach (var item in lstCouple1)
                    {
                        coupleInfo += item;
                    }
                    coupleInfo += "<br>";

                    coupleInfo += string.Format(@"<span style=""text-decoration: underline;font-weight:bold"">{0} 2</span><br>", Resources.Resource.lblCouple);
                    foreach (var item in lstCouple2)
                    {
                        coupleInfo += item;
                    }
                    coupleInfo += "<br>";

                    bedGuestList = coupleInfo + bedGuestList;
                }
                else if (lstCouple1.Count > 0)
                {
                    string coupleInfo = string.Format(@"<span style=""text-decoration: underline;font-weight:bold"">{0}</span><br>", Resources.Resource.lblThereAre1Couple);
                    coupleInfo += string.Format(@"<span style=""text-decoration: underline;font-weight:bold"">{0} 1</span><br>", Resources.Resource.lblCouple);
                    foreach (var item in lstCouple1)
                    {
                        coupleInfo += item;
                    }
                    coupleInfo += "<br>";                    

                    bedGuestList = coupleInfo + bedGuestList;
                }
            }
            finally
            {

            }

            toCalculatePricePerDay = (totalPriceCalculated) / (double)(totalBeds == 0 ? 1 : totalBeds);

            //OrderItemProcess oip = new OrderItemProcess();
            //string services = string.Empty;
            //try
            //{
            //    using (IDataReader dr = oip.GetOrderProcessItemsReader(dbHelp, ord.OrdID))
            //    {
            //        while (dr.Read())
            //        {
            //            services += string.Format("<tr><td>{0}</td><td>{1} {2:F}</td></tr>", dr["ProcessDescription"], ts.CurrencyCode, dr["ProcessCost"]);
            //        }
            //    }
            //}
            //finally
            //{

            //}
            string services = string.Empty;
            SysProcessGroup serviceProducts = new SysProcessGroup();
            double serviceSubTotal = 0.0D;
            try
            {
                using (IDataReader dr = serviceProducts.GetReserverdServicesForWeb(dbHelp, orderID, ts.CurrencyCode))
                {
                    while (dr.Read())
                    {
                        double subTotal = BusinessUtility.GetDouble(dr["ordProductUnitPrice"]) * BusinessUtility.GetDouble(dr["ordProductQty"]);
                        services += string.Format("<tr><td>{0}</td><td>{1} <b>x</b> {2:F} ={3} {4:F}</td></tr>", dr["prdName"], dr["ordProductQty"], dr["ordProductUnitPrice"], ts.CurrencyCode, subTotal);
                        serviceSubTotal += subTotal;
                    }
                    dr.Close();
                    dr.Dispose();
                }
            }
            finally
            {
            }
            services += string.Format("<tr><td>{0}</td><td>{1} {2:F}</td></tr>", "Service Sub Total", ts.CurrencyCode, serviceSubTotal);
            
            msg = msg.Replace("#GUEST_NAME#", string.Format("{0} {1}", part.ExtendedProperties.FirstName, part.ExtendedProperties.LastName))
                .Replace("#COMPANY_NAME#", ci.CompanyName)
                .Replace("#COMPANY_CITY#", ci.CompanyCity)
                .Replace("#COMPANY_STATE#", ci.CompanyState)
                .Replace("#CONFIRMATION_NO#", string.Format("{0}{1}", part.ExtendedProperties.LastName, ord.OrdID))
                .Replace("#CHECKIN_DATE#", ri.CheckInDate.ToString("dddd MMMM dd, yyyy"))
                .Replace("#CHECKOUT_DATE#", ri.CheckOutDate.ToString("dddd MMMM dd, yyyy"))
                .Replace("#TOTAL_BEDS#", totalBeds.ToString())
                .Replace("#ROOM_TYPE#", UIHelper.GetRoomType(b.RoomType))
                .Replace("#BED_GUEST_LIST#", bedGuestList)
                .Replace("#BEDCOST_PER_NIGHT#", string.Format("{0} {1:F}", ts.CurrencyCode, toCalculatePricePerDay))                
                .Replace("#NO_OF_NIGHTS#", ri.TotalNights.ToString())
                .Replace("#BED_SUB_TOTAL#", string.Format("{0} {1:F}", ts.CurrencyCode, totalPriceCalculated))
                .Replace("#SERVICES#", services)
                .Replace("#REMAINING_BALANCE#", string.Format("{0} {1:F}", ts.CurrencyCode, ts.OutstandingAmount))
                .Replace("#GUEST_EMAIL#", part.PartnerEmail)
                .Replace("#RESERVATION_NOTES#", Session["CURRENT_GUEST_RESERVATION_NOTES"] != null ? (string)Session["CURRENT_GUEST_RESERVATION_NOTES"] : string.Empty)
                .Replace("#CUSTOMER_NOTES#", part.PartnerNote)
                .Replace("#GRAND_TOTAL#", string.Format("{0} {1:F}", ts.CurrencyCode, ts.GrandTotal))
                .Replace("#TRANSACTION_ID#", hdnTransactionID.Value)
                .Replace("#SPIRITUAL_NAME#", !string.IsNullOrEmpty(spName) ? string.Format("({0})", spName) : string.Empty);

            if (totalBeds > 1)
            {
                msg = msg.Replace("#BED_RESOURCE#", Resources.Resource.lblBedPlural);
            }
            else
            {
                msg = msg.Replace("#BED_RESOURCE#", Resources.Resource.lblBedSingular);
            }
            sub = sub.Replace("#SPIRITUAL_NAME#", !string.IsNullOrEmpty(spName) ? string.Format("({0})", spName) : string.Empty);
            //EmailHelper.SendEmail(ConfigurationManager.AppSettings["NoReplyEmail"], ci.CompanyEmail, msg, sub, true);
            EmailHelper.SendAppointment(ConfigurationManager.AppSettings["NoReplyEmail"], ci.CompanyEmail, msg, sub, ci.CompanyCity, ri.CheckInDate.AddHours(15).ToUniversalTime(), ri.CheckOutDate.AddHours(11).ToUniversalTime());
            return true;
        }
        catch
        {
            return false;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private string GetGuestEmailTemplate()
    {        
        return System.IO.File.ReadAllText(Server.MapPath(string.Format("~/Sivananda/EmailTemplates/ToGuest_{0}.txt", Globals.CurrentAppLanguageCode.Trim().ToLower())));
    }

    private string GetAshramEmailTemplate()
    {
        return System.IO.File.ReadAllText(Server.MapPath(string.Format("~/Sivananda/EmailTemplates/ToAshram_{0}.txt", Globals.CurrentAppLanguageCode.Trim().ToLower())));
    }*/

    private int PlaceOrder(DbHelper dbHelp)
    {
        int guestID = Session["CURRENT_GUEST_LIST"] != null ? (int)Session["CURRENT_GUEST_LIST"] : 0;
        if (guestID <= 0)
        {
            throw new Exception("GUEST_NOT_FOUND");
        }
        string webWhsCode = ConfigurationManager.AppSettings["WebSaleWhsCode"];
        SysWarehouses whs = new SysWarehouses();
        whs.PopulateObject(webWhsCode, dbHelp);

        SysCompanyInfo ci = new SysCompanyInfo();
        ci.PopulateObject(whs.WarehouseCompanyID, dbHelp);

        Partners cust = new Partners();
        cust.PopulateObject(dbHelp, guestID);

        //Create Reservation first
        Reservation rsv = new Reservation();
        rsv.NoOfGuestOlderFeMale = 0;
        rsv.NoOfGuestOlderMale = 0;
        rsv.NoOfGuestYoungerFeMale = 0;
        rsv.NoOfGuestYoungerMale = 0;
        rsv.PrimaryPartnerID = cust.PartnerID;
        rsv.ReservationStatus = (int)StatusReservation.New;
        rsv.ReserveFor = cust.ExtendedProperties.GuestType;
        rsv.ReservationNote = Session["CURRENT_GUEST_RESERVATION_NOTES"] != null ? (string)Session["CURRENT_GUEST_RESERVATION_NOTES"] : string.Empty;

        rsv.Insert(dbHelp, CurrentUser.UserID); //Crate Reservation & aslo 

        List<ReservationItems> lstRsvItems = new List<ReservationItems>();
        //Reservation Item
        if (rsv.ReservationID > 0)
        {
            hdnReservationID.Value = rsv.ReservationID.ToString();

            foreach (var item in _srchResult)
            {
                ReservationItems ri = new ReservationItems();
                ri.BedID = item.BedID;
                ri.CheckInDate = _srchResult[0].CheckInDate;
                ri.CheckOutDate = _srchResult[0].CheckOutDate;
                ri.GuestID = cust.PartnerID;
                ri.PricePerDay = item.UnitPrice;
                ri.ReservationID = rsv.ReservationID;
                ri.IsCouple = item.IsCouple;
                ri.Sex = item.Sex;                
                lstRsvItems.Add(ri);
            }



            ReservationItems rItem = new ReservationItems();
            rItem.AddList(dbHelp, lstRsvItems);            
            
            //Check beds avaialability for the last time before creating reservation
            if (ProcessReservation.IsAccommodationAvailalbe(dbHelp, lstRsvItems) == false)
            {
                throw new Exception(CustomExceptionCodes.ACCOMMODATION_NOT_AVAILABLE);
            }

            Orders ord = new Orders();            

            ord.OrdCompanyID = ci.CompanyID;
            ord.OrdCreatedBy = CurrentUser.UserID;
            ord.OrdCreatedFromIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
            ord.OrdCurrencyCode = cust.PartnerCurrencyCode;
            ord.OrdCurrencyExRate = SysCurrencies.GetRelativePrice(ord.OrdCurrencyCode);
            ord.OrdCustID = cust.PartnerID;
            ord.OrdCustType = Globals.GetPartnerType(cust.PartnerType);
            ord.OrderTypeCommission = (int)OrderCommission.Reservation;
            ord.OrdLastUpdateBy = CurrentUser.UserID;
            ord.OrdSalesRepID = CurrentUser.UserID;
            ord.OrdSaleWeb = true;
            ord.OrdShippingTerms = ci.CompanyShpToTerms;
            ord.OrdShpBlankPref = false;
            ord.OrdShpCode = string.Empty;
            ord.OrdShpCost = 0;
            ord.OrdShpDate = DateTime.Now;
            ord.OrdShpTrackNo = string.Empty;
            ord.OrdShpWhsCode = webWhsCode;
            ord.OrdStatus = SOStatus.IN_PROGRESS;
            ord.OrdType = StatusSalesOrderType.QUOTATION;
            ord.OrdVerified = true;
            ord.OrdVerifiedBy = CurrentUser.UserID;
            ord.QutExpDate = DateTime.Now;
            ord.OrdNetTerms = string.Empty;
            ord.OrderRejectReason = string.Empty;
            ord.OrdComment = rsv.ReservationNote;
            ord.Insert(dbHelp, CurrentUser.UserID); //Insert Order

            int soid = ord.OrdID;
            if (soid > 0)
            {                
                OrderItems processItems = new OrderItems();
                processItems.AddReservationItems(dbHelp, soid, rsv.ReservationID, cust.PartnerID, webWhsCode, _srch.Services); //Insert Order Items

                //Process Items to Order
                //OrderItemProcess proc = new OrderItemProcess();
                //proc.AddProcessToOrder(processID, soid);

                //To do add into log
                //new OrderActionLog().AddLog(dbHelp, CurrentUser.UserID, soid, "Reservation created", ord.OrdComment); //Commented because it was throwing error 
            }

            return soid;
        }

        return 0;
    }
}
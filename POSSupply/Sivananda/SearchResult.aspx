﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Sivananda/sivananda.master" AutoEventWireup="true"
    CodeFile="SearchResult.aspx.cs" Inherits="Sivananda_SearchResult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <style type="text/css">
        ul.ulResult
        {
            font-weight: normal !important;
        }
        ul.ulResult li.available
        {
            font-weight: normal !important;
        }
        ul.ulResult li.na
        {
            font-weight: normal !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFullWidth" runat="Server">
    <div style="border: 1px solid #FF7300; margin: 10px auto; padding: 5px;">
        <asp:Panel ID="pnlSearchResult" runat="server">
            <h2>
                <asp:Label ID="lblTitle" Text="<%$Resources:Resource, lblAccommodationDetails%>" runat="server" />
            </h2>
            <div style="padding: 5px;">
                <table border="0" cellpadding="2" cellspacing="2" width="100%">
                    <tr>
                        <td style="width: 50%; background-color: #FFECB5; border: 1px solid #E5C365;">
                            <asp:Label ID="lblAvailability" 
                                Text="<%$ Resources:Resource, lblAvailability %>" runat="server" 
                                Font-Bold="True" />
                        </td>
                        <td style="width: 50%; background-color: #FFECB5; border: 1px solid #E5C365;">
                            <asp:Label ID="lblAccommodationSummary" 
                                Text="<%$ Resources:Resource, lblAccommodationSummary %>" runat="server" 
                                Font-Bold="True" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="border: 1px solid #E5C365;">
                            <ul class="ulResult">
                                <asp:Repeater ID="rptAvailability" runat="server">
                                    <ItemTemplate>
                                        <%#Container.DataItem%>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                            <p>
                                <asp:Label ID="Label1" Text="<%$Resources:Resource, msgToChangeDate%>" runat="server" />                                
                            </p>                            
                        </td>
                        <td valign="top" style="border: 1px solid #E5C365;">
                            <div id="tabs">
                                <ul>
                                    <asp:Repeater ID="rptGuest" runat="server">
                                        <ItemTemplate>
                                            <li><a href='#tabs-<%#Eval("GuestNo")%>'>                                                
                                                <asp:Literal ID="ltGuest" Text="<%$Resources:Resource, lblGuest %>" runat="server" /> #
                                                <%#Eval("GuestNo")%></a> </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                                <asp:Repeater ID="rptGuestInfo" runat="server">
                                    <ItemTemplate>
                                        <div id='tabs-<%#Eval("GuestNo")%>'>
                                            <asp:HiddenField ID="hdnBedID" runat="server" Value='<%#Eval("BedID")%>' />
                                            <table border="0" cellpadding="2" cellspacing="5">
                                                <tr>
                                                    <td style="width:120px;">                                                        
                                                        <asp:Label ID="lblCheckInDate" Text="<%$Resources:Resource, lblCheckIn %>" runat="server" />
                                                    </td>
                                                    <td>
                                                        <%#Eval("CheckInDate", "{0:dddd dd MMMM yyyy}")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblCheckOutDate" Text="<%$Resources:Resource, lblCheckout %>" runat="server" />
                                                    </td>
                                                    <td>
                                                        <%#Eval("CheckOutDate", "{0:dddd dd MMMM yyyy}")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>                                                        
                                                        <asp:Label ID="lblNoOfNights" Text="<%$Resources:Resource, lblNoOfNights %>" runat="server" />
                                                    </td>
                                                    <td>
                                                        <%#Eval("NoOfNight")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblNoOfGuest" Text="<%$Resources:Resource, lblNoOfGuest %>" runat="server" />
                                                    </td>
                                                    <td>
                                                        <%#Eval("NoOfGuest")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>                                                        
                                                        <asp:Label ID="lblRoomType" Text="<%$Resources:Resource,lblRoomType  %>" runat="server" />
                                                    </td>
                                                    <td>
                                                         <%#Eval("RoomTypeDesc")%> (<%#Eval("UnitPrice", "<span style='font-weight:bold; font-size:14px;'>$ </span>{0:F}")%><%#Resources.Resource.lblPerNight%>)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblAmenities" Text="<%$Resources:Resource, lblAmenities %>" runat="server" />
                                                    </td>
                                                    <td>
                                                        <%#Eval("Amenities")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblSubTotal" Text="<%$Resources:Resource, lblSubTotal %>" runat="server" />
                                                    </td>
                                                    <td>
                                                        <%#Eval("SubTotal", "<span style='font-weight:bold; font-size:14px;'>$ </span>{0:F}")%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <table border="0" cellpadding="2" cellspacing="5" style="margin-left:16px;">
                                    <tr>
                                        <td style="width:120px;">
                                            <asp:Label ID="lblTotal" Text="<%$Resources:Resource, lblTotal %>" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Literal ID="ltTotal" Text="" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <div style="text-align: right;">
                    <asp:Button ID="btnGoBack" Text="<%$Resources:Resource, lblGoBack%>" runat="server" OnClick="btnGoBack_Click" />
                    <asp:Button ID="btnReserve" Text="<%$Resources:Resource, lblReserve%>" runat="server" OnClick="btnReserve_Click" />                         
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $(function () {
            $("#tabs").tabs();
        });   
    </script>
</asp:Content>

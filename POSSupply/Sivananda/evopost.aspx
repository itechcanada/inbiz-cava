﻿<%@ Page Language="C#" MasterPageFile="~/Sivananda/sivananda.master" AutoEventWireup="true" CodeFile="evopost.aspx.cs" Inherits="Sivananda_evopost" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <%=UIHelper.GetScriptTags("blockui", 1)%>   
    <script src="../lib/scripts/jquery-plugins/jquery.addplaceholder.js" type="text/javascript"></script>
     <script type="text/javascript">
         window.history.forward();
         function noBack() { window.history.forward(); }

         function blockContentArea(elementToBlock, loadingMessage) {
             $(elementToBlock).block({
                 message: '<div>' + loadingMessage + '</div>',
                 css: { border: '3px solid #a00' }
             });
         }
         function unblockContentArea(elementToBlock) {
             $(elementToBlock).unblock();
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFullWidth" runat="Server">
    <div style="border: 1px solid #FF7300; margin: 10px auto; padding: 5px;">
        <h2>            
            <asp:Label ID="lblAccommodationSummary" Text="<%$Resources:Resource, lblAccommodationSummary %>" runat="server" />
        </h2>
        <div style="padding: 2px; margin: 2px;">
            <table border="0" cellpadding="2" cellspacing="2" width="100%">
                <%--<tr>                    
                    <td style="width: 50%; background-color: #FFECB5; border: 1px solid #E5C365;">
                        
                    </td>
                </tr>--%>
                <tr>                    
                    <td valign="top">
                        <div id="tabs">
                            <ul>
                                <asp:Repeater ID="rptGuest" runat="server">
                                    <ItemTemplate>
                                        <li><a href='#tabs-<%#Eval("GuestNo")%>'>                                                
                                                <asp:Literal ID="ltGuest" Text="<%$Resources:Resource, lblGuest %>" runat="server" /> #
                                                <%#Eval("GuestNo")%></a> </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                            <asp:Repeater ID="rptGuestInfo" runat="server">
                                <ItemTemplate>
                                    <div id='tabs-<%#Eval("GuestNo")%>'>
                                        <asp:HiddenField ID="hdnBedID" runat="server" Value='<%#Eval("BedID")%>' />
                                        <table border="0" cellpadding="2" cellspacing="5">
                                            <tr>
                                                <td style="width: 120px;">
                                                     <asp:Label ID="lblCheckInDate" Text="<%$Resources:Resource, lblCheckIn%>" runat="server" />
                                                </td>
                                                <td>
                                                    <%#Eval("CheckInDate", "{0:dddd dd MMMM yyyy}")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                     <asp:Label ID="lblCheckOutDate" Text="<%$Resources:Resource, lblCheckout%>" runat="server" />
                                                </td>
                                                <td>
                                                    <%#Eval("CheckOutDate", "{0:dddd dd MMMM yyyy}")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   <asp:Label ID="lblNoOfNights" Text="<%$Resources:Resource, lblNoOfNights %>" runat="server" />
                                                </td>
                                                <td>
                                                    <%#Eval("NoOfNight")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblNoOfGuest" Text="<%$Resources:Resource, lblNoOfGuest %>" runat="server" />
                                                </td>
                                                <td>
                                                    <%#Eval("NoOfGuest")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblRoomType" Text="<%$Resources:Resource, lblRoomType %>" runat="server" />
                                                </td>
                                                <td>
                                                   <%#Eval("RoomTypeDesc")%> (<%#Eval("UnitPrice", "<span style='font-weight:bold; font-size:14px;'>$ </span>{0:F}")%><%#Resources.Resource.lblPerNight%>)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblAmenities" Text="<%$Resources:Resource, lblAmenities%>" runat="server" />
                                                </td>
                                                <td>
                                                    <%#Eval("Amenities")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblSubTotal" Text="<%$Resources:Resource, lblSubTotal%>" runat="server" />
                                                </td>
                                                <td>
                                                    <%#Eval("SubTotal", "${0:F}")%>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <table border="0" cellpadding="2" cellspacing="5" style="margin-left: 16px;">
                                <asp:Repeater ID="rptServices" runat="server">
                                    <ItemTemplate>
                                        <tr style="">
                                            <td style="width: 120px;">
                                                <asp:Label ID="lblServiceDesc" Text='<%#Eval("ProcessDesc")%>' runat="server" />
                                            </td>
                                            <td>
                                                <%#string.Format("{1} <b>x</b> {0:F}=", Eval("ProcessCost"), Eval("ProcessUnits"))%>
                                                <%#string.Format("<span style='font-weight:bold; font-size:14px;'>$ </span>{0:F}", Eval("ProcessSubTotal"))%>                                                
                                            </td>                                           
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr>
                                    <td style="width: 120px;font-size:14px; font-weight:bold; border-top:1px solid #ccc">
                                        <asp:Label ID="lblTotal" Text="<%$Resources:Resource, lblTotal%>" runat="server" />
                                    </td>
                                    <td style="font-size:14px;font-weight:bold; border-top:1px solid #ccc">
                                        <asp:Literal ID="ltTotal" Text="" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <table border="0" cellpadding="3" cellspacing="3">
                <tr>
                    <td>
                         
                    </td>
                    <td id="tdCardType">
                        <asp:RadioButton ID="rdoVisa" Text="" runat="server" GroupName="CardType" style="vertical-align:middle;"/> 
                        <asp:Label ID="lblVisa" AssociatedControlID="rdoVisa" Text="" runat="server" >
                            <asp:image ID="imgVisa" imageurl="~/Images/Credit-card-Visa-32.png" runat="server" style="vertical-align:middle;"/> 
                        </asp:Label>
                        <asp:RadioButton ID="rdoMaster" Text="" runat="server" GroupName="CardType" style="vertical-align:middle;"/> 
                        <asp:Label ID="lblMaster" AssociatedControlID="rdoMaster" Text="" runat="server" >
                            <asp:image ID="imgMaster" imageurl="~/Images/Credit-card-MasterCard-32.png" runat="server" style="vertical-align:middle;"/> 
                        </asp:Label>
                    </td>
                </tr>
                <tr id="trNameOnCard" runat="server" visible="false">
                    <td valign="top" style="width: 150px;">
                        <asp:Label ID="Label1" Text="<%$Resources:Resource, lblNameOnCard %>"
                            runat="server" />
                    </td>
                    <td valign="top">
                        <asp:TextBox ID="txtNameOnCard" runat="server" autocomplete="off" Width="250px" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="*" ControlToValidate="txtNameOnCard"
                            runat="server" SetFocusOnError="true" />
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="width: 150px;">
                        <asp:Label ID="lblCreditCardNo" Text="<%$Resources:Resource, lblCreditCardNo %>"
                            runat="server" />
                    </td>
                    <td valign="top">
                        <asp:TextBox ID="txtCreditCardNo" runat="server" autocomplete="off" Width="250px" />
                        <asp:RequiredFieldValidator ID="rfvCreditCard" ErrorMessage="*" ControlToValidate="txtCreditCardNo"
                            runat="server" SetFocusOnError="true" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <asp:Label ID="lblExp" Text="<%$Resources:Resource, lblExpiryDate %>" runat="server" />
                    </td>
                    <td valign="top">
                        <asp:DropDownList ID="ddlMonth" runat="server" Width="50px">
                            <asp:ListItem Value="01">01</asp:ListItem>
                            <asp:ListItem Value="02">02</asp:ListItem>
                            <asp:ListItem Value="03">03</asp:ListItem>
                            <asp:ListItem Value="04">04</asp:ListItem>
                            <asp:ListItem Value="05">05</asp:ListItem>
                            <asp:ListItem Value="06">06</asp:ListItem>
                            <asp:ListItem Value="07">07</asp:ListItem>
                            <asp:ListItem Value="08">08</asp:ListItem>
                            <asp:ListItem Value="09">09</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                            <asp:ListItem Value="11">11</asp:ListItem>
                            <asp:ListItem Value="12">12</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;
                        <asp:DropDownList ID="ddlYear" runat="server" Width="60px">
                            
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <asp:Label ID="lblCVV" Text="<%$Resources:Resource, lblCVV%>" runat="server" />
                    </td>
                    <td valign="top">
                        <asp:TextBox runat="server" ID="txtCVV" MaxLength="3" TextMode="Password" Width="60px" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ControlToValidate="txtCVV"
                            runat="server" SetFocusOnError="true" />
                        <%=Resources.Resource.lbl3DigitSecurityNo%>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>                        
                        <asp:Button ID="btnCheckout" CssClass="buttonsToDisable" Text="<%$Resources:Resource, lblPay%>" runat="server" OnClick="btnPay_Click" />
                        <asp:Button ID="btnCancel" CssClass="buttonsToDisable" Text="<%$Resources:Resource, Cancel%>" 
                            runat="server" OnClick="btnCancel_Click" CausesValidation="False" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:HiddenField ID="hdnReservationID" runat="server" />
    <asp:HiddenField ID="hdnTransactionID" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        $("#tabs").tabs();
    </script>
     <script type="text/javascript">
         $(document).ready(noBack);

         $("#<%=btnCheckout.ClientID%>").click(function () {
             var isSelected = false;
             $("#tdCardType input:radio").each(function () {
                 if (this.checked) {
                     isSelected = true;
                 }
             });
             if (isSelected) {
                 //                 if (Page_IsValid) {
                 //                     $(".buttonsToDisable").button({ disabled: true });
                 //                 }
                 if (Page_IsValid) {
                     blockContentArea($("body"), "<div>Please wait while processing your payment.</div><div>Do not refresh page or press back button</div>");
                 }
                 return Page_IsValid;
             }
             else {
                 alert("Please select card type!");
                 return Page_IsValid && isSelected;
             }
         });         
     </script>
</asp:Content>
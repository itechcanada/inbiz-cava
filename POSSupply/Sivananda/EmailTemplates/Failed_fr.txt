﻿Dear <b>#CUSTOMER_NAME#</b>,
<p>Your reservation could not be completed for the following reason:</p>
<p><b>#REASON#</b></p>
<p>Please try again. If the problem persists please call the Ashram at (+1) 819-322-3226 or 1-800-263-9642 (Canada/USA). <br>
<a href="Default.aspx">Try again!</a>
</p>

﻿ <p>
        Thank you for your reservation #GUEST_NAME# #SPIRITUAL_NAME#!
    </p>
    <p>
        Your reservation was successful with Sivananda Ashram Yoga Camp in Val Morin, QC.</p>
    <span style="text-decoration: underline;font-weight:bold">Reservation Details: </span>
    <br />
    <br />
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
               Confirmation #:
            </td>
            <td>
                #CONFIRMATION_NO#
            </td>
        </tr>
        <tr>
            <td>
                Check-In:
            </td>
            <td>
                #CHECKIN_DATE#
            </td>
        </tr>
        <tr>
            <td>
                Check-out:
            </td>
            <td>
                #CHECKOUT_DATE#
            </td>
        </tr>
		<tr>
			<td>Transaction ID:</td>
			<td>#TRANSACTION_ID#</td>
		</tr>
    </table>
    <p>
        #TOTAL_BEDS# #BED_RESOURCE# in a #ROOM_TYPE# type accommodation
    </p>
    <p>
        #BED_GUEST_LIST#</p>
    <span style="text-decoration: underline;font-weight:bold">Summary of Charges: </span>
    <br />
    <br />
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                Bed Cost (average per bed per night):
            </td>
            <td>
                #BEDCOST_PER_NIGHT#
            </td>
        </tr>
        <tr>
            <td>
                Number of beds:
            </td>
            <td>
                #TOTAL_BEDS#
            </td>
        </tr>
        <tr>
            <td>
                Type of beds:
            </td>
            <td>
                #ROOM_TYPE#
            </td>
        </tr>
        <tr>
            <td>
                Number of nights:
            </td>
            <td>
                #NO_OF_NIGHTS#
            </td>
        </tr>
        <tr>
            <td>
                Beds Subtotal:
            </td>
            <td>
                #BED_SUB_TOTAL#
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="margin-top: 10px;">
                    Services (if any):</div>
            </td>
        </tr>
        #SERVICES#
        <tr>
            <td>
                Total Cost:
            </td>
            <td>
                #GRAND_TOTAL#
            </td>
        </tr>
		<tr>
			<td>Remaining Balance:</td>
			<td>#REMAINING_BALANCE#</td>
		</tr>
    </table>
    <p style="font-weight: bold;">
        Sivananda Ashram Yoga Camp has charged your card for the full payment of this reservation.
    </p>
    <p style="font-style: italic">
        Please call the Ashram at (+1) 819-322-3226 or 1-800-263-9642 (Canada/USA) for any
        questions or concerns
    </p>   
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Sivananda/sivananda.master" AutoEventWireup="true" CodeFile="AddServices.aspx.cs" Inherits="Sivananda_AddServices" %>

<%@ Register src="Controls/WelcomeDiv.ascx" tagname="WelcomeDiv" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFullWidth" Runat="Server">
    <uc2:WelcomeDiv ID="WelcomeDiv1" runat="server" />    
    <div style="border: 1px solid #FF7300; margin: 10px auto; padding: 5px;">         
        <h2>
            <asp:Label ID="Label1" Text="<%$Resources:Resource, lblAddServices %>" runat="server" />
        </h2>
        <div style="padding: 5px;">
            <table border="0" cellpadding="2" cellspacing="2" width="100%">
                <tr>
                    <td style="width: 50%; background-color: #FFECB5; border: 1px solid #E5C365;">
                        <asp:Label ID="lblAddServices" Text="<%$ Resources:Resource, lblAddServices %>" 
                            runat="server" Font-Bold="True" />
                    </td>
                    <td style="width: 50%; background-color: #FFECB5; border: 1px solid #E5C365;">
                        <asp:Label ID="lblAccommodationSummary" 
                            Text="<%$ Resources:Resource, lblAccommodationSummary %>" runat="server" 
                            Font-Bold="True" />
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="border: 1px solid #E5C365;">
                        <table border="0" cellpadding="2" cellspacing="0">
                            <asp:Repeater ID="rptServicesToAdd" runat="server" 
                                onitemcommand="rptServicesToAdd_ItemCommand">
                                <HeaderTemplate>
                                    <tr>
                                        <th style="text-align:left;"><%#Resources.Resource.lblServices %></th>
                                        <th style="width:50px; text-align:right;">Qty</th>
                                        <th style="width:50px"></th>
                                    </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>                                            
                                            <asp:Label ID="lblDesc" Text='<%#Eval("ProcessDesc")%>' runat="server" />
                                        </td>
                                        <td style="width:50px; text-align:right;">
                                            <asp:TextBox ID="txtQty" runat="server" Width="25px" MaxLength="2" Text="1" />
                                        </td>
                                        <td>                                            
                                            <asp:LinkButton ID="lnkAdd" CssClass="inbiz_icon icon_add_24x24" Text="" runat="server" CausesValidation="false" CommandName="addtocart" CommandArgument='<%#Eval("ProcessID")%>'/>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                        <%--<asp:CheckBoxList ID="chkServices" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="chkServices_SelectedIndexChanged">                                                       
                        </asp:CheckBoxList>--%>
                        <asp:Literal ID="ltProcess" Text="" runat="server" />                        
                    </td>
                    <td valign="top" style="border: 1px solid #E5C365;">
                        <div id="tabs">
                            <ul>
                                <asp:Repeater ID="rptGuest" runat="server">
                                    <ItemTemplate>
                                         <li><a href='#tabs-<%#Eval("GuestNo")%>'>                                                
                                                <asp:Literal ID="ltGuest" Text="<%$Resources:Resource, lblGuest %>" runat="server" /> #
                                                <%#Eval("GuestNo")%></a> </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                            <asp:Repeater ID="rptGuestInfo" runat="server">
                                <ItemTemplate>
                                    <div id='tabs-<%#Eval("GuestNo")%>'>
                                        <asp:HiddenField ID="hdnBedID" runat="server" Value='<%#Eval("BedID")%>' />
                                        <table border="0" cellpadding="2" cellspacing="5">
                                            <tr>
                                                <td style="width: 120px;">                                                    
                                                    <asp:Label ID="lblCheckInDate" Text="<%$Resources:Resource, lblCheckIn%>" runat="server" />
                                                </td>
                                                <td>
                                                    <%#Eval("CheckInDate", "{0:dddd dd MMMM yyyy}")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>                                                                                            
                                                    <asp:Label ID="lblCheckOutDate" Text="<%$Resources:Resource, lblCheckout%>" runat="server" />
                                                </td>
                                                <td>
                                                    <%#Eval("CheckOutDate", "{0:dddd dd MMMM yyyy}")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>                                                    
                                                    <asp:Label ID="lblNoOfNights" Text="<%$Resources:Resource, lblNoOfNights %>" runat="server" />
                                                </td>
                                                <td>
                                                    <%#Eval("NoOfNight")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>                                                    
                                                    <asp:Label ID="lblNoOfGuest" Text="<%$Resources:Resource, lblNoOfGuest %>" runat="server" />
                                                </td>
                                                <td>
                                                    <%#Eval("NoOfGuest")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>                                                    
                                                    <asp:Label ID="lblRoomType" Text="<%$Resources:Resource, lblRoomType %>" runat="server" />
                                                </td>
                                                <td>
                                                   <%#Eval("RoomTypeDesc")%> (<%#Eval("UnitPrice", "<span style='font-weight:bold; font-size:14px;'>$ </span>{0:F}")%><%#Resources.Resource.lblPerNight%>)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>                                                    
                                                    <asp:Label ID="lblAmenities" Text="<%$Resources:Resource, lblAmenities%>" runat="server" />
                                                </td>
                                                <td>
                                                    <%#Eval("Amenities")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>                                                    
                                                    <asp:Label ID="lblSubTotal" Text="<%$Resources:Resource, lblSubTotal%>" runat="server" />
                                                </td>
                                                <td>
                                                    <%#Eval("SubTotal", "<span style='font-weight:bold; font-size:14px;'>$ </span>{0:F}")%>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <table border="0" cellpadding="2" cellspacing="5" style="margin-left: 16px;">
                                <asp:Repeater ID="rptServices" runat="server" OnItemCommand="rptServices_ItemCommand">
                                    <ItemTemplate>
                                        <tr style="">
                                            <td style="width: 120px;">
                                                <asp:Label ID="lblServiceDesc" Text='<%#Eval("ProcessDesc")%>' runat="server" />
                                            </td>
                                            <td>
                                                <%#string.Format("{1} <b>x</b> {0:F}=", Eval("ProcessCost"), Eval("ProcessUnits"))%>
                                                <%#string.Format("<span style='font-weight:bold; font-size:14px;'>$ </span>{0:F}", Eval("ProcessSubTotal"))%>                                                
                                            </td>
                                            <td>                                                
                                                <asp:LinkButton ID="lnkDelete" Text="<%$Resources:Resource,delete%>" runat="server" CausesValidation="false" CommandName="delete" CommandArgument='<%#Eval("ProcessID")%>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr>
                                    <td style="width: 120px;font-size:14px; font-weight:bold; border-top:1px solid #ccc">                                        
                                        <asp:Label ID="lblTotal" Text="<%$Resources:Resource, lblTotal%>" runat="server" />
                                    </td>
                                    <td style="font-size:14px;font-weight:bold; border-top:1px solid #ccc">
                                        <asp:Literal ID="ltTotal" Text="" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <div style="text-align: right; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;"> 
                <div id="divPaymentOptions" title="Checkout Options">
                    <div style="text-align:center;">
                        <asp:RadioButton ID="rdoEvo" GroupName="payopt" Text="" runat="server" Checked="true" />
                        <asp:Label ID="lblEvo" AssociatedControlID="rdoEvo" Text="" runat="server">
                            <asp:Image ID="Image2" ImageUrl="~/Sivananda/images/EVO_logo.jpg" runat="server"
                                Style="vertical-align: middle" />
                        </asp:Label>
                        <asp:RadioButton ID="rdoPaypal" GroupName="payopt" Text="" runat="server"  />
                        <asp:Label ID="Label2" AssociatedControlID="rdoPaypal" Text="" runat="server">
                            <asp:Image ID="Image1" ImageUrl="~/Sivananda/images/PayPal_mark_60x38.gif" runat="server"
                                Style="vertical-align: middle" />
                        </asp:Label>                        
                       </div>
                        <br />
                    <div class="div_command">
                        <%--<asp:Button ID="btnAddServices" Text="<%$Resources:Resource,lblAddServicesAndCheckout%>"
                            runat="server" OnClick="btnAddServices_Click" />
                        <asp:Button ID="btnSkipAndCheckout" Text="<%$Resources:Resource, lblSkipAndCheckout %>"
                            runat="server" OnClick="btnSkipAndCheckout_Click" />--%>
                    </div>
                </div>
                <%-- <div>
                            <input type="checkbox" id="chkRules" runat="server" />
                            <asp:Label ID="lblRules" AssociatedControlID="chkRules" Font-Bold="true" runat="server">
                            <%=Resources.Resource.lblRules%>
                            </asp:Label>
                        </div>--%>
                    <asp:Button ID="Button1" CssClass="chekcout_options_pop" Text="<%$Resources:Resource,lblAddServicesAndCheckout%>" runat="server" Visible="false"  />
                    <asp:Button ID="Button2" CssClass="chekcout_options_pop" Text="<%$Resources:Resource, lblSkipAndCheckout%>" runat="server" Visible="false" />                                            

                    <asp:Button ID="btnEditReservation" 
                    Text="<%$Resources:Resource, lblEditReservation%>" runat="server" 
                    onclick="btnEditReservation_Click" />
                     <asp:Button ID="btnAddServices"  CssClass="btn_check_rules" Text="<%$Resources:Resource,lblAddServicesAndCheckout%>"
                            runat="server" OnClick="btnAddServices_Click" />
                        <asp:Button ID="btnSkipAndCheckout"  CssClass="btn_check_rules" Text="<%$Resources:Resource, lblSkipAndCheckout %>"
                            runat="server" OnClick="btnSkipAndCheckout_Click" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $("#tabs").tabs();

        $("#divPaymentOptions").dialog({
            modal: true,
            autoOpen: false,
            resizable: false,
            width: 500,
            height: 150
        }).parent().appendTo(jQuery("form:first"));

        $(".chekcout_options_pop").click(function () {
            $("#divPaymentOptions").dialog("open");
            return false;
        });

        /*$(".btn_check_rules").click(function () {
            if (!$("#<=chkRules.ClientID>").is(":checked")) {
                alert("<%=Resources.Resource.msgAgreeRules%>");
                return false;
            }            
        });*/
    </script>
</asp:Content>


﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Sivananda/sivananda.master" CodeFile="evosuccess.aspx.cs" Inherits="Sivananda_evosuccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript">
        window.history.forward();
        function noBack() { window.history.forward(); }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFullWidth" runat="Server">
    <div style="border: 1px solid #FF7300; margin: 10px auto; padding: 5px; font-size:12px;">
        <div id="divReceipt" runat="server">
        </div>
        <div id="divFailed" runat="server" visible="false">
            <p>
                Transaction was Failed!
            </p>
            <p>
                <%Response.Write(Request.QueryString["responsetext"]);%><br />
                <a href="Default.aspx">Try again!</a>
            </p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(noBack);
    </script>
</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Sivananda_GuestInfo : PayPalBasePage
{
    AccommodationSearch _srch = new AccommodationSearch();
    List<SearchResult> _srchResult;

    protected void Page_Load(object sender, EventArgs e)
    {              
        _srchResult = _srch.SearchedData;  
        if (_srchResult.Count <= 0)        
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPleaseSearchAccommodation);
            Response.Redirect("Default.aspx?lang=" + Globals.CurrentAppLanguageCode);
        }

        if (!IsPostBack)
        {
            Session["CURRENT_GUEST_LIST"] = null;
          
            DropDownHelper.FillDropdown(rblMember, "CU", "dlMem", Globals.CurrentAppLanguageCode, null);

            //To do here if guest came as logged in guest Fill form & need to hide login 
            rblSex.SelectedValue = _srchResult[0].Sex;
            chkCouple.Checked = _srchResult[0].IsCouple;
            rblSex.Enabled = false;
            chkCouple.Enabled = false;
            ltCheckIn.Text = BusinessUtility.GetDateTimeString(_srchResult[0].CheckInDate, DateFormat.MMddyyyy);
            ltCheckOut.Text = BusinessUtility.GetDateTimeString(_srchResult[0].CheckOutDate, DateFormat.MMddyyyy);

            //rptGuests.DataSource = _srchResult;
            //rptGuests.DataBind();

            //tblOtherGuests.Visible = !(_srchResult.Count <= 1);

            if (SivanandaSession.IsAuthenticated)
            {                
                //Fill Guest Information
                FillFormData();
            }
            else
            {
                pnlCustomerInfo.Visible = false;
                pnlGuestInfo.Visible = false;
            }
        }
    }

    private void FillFormData()
    {
        Partners cust = new Partners();
        cust.PopulateObject(SivanandaSession.CurrentGuest.PartnerID);
        Session["CURRENT_GUEST_LIST"] = SivanandaSession.CurrentGuest.PartnerID;
        Addresses billToAddress = new Addresses();
        Addresses shipToAddress = new Addresses();

        billToAddress = cust.GetBillToAddress(cust.PartnerID, cust.PartnerType);
        shipToAddress = cust.GetShipToAddress(cust.PartnerID, cust.PartnerType);

        txtEmailAddress.Text = cust.PartnerEmail;
        //string.Format("{0} {1}", txtLastName.Text, txtFirstName.Text) = cust.PartnerLongName;
        txtCustomerNote.Text = cust.PartnerNote;
        txtTelephone.Text = cust.PartnerPhone;

        txtCity.Text = billToAddress.AddressCity;
        txtCountry.Text = billToAddress.AddressCountry;
        txtAddressLine1.Text = billToAddress.AddressLine1;
        txtAddressLine2.Text = billToAddress.AddressLine2;
        txtPostalCode.Text = billToAddress.AddressPostalCode;
        txtState.Text = billToAddress.AddressState;


        txtDOB.Text = BusinessUtility.GetDateTimeString(cust.ExtendedProperties.DOB, DateFormat.MMddyyyy);
        txtAlterTelephone.Text = cust.ExtendedProperties.EmergencyAltTelephone;
        txtEmName.Text = cust.ExtendedProperties.EmergencyContactName;
        txtEmRelationship.Text = cust.ExtendedProperties.EmergencyRelationship;
        cust.ExtendedProperties.EmergencyTelephone = txtEmTelephone.Text;
        txtFirstName.Text = cust.ExtendedProperties.FirstName;
        rblMember.SelectedValue = cust.ExtendedProperties.IsMember;
        txtLastName.Text = cust.ExtendedProperties.LastName;
        txtMembership.Text = cust.ExtendedProperties.MembershipID;
        chkNewsLetter.Checked = cust.ExtendedProperties.ReceiveNewsLetters;
        //cust.ExtendedProperties.GuestType = (int)StatusGuestType.Guest;  //no need to modify it just keep same guest type as previousl saved
        rblSex.SelectedValue = cust.ExtendedProperties.Sex;
        txtSpritualName.Text = cust.ExtendedProperties.SpirtualName;
    }

    protected void btnCheckout_Command(object sender, CommandEventArgs e)
    {
        pnlConfirmationReceivePassScript.Visible = false;
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            Session["CURRENT_GUEST_RESERVATION_NOTES"] = txtReservationNote.Text;

            //Save Or Overrite Guest Information
            Partners cust = this.SavePartnerInfo(dbHelp);
            Session["CURRENT_GUEST_LIST"] = cust.PartnerID;
            //Crete Reservation
            /*Reservation rsv = new Reservation();
            rsv.NoOfGuestOlderFeMale = 0;
            rsv.NoOfGuestOlderMale = 0;
            rsv.NoOfGuestYoungerFeMale = 0;
            rsv.NoOfGuestYoungerMale = 0;
            rsv.PrimaryPartnerID = cust.PartnerID;
            rsv.ReservationStatus = (int)StatusReservation.New;
            rsv.ReserveFor = cust.ExtendedProperties.GuestType;
            rsv.ReservationNote = txtReservationNote.Text;

            rsv.Insert(CurrentUser.UserID); //Crate Reservation & aslo 

            List<ReservationItems> lstRsvItems = new List<ReservationItems>();
            //Reservation Item
            if (rsv.ReservationID > 0)
            {
                foreach (var item in _srchResult)
                {
                    ReservationItems ri = new ReservationItems();
                    ri.BedID = item.BedID;
                    ri.CheckInDate = _srchResult[0].CheckInDate;
                    ri.CheckOutDate = _srchResult[0].CheckOutDate;
                    ri.GuestID = cust.PartnerID;
                    ri.PricePerDay = item.UnitPrice;
                    ri.ReservationID = rsv.ReservationID;
                    ri.IsCouple = item.IsCouple;
                    lstRsvItems.Add(ri);
                }

                //foreach (RepeaterItem itm in rptGuests.Items)
                //{
                //    TextBox txtFirstNameG = (TextBox)itm.FindControl("txtFirstNameG");
                //    TextBox txtLastNameG = (TextBox)itm.FindControl("txtLastNameG");
                //    TextBox txtEmailAddressG = (TextBox)itm.FindControl("txtEmailAddressG");
                //    HiddenField hdnGuestNo = (HiddenField)itm.FindControl("hdnGuestNo");
                //    HiddenField hdnBedID = (HiddenField)itm.FindControl("hdnBedID");
                //    HiddenField hdnSex = (HiddenField)itm.FindControl("hdnSex");
                //    HiddenField hdnUnitPrice = (HiddenField)itm.FindControl("hdnUnitPrice");

                //    if (BusinessUtility.GetInt(hdnGuestNo.Value) > 1)
                //    {

                //        Partners gst = this.SaveOtherGuestInfo(txtFirstNameG.Text, txtLastNameG.Text, txtEmailAddressG.Text, hdnSex.Value);
                //        if (gst.PartnerID > 0)
                //        {
                //            ReservationItems ri = new ReservationItems();
                //            ri.BedID = BusinessUtility.GetInt(hdnBedID.Value);
                //            ri.CheckInDate = _srchResult[0].CheckInDate;
                //            ri.CheckOutDate = _srchResult[0].CheckOutDate;
                //            ri.GuestID = gst.PartnerID;
                //            ri.PricePerDay = BusinessUtility.GetDouble(hdnUnitPrice.Value);
                //            ri.ReservationID = rsv.ReservationID;
                //            lstRsvItems.Add(ri);
                //        }
                //    }
                //    else
                //    {
                //        ReservationItems ri = new ReservationItems();
                //        ri.BedID = BusinessUtility.GetInt(hdnBedID.Value);
                //        ri.CheckInDate = _srchResult[0].CheckInDate;
                //        ri.CheckOutDate = _srchResult[0].CheckOutDate;
                //        ri.GuestID = cust.PartnerID;
                //        ri.PricePerDay = BusinessUtility.GetDouble(hdnUnitPrice.Value);
                //        ri.ReservationID = rsv.ReservationID;
                //        lstRsvItems.Add(ri);
                //    }
                //}

                ReservationItems rItem = new ReservationItems();
                rItem.AddList(lstRsvItems);

                switch (e.CommandName)
                {
                    case "SERVICE_DISCOUNT":
                        Response.Redirect("AddServices.aspx?rsvid=" + rsv.ReservationID + "&pid=" + cust.PartnerID + "&lang=" + Globals.CurrentAppLanguageCode, false);
                        break;
                    case "CHECKOUT":
                        //Response.Redirect("Checkout.aspx"); 
                        //& validate for existance Create Order
                        int oid = this.PlaceOrder(rsv.ReservationID, new List<int>().ToArray(), cust);
                        if (oid > 0)
                        {
                            TotalSummary total = CalculationHelper.GetOrderTotal(oid);
                            if (rdoEvo.Checked)
                            {
                                Response.Redirect("evopost.aspx?oid=" + oid + "&lang=" + Globals.CurrentAppLanguageCode, false);
                            }
                            else if (rdoPaypal.Checked)
                            {
                                SetProfile.SessionProfile = SetProfile.CreateAPIProfile(PaypalConstants.API_USERNAME, PaypalConstants.API_PASSWORD, PaypalConstants.API_SIGNATURE, "", "", PaypalConstants.CERTIFICATE, PaypalConstants.PRIVATE_KEY_PASSWORD);
                                this.PaymentAction = com.paypal.soap.api.PaymentActionCodeType.Sale;
                                Response.Redirect("ReviewOrder.aspx?" + PaypalConstants.PAYMENT_AMOUNT_PARAM + "=" + string.Format("{0:F}", total.GrandTotal) + "&" + PaypalConstants.PAYMENT_CURRENCY_PARAM + "=" + "CAD" + "&oid=" + oid, false);
                            }
                        }
                        else
                        {
                            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgAccommodationNoLongerAvailable);
                        }
                        break;
                }
            } */

            if (Session["CURRENT_GUEST_LIST"] != null && (int)Session["CURRENT_GUEST_LIST"] > 0)
            {
                switch (e.CommandName)
                {
                    case "SERVICE_DISCOUNT":
                        Response.Redirect("AddServices.aspx?lang=" + Globals.CurrentAppLanguageCode, false);
                        break;
                    case "CHECKOUT":
                        Response.Redirect("evopost.aspx?lang=" + Globals.CurrentAppLanguageCode, false);
                        break;
                }
            }
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.DUPLICATE_KEY)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgEmailIDAlreadyExists);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private Partners SavePartnerInfo(DbHelper dbHelp)
    {        
        try
        {
            //Save Customer Information in CRM
            Partners cust = new Partners();
            Addresses billToAddress = new Addresses();
            Addresses shipToAddress = new Addresses();
            List<Addresses> lstAddress = new List<Addresses>();

            if (SivanandaSession.IsAuthenticated)
            {
                cust.PopulateObject(dbHelp, SivanandaSession.CurrentGuest.PartnerID);
            }
            else
            {
                cust.PopulateObject(dbHelp, txtEmailAddress.Text); //populate Partner by emailid if exists
                if (cust.PartnerID > 0)
                {                    
                    pnlConfirmationReceivePassScript.Visible = true;
                    throw new Exception(CustomExceptionCodes.DUPLICATE_KEY);
                }
            }

            //Check if partner exists        
            if (cust.PartnerID > 0)
            {
                billToAddress = cust.GetBillToAddress(dbHelp, cust.PartnerID, cust.PartnerType);
                shipToAddress = cust.GetShipToAddress(dbHelp, cust.PartnerID, cust.PartnerType);

                cust.PartnerEmail = txtEmailAddress.Text;
                cust.PartnerLongName = string.Format("{0} {1}", txtLastName.Text, txtFirstName.Text);
                cust.PartnerNote = txtCustomerNote.Text;
                cust.PartnerPhone = txtTelephone.Text;

                billToAddress.AddressCity = txtCity.Text;
                billToAddress.AddressCountry = txtCountry.Text;
                billToAddress.AddressLine1 = txtAddressLine1.Text;
                billToAddress.AddressLine2 = txtAddressLine2.Text;
                billToAddress.AddressPostalCode = txtPostalCode.Text;
                billToAddress.AddressState = txtState.Text;
                billToAddress.AddressRef = AddressReference.END_CLIENT;
                billToAddress.AddressType = AddressType.BILL_TO_ADDRESS;
                lstAddress.Add(billToAddress);

                shipToAddress.AddressCity = txtCity.Text;
                shipToAddress.AddressCountry = txtCountry.Text;
                shipToAddress.AddressLine1 = txtAddressLine1.Text;
                shipToAddress.AddressLine2 = txtAddressLine2.Text;
                shipToAddress.AddressPostalCode = txtPostalCode.Text;
                shipToAddress.AddressState = txtState.Text;
                shipToAddress.AddressRef = AddressReference.END_CLIENT;
                shipToAddress.AddressType = AddressType.SHIP_TO_ADDRESS;
                lstAddress.Add(shipToAddress);

                cust.ExtendedProperties.PartnerID = cust.PartnerID;
                cust.ExtendedProperties.Age = 0;
                cust.ExtendedProperties.DOB = BusinessUtility.GetDateTime(txtDOB.Text, DateFormat.MMddyyyy);
                cust.ExtendedProperties.EmergencyAltTelephone = txtAlterTelephone.Text;
                cust.ExtendedProperties.EmergencyContactName = txtEmName.Text;
                cust.ExtendedProperties.EmergencyRelationship = txtEmRelationship.Text;
                cust.ExtendedProperties.EmergencyTelephone = txtEmTelephone.Text;
                cust.ExtendedProperties.FirstName = txtFirstName.Text;
                cust.ExtendedProperties.IsMember = rblMember.SelectedValue;
                cust.ExtendedProperties.LastName = txtLastName.Text;
                cust.ExtendedProperties.MembershipID = txtMembership.Text;
                cust.ExtendedProperties.ReceiveNewsLetters = chkNewsLetter.Checked;
                //cust.ExtendedProperties.GuestType = (int)StatusGuestType.Guest;  //no need to modify it just keep same guest type as previousl saved
                cust.ExtendedProperties.Sex = rblSex.SelectedValue;
                cust.ExtendedProperties.SpirtualName = txtSpritualName.Text;

                string dulicateEmailAddress = System.Configuration.ConfigurationManager.AppSettings["DuplicatEmailAddress"];
                cust.PartnerEmail = cust.PartnerEmail.ToLower().Trim() == dulicateEmailAddress.ToLower().Trim() ? string.Format("{0:yyyyMMddhhmmss}_{1}", DateTime.Now, dulicateEmailAddress) : cust.PartnerEmail;
                cust.Update(dbHelp, CurrentUser.UserID, lstAddress);
            }
            else
            {
                //Retreive Whs Info
                SysWarehouses whs = new SysWarehouses();
                whs.PopulateObject(ConfigurationManager.AppSettings["WebSaleWhsCode"], dbHelp);

                SysCompanyInfo ci = new SysCompanyInfo();
                ci.PopulateObject(whs.WarehouseCompanyID, dbHelp);

                //SysCompanyInfo ci = new SysCompanyInfo();
                //ci.PopulateObject(CurrentUser.DefaultCompanyID);
                //currencyCode = ci.CompanyBasCur;

                //SysWarehouses whs = new SysWarehouses();
                //whs.PopulateObject(CurrentUser.UserDefaultWarehouse);
                //taxCode = whs.WarehouseRegTaxCode;

                cust.PartnerActive = true;
                cust.PartnerEmail = txtEmailAddress.Text;
                cust.PartnerLongName = string.Format("{0} {1}", txtLastName.Text, txtFirstName.Text);
                cust.PartnerNote = txtCustomerNote.Text;
                cust.PartnerPhone = txtTelephone.Text;
                cust.PartnerStatus = StatusPartnerStatus.ACTIVE;
                cust.PartnerTaxCode = whs.WarehouseRegTaxCode;
                cust.PartnerType = (int)PartnerTypeIDs.EndClient;
                cust.PartnerCurrencyCode = ci.CompanyBasCur;


                billToAddress.AddressCity = txtCity.Text;
                billToAddress.AddressCountry = txtCountry.Text;
                billToAddress.AddressLine1 = txtAddressLine1.Text;
                billToAddress.AddressLine2 = txtAddressLine2.Text;
                billToAddress.AddressPostalCode = txtPostalCode.Text;
                billToAddress.AddressState = txtState.Text;
                billToAddress.AddressRef = AddressReference.END_CLIENT;
                billToAddress.AddressType = AddressType.BILL_TO_ADDRESS;
                lstAddress.Add(billToAddress);

                shipToAddress.AddressCity = txtCity.Text;
                shipToAddress.AddressCountry = txtCountry.Text;
                shipToAddress.AddressLine1 = txtAddressLine1.Text;
                shipToAddress.AddressLine2 = txtAddressLine2.Text;
                shipToAddress.AddressPostalCode = txtPostalCode.Text;
                shipToAddress.AddressState = txtState.Text;
                shipToAddress.AddressRef = AddressReference.END_CLIENT;
                shipToAddress.AddressType = AddressType.SHIP_TO_ADDRESS;
                lstAddress.Add(shipToAddress);

                cust.ExtendedProperties.Age = 0;
                cust.ExtendedProperties.DOB = BusinessUtility.GetDateTime(txtDOB.Text, DateFormat.MMddyyyy);
                cust.ExtendedProperties.EmergencyAltTelephone = txtAlterTelephone.Text;
                cust.ExtendedProperties.EmergencyContactName = txtEmName.Text;
                cust.ExtendedProperties.EmergencyRelationship = txtEmRelationship.Text;
                cust.ExtendedProperties.EmergencyTelephone = txtEmTelephone.Text;
                cust.ExtendedProperties.FirstName = txtFirstName.Text;
                cust.ExtendedProperties.IsMember = rblMember.SelectedValue;
                cust.ExtendedProperties.LastName = txtLastName.Text;
                cust.ExtendedProperties.MembershipID = txtMembership.Text;
                cust.ExtendedProperties.ReceiveNewsLetters = chkNewsLetter.Checked;
                cust.ExtendedProperties.GuestType = (int)StatusGuestType.Guest; //explicitly set as guest type
                cust.ExtendedProperties.Sex = rblSex.SelectedValue;
                cust.ExtendedProperties.SpirtualName = txtSpritualName.Text;

                string dulicateEmailAddress = System.Configuration.ConfigurationManager.AppSettings["DuplicatEmailAddress"];
                cust.PartnerEmail = cust.PartnerEmail.ToLower().Trim() == dulicateEmailAddress.ToLower().Trim() ? string.Format("{0:yyyyMMddhhmmss}_{1}", DateTime.Now, dulicateEmailAddress) : cust.PartnerEmail;
                cust.Insert(dbHelp, CurrentUser.UserID, lstAddress);
            }

            return cust;
        }
        catch(Exception ex) 
        {
            
            throw;
        }
    }

    private Partners SaveOtherGuestInfo(DbHelper dbHelp, string fName, string lName, string email, string sex)
    {
        try
        {
            //Save Customer Information in CRM
            Partners cust = new Partners();
            Addresses billToAddress = new Addresses();
            Addresses shipToAddress = new Addresses();
            List<Addresses> lstAddress = new List<Addresses>();

            cust.PopulateObject(dbHelp, email); //populate Partner by emailid if exists

            //Check if partner exists        
            if (cust.PartnerID > 0)
            {
                billToAddress = cust.GetBillToAddress(cust.PartnerID, cust.PartnerType);
                shipToAddress = cust.GetShipToAddress(cust.PartnerID, cust.PartnerType);

                cust.PartnerEmail = email;
                cust.PartnerLongName = string.Format("{0} {1}", lName, fName);
                cust.PartnerNote = txtCustomerNote.Text;
                cust.PartnerPhone = txtTelephone.Text;

                billToAddress.AddressCity = txtCity.Text;
                billToAddress.AddressCountry = txtCountry.Text;
                billToAddress.AddressLine1 = txtAddressLine1.Text;
                billToAddress.AddressLine2 = txtAddressLine2.Text;
                billToAddress.AddressPostalCode = txtPostalCode.Text;
                billToAddress.AddressState = txtState.Text;
                billToAddress.AddressRef = AddressReference.END_CLIENT;
                billToAddress.AddressType = AddressType.BILL_TO_ADDRESS;
                lstAddress.Add(billToAddress);

                shipToAddress.AddressCity = txtCity.Text;
                shipToAddress.AddressCountry = txtCountry.Text;
                shipToAddress.AddressLine1 = txtAddressLine1.Text;
                shipToAddress.AddressLine2 = txtAddressLine2.Text;
                shipToAddress.AddressPostalCode = txtPostalCode.Text;
                shipToAddress.AddressState = txtState.Text;
                shipToAddress.AddressRef = AddressReference.END_CLIENT;
                shipToAddress.AddressType = AddressType.SHIP_TO_ADDRESS;
                lstAddress.Add(shipToAddress);

                cust.ExtendedProperties.PartnerID = cust.PartnerID;
                cust.ExtendedProperties.Age = 0;
                //cust.ExtendedProperties.DOB = BusinessUtility.GetDateTime(txtDOB.Text, DateFormat.MMddyyyy);
                cust.ExtendedProperties.EmergencyAltTelephone = txtAlterTelephone.Text;
                cust.ExtendedProperties.EmergencyContactName = txtEmName.Text;
                cust.ExtendedProperties.EmergencyRelationship = txtEmRelationship.Text;
                cust.ExtendedProperties.EmergencyTelephone = txtEmTelephone.Text;
                cust.ExtendedProperties.FirstName = fName;
                //cust.ExtendedProperties.IsMember = rblMember.SelectedValue;
                cust.ExtendedProperties.LastName = lName;
                //cust.ExtendedProperties.MembershipID = txtMembership.Text;
                //cust.ExtendedProperties.ReceiveNewsLetters = chkNewsLetter.Checked;
                //cust.ExtendedProperties.GuestType = (int)StatusGuestType.Guest;  //no need to modify it just keep same guest type as previousl saved
                cust.ExtendedProperties.Sex = sex;
                //cust.ExtendedProperties.SpirtualName = txtSpritualName.Text;

                string dulicateEmailAddress = System.Configuration.ConfigurationManager.AppSettings["DuplicatEmailAddress"];
                cust.PartnerEmail = cust.PartnerEmail.ToLower().Trim() == dulicateEmailAddress.ToLower().Trim() ? string.Format("{0:yyyyMMddhhmmss}_{1}", DateTime.Now, dulicateEmailAddress) : cust.PartnerEmail;
                cust.Update(dbHelp, CurrentUser.UserID, lstAddress);
            }
            else
            {
                //Retreive Whs Info
                SysWarehouses whs = new SysWarehouses();
                whs.PopulateObject(ConfigurationManager.AppSettings["WebSaleWhsCode"], dbHelp);

                SysCompanyInfo ci = new SysCompanyInfo();
                ci.PopulateObject(whs.WarehouseCompanyID, dbHelp);

                //SysCompanyInfo ci = new SysCompanyInfo();
                //ci.PopulateObject(CurrentUser.DefaultCompanyID);
                //currencyCode = ci.CompanyBasCur;

                //SysWarehouses whs = new SysWarehouses();
                //whs.PopulateObject(CurrentUser.UserDefaultWarehouse);
                //taxCode = whs.WarehouseRegTaxCode;

                cust.PartnerActive = true;
                cust.PartnerEmail = email;
                cust.PartnerLongName = string.Format("{0} {1}", lName, fName);
                cust.PartnerNote = txtCustomerNote.Text;
                cust.PartnerPhone = txtTelephone.Text;
                cust.PartnerStatus = StatusPartnerStatus.ACTIVE;
                cust.PartnerTaxCode = whs.WarehouseRegTaxCode;
                cust.PartnerType = (int)PartnerTypeIDs.EndClient;
                cust.PartnerCurrencyCode = ci.CompanyBasCur;


                billToAddress.AddressCity = txtCity.Text;
                billToAddress.AddressCountry = txtCountry.Text;
                billToAddress.AddressLine1 = txtAddressLine1.Text;
                billToAddress.AddressLine2 = txtAddressLine2.Text;
                billToAddress.AddressPostalCode = txtPostalCode.Text;
                billToAddress.AddressState = txtState.Text;
                billToAddress.AddressRef = AddressReference.END_CLIENT;
                billToAddress.AddressType = AddressType.BILL_TO_ADDRESS;
                lstAddress.Add(billToAddress);

                shipToAddress.AddressCity = txtCity.Text;
                shipToAddress.AddressCountry = txtCountry.Text;
                shipToAddress.AddressLine1 = txtAddressLine1.Text;
                shipToAddress.AddressLine2 = txtAddressLine2.Text;
                shipToAddress.AddressPostalCode = txtPostalCode.Text;
                shipToAddress.AddressState = txtState.Text;
                shipToAddress.AddressRef = AddressReference.END_CLIENT;
                shipToAddress.AddressType = AddressType.SHIP_TO_ADDRESS;
                lstAddress.Add(shipToAddress);

                cust.ExtendedProperties.Age = 0;
                //cust.ExtendedProperties.DOB = BusinessUtility.GetDateTime(txtDOB.Text, DateFormat.MMddyyyy);
                cust.ExtendedProperties.EmergencyAltTelephone = txtAlterTelephone.Text;
                cust.ExtendedProperties.EmergencyContactName = txtEmName.Text;
                cust.ExtendedProperties.EmergencyRelationship = txtEmRelationship.Text;
                cust.ExtendedProperties.EmergencyTelephone = txtEmTelephone.Text;
                cust.ExtendedProperties.FirstName = fName;
                //cust.ExtendedProperties.IsMember = rblMember.SelectedValue;
                cust.ExtendedProperties.LastName = lName;
                //cust.ExtendedProperties.MembershipID = txtMembership.Text;
                //cust.ExtendedProperties.ReceiveNewsLetters = chkNewsLetter.Checked;
                cust.ExtendedProperties.GuestType = (int)StatusGuestType.Guest; //explicitly set as guest type
                cust.ExtendedProperties.Sex = sex;
                //cust.ExtendedProperties.SpirtualName = txtSpritualName.Text;

                string dulicateEmailAddress = System.Configuration.ConfigurationManager.AppSettings["DuplicatEmailAddress"];
                cust.PartnerEmail = cust.PartnerEmail.ToLower().Trim() == dulicateEmailAddress.ToLower().Trim() ? string.Format("{0:yyyyMMddhhmmss}_{1}", DateTime.Now, dulicateEmailAddress) : cust.PartnerEmail;
                cust.Insert(dbHelp, CurrentUser.UserID, lstAddress);
            }

            return cust;
        }
        catch
        {
            throw;
        }

    }

    //private int PlaceOrder(int reservationID, int[] processID, Partners primaryPartner)
    //{
    //    List<Bed> lstReservedBeds = new Bed().GetListByReservationID(reservationID);

    //    //Check beds avaialability for the last time before creating reservation
    //    if (ProcessReservation.IsAccommodationAvailalbe(lstReservedBeds) == false)
    //    {
    //        return 0;
    //    }

    //    Orders ord = new Orders();
    //    SysCompanyInfo cinf = new SysCompanyInfo();
    //    cinf.PopulateObject(CurrentUser.DefaultCompanyID);

    //    ord.OrdCompanyID = CurrentUser.DefaultCompanyID;
    //    ord.OrdCreatedBy = CurrentUser.UserID;
    //    ord.OrdCreatedFromIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
    //    ord.OrdCurrencyCode = primaryPartner.PartnerCurrencyCode;
    //    ord.OrdCurrencyExRate = SysCurrencies.GetRelativePrice(ord.OrdCurrencyCode);
    //    ord.OrdCustID = primaryPartner.PartnerID;
    //    ord.OrdCustType = Globals.GetPartnerType(primaryPartner.PartnerType);
    //    ord.OrderTypeCommission = (int)OrderCommission.Reservation;
    //    ord.OrdLastUpdateBy = CurrentUser.UserID;
    //    ord.OrdSalesRepID = CurrentUser.UserID;
    //    ord.OrdSaleWeb = true;
    //    ord.OrdShippingTerms = cinf.CompanyShpToTerms;
    //    ord.OrdShpBlankPref = false;
    //    ord.OrdShpCode = string.Empty;
    //    ord.OrdShpCost = 0;
    //    ord.OrdShpDate = DateTime.Now;
    //    ord.OrdShpTrackNo = string.Empty;
    //    ord.OrdShpWhsCode = CurrentUser.UserDefaultWarehouse;
    //    ord.OrdStatus = SOStatus.IN_PROGRESS;
    //    ord.OrdType = StatusSalesOrderType.QUOTATION;
    //    ord.OrdVerified = true;
    //    ord.OrdVerifiedBy = CurrentUser.UserID;
    //    ord.QutExpDate = DateTime.Now;
    //    ord.OrdNetTerms = string.Empty;
    //    ord.OrderRejectReason = string.Empty;

    //    Reservation rsv = new Reservation();
    //    rsv.PopulateObject(reservationID);

    //    ord.OrdComment = rsv.ReservationNote;
    //    ord.Insert(CurrentUser.UserID); //Insert Order

    //    int soid = ord.OrdID;
    //    if (soid > 0)
    //    {
    //        List<OrderItems> itms = new List<OrderItems>();
    //        foreach (Bed item in lstReservedBeds)
    //        {
    //            OrderItems oitem = new OrderItems();
    //            oitem.OrderItemDesc = item.ToString();
    //            oitem.OrdID = soid;
    //            oitem.OrdProductDiscount = 0;
    //            oitem.OrdProductDiscountType = "P";
    //            oitem.OrdProductID = item.BedID;
    //            oitem.OrdProductQty = item.TotalDays;
    //            oitem.OrdProductTaxGrp = 0;//primaryPartner.PartnerTaxCode;
    //            oitem.OrdProductUnitPrice = item.BedPrice;
    //            oitem.OrdGuestID = item.GuestID;
    //            oitem.OrdReservationItemID = item.ReservationItemID;
    //            itms.Add(oitem);
    //        }

    //        OrderItems processItems = new OrderItems();
    //        processItems.AddOrderItems(itms, CurrentUser.UserDefaultWarehouse, false); //Insert Order Items

    //        //Process Items to Order
    //        OrderItemProcess proc = new OrderItemProcess();
    //        proc.AddProcessToOrder(processID, soid);

    //        ProcessReservation.UpdateReservationStatus(reservationID, soid, StatusReservation.Processed);
    //    }

    //    return soid;
    //}

    protected void rptGuests_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            System.Web.UI.HtmlControls.HtmlTableRow trGuest = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trGuest");
            HiddenField hdnGuestNo = (HiddenField)e.Item.FindControl("hdnGuestNo");
            if (BusinessUtility.GetInt(hdnGuestNo.Value) <= 1)
            {
                trGuest.Style.Add(HtmlTextWriterStyle.Display, "none");

                ((RequiredFieldValidator)e.Item.FindControl("rfvFirstNameG")).Visible = false;
                ((RequiredFieldValidator)e.Item.FindControl("rfvLastNameG")).Visible = false;
                ((RequiredFieldValidator)e.Item.FindControl("rfvEmailAddresG")).Visible = false;                
            }
        }
    }
    protected void btnReceivePassword_Click(object sender, EventArgs e)
    {
        pnlConfirmationReceivePassScript.Visible = false;
        if (!string.IsNullOrEmpty(txtEmailAddress.Text))
        {
            Partners part = new Partners();
            try
            {
                string newPass = part.RestPassword(txtEmailAddress.Text);
                if (!string.IsNullOrEmpty(newPass))
                {
                    //To do here to email password to customer
                    string msg = string.Format("Dear Guest, <p>" + "Your Login Credentials are as follows:<br>EmailID={0}<br>Password={1}<p> Thank you <br>Sivananda", txtEmailAddress.Text, newPass);
                    EmailHelper.SendEmail(ConfigurationManager.AppSettings["NoReplyEmail"], txtEmailAddress.Text, msg, "Sivananda-Account Password Reset", true);
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgPasswordHasBeenSent);
                }
                //MessageState.SetGlobalMessage(MessageType.Failure, "Password could not be reset!");
            }
            catch(Exception ex)
            {
                if (ex.Message == CustomExceptionCodes.INVALID_EMAIL_ID)
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgProvidedEmailIDDoesNotExists);
                }
            }
        }
    }
    protected void btnEditReservation_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(SivanandaSession.SearchedHash))
        {
            Response.Redirect(SivanandaSession.SearchedHash);
        }
        else
        {
            Response.Redirect("Default.aspx?lang=" + Globals.CurrentAppLanguageCode);
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Sivananda/sivananda.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Sivananda_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">    
    <script src="../lib/scripts/jquery-plugins/jquery.addplaceholder.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFullWidth" Runat="Server">
    <div style="border:1px solid #FF7300; margin:10px auto;padding:5px;">        
        <asp:Panel ID="pnlFindAccommodation" runat="server" DefaultButton="btnFindAccommodation">
            <h2>
                <asp:Label ID="lblTitle" Text="" runat="server" />
            </h2>
            <div class="rsv_form">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="padding0" style="width: 150px;">
                        </td>
                        <td colspan="4">
                            <div class="divAutoCell">                                
                                <asp:Label ID="lblCheckIn" Text="<%$Resources:Resource, lblCheckIn%>" runat="server" />
                                <br />
                                <asp:TextBox ID="txtChekcIn" CssClass="datepicker" runat="server" Width="90px" ph="mm/dd/yyyy" />
                            </div>
                            <div class="divAutoCell">                                
                                <asp:Label ID="lblCheckOut" Text="<%$Resources:Resource, lblCheckout %>" runat="server" />
                                <br />
                                <asp:TextBox ID="txtCheckout" CssClass="datepicker" runat="server" Width="90px" ph="mm/dd/yyyy" />
                            </div>
                            <div class="divAutoCell">
                                <asp:Label ID="lblGuest" Text="<%$Resources:Resource, lblGuests %>" runat="server" />
                                <br />
                                <asp:DropDownList ID="ddlTotalGuests" runat="server" Width="50px" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlTotalGuests_SelectedIndexChanged">
                                    <asp:ListItem Text="1" />
                                    <asp:ListItem Text="2" />
                                    <asp:ListItem Text="3" />
                                    <asp:ListItem Text="4" />
                                    <asp:ListItem Value="5" Text=">4" />
                                </asp:DropDownList>
                            </div>
                            <div class="divAutoCell">
                                <asp:Label ID="Label2" Text="<%$Resources:Resource, lblChildrenUnder12 %>" runat="server" />
                                <br />
                                <asp:DropDownList ID="ddlChildrenUnder12" runat="server" Width="50px">
                                    <asp:ListItem Value="0" Text="<%$Resources:Resource, lblNo %>" />
                                    <asp:ListItem Value="1" Text="<%$Resources:Resource, lblYes %>" />
                                </asp:DropDownList>
                            </div>
                            <div style="clear: left;">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="padding0" style="width: 150px;">
                        </td>
                        <td style="width: 150px;">                            
                            <asp:Label ID="lblGst1" Text="<%$Resources:Resource, lblGuest1%>" runat="server" />
                        </td>
                        <td style="width: 150px;">
                            <asp:Label ID="lblGst2" Text="<%$Resources:Resource, lblGuest2%>" runat="server" />
                        </td>
                        <td style="width: 150px;">
                            <asp:Label ID="lblGst3" Text="<%$Resources:Resource, lblGuest3%>" runat="server" />
                        </td>
                        <td style="width: 150px;">
                            <asp:Label ID="lblGst4" Text="<%$Resources:Resource, lblGuest4%>" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="padding_left0 align_right" style="width: 150px;">                            
                            <asp:Label ID="lblSex" Text="<%$Resources:Resource, lblSex%>" runat="server" />
                        </td>
                        <td style="width: 150px;">                            
                            <asp:RadioButtonList ID="ddlGender1" runat="server" RepeatDirection="Horizontal"
                                BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px">                                
                                <asp:ListItem Value="M" Text="<%$Resources:Resource, lblMale%>" Selected="True" />
                                <asp:ListItem Value="F" Text="<%$Resources:Resource, lblFemale%>" />
                            </asp:RadioButtonList>
                        </td>
                        <td style="width: 150px;">
                            <asp:RadioButtonList ID="ddlGender2" runat="server" RepeatDirection="Horizontal"
                                BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px">
                                <asp:ListItem Value="M" Text="<%$Resources:Resource, lblMale%>" Selected="True" />
                                <asp:ListItem Value="F" Text="<%$Resources:Resource, lblFemale%>" />
                            </asp:RadioButtonList>
                        </td>
                        <td style="width: 150px;">
                            <asp:RadioButtonList ID="ddlGender3" runat="server" RepeatDirection="Horizontal"
                                BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px">
                                <asp:ListItem Value="M" Text="<%$Resources:Resource, lblMale%>" Selected="True" />
                                <asp:ListItem Value="F" Text="<%$Resources:Resource, lblFemale%>" />
                            </asp:RadioButtonList>
                        </td>
                        <td style="width: 150px;">
                            <asp:RadioButtonList ID="ddlGender4" runat="server" RepeatDirection="Horizontal"
                                BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px">
                                <asp:ListItem Value="M" Text="<%$Resources:Resource, lblMale%>" Selected="True" />
                                <asp:ListItem Value="F" Text="<%$Resources:Resource, lblFemale%>" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="padding_left0 align_right" style="width: 150px;">                            
                            <asp:Label ID="lblAccommodationType" Text="<%$Resources:Resource, lblAccommodationType %>" runat="server" />
                        </td>
                        <td style="width: 150px;">
                            <asp:DropDownList ID="ddlAccType1" runat="server" Width="90px">                                                           
                            </asp:DropDownList>
                        </td>
                        <td style="width: 150px;">
                            <asp:DropDownList ID="ddlAccType2" runat="server" Width="90px">                                
                            </asp:DropDownList>
                        </td>
                        <td style="width: 150px;">
                            <asp:DropDownList ID="ddlAccType3" runat="server" Width="90px">                                
                            </asp:DropDownList>
                        </td>
                        <td style="width: 150px;">
                            <asp:DropDownList ID="ddlAccType4" runat="server" Width="90px">                                
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trNoOfCouple1" runat="server">
                        <td class="padding_left0 align_right" style="width: 150px;">                            
                            <asp:Label ID="lblNoOfCouple" Text="<%$Resources:Resource, lblNoOfCouple%>" runat="server" />
                        </td>
                        <td style="width: 150px;">
                            <asp:DropDownList ID="ddlNoOfCouple" runat="server" Width="50px" OnSelectedIndexChanged="ddlNoOfCouple_SelectedIndexChanged"
                                AutoPostBack="True">
                                <asp:ListItem Text="0" />
                                <asp:ListItem Text="1" />
                                <asp:ListItem Text="2" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trCoupleRelation1" runat="server">
                        <td class="padding_left0 align_right" style="width: 150px;">                            
                            <asp:Label ID="lblCouple1" Text="<%$Resources:Resource,lblCouple1%>" runat="server" />
                        </td>
                        <td colspan="4">
                            <asp:DropDownList ID="ddlRelation11" runat="server" Width="120px" 
                                CssClass="ddl_relation">
                            </asp:DropDownList>
                            <asp:Label ID="lblWith" Text="<%$Resources:Resource, lblWith%>" runat="server" />
                            <asp:DropDownList ID="ddlRelation12" runat="server" Width="120px" 
                                CssClass="ddl_relation">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trCoupleRelation2" runat="server">
                        <td class="padding_left0 align_right" style="width: 150px;">
                            <asp:Label ID="lblCouple2" Text="<%$Resources:Resource,lblCouple2%>" runat="server" />
                        </td>
                        <td colspan="4">
                            <asp:DropDownList ID="ddlRelation21" runat="server" Width="120px" 
                                CssClass="ddl_relation">
                            </asp:DropDownList>
                            <asp:Label ID="Label1" Text="<%$Resources:Resource, lblWith%>" runat="server" />
                            <asp:DropDownList ID="ddlRelation22" runat="server" Width="120px" 
                                CssClass="ddl_relation">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td colspan="4">                                                                                                         
                            <asp:Button ID="btnFindAccommodation" Text="<%$Resources:Resource, lblContinue%>" runat="server" OnClick="btnFindAccommodation_Click" />
                            <asp:Button ID="btnSelectAnotherAccommodation" Text="<%$Resources:Resource, lblGoBack%>"
                                runat="server" />

                               
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <p style="font-weight:bold;">
            <%=Resources.Resource.lblNoteWebReservation%>
        </p>
    </div>
    
    <div id="dialogContact" title="<%=Resources.Resource.lblContactTheAshram%>" style="display:none;">
        <div style="font-weight: bold;">
            <p>
                <asp:Label ID="lblMoreThan4GuestMessage" Text="<%$Resources:Resource, msgMoreThan4Gues%>"
                    runat="server" />
            </p>
            <p style="text-align:left;">
                <asp:Label ID="lblCall" Text="<%$Resources:Resource, lblCallUsAt %>" runat="server" /> 
                <a href="callto: 1-819-322-3226">1-819-322-3226</a> <%=Resources.Resource.lblOR%>
                <br />
                <asp:Label ID="lblEmail" Text="<%$Resources:Resource, lblEmailUsAt %>" runat="server" /> 
                <a href="mailto: yogacampreception@sivananda.org">yogacampreception@sivananda.org</a>
            </p>            
        </div>
    </div>
    <asp:HiddenField ID="hdnPreviousSelectedValueOfTotalGuest" runat="server" />
    <asp:HiddenField ID="hdnShowContactPop" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        $(".datepicker").addPlaceholder();
        
//        function callAshram() {
//            var call = <%=OpenContactPopup%>;
//            if (call) {
//                alert("Call: 1-819-322-3226 Or \nEmail: yogacampreception@sivananda.org");
//            }
//        }

        //        callAshram();

//        $(".ddl_relation").change(function () {
//            $this = $(this);
//            $this.addClass("activeddl");
//            $(".ddl_relation").each(function () {
//                if (!$(this).hasClass("activeddl")) {
//                    $("option[value='" + $this.val() + "']", this).remove();
//                }
//            });
//            $this.removeClass("activeddl");
        //        });       
    </script>
    <asp:Panel ID="pnlContectScript" runat="server" Visible="false">
    </asp:Panel>
    <script type="text/javascript">
        $("#<%=ddlChildrenUnder12.ClientID%>").change(function () {
            $(this).val("0");
            $("#dialogContact").dialog({
                modal: true,
                width: 450,
                height: 210,
                buttons: [{
                    text: "<%=Resources.Resource.lblClose%>",
                    click: function () { $(this).dialog("close"); }
                }]
            });
        });

        if ($("#<%=hdnShowContactPop.ClientID%>").val() == "1") {            
            $("#dialogContact").dialog({
                modal: true,
                width: 450,
                height: 210,
                buttons: [{
                    text: "<%=Resources.Resource.lblClose%>",
                    click: function () { $(this).dialog("close"); }
                }]
            });
        }
    </script>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Sivananda/sivananda.master" AutoEventWireup="true" CodeFile="GuestInfo.aspx.cs" Inherits="Sivananda_GuestInfo" %>
<%@ Register src="Controls/Login.ascx" tagname="Login" tagprefix="uc1" %>
<%@ Register src="Controls/WelcomeDiv.ascx" tagname="WelcomeDiv" tagprefix="uc2" %>
<%@ Register src="Controls/AddServices.ascx" tagname="AddServices" tagprefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <script src="../lib/scripts/jquery-plugins/jquery.addplaceholder.js" type="text/javascript"></script>
    <style type="text/css">
        h3{margin-bottom:0px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFullWidth" runat="Server">
    <uc2:WelcomeDiv ID="WelcomeDiv1" runat="server" />
    
        <div style="border: 1px solid #FF7300; margin: 10px auto; padding: 5px;">
            <uc1:Login ID="Login2" runat="server" />
            <asp:Panel ID="pnlCustomerInfo" runat="server">
                <h3 style="display:none;">
                    <asp:Label ID="lblGuestPersonalInfo" Text="<%$Resources:Resource, lblGuestPersonalInfo %>"
                        runat="server" />
                </h3>
                <div style="padding: 2px; margin: 2px; background-color: #FFECB5; border: 1px solid #E5C365;
                    margin-top: 0px;">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tr>
                            <%--<td style="font-weight: bold;">
                            <asp:Label ID="lblAccomodationType" Text="Accommodation Type" runat="server" meta:resourcekey="lblAccomodationTypeResource1" />:
                        </td>
                        <td>
                            <asp:Literal ID="ltAccommodationType" runat="server" meta:resourcekey="ltAccommodationTypeResource1" />
                        </td>--%>
                            <td style="font-weight: bold;">
                                <asp:Label ID="lblCheckIn" Text="<%$Resources:Resource, lblCheckIn %>" runat="server" />:
                            </td>
                            <td>
                                <asp:Literal ID="ltCheckIn" runat="server" meta:resourcekey="ltCheckInResource1" />
                            </td>
                            <td style="font-weight: bold;">
                                <asp:Label ID="lblCheckOut" Text="<%$Resources:Resource, lblCheckout %>" runat="server"
                                    meta:resourcekey="lblCheckOutResource1" />:
                            </td>
                            <td>
                                <asp:Literal ID="ltCheckOut" runat="server" meta:resourcekey="ltCheckOutResource1" />
                            </td>
                        </tr>
                    </table>
                </div>
                <ul class="form">
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblFirstName" Text="<%$Resources:Resource, lblFName %>" runat="server"
                                meta:resourcekey="lblFirstNameResource1" />*
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtFirstName" runat="server" MaxLength="45" onchange='_objClientGuest.FirstName=this.value;'
                                meta:resourcekey="txtFirstNameResource1" />
                            <asp:RequiredFieldValidator ID="rfvFirstName" ControlToValidate="txtFirstName" SetFocusOnError="True"
                                runat="server" ErrorMessage="*" meta:resourcekey="rfvFirstNameResource1"></asp:RequiredFieldValidator>
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblLastName" Text="<%$Resources:Resource, lblLName%>" runat="server"
                                meta:resourcekey="lblLastNameResource1" />*
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtLastName" runat="server" MaxLength="45" onchange='_objClientGuest.LastName=this.value;'
                                meta:resourcekey="txtLastNameResource1" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ControlToValidate="txtLastName"
                                SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator12Resource1"></asp:RequiredFieldValidator>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblSpiritualName" Text="<%$Resources:Resource, lblSpiritualName %>"
                                runat="server" meta:resourcekey="lblSpiritualNameResource1" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtSpritualName" runat="server" MaxLength="45" meta:resourcekey="txtSpritualNameResource1" />
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblSex" Text="<%$resources:Resource, lblSex %>" runat="server" meta:resourcekey="lblSexResource1" />*
                        </div>
                        <div class="input" style="width: 150px;">
                            <asp:RadioButtonList ID="rblSex" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="F" Selected="True" Text="<%$Resources:Resource, lblFemale%>"></asp:ListItem>
                                <asp:ListItem Value="M" Text="<%$Resources:Resource, lblMale%>"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="lbl" style="text-align: left;">
                            <asp:CheckBox ID="chkCouple" Text="<%$Resources:Resource, lblIsCouple%>" runat="server"
                                meta:resourcekey="chkCoupleResource1" Visible="false" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblDob" Text="<%$Resources:Resource, lblDateOfBirth %>" runat="server"
                                meta:resourcekey="lblDobResource1" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtDOB" CssClass="dob_datepicker" runat="server" meta:resourcekey="txtDOBResource1" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblTelephoneNumber" Text="<%$Resources:Resource, lblTelephone %>"
                                runat="server" meta:resourcekey="lblTelephoneNumberResource1" />*
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtTelephone" runat="server" MaxLength="15" onchange='_objClientGuest.TelephoneNo=this.value;'
                                meta:resourcekey="txtTelephoneResource1" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtTelephone"
                                SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblEmailAddress" Text="<%$Resources:Resource, lblEmail %>" runat="server"
                                meta:resourcekey="lblEmailAddressResource1" />*
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtEmailAddress" runat="server" MaxLength="45" onchange='_objClientGuest.EmailAddress=this.value;'
                                meta:resourcekey="txtEmailAddressResource1" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtEmailAddress"
                                SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator3Resource1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revalSecEmail" runat="server" ControlToValidate="txtEmailAddress"
                                ErrorMessage="<%$ Resources:Resource, revalCMSecEmail %>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                meta:resourcekey="revalSecEmailResource1"></asp:RegularExpressionValidator>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                </ul>
                <h3>
                    <asp:Literal ID="ltAddress" Text="<%$Resources:Resource, lblAddress %>" runat="server"
                        meta:resourcekey="ltAddressResource1" />
                </h3>
                <ul class="form">
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblAddressLine1" Text="<%$Resources:Resource, lblAddressLine1 %>"
                                runat="server" meta:resourcekey="lblAddressLine1Resource1" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtAddressLine1" runat="server" MaxLength="45" meta:resourcekey="txtAddressLine1Resource1" />
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblAddressLine2" Text="<%$Resources:Resource, lblAddressLine2 %>"
                                runat="server" meta:resourcekey="lblAddressLine2Resource1" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtAddressLine2" runat="server" MaxLength="45" meta:resourcekey="txtAddressLine2Resource1" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblCity" Text="<%$Resources:Resource, lblCity %>" runat="server" meta:resourcekey="lblCityResource1" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtCity" runat="server" MaxLength="45" meta:resourcekey="txtCityResource1" />
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblState" Text="<%$Resources:Resource, lblStateOrProvince%>" runat="server"
                                meta:resourcekey="lblStateResource1" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtState" runat="server" MaxLength="45" meta:resourcekey="txtStateResource1" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblPostalCode" Text="<%$Resources:Resource, lblPostalCode %>" runat="server"
                                meta:resourcekey="lblPostalCodeResource1" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtPostalCode" runat="server" MaxLength="45" meta:resourcekey="txtPostalCodeResource1" />
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblCountry" Text="<%$Resources:Resource, lblCountry %>" runat="server"
                                meta:resourcekey="lblCountryResource1" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtCountry" runat="server" MaxLength="45" meta:resourcekey="txtCountryResource1" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                </ul>
                <h3>
                    <asp:Literal ID="ltEmergencyContact" Text="<%$Resources:Resource, lblEmergencyContact%>"
                        runat="server" meta:resourcekey="ltEmergencyContactResource1" />
                </h3>
                <ul class="form">
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblName" Text="<%$resources:Resource, lblName %>" runat="server" meta:resourcekey="lblNameResource1" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtEmName" runat="server" MaxLength="45" meta:resourcekey="txtEmNameResource1" />
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblRelationship" Text="<%$Resources:Resource, lblRelationship%>" runat="server"
                                meta:resourcekey="lblRelationshipResource1" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtEmRelationship" runat="server" MaxLength="45" meta:resourcekey="txtEmRelationshipResource1" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblTelephoneNo" Text="<%$Resources:Resource, lblTelephone %>" runat="server"
                                meta:resourcekey="lblTelephoneNoResource1" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtEmTelephone" runat="server" MaxLength="15" meta:resourcekey="txtEmTelephoneResource1" />
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblAltPhoneNo" Text="<%$Resources:Resource, lblAlternatePhoneNo%>"
                                runat="server" meta:resourcekey="lblAltPhoneNoResource1" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtAlterTelephone" runat="server" MaxLength="45" meta:resourcekey="txtAlterTelephoneResource1" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                </ul>
                <h3>
                    <asp:Literal ID="ltOtherInfo" Text="<%$Resources:Resource, lblOtherInfo%>" runat="server"
                        meta:resourcekey="ltOtherInfoResource1" />
                </h3>
                <div id="tblOtherGuests" runat="server" visible="false">
                    <table border="1" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <th>
                                <asp:Label ID="Label3" Text="<%$Resources:Resource, lblGuestNo%>" runat="server" />
                            </th>
                            <th>
                                <asp:Label ID="Label4" Text="<%$Resources:Resource, lblFName%>" runat="server" />
                            </th>
                            <th>
                                <asp:Label ID="Label5" Text="<%$Resources:Resource, lblLName%>" runat="server" />
                            </th>
                            <th>
                                <asp:Label ID="Label6" Text="<%$Resources:Resource, lblEmail%>" runat="server" />
                            </th>
                            <th>
                                <asp:Label ID="Label7" Text="<%$Resources:Resource, lblSex%>" runat="server" />
                            </th>
                        </tr>
                        <asp:Repeater ID="rptGuests" runat="server" OnItemDataBound="rptGuests_ItemDataBound">
                            <ItemTemplate>
                                <tr id="trGuest" runat="server">
                                    <td style="text-align: center">
                                        <%#Eval("GuestNo")%>
                                    </td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtFirstNameG" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvFirstNameG" ControlToValidate="txtEmailAddressG"
                                            SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator3Resource1"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtLastNameG" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvLastNameG" ControlToValidate="txtEmailAddressG"
                                            SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator3Resource1"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtEmailAddressG" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvEmailAddresG" ControlToValidate="txtEmailAddressG"
                                            SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator3Resource1"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revalSecEmail" runat="server" ControlToValidate="txtEmailAddressG"
                                            ErrorMessage="<%$ Resources:Resource, revalCMSecEmail %>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            meta:resourcekey="revalSecEmailResource1" Display="Dynamic"></asp:RegularExpressionValidator>
                                    </td>
                                    <td style="text-align: center">
                                        <asp:RadioButtonList ID="rblSexG" runat="server" SelectedValue='<%#Eval("Sex")%>'
                                            Enabled="false" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="F" Selected="True" meta:resourcekey="ListItemResource1"></asp:ListItem>
                                            <asp:ListItem Value="M" meta:resourcekey="ListItemResource2"></asp:ListItem>
                                        </asp:RadioButtonList>
                                        <asp:HiddenField ID="hdnBedID" runat="server" Value='<%#Eval("BedID")%>' />
                                        <asp:HiddenField ID="hdnSex" runat="server" Value='<%#Eval("Sex")%>' />
                                        <asp:HiddenField ID="hdnGuestNo" runat="server" Value='<%#Eval("GuestNo")%>' />
                                        <asp:HiddenField ID="hdnUnitPrice" runat="server" Value='<%#Eval("UnitPrice")%>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
                <ul class="form">
                    <li>
                        <div class="lbl" style="width: 300px">
                            <asp:Label ID="lblAreYouMemberOfSivananda" Text="<%$Resources:Resource, msgAreYouaMemberofSivanandaYogaVedantaCenter%>"
                                runat="server" meta:resourcekey="lblAreYouMemberOfSivanandaResource1" />
                        </div>
                        <div class="input" style="width: 400px">
                            <asp:RadioButtonList ID="rblMember" runat="server" RepeatDirection="Horizontal" CellPadding="0"
                                CellSpacing="0">
                            </asp:RadioButtonList>
                        </div>
                        <div class="clearBoth">
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblMemberID" Text="<%$Resources:Resource, lblMemberID%>" runat="server"
                                meta:resourcekey="lblMemberIDResource1" />
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtMembership" runat="server" meta:resourcekey="txtMembershipResource1" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li id="liReservationNote" runat="server">
                        <div class="lbl">
                        </div>
                        <div class="input" style="width: 250px;">
                            <asp:Label ID="lblReservationNotes" Text="<%$Resources:Resource, lblReservationNotes%>"
                                runat="server" meta:resourcekey="lblReservationNotesResource1" />
                            <br />
                            <asp:TextBox ID="txtReservationNote" runat="server" TextMode="MultiLine" Width="250px"
                                Height="100px" meta:resourcekey="txtReservationNoteResource1" />
                        </div>
                        <div class="input" style="width: 250px;">
                            <asp:Label ID="lblCustomerNotes" Text="<%$Resources:Resource, lblCustomerNotes%>"
                                runat="server" meta:resourcekey="lblCustomerNotesResource1" /><br />
                            <asp:TextBox ID="txtCustomerNote" runat="server" TextMode="MultiLine" Width="250px"
                                Height="100px" meta:resourcekey="txtCustomerNoteResource1" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div style="padding: 5px;">
                            <input type="checkbox" id="chkNewsLetter" runat="server" />
                            <asp:Label ID="Label2" AssociatedControlID="chkNewsLetter" Font-Bold="true" runat="server">
                            <%=Resources.Resource.lblNewsLetter%>
                            </asp:Label>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                </ul>
            </asp:Panel>
        </div>
    <asp:Panel ID="pnlGuestInfo" runat="server">  
        <uc3:AddServices ID="AddServices1" runat="server" />
        <div class="div_command" style="text-align: left;">
            <input type="checkbox" id="chkRules" runat="server" />
            <asp:Label ID="lblRules" CssClass="rulesAndRegulation" AssociatedControlID="chkRules"
                Font-Bold="true" runat="server">
                            <%=Resources.Resource.lblRules%>
            </asp:Label>
        </div>
        <div style="text-align: right; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;">
            <div id="divPaymentOptions" title="Checkout Options">
                <div style="text-align: center;">
                    <asp:RadioButton ID="rdoEvo" GroupName="payopt" Text="" runat="server" Checked="true" />
                    <asp:Label ID="lblEvo" AssociatedControlID="rdoEvo" Text="" runat="server">
                        <asp:Image ID="Image2" ImageUrl="~/Sivananda/images/EVO_logo.jpg" runat="server"
                            Style="vertical-align: middle" />
                    </asp:Label>
                    <asp:RadioButton ID="rdoPaypal" GroupName="payopt" Text="" runat="server" />
                    <asp:Label ID="Label1" AssociatedControlID="rdoPaypal" Text="" runat="server">
                        <asp:Image ID="Image1" ImageUrl="~/Sivananda/images/PayPal_mark_60x38.gif" runat="server"
                            Style="vertical-align: middle" />
                    </asp:Label>
                </div>
                <br />
                <div class="div_command">
                </div>
            </div>
            <asp:Button ID="btnEditReservation" Text="<%$Resources:Resource, lblEditReservation%>"
                runat="server" OnClick="btnEditReservation_Click" CausesValidation="False" />
            <asp:Button ID="btnAddServices" CssClass="btn_check_rules" Text="<%$Resources:resource, lblAddServices%>"
                runat="server" meta:resourcekey="btnAddServicesResource1" CommandName="SERVICE_DISCOUNT"
                OnCommand="btnCheckout_Command" Visible="false" />
            <asp:Button ID="btnCheckoutOptions" Text="<%$Resources:Resource, lblCheckout%>" runat="server"
                Visible="false" />
            <asp:Button ID="btnCheckout" CssClass="btn_check_rules" Text="<%$Resources:Resource, lblCheckoutCmd%>"
                runat="server" OnCommand="btnCheckout_Command" CommandName="CHECKOUT" />
            <asp:Button ID="btnReceivePassword" runat="server" CausesValidation="false" OnClick="btnReceivePassword_Click"
                Style="display: none;" />
        </div>
    </asp:Panel>
    <asp:HiddenField ID="hdnGuestType" runat="server" />
    <br />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        $("input[type=text]:first").focus();

        $("#divPaymentOptions").dialog({
            modal: true,
            autoOpen: false,
            resizable: false,
            width: 500,
            height: 150
        }).parent().appendTo(jQuery("form:first"));

        $("#<%=btnCheckoutOptions.ClientID %>").click(function () {
            if (Page_IsValid) {
                $("#divPaymentOptions").dialog("open");
            }                   
            return false;
        });

        $(".btn_check_rules").click(function () {
            if (!$("#<%=chkRules.ClientID%>").is(":checked")) {
                alert("<%=Resources.Resource.msgAgreeRules%>");
                return false;
            }
            //return true;
        });

        $(function () {
            $(".accordion").accordion({autoHeight: false});
        });

        $('body').append('<div id="refundPolicy"></div>');
        $('#refundPolicy').dialog({
            width: 500,
            height: 400,
            autoOpen: false
        });
        $(".rulesAndRegulation a:last").click(function (e) {
            e.preventDefault();
            var obj = $(this);
            // Store a link to the dialog:
            var dia = $('#refundPolicy');
            // Empty the content of the popup:
            dia.html('');
            // Load the contents into the dialog:
            dia.load("RefundPolicy.aspx")
            // Set the title:
           .dialog({ title: "<%=Resources.Resource.lblRefundPolicy%>" })
            // Open the dialog:
           .dialog('open');
        });
    </script>
    <asp:Panel ID="pnlConfirmationReceivePassScript" runat="server" Visible="false">
        <script type="text/javascript">
            $(document).ready(function () {
                if (confirm("<%=Resources.Resource.confirmProvidedEmailDAlreadyExists%>")) {
                    $("#<%=btnReceivePassword.ClientID%>").trigger("click");
                }
            });
        </script>
    </asp:Panel>
</asp:Content>


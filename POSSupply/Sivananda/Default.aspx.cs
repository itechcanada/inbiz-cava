﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Newtonsoft.Json;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Sivananda_Default : PayPalBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //pnlContectScript.Visible = false;
        hdnShowContactPop.Value = "0";
        if (!IsPostBack)
        {
            btnSelectAnotherAccommodation.OnClientClick = string.Format("location.href = '{0}'; return false;", System.Configuration.ConfigurationManager.AppSettings["SivanandaWebUrl"]);
            InitForm();
            hdnPreviousSelectedValueOfTotalGuest.Value = ddlTotalGuests.SelectedValue;
        }
    }

    protected void ddlTotalGuests_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlTotalGuests.SelectedValue == "5")
        {
            //pnlContectScript.Visible = true;
            hdnShowContactPop.Value = "1";
            ddlTotalGuests.SelectedValue = hdnPreviousSelectedValueOfTotalGuest.Value;
            //Response.Redirect("morethan4.aspx?lang=" + Globals.CurrentAppLanguageCode);
        }
        hdnPreviousSelectedValueOfTotalGuest.Value = ddlTotalGuests.SelectedValue;
        ToggelFormView();
    }

    protected void ddlNoOfCouple_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillCoupleForm();
    }

    protected void btnFindAccommodation_Click(object sender, EventArgs e)
    {
        AccommodationSearch search = new AccommodationSearch();
        search.BuildingID = this.BuildingID;
        search.CheckInDate = BusinessUtility.GetDateTime(txtChekcIn.Text, DateFormat.MMddyyyy);
        search.CheckOutDate = BusinessUtility.GetDateTime(txtCheckout.Text, DateFormat.MMddyyyy);        
        search.TotalGuest = this.TotalGuests;
        search.TotalCouple = this.TotalCouple;                

        Guest gst1 = new Guest();
        gst1.GuestNo = 1;
        gst1.AccommodationType = ddlAccType1.SelectedValue.ToUpper();
        gst1.Sex = ddlGender1.SelectedValue;
        gst1.CoupleWith = this.GetRelativeGuest(gst1.GuestNo);
        search.GuestCriteria.Add(gst1);

        if (this.TotalGuests > 1)
        {
            Guest gst2 = new Guest();
            gst2.GuestNo = 2;
            gst2.AccommodationType = ddlAccType2.SelectedValue.ToUpper();
            gst2.Sex = ddlGender2.SelectedValue;
            gst2.CoupleWith = this.GetRelativeGuest(gst2.GuestNo);
            search.GuestCriteria.Add(gst2);
        }

        if (this.TotalGuests > 2)
        {
            Guest gst3 = new Guest();
            gst3.GuestNo = 3;
            gst3.AccommodationType = ddlAccType3.SelectedValue.ToUpper();
            gst3.Sex = ddlGender3.SelectedValue;
            gst3.CoupleWith = this.GetRelativeGuest(gst3.GuestNo);
            search.GuestCriteria.Add(gst3);
        }

        if (this.TotalGuests > 3)
        {
            Guest gst4 = new Guest();
            gst4.GuestNo = 4;
            gst4.AccommodationType = ddlAccType4.SelectedValue.ToUpper();
            gst4.Sex = ddlGender4.SelectedValue;
            gst4.CoupleWith = this.GetRelativeGuest(gst4.GuestNo);
            search.GuestCriteria.Add(gst4);
        }
        

        if (this.ValidateSearchCriteria(search))
        {
            //Set Couple Pair 
            if (this.TotalCouple == 1)
            {
                search.CouplePair.Add(string.Format("{0}-{1}", BusinessUtility.GetInt(ddlRelation11.SelectedValue), BusinessUtility.GetInt(ddlRelation12.SelectedValue)));
            }
            else if (this.TotalCouple == 2)
            {
                search.CouplePair.Add(string.Format("{0}-{1}", BusinessUtility.GetInt(ddlRelation11.SelectedValue), BusinessUtility.GetInt(ddlRelation12.SelectedValue)));
                search.CouplePair.Add(string.Format("{0}-{1}", BusinessUtility.GetInt(ddlRelation21.SelectedValue), BusinessUtility.GetInt(ddlRelation22.SelectedValue)));                
            }

            string searchKeys = JsonConvert.SerializeObject(search);
            
            var qry = HttpUtility.ParseQueryString(Request.Url.Query);
            qry["search"] = searchKeys;
            SivanandaSession.SearchedHash = "Default.aspx?" + qry;
            Response.Redirect(string.Format("SearchResult.aspx?{0}", qry));            
        }
        else
        {
            return;
        }
    }

    #region Methods
    private void InitForm()
    {
        DropDownHelper.FillDropdown(ddlAccType1, "CT", "dlSGp", Globals.CurrentAppLanguageCode, null);
        DropDownHelper.FillDropdown(ddlAccType2, "CT", "dlSGp", Globals.CurrentAppLanguageCode, null);
        DropDownHelper.FillDropdown(ddlAccType3, "CT", "dlSGp", Globals.CurrentAppLanguageCode, null);
        DropDownHelper.FillDropdown(ddlAccType4, "CT", "dlSGp", Globals.CurrentAppLanguageCode, null);

        ListItem lItemToRemove = ddlAccType1.Items.FindByValue(((int)StatusRoomType.SCabin).ToString());
        if (lItemToRemove != null)
        {
            ddlAccType1.Items.Remove(lItemToRemove);
            ddlAccType2.Items.Remove(lItemToRemove);
            ddlAccType3.Items.Remove(lItemToRemove);
            ddlAccType4.Items.Remove(lItemToRemove);
        }

        lblTitle.Text = Resources.Resource.lblFindAccommodation;

        if (Request.QueryString.AllKeys.Contains<string>("search") && !string.IsNullOrEmpty(Request.QueryString["search"]))
        {
            try
            {
                AccommodationSearch search = JsonConvert.DeserializeObject<AccommodationSearch>(Request.QueryString["search"]);
                if (search != null)
                {
                    ddlTotalGuests.SelectedValue = search.TotalGuest.ToString();
                    this.ToggelFormView();
                    ddlNoOfCouple.SelectedValue = search.TotalCouple.ToString();
                    this.FillCoupleForm();

                    txtChekcIn.Text = BusinessUtility.GetDateTimeString(search.CheckInDate, DateFormat.MMddyyyy);
                    txtCheckout.Text = BusinessUtility.GetDateTimeString(search.CheckOutDate, DateFormat.MMddyyyy);

                    switch (search.GuestCriteria.Count)
                    {
                        case 1:
                            ddlGender1.SelectedValue = search.GuestCriteria[0].Sex;
                            ddlAccType1.SelectedValue = search.GuestCriteria[0].AccommodationType;
                            break;
                        case 2:
                            ddlGender1.SelectedValue = search.GuestCriteria[0].Sex;
                            ddlAccType1.SelectedValue = search.GuestCriteria[0].AccommodationType;
                            ddlGender2.SelectedValue = search.GuestCriteria[1].Sex;
                            ddlAccType2.SelectedValue = search.GuestCriteria[1].AccommodationType;                    
                            break;
                        case 3:
                            ddlGender1.SelectedValue = search.GuestCriteria[0].Sex;
                            ddlAccType1.SelectedValue = search.GuestCriteria[0].AccommodationType;
                            ddlGender2.SelectedValue = search.GuestCriteria[1].Sex;
                            ddlAccType2.SelectedValue = search.GuestCriteria[1].AccommodationType;
                            ddlGender3.SelectedValue = search.GuestCriteria[2].Sex;
                            ddlAccType3.SelectedValue = search.GuestCriteria[2].AccommodationType;                         
                            break;
                        case 4:
                            ddlGender1.SelectedValue = search.GuestCriteria[0].Sex;
                            ddlAccType1.SelectedValue = search.GuestCriteria[0].AccommodationType;
                            ddlGender2.SelectedValue = search.GuestCriteria[1].Sex;
                            ddlAccType2.SelectedValue = search.GuestCriteria[1].AccommodationType;
                            ddlGender3.SelectedValue = search.GuestCriteria[2].Sex;
                            ddlAccType3.SelectedValue = search.GuestCriteria[2].AccommodationType;
                            ddlGender4.SelectedValue = search.GuestCriteria[3].Sex;
                            ddlAccType4.SelectedValue = search.GuestCriteria[3].AccommodationType;
                            break;  
                    }

                    if (search.CouplePair.Count == 1)
                    {
                        ddlRelation11.SelectedValue = search.CouplePair[0].Split('-')[0];
                        ddlRelation12.SelectedValue = search.CouplePair[0].Split('-')[1];
                    }
                    else if (search.CouplePair.Count == 2)
                    {
                        ddlRelation11.SelectedValue = search.CouplePair[0].Split('-')[0];
                        ddlRelation12.SelectedValue = search.CouplePair[0].Split('-')[1];

                        ddlRelation21.SelectedValue = search.CouplePair[1].Split('-')[0];
                        ddlRelation22.SelectedValue = search.CouplePair[1].Split('-')[1];
                    }
                }
            }
            catch
            {
                this.ToggelFormView();
                this.FillCoupleForm();
            }
        }
        else
        {
            this.ToggelFormView();
            this.FillCoupleForm();
        }

        ddlAccType1.SelectedValue = this.AccommodationType.ToString();
        ddlAccType2.SelectedValue = this.AccommodationType.ToString();
        ddlAccType3.SelectedValue = this.AccommodationType.ToString();
        ddlAccType4.SelectedValue = this.AccommodationType.ToString();
    }

    private void ToggelFormView()
    {
        switch (this.TotalGuests)
        {
            case 1:
                lblGst2.Visible = false;
                lblGst3.Visible = false;
                lblGst4.Visible = false;
                ddlAccType2.Visible = false;
                ddlAccType3.Visible = false;
                ddlAccType4.Visible = false;
                ddlGender2.Visible = false;
                ddlGender3.Visible = false;
                ddlGender4.Visible = false;
                trNoOfCouple1.Visible = false;
                trCoupleRelation1.Visible = false;
                trCoupleRelation2.Visible = false;
                break;
            case 2:
                lblGst2.Visible = true;
                lblGst3.Visible = false;
                lblGst4.Visible = false;
                ddlAccType2.Visible = true;
                ddlAccType3.Visible = false;
                ddlAccType4.Visible = false;
                ddlGender2.Visible = true;
                ddlGender3.Visible = false;
                ddlGender4.Visible = false;
                trNoOfCouple1.Visible = true;
                trCoupleRelation1.Visible = false;
                trCoupleRelation2.Visible = false;
                break;
            case 3:
                lblGst2.Visible = true;
                lblGst3.Visible = true;
                lblGst4.Visible = false;
                ddlAccType2.Visible = true;
                ddlAccType3.Visible = true;
                ddlAccType4.Visible = false;
                ddlGender2.Visible = true;
                ddlGender3.Visible = true;
                ddlGender4.Visible = false;
                trNoOfCouple1.Visible = true;
                trCoupleRelation1.Visible = false;
                trCoupleRelation2.Visible = false;
                break;
            case 4:
                lblGst2.Visible = true;
                lblGst3.Visible = true;
                lblGst4.Visible = true;
                ddlAccType2.Visible = true;
                ddlAccType3.Visible = true;
                ddlAccType4.Visible = true;
                ddlGender2.Visible = true;
                ddlGender3.Visible = true;
                ddlGender4.Visible = true;
                trNoOfCouple1.Visible = true;
                trCoupleRelation1.Visible = false;
                trCoupleRelation2.Visible = false;
                break;
        }

        FillNoOfCoupleDDL();
        FillRelationDDL();
    }

    private void FillNoOfCoupleDDL()
    {
        Dictionary<int, string> dic = new Dictionary<int, string>();
        int counter = 0;
        switch (this.TotalGuests)
        {
            case 4:
                counter = 2;
                break;
            case 3:
            case 2:
                counter = 1;
                break;
            case 1:
            case 0:
            default:
                counter = 0;
                break;
        }
        for (int i = 0; i < counter + 1; i++)
        {
            dic[i] = i.ToString();
        }

        ddlNoOfCouple.DataSource = dic;
        ddlNoOfCouple.DataTextField = "Value";
        ddlNoOfCouple.DataValueField = "Key";
        ddlNoOfCouple.DataBind();
    }

    private void FillCoupleForm()
    {        
        ddlRelation11.Enabled = true;
        ddlRelation12.Enabled = true;
        ddlRelation21.Enabled = true;
        ddlRelation22.Enabled = true;

        switch (this.TotalCouple)
        {
            case 0:
                trCoupleRelation1.Visible = false;
                trCoupleRelation2.Visible = false;
                break;
            case 1:
                trCoupleRelation1.Visible = true;
                trCoupleRelation2.Visible = false;
                ddlRelation11.SelectedIndex = 1;
                ddlRelation12.SelectedIndex = 2;

                ddlRelation11.Enabled = false;
                ddlRelation12.Enabled = false;
                //if (this.TotalGuests == 2)
                //{
                //    ddlRelation11.SelectedIndex = 1;
                //    ddlRelation12.SelectedIndex = 2;

                //    ddlRelation11.Enabled = false;
                //    ddlRelation12.Enabled = false;
                //}
                break;
            case 2:
                trCoupleRelation1.Visible = true;
                trCoupleRelation2.Visible = true;

                ddlRelation11.SelectedIndex = 1;
                ddlRelation12.SelectedIndex = 2;
                ddlRelation21.SelectedIndex = 3;
                ddlRelation22.SelectedIndex = 4;

                ddlRelation11.Enabled = false;
                ddlRelation12.Enabled = false;
                ddlRelation21.Enabled = false;
                ddlRelation22.Enabled = false;
                break;
        }
    }

    private void FillRelationDDL()
    {
        Dictionary<int, string> dic = new Dictionary<int, string>();
        dic[0] = "N/A";
        for (int i = 1; i <= this.TotalGuests; i++)
        {
            dic[i] = string.Format("{0} {1}", Resources.Resource.lblGuest, i);
        }


        ddlRelation11.DataSource = dic;
        ddlRelation11.DataTextField = "Value";
        ddlRelation11.DataValueField = "Key";
        ddlRelation11.DataBind();

        ddlRelation12.DataSource = dic;
        ddlRelation12.DataTextField = "Value";
        ddlRelation12.DataValueField = "Key";
        ddlRelation12.DataBind();

        ddlRelation21.DataSource = dic;
        ddlRelation21.DataTextField = "Value";
        ddlRelation21.DataValueField = "Key";
        ddlRelation21.DataBind();

        ddlRelation22.DataSource = dic;
        ddlRelation22.DataTextField = "Value";
        ddlRelation22.DataValueField = "Key";
        ddlRelation22.DataBind();
    }

    private int AccommodationType {
        get {
            switch (Request.QueryString["atype"])
            {
                case "p":
                    return (int)StatusRoomType.Private;
                default:
                    return (int)StatusRoomType.Shared;                    
            }            
        }
    }

    private int GetRelativeGuest(int currentGstNo) {
        if (this.TotalCouple > 0)
        {
            if (ddlRelation11.SelectedValue == currentGstNo.ToString())
            {
                return BusinessUtility.GetInt(ddlRelation12.SelectedValue);
            }
            else if (ddlRelation12.SelectedValue == currentGstNo.ToString())
            {
                return BusinessUtility.GetInt(ddlRelation11.SelectedValue);
            }
            else if (ddlRelation21.SelectedValue == currentGstNo.ToString())
            {
                return BusinessUtility.GetInt(ddlRelation22.SelectedValue);
            }
            else if (ddlRelation22.SelectedValue == currentGstNo.ToString())
            {
                return BusinessUtility.GetInt(ddlRelation21.SelectedValue);
            } 
        }

        return 0;
    }

    private bool ValidateSearchCriteria(AccommodationSearch search)
    {
        if (search.CheckInDate == DateTime.MinValue || search.CheckOutDate == DateTime.MinValue || search.CheckInDate >= search.CheckOutDate)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidDateRange);
            return false;
        }

        if (search.CheckInDate < DateTime.Today || search.CheckOutDate < DateTime.Today)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, "Reservation can not be process for past dates!");
            return false;
        }

        switch (this.TotalGuests)
        {
            case 2:
            case 3:
                if (this.TotalCouple == 1 && 
                    (ddlRelation11.SelectedValue == "0" || 
                    ddlRelation12.SelectedValue == "0" || 
                    ddlRelation11.SelectedValue == ddlRelation12.SelectedValue))
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidCoupleRelationship);
                    return false;
                }
                break;
            case 4 :                               
                if (this.TotalCouple == 1 &&
                    (ddlRelation11.SelectedValue == "0" ||
                    ddlRelation12.SelectedValue == "0" ||
                    ddlRelation11.SelectedValue == ddlRelation12.SelectedValue))
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidCoupleRelationship);
                    return false;
                }
                else if (this.TotalCouple == 2 &&
                    (ddlRelation11.SelectedValue == "0" ||
                    ddlRelation12.SelectedValue == "0" ||
                    ddlRelation11.SelectedValue == ddlRelation12.SelectedValue))
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidCoupleRelationship);
                    return false;
                }
                else if (this.TotalCouple == 2 &&
                    (ddlRelation21.SelectedValue == "0" ||
                    ddlRelation22.SelectedValue == "0" ||
                    ddlRelation21.SelectedValue == ddlRelation22.SelectedValue))
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidCoupleRelationship);
                    return false;
                }
                else if (this.TotalCouple == 2 && (ddlRelation11.SelectedValue == ddlRelation21.SelectedValue ||
                    ddlRelation11.SelectedValue == ddlRelation22.SelectedValue || ddlRelation12.SelectedValue == ddlRelation21.SelectedValue ||
                    ddlRelation12.SelectedValue == ddlRelation22.SelectedValue))
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidCoupleRelationship);
                    return false;
                }
                break;
        }

        if (this.TotalCouple == 1 && (ddlAccType1.SelectedValue == ((int)StatusRoomType.Cabin).ToString() ||
                ddlAccType2.SelectedValue == ((int)StatusRoomType.Cabin).ToString()))
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblCabinsCantSelected);
            return false;
        }
        else if (this.TotalCouple == 2 && (ddlAccType1.SelectedValue == ((int)StatusRoomType.Cabin).ToString() ||
                ddlAccType2.SelectedValue == ((int)StatusRoomType.Cabin).ToString() || ddlAccType3.SelectedValue == ((int)StatusRoomType.Cabin).ToString() ||
                ddlAccType4.SelectedValue == ((int)StatusRoomType.Cabin).ToString()))
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblCabinsCantSelected);
            return false;
        }

        return true;
    }
    #endregion

    #region Properties    

    protected string OpenContactPopup
    {
        get
        {
            return (BusinessUtility.GetInt(ddlTotalGuests.SelectedValue) == 5).ToString().ToLower();
        }
    }

    private int TotalGuests {
        get {
            return BusinessUtility.GetInt(ddlTotalGuests.SelectedValue);
        }
    }

    private int TotalCouple {
        get
        {
            return BusinessUtility.GetInt(ddlNoOfCouple.SelectedValue);
        }
    }

    //private SpecialType Special
    //{
    //    get
    //    {
    //        string sp = BusinessUtility.GetString(Request.QueryString["sp"]);
    //        switch (sp.ToLower())
    //        {
    //            case "ws":
    //                return SpecialType.WeekSpecial;
    //            case "wk":
    //                return SpecialType.WeekEndSpecial;
    //            case "2ws":
    //                return SpecialType.TwoWeeksSpecial;
    //            case "n":
    //                return SpecialType.OneNight;
    //            case "na":
    //            default:
    //                return SpecialType.None;
    //        }
    //    }
    //}

    private string AccType
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["act"]);
        }
    }

    private int BuildingID
    {
        get
        {
            int bid = 0;
            int.TryParse(Request.QueryString["bid"], out bid);
            return bid;
        }
    }   
    #endregion         
    protected void ddlChildrenUnder12_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlChildrenUnder12.SelectedValue == "1")
        {
            //pnlContectScript.Visible = true;
            ddlChildrenUnder12.SelectedValue = "0";
        }
    }
}


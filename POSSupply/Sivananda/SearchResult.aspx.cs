﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Newtonsoft.Json;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Sivananda_SearchResult : SivaBasePage
{
    AccommodationSearch search;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowAccommodationDetails();            
        }
    }

    private void ShowAccommodationDetails()
    {
        if (!Request.QueryString.AllKeys.Contains("search") || string.IsNullOrEmpty(Request.QueryString["search"]))
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPleaseSearchAccommodation);
            Response.Redirect("Default.aspx?lang=" + Globals.CurrentAppLanguageCode);
        }
        try
        {
            search = JsonConvert.DeserializeObject<AccommodationSearch>(Request.QueryString["search"]);
            if (search == null)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgAccommodationNotFound);
                Response.Redirect("Default.aspx?lang=" + Globals.CurrentAppLanguageCode, false);
            }
            else
            {
                //Check if checkin date is require to validate min no of days being reserved
                List<int> lRomType = new List<int>();                
                foreach (var item in search.GuestCriteria)
                {
                    lRomType.Add(BusinessUtility.GetInt(item.AccommodationType));                    
                }
                ReservationSpecialDays spDays = new ReservationSpecialDays();
                int minDaysBeingReserved = search.CheckOutDate.Subtract(search.CheckInDate).Days;
                if (!spDays.IsValidChecInkDate(search.CheckInDate, minDaysBeingReserved, lRomType))
                {
                    string msg = string.Empty;
                    if (search.CheckInDate == spDays.IfCheckinDate)
                    {
                        msg = string.Format(Resources.Resource.msgMinDaysRequired, search.CheckInDate, search.CheckOutDate.Subtract(search.CheckInDate).Days, spDays.MinDaysToBeReserved); //Globals.CurrentAppLanguageCode == AppLanguageCode.FR ? spDays.MessageToDisplayFr : spDays.MessageToDisplayEn;
                    }
                    else
                    {
                        msg = string.Format(Resources.Resource.msgMinDaysRequired1, search.CheckInDate, search.CheckOutDate.Subtract(search.CheckInDate).Days);
                        //msg = string.Format(Resources.Resource.msgMinDaysRequired, search.CheckInDate, search.CheckOutDate.Subtract(search.CheckInDate).Days, spDays.MinDaysToBeReserved); 
                    }
                    
                    string title = Globals.CurrentAppLanguageCode == AppLanguageCode.FR ? spDays.SpecialNameFr : spDays.SpecialNameEn;
                    title += string.Format(" (" + Resources.Resource.msgDateForNights + ")", spDays.IfCheckinDate, spDays.MinDaysToBeReserved);
                    MessageState.SetGlobalMessage(MessageType.Failure, string.Format("<b>{0}<b/><br />{1}", title, msg));
                    Response.Redirect("Default.aspx" + Request.Url.Query, false);
                }

                List<SearchResult> dt = search.GetSearchResult(Globals.CurrentAppLanguageCode);

                List<string> lstAvailability = new List<string>();
                Dictionary<int, int> dicAvailBed = new Dictionary<int, int>(); //<roomType, total availbeds>

                int bedsFound = 0;
                double total = 0.0D;
                string itemHtml = string.Empty;                
                foreach (SearchResult r in dt)
                {                   
                    if (!dicAvailBed.ContainsKey(r.RoomType))
                    {
                        dicAvailBed[r.RoomType] = 0;
                    }
                    if (r.BedID > 0)
                    {
                        bedsFound++;
                        dicAvailBed[r.RoomType]++;
                    }

                    total += r.SubTotal;
                }

                
                if (dt.Count > 0)
                {
                    //Set result message
                    foreach (var item in dicAvailBed.Keys)
                    {
                        if (dicAvailBed[item] > 1)
                        {
                            itemHtml = string.Format(Resources.Resource.formatSearchResultPlural,
                            dicAvailBed[item], search.CheckInDate, search.CheckOutDate, search.CheckOutDate.Subtract(search.CheckInDate).Days, UIHelper.GetRoomType(item));
                            itemHtml = "<li>" + itemHtml + "</li>";
                        }
                        else
                        {
                            itemHtml = string.Format(Resources.Resource.formatSearchResultSingle,
                            dicAvailBed[item], search.CheckInDate, search.CheckOutDate, search.CheckOutDate.Subtract(search.CheckInDate).Days, UIHelper.GetRoomType(item));
                            itemHtml = "<li>" + itemHtml + "</li>";
                        }
                        
                        lstAvailability.Add(itemHtml);
                    } 
                }
                else
                {
                    itemHtml = string.Format("<li>Sorry, no beds in a {0} room  available for your selected date: <b>{1:dddd dd MMMM yyyy}</b> to <b>{2:dddd dd MMMM yyyy}</b> for <b>{3} nights</b></li>",
                        UIHelper.GetRoomType(BusinessUtility.GetInt(search.GuestCriteria[0].AccommodationType)), search.CheckInDate, search.CheckOutDate, search.CheckOutDate.Subtract(search.CheckInDate).Days);
                    lstAvailability.Add(itemHtml);
                }

                rptAvailability.DataSource = lstAvailability;
                rptAvailability.DataBind();

                rptGuest.DataSource = dt;
                rptGuest.DataBind();

                rptGuestInfo.DataSource = dt;
                rptGuestInfo.DataBind();


                ltTotal.Text = string.Format("<span style='font-weight:bold; font-size:14px;'>$ </span>{0:F}", total);

                Session["DATA_TO_RESERVE"] = dt;

                if (bedsFound < search.GuestCriteria.Count)
                {
                    MessageState.SetGlobalMessage(MessageType.Information, "Total no of accommodation found is not match with total no of guest being reserved! To Change the dates, click on the \"Go Back\" button!");
                    btnReserve.Enabled = false;
                }
            }
        }
        catch
        {
            Session["DATA_TO_RESERVE"] = null;
        }
    }   

    protected void btnGoBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx" + Request.Url.Query);
    }
    protected void btnReserve_Click(object sender, EventArgs e)
    {
        Response.Redirect("GuestInfo.aspx?lang=" + Globals.CurrentAppLanguageCode);
    }
}
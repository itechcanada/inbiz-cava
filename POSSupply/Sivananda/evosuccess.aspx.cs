﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Net;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;

public partial class Sivananda_evosuccess : SivaBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        //var r = Request;
        if (!IsPostBack)
        {
            //TotalSummary ts = CalculationHelper.GetOrderTotal(this.OrderID);

            //valOrderTotal.Text = string.Format("{0} {1:F}", ts.CurrencyCode, ts.GrandTotal);
            //valResponsecode.Text = this.ResponseText;
            //valTransactionID.Text = this.TransactionID;            
            //if (!string.IsNullOrEmpty(this.TransactionID))
            if (Request.QueryString.AllKeys.Contains("response") && BusinessUtility.GetInt(Request.QueryString["response"]) == (int)EvoResponse.TransactionApproved)
            {
                divReceipt.InnerHtml = new EmailNotifications().GetMessage(this.OrderID, NotificationType.ReservationOrderSuccessToGuest); //this.GetReceipt(this.OrderID);
                divReceipt.InnerHtml += string.Format("<a href='default.aspx?lang={0}' >Reserve More!</a>", Globals.CurrentAppLanguageCode);
            }
            else
            {
                string msg = System.IO.File.ReadAllText(Server.MapPath(string.Format("~/Sivananda/EmailTemplates/Failed_{0}.txt", Globals.CurrentAppLanguageCode.Trim().ToLower())));
                msg = msg.Replace("#CUSTOMER_NAME#", SivanandaSession.CurrentGuest.PartnerLongName);
                msg = msg.Replace("#REASON#", Request.QueryString["responsetext"]);
                divReceipt.InnerHtml = msg;
                //divFailed.Visible = true;
            }
        }
    }

    private string TransactionID {
        get {
            return BusinessUtility.GetString(Request.QueryString["transactionid"]);
        }
    }

    private string ResponseText {
        get {
            return BusinessUtility.GetString(Request.QueryString["responsetext"]);
        }
    }

    private int OrderID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["orderid"], out id);
            return id;
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx?lang=" + Globals.CurrentAppLanguageCode);
    }

    private string GetReceipt(int orderID)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            Orders ord = new Orders();
            ord.PopulateObject(dbHelp, orderID);

            TotalSummary ts = CalculationHelper.GetOrderTotal(dbHelp, orderID);

            string webWhs = ConfigurationManager.AppSettings["WebSaleWhsCode"];
            SysWarehouses whs = new SysWarehouses();
            whs.PopulateObject(webWhs, dbHelp);

            SysCompanyInfo ci = new SysCompanyInfo();
            ci.PopulateObject(whs.WarehouseCompanyID, dbHelp);

            Partners part = new Partners();
            part.PopulateObject(dbHelp, ord.OrdCustID);

            Reservation rsv = new Reservation();
            ReservationItems ri = rsv.GetFirstReservationItem(dbHelp, ord.OrdID);

            Bed b = new Bed();
            b.BedID = ri.BedID;
            b.PopulateBedInfo(dbHelp, b, Globals.CurrentAppLanguageCode);

            string sub = string.Format("Reservation Confirmation {0} ({1}, {2})", ci.CompanyName, ci.CompanyCity, ci.CompanyState);
            string msg = this.GetGuestEmailTemplate();

            string sqlGuest = "SELECT v.BedTitle, p.FirstName, p.LastName, p.SpirtualName, ri.PricePerDay FROM z_reservation_items ri INNER JOIN vw_beds v ON v.BedID=ri.BedID INNER JOIN z_customer_extended p ON p.PartnerID=ri.GuestID WHERE ri.ReservationID = @ReservationID";
            string bedGuestList = string.Empty;
            int totalBeds = 0;
            double toCalculatePricePerDay = 0.0D;
            double totalPriceCalculated = 0.0D;
            string spName = string.Empty;
            try
            {
                using (MySqlDataReader dr = dbHelp.GetDataReader(sqlGuest, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("ReservationID", ri.ReservationID, MyDbType.Int)
                    }))
                {
                    int iCount = 0;
                    while (dr.Read())
                    {
                        iCount++;
                        bedGuestList += string.Format("{3}-{0} : {1} {2}<br>", iCount, dr["LastName"], dr["FirstName"], Resources.Resource.lblBedCaps);
                        totalPriceCalculated += BusinessUtility.GetDouble(dr["PricePerDay"]);
                        spName = BusinessUtility.GetString(dr["SpirtualName"]);
                        totalBeds++;
                    }
                }
            }
            finally
            {

            }

            toCalculatePricePerDay = (totalPriceCalculated) / (double)(totalBeds == 0 ? 1 : totalBeds);

            //OrderItemProcess oip = new OrderItemProcess();
            //string services = string.Empty;
            //try
            //{
            //    using (IDataReader dr = oip.GetOrderProcessItemsReader(dbHelp, ord.OrdID))
            //    {
            //        while (dr.Read())
            //        {
            //            services += string.Format("<tr><td>{0}</td><td>{1} {2:F}</td></tr>", dr["ProcessDescription"], ts.CurrencyCode, dr["ProcessCost"]);
            //        }
            //    }
            //}
            //finally
            //{

            //}
            string services = string.Empty;
            SysProcessGroup serviceProducts = new SysProcessGroup();
            double serviceSubTotal = 0.0D;
            try
            {
                using (IDataReader dr = serviceProducts.GetReserverdServicesForWeb(dbHelp, orderID, ts.CurrencyCode))
                {
                    while (dr.Read())
                    {
                        double subTotal = BusinessUtility.GetDouble(dr["ordProductUnitPrice"]) * BusinessUtility.GetDouble(dr["ordProductQty"]);
                        services += string.Format("<tr><td>{0}</td><td>{1} <b>x</b> {2:F} ={3} {4:F}</td></tr>", dr["prdName"], dr["ordProductQty"], dr["ordProductUnitPrice"], ts.CurrencyCode, subTotal);
                        serviceSubTotal += subTotal;
                    }
                    dr.Close();
                    dr.Dispose();
                }
            }
            finally
            {
            }

            services += string.Format("<tr><td>{0}</td><td>{1} {2:F}</td></tr>", "Service Sub Total", ts.CurrencyCode, serviceSubTotal);

            msg = msg.Replace("#GUEST_NAME#", string.Format("{0} {1}", part.ExtendedProperties.FirstName, part.ExtendedProperties.LastName))
                .Replace("#COMPANY_NAME#", ci.CompanyName)
                .Replace("#COMPANY_CITY#", ci.CompanyCity)
                .Replace("#COMPANY_STATE#", ci.CompanyState)
                .Replace("#CONFIRMATION_NO#", string.Format("{0}{1}", part.ExtendedProperties.LastName, ord.OrdID))
                .Replace("#CHECKIN_DATE#", ri.CheckInDate.ToString("dddd MMMM dd, yyyy"))
                .Replace("#CHECKOUT_DATE#", ri.CheckOutDate.ToString("dddd MMMM dd, yyyy"))
                .Replace("#TOTAL_BEDS#", totalBeds.ToString())
                .Replace("#ROOM_TYPE#", UIHelper.GetRoomType(b.RoomType))
                .Replace("#BED_GUEST_LIST#", bedGuestList)
                .Replace("#BEDCOST_PER_NIGHT#", string.Format("{0} {1:F}", ts.CurrencyCode, toCalculatePricePerDay))                
                .Replace("#NO_OF_NIGHTS#", ri.TotalNights.ToString())
                .Replace("#BED_SUB_TOTAL#", string.Format("{0} {1:F}", ts.CurrencyCode, totalPriceCalculated))
                .Replace("#SERVICES#", services)
                .Replace("#REMAINING_BALANCE#", string.Format("{0} {1:F}", ts.CurrencyCode, ts.OutstandingAmount))
                .Replace("#GRAND_TOTAL#", string.Format("{0} {1:F}", ts.CurrencyCode, ts.GrandTotal))
                .Replace("#TRANSACTION_ID#", Request.QueryString["transactionid"])
                .Replace("#SPIRITUAL_NAME#", !string.IsNullOrEmpty(spName) ? string.Format("({0})", spName) : string.Empty);

            if (totalBeds > 1)
            {
                msg = msg.Replace("#BED_RESOURCE#", Resources.Resource.lblBedPlural);
            }
            else
            {
                msg = msg.Replace("#BED_RESOURCE#", Resources.Resource.lblBedSingular);
            }

            return msg;
        }
        catch
        {
            return string.Empty;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private enum EvoResponse
    {
        TransactionApproved = 1,
        TransactionDeclined = 2,
        ErrorInTransactionDataOrSystemError = 3
    }

    private string GetGuestEmailTemplate()
    {
        return System.IO.File.ReadAllText(Server.MapPath(string.Format("~/Sivananda/EmailTemplates/ToGuest_{0}.txt", Globals.CurrentAppLanguageCode.Trim().ToLower())));
    }
}
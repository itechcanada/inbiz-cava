﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null || Session["UserModules"] == null){
            Response.Redirect("~/AdminLogin.aspx");
        }
        else
        {
            Response.Redirect("~/Admin/");
        }
    }
}
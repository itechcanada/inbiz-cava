''Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports iCtrl = iTECH.WebControls

Partial Class Admin_AddEditTaxes_old
    Inherits BasePage
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If

        If Not IsPostBack Then
            lblTitle.Text = Resources.Resource.AddTaxes
            subPopulateTaxGroup()
            subPopulateTaxGrid()
            If Request.QueryString("TaxesID") <> "" And Request.QueryString("TaxesDesc") <> "" Then
                lblTitle.Text = Resources.Resource.EditTaxes
                subFillRecord()
            ElseIf Request.QueryString("TaxesID") <> "" Then
                dlTaxGroup.SelectedValue = Request.QueryString("TaxesID")
            End If
        Else
            If Request.QueryString("TaxesID") <> "" Then
                dlTaxGroup.SelectedValue = Request.QueryString("TaxesID")
            End If
        End If

        AddHandler CommonKeyword1.OnSearch, AddressOf SearchByKeyword
    End Sub
    'Populate Tax Group
    Private Sub subPopulateTaxGroup()
        Dim objTG As New clsTaxCodeDesc
        objTG.PopulateTaxGroup(dlTaxGroup)
        objTG = Nothing
    End Sub
    'Populate Tax Grid
    Private Sub subPopulateTaxGrid()
        Dim objTax As New clsTaxCode
        If Request.QueryString("TaxesID") <> "" Then
            sqldsTaxes.SelectCommand = objTax.funFillGrid(Request.QueryString("TaxesID"))  'fill Tax Record
        Else
            sqldsTaxes.SelectCommand = objTax.funFillGrid()  'fill Tax Record
        End If
        objTax = Nothing
    End Sub
    'On Page Index Change
    Protected Sub grdTaxes_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdTaxes.PageIndexChanging
        subPopulateTaxGrid()
    End Sub
    'On Row Data Bound
    Protected Sub grdTaxes_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdTaxes.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As LinkButton = CType(e.Row.FindControl("imgDelete"), LinkButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & Resources.Resource.ConfirmDeleteTaxes & " ')")
            If e.Row.Cells(5).Text = "0" Then
                e.Row.Cells(5).Text = Resources.Resource.lblNo
            Else
                e.Row.Cells(5).Text = Resources.Resource.lblYes
            End If
        End If
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim lblTotalNumberOfPages As Label = DirectCast(e.Row.FindControl("lblTotalNumberOfPages"), Label)
            lblTotalNumberOfPages.Text = grdTaxes.PageCount.ToString()

            Dim txtGoToPage As TextBox = DirectCast(e.Row.FindControl("txtGoToPage"), TextBox)
            txtGoToPage.Text = (grdTaxes.PageIndex + 1).ToString()

            Dim ddlPageSize As DropDownList = DirectCast(e.Row.FindControl("ddlPageSize"), DropDownList)
            ddlPageSize.SelectedValue = grdTaxes.PageSize.ToString()
        End If
    End Sub
    'Deleting
    Protected Sub grdTaxes_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdTaxes.RowDeleting
        Dim strTaxID As String = grdTaxes.DataKeys(e.RowIndex).Value.ToString()
        Dim strTaxDesc As String = grdTaxes.Rows(e.RowIndex).Cells(2).Text
        Session.Add("MsgTax", Resources.Resource.TaxesDeletedSuccessfully)
        Dim objTC As New clsTaxCode
        objTC.SysTaxCode = strTaxID
        objTC.SysTaxDesc = strTaxDesc
        sqldsTaxes.DeleteCommand = objTC.funDeleteTax() 'Delete Tax
        objTC = Nothing
        subClearControls()
        subPopulateTaxGrid()
    End Sub
    'Editing
    Protected Sub grdTaxes_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdTaxes.RowEditing
        Dim strTaxID As String = grdTaxes.DataKeys(e.NewEditIndex).Value.ToString()
        Dim strTaxDesc As String = grdTaxes.Rows(e.NewEditIndex).Cells(2).Text
        subEditFillRecord(strTaxID, strTaxDesc)
        subPopulateTaxGrid()
    End Sub
    'Populate Controls
    Public Sub subEditFillRecord(ByVal TaxID As String, ByVal TaxDesc As String)
        Dim objTC As New clsTaxCode
        objTC.SysTaxCode = TaxID
        objTC.SysTaxDesc = TaxDesc
        objTC.getTaxCodeSpecific() 'Get Specific Tax
        dlTaxGroup.SelectedValue = objTC.SysTaxCode
        dlTaxGroup.Enabled = False
        txtTaxDescription.Text = objTC.SysTaxDesc
        txtTaxDescription.ReadOnly = True
        txtTaxPercentage.Text = objTC.SysTaxPercentage
        txtTaxSequence.Text = objTC.SysTaxSequence
        rblstTaxOnTotal.SelectedValue = objTC.SysTaxOnTotal
        objTC = Nothing
    End Sub
    'Sorting
    Protected Sub grdTaxes_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdTaxes.Sorting
        subPopulateTaxGrid()
    End Sub
    ' Sql Data Source Event Track
    Protected Sub sqldsTaxes_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsTaxes.Selected
        If e.AffectedRows = 0 Then
            'lblMsg.Text = NoDataFound
        End If
    End Sub
    'Clear Controls
    Private Sub subClearControls()
        If Request.QueryString("TaxesID") <> "" Then
            subFillRecord()
            grdTaxes.EditIndex = -1
            subPopulateTaxGrid()
        End If
        txtTaxDescription.Text = ""
        txtTaxDescription.ReadOnly = False
        txtTaxPercentage.Text = ""
        txtTaxSequence.Text = ""
        rblstTaxOnTotal.SelectedValue = "0"
    End Sub
    'Populate Controls
    Public Sub subFillRecord()
        Dim objTC As New clsTaxCode
        objTC.SysTaxCode = Request.QueryString("TaxesID")
        objTC.SysTaxDesc = Request.QueryString("TaxesDesc")
        objTC.getTaxCodeSpecific() 'Get Specific Tax
        dlTaxGroup.SelectedValue = objTC.SysTaxCode
        dlTaxGroup.Enabled = False
        txtTaxDescription.Text = objTC.SysTaxDesc
        txtTaxDescription.ReadOnly = True
        txtTaxPercentage.Text = objTC.SysTaxPercentage
        txtTaxSequence.Text = objTC.SysTaxSequence
        rblstTaxOnTotal.SelectedValue = objTC.SysTaxOnTotal
    End Sub
    'Populate Object
    Private Sub subSetData(ByRef objT As clsTaxCode)
        objT.SysTaxCode = dlTaxGroup.SelectedValue
        objT.SysTaxDesc = txtTaxDescription.Text
        objT.SysTaxPercentage = txtTaxPercentage.Text
        objT.SysTaxSequence = txtTaxSequence.Text
        objT.SysTaxOnTotal = rblstTaxOnTotal.SelectedValue
    End Sub

    'On Tax Group Change
    Protected Sub dlTaxGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlTaxGroup.SelectedIndexChanged
        Dim objTax As New clsTaxCode
        sqldsTaxes.SelectCommand = objTax.funFillGrid(dlTaxGroup.SelectedValue)  'fill Tax Record
        objTax = Nothing
    End Sub

    Private Sub SearchByKeyword(ByVal sender As Object, ByVal e As EventArgs)
        Response.Redirect(String.Format("~/Taxes/ViewTaxes.aspx?sdata={0}", CommonKeyword1.SearchData))
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not Page.IsValid Then
            Exit Sub
        End If

        Dim objTC As New clsTaxCode
        subSetData(objTC)

        If Request.QueryString("TaxesID") <> "" And Request.QueryString("TaxesDesc") <> "" Then
            objTC.updateTaxCodeSpecific() 'Update Taxes 
            Session.Add("MsgTax", Resources.Resource.TaxesUpdatedSuccessfully)
            Response.Redirect("AddEditTaxes.aspx?TaxesID=" & objTC.SysTaxCode)
            Exit Sub
        ElseIf grdTaxes.EditIndex > -1 Then
            objTC.updateTaxCodeSpecific() 'Update Taxes 
            Session.Add("MsgTax", Resources.Resource.TaxesUpdatedSuccessfully)
            Response.Redirect("AddEditTaxes.aspx?TaxesID=" & objTC.SysTaxCode)
            Exit Sub
        Else
            If objTC.funCheckDuplicateTaxDescription() Then   'check Duplicate Tax Description
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, Resources.Resource.TaxesDescriptionAlreadyExists)
                txtTaxDescription.Focus()
                Exit Sub
            End If
            If objTC.insertTaxCode = True Then 'Insert Taxes
                Session.Add("MsgTax", Resources.Resource.TaxesAddedSuccessfully)
                Response.Redirect("AddEditTaxes.aspx?TaxesID=" & objTC.SysTaxCode)
                Exit Sub
            End If
        End If
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        subClearControls()
    End Sub

    Protected Sub GvCustomers_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim dropDown As DropDownList = DirectCast(sender, DropDownList)
        grdTaxes.PageSize = Integer.Parse(dropDown.SelectedValue)
        subPopulateTaxGrid()
    End Sub

    Protected Sub GoToPage_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim txtGoToPage As TextBox = DirectCast(sender, TextBox)

        Dim pageNumber As Integer
        If Integer.TryParse(txtGoToPage.Text.Trim(), pageNumber) AndAlso pageNumber > 0 AndAlso pageNumber <= grdTaxes.PageCount Then
            grdTaxes.PageIndex = pageNumber - 1
        Else
            grdTaxes.PageIndex = 0
        End If
        subPopulateTaxGrid()
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim pagerRow As GridViewRow = DirectCast(grdTaxes.BottomPagerRow, GridViewRow)
        If pagerRow IsNot Nothing AndAlso pagerRow.Visible = False Then
            pagerRow.Visible = True
        End If
    End Sub
End Class


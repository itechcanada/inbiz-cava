Imports System.Threading
Imports System.Globalization
Imports iTECH.InbizERP.BusinessLogic
Imports iTECH.Library.DataAccess.MySql
Imports iTECH.WebControls

Partial Class Admin_AddEditTaxGroup
    Inherits BasePage
    Dim _objTCD As New SysTaxCodeDesc
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Not IsPostBack Then
            If Request.QueryString("TaxesID") <> "" Then
                subFillRecord(Convert.ToInt32(Request.QueryString("TaxesID")))
            End If
            txtTaxCodeDescText.Focus()
        End If
    End Sub
    Public Sub subFillRecord(ByVal taxId As Int32)
        _objTCD.SysTaxCodeDescID = taxId
        _objTCD.PopulateObject(Nothing, taxId)
        txtTaxCodeDescText.Text = _objTCD.SysTaxCodeDescText
    End Sub
    
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Dim dbHelp As New DbHelper(True)
            Try
                _objTCD.SysTaxCodeDescText = txtTaxCodeDescText.Text
                If Request.QueryString("TaxesID") <> "" Then
                    _objTCD.SysTaxCodeDescID = Convert.ToInt32(Request.QueryString("TaxesID"))
                    _objTCD.Update(Nothing) 'Update Taxes 
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.TaxesUpdatedSuccessfully)                    
                    Globals.RegisterReloadParentScript(Page)
                Else
                    _objTCD.Insert(Nothing)
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.TaxesAddedSuccessfully)
                    Globals.RegisterReloadParentScript(Page)                   
                End If
            Catch ex As Exception
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message)
            Finally
                dbHelp.CloseDatabaseConnection()
            End Try
        End If
    End Sub
End Class
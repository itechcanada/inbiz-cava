<%@ Page Language="VB"  MasterPageFile="~/Shared/popup.master" AutoEventWireup="false"
    CodeFile="AddEditTaxGroup.aspx.vb" Inherits="Admin_AddEditTaxGroup" %>

<%@ Register Src="../Controls/CommonSearch/CommonKeyword.ascx" TagName="CommonKeyword"
    TagPrefix="uc1" %>
<asp:Content ID="cntAddEditTaxGroup" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="contentBottom" style="padding:5px; height:100px; overflow:auto;">
        <div id="contentTableForm" onkeypress="return disableEnterKey(event)">
            <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
            <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0" width="100%">            
                <tr>
                    <td class="text" style="width: 153px">
                        <asp:Label Text="<%$Resources:Resource, lblTaxesDescription %>"  ID="lblTaxesDescription" runat="server" />*
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtTaxCodeDescText" runat="server" MaxLength="75" />
                        <asp:RequiredFieldValidator ID="reqvalTaxesDescription" ErrorMessage="<%$ Resources:Resource, reqvalTaxesDescription%>" runat="server" ControlToValidate="txtTaxCodeDescText"
                            SetFocusOnError="true" Display="None" ValidationGroup="TexAdd"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <div class="clear">
            </div>                                
        </div>
    </div>
    <div class="div-dialog-command">
        <asp:Button Text="<%$Resources:Resource, btnSave %>"  ID="btnSave" runat="server" ValidationGroup="TexAdd" />
        <asp:Button Text="<%$Resources:Resource, btnCancel %>"  ID="btnCancel" runat="server" CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
    </div>
    <div class="clear">
    </div>
    <asp:ValidationSummary ID="valsAdminTaxes" runat="server" ShowMessageBox="true" ShowSummary="false"
        ValidationGroup="TexAdd" />
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
</asp:Content>

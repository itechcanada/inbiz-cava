﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Taxes_AddEditTaxes : BasePage
{
    string[] _aarKey = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains("TaxesID"))
        {
            _aarKey = Request.QueryString["TaxesID"].Split('~');
        }
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);

            try
            {
                dbHelp.OpenDatabaseConnection();
                SysTaxCodeDesc tg = new SysTaxCodeDesc();
                SysTaxCode tc = new SysTaxCode();
                tg.FillListControl(dbHelp, dlTaxGroup, null);
                if (dlTaxGroup.Items.Count > 0)
                {
                    dlTaxGroup.SelectedIndex = 0;
                }

                if (this.TaxGroupID > 0 && !string.IsNullOrEmpty(this.TaxCode) && tc.PopulateObject(dbHelp, this.TaxGroupID, this.TaxCode))
                {
                    dlTaxGroup.SelectedValue = this.TaxGroupID.ToString();
                    txtTaxDescription.Text = tc.sysTaxDesc.ToString();
                    txtTaxPercentage.Text = tc.sysTaxPercentage.ToString();
                    ddlTaxSequence.SelectedValue = tc.sysTaxSequence.ToString();

                    int tgID = 0;
                    int.TryParse(dlTaxGroup.SelectedValue, out tgID);
                    tc.FillListControl(dbHelp, tgID, ddlTaxes, new ListItem(""));

                    ddlTaxes.SelectedValue = tc.TaxOnTotalTaxCode;

                    dlTaxGroup.Enabled = false;
                    txtTaxDescription.Enabled = false;
                }
                else
                {
                    int tgID = 0;
                    int.TryParse(dlTaxGroup.SelectedValue, out tgID);

                    tc.FillListControl(dbHelp, tgID, ddlTaxes, new ListItem(""));
                    dlTaxGroup.Enabled = true;
                    txtTaxDescription.Enabled = true;
                }                
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();   
            }
        }
    }

    private int TaxGroupID {
        get {
            int id = 0;
            if (_aarKey != null && _aarKey.Length >= 1)
            {
                int.TryParse(_aarKey[0], out id);
            }            
            return id;
        }
    }
    private string TaxCode {
        get {
            if (_aarKey != null && _aarKey.Length >= 2)
            {
                return _aarKey[1];
            }
            return string.Empty;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                SysTaxCode tc = new SysTaxCode();
                tc.sysTaxCode = BusinessUtility.GetInt(dlTaxGroup.SelectedValue);
                tc.sysTaxDesc = txtTaxDescription.Text;
                tc.sysTaxOnTotal = ddlTaxes.SelectedIndex > 0;
                tc.sysTaxPercentage = BusinessUtility.GetDouble(txtTaxPercentage.Text);
                tc.sysTaxSequence = BusinessUtility.GetInt(ddlTaxSequence.SelectedValue);
                tc.TaxOnTotalTaxCode = ddlTaxes.SelectedValue;
                if (this.TaxGroupID > 0 && !string.IsNullOrEmpty(this.TaxCode))
                {
                    tc.Update(dbHelp);
                }
                else
                {
                    tc.Insert(dbHelp);
                }                
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.TaxesAddedSuccessfully);
                Globals.RegisterCloseDialogScript(this, "parent.reloadGrid();", true);
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void dlTaxGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        int txGrp = 0;
        int.TryParse(dlTaxGroup.SelectedValue, out txGrp);       

        SysTaxCode tc = new SysTaxCode();
        tc.FillListControl(null, txGrp, ddlTaxes, new ListItem(""));
    }
}
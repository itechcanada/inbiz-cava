<%@ Page Language="VB" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="false"
    CodeFile="AddEditTaxes_old.aspx.vb" Inherits="Admin_AddEditTaxes_old" %>

<%@ Register Src="../Controls/CommonSearch/CommonKeyword.ascx" TagName="CommonKeyword"
    TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div>
        <div class="col1">
            <img src="../lib/image/icon/ico_admin.png" />
        </div>
        <div class="col2">
            <h1>
                Administration</h1>
            <b>Add/Edit:Tex</b>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphLeft" runat="Server">
    <div id="colRight">
        <uc1:CommonKeyword ID="CommonKeyword1" runat="server" />
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntAddEditTaxes" ContentPlaceHolderID="cphMaster" runat="Server">
    <div >
        <div onkeypress="return disableEnterKey(event)">
            <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblSelectTaxGroup %>"  ID="lblSelectTaxGroup"  runat="server" />
                    </td>
                    <td class="input">
                        <asp:DropDownList ID="dlTaxGroup" runat="server" Width="180px" AutoPostBack="true">
                        </asp:DropDownList>
                        <span class="style1">*</span>
                        <asp:CustomValidator ID="custvalTaxGroup" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, custvalSelTaxGroup%>"
                            ClientValidationFunction="funCheckSelTaxGroup" Display="None" ValidationGroup="TexAdd1"></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label ID="lblTaxDescription" CssClass="lblBold" Text="<%$ Resources:Resource, lblTaxDescription%>"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtTaxDescription" runat="server" MaxLength="10" Width="330px" />
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalTaxDescription" runat="server" ControlToValidate="txtTaxDescription"
                            ErrorMessage="<%$ Resources:Resource, reqvalTaxDescription%>" SetFocusOnError="true"
                            Display="None" ValidationGroup="TexAdd1"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label ID="lblTaxPercentage" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblTaxPercentage%>">
                        </asp:Label>
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtTaxPercentage" runat="server" MaxLength="5" Width="330px">
                        </asp:TextBox>
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalTaxPercentage" runat="server" ControlToValidate="txtTaxPercentage"
                            ErrorMessage="<%$ Resources:Resource, reqvalTaxPercentage%>" SetFocusOnError="true"
                            Display="None" ValidationGroup="TexAdd1"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="compvalTaxPercentage" EnableClientScript="true" SetFocusOnError="true"
                            ControlToValidate="txtTaxPercentage" Operator="DataTypeCheck" Type="Double" Display="None"
                            ErrorMessage="<%$ Resources:Resource, compvalTaxPercentage%>" runat="server"
                            ValidationGroup="TexAdd1" />
                        <asp:CompareValidator ID="cmpvalPercentRng" EnableClientScript="true" SetFocusOnError="true"
                            ControlToValidate="txtTaxPercentage" ValueToCompare="100" Operator="LessThan"
                            Type="Double" Display="None" ErrorMessage="<%$ Resources:Resource, compvalTaxPercentRange%>"
                            runat="server" ValidationGroup="TexAdd1" />
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label ID="lblTaxSequence" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblTaxSequence%>">
                        </asp:Label>
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtTaxSequence" runat="server" MaxLength="2" Width="330px">
                        </asp:TextBox>
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalTaxSequence" runat="server" ControlToValidate="txtTaxSequence"
                            ErrorMessage="<%$ Resources:Resource, reqvalTaxSequence%>" SetFocusOnError="true"
                            Display="None" ValidationGroup="TexAdd1"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="compvalTaxSequence" EnableClientScript="true" SetFocusOnError="true"
                            ControlToValidate="txtTaxSequence" Operator="DataTypeCheck" Type="integer" Display="None"
                            ErrorMessage="<%$ Resources:Resource, compvalTaxSequence%>" runat="server" ValidationGroup="TexAdd1" />
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label ID="lblTaxOnTotal" CssClass="lblBold" Text="<%$ Resources:Resource, lblTaxOnTotal%>"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:RadioButtonList ID="rblstTaxOnTotal" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="<%$ Resources:Resource, lblYes%>" Value="1">
                            </asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lblNo%>" Value="0" Selected="True">
                            </asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <div class="inputCBLbt" style="padding-left: 220px; padding-bottom: 10px;">
                <asp:Button ID="btnSave" runat="server" Text="<%$Resources:Resource,cmdCssSave%>"
                    ValidationGroup="TexAdd1" />
                <asp:Button ID="btnReset" runat="server" Text=" <%$Resources:Resource,cmdCssReset%>" />
            </div>
            <asp:GridView ID="grdTaxes" runat="server" AllowSorting="True" DataSourceID="sqldsTaxes"
                AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" AutoGenerateColumns="False"
                UseAccessibleHeader="False" DataKeyNames="sysTaxCode" GridLines="None" Width="100%"
                CssClass="mGrid" CellPadding="0" CellSpacing="0">
                <Columns>
                    <asp:BoundField DataField="sysTaxCode" Visible="false" HeaderText="<%$ Resources:Resource, grdTaxCode %>"
                        ReadOnly="True" SortExpression="sysTaxCode">
                        <ItemStyle Wrap="true" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="sysTaxCodeDescText" HeaderText="<%$ Resources:Resource, grdGroupTaxDescription %>"
                        ReadOnly="True" SortExpression="sysTaxCodeDescText">
                        <ItemStyle Wrap="true" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="sysTaxDesc" HeaderText="<%$ Resources:Resource, grdTaxDescription %>"
                        ReadOnly="True" SortExpression="sysTaxDesc">
                        <ItemStyle Wrap="true" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="sysTaxPercentage" HeaderText="<%$ Resources:Resource, grdTaxPercentage %>"
                        ReadOnly="True" SortExpression="sysTaxPercentage">
                        <ItemStyle Wrap="true" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="sysTaxSequence" HeaderText="<%$ Resources:Resource, grdTaxSequence %>"
                        ReadOnly="True" SortExpression="sysTaxSequence">
                        <ItemStyle Wrap="true" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="sysTaxOnTotal" HeaderText="<%$ Resources:Resource, grdTaxOnTotal %>"
                        ReadOnly="True" SortExpression="sysTaxOnTotal">
                        <ItemStyle Wrap="true" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdEdit %>" HeaderStyle-ForeColor="#ffffff"
                        HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("sysTaxCode") %>'
                                CommandName="Edit" CausesValidation="false"><%= Resources.Resource.edit %></asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" CssClass="action" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="imgDelete" runat="server" CausesValidation="false" CommandArgument='<%# Eval("sysTaxCode") %>'
                                CommandName="Delete"><span style="color:#AF3333"><%= Resources.Resource.delete %></span></asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle CssClass="action" />
                    </asp:TemplateField>
                </Columns>
                <PagerTemplate>
                    <div style="text-align: right">
                        <asp:Label ID="Label5" runat="server" Text="Show rows:" />
                        <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="GvCustomers_SelectedIndexChanged">
                            <asp:ListItem Value="10" />
                            <asp:ListItem Value="50" />
                            <asp:ListItem Value="100" />
                            <asp:ListItem Value="500" />
                        </asp:DropDownList>
                        | Page
                        <asp:TextBox ID="txtGoToPage" runat="server" Width="20" AutoPostBack="true" OnTextChanged="GoToPage_TextChanged" />
                        of
                        <asp:Label ID="lblTotalNumberOfPages" runat="server" />
                        |
                        <asp:Button ID="Button1" runat="server" CommandName="Page" ToolTip="Previous Page"
                            CommandArgument="Prev" Text="<%$ Resources:Resource, Previouspage %>" />
                        <asp:Button ID="Button2" runat="server" CommandName="Page" ToolTip="Next Page" CommandArgument="Next"
                            Text="<%$ Resources:Resource, nextpage %>" />
                    </div>
                </PagerTemplate>
                <FooterStyle CssClass="grid_footer" />
                <RowStyle CssClass="grid_rowstyle" />
                <HeaderStyle CssClass="grid_header" />
                <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
            </asp:GridView>
            <asp:SqlDataSource ID="sqldsTaxes" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
            <asp:ValidationSummary ID="valsAdminTaxes" runat="server" ShowMessageBox="true" ShowSummary="false"
                ValidationGroup="TexAdd1" />
        </div>
    </div>
</asp:Content>

<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
    <script language="javascript">
        function funCheckSelTaxGroup(source, args) {
            if (document.getElementById('<%=dlTaxGroup.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphHead">
</asp:Content>

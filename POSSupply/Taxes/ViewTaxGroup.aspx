﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ViewTaxGroup.aspx.cs" Inherits="Taxes_ViewTaxGroup" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphHead">
     <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <%= Resources.Resource.lblView%>:
            <%= Resources.Resource.lblTaxGroup%></b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <asp:Button ID="btnCreateTax" runat="server" Text="<%$ Resources:Resource, lblAddGroupTax %>"  />
        <iCtrl:IframeDialog ID="mdCreateCurrency" TriggerControlID="btnCreateTax" Width="450"
            Height="360" Url="AddEditTaxGroup.aspx" Title="<%$ Resources:Resource, lblAddEditTaxGroup %>"   runat="server"
            OnCloseClientFunction="reloadGrid">
        </iCtrl:IframeDialog>
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif" CssClass="csv_export" />
        <asp:LinkButton ID="lnkAddTaxes" runat="server" Visible="false"></asp:LinkButton>
    </div>
     <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphLeft" runat="Server">
    <div >
        <asp:Panel runat="server" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                       <%= Resources.Resource.lblSearchForm%></div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch" CssClass="filter-key-txt"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntViewTaxGroup" ContentPlaceHolderID="cphMaster" runat="Server">
    <div >
        <div onkeypress="return disableEnterKey(event)">
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="jgdvTaxes" Height="300px"
                    AutoWidth="True" oncellbinding="jgdvTaxes_CellBinding" 
                    ondatarequesting="jgdvTaxes_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="sysTaxCodeDescID" HeaderText="" PrimaryKey="True"
                            Visible="false" />
                        <trirand:JQGridColumn DataField="sysTaxCodeDescID" HeaderText="<%$ Resources:Resource, grdTaxesCode %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="sysTaxCodeDescText" HeaderText="<%$ Resources:Resource, grdTaxesDescription %>"
                            Editable="false" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="sysTaxCodeDescID"
                            Sortable="false" TextAlign="Center" Width="50" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="sysTaxCodeDescID"
                            Sortable="false" TextAlign="Center" Width="50" />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents LoadComplete="jqGridResize" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sqldsTaxes" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
            </div>
             <iCtrl:IframeDialog ID="mdEditTax" Height="360" Width="490" Title="<%$ Resources:Resource, lblAddEditTaxGroup %>"
                    Dragable="true" TriggerSelectorClass="edit-Tax" TriggerSelectorMode="Class"
                    OnCloseClientFunction="reloadGrid" UrlSelector="TriggerControlHrefAttribute"
                    runat="server">
                </iCtrl:IframeDialog>
            </div>
        </div>
 
</asp:Content>

<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
    <script type="text/javascript">
        $grid = $("#<%=jgdvTaxes.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=jgdvTaxes.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function deleteRow(rowID) {
            if (confirm("<%=Resources.Resource.msgDeleteCategory%>")) {
                var dataToPost = {};
                dataToPost.cmdDelete = "1"; //$grid.jqCustomSearch
                dataToPost.groupTaxID = rowID;
                $grid.jqCustomSearch(dataToPost);
            }
        }
    </script>
</asp:Content>


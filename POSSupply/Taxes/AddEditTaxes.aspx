﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AddEditTaxes.aspx.cs" Inherits="Taxes_AddEditTaxes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <ul class="form">
                <li>
                    <div class="lbl">
                        <asp:Label Text="<%$Resources:Resource, lblSelectTaxGroup %>" ID="lblSelectTaxGroup"
                            runat="server" /></div>
                    <div class="input">
                        <asp:DropDownList ID="dlTaxGroup" runat="server" Width="180px" AutoPostBack="true"
                            OnSelectedIndexChanged="dlTaxGroup_SelectedIndexChanged">
                        </asp:DropDownList>
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="dlTaxGroup"
                            ErrorMessage="<%$ Resources:Resource, reqvalTaxDescription%>" SetFocusOnError="true"
                            Display="Dynamic" ValidationGroup="TexAdd1"></asp:RequiredFieldValidator>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblTaxDescription" CssClass="lblBold" Text="<%$ Resources:Resource, lblTaxDescription%>"
                            runat="server" /></div>
                    <div class="input">
                        <asp:TextBox ID="txtTaxDescription" runat="server" MaxLength="10" />
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalTaxDescription" runat="server" ControlToValidate="txtTaxDescription"
                            ErrorMessage="<%$ Resources:Resource, reqvalTaxDescription%>" SetFocusOnError="true"
                            Display="Dynamic" ValidationGroup="TexAdd1"></asp:RequiredFieldValidator>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblTaxPercentage" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblTaxPercentage%>">
                        </asp:Label></div>
                    <div class="input">
                        <asp:TextBox ID="txtTaxPercentage" runat="server" MaxLength="5">
                        </asp:TextBox>
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalTaxPercentage" runat="server" ControlToValidate="txtTaxPercentage"
                            ErrorMessage="<%$ Resources:Resource, reqvalTaxPercentage%>" SetFocusOnError="true"
                            Display="Dynamic" ValidationGroup="TexAdd1"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="compvalTaxPercentage" EnableClientScript="true" SetFocusOnError="true"
                            ControlToValidate="txtTaxPercentage" Operator="DataTypeCheck" Type="Double" Display="Dynamic"
                            ErrorMessage="<%$ Resources:Resource, compvalTaxPercentage%>" runat="server"
                            ValidationGroup="TexAdd1" />
                        <asp:CompareValidator ID="cmpvalPercentRng" EnableClientScript="true" SetFocusOnError="true"
                            ControlToValidate="txtTaxPercentage" ValueToCompare="100" Operator="LessThan"
                            Type="Double" Display="Dynamic" ErrorMessage="<%$ Resources:Resource, compvalTaxPercentRange%>"
                            runat="server" ValidationGroup="TexAdd1" /></div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblTaxSequence" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblTaxSequence%>">
                        </asp:Label></div>
                    <div class="input">
                        <asp:DropDownList ID="ddlTaxSequence" runat="server">
                            <asp:ListItem Text="1" />
                            <asp:ListItem Text="2" />
                            <asp:ListItem Text="3" />
                            <asp:ListItem Text="4" />
                            <asp:ListItem Text="5" />
                        </asp:DropDownList>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblTaxOnTotal" CssClass="lblBold" Text="<%$ Resources:Resource, lblTaxOnTotal%>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:DropDownList ID="ddlTaxes" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
            </ul>
            <div class="div_command">
                <asp:Button ID="btnSave" runat="server" Text="<%$Resources:Resource,cmdCssSave%>"
                    ValidationGroup="TexAdd1" OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" Text="<%$Resources:Resource, btnCancel%>" runat="server" CausesValidation="false" OnClientClick="jQuery.FrameDialog.closeDialog();" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


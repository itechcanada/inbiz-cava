﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ViewTaxes.aspx.cs" Inherits="Taxes_ViewTaxes" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphHead">
    <%--Keep script file reference here those were required for this page only. 
    Note* if it is to being used on all pages than should pe kept on master page.--%>
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <%= Resources.Resource.lblView%>:
            <%= Resources.Resource.lblTax%></b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <asp:Button ID="btnAddTax" runat="server" Text="<%$ Resources:Resource,btnAddTaxes %>" OnClientClick="return editPopup('');" />
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif" CssClass="csv_export" />
        <asp:LinkButton ID="lnkAddTaxes" runat="server" Visible="false" OnClick="lnkAddTaxes_Click"></asp:LinkButton>
    </div>
    <div style="clear:both;"></div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <%--Search Panel Needs to --%>
        <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        <%= Resources.Resource.lblSearchForm%></div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" alt="" /></div>
                </div>
                <h4>
                    <asp:Label ID="Label1" AssociatedControlID="dlTaxGroup" CssClass="filter-key" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:DropDownList ID="dlTaxGroup" runat="server" Width="180px" >
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" CssClass="filter-key" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <%--End Here--%>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntViewTaxes" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="jgdvTaxes" Height="300px"
                    AutoWidth="True"
                    OnCellBinding="jgdvTaxes_CellBinding" OnDataRequesting="jgdvTaxes_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="ID" 
                            PrimaryKey="True" Visible="false" />
                        <trirand:JQGridColumn DataField="TaxCode" HeaderText="<%$ Resources:Resource, grdTaxCode %>"
                            Editable="false" TextAlign="Center"/>
                        <trirand:JQGridColumn DataField="TaxGroupDesc" HeaderText="<%$ Resources:Resource, grdGroupTaxDescription %>"
                            Editable="false" TextAlign="Center"/>
                        <trirand:JQGridColumn DataField="sysTaxPercentage" HeaderText="<%$ Resources:Resource, grdTaxPercentage %>"
                            Editable="false" TextAlign="Center" />                        
                        <trirand:JQGridColumn DataField="sysTaxSequence" HeaderText="<%$ Resources:Resource, grdTaxSequence %>"
                            Editable="false" TextAlign="Center"/>
                        <trirand:JQGridColumn DataField="TaxOnTotalTaxCode" HeaderText="<%$ Resources:Resource, grdTaxOnTotal %>"
                            Editable="false" TextAlign="Center"/>                                               
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="ID"
                            Sortable="false" TextAlign="Center" Width="50" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="ID"
                            Sortable="false" TextAlign="Center" Width="50" />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents LoadComplete="jqGridResize" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sqldsTaxes" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
    <script type="text/javascript">
        $grid = $("#<%=jgdvTaxes.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=jgdvTaxes.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }        

        function reloadGrid() {
            $grid.trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function editPopup(key) {
            var url = 'AddEditTaxes.aspx';
            var queryData = {};
            queryData.TaxesID = key;            
            var t = "";
            var height = 250;
            if (key == "") {
                t = "<%=Resources.Resource.AddTaxes%>";                
            }
            else {
                t = "<%=Resources.Resource.EditTaxes%>";                
            }

            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 520,
                height: height,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');

            return false;
        }

        function deleteTax(key) {
            if (confirm("Are you sure you want to delete?")) {
                var queryData = {};
                queryData.IsDeleting = 1;
                queryData.TaxesID = key;

                for (var k in queryData) {
                    $grid.setPostDataItem(k, queryData[k]);
                }

                $grid.trigger("reloadGrid");

                for (var k in queryData) {
                    $grid.removePostDataItem(k);
                }
            }            
        }
    </script>
</asp:Content>


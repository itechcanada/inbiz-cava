﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;

public partial class Taxes_ViewTaxes : BasePage
{
    private string strTaxID = "";
    private SysTaxCode _objTax = new SysTaxCode();
    // On Page Load
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/AdminLogin.aspx");
        }
        
        if (!IsPagePostBack(jgdvTaxes)) //Instead of IsPostBack use this to prevent postback as fress requens in each page of jqgrid
        {
            FillGroupTax();
            txtSearch.Focus();
        }               
    }
    private void FillGroupTax()
    {
        SysTaxCodeDesc objTG = new SysTaxCodeDesc();
        objTG.FillListControl(null, dlTaxGroup, new ListItem(Resources.Resource.liTaxGroup, "0"));        
    }
    protected void lnkAddTaxes_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("~/Taxes/AddEditTaxes.aspx");
    }
   
    protected void  jgdvTaxes_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 6)
        {
            e.CellHtml = string.Format(@"<a class=""edit-Taxs""  href=""javascript:void(0);"" onclick=""editPopup('{0}')"">{1}</a>", e.RowKey, Resources.Resource.edit);
        }
        if (e.ColumnIndex == 7)
        {
            e.CellHtml = string.Format(@"<a href=""{0}""  onclick=""deleteTax('{1}')"">{2}</a>", "javascript:void(0);", e.RowKey, Resources.Resource.delete);
        }
    }

    protected void jgdvTaxes_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string search = Request.QueryString[txtSearch.ClientID];
        int id = 0;
        int.TryParse(Request.QueryString[dlTaxGroup.ClientID], out id);

        if (Request.QueryString.AllKeys.Contains("IsDeleting") && Request.QueryString["IsDeleting"] == "1")
        {
            int txGrp = 0;
            string[] arrKey = Request.QueryString["TaxesID"].Split('~');
            if (arrKey.Length > 1 && int.TryParse(arrKey[0], out txGrp) && txGrp > 0)
            {
                _objTax.Delete(null, txGrp, arrKey[1]);
            }
        }
        
        jgdvTaxes.DataSource = _objTax.GetAllTaxes(null, id, search);
    }


    protected void btnAddTax_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("~/Taxes/AddEditTaxes.aspx");
    }
}
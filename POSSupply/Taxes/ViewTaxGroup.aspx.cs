﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;

public partial class Taxes_ViewTaxGroup : BasePage
{
    private string strTaxID = "";
    private SysTaxCodeDesc _objTax = new SysTaxCodeDesc();
    // On Page Load
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/AdminLogin.aspx");
        }
        if (!IsPagePostBack(jgdvTaxes))
        {
            lnkAddTaxes.Text = Resources.Resource.AddTaxGroup;
            lblTitle.Text = Resources.Resource.ViewTaxGroup;
            if (Convert.ToString(Session["UserRole"]) == "1")
            {
                lnkAddTaxes.Text = "";
            }

            txtSearch.Focus();
        }        
    }
    protected void lnkAddTaxes_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("~/Taxes/AddEditTaxGroup.aspx");
    }  

    protected void jgdvTaxes_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 3)
        {
            e.CellHtml = string.Format("<a class=edit-Tax  href=AddEditTaxGroup.aspx?TaxesID={0} >" + Resources.Resource.grdEdit + "</a>", e.RowValues[0]);
        }
        if (e.ColumnIndex == 4)
        {
            e.CellHtml = string.Format("<a href=\"{0}\" onclick='deleteRow({1});'>{2}</a>", "javascript:void(0);", e.RowValues[0], Resources.Resource.delete);
        }
    }

    protected void jgdvTaxes_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string search = !string.IsNullOrEmpty(Request.QueryString[txtSearch.ClientID]) ? Request.QueryString[txtSearch.ClientID] : string.Empty;
        if (Request.QueryString.AllKeys.Contains("cmdDelete"))
        {
            int id = 0;
            if (int.TryParse(Request.QueryString["groupTaxID"], out id)) {
                _objTax.Delete(id);
            }
        }
        jgdvTaxes.DataSource = _objTax.GetAllTaxGroups(null, search);        
    }
}

﻿Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Imports System.IO
Imports iTECH.InbizERP.BusinessLogic
Imports iTECH.Library.Utilities
Partial Class POSMaster
    Inherits System.Web.UI.MasterPage
    Protected Overrides Sub OnInit(ByVal e As EventArgs)
        MyBase.OnInit(e)
        ltScripts.Text = UIHelper.GetScriptTags("jquery,jqueryui,json,jsload,messagebox,jqgrid,framedialog,jqchosen", 4)
    End Sub

    Protected Sub POSMaster_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("lang") = "en" Then
            Session("Language") = "en-CA"
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        ElseIf Request.QueryString("lang") = "fr" Then
            Session("Language") = "fr-CA"
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        ElseIf Request.QueryString("lang") = "sp" Then
            Session("Language") = "es-MX"
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        ElseIf Request.QueryString("lang") = "logout" Then
            If Session("Label") = "1" Then
                subCopyResource() 'copy resource file
            End If
            Session.Abandon()
            Response.Redirect("../AdminLogin.aspx?lang=logout")
        End If

        Me.imgCmdHelp.Attributes.Add("OnClick", "return openPDF('../Help.aspx')")
        If Request.Url.AbsoluteUri.Contains("/Admin/") Then
            trHeader.Visible = True
        End If
        Dim objCompany As New clsCompanyInfo
        Dim strPic() As String
        lblComName.Text = objCompany.funGetFirstCompanyName
        Dim objCommon As New clsCommon
        Dim strLogo As String = objCompany.funGetCompanyLogo
        If strLogo <> "" Then
            imgHeader.Src = ConfigurationManager.AppSettings("CompanyLogoPath") & strLogo
            strPic = objCommon.funImageSize(strLogo, "170", "78", "Comp", "Yes").Split("/")
            imgHeader.Width = strPic(0)
            imgHeader.Height = strPic(1)
        End If

        If Not IsPostBack Then
            Dim objPrint As New InbizUser
            'objPrint.FillSalesRepresentatives(ddlSaleRep, CurrentUser.UserDefaultWarehouse)
            Dim lstItem As New ListItem
            lstItem = New ListItem(lblSelectSaleRep, "0")
            objPrint.FillSalesRepresentatives(ddlSaleRep, BusinessUtility.GetString(Session("RegWhsCode")), lstItem)

            For Each li As ListItem In ddlSaleRep.Items
                'If li.Value = Convert.ToString(CurrentUser.UserID) Then
                '    li.Selected = True
                'End If
            Next
        End If

    End Sub
    Public Sub subCopyResource()
        Dim sourcePath As String = Server.MapPath("../translator/Label/")
        Dim DestinationPath As String = Server.MapPath("../App_globalResources/")
        If (Directory.Exists(sourcePath)) Then
            For Each fName As String In Directory.GetFiles(sourcePath)
                If File.Exists(fName) Then
                    Dim dFile As String = String.Empty
                    dFile = Path.GetFileName(fName)
                    Dim dFilePath As String = String.Empty
                    dFilePath = DestinationPath + dFile
                    'If Not System.IO.File.Exists(dFilePath) Then
                    File.Copy(fName, dFilePath, True)
                    'End If
                End If
            Next
        End If
    End Sub
    Protected Sub imgCmdHelp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCmdHelp.Click
        Response.Write("<script>window.open('../help.aspx', 'Help','height=500,width=700,right=150,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');</script>")
    End Sub
End Class

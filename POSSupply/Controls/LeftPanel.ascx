<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LeftPanel.ascx.vb" Inherits="Admin_LeftPanel" %>
<table align="left" style="border: 0" border="0">
    <tr>
        <td align="left" width="170px">
            <div id="divADM" runat="server" align="left" style="clear: left;">
                <table align="left" width="100%" class="table" style="border: 0" border="0">
                    <tr>
                        <td height="15">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" style="padding-left: 10px;">
                            <asp:Label ID="lblAdministration" runat="server" Text="<%$ Resources:Resource, Administration %>"
                                CssClass="lblBold">
                            </asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="6%">
                                    </td>
                                    <td width="94%" background="../images/border1.jpg" height="2px" valign="middle">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBullet">
                            <img src="../images/bullet.gif" alt="" />
                        </td>
                        <td align="left" width="82%">
                            <asp:LinkButton ID="lnkInternalUser" runat="server" Text="<%$ Resources:Resource, InternalUser%>"
                                CausesValidation="false" CssClass="leftlink">
                            </asp:LinkButton>
                        </td>
                        <td width="2%">
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBullet">
                            <img src="../images/bullet.gif" alt="" />
                        </td>
                        <td align="left">
                            <asp:LinkButton ID="lnkExternalUser" runat="server" Text="<%$ Resources:Resource, ExternalUser%>"
                                CausesValidation="false" CssClass="leftlink">
                            </asp:LinkButton>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td height="15">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" style="padding-left: 10px;">
                            <asp:Label ID="lblEnterpriseParameters" runat="server" Text="<%$ Resources:Resource, EnterpriseParameters %>"
                                CssClass="lblBold">
                            </asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="6%">
                                    </td>
                                    <td width="94%" background="../images/border1.jpg" height="2px" valign="middle">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBullet">
                            <img src="../images/bullet.gif" alt="" />
                        </td>
                        <td align="left" width="82%">
                            <asp:LinkButton ID="lnkWarehouse" runat="server" Text="<%$ Resources:Resource, Warehouse%>"
                                CausesValidation="false" CssClass="leftlink">
                            </asp:LinkButton>
                        </td>
                        <td width="2%">
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBullet">
                            <img src="../images/bullet.gif" alt="" />
                        </td>
                        <td align="left" width="82%">
                            <asp:LinkButton ID="lnkGlobalParameters" runat="server" Text="<%$ Resources:Resource, GlobalParameters%>"
                                CausesValidation="false" CssClass="leftlink">
                            </asp:LinkButton>
                        </td>
                        <td width="2%">
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBullet">
                            <img src="../images/bullet.gif" alt="" />
                        </td>
                        <td align="left" width="82%">
                            <asp:LinkButton ID="lnkExchangeRates" runat="server" Text="<%$ Resources:Resource, ExchangeRates%>"
                                CausesValidation="false" CssClass="leftlink">
                            </asp:LinkButton>
                        </td>
                        <td width="2%">
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                </table>
            </div>
            <%--<br clear="left" />--%>
            <div id="divVMN" runat="server" align="left" style="clear: left;">
                <table align="left" width="100%" class="table" style="border: 0" border="0">
                    <tr>
                        <td height="15">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" style="padding-left: 10px;">
                            <asp:Label Text="<%$Resources:Resource, lblVendorManagement %>"  ID="lblVendorManagement" runat="server" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="6%">
                                    </td>
                                    <td width="94%" background="../images/border1.jpg" height="2px" valign="middle">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBullet">
                            <img src="../images/bullet.gif" alt="" />
                        </td>
                        <td align="left" width="82%">
                            <asp:LinkButton ID="lnkVendor" runat="server" Text="<%$ Resources:Resource, lblVendor%>"
                                CausesValidation="false" CssClass="leftlink">
                            </asp:LinkButton>
                        </td>
                        <td width="2%">
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                </table>
            </div>
            <%--<br clear="left" />--%>
            <div id="divCRM" runat="server" align="left" style="clear: left;">
                <table align="left" width="100%" class="table" style="border: 0" border="0">
                    <tr>
                        <td height="15">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" style="padding-left: 10px;">
                            <asp:Label ID="lblCustomerRelationshipManagement" runat="server" Text="<%$ Resources:Resource, lblCustomerRelationship %>"
                                CssClass="lblBold">
                            </asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="6%">
                                    </td>
                                    <td width="94%" background="../images/border1.jpg" height="2px" valign="middle">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBullet">
                            <img src="../images/bullet.gif" alt="" />
                        </td>
                        <td align="left" width="82%">
                            <asp:LinkButton ID="lnkDistributor" runat="server" Text="<%$ Resources:Resource, Distributor%>"
                                CausesValidation="false" CssClass="leftlink">
                            </asp:LinkButton>
                        </td>
                        <td width="2%">
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBullet">
                            <img src="../images/bullet.gif" alt="" />
                        </td>
                        <td align="left" width="82%">
                            <asp:LinkButton ID="lnkReseller" runat="server" Text="<%$ Resources:Resource, Reseller%>"
                                CausesValidation="false" CssClass="leftlink">
                            </asp:LinkButton>
                        </td>
                        <td width="2%">
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBullet">
                            <img src="../images/bullet.gif" alt="" />
                        </td>
                        <td align="left" width="82%">
                            <asp:LinkButton ID="lnkAddresses" runat="server" Text="<%$ Resources:Resource, lblDisRslAdd%>"
                                CausesValidation="false" CssClass="leftlink">
                            </asp:LinkButton>
                        </td>
                        <td width="2%">
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                </table>
            </div>
            <%--<br clear="left" />--%>
            <div id="divINV" runat="server" align="left" style="clear: left;">
                <table align="left" width="100%" class="table" style="border: 0" border="0">
                    <tr>
                        <td height="15">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" style="padding-left: 10px;">
                            <asp:Label ID="lblInventoryManagement" runat="server" Text="<%$ Resources:Resource, lblInventoryManagement %>"
                                CssClass="lblBold">
                            </asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="6%">
                                    </td>
                                    <td width="94%" background="../images/border1.jpg" height="2px" valign="middle">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBullet">
                            <img src="../images/bullet.gif" alt="" />
                        </td>
                        <td align="left" width="82%">
                            <asp:LinkButton ID="lnkInventory" runat="server" Text="<%$ Resources:Resource, lblStock%>"
                                CausesValidation="false" CssClass="leftlink">
                            </asp:LinkButton>
                        </td>
                        <td width="2%">
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                </table>
            </div>
            <%--<br clear="left" />--%>
            <div id="divPRO" runat="server" align="left" style="clear: left;">
                <table align="left" width="100%" class="table" style="border: 0" border="0">
                    <tr>
                        <td height="15">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" style="padding-left: 10px;">
                            <asp:Label ID="lblProcurement" runat="server" Text="<%$ Resources:Resource, lblProcurement %>"
                                CssClass="lblBold">
                            </asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="6%">
                                    </td>
                                    <td width="94%" background="../images/border1.jpg" height="2px" valign="middle">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBullet">
                            <img src="../images/bullet.gif" alt="" />
                        </td>
                        <td align="left" width="82%">
                            <asp:LinkButton ID="lnkProcurement" runat="server" Text="<%$ Resources:Resource, lblPurchaseOrder%>"
                                CausesValidation="false" CssClass="leftlink">
                            </asp:LinkButton>
                        </td>
                        <td width="2%">
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                </table>
            </div>
            <%--<br clear="left" />--%>
            <div id="divRCV" runat="server" align="left" style="clear: left;">
                <table align="left" width="100%" class="table" style="border: 0" border="0">
                    <tr>
                        <td height="15">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" style="padding-left: 10px;">
                            <asp:Label ID="lblReceiving" runat="server" Text="<%$ Resources:Resource, lblReceiving %>"
                                CssClass="lblBold">
                            </asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="6%">
                                    </td>
                                    <td width="94%" background="../images/border1.jpg" height="2px" valign="middle">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBullet">
                            <img src="../images/bullet.gif" alt="" />
                        </td>
                        <td align="left" width="82%">
                            <asp:LinkButton ID="lnkReceiving" runat="server" Text="<%$ Resources:Resource, lblReceiving%>"
                                CausesValidation="false" CssClass="leftlink">
                            </asp:LinkButton>
                        </td>
                        <td width="2%">
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                </table>
            </div>
            <%--<br clear="left" />--%>
            <div id="divSAL" runat="server" align="left" style="clear: left;">
                <table align="left" width="100%" class="table" style="border: 0" border="0">
                    <tr>
                        <td height="15">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" style="padding-left: 10px;">
                            <asp:Label ID="lblSales" runat="server" Text="<%$ Resources:Resource, lblSales %>"
                                CssClass="lblBold">
                            </asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="6%">
                                    </td>
                                    <td width="94%" background="../images/border1.jpg" height="2px" valign="middle">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBullet">
                            <img src="../images/bullet.gif" alt="" />
                        </td>
                        <td align="left" width="82%">
                            <asp:LinkButton ID="lnkSales" runat="server" Text="<%$ Resources:Resource, lblSales%>"
                                CausesValidation="false" CssClass="leftlink">
                            </asp:LinkButton>
                        </td>
                        <td width="2%">
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                </table>
            </div>
            <%--<br clear="left" />--%>
            <div id="divSHP" runat="server" align="left" style="clear: left;">
                <table align="left" width="100%" class="table" style="border: 0" border="0">
                    <tr>
                        <td height="15">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" style="padding-left: 10px;">
                            <asp:Label ID="lblShipping" runat="server" Text="<%$ Resources:Resource, lblShipping %>"
                                CssClass="lblBold">
                            </asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="6%">
                                    </td>
                                    <td width="94%" background="../images/border1.jpg" height="2px" valign="middle">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBullet">
                            <img src="../images/bullet.gif" alt="" />
                        </td>
                        <td align="left" width="82%">
                            <asp:LinkButton ID="lnkShipping" runat="server" Text="<%$ Resources:Resource, lblShipping%>"
                                CausesValidation="false" CssClass="leftlink">
                            </asp:LinkButton>
                        </td>
                        <td width="2%">
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                </table>
            </div>
            <%--<br clear="left" />--%>
            <div id="divINVGen" runat="server" align="left" style="clear: left;">
                <table align="left" width="100%" class="table" style="border: 0" border="0">
                    <tr>
                        <td height="15">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" style="padding-left: 10px;">
                            <asp:Label ID="lblInvoiceGeneration" runat="server" Text="<%$ Resources:Resource, lblInvoiceGeneration %>"
                                CssClass="lblBold">
                            </asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="6%">
                                    </td>
                                    <td width="94%" background="../images/border1.jpg" height="2px" valign="middle">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBullet">
                            <img src="../images/bullet.gif" alt="" />
                        </td>
                        <td align="left" width="82%">
                            <asp:LinkButton ID="lnkInvoice" runat="server" Text="<%$ Resources:Resource, lblInvoice%>"
                                CausesValidation="false" CssClass="leftlink">
                            </asp:LinkButton>
                        </td>
                        <td width="2%">
                        </td>
                    </tr>
                    <tr>
                        <td height="1">
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>

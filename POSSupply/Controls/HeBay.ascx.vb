
Partial Class Controls_HeBay
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not String.IsNullOrEmpty(Request.QueryString("t")) Then
            Session.Remove("st")
        End If

        SetCurrentTab()
    End Sub

    Private Sub SetCurrentTab()
        If Session("st") = "0" Then
            cmdeBayProductExport.Attributes.Add("class", "current")
        ElseIf Session("st") = "1" Then
            cmdeBaySOImport.Attributes.Add("class", "current")
        Else
            cmdeBayProductExport.Attributes.Add("class", "current")
        End If
    End Sub

    Protected Sub cmdeBayProductExport_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdeBayProductExport.ServerClick
        Session("st") = "0"
        Response.Redirect("~/eBay/ProductExport.aspx")
    End Sub

    Protected Sub cmdeBaySOImport_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdeBaySOImport.ServerClick
        Session("st") = "1"
        Response.Redirect("~/eBay/SOImport.aspx")
    End Sub
End Class
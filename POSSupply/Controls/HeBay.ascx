<%@ Control Language="VB" AutoEventWireup="false" CodeFile="HeBay.ascx.vb" Inherits="Controls_HeBay" %>

<div id="tabsH">
    <ul id="ulMainMenu" runat="server">
        <%--<li>
            <asp:Label ID="lblPO" runat="server" CssClass="heading" Text="<%$ Resources:Resource, lbleBay %>"></asp:Label>
        </li>--%>
        <li><a id="cmdeBayProductExport" runat="server" causesvalidation="false" href="#"><span
            style="min-width: 120px; text-align: center;">
            <%=Resources.Resource.lbleBayProductExport%>
        </span></a></li>
        <li><a id="cmdeBaySOImport" runat="server" causesvalidation="false" href="#"><span
            style="min-width: 120px; text-align: center;">
            <%=Resources.Resource.lbleBaySOImport%>
        </span></a></li>
    </ul>
</div>

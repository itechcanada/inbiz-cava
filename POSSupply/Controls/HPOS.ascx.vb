
Partial Class Controls_HPOS
    Inherits System.Web.UI.UserControl

    Protected Sub POS_LeftPOS_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If

        If Not String.IsNullOrEmpty(Request.QueryString("t")) Then
            Session.Remove("st")
        End If        
        If Not String.IsNullOrEmpty(Request.QueryString("TType")) And Not String.IsNullOrEmpty(Request.QueryString("TID")) Then
            If Request.QueryString("TType").Equals("P") Then
                Session("st") = "1"
            End If
        End If
        SetCurrentTab()
    End Sub

    Private Sub SetCurrentTab()
        If Session("st") = "0" Then
            imgCmdRefund.Attributes.Add("class", "current")
        ElseIf Session("st") = "1" Then
            imgCmdSell.Attributes.Add("class", "current")
        ElseIf Session("st") = "2" Then
            imgCmdPaused.Attributes.Add("class", "current")
        ElseIf Session("st") = "3" Then
            imgCmdPinPadOpen.Attributes.Add("class", "current")
        ElseIf Session("st") = "4" Then
            imgCmdPinPadClose.Attributes.Add("class", "current")
        ElseIf Session("st") = "5" Then
            imgReceipts.Attributes.Add("class", "current")
        ElseIf Session("st") = "6" Then
            imgMgnReg.Attributes.Add("class", "current")
        ElseIf Session("st") = "7" Then
            imgMgnKitchen.Attributes.Add("class", "current")
        Else
            imgCmdSell.Attributes.Add("class", "current")
        End If
    End Sub
    Protected Sub imgMgnKitchen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgMgnKitchen.ServerClick
        Session("st") = "7"
        Response.Redirect("~/POS/Kitchen.aspx")
    End Sub

    Protected Sub imgCmdRefund_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdRefund.ServerClick
        Session("st") = "0"
        Session.Remove("transactionType")
        Response.Redirect("~/POS/refund.aspx?TType=R")
    End Sub

    Protected Sub imgCmdSell_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdSell.ServerClick
        Session("st") = "1"
        Session.Remove("transactionType")
        Response.Redirect("~/POS/POS.aspx")
    End Sub

    Protected Sub imgCmdPaused_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPaused.ServerClick
        Session("st") = "2"
        Session.Remove("transactionType")
        Response.Redirect("~/POS/refund.aspx?TType=P")
    End Sub

    Protected Sub imgCmdPinPadOpen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPinPadOpen.ServerClick
        Session("st") = "3"
        Session.Remove("transactionType")
        Response.Redirect("~/POS/PinPad.aspx?PinPad=TI")
    End Sub

    Protected Sub imgCmdPinPadClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPinPadClose.ServerClick
        Session("st") = "4"
        Session.Remove("transactionType")
        Response.Redirect("~/POS/PinPad.aspx?PinPad=TS")
    End Sub
    Protected Sub imgReceipts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgReceipts.ServerClick
        Session("st") = "5"
        Session.Remove("transactionType")
        Response.Redirect("~/POS/Receipts.aspx")
    End Sub
    Protected Sub imgMgnReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgMgnReg.ServerClick
        Session("st") = "6"
        Session.Remove("transactionType")
        Response.Redirect("~/POS/ManageRegister.aspx")
    End Sub
End Class




















<%@ Control Language="VB" AutoEventWireup="false" CodeFile="HSales.ascx.vb" Inherits="Controls_HSales" %>

<div id="tabsH">
    <ul id="ulMainMenu" runat="server">
        <%--<li><span class="heading">
            <%=Resources.Resource.ManageOrders%>
        </span></li>--%>
        <% If Session("UserModules").ToString.Contains("SAL") = True Or Session("UserModules").ToString.Contains("ADM") = True Then%>
        <li><a id="imgCmdView" runat="server" causesvalidation="false" href="#"><span style="min-width: 120px;
            text-align: center;">
            <%=Resources.Resource.btnViewOrders%>
        </span></a></li>
        <% End If%>
        <% If Session("UserModules").ToString.Contains("QOA") = True Or Session("UserModules").ToString.Contains("ADM") = True Then%>
        <li style="display:none;"><a id="imgCmdApprove" runat="server" causesvalidation="false" href="#"><span
            style="min-width: 120px; text-align: center;">
            <%=Resources.Resource.btnApproveOrder%>
        </span></a></li>
        <%End If%>
        <% If Session("UserModules").ToString.Contains("SAL") = True Or Session("UserModules").ToString.Contains("ADM") = True Then%>
        <li><a id="imgCmdCreate" runat="server" causesvalidation="false" href="#"><span style="min-width: 120px;
            text-align: center;">
            <%=Resources.Resource.btnCreateOrder%>
        </span></a></li>
        <% End If%>
        <% If Request.QueryString("custID") <> "" Then%>
        <%--<li>
            <asp:Label CssClass="heading" ID="lblName" Text="" runat="server">
            </asp:Label></li>--%>
        <li style="display:none;"><a id="imgCmdAddSalesProcess" runat="server" causesvalidation="false" href="#"><span
            style="min-width: 120px; text-align: center;">
            <%=Resources.Resource.btnAddSalesProcess%>
        </span></a></li>
        <li><a id="imgCmdShowQuotation" runat="server" causesvalidation="false" visible="false"
            href="#"><span style="min-width: 120px; text-align: center;">
                <%=Resources.Resource.btnShowQuotation%>
            </span></a></li>
        <%End If%>
    </ul>
</div>

<script language="javascript" type="text/javascript">
     function openPopup(strOpen) {
         open(strOpen, "AddItemProcess", 'height=365,width=840,left=180,top=150,resizable=no,location=no,scrollbars=yes,toolbar=no,status=no');
         return false;
     }
</script>

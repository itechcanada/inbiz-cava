
Partial Class eBay_LeftPaneleBay
    Inherits System.Web.UI.UserControl

    Protected Sub cmdeBayProductExport_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdeBayProductExport.ServerClick
        Response.Redirect("~/eBay/ProductExport.aspx")
    End Sub

    Protected Sub cmdeBaySOImport_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdeBaySOImport.ServerClick
        Response.Redirect("~/eBay/SOImport.aspx")
    End Sub
End Class

Partial Class Controls_HSales
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("custID") <> "" And Request.QueryString("custName") <> "" Then
            'lblName.Text = lblSOCustomer & " " & Request.QueryString("custName")
            Me.imgCmdAddSalesProcess.Attributes.Add("OnClick", "return openPopup( '" & ResolveUrl("~/Sales/CreateSalesProcess.aspx") & "?custID=" & Request.QueryString("custID") & "&custName=" & Request.QueryString("custName") & "&ComID=" & Request.QueryString("ComID") & "&whsID=" & Request.QueryString("whsID") & "')")
        End If
        If Not String.IsNullOrEmpty(Request.QueryString("t")) Then
            Session.Remove("st")
        End If

        SetCurrentTab()
    End Sub

    Private Sub SetCurrentTab()
        If Session("st") = "0" Then
            imgCmdApprove.Attributes.Add("class", "current")
        ElseIf Session("st") = "1" Then
            imgCmdCreate.Attributes.Add("class", "current")
        ElseIf Session("st") = "2" Then
            imgCmdView.Attributes.Add("class", "current")
        ElseIf Session("st") = "3" Then
            imgCmdShowQuotation.Attributes.Add("class", "current")
        Else

        End If
    End Sub

    Protected Sub imgCmdApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdApprove.ServerClick
        Session("st") = "0"
        Response.Redirect("~/Sales/Default.aspx?Default=1")
    End Sub

    Protected Sub imgCmdCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdCreate.ServerClick
        Session("st") = "1"
        Session("SalesCart") = ""
        Session("IsSalesCart") = ""
        Session("SalesProcessCart") = ""
        Session("IsSalesProcessCart") = ""
        Response.Redirect("~/Sales/Create.aspx")
    End Sub

    Protected Sub imgCmdView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdView.ServerClick
        Session("st") = "2"
        Response.Redirect("~/Sales/ViewSalesOrder.aspx?Default=1")
    End Sub

    'Protected Sub imgCmdAddSalesProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdAddSalesProcess.Click
    '    Response.Redirect("CreateSalesProcess.aspx?custID=" & Request.QueryString("custID") & "&custName=" & Request.QueryString("custName"))
    'End Sub

    Protected Sub imgCmdShowQuotation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdShowQuotation.ServerClick
        Session("st") = "3"
        Response.Redirect("~/Sales/Create.aspx?custID=" & Request.QueryString("custID") & "&custName=" & Request.QueryString("custName"))
    End Sub
End Class

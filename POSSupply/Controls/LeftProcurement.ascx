<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LeftProcurement.ascx.vb" Inherits="LeftProcurement" %>
  <div>
  <BR /><BR />
  <table align=left width=100% class="table" style="border:0" border=0 >
     <tr height=25>
       <td align=center class=titleBgColor ><%--tdborder--%>
            <asp:Label ID="lblPO" runat=server cssclass="leftHeading" 
            Text="<%$ Resources:Resource, ManagePurchaseOrders %>" ></asp:Label></td>
    </tr>
    <tr><td height=5></td></tr>
    <% if Session("UserModules").ToString.Contains("PRO") = True OR Session("UserModules").ToString.Contains("ADM") = True %>
    <tr>
        <td align=right>
            <div class="buttonwrapper"><a ID="imgCmdViewPO" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnViewPurchaseOrder%></span></a></div>
        </td>
    </tr>
    <tr><td height=2></td></tr>
    <% end if %>
     <% if Session("UserModules").ToString.Contains("POA") = True OR Session("UserModules").ToString.Contains("ADM") = True %>
    <tr>
        <td align=right>
            <div class="buttonwrapper"><a ID="imgCmdApprovePO" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnApprovePO%></span></a></div>
        </td>
    </tr>  
    <% end if %>
    <tr><td height=2></td></tr>
    <% if Session("UserModules").ToString.Contains("PRO") = True OR Session("UserModules").ToString.Contains("ADM") = True %>
    <tr>
        <td align=right>
            <div class="buttonwrapper"><a ID="imgCmdCreatePO" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnCreatePO%></span></a></div>
        </td>
    </tr>
    <% end if %>
     <tr><td height=2></td></tr>
    <tr runat="server" visible="false">
        <td align=right>
            <div class="buttonwrapper"><a ID="imgCmdUpdatePO" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnUpdatePO%></span></a></div>
        </td>
    </tr>
    <% if Request.QueryString("vdrID")<>"" then %>
     <tr><td height=10></td></tr>
     <tr height=25>
       <td align=center class=tdBorder>
            <asp:Label ID="lblVendorName" runat=server cssclass="leftHeading" >
            </asp:Label>
       </td>
    </tr>
    <tr><td height=5></td></tr>
    <tr>
        <td align=right>
            <div class="buttonwrapper"><a ID="imgCmdAddList" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnproddesc%></span></a></div>
        </td>
    </tr>
    <%end if %>
    <tr><td height=10></td></tr>
  </table> 
  </div>
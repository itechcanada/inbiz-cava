<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LeftPOS.ascx.vb" Inherits="POS_LeftPOS" %>

<script type="text/javascript" language="JavaScript" src="../scripts/akModal/akModal.js"></script>
<script type="text/javascript" language="JavaScript" src="../scripts/akModal/dimmer.js"></script>
 <div>
     <BR />
     <table align=left width=100% class="table" style="border:0" border=0 >
        <tr height=25>
         <td align=center class=titleBgColor >
                <asp:Label ID="lblAdministration" runat=server cssclass="leftHeading" Text="<%$ Resources:Resource, POSHeading%>" ></asp:Label></td>
        </tr>
        <tr><td height=5></td></tr>
          <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID=imgCmdSell runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnsell%></span></a></div>
            </td>
        </tr>
        <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID=imgCmdRefund runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnrefund%></span></a></div>
            </td>
        </tr>
          <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID=imgCmdPaused runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnTabBig%></span></a></div>
            </td>
        </tr>
         <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID=imgReceipts runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnTabrecipt%></span></a></div>
            </td>
        </tr>
        
        <% if (Session("UserModules").ToString.Contains("ADM") = True OR Session("UserModules").ToString.Contains("PSMGR") = True) and  ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" And (Request.Url.AbsoluteUri.ToLower.Contains("/pos.aspx") Or Request.Url.AbsoluteUri.Tolower.Contains("/refund.aspx?ttype=r")) then %>
         <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID=imgCmdPinPadOpen runat=server CausesValidation=false onclick="doAppletTransaction('TI'); return false;" class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnInitializePinPad%></span></a></div>
            </td>
        </tr>
          <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID=imgCmdPinPadClose runat=server CausesValidation=false onclick="doAppletTransaction('TS'); return false;" class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnCloseBalanceRegister%></span></a></div>
            </td>
        </tr>
        <% end if %>
        
        <% if (Session("UserModules").ToString.Contains("ADM") = True OR Session("UserModules").ToString.Contains("PSMGR") = True) then %>
         <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID=imgMgnReg runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnManageRegister%></span></a></div>
            </td>
        </tr>
          <% end if %>
     </table> 
</div>   
        
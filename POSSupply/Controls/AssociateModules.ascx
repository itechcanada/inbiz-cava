<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssociateModules.ascx.vb" Inherits="Controls_AssociateModules" %>
<table width=100%>
  <tr height=30 valign="top">
            <td class="tdAlign" width="30%">
                <asp:Label ID="Label3" runat=server CssClass="lblBold" 
                Text="<%$ Resources:Resource, lblGeneralAdministration %>" ></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="CheckBox1" runat="server" />
            </td>
            <td width="3%"></td>
            <td class="tdAlignLeft" width="65%">
                <asp:CheckBoxList ID="chklstAdmin" runat="server" RepeatDirection=vertical RepeatColumns=3>
                    <asp:ListItem Text="<%$ Resources:Resource, liADM %>" Value="ADM"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liENT %>" Value="ENT"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liRPT %>" Value="RPT"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liFAX %>" Value="FAX"/>
                </asp:CheckBoxList>
            </td>
        </tr>
          <tr><td height="5"></td></tr>
        <tr height=30 valign="top">
            <td class="tdAlign">
                <asp:Label ID="Label4" runat=server CssClass="lblBold" 
                Text="<%$ Resources:Resource, lblSalesManager %>" ></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="CheckBox2" runat="server" />
            </td>
            <td ></td>
            <td class="tdAlignLeft">
                <asp:CheckBoxList ID="chklstSM" runat="server" RepeatDirection=vertical RepeatColumns=3>
                   <asp:ListItem Text="<%$ Resources:Resource, liCRM %>" Value="CRM"/>
                  <asp:ListItem Text="<%$ Resources:Resource, liSAL %>" Value="SAL"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liSR %>" Value="SR"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liQOA %>" Value="QOA"/>
                     <asp:ListItem Text="<%$ Resources:Resource, liINV %>" Value="INV"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liLM %>" Value="LMA"/>
                  <asp:ListItem Text="<%$ Resources:Resource, liINVRO %>" Value="INROL"/>
                     <asp:ListItem Text="<%$ Resources:Resource, liRPT %>" Value="RPT"/>
                   <asp:ListItem Text="<%$ Resources:Resource, liMSG %>" Value="MSG"/>
                </asp:CheckBoxList>
            </td>
        </tr>
          <tr><td height="5"></td></tr>
        <tr height=30 valign="top">
            <td class="tdAlign">
                <asp:Label ID="Label5" runat=server CssClass="lblBold" 
                Text="<%$ Resources:Resource, lblSalesAgents %>" ></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="CheckBox3" runat="server" />
            </td>
            <td ></td>
            <td class="tdAlignLeft">
                <asp:CheckBoxList ID="chklstSAgents" runat="server" RepeatDirection=vertical RepeatColumns=3>
                   <asp:ListItem Text="<%$ Resources:Resource, liCRM %>" Value="CRM"/>
                   <asp:ListItem Text="<%$ Resources:Resource, liSAL %>" Value="SAL"/>
                   <asp:ListItem Text="<%$ Resources:Resource, liSR %>" Value="SR"/>
                   <asp:ListItem Text="<%$ Resources:Resource, liINVRO %>" Value="INROL"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liMSG %>" Value="MSG"/>
                </asp:CheckBoxList>
            </td>
        </tr>
          <tr><td height="5"></td></tr>
        <tr height=30 valign="top">
            <td class="tdAlign">
                <asp:Label ID="Label6" runat=server CssClass="lblBold" 
                Text="<%$ Resources:Resource, lblProcurementManager %>" ></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="CheckBox4" runat="server" />
            </td>
            <td ></td>
            <td class="tdAlignLeft">
                <asp:CheckBoxList ID="chklstPM" runat="server" RepeatDirection=vertical RepeatColumns=3>
                   <asp:ListItem Text="<%$ Resources:Resource, liVMN %>" Value="VMN"/>
                     <asp:ListItem Text="<%$ Resources:Resource, liPRO %>" Value="PRO"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liRCV %>" Value="RCV"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liSTK %>" Value="STK"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liPOA %>" Value="POA"/>
                     <asp:ListItem Text="<%$ Resources:Resource, liMSG %>" Value="MSG"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liRPT %>" Value="RPT"/>
                </asp:CheckBoxList>
            </td>
        </tr>
          <tr><td height="5"></td></tr>
        <tr height=30 valign="top">
            <td class="tdAlign">
                <asp:Label ID="Label7" runat=server CssClass="lblBold" 
                Text="<%$ Resources:Resource, lblProcurementAgent %>" ></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="CheckBox5" runat="server" />
            </td>
            <td ></td>
            <td class="tdAlignLeft">
                <asp:CheckBoxList ID="chklstPA" runat="server" RepeatDirection=vertical RepeatColumns=3>
                     <asp:ListItem Text="<%$ Resources:Resource, liVMN %>" Value="VMN"/>
                     <asp:ListItem Text="<%$ Resources:Resource, liPRO %>" Value="PRO"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liRCV %>" Value="RCV"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liSTK %>" Value="STK"/>
         <asp:ListItem Text="<%$ Resources:Resource, liMSG %>" Value="MSG"/>
                </asp:CheckBoxList>
            </td>
        </tr>
          <tr><td height="5"></td></tr>
        <tr height=30 valign="top">
            <td class="tdAlign">
                <asp:Label ID="Label8" runat=server CssClass="lblBold" 
                Text="<%$ Resources:Resource, lblOperationManager %>" ></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="CheckBox6" runat="server" />
            </td>
            <td ></td>
            <td class="tdAlignLeft">
                <asp:CheckBoxList ID="chklstOM" runat="server" RepeatDirection=vertical RepeatColumns=3>
                     <asp:ListItem Text="<%$ Resources:Resource, liJOBP %>" Value="JOBP"/>
                     <asp:ListItem Text="<%$ Resources:Resource, liJBPM %>" Value="JBPM"/>
                     <asp:ListItem Text="<%$ Resources:Resource, liSHP %>" Value="SHP"/>
                     <asp:ListItem Text="<%$ Resources:Resource, liSAL %>" Value="SAL"/>
                     <asp:ListItem Text="<%$ Resources:Resource, liRCV %>" Value="RCV"/>
                     <asp:ListItem Text="<%$ Resources:Resource, liSTK %>" Value="STK"/>
                     <asp:ListItem Text="<%$ Resources:Resource, liMSG %>" Value="MSG"/>
                </asp:CheckBoxList>
            </td>
        </tr>
         <tr><td height="5"></td></tr> 
         <tr height=30 valign="top">
            <td class="tdAlign">
                <asp:Label ID="Label9" runat=server CssClass="lblBold" 
                Text="<%$ Resources:Resource, lblAssemblerPicker %>" ></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="CheckBox7" runat="server" />
            </td>
            <td ></td>
            <td class="tdAlignLeft">
                <asp:CheckBoxList ID="chklstAP" runat="server" RepeatDirection=vertical RepeatColumns=3>
                   <asp:ListItem Text="<%$ Resources:Resource, liJOBP %>" Value="JOBP"/>
                     <asp:ListItem Text="<%$ Resources:Resource, liRCV %>" Value="RCV"/>
                     <asp:ListItem Text="<%$ Resources:Resource, liMSG %>" Value="MSG"/>
                </asp:CheckBoxList>
            </td>
        </tr>
       <tr><td height="5"></td></tr>
        <tr height=30 valign="top">
            <td class="tdAlign">
                <asp:Label ID="Label10" runat=server CssClass="lblBold" 
                Text="<%$ Resources:Resource, lblPOSM %>" ></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="CheckBox8" runat="server" />
            </td>
            <td ></td>
            <td class="tdAlignLeft">
                <asp:CheckBoxList ID="chklstPOSM" runat="server" RepeatDirection=vertical RepeatColumns=3>
                  <asp:ListItem Text="<%$ Resources:Resource, liPOSMGR %>" Value="PSMGR"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liPOS %>" Value="POS"/>
                   <asp:ListItem Text="<%$ Resources:Resource, liRPT %>" Value="RPT"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liMSG %>" Value="MSG"/>
                </asp:CheckBoxList>
            </td>
        </tr> 
        <tr><td height="5"></td></tr>
          <tr height=30 valign="top">
            <td class="tdAlign">
                <asp:Label ID="Label11" runat=server CssClass="lblBold" 
                Text="<%$ Resources:Resource, lblPOSU %>" ></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="CheckBox9" runat="server" />
            </td>
            <td ></td>
            <td class="tdAlignLeft">
                <asp:CheckBoxList ID="chklstPOS" runat="server" RepeatDirection=vertical RepeatColumns=3>
                   <asp:ListItem Text="<%$ Resources:Resource, liPOS %>" Value="POS"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liMSG %>" Value="MSG"/>
                </asp:CheckBoxList>
            </td>
        </tr>
       <tr><td height="5"></td></tr> 
         <tr height=30 valign="top">
            <td class="tdAlign">
                <asp:Label ID="Label12" runat=server CssClass="lblBold" 
                Text="<%$ Resources:Resource, lblARCM %>" ></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="CheckBox10" runat="server" />
            </td>
            <td ></td>
            <td class="tdAlignLeft">
                <asp:CheckBoxList ID="chklstARCM" runat="server" RepeatDirection=vertical RepeatColumns=3>
                 <asp:ListItem Text="<%$ Resources:Resource, liACCRCV %>" Value="ARC"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liCOL %>" Value="COL"/>
                    <asp:ListItem Text="<%$ Resources:Resource, liCUSR %>" Value="CUA"/>
                   <asp:ListItem Text="<%$ Resources:Resource, liMSG %>" Value="MSG"/>
                </asp:CheckBoxList>
            </td>
        </tr>
      <tr><td height="5"></td></tr>  
        <tr height=30 valign="top">
            <td class="tdAlign">
                <asp:Label ID="Label13" runat=server CssClass="lblBold" 
                Text="<%$ Resources:Resource, lblCollectionAgent %>" ></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="CheckBox11" runat="server" />
            </td>
            <td ></td>
            <td class="tdAlignLeft">
                <asp:CheckBoxList ID="chklstCA" runat="server" RepeatDirection=vertical RepeatColumns=3>
                 <asp:ListItem Text="<%$ Resources:Resource, liACCRCV %>" Value="ARC"/>
                   <asp:ListItem Text="<%$ Resources:Resource, liCUSR %>" Value="CUA"/>
                   <asp:ListItem Text="<%$ Resources:Resource, liMSG %>" Value="MSG"/>
                </asp:CheckBoxList>
            </td>
        </tr>
        <tr><td height="5"></td></tr>
        <tr height=30 valign="top">
            <td class="tdAlign">
                <asp:Label ID="Label14" runat=server CssClass="lblBold" 
                Text="<%$ Resources:Resource, lbleCommerceManager %>" ></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="CheckBox12" runat="server" />
            </td>
            <td ></td>
            <td class="tdAlignLeft">
                <asp:CheckBoxList ID="chklsteCM" runat="server" RepeatDirection=vertical RepeatColumns=3>
                     <asp:ListItem Text="<%$ Resources:Resource, lieCOM %>" Value="eCOM"/>
                </asp:CheckBoxList>
            </td>
        </tr>
</table>


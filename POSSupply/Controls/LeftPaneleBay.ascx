<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LeftPaneleBay.ascx.vb" Inherits="eBay_LeftPaneleBay" %>
<div>
  <BR /><BR />
  <table align=left width=100% class="table" style="border:0" border=0 >
     <tr height=25>
       <td align=center class=titleBgColor >
            <asp:Label ID="lblPO" runat=server cssclass="leftHeading" 
            Text="<%$ Resources:Resource, lbleBay %>" ></asp:Label></td>
    </tr>
    <tr><td height=5></td></tr>
    <tr>
        <td align=right>
            <div class="buttonwrapper"><a ID="cmdeBayProductExport" runat="server" CausesValidation="false" class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.lbleBayProductExport%></span></a></div>
        </td>
    </tr>
    <tr><td height=2></td></tr>
    <tr>
        <td align=right>
            <div class="buttonwrapper"><a ID="cmdeBaySOImport" runat="server" CausesValidation="false" class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.lbleBaySOImport%></span></a></div>
        </td>
    </tr>  
    <tr><td height=10></td></tr>
  </table> 
  </div>
Imports Resources.Resource
Imports System.Threading
Imports System.Globalization

Partial Class Controls_HInventory
    Inherits System.Web.UI.UserControl
    Public objPrdDesc As New clsPrdDescriptions
    Protected Sub InventoryLeft_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Request.QueryString("PrdID") <> "" Then
            'lblPrdName.Text = objPrdDesc.funProductName
            'If lblPrdName.Text <> "" Then
            '    lblPrdName.Text = lblPrdName.Text.TrimEnd("/")
            'End If
        End If

        If Request.QueryString("eComCheck") = "0" Then
            Session.Remove("eComCheck")
        End If

        If Request.QueryString("eComCheck") = "1" Or Session("eComCheck") = "True" Then
            Session("eComCheck") = "True"
        End If

        If Not String.IsNullOrEmpty(Request.QueryString("t")) Then
            Session.Remove("st")
            If Not String.IsNullOrEmpty(Request.QueryString("st")) Then
                Session("st") = Request.QueryString("st")
            End If
        End If

        If Session("UserModules").ToString.Contains("INROL") = True And Session("UserModules").ToString.Contains("STK") = False Then
            'lblInvHeading.Text = lblInventoryReadOnly
        End If

        SetCurrentTab()
    End Sub

    Private Sub SetCurrentTab()
        If Session("st") = "0" Then
            imgCmdPrdKit.Attributes.Add("class", "current")
        ElseIf Session("st") = "1" Then
            imgCmdAddPrd.Attributes.Add("class", "current")
        ElseIf Session("st") = "2" Then
            imgCmdUpdPrd.Attributes.Add("class", "current")
        ElseIf Session("st") = "3" Then
            imgCmdAssociteVendor.Attributes.Add("class", "current")
        ElseIf Session("st") = "4" Then
            imgCmdDescri.Attributes.Add("class", "current")
        ElseIf Session("st") = "5" Then
            imgCmdPrdAss.Attributes.Add("class", "current")
        ElseIf Session("st") = "6" Then
            imgCmdPrdColor.Attributes.Add("class", "current")
        ElseIf Session("st") = "7" Then
            imgCmdPrdImages.Attributes.Add("class", "current")
        ElseIf Session("st") = "8" Then
            imgCmdPrdSale.Attributes.Add("class", "current")
        ElseIf Session("st") = "9" Then
            imgCmdViewPrd.Attributes.Add("class", "current")
        ElseIf Session("st") = "10" Then
            imgCmdPrdQty.Attributes.Add("class", "current")
        ElseIf Session("st") = "11" Then
            imgAddCategory.Attributes.Add("class", "current")
        ElseIf Session("st") = "12" Then
            imgAddSubCategory.Attributes.Add("class", "current")
        ElseIf Session("st") = "13" Then
            imgViewCategory.Attributes.Add("class", "current")
        ElseIf Session("st") = "14" Then
            imgViewSubcategory.Attributes.Add("class", "current")
        ElseIf Session("st") = "15" Then
            imgAssignProducts.Attributes.Add("class", "current")
        ElseIf Session("st") = "16" Then
            imgview.Attributes.Add("class", "current")
        ElseIf Session("st") = "17" Then
            imgeComMetaTag.Attributes.Add("class", "current")
        ElseIf Session("st") = "18" Then
            ImgeComManageWebPages.Attributes.Add("class", "current")
        Else
            If Session("eComCheck") = "True" Then
                imgViewCategory.Attributes.Add("class", "current")
            Else
                imgCmdViewPrd.Attributes.Add("class", "current")
            End If
        End If

        If Request.QueryString("prdID") <> "" Then
            imgCmdViewPrd.Attributes.Add("class", "current")
        End If

    End Sub

    Protected Sub imgCmdPrdKit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPrdKit.ServerClick
        Session("st") = "0"
        'Response.Redirect("~/Inventory/ProductKit.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
        Response.Redirect(String.Format("~/Inventory/Product.aspx?PrdID={0}&Kit={1}&section=5", Request.QueryString("PrdID"), Request.QueryString("Kit")))
    End Sub
    Protected Sub imgCmdAddPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdAddPrd.ServerClick
        Session("st") = "1"
        Session.Remove("eComCheck")
        Response.Redirect("~/Inventory/Product.aspx")
    End Sub
    Protected Sub imgCmdUpdPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdUpdPrd.ServerClick
        Session("st") = "2"
        Response.Redirect("~/Inventory/InventoryUpdate.aspx")
    End Sub
    Protected Sub imgCmdAssociteVendor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdAssociteVendor.ServerClick
        Session("st") = "3"
        Response.Redirect("~/Inventory/AssociateVendor.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
    End Sub
    'Protected Sub imgCmdAvaWarehouse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdAvaWarehouse.Click
    '    Response.Redirect("AvailableWarehouse.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
    'End Sub
    Protected Sub imgCmdDescri_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdDescri.ServerClick
        Session("st") = "4"
        'Response.Redirect("~/Inventory/ProductDescription.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
        Response.Redirect(String.Format("~/Inventory/Product.aspx?PrdID={0}&Kit={1}&section=1", Request.QueryString("PrdID"), Request.QueryString("Kit")))
    End Sub
    Protected Sub imgCmdPrdAss_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPrdAss.ServerClick
        Session("st") = "5"
        Response.Redirect("~/Inventory/ProductAssociation.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
    End Sub
    Protected Sub imgCmdPrdColor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPrdColor.ServerClick
        Session("st") = "6"
        'Response.Redirect("~/Inventory/ProductColor.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
        Response.Redirect(String.Format("~/Inventory/Product.aspx?PrdID={0}&Kit={1}&section=2", Request.QueryString("PrdID"), Request.QueryString("Kit")))
    End Sub
    Protected Sub imgCmdPrdImages_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPrdImages.ServerClick
        Session("st") = "7"
        'Response.Redirect("~/Inventory/ProductImages.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
        Response.Redirect(String.Format("~/Inventory/Product.aspx?PrdID={0}&Kit={1}&section=3", Request.QueryString("PrdID"), Request.QueryString("Kit")))
    End Sub
    Protected Sub imgCmdPrdSale_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPrdSale.ServerClick
        Session("st") = "8"
        Response.Redirect("~/Inventory/ProductSalePrice.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
    End Sub
    Protected Sub imgCmdViewPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdViewPrd.ServerClick
        Session("st") = "9"
        Session.Remove("eComCheck")
        Response.Redirect("~/Inventory/ViewProduct.aspx?default=1")

    End Sub
    Protected Sub imgCmdPrdQty_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPrdQty.ServerClick
        Session("st") = "10"
        'Response.Redirect("~/Inventory/ProductQuantity.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
        Response.Redirect(String.Format("~/Inventory/Product.aspx?PrdID={0}&Kit={1}&section=4", Request.QueryString("PrdID"), Request.QueryString("Kit")))
    End Sub
    Protected Sub imgAddCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgAddCategory.ServerClick
        Session("st") = "11"
        Session.Add("eComCheck", "True")
        Response.Redirect("~/Inventory/AddCategory.aspx")
    End Sub
    Protected Sub imgAddSubCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgAddSubCategory.ServerClick
        Session("st") = "12"
        Session.Add("eComCheck", "True")
        Response.Redirect("~/Inventory/AddSubCategory.aspx")
    End Sub

    Protected Sub imgViewCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgViewCategory.ServerClick
        Session("st") = "13"
        Session.Add("eComCheck", "True")
        Response.Redirect("~/Inventory/ViewCategory.aspx?default=1")
    End Sub

    Protected Sub imgViewSubcategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgViewSubcategory.ServerClick
        Session("st") = "14"
        Session.Add("eComCheck", "True")
        Response.Redirect("~/Inventory/ViewSubCategory.aspx?default=1")
    End Sub

    Protected Sub imgAssignProducts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgAssignProducts.ServerClick
        Session("st") = "15"
        Session.Add("eComCheck", "True")
        Response.Redirect("~/Inventory/AssignPrd.aspx")
    End Sub
    Protected Sub imgview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgview.ServerClick
        Session("st") = "16"
        'Response.Redirect("~/Inventory/Product.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
        Response.Redirect(String.Format("~/Inventory/Product.aspx?PrdID={0}&Kit={1}&section=0", Request.QueryString("PrdID"), Request.QueryString("Kit")))
    End Sub

    Protected Sub imgeComMetaTag_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgeComMetaTag.ServerClick
        Session("st") = "17"
        Session.Add("eComCheck", "True")
        Response.Redirect("~/Inventory/eComMetaTags.aspx")
    End Sub

    Protected Sub ImgeComManageWebPages_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImgeComManageWebPages.ServerClick
        Session("st") = "18"
        Session.Add("eComCheck", "True")
        Response.Redirect("~/Inventory/eComManageWebPages.aspx")
    End Sub
End Class


Partial Class POS_LeftPOS
    Inherits System.Web.UI.UserControl

    Protected Sub imgCmdRefund_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdRefund.ServerClick
        Session.Remove("transactionType")
        Response.Redirect("~/POS/refund.aspx?TType=R")
    End Sub

    Protected Sub imgCmdSell_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdSell.ServerClick
        Session.Remove("transactionType")
        Response.Redirect("~/POS/POS.aspx")
    End Sub

    Protected Sub imgCmdPaused_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPaused.ServerClick
        Session.Remove("transactionType")
        Response.Redirect("~/POS/refund.aspx?TType=P")
    End Sub

    Protected Sub POS_LeftPOS_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
    End Sub

    Protected Sub imgCmdPinPadOpen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPinPadOpen.ServerClick
        Session.Remove("transactionType")
        Response.Redirect("~/POS/PinPad.aspx?PinPad=TI")
    End Sub

    Protected Sub imgCmdPinPadClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPinPadClose.ServerClick
        Session.Remove("transactionType")
        Response.Redirect("~/POS/PinPad.aspx?PinPad=TS")
    End Sub
    Protected Sub imgReceipts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgReceipts.ServerClick
        Session.Remove("transactionType")
        Response.Redirect("~/POS/Receipts.aspx")
    End Sub
    Protected Sub imgMgnReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgMgnReg.ServerClick
        Session.Remove("transactionType")
        Response.Redirect("~/POS/ManageRegister.aspx")
    End Sub
End Class




















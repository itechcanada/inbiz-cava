
Partial Class Admin_LeftPanel
    Inherits System.Web.UI.UserControl
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        subSetLeftMenu()
    End Sub

    Protected Sub lnkInternalUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkInternalUser.Click
        Response.Redirect("~/Admin/ViewInternalUser.aspx")
    End Sub
    Protected Sub lnkExternalUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExternalUser.Click
        Response.Redirect("~/Admin/ViewExternalUser.aspx")
    End Sub

    Protected Sub lnkWarehouse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkWarehouse.Click
        Response.Redirect("~/Admin/ViewWarehouse.aspx")
    End Sub

    Protected Sub lnkGlobalParameters_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGlobalParameters.Click
        Response.Redirect("~/Admin/ViewGlobalParameters.aspx")
    End Sub

    Protected Sub lnkExchangeRates_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExchangeRates.Click
        Response.Redirect("~/Admin/ViewExchangeRates.aspx")
    End Sub

    Protected Sub lnkVendor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkVendor.Click
        Response.Redirect("~/Admin/ViewVendor.aspx")
    End Sub

    Protected Sub lnkDistributor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDistributor.Click
        Response.Redirect("~/Admin/ViewDistributor.aspx")
    End Sub

    Protected Sub lnkReseller_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReseller.Click
        Response.Redirect("~/Admin/ViewReseller.aspx")
    End Sub

    'Set Left Menu
    Private Sub subSetLeftMenu()
        If Session("UserModules").ToString.Contains("VMN") = True Then
            divVMN.Visible = True
        Else
            divVMN.Visible = False
        End If
        If Session("UserModules").ToString.Contains("CRM") = True Then
            divCRM.Visible = True
        Else
            divCRM.Visible = False
        End If
        If Session("UserModules").ToString.Contains("PRO") = True Then
            divPRO.Visible = True
        Else
            divPRO.Visible = False
        End If
        If Session("UserModules").ToString.Contains("RCV") = True Then
            divRCV.Visible = True
        Else
            divRCV.Visible = False
        End If
        If Session("UserModules").ToString.Contains("SAL") = True Then
            divSAL.Visible = True
        Else
            divSAL.Visible = False
        End If
        If Session("UserModules").ToString.Contains("SHP") = True Then
            divSHP.Visible = True
        Else
            divSHP.Visible = False
        End If
        If Session("UserModules").ToString.Contains("INV") = True Then
            divINV.Visible = True
            divINVGen.Visible = True
        Else
            divINV.Visible = False
            divINVGen.Visible = False
        End If
        If Session("UserModules").ToString.Contains("ADM") = True Then
            divADM.Visible = True
            divVMN.Visible = True
            divRCV.Visible = True
            divSHP.Visible = True
            divSAL.Visible = True
            divCRM.Visible = True
            divINV.Visible = True
            divINVGen.Visible = True
        Else
            divADM.Visible = False
        End If
    End Sub

    Protected Sub lnkInventory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkInventory.Click
        Response.Redirect("~/Admin/uc.aspx")
    End Sub

    Protected Sub lnkInvoice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkInvoice.Click
        Response.Redirect("~/Admin/uc.aspx")
    End Sub

    Protected Sub lnkProcurement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkProcurement.Click
        Response.Redirect("~/Admin/uc.aspx")
    End Sub

    Protected Sub lnkReceiving_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReceiving.Click
        Response.Redirect("~/Admin/uc.aspx")
    End Sub

    Protected Sub lnkSales_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSales.Click
        Response.Redirect("~/Admin/uc.aspx")
    End Sub

    Protected Sub lnkAddresses_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddresses.Click
        Response.Redirect("~/Admin/ViewAddress.aspx")
    End Sub
End Class

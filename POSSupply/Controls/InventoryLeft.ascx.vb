Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Partial Class InventoryLeft
    Inherits System.Web.UI.UserControl
    Public objPrdDesc As New clsPrdDescriptions
    Protected Sub InventoryLeft_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Request.QueryString("PrdID") <> "" Then
            lblPrdName.Text = objPrdDesc.funProductName
            If lblPrdName.Text <> "" Then
                lblPrdName.Text = lblPrdName.Text.TrimEnd("/")
            End If
        End If
        If Session("UserModules").ToString.Contains("INROL") = True And Session("UserModules").ToString.Contains("STK") = False Then
            lblInvHeading.Text = lblInventoryReadOnly
        End If
    End Sub
    Protected Sub imgCmdPrdKit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPrdKit.ServerClick
        Response.Redirect("~/Inventory/ProductKit.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
    End Sub
    Protected Sub imgCmdAddPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdAddPrd.ServerClick
        Session.Remove("eComCheck")
        Response.Redirect("~/Inventory/Product.aspx")
    End Sub
    Protected Sub imgCmdUpdPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdUpdPrd.ServerClick
        Response.Redirect("~/Inventory/InventoryUpdate.aspx")
    End Sub
    Protected Sub imgCmdAssociteVendor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdAssociteVendor.ServerClick
        Response.Redirect("~/Inventory/AssociateVendor.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
    End Sub
    'Protected Sub imgCmdAvaWarehouse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdAvaWarehouse.Click
    '    Response.Redirect("AvailableWarehouse.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
    'End Sub
    Protected Sub imgCmdDescri_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdDescri.ServerClick
        Response.Redirect("~/Inventory/ProductDescription.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
    End Sub
    Protected Sub imgCmdPrdAss_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPrdAss.ServerClick
        Response.Redirect("~/Inventory/ProductAssociation.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
    End Sub
    Protected Sub imgCmdPrdColor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPrdColor.ServerClick
        Response.Redirect("~/Inventory/ProductColor.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
    End Sub
    Protected Sub imgCmdPrdImages_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPrdImages.ServerClick
        Response.Redirect("~/Inventory/ProductImages.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
    End Sub
    Protected Sub imgCmdPrdSale_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPrdSale.ServerClick
        Response.Redirect("~/Inventory/ProductSalePrice.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
    End Sub
    Protected Sub imgCmdViewPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdViewPrd.ServerClick
        Session.Remove("eComCheck")
        Response.Redirect("~/Inventory/ViewProduct.aspx?default=1")

    End Sub
    Protected Sub imgCmdPrdQty_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPrdQty.ServerClick
        Response.Redirect("~/Inventory/ProductQuantity.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
    End Sub
    Protected Sub imgAddCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgAddCategory.ServerClick
        Session.Add("eComCheck", "True")
        Response.Redirect("~/Inventory/AddCategory.aspx")
    End Sub
    Protected Sub imgAddSubCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgAddSubCategory.ServerClick
        Session.Add("eComCheck", "True")
        Response.Redirect("~/Inventory/AddSubCategory.aspx")
    End Sub

    Protected Sub imgViewCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgViewCategory.ServerClick
        Session.Add("eComCheck", "True")
        Response.Redirect("~/Inventory/ViewCategory.aspx?default=1")
    End Sub

    Protected Sub imgViewSubcategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgViewSubcategory.ServerClick
        Session.Add("eComCheck", "True")
        Response.Redirect("~/Inventory/ViewSubCategory.aspx?default=1")
    End Sub

    Protected Sub imgAssignProducts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgAssignProducts.ServerClick
        Session.Add("eComCheck", "True")
        Response.Redirect("~/Inventory/AssignPrd.aspx")
    End Sub
    Protected Sub imgview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgview.ServerClick
        Response.Redirect("~/Inventory/Product.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
    End Sub

    Protected Sub imgeComMetaTag_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgeComMetaTag.ServerClick
        Session.Add("eComCheck", "True")
        Response.Redirect("~/Inventory/eComMetaTags.aspx")
    End Sub

    Protected Sub ImgeComManageWebPages_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImgeComManageWebPages.ServerClick
        Session.Add("eComCheck", "True")
        Response.Redirect("~/Inventory/eComManageWebPages.aspx")
    End Sub
End Class

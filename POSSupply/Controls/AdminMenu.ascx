<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AdminMenu.ascx.vb" Inherits="Controls_AdminMenu" %>

<div class="menu_seperator">
    <div class="menu_seperator_container">
        <div class="menu_bar">
            <ul>
                <li><a href="#"><%= Resources.Resource.UserAdministration %></a>
                    <ul>
                        <li>
                            <a id="cmdInternalUser" runat="server" CausesValidation="false" href="#" ><%=Resources.Resource.btnimgInternalUser%></a>
                            <a id="cmdAddInternalUser" runat="server" CausesValidation="false" href="#" ><%=Resources.Resource.btnimgAddInternalUser%></a>
                        </li>
                    </ul>
                </li>
                                
            </ul>
        </div>
    </div>
</div>

Imports Resources.Resource

Partial Class LeftReport
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
    End Sub

    Protected Sub cmdSalesByRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSalesByRegister.ServerClick
        Response.Redirect("~/Report/Default.aspx?XMLFile=SalesByRegister.xml")
    End Sub

    Protected Sub cmdTaxCollectedByRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaxCollectedByRegister.ServerClick
        Response.Redirect("~/Report/Default.aspx?XMLFile=TaxCollectedByRegister.xml")
    End Sub

    Protected Sub cmdSalesByProduct_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSalesByProduct.ServerClick
        Response.Redirect("~/Report/Default.aspx?XMLFile=SalesByProduct.xml")
    End Sub
End Class

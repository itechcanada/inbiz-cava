<%@ Control Language="VB" AutoEventWireup="false" CodeFile="HReport.ascx.vb" Inherits="Controls_HReport" %>

<div id="tabsH" style="display:none;">
    <ul id="ulMainMenu" runat="server">
       <%-- <li>
            <asp:Label ID="lblPO" runat="server" CssClass="heading" Text="<%$ Resources:Resource, lblReports %>"></asp:Label>
        </li>--%>
        <li><a id="cmdSalesByRegister" runat="server" causesvalidation="false" href="#"><span
            style="min-width: 120px; text-align: center;">
            <%=Resources.Resource.cmdSalesByRegister%>
        </span></a></li>
        <li><a id="cmdTaxCollectedByRegister" runat="server" causesvalidation="false" href="#">
            <span style="min-width: 120px; text-align: center;">
                <%=Resources.Resource.cmdTaxCollectedByRegister%>
            </span></a></li>
        <li><a id="cmdSalesByProduct" runat="server" causesvalidation="false" href="#"><span
            style="min-width: 120px; text-align: center;">
            <%=Resources.Resource.cmdCssSalesByProduct%>
        </span></a></li>
        <li><a id="cmdWasteReport" runat="server" causesvalidation="false" href="#"><span
            style="min-width: 120px; text-align: center;">
            <%=Resources.Resource.cmdWasteReport%>
        </span></a></li>
        <li><a id="cmdOpenOrder" runat="server" causesvalidation="false" href="#"><span
            style="min-width: 120px; text-align: center;">
            <%= Resources.Resource.cmdOpenOrderFulfillment%>
        </span></a></li>
        <li><a id="cmdStorageInventory" runat="server" causesvalidation="false" href="#"><span
            style="min-width: 120px; text-align: center;">
            <%= Resources.Resource.cmdInventoryByProduct%> 
        </span></a></li>
        <li><a id="cmdVendorPoHistory" runat="server" causesvalidation="false" href="#"><span
            style="min-width: 120px; text-align: center;">
            <%= Resources.Resource.cmdVendorPOHistory%>
        </span></a></li>
    </ul>
</div>


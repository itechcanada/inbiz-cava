Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.IO
Imports System.Web.UI.HtmlControls
Imports iTECH.InbizERP.BusinessLogic

Partial Class Controls_Menu_New
    Inherits System.Web.UI.UserControl

    Dim objCom As New clsCommon

    Private Const UL_START As String = "<ul>"
    Private Const UL_END As String = "</ul>"
    Private Const LI_START As String = "<li>"
    Private Const LI_END As String = "</li>"
    Private Const CURRENT_LINK_FORMAT As String = "<a class=""{0}"" href=""{1}"">{2}</a>"
    Private Const LI_LINK_FORMAT As String = "<li class=""{0}""><a href=""{1}"">{2}</a></li>"
    Private Const LI_LINK_FORMAT_TARGET As String = "<li class=""{0}""><a href=""{1}"" target=""{2}"">{3}</a></li>"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not String.IsNullOrEmpty(Request.QueryString("t")) Then
                Session("t") = Request.QueryString("t")
            End If

            AddUserAdmnistration(ulMainMenu)
            'AddReservation(ulMainMenu)
            AddProcurement(ulMainMenu)
            If Session("UserModules").ToString.Contains("CRM") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
                AddCRM(ulMainMenu)
            End If

            If Session("UserModules").ToString.Contains("MSG") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
                'AddCRMessages(ulMainMenu)
            End If

            If Session("UserModules").ToString.Contains("STK") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
                AddInventory(ulMainMenu)
            End If

            AddSales(ulMainMenu)
            'AddTasks(ulMainMenu)
            AddInvoice(ulMainMenu)

            If Session("UserModules").ToString.Contains("POS") = True Or Session("UserModules").ToString.Contains("PSMGR") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
                AddPOS(ulMainMenu)
            End If

            If Session("UserModules").ToString.Contains("eCOM") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
                AddEcommerce(ulMainMenu)
            End If

            If Session("UserModules").ToString.Contains("RPT") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
                AddReports(ulMainMenu)
            End If

            If Request.QueryString("rs") = "1" Then
                objCom.funRemoveSession()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub AddUserAdmnistration(ByVal parentControl As Control)
        Dim sb As New StringBuilder()

        'First Need to validate user before adding following subrutine
        'Main Item Container Start Here
        sb.Append(LI_START)

        'Add Main Item
        If Session("t") = "0" Then
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "current", "#", Resources.Resource.UserAdministration))
        Else
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.UserAdministration))
        End If


        'Add SubItem Container
        sb.Append(UL_START)

        'Add Sub Items
        Dim visibleItemCount As Integer = 0
        If Session("UserModules").ToString.Contains("ADM") = True Then
            visibleItemCount = visibleItemCount + 1
            sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.btnimgInternalUser))
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewInternalUser.aspx?Default=1&t=0"), Resources.Resource.lblManageUser))
            'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditUser.aspx?t=0"), Resources.Resource.btnimgAddInternalUser))
        End If

        If Session("UserModules").ToString.Contains("ENT") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
            visibleItemCount = visibleItemCount + 1
            sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.EnterpriseParameters))
            If ConfigurationManager.AppSettings("MultipleCompanies").ToLower = "true" Then
                sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditCompany.aspx?t=0"), Resources.Resource.btnimgAddCompany))
            End If
            If ConfigurationManager.AppSettings("MultipleCompanies").ToLower = "true" Then
                sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewGlobalParameters.aspx?Default=1&t=0"), Resources.Resource.btnimgGlobalParameters))
            Else
                Dim objCompany As New clsCompanyInfo
                Dim strCompanyID As String = ""
                strCompanyID = objCompany.funGetSingleCompID
                sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditCompany.aspx?CompanyID=" & strCompanyID & "&t=0"), Resources.Resource.btnimgGlobalParameters))
            End If

            'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddBaseCurrency.aspx?t=0"), Resources.Resource.btnAddBaseCurrency))
            'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditCurrency.aspx?t=0"), Resources.Resource.btnimgAddCurrency))
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewExchangeRates.aspx"), Resources.Resource.btnimgExchangeRates))
            'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Translator/Default.aspx"), Resources.Resource.btnLBLTXT))
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Taxes/ViewTaxGroup.aspx"), Resources.Resource.btnGroupTaxes))
            'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Taxes/AddEditTaxGroup.aspx?t=0"), Resources.Resource.btnAddTaxGroup))
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Taxes/ViewTaxes.aspx"), Resources.Resource.btnTaxes))
            'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Taxes/AddEditTaxes.aspx"), Resources.Resource.btnAddTaxes))
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewProcessGroup.aspx"), Resources.Resource.btnimgProcessGroup))
            'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditProcess.aspx?t=0"), Resources.Resource.btnimgAddProcess))
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewWarehouse.aspx"), Resources.Resource.btnimgWarehouse))
            'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditWarehouse.aspx?t=0"), Resources.Resource.btnimgAddWarehouse))
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewRegister.aspx"), Resources.Resource.lblPOS))
            'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditRegister.aspx?t=0"), Resources.Resource.lblAddPOS))
            If Session("UserModules").ToString.Contains("IMPEXP") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
                sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/ImportExport/Default.aspx?Type=C&t=0"), Resources.Resource.btnImpCustomer))
                sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/ImportExport/Default.aspx?Type=P&t=0"), Resources.Resource.btnImpProd))
            End If
        End If

        'To do here to add more sub items
        

        'End SubItemContainer
        sb.Append(UL_END)

        'End Main Item Container
        sb.Append(LI_END)

        Dim ltText As New Literal
        ltText.Text = sb.ToString()
        If (visibleItemCount > 0) Then
            parentControl.Controls.Add(ltText)
        End If
    End Sub

    Private Sub AddReservation(ByVal parentControl As Control)
        Dim sb As New StringBuilder()
        'First Need to validate user before adding following subrutine
        'Main Item Container Start Here
        sb.Append(LI_START)
        'Add Main Item
        sb.Append(String.Format(CURRENT_LINK_FORMAT, "current", Page.ResolveUrl("~/Reservation/Default.aspx"), "Reservation"))

        'End Main Item Container
        sb.Append(LI_END)

        Dim ltText As New Literal
        ltText.Text = sb.ToString()
        parentControl.Controls.Add(ltText)
    End Sub

    Private Sub AddCRMessages(ByVal parentControl As Control)
        Dim sb As New StringBuilder()

        'First Need to validate user before adding following subrutine
        'Main Item Container Start Here
        sb.Append(LI_START)

        'Add Main Item
        If Session("t") = "1" Then
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "current", "#", Resources.Resource.lblCustomerMessages))
        Else
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.lblCustomerMessages))
        End If

        'Add SubItem Container
        sb.Append(UL_START)

        'Add Sub Items
        sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblCustomerMessages))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Partner/ViewAlert.aspx?Default=1&t=1"), Resources.Resource.btnViewAlerts))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/Home.aspx?t=1"), Resources.Resource.lblMessageCalendar))


        If ConfigurationManager.AppSettings("BtnEmailVisible").ToLower = "yes" Then
            sb.Append(String.Format(LI_LINK_FORMAT_TARGET, "", "http://callmgmt.itechcanada.com/", "_blank", Resources.Resource.btnMsgEmail))
        End If

        If Session("UserModules").ToString.Contains("FAX") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/FaxInbox/Default.aspx?Default=1&t=1"), Resources.Resource.btnFaxInbox))
        End If

        'To do here to add more sub items

        'End SubItemContainer
        sb.Append(UL_END)

        'End Main Item Container
        sb.Append(LI_END)

        Dim ltText As New Literal
        ltText.Text = sb.ToString()
        parentControl.Controls.Add(ltText)
    End Sub

    Private Sub AddProcurement(ByVal parentControl As Control)
        Dim sb As New StringBuilder()

        'First Need to validate user before adding following subrutine
        'Main Item Container Start Here
        sb.Append(LI_START)

        'Add Main Item
        If Session("t") = "2" Then
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "current", "#", Resources.Resource.lblProcurement))
        Else
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.lblProcurement))
        End If

        'Add SubItem Container
        sb.Append(UL_START)

        'Add Sub Items
        Dim visibleItemCount As Integer = 0
        If Session("UserModules").ToString.Contains("VMN") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
            sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblVendorManagement))
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewVendor.aspx?Default=1&t=2"), Resources.Resource.btnimgVendor))
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditVendor.aspx?t=2"), Resources.Resource.btnimgAddVendor))
            visibleItemCount = visibleItemCount + 1
        End If

        If Session("UserModules").ToString.Contains("PRO") = True Or Session("UserModules").ToString.Contains("POA") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
            sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblProcurement))
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Procurement/ViewPurchaseOrder.aspx?Default=1"), Resources.Resource.btnimgProcurement))
            visibleItemCount = visibleItemCount + 1
        End If

        If Session("UserModules").ToString.Contains("RCV") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
            sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblReceiving))
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Receiving/Default.aspx?t=2"), Resources.Resource.btnimgReceiving))
            visibleItemCount = visibleItemCount + 1
        End If

        'To do here to add more sub items

        'End SubItemContainer
        sb.Append(UL_END)

        'End Main Item Container
        sb.Append(LI_END)

        Dim ltText As New Literal
        ltText.Text = sb.ToString()
        If (visibleItemCount > 0) Then
            parentControl.Controls.Add(ltText)
        End If
    End Sub

    Private Sub AddCRM(ByVal parentControl As Control)
        Dim sb As New StringBuilder()

        'First Need to validate user before adding following subrutine
        'Main Item Container Start Here
        sb.Append(LI_START)

        'Add Main Item
        If Session("t") = "3" Then
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "current", "#", Resources.Resource.lblCustomerRelationship))
        Else
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.lblCustomerRelationship))
        End If

        'Add SubItem Container
        sb.Append(UL_START)

        'Add Sub Items
        sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblCustomerRelationship))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/Partner/ViewCustomer.aspx?pType={0}&gType={1}", StatusCustomerTypes.DISTRIBUTER, CType(StatusGuestType.None, Integer))), "View Customers"))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/Partner/CustomerEdit.aspx?pType={0}&gType={1}", StatusCustomerTypes.DISTRIBUTER, CType(StatusGuestType.None, Integer))), "Add Customers"))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/Partner/ViewCustomer.aspx?pType={0}&gType={1}", StatusCustomerTypes.END_CLINET, CType(StatusGuestType.Guest, Integer))), "View Guest"))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/Partner/CustomerEdit.aspx?pType={0}&gType={1}", StatusCustomerTypes.END_CLINET, CType(StatusGuestType.Guest, Integer))), "Add Guest"))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/Partner/ViewCustomer.aspx?pType={0}&gType={1}", StatusCustomerTypes.END_CLINET, CType(StatusGuestType.Staff, Integer))), "View Staff"))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/Partner/CustomerEdit.aspx?pType={0}&gType={1}", StatusCustomerTypes.END_CLINET, CType(StatusGuestType.Staff, Integer))), "Add Staff Member"))
        'If Not Session("CurrentUserType") = "C" Then
        '    sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Partner/AddEditCustomer.aspx?rs=1&t=3"), Resources.Resource.btnAddCustomer))
        'End If

        'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Partner/ViewContact.aspx?Default=1&rs=1&t=3"), Resources.Resource.btnViewContacts))
        'If Not Session("CurrentUserType") = "C" Then
        '    sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Partner/SelectPartner.aspx?Default=1&rs=1&t=3"), Resources.Resource.btnAddContact))
        '    'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/MassEmail/MassEmail.aspx?t=3"), Resources.Resource.btnMassEmail))
        'End If

        'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Partner/ActivityCustomers.aspx?Default=1&rs=1&t=3"), Resources.Resource.btnActivities))
        'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewCategories.aspx?t=3"), Resources.Resource.lblCustomerCategories))
        'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditCategories.aspx?t=3"), Resources.Resource.lblAddCustomerCategory))
        'To do here to add more sub items

        'End SubItemContainer
        sb.Append(UL_END)

        'End Main Item Container
        sb.Append(LI_END)

        Dim ltText As New Literal
        ltText.Text = sb.ToString()
        parentControl.Controls.Add(ltText)
    End Sub

    Private Sub AddInventory(ByVal parentControl As Control)
        Dim sb As New StringBuilder()

        'First Need to validate user before adding following subrutine
        'Main Item Container Start Here
        sb.Append(LI_START)

        'Add Main Item
        If Session("t") = "4" Then
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "current", Page.ResolveUrl("~/Inventory/ViewProduct.aspx?Default=1&eComCheck=0&t=4"), Resources.Resource.lblInventoryManagement))
        Else
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "", Page.ResolveUrl("~/Inventory/ViewProduct.aspx?Default=1&eComCheck=0&t=4"), Resources.Resource.lblInventoryManagement))
        End If

        'Add SubItem Container
        sb.Append(UL_START)

        'Add Sub Items
        sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblInventoryManagement))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/Inventory/ViewProduct.aspx?ptype={0}", CType(StatusProductType.Product, Integer))), Resources.Resource.btnviewprod))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/Inventory/Product.aspx?ptype={0}", CType(StatusProductType.Product, Integer))), Resources.Resource.btnaddprod))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Inventory/InventoryUpdate.aspx?t=4&st=2&eComCheck=0"), Resources.Resource.btnupdateprod))
        'To do here to add more sub items

        sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "javascript:void(0);", "Accommodation"))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/Inventory/ViewProduct.aspx?ptype={0}", CType(StatusProductType.Accommodation, Integer))), "View Accommodation"))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/Inventory/Product.aspx?ptype={0}", CType(StatusProductType.Accommodation, Integer))), "Add Accommodation"))

        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Inventory/BuildingView.aspx"), "Manage Buildings"))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Inventory/RoomView.aspx"), "Manage Rooms"))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Inventory/AmenityView.aspx"), "Manage Amenities"))

        'End SubItemContainer
        sb.Append(UL_END)

        'End Main Item Container
        sb.Append(LI_END)

        Dim ltText As New Literal
        ltText.Text = sb.ToString()
        parentControl.Controls.Add(ltText)
    End Sub

    Private Sub AddSales(ByVal parentControl As Control)
        Dim sb As New StringBuilder()

        'First Need to validate user before adding following subrutine
        'Main Item Container Start Here
        sb.Append(LI_START)

        'Add Main Item        
        If Session("t") = "5" Then
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "current", "#", Resources.Resource.lblSales + "/Reservation"))
        Else
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.lblSales + "/Reservation"))
        End If

        'Add SubItem Container
        sb.Append(UL_START)

        'Add Sub Items
        sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblSales + "/Reservation"))
        Dim visibleItemCount As Integer = 0
        sb.Append(String.Format(LI_LINK_FORMAT, "current", Page.ResolveUrl("~/Reservation/Default.aspx"), "Reservation"))
        If Session("UserModules").ToString.Contains("SAL") = True Or Session("UserModules").ToString.Contains("QOA") = True Or (Session("UserModules").ToString.Contains("ADM") = True) Then
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Sales/ViewSalesOrder.aspx?Default=1&t=5"), "Sales/Reservation Orders")) '+ Resources.Resource.btnimgSales))
            visibleItemCount = visibleItemCount + 1
        End If

        If (Session("UserModules").ToString.Contains("ADM") = True Or Session("UserModules").ToString.Contains("LMA") = True) Then
            'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/TeleMarketing/Assignleads.aspx?Default=1&t=5"), Resources.Resource.cmdAssignleads))
            'visibleItemCount = visibleItemCount + 1
        End If

        If (Session("UserModules").ToString.Contains("ADM") = True Or Session("UserModules").ToString.Contains("LMA") = True Or Session("UserModules").ToString.Contains("SAL") = True) Then
            'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/TeleMarketing/LeadsActivities.aspx?Default=1&t=5"), Resources.Resource.btnActivities))
            'visibleItemCount = visibleItemCount + 1
        End If

        'Some items from task module moved here below for now
        sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblJobs))
        If Session("UserModules").ToString.Contains("SHP") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Shipping/Default.aspx?t=6"), Resources.Resource.btnimgShipping))
            visibleItemCount = visibleItemCount + 1
        End If


        'To do here to add more sub items

        'End SubItemContainer
        sb.Append(UL_END)

        'End Main Item Container
        sb.Append(LI_END)

        Dim ltText As New Literal
        ltText.Text = sb.ToString()
        If (visibleItemCount > 0) Then
            parentControl.Controls.Add(ltText)
        End If

    End Sub

    Private Sub AddTasks(ByVal parentControl As Control)
        Dim sb As New StringBuilder()

        'First Need to validate user before adding following subrutine
        'Main Item Container Start Here
        sb.Append(LI_START)

        'Add Main Item        
        If Session("t") = "6" Then
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "current", "#", Resources.Resource.lblJobs))
        Else
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.lblJobs))
        End If

        'Add SubItem Container
        sb.Append(UL_START)

        'Add Sub Items

        sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblJobs))
        Dim visibleItemCount As Integer = 0
        If Session("UserModules").ToString.Contains("JOBP") = True Or Session("UserModules").ToString.Contains("ADM") = True Or Session("UserModules").ToString.Contains("JBPM") = True Then
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Shipping/Jobs.aspx?Default=1&t=6"), Resources.Resource.cmdCssJobs))
            visibleItemCount = visibleItemCount + 1
        End If

        If Session("UserModules").ToString.Contains("JBPM") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Shipping/JobsAssign.aspx?Default=1&t=6"), Resources.Resource.cmdCssAssignJobs))
            visibleItemCount = visibleItemCount + 1
        End If

        If Session("UserModules").ToString.Contains("SHP") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Shipping/Default.aspx?t=6"), Resources.Resource.btnimgShipping))
            visibleItemCount = visibleItemCount + 1
        End If

        'To do here to add more sub items

        'End SubItemContainer
        sb.Append(UL_END)

        'End Main Item Container
        sb.Append(LI_END)

        Dim ltText As New Literal
        ltText.Text = sb.ToString()
        If (visibleItemCount > 0) Then
            parentControl.Controls.Add(ltText)
        End If

    End Sub

    Private Sub AddInvoice(ByVal parentControl As Control)
        Dim sb As New StringBuilder()

        'First Need to validate user before adding following subrutine
        'Main Item Container Start Here
        sb.Append(LI_START)

        'Add Main Item

        If Session("t") = "8" Then
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "current", "#", Resources.Resource.lblInvoiceGeneration))
        Else
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.lblInvoiceGeneration))
        End If

        'Add SubItem Container
        sb.Append(UL_START)

        'Add Sub Items
        Dim visibleItemCount As Integer = 0
        If Session("UserModules").ToString.Contains("INV") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
            sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblInvoiceGeneration))
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Invoice/ViewInvoice.aspx?Default=1&dflt=Y&t=8"), Resources.Resource.btnimgInvoice))
            'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Invoice/GenerateInvoice.aspx?t=8"), Resources.Resource.btnGenerateInvoice))
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Invoice/BatchPrint.aspx?t=8"), Resources.Resource.btnbatchPrint))
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Invoice/BatchStatement.aspx?t=8"), Resources.Resource.cmdCssBatchstatement))
            visibleItemCount = visibleItemCount + 1
        End If

        If Session("UserModules").ToString.Contains("ARC") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
            sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblAccountsReceivable))
            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/AccountsReceivable/ViewAccountsReceivable.aspx?Default=1&t=8"), Resources.Resource.btnAccountDetails))
            visibleItemCount = visibleItemCount + 1
        End If

        If Session("UserModules").ToString.Contains("COL") = True Or Session("UserModules").ToString.Contains("ADM") = True Then
            sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblCollection))
            If Session("UserModules").ToString.Contains("CUA") = False Then
                sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Collection/ViewCollection.aspx?Default=1&t=8"), Resources.Resource.btnAssignAgents))
            End If

            sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/CollectionAgent/ViewFollowUp.aspx?Default=1&t=8"), Resources.Resource.btnActivities))
            visibleItemCount = visibleItemCount + 1
        End If

        'To do here to add more sub items

        'End SubItemContainer
        sb.Append(UL_END)

        'End Main Item Container
        sb.Append(LI_END)

        Dim ltText As New Literal
        ltText.Text = sb.ToString()
        If (visibleItemCount > 0) Then
            parentControl.Controls.Add(ltText)
        End If

    End Sub

    Private Sub AddPOS(ByVal parentControl As Control)
        Dim sb As New StringBuilder()

        'First Need to validate user before adding following subrutine
        'Main Item Container Start Here
        sb.Append(LI_START)

        'Add Main Item
        If Session("RegCode") <> "" Then
            If Session("t") = "9" Then
                sb.Append(String.Format(CURRENT_LINK_FORMAT, "current", Page.ResolveUrl("~/POS/POS.aspx?t=9"), Resources.Resource.btnPointOfSales))
            Else
                sb.Append(String.Format(CURRENT_LINK_FORMAT, "", Page.ResolveUrl("~/POS/POS.aspx?t=9"), Resources.Resource.btnPointOfSales))
            End If
        Else
            If Session("t") = "9" Then
                sb.Append(String.Format(CURRENT_LINK_FORMAT, "current", Page.ResolveUrl("~/Admin/SelectRegister.aspx?t=9"), Resources.Resource.btnPointOfSales))
            Else
                sb.Append(String.Format(CURRENT_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/SelectRegister.aspx?t=9"), Resources.Resource.btnPointOfSales))
            End If
        End If


        'End Main Item Container
        sb.Append(LI_END)

        Dim ltText As New Literal
        ltText.Text = sb.ToString()
        parentControl.Controls.Add(ltText)
    End Sub

    Private Sub AddEcommerce(ByVal parentControl As Control)
        Dim sb As New StringBuilder()

        'First Need to validate user before adding following subrutine
        'Main Item Container Start Here
        sb.Append(LI_START)

        'Add Main Item

        If Session("t") = "10" Then
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "current", "#", Resources.Resource.lblECommerce))
        Else
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.lblECommerce))
        End If

        'Add SubItem Container
        sb.Append(UL_START)

        'Add Sub Items
        sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblECommerce))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Inventory/viewCategory.aspx?Default=1&eComCheck=1&t=10"), Resources.Resource.btnecom))
        'sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/eBay/ProductExport.aspx?t=10"), Resources.Resource.lbleBay))

        'New CMS Implementation
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/ECommerce/cmspageedit.aspx"), "CMS"))

        'To do here to add more sub items

        'End SubItemContainer
        sb.Append(UL_END)

        'End Main Item Container
        sb.Append(LI_END)

        Dim ltText As New Literal
        ltText.Text = sb.ToString()
        parentControl.Controls.Add(ltText)
    End Sub

    Private Sub AddReports(ByVal parentControl As Control)
        Dim sb As New StringBuilder()

        'First Need to validate user before adding following subrutine
        'Main Item Container Start Here
        sb.Append(LI_START)

        'Add Main Item        
        If Session("t") = "11" Then
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "current", Page.ResolveUrl("~/Report/Home.aspx?t=11"), Resources.Resource.btnReports))
        Else
            sb.Append(String.Format(CURRENT_LINK_FORMAT, "", Page.ResolveUrl("~/Report/Home.aspx?t=11"), Resources.Resource.btnReports))
        End If

        'Add SubItem Container
        sb.Append(UL_START)

        'Add Sub Items        
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Report/Default.aspx?XMLFile=SalesByRegister.xml"), Resources.Resource.cmdSalesByRegister))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Report/Default.aspx?XMLFile=TaxCollectedByRegister.xml"), Resources.Resource.cmdTaxCollectedByRegister))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Report/Default.aspx?XMLFile=SalesByProduct.xml"), Resources.Resource.cmdCssSalesByProduct))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Report/Default.aspx?XMLFile=WasteTransactionByRegister.xml"), Resources.Resource.cmdWasteReport))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/NewReport/report.aspx?rpt={0}", CType(ReportKey.OpenOrderFullfillment, Integer))), Resources.Resource.cmdOpenOrderFulfillment))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/NewReport/report.aspx?rpt={0}", CType(ReportKey.InventoryByProduct, Integer))), Resources.Resource.cmdInventoryByProduct))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/NewReport/report.aspx?rpt={0}", CType(ReportKey.VendorPOHistory, Integer))), Resources.Resource.cmdVendorPOHistory))

        sb.Append(String.Format(LI_LINK_FORMAT, "menutitle", "javascript:void(0)", "Reservation"))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/NewReport/report.aspx?rpt={0}", CType(ReportKey.PreviousWeekCount, Integer))), "Previous Week Count"))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/NewReport/report.aspx?rpt={0}", CType(ReportKey.MealCountReport, Integer))), "Meal Count Report"))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/NewReport/report.aspx?rpt={0}", CType(ReportKey.SpecialGuestList, Integer))), "Special Guest List"))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/NewReport/report.aspx?rpt={0}", CType(ReportKey.Housekeeping, Integer))), "Housekeeping"))
        sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(String.Format("~/NewReport/report.aspx?rpt={0}", CType(ReportKey.GuestList, Integer))), "GuestList"))

        'To do here to add more sub items

        'End SubItemContainer
        sb.Append(UL_END)


        'End Main Item Container
        sb.Append(LI_END)

        Dim ltText As New Literal
        ltText.Text = sb.ToString()
        parentControl.Controls.Add(ltText)
    End Sub

End Class


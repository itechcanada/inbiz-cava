﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MainSearch.ascx.vb" Inherits="Controls_new_MainSearch" %>
<div class="searchBar">
            <div class="header">
                <div class="title">
                    Search form</div>
                <div class="icon">
                    <img src="lib/image/iconSearch.png" style="width: 17px" /></div>
            </div>
            <h4>Vendor</h4>
            <div class="inner">
                <asp:TextBox ID="txt_vendor" runat="server" Width="150px"></asp:TextBox>
            </div>
            <h4>Status</h4>
            <div class="inner">
                <asp:TextBox ID="txt_status" runat="server" Width="150px"></asp:TextBox>
            </div>
            <h4>ID</h4>
            <div class="inner">
                <asp:TextBox ID="txt_id" runat="server" Width="150px"></asp:TextBox>
            </div>
            <h4>Date</h4>
            <div class="inner">
                <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="text">
                            Between
                        </td>
                        <td class="input">
                            <asp:TextBox ID="datepicker01" CssClass="datepicker" runat="server" Width="80px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="text">
                            and
                        </td>
                        <td class="input">
                            <asp:TextBox ID="datepicker02" CssClass="datepicker" runat="server" Width="80px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="hrLine">
                                <span class="hrColor">or</span></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text">
                            Month
                        </td>
                        <td class="input">
                            <asp:TextBox ID="datepicker03" CssClass="datepicker" runat="server" Width="80px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="footer">
                <div class="submit">
                    <asp:Button runat="server" Text="Submit" />
                </div>
            </div>
        </div>
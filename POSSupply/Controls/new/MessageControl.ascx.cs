﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.ComponentModel;
using System.Drawing.Design;
using System.Net;
using System.IO;

public partial class Controls_new_MessageControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        pnlMessage.InnerHtml = string.Empty;
        if (!string.IsNullOrEmpty(this.MessageUrl))
        {
            try
            {
                //string url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, this.ResolveUrl(this.MessageUrl)); //Request.ApplicationPath + this.ResolveUrl(this.MessageUrl);    
                //HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                //httpWebRequest.Method = "POST";
                //httpWebRequest.ContentType = "application/x-www-form-urlencoded";

                //HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                //Stream responseStream = httpWebResponse.GetResponseStream();
                //StreamReader streamReader = new StreamReader(responseStream);
                //pnlMessage.InnerHtml = streamReader.ReadToEnd();
            }
            catch 
            {
                
            }            
        }
    }

    [Bindable(true)]    
    [DefaultValue("")]
    [Editor("System.Web.UI.Design.UrlEditor, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
    [UrlProperty]
    public string MessageUrl {
        get {
            return !string.IsNullOrEmpty(hlMessage.NavigateUrl) ? this.ResolveUrl(hlMessage.NavigateUrl) : string.Empty;
        }
        set {
            hlMessage.NavigateUrl = value;
        }
    }

    //public bool AllowAsyncCall {
    //    get {
    //        return pnlAsyncCallPanel.Visible;
    //    }
    //    set {
    //        pnlAsyncCallPanel.Visible = value;
    //    }
    //}
}
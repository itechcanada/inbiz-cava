﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Controls_new_Menu : System.Web.UI.UserControl
{
    #region Members
    private const string UL_START = "<ul>";
    private const string UL_END = "</ul>";
    private const string LI_START = "<li>";
    private const string LI_END = "</li>";
    private const string CURRENT_LINK_FORMAT = "<a class=\"{0}\" href=\"{1}\">{2}</a>";
    private const string LI_LINK_FORMAT = "<li class=\"{0}\"><a href=\"{1}\">{2}</a></li>";

    private const string LI_LINK_FORMAT_TARGET = "<li class=\"{0}\"><a href=\"{1}\" target=\"{2}\">{3}</a></li>";

    protected void Page_Load(object sender, System.EventArgs e)
    {
        //If user is not authenticated then stop building menu
        if (!CurrentUser.IsAutheticated)
        {
            return;
        }

        //try
        //{
        // SaveMenuContentInSessionForBothLangToAvoidDataBaseHitOnEachPageRequest
        if (BusinessUtility.GetString(Session["MenuHtmlEn"]) == "")
        {
            SuckerFishMenuHelper oHelper = new SuckerFishMenuHelper(this.Page, AppLanguageCode.EN);
            litMenu.Text = oHelper.GetHtml();
            Session["MenuHtmlEn"] = litMenu.Text;
            oHelper = new SuckerFishMenuHelper(this.Page, AppLanguageCode.FR);
            Session["MenuHtmlFr"] = oHelper.GetHtml(); 
        }
        else
        {
            if (Globals.CurrentAppLanguageCode == AppLanguageCode.FR)
            {
                litMenu.Text = BusinessUtility.GetString(Session["MenuHtmlFr"]);
            }
            else
            {
                litMenu.Text = BusinessUtility.GetString(Session["MenuHtmlEn"]);
            }
        }

        /*if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            AddUserAdmnistration(ulMainMenu);
        }

        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR) || CurrentUser.IsInRole(RoleID.PROCURMENT_MANAGER))
        {
            AddProcurement(ulMainMenu);
        }

        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR) || CurrentUser.IsInRole(RoleID.CRM_MANAGER))
        {
            AddCRM(ulMainMenu);
        }

        if (CurrentUser.IsInRole(RoleID.MESSAGE_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            //AddCRMessages(ulMainMenu)
        }

        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR) || CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER))
        {
            AddInventory(ulMainMenu);
        }

        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR) || CurrentUser.IsInRole(RoleID.SALES_MANAGER) || CurrentUser.IsInRole(RoleID.SALES_REPRESENTATIVE))
        {
            AddSales(ulMainMenu);
        }

        //AddTasks(ulMainMenu);

        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR) || CurrentUser.IsInRole(RoleID.INVOICE_GENERATION) || CurrentUser.IsInRole(RoleID.ACCOUNT_RECEIVABLE) || CurrentUser.IsInRole(RoleID.COLLECTION_AGENT) || CurrentUser.IsInRole(RoleID.COLLECTION_MANAGER))
        {
            AddInvoice(ulMainMenu);
        }

        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR) || CurrentUser.IsInRole(RoleID.POS_USER) || CurrentUser.IsInRole(RoleID.POS_MANAGER))
        {
            AddPOS(ulMainMenu);
        }

        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR) || CurrentUser.IsInRole(RoleID.ECOMMERCE_MANAGER))
        {
            AddEcommerce(ulMainMenu);
        }

        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR) || CurrentUser.IsInRole(RoleID.REPORTER))
        {
            AddReports(ulMainMenu);
        }*/
        //}
        //catch (Exception ex)
        //{
        //}
    }

    /*
    private void AddUserAdmnistration(Control parentControl)
    {
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            StringBuilder sb = new StringBuilder();

            //First Need to validate user before adding following subrutine
            //Main Item Container Start Here
            sb.Append(LI_START);

            //Add Main Item
            sb.Append(string.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.UserAdministration));


            //Add SubItem Container
            sb.Append(UL_START);

            //Add Sub Items                            
            sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.btnimgInternalUser));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewInternalUser.aspx?Default=1&t=0"), Resources.Resource.lblManageUser));
            //sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditUser.aspx?t=0"), Resources.Resource.btnimgAddInternalUser))

            sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.EnterpriseParameters));
            if (ConfigurationManager.AppSettings["MultipleCompanies"].ToLower() == "true")
            {
                sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditCompany.aspx?t=0"), Resources.Resource.btnimgAddCompany));
            }
            if (ConfigurationManager.AppSettings["MultipleCompanies"].ToLower() == "true")
            {
                sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewGlobalParameters.aspx?Default=1&t=0"), Resources.Resource.btnimgGlobalParameters));
            }
            else
            {
                SysCompanyInfo objCompany = new SysCompanyInfo();
                int comID = objCompany.GetCompanyID();
                sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditCompany.aspx?CompanyID=" + comID + "&t=0"), Resources.Resource.btnimgGlobalParameters));
            }

            //sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddBaseCurrency.aspx?t=0"), Resources.Resource.btnAddBaseCurrency))
            //sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditCurrency.aspx?t=0"), Resources.Resource.btnimgAddCurrency))
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewExchangeRates.aspx"), Resources.Resource.btnimgExchangeRates));
            //sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Translator/Default.aspx"), Resources.Resource.btnLBLTXT))
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Taxes/ViewTaxGroup.aspx"), Resources.Resource.btnGroupTaxes));
            //sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Taxes/AddEditTaxGroup.aspx?t=0"), Resources.Resource.btnAddTaxGroup))
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Taxes/ViewTaxes.aspx"), Resources.Resource.btnTaxes));
            //sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Taxes/AddEditTaxes.aspx"), Resources.Resource.btnAddTaxes))
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewProcessGroup.aspx"), Resources.Resource.btnimgProcessGroup));
            //sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditProcess.aspx?t=0"), Resources.Resource.btnimgAddProcess))
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewWarehouse.aspx"), Resources.Resource.btnimgWarehouse));
            //sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditWarehouse.aspx?t=0"), Resources.Resource.btnimgAddWarehouse))
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewRegister.aspx"), Resources.Resource.lblPOS));
            //sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditRegister.aspx?t=0"), Resources.Resource.lblAddPOS))

            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/ImportExport/Default.aspx?Type=C&t=0"), Resources.Resource.btnImpCustomer));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/ImportExport/Default.aspx?Type=P&t=0"), Resources.Resource.btnImpProd));

            //sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/CustomFieldView.aspx"), "Manage Custom Fields"));
            //To do here to add more sub items


            //End SubItemContainer
            sb.Append(UL_END);

            //End Main Item Container
            sb.Append(LI_END);

            Literal ltText = new Literal();
            ltText.Text = sb.ToString();
            parentControl.Controls.Add(ltText);
        }
    }

    private void AddCRMessages(Control parentControl)
    {
        StringBuilder sb = new StringBuilder();

        //First Need to validate user before adding following subrutine
        //Main Item Container Start Here
        sb.Append(LI_START);

        //Add Main Item
        sb.Append(string.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.lblCustomerMessages));

        //Add SubItem Container
        sb.Append(UL_START);

        //Add Sub Items
        sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblCustomerMessages));
        sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Partner/ViewAlert.aspx?Default=1&t=1"), Resources.Resource.btnViewAlerts));
        sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/Home.aspx?t=1"), Resources.Resource.lblMessageCalendar));


        if (ConfigurationManager.AppSettings["BtnEmailVisible"].ToLower() == "yes")
        {
            sb.Append(string.Format(LI_LINK_FORMAT_TARGET, "", "http://callmgmt.itechcanada.com/", "_blank", Resources.Resource.btnMsgEmail));
        }

        if (CurrentUser.IsInRole(RoleID.FAX_INBOX_USER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/FaxInbox/Default.aspx?Default=1&t=1"), Resources.Resource.btnFaxInbox));
        }

        //To do here to add more sub items

        //End SubItemContainer
        sb.Append(UL_END);

        //End Main Item Container
        sb.Append(LI_END);

        Literal ltText = new Literal();
        ltText.Text = sb.ToString();
        parentControl.Controls.Add(ltText);
    }

    private void AddProcurement(Control parentControl)
    {
        StringBuilder sb = new StringBuilder();

        //First Need to validate user before adding following subrutine
        //Main Item Container Start Here
        sb.Append(LI_START);

        //Add Main Item
        sb.Append(string.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.lblProcurement));

        //Add SubItem Container
        sb.Append(UL_START);

        //Add Sub Items
        int visibleItemCount = 0;
        if (CurrentUser.IsInRole(RoleID.VENDOR_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblVendorManagement));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewVendor.aspx?Default=1&t=2"), Resources.Resource.btnimgVendor));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditVendor.aspx?t=2"), Resources.Resource.btnimgAddVendor));
            visibleItemCount = visibleItemCount + 1;
        }

        if (CurrentUser.IsInRole(RoleID.PROCURMENT_MANAGER) || CurrentUser.IsInRole(RoleID.PO_APPROVER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblProcurement));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Procurement/ViewPurchaseOrder.aspx?Default=1"), Resources.Resource.btnimgProcurement));
            visibleItemCount = visibleItemCount + 1;
        }

        if (CurrentUser.IsInRole(RoleID.RECEIVING_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblReceiving));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Receiving/Default.aspx?t=2"), Resources.Resource.btnimgReceiving));
            visibleItemCount = visibleItemCount + 1;
        }

        //To do here to add more sub items

        //End SubItemContainer
        sb.Append(UL_END);

        //End Main Item Container
        sb.Append(LI_END);

        Literal ltText = new Literal();
        ltText.Text = sb.ToString();
        if ((visibleItemCount > 0))
        {
            parentControl.Controls.Add(ltText);
        }
    }

    private void AddCRM(Control parentControl)
    {
        if (CurrentUser.IsInRole(RoleID.CRM_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            StringBuilder sb = new StringBuilder();

            //First Need to validate user before adding following subrutine
            //Main Item Container Start Here
            sb.Append(LI_START);

            //Add Main Item
            sb.Append(string.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.lblCustomerRelationship));

            //Add SubItem Container
            sb.Append(UL_START);

            //Add Sub Items

            sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblCustomerRelationship));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Partner/ViewCustomer.aspx?pType={0}&gType={1}", StatusCustomerTypes.DISTRIBUTER, Convert.ToInt32(StatusGuestType.None))), "View Customers"));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Partner/CustomerEdit.aspx?pType={0}&gType={1}", StatusCustomerTypes.DISTRIBUTER, Convert.ToInt32(StatusGuestType.None))), "Add Customers"));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Partner/ViewCustomer.aspx?pType={0}&gType={1}", StatusCustomerTypes.END_CLINET, Convert.ToInt32(StatusGuestType.Guest))), "View Guest"));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Partner/CustomerEdit.aspx?pType={0}&gType={1}", StatusCustomerTypes.END_CLINET, Convert.ToInt32(StatusGuestType.Guest))), "Add Guest"));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Partner/ViewCustomer.aspx?pType={0}&gType={1}", StatusCustomerTypes.END_CLINET, Convert.ToInt32(StatusGuestType.Staff))), "View Staff"));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Partner/CustomerEdit.aspx?pType={0}&gType={1}", StatusCustomerTypes.END_CLINET, Convert.ToInt32(StatusGuestType.Staff))), "Add Staff Member"));

            sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblCustomerCategories));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/AddEditCategories.aspx"), "Add Categories"));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/ViewCategories.aspx"), "View Categories"));


            //To do here to add more sub items

            //End SubItemContainer
            sb.Append(UL_END);

            //End Main Item Container
            sb.Append(LI_END);

            Literal ltText = new Literal();
            ltText.Text = sb.ToString();
            parentControl.Controls.Add(ltText);
        }
    }

    private void AddInventory(Control parentControl)
    {
        StringBuilder sb = new StringBuilder();

        //First Need to validate user before adding following subrutine
        //Main Item Container Start Here
        sb.Append(LI_START);

        int visibleItemCount = 0;
        //Add Main Item
        if (CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            sb.Append(string.Format(CURRENT_LINK_FORMAT, "", Page.ResolveUrl("~/Inventory/ViewProduct.aspx?Default=1&eComCheck=0&t=4"), Resources.Resource.lblInventoryManagement));
            visibleItemCount++;
        }

        //Add SubItem Container
        sb.Append(UL_START);

        //Add Sub Items
        if (CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblInventoryManagement));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Inventory/ViewProduct.aspx?ptype={0}", Convert.ToInt32(StatusProductType.Product))), Resources.Resource.btnviewprod));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Inventory/Product.aspx?ptype={0}", Convert.ToInt32(StatusProductType.Product))), Resources.Resource.btnaddprod));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Inventory/InventoryUpdate.aspx?t=4&st=2&eComCheck=0"), Resources.Resource.btnupdateprod));
            //To do here to add more sub items

            sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "javascript:void(0);", Resources.Resource.lblAccommodation));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Inventory/ViewProduct.aspx?ptype={0}", Convert.ToInt32(StatusProductType.Accommodation))), Resources.Resource.lblViewAccommodation));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Inventory/Product.aspx?ptype={0}", Convert.ToInt32(StatusProductType.Accommodation))), Resources.Resource.lblAddAccommodation));

            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Inventory/ViewProduct.aspx?ptype={0}", Convert.ToInt32(StatusProductType.ChildUnder12))), Resources.Resource.lblViewChildProducts));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Inventory/Product.aspx?ptype={0}", Convert.ToInt32(StatusProductType.ChildUnder12))), Resources.Resource.lblAddChildProduct));

            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Inventory/ViewProduct.aspx?ptype={0}", Convert.ToInt32(StatusProductType.CourseProduct))), Resources.Resource.lblViewCourseProduct));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Inventory/Product.aspx?ptype={0}", Convert.ToInt32(StatusProductType.CourseProduct))), Resources.Resource.lblAddCourseProduct));

            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Inventory/BuildingView.aspx"), Resources.Resource.lblManageBuildings));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Inventory/RoomView.aspx"), Resources.Resource.lblManageRooms));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Inventory/AmenityView.aspx"), Resources.Resource.lblManageAmenities));

            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Inventory/ViewBlackOutReasons.aspx"), "Manage Blackout Reasons"));

            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Inventory/ViewProduct.aspx?ptype={0}", Convert.ToInt32(StatusProductType.ServiceProduct))), Resources.Resource.lblViewServiceProducts));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/Inventory/ViewProduct.aspx?ptype={0}", Convert.ToInt32(StatusProductType.AdminFee))), Resources.Resource.lblManageAdminFee));

            visibleItemCount++;
        }

        //End SubItemContainer
        sb.Append(UL_END);

        //End Main Item Container
        sb.Append(LI_END);

        Literal ltText = new Literal();
        ltText.Text = sb.ToString();
        if (visibleItemCount > 0)
        {
            parentControl.Controls.Add(ltText);
        }
    }

    private void AddSales(Control parentControl)
    {
        StringBuilder sb = new StringBuilder();

        //First Need to validate user before adding following subrutine
        //Main Item Container Start Here
        sb.Append(LI_START);

        //Add Main Item        
        sb.Append(string.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.lblSalesOrders)); //+ "/Reservation"));

        //Add SubItem Container
        sb.Append(UL_START);

        //Add Sub Items
        sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblSalesOrders)); //+ "/Reservation"));
        int visibleItemCount = 0;

        if (CurrentUser.IsInRole(RoleID.RESERVATION_MANAGER))
        {
            sb.Append(string.Format(LI_LINK_FORMAT, "current", Page.ResolveUrl("~/Reservation/Default.aspx"), "Reservation"));
        }
        if (CurrentUser.IsInRole(RoleID.SALES_MANAGER) || CurrentUser.IsInRole(RoleID.QUOTATION_APPROVER) || CurrentUser.IsInRole(RoleID.SALES_REPRESENTATIVE) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Sales/ViewSalesOrder.aspx?Default=1&t=5"), Resources.Resource.lblSalesOrders));
            //+ Resources.Resource.btnimgSales))
            visibleItemCount = visibleItemCount + 1;
        }

        if (CurrentUser.IsInRole(RoleID.LEADS_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            //sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/TeleMarketing/Assignleads.aspx?Default=1&t=5"), Resources.Resource.cmdAssignleads))
            //visibleItemCount = visibleItemCount + 1
        }

        if (CurrentUser.IsInRole(RoleID.LEADS_MANAGER) || CurrentUser.IsInRole(RoleID.SALES_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            //sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/TeleMarketing/LeadsActivities.aspx?Default=1&t=5"), Resources.Resource.btnActivities))
            //visibleItemCount = visibleItemCount + 1
        }


        if (CurrentUser.IsInRole(RoleID.SHIPPING_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            //Some items from task module moved here below for now
            sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblJobs));

            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Shipping/Default.aspx?t=6"), Resources.Resource.btnimgShipping));
            visibleItemCount = visibleItemCount + 1;
        }


        //To do here to add more sub items

        //End SubItemContainer
        sb.Append(UL_END);

        //End Main Item Container
        sb.Append(LI_END);

        Literal ltText = new Literal();
        ltText.Text = sb.ToString();
        if ((visibleItemCount > 0))
        {
            parentControl.Controls.Add(ltText);
        }

    }

    private void AddTasks(Control parentControl)
    {
        StringBuilder sb = new StringBuilder();

        //First Need to validate user before adding following subrutine
        //Main Item Container Start Here
        sb.Append(LI_START);

        //Add Main Item        
        sb.Append(string.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.lblJobs));

        //Add SubItem Container
        sb.Append(UL_START);

        //Add Sub Items

        sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblJobs));
        int visibleItemCount = 0;
        if (CurrentUser.IsInRole(RoleID.JOB_PLANNER) || CurrentUser.IsInRole(RoleID.JOB_PLANNING_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Shipping/Jobs.aspx?Default=1&t=6"), Resources.Resource.cmdCssJobs));
            visibleItemCount = visibleItemCount + 1;
        }

        if (CurrentUser.IsInRole(RoleID.JOB_PLANNING_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Shipping/JobsAssign.aspx?Default=1&t=6"), Resources.Resource.cmdCssAssignJobs));
            visibleItemCount = visibleItemCount + 1;
        }

        if (CurrentUser.IsInRole(RoleID.SHIPPING_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Shipping/Default.aspx?t=6"), Resources.Resource.btnimgShipping));
            visibleItemCount = visibleItemCount + 1;
        }

        //To do here to add more sub items

        //End SubItemContainer
        sb.Append(UL_END);

        //End Main Item Container
        sb.Append(LI_END);

        Literal ltText = new Literal();
        ltText.Text = sb.ToString();
        if ((visibleItemCount > 0))
        {
            parentControl.Controls.Add(ltText);
        }

    }

    private void AddInvoice(Control parentControl)
    {
        StringBuilder sb = new StringBuilder();

        //First Need to validate user before adding following subrutine
        //Main Item Container Start Here
        sb.Append(LI_START);

        //Add Main Item
        sb.Append(string.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.lblInvoiceGeneration));

        //Add SubItem Container
        sb.Append(UL_START);

        //Add Sub Items
        int visibleItemCount = 0;
        if (CurrentUser.IsInRole(RoleID.INVOICE_GENERATION) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblInvoiceGeneration));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Invoice/ViewInvoice.aspx?Default=1&dflt=Y&t=8"), Resources.Resource.btnimgInvoice));
            //sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Invoice/GenerateInvoice.aspx?t=8"), Resources.Resource.btnGenerateInvoice))
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Invoice/BatchPrint.aspx?t=8"), Resources.Resource.btnbatchPrint));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Invoice/BatchStatement.aspx?t=8"), Resources.Resource.cmdCssBatchstatement));
            visibleItemCount = visibleItemCount + 1;
        }

        if (CurrentUser.IsInRole(RoleID.ACCOUNT_RECEIVABLE) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblAccountsReceivable));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/AccountsReceivable/ViewAccountsReceivable.aspx?Default=1&t=8"), Resources.Resource.btnAccountDetails));
            visibleItemCount = visibleItemCount + 1;
        }

        if (CurrentUser.IsInRole(RoleID.COLLECTION_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblCollection));
            //if (CurrentUser.IsInRole(RoleID.COLLECTION_AGENT) == false) //Prabh Asked to do so to allow assing collection agent to Collection Manager User Also
            //{
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Collection/ViewCollection.aspx?Default=1&t=8"), Resources.Resource.btnAssignAgents));
            //}

            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/CollectionAgent/ViewFollowUp.aspx?Default=1&t=8"), Resources.Resource.btnActivities));
            visibleItemCount = visibleItemCount + 1;
        }

        //To do here to add more sub items

        //End SubItemContainer
        sb.Append(UL_END);

        //End Main Item Container
        sb.Append(LI_END);

        Literal ltText = new Literal();
        ltText.Text = sb.ToString();
        if ((visibleItemCount > 0))
        {
            parentControl.Controls.Add(ltText);
        }

    }

    private void AddPOS(Control parentControl)
    {
        if (CurrentUser.IsInRole(RoleID.POS_USER) || CurrentUser.IsInRole(RoleID.POS_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            StringBuilder sb = new StringBuilder();

            //First Need to validate user before adding following subrutine
            //Main Item Container Start Here
            sb.Append(LI_START);

            //Add Main Item
            if (Session["RegCode"] != null)
            {
                sb.Append(string.Format(CURRENT_LINK_FORMAT, "", Page.ResolveUrl("~/POS/POS.aspx?t=9"), Resources.Resource.btnPointOfSales));
            }
            else
            {
                sb.Append(string.Format(CURRENT_LINK_FORMAT, "", Page.ResolveUrl("~/Admin/SelectRegister.aspx?t=9"), Resources.Resource.btnPointOfSales));
            }

            //End Main Item Container
            sb.Append(LI_END);

            Literal ltText = new Literal();
            ltText.Text = sb.ToString();
            parentControl.Controls.Add(ltText);
        }
    }

    private void AddEcommerce(Control parentControl)
    {
        if (CurrentUser.IsInRole(RoleID.ECOMMERCE_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            StringBuilder sb = new StringBuilder();

            //First Need to validate user before adding following subrutine
            //Main Item Container Start Here
            sb.Append(LI_START);

            //Add Main Item
            sb.Append(string.Format(CURRENT_LINK_FORMAT, "", "#", Resources.Resource.lblECommerce));

            //Add SubItem Container
            sb.Append(UL_START);

            //Add Sub Items
            sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "#", Resources.Resource.lblECommerce));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Inventory/viewCategory.aspx?Default=1&eComCheck=1&t=10"), Resources.Resource.btnecom));
            //sb.Append(String.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/eBay/ProductExport.aspx?t=10"), Resources.Resource.lbleBay))

            //New CMS Implementation
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/ECommerce/cmspageedit.aspx"), "CMS"));

            //To do here to add more sub items

            //End SubItemContainer
            sb.Append(UL_END);

            //End Main Item Container
            sb.Append(LI_END);

            Literal ltText = new Literal();
            ltText.Text = sb.ToString();
            parentControl.Controls.Add(ltText);
        }
    }

    private void AddReports(Control parentControl)
    {
        if (CurrentUser.IsInRole(RoleID.REPORTER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            StringBuilder sb = new StringBuilder();

            //First Need to validate user before adding following subrutine
            //Main Item Container Start Here
            sb.Append(LI_START);

            //Add Main Item        
            sb.Append(string.Format(CURRENT_LINK_FORMAT, "", Page.ResolveUrl("~/Report/Home.aspx?t=11"), Resources.Resource.btnReports));

            //Add SubItem Container
            sb.Append(UL_START);

            //Add Sub Items        
            //sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Report/Default.aspx?XMLFile=SalesByRegister.xml"), Resources.Resource.cmdSalesByRegister));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/NewReport/report.aspx?rpt={0}", Convert.ToInt32(ReportKey.SalesByRegister))), Resources.Resource.cmdSalesByRegister));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Report/Default.aspx?XMLFile=TaxCollectedByRegister.xml"), Resources.Resource.cmdTaxCollectedByRegister));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Report/Default.aspx?XMLFile=SalesByProduct.xml"), Resources.Resource.cmdCssSalesByProduct));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl("~/Report/Default.aspx?XMLFile=WasteTransactionByRegister.xml"), Resources.Resource.cmdWasteReport));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/NewReport/report.aspx?rpt={0}", Convert.ToInt32(ReportKey.OpenOrderFullfillment))), Resources.Resource.cmdOpenOrderFulfillment));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/NewReport/report.aspx?rpt={0}", Convert.ToInt32(ReportKey.InventoryByProduct))), Resources.Resource.cmdInventoryByProduct));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/NewReport/report.aspx?rpt={0}", Convert.ToInt32(ReportKey.VendorPOHistory))), Resources.Resource.cmdVendorPOHistory));

            sb.Append(string.Format(LI_LINK_FORMAT, "menutitle", "javascript:void(0)", "Reservation"));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/NewReport/report.aspx?rpt={0}", Convert.ToInt32(ReportKey.PreviousWeekCount))), "Previous Week Count"));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/NewReport/report.aspx?rpt={0}", Convert.ToInt32(ReportKey.MealCountReport))), "Meal Count Report (AM)"));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/NewReport/report.aspx?rpt={0}", Convert.ToInt32(ReportKey.MealCountReport_PM))), "Meal Count Report (PM)"));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/NewReport/report.aspx?rpt={0}", Convert.ToInt32(ReportKey.SpecialGuestList))), "Special Guest List"));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/NewReport/report.aspx?rpt={0}", Convert.ToInt32(ReportKey.Housekeeping))), "Housekeeping"));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/NewReport/report.aspx?rpt={0}", Convert.ToInt32(ReportKey.GuestList))), "Guest List"));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/NewReport/report.aspx?rpt={0}", Convert.ToInt32(ReportKey.StaffList))), "Staff List"));
            sb.Append(string.Format(LI_LINK_FORMAT, "", Page.ResolveUrl(string.Format("~/NewReport/report.aspx?rpt={0}", Convert.ToInt32(ReportKey.SalesByProductForAssociatedVendor))), "Sales By Product For Vendors"));

            //To do here to add more sub items

            //End SubItemContainer
            sb.Append(UL_END);


            //End Main Item Container
            sb.Append(LI_END);

            Literal ltText = new Literal();
            ltText.Text = sb.ToString();
            parentControl.Controls.Add(ltText);
        }
    } */
    #endregion
}

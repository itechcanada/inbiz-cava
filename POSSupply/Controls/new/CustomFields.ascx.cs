﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Newtonsoft.Json;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;


public partial class Controls_new_CustomFields : System.Web.UI.UserControl
{
    CustomFields _cf = new CustomFields();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Initialize(int primaryKey, CustomFieldApplicableSection section)
    {
        hdnPrimaryKey.Value = primaryKey.ToString();
        hdnSection.Value = ((int)section).ToString();
        sdsCustomFields.SelectCommand = _cf.GetSql(sdsCustomFields.SelectParameters, "", (int)this.Section, Globals.CurrentAppLanguageCode);
        rptControls.DataBind();

        this.Visible = rptControls.Items.Count > 0;
    }

    public void Save()
    {
        List<CustomFieldValues> lValues = this.GetCustomFieldValuesToSave();
        _cf.SetValues(lValues);
    }

    public int RefPrimaryKey {
        get {
            int id = 0;
            int.TryParse(hdnPrimaryKey.Value, out id);
            return id;
        }
    }

    public CustomFieldApplicableSection Section {
        get {
            int id = 0;
            int.TryParse(hdnSection.Value, out id);
            return (CustomFieldApplicableSection)id;
        }
    }


    private List<CustomFieldValues> GetCustomFieldValuesToSave()
    {
        List<CustomFieldValues> lValues = new List<CustomFieldValues>();
        if (this.RefPrimaryKey > 0)
        {
            foreach (RepeaterItem item in rptControls.Items)
            {
                TextBox txtInputCtrl = (TextBox)item.FindControl("txtInputCtrl");
                CheckBox chkBool = (CheckBox)item.FindControl("chkBool");
                HiddenField hdnFieldType = (HiddenField)item.FindControl("hdnFieldType");
                HiddenField hdnFefFieldID = (HiddenField)item.FindControl("hdnFefFieldID");
                CustomFieldValues v = new CustomFieldValues();
                v.RefFieldID = BusinessUtility.GetInt(hdnFefFieldID.Value);
                v.RefPrimaryKey = this.RefPrimaryKey;

                switch (BusinessUtility.GetInt(hdnFieldType.Value))
                {
                    case (int)CustomFieldType.BooleanCheckBox:
                        v.ValueData = chkBool.Checked ? "1" : "0";
                        break;                           
                    default:
                        v.ValueData = txtInputCtrl.Text;
                        break;
                }               
                lValues.Add(v);
            }    
        }        
        return lValues;
    }

    //Custom field repeater databoun   
    protected void rptControls_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            //PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phInputControl");
            TextBox tb = (TextBox)e.Item.FindControl("txtInputCtrl");
            CheckBox chkBool = (CheckBox)e.Item.FindControl("chkBool");
            int fieldType = BusinessUtility.GetInt(DataBinder.Eval(e.Item.DataItem, "FieldType"));
            CustomFieldValues val = null;
            switch (fieldType)
            {
                //case (int)CustomFieldType.Currency:
                //case (int)CustomFieldType.Double:                   
                //case (int)CustomFieldType.Integer:                    
                //case (int)CustomFieldType.StringSingleLine:
                case (int)CustomFieldType.StringMultiline:
                    tb.TextMode = TextBoxMode.MultiLine;
                    goto default;
                case (int)CustomFieldType.BooleanCheckBox:
                    chkBool.Visible = true;
                    if (this.RefPrimaryKey > 0)
                    {
                        val = _cf.GetValue(this.RefPrimaryKey, BusinessUtility.GetInt(DataBinder.Eval(e.Item.DataItem, "ID")), this.Section);
                        if (val != null && val.TypedValue != null)
                        {
                            chkBool.Checked = (bool)val.TypedValue;
                        }
                    }   
                    break;      
                default:
                    tb.Visible = true;
                    if (this.RefPrimaryKey > 0)
                    {
                        val = _cf.GetValue(this.RefPrimaryKey, BusinessUtility.GetInt(DataBinder.Eval(e.Item.DataItem, "ID")), this.Section);
                        if (val != null && val.TypedValue != null)
                        {
                            tb.Text = val.TypedValue.ToString();
                        }
                    }    
                    break;
            }            
        }
    }
}
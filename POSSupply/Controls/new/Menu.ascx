﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Menu.ascx.cs" Inherits="Controls_new_Menu" %>
<%--<div class="sf_menu_container" style="min-width:1010px;">
    <asp:Literal ID="litMenu" Visible="true" runat="server"></asp:Literal>    
    <div style="clear:both;"></div>
</div>
<div style="clear:both;"></div>
<script type="text/javascript">
    $(document).ready(function () {
        $("ul.sf-menu").superfish({ delay: 100});
    }); 
</script> --%>
<div id="smoothmenu1" class="ddsmoothmenu">
    <asp:Literal ID="litMenu" Visible="true" runat="server"></asp:Literal>    
    <div style="clear:both;"></div>
</div>
<div style="clear:both;"></div>
<script type="text/javascript">    
    ddsmoothmenu.init({
        mainmenuid: "smoothmenu1", //menu DIV id
        orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
        classname: 'ddsmoothmenu', //class added to menu's outer DIV
        //customtheme: ["#1c5a80", "#18374a"],
        contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
    })
</script>

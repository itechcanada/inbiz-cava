﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomFields.ascx.cs"
    Inherits="Controls_new_CustomFields" %>
<h3>
    <asp:Literal ID="Literal1" Text="<%$Resources:Resource, lblCustomFields%>" runat="server" />
</h3>
<ul class="form">
    <asp:Repeater ID="rptControls" DataSourceID="sdsCustomFields" runat="server" OnItemDataBound="rptControls_ItemDataBound">
        <ItemTemplate>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblFieldName" Text='<%#Eval("Desc")%>' runat="server" />
                </div>
                <div class="input">
                    <%--<asp:PlaceHolder ID="phInputControl" runat="server" /> --%>
                    <asp:TextBox ID="txtInputCtrl" runat="server" Visible="false" />
                    <asp:CheckBox ID="chkBool" runat="server" Visible="false" />
                    <asp:HiddenField ID="hdnFieldType" runat="server" Value='<%#Eval("FieldType")%>' />
                    <asp:HiddenField ID="hdnFefFieldID" runat="server" Value='<%#Eval("ID")%>' />
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</ul>
<asp:SqlDataSource ID="sdsCustomFields" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
<asp:HiddenField ID="hdnPrimaryKey" runat="server" Value="" />
<asp:HiddenField ID="hdnSection" runat="server" Value="" />
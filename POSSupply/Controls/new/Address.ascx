﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Address.ascx.cs" Inherits="Controls_new_Address" %>
<ul class="form">
    <li>
        <div class="lbl">
            
            <asp:Label Text="<%$Resources:Resource, lblAddressLine1 %>"  ID="lblAddressLine1" runat="server" AssociatedControlID="txtAddressLine1"></asp:Label>
            *</div>
        <div class="input">
            <asp:TextBox ID="txtAddressLine1" runat="server" />
            <asp:RequiredFieldValidator ID="rfvAddressLine1" ErrorMessage="<%$ Resources:Resource, reqvalRegCode%>" runat="server" ControlToValidate="txtAddressLine1"
                Display="None" SetFocusOnError="True"> </asp:RequiredFieldValidator>
        </div>
        <div class="lbl">
            
            <asp:Label Text="<%$Resources:Resource, lblAddressLine2 %>"  ID="lblAddressLine2" runat="server" AssociatedControlID="txtAddressLine2"></asp:Label>
            
        </div>
        <div class="input">
            <asp:TextBox ID="txtAddressLine2" runat="server" />
        </div>
        <div class="clearBoth"></div>
    </li>
    <li>
        <div class="lbl">
            
            <asp:Label Text="<%$Resources:Resource, lblAddressLine3 %>"  ID="lblAddressLine3" runat="server" AssociatedControlID="txtAddressLine3"></asp:Label>
            
        </div>
        <div class="input">
            <asp:TextBox ID="txtAddressLine3" runat="server" />
        </div>
        <div class="lbl">
            
            <asp:Label Text="<%$Resources:Resource, lblCountry %>"  ID="lblCountry" runat="server" AssociatedControlID="txtCountry"></asp:Label>
            *</div>
        <div class="input">
            <asp:TextBox ID="txtCountry" runat="server" />
            <asp:RequiredFieldValidator ID="rfvCountry" ErrorMessage="<%$ Resources:Resource, rfvCountry%>" runat="server" ControlToValidate="txtCountry"
                Display="None" SetFocusOnError="True">
            </asp:RequiredFieldValidator>
        </div>
        <div class="clearBoth"></div>
    </li>
    <li>
        <div class="lbl">
            
            <asp:Label Text="<%$Resources:Resource, lblStateOrProvince %>"  ID="lblStateOrProvince" runat="server" AssociatedControlID="txtState"></asp:Label>
            *</div>
        <div class="input">
            <asp:TextBox ID="txtState" runat="server" />
            <asp:RequiredFieldValidator ID="rfvState" ErrorMessage="<%$ Resources:Resource, rfvState%>" runat="server" ControlToValidate="txtState"
                Display="None" SetFocusOnError="True">
            </asp:RequiredFieldValidator>
        </div>
        <div class="lbl">
            
            <asp:Label Text="<%$Resources:Resource, lblCity %>"  ID="lblCity" runat="server" AssociatedControlID="txtCity"></asp:Label>
            *</div>
        <div class="input">
            <asp:TextBox ID="txtCity" runat="server" />
            <asp:RequiredFieldValidator ID="rfvCity" ErrorMessage="<%$ Resources:Resource, rfvCity%>" runat="server" ControlToValidate="txtCity"
                Display="None" SetFocusOnError="True">
            </asp:RequiredFieldValidator>
        </div>
        <div class="clearBoth"></div>
    </li>
    <li>
        <div class="lbl">
            
            <asp:Label Text="<%$Resources:Resource, lblPostalCode %>"  ID="lblPostalCode" runat="server" AssociatedControlID="txtPostalCode"></asp:Label>
            *</div>
        <div class="input">
            <asp:TextBox ID="txtPostalCode" runat="server" />
            <asp:RequiredFieldValidator ID="rfvPostalCode" ErrorMessage="<%$ Resources:Resource, rfvPostalCode%>" runat="server" ControlToValidate="txtPostalCode"
                Display="None" SetFocusOnError="True">
            </asp:RequiredFieldValidator>
        </div>
        <div class="lbl">
            
        </div>
        <div class="input">
        </div>
        <div class="clearBoth"></div>
    </li>
</ul>
<asp:HiddenField ID="hdnAddressID" runat="server" />
<asp:HiddenField ID="hdnReferenceTable" runat="server" />
<asp:HiddenField ID="hdnAddressType" runat="server" />
<asp:HiddenField ID="hdnSourcePrimaryKey" runat="server" />

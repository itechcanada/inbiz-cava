﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MessageControl.ascx.cs" Inherits="Controls_new_MessageControl" %>
<div id="pnlMessage" runat="server">
</div>
<asp:HyperLink ID="hlMessage" runat="server" Visible="false" NavigateUrl=""></asp:HyperLink>
<script type="text/javascript">
    function getGlobalMessage() {
        var messageUrl = '<%=MessageUrl%>';
        if (messageUrl.length > 0) {
            $("#<%=pnlMessage.ClientID%>").load(messageUrl);
            $(window).trigger("resize");
        }
    }
    $(document).ready(getGlobalMessage());
</script>
<%--<asp:Panel ID="pnlAsyncCallPanel" runat="server">
    <script type="text/javascript">        
       
    </script>
</asp:Panel>--%>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;

public partial class Controls_new_Address : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public int AddressID
    {
        get
        {
            int addressID1 = 0;
            int.TryParse(hdnAddressID.Value, out addressID1);
            return addressID1;
        }
        set { hdnAddressID.Value = value.ToString(); }
    }

    public string ReferenceTable
    {
        get { return hdnReferenceTable.Value; }
        set { hdnReferenceTable.Value = value; }
    }

    public int SourcePrimaryKey
    {
        get
        {
            int pk = 0;
            int.TryParse(hdnSourcePrimaryKey.Value, out pk);
            return pk;
        }
        set { hdnSourcePrimaryKey.Value = value.ToString(); }
    }

    public string AddressType
    {
        get { return hdnAddressType.Value; }
        set { hdnAddressType.Value = value; }
    }


    public string ValidationGroup
    {
        get { return rfvAddressLine1.ValidationGroup; }
        set
        {
            rfvAddressLine1.ValidationGroup = value;
            rfvCity.ValidationGroup = value;
            rfvCountry.ValidationGroup = value;
            rfvPostalCode.ValidationGroup = value;
            rfvState.ValidationGroup = value;
        }
    }

    public Addresses Address
    {
        get
        {
            Addresses addr = new Addresses();
            addr.AddressLine1 = txtAddressLine1.Text;
            addr.AddressLine2 = txtAddressLine2.Text;
            addr.AddressLine3 = txtAddressLine3.Text;
            addr.AddressType = this.AddressType;
            addr.AddressCity = txtCity.Text;
            addr.AddressCountry = txtCountry.Text;
            addr.AddressPostalCode = txtPostalCode.Text;
            addr.AddressState = txtState.Text;
            addr.AddressRef = this.ReferenceTable;
            addr.AddressSourceID = this.SourcePrimaryKey;
            return addr;
        }
    }

    public void Initialize()
    {
        //To Here to initialize control
        Addresses addr = new Addresses();
        addr.GetAddress(this.SourcePrimaryKey, this.ReferenceTable, this.AddressType);
        txtAddressLine1.Text = addr.AddressLine1;
        txtAddressLine2.Text = addr.AddressLine2;
        txtAddressLine3.Text = addr.AddressLine3;
        txtCity.Text = addr.AddressCity;
        txtCountry.Text = addr.AddressCountry;
        txtPostalCode.Text = addr.AddressPostalCode;
        txtState.Text = addr.AddressState;
    }
}
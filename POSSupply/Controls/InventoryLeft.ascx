<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InventoryLeft.ascx.vb" Inherits="InventoryLeft" %>
        <div>
        <BR /><BR />
        <table align=left width=100% class="table" style="border:0" border=0 >

        <% If Session.Item("eComCheck") = "True" And (Session("UserModules").ToString.Contains("eCOM") = True Or Session("UserModules").ToString.Contains("ADM") = True) Then%>

             <tr><td height=5></td></tr>
        <tr height=25>
           <td align=center class=titleBgColor >
                <asp:Label ID="lblECom" runat=server cssclass="leftHeading" Text="<%$ Resources:Resource, lblECommerce %>" ></asp:Label></td>
        </tr>
        <tr><td height=5></td></tr>
         <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID="imgViewCategory" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnviewcat%></span></a></div>
            </td>
        </tr>
        <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID="imgAddCategory" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnaddcat%></span></a></div>
            </td>
        </tr>
         <tr><td height=2></td></tr>
         <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID="imgViewSubcategory" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnviewsubcat%></span></a></div>
            </td>
        </tr>
        <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID="imgAddSubCategory" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnaddsubcat%></span></a></div>
            </td>
        </tr>
         <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID="imgAssignProducts" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnassprod%></span></a></div>
            </td>
        </tr>
        <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID="imgeComMetaTag" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnmetatags%></span></a></div>
            </td>
        </tr>
        <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID="ImgeComManageWebPages" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnmanageWebPages%></span></a></div>
            </td>
        </tr>
       <% Else%> 
               
        <tr height=25>
           <td align=center class=titleBgColor >
                <asp:Label ID="lblInvHeading" runat=server cssclass="leftHeading" Text="<%$ Resources:Resource, lblInventory %>" ></asp:Label></td>
        </tr>
        <tr><td height=5></td></tr>
          <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID="imgCmdViewPrd" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnviewprod%></span></a></div>
            </td>
        </tr>
        <% If Session("UserModules").ToString.Contains("STK") = True Or Session("UserModules").ToString.Contains("ADM") = True Then%>
        <tr><td height=2></td></tr>
        <tr>
        <td align=right>
          <div class="buttonwrapper"><a ID="imgCmdAddPrd" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnaddprod%></span></a></div>
        </td>
        </tr>
        <tr><td height=2></td></tr>
        <tr>
        <td align=right>
          <div class="buttonwrapper"><a ID="imgCmdUpdPrd" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnupdateprod%></span></a></div>
        </td>
        </tr>
        <% end if %>
        <% If Request.QueryString("prdID") <> "" Then%>
         <tr><td height=10></td></tr>
         <tr height=25>
           <td align=center class=titleBgColor>
                <asp:Label ID="lblPrdName" runat=server cssclass="leftHeading" ></asp:Label></td>
        </tr>
        <tr><td height=5></td></tr>
          <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID="imgview" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnProductDetail%></span></a></div>
            </td>
        </tr>
        <tr><td height=5></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID="imgCmdDescri" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnproddesc%></span></a></div>
            </td>
        </tr>
          <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID="imgCmdPrdColor" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnprodcolor%></span></a></div>
            </td>
        </tr>
          <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID="imgCmdPrdImages" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnprodimages%></span></a></div>
            </td>
        </tr>
         <% if request.QueryString("kit")="1" then %>
        <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID="imgCmdPrdKit" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnprodkit%></span></a></div>
            </td>
        </tr>
        <% end if %>
         <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID="imgCmdPrdQty" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnProductQuantity%></span></a></div>
            </td>
        </tr>

        <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                 <div class="buttonwrapper"><a ID="imgCmdPrdAss" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnprodasso%></span></a></div>
            </td>
        </tr>
          <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                 <div class="buttonwrapper"><a ID="imgCmdPrdSale" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnprodsale%></span></a></div>
            </td>
        </tr>
       
       <% If Session("UserModules").ToString.Contains("STK") = True Or Session("UserModules").ToString.Contains("ADM") = True Then%>
        <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                <div class="buttonwrapper"><a ID="imgCmdAssociteVendor" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnassovender%></span></a></div>
            </td>
        </tr>
        <% end if %>
        <%--  <tr><td height=2></td></tr>
        <tr>
            <td align=right>
                 <asp:ImageButton ID=imgCmdAvaWarehouse runat=server CausesValidation=false ImageUrl="~/images/avawhrhouse.png" />
            </td>
        </tr>--%>
        <% end if %>
        <%End If%>
        <tr><td height=10></td></tr>
        </table> 
        </div>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;

public partial class Controls_UsersLeft : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        {
            BindNavigation();
        }
    }


    public void BindNavigation()
    {
        blstNav.Items.Clear();

        if (BusinessUtility.GetString(Request.QueryString["WhsCode"]) != "")
        {
            blstNav.Items.Add(GetSectionLink(UserSectionKey.Whs));
            return;
        }

        blstNav.Items.Add(GetSectionLink(UserSectionKey.Details));
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            if (this.UserID > 0)
            {
                blstNav.Items.Add(GetSectionLink(UserSectionKey.Commission));
                blstNav.Items.Add(GetSectionLink(UserSectionKey.Role));
                blstNav.Items.Add(GetSectionLink(UserSectionKey.SalesGoal));
                blstNav.Items.Add(GetSectionLink(UserSectionKey.AccessIPs));
            }
            blstNav.Items.Add(GetSectionLink(UserSectionKey.View));
        }
    }

    public int UserID
    {
        get
        {
            int uid = 0;
            int.TryParse(Request.QueryString["UserID"], out uid);
            return uid;
        }
    }

    private ListItem GetSectionLink(UserSectionKey sectionIndex)
    {
        ListItem li = new ListItem(this.MenuName(sectionIndex), this.MenuUrl(sectionIndex));
        if (sectionIndex == this.ActiveSectionIndex)
        {
            li.Attributes["class"] = "open";
        }
        return li;
    }

    private string MenuName(UserSectionKey sectionIndex)
    {
        string sMenuName = "";
        switch (sectionIndex)
        {
            case UserSectionKey.Details:
                sMenuName = Resources.Resource.lnkUserDetail;
                break;
            case UserSectionKey.Commission:
                sMenuName = Resources.Resource.lnkUserCommission;
                break;
            case UserSectionKey.Role:
                sMenuName = Resources.Resource.lnkUserRole;
                break;
            case UserSectionKey.SalesGoal:
                sMenuName = Resources.Resource.lnkUserSalesGoal;
                break;
            case UserSectionKey.AccessIPs:
                sMenuName = Resources.Resource.lnkUserAccessIPs;
                break;
            case UserSectionKey.View:
                sMenuName = Resources.Resource.lnkViewUsers;
                break;
            case UserSectionKey.Whs:
                sMenuName = Resources.Resource.ViewWarehouse;
                break;
        }
        return sMenuName;
    }

    private string MenuUrl(UserSectionKey sectionIndex)
    {
        string sMenuUrl = "";
        switch (sectionIndex)
        {
            case UserSectionKey.Details:
                sMenuUrl = "~/Admin/AddEditUser.aspx?UserID=" + this.UserID + "&section=" + ((int)UserSectionKey.Details).ToString() + "";
                break;
            case UserSectionKey.Commission:
                sMenuUrl = "~/Admin/ViewUserCommissions.aspx?UserID=" + this.UserID + "&section=" + ((int)UserSectionKey.Commission).ToString() + "";
                break;
            case UserSectionKey.Role:
                sMenuUrl = "~/Admin/AddEditUserRole.aspx?UserID=" + this.UserID + "&section=" + ((int)UserSectionKey.Role).ToString() + "";
                break;
            case UserSectionKey.SalesGoal:
                sMenuUrl = "~/Admin/ViewUserSalesGoal.aspx?UserID=" + this.UserID + "&section=" + ((int)UserSectionKey.SalesGoal).ToString() + "";
                break;
            case UserSectionKey.AccessIPs:
                sMenuUrl = "~/Admin/ViewUserAccessIPs.aspx?UserID=" + this.UserID + "&section=" + ((int)UserSectionKey.AccessIPs).ToString() + "";
                break;
            case UserSectionKey.View:
                sMenuUrl = "~/Admin/ViewInternalUser.aspx?Default=1&t=0";
                break;
            case UserSectionKey.Whs:
                sMenuUrl = "~/Admin/ViewWarehouse.aspx";
                break;
        }
        return sMenuUrl;
    }

    private UserSectionKey ActiveSectionIndex
    {
        get
        {
            int section = 0;
            if (int.TryParse(Request.QueryString["section"], out section))
            {
                if (Enum.IsDefined(typeof(UserSectionKey), section))
                {
                    return (UserSectionKey)Enum.ToObject(typeof(UserSectionKey), section);
                }
                else
                {
                    return UserSectionKey.Details;
                }
            }
            return UserSectionKey.Details;
        }
    }
}
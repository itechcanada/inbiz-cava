<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LeftSales.ascx.vb" Inherits="LeftSales" %>
  <div>
  <BR /><BR />
  <table align=left width=100% class="table" style="border:0" border=0 >
     <tr height=25>
       <td align=center class=titleBgColor >
            <asp:Label ID="lblPO" runat=server cssclass="leftHeading" 
            Text="<%$ Resources:Resource, ManageOrders %>" ></asp:Label>
       </td>
    </tr>
    <tr><td height=5></td></tr>
    <% if Session("UserModules").ToString.Contains("SAL") = True OR Session("UserModules").ToString.Contains("ADM") = True %>
    <tr>
        <td align=right>
            <div class="buttonwrapper"><a ID="imgCmdView" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnViewOrders%></span></a></div>
        </td>
    </tr>
    <tr><td height=2></td></tr>
    <% end if %>
    <% if Session("UserModules").ToString.Contains("QOA") = True OR Session("UserModules").ToString.Contains("ADM") = True %>
    <tr>
        <td align=right>
            <div class="buttonwrapper"><a ID="imgCmdApprove" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnApproveOrder%></span></a></div>
        </td>
    </tr>
    <%end if %>
    <% if Session("UserModules").ToString.Contains("SAL") = True OR Session("UserModules").ToString.Contains("ADM") = True %>
    <tr><td height=2></td></tr>
    <tr>
        <td align=right>
            <div class="buttonwrapper"><a ID="imgCmdCreate" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnCreateOrder%></span></a></div>
        </td>
    </tr>
    <% end if %>
    <% if Request.QueryString("custID")<>"" then %>
     <tr><td height=10></td></tr>
     <tr height=25>
       <td align=center class=tdBorder>
            <asp:Label ID="lblName" runat=server cssclass="leftHeading" >
            </asp:Label>
       </td>
    </tr>
    <tr><td height=5></td></tr>
    <tr>
        <td align=right>
            <div class="buttonwrapper"><a ID="imgCmdAddSalesProcess" runat=server CausesValidation=false class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnAddSalesProcess%></span></a></div>
        </td>
    </tr>
    <tr><td height=5></td></tr>
    <tr>
        <td align=right>
            <div class="buttonwrapper"><a ID="imgCmdShowQuotation" runat=server CausesValidation=false Visible="false" class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:120px;text-align:center;"><%=Resources.Resource.btnShowQuotation%></span></a></div>
        </td>
    </tr>
    <%end if %>
    <tr><td height=10></td></tr>
  </table> 
  </div>
    <script language=javascript>
    function openPopup(strOpen)
    {
        open(strOpen, "AddItemProcess",'height=365,width=840,left=180,top=150,resizable=no,location=no,scrollbars=yes,toolbar=no,status=no');
        return false;
    }
</script> 
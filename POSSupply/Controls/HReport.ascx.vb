Imports Resources.Resource
Partial Class Controls_HReport
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not String.IsNullOrEmpty(Request.QueryString("t")) Then
            Session.Remove("st")
        End If

        SetCurrentTab()
    End Sub

    Private Sub SetCurrentTab()
        If Session("st") = "0" Then
            cmdSalesByRegister.Attributes.Add("class", "current")
        ElseIf Session("st") = "1" Then
            cmdTaxCollectedByRegister.Attributes.Add("class", "current")
        ElseIf Session("st") = "2" Then
            cmdSalesByProduct.Attributes.Add("class", "current")
        ElseIf Session("st") = "3" Then
            cmdWasteReport.Attributes.Add("class", "current")
        End If
    End Sub

    Protected Sub cmdSalesByRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSalesByRegister.ServerClick
        Session("st") = "0"
        Response.Redirect("~/Report/Default.aspx?XMLFile=SalesByRegister.xml")
    End Sub

    Protected Sub cmdTaxCollectedByRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaxCollectedByRegister.ServerClick
        Session("st") = "1"
        Response.Redirect("~/Report/Default.aspx?XMLFile=TaxCollectedByRegister.xml")
    End Sub

    Protected Sub cmdSalesByProduct_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSalesByProduct.ServerClick
        Session("st") = "2"
        Response.Redirect("~/Report/Default.aspx?XMLFile=SalesByProduct.xml")
    End Sub

    Protected Sub cmdWasteReport_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdWasteReport.ServerClick
        Session("st") = "3"
        Response.Redirect("~/Report/Default.aspx?XMLFile=WasteTransactionByRegister.xml")
    End Sub

    Protected Sub cmdOpenOrder_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOpenOrder.ServerClick
        Response.Redirect(String.Format("~/NewReport/report.aspx?xmlpath={0}", Server.UrlEncode("~/NewReport/xml/OpenOrderFulfillment.xml")))
    End Sub

    Protected Sub cmdStorageInventory_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStorageInventory.ServerClick
        Response.Redirect(String.Format("~/NewReport/report.aspx?xmlpath={0}", Server.UrlEncode("~/NewReport/xml/ShortageInventory.xml")))
    End Sub


    Protected Sub cmdVendorPoHistory_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVendorPoHistory.ServerClick
        Response.Redirect(String.Format("~/NewReport/report.aspx?xmlpath={0}", Server.UrlEncode("~/NewReport/xml/VendorPOHistory.xml")))
    End Sub
End Class
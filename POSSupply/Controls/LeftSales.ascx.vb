Imports Resources.Resource
Partial Class LeftSales
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("custID") <> "" And Request.QueryString("custName") <> "" Then
            lblName.Text = lblSOCustomer & " " & Request.QueryString("custName")
            Me.imgCmdAddSalesProcess.Attributes.Add("OnClick", "return openPopup('~/Sales/CreateSalesProcess.aspx?custID=" & Request.QueryString("custID") & "&custName=" & Request.QueryString("custName") & "&ComID=" & Request.QueryString("ComID") & "&whsID=" & Request.QueryString("whsID") & "')")
        End If
    End Sub

    Protected Sub imgCmdApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdApprove.ServerClick
        Response.Redirect("~/Sales/Default.aspx?Default=1")
    End Sub

    Protected Sub imgCmdCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdCreate.ServerClick
        Session("SalesCart") = ""
        Session("IsSalesCart") = ""
        Session("SalesProcessCart") = ""
        Session("IsSalesProcessCart") = ""
        Response.Redirect("~/Sales/Create.aspx")
    End Sub

    Protected Sub imgCmdView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdView.ServerClick
        Response.Redirect("~/Sales/ViewSalesOrder.aspx?Default=1")
    End Sub

    'Protected Sub imgCmdAddSalesProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdAddSalesProcess.Click
    '    Response.Redirect("CreateSalesProcess.aspx?custID=" & Request.QueryString("custID") & "&custName=" & Request.QueryString("custName"))
    'End Sub

    Protected Sub imgCmdShowQuotation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdShowQuotation.ServerClick
        Response.Redirect("~/Sales/Create.aspx?custID=" & Request.QueryString("custID") & "&custName=" & Request.QueryString("custName"))
    End Sub
End Class

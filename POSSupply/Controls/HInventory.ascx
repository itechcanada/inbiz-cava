<%@ Control Language="VB" AutoEventWireup="false" CodeFile="HInventory.ascx.vb" Inherits="Controls_HInventory" %>
<div id="tabsH">
    <ul id="ulMainMenu" runat="server">
        <% If Session.Item("eComCheck") = "True" And (Session("UserModules").ToString.Contains("eCOM") = True Or Session("UserModules").ToString.Contains("ADM") = True) Then%>
        <%--<li>
            <a href="#"><asp:Label ID="lblECom" runat="server" CssClass="heading" Text="<%$ Resources:Resource, lblECommerce %>"></asp:Label></a>
        </li>--%>
        <li><a id="imgViewCategory" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnviewcat%></span>
       </a></li>
        <li><a id="imgAddCategory" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnaddcat%></span>
        </a></li>
        <li><a id="imgViewSubcategory" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnviewsubcat%></span>
        </a></li>
        <li><a id="imgAddSubCategory" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnaddsubcat%></span>
        </a></li>
        <li><a id="imgAssignProducts" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnassprod%></span>
        </a></li>
        <li><a id="imgeComMetaTag" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnmetatags%></span>
        </a></li>
        <li><a id="ImgeComManageWebPages" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnmanageWebPages%></span>
        </a></li>
        <% Else%>
        <%--<li>
            <a href="#"><asp:Label ID="lblInvHeading" runat="server" CssClass="heading" Text="<%$ Resources:Resource, lblInventory %>"></asp:Label></a>
        </li>--%>
        <li ><a id="imgCmdViewPrd" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnviewprod%></span>
        </a></li>
        <% If Session("UserModules").ToString.Contains("STK") = True Or Session("UserModules").ToString.Contains("ADM") = True Then%>
        <li><a id="imgCmdAddPrd" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnaddprod%></span>
        </a></li>
        <li><a id="imgCmdUpdPrd" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnupdateprod%></span>
        </a></li>
        <% End If%>
        <% If Request.QueryString("prdID") <> "" Then%>
        <%--<li>
             <a href="#"><asp:Label ID="lblPrdName" runat="server" CssClass="heading"></asp:Label></a>
        </li>--%>
        <li class="sMenu"><a id="imgview" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnProductDetail%></span>
        </a></li>
        <li class="sMenu"><a id="imgCmdDescri" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnproddesc%></span>
        </a></li>
        <li class="sMenu"><a id="imgCmdPrdColor" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnprodcolor%></span>
        </a></li>
        <li class="sMenu"><a id="imgCmdPrdImages" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnprodimages%></span>
        </a></li>
        <% If Request.QueryString("kit") = "1" Then%>
        <li class="sMenu"><a id="imgCmdPrdKit" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnprodkit%></span>
        </a></li>
        <% End If%>
        <li class="sMenu"><a id="imgCmdPrdQty" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnProductQuantity%></span>
        </a></li>
        <li class="sMenu"><a id="imgCmdPrdAss" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnprodasso%></span>
        </a></li>
        <li class="sMenu"><a id="imgCmdPrdSale" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnprodsale%></span>
        </a></li>
        <% If Session("UserModules").ToString.Contains("STK") = True Or Session("UserModules").ToString.Contains("ADM") = True Then%>
        <li class="sMenu"><a id="imgCmdAssociteVendor" runat="server" causesvalidation="false" href="#">
            <span><%=Resources.Resource.btnassovender%></span>
        </a></li>
        <% End If%>
        <% End If%>
        <% End If%>
    </ul>
</div>



      
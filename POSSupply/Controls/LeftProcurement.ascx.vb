Imports Resources.Resource
Partial Class LeftProcurement
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("VdrID") <> "" Then
            Dim objVendor As New clsVendor
            objVendor.VendorID = Request.QueryString("VdrID")
            objVendor.getVendorInfo()
            lblVendorName.Text = lblPOVendor & " " & objVendor.VendorName
            objVendor = Nothing
        End If
    End Sub

    Protected Sub imgCmdApprovePO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdApprovePO.ServerClick
        Response.Redirect("~/Procurement/Default.aspx?Default=1")
    End Sub

    Protected Sub imgCmdCreatePO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdCreatePO.ServerClick
        Session("SRCart") = ""
        Session("IsCart") = ""
        Response.Redirect("~/Procurement/Create.aspx")
    End Sub

    Protected Sub imgCmdUpdatePO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdUpdatePO.ServerClick
        Response.Redirect("~/Procurement/UpdatePurchaseOrder.aspx")
    End Sub

    Protected Sub imgCmdViewPO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdViewPO.ServerClick
        Response.Redirect("~/Procurement/ViewPurchaseOrder.aspx?Default=1")
    End Sub
End Class

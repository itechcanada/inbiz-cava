<%@ Control Language="VB" AutoEventWireup="false" CodeFile="HPOS.ascx.vb" Inherits="Controls_HPOS" %>
<script type="text/javascript" language="JavaScript" src="../scripts/akModal/akModal.js"></script>
<script type="text/javascript" language="JavaScript" src="../scripts/akModal/dimmer.js"></script>
<div id="tabsH">
    <ul id="ulMainMenu" runat="server">
        <li><a id="imgCmdSell" runat="server" causesvalidation="false" href="#"><span style="min-width: 120px;
            text-align: center;">
            <%=Resources.Resource.lblSell%>
        </span></a></li>
        <li><a id="imgCmdRefund" runat="server" causesvalidation="false" href="#"><span style="min-width: 120px;
            text-align: center;">
            <%=Resources.Resource.lblPORefund%>
        </span></a></li>
        <li><a id="imgCmdPaused" runat="server" causesvalidation="false" href="#"><span style="min-width: 120px;
            text-align: center;">
            <%=Resources.Resource.lblOrder%>
        </span></a></li>
        <li><a id="imgReceipts" runat="server" causesvalidation="false" href="#"><span style="min-width: 120px;
            text-align: center;">
            <%=Resources.Resource.lblReceipt%>
        </span></a></li>
        <% If (Session("UserModules").ToString.Contains("ADM") = True Or Session("UserModules").ToString.Contains("PSMGR") = True) And ConfigurationManager.AppSettings("MerchantID").ToLower = "yes" And (Request.Url.AbsoluteUri.ToLower.Contains("/pos.aspx") Or Request.Url.AbsoluteUri.ToLower.Contains("/refund.aspx?ttype=r")) Then%>
        <li><a id="imgCmdPinPadOpen" runat="server" causesvalidation="false" onclick="doAppletTransaction('TI'); return false;"
            href="#"><span style="min-width: 120px; text-align: center;">
                <%=Resources.Resource.btnInitializePinPad%>
            </span></a></li>
        <li><a id="imgCmdPinPadClose" runat="server" causesvalidation="false" onclick="doAppletTransaction('TS'); return false;"
            href="#"><span style="min-width: 120px; text-align: center;">
                <%=Resources.Resource.btnCloseBalanceRegister%>
            </span></a></li>
        <% End If%>
        <% If (Session("UserModules").ToString.Contains("ADM") = True Or Session("UserModules").ToString.Contains("PSMGR") = True) Then%>
        <li><a id="imgMgnReg" runat="server" causesvalidation="false" href="#"><span style="min-width: 120px;
            text-align: center;">
            <%=Resources.Resource.btnManageRegister%>
        </span></a></li>
        <% End If%>
        <li style="display:none;"><a id="imgMgnKitchen" runat="server" causesvalidation="false" href="#"><span style="min-width: 120px;
            text-align: center;">
            
            <%=Resources.Resource.lblKitchen%>
        </span></a></li>
    </ul>
    <div class="clr">
        &nbsp;</div>
</div>

﻿
Partial Class Controls_CommonSearch_InventorySearch
    Inherits System.Web.UI.UserControl
    Private clsStatus As New clsSysStatus

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not IsPostBack) Then
            clsStatus.subGetStatus(dlSearch, "PD", "dlSh1")
        End If
    End Sub

    
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect(String.Format("~/Inventory/ViewProduct.aspx?status={0}&sdata={1}", dlSearch.SelectedValue, txtSearch.Text))
    End Sub
End Class

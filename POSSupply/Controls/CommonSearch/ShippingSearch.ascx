﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ShippingSearch.ascx.vb"
    Inherits="Controls_CommonSearch_ShippingSearch" %>
<div class="searchBar">
        <div class="header">
            <div class="title">
                Search form</div>
            <div class="icon">
                <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
        </div>
        <h4>
                <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, liWarehouseLocation%>"></asp:Label>
                  </h4>
        <div class="inner">
                <asp:DropDownList ID="dlWarehouse" runat="server" Width="175px" ValidationGroup="PrdSrch">
                </asp:DropDownList>
        </div>
        <h4>
                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"></asp:Label>
                 </h4>
        <div class="inner">
                <asp:DropDownList ID="dlSearch" runat="server" Width="175px" ValidationGroup="PrdSrch">
                    <asp:ListItem Text="Search By" Value="" />
                    <asp:ListItem Text="Order No" Value="ON" />
                    <asp:ListItem Text="Customer Name" Value="CN" />
                    <asp:ListItem Text="Sales Agent Name" Value="AN" />
                    <asp:ListItem Text="Shipping Track No" Value="TN" />
                    <asp:ListItem Text="Today" Value="T" />
                    <asp:ListItem Text="This Week" Value="TW" />
                    <asp:ListItem Text="Next Week" Value="NW" />
                    <asp:ListItem Text="All" Value="AL" />
                </asp:DropDownList>
     </div>
        <h4>
                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword%>"></asp:Label>
                   </h4>
        <div class="inner">
                <asp:TextBox runat="server" Width="170px" ID="txtSearch" ValidationGroup="PrdSrch"></asp:TextBox>
         </div>
       
              
            
<div class="footer">
            <div class="submit">
                <asp:Button runat="server" Text="Submit" ID="btnSubmit" ValidationGroup="PrdSrch" 
                    CausesValidation="False" />
            </div>
        </div>
    </div>
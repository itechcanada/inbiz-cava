﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="OrderSearch.ascx.vb" Inherits="Controls_CommonSearch_OrderSearch" %>


<asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel" DefaultButton="btnSubmit">
     <div class="header">
            <div class="title">
                Search form</div>
            <div class="icon">
                <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
        </div>
        <h4>
                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"></asp:Label>
                    </h4>
        <div class="inner">
                <asp:DropDownList ID="dlSearch" runat="server" Width="175px" ValidationGroup="PrdSrch">
                </asp:DropDownList>
        </div>
        <h4>
                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lblStatus %>"></asp:Label>
                    </h4>
        <div class="inner">
                <asp:DropDownList ID="dlPOStatus" runat="server" Width="175px">
                </asp:DropDownList>
          </div>
        <h4>
                <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword%>"></asp:Label>
                    </h4>
        <div class="inner">
                <asp:TextBox runat="server" Width="170px" ID="txtSearch" ValidationGroup="PrdSrch"></asp:TextBox>
        </div>
      
               
           <div class="footer">
            <div class="submit">
                <asp:Button runat="server" Text="Submit" ID="btnSubmit" ValidationGroup="na" 
                    CausesValidation="False" />
            </div>
        </div>
    </div>
</asp:Panel>

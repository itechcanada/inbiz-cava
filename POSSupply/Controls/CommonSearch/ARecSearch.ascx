﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ARecSearch.ascx.vb" Inherits="Controls_CommonSearch_ARecSearch" %>

<asp:Panel CssClass="divSectionContent" runat="server" ID="SearchPanel" DefaultButton="imgSearch">
    <table id="tblSearch" class="searchOptionTable" cellpadding="1" cellspacing="1" border="0"
        runat="server">
        <tr>
            <td align="left">
                <asp:Label ID="lblSearchBy" runat="server" Text="<%$ Resources:Resource, lblSearchBy %>"></asp:Label>
                <br class="brMargin5" />
                <asp:DropDownList ID="dlSearch" runat="server" Width="175px" ValidationGroup="PrdSrch">
                    <asp:ListItem Value="" Text="<%$ Resources:Resource, liSelect %>" />
                    <asp:ListItem Value="ON" Text="<%$ Resources:Resource, liAROrderNo %>" />
                    <asp:ListItem Value="IN" Text="<%$ Resources:Resource, liARInvoiceNo %>" />
                    <asp:ListItem Value="CN" Text="<%$ Resources:Resource, liARCustomerName %>" />
                    <asp:ListItem Value="CP" Text="<%$ Resources:Resource, liARCustomerPhoneNumber %>" />
                    <asp:ListItem Value="PO" Text="<%$ Resources:Resource, liCustomerPO %>" />
                    <asp:ListItem Value="AY" Text="<%$ Resources:Resource, liCMAcronyme %>" />
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblCreatedDays" runat="server" Text="<%$ Resources:Resource, lblCreatedDays %>"></asp:Label>
                <br class="brMargin5" />
                <asp:DropDownList ID="dlCreatedDate" runat="server" Width="175px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblSearchKeyword" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label>
                <br class="brMargin5" />
                <asp:TextBox runat="server" Width="170px" ID="txtSearch" ValidationGroup="PrdSrch"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:ImageButton ID="imgSearch" CausesValidation="false" runat="server" AlternateText="Search" ImageUrl="~/images/search-btn.gif"
                    ValidationGroup="PrdSrch" />
            </td>
        </tr>
    </table>
</asp:Panel>

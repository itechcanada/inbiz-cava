﻿
Partial Class Controls_CommonSearch_ARecSearch
    Inherits System.Web.UI.UserControl

    Private clsStatus As New clsSysStatus

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not IsPostBack) Then
            clsStatus.subGetStatus(dlCreatedDate, "AR", "dlSh2", "AR")
        End If
    End Sub

    Protected Sub imgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
        Response.Redirect(String.Format("~/AccountsReceivable/ViewAccountsReceivable.aspx?status={0}&sby={1}&sdata={2}", dlSearch.SelectedValue, dlCreatedDate.SelectedValue, txtSearch.Text))
    End Sub
End Class

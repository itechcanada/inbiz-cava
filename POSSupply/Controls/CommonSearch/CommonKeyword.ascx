﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CommonKeyword.ascx.vb"
    Inherits="Controls_CommonSearch_CommonKeyword" %>
<asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel" DefaultButton="btnSubmit">
    <div class="searchBar">
        <div class="header">
            <div class="title">
                Search form</div>
            <div class="icon">
                <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
        </div>
        <h4>
            <asp:Label ID="lblSearchKeyword" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label>
        </h4>
        <div class="inner">
            <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
        </div>
        <div class="footer">
            <div class="submit">
                <asp:Button runat="server" Text="Submit" ID="btnSubmit" ValidationGroup="na" 
                    CausesValidation="False" />
            </div>
        </div>
    </div>
</asp:Panel>

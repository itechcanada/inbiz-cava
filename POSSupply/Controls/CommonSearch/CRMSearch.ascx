﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CRMSearch.ascx.vb" Inherits="Controls_CommonSearch_CRMSearch" %>
<%--New Codeing Start Here--%>
<asp:Panel runat="server" CssClass="divSectionContent" ID="pnlSearchName" DefaultButton="btnSubmit">
    <div class="searchBar">
        <div class="header">
            <div class="title">
                Search form</div>
            <div class="icon">
                <img src="../lib/image/iconSearch.png" style="width: 17px" alt="" /></div>
        </div>
        <h4>
            <asp:Label ID="lblStatus" runat="server" Text="<%$Resources:Resource, lblStatus %>">            
            </asp:Label>
        </h4>
        <div class="inner">
            <asp:DropDownList ID="ddlStatus" runat="server" Width="180px">
            </asp:DropDownList>
        </div>
        <h4>
            <asp:Label ID="lblSearchCategory" runat="server" Text="<%$Resources:Resource, lblSearchFields %>">            
            </asp:Label></h4>
        <div class="inner">
            <asp:DropDownList ID="ddlSearchBy" runat="server" ValidationGroup="PrdSrch" Width="180px">
            </asp:DropDownList>
        </div>
        <h4>
            <asp:Label ID="lblKeywordSearch" runat="server" Text="<%$Resources:Resource, lblKeyword %>">            
            </asp:Label></h4>
        <div class="inner">
            <asp:TextBox runat="server" ID="txtSearch" Width="180px"></asp:TextBox>
        </div>
        <h4>
            <asp:Label ID="lblCreatedOn" runat="server" Text="<%$Resources:Resource, lblCreatedOn %>">            
            </asp:Label></h4>
        <div class="inner">
            <asp:TextBox runat="server" ID="txtCreatedOnStart" Width="70px" MaxLength="10" autocomplete="off"
                CssClass="datepicker"></asp:TextBox>
            -
            <asp:TextBox runat="server" ID="txtCreatedOnEnd" Width="70px" MaxLength="10" autocomplete="off"
                CssClass="datepicker"></asp:TextBox>
            <asp:CustomValidator ID="cmvCreateDateRange" ControlToValidate="txtCreatedOnStart"
                ErrorMessage="&lt;br&gt;Invalid Date Range!" SetFocusOnError="true" ClientValidationFunction="compareCreatedDateRange"
                Display="Dynamic" ValidationGroup="crmsearch" runat="server" />
        </div>
        <h4>
            <asp:Label ID="Label1" runat="server" Text="<%$Resources:Resource, lblLastModifiedOn %>">            
            </asp:Label>
        </h4>
        <div class="inner">
            <asp:TextBox runat="server" ID="txtModifiedOnStrt" Width="70px" MaxLength="10" autocomplete="off"
                CssClass="datepicker"></asp:TextBox>
            -
            <asp:TextBox runat="server" ID="txtModifiedOnEnd" Width="70px" MaxLength="10" autocomplete="off"
                CssClass="datepicker"></asp:TextBox>
            <asp:CustomValidator ID="CustomValidator1" ControlToValidate="txtModifiedOnStrt"
                ErrorMessage="&lt;br&gt;Invalid Date Range!" SetFocusOnError="true" ClientValidationFunction="compareModifiedDateRange"
                Display="Dynamic" ValidationGroup="crmsearch" runat="server" />
        </div>
        <div class="footer">
            <div class="submit">
                <asp:Button runat="server" Text="Submit" ID="btnSubmit" CausesValidation="false" />
            </div>
        </div>
    </div>
</asp:Panel>
<script type="text/javascript">
    function compareCreatedDateRange(object, args) {
        var op1 = args.Value;
        var op2 = document.getElementById("<%=txtCreatedOnEnd.ClientID %>").value;

        var objDate1 = GetDateObj(op1);
        var objDate2 = GetDateObj(op2);

        if (objDate1 == null) {
            args.IsValid = false;
        }
        else if (objDate2 != null) {
            args.IsValid = (objDate1 <= objDate2);
        }
        else {
            args.IsValid = true;
        }
    }

    function compareModifiedDateRange(object, args) {
        var op1 = args.Value;
        var op2 = document.getElementById("<%=txtModifiedOnEnd.ClientID %>").value;

        var objDate1 = GetDateObj(op1);
        var objDate2 = GetDateObj(op2);

        if (objDate1 == null) {
            args.IsValid = false;
        }
        else if (objDate2 != null) {
            args.IsValid = (objDate1 <= objDate2);
        }
        else {
            args.IsValid = true;
        }
    }

    function customDateValidator(object, args) {
        args.IsValid = IsValidDate(args.Value);
    }

    function CustomValidatorTrim(s) {
        var m = s.match(/^\s*(\S+(\s+\S+)*)\s*$/);
        return (m == null) ? "" : m[1];
    }

    function GetDateObj(op) {
        var dateorder = "mdy";
        function GetFullYear(year) {
            var twoDigitCutoffYear = 2029 % 100;
            var cutoffYearCentury = 2029 - twoDigitCutoffYear;
            return ((year > twoDigitCutoffYear) ? (cutoffYearCentury - 100 + year) : (cutoffYearCentury + year));
        }
        var yearFirstExp = new RegExp("^\\s*((\\d{4})|(\\d{2}))([-/]|\\. ?)(\\d{1,2})\\4(\\d{1,2})\\.?\\s*$");
        m = op.match(yearFirstExp);
        var day, month, year;
        if (m != null && (m[2].length == 4 || dateorder == "ymd")) {
            day = m[6];
            month = m[5];
            year = (m[2].length == 4) ? m[2] : GetFullYear(parseInt(m[3], 10))
        }
        else {
            if (dateorder == "ymd") {
                return null;
            }
            var yearLastExp = new RegExp("^\\s*(\\d{1,2})([-/]|\\. ?)(\\d{1,2})(?:\\s|\\2)((\\d{4})|(\\d{2}))(?:\\s\u0433\\.)?\\s*$");
            m = op.match(yearLastExp);
            if (m == null) {
                return null;
            }
            if (dateorder == "mdy") {
                day = m[3];
                month = m[1];
            }
            else {
                day = m[1];
                month = m[3];
            }
            year = (m[5].length == 4) ? m[5] : GetFullYear(parseInt(m[6], 10))
        }
        month -= 1;
        var date = new Date(year, month, day);
        if (year < 100) {
            date.setFullYear(year);
        }
        return (typeof (date) == "object" && year == date.getFullYear() && month == date.getMonth() && day == date.getDate()) ? date.valueOf() : null;
    }
   
</script>

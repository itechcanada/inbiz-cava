﻿
Partial Class Controls_CommonSearch_CommonKeyword
    Inherits System.Web.UI.UserControl

    Public Event OnSearch As EventHandler


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Property SearchData() As String
        Get
            Return txtSearch.Text
        End Get
        Set(ByVal value As String)
            txtSearch.Text = value
        End Set
    End Property

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        RaiseEvent OnSearch(sender, EventArgs.Empty)
    End Sub
End Class

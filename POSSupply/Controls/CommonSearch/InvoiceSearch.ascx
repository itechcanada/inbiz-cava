﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InvoiceSearch.ascx.vb" Inherits="Controls_CommonSearch_InvoiceSearch" %>


<asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel" DefaultButton="btnSubmit">
     <div class="searchBar">
        <div class="header">
            <div class="title">
                Search form</div>
            <div class="icon">
                <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
        </div>
        <h4>
                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"></asp:Label>
             </h4>
        <div class="inner">
                <asp:DropDownList ID="dlSearch" runat="server" Width="180px" ValidationGroup="PrdSrch">
                </asp:DropDownList>
      </div>
        <h4>
                <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lblStatus %>"></asp:Label>
                </h4>
        <div class="inner">
                <asp:DropDownList ID="dlPOStatus" runat="server" Width="180px">
                </asp:DropDownList>
       </div>
        <h4>
                <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lblReferenceType %>"></asp:Label>
               </h4>
        <div class="inner">
                <asp:DropDownList ID="dlRefType" runat="server" Width="180px" ValidationGroup="PrdSrch">
                    <asp:ListItem Value="" Text="<%$ Resources:Resource, liINSelectRefType %>" />
                    <%--<asp:ListItem Value="CN" Text="<%$ Resources:Resource, liINCreditNote %>"/>--%>
                    <asp:ListItem Value="IV" Text="<%$ Resources:Resource, liINInvoice %>" />
                </asp:DropDownList>
          </div>
        <h4>
                <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword%>"></asp:Label>
               </h4>
        <div class="inner">
                <asp:TextBox runat="server" Width="180px" ID="txtSearch" ValidationGroup="PrdSrch"></asp:TextBox>
       </div>
       
                            
             <div class="footer">
            <div class="submit">
                <asp:Button runat="server" Text="Submit" ID="btnSubmit" ValidationGroup="na" 
                    CausesValidation="False" />
            </div>
        </div>
    </div>
</asp:Panel>

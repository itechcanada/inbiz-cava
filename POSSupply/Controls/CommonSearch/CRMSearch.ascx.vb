﻿
Partial Class Controls_CommonSearch_CRMSearch
    Inherits System.Web.UI.UserControl

    Private clsStatus As New clsSysStatus

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            clsStatus.subGetStatus(ddlStatus, "CU", "dlSch")
            clsStatus.subGetStatus(ddlSearchBy, "CU", "dlSh2")
        End If
    End Sub

 
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim url As String = String.Format("~/Partner/ViewCustomer.aspx?status={0}&sby={1}&sdata={2}&cdtr={3}&mdtr={4}", ddlStatus.SelectedValue, ddlSearchBy.SelectedValue, HttpUtility.UrlEncode(txtSearch.Text), HttpUtility.UrlEncode(txtCreatedOnStart.Text) & "~" & HttpUtility.UrlEncode(txtCreatedOnEnd.Text), HttpUtility.UrlEncode(txtModifiedOnStrt.Text) & "~" & HttpUtility.UrlEncode(txtModifiedOnEnd.Text))
        Response.Redirect(url)
    End Sub
End Class

﻿
Partial Class Controls_CommonSearch_InvoiceSearch
    Inherits System.Web.UI.UserControl

    Private objStatus As New clsSysStatus

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            objStatus.subGetStatus(dlSearch, "IN", "dlSh1", "IN")
            objStatus.subGetStatus(dlPOStatus, "IN", "dlSh2")
        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect(String.Format("~/Invoice/ViewInvoice.aspx?status={0}&sby={1}&sdata={2}&sreft={3}", dlPOStatus.SelectedValue, dlSearch.SelectedValue, txtSearch.Text, dlRefType.SelectedValue))
    End Sub
End Class

﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ReceivingSearch.ascx.vb"
    Inherits="Controls_CommonSearch_ReceivingSearch" %>
<asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel" DefaultButton="btnSubmit">
    <div class="searchBar">
        <div class="header">
            <div class="title">
                Search form</div>
            <div class="icon">
                <img src="../../lib/image/iconSearch.png" style="width: 17px" /></div>
        </div>
        <h4>
            <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, liWarehouseLocation%>"></asp:Label>
        </h4>
        <div class="inner">
            <asp:DropDownList ID="dlWarehouse" runat="server" Width="150px" ValidationGroup="PrdSrch">
            </asp:DropDownList>
        </div>
        <h4>
            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"></asp:Label>
        </h4>
        <div class="inner">
            <asp:DropDownList ID="dlSearch" runat="server" Width="150px" ValidationGroup="PrdSrch">
                <asp:ListItem Text="Search By" Value="" />
                <asp:ListItem Value="PO" Text="PO Number" />
                <asp:ListItem Text="Receipt No" Value="RN" />
                <asp:ListItem Text="Vendor ID" Value="VI" />
                <asp:ListItem Text="UPC Code" Value="UC" />
                <asp:ListItem Text="Today" Value="T" />
                <asp:ListItem Text="This Week" Value="C" />
                <asp:ListItem Text="Next Week" Value="N" />
                <asp:ListItem Text="All" Value="A" />
            </asp:DropDownList>
        </div>
        <h4>
            <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword%>"></asp:Label>
        </h4>
        <div class="inner">
            <asp:TextBox runat="server" Width="150px" ID="txtSearch" ValidationGroup="PrdSrch"></asp:TextBox>
        </div>
        <div class="footer">
            <div class="submit">
                <asp:Button runat="server" Text="Submit" ID="btnSubmit" ValidationGroup="PrdSrch" />
            </div>
        </div>
    </div>
</asp:Panel>

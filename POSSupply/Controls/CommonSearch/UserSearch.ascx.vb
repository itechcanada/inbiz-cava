﻿
Partial Class Controls_CommonSearch_UserSearch
    Inherits System.Web.UI.UserControl

    Private clsStatus As New clsSysStatus

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            clsStatus.subGetStatus(ddlStatus, "US", "dlSea")
        End If
        txtSearch.Focus()
    End Sub

    Protected Sub imgSearch_Click(sender As Object, e As System.EventArgs) Handles imgSearch.Click
        Response.Redirect(String.Format("~/Admin/ViewInternalUser.aspx?status={0}&sdata={1}", ddlStatus.SelectedValue, txtSearch.Text))
    End Sub
End Class

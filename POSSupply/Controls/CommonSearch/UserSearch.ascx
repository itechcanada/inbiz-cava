﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UserSearch.ascx.vb" Inherits="Controls_CommonSearch_UserSearch" %>
<asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel" DefaultButton="imgSearch">
    <div class="searchBar">
        <div class="header">
            <div class="title">
                <%=Resources.Resource.lblSearchOptions%></div>
            <div class="icon">
                <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
        </div>
        <h4>
            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lblStatus %>"></asp:Label></h4>
        <div class="inner">
            <asp:DropDownList ID="ddlStatus" runat="server" Width="175px">
            </asp:DropDownList>
        </div>
        <h4>
            <asp:Label ID="lblSearchKeyword" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
        <div class="inner">
            <asp:TextBox runat="server" Width="170px" ID="txtSearch"></asp:TextBox>
        </div>
        <div class="footer">
            <div class="submit">
                <asp:Button ID="imgSearch" CausesValidation="false" runat="server" Text="Search" />
            </div>
            <br />
        </div>
    </div>
    <br />
</asp:Panel>

﻿
Partial Class Controls_CommonSearch_ReceivingSearch
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            subPopulateWarehouse()

            If Not String.IsNullOrEmpty(Request.QueryString("opt")) Then
                dlSearch.SelectedValue = Request.QueryString("opt")
            End If

            If Not String.IsNullOrEmpty(Request.QueryString("srh")) Then
                txtSearch.Text = Request.QueryString("srh")
            End If

            If Not String.IsNullOrEmpty(Request.QueryString("whs")) Then
                dlWarehouse.SelectedValue = Request.QueryString("whs")
            End If
        End If
    End Sub



    'Populate Warehouse
    Private Sub subPopulateWarehouse()
        Dim objWhs As New clsWarehouses
        objWhs.PopulateWarehouse(dlWarehouse)
        dlWarehouse.Items.RemoveAt(0)
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = Resources.Resource.liWarehouseLocation
        dlWarehouse.Items.Insert(0, itm)
        Dim itm1 As New ListItem
        itm1.Value = "ALL"
        itm1.Text = Resources.Resource.liAllWarehouse
        dlWarehouse.Items.Insert(1, itm1)
        objWhs = Nothing
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect(String.Format("~/Receiving/ViewReceiving.aspx?opt={0}&srh={1}&whs={2}", dlSearch.SelectedValue, clsCommon.funRemove(txtSearch.Text), dlWarehouse.SelectedValue))
    End Sub
End Class

﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InventorySearch.ascx.vb"
    Inherits="Controls_CommonSearch_InventorySearch" %>
<asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel" DefaultButton="btnSubmit">
    <div class="searchBar">
        <div class="header">
            <div class="title">
                Search form</div>
            <div class="icon">
                <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
        </div>
        <h4>
            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"></asp:Label>
        </h4>
        <div class="inner">
            <asp:DropDownList ID="dlSearch" runat="server" ValidationGroup="PrdSrch" Width="150px">
            </asp:DropDownList>
        </div>
        <h4>
            <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword%>"></asp:Label></h4>
        <div class="inner">
            <asp:TextBox ID="txtSearch" runat="server" CssClass="innerText" Width="170px"></asp:TextBox>
        </div>
        <div class="footer">
            <div class="submit">
                <asp:Button runat="server" Text="Submit" ID="btnSubmit" />
            </div>
        </div>
    </div>
</asp:Panel>

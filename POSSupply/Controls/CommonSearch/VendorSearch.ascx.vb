﻿
Partial Class Controls_CommonSearch_VendorSearch
    Inherits System.Web.UI.UserControl
    Private clsStatus As New clsSysStatus

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            clsStatus.subGetStatus(ddlvendorStatus, "VN", "dlSea")
        End If
    End Sub

  
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect(String.Format("~/Admin/ViewVendor.aspx?status={0}&sdata={1}", ddlvendorStatus.SelectedValue, txtvendorSearch.Text))
    End Sub
End Class

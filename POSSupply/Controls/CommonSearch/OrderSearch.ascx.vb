﻿
Partial Class Controls_CommonSearch_OrderSearch
    Inherits System.Web.UI.UserControl
    Private clsStatus As New clsSysStatus

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            clsStatus.subGetStatus(dlPOStatus, "SO", "SOSts")
            clsStatus.subGetStatus(dlSearch, "SO", "dlSh1", "NO")
        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        Response.Redirect(String.Format("~/Sales/ViewSalesOrder.aspx?status={0}&sby={1}&sdata={2}", dlPOStatus.SelectedValue, dlSearch.SelectedValue, txtSearch.Text))
    End Sub
End Class

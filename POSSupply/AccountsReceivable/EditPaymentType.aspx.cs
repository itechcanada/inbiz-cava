﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;
public partial class AccountsReceivable_EditPaymentType : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security check for authorization
        if (!CurrentUser.IsInRole(RoleID.ACCOUNT))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            DropDownHelper.FillDropdown(dbHelp, dlRcvdVia, "AR", "dlRcv", Globals.CurrentAppLanguageCode, new ListItem(""));
            if (!string.IsNullOrEmpty(this.AccountRcbVia))
            {
                //dlRcvdVia.Items.Remove("Refund");

                ListItem li = dlRcvdVia.Items.FindByValue(this.AccountRcbVia);
                dlRcvdVia.Items.Remove(li);

                // li = dlRcvdVia.Items.FindByValue(BusinessUtility.GetString(""));
                //dlRcvdVia.Items.Remove(li);
            }
            dlRcvdVia.Focus();
        }
    }

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                if(this.AccountRcbID>0)
                {
                AccountReceivable acrcb = new AccountReceivable();
                acrcb.PopulateObject(null, this.AccountRcbID);
                acrcb.InsertHistory(this.AccountRcbID, CurrentUser.UserID, BusinessUtility.GetInt(dlRcvdVia.SelectedValue));
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                }
                else
                {
                    Globals.RegisterCloseDialogScript(Page);
                }   

                }
            }
            catch
            {
                
            }
        }
    }

    private int AccountRcbID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["AccountRcbID"]);
        }
    }
    private string AccountRcbVia
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["AccountRcbVia"]);
        }
    }
    

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }
    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
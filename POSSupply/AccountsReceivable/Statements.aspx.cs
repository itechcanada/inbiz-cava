﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AccountsReceivable_Statements : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PreparePrintHtml();
        }
    }

    private void PreparePrintHtml()
    {
        ltPrint.Text = CommonPrint.GetStatementPrintHtml(this, this.CompanyID, this.DaysRange, this.CustomerID);
    }

    private int CompanyID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["companyid"], out id);
            return id;
        }
    }

    private string DaysRange
    {
        get {
            return Request.QueryString["days"];
        }
    }

    private int CustomerID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["custID"], out id);
            return id;
        }
    }
}
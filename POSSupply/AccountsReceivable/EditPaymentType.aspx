﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="EditPaymentType.aspx.cs" Inherits="AccountsReceivable_EditPaymentType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="contentBottom" style="padding:5px; height:100px; overflow:auto;">
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
        <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0">
            
            <tr>
                <td class="text">
                     <asp:Label ID="lblARAmtRcvdVia" CssClass="lblBold" Text="<%$ Resources:Resource, lblARAmtRcvdVia%>"
                        runat="server" />
                </td>
                <td class="input">
                     <asp:DropDownList ID="dlRcvdVia" runat="server">
                    </asp:DropDownList>
                    <asp:CustomValidator ID="custvalRcvdVia" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, msgARPlzSelAmtRcvdVia %>"
                        ClientValidationFunction="funCheckAmountReceivedVia" Display="None">
                    </asp:CustomValidator>
                </td>
            </tr>
            
        </table>        
    </div>
    
        <br />
        <div class="div-dialog-command">
            <asp:Button Text="<%$Resources:Resource, btnSave %>"  ID="btnSave" runat="server" OnClick="btnSave_Click" />
            <asp:Button Text="<%$Resources:Resource, btnCancel %>"  ID="btnCancel" runat="server" CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
            <div class="clear">
            </div>
        </div>
        <asp:ValidationSummary ID="valsAdminWarehouse" runat="server" ShowMessageBox="true"
            ShowSummary="false" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        function funCheckAmountReceivedVia(source, args) {
            if (document.getElementById('<%=dlRcvdVia.ClientID%>').selectedIndex == 0) {
                document.getElementById('<%=dlRcvdVia.ClientID%>').focus();
                args.IsValid = false;
            }
        }
        </script>
</asp:Content>



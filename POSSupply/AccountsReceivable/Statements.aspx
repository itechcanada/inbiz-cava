﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Statements.aspx.cs" Inherits="AccountsReceivable_Statements" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <script type="text/javascript">
         var PrintCommandObject = null;

         function printPage() {
             if (PrintCommandObject) {

                 try {
                     PrintCommandObject.ExecWB(6, 2);
                     PrintCommandObject.outerHTML = "";

                 }
                 catch (e) {
                     window.print();
                 }
             }
             else {
                 window.print();
             }
             var browserName = navigator.appName;
             if (navigator.appName != "Microsoft Internet Explorer") {
                 window.close();
             }
             else {
                 window.close();
             }
         }

         window.onload = function () {
             Minimize();
             if (navigator.appName == "Microsoft Internet Explorer") {
                 // attach and initialize print command ActiveX object
                 try {
                     var PrintCommand = '<object id="PrintCommandObject" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2" width="0" height="0"></object>';
                     document.body.insertAdjacentHTML('beforeEnd', PrintCommand);
                 }
                 catch (e) { }
             }
             setTimeout(printPage, 1);
         };


         function Minimize() {
             window.innerWidth = 10;
             window.innerHeight = 10;
             window.screenX = screen.width;
             window.screenY = screen.height;
             alwaysLowered = true;

         }

    </script>
    <style type="text/css">
        body
        {
            font-size: 12px;
        }
        table
        {
            border-collapse: collapse;
        }
        td.border,th.border
        {
            border:1px solid #000;
        }
    </style>   
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Literal ID="ltPrint" Text="" runat="server" />
    </div>
    </form>
</body>
</html>

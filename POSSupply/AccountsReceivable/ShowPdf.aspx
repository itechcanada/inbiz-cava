<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowPdf.aspx.vb" Inherits="AR_Show" %>

<%@ Import Namespace="Resources.Resource" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <% =prjCompanyTitle%></title>
    <link href="../Css/style.css" rel="stylesheet" type="text/css" />
    <script language="javascript">

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="MasterScriptManager" runat="Server">
    </asp:ScriptManager>
    <div align="center">
        <table width="750" class="table">
            <tr>
                <td class="titleBgColor" height="20" align="center" colspan="2">
                    <asp:Label CssClass="lblBold" runat="server" ID="lblTitle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="5" colspan="2">
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:UpdatePanel ID="upnlMsg" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <input type="text" size="350" width='250' />
                </td>
            </tr>
            <tr height="30">
                <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                    <%--<asp:Button runat="server" ID="cmdSave" CssClass="imgNext" />&nbsp;&nbsp;
            <asp:Button runat="server" ID="cmdReset" CssClass="imgReset" 
            CausesValidation="false" Visible="false" />--%>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsSales" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
    <br />
    <br />
    </form>
</body>
</html>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="InvoicePayment.aspx.cs" Inherits="AccountsReceivable_InvoicePayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <link href="../lib/scripts/collapsible-panel/collapsible_salesedit.css" rel="stylesheet" type="text/css" />
    <script src="../lib/scripts/collapsible-panel/jquery.collapsiblepanel.js" type="text/javascript"></script>
    <style type="text/css">
        .dialog li{line-height:30px;}
        table.readonlye_info{margin-bottom:5px;}
        table.readonlye_info td{ padding:2px 5px;}
        table.readonlye_info td.bold{font-weight:bold;}                                      
    </style>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img alt="" src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <asp:Literal runat="server" ID="lblTitle"></asp:Literal>
        </b>
    </div>
    <div>
        <%--<asp:Button ID="btnCustomerNotes" Text="<%$Resources:Resource,lblCustomerNotes %>"
            runat="server" />--%>            
        <asp:Button ID="btnReceive" Text="<%$Resources:Resource,lblReceive %>" runat="server" CausesValidation="false" />
        <iCtrl:IframeDialog ID="mdReceive" TriggerControlID="btnReceive" Width="750" Height="460"
                    Url="mdReceive.aspx" Title="<%$Resources:Resource, lblReceive %>"
                    runat="server"></iCtrl:IframeDialog>
        <asp:Button ID="btnRefund" Text="<%$Resources:Resource,lblRefund %>" runat="server"  CausesValidation="false"/>
        <iCtrl:IframeDialog ID="mdRefund" TriggerControlID="btnRefund" Width="750" Height="460"
                    Url="mdReceive.aspx" Title="<%$Resources:Resource, lblRefund %>"
                    runat="server"></iCtrl:IframeDialog>
        <%-- <asp:Button ID="btnEvoRefund" Text="<%$Resources:Resource,lblEvoRefund %>" runat="server"  CausesValidation="false"/>
        <iCtrl:IframeDialog ID="mdEvoRefund" TriggerControlID="btnEvoRefund" Width="750" Height="460"
                    Url="mdReceive.aspx" Title="<%$Resources:Resource, lblEvoRefund %>"
                    runat="server"></iCtrl:IframeDialog>--%>
        <asp:Button ID="btnWriteOff" Text="<%$Resources:Resource,lblWriteOff %>" runat="server"  CausesValidation="false" Visible="false"/>
        <iCtrl:IframeDialog ID="mdWriteOff" TriggerControlID="btnWriteOff" Width="750" Height="460"
                    Url="mdReceive.aspx" Title="<%$Resources:Resource, lblWriteOff %>"
                    runat="server"></iCtrl:IframeDialog>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphLeft" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div class="panel">
        <h3>
            <asp:Label ID="lblTitle2" Text="Account Receivable" runat="server" />
        </h3>
        <div class="panelcontent">
            <table class="readonlye_info" border="0" cellpadding="0" cellspacing="0">
                <%--<tr>
                    <td class="bold"></td>
                    <td></td>
                    <td></td>
                    <td class="bold"></td>
                    <td></td>
                    <td></td>
                    <td class="bold"></td>
                    <td></td>                    
                </tr>--%>
                <tr>
                    <td class="bold">
                        <asp:Label ID="lblSODate1" Text="<%$ Resources:Resource, lblINInvoiceDate %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblSODate" CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblInvNumber" Text="<%$ Resources:Resource, lblINInvoiceNo %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblInvID" CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblSONumber" Text="<%$ Resources:Resource, lblSOOrderNo %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblSOID" CssClass="lblBold" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="bold">
                        <asp:Label ID="lblCustomerName1" Text="<%$ Resources:Resource, lblSOCustomerName %>"
                            CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblCustomerName" CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblAREmail" CssClass="lblBold" Text="<%$ Resources:Resource, lblEmail%>"
                            runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblEmail" runat="server" CssClass="lblBold">
                        </asp:Label>
                    </td>
                    <td>
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblARPhone" CssClass="lblBold" Text="<%$ Resources:Resource, lblPhone%>"
                            runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblPhone" runat="server" CssClass="lblBold">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="bold" valign="top">
                        
                        <asp:Label ID="Label1" Text="<%$ Resources:Resource, POAddress %>" CssClass="lblBold"
                            runat="server" />
                        
                    </td>
                    <td valign="top">
                        <asp:Label ID="lblAddress" CssClass="lblBold" runat="server" />
                    </td>
                    <td valign="top">
                    </td>
                    <td class="bold" valign="top">
                        <asp:Label ID="lblARFax" Text="<%$ Resources:Resource, lblFax %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td valign="top">
                        <asp:Label ID="lblFax" CssClass="lblBold" runat="server" />
                    </td>
                    <td valign="top">
                    </td>
                    <td class="bold" valign="top">
                        <asp:Label ID="lblARColAsigned" CssClass="lblBold" Text="<%$ Resources:Resource, lblARColAsigned%>"
                            runat="server" />
                    </td>
                    <td valign="top">
                        <asp:Label ID="lblAssignedTo" CssClass="lblBold" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="bold">
                        <asp:Label ID="lblARInvoiceAmount" Text="<%$ Resources:Resource, lblARInvoiceAmount %>"
                            CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblInvoiceAmount" CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblARPreviousReceived" Text="<%$ Resources:Resource, lblARPreviousReceived %>"
                            CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblPreviousReceived" CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblARRefundAmout" Text="<%$ Resources:Resource, lblAmountRefund %>"
                            CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblRefundAmount" CssClass="lblBold" runat="server"  />
                    </td>
                </tr>
                <tr>
                    <td class="bold">
                        <asp:Label ID="lblARBalanceAmount" Text="<%$ Resources:Resource, lblARBalanceAmount %>"
                            CssClass="lblBold" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblBalanceAmount" CssClass="lblBold" runat="server" BackColor="#FFFF00" />
                    </td>
                    <td>
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblARPreviousWriteOff" Text="<%$ Resources:Resource, lblARPreviousWriteOff %>"
                            CssClass="lblBold" runat="server" Visible="false"    />
                    </td>
                    <td>
                        <asp:Label ID="lblPreviousWriteOff" CssClass="lblBold" runat="server" Visible="false" />
                    </td>

                    
                    <td>
                    </td>
                    <td class="bold">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>                        
            <div class="div_command" style="padding-right: 0px;">
                <asp:Button ID="btnSave" Text="<%$Resources:Resource, cmdCssSave%>" runat="server"
                    OnClick="btnSave_Click" Visible="False" />
                <asp:Button ID="btnSaveAndGoToCal" Text="Save & Go To Calendar" runat="server" OnClick="btnSaveAndGoToCal_Click" Visible="false" />
                <asp:Button ID="btnBack" Text="<%$Resources:Resource, cmdCssBack%>" runat="server"
                    OnClick="btnBack_Click" CausesValidation="false" />
            </div>
            
        </div>
    </div>
    <div id="grid_wrapper" style="width: 100%;">
        <trirand:JQGrid runat="server" ID="grdRcv" DataSourceID="sdsArc" Height="300px" AutoWidth="True"
            OnCellBinding="grdRcv_CellBinding" OnDataRequesting="grdRcv_DataRequesting">
            <Columns>
                <%--<trirand:JQGridColumn DataField="AccountReceivableID" HeaderText="S.No."
                        Editable="false" Width="10" />     --%>
                <trirand:JQGridColumn DataField="ARNote" HeaderText="<%$ Resources:Resource, grdARAccountReceivableHistory %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="ARReceiptNo" HeaderText="<%$ Resources:Resource, lblReceiptNo %>"
                    Editable="false" />
                 <trirand:JQGridColumn DataField="AccountReceivableID" HeaderText="<%$ Resources:Resource, msgTransactionID %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="Mode" HeaderText="<%$ Resources:Resource, lblARAmtRcvdVia %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="AccountReceivableID" HeaderText="<%$ Resources:Resource, grdEdit %>"
                    Editable="false" Width="40" Visible="false"/>
                <trirand:JQGridColumn DataField="AccountReceivableID" HeaderText="<%$ Resources:Resource, lblHistory %>"
                    Editable="false" Width="40" Visible="true"/>
                <trirand:JQGridColumn DataField="ARAmtRcvdDateTime" HeaderText="<%$ Resources:Resource, grdARAmountReceivedDate %>"
                    Editable="false" TextAlign="Center" Width="80" />
                <trirand:JQGridColumn DataField="InvoiceTotal" HeaderText="<%$ Resources:Resource, grdARInvoiceAmount %>"
                    Editable="false"  TextAlign="Right" Width="80" />
                <trirand:JQGridColumn DataField="ARAmtRcvd" HeaderText="<%$ Resources:Resource, grdARAmountReceived %>"
                    Editable="false"  TextAlign="Right" Width="80" />
                <trirand:JQGridColumn DataField="ARWriteOff" HeaderText="<%$ Resources:Resource, grdARWriteOffAmount %>"
                    Editable="false"  TextAlign="Right" Width="80" Visible="false"/>
                <trirand:JQGridColumn DataField="AccountReceivableID" HeaderText="<%$ Resources:Resource, grdARDetail %>"
                    Editable="false" Width="70" Visible="false" />
                <trirand:JQGridColumn DataField="AccountReceivableID" HeaderText="<%$ Resources:Resource, grdDelete %>"
                    Editable="false" Width="70" Visible="false" />
                <trirand:JQGridColumn DataField="AccountReceivableID" Visible="false" PrimaryKey="True" />
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <ClientSideEvents LoadComplete="resize_the_grid" />
        </trirand:JQGrid>
        <iCtrl:IframeDialog ID="mdEditPaymentType" Height="250" Width="490" Title="<%$ Resources:Resource, lblEditAmountReceivedVia %>"
                    Dragable="true" TriggerSelectorClass="edit-PaymentType" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
                    runat="server">
                </iCtrl:IframeDialog>
        <asp:SqlDataSource ID="sdsArc" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
    </div>
    <asp:ValidationSummary ID="valsSales" runat="server" ShowMessageBox="true" ShowSummary="false" />
    <asp:HiddenField ID="hdnPartnerID" runat="server" />
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdRcv.ClientID %>";

        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }
        
        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();
        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }
       
    </script>
    <script type="text/javascript" language="javascript">

        function funConfirm(id) {
            var frm = document.forms[0];
            for (i = 0; i < frm.elements.length; i++) {
                if (frm.elements[i].type == "checkbox") {
                    if (frm.elements[i].checked == false) {
                        alert('<% =Resources.Resource.POConfirmItems %>');
                        return false;
                    }
                }
            }
            return true;
        }
        
        function openPDF(strOpen) {
            open(strOpen, "", 'height=365,width=810,left=180,top=150,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');
            return false;
        }
        function funClosePopUp(strOption) {
            document.getElementById(strOption).style.display = "none";
        }
        function popUp(strOption) {
            document.getElementById(strOption).style.display = "block";
            return false;
        }
        function popAlert(strOption) {
            alert(strOption);
            return false;
        }

        function ShowAccountHistory(accRcbID) {

            //Account Receivable  History Popup  href=""mdAccountHistory.aspx?AccountRcbID={0}&jscallback={1}""
            $this = $(this);
            var pageTitle = "<%=Resources.Resource.grdARAccountReceivableHistory%>";
                var $dialog = jQuery.FrameDialog.create({
                    url: "mdAccountHistory.aspx?AccountRcbID=" + accRcbID ,
                    title: pageTitle,
                    loadingClass: "loading-image",
                    modal: true,
                    width: "800",
                    height: "350",
                    autoOpen: false
                });
                $dialog.dialog('open'); return false;

                return false;
        }
    </script>
</asp:Content>

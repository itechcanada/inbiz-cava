﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Trirand.Web.UI.WebControls;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.PaymentProvider;
using iTECH.Library.DataAccess.MySql;
using System.Data;

public partial class AccountsReceivable_mdReceive : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack) { return; }
        DbHelper dbHelp = new DbHelper(true);

        try
        {
            if (BusinessUtility.GetString(Request.QueryString["rmvSession"]).ToLower() == "true".ToLower())
            {
                liReason.Visible = false;
                chkPrintReceipt.Visible = false;
                chkPrintReceipt.Checked = false;
                Session["RETURN_CART"] = null;
            }


            dbHelp.OpenDatabaseConnection();
            Orders ord = new Orders();
            OrderTransaction ordTrans = new OrderTransaction();
            TotalSummary total;

            double amtToRefund = 0.00D, OutstandingAmount = 0.00D;
            double.TryParse(Request.QueryString["amtToRefund"], out amtToRefund);
            //txtRefund.Text = string.Format("{0:F}", amtToRefund);
            txtRefund.Text = CurrencyFormat.GetAmountInUSCulture(amtToRefund);


            DropDownHelper.FillDropdown(dbHelp, dlRcvdVia, "AR", "dlRcv", Globals.CurrentAppLanguageCode, new ListItem(""));
            DropDownHelper.FillDropdown(dbHelp, rblRefundType, "SO", "dlRef", Globals.CurrentAppLanguageCode, null);
            DropDownHelper.FillDropdown(dbHelp, ddlReason, "AR", "dlRet", Globals.CurrentAppLanguageCode, null);
            if (rblRefundType.Items.Count > 0)
            {
                rblRefundType.Items[0].Selected = true;

                foreach (ListItem item in rblRefundType.Items)
                {
                    if (!string.IsNullOrEmpty(item.Value))
                    {
                        if (item.Value.ToUpper() == "C".ToUpper())
                        {
                            item.Text = Resources.Resource.lblInstoreCreditExchange; 
                            //lstItemsToRemove.Add(item);
                        }
                    }
                }
            }


            this.FillYear();
            this.FillMonth();

            switch (this.ReceivingType)
            {
                case "refund":
                    lblTitle.Text = Resources.Resource.lblRefund;
                    if (IsReturn)
                    {
                        lblARAmtRcvdVia.Text = Resources.Resource.lblAmountRefundedVia;
                    }
                    else
                    {
                        lblARAmtRcvdVia.Text = Resources.Resource.lblARAmtRcvdVia;
                    }
                    liRefund0.Visible = true;
                    liRefund1.Visible = true;
                    liRefund2.Visible = true;
                    liRefund3.Visible = true;
                    liRefund4.Visible = true;
                    liRefund5.Visible = true;
                    liWriteOff1.Visible = false;
                    liReceive1.Visible = false;
                    liReceive2.Visible = false;
                    liReceive3.Visible = false;
                    liReceive4.Visible = false;
                    liReceive5.Visible = true;
                    List<ListItem> lstItemsToRemove = new List<ListItem>();
                    foreach (ListItem item in dlRcvdVia.Items)
                    {
                        if (!string.IsNullOrEmpty(item.Value) && item.Value != ((int)StatusAmountReceivedVia.Refund).ToString() && item.Value != ((int)StatusAmountReceivedVia.Credit).ToString())
                        {
                            lstItemsToRemove.Add(item);
                        }
                    }
                    //foreach (var item in lstItemsToRemove)
                    //{
                    //    dlRcvdVia.Items.Remove(item);
                    //}
                    btnRefund1.Visible = true;
                    btnWriteOff.Visible = false;
                    btnReceivePayment.Visible = false;
                    break;
                case "writeoff":
                    liReason.Visible = false;
                    chkPrintReceipt.Visible = false;
                    lblTitle.Text = Resources.Resource.lblWriteOff;
                    liRefund0.Visible = false;
                    liRefund1.Visible = false;
                    liRefund2.Visible = false;
                    liRefund3.Visible = false;
                    liRefund4.Visible = false;
                    liRefund5.Visible = false;
                    liWriteOff1.Visible = true;
                    liReceive1.Visible = false;
                    liReceive2.Visible = false;
                    liReceive3.Visible = false;
                    liReceive4.Visible = false;
                    liReceive5.Visible = false;

                    btnRefund1.Visible = false;
                    btnWriteOff.Visible = true && this.IsOnInvoice;
                    btnReceivePayment.Visible = false;
                    break;
                default:
                    lblTitle.Text = Resources.Resource.lblReceivePayment;
                    liRefund0.Visible = false;
                    liRefund1.Visible = false;
                    liRefund2.Visible = false;
                    liRefund3.Visible = false;
                    liRefund4.Visible = false;
                    liRefund5.Visible = false;
                    liWriteOff1.Visible = false;
                    liReceive1.Visible = true;
                    liReceive2.Visible = true;
                    liReceive3.Visible = true;
                    liReceive4.Visible = true;
                    liReceive5.Visible = true;

                    btnRefund1.Visible = false;
                    liReason.Visible = false;

                    btnWriteOff.Visible = false;
                    btnReceivePayment.Visible = true;
                    break;
            }

            txtARAmtRcvdDateTime.Text = BusinessUtility.GetDateTimeString(DateTime.Now, DateFormat.MMddyyyy);
            int orderID = 0;
            if (this.IsOnInvoice)
            {
                total = CalculationHelper.GetInvoiceTotal(dbHelp, this.InvoiceID);
                Invoice inv = new Invoice();
                AccountReceivable arc = new AccountReceivable();
                inv.PopulateObject(dbHelp, this.InvoiceID);
                orderID = inv.InvForOrderNo;

                txtTransactionID.Text = ordTrans.GetSaleTransactionID(dbHelp, inv.InvForOrderNo);

                double amt = 0.0, amountToWriteOff = 0.0;
                InvoiceReturn invReturn = new InvoiceReturn();
                var lstToReturn = Session["RETURN_CART"] as List<ReturnCart>;
                var lstProcess = Session["RETURN_PROCESS_CART"] as List<ReturnProcessCart>;
                if (lstToReturn != null)
                {
                    foreach (var item in lstToReturn)
                    {
                        if (item.ProductToReturnQty > 0)
                        {
                            //Calculate writeoff
                            amt = invReturn.GetInvReturnWriteOff(dbHelp, item.InvoiceItemID, item.ProductToReturnQty);
                            amountToWriteOff = CalculationHelper.GetAmount(amountToWriteOff + amt);
                        }
                    }
                }
                if (lstProcess != null)
                {
                    foreach (var item in lstProcess)
                    {
                        if (item.AmountToReturn > 0)
                        {
                            amountToWriteOff = CalculationHelper.GetAmount(amountToWriteOff + item.AmountToReturn);
                        }
                    }
                }

                OutstandingAmount = CalculationHelper.GetAmount((total.GrandTotal - amountToWriteOff - total.TotalAmountReceived) - total.ReturnedOverPaidAmount);
                ltBalance.Text = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(OutstandingAmount), inv.InvCurrencyCode);// string.Format("{0:F}", BusinessUtility.GetCurrencyString(total.OutstandingAmount, Globals.CurrentCultureName));
                ltInvoiceTotal.Text = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(total.GrandTotal - amountToWriteOff), inv.InvCurrencyCode);// string.Format("{0:F}", BusinessUtility.GetCurrencyString(total.GrandTotal, Globals.CurrentCultureName));
                ltReceivedAmount.Text = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(total.TotalAmountReceived), inv.InvCurrencyCode);// string.Format("{0:F}", BusinessUtility.GetCurrencyString(total.TotalAmountReceived, Globals.CurrentCultureName));
                ltAdminFee.Text = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(total.TotalAdminFeeApplied), inv.InvCurrencyCode);// string.Format("{0:F}", BusinessUtility.GetCurrencyString(total.TotalAdminFeeApplied, Globals.CurrentCultureName));
                lblCur.Text = total.CurrencyCode;
                double writeOff = arc.GetWriteOffAmount(inv.InvID);
                txtARAmtRcvd.Text = CurrencyFormat.GetAmountInUSCulture(OutstandingAmount);
                string assingTo = arc.AssignTo(inv.InvID);
                if (!string.IsNullOrEmpty(assingTo))
                {
                    lblAssignedTo.Text = string.Format("[ {0} ]", assingTo);
                    rblstColAsigned.SelectedValue = "1";
                }
                else
                {
                    lblAssignedTo.Text = "";
                    rblstColAsigned.SelectedValue = "0";
                }

                if (this.ReceivingType == "receive")
                {
                    //Set customer Available Credit
                    CustomerCredit cr = new CustomerCredit();
                    var lst = cr.GetCustomerCreditByOrder(dbHelp, inv.InvForOrderNo);
                    lstApplyCredit.Items.Clear();
                    var totalCredit = 0.0D;
                    foreach (var item in lst)
                    {
                        totalCredit += item.CreditAmount;
                        lstApplyCredit.Items.Add(new ListItem(string.Format("{0} {1}{2:F}", item.CustomerName, total.CurrencyCode, item.CreditAmount), item.CustomerID.ToString()));
                    }
                    //valAvailCredit.Text = string.Format("{0} {1:F}", total.CurrencyCode, BusinessUtility.GetCurrencyString(totalCredit, Globals.CurrentCultureName));
                    valAvailCredit.Text = CurrencyFormat.GetCompanyCurrencyFormat(totalCredit, total.CurrencyCode);
                }

                AccountReceivable objAccountReceivable = new AccountReceivable();
                DataTable dtAcountReceivable = new DataTable();
                DataRow[] result = objAccountReceivable.GetInvoicePartialPaymentList(BusinessUtility.GetInt(inv.InvID), Globals.CurrentAppLanguageCode).Select("ARAmtRcvd>0");
                foreach (DataRow dRow in result)
                {

                    if (ltrAmmoutDetail.Text == "")
                    {
                        ltrAmmoutDetail.Text = BusinessUtility.GetString(dRow["Mode"]) + " (" + string.Format("{0:F}", Convert.ToDouble(dRow["ARAmtRcvd"].ToString())) + ") ";
                    }
                    else
                    {
                        ltrAmmoutDetail.Text += ", " + BusinessUtility.GetString(dRow["Mode"]) + " (" + string.Format("{0:F}", Convert.ToDouble(dRow["ARAmtRcvd"].ToString())) + ") ";
                    }
                }
            }
            else
            {
                int.TryParse(Request.QueryString["oid"], out orderID);
                total = CalculationHelper.GetOrderTotal(dbHelp, orderID);

                txtTransactionID.Text = ordTrans.GetSaleTransactionID(dbHelp, orderID);

                ord.PopulateObject(orderID);

                double amt = 0.0, amountToWriteOff = 0.0;
                OrderReturn ordReturn = new OrderReturn();
                var lstToReturn = Session["RETURN_CART"] as List<ReturnCart>;
                var lstProcess = Session["RETURN_PROCESS_CART"] as List<ReturnProcessCart>;
                if (lstToReturn != null)
                {
                    foreach (var item in lstToReturn)
                    {
                        if (item.ProductToReturnQty > 0)
                        {
                            //Calculate writeoff
                            amt = ordReturn.GetOrderReturnWriteOff(dbHelp, item.OrderItemID, item.ProductToReturnQty);
                            amountToWriteOff = CalculationHelper.GetAmount(amountToWriteOff + amt);
                        }
                    }
                }
                if (lstProcess != null)
                {
                    foreach (var item in lstProcess)
                    {
                        if (item.AmountToReturn > 0)
                        {
                            amountToWriteOff = CalculationHelper.GetAmount(amountToWriteOff + item.AmountToReturn);
                        }
                    }
                }
                OutstandingAmount = CalculationHelper.GetAmount((total.GrandTotal - amountToWriteOff - total.TotalAmountReceived) - total.ReturnedOverPaidAmount);
                ltBalance.Text = CurrencyFormat.GetCompanyCurrencyFormat(OutstandingAmount, ord.OrdCurrencyCode);
                ltInvoiceTotal.Text = CurrencyFormat.GetCompanyCurrencyFormat(total.GrandTotal - amountToWriteOff, ord.OrdCurrencyCode);
                ltReceivedAmount.Text = CurrencyFormat.GetCompanyCurrencyFormat(total.TotalAmountReceived, ord.OrdCurrencyCode);
                ltAdminFee.Text = CurrencyFormat.GetCompanyCurrencyFormat(total.TotalAdminFeeApplied, ord.OrdCurrencyCode);
                txtARAmtRcvd.Text = CurrencyFormat.GetAmountInUSCulture(OutstandingAmount);
                lblCur.Text = total.CurrencyCode;
                string assingTo = string.Empty;
                if (!string.IsNullOrEmpty(assingTo))
                {
                    lblAssignedTo.Text = string.Format("[ {0} ]", assingTo);
                    rblstColAsigned.SelectedValue = "1";
                }
                else
                {
                    lblAssignedTo.Text = "";
                    rblstColAsigned.SelectedValue = "0";
                }

                if (this.ReceivingType == "receive")
                {
                    //Set customer Available Credit
                    CustomerCredit cr = new CustomerCredit();
                    var lst = cr.GetCustomerCreditByOrder(dbHelp, orderID);
                    lstApplyCredit.Items.Clear();
                    var totalCredit = 0.0D;
                    foreach (var item in lst)
                    {
                        totalCredit += item.CreditAmount;
                        lstApplyCredit.Items.Add(new ListItem(string.Format("{0} {1}{2:F}", item.CustomerName, total.CurrencyCode, item.CreditAmount), item.CustomerID.ToString()));
                    }
                    //valAvailCredit.Text = string.Format("{0} {1:F}", total.CurrencyCode, BusinessUtility.GetCurrencyString(totalCredit, Globals.CurrentCultureName));
                    valAvailCredit.Text = CurrencyFormat.GetCompanyCurrencyFormat(totalCredit, total.CurrencyCode);
                }
            }

            if (txtTransactionID.Text == "")
            {
                ListItem listItem = rblRefundType.Items[1];
                if (listItem != null)
                {
                    rblRefundType.Items.Remove(listItem);
                }

            }

            if (amtToRefund <= 0 && OutstandingAmount < 0)
            {
                txtRefund.Text = string.Format("{0:F}", OutstandingAmount * (-1.00D));
            }
            double adminFee = 0.00D;
            double.TryParse(Request.QueryString["adminFee"], out adminFee);
            txtAdminFee.Text = CurrencyFormat.GetAmountInUSCulture(adminFee);

            //Fill customer dropdown list make refund for selected guest
            ddlCustomers.DataSource = ord.GetOrderGuests(dbHelp, orderID);
            ddlCustomers.DataValueField = "GuestID";
            ddlCustomers.DataTextField = "PartnerLongName";
            ddlCustomers.DataBind();
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    protected void btnRefund_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                //Verify security password if not matched, do not allow to make refund
                if (txtVerificationPassword.Text != AppConfiguration.AppSettings[AppSettingKey.VerificationPassword])
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, "Invalid security password!");
                    return;
                }

                //Check to perform on invoice or on order level
                if (this.IsOnInvoice)
                {
                    this.Refund(dbHelp);
                }
                else
                {
                    this.RefundOnOrderLevel(dbHelp);
                }
            }
            catch
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnReceivePayment_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                //Check to perform on invoice or on order level
                if (this.IsOnInvoice)
                {
                    this.Receive(dbHelp);
                }
                else
                {
                    this.ReceiveOnOrderLevel(dbHelp);
                }
            }
            catch
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnWriteOff_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                if (this.IsOnInvoice)
                {
                    this.WriteOff(dbHelp);
                }
            }
            catch
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    private void Receive(DbHelper dbHelp)
    {
        TotalSummary total = CalculationHelper.GetInvoiceTotal(dbHelp, this.InvoiceID);
        double receveingAmt = BusinessUtility.GetDouble(txtARAmtRcvd.Text);

        Invoice inv = new Invoice();
        inv.PopulateObject(dbHelp, this.InvoiceID);

        CustomerCredit cr = new CustomerCredit();
        var lst = cr.GetCustomerCreditByOrder(dbHelp, inv.InvForOrderNo);

        Dictionary<int, double> dicDeducttLstFromCredit = new Dictionary<int, double>();//<CustomerID, NegetiveCreditAmt>
        Dictionary<int, double> dicDepositListToRcv = new Dictionary<int, double>();//<CustomerID, PossitivePaidAmount>
        double totalDepositByCredit = 0.0D;
        double outstanding = total.OutstandingAmount;
        foreach (var item in lst)
        {
            if (totalDepositByCredit == receveingAmt)
            {
                break;
            }

            ListItem li = lstApplyCredit.Items.FindByValue(item.CustomerID.ToString());
            if (li != null && li.Selected)
            {
                if (item.CreditAmount >= receveingAmt)
                {
                    totalDepositByCredit += receveingAmt;
                    outstanding = outstanding - receveingAmt;

                    dicDepositListToRcv[item.CustomerID] = receveingAmt;
                    dicDeducttLstFromCredit[item.CustomerID] = (-1.0D) * receveingAmt;
                }
                else //if (item.CreditAmount < amtToBeingPaid)
                {
                    totalDepositByCredit += item.CreditAmount;
                    outstanding = outstanding - item.CreditAmount;

                    dicDepositListToRcv[item.CustomerID] = item.CreditAmount;
                    dicDeducttLstFromCredit[item.CustomerID] = (-1.0D) * item.CreditAmount;
                }
            }
        }

        foreach (var item in dicDeducttLstFromCredit.Keys)
        {
            cr.CreditAmount = dicDeducttLstFromCredit[item];
            cr.CreditedBy = CurrentUser.UserID;
            cr.CreditedOn = DateTime.Now;
            cr.CreditVia = (int)StatusAmountReceivedVia.Credit;
            cr.CustomerID = item;
            cr.RefundType = "C";
            cr.Insert(dbHelp, CurrentUser.UserID);
        }


        //Add Account receivable Entry
        AccountReceivable arc = new AccountReceivable();
        arc.ARAmtRcvd = totalDepositByCredit > 0 ? totalDepositByCredit : receveingAmt;
        //arc.ARAmtRcvdDateTime = BusinessUtility.GetDateTime(txtARAmtRcvdDateTime.Text, DateFormat.MMddyyyy);
        //arc.ARAmtRcvdDateTime = BusinessUtility.GetDateTime(txtARAmtRcvdDateTime.Text + DateTime.Now.ToString(" HH:mm:ss"), DateFormat.MMddyyyy);
        arc.ARAmtRcvdDateTime = BusinessUtility.GetDateTime(txtARAmtRcvdDateTime.Text + DateTime.Now.ToString(" HH:mm:ss"), "MM/dd/yyyy HH:mm:ss"); //Changed by mukesh 20141112
        
        arc.ARAmtRcvdVia = totalDepositByCredit > 0 ? (int)StatusAmountReceivedVia.Credit : BusinessUtility.GetInt(dlRcvdVia.SelectedValue);
        arc.ARChequeNo = txtChequeNo.Text;
        arc.ARColAsigned = rblstColAsigned.SelectedValue == "1";
        arc.ARContactEmail = txtContactEmail.Text;
        arc.ARContactNm = txtContactNm.Text;
        arc.ARContactPhone = txtARContactPhone.Text;
        arc.ARCreditCardCVVCode = txtCreditCardCVVCode.Text;
        arc.ARCreditCardExp = string.Empty;
        arc.ARCreditCardFirstName = txtCreditCardFirstName.Text;
        arc.ARCreditCardLastName = txtCreditCardLastName.Text;
        arc.ARCreditCardNo = txtCreditCardNo.Text;
        arc.ARInvoiceNo = this.InvoiceID;
        arc.ARNote = txtNote.Text;
        arc.ARRcvdBy = CurrentUser.UserID;
        arc.ARWriteOff = 0.00;
        arc.ARReceiptNo = txtReceiptNo.Text;
        arc.Insert(dbHelp, CurrentUser.UserID);

        //Once Amount received successfully just to update entry in sls Analysis table for invoice 
        //new SlsAnalysis().UpdateOnPaymentReceived(dbHelp, arc.ARInvoiceNo, arc.ARAmtRcvd, arc.ARAmtRcvdDateTime);

        //if (arc.AccountReceivableID <= 0) return;

        //Update Invoice Status                
        //if (total.OutstandingAmount - arc.ARAmtRcvd == 0)
        //{
        //    inv.UpdateStatus(dbHelp, this.InvoiceID, InvoicesStatus.PAYMENT_RECEIVED, CurrentUser.UserID);            
        //}
        //else
        //{
        //    inv.UpdateStatus(dbHelp, this.InvoiceID, InvoicesStatus.PARTIAL_PAYMENT_RECEIVED, CurrentUser.UserID);            
        //}       

        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgARAmtRcvSuccessfully);
        Globals.RegisterReloadParentScript(this);
    }

    private void ReceiveOnOrderLevel(DbHelper dbHelp)
    {
        double receveingAmt = BusinessUtility.GetDouble(txtARAmtRcvd.Text);

        int orderID = 0;
        int.TryParse(Request.QueryString["oid"], out orderID);

        TotalSummary lTotal = CalculationHelper.GetOrderTotal(dbHelp, orderID);

        //validate receiving ammount
        /*if (receveingAmt > lTotal.OutstandingAmount)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgRcvAmountShouldLessOrEqualToBalance);
            return;
        }
        else*/
        if (receveingAmt < 0)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgAmountShouldGreaterThenZero);
            return;
        }

        Orders ord = new Orders();
        ord.PopulateObject(dbHelp, orderID);

        PreAccountRcv arc = new PreAccountRcv();
        arc.CustomerID = ord.OrdCustID;
        arc.DateReceived = BusinessUtility.GetDateTime(txtARAmtRcvdDateTime.Text, DateFormat.MMddyyyy);
        arc.OrderID = orderID;
        arc.PaymentMethod = BusinessUtility.GetInt(dlRcvdVia.SelectedValue);
        arc.ReceiptNo = txtReceiptNo.Text;
        arc.Notes = txtNote.Text;

        CustomerCredit cr = new CustomerCredit();
        var lst = cr.GetCustomerCreditByOrder(dbHelp, orderID);

        double outstanding = lTotal.OutstandingAmount;
        double amtToBeingPaid = BusinessUtility.GetDouble(txtARAmtRcvd.Text);

        /*if (outstanding < amtToBeingPaid)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgDepositAmountMustBeLessOrEqualToOutstandingAmount);
            return;
        }*/

        Dictionary<int, double> dicDeducttLstFromCredit = new Dictionary<int, double>();//<CustomerID, NegetiveCreditAmt>
        Dictionary<int, double> dicDepositListToRcv = new Dictionary<int, double>();//<CustomerID, PossitivePaidAmount>
        double totalDepositByCredit = 0.0D;
        foreach (var item in lst)
        {
            if (totalDepositByCredit == amtToBeingPaid)
            {
                break;
            }
            ListItem li = lstApplyCredit.Items.FindByValue(item.CustomerID.ToString());
            if (li != null && li.Selected)
            {
                if (item.CreditAmount >= amtToBeingPaid)
                {
                    totalDepositByCredit += amtToBeingPaid;
                    outstanding = outstanding - amtToBeingPaid;

                    dicDepositListToRcv[item.CustomerID] = amtToBeingPaid;
                    dicDeducttLstFromCredit[item.CustomerID] = (-1.0D) * amtToBeingPaid;
                }
                else //if (item.CreditAmount < amtToBeingPaid)
                {
                    totalDepositByCredit += item.CreditAmount;
                    outstanding = outstanding - item.CreditAmount;

                    dicDepositListToRcv[item.CustomerID] = item.CreditAmount;
                    dicDeducttLstFromCredit[item.CustomerID] = (-1.0D) * item.CreditAmount;
                }
            }
        }

        foreach (var item in dicDeducttLstFromCredit.Keys)
        {
            cr.CreditAmount = dicDeducttLstFromCredit[item];
            cr.CreditedBy = CurrentUser.UserID;
            cr.CreditedOn = DateTime.Now;
            cr.CreditVia = (int)StatusAmountReceivedVia.Credit;
            cr.CustomerID = item;
            cr.RefundType = "C";
            cr.Insert(dbHelp, CurrentUser.UserID);
        }

        foreach (var item in dicDepositListToRcv.Keys)
        {
            arc.CustomerID = item;
            arc.AmountDeposit = dicDepositListToRcv[item];
            arc.PaymentMethod = (int)StatusAmountReceivedVia.Credit;
            arc.Insert(dbHelp);
        }

        if (totalDepositByCredit <= 0)
        {
            arc.CustomerID = BusinessUtility.GetInt(ord.OrdCustID);
            arc.AmountDeposit = amtToBeingPaid;
            arc.PaymentMethod = BusinessUtility.GetInt(dlRcvdVia.SelectedValue);
            arc.Insert(dbHelp);
        }

        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgARAmtRcvSuccessfully);
        Globals.RegisterReloadParentScript(this);
    }

    private void Refund(DbHelper dbHelp)
    {
        TotalSummary total = CalculationHelper.GetInvoiceTotal(dbHelp, this.InvoiceID);
        double refundAmt = 0.00D;
        if (BusinessUtility.GetString(Request.QueryString["IsRefundOnZeroBalance"]) != "1")
        {
            if (!double.TryParse(txtRefund.Text, out refundAmt))
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidAmountToBeRefund);
                return;
            }

            //validate receiving ammount        
            if (refundAmt > total.TotalAmountReceived)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgRefunAmtShouldBeLessThenOrEqualReceivedAmount);
                return;
            }
            else if (refundAmt < 0)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgAmountShouldGreaterThenZero);
                return;
            }
        }

        //Refund on payment gateway if transaction id available!
        bool refundAcceptedByGateway = true;
        Invoice inv = new Invoice();
        inv.PopulateObject(dbHelp, this.InvoiceID);

        //Make refund on evo cananda payment gateway
        refundAcceptedByGateway = this.RefundOnEvoCanada(dbHelp, refundAmt, inv.InvForOrderNo);

        if (!refundAcceptedByGateway)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgRefundRequestNotAcceptedOnGateWay);
            return;
        }

        //Make amout -ve so that it can be added in same received ammount field
        if (refundAmt > 0) refundAmt = refundAmt * (-1.00D);

        //Add Account receivable Entry
        AccountReceivable arc = new AccountReceivable();
        arc.ARAmtRcvd = refundAmt;
        arc.ARAmtRcvdDateTime = BusinessUtility.GetDateTime(txtARAmtRcvdDateTime.Text + DateTime.Now.ToString(" HH:mm:ss"), DateFormat.MMddyyyyHHmmss);
        //arc.ARAmtRcvdDateTime = Convert.ToDateTime(txtARAmtRcvdDateTime.Text + DateTime.Now.ToString(" HH:mm:ss"));
        //arc.ARAmtRcvdDateTime = Convert.ToDateTime(txtARAmtRcvdDateTime.Text + DateTime.Now.ToString(" HH:mm:ss"));
        //arc.ARAmtRcvdVia = (int)StatusAmountReceivedVia.Refund;
        arc.ARAmtRcvdVia = BusinessUtility.GetInt(dlRcvdVia.SelectedValue);
        arc.ARChequeNo = txtChequeNo.Text;
        arc.ARColAsigned = rblstColAsigned.SelectedValue == "1";
        arc.ARContactEmail = txtContactEmail.Text;
        arc.ARContactNm = txtContactNm.Text;
        arc.ARContactPhone = txtARContactPhone.Text;
        arc.ARCreditCardCVVCode = txtCreditCardCVVCode.Text;
        arc.ARCreditCardExp = string.Empty;
        arc.ARCreditCardFirstName = txtCreditCardFirstName.Text;
        arc.ARCreditCardLastName = txtCreditCardLastName.Text;
        arc.ARCreditCardNo = txtCreditCardNo.Text;
        arc.ARInvoiceNo = this.InvoiceID;
        arc.ARNote = txtNote.Text;
        arc.ARRcvdBy = CurrentUser.UserID;
        //arc.ARWriteOff = 0.00;
        if (Session["RETURN_CART"] == null)
        {
            arc.ARWriteOff = 0.00;
        }
        else
        {
            arc.ARWriteOff = refundAmt * (-1.00D);
        }
        arc.ARReceiptNo = txtReceiptNo.Text;
        arc.Reason = ddlReason.SelectedItem.Value;
        arc.Insert(dbHelp, CurrentUser.UserID);
        Session["AccountReceivableID"] = arc.AccountReceivableID.ToString();
        //if (arc.AccountReceivableID <= 0) return;

        CustomerCredit cc = new CustomerCredit();
        cc.RefundType = rblRefundType.SelectedValue;
        cc.CreditVia = cc.RefundType == "C" ? (int)StatusAmountReceivedVia.Credit : (int)StatusAmountReceivedVia.Refund;
        cc.CreditedBy = CurrentUser.UserID;
        cc.CustomerID = inv.InvCustID;
        cc.ReservationItemID = 0;
        cc.Notes = txtNote.Text;
        //cc.WhsCode = inv.InvShpWhsCode;
        //cc.RegCode = inv.InvRegCode;
        SysRegister sreg = new SysRegister();
        if (!string.IsNullOrEmpty(this.POSRegCode))
        {
            sreg.PopulateObject(this.POSRegCode);
            cc.WhsCode = inv.InvShpWhsCode;
            cc.RegCode = this.POSRegCode;
        }
        else
        {
            cc.WhsCode = inv.InvShpWhsCode;
            cc.RegCode = this.POSRegCode;
        }
        if (refundAmt * (-1.00D) > 0)
        {
            cc.CreditAmount = refundAmt * (-1.00D);
            cc.Insert(dbHelp, CurrentUser.UserID); //Insert Record to Track Customer refunds
        }

        //Update Invoice Status           
        //if (total.OutstandingAmount - arc.ARAmtRcvd == 0)
        //{
        //    inv.UpdateStatus(dbHelp, this.InvoiceID, InvoicesStatus.PAYMENT_RECEIVED, CurrentUser.UserID);
        //}
        //else
        //{
        //    inv.UpdateStatus(dbHelp, this.InvoiceID, InvoicesStatus.PARTIAL_PAYMENT_RECEIVED, CurrentUser.UserID);
        //}

        //Process inventory

        int orderID = 0;
        int.TryParse(Request.QueryString["oid"], out orderID);

        OrderReturn oi = new OrderReturn();
        var lstSessionCart = Session["RETURN_CART"] as List<ReturnCart>;
        var lstReturnCart = Session["RETURN_PROCESS_CART"] as List<ReturnProcessCart>;
        if (lstSessionCart != null)
        {
            oi.ReturnAndUpdateInventory(dbHelp, orderID, CurrentUser.UserID, lstSessionCart, lstReturnCart, false, ddlReason.SelectedValue, this.POSRegCode, arc.AccountReceivableID);
        }
        //if (BusinessUtility.GetString(Request.QueryString["IsRefundOnZeroBalance"]) != "1")
        //{
        //    this.ProcessReturn(dbHelp);
        //}
        Session["ReceiptRefundAmount"] = BusinessUtility.GetDouble(txtRefund.Text);
        if (BusinessUtility.GetString(Session["InvokeSrc"]).ToUpper() == "POS")
        {

            //Session["ReceiptRefundAmount"] = BusinessUtility.GetDouble(txtRefund.Text);

            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgAmountRefundedSuccessfully);
            //Globals.RegisterCloseDialogScript(Page, string.Format("parent.PrintReturnReceipt('{0}')", Globals.CurrentAppLanguageCode), true);
            string isPrint = "0";
            if (chkPrintReceipt.Checked == true)
            {
                isPrint = "1";
            }

            int partID = BusinessUtility.GetInt(ddlCustomers.SelectedValue);
            Orders ord = new Orders();
            ord.PopulateObject(dbHelp, orderID);
            if (partID <= 0)
            {
                partID = ord.OrdCustID;
            }
            Globals.RegisterCloseDialogScript(Page, string.Format("parent.PrintReturnReceipt('{0}', '{1}', '{2}')", Globals.CurrentAppLanguageCode, isPrint, partID), true);
            //Globals.RegisterReloadParentScript(this);
        }
        else
        {

            if (BusinessUtility.GetString(Request.QueryString["rmvSession"]).ToLower() == "true".ToLower())
            {
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgAmountRefundedSuccessfully);
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgAmountRefundedSuccessfully);
                Globals.RegisterReloadParentScript(this);
                //Globals.RegisterCloseDialogScript(Page, string.Format("parent.PrintRcpt('{0}')", Globals.CurrentAppLanguageCode), true);
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.printReceipt('{0}','{1}','{2}')", orderID, arc.AccountReceivableID, Globals.CurrentAppLanguageCode), true);
                
            }
        }
    }

    //private void ProcessReturn(DbHelper dbHelp)
    //{
    //    try
    //    {
    //        OrderReturn oi = new OrderReturn();
    //        var lstSessionCart = Session["RETURN_CART"] as List<ReturnCart>;
    //        var lstReturnCart = Session["RETURN_PROCESS_CART"] as List<ReturnProcessCart>;
    //        if (lstSessionCart != null)
    //        {
    //            if (this.IsTransfer || isUpdateAndtransfer)
    //            {
    //                oi.ReturnAndUpdateInventory(dbHelp, this.OrderID, CurrentUser.UserID, lstSessionCart, lstReturnCart, true, "");
    //            }
    //            else
    //            {
    //                oi.ReturnAndUpdateInventory(dbHelp, this.OrderID, CurrentUser.UserID, lstSessionCart, lstReturnCart, false, "");
    //            }
    //        }
    //    }
    //    catch
    //    {
    //        throw;
    //    }
    //}

    protected void btnCancel_OnClick(object sender, EventArgs e)
    {
        if (BusinessUtility.GetString(Session["InvokeSrc"]).ToUpper() == "POS")
        {
            int partID = 0;
            if (this.IsOnInvoice)
            {
                Invoice objInvoice = new Invoice();
                objInvoice.PopulateObject(this.InvoiceID);
                partID = objInvoice.InvCustID;
            }
            else
            {
                int orderID = 0;
                int.TryParse(Request.QueryString["oid"], out orderID);
                Orders ord = new Orders();
                ord.PopulateObject(orderID);
                if (partID <= 0)
                {
                    partID = ord.OrdCustID;
                }
            }
            Globals.RegisterCloseDialogScript(Page, string.Format("parent.PrintReturnReceipt('{0}', '{1}', '{2}')", Globals.CurrentAppLanguageCode, 0, partID), true);
        }
        else
        {
            Globals.RegisterCloseDialogScript(this.Page);
        }
    }

    private void RefundOnOrderLevel(DbHelper dbHelp)
    {
        double refundAmt = 0.00D;
        if (BusinessUtility.GetString(Request.QueryString["IsRefundOnZeroBalance"]) != "1")
        {
            if (!double.TryParse(txtRefund.Text, out refundAmt))
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidAmountToBeRefund);
                return;
            }
        }

        int orderID = 0;
        int.TryParse(Request.QueryString["oid"], out orderID);

        TotalSummary ts = CalculationHelper.GetOrderTotal(dbHelp, orderID);
        if (BusinessUtility.GetString(Request.QueryString["IsRefundOnZeroBalance"]) != "1")
        {
            //validate receiving ammount        
            if (refundAmt > ts.TotalAmountReceived)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgRefunAmtShouldBeLessThenOrEqualReceivedAmount);
                return;
            }
            else if (refundAmt < 0)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgAmountShouldGreaterThenZero);
                return;
            }
        }

        int partID = BusinessUtility.GetInt(ddlCustomers.SelectedValue);

        Orders ord = new Orders();
        ord.PopulateObject(dbHelp, orderID);
        if (partID <= 0)
        {
            partID = ord.OrdCustID;
        }

        PreAccountRcv ar = new PreAccountRcv();
        ar.CustomerID = BusinessUtility.GetInt(ddlCustomers.SelectedValue);
        ar.DateReceived = DateTime.Now;
        ar.OrderID = orderID;
        ar.PaymentMethod = (int)StatusAmountReceivedVia.Refund;
        ar.Notes = txtNote.Text;

        CustomerCredit cc = new CustomerCredit();
        cc.RefundType = rblRefundType.SelectedValue;
        cc.CreditedBy = CurrentUser.UserID;
        cc.CustomerID = partID;
        cc.ReservationItemID = 0;
        cc.Notes = txtNote.Text;
        //cc.WhsCode = ord.OrdShpWhsCode;
        //cc.RegCode = ord.OrdRegCode;
        SysRegister sreg = new SysRegister();
        if (!string.IsNullOrEmpty(this.POSRegCode))
        {
            sreg.PopulateObject(this.POSRegCode);
            cc.WhsCode = ord.OrdShpWhsCode;
            cc.RegCode = this.POSRegCode;
        }
        else
        {
            cc.WhsCode = ord.OrdShpWhsCode;
            cc.RegCode = this.POSRegCode;
        }

        //Check if admin choose refund or Card on file option
        if (rblRefundType.SelectedValue == "C")
        {
            //Insert amount to customer credit account
            cc.CreditAmount = refundAmt;
            cc.CreditVia = (int)StatusAmountReceivedVia.Credit;
            cc.Insert(dbHelp, CurrentUser.UserID); //Inser Record to Track Customer Credits  

            //Add a -vie amount to order accoount receivebable to adjsut advacne payment credited to guest account
            ar.AmountDeposit = (-1.00) * cc.CreditAmount;
            ar.Insert(dbHelp);
        }
        else //Marked as refund only
        {
            bool refundAcceptedByGateway = true;
            if (rblRefundType.SelectedValue == "F") //Need to refund on payment gateway
            {
                refundAcceptedByGateway = this.RefundOnEvoCanada(dbHelp, refundAmt, orderID);
            }

            // IF refund failed to process on payment gateway terminate process here without updateing order
            if (!refundAcceptedByGateway)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgRefundRequestNotAcceptedOnGateWay);
                return;
            }

            //Insert amount to customer credit account
            cc.CreditAmount = refundAmt;
            cc.CreditVia = (int)StatusAmountReceivedVia.Refund;
            cc.Insert(dbHelp, CurrentUser.UserID); //Inser Record to Track Customer Refunds  

            //Add a -vie amount to order accoount receivebable to adjsut advacne payment refunded to guest account
            ar.AmountDeposit = (-1.00) * cc.CreditAmount;
            ar.Insert(dbHelp);
        }
        Session["ReceiptRefundAmount"] = BusinessUtility.GetDouble(txtRefund.Text); 
        Session["AccountReceivableID"] = ar.RcvID.ToString();
        //inventory process
        OrderReturn oi = new OrderReturn();
        var lstSessionCart = Session["RETURN_CART"] as List<ReturnCart>;
        var lstReturnCart = Session["RETURN_PROCESS_CART"] as List<ReturnProcessCart>;
        if (lstSessionCart != null)
        {
            oi.ReturnAndUpdateInventory(dbHelp, orderID, CurrentUser.UserID, lstSessionCart, lstReturnCart, false, ddlReason.SelectedValue, this.POSRegCode, ar.RcvID);
        }




        if (BusinessUtility.GetString(Session["InvokeSrc"]).ToUpper() == "POS")
        {

            string isPrint = "0";
            if (chkPrintReceipt.Checked == true)
            {
                isPrint = "1";
            }

            //int partID = BusinessUtility.GetInt(ddlCustomers.SelectedValue);
            //Orders ord = new Orders();
            ord.PopulateObject(dbHelp, orderID);
            if (partID <= 0)
            {
                partID = ord.OrdCustID;
            }
            Globals.RegisterCloseDialogScript(Page, string.Format("parent.PrintReturnReceipt('{0}', '{1}', '{2}')", Globals.CurrentAppLanguageCode, isPrint, partID), true);
            //Globals.RegisterReloadParentScript(this);
        }
        else
        {
            if (BusinessUtility.GetString(Request.QueryString["rmvSession"]).ToLower() == "true".ToLower())
            {
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgAmountRefundedSuccessfully);
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgAmountRefundedSuccessfully);
                Globals.RegisterReloadParentScript(this);
                //Globals.RegisterCloseDialogScript(Page, string.Format("parent.PrintRcpt('{0}')", Globals.CurrentAppLanguageCode), true);
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.printReceipt('{0}','{1}','{2}')", orderID, ar.RcvID, Globals.CurrentAppLanguageCode), true);
            }
        }
    }

    private bool RefundOnEvoCanada(DbHelper dbHelp, double refundAmt, int orderID)
    {
        //Refund on payment gateway if transaction id available!
        bool refundAcceptedByGateway = true;
        OrderTransaction ordTrans = new OrderTransaction();
        string transID = ordTrans.GetSaleTransactionID(dbHelp, orderID);

        if (transID.Length > 0) //Need to refund perform on gateway
        {
            try
            {
                EvoPaymentHelper evoPayment = new EvoPaymentHelper(ConfigurationManager.AppSettings["EVO_MERCHANT_USER"], ConfigurationManager.AppSettings["EVO_MERCHANT_PASSWORD"]);
                NameValueCollection nvc = evoPayment.Refund(transID, refundAmt.ToString());
                refundAcceptedByGateway = (nvc.AllKeys.Contains("response") && BusinessUtility.GetInt(nvc["response"]) == (int)EvoResponse.TransactionApproved);
                if (refundAcceptedByGateway)
                {
                    ordTrans.Amount = refundAmt;
                    ordTrans.CreatedBy = CurrentUser.UserID;
                    ordTrans.CreatedOn = DateTime.Now;
                    ordTrans.GatewayErrors = string.Empty;
                    ordTrans.GatewayResponse = nvc["responsetext"];
                    ordTrans.OrderID = orderID;
                    ordTrans.PaymentMethod = "EVO Cananda";
                    ordTrans.TransactionDate = DateTime.Now;
                    ordTrans.TransactionID = nvc["transactionid"];
                    ordTrans.CardType = "";
                    ordTrans.IPAddress = Request.ServerVariables["REMOTE_ADDR"];
                    ordTrans.TransactionStatusID = TransactionStatus.Success;
                    ordTrans.TransactionTypeID = TransactionType.Refund;
                    foreach (var item in nvc.AllKeys)
                    {
                        ordTrans.MetaDataCollection[item] = nvc[item];
                    }
                    ordTrans.Insert(dbHelp, CurrentUser.UserID);

                    //Need to send Notificatin to guest
                    Orders ord = new Orders();
                    ord.PopulateObject(dbHelp, orderID);
                    Partners part = new Partners();
                    part.PopulateObject(dbHelp, ord.OrdCustID);
                    string gName = part.ExtendedProperties.SpirtualName != string.Empty ? string.Format("{0} ({1})", part.PartnerLongName, part.ExtendedProperties.SpirtualName) : part.PartnerLongName;
                    Notifications.SendRefundNotification(orderID, ordTrans.TransactionID, ordTrans.Amount, gName, part.PartnerEmail);
                }
                else
                {
                    ordTrans.Amount = refundAmt;
                    ordTrans.CreatedBy = CurrentUser.UserID;
                    ordTrans.CreatedOn = DateTime.Now;
                    ordTrans.GatewayErrors = string.Empty;
                    ordTrans.GatewayResponse = nvc["responsetext"];
                    ordTrans.OrderID = orderID;
                    ordTrans.PaymentMethod = "EVO Cananda";
                    ordTrans.TransactionDate = DateTime.Now;
                    ordTrans.TransactionID = nvc["transactionid"];
                    ordTrans.CardType = "";
                    ordTrans.IPAddress = Request.ServerVariables["REMOTE_ADDR"];
                    ordTrans.TransactionStatusID = TransactionStatus.Failed;
                    ordTrans.TransactionTypeID = TransactionType.Refund;
                    foreach (var item in nvc.AllKeys)
                    {
                        ordTrans.MetaDataCollection[item] = nvc[item];
                    }
                    ordTrans.Insert(dbHelp, CurrentUser.UserID);
                }
            }
            catch
            {
                refundAcceptedByGateway = false;
            }
        }

        return refundAcceptedByGateway;
    }

    private void WriteOff(DbHelper dbHelp)
    {
        TotalSummary total = CalculationHelper.GetInvoiceTotal(dbHelp, this.InvoiceID);
        double writeOffAmt = BusinessUtility.GetDouble(txtARWriteOff.Text);

        //validate receiving ammount
        if (writeOffAmt > total.GrandTotal || writeOffAmt == 0)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgWriteOffAmountShouldLessOrEqualToInvoiceTotal);
            return;
        }

        //Add Account receivable Entry
        AccountReceivable arc = new AccountReceivable();
        arc.ARAmtRcvd = 0.00D;
        arc.ARAmtRcvdDateTime = BusinessUtility.GetDateTime(txtARAmtRcvdDateTime.Text, DateFormat.MMddyyyy);
        arc.ARAmtRcvdVia = (int)StatusAmountReceivedVia.WriteOff;
        arc.ARChequeNo = txtChequeNo.Text;
        arc.ARColAsigned = rblstColAsigned.SelectedValue == "1";
        arc.ARContactEmail = txtContactEmail.Text;
        arc.ARContactNm = txtContactNm.Text;
        arc.ARContactPhone = txtARContactPhone.Text;
        arc.ARCreditCardCVVCode = txtCreditCardCVVCode.Text;
        arc.ARCreditCardExp = string.Empty;
        arc.ARCreditCardFirstName = txtCreditCardFirstName.Text;
        arc.ARCreditCardLastName = txtCreditCardLastName.Text;
        arc.ARCreditCardNo = txtCreditCardNo.Text;
        arc.ARInvoiceNo = this.InvoiceID;
        arc.ARNote = txtNote.Text;
        arc.ARRcvdBy = CurrentUser.UserID;
        arc.ARWriteOff = writeOffAmt;
        arc.ARReceiptNo = txtReceiptNo.Text;
        arc.Insert(dbHelp, CurrentUser.UserID);

        if (arc.AccountReceivableID <= 0) return;

        //Once Write-Off Amount successfully just to update entry in sls Analysis table for invoice 
        //new SlsAnalysis().UpdateOnPaymentWriteOff(dbHelp, arc.ARInvoiceNo, arc.ARWriteOff, arc.ARAmtRcvdDateTime, arc.ARRcvdBy);

        //Update Invoice Status
        //Invoice inv = new Invoice();
        //if (total.OutstandingAmount - arc.ARAmtRcvd == 0)
        //{
        //    inv.UpdateStatus(dbHelp, this.InvoiceID, InvoicesStatus.PAYMENT_RECEIVED, CurrentUser.UserID);
        //}
        //else
        //{
        //    inv.UpdateStatus(dbHelp, this.InvoiceID, InvoicesStatus.PARTIAL_PAYMENT_RECEIVED, CurrentUser.UserID);
        //}

        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgARAmtRcvSuccessfully);
        Globals.RegisterReloadParentScript(this);
    }

    //Writeoff on return
    private void ReturnWriteOff(DbHelper dbHelp)
    {
        int orderID = 0;
        int.TryParse(Request.QueryString["oid"], out orderID);
        if (orderID <= 0)
        {
            return;
        }
        Orders ord = new Orders();
        Invoice inv = new Invoice();

        int inVID = this.InvoiceID;
        TotalSummary total;
        double writeOffAmt = 0.0D;
        if (inVID <= 0)
        {
            total = CalculationHelper.GetOrderTotal(orderID);
            writeOffAmt = total.GrandTotal;

            //Add Account receivable Entry
            PreAccountRcv arc = new PreAccountRcv();
            arc.AmountDeposit = 0.00D;
            arc.DateReceived = DateTime.Now;
            arc.PaymentMethod = (int)StatusAmountReceivedVia.WriteOff;
            arc.CustomerID = ord.OrdCustID;
            arc.Notes = "Returns";
            arc.OrderID = ord.OrdID;
            arc.ReceiptNo = string.Empty;
            arc.WriteOffAmount = writeOffAmt;
            arc.Insert(dbHelp);

            //Update quantity on full return in order items
        }
        else
        {
            total = CalculationHelper.GetInvoiceTotal(dbHelp, inVID);
            writeOffAmt = total.GrandTotal;
            //Add Account receivable Entry
            AccountReceivable arc = new AccountReceivable();
            arc.ARAmtRcvd = 0.00D;
            arc.ARAmtRcvdDateTime = DateTime.Now;
            arc.ARAmtRcvdVia = (int)StatusAmountReceivedVia.WriteOff;
            arc.ARChequeNo = string.Empty;
            arc.ARColAsigned = false;
            arc.ARContactEmail = string.Empty;
            arc.ARContactNm = string.Empty;
            arc.ARContactPhone = string.Empty;
            arc.ARCreditCardCVVCode = string.Empty;
            arc.ARCreditCardExp = string.Empty;
            arc.ARCreditCardFirstName = string.Empty;
            arc.ARCreditCardLastName = string.Empty;
            arc.ARCreditCardNo = string.Empty;
            arc.ARInvoiceNo = inVID;
            arc.ARNote = "Returns";
            arc.ARRcvdBy = CurrentUser.UserID;
            arc.ARWriteOff = writeOffAmt;
            arc.ARReceiptNo = string.Empty;
            arc.Insert(dbHelp, CurrentUser.UserID);


            //Once Write-Off Amount successfully just to update entry in sls Analysis table for invoice 
            //new SlsAnalysis().UpdateOnPaymentWriteOff(dbHelp, arc.ARInvoiceNo, arc.ARWriteOff, arc.ARAmtRcvdDateTime, arc.ARRcvdBy);
            //Update Invoice Status      
            //if (total.OutstandingAmount - arc.ARAmtRcvd == 0)
            //{
            //    inv.UpdateStatus(dbHelp, inVID, InvoicesStatus.PAYMENT_RECEIVED, CurrentUser.UserID);
            //}
            //else
            //{
            //    inv.UpdateStatus(dbHelp, inVID, InvoicesStatus.PARTIAL_PAYMENT_RECEIVED, CurrentUser.UserID);
            //}
        }
    }

    private void FillYear()
    {
        for (Int16 idx = 0; idx <= 11; idx++)
        {
            dlYear.Items.Add((DateTime.Now.Year + idx).ToString());
        }
    }
    private void FillMonth()
    {
        for (Int16 idx = 1; idx <= 12; idx++)
        {
            if (idx < 10)
            {
                dlMonth.Items.Add("0" + idx);
            }
            else
            {
                dlMonth.Items.Add(idx.ToString());
            }
        }
        dlMonth.SelectedValue = DateTime.Now.Month.ToString("00");
    }

    private int InvoiceID
    {
        get
        {
            int invID = 0;
            int.TryParse(Request.QueryString["invID"], out invID);
            return invID;
        }
    }

    private string ReceivingType
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["rcvType"]);
        }
    }

    private bool IsOnInvoice
    {
        get
        {
            return this.InvoiceID > 0;
        }
    }


    private bool IsReturn
    {
        get
        {
            if (BusinessUtility.GetString(Request.QueryString["isreturn"]) == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public string POSRegCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["regcode"]);
        }
    }
}
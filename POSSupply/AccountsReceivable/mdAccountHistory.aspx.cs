﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class AccountsReceivable_mdAccountHistory : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdAccHst))
        {

        }
    }
    protected void grdAccHst_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
    }

    protected void grdAccHst_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (this.AccRcbID > 0)
        {
            AccountReceivable objAccRcb = new AccountReceivable();
            grdAccHst.DataSource = objAccRcb.GetAccountReceivableHistory(this.AccRcbID, Globals.CurrentAppLanguageCode);
            grdAccHst.DataBind();
        }
        
    }

    public int AccRcbID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["AccountRcbID"], out id);
            return id;
        }
    }
  
}
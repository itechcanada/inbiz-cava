﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Trirand.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class AccountsReceivable_ViewAccountsReceivable : BasePage
{
    AccountReceivable _arc = new AccountReceivable();
    int _currentRow = -1;
    TotalSummary _total;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (BusinessUtility.GetString(Request.QueryString["PartnerID"]) != "")
        {
            dlSearch.SelectedIndex = dlSearch.Items.IndexOf(dlSearch.Items.FindByValue("CN"));
            txtSearch.Text = BusinessUtility.GetString(Request.QueryString["PartnerID"]);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdInvoices))
        {
            btnBack.Visible = Request.QueryString.AllKeys.Contains("returnUrl");

            DropDownHelper.FillDropdown(dlCreatedDate, "AR", "dlSh2", Globals.CurrentAppLanguageCode, new ListItem(""));
            dlSearch.Attributes["data-placeholder"] = Resources.Resource.liSelect;
            dlCreatedDate.Attributes["data-placeholder"] = Resources.Resource.liARSelectCreatedDays;
            
        }
    }
    //string _transID = string.Empty;
    protected void grdInvoices_CellBinding(object sender, JQGridCellBindEventArgs e)
    {        
        if (e.ColumnIndex == 1) {
            e.CellHtml = string.Format("{0} ({1})", e.CellHtml, e.RowValues[2]);
        }
        else if (e.ColumnIndex == 6)//Format Status Column
        {
            switch (e.CellHtml.ToUpper())
            {
                case InvoicesStatus.PARTIAL_PAYMENT_RECEIVED:
                    e.CellHtml = Resources.Resource.liINPartialPaymentReceived;
                    break;
                case InvoicesStatus.PAYMENT_RECEIVED:
                    e.CellHtml = Resources.Resource.liINPaymentReceived;
                    break;
                case InvoicesStatus.RE_SUBMITTED:
                    e.CellHtml = Resources.Resource.liINReSubmitted;
                    break;
                case InvoicesStatus.SUBMITTED:
                    e.CellHtml = Resources.Resource.liINSubmitted;
                    break;
            }
        }        
        else if(e.ColumnIndex == 10)
        {
            switch (e.CellHtml.ToUpper())
            {
                case InvoiceReferenceType.CREDIT_NOTE:
                    e.CellHtml = Resources.Resource.liINCreditNote;
                    break;
                case InvoiceReferenceType.INVOICE:
                    e.CellHtml = Resources.Resource.liINInvoice;
                    break;
            }
        }
        /*else if (e.ColumnIndex == 11)
        {
            _transID = e.CellHtml;
        }
        else if (e.ColumnIndex == 12)
        {
            if (!string.IsNullOrEmpty(_transID))
            {
                string url = string.Format("InvoicePayment.aspx?SOID={0}", e.RowKey);
                e.CellHtml = string.Format("<a class='' href='{0}' >{1}</a>", url, Resources.Resource.lblRefund);
            }
            else
            {
                e.CellHtml = "N/A";
            }
        } */
        else if (e.ColumnIndex == 11)
        {
            string url = string.Format("InvoicePayment.aspx?SOID={0}", e.RowKey);
            e.CellHtml = string.Format("<a class='' href='{0}' >{1}</a>", url, Resources.Resource.lblEdit);
        }
        else if (e.ColumnIndex == 8)
        {
            e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
        }
        else if (e.ColumnIndex == 9)
        {
            e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
        }
    }

    protected void grdInvoices_DataRequesting(object sender, JQGridDataRequestEventArgs e)
    {
        bool isSalesRescricted = CurrentUser.IsInRole(RoleID.SALES_REPRESENTATIVE) && BusinessUtility.GetBool(Session["SalesRepRestricted"]);
        int invID = 0;
        int.TryParse(Request.QueryString["qInvID"], out invID);
        if (Request.QueryString.AllKeys.Contains<string>("_history"))
        {
            sdsInvoices.SelectCommand = _arc.GetSql(sdsInvoices.SelectParameters, Request.QueryString[dlSearch.ClientID], Request.QueryString[txtSearch.ClientID], Request.QueryString[dlCreatedDate.ClientID], invID, CurrentUser.UserID, isSalesRescricted);
        }
        else
        {
            sdsInvoices.SelectCommand = _arc.GetSql(sdsInvoices.SelectParameters, dlSearch.SelectedValue, txtSearch.Text, dlCreatedDate.SelectedValue, invID, CurrentUser.UserID, isSalesRescricted);
        }       
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.QueryString["returnUrl"]);
    }
}
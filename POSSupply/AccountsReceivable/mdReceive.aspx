﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdReceive.aspx.cs" Inherits="AccountsReceivable_mdReceive" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
     <style type="text/css">
        ul.form li div.lbl{width:250px !important;}
        
        .hide{display:none;}
        .tdpadding td, .tdpadding th{padding:0px 5px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div id="formContainer" style="padding: 5px; height: 380px; overflow: auto;">
        <h3>
            <asp:Label ID="lblTitle" Text="" runat="server" />
        </h3>
        <ul class="form">
            <li>
                <div class="lbl">                    
                    <asp:Label ID="lblAmount" Text="<%$Resources:Resource, lblAmount%>" runat="server" />                    
                </div>
                <div class="input" style="width: 420px;">
                    <table class="tdpadding" border="1" cellpadding="0" cellspacing="0">
                        <tr>
                            <th>
                                <asp:Label ID="Label4" Text="<%$Resources:Resource, lblTotal%>" runat="server" />
                            </th>
                            <th>
                                <asp:Label ID="Label5" Text="<%$Resources:Resource, grdARAmountReceived%>" runat="server" />
                            </th>
                            <th>
                                <asp:Label ID="Label10" Text="<%$Resources:Resource, lblAdminFee%>" runat="server" />
                            </th>
                            <th>
                                <asp:Label ID="Label6" Text="<%$Resources:Resource, grdARBalanceAmount%>" runat="server" />
                            </th>
                        </tr>
                        <tr>
                            <th>
                                <asp:Literal ID="ltInvoiceTotal" Text="" runat="server" />
                            </th>
                            <th>
                                <asp:Literal ID="ltReceivedAmount" Text="" runat="server" />
                            </th>
                            <th>
                                (<asp:Literal ID="ltAdminFee" Text="" runat="server" />)
                            </th>
                            <th style="background-color: Yellow;">
                                <asp:Literal ID="ltBalance" Text="" runat="server" />
                            </th>
                        </tr>
                    </table>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                </div>
                <div class="input" style="width: 420px;">
                    <asp:Literal ID="ltrAmmoutDetail" runat="server"></asp:Literal>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li id="liRefund0" runat="server">
                <div class="lbl">
                    <asp:Label ID="Label12" Text="Verification Password" runat="server" />
                </div>
                 <div class="input">
                     <asp:TextBox ID="txtVerificationPassword" runat="server" TextMode="Password" />
                 </div>
                 <div class="clearBoth">                    
                </div>
            </li>            
            <li id="liRefund1" runat="server">
                <div class="lbl">
                    <asp:Label ID="Label8" Text="<%$Resources:Resource, lblRefundType%>" runat="server" />
                </div>
                <div class="input">
                    <asp:RadioButtonList ID="rblRefundType" runat="server" RepeatDirection="Horizontal" >  <%--AutoPostBack="true" OnSelectedIndexChanged="rblRefundType_OnSelectedIndexChanged"--%>                                             
                    </asp:RadioButtonList>
                </div>
                <div class="clearBoth">                    
                </div>
            </li> 
            <li id="liRefund2" runat="server">
                <div class="lbl">                    
                    <asp:Label ID="Label1" Text="<%$Resources:Resource, lblGatewayTransactionID%>" CssClass="lblBold" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtTransactionID" runat="server" Width="90px" MaxLength="30" Text="" Enabled="false">
                    </asp:TextBox>                    
                </div>
                <div class="clearBoth">
                </div>
            </li> 
            <li id="liRefund3" runat="server">
                <div class="lbl">                    
                    <asp:Label ID="Label2" Text="<%$Resources:Resource, lblAmountToRefund%>" CssClass="lblBold" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtRefund" runat="server" Width="90px" MaxLength="10" Text="0">
                    </asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator1" EnableClientScript="true" SetFocusOnError="true"
                        ControlToValidate="txtRefund" Operator="DataTypeCheck" Type="double" Display="None"
                        ErrorMessage="Invalid refund amount!" runat="server" />
                </div>
                <div class="clearBoth">
                </div>
            </li> 
             <li id="liRefund4" runat="server">
                 <div class="lbl">
                    <asp:Label ID="Label9" Text="<%$Resources:Resource, lblAdminFee%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtAdminFee" runat="server" MaxLength="8" Enabled="false" />
                </div>
                <div class="clearBoth">
                </div>
            </li>  
            <li id="liRefund5" runat="server">
                 <div class="lbl">                                                             
                    <asp:Label ID="Label3" Text="<%$Resources:Resource,lblRefundOrCreditTo%>" runat="server" />                                   
                </div>
                <div class="input">
                    <asp:DropDownList ID="ddlCustomers" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="clearBoth">
                </div>
            </li>                     
           <li id="liWriteOff1" runat="server">
                <div class="lbl">
                    <asp:Label ID="lblARWriteOff" Text="<%$ Resources:Resource, lblARWriteOff %>" CssClass="lblBold"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtARWriteOff" runat="server" Width="90px" MaxLength="10" Text="0">
                    </asp:TextBox>
                    <asp:CompareValidator ID="comvalWriteOff" EnableClientScript="true" SetFocusOnError="true"
                        ControlToValidate="txtARWriteOff" Operator="DataTypeCheck" Type="double" Display="None"
                        ErrorMessage="<%$ Resources:Resource, msgARWriteOffNumericValue %>" runat="server" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
             <li id="liReceive1" runat="server">
                <div class="lbl">
                    <asp:Label ID="lblARAmountReceived" CssClass="lblBold" Text="<%$ Resources:Resource, lblARAmountReceived %>"
                        runat="server" />
                    *</div>
                <div class="input">
                    <asp:TextBox ID="txtARAmtRcvd" runat="server" Width="90px" MaxLength="8" onkeypress="return disableEnterKey(event)">
                    </asp:TextBox>*<asp:RequiredFieldValidator ID="reqvalAmtRcvd" runat="server" ControlToValidate="txtARAmtRcvd"
                        ErrorMessage="<%$ Resources:Resource, msgARPlzEntAmtRcvd %>" SetFocusOnError="true"
                        Display="None">
                    </asp:RequiredFieldValidator>
                   <%-- <asp:CompareValidator ID="comvalAmtRcvd" EnableClientScript="true" SetFocusOnError="true"
                        ControlToValidate="txtARAmtRcvd" Operator="DataTypeCheck" Type="double" Display="None"
                        ErrorMessage="<%$ Resources:Resource, msgARPlzEntNumericValueInAmtRcvd %>" runat="server" />--%>
                    <asp:CompareValidator ID="compvalAmtGT0" EnableClientScript="true" SetFocusOnError="true"
                        ControlToValidate="txtARAmtRcvd" Operator="GreaterThanEqual" ValueToCompare="0"
                        Display="None" ErrorMessage="<%$ Resources:Resource, msgARPlzEntAmtGTE0 %>" runat="server" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li id="liReceive2" runat="server">
                <div class="lbl">
                    <asp:Label ID="Label7" Text="<%$Resources:Resource,lblReceiptNo%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtReceiptNo" runat="server" />                    
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li id="liReceive3" runat="server">
                <div class="lbl">
                    <asp:Label ID="lblAvailabeCredit" Text="<%$Resources:Resource,lblGuestAvailableCredit%>" runat="server" />
                </div>
                <div class="input">
                    <asp:Label ID="valAvailCredit" Text="" runat="server" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li id="liReceive4" runat="server">
                <div class="lbl">
                    <asp:Label ID="lblApplyCredit" Text="<%$Resources:Resource,lblApplyCredit%>" runat="server" />
                </div>
                <div class="input">
                    <asp:ListBox CssClass="modernized_select" ID="lstApplyCredit" runat="server" 
                        SelectionMode="Multiple" Width="250px">                        
                    </asp:ListBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li id="liReceive5" runat="server">
                <div class="lbl">
                    <asp:Label ID="lblARAmtRcvdVia" CssClass="lblBold" Text="<%$ Resources:Resource, lblARAmtRcvdVia%>"
                        runat="server" />
                    *</div>
                <div class="input">
                    <asp:DropDownList ID="dlRcvdVia" runat="server">
                    </asp:DropDownList>
                    <asp:CustomValidator ID="custvalRcvdVia" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, msgARPlzSelAmtRcvdVia %>"
                        ClientValidationFunction="funCheckAmountReceivedVia" Display="None">
                    </asp:CustomValidator>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblARAmtRcvdDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblARAmtRcvdDate %>">
                    </asp:Label>
                    *</div>
                <div class="input">
                    <asp:TextBox ID="txtARAmtRcvdDateTime" CssClass="datepicker" Enabled="true" runat="server" Width="90px"
                        MaxLength="20" onkeypress="return disableEnterKey(event)">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqvalAmtRcvdDateTime" runat="server" ControlToValidate="txtARAmtRcvdDateTime"
                        ErrorMessage="<%$ Resources:Resource, msgARPlzEntAmtRcvdDate %>" SetFocusOnError="true"
                        Display="None">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li id="liReason" runat="server">
                <div class="lbl">
                    <asp:Label ID="Label13" Text="<%$Resources:Resource,lblReason%>" runat="server" />
                </div>
                <div class="input">
                    <asp:DropDownList ID="ddlReason" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="clearBoth">
                </div>
            </li>  
            
            <li class="hide">
                <div class="lbl">
                    <asp:Label ID="lblARContactNm" CssClass="lblBold" Text="<%$ Resources:Resource, lblARContactNm%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtContactNm" runat="server" Width="150px" MaxLength="150">
                    </asp:TextBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li class="hide">
                <div class="lbl">
                    <asp:Label ID="lblARContactPhone" CssClass="lblBold" Text="<%$ Resources:Resource, lblARContactPhone%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtARContactPhone" runat="server" Width="150px" MaxLength="20">
                    </asp:TextBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li class="hide">
                <div class="lbl">
                    <asp:Label ID="lblARContactEmail" CssClass="lblBold" Text="<%$ Resources:Resource, lblARContactEmail%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtContactEmail" runat="server" Width="150px" MaxLength="75">
                    </asp:TextBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li class="hide">
                <div class="lbl">
                    <asp:Label ID="lblARColAsigned" CssClass="lblBold" Text="<%$ Resources:Resource, lblARColAsigned%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:RadioButtonList ID="rblstColAsigned" runat="server" RepeatDirection="Horizontal"
                        RepeatLayout="flow" CssClass="lblBold">
                        <asp:ListItem Text="<%$ Resources:Resource, lblYes %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, lblNo %>" Value="0" Selected="true"></asp:ListItem>
                    </asp:RadioButtonList>
                    &nbsp;<asp:Label ID="lblAssignedTo" CssClass="lblBold" runat="server" />
                </div>
                <div class="clearBoth">
                </div>
            </li>            
            <li class="hide">
                <div class="lbl">
                    <asp:Label ID="lblARCreditCardNo" CssClass="lblBold" Text="<%$ Resources:Resource, lblARCreditCardNo%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCreditCardNo" runat="server" Width="150px" MaxLength="4">
                    </asp:TextBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li class="hide">
                <div class="lbl">
                    <asp:Label ID="lblARCreditCardExp" CssClass="lblBold" Text="<%$ Resources:Resource, lblARCreditCardExp%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:DropDownList ID="dlMonth" runat="server" Width="50px">
                    </asp:DropDownList>
                    <asp:DropDownList ID="dlYear" runat="server" Width="100px">
                    </asp:DropDownList>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li class="hide">
                <div class="lbl">
                    <asp:Label ID="lblARCreditCardCVVCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblARCreditCardCVVCode%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCreditCardCVVCode" runat="server" Width="150px" MaxLength="4">
                    </asp:TextBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li class="hide">
                <div class="lbl">
                    <asp:Label ID="lblARCreditCardFirstName" CssClass="lblBold" Text="<%$ Resources:Resource, lblARCreditCardFirstName%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCreditCardFirstName" runat="server" Width="150px" MaxLength="60">
                    </asp:TextBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li class="hide">
                <div class="lbl">
                    <asp:Label ID="lblARCreditCardLastName" CssClass="lblBold" Text="<%$ Resources:Resource, lblARCreditCardLastName%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCreditCardLastName" runat="server" Width="150px" MaxLength="60">
                    </asp:TextBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li class="hide">
                <div class="lbl">
                    <asp:Label ID="lblARChequeNo" CssClass="lblBold" Text="<%$ Resources:Resource, lblARChequeNo%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtChequeNo" runat="server" Width="150px" MaxLength="15">
                    </asp:TextBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblARNote" CssClass="lblBold" Text="<%$ Resources:Resource, lblARNote%>"
                                runat="server" />
                </div>                
                <div class="input" style="width:300px;">
                    <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="3" Width="290px">
                            </asp:TextBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
    </div>
    <div id="divRefundConfirmation" title="Confirm">
        <div id="divConfirm" class="" style="text-align:center;">
            <asp:Label ID="Label11" Text="<%$Resources:Resource, msgAreYousuretoWantCreditOrRefund%>"
                runat="server" /><br />
            <asp:Label ID="lblCur" Font-Bold="true" Text="" runat="server" /> <asp:Label ID="lblConfirmRefund" Font-Bold="true" Text="" runat="server" /></div>
      <div class="div_command" >  
        <asp:Button ID="btnRefund" Text="<%$Resources:Resource, lblProcessCreditOrRefund%>" runat="server" OnClick="btnRefund_Click" />
        <asp:Button ID="Button1" Text="<%$Resources:Resource, btnCancel%>" runat="server"
            CausesValidation="false" OnClick="btnCancel_OnClick" /> <%--OnClientClick="jQuery.FrameDialog.closeDialog();"--%>
      </div>
    </div>
    <div class="div_command">  
        <asp:CheckBox ID="chkPrintReceipt" runat="server"  Text="<%$Resources:Resource, lblPrintReceipt%>" />     <%--Checked="true"--%> 
        <asp:Button ID="btnRefund1" Text="<%$Resources:Resource, lblProcessCreditOrRefund%>" OnClientClick="return confirmaRefunOrCredit();"  runat="server" />        
        <asp:Button ID="btnReceivePayment" 
            Text="<%$Resources:Resource, lblReceivePayment%>" runat="server" 
            onclick="btnReceivePayment_Click" />
        <asp:Button ID="btnWriteOff" Text="<%$Resources:Resource, lblWriteOff%>" 
            runat="server" onclick="btnWriteOff_Click" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, btnCancel%>" runat="server"
            CausesValidation="false" OnClientClick="jQuery.FrameDialog.closeDialog();" Visible="false" />
    </div>
     <asp:ValidationSummary ID="valsSales" runat="server" ShowMessageBox="true" ShowSummary="false" />
     <asp:HiddenField ID="hdnOrderCustID" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        function funCheckAmountReceivedVia(source, args) {
            if (document.getElementById('<%=dlRcvdVia.ClientID%>').selectedIndex == 0) {
                document.getElementById('<%=dlRcvdVia.ClientID%>').focus();
                args.IsValid = false;
            }
        }

        $("#global_message_container").prependTo("#formContainer");

        $("#divRefundConfirmation").dialog({
            autoOpen: false,
            height: 200,
            width: 430,
            modal: true
        }).parent().appendTo(jQuery("form:first"));

        function confirmaRefunOrCredit() {
            if (Page_IsValid) {
                $("#<%=lblConfirmRefund.ClientID%>").text($("#<%=txtRefund.ClientID%>").val());
                $("#divRefundConfirmation").dialog("open");
                return false;
            }
            return false;
        }


        $("#<%=rblRefundType.ClientID%>").live('change', function (e) {
            var sSelectedValue = $('#<%=rblRefundType.ClientID %> input:checked').val();
            if (sSelectedValue == "C") {
                $("#<%=dlRcvdVia.ClientID%> option[value='" + "13" + "']").attr("selected", "selected");
            }
            else {
                $("#<%=dlRcvdVia.ClientID%> option[value='" + "" + "']").attr("selected", "selected");
            }
        });

    </script>
</asp:Content>


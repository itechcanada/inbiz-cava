﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ViewAccountsReceivable.aspx.cs" Inherits="AccountsReceivable_ViewAccountsReceivable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
     <div class="col1">
        <img alt="" src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <asp:Literal runat="server" ID="lblTitle" Text="<%$Resources:Resource, lblAccountsReceivable %>"></asp:Literal>
        </b>
    </div>
    <div>
        
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" Runat="Server">
     <asp:Panel runat="server" ID="SearchPanel">
         <div class="searchBar">
             <div class="header">
                 <div class="title">
                     Search form</div>
                 <div class="icon">
                     <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
             </div>
             <h4>
                <asp:Label CssClass="filter-key" AssociatedControlID="dlSearch" ID="lblSearchBy" runat="server" Text="<%$ Resources:Resource, lblSearchBy %>"></asp:Label>
             </h4>
             <div class="inner">
                 <asp:DropDownList ID="dlSearch" runat="server" Width="175px" ValidationGroup="PrdSrch">
                     <asp:ListItem Value="" Text="" />
                     <asp:ListItem Value="ON" Text="<%$ Resources:Resource, liAROrderNo %>" />
                     <asp:ListItem Value="IN" Text="<%$ Resources:Resource, liARInvoiceNo %>" />
                     <asp:ListItem Value="CN" Text="<%$ Resources:Resource, liARCustomerName %>" />
                     <asp:ListItem Value="CP" Text="<%$ Resources:Resource, liARCustomerPhoneNumber %>" />
                     <asp:ListItem Value="PO" Text="<%$ Resources:Resource, liCustomerPO %>" />
                     <asp:ListItem Value="AY" Text="<%$ Resources:Resource, liCMAcronyme %>" />
                 </asp:DropDownList>
             </div>
             <h4>
                <asp:Label CssClass="filter-key" AssociatedControlID="dlCreatedDate" ID="lblCreatedDays" runat="server" Text="<%$ Resources:Resource, lblCreatedDays %>"></asp:Label>
             </h4>
             <div class="inner">
             <asp:DropDownList ID="dlCreatedDate" runat="server" Width="175px">                        
                    </asp:DropDownList>
             </div>
             <h4>
                <asp:Label CssClass="filter-key" AssociatedControlID="txtSearch" ID="lblSearchKeyword" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label>
             </h4>
             <div class="inner">
                <asp:TextBox runat="server" Width="170px" ID="txtSearch" 
                        ValidationGroup="PrdSrch"></asp:TextBox>
             </div>
             <div class="footer">
                 <div class="submit">
                     <input id="btnSearch" type="button" value="Search" />
                 </div>
                 <br />
             </div>
         </div>
     </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div id="grid_wrapper" style="width: 100%;">
        <trirand:JQGrid runat="server" ID="grdInvoices" DataSourceID="sdsInvoices" Height="300px"
            AutoWidth="True" OnCellBinding="grdInvoices_CellBinding" OnDataRequesting="grdInvoices_DataRequesting">
            <Columns>
                <trirand:JQGridColumn DataField="invID" HeaderText="" PrimaryKey="True" Visible="false" />
                <trirand:JQGridColumn DataField="InvRefNo" HeaderText="#" Width="50" TextAlign="Center" />
                <trirand:JQGridColumn DataField="PartnerAcronyme" HeaderText="<%$ Resources:Resource, grdCMAcronyme %>"
                    Editable="false" Visible="false" />
                <trirand:JQGridColumn DataField="CustomerName" HeaderText="<%$ Resources:Resource, grdSOCustomerName %>"
                    Editable="false" />
                 <trirand:JQGridColumn DataField="invDate" HeaderText="<%$ Resources:Resource, grdSOCreatedDate %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="NetTerms" HeaderText="<%$ Resources:Resource, grdARNetTerms %>"
                    Editable="false" Visible="false" />
                 <trirand:JQGridColumn DataField="invStatus" HeaderText="<%$ Resources:Resource, grdPOStatus %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="AssignedTo" HeaderText="<%$ Resources:Resource, grdAssignedTo %>"
                    Editable="false" NullDisplayText="--" />
                <trirand:JQGridColumn DataField="InvoiceTotal" HeaderText="<%$ Resources:Resource, grdARInvoiceAmount %>"
                    Editable="false" TextAlign="Right" >                   
                </trirand:JQGridColumn>                
                <trirand:JQGridColumn DataField="BalanceAmount" HeaderText="<%$ Resources:Resource, grdARBalanceAmount %>" Editable="false" TextAlign="Right"  />                
                <trirand:JQGridColumn DataField="invRefType" HeaderText="<%$ Resources:Resource, grdINRefType %>"
                    Editable="false" TextAlign="Center" /> 
                <%--<trirand:JQGridColumn HeaderText="<%$ Resources:Resource, lblTransactionID %>" DataField="GatewayTransactionID"
                    Sortable="true" TextAlign="Center" Width="80" />   
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, lblRefund %>" DataField="invID"
                    Sortable="false" TextAlign="Center" Width="80" />--%>
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="invID"
                    Sortable="false" TextAlign="Center" Width="80" />
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <ClientSideEvents LoadComplete="jqGridResize" />
        </trirand:JQGrid>
        <asp:SqlDataSource ID="sdsInvoices" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
    </div>
    <br />
    <div class="div_command">
        <asp:Button ID="btnBack" Text="<%$Resources:Resource, lblGoBack%>" CausesValidation="false"
            runat="server" OnClick="btnBack_Click" />
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        $grid = $("#<%=grdInvoices.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=grdInvoices.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }
               
    </script>
</asp:Content>


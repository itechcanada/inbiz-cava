﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Trirand.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class AccountsReceivable_InvoicePayment : BasePage
{
    Invoice _inv = new Invoice();
    Customer objCust = new Customer();
    AccountReceivable _arc = new AccountReceivable();
    int _currentRow = -1;
    int _dsIdxInvID = 0;
    TotalSummary _total;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdRcv))
        {
            if (CurrentUser.IsInRole(RoleID.ACCOUNT))
            {
                grdRcv.Columns[4].Visible = true;
            }
            
            mdReceive.Url = string.Format("mdReceive.aspx?invID={0}&rcvType={1}&rmvSession={2}", this.InvoiceID, "receive", "true");
            mdRefund.Url = string.Format("mdReceive.aspx?invID={0}&rcvType={1}&rmvSession={2}", this.InvoiceID, "refund", "true");
            //mdEvoRefund.Url = string.Format("mdReceive.aspx?invID={0}&rcvType={1}", this.InvoiceID, "evorefund");
            mdWriteOff.Url = string.Format("mdReceive.aspx?invID={0}&rcvType={1}", this.InvoiceID, "writeoff");
            if (this.InvoiceID > 0)
            {
                _inv.PopulateObject(this.InvoiceID);
                lblSODate.Text = _inv.InvCreatedOn.ToString("MMM-dd yyyy"); //'IIf(objSO.InvCreatedOn <> "", CDate(objSO.InvCreatedOn).ToString("MMM-dd yyyy"), "--")
                lblInvID.Text = _inv.InvRefNo.ToString(); //'objSO.InvRefNo
                lblSOID.Text = _inv.InvForOrderNo.ToString();

                hdnPartnerID.Value = _inv.InvCustID.ToString();
                objCust.PopulateObject(_inv.InvCustID);

                Addresses addr = new Addresses();
                //addr.PopulateObject(_inv.InvCustID, _inv.InvCustType, AddressType.SHIP_TO_ADDRESS);
                addr.GetOrderAddress(_inv.InvForOrderNo, AddressType.SHIP_TO_ADDRESS);
                lblAddress.Text = addr.ToHtml();
                lblCustomerName.Text = objCust.CustomerName;
                lblPhone.Text = !string.IsNullOrEmpty(objCust.Phone) ? objCust.Phone : "--";
                lblFax.Text = !string.IsNullOrEmpty(objCust.Fax) ? objCust.Fax : "--";
                lblEmail.Text = !string.IsNullOrEmpty(objCust.Email) ? objCust.Email : "--";

                double writeOff = _arc.GetWriteOffAmount(_inv.InvID);

                _total = CalculationHelper.GetInvoiceTotal(_inv.InvID);
                //lblInvoiceAmount.Text = string.Format("{0} {1:F}", _total.CurrencyCode, BusinessUtility.GetCurrencyString( _total.GrandTotal, Globals.CurrentCultureName));
                //lblPreviousReceived.Text = string.Format("{0} {1:F}", _total.CurrencyCode, BusinessUtility.GetCurrencyString(_total.TotalAmountReceived,Globals.CurrentCultureName));
                //lblPreviousWriteOff.Text = string.Format("{0} {1:F}", _total.CurrencyCode,BusinessUtility.GetCurrencyString( writeOff,Globals.CurrentCultureName));
                //lblBalanceAmount.Text = string.Format("{0} {1:F}", _total.CurrencyCode, BusinessUtility.GetCurrencyString(_total.OutstandingAmount,Globals.CurrentCultureName));

                lblInvoiceAmount.Text = CurrencyFormat.GetCompanyCurrencyFormat(_total.GrandTotal, _total.CurrencyCode);
                lblPreviousReceived.Text = CurrencyFormat.GetCompanyCurrencyFormat(_total.TotalAmountReceived, _total.CurrencyCode);
                lblPreviousWriteOff.Text = CurrencyFormat.GetCompanyCurrencyFormat(writeOff, _total.CurrencyCode);

                //lblRefundAmount.Text = CurrencyFormat.GetCompanyCurrencyFormat(_total.TotalReturnedAmount, _total.CurrencyCode);
                lblRefundAmount.Text = CurrencyFormat.GetCompanyCurrencyFormat(_total.ReturnedItemReturnAmount + _total.ReturnedOverPaidAmount, _total.CurrencyCode);
                if (_inv.InvTypeCommission == (int)OrderCommission.POS)
                {
                    lblBalanceAmount.Text = CurrencyFormat.GetCompanyCurrencyFormat(0, _total.CurrencyCode);
                    //btnWriteOff.Visible = false;
                }
                else
                {
                    lblBalanceAmount.Text = CurrencyFormat.GetCompanyCurrencyFormat(_total.OutstandingAmount - _total.ReturnedOverPaidAmount, _total.CurrencyCode);
                    //btnWriteOff.Visible = true;
                }


                string assingTo = _arc.AssignTo(_inv.InvID);
                if (!string.IsNullOrEmpty(assingTo))
                {
                    lblAssignedTo.Text = string.Format("[ {0} ]", assingTo);
                }
                else
                {
                    lblAssignedTo.Text = "";
                }

                
            }
        }
    }

    protected void grdRcv_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (_currentRow != e.RowIndex)
        {
            //_total = CalculationHelper.GetInvoiceTotal(BusinessUtility.GetInt(e.RowValues[_dsIdxInvID]));
        }
        if (e.ColumnIndex == 0)
        {
            e.CellHtml = e.CellHtml.Replace(Environment.NewLine, "<br>");
            if (string.IsNullOrEmpty(e.CellHtml))
            {
                e.CellHtml = "--";
            }
        }
        else if (e.ColumnIndex == 4)
        {
            e.CellHtml = string.Format(@"<a class=""edit-PaymentType""  href=""EditPaymentType.aspx?AccountRcbID={0}&jscallback={1}&AccountRcbVia={2}"" >Edit</a>", e.RowKey, "reloadGrid", e.RowValues[18]);
        }
        else if (e.ColumnIndex == 5)
        {
            //Check Is Account Adjusment History Exist 
            if (_arc.ExistAccountHistory(null, BusinessUtility.GetInt(e.RowKey)))
            {
                //hlReturnHistory.NavigateUrl = string.Format("~/Returns/mdReturnHistory.aspx?oid={0}&invid={1}&IsTransfer=1", this.OrderID, invID);class=""edit-PaymentType""
                e.CellHtml = string.Format(@"<a onclick=""ShowAccountHistory({0});""  ><img src=""../Images/HistroyImage.jpg"" /></a>", e.RowKey);
            }
            else
            {
                e.CellHtml = "";
            }
        }
        else if (e.ColumnIndex == 7)
        {
            _inv.PopulateObject(this.InvoiceID);
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(e.CellHtml), _inv.InvCurrencyCode);
            //e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
        }
        else if (e.ColumnIndex == 8)
        {
            _inv.PopulateObject(this.InvoiceID);
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(e.CellHtml), _inv.InvCurrencyCode);
            //e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
        }
        else if (e.ColumnIndex == 9)
        {
            _inv.PopulateObject(this.InvoiceID);
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(e.CellHtml), _inv.InvCurrencyCode);
            //e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
        }
    }

    protected void grdRcv_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        sdsArc.SelectCommand = _arc.GetSql(sdsArc.SelectParameters, this.InvoiceID);
    }

    private int InvoiceID
    {
        get
        {
            int invID = 0;
            int.TryParse(Request.QueryString["SOID"], out invID);
            return invID;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        string returnUrl = Request.QueryString["returnUrl"];
        if (!string.IsNullOrEmpty(returnUrl))
        {
            Response.Redirect(returnUrl);
        }
        else
        {
            Response.Redirect("~/AccountsReceivable/ViewAccountsReceivable.aspx");
        }
    }

    protected void btnSaveAndGoToCal_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Reservation/ReservationCalendar.aspx");
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="AddEditProductDetail.aspx.cs" Inherits="Admin_AddEditProductDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="contentBottom" style="padding: 5px; height: 650px; overflow: auto;" onkeypress="return disableEnterKey(event)">
        <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        <asp:Panel ID="pnlItemDesc" runat="server">
            <div style="padding: 5px;">
                <table style="width:100%;" >
                    <tr>
                        <td valign="top" style="width:auto;" >
                            <asp:ListBox CssClass="modernized_select" ID="ddlMaterial" runat="server" SelectionMode="Single"
                                data-placeholder="<%$ Resources:Resource, lblMaterial %>" Height="100px" Width="350px" ></asp:ListBox>
                        </td>

                        <td valign="top" style="width:auto;">
                            <asp:ListBox CssClass="modernized_select" ID="ddlColor" runat="server" SelectionMode="Multiple"
                                data-placeholder="<%$ Resources:Resource, lblColor %>" Height="100px" Width="350px"></asp:ListBox>
                        </td>
                        <td valign="top" style="width:auto;">
                            <asp:ListBox CssClass="modernized_select" ID="ddlSizeCatg" runat="server" SelectionMode="Single"
                                data-placeholder="<%$ Resources:Resource, lblSize %>" Height="100px"  Width="150px" ></asp:ListBox>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td colspan="5">
                    <h3>
                        <%= Resources.Resource.lblProductDecription%>
                    </h3>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblProdName" Text="<%$ Resources:Resource, lblPrdName %>" runat="server" />*
                </td>
                <td class="input">
                    <asp:TextBox ID="txtProdName" runat="server" MaxLength="250" />
                    <asp:RequiredFieldValidator ID="reqvalProdName" runat="server" ControlToValidate="txtProdName"
                        ErrorMessage="<%$ Resources:Resource, reqvalPrdName %>" Text="*" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </td>
                <td class="text">
                    <asp:Label ID="lblBarcode" Text="<%$ Resources:Resource, lblPrdBarcodeNew %>" runat="server" />*
                </td>
                <td class="input">
                    <asp:TextBox ID="txtBarcode" runat="server" MaxLength="35" ReadOnly="true" />
                    <%--                    <asp:Button ID="btnRefresh" Text="Generate UPC" runat="server" CausesValidation="false" />
                    <asp:RequiredFieldValidator ID="reqvalBarCode" runat="server" ControlToValidate="txtBarcode"
                        ErrorMessage="<%$ Resources:Resource, reqvalProdBarCode %>" SetFocusOnError="true">*</asp:RequiredFieldValidator>
                    <script type="text/javascript">
                        var $barCodeTxtFld = $("#<%=txtBarcode.ClientID%>");
                        $("#<%=btnRefresh.ClientID%>").click(function () {
                            $this = $(this);
                            $.post("../ajax/barCodeAPI.aspx", { pType: "<%=SequenceType%>", pfx: "<%=SequencePfx%>", f: "<%=SequenceFormat%>" }, function (data) {
                                $barCodeTxtFld.val(data);
                                $this.hide();
                            });
                            return false;
                        });                    
                    </script>--%>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="Label2" Text="<%$ Resources:Resource, lblPrdSalesPriceperMinQty %>"
                        runat="server" />*
                </td>
                <td class="input">
                    <asp:TextBox ID="txtSalPriceMinQua" MaxLength="20" runat="server" CssClass="numericTextField" />
                    <asp:CompareValidator ID="cmvSalPriceMinQua" EnableClientScript="true" SetFocusOnError="true"
                        ControlToValidate="txtSalPriceMinQua" Operator="DataTypeCheck" Type="Double"
                        ErrorMessage="<%$ Resources:Resource, cmvPrdSalPriceMinQua %>" runat="server">*</asp:CompareValidator>
                    <asp:RequiredFieldValidator ID="reqvalSalPriceMinQua" runat="server" ControlToValidate="txtSalPriceMinQua"
                        ErrorMessage="<%$ Resources:Resource, reqvalSalPriceMinQua %>" SetFocusOnError="true">*</asp:RequiredFieldValidator>
                </td>
                <td class="text">
                    <asp:Label ID="lblEndUserSalesPrice" Text="<%$ Resources:Resource, lblPrdEndUserSalesPrice %>"
                        runat="server" />*
                </td>
                <td class="input">
                    <asp:TextBox ID="txtEndUserSalesPrice" runat="server" MaxLength="20" CssClass="numericTextField" />
                    <asp:CompareValidator ID="compvalProdPrice" EnableClientScript="true" SetFocusOnError="true"
                        ControlToValidate="txtEndUserSalesPrice" Operator="DataTypeCheck" Type="Double"
                        ErrorMessage="<%$ Resources:Resource, compvalPrdPrice %>" runat="server">*</asp:CompareValidator>
                    <asp:RequiredFieldValidator ID="reqvalEndUserSalesPrice" runat="server" ControlToValidate="txtEndUserSalesPrice"
                        ErrorMessage="<%$ Resources:Resource, reqvalEndUserSalesPrice %>" SetFocusOnError="true">*</asp:RequiredFieldValidator>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblInternalID" runat="server" Text="<%$ Resources:Resource, lblPrdInternalID %>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtInternalID" runat="server" MaxLength="20" />
                </td>
                <td class="text">
                    <asp:Label ID="lblWebSalesPrice" Text="<%$ Resources:Resource, lblWebSalesPrice %>"
                        runat="server" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtWebSalesPrice" runat="server" MaxLength="10" CssClass="numericTextField" />
                    <asp:CompareValidator ID="compvalWebSalesPrice" EnableClientScript="true" SetFocusOnError="true"
                        ControlToValidate="txtWebSalesPrice" Operator="DataTypeCheck" Type="Double" ErrorMessage="<%$ Resources:Resource, CompvalPrdWebSalesPrice %>"
                        runat="server">*</asp:CompareValidator>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="Label19" Text="<%$ Resources:Resource, lblFOBPrice %>" runat="server" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtFOBPrice" runat="server" MaxLength="10" CssClass="numericTextField" />
                    <asp:CompareValidator ID="CompareValidator2" EnableClientScript="true" SetFocusOnError="true"
                        ControlToValidate="txtFOBPrice" Operator="DataTypeCheck" Type="Double" ErrorMessage="<%$ Resources:Resource, CompvalPrdFobPrice %>"
                        runat="server">*</asp:CompareValidator>
                </td>
                <td class="text">
                    <asp:Label ID="Label20" Text="<%$ Resources:Resource, lblLandedPrice %>" runat="server" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtLandedPrice" runat="server" MaxLength="10" CssClass="numericTextField" />
                    <asp:CompareValidator ID="CompareValidator1" EnableClientScript="true" SetFocusOnError="true"
                        ControlToValidate="txtLandedPrice" Operator="DataTypeCheck" Type="Double" ErrorMessage="<%$ Resources:Resource, CompvalPrdLandedPrice %>"
                        runat="server">*</asp:CompareValidator>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <h3>
                        <%= Resources.Resource.lblPackeageDetails%>
                    </h3>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblProdPkgHeight0" runat="server" Text="<%$Resources:Resource, lblProductWeight%>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtWeight" CssClass="numericTextField" runat="server" MaxLength="30"
                        Width="70px" />
                    <asp:Label ID="lblUnitWeight" Text="" runat="server" />
                </td>
                <td class="text">
                    <asp:Label ID="lblProdPkgLength0" runat="server" Text="<%$Resources:Resource, lblProductLength%>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtLength" CssClass="numericTextField" runat="server" MaxLength="30"
                        Width="70px" />
                    <asp:Label ID="lblUnitLength" Text="" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="Label4" runat="server" Text="<%$Resources:Resource, lblPrdPackageWeight%>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtPkgWeight" CssClass="numericTextField" runat="server" MaxLength="30"
                        Width="70px" />
                    <asp:Label ID="lblUnitPkgWeight" Text="" runat="server" />
                </td>
                <td class="text">
                    <asp:Label ID="Label6" runat="server" Text="<%$Resources:Resource, lblPackageLenght%>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtPkgLength" CssClass="numericTextField" runat="server" MaxLength="30"
                        Width="70px" />
                    <asp:Label ID="lblUnitPkgLength" Text="" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="Label7" runat="server" Text="<%$Resources:Resource, lblPrdPackageWidth%>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtPkgWidth" CssClass="numericTextField" runat="server" MaxLength="30"
                        Width="70px" />
                    <asp:Label ID="lblUnitPkgWidth" Text="" runat="server" />
                </td>
                <td class="text">
                    <asp:Label ID="Label8" runat="server" Text="<%$Resources:Resource, lblPrdPackageHeight%>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtPkgHeigth" CssClass="numericTextField" runat="server" MaxLength="30"
                        Width="70px" />
                    <asp:Label ID="lblUnitPkgHeight" Text="" runat="server" />
                </td>
                <td>
                </td>
            </tr>

            <tr>
                <td class="text">
                    <asp:Label ID="Label22" runat="server" Text="<%$Resources:Resource, lblProductWeightIncrement%>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtPrdWgtInc" CssClass="numericTextField" runat="server" MaxLength="30"
                        Width="70px" />
                    <asp:Label ID="Label23" Text="" runat="server" />
                </td>
                <td class="text">
                    &nbsp;
                </td>
                <td class="input">
                    &nbsp;
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <h3>
                        <%= Resources.Resource.lblProductSize%>
                    </h3>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="Label27" runat="server" Text="<%$Resources:Resource, lblStartLength%>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtStartLength" CssClass="numericTextField" runat="server" MaxLength="30"
                        Width="70px" />
                </td>
                <td class="text">
                    <asp:Label ID="Label1" runat="server" Text="<%$Resources:Resource, lblIncrementLength%>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtIncrementLength" CssClass="numericTextField" runat="server" MaxLength="30"
                        Width="70px" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="Label29" runat="server" Text="<%$Resources:Resource, lblStartChest%>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtStartChest" CssClass="numericTextField" runat="server" MaxLength="30"
                        Width="70px" />
                </td>
                <td class="text">
                    <asp:Label ID="Label3" runat="server" Text="<%$Resources:Resource, lblIncrementChest%>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtIncrementChest" CssClass="numericTextField" runat="server" MaxLength="30"
                        Width="70px" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="Label5" runat="server" Text="<%$Resources:Resource, lblStartShoulder%>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtStartShoulder" CssClass="numericTextField" runat="server" MaxLength="30"
                        Width="70px" />
                </td>
                <td class="text">
                    <asp:Label ID="Label9" runat="server" Text="<%$Resources:Resource, lblIncrementShoulder%>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtIncrementShoulder" CssClass="numericTextField" runat="server"
                        MaxLength="30" Width="70px" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="Label30" runat="server" Text="<%$Resources:Resource, lblStartBicep%>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtStartBicep" CssClass="numericTextField" runat="server" MaxLength="30"
                        Width="70px" />
                </td>
                <td class="text">
                    <asp:Label ID="Label10" runat="server" Text="<%$Resources:Resource, lblIncrementBicep%>" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtIncrementBicep" CssClass="numericTextField" runat="server" MaxLength="30"
                        Width="70px" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <h3>
                        <%= Resources.Resource.lblPOInformation%>
                    </h3>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblMinQtyforPO" Text="<%$ Resources:Resource, lblPrdMinimumQtyforPO %>"
                        runat="server" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtMinQtyPO" runat="server" CssClass="integerTextField" Width="70px" />
                    <asp:CompareValidator ID="comvalMinQtyforPO" EnableClientScript="true" SetFocusOnError="true"
                        ControlToValidate="txtMinQtyPO" Operator="DataTypeCheck" Type="double" ErrorMessage="<%$ Resources:Resource, comvalMinQtyforPO %>"
                        runat="server">*</asp:CompareValidator>
                </td>
                <td class="text">
                    <asp:Label ID="lblQuaToOrd" Text="<%$ Resources:Resource, lblPrdQuaToOrd %>" runat="server" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtQtyPo" runat="server" CssClass="integerTextField" Width="70px" />
                    <asp:CompareValidator ID="comvalQuaToPOrd" EnableClientScript="true" SetFocusOnError="true"
                        ControlToValidate="txtQtyPo" Operator="DataTypeCheck" Type="double" ErrorMessage="<%$ Resources:Resource, comvalQuaToPOrd %>"
                        runat="server">*</asp:CompareValidator>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="Label11" Text="<%$ Resources:Resource, lblPrdCreateAutoPOFlag %>"
                        runat="server" />
                </td>
                <td class="input">
                    <asp:RadioButtonList ID="rdlstPOFlag" runat="server" RepeatDirection="Horizontal"
                        CssClass="rbl_list">
                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Selected="true" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <h3>
                        <%= Resources.Resource.lblWebSiteInfo%>
                    </h3>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="Label16" Text="<%$ Resources:Resource, lblWebsiteCategory %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlCategory" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblWebCategory %>" Height="100px">
                    </asp:ListBox>
                </td>
                <td class="text" valign="top">
                    <asp:Label ID="Label17" Text="<%$ Resources:Resource, lblWebSiteSubCategory %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlWebSiteSubCatg" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblWebSiteSubCategory %>" Height="100px">
                    </asp:ListBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <h3>
                        <%= Resources.Resource.lblProductDetail%>
                    </h3>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblProdSpecial" Text="<%$ Resources:Resource, lblPrdSpecial %>" runat="server" />
                </td>
                <td class="input">
                    <asp:RadioButtonList ID="rblstSpecial" runat="server" RepeatDirection="Horizontal"
                        CssClass="rbl_list">
                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Selected="true" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td class="text">
                    <asp:Label ID="lblisPOSMenu" Text="<%$ Resources:Resource, lblIsPOSMenu %>" runat="server" />
                </td>
                <td class="input">
                    <asp:RadioButtonList ID="rdlstISPOSMenu" runat="server" RepeatDirection="Horizontal"
                        CssClass="rbl_list">
                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Selected="true" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblIsKit" Text="<%$ Resources:Resource, lblPrdIsProductKit %>" runat="server" />
                </td>
                <td class="input">
                    <asp:RadioButtonList ID="rblstProKit" runat="server" RepeatDirection="Horizontal"
                        CssClass="rbl_list">
                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Selected="true" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td class="text">
                    <asp:Label ID="lblIsProdActive" Text="<%$ Resources:Resource, lblIsActive %>" runat="server" />
                </td>
                <td class="input">
                    <asp:RadioButtonList ID="rblstActive" runat="server" RepeatDirection="Horizontal"
                        CssClass="rbl_list">
                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Selected="true" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblISWeb" Text="<%$ Resources:Resource, lblIsWeb %>" runat="server" />
                </td>
                <td class="input">
                    <asp:RadioButtonList ID="rdlstISWeb" runat="server" RepeatDirection="Horizontal"
                        CssClass="rbl_list">
                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Selected="true" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td class="text">
               <%-- <asp:Label ID="Label21" runat="server" Text="<%$Resources:Resource, lblGauge%>" />--%>
                </td>
                <td class="input">
                <%--<asp:TextBox ID="txtGauage" runat="server" CssClass="integerTextField" Width="70px" />--%>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text" valign="top">
                    <asp:Label ID="lblNickLine" Text="<%$ Resources:Resource, lblNickLine %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlNickline" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblNickLine %>"></asp:ListBox>
                </td>
                <td class="text" valign="top">
                    <asp:Label ID="lblExtraCategory" Text="<%$ Resources:Resource, lblExtraCategory %>"
                        runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlExtraCategory" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblExtraCategory %>"></asp:ListBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblSleeve" Text="<%$ Resources:Resource, lblSleeve %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlSleeve" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblSleeve %>"></asp:ListBox>
                </td>
                <td class="text" valign="top">
                    <asp:Label ID="lblSilhouette" Text="<%$ Resources:Resource, lblSilhouette %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlSilhouette" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblSilhouette %>"></asp:ListBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="Label14" Text="<%$ Resources:Resource, lblTrend %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlTrend" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblTrend %>"></asp:ListBox>
                </td>
                <td class="text" valign="top">
                    <asp:Label ID="Label15" Text="<%$ Resources:Resource, lblProductKeyWord %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlKeyWord" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblProductKeyWord %>"></asp:ListBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblTexture" Text="<%$ Resources:Resource, lblTexture %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select chosen-select" ID="ddlTexture" runat="server"
                        SelectionMode="Multiple" data-placeholder="<%$ Resources:Resource, lblTexture %>"
                        limit="2" Height="100px"></asp:ListBox>
                </td>
                <td class="text" valign="top">
                    <asp:Label ID="Label21" runat="server" Text="<%$Resources:Resource, lblGauge%>" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlGauge" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblGauge %>"></asp:ListBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <h3>
                        <%= Resources.Resource.lblProductTextDesc%>
                    </h3>
                </td>
            </tr>
            <tr>
                <td class="text" valign="top">
                    <asp:Label ID="Label12" Text="<%$ Resources:Resource, lblFrenchDesc %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:TextBox ID="txtFrenchDesc" runat="server" Rows="3" TextMode="MultiLine" />
                    <asp:CustomValidator ID="CustomValidator1" SetFocusOnError="true" runat="server"
                        ErrorMessage="<%$ Resources:Resource, CustvalPrdIncolTerms %>" ClientValidationFunction="funMaxLengthIncolTerms">*</asp:CustomValidator>
                </td>
                <td class="text" valign="top">
                    <asp:Label ID="Label13" Text="<%$ Resources:Resource, lblEnglishDesc %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:TextBox ID="txtEnglishDesc" runat="server" Rows="3" TextMode="MultiLine" />
                    <asp:CustomValidator ID="CustomValidator2" SetFocusOnError="true" runat="server"
                        ErrorMessage="<%$ Resources:Resource, CustvalPrdIncolTerms %>" ClientValidationFunction="funMaxLengthIncolTerms">*</asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="text" valign="top">
                    <asp:Label ID="lblProdIncolTerms" Text="<%$ Resources:Resource, lblNotes %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:TextBox ID="txtProdIncolTerms" runat="server" Rows="3" TextMode="MultiLine" />
                    <asp:CustomValidator ID="custvalProdIncolTerms" SetFocusOnError="true" runat="server"
                        ErrorMessage="<%$ Resources:Resource, CustvalPrdIncolTerms %>" ClientValidationFunction="funMaxLengthIncolTerms">*</asp:CustomValidator>
                </td>
                <td class="text" valign="top">
                    <asp:Label ID="Label18" Text="<%$ Resources:Resource, lblAssociatedStyle %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlAssociatedStyle" runat="server"
                        SelectionMode="Multiple" data-placeholder="<%$ Resources:Resource, lblAssociatedStyle %>">
                    </asp:ListBox>
                </td>
            </tr>
        </table>
    </div>
    <div class="div-dialog-command" style="border-top: 1px solid #ccc; padding: 5px;">
        <asp:Button Style="float: left;" Text="<%$Resources:Resource, btnProductPublish %>"
            ID="btnPublish" runat="server" OnClick="btnPublish_Click" />
        <asp:Button Text="<%$Resources:Resource, btnSave %>" ID="btnSave" runat="server"
            ValidationGroup="RegisterUser" OnClick="btnSave_Click" />
        <asp:Button ID="btnSaveAfterPublish" runat="server" Visible="false" Text="<%$ Resources:Resource,btnSave%>"
            OnClientClick="return ConfirmMsg()" OnClick="btnSaveAfterPublish_Click" />
        <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
            CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
    </div>
    <asp:ValidationSummary ID="valsAdminWarehouse" runat="server" ShowMessageBox="true"
        ShowSummary="false" ValidationGroup="RegisterUser" />
    <script language="javascript">

        $(document).ready(function () {
        });

        function ConfirmMsg(msg) {
            return confirm("<%=Resources.Resource.msgEditPublishedProduct%>");
        }
    </script>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;

public partial class Inventory_QuickInventoryUpdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!CurrentUser.IsInRole(RoleID.RECEIVING_MANAGER) && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        if (!IsPostBack)
        {
            SysWarehouses wh = new SysWarehouses();
            wh.FillAllWharehouse(null, dlWarehouses, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));
            liSalesQty.Visible = false;
            txtSKU.Focus();
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                if (rblAutoCommit.SelectedItem.Value == "0" && txtQty.Text == "0")
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblEnterQty);
                    txtQty.Focus();
                    return;
                }

                Product objProduct = new Product();
                int productID = objProduct.GetProductID(null, txtSKU.Text);
                if (productID > 0)
                {
                    Product prd = new Product();
                    InventoryMovment objIM = new InventoryMovment();
                    prd.UpdatePrdOnHandQty(null, BusinessUtility.GetInt(productID), BusinessUtility.GetInt(txtQty.Text), BusinessUtility.GetString(dlWarehouses.SelectedItem.Value));
                    objIM.AddMovment(BusinessUtility.GetString(dlWarehouses.SelectedItem.Value), CurrentUser.UserID, BusinessUtility.GetInt(productID), BusinessUtility.GetString(InvMovmentSrc.QIU), BusinessUtility.GetInt(txtQty.Text), BusinessUtility.GetString(InvMovmentUpdateType.ORWT));

                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.lblInventoryUpdatedSuccessfully);
                   
                    ResetPage();
                    txtSKU.Focus();
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblInvalidSKU);
                }
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
            }
        }
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ResetPage();
    }

    public void ResetPage()
    {
        liSalesQty.Visible = false;
        rblAutoCommit.SelectedItem.Value = "1";
        txtSKU.Text = "";
        txtQty.Text = "1";
    }

    protected void rblAutoCommit_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblAutoCommit.SelectedItem.Value == "1")
        {
            liSalesQty.Visible = false;
            txtQty.Text = "1";
            txtSKU.Focus();
        }
        else if (rblAutoCommit.SelectedItem.Value == "0")
        {
            liSalesQty.Visible = true;
            txtQty.Text = "0";
            txtSKU.Focus();
        }
    }


    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
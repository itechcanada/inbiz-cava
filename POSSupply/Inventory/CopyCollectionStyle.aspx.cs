﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;
using System.Web.Services;
using Newtonsoft.Json;
using System.Web.Script.Services;

public partial class Inventory_CopyCollectionStyle : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MessageState.SetGlobalMessage(MessageType.Success, "");
            Collection objCollection = new Collection();

            List<Collection> lResult = new List<Collection>();

            lResult.Add(new Collection { CollectionName = "", CollectionID = BusinessUtility.GetInt(0) });

            var lst = objCollection.GetCollectionList(null, Globals.CurrentAppLanguageCode);
            foreach (var item in lst)
            {
                lResult.Add(new Collection { CollectionName = item.ShortName, CollectionID = BusinessUtility.GetInt(item.CollectionID) });
            }

            ddlCollection.DataSource = lResult;// objCollection.GetCollectionList(null, Globals.CurrentAppLanguageCode);
            ddlCollection.DataTextField = "CollectionName";
            ddlCollection.DataValueField = "CollectionID";
            ddlCollection.DataBind();
            if (ddlCollection.Items.Count > 0)
            {
                ddlCollection.SelectedIndex = ddlCollection.Items.IndexOf(ddlCollection.Items.FindByValue(Request.QueryString["CollectionID"]));
            }
        }
        
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            ProductStyle objProductStyle = new ProductStyle();
            objProductStyle.StyleName = txtEnterStyle.Text;
            if(string.IsNullOrEmpty(objProductStyle.StyleName))
            {
                MessageState.SetGlobalMessage(MessageType.Failure, "Please Enter Style Name.");
                return;
            }
            objProductStyle.CollectionID = BusinessUtility.GetInt(ddlCollection.SelectedValue);

            if (objProductStyle.Save(null, CurrentUser.UserID) == true)
            {
                ProductMaterial objProductMaterial = new ProductMaterial();
                List<ProductMaterial> lResult = new List<ProductMaterial>();
                lResult = objProductMaterial.GetStyleMaterial(null, BusinessUtility.GetInt(Request.QueryString["styleID"]), Globals.CurrentAppLanguageCode);
                var lst = new List<int>();
                for (int i = 0; i < lResult.Count; i++)
                {
                    lst.Add(lResult[i].MaterialID);
                }
                if (objProductMaterial.AddMaterialInStyle(BusinessUtility.GetInt(objProductStyle.MasterID), lst.ToArray()) == true)
                {
                    ProductTexture objProductTexture = new ProductTexture();
                    List<ProductTexture> lPTexture = new List<ProductTexture>();
                    lPTexture = objProductTexture.GetStyleTexture(null, BusinessUtility.GetInt(Request.QueryString["styleID"]), Globals.CurrentAppLanguageCode);
                    var lstTexture = new List<int>();
                    for (int j = 0; j < lPTexture.Count; j++)
                    {
                        lstTexture.Add(lPTexture[j].TextureID);
                    }

                    if (objProductTexture.AddTextureInStyle(BusinessUtility.GetInt(objProductStyle.MasterID), lstTexture.ToArray()) == true)
                    {
                        ProductColor objProductColor = new ProductColor();
                        List<ProductColor> lColor = new List<ProductColor>();
                        lColor = objProductColor.GetStyleColor(null, BusinessUtility.GetInt(Request.QueryString["styleID"]), Globals.CurrentAppLanguageCode);

                        var lstColor = new List<int>();
                        for (int k = 0; k < lColor.Count; k++)
                        {
                            lstColor.Add(lColor[k].ColorID);
                        }

                        if (objProductColor.AddColorInStyle(BusinessUtility.GetInt(objProductStyle.MasterID), lstColor.ToArray()) == true)
                        {
                            ProductSize objProductSize = new ProductSize();
                            List<ProductSize> lPSize = new List<ProductSize>();
                            lPSize = objProductSize.GetStyleSizeCategory(null, BusinessUtility.GetInt(Request.QueryString["styleID"]), Globals.CurrentAppLanguageCode);
                            string[] sizeCategoryID = new string[lPSize.Count];
                            for (int l = 0; l < lPSize.Count; l++)
                            {
                                sizeCategoryID[l] = lPSize[l].CategoryName;
                            }
                            if (objProductSize.AddSizeCategoryInStyle(BusinessUtility.GetInt(objProductStyle.MasterID), sizeCategoryID) == true)
                            {
                                ProductCategory objProductCategory = new ProductCategory();
                                List<ProductCategory> lPCategory = new List<ProductCategory>();
                                objProductCategory.CatgHrdID = 1;
                                lPCategory = objProductCategory.GetStyleCatg(null, BusinessUtility.GetInt(Request.QueryString["styleID"]), Globals.CurrentAppLanguageCode);
                                var lstCategory = new List<int>();
                                for (int m = 0; m < lPCategory.Count; m++)
                                {
                                    lstCategory.Add(lPCategory[m].CatgID);
                                }

                                if (objProductCategory.AddCatgInStyle(BusinessUtility.GetInt(objProductStyle.MasterID), lstCategory.ToArray()) == true)
                                {
                                    //Add website Sub category
                                    List<ProductCategory> lPWebSubCtg = new List<ProductCategory>();
                                    objProductCategory.CatgHrdID = 8;
                                    lPWebSubCtg = objProductCategory.GetStyleCatg(null, BusinessUtility.GetInt(Request.QueryString["styleID"]), Globals.CurrentAppLanguageCode);
                                    var lstWebSubCtg = new List<int>();
                                    for (int n = 0; n < lPWebSubCtg.Count; n++)
                                    {
                                        lstWebSubCtg.Add(lPWebSubCtg[n].CatgID);
                                    }
                                    if (objProductCategory.AddCatgInStyle(BusinessUtility.GetInt(objProductStyle.MasterID), lstWebSubCtg.ToArray()) == true)
                                    {
                                        //objProductWebSubCatg.AddAssociatedStyleInStyle(BusinessUtility.GetInt(this.MasterID), iCollectionID, lstProductAssociatedStyle.ToArray());
                                        //var lstStyle = objPRoductStyle.GetProductMasterAssociatedStyle(null, BusinessUtility.GetInt(this.MasterID), iCollectionID);
                                        List<ProductStyle> lPAssStyle = new List<ProductStyle>();
                                         ProductStyle objPRoductStyle = new ProductStyle();
                                         lPAssStyle = objPRoductStyle.GetProductMasterAssociatedStyle(null, BusinessUtility.GetInt(Request.QueryString["styleID"]), BusinessUtility.GetInt(Request.QueryString["CollectionID"]));
                                         var lstpAssStyle = new List<string>();
                                         for (int p = 0; p < lPAssStyle.Count; p++)
                                         {
                                             lstpAssStyle.Add(lPAssStyle[p].StyleName);
                                         }
                                         if (objProductCategory.AddAssociatedStyleInStyle(BusinessUtility.GetInt(objProductStyle.MasterID), objProductStyle.CollectionID, lstpAssStyle.ToArray()) == true)
                                         {
                                             ProductMasterDetail _prd = new ProductMasterDetail();
                                             _prd.MasterID = BusinessUtility.GetInt(Request.QueryString["styleID"]);
                                             _prd.PopulateObject(null, BusinessUtility.GetInt(Request.QueryString["styleID"]));
                                             _prd.MasterID = BusinessUtility.GetInt(objProductStyle.MasterID);
                                             _prd.PrdIsActive = false;
                                             Collection objCollection = new Collection();
                                             var vCollection = objCollection.GetCollection(null, BusinessUtility.GetInt(Request.QueryString["styleID"]), Globals.CurrentAppLanguageCode);
                                             _prd.PrdName = (vCollection[0].ShortName +"-"+ txtEnterStyle.Text).ToUpper();
                                             _prd.PrdUPCCode = BusinessUtility.GetString(vCollection[0].CollectionID.ToString("00")) + objProductStyle.MasterID.ToString("00");
                                             _prd.Save(CurrentUser.UserID);
                                             //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated );
                                             Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadStyle();"), true);
                                         }
                                    }
                                }

                            }
                        }

                    }
                }


            }
            else
            {
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }


}
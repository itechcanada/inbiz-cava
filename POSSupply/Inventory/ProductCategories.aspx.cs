﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Inventory_ProductCategories : System.Web.UI.Page
{
    private Control _contentControl;
    private ProductCategory _prdCtgy = new ProductCategory();
    List<ProductCategory> lstPrdCtgy = new List<ProductCategory>();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security Check
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER))
        {
            Response.Redirect("~/Errors/AccessDenined.aspx");
        }
        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
        {
            btnAdd.Visible = false;
        }
        //btnAdd.Attributes.Add("OnClientClick", "return editPopup('4444');");

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "deleteCategory")
        {
            try
            {
                int iCategoryID = BusinessUtility.GetInt(Request.Form["CategoryID"]);
                int iCategoryHdrID = BusinessUtility.GetInt(Request.Form["CategoryHrdID"]);
                if (iCategoryID > 0)
                {
                    ProductCategory prdCategory = new ProductCategory();
                    prdCategory.CatgHrdID = iCategoryHdrID;
                    prdCategory.CatgID = iCategoryID;
                    prdCategory.CatgIsActive = false;
                    prdCategory.CatgUpdatedBy = CurrentUser.UserID;
                    if (prdCategory.UpdateCategoryStatus(null) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }

            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }

        
        BindNavigation();
        LoadUserControl(this.ActiveSectionIndex);
    }
    public void BindNavigation()
    {
        blstNav.Items.Clear();
        blstNav.Items.Add(this.GetSectionLink(ProductCategories.Extra));
        blstNav.Items.Add(this.GetSectionLink(ProductCategories.Gauge));
        blstNav.Items.Add(this.GetSectionLink(ProductCategories.Keywords));
        blstNav.Items.Add(this.GetSectionLink(ProductCategories.Neckline));
        blstNav.Items.Add(this.GetSectionLink(ProductCategories.Silhouette));
        blstNav.Items.Add(this.GetSectionLink(ProductCategories.Sleeve));
        blstNav.Items.Add(this.GetSectionLink(ProductCategories.Trend));
        blstNav.Items.Add(this.GetSectionLink(ProductCategories.Website));
        blstNav.Items.Add(this.GetSectionLink(ProductCategories.WebsiteSubCategory));
    }

    private ListItem GetSectionLink(ProductCategories sectionIndex)
    {
        ListItem li = new ListItem(this.InventorySectionsNames[sectionIndex], this.InventorySectionsUrl[sectionIndex]);
        if (sectionIndex == this.ActiveSectionIndex)
        {
            li.Attributes["class"] = "open";
        }
        return li;
    }

    private Dictionary<ProductCategories, string> InventorySectionsNames
    {
        get
        {
            Dictionary<ProductCategories, string> sec = new Dictionary<ProductCategories, string>();
           
            lstPrdCtgy = _prdCtgy.GetALLCategoryHeader(null, Globals.CurrentAppLanguageCode);
            
            sec[ProductCategories.Website] = lstPrdCtgy[0].CatgHdrName;
            sec[ProductCategories.Gauge] = lstPrdCtgy[8].CatgHdrName;
            sec[ProductCategories.Neckline] = lstPrdCtgy[1].CatgHdrName;
            sec[ProductCategories.Extra] = lstPrdCtgy[2].CatgHdrName;
            sec[ProductCategories.Sleeve] = lstPrdCtgy[3].CatgHdrName;
            sec[ProductCategories.Silhouette] = lstPrdCtgy[4].CatgHdrName;
            sec[ProductCategories.Trend] = lstPrdCtgy[5].CatgHdrName;
            sec[ProductCategories.Keywords] = lstPrdCtgy[6].CatgHdrName;
            sec[ProductCategories.WebsiteSubCategory] = lstPrdCtgy[7].CatgHdrName;
            

            return sec;
        }
    }

    private Dictionary<ProductCategories, string> InventorySectionsUrl
    {
        get
        {
            Dictionary<ProductCategories, string> sec = new Dictionary<ProductCategories, string>();
            sec[ProductCategories.Website] = QueryString.GetModifiedUrl("section", ((int)ProductCategories.Website).ToString());
            sec[ProductCategories.Gauge] = QueryString.GetModifiedUrl("section", ((int)ProductCategories.Gauge).ToString());
            sec[ProductCategories.Neckline] = QueryString.GetModifiedUrl("section", ((int)ProductCategories.Neckline).ToString());
            sec[ProductCategories.Sleeve] = QueryString.GetModifiedUrl("section", ((int)ProductCategories.Sleeve).ToString());
            sec[ProductCategories.Extra] = QueryString.GetModifiedUrl("section", ((int)ProductCategories.Extra).ToString());
            sec[ProductCategories.Silhouette] = QueryString.GetModifiedUrl("section", ((int)ProductCategories.Silhouette).ToString());
            sec[ProductCategories.Trend] = QueryString.GetModifiedUrl("section", ((int)ProductCategories.Trend).ToString());
            sec[ProductCategories.Keywords] = QueryString.GetModifiedUrl("section", ((int)ProductCategories.Keywords).ToString());
            sec[ProductCategories.WebsiteSubCategory] = QueryString.GetModifiedUrl("section", ((int)ProductCategories.WebsiteSubCategory).ToString());
            return sec;
        }
    }

    private ProductCategories ActiveSectionIndex
    {
        get
        {
            int section = 0;
            if (int.TryParse(Request.QueryString["section"], out section))
            {
                if (Enum.IsDefined(typeof(InventorySectionKey), section))
                {
                    return (ProductCategories)Enum.ToObject(typeof(ProductCategories), section);
                }
                else
                {
                    return ProductCategories.Website;
                }
            }
            return ProductCategories.Website;
        }
    }

   
    private void LoadUserControl(ProductCategories index)
    {
        string ctrlPath = this.InventorySections[index];

        _contentControl = Page.LoadControl(ctrlPath);
        phControl.Controls.Clear();
        phControl.Controls.Add(_contentControl);
        _contentControl.ID = "ctlContent";
        IProduct ctrl = (IProduct)_contentControl;
        lstPrdCtgy = _prdCtgy.GetALLCategoryHeader(null, Globals.CurrentAppLanguageCode);

        switch (index)
            {
                case ProductCategories.Website:
                    ltTitle.Text = lstPrdCtgy[0].CatgHdrName;
                    btnAdd.Text = Resources.Resource.lblAddNew + " " + ltTitle.Text;
                    break;
                case ProductCategories.Gauge:
                    ltTitle.Text = lstPrdCtgy[8].CatgHdrName;
                    btnAdd.Text = Resources.Resource.lblAddNew + " " + ltTitle.Text;
                    break;
                case ProductCategories.Neckline:
                    ltTitle.Text = lstPrdCtgy[1].CatgHdrName;
                    btnAdd.Text = Resources.Resource.lblAddNew + " " + ltTitle.Text;
                    break;
                case ProductCategories.Extra:
                    ltTitle.Text = lstPrdCtgy[2].CatgHdrName;
                    btnAdd.Text = Resources.Resource.lblAddNew + " " + ltTitle.Text;
                    break;
                case ProductCategories.Sleeve:
                    ltTitle.Text = lstPrdCtgy[3].CatgHdrName;
                    btnAdd.Text = Resources.Resource.lblAddNew + " " + ltTitle.Text;
                    break;
                case ProductCategories.Silhouette:
                    ltTitle.Text = lstPrdCtgy[4].CatgHdrName;
                    btnAdd.Text = Resources.Resource.lblAddNew + " " + ltTitle.Text;
                    break;
                case ProductCategories.Trend:
                    ltTitle.Text = lstPrdCtgy[5].CatgHdrName;
                    btnAdd.Text = Resources.Resource.lblAddNew + " " + ltTitle.Text;
                    break;
                case ProductCategories.Keywords:
                    ltTitle.Text = lstPrdCtgy[6].CatgHdrName;
                    btnAdd.Text = Resources.Resource.lblAddNew + " " + ltTitle.Text;
                    break;
                case ProductCategories.WebsiteSubCategory:
                    ltTitle.Text = lstPrdCtgy[7].CatgHdrName;
                    btnAdd.Text = Resources.Resource.lblAddNew + " " + ltTitle.Text;
                    break;
                default:
                     ltTitle.Text = lstPrdCtgy[0].CatgHdrName;
                    btnAdd.Text = Resources.Resource.lblAddNew + ltTitle.Text;

                    break;
            }
        ctrl.Initialize();
        //mdProductKitContent.Url = string.Format("~/Inventory/ProductKit.aspx?PrdID={0}&jscallback={1}", this.ProductID, "reloadGrid");
    }

    private Dictionary<ProductCategories, string> InventorySections
    {
        get
        {
            Dictionary<ProductCategories, string> sec = new Dictionary<ProductCategories, string>();
            sec[ProductCategories.Website] = "~/Inventory/UserControls/ProductCategories.ascx";
            sec[ProductCategories.Gauge] = "~/Inventory/UserControls/ProductCategories.ascx";
            sec[ProductCategories.Neckline] = "~/Inventory/UserControls/ProductCategories.ascx";
            sec[ProductCategories.Sleeve] = "~/Inventory/UserControls/ProductCategories.ascx";
            sec[ProductCategories.Extra] = "~/Inventory/UserControls/ProductCategories.ascx";
            sec[ProductCategories.Silhouette] = "~/Inventory/UserControls/ProductCategories.ascx";
            sec[ProductCategories.Trend] = "~/Inventory/UserControls/ProductCategories.ascx";
            sec[ProductCategories.Keywords] = "~/Inventory/UserControls/ProductCategories.ascx";
            sec[ProductCategories.WebsiteSubCategory] = "~/Inventory/UserControls/ProductCategories.ascx";
            return sec;
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
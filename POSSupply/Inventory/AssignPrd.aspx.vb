Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Partial Class Inventory_AssignPrd
    Inherits BasePage
    Dim ObjSubcategory As New clsSubcategory()
    Dim Objcategory As New clsCategory()
    Dim ObjProduct As New clsProducts
    Protected Sub Inventory_AssignPrd_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Not IsPostBack Then
            Objcategory.subGetCategoryName(dlCatName)
        End If
    End Sub
    Protected Sub dlCatName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlCatName.SelectedIndexChanged
        ObjSubcategory.subGetSubCategoryName(dlSubcatName, dlCatName.SelectedValue)
    End Sub
    Protected Sub dlSubcatName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlSubcatName.SelectedIndexChanged
        ObjProduct.subGetProductForList(lstFromPrd, dlCatName.SelectedValue, dlSubcatName.SelectedValue, "From")
        ObjProduct.subGetProductForList(lstToPrd, dlCatName.SelectedValue, dlSubcatName.SelectedValue, "To")
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    Protected Sub imgCmdReset_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdReset.ServerClick
        Objcategory.subGetCategoryName(dlCatName)
        lstFromPrd.Items.Clear()
        lstToPrd.Items.Clear()
        dlSubcatName.Items.Clear()
    End Sub
    Protected Sub imgCmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdSave.ServerClick
        If ObjProduct.funAssignPrd(hdnListValue.Value, dlCatName.SelectedValue, dlSubcatName.SelectedValue) = True Then
            lblMsg.Text = msgProductHasBeenAssignedSuccessfully
        End If
        Objcategory.subGetCategoryName(dlCatName)
        lstFromPrd.Items.Clear()
        lstToPrd.Items.Clear()
        dlSubcatName.Items.Clear()
    End Sub
End Class

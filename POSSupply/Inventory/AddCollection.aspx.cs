﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;


public partial class Inventory_AddCollection : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                MessageState.SetGlobalMessage(MessageType.Success, "");

                if (BusinessUtility.GetString(Request.QueryString["Status"]) == "INDIVIDUAL")
                {
                    trIsAxctive.Visible = true;
                    if (BusinessUtility.GetInt(Request.QueryString["CollectioID"]) > 0)
                    {
                        Collection objCollection = new Collection();
                        objCollection.CollectionID = BusinessUtility.GetInt(Request.QueryString["CollectioID"]);
                        var lst = objCollection.GetAllCollectionList(null, Globals.CurrentAppLanguageCode);
                        foreach (var item in lst)
                        {
                            txtCollectionNameEn.Text = item.CollectionEn;
                            txtCollectionNameFr.Text = item.CollectionFr;
                            txtShortName.Text = item.ShortName;
                            rblstActive.SelectedValue = BusinessUtility.GetBool(item.IsActive) ? "1" : "0";
                        }

                    }
                }

                txtCollectionNameFr.Focus();
                
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Collection objCollection = new Collection();
            objCollection.CollectionEn = txtCollectionNameEn.Text;
            objCollection.CollectionFr = txtCollectionNameFr.Text;
            objCollection.ShortName = txtShortName.Text;
            objCollection.IsActive = BusinessUtility.GetInt(rblstActive.SelectedItem.Value);
            objCollection.CollectionID = BusinessUtility.GetInt(Request.QueryString["CollectioID"]);
            if (objCollection.Save(null, CurrentUser.UserID) == true)
            {
                if (BusinessUtility.GetString(Request.QueryString["Status"]) == "INDIVIDUAL")
                {
                    if (objCollection.CollectionID > 0)
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgCollectionUpdate);
                        Globals.RegisterReloadParentScript(this);
                        return;
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated);
                        Globals.RegisterReloadParentScript(this);
                        return;
                    }
                }
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.CollectionCreated({0});", objCollection.CollectionID), true);
            }
            else
            {
                //MessageState.SetGlobalMessage(MessageType.Failure, "Collection Creation failure");
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }

        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
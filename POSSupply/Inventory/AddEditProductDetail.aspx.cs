﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Admin_AddEditProductDetail : System.Web.UI.Page
{
    ProductMasterDetail _prd = new ProductMasterDetail();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                MessageState.SetGlobalMessage(MessageType.Success, "");
                LoadDefaultvalue();
                GetProductSavedDetail();

                if (IsPrductPublished() == true)
                {
                    pnlItemDesc.Enabled = false;
                    btnPublish.Visible = false;
                    txtProdName.Enabled = false;
                    txtBarcode.Enabled = false;
                    txtInternalID.Enabled = false;
                    btnSave.Visible = false;
                    btnSaveAfterPublish.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }

            txtProdName.Focus();
        }
    }

    public void SetData()
    {
        _prd.MasterID = BusinessUtility.GetInt(this.MasterID);
        _prd.PrdName = txtProdName.Text;
        _prd.PrdUPCCode = txtBarcode.Text;
        _prd.PrdSalePricePerMinQty = BusinessUtility.GetDouble(txtSalPriceMinQua.Text);
        _prd.PrdEndUserSalesPrice = BusinessUtility.GetDouble(txtEndUserSalesPrice.Text);
        _prd.PrdIntID = txtInternalID.Text;
        _prd.PrdWebSalesPrice = BusinessUtility.GetDouble(txtWebSalesPrice.Text);
        _prd.PrdWeight = txtWeight.Text;
        _prd.PrdLength = txtLength.Text;
        _prd.PrdWeightPkg = txtPkgWeight.Text;
        _prd.PrdLengthPkg = txtPkgLength.Text;
        _prd.PrdWidthPkg = txtPkgWidth.Text;
        _prd.PrdHeightPkg = txtPkgHeigth.Text;
        _prd.PrdStartLength = txtStartLength.Text;
        _prd.PrdIncrementLength = txtIncrementLength.Text;
        _prd.PrdStartChest = txtStartChest.Text;
        _prd.PrdIncrementChest = txtIncrementChest.Text;
        _prd.PrdStartShoulder = txtStartShoulder.Text;
        _prd.PrdIncrementShoulder = txtIncrementShoulder.Text;
        _prd.PrdStartBicep = txtStartBicep.Text;
        _prd.PrdIncrementBicep = txtIncrementBicep.Text;
        _prd.PrdMinQtyPO = BusinessUtility.GetInt(txtMinQtyPO.Text);
        _prd.PrdQtyPO = BusinessUtility.GetInt(txtQtyPo.Text);
        _prd.PrdAutoPO = rdlstPOFlag.SelectedValue == "1";
        _prd.PrdIsSpecial = rblstSpecial.SelectedValue == "1";
        _prd.PrdIsKit = rblstProKit.SelectedValue == "1";
        _prd.IsPOSMenu = rdlstISPOSMenu.SelectedValue == "1";
        _prd.PrdIsWeb = rdlstISWeb.SelectedValue == "1";
        _prd.PrdIsActive = rblstActive.SelectedValue == "1";
        _prd.PrdNotes = txtProdIncolTerms.Text;
        _prd.PrdFrDesc = txtFrenchDesc.Text;
        _prd.PrdEnDesc = txtEnglishDesc.Text;
        _prd.PrdFOBPrice = BusinessUtility.GetDouble(txtFOBPrice.Text);
        _prd.PrdLandedPrice = BusinessUtility.GetDouble(txtLandedPrice.Text);
        //_prd.PrdGauge = BusinessUtility.GetInt(txtGauage.Text);
        _prd.ProdWeightIncrement = txtPrdWgtInc.Text;


        var lstNickLine = new List<int>();
        string sNickLine = "";
        int id = 0;

        ProductCategory objPCategory = new ProductCategory();
        List<ProductCategory> lstPC = new List<ProductCategory>();
        objPCategory.CatgHrdID = (int)ProductCategories.Neckline;
        lstPC = objPCategory.GetCategoryHeader(null, Globals.CurrentAppLanguageCode);
        int lmtCount = 0;
        foreach (ListItem item in ddlNickline.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstNickLine.Contains(id))
            {
                lstNickLine.Add(id);
                if (sNickLine != "")
                    sNickLine += " ," + BusinessUtility.GetString(id);
                else
                {
                    sNickLine = BusinessUtility.GetString(id);
                }

            }
        }
        _prd.PrdNickLine = sNickLine;


        var lstGauge = new List<int>();
        string sGauge = "";
        id = 0;
        foreach (ListItem item in ddlGauge.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstGauge.Contains(id))
            {
                lstGauge.Add(id);
                if (sGauge != "")
                    sGauge += " ," + BusinessUtility.GetString(id);
                else
                {
                    sGauge = BusinessUtility.GetString(id);
                }
            }
        }
        _prd.PrdGauge = sGauge;



        var lstExtraCategory = new List<int>();
        string sExtraCategory = "";
        id = 0;
        foreach (ListItem item in ddlExtraCategory.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstExtraCategory.Contains(id))
            {
                lstExtraCategory.Add(id);
                if (sExtraCategory != "")
                    sExtraCategory += " ," + BusinessUtility.GetString(id);
                else
                {
                    sExtraCategory = BusinessUtility.GetString(id);
                }
            }
        }
        _prd.PrdExtraCategory = sExtraCategory;


        var lstSleeve = new List<int>();
        string sSleeve = "";
        id = 0;
        foreach (ListItem item in ddlSleeve.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstSleeve.Contains(id))
            {
                lstSleeve.Add(id);
                if (sSleeve != "")
                    sSleeve += " ," + BusinessUtility.GetString(id);
                else
                {
                    sSleeve = BusinessUtility.GetString(id);
                }
            }
        }
        _prd.PrdSleeve = sSleeve;


        var lstSilhouette = new List<int>();
        string sSilhouette = "";
        id = 0;
        foreach (ListItem item in ddlSilhouette.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstSilhouette.Contains(id))
            {
                lstSilhouette.Add(id);
                if (sSilhouette != "")
                    sSilhouette += " ," + BusinessUtility.GetString(id);
                else
                {
                    sSilhouette = BusinessUtility.GetString(id);
                }
            }
        }
        _prd.PrdSilhouette = sSilhouette;



        var lstTrend = new List<int>();
        string sTrend = "";
        id = 0;
        foreach (ListItem item in ddlTrend.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstTrend.Contains(id))
            {
                lstTrend.Add(id);
                if (sTrend != "")
                    sTrend += " ," + BusinessUtility.GetString(id);
                else
                {
                    sTrend = BusinessUtility.GetString(id);
                }
            }
        }
        _prd.PrdTrend = sTrend;


        var lstKeyWord = new List<int>();
        string sKeyWord = "";
        id = 0;
        foreach (ListItem item in ddlKeyWord.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstKeyWord.Contains(id))
            {
                lstKeyWord.Add(id);
                if (sKeyWord != "")
                    sKeyWord += " ," + BusinessUtility.GetString(id);
                else
                {
                    sKeyWord = BusinessUtility.GetString(id);
                }
            }
        }
        _prd.PrdKeyWord = sKeyWord;

    }

    public bool IsPrductPublished()
    {
        ProductMasterHeader objProductMasterHeader = new ProductMasterHeader();
        objProductMasterHeader.MasterID = BusinessUtility.GetInt(this.MasterID);
        var lstProductMasterHdr = objProductMasterHeader.GetMasterHeader(null, Globals.CurrentAppLanguageCode);

        if (lstProductMasterHdr.Count > 0)
        {
            if (lstProductMasterHdr[0].MasterPublished == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (ValidatePage() == false)
            {
                return;
            }
            if (saveProduct() == true)
            {
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated);
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadStyle();"), true);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.PRODUCT_ALREADY_EXISTS)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgProductNameAlreadyExists);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }
    }

    protected void btnSaveAfterPublish_Click(object sender, EventArgs e)
    {
        try
        {
            if (ValidatePage() == false)
            {
                return;
            }

            ProductPublishEdit objProductPublishEdit = new ProductPublishEdit();
            objProductPublishEdit.MasterID = BusinessUtility.GetInt(this.MasterID);

            if (saveProduct() == true && objProductPublishEdit.Save(CurrentUser.UserID) == true)
            {
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated);
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadStyle();"), true);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.PRODUCT_ALREADY_EXISTS)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgProductNameAlreadyExists);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }
    }

    public bool saveProduct()
    {
        SetData();
        ProductMaterial objProductMaterial = new ProductMaterial();
        var lstProductMaterial = new List<int>();
        int id = 0;
        foreach (ListItem item in ddlMaterial.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstProductMaterial.Contains(id))
            {
                lstProductMaterial.Add(id);

            }
        }

        ProductTexture objProductTexture = new ProductTexture();
        var lstProductTexture = new List<int>();
        id = 0;
        foreach (ListItem item in ddlTexture.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstProductTexture.Contains(id))
            {
                lstProductTexture.Add(id);
            }
        }


        ProductColor objProductColor = new ProductColor();
        var lstProductColor = new List<int>();
        id = 0;
        foreach (ListItem item in ddlColor.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstProductColor.Contains(id))
            {
                lstProductColor.Add(id);
            }
        }

        ProductSize objProductSize = new ProductSize();
        var lstProductSize = new List<string>();
        string sid = "";
        foreach (ListItem item in ddlSizeCatg.Items)
        {
            sid = item.Value;
            if (item.Selected && sid != "" && !lstProductSize.Contains(sid))
            {
                lstProductSize.Add(sid);
            }
        }

        ProductCategory objProductCategory = new ProductCategory();
        var lstProductCategory = new List<int>();
        id = 0;
        foreach (ListItem item in ddlCategory.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstProductCategory.Contains(id))
            {
                lstProductCategory.Add(id);
            }
        }
        objProductCategory.CatgHrdID = (int)ProductCategories.Website;


        ProductCategory objProductWebSubCatg = new ProductCategory();
        var lstProductWebSubCatg = new List<int>();
        id = 0;
        foreach (ListItem item in ddlWebSiteSubCatg.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstProductWebSubCatg.Contains(id))
            {
                lstProductWebSubCatg.Add(id);
            }
        }
        objProductWebSubCatg.CatgHrdID = (int)ProductCategories.WebsiteSubCategory;
        var lstProductAssociatedStyle = new List<string>();
        foreach (ListItem item in ddlAssociatedStyle.Items)
        {
            string sItem = item.Value;
            if (item.Selected && !lstProductAssociatedStyle.Contains(sItem))
            {
                lstProductAssociatedStyle.Add(sItem);
            }
        }

        // Get Product Default Name
        Collection objCollection = new Collection();
        int iCollectionID = 0;
        var vCollection = objCollection.GetCollection(null, BusinessUtility.GetInt(this.MasterID), Globals.CurrentAppLanguageCode);
        if (vCollection.Count > 0)
        {
            iCollectionID = BusinessUtility.GetInt(vCollection[0].CollectionID);
        }

        bool IsProductDetailSaved = _prd.Save(CurrentUser.UserID);
        bool IsProductMaterialSaved = objProductMaterial.AddMaterialInStyle(BusinessUtility.GetInt(this.MasterID), lstProductMaterial.ToArray());
        bool IsProductTextureSaved = objProductTexture.AddTextureInStyle(BusinessUtility.GetInt(this.MasterID), lstProductTexture.ToArray());
        bool IsProductColorSaved = objProductColor.AddColorInStyle(BusinessUtility.GetInt(this.MasterID), lstProductColor.ToArray());
        bool IsProductSizeCatgSave = objProductSize.AddSizeCategoryInStyle(BusinessUtility.GetInt(this.MasterID), lstProductSize.ToArray());
        bool IsProductCategorySave = objProductCategory.AddCatgInStyle(BusinessUtility.GetInt(this.MasterID), lstProductCategory.ToArray());
        bool IsProductWebSubCatgSave = objProductWebSubCatg.AddCatgInStyle(BusinessUtility.GetInt(this.MasterID), lstProductWebSubCatg.ToArray());
        bool IsProductAssociatedStyleSave = objProductWebSubCatg.AddAssociatedStyleInStyle(BusinessUtility.GetInt(this.MasterID), iCollectionID, lstProductAssociatedStyle.ToArray());

        if (IsProductDetailSaved = true && IsProductMaterialSaved == true && IsProductTextureSaved == true && IsProductColorSaved == true &&
            IsProductSizeCatgSave == true && IsProductCategorySave == true && IsProductWebSubCatgSave == true && IsProductAssociatedStyleSave == true)
        {
            return true;
            //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated);
            //Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadStyle();"), true);
        }
        else
        {
            return false;
            //MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
    }

    private string MasterID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["StyleID"]);
        }
    }

    public void GetProductSavedDetail()
    {
        _prd.MasterID = BusinessUtility.GetInt(this.MasterID);
        _prd.PopulateObject(null, BusinessUtility.GetInt(this.MasterID));
        if (BusinessUtility.GetString(_prd.PrdName) != "")
            txtProdName.Text = _prd.PrdName;
        if (BusinessUtility.GetString(_prd.PrdUPCCode) != "")
            txtBarcode.Text = _prd.PrdUPCCode;

        txtSalPriceMinQua.Text = BusinessUtility.GetString(_prd.PrdSalePricePerMinQty);
        txtEndUserSalesPrice.Text = BusinessUtility.GetString(_prd.PrdEndUserSalesPrice);
        txtInternalID.Text = _prd.PrdIntID;
        txtWebSalesPrice.Text = BusinessUtility.GetString(_prd.PrdWebSalesPrice);
        txtWeight.Text = _prd.PrdWeight;
        txtLength.Text = _prd.PrdLength;
        txtPkgWeight.Text = _prd.PrdWeightPkg;
        txtPkgLength.Text = _prd.PrdLengthPkg;
        txtPkgWidth.Text = _prd.PrdWidthPkg;
        txtPkgHeigth.Text = _prd.PrdHeightPkg;
        txtStartLength.Text = _prd.PrdStartLength;
        txtIncrementLength.Text = _prd.PrdIncrementLength;
        txtStartChest.Text = _prd.PrdStartChest;
        txtIncrementChest.Text = _prd.PrdIncrementChest;
        txtStartShoulder.Text = _prd.PrdStartShoulder;
        txtIncrementShoulder.Text = _prd.PrdIncrementShoulder;
        txtStartBicep.Text = _prd.PrdStartBicep;
        txtIncrementBicep.Text = _prd.PrdIncrementBicep;
        txtMinQtyPO.Text = BusinessUtility.GetString(_prd.PrdMinQtyPO);
        txtQtyPo.Text = BusinessUtility.GetString(_prd.PrdQtyPO);
        rdlstPOFlag.SelectedValue = _prd.PrdAutoPO ? "1" : "0";
        rblstSpecial.SelectedValue = _prd.PrdIsSpecial ? "1" : "0";
        rblstProKit.SelectedValue = _prd.PrdIsKit ? "1" : "0";
        rdlstISPOSMenu.SelectedValue = _prd.IsPOSMenu ? "1" : "0";
        rdlstISWeb.SelectedValue = _prd.PrdIsWeb ? "1" : "0";
        rblstActive.SelectedValue = _prd.PrdIsActive ? "1" : "0";
        txtProdIncolTerms.Text = _prd.PrdNotes;
        txtFrenchDesc.Text = _prd.PrdFrDesc;
        txtEnglishDesc.Text = _prd.PrdEnDesc;
        txtFOBPrice.Text = BusinessUtility.GetString(_prd.PrdFOBPrice);
        txtLandedPrice.Text = BusinessUtility.GetString(_prd.PrdLandedPrice);
        //txtGauage.Text = BusinessUtility.GetString(_prd.PrdGauge);
        txtPrdWgtInc.Text = _prd.ProdWeightIncrement;

        if (_prd.PrdNickLine != "")
        {
            string[] sNickLine = _prd.PrdNickLine.Split(',');
            foreach (string nickLine in sNickLine)
            {
                ListItem li = ddlNickline.Items.FindByValue(nickLine);
                if (li != null)
                {
                    li.Selected = true;
                }
            }
        }


        if (_prd.PrdGauge != "")
        {
            string[] sPrdGauge = _prd.PrdGauge.Split(',');
            foreach (string gauge in sPrdGauge)
            {
                ListItem li = ddlGauge.Items.FindByValue(gauge);
                if (li != null)
                {
                    li.Selected = true;
                }
            }
        }

        if (_prd.PrdExtraCategory != "")
        {
            string[] sExtraCategory = _prd.PrdExtraCategory.Split(',');
            foreach (string extraCategory in sExtraCategory)
            {
                ListItem li = ddlExtraCategory.Items.FindByValue(extraCategory);
                if (li != null)
                {
                    li.Selected = true;
                }
            }
        }

        if (_prd.PrdSleeve != "")
        {
            string[] sSleeve = _prd.PrdSleeve.Split(',');
            foreach (string sleeve in sSleeve)
            {
                ListItem li = ddlSleeve.Items.FindByValue(sleeve);
                if (li != null)
                {
                    li.Selected = true;
                }
            }
        }

        if (_prd.PrdSilhouette != "")
        {
            string[] sSilhouette = _prd.PrdSilhouette.Split(',');
            foreach (string silhouette in sSilhouette)
            {
                ListItem li = ddlSilhouette.Items.FindByValue(silhouette);
                if (li != null)
                {
                    li.Selected = true;
                }
            }
        }


        if (_prd.PrdTrend != "")
        {
            string[] sTrend = _prd.PrdTrend.Split(',');
            foreach (string Trend in sTrend)
            {
                ListItem li = ddlTrend.Items.FindByValue(Trend);
                if (li != null)
                {
                    li.Selected = true;
                }
            }
        }

        if (_prd.PrdKeyWord != "")
        {
            string[] sKeyWord = _prd.PrdKeyWord.Split(',');
            foreach (string KeyWord in sKeyWord)
            {
                ListItem li = ddlKeyWord.Items.FindByValue(KeyWord);
                if (li != null)
                {
                    li.Selected = true;
                }
            }
        }

    }

    private void LoadDefaultvalue()
    {

        ProductMaterial objProductMaterial = new ProductMaterial();

        ddlMaterial.DataSource = objProductMaterial.GetMaterialList(null, Globals.CurrentAppLanguageCode);
        //ddlMaterial.DataTextField = "MaterialShortName";
        ddlMaterial.DataTextField = "MaterialName";
        ddlMaterial.DataValueField = "MaterialID";
        ddlMaterial.DataBind();

        var lst = objProductMaterial.GetStyleMaterial(null, BusinessUtility.GetInt(this.MasterID), Globals.CurrentAppLanguageCode);
        foreach (var item in lst)
        {
            ListItem li = ddlMaterial.Items.FindByValue(item.MaterialID.ToString());
            if (li != null)
            {
                li.Selected = true;
            }
        }

        ProductTexture objProductTexture = new ProductTexture();
        ddlTexture.DataSource = objProductTexture.GetTextureList(null, Globals.CurrentAppLanguageCode);
        //ddlTexture.DataTextField = "ShortName";
        ddlTexture.DataTextField = "TextureName";
        ddlTexture.DataValueField = "TextureID";
        ddlTexture.DataBind();

        var lstTexture = objProductTexture.GetStyleTexture(null, BusinessUtility.GetInt(this.MasterID), Globals.CurrentAppLanguageCode);
        foreach (var item in lstTexture)
        {
            ListItem li = ddlTexture.Items.FindByValue(item.TextureID.ToString());
            if (li != null)
            {
                li.Selected = true;
            }
        }

        ProductColor objProductColor = new ProductColor();
        ddlColor.DataSource = objProductColor.GetColorList(null, Globals.CurrentAppLanguageCode);
        //ddlColor.DataTextField = "ShortName";
        ddlColor.DataTextField = "ColorName";
        ddlColor.DataValueField = "ColorID";
        ddlColor.DataBind();

        var lstColor = objProductColor.GetStyleColor(null, BusinessUtility.GetInt(this.MasterID), Globals.CurrentAppLanguageCode);
        foreach (var item in lstColor)
        {
            ListItem li = ddlColor.Items.FindByValue(item.ColorID.ToString());
            if (li != null)
            {
                li.Selected = true;
            }
        }

        ProductCategory objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = 1;
        ddlCategory.DataSource = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        ddlCategory.DataTextField = "CatgName";
        ddlCategory.DataValueField = "CatgID";
        ddlCategory.DataBind();

        objProductCategory.CatgHrdID = 1;
        var lstProductCategory = objProductCategory.GetStyleCatg(null, BusinessUtility.GetInt(this.MasterID), Globals.CurrentAppLanguageCode);
        foreach (var item in lstProductCategory)
        {
            ListItem li = ddlCategory.Items.FindByValue(item.CatgID.ToString());
            if (li != null)
            {
                li.Selected = true;
            }
        }

        ProductSize objProductSize = new ProductSize();
        objProductCategory.CatgHrdID = 1;
        ddlSizeCatg.DataSource = objProductSize.GetSizeCatg(null, Globals.CurrentAppLanguageCode);
        ddlSizeCatg.DataTextField = "CategoryName";
        ddlSizeCatg.DataValueField = "CategoryName";
        ddlSizeCatg.DataBind();

        var lstProductSize = objProductSize.GetStyleSizeCategory(null, BusinessUtility.GetInt(this.MasterID), Globals.CurrentAppLanguageCode);
        foreach (var item in lstProductSize)
        {
            ListItem li = ddlSizeCatg.Items.FindByValue(item.CategoryName.ToString());
            if (li != null)
            {
                li.Selected = true;
            }
        }


        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = 2;
        ddlNickline.DataSource = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        ddlNickline.DataTextField = "CatgName";
        ddlNickline.DataValueField = "CatgID";
        ddlNickline.DataBind();

        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = 3;
        ddlExtraCategory.DataSource = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        ddlExtraCategory.DataTextField = "CatgName";
        ddlExtraCategory.DataValueField = "CatgID";
        ddlExtraCategory.DataBind();

        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = 4;
        ddlSleeve.DataSource = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        ddlSleeve.DataTextField = "CatgName";
        ddlSleeve.DataValueField = "CatgID";
        ddlSleeve.DataBind();

        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = 5;
        ddlSilhouette.DataSource = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        ddlSilhouette.DataTextField = "CatgName";
        ddlSilhouette.DataValueField = "CatgID";
        ddlSilhouette.DataBind();


        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = 6;
        ddlTrend.DataSource = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        ddlTrend.DataTextField = "CatgName";
        ddlTrend.DataValueField = "CatgID";
        ddlTrend.DataBind();

        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = 7;
        ddlKeyWord.DataSource = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        ddlKeyWord.DataTextField = "CatgName";
        ddlKeyWord.DataValueField = "CatgID";
        ddlKeyWord.DataBind();

        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = 8;
        ddlWebSiteSubCatg.DataSource = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        ddlWebSiteSubCatg.DataTextField = "CatgName";
        ddlWebSiteSubCatg.DataValueField = "CatgID";
        ddlWebSiteSubCatg.DataBind();
        lstProductCategory = objProductCategory.GetStyleCatg(null, BusinessUtility.GetInt(this.MasterID), Globals.CurrentAppLanguageCode);
        foreach (var item in lstProductCategory)
        {
            ListItem li = ddlWebSiteSubCatg.Items.FindByValue(item.CatgID.ToString());
            if (li != null)
            {
                li.Selected = true;
            }
        }

        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = (int)ProductCategories.Gauge;
        ddlGauge.DataSource = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        ddlGauge.DataTextField = "CatgName";
        ddlGauge.DataValueField = "CatgID";
        ddlGauge.DataBind();



        // Get Product Default Name
        Collection objCollection = new Collection();
        var vCollection = objCollection.GetCollection(null, BusinessUtility.GetInt(this.MasterID), Globals.CurrentAppLanguageCode);
        if (vCollection.Count > 0)
        {
            ProductStyle objPRoductStyle = new ProductStyle();
            objPRoductStyle.MasterID = BusinessUtility.GetInt(this.MasterID);
            var vProductStyle = objPRoductStyle.GetStyleList(null, Globals.CurrentAppLanguageCode);
            txtProdName.Text = (vCollection[0].ShortName + "-" + vProductStyle[0].StyleName).ToUpper();
            txtBarcode.Text = BusinessUtility.GetString(vCollection[0].CollectionID.ToString("00")) + BusinessUtility.GetString(vProductStyle[0].StyleID.ToString("00"));
            int iCollectionID = BusinessUtility.GetInt(vCollection[0].CollectionID);
            ProductStyle objProductStyle = new ProductStyle();
            ddlAssociatedStyle.DataSource = objProductStyle.GetCollectionStyleList(null, BusinessUtility.GetInt(this.MasterID), BusinessUtility.GetInt(vCollection[0].CollectionID));
            ddlAssociatedStyle.DataTextField = "StyleName";
            ddlAssociatedStyle.DataValueField = "StyleName";
            ddlAssociatedStyle.DataBind();

            var lstStyle = objPRoductStyle.GetProductMasterAssociatedStyle(null, BusinessUtility.GetInt(this.MasterID), iCollectionID);
            foreach (var item in lstStyle)
            {
                ListItem li = ddlAssociatedStyle.Items.FindByValue(item.StyleName.ToString());
                if (li != null)
                {
                    li.Selected = true;
                }
            }
        }

        SetMassLengthUnitNames();
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    protected void btnPublish_Click(object sender, EventArgs e)
    {
        try
        {

            ProductMasterDetail objProductMstDtl = new ProductMasterDetail();
            objProductMstDtl.MasterID = BusinessUtility.GetInt(this.MasterID);
            objProductMstDtl.PopulateObject(null, BusinessUtility.GetInt(this.MasterID));


            if (BusinessUtility.GetString(objProductMstDtl.PrdName) == "")
            {
                MessageState.SetGlobalMessage(MessageType.Information, Resources.Resource.msgSaveProductBeforePublish);
                return;
            }


            if (BusinessUtility.GetBool(objProductMstDtl.PrdIsActive) == false)
            {
                MessageState.SetGlobalMessage(MessageType.Information, Resources.Resource.msgActiveProductPublished);
                return;
            }

            ProductPublish objProductPublish = new ProductPublish();
            objProductPublish.MasterID = BusinessUtility.GetInt(this.MasterID);
            if (objProductPublish.Save(CurrentUser.UserID) == true)
            {
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated);
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadStyle();"), true);
            }
        }
        catch (Exception ex)
        {
            //if (ex.Message == CustomExceptionCodes.PRODUCT_ALREADY_EXISTS)
            //{
            //    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgProductNameAlreadyExists);
            //}
            //else
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }

    }

    private bool ValidatePage()
    {

        ProductCategory objPCategory = new ProductCategory();
        List<ProductCategory> lstPC = new List<ProductCategory>();
        objPCategory.CatgHrdID = (int)ProductCategories.Neckline;
        lstPC = objPCategory.GetCategoryHeader(null, Globals.CurrentAppLanguageCode);
        if (lstPC.Count > 0)
        {
            int lmtCount = BusinessUtility.GetInt(lstPC[0].CatgHdrSelNo);
            int iddlCount = 0;
            foreach (ListItem item in ddlNickline.Items)
            {
                if (item.Selected)
                {
                    iddlCount += 1;
                }
            }
            if (iddlCount > lmtCount)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblLimitNOFSelection + " " + lstPC[0].CatgHdrSelNo + " " + lstPC[0].CatgHdrName);
                return false;
            }
        }


        lstPC = new List<ProductCategory>();
        objPCategory.CatgHrdID = (int)ProductCategories.Gauge;
        lstPC = objPCategory.GetCategoryHeader(null, Globals.CurrentAppLanguageCode);
        if (lstPC.Count > 0)
        {
            int lmtCount = BusinessUtility.GetInt(lstPC[0].CatgHdrSelNo);
            int iddlCount = 0;
            foreach (ListItem item in ddlGauge.Items)
            {
                if (item.Selected)
                {
                    iddlCount += 1;
                }
            }
            if (iddlCount > lmtCount)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblLimitNOFSelection + " " + lstPC[0].CatgHdrSelNo + " " + lstPC[0].CatgHdrName);
                return false;
            }
        }


        lstPC = new List<ProductCategory>();
        objPCategory.CatgHrdID = (int)ProductCategories.Sleeve;
        lstPC = objPCategory.GetCategoryHeader(null, Globals.CurrentAppLanguageCode);
        if (lstPC.Count > 0)
        {
            int lmtCount = BusinessUtility.GetInt(lstPC[0].CatgHdrSelNo);
            int iddlCount = 0;
            foreach (ListItem item in ddlSleeve.Items)
            {
                if (item.Selected)
                {
                    iddlCount += 1;
                }
            }
            if (iddlCount > lmtCount)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblLimitNOFSelection + " " + lstPC[0].CatgHdrSelNo + " " + lstPC[0].CatgHdrName);
                return false;
            }
        }

        lstPC = new List<ProductCategory>();
        objPCategory.CatgHrdID = (int)ProductCategories.Extra;
        lstPC = objPCategory.GetCategoryHeader(null, Globals.CurrentAppLanguageCode);
        if (lstPC.Count > 0)
        {
            int lmtCount = BusinessUtility.GetInt(lstPC[0].CatgHdrSelNo);
            int iddlCount = 0;
            foreach (ListItem item in ddlExtraCategory.Items)
            {
                if (item.Selected)
                {
                    iddlCount += 1;
                }
            }
            if (iddlCount > lmtCount)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblLimitNOFSelection + " " + lstPC[0].CatgHdrSelNo + " " + lstPC[0].CatgHdrName);
                return false;
            }
        }

        lstPC = new List<ProductCategory>();
        objPCategory.CatgHrdID = (int)ProductCategories.Silhouette;
        lstPC = objPCategory.GetCategoryHeader(null, Globals.CurrentAppLanguageCode);
        if (lstPC.Count > 0)
        {
            int lmtCount = BusinessUtility.GetInt(lstPC[0].CatgHdrSelNo);
            int iddlCount = 0;
            foreach (ListItem item in ddlSilhouette.Items)
            {
                if (item.Selected)
                {
                    iddlCount += 1;
                }
            }
            if (iddlCount > lmtCount)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblLimitNOFSelection + " " + lstPC[0].CatgHdrSelNo + " " + lstPC[0].CatgHdrName);
                return false;
            }
        }

        lstPC = new List<ProductCategory>();
        objPCategory.CatgHrdID = (int)ProductCategories.Keywords;
        lstPC = objPCategory.GetCategoryHeader(null, Globals.CurrentAppLanguageCode);
        if (lstPC.Count > 0)
        {
            int lmtCount = BusinessUtility.GetInt(lstPC[0].CatgHdrSelNo);
            int iddlCount = 0;
            foreach (ListItem item in ddlKeyWord.Items)
            {
                if (item.Selected)
                {
                    iddlCount += 1;
                }
            }
            if (iddlCount > lmtCount)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblLimitNOFSelection + " " + lstPC[0].CatgHdrSelNo + " " + lstPC[0].CatgHdrName);
                return false;
            }
        }

        lstPC = new List<ProductCategory>();
        objPCategory.CatgHrdID = (int)ProductCategories.Trend;
        lstPC = objPCategory.GetCategoryHeader(null, Globals.CurrentAppLanguageCode);
        if (lstPC.Count > 0)
        {
            int lmtCount = BusinessUtility.GetInt(lstPC[0].CatgHdrSelNo);
            int iddlCount = 0;
            foreach (ListItem item in ddlTrend.Items)
            {
                if (item.Selected)
                {
                    iddlCount += 1;
                }
            }
            if (iddlCount > lmtCount)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblLimitNOFSelection + " " + lstPC[0].CatgHdrSelNo + " " + lstPC[0].CatgHdrName);
                return false;
            }
        }


        lstPC = new List<ProductCategory>();
        objPCategory.CatgHrdID = (int)ProductCategories.Website;
        lstPC = objPCategory.GetCategoryHeader(null, Globals.CurrentAppLanguageCode);
        if (lstPC.Count > 0)
        {
            int lmtCount = BusinessUtility.GetInt(lstPC[0].CatgHdrSelNo);
            int iddlCount = 0;
            foreach (ListItem item in ddlCategory.Items)
            {
                if (item.Selected)
                {
                    iddlCount += 1;
                }
            }
            if (iddlCount > lmtCount)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblLimitNOFSelection + " " + lstPC[0].CatgHdrSelNo + " " + lstPC[0].CatgHdrName);
                return false;
            }
        }

        lstPC = new List<ProductCategory>();
        objPCategory.CatgHrdID = (int)ProductCategories.WebsiteSubCategory;
        lstPC = objPCategory.GetCategoryHeader(null, Globals.CurrentAppLanguageCode);
        if (lstPC.Count > 0)
        {
            int lmtCount = BusinessUtility.GetInt(lstPC[0].CatgHdrSelNo);
            int iddlCount = 0;
            foreach (ListItem item in ddlWebSiteSubCatg.Items)
            {
                if (item.Selected)
                {
                    iddlCount += 1;
                }
            }
            if (iddlCount > lmtCount)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblLimitNOFSelection + " " + lstPC[0].CatgHdrSelNo + " " + lstPC[0].CatgHdrName);
                return false;
            }
        }

        int idSelCount = 0;
        int iMaxSel = 4;
        foreach (ListItem item in ddlAssociatedStyle.Items)
        {
            if (item.Selected)
            {
                idSelCount += 1;
            }
        }
        if (idSelCount > iMaxSel)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblLimitNOFSelection + " " + iMaxSel + " " + Resources.Resource.lblAssociatedStyle);
            return false;
        }

        return true;
    }

    private void SetMassLengthUnitNames()
    {
        CompanyUnitType ut = new SysCompanyInfo().GetUnitType(null, CurrentUser.DefaultCompanyID);
        switch (ut)
        {
            case CompanyUnitType.Metric:
                //lblUnitHeight.Text = Resources.Resource.lblLenghtMetricUnit;
                lblUnitLength.Text = Resources.Resource.lblLenghtMetricUnit;
                lblUnitPkgHeight.Text = Resources.Resource.lblLenghtMetricUnit;
                lblUnitPkgLength.Text = Resources.Resource.lblLenghtMetricUnit;
                lblUnitPkgWeight.Text = Resources.Resource.lblMassMetricUnit;
                lblUnitPkgWidth.Text = Resources.Resource.lblLenghtMetricUnit;
                lblUnitWeight.Text = Resources.Resource.lblMassMetricUnit;
                //lblUnitWidth.Text = Resources.Resource.lblLenghtMetricUnit;
                break;
            case CompanyUnitType.English:
                //lblUnitHeight.Text = Resources.Resource.lblLenghtEngUnit;
                lblUnitLength.Text = Resources.Resource.lblLenghtEngUnit;
                lblUnitPkgHeight.Text = Resources.Resource.lblLenghtEngUnit;
                lblUnitPkgLength.Text = Resources.Resource.lblLenghtEngUnit;
                lblUnitPkgWeight.Text = Resources.Resource.lblMassEngUnit;
                lblUnitPkgWidth.Text = Resources.Resource.lblLenghtEngUnit;
                lblUnitWeight.Text = Resources.Resource.lblMassEngUnit;
                //lblUnitWidth.Text = Resources.Resource.lblLenghtEngUnit;
                break;
        }
    }

}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AssignPOSCategoryModal.aspx.cs" Inherits="Inventory_AssignPOSCategoryModal" ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="contentBottom" style="padding: 5px; height: 300px; overflow: auto;">
        <table class="contentTableForm" border="0" cellpadding="0" cellspacing="0">            
            <tr>
                <td class="text">
                    <asp:Label runat="server" CssClass="lblBold" ID="lblselect" Text="<%$ Resources:Resource, lblAllCatg %>"></asp:Label>
                </td>   
                <td></td>            
                <td class="text">
                    <asp:Label runat="server" CssClass="lblBold" ID="Label145" Text="<%$ Resources:Resource, lblSelectedCatg %>"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="input">
                    <asp:ListBox ID="lstFromPOSCatg" runat="server" onclick="funFrom()" Width="200px"
                        Height="250px"></asp:ListBox>
                </td>
                <td>
                    <input type="button" name="Move" value=">>" disabled="disabled" id="cmdMoveRight"
                        language="javascript" style="width: 25px; background-color: LightGrey; font-size: 11px;
                        font-weight: bold; height: 22px; display: none;" onclick="return cmdMoveRight_onclick()"><br />
                    <br />
                    <br />
                    <input type="button" name="Move" value="<<" disabled="disabled" id="cmdMoveLeft"
                        language="javascript" style="width: 25px; background-color: LightGrey; font-size: 11px;
                        font-weight: bold; height: 22px; display: none;" onclick="return cmdMoveLeft_onclick()">
                </td>
                <td class="input">
                    <asp:ListBox ID="lstToPOSCatg" onclick="funTo()" runat="server" Width="200px" Height="250px">
                    </asp:ListBox>
                </td>
            </tr>            
        </table>
        <asp:HiddenField ID="hdnListValue" runat="server" />
        <asp:HiddenField ID="hdnListText" runat="server" />
        <asp:ValidationSummary ID="sumvalAssignPrd" runat="server" ShowMessageBox="true"
            ShowSummary="false" />
    </div>
    <div style="text-align: right; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, cmdCssSubmit%>" 
            runat="server" onclick="btnSave_Click" OnClientClick="Javascript:funInsert();"  />
        <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
            CausesValidation="False" 
            OnClientClick=" jQuery.FrameDialog.closeDialog(); return false;" />
    </div> 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        function cmdMoveLeft_onclick() {
            var len = window.document.getElementById('<%=lstToPOSCatg.ClientID%>').length - 1;
            for (i = len; i >= 0; i--) {
                if (window.document.getElementById('<%=lstToPOSCatg.ClientID%>').selected = true)
                    window.document.getElementById('<%=lstFromPOSCatg.ClientID%>').appendChild(window.document.getElementById('<%=lstToPOSCatg.ClientID%>').item(window.document.getElementById('<%=lstToPOSCatg.ClientID%>').selectedIndex));

                // window.document.getElementById('<%=lstToPOSCatg.ClientID%>').appendChild(window.document.getElementById('<%=lstToPOSCatg.ClientID%>').item(i));
            }
        }

        function cmdMoveRight_onclick() {
            var len = window.document.getElementById('<%=lstFromPOSCatg.ClientID%>').length - 1;
            for (i = len; i >= 0; i--) {
                if (window.document.getElementById('<%=lstFromPOSCatg.ClientID%>').selected = true)
                    window.document.getElementById('<%=lstToPOSCatg.ClientID%>').appendChild(window.document.getElementById('<%=lstFromPOSCatg.ClientID%>').item(window.document.getElementById('<%=lstFromPOSCatg.ClientID%>').selectedIndex));
                // window.document.getElementById('<%=lstFromPOSCatg.ClientID%>').appendChild(window.document.getElementById('<%=lstFromPOSCatg.ClientID%>').item(i));
            }
        }
        function funFrom() {
            if (window.document.getElementById('<%=lstFromPOSCatg.ClientID%>').selectedIndex != -1)
                window.document.getElementById('<%=lstToPOSCatg.ClientID%>').appendChild(window.document.getElementById('<%=lstFromPOSCatg.ClientID%>').item(window.document.getElementById('<%=lstFromPOSCatg.ClientID%>').selectedIndex));
        }

        function funTo() {
            if (window.document.getElementById('<%=lstToPOSCatg.ClientID%>').selectedIndex != -1)
                window.document.getElementById('<%=lstFromPOSCatg.ClientID%>').appendChild(window.document.getElementById('<%=lstToPOSCatg.ClientID%>').item(window.document.getElementById('<%=lstToPOSCatg.ClientID%>').selectedIndex));
        }

        function funInsert() {
            var hdn = document.getElementById('<%=hdnListValue.ClientID%>');
            hdn.value = "";
            var hdn1 = document.getElementById('<%=hdnListText.ClientID%>');
            hdn1.value = "";

            var listBox = document.getElementById('<%=lstToPOSCatg.ClientID%>');
            var elements = "";

            var elements1 = "";
            var intCount = listBox.options.length;
            //store the elements in a hidden input that we can get server side 
            for (i = 0; i < intCount; i++) {
                elements += listBox.options[i].value + '~';
                elements1 += listBox.options[i].text + '^';
            }
            hdn.value = elements;
            hdn1.value = elements1;
        }
    </script>
</asp:Content>


<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="ProductImages.aspx.vb" Inherits="Inventory_ProductImages" %>

<%@ Register src="../Controls/CommonSearch/InventorySearch.ascx" tagname="InventorySearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>
    
    <uc1:InventorySearch ID="InventorySearch1" runat="server" />
    
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" Text="<%$ Resources:Resource, lblPrdImages %>"
                                ID="lblHeading"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
    </div>
    <div class="divMainContent">
        <script src="../scripts/PopBox.js" type="text/javascript"></script> 
        <script type="text/javascript">
            popBoxWaitImage.src = "../images/spinner40.gif";
            popBoxRevertImage = "../images/magminus.gif";
            popBoxPopImage = "../images/magplus.gif";
        </script>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr runat="server" id="trError" visible="false">
                <td height="25" colspan="3" align="center">
                    <asp:Label ForeColor="red" Font-Bold="true" runat="server" ID="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel runat="server" ID="pnlAddImages" Width="95%" GroupingText="<%$ Resources:Resource, pnlImage1 %>">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                            <tr runat="server" height="25" id="trLowImgBrowse1">
                                <td class="tdAlign" width="40%">
                                    <asp:Label runat="server" ID="lblLowImage1" Text="<%$ Resources:Resource, lblPrdLowResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td width="2%">
                                </td>
                                <td align="left" width="58%">
                                    <asp:FileUpload runat="server" ID="fileLowImage1" CssClass="innerText" />
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="<%$ Resources:Resource, custvalPrdImageLow1 %>"
                                        ClientValidationFunction="funImageLowUpload" Display="None"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr height="25" id="trLowImgBtn1" runat="server" visible="false" valign="middle">
                                <td class="tdAlign" width="40%">
                                    <asp:Label runat="server" ID="Label3" Text="<%$ Resources:Resource, lblPrdLowResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td width="2%">
                                </td>
                                <td width="58%" align="left">
                                    <a runat="server" id="aImgLow1" target="_blank">
                                        <img id="imgLow1" runat="server" alt="" class="PopBoxImageSmall" title="Click to magnify/shrink" />
                                    </a>&nbsp;&nbsp;
                                    <asp:ImageButton ID="imgLowImage1" runat="server" ToolTip="Delete" CausesValidation="false"
                                        ImageUrl="~/Images/delete_icon.png" />
                                </td>
                            </tr>
                            <tr>
                                <td height="5">
                                </td>
                            </tr>
                            <tr height="25" runat="server" id="trHighImgBrowse1">
                                <td class="tdAlign">
                                    <asp:Label runat="server" ID="lblHighResolution" Text="<%$ Resources:Resource, lblPrdHighResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:FileUpload runat="server" ID="fileHighImage1" CssClass="innerText" />
                                    <asp:CustomValidator ID="custImage1" runat="server" ErrorMessage="<%$ Resources:Resource, custvalPrdImageHigh1 %>"
                                        ClientValidationFunction="funImageHighUpload" Display="None"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr height="25" id="trHighBtn1" runat="server" visible="false" valign="middle">
                                <td class="tdAlign">
                                    <asp:Label runat="server" ID="Label4" Text="<%$ Resources:Resource, lblPrdHighResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <a runat="server" id="aImgHigh1" target="_blank">
                                        <img id="ImgHigh1" runat="server" alt="" class="PopBoxImageSmall" title="Click to magnify/shrink" /></a>&nbsp;&nbsp;
                                    <asp:ImageButton ID="imgHighImage1" runat="server" ToolTip="Delete" CausesValidation="false"
                                        ImageUrl="~/Images/delete_icon.png" />
                                </td>
                            </tr>
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel runat="server" ID="pnlImage2" Width="95%" GroupingText="<%$ Resources:Resource, pnlImage2 %>">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                            <tr id="trLowImgBrowse2" runat="server" height="25">
                                <td class="tdAlign" width="40%">
                                    <asp:Label runat="server" ID="lblLowImage2" Text="<%$ Resources:Resource, lblPrdLowResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td width="2%">
                                </td>
                                <td align="left" width="58%">
                                    <asp:FileUpload runat="server" ID="fileLowImage2" CssClass="innerText" />
                                </td>
                            </tr>
                            <tr height="25" id="trLowImgBtn2" runat="server" visible="false" valign="middle">
                                <td width="40%" class="tdAlign">
                                    <asp:Label runat="server" ID="Label5" Text="<%$ Resources:Resource, lblPrdLowResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td width="2%">
                                </td>
                                <td width="58%" align="left">
                                    <a runat="server" id="aimgLow2" target="_blank">
                                        <img id="imgLow2" runat="server" alt="" class="PopBoxImageSmall" title="Click to magnify/shrink" /></a>&nbsp;&nbsp;
                                    <asp:ImageButton ID="imgLowImage2" runat="server" ToolTip="Delete" CausesValidation="false"
                                        ImageUrl="~/Images/delete_icon.png" />
                                </td>
                            </tr>
                            <tr>
                                <td height="5">
                                </td>
                            </tr>
                            <tr height="25" runat="server" id="trHighImgBrowse2">
                                <td class="tdAlign">
                                    <asp:Label runat="server" ID="lblHighImage2" Text="<%$ Resources:Resource, lblPrdHighResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:FileUpload runat="server" ID="fileHighImage2" CssClass="innerText" />
                                </td>
                            </tr>
                            <tr height="25" id="trHighBtn2" runat="server" visible="false" valign="middle">
                                <td class="tdAlign">
                                    <asp:Label runat="server" ID="Label6" Text="<%$ Resources:Resource, lblPrdHighResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <a runat="server" id="aimgHigh2" target="_blank">
                                        <img id="imgHigh2" runat="server" alt="" class="PopBoxImageSmall" title="Click to magnify/shrink" /></a>
                                    &nbsp;&nbsp;
                                    <asp:ImageButton ID="imgHighImage2" runat="server" ToolTip="Delete" CausesValidation="false"
                                        ImageUrl="~/Images/delete_icon.png" />
                                </td>
                            </tr>
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel runat="server" ID="Panel2" Width="95%" GroupingText="<%$ Resources:Resource, pnlImage3 %>">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                            <tr id="trLowImgBrowse3" runat="server" height="25">
                                <td class="tdAlign" width="40%">
                                    <asp:Label runat="server" ID="lblLowImage3" Text="<%$ Resources:Resource, lblPrdLowResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td width="2%">
                                </td>
                                <td align="left" width="58%">
                                    <asp:FileUpload runat="server" ID="fileLowImage3" CssClass="innerText" />
                                </td>
                            </tr>
                            <tr height="25" id="trLowImgBtn3" runat="server" visible="false" valign="middle">
                                <td width="40%" class="tdAlign">
                                    <asp:Label runat="server" ID="Label7" Text="<%$ Resources:Resource, lblPrdLowResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td width="2%">
                                </td>
                                <td width="58%" align="left">
                                    <a runat="server" id="aimgLow3" target="_blank">
                                        <img id="imgLow3" runat="server" alt="" class="PopBoxImageSmall" title="Click to magnify/shrink" /></a>
                                    &nbsp;&nbsp;
                                    <asp:ImageButton ID="imgLowImage3" runat="server" ToolTip="Delete" CausesValidation="false"
                                        ImageUrl="~/Images/delete_icon.png" />
                                </td>
                            </tr>
                            <tr>
                                <td height="5">
                                </td>
                            </tr>
                            <tr height="25" runat="server" id="trHighImgBrowse3">
                                <td class="tdAlign">
                                    <asp:Label runat="server" ID="lblHighImage3" Text="<%$ Resources:Resource, lblPrdHighResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:FileUpload runat="server" ID="fileHighImage3" CssClass="innerText" />
                                </td>
                            </tr>
                            <tr height="25" id="trHighBtn3" runat="server" visible="false" valign="middle">
                                <td class="tdAlign">
                                    <asp:Label runat="server" ID="Label8" Text="<%$ Resources:Resource, lblPrdHighResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <a runat="server" id="aimgHigh3" target="_blank">
                                        <img id="imgHigh3" runat="server" alt="" class="PopBoxImageSmall" title="Click to magnify/shrink" />
                                    </a>&nbsp;&nbsp;
                                    <asp:ImageButton ID="imgHighImage3" runat="server" ToolTip="Delete" CausesValidation="false"
                                        ImageUrl="~/Images/delete_icon.png" />
                                </td>
                            </tr>
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel runat="server" ID="Panel3" Width="95%" GroupingText="<%$ Resources:Resource, pnlImage4 %>">
                        <table cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                            <tr height="25" runat="server" id="trLowImgBrowse4">
                                <td class="tdAlign" width="40%">
                                    <asp:Label runat="server" ID="lblLowImage4" Text="<%$ Resources:Resource, lblPrdLowResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td width="2%">
                                </td>
                                <td align="left" width="58%">
                                    <asp:FileUpload runat="server" ID="fileLowImage4" CssClass="innerText" />
                                </td>
                            </tr>
                            <tr height="25" id="trLowImgBtn4" runat="server" visible="false" valign="middle">
                                <td width="40%" class="tdAlign">
                                    <asp:Label runat="server" ID="Label1" Text="<%$ Resources:Resource, lblPrdLowResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td width="2%">
                                </td>
                                <td width="58%" align="left">
                                    <a runat="server" id="aimgLow4" target="_blank">
                                        <img id="imgLow4" runat="server" alt="" class="PopBoxImageSmall" title="Click to magnify/shrink" /></a>
                                    &nbsp;&nbsp;
                                    <asp:ImageButton ID="imgLowImage4" runat="server" ToolTip="Delete" CausesValidation="false"
                                        ImageUrl="~/Images/delete_icon.png" />
                                </td>
                            </tr>
                            <tr>
                                <td height="5">
                                </td>
                            </tr>
                            <tr height="25" runat="server" id="trHighImgBrowse4">
                                <td class="tdAlign">
                                    <asp:Label runat="server" ID="lblHighImage4" Text="<%$ Resources:Resource, lblPrdHighResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:FileUpload runat="server" ID="fileHighImage4" CssClass="innerText" />
                                </td>
                            </tr>
                            <tr height="25" id="trHighBtn4" runat="server" visible="false" valign="middle">
                                <td class="tdAlign">
                                    <asp:Label runat="server" ID="Label2" Text="<%$ Resources:Resource, lblPrdHighResolutionImage %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <a runat="server" id="aimgHigh4" target="_blank">
                                        <img id="imgHigh4" runat="server" alt="" class="PopBoxImageSmall" title="Click to magnify/shrink" /></a>
                                    &nbsp;&nbsp;
                                    <asp:ImageButton ID="imgHighImage4" runat="server" ToolTip="Delete" CausesValidation="false"
                                        ImageUrl="~/Images/delete_icon.png" />
                                </td>
                            </tr>
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table width="100%">
                        <tr>
                            <td height="20" align="center" width="40%">
                            </td>
                            <td width="60%">
                                <div class="buttonwrapper">
                                    <a id="imgCmdSave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                        style="min-width: 120px; text-align: center;">
                                        <%=Resources.Resource.cmdCssSubmit%></span></a></div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="10">
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="sumvalProdImage" runat="server" ShowMessageBox="true"
            ShowSummary="false" />
        <br />
    </div>
    <script type="text/javascript" language="javascript">
        function funImageLowUpload(source, arguments) {
            if (document.getElementById('<%=fileLowImage1.ClientID%>').value.length == 0) {
                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;
            }
        }

        function funImageHighUpload(source, arguments) {
            if (document.getElementById('<%=fileHighImage1.ClientID%>').value.length == 0) {
                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;
            }
        }
    
    </script>
</asp:Content>

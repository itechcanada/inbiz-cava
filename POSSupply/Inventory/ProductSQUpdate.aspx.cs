﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;
using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Text;

public partial class Inventory_ProductSQUpdate : System.Web.UI.Page
{
    Product _prd = new Product();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security Check
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER))
        {
            Response.Redirect("~/Errors/AccessDenined.aspx");
        }
        MessageState.SetGlobalMessage(MessageType.Success, "");
        if (!IsPostBack)
        {
            Collection objCollection = new Collection();
            List<Collection> lResult = new List<Collection>();
            lResult.Add(new Collection { CollectionName = "", CollectionID = BusinessUtility.GetInt(0) });
            SysWarehouses wh = new SysWarehouses();
            wh.FillAllWharehouse(null, ddlWarehouse, CurrentUser.UserDefaultWarehouse, 0, new ListItem(""));
            MessageState.SetGlobalMessage(MessageType.Information, Resources.Resource.msgSelectCollectionAndStyle);
        }
        ddlCollection.Focus();
    }

    protected void btnSearch_OnClick(object sender, EventArgs e)
    {
        MessageState.SetGlobalMessage(MessageType.Success, "");
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Collection> GetCollectionList()
    {
        Collection objCollection = new Collection();
        var t = objCollection.GetCollectionList(null, Globals.CurrentAppLanguageCode);
        return t;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string UpdateCollectionStyle(string csIDs, string csQtys, string whs)
    {
        try
        {
            string[] cstyleIDs = csIDs.Split('~');
            string[] cstyleQtys = csQtys.Split('~');
            if (cstyleIDs.Length == cstyleQtys.Length)
            {
                Product prd = new Product();
                InventoryMovment objIM = new InventoryMovment();
                for (int i = 0; i < cstyleIDs.Length; i++)
                {
                    prd.UpdatePrdOnHandQty(null, BusinessUtility.GetInt(cstyleIDs[i]), BusinessUtility.GetInt(cstyleQtys[i]), whs);
                    objIM.AddMovment(whs, CurrentUser.UserID, BusinessUtility.GetInt(cstyleIDs[i]), BusinessUtility.GetString(InvMovmentSrc.SU), BusinessUtility.GetInt(cstyleQtys[i]), BusinessUtility.GetString(InvMovmentUpdateType.ORWT));
                }
            }
            //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgPrdQuantityUpdated);
            //Globals.RegisterReloadParentScript();
            return "success";
        }
        catch (Exception ex)
        {
            throw;
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetStyleCollection(string style, string collectionID, string whs)
    {

        string sDisabled = "";
        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
        {
            sDisabled = "disabled";
        }

        Product prd = new Product();
        DataTable dt = prd.GetProductDetailStyle(null, style, StatusProductType.Product, Globals.CurrentAppLanguageCode, BusinessUtility.GetInt(collectionID), whs);
        if (dt != null && dt.Rows.Count > 0)
        {
            StringBuilder sbPrdPrice = new StringBuilder();
            sbPrdPrice.Append("<table  border='0' style='border:1;width:99%;padding:5px;'>");
            sbPrdPrice.Append("<tr>");
            sbPrdPrice.Append("<td style='font-weight:bold;'>");
            sbPrdPrice.Append(CommonResource.ResourceValue("lblPrdSalesPriceperMinQty"));
            sbPrdPrice.Append("</td>");
            sbPrdPrice.Append("<td style='font-weight:bold;'>");
            sbPrdPrice.Append(CommonResource.ResourceValue("lblPrdEndUserSalesPrice"));
            sbPrdPrice.Append("</td>");
            sbPrdPrice.Append("<td style='font-weight:bold;'>");
            sbPrdPrice.Append(CommonResource.ResourceValue("lblWebSalesPrice"));
            sbPrdPrice.Append("</td>");
            if (!CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
            {
                sbPrdPrice.Append("<td style='font-weight:bold;'>");
                sbPrdPrice.Append(CommonResource.ResourceValue("lblFOBPrice"));
                sbPrdPrice.Append("</td>");
                sbPrdPrice.Append("<td style='font-weight:bold;'>");
                sbPrdPrice.Append(Resources.Resource.lblLandedPrice);
                sbPrdPrice.Append("</td>");
                sbPrdPrice.Append("<td >");
                sbPrdPrice.Append("&nbsp;");
                sbPrdPrice.Append("</td>");
            }
            sbPrdPrice.Append("</tr>");

            sbPrdPrice.Append("<tr>");
            sbPrdPrice.Append("<td style='font-weight:bold;'>");
            sbPrdPrice.Append("<input type='text' id='txtSMQ' style='width:80px;' " + sDisabled + " value ='" + CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(dt.Rows[0]["Price"])) + "' />");
            sbPrdPrice.Append("</td>");
            sbPrdPrice.Append("<td style='font-weight:bold;'>");
            sbPrdPrice.Append("<input type='text' id='txtEUSP' style='width:80px;' " + sDisabled + " value ='" + CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(dt.Rows[0]["prdEndUserSalesPrice"])) + "' />");
            sbPrdPrice.Append("</td>");
            sbPrdPrice.Append("<td style='font-weight:bold;'>");
            sbPrdPrice.Append("<input type='text' id='txtWSP' style='width:80px;' " + sDisabled + " value ='" + CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(dt.Rows[0]["prdWebSalesPrice"])) + "' />");
            sbPrdPrice.Append("</td>");
            if (!CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
            {
                sbPrdPrice.Append("<td style='font-weight:bold;'>");
                sbPrdPrice.Append("<input type='text' id='txtFOB' style='width:80px;' " + sDisabled + " value ='" + CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(dt.Rows[0]["prdFOBPrice"])) + "' />");
                sbPrdPrice.Append("</td>");
                sbPrdPrice.Append("<td style='font-weight:bold;'>");
                sbPrdPrice.Append("<input type='text' id='txtlnd' style='width:80px;' " + sDisabled + "  value ='" + CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(dt.Rows[0]["prdLandedPrice"])) + "' />");
                sbPrdPrice.Append("</td>");
            }
            sbPrdPrice.Append("<td align='right' >");
            sbPrdPrice.Append("<div class='submit'> <input type='button' class='updateQty ui-button ui-widget ui-state-default ui-corner-all '  id='btnUpdatePrice' name='btnUpdatePrice'  value= '" + CommonResource.ResourceValue("lblUpdatePrice") + "' onclick =  updateCollStylePrice('" + collectionID + "') stlytValue='" + style + "' /> </div>  ");
            sbPrdPrice.Append("</td>");
            sbPrdPrice.Append("</tr>");
            sbPrdPrice.Append("</table>");

            var result = from row in dt.AsEnumerable()
                         group row by row.Field<string>("Size") into grp
                         select new
                         {
                             Size = grp.Key,
                             MemberCount = grp.Count()
                         };

            string sHtmlHeading = "<tr> ";
            sHtmlHeading += "<td> </td>";
            string sHtmlContent = "";
            int iItemSizeCount = 0;

            foreach (var item in result)
            {
                sHtmlHeading += "<td> <b> " + BusinessUtility.GetString(item.Size) + " </b> </td>";
                iItemSizeCount = BusinessUtility.GetInt(item.MemberCount);
            }
            sHtmlHeading += "<td> &nbsp; </td>";
            sHtmlHeading += "</tr>";

            DataTable dtProductColor = prd.GetProductDetailGroupbyColor(null, style, StatusProductType.Product, Globals.CurrentAppLanguageCode, BusinessUtility.GetInt(collectionID), whs);
            int i = 0;
            string sFirstProcutID = "";
            int iCount = 0;
            foreach (DataRow dRow in dtProductColor.Rows)
            //foreach (DataRow dRow in dt.Rows)
            {
                string sClass = "cssPqty";
                sHtmlContent += "<tr>";
                sHtmlContent += "<td>Color  <b> " + BusinessUtility.GetString(dRow["Color"]) + "</b> </br>  SKU #    </td> ";
                DataView datavw = new DataView();
                datavw = dt.DefaultView;
                datavw.RowFilter = "Color='" + BusinessUtility.GetString(dRow["Color"]) + "'";
                DataTable dtProduct = datavw.ToTable();
                string sPIDs = "";

                if (dtProduct.Rows.Count > 0)
                {
                    foreach (DataRow drProduct in dtProduct.Rows)
                    {
                        if (iCount == 0)
                        {
                            sFirstProcutID = "qty" + BusinessUtility.GetString(drProduct["productID"]);
                        }
                        iCount += 1;

                        //sHtmlContent += "<td><input  id=" + "txtSMQ" + BusinessUtility.GetString(drProduct["productID"]) + " value ='" + BusinessUtility.GetString(drProduct["Price"]) + "' pname='" + BusinessUtility.GetString(drProduct["prdName"]) + "' pbarcode='" + BusinessUtility.GetString(drProduct["prdUPCCode"]) + "'  class=" + sClass + " ppid=" + BusinessUtility.GetString(dRow["productID"]) + "   pid=" + BusinessUtility.GetString(drProduct["productID"]) + "  type='text'   style='width:80px;'/> </br> " + BusinessUtility.GetString(drProduct["prdUPCCode"]) + " </td> ";
                        //sHtmlContent += "<td><input  id=" + "txtEUSP" + BusinessUtility.GetString(drProduct["productID"]) + " value ='" + BusinessUtility.GetString(drProduct["prdEndUserSalesPrice"]) + "' pname='" + BusinessUtility.GetString(drProduct["prdName"]) + "' pbarcode='" + BusinessUtility.GetString(drProduct["prdUPCCode"]) + "'  class=" + sClass + " ppid=" + BusinessUtility.GetString(dRow["productID"]) + "   pid=" + BusinessUtility.GetString(drProduct["productID"]) + "  type='text'   style='width:80px;'/> </br> " + BusinessUtility.GetString(drProduct["prdUPCCode"]) + " </td> ";
                        //sHtmlContent += "<td><input  id=" + "txtWSP" + BusinessUtility.GetString(drProduct["productID"]) + " value ='" + BusinessUtility.GetString(drProduct["prdWebSalesPrice"]) + "' pname='" + BusinessUtility.GetString(drProduct["prdName"]) + "' pbarcode='" + BusinessUtility.GetString(drProduct["prdUPCCode"]) + "'  class=" + sClass + " ppid=" + BusinessUtility.GetString(dRow["productID"]) + "   pid=" + BusinessUtility.GetString(drProduct["productID"]) + "  type='text'   style='width:80px;'/> </br> " + BusinessUtility.GetString(drProduct["prdUPCCode"]) + " </td> ";
                        //sHtmlContent += "<td><input  id=" + "txtFOB" + BusinessUtility.GetString(drProduct["productID"]) + " value ='" + BusinessUtility.GetString(drProduct["prdFOBPrice"]) + "' pname='" + BusinessUtility.GetString(drProduct["prdName"]) + "' pbarcode='" + BusinessUtility.GetString(drProduct["prdUPCCode"]) + "'  class=" + sClass + " ppid=" + BusinessUtility.GetString(dRow["productID"]) + "   pid=" + BusinessUtility.GetString(drProduct["productID"]) + "  type='text'   style='width:80px;'/> </br> " + BusinessUtility.GetString(drProduct["prdUPCCode"]) + " </td> ";
                        //sHtmlContent += "<td><input  id=" + "txtlnd" + BusinessUtility.GetString(drProduct["productID"]) + " value ='" + BusinessUtility.GetString(drProduct["prdLandedPrice"]) + "' pname='" + BusinessUtility.GetString(drProduct["prdName"]) + "' pbarcode='" + BusinessUtility.GetString(drProduct["prdUPCCode"]) + "'  class=" + sClass + " ppid=" + BusinessUtility.GetString(dRow["productID"]) + "   pid=" + BusinessUtility.GetString(drProduct["productID"]) + "  type='text'   style='width:80px;'/> </br> " + BusinessUtility.GetString(drProduct["prdUPCCode"]) + " </td> ";
                        sHtmlContent += "<td><input  id=" + "qty" + BusinessUtility.GetString(drProduct["productID"]) + "  " + sDisabled + " value ='" + BusinessUtility.GetString(drProduct["prdOhdQty"]) + "' pname='" + BusinessUtility.GetString(drProduct["prdName"]) + "' pbarcode='" + BusinessUtility.GetString(drProduct["prdUPCCode"]) + "'  class=" + sClass + " ppid=" + BusinessUtility.GetString(dRow["productID"]) + "   pid=" + BusinessUtility.GetString(drProduct["productID"]) + "  type='text'   style='width:80px;'/> </br> " + BusinessUtility.GetString(drProduct["prdUPCCode"]) + " </td> ";

                        if (sPIDs == "")
                        {
                            sPIDs = BusinessUtility.GetString(drProduct["productID"]);
                        }
                        else
                        {
                            sPIDs += "~" + BusinessUtility.GetString(drProduct["productID"]);
                        }

                    }
                }
                sHtmlContent += string.Format("<td style='vertical-align:top;' align='right'><div class='submit'> <input type='button' class='updateQty ui-button ui-widget ui-state-default ui-corner-all '  id='" + "btn" + BusinessUtility.GetString(dRow["productID"]) + "' name='" + "btn" + BusinessUtility.GetString(dRow["productID"]) + "'  value= '" + CommonResource.ResourceValue("lblUpdate") + "' onclick =  updateCollStyle('{0}') /> </div> </td> ", sPIDs);
                sHtmlContent += "</tr>";
                i += 1;
            }

            string sHtmlTable = "";
            sHtmlTable += BusinessUtility.GetString(sbPrdPrice);
            sHtmlTable += "</br>";
            sHtmlTable += "<table border='0' style='border:1;width:99%;padding:5px;'> ";
            sHtmlTable += sHtmlHeading;
            sHtmlTable += sHtmlContent;
            sHtmlTable += "</table> ";
            return sHtmlTable + "*" + sFirstProcutID;
        }
        else
        {
            //return "<font style='color:Red; font-Weight:bold; font-size:14px; text-align: center; '> " + Resources.Resource.msgPrdNotFound + " </font> *";
            return "";
        }
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string UpdateCollectionStylePrice(string collectionID, string style, string txtSMQ, string txtEUSP, string txtWSP, string txtFOB, string txtlnd)
    {
        try
        {
            Product prd = new Product();
            prd.UpdatePrdPrice(null, BusinessUtility.GetInt(collectionID), style, BusinessUtility.GetDouble(txtSMQ), BusinessUtility.GetDouble(txtEUSP), BusinessUtility.GetDouble(txtWSP),
                BusinessUtility.GetDouble(txtFOB), BusinessUtility.GetDouble(txtlnd));
            return "success";
        }
        catch (Exception ex)
        {
            throw;
        }

    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.WebControls;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;

public partial class Inventory_AssignPOSCategoryModal : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            DropDownHelper.FillPosCategory(lstFromPOSCatg, this.ProductID);
            DropDownHelper.FillPosCategoryAssigned(lstToPOSCatg, this.ProductID);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ProcessInventory.AssignPOSCategories(hdnListValue.Value, this.ProductID);
        Globals.RegisterCloseDialogScript(this);
    }

    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }
}
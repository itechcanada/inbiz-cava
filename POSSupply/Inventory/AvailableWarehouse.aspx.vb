Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Partial Class Inventory_AvailableWarehouse
    Inherits BasePage
    Dim objPrdAvailWhs As New clsPrdAvailableWhs
    Public objPrdDesc As New clsPrdDescriptions
    Protected Sub Inventory_AvailableWarehouse_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblHeading.Text = objPrdDesc.funProductName & lblPrdAvailableWarehouse
        If Not Page.IsPostBack Then
            objPrdAvailWhs.subGetWarehouse(dlWarehouses)
            subFillGrid()
            If Request.QueryString("WHID") <> "" Then
                imgCmdReset.Visible = False
                subFillData()
            End If
        End If
        If Session("msg") <> "" Then
            lblMsg.Text = Session("msg").ToString
            Session.Remove("msg")
        End If
    End Sub
    Public Sub subFillGrid()
        sqlsdAvailWhs.SelectCommand = objPrdAvailWhs.subFillAvailWhs()
    End Sub
    Public Sub subFillData()
        objPrdAvailWhs.Id = Request.QueryString("WHID")
        objPrdAvailWhs.subGetPrdAvailableWhsInfo()
        dlWarehouses.SelectedValue = objPrdAvailWhs.PrdWhsCode
        txtAisie.Text = objPrdAvailWhs.PrdAisle
        txtBin.Text = objPrdAvailWhs.PrdBin
        txtBlock.Text = objPrdAvailWhs.PrdBlock
        txtFloor.Text = objPrdAvailWhs.PrdFloor
        txtRack.Text = objPrdAvailWhs.PrdRack
    End Sub
    Public Sub subSetData()
        objPrdAvailWhs.PrdWhsCode = dlWarehouses.SelectedValue
        objPrdAvailWhs.PrdWhsID = Request.QueryString("PrdID")
        objPrdAvailWhs.PrdAisle = txtAisie.Text
        objPrdAvailWhs.PrdBin = txtBin.Text
        objPrdAvailWhs.PrdBlock = txtBlock.Text
        objPrdAvailWhs.PrdFloor = txtFloor.Text
        objPrdAvailWhs.PrdRack = txtRack.Text
    End Sub
    Protected Sub grdvAvailWhs_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdvAvailWhs.PageIndexChanging
        subFillGrid()
    End Sub
    Protected Sub grdvAvailWhs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdvAvailWhs.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
            "confirm('" & msgPrdAvaiWarehouseDelete & "')")
        End If
    End Sub
    Protected Sub grdvAvailWhs_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdvAvailWhs.RowDeleting
        Dim strWhsID As String = grdvAvailWhs.DataKeys(e.RowIndex).Value.ToString()
        lblMsg.Text = msgPrdAvaiWarehouseDeleted
        sqlsdAvailWhs.DeleteCommand = objPrdAvailWhs.subAvailWhsdDelete(strWhsID) 'Delete Associate Product
        subFillGrid()
    End Sub

    Protected Sub grdvAvailWhs_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdvAvailWhs.RowEditing
        Dim strWHsID As String = grdvAvailWhs.DataKeys(e.NewEditIndex).Value.ToString()
        Response.Redirect("Availablewarehouse.aspx?WHID=" & strWHsID & "&PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
    End Sub

    Protected Sub grdvAvailWhs_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdvAvailWhs.Sorting
        subFillGrid()
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    Protected Sub imgCmdSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCmdSave.Click
        subSetData()
        If Request.QueryString("WHID") <> "" Then
            objPrdAvailWhs.Id = Request.QueryString("WHID")
            If objPrdAvailWhs.funUpdatePrdAvailableWhs Then
                Session.Add("msg", msgPrdAvaiWarehouseAdded)
            End If
        Else
            If objPrdAvailWhs.funInsertPrdAvailableWhs Then
                Session.Add("msg", msgPrdAvaiWarehouseUpdated)
            End If
        End If
        Response.Redirect("Availablewarehouse.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
        subFillGrid()
    End Sub

    Protected Sub imgCmdReset_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCmdReset.Click
        dlWarehouses.SelectedValue = 0
        txtAisie.Text = ""
        txtBin.Text = ""
        txtBlock.Text = ""
        txtFloor.Text = ""
        txtRack.Text = ""
    End Sub
End Class

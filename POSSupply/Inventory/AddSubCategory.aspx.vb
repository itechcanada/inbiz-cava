Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Data.Odbc
Imports System.IO
Imports clsCommon
Partial Class Inventory_AddSubCategory
    Inherits BasePage
    Dim ObjSubcategory As New clsSubcategory()
    Dim Objcategory As New clsCategory()
    Dim ObjDataClass As New clsDataClass()
    Shared Flag As Boolean = False
    Dim newWebSeq As String
    Dim maxWebSeq As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Request.QueryString("subcatID") <> "" Then
         
        End If
        'To be assigned
        lblHeading.Text = AddSubCategory
        'This is for automatically selecting web sequence
        If Not IsPostBack Then
            dlWebSeq.Items.Clear()
            Objcategory.subGetCategoryName(dlSubcatCatName)
            If ObjDataClass.GetScalarData("SELECT MAX(subcatWebSeq) FROM subcategory inner join category on category.catID=subcategory.subcatcatid where catStatus='1' And subcatStatus='1'") Is System.DBNull.Value Then
                dlWebSeq.Items.Add((maxWebSeq + 1).ToString)
                hdnOldvalue.Value = maxWebSeq
            Else
                maxWebSeq = ObjDataClass.GetScalarData("SELECT MAX(subcatWebSeq) FROM subcategory inner join category on category.catID=subcategory.subcatcatid where catStatus='1' And subcatStatus='1'")
                Dim i As Integer
                For i = 0 To IIf(Request.QueryString("subcatID") <> "", maxWebSeq - 1, maxWebSeq)
                    dlWebSeq.Items.Add((i + 1).ToString)
                    If i = maxWebSeq Then
                        dlWebSeq.SelectedValue = maxWebSeq + 1
                    End If
                Next
                hdnOldvalue.Value = maxWebSeq + 1
            End If
            '------------------------------------------------------------------
            'Fields subcatName-0,subcatImagePath-1,subcatWebSeq-2,subcatIsActive-3

            If Not Request.QueryString("subcatID") Is Nothing Then
                fillDetails()
            End If
            dlSubcatCatName.Focus()
        End If
    End Sub
    Protected Sub fillDetails()
        ObjDataClass = New clsDataClass()
        Dim strSQL As String = "SELECT subcatName,subcatImagePath,subcatCatId,subcatIsActive,subcatWebSeq" & _
        ", eBayCatgNo FROM subcategory WHERE subcatId=" & Request.QueryString("subcatID") & " and subcatdescLang = '" & rblLanguage.SelectedValue & "'"

        Dim drCategory As OdbcDataReader = ObjDataClass.GetDataReader(strSQL)
        While drCategory.Read
            dlSubcatCatName.SelectedValue = drCategory.Item("subcatCatId")
            txtSubCatName.Text = drCategory.Item("subcatName")
            dlWebSeq.SelectedValue = drCategory.Item("subcatWebSeq")
            hdnOldvalue.Value = dlWebSeq.SelectedValue
            txteBayCatgNo.Text = drCategory.Item("eBayCatgNo").ToString

            If (drCategory.Item("subcatIsActive") = 1) Then
                rbYes.Checked = True
            Else
                rbNo.Checked = True
            End If
            If Not drCategory.Item("subcatImagePath").ToString = "" Then
                imgCat.Src = "../Thumbnail.ashx?p=" & ConfigurationManager.AppSettings("RelativePathForSubCategory") & drCategory.Item("subcatImagePath").ToString & "&Cat=Subcat"
                aTag.HRef = ConfigurationManager.AppSettings("RelativePathForSubCategory") & drCategory.Item("subcatImagePath").ToString
                trsubcatImageUrl.Visible = True
                trsubcatImagePath.Visible = False
            End If
        End While
    End Sub
    Public Sub AdjustWebSeq(ByVal oldWebSeq As Integer, ByVal newWebSeq As Integer, ByVal objclsDataClass As clsDataClass)
        Dim qry As String
        If (oldWebSeq < newWebSeq) Then
            qry = "UPDATE subcategory SET subcatWebSeq=subcatWebSeq-1 WHERE subcatWebSeq >" & oldWebSeq & " and subcatWebSeq <=" & newWebSeq
            objclsDataClass.SetData(qry)
        ElseIf oldWebSeq > newWebSeq Then
            qry = "UPDATE subcategory SET subcatWebSeq=subcatWebSeq+1 WHERE subcatWebSeq >=" & newWebSeq & " and subcatWebSeq <" & oldWebSeq
            objclsDataClass.SetData(qry)
        End If
    End Sub
    Public Sub UploadFile()
        If filesubcatImagePath.HasFile Then
            Dim StrCategoryPic As String = System.IO.Path.GetFileName(filesubcatImagePath.FileName).Trim
            Dim strCategory() As String = Split((StrCategoryPic), ".")
            Dim count As Int16 = strCategory.Length - 1
            If strCategory.Length = "1" Then
                lblErrorMsg.Text = msgCMInvalidFileExtension
                filesubcatImagePath.Focus()
                Exit Sub
            End If
            If strCategory(count).ToLower = "jpg" Or strCategory(count).ToLower = "gif" Or strCategory(count).ToLower = "png" Or strCategory(count).ToLower = "jpeg" Or strCategory(count).ToLower = "bmp" Then
                Dim strPicture As String
                If Request.QueryString("subcatID") <> "" Then
                    strPicture = Request.QueryString("subcatID")
                Else
                    strPicture = ObjSubcategory.funGetMaxSubCategoryID + 1
                End If
                strPicture += "_" & Format(Now(), "HHmmss") & "." & strCategory(count)
                strPicture = Replace(strPicture, " ", "_")
                strPicture = Replace(strPicture, "/", "")
                strPicture = Replace(strPicture, "\", "")
                Try
                    Dim SaveLocation As String = Server.MapPath("~/upload/subCategory/") & "\" & strPicture
                    filesubcatImagePath.SaveAs(SaveLocation)
                    ObjSubcategory.SubcategoryImagepath = strPicture
                Catch ex As Exception
                End Try
            Else
                lblErrorMsg.Text = msgUnrecognizedImagePleaseUploadImg
                Exit Sub
            End If
        End If
    End Sub
    Protected Sub lnkClickMore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClickMore.Click
        trClickHere.Visible = False
        trAllControl.Visible = True
        txtSubCatName.Text = ""
        Response.Redirect("~/Inventory/AddSubCategory.aspx")
    End Sub
    Protected Sub imgsubcatImageUrl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgsubcatImageUrl.Click
        trsubcatImageUrl.Visible = False
        trsubcatImagePath.Visible = True
        Dim strSql As String = "UPDATE subcategory SET subcatImagePath='' WHERE subcatId=" & Request.QueryString("subcatID")
        ObjDataClass.SetData(strSql)
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub

    Protected Sub rblLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblLanguage.SelectedIndexChanged
        If Request.QueryString("subcatID") <> "" Then
            fillDetails()
        End If
    End Sub

    Protected Sub imgCmdReset_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdReset.ServerClick
        If Request.QueryString("subcatID") <> "" Then
            Response.Redirect("~/Inventory/AddSubCategory.aspx?subcatID=" & Request.QueryString("subcatID") & "&Approve=" & Request.QueryString("Approve"))
        Else
            Response.Redirect("~/Inventory/AddSubCategory.aspx")
        End If
    End Sub

    Protected Sub imgCmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdSave.ServerClick
        lblSuccess.Text = ""
        lblErrorMsg.Text = ""
        Flag = True

        If rbYes.Checked = True Then
            ObjSubcategory.SubcategoryIsActive = 1
        ElseIf rbNo.Checked = True Then
            ObjSubcategory.SubcategoryIsActive = 0
        End If
        ObjSubcategory.SubcategoryCatID = dlSubcatCatName.SelectedValue
        If txtSubCatName.Text <> "" Then
            ObjSubcategory.SubcategoryName = Replace(Replace(txtSubCatName.Text, "'", "''").Trim, "\", "\\")
        Else
            ObjSubcategory.SubcategoryName = ""
        End If
        ObjSubcategory.SubcategoryUpdatedby = Session("UserID")
        ObjSubcategory.SubcategoryUpdatedon = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
        ObjSubcategory.SubcategoryWebSequence = dlWebSeq.SelectedItem.ToString

        If txteBayCatgNo.Text <> "" Then
            ObjSubcategory.eBayCatgNo = Replace(Replace(txteBayCatgNo.Text, "'", "''").Trim, "\", "\\")
        Else
            ObjSubcategory.eBayCatgNo = ""
        End If

        Dim strSubID As String = Request.QueryString("subcatID")
        If ObjSubcategory.funDuplicateSubCategory(strSubID) Then   'check Duplicate sub-Category Name.
            lblErrorMsg.Text = msgThisSubCategoryAlreadyExists
            txtSubCatName.Focus()
            Exit Sub
        End If


        If Request.QueryString("subcatID") Is Nothing Then
            Dim objclsDataClass As New clsDataClass()
            'To set CategoryImagePath
            UploadFile()
            '------------------------------------------------------------------------------
            'Adjust for websequence
            newWebSeq = ObjSubcategory.SubcategoryWebSequence
            AdjustWebSeq(hdnOldvalue.Value, newWebSeq, objclsDataClass)
            '-----------------------------------------------------------------------------
            objclsDataClass = New clsDataClass()
            ObjSubcategory.subcatdescLang = rblLanguage.SelectedValue
            If ObjSubcategory.funInsertSubCategory(objclsDataClass) = True Then
                lblSuccess.Text = msgSubCategoryHasBeenAddedSuccessfully
                trClickHere.Visible = True
                trAllControl.Visible = False
            End If

        Else
            ObjSubcategory.SubcategoryID = Request.QueryString("subcatID")

            Dim objclsDataClass As New clsDataClass()

            UploadFile()

            'Adjust for websequence
            Dim newWebSeq As String = dlWebSeq.SelectedItem.ToString
            AdjustWebSeq(hdnOldvalue.Value, newWebSeq, objclsDataClass)
            '-----------------------------------------------------------------------------


            objclsDataClass = New clsDataClass()
            Dim sql As String = "UPDATE subcategory SET " & _
            " subcatCatId='" & ObjSubcategory.SubcategoryCatID & _
            "',subcatIsActive=" & ObjSubcategory.SubcategoryIsActive & _
            " ,subcatWebSeq=" & ObjSubcategory.SubcategoryWebSequence & _
            " ,eBayCatgNo='" & ObjSubcategory.eBayCatgNo & "' "
            If ObjSubcategory.SubcategoryImagepath <> "" Then
                sql += " ,subcatImagePath='" & ObjSubcategory.SubcategoryImagepath & "'"
            End If
            sql += " WHERE subcatId=" & ObjSubcategory.SubcategoryID
            objclsDataClass.SetData(sql)

            sql = "UPDATE subcategory SET subcatName='" & ObjSubcategory.SubcategoryName
            sql += "' WHERE subcatId=" & ObjSubcategory.SubcategoryID & " and subcatdescLang ='" + rblLanguage.SelectedValue + "'"
            objclsDataClass.SetData(sql)

            lblSuccess.Visible = True
            Session.Add("Msg", msgSubCategoryHasBeenUpdatedSuccessfully)
            Response.Redirect("~/Inventory/ViewSubcategory.aspx")
        End If
    End Sub
End Class

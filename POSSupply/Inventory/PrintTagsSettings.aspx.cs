﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Inventory_PrintTagsSettings : BasePage
{
    Product _prd = new Product();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            //string key = Guid.NewGuid().ToString();
            //this.ClientScript.RegisterClientScriptBlock(this.GetType(), key, "window.open('PrintTags.aspx', 'Print','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');", true);
            
            if (this.ProductID > 0)
            {
                _prd.PopulateObject(this.ProductID);

                Product.ProductClothDesc prdCD = new Product.ProductClothDesc();
                prdCD.ProductID = _prd.ProductID;
                prdCD.getClothDesc(null, 0);
                ProductSize ps = new ProductSize();
                ps.SizeID = prdCD.Size;
                List<ProductSize> lPS = new List<ProductSize>();
                lPS = ps.GetAllSizeList(null, "En");

                ProductColor pc = new ProductColor();
                pc.ColorID = prdCD.Color;
                List<ProductColor> lPC = new List<ProductColor>();
                lPC = pc.GetAllColorList(null, "fr");


                txtUpcCode.Text = _prd.PrdUPCCode;
                //txtName.Text = _prd.PrdName;
                txtName.Text = prdCD.Style;
                txtNoOfTags.Text = "30";
                if (lPC.Count > 0 && lPS.Count > 0)
                {
                    HiddenPrdColor.Value = lPC[0].ColorFr;
                    HiddenPrdSize.Value = lPS[0].SizeEn;
                }
                txtPrice.Text = string.Format("{0:F}", _prd.PrdEndUserSalesPrice);

                //SysCompanyInfo ci = new SysCompanyInfo();
                //ci.PopulateObject(CurrentUser.DefaultCompanyID);
                //hdnCurrency.Value = ci.CompanyBasCur;                
            }
        }
    }

    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //Session["__DATA_TO_PRINT"] = new List<string>();        
        SetDataSourceToPrint();

        Globals.RegisterScript(this, "window.onload = function () { window.open('PrintTags.aspx', 'Print','height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');}");
        //Globals.RegisterCloseDialogScript(this);
    }

    private void SetDataSourceToPrint()
    {
        List<PrintSchema> data = new List<PrintSchema>();        

        int lth = BusinessUtility.GetInt(txtNoOfTags.Text);

        for (int i = 1; i <= lth; i++)
        {
            PrintSchema sch = new PrintSchema();
            sch.Column1 = txtUpcCode.Text;
            sch.Column2 = GetProductName();
            sch.Column3 = GetCurrency();
            sch.Column4 = string.Format("{0:F}", BusinessUtility.GetDouble(txtPrice.Text));           
            data.Add(sch);
        }
        Session["__DATA_TO_PRINT"] = data;
    }

    private string GetCurrency() {
        if (!string.IsNullOrEmpty(hdnCurrency.Value.Trim())) { 
            return  hdnCurrency.Value.ToUpper();
        }
        return "CAD";
    }

    private string GetProductName() {
        if (txtName.Text.Trim().Length > 15) {
            return txtName.Text.Trim().Substring(0, 15);
        }
        return txtUpcCode.Text;
    }
}
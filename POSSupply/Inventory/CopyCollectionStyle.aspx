﻿<%@ Page Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="CopyCollectionStyle.aspx.cs" Inherits="Inventory_CopyCollectionStyle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
<div style="text-align:center;">
 <b><span id="lblCollectionStyleName" style="font-size: 15px;" >&nbsp;</span></b>
</div>
    <div id="contentBottom" style="padding: 5px; height: 100px; overflow: auto; text-align:center" onkeypress="return disableEnterKey(event)">
        <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        <br />
       
        <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td class="text">
                    <asp:Label ID="lblSelectCollection" Text="<%$ Resources:Resource, grdSelect %>" runat="server" />
                </td>
                <td >
                    <asp:DropDownList ID="ddlCollection" runat="server" >
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblEnterStyle" Text="<%$ Resources:Resource, lblEnterStyle %>" runat="server" />
                </td>
                <td >
                    <asp:TextBox ID="txtEnterStyle" runat="server"  />
                </td>
            </tr>
        </table>
    </div>
    <div class="div-dialog-command" style="border-top: 1px solid #ccc; padding: 5px;">
        <asp:Button Text="<%$Resources:Resource, btnSave %>" ID="btnSave" runat="server"
            ValidationGroup="RegisterUser" OnClick="btnSave_Click" />
        <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
            CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
    </div>
    <asp:ValidationSummary ID="valsAdminWarehouse" runat="server" ShowMessageBox="true"
        ShowSummary="false" ValidationGroup="RegisterUser" />

        <script type = "text/javascript">
            $(document).ready(function () {
                $("#lblCollectionStyleName").html($.getParamValue('StyleName'));
            });

            $.extend({
                getParamValue: function (paramName) {
                    parName = paramName.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
                    var pattern = '[\\?&]' + paramName + '=([^&#]*)';
                    var regex = new RegExp(pattern);
                    var matches = regex.exec(window.location.href);
                    if (matches == null) return '';
                    else return decodeURIComponent(matches[1].replace(/\+/g, ' '));
                }
            });

//            $("#<%=ddlCollection.ClientID%>").change(function () {
//                var sText = $("#<%=ddlCollection.ClientID %> :selected").text();
//                var sCollectionID = $("#<%=ddlCollection.ClientID %> :selected").val();
//               // $("#lblCollectionStyleName").html(sText + " --" + $.getParamValue('StyleOrgName'));
//               
//            });
            
        </script>
</asp:Content>

<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="eComMetaTags.aspx.vb" Inherits="eComMetaTags" %>

<%@ Register src="../Controls/CommonSearch/CommonKeyword.ascx" tagname="CommonKeyword" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>

    <uc1:CommonKeyword ID="CommonKeyword1" runat="server" />

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal ID="lblHeading" runat="server" Text="<%$ Resources:Resource, lbleComMetaTags %>"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <asp:UpdatePanel runat="server" ID="udpnleComMetaTags">
            <ContentTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td height="10">
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                            <asp:Label ID="lblSelectLang" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, SelectLanguage%>"></asp:Label>
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:RadioButtonList ID="rblLanguage" runat="server" RepeatDirection="Horizontal"
                                AutoPostBack="true">
                                <asp:ListItem Text="<%$ Resources:Resource, English%>" Value="en" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, French%>" Value="fr"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, Spanish%>" Value="sp"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    </tr>
                    <td height="10">
                    </td>
                    <tr height="25">
                        <td class="tdAlign">
                            <asp:Label ID="lbleComTitle" Text="<%$ Resources:Resource, lbleComTitle %>" CssClass="lblBold"
                                runat="server" />
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txteComTitle" CssClass="innerText" runat="server" Width="330Px" />
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign" width="35%">
                            <asp:Label ID="lbleComKeyWords" Text="<%$ Resources:Resource, lbleComKeyWords %>"
                                CssClass="lblBold" runat="server" />
                        </td>
                        <td width="1%">
                        </td>
                        <td align="left" width="64%">
                            <asp:TextBox ID="txteComKeyWords" CssClass="innerText" runat="server" Width="330Px" />
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                            <asp:Label ID="lbleComDesc" Text="<%$ Resources:Resource, lbleComDesc %>" CssClass="lblBold"
                                runat="server" />
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txteComDesc" CssClass="innerText" runat="server" Width="330Px" TextMode="MultiLine" />
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <div class="buttonwrapper">
                                <a id="imgCmdSave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                    style="min-width: 120px; text-align: center;">
                                    <%=Resources.Resource.cmdCssSubmit%></span></a></div>
                        </td>
                    </tr>
                    <tr>
                        <td height="20">
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
    </div>
</asp:Content>

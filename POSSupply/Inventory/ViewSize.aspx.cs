﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Inventory_ViewSize : BasePage
{
    private string strTaxID = "";
    private SysTaxCode _objTax = new SysTaxCode();
    // On Page Load
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/AdminLogin.aspx");
        }

        if (!IsPagePostBack(jgdvTaxes)) //Instead of IsPostBack use this to prevent postback as fress requens in each page of jqgrid
        {
            txtSearch.Focus();
        }

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "deleteSize")
        {
            try
            {
                int iSizeID = BusinessUtility.GetInt(Request.Form["SizeID"]);
                if (iSizeID > 0)
                {
                    ProductSize objSize = new ProductSize();
                    objSize.SizeID = iSizeID;
                    if (objSize.Delete(null, CurrentUser.UserID) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }

            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
    }

    protected void jgdvTaxes_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 3)
        {

            e.CellHtml = string.Format(@"<img src=""{0}"" />", ResolveUrl(string.Format("~/images/{0}", e.CellHtml)));
        }
        if (e.ColumnIndex == 4)
        {
            int iSizeID = BusinessUtility.GetInt(e.CellHtml);
            e.CellHtml = string.Format(@"<a class=""edit-Taxs""  href=""javascript:void(0);"" onclick=""editPopup('{0}')"">{1}</a>", iSizeID, Resources.Resource.edit);
        }
        if (e.ColumnIndex == 5)
        {
            int iSizeID = BusinessUtility.GetInt(e.CellHtml);
            e.CellHtml = string.Format(@"<a href=""{0}""  onclick=""deleteSize('{1}')"">{2}</a>", "javascript:void(0);", BusinessUtility.GetString(iSizeID), Resources.Resource.delete);
        }
    }

    protected void jgdvTaxes_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        ProductSize objProductSize = new ProductSize();
        string search = Request.QueryString[txtSearch.ClientID];
        objProductSize = new ProductSize();
        objProductSize.SearchKey = search;
        jgdvTaxes.DataSource = objProductSize.GetAllSizeList(null, Globals.CurrentAppLanguageCode);
    }
}
Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.IO
Imports System.Drawing.Image
Partial Class Inventory_ProductImages
    Inherits BasePage
    Public objPrdImage As New clsPrdImages
    Public objPrdDesc As New clsPrdDescriptions
    Protected Sub Inventory_ProductImages_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        lblError.Text = ""
        lblHeading.Text = objPrdDesc.funProductName & lblPrdImages
        If Not Page.IsPostBack Then
            If Request.QueryString("PrdID") <> "" Then
                subFillData()
            End If
        End If
        If Session("msg") <> "" Then
            trError.Visible = True
            lblError.Text = Session("msg").ToString
            Session.Remove("msg")
        End If
        If Session("UserModules").ToString.Contains("INROL") = True And (Session("UserModules").ToString.Contains("STK") = False Or Session("UserModules").ToString.Contains("ADM") = False) Then
            imgCmdSave.Visible = False
            imgHighImage1.Visible = False
            imgHighImage2.Visible = False
            imgHighImage3.Visible = False
            imgHighImage4.Visible = False
            imgLowImage4.Visible = False
            imgLowImage3.Visible = False
            imgLowImage2.Visible = False
            imgLowImage1.Visible = False
        End If
    End Sub
    Public Sub subImagesPath(ByVal trBtn As HtmlTableRow, ByVal trBrowse As HtmlTableRow, ByVal img As HtmlImage, ByVal strImagePath As String, ByVal strImageTarget As HtmlAnchor)
        If strImagePath = "_low_1." Or strImagePath = "_low_2." Or strImagePath = "_low_3." Or strImagePath = "_low_4." Or strImagePath = "_high_1." Or strImagePath = "_high_2." Or strImagePath = "_high_3." Or strImagePath = "_high_4." Or strImagePath = "" Then
            trBtn.Visible = False
            trBrowse.Visible = True
        Else
            img.Src = "../Thumbnail.ashx?p=" & ConfigurationManager.AppSettings("RelativePath") & strImagePath & "&Pimg=y"
            strImagePath = ConfigurationManager.AppSettings("RelativePath") & strImagePath
            strImageTarget.HRef = strImagePath

            'Dim objImage As System.Drawing.Image
            'objImage = System.Drawing.Image.FromFile(strImagePath)
            'Dim Height As Integer = objImage.Height
            'Dim Width As Integer = objImage.Width
            'Dim minHeight As Integer = 100
            'Dim minWidth As Integer = 100
            'Dim Ly As Integer = 600 ' max height for thumbnails
            'Dim Lx As Integer = 700 ' max width for thumbnails
            'Dim newWidth, newHeight As Integer
            'Dim l2 ' temp variable used when calculating new size
            'If (Width / Lx) > (Width / Ly) Then
            '    l2 = Width
            '    newWidth = Lx
            '    newHeight = Height * (Lx / l2)
            '    If newHeight > Ly Then
            '        newWidth = newWidth * (Ly / newHeight)
            '        newHeight = Ly
            '    End If
            'Else
            '    l2 = Height
            '    newHeight = Ly
            '    newWidth = Width * (Ly / l2)
            '    If newWidth > Lx Then
            '        newHeight = newHeight * (Lx / newWidth)
            '        newWidth = Lx
            '    End If
            'End If
            'img.Attributes.Add("onclick", "Pop(this,50,'PopBoxImageLarge','" & strImagePath & "');return false;")
            trBtn.Visible = True
            trBrowse.Visible = False
        End If
    End Sub
    Public Sub subFillData()
        objPrdImage.PrdID = Request.QueryString("PrdID")
        objPrdImage.PrdImageID = Request.QueryString("PrdImgID")
        objPrdImage.subGetPrdImagesInfo()
        subImagesPath(trLowImgBtn1, trLowImgBrowse1, imgLow1, objPrdImage.PrdLowImg1, aImgLow1)
        subImagesPath(trLowImgBtn2, trLowImgBrowse2, imgLow2, objPrdImage.PrdLowImg2, aimgLow2)
        subImagesPath(trLowImgBtn3, trLowImgBrowse3, imgLow3, objPrdImage.PrdLowImg3, aimgLow3)
        subImagesPath(trLowImgBtn4, trLowImgBrowse4, imgLow4, objPrdImage.PrdLowImg4, aimgLow4)
        subImagesPath(trHighBtn1, trHighImgBrowse1, ImgHigh1, objPrdImage.PrdHighImg1, aImgHigh1)
        subImagesPath(trHighBtn2, trHighImgBrowse2, imgHigh2, objPrdImage.PrdHighImg2, aimgHigh2)
        subImagesPath(trHighBtn3, trHighImgBrowse3, imgHigh3, objPrdImage.PrdHighImg3, aimgHigh3)
        subImagesPath(trHighBtn4, trHighImgBrowse4, imgHigh4, objPrdImage.PrdHighImg4, aimgHigh4)
    End Sub
    Public Function funUploadFile(ByVal strImage As FileUpload, ByVal strAdd As String, ByVal strImageType As String) As String
        'Dim strPath As String = ConfigurationManager.AppSettings("UploadDir")
        If strImage.HasFile Then
            Dim StrProductPic As String = System.IO.Path.GetFileName(strImage.FileName).Trim
            Dim strProduct() As String = Split((StrProductPic), ".")
            Dim count As Int16 = strProduct.Length - 1
            If strProduct.Length = "1" Then
                Session.Add("msg", msgprdInvalidExt)
                Exit Function
            End If
            If strProduct(count).ToLower = "jpg" Or strProduct(count).ToLower = "gif" Or strProduct(count).ToLower = "png" Or strProduct(count).ToLower = "jpeg" Or strProduct(count).ToLower = "bmp" Then
                Dim strPicture As String
                strPicture = Request.QueryString("prdID") & "_" & strImageType
                'Else
                '    strPicture = Request.QueryString("prdID") & "_" & objPrdImage.funGetMaxPrdImageID() + 1 & "_" & strImageType
                'End If
                strPicture += "_" & strAdd & "_" & Format(Now(), "HHmmss") & "." & strProduct(count)
                Try
                    Dim SaveLocation As String = Server.MapPath("~/Upload/Product/") & "\" & strPicture
                    strImage.SaveAs(SaveLocation)
                    If strImageType = "low" Then
                        objPrdImage.PrdSmallImagePath = strPicture
                    Else
                        objPrdImage.PrdLargeImagePath = strPicture
                    End If
                    Return strPicture
                Catch ex As Exception
                    Session.Add("msg", ex.Message)
                End Try
            Else
                Session.Add("msg", msgPrdUnrecognizedImage)
                Exit Function
            End If
        End If
    End Function
    Public Sub SubUpdateImages()
        If fileLowImage1.HasFile Then
            If objPrdImage.PrdImageID1 <> "" Then
                funUploadFile(fileLowImage1, 1, "low")
                objPrdImage.funUpdatePrdImages(objPrdImage.PrdImageID1, "low")
            End If
        End If
        If fileHighImage1.HasFile Then
            If objPrdImage.PrdImageID1 <> "" Then
                funUploadFile(fileHighImage1, 1, "high")
                objPrdImage.funUpdatePrdImages(objPrdImage.PrdImageID1, "high")
            End If
        End If
        If fileLowImage2.HasFile Then
            If objPrdImage.PrdImageID2 <> "" Then
                funUploadFile(fileLowImage2, 2, "low")
                objPrdImage.funUpdatePrdImages(objPrdImage.PrdImageID2, "low")
            End If
        End If
        If fileHighImage2.HasFile Then
            If objPrdImage.PrdImageID2 <> "" Then
                funUploadFile(fileHighImage2, 2, "high")
                objPrdImage.funUpdatePrdImages(objPrdImage.PrdImageID2, "high")
            End If
        End If
        If fileLowImage3.HasFile Then
            If objPrdImage.PrdImageID3 <> "" Then
                funUploadFile(fileLowImage3, 3, "low")
                objPrdImage.funUpdatePrdImages(objPrdImage.PrdImageID3, "low")
            End If
        End If
        If fileHighImage3.HasFile Then
            If objPrdImage.PrdImageID3 <> "" Then
                funUploadFile(fileHighImage3, 3, "high")
                objPrdImage.funUpdatePrdImages(objPrdImage.PrdImageID3, "high")
            End If
        End If
        If fileLowImage4.HasFile Then
            If objPrdImage.PrdImageID4 <> "" Then
                funUploadFile(fileLowImage4, 4, "low")
                objPrdImage.funUpdatePrdImages(objPrdImage.PrdImageID4, "low")
            End If
        End If
        If fileHighImage4.HasFile Then
            If objPrdImage.PrdImageID4 <> "" Then
                funUploadFile(fileHighImage4, 4, "high")
                objPrdImage.funUpdatePrdImages(objPrdImage.PrdImageID4, "high")
            End If
        End If
    End Sub
    Public Sub subInsertImage()
        If fileLowImage1.HasFile Or fileHighImage1.HasFile Then
            objPrdImage.PimgIsDefault = "1"
            funUploadFile(fileLowImage1, 1, "low")
            funUploadFile(fileHighImage1, 1, "high")
            If lblError.Text = "" Then
                If objPrdImage.PrdImageID1 = "" Then
                    objPrdImage.funInsertPrdImages()
                End If
                objPrdImage.PrdSmallImagePath = ""
                objPrdImage.PrdLargeImagePath = ""
            Else
                Exit Sub
            End If
        End If
        If fileLowImage2.HasFile Or fileHighImage2.HasFile Then
            objPrdImage.PimgIsDefault = "0"
            funUploadFile(fileLowImage2, 2, "low")
            funUploadFile(fileHighImage2, 2, "high")
            If lblError.Text = "" Then
                If objPrdImage.PrdImageID2 = "" Then
                    objPrdImage.funInsertPrdImages()
                End If
                objPrdImage.PrdSmallImagePath = ""
                objPrdImage.PrdLargeImagePath = ""
            Else
                Exit Sub
            End If
        End If
        If fileLowImage3.HasFile Or fileHighImage3.HasFile Then
            objPrdImage.PimgIsDefault = "0"
            funUploadFile(fileLowImage3, 3, "low")
            funUploadFile(fileHighImage3, 3, "high")
            If lblError.Text = "" Then
                If objPrdImage.PrdImageID3 = "" Then
                    objPrdImage.funInsertPrdImages()
                End If
                objPrdImage.PrdSmallImagePath = ""
                objPrdImage.PrdLargeImagePath = ""
            Else
                Exit Sub
            End If
        End If
        If fileLowImage4.HasFile Or fileHighImage4.HasFile Then
            objPrdImage.PimgIsDefault = "0"
            funUploadFile(fileLowImage4, 4, "low")
            funUploadFile(fileHighImage4, 4, "high")
            If lblError.Text = "" Then
                If objPrdImage.PrdImageID4 = "" Then
                    objPrdImage.funInsertPrdImages()
                End If
                objPrdImage.PrdSmallImagePath = ""
                objPrdImage.PrdLargeImagePath = ""
            Else
                Exit Sub
            End If
        End If
    End Sub
    Public Sub subDeleteImage(ByVal trBtn As HtmlTableRow, ByVal trBrowse As HtmlTableRow)
        trBtn.Visible = False
        trBrowse.Visible = True
        objPrdImage.PrdID = Request.QueryString("PrdID")
        objPrdImage.subGetPrdImagesInfo()
    End Sub
    Protected Sub imgHighImage1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgHighImage1.Click
        subDeleteImage(trHighBtn1, trHighImgBrowse1)
        objPrdImage.SubDeleteImage(objPrdImage.PrdImageID1, "high", "_high_1.")
    End Sub
    Protected Sub imgLowImage1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgLowImage1.Click
        subDeleteImage(trLowImgBtn1, trLowImgBrowse1)
        objPrdImage.SubDeleteImage(objPrdImage.PrdImageID1, "low", "_low_1.")
    End Sub
    Protected Sub imgLowImage2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgLowImage2.Click
        subDeleteImage(trLowImgBtn2, trLowImgBrowse2)
        objPrdImage.SubDeleteImage(objPrdImage.PrdImageID2, "low", "_low_2.")
    End Sub
    Protected Sub imgLowImage3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgLowImage3.Click
        subDeleteImage(trLowImgBtn3, trLowImgBrowse3)
        objPrdImage.SubDeleteImage(objPrdImage.PrdImageID3, "low", "_low_3.")
    End Sub
    Protected Sub imgLowImage4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgLowImage4.Click
        subDeleteImage(trLowImgBtn4, trLowImgBrowse4)
        objPrdImage.SubDeleteImage(objPrdImage.PrdImageID4, "low", "_low_4.")
    End Sub
    Protected Sub imgHighImage2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgHighImage2.Click
        subDeleteImage(trHighBtn2, trHighImgBrowse2)
        objPrdImage.SubDeleteImage(objPrdImage.PrdImageID2, "high", "_high_2.")
    End Sub
    Protected Sub imgHighImage3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgHighImage3.Click
        subDeleteImage(trHighBtn3, trHighImgBrowse3)
        objPrdImage.SubDeleteImage(objPrdImage.PrdImageID3, "high", "_high_3.")
    End Sub

    Protected Sub imgHighImage4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgHighImage4.Click
        subDeleteImage(trHighBtn4, trHighImgBrowse4)
        objPrdImage.SubDeleteImage(objPrdImage.PrdImageID4, "high", "_high_4.")
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    Protected Sub imgCmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdSave.ServerClick
        objPrdImage.PrdID = Request.QueryString("prdID")
        objPrdImage.subGetPrdImagesInfo()
        SubUpdateImages()
        If objPrdImage.funIsExistImage < 4 Then
            subInsertImage()
        End If
        Response.Redirect("ProductImages.aspx?prdID=" & Request.QueryString("prdID"))
    End Sub
End Class


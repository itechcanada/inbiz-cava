﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Inventory_ProductTagsModal : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindTags();
        }
    }

    private void BindTags()
    {
        sdsTags.SelectCommand = new ProductTag().GetSql(sdsTags.SelectParameters, this.ProductID, this.WhsCode);
    }

    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    public string WhsCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["whscode"]);
        }
    }
}
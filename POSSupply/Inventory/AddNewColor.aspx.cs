﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Inventory_AddNewColor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MessageState.SetGlobalMessage(MessageType.Success, "");
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                if (BusinessUtility.GetString(Request.QueryString["Status"]) == "INDIVIDUAL")
                {
                    trIsAxctive.Visible = true;
                    if (BusinessUtility.GetInt(Request.QueryString["ColorID"]) > 0)
                    {
                        ProductColor objProductColor = new ProductColor();
                        objProductColor.ColorID = BusinessUtility.GetInt(Request.QueryString["ColorID"]);
                        var lst = objProductColor.GetAllColorList(null, Globals.CurrentAppLanguageCode);
                        foreach (var item in lst)
                        {
                            txtColorNameEn.Text = item.ColorEn;
                            txtColorNameFr.Text = item.ColorFr;
                            txtShortName.Text = item.ShortName;
                            rblstActive.SelectedValue = BusinessUtility.GetBool(item.IsActive) ? "1" : "0";
                        }

                    }
                }
                txtColorNameFr.Focus();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            ProductColor objProductColor = new ProductColor();
            objProductColor.ColorEn = txtColorNameEn.Text;
            objProductColor.ColorFr = txtColorNameFr.Text;
            //objCollection.ShortName = txtShortName.Text;
            objProductColor.IsActive = BusinessUtility.GetInt(rblstActive.SelectedItem.Value);
            objProductColor.ColorID = BusinessUtility.GetInt(Request.QueryString["ColorID"]);
            objProductColor.ShortName = txtShortName.Text;
            if (objProductColor.SaveColor(null, CurrentUser.UserID) == true)
            {
                if (BusinessUtility.GetString(Request.QueryString["Status"]) == "INDIVIDUAL")
                {
                    if (BusinessUtility.GetInt(Request.QueryString["ColorID"]) > 0)
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgColorUpdate);
                        Globals.RegisterReloadParentScript(this);
                        Globals.RegisterCloseDialogScript(this);
                        return;
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgColorCreate);
                        Globals.RegisterReloadParentScript(this);
                        Globals.RegisterCloseDialogScript(this);
                        return;
                    }
                }
                //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated );
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.ColorCreated({0});", objProductColor.ColorID), true);
            }
            else
            {
                //MessageState.SetGlobalMessage(MessageType.Failure, "Collection Creation failure");
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }

        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
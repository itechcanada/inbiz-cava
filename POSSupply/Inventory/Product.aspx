﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="Product.aspx.cs" Inherits="Inventory_Product" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">   
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
    <%=UIHelper.GetScriptTags("blockui", 1)%>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
     <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <asp:Literal ID="ltTitle" Text="" runat="server" /> </b>
    </div>
    <div style="float: left; padding-top: 12px;">        
        <asp:Button ID="btnAdd" runat="server" Text="Add New Product" OnClick="btnAdd_Click" CausesValidation="false" />
        
        <span id="prdQty" runat="server" visible="false">
            <asp:Button ID="btnAddQuantiry" Text="<%$ Resources:Resource, lblPrdQuantity %>"
                runat="server" CausesValidation="false" />
            <iCtrl:IframeDialog ID="mdAddProductQuantity" TriggerControlID="btnAddQuantiry"
                Width="750" Height="450" Url="~/Inventory/ProductQuantity.aspx" Title="" runat="server">
            </iCtrl:IframeDialog></span>
        <span id="prdKit" runat="server">
            <asp:Button ID="btnProductKit" Text="Add Kit Content"
                runat="server" CausesValidation="false" />
            <iCtrl:IframeDialog ID="mdProductKitContent" TriggerControlID="btnProductKit"
                Width="450" Height="250" Url="~/Inventory/ProductQuantity.aspx" Title="Add Kit Content" runat="server">
            </iCtrl:IframeDialog></span>
        <span id="prdPosCategory" runat="server">
            <asp:Button ID="bntAssignPosCategory" Text="<%$Resources:Resource, cmdCssAssignPOSCatg %>"
                runat="server" CausesValidation="false" />
            <iCtrl:IframeDialog ID="mdAssignPosCateg" TriggerControlID="bntAssignPosCategory"
                Width="650" Height="410" Url="~/Inventory/AssignPOSCategoryModal.aspx" Title="<%$Resources:Resource, lblAssignPOSPrdCatg %>" runat="server">
            </iCtrl:IframeDialog>
       </span>
       <span id="prdTagPrinting" runat="server">
            <asp:Button ID="btnPrintTags" Text="<%$Resources:Resource,lblPrintTags %>"
                runat="server" CausesValidation="false" />
            <iCtrl:IframeDialog ID="mdTagPrinting" TriggerControlID="btnPrintTags"
                Width="550" Height="230" Url="~/Inventory/PrintTagsSettings.aspx" Title="<%$Resources:Resource,lblPrintTags %>" runat="server">
            </iCtrl:IframeDialog>
       </span>
       <asp:Button ID="btnApplyDiscount" runat="server" Text="<%$Resources:Resource, lblApplyDiscount %>" CausesValidation="false" OnClientClick="return applyDiscountPopup();" Visible="false" />   
       <asp:Button ID="btnApplyBlackOuts" runat="server" Text="<%$Resources:Resource, lblApplyBlackOuts %>" CausesValidation="false" OnClientClick="return applyBlackOutsPopup();" Visible="false" />     
    </div>
    <div style="float: right;">        
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">    
     <asp:PlaceHolder ID="phControl" runat="server" /> 
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" Runat="Server">
    <ul id="custMenu" class="menu inbiz-left-menu collapsible">
        <li class="active"><a href="#">Action</a>
            <asp:BulletedList ID="blstNav" DisplayMode="HyperLink" DataTextField="Text"
                DataValueField="Value" runat="server" style="display:block;">
            </asp:BulletedList>
        </li>
    </ul>
    <asp:HiddenField ID="hdnNexUrl" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        function applyDiscountPopup() {            
            dataToPost = {}
            dataToPost.productID = <%=ProductID%>;
            var $dialog = jQuery.FrameDialog.create({ url: 'mdApplyDiscountNew.aspx?' + $.param(dataToPost),
                title: "<%=Resources.Resource.lblApplyDiscount %>",
                loadingClass: "loading-image",
                modal: true,
                width: 850,
                height: 500,
                autoOpen: false
            });
            $dialog.dialog('open');

            return false;
        }

    </script>
</asp:Content>


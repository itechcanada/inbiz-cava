﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="IntransitQtyModal.aspx.cs" Inherits="Inventory_IntransitQtyModal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="contentBottom" style="padding: 5px; height: 220px; overflow: auto;">
        <div id="grid_wrapper" style="width: 100%;">
            <trirand:JQGrid runat="server" ID="grdQuantity" DataSourceID="sdsQuantity" Height="150px"
                AutoWidth="True" OnDataRequesting="grdQuantity_DataRequesting">
                <Columns>
                    <trirand:JQGridColumn DataField="poID" HeaderText="" PrimaryKey="True" Visible="false" />
                    <trirand:JQGridColumn DataField="poID" HeaderText="<%$ Resources:Resource, grdvInvPO %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="recDateTime" HeaderText="<%$ Resources:Resource, grdvInvReceivingDate %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="poQty" HeaderText="<%$ Resources:Resource, grdvInvPOQuntity %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="poUnitPrice" HeaderText="<%$ Resources:Resource, grdvInvPOPrice %>"
                        Editable="false" />                    
                </Columns>
                <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                    ShowDeleteButton="false" ShowSearchButton="false" />
                <SortSettings InitialSortColumn=""></SortSettings>
                <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="gridLoadComplete" />
            </trirand:JQGrid>           
            <asp:SqlDataSource ID="sdsQuantity" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdQuantity.ClientID %>";

        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function gridLoadComplete(data) {
            $(window).trigger("resize");
        }        
    </script>
</asp:Content>

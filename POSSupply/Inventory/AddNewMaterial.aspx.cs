﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;


public partial class Inventory_AddNewMaterial : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MessageState.SetGlobalMessage(MessageType.Success, "");
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                if (BusinessUtility.GetString(Request.QueryString["Status"]) == "INDIVIDUAL")
                {
                    trIsAxctive.Visible = true;
                    if (BusinessUtility.GetInt(Request.QueryString["MaterialID"]) > 0)
                    {
                        ProductMaterial objProductMaterial = new ProductMaterial();
                        objProductMaterial.MaterialID = BusinessUtility.GetInt(Request.QueryString["MaterialID"]);
                        var lst = objProductMaterial.GetAllMaterialList(null, Globals.CurrentAppLanguageCode);
                        foreach (var item in lst)
                        {
                            txtMaterialNameEn.Text = item.MaterialEn;
                            txtMaterialNameFr.Text = item.MaterialFr;
                            txtShortName.Text = item.MaterialShortName;
                            rblstActive.SelectedValue = BusinessUtility.GetBool(item.IsActive) ? "1" : "0";
                        }

                    }
                }

                txtMaterialNameFr.Focus();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            ProductMaterial objProductMaterial = new ProductMaterial();
            objProductMaterial.MaterialEn = txtMaterialNameEn.Text;
            objProductMaterial.MaterialFr = txtMaterialNameFr.Text;
            objProductMaterial.IsActive = BusinessUtility.GetInt(rblstActive.SelectedItem.Value);
            objProductMaterial.MaterialID = BusinessUtility.GetInt(Request.QueryString["MaterialID"]);
            objProductMaterial.MaterialShortName = txtShortName.Text;
            if (objProductMaterial.Save(null, CurrentUser.UserID) == true)
            {
                if (BusinessUtility.GetString(Request.QueryString["Status"]) == "INDIVIDUAL")
                {
                    // if (objProductMaterial.MaterialID > 0)
                    if (BusinessUtility.GetInt(Request.QueryString["MaterialID"]) > 0)
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgMaterialUpdate);
                        Globals.RegisterReloadParentScript(this);
                        Globals.RegisterCloseDialogScript(this);
                        return;
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgMaterialCreate);
                        Globals.RegisterReloadParentScript(this);
                        Globals.RegisterCloseDialogScript(this);
                        return;
                    }
                }
                //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated );
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.MaterialCreated({0});", objProductMaterial.MaterialID), true);
            }
            else
            {
                //MessageState.SetGlobalMessage(MessageType.Failure, "Collection Creation failure");
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }

        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
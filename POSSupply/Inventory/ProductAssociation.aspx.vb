Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Partial Class Inventory_ProductAssociation
    Inherits BasePage
    Dim objProduct As New clsProducts
    Dim objAssPrd As New clsPrdAssociations
    Public objPrdDesc As New clsPrdDescriptions
    Protected Sub Inventory_ProductAssociation_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        lblHeading.Text = objPrdDesc.funProductName & lblPrdAssociate
        If Not Page.IsPostBack Then
            objProduct.subGetProduct(dlProduct)
            subFillGrid()
        End If
        If Session("UserModules").ToString.Contains("INROL") = True And (Session("UserModules").ToString.Contains("STK") = False Or Session("UserModules").ToString.Contains("ADM") = False) Then
            imgCmdReset.Visible = False
            imgCmdSave.Visible = False
        End If
        dlProduct.Focus()
    End Sub
    Public Sub subFillGrid()
        sqlsdAssPrd.SelectCommand = objAssPrd.subFillAssProduct()
    End Sub
    Protected Sub grdvAssProduct_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdvAssProduct.PageIndexChanging
        subFillGrid()
    End Sub
    Protected Sub grdvAssProduct_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdvAssProduct.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
            "confirm('" & msgPrdAssociateDelete & "')")
        End If
        If Session("UserModules").ToString.Contains("INROL") = True And (Session("UserModules").ToString.Contains("STK") = False Or Session("UserModules").ToString.Contains("ADM") = False) Then
            grdvAssProduct.Columns(1).Visible = False
        End If
    End Sub
    Protected Sub grdvAssProduct_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdvAssProduct.RowDeleting
        Dim strAssPrdID As String = grdvAssProduct.DataKeys(e.RowIndex).Value.ToString()
        lblMsg.Text = msgPrdAssociateDeleted
        sqlsdAssPrd.DeleteCommand = objAssPrd.subAssPrdDelete(strAssPrdID) 'Delete Associate Product
        subFillGrid()
    End Sub
    Protected Sub grdvAssProduct_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdvAssProduct.Sorting
        subFillGrid()
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    Protected Sub imgCmdReset_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdReset.ServerClick
        dlProduct.SelectedValue = 0
    End Sub

    Protected Sub imgCmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdSave.ServerClick
        objAssPrd.AssProdID = dlProduct.SelectedValue
        objAssPrd.Id = Request.QueryString("prdID")
        If objAssPrd.funDuplicateAssProduct Then
            lblMsg.Text = msgPrdAssociateExist
            lblMsg.ForeColor = Drawing.Color.Red
            Exit Sub
        Else
            If objAssPrd.funInsertPrdAssociations() Then
                lblMsg.Text = msgPrdAssociateSaved
                lblMsg.ForeColor = Drawing.Color.Green
            End If
        End If
        subFillGrid()
    End Sub
End Class

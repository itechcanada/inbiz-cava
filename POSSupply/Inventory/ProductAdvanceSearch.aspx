﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="ProductAdvanceSearch.aspx.cs" Inherits="Inventory_ProductAdvanceSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="dialogAssignUser" style="padding: 5px;width:100%;">
        <table>
            <tr>
                <td colspan="5">
                    <h3>
                        <%= Resources.Resource.lblWebSiteInfo%>
                    </h3>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="Label16" Text="<%$ Resources:Resource, lblWebsiteCategory %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlCategory" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblWebCategory %>" Height="100px">
                    </asp:ListBox>
                </td>
                <td class="text" valign="top">
                    <asp:Label ID="Label17" Text="<%$ Resources:Resource, lblWebSiteSubCategory %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlWebSiteSubCatg" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblWebSiteSubCategory %>" Height="100px">
                    </asp:ListBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <h3>
                        <%= Resources.Resource.lblProductDetail%>
                    </h3>
                </td>
            </tr>
            <tr>
                <td class="text" valign="top">
                    <asp:Label ID="lblNickLine" Text="<%$ Resources:Resource, lblNickLine %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlNickline" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblNickLine %>"></asp:ListBox>
                </td>
                <td class="text" valign="top">
                    <asp:Label ID="lblExtraCategory" Text="<%$ Resources:Resource, lblExtraCategory %>"
                        runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlExtraCategory" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblExtraCategory %>"></asp:ListBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblSleeve" Text="<%$ Resources:Resource, lblSleeve %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlSleeve" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblSleeve %>"></asp:ListBox>
                </td>
                <td class="text" valign="top">
                    <asp:Label ID="lblSilhouette" Text="<%$ Resources:Resource, lblSilhouette %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlSilhouette" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblSilhouette %>"></asp:ListBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="Label14" Text="<%$ Resources:Resource, lblTrend %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlTrend" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblTrend %>"></asp:ListBox>
                </td>
                <td class="text" valign="top">
                    <asp:Label ID="Label15" Text="<%$ Resources:Resource, lblProductKeyWord %>" runat="server" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlKeyWord" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblProductKeyWord %>"></asp:ListBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text" valign="top">
                    <asp:Label ID="Label21" runat="server" Text="<%$Resources:Resource, lblGauge%>" />
                </td>
                <td class="input" valign="top">
                    <asp:ListBox CssClass="modernized_select" ID="ddlGauge" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblGauge %>"></asp:ListBox>
                </td>
                <td class="text">
                    &nbsp;
                </td>
                <td class="input" valign="top">
                    &nbsp;
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
            OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
</asp:Content>

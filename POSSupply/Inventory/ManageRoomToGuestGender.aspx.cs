﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Inventory_ManageRoomToGuestGender : BasePage
{
    Rooms _room = new Rooms();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillForm();
        }
    }

    private void FillForm()
    {
        DropDownHelper.FillBuildings(ddlBuilding, new ListItem("--Building---", "0"), Globals.CurrentAppLanguageCode);         
        DropDownHelper.FillDropdown(rblReserveFor, "PD", "dlSex", Globals.CurrentAppLanguageCode, null);
    }

    protected void ddlRoom_SelectedIndexChanged(object sender, EventArgs e)
    {
        rblReserveFor.ClearSelection();
        int val = 0;
        foreach (ListItem item in ddlRoom.Items)
        {
            if (item.Selected)
            {
                val = _room.GetSexAssociatedToRoom(BusinessUtility.GetInt(item.Value));
                break;
            }
        }
        if (val > 0)
        {
            rblReserveFor.SelectedValue = val.ToString();
            ddlRoom.AutoPostBack = false;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            List<int> lstRooms = new List<int>();
            foreach (ListItem item in ddlRoom.Items)
            {
                if (item.Selected)
                {
                    lstRooms.Add(BusinessUtility.GetInt(item.Value));
                }
            }

            _room.AssignSexToRoom(lstRooms.ToArray(), BusinessUtility.GetInt(rblReserveFor.SelectedValue));
            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgRecordAddedSuccess);
        }
        catch
        { }
        finally { 
        }
        //Response.Redirect(Request.RawUrl);
    }
    protected void ddlBuilding_SelectedIndexChanged(object sender, EventArgs e)
    {
        rblReserveFor.ClearSelection();

        int buildingID = 0;
        int.TryParse(ddlBuilding.SelectedValue, out buildingID);
        DropDownHelper.FillRooms(ddlRoom, null, buildingID, Globals.CurrentAppLanguageCode);
        //ddlRoom.AutoPostBack = true;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;
using System.Web.Services;
using Newtonsoft.Json;
using System.Web.Script.Services;

public partial class Inventory_AddEditProductCategory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            int CategoryHdrID = BusinessUtility.GetInt(Request.QueryString["CategoryHrdID"]);
            int CategoryID = BusinessUtility.GetInt(Request.QueryString["CategoryID"]);
            ProductCategory prdCategory = new ProductCategory();
            prdCategory.CatgHrdID = CategoryHdrID;
            List<ProductCategory> lResult = new List<ProductCategory>();
            lResult = prdCategory.GetDetailStyleCatg(null, CategoryID, Globals.CurrentAppLanguageCode);
            InbizUser inbizuser = new InbizUser();

            if (lResult.Count > 0)
            {
                lblLastModifyBy.Text = Resources.Resource.lblLastModifiedBy + " " + inbizuser.GetUserName(lResult[0].CatgUpdatedBy); //lResult[0].CatgUpdatedBy;
                lblLastModifyAt.Text = Resources.Resource.lblLastModifiedAt + " " + (lResult[0].CatgUpdatedAt).ToString("MMM dd,yyyy HH:mm");
                chkIsActive.Checked = lResult[0].CatgIsActive;
                lblCategoryName.Text = lResult[0].CatgName;
                txtNameFrench.Text = lResult[0].CatgNameFr;
                txtNameEnglish.Text = lResult[0].CatgNameEn;
            }
            txtNameFrench.Focus();
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (BusinessUtility.GetString(Request.QueryString["status"]) == "INDIVIDUAL")
            {
                int ctgHdrID = BusinessUtility.GetInt(Request.QueryString["CatgHdrID"]);
                if (ctgHdrID == 0)
                {
                    ctgHdrID = 1;
                }
                ProductCategory prdCategory = new ProductCategory();
                prdCategory.CatgNameFr = txtNameFrench.Text;
                prdCategory.CatgNameEn = txtNameEnglish.Text;
                prdCategory.CatgIsActive = chkIsActive.Checked;
                prdCategory.CatgUpdatedBy = CurrentUser.UserID;
                prdCategory.AddCategory(null, ctgHdrID);
            }
            else
            {
                ProductCategory prdCategory = new ProductCategory();
                prdCategory.CatgNameFr = txtNameFrench.Text;
                prdCategory.CatgNameEn = txtNameEnglish.Text;
                prdCategory.CatgIsActive = chkIsActive.Checked;
                prdCategory.CatgUpdatedBy = CurrentUser.UserID;
                prdCategory.CatgHrdID = BusinessUtility.GetInt(Request.QueryString["CategoryHrdID"]);
                prdCategory.CatgID = BusinessUtility.GetInt(Request.QueryString["CategoryID"]);
                prdCategory.UpdateCategory(null);
            }
            Globals.RegisterCloseDialogScript(Page, string.Format("parent.reloadGrid();"), true);
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
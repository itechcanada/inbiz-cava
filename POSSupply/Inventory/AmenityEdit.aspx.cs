﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_AmenityEdit : BasePage
{
    Attributes _attr = new Attributes();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            BindData();
        }
    }

    private void BindData()
    {             
        if (this.AttributeID > 0) {
            _attr.PopulateObject(this.AttributeID);
            txtDescriptionEn.Text = _attr.AttributeDescEn;
            txtDescriptionFr.Text = _attr.AttributeDescFr;
            hdnImage.Value = _attr.AttributeImage;
            if (!string.IsNullOrEmpty(_attr.AttributeImage)) {
                imgIcon.Visible = true;
                imgIcon.ImageUrl = ImageUploadPath.ATTRIBUTE_IMAGE_ICONS + ThumbSizePrefix._32x32 + _attr.AttributeImage;
                ibtnDelete.Visible = true;
            }
        }
    }

    private void SetData() {
        _attr.AttributeDescEn = txtDescriptionEn.Text;
        _attr.AttributeDescFr = txtDescriptionFr.Text;
        _attr.AttributeID = this.AttributeID;
        _attr.AttributeImage = hdnImage.Value;
        _attr.IsActive = true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid) {
            //Upload Image file first
            if (fuImage.HasFile) {
                if (FileManager.IsValidImageFile(fuImage.FileName))
                {
                    string fileName = FileManager.GetRandomFileName(fuImage.FileName);
                    string thumImage1 = string.Format("{0}{1}", ThumbSizePrefix._32x32, fileName);
                    bool orgImage = FileManager.UploadImage(fuImage.PostedFile, ImageUploadPath.ATTRIBUTE_IMAGE_ICONS, fileName);
                    bool thumbImage = FileManager.UploadImage(fuImage.PostedFile, 32, 32, ImageUploadPath.ATTRIBUTE_IMAGE_ICONS, thumImage1);
                    if (orgImage && thumbImage) {
                        hdnImage.Value = fileName;
                    }
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidFileType);
                    return;
                }
            }

            SetData();
            if (this.AttributeID > 0) {
                _attr.Update();
                MessageState.SetGlobalMessage(MessageType.Success, "Amenity updated successfully!");

                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                }
                else
                {
                    Globals.RegisterCloseDialogScript(Page);
                }    
            }
            else
            {
                _attr.Insert();
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgRecordAddedSuccess);

                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                }
                else
                {
                    Globals.RegisterCloseDialogScript(Page);
                }    
            }
        }
    }

    private int AttributeID
    {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["attrid"], out id);
            return id;
        }
    }

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }

    protected void ibtnDelete_Click(object sender, ImageClickEventArgs e)
    {
        hdnImage.Value = "";
        SetData();
        if (this.AttributeID > 0)
        {
            _attr.Update();
            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgRecordUpdatedSuccessfully);
            Response.Redirect(Request.RawUrl);            
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Inventory_AddNewSize : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MessageState.SetGlobalMessage(MessageType.Success, "");
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                if (BusinessUtility.GetString(Request.QueryString["Status"]) == "INDIVIDUAL")
                {
                    trIsAxctive.Visible = true;
                    if (BusinessUtility.GetInt(Request.QueryString["SizeID"]) > 0)
                    {
                        ProductSize objProductSize = new ProductSize();
                        objProductSize.SizeID = BusinessUtility.GetInt(Request.QueryString["SizeID"]);
                        var lst = objProductSize.GetAllSizeList(null, Globals.CurrentAppLanguageCode);
                        foreach (var item in lst)
                        {
                            txtSizeNameEn.Text = item.SizeEn;
                            txtSizeNameFr.Text = item.SizeFr;
                            txtSeq.Text = BusinessUtility.GetString(item.Seq);
                            //txtShortName.Text = item.ShortName;
                            rblstActive.SelectedValue = BusinessUtility.GetBool(item.IsActive) ? "1" : "0";
                        }

                    }
                }
                txtSizeNameFr.Focus();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            ProductSize objProductSize = new ProductSize();
            objProductSize.SizeEn = txtSizeNameEn.Text;
            objProductSize.SizeFr = txtSizeNameFr.Text;
            //objCollection.ShortName = txtShortName.Text;
            objProductSize.IsActive = BusinessUtility.GetInt(rblstActive.SelectedItem.Value);
            objProductSize.SizeID = BusinessUtility.GetInt(Request.QueryString["SizeID"]);
            objProductSize.Seq = BusinessUtility.GetInt(txtSeq.Text);
            if (objProductSize.SaveSize(null, CurrentUser.UserID) == true)
            {
                if (BusinessUtility.GetString(Request.QueryString["Status"]) == "INDIVIDUAL")
                {
                    if (BusinessUtility.GetInt(Request.QueryString["SizeID"]) > 0)
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgSizeUpdate);
                        Globals.RegisterReloadParentScript(this);
                        Globals.RegisterCloseDialogScript(this);
                        return;
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgSizeCreate);
                        Globals.RegisterReloadParentScript(this);
                        Globals.RegisterCloseDialogScript(this);
                        return;
                    }
                }
                //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated );
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.ColorCreated({0});", objProductSize.SizeID), true);
            }
            else
            {
                //MessageState.SetGlobalMessage(MessageType.Failure, "Collection Creation failure");
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }

        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
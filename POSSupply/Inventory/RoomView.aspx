﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="RoomView.aspx.cs" Inherits="Admin_RoomView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <%= Resources.Resource.lblImport%>:
            <%= Resources.Resource.lblCustomerUser%></b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <asp:Button ID="btnAddBuilding" runat="server" Text="<%$Resources:Resource, lblAddRoom %>" CausesValidation="false" />
        <iCtrl:IframeDialog ID="mdAdd" TriggerControlID="btnAddBuilding" Width="400" Height="240"
            Url="RoomEdit.aspx?cg=R&jscallback=reloadGrid" Title="<%$Resources:Resource, lblAddRoom %>" runat="server">
        </iCtrl:IframeDialog>  
        
        <asp:Button ID="btnApplyDiscount" runat="server" Text="<%$Resources:Resource, lblApplyDiscount %>" CausesValidation="false" OnClientClick="return applyDiscountPopup();" />   
        <asp:Button ID="btnApplyBlackOuts" runat="server" Text="<%$Resources:Resource, lblApplyBlackOuts %>" CausesValidation="false" OnClientClick="return applyBlackOutsPopup();" />     
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif" CssClass="csv_export" />
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">    
    <div id="grid_wrapper" style="width: 100%;">
        <trirand:JQGrid runat="server" ID="grdCategories" DataSourceID="sdsCategories" Height="300px"
            AutoWidth="True" OnCellBinding="grdCategories_CellBinding" 
            OnDataRequesting="grdCategories_DataRequesting" MultiSelect="True" 
            MultiSelectMode="SelectOnCheckBoxClickOnly" 
            onrowdeleting="grdCategories_RowDeleting" 
            onrowediting="grdCategories_RowEditing">
            <Columns>
                <trirand:JQGridColumn DataField="RoomID" HeaderText=""
                    Editable="false" PrimaryKey="True" Visible="false" />                
                <trirand:JQGridColumn DataField="BuildingTitle" HeaderText="<%$ Resources:Resource, lblBuilding %>" Editable="false" />
                <trirand:JQGridColumn DataField="RoomTitle" HeaderText="<%$ Resources:Resource, lblRoom %>" Editable="false" />                
                <trirand:JQGridColumn DataField="RoomType" HeaderText="<%$ Resources:Resource, lblRoomType %>" Editable="false" /> 
                <trirand:JQGridColumn DataField="ProritySingle" HeaderText="<%$ Resources:Resource, lblProritySingle %>" Editable="true" Width="70" TextAlign="Center" /> 
                <trirand:JQGridColumn DataField="PriorityCouple" HeaderText="<%$ Resources:Resource, lblPriorityCouple %>" Editable="true" Width="70" TextAlign="Center" />  
                <trirand:JQGridColumn DataField="Seq" HeaderText="<%$ Resources:Resource, lblSeq %>" Editable="true" Width="50" TextAlign="Center" /> 
                <trirand:JQGridColumn DataField="RoomID" HeaderText="<%$ Resources:Resource, lblPrice %>" Editable="true" Width="50" TextAlign="Center" /> 
                <trirand:JQGridColumn DataField="RoomID" HeaderText="<%$ Resources:Resource, lblManageBlackOut %>" Editable="false" Sortable="false" TextAlign="Center" /> 
                <trirand:JQGridColumn DataField="RoomID" HeaderText="<%$ Resources:Resource, lblManageDiscount %>" Editable="false" Sortable="false" TextAlign="Center" /> 
                <trirand:JQGridColumn DataField="IsActive" HeaderText="<%$ Resources:Resource, lblIsActive %>" Editable="false" Sortable="false" TextAlign="Center" Width="50" /> 
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="RoomID"
                    Sortable="false" TextAlign="Center" Width="50" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="RoomID"
                    Sortable="false" TextAlign="Center" Width="50" />
            </Columns>
            <PagerSettings PageSize="30" PageSizeOptions="[30,100,200]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="loadComplete" />
        </trirand:JQGrid>
        <asp:SqlDataSource ID="sdsCategories" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
        <iCtrl:IframeDialog ID="mdEdit" Width="400" Height="240" Title="Edit Room"
            Dragable="true" TriggerSelectorClass="pop_edit" TriggerSelectorMode="Class"
            UrlSelector="TriggerControlHrefAttribute" runat="server">
        </iCtrl:IframeDialog>                
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" Runat="Server">
    <div>
        <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        <asp:Literal ID="ltSearchForm" Text="<%$Resources:Resource,lblSearchForm %>" runat="server" />
                    </div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label CssClass="filter-key" ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch" ></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="Search" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%= Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        //Variables        
        var gridID = "<%=grdCategories.ClientID %>";
        var searchPanelID = "<%=SearchPanel.ClientID %>";
        $grid = $("#<%=grdCategories.ClientID %>");


        //Function To Resize the grid
        function resize_the_grid() {
            $grid.fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $grid.onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $grid.onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        function reloadGrid(event, ui) {
            $grid.trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function loadComplete(data) {
          
        }

        function deleteRow(rowID) {
            // Get the currently selected row
            $grid.setSelection(rowID);
            var rowKey = $grid.getGridParam("selrow");
            $grid.jqGrid('delGridRow', rowKey, {
                errorTextFormat: function (data) {
                    if (data.responseText.substr(0, 6) == "<html ") {
                        return jQuery(data.responseText).html();
                    }
                    else {
                        return "<%=Resources.Resource.msgRoomCannnotDeleted%>";
                        // or
                        // return "Status: '" + data.statusText + "'. Error code: " +data.status;
                    }
                }
            });
        }

        function editRow(key) {            
            var grid = $grid;
            grid.setSelection(key);
            //alert(key);
            var dataToPost = {};
            dataToPost["editRowKey"] = key;
            dataToPost["_psingle"] = $("#" + key + "_psingle").val();
            dataToPost["_pcouple"] = $("#" + key + "_pcouple").val();
            dataToPost["_seq"] = $("#" + key + "_seq").val();

            for (var k in dataToPost) {
                grid.setPostDataItem(k, dataToPost[k]);
            }

            grid.trigger("reloadGrid");


            for (var k in dataToPost) {
                grid.removePostDataItem(k);
            }
        }

        $(".priceRoomLevel").live("change", function () {
            var grid = $grid;
            var dataToPost = {};
            dataToPost["editPriceRowKey"] = $(this).attr('roomid');
            dataToPost["newPrice"] = $(this).val();
            for (var k in dataToPost) {
                grid.setPostDataItem(k, dataToPost[k]);
            }

            grid.trigger("reloadGrid");
            for (var k in dataToPost) {
                grid.removePostDataItem(k);
            }
        });

        function applyDiscountPopup() {            
            arrRooms = $grid.getGridParam('selarrrow');            
            if (arrRooms.length <= 0) {
                alert("<%=Resources.Resource.lblPleaseSelectRooms%>");
                return false;
            }
            dataToPost = {}
            dataToPost.rooms = arrRooms.join(",");
            var $dialog = jQuery.FrameDialog.create({ url: 'mdApplyDiscountNew.aspx?' + $.param(dataToPost),
                title: "<%=Resources.Resource.lblApplyDiscount %>",
                loadingClass: "loading-image",
                modal: true,
                width: 850,
                height: 500,
                autoOpen: false
            });
            $dialog.dialog('open');      

            return false;
        }

        function applyBlackOutsPopup() {
            arrRooms = $grid.getGridParam('selarrrow');
            if (arrRooms.length <= 0) {
                alert("<%=Resources.Resource.lblPleaseSelectRooms%>");
                return false;
            }
            dataToPost = {}
            dataToPost.rooms = arrRooms.join(",");
            var $dialog = jQuery.FrameDialog.create({ url: 'mdApplyBlackOuts.aspx?' + $.param(dataToPost),
                title: "<%=Resources.Resource.lblApplyBlackOuts %>",
                loadingClass: "loading-image",
                modal: true,
                width: 850,
                height: 400,
                autoOpen: false
            });
            $dialog.dialog('open');

            return false;
        }

        function manageBlackOut(rowKey) {
            dataToPost = {}
            dataToPost.rooms = rowKey;
            dataToPost.sid = rowKey;
            var $dialog = jQuery.FrameDialog.create({ url: 'mdApplyBlackOuts.aspx?' + $.param(dataToPost),
                title: "<%=Resources.Resource.lblApplyBlackOuts %>",
                loadingClass: "loading-image",
                modal: true,
                width: 850,
                height: 400,
                autoOpen: false
            });
            $dialog.dialog('open');

            return false;
        }

        function manageDiscount(rowKey) {
            dataToPost = {}
            dataToPost.rooms = rowKey;
            dataToPost.sid = rowKey;
            var $dialog = jQuery.FrameDialog.create({ url: 'mdApplyDiscountNew.aspx?' + $.param(dataToPost),
                title: "<%=Resources.Resource.lblApplyDiscount %>",
                loadingClass: "loading-image",
                modal: true,
                width: 890,
                height: 530,
                autoOpen: false
            });
            $dialog.dialog('open');

            return false;
        }
    </script>  
</asp:Content>


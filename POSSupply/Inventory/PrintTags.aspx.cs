﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Inventory_PrintTags : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<PrintSchema> data = new List<PrintSchema>();

        for (int i = 1; i <= this.Count; i++)
        {
            PrintSchema sch = new PrintSchema();
            sch.Column1 = this.Barcode;
            sch.Column2 = GetProductName();
            if (BusinessUtility.GetDouble(this.ProductPrice) > 0.0)
            {
                sch.Column3 = GetCurrency();
                sch.Column4 = string.Format("{0:F}", BusinessUtility.GetDouble(this.ProductPrice));
            }
            data.Add(sch);
        }

        dlstTags.DataSource = data;
        dlstTags.DataBind();
    }


    private string GetCurrency()
    {
        if (!string.IsNullOrEmpty(this.CurrencyCode.Trim()))
        {
            return this.CurrencyCode.Trim().ToUpper();
        }
        return "CAD";
    }

    private string GetProductName()
    {
        if (this.ProductName.Trim().Length > 15)
        {
            return this.ProductName.Trim().Substring(0, 15);
        }
        return this.ProductName.Trim();
    }

    private string Barcode {
        get {
            return BusinessUtility.GetString(Request.QueryString["tag"]);
        }
    }

    public string ProductName
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["name"]);
        }
    }

    public double ProductPrice
    {
        get
        {
            double p = 0;
            double.TryParse(Request.QueryString["price"], out p);
            return p;            
        }
    }

    public string CurrencyCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["cur"]);
        }
    }

    public int Count {
        get
        {
            int c = 0;
            int.TryParse(Request.QueryString["count"], out c);
            return c;
        }
    }
}
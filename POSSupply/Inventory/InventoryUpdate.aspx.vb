Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Data
Imports System.Data.Odbc
Imports System.IO
Imports iTECH.InbizERP.BusinessLogic
Imports iTECH.Library.DataAccess.MySql

Partial Class Inventory_InventoryUpdate
    Inherits BasePage
    Public objProduct As New clsProducts
    Public objPrdDesc As New clsPrdDescriptions
    Public objPrdQuantity As New clsPrdQuantity
    Public objReceiving As New clsReceiving
    Dim Column As String() = New String() {"ID", "Warehouse", "UPC_Code", "In_Hand_Quantity", "Defective_Quantity", "Product_Name", "Internal_ID", "External_ID", "Block", "Floor", "Aisle", "Rack", "Bin", "Gluten_Free", "Spicy", "Contains_Nuts", "Vegetarian", "Sales_Price", "End_User_Price", "Web_Price"}
    Protected errorMessage As String

    Protected Sub Inventory_ViewProduct_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Not Page.IsPostBack Then
            subPopulateWarehouse()
            'dlWarehouses.SelectedValue = Session("UserWarehouse")
        End If
        If (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY)) Then
            btnUpload.Visible = False
        End If

        If Request.QueryString("type") = "" Then
            rblUpdateType.Visible = True
        End If

        'btnUpload.Attributes.Add("onclick", "javascript:return " + "confirm('" + "Count.." + " ')")
        btnUpload.Attributes.Add("onclick", "javascript:return " + "funConfirm()")
    End Sub

    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub

    Public Sub DownloadCSV(ByVal Column() As String, ByVal dt As DataTable, ByVal WarehouseCode As String)
        'Dim CSVPath As String = "Product_" & WarehouseCode & "_" & Date.Now.Year & Date.Now.Month & Date.Now.Day & Date.Now.Hour & Date.Now.Minute & Date.Now.Second & ".csv"
        Dim CSVPath As String = "Product_" & WarehouseCode & "_" & Date.Now.Year & Date.Now.Month & Date.Now.Day & Date.Now.Hour & Date.Now.Minute & Date.Now.Second & ".xlsx"
        'Dim sw As StreamWriter = New StreamWriter(Request.PhysicalApplicationPath & "\Upload\Export\" & CSVPath, True, Encoding.GetEncoding(1252)) 'Added by mukesh 20130730
        'Dim sw As StreamWriter = New StreamWriter(Request.PhysicalApplicationPath & "\Upload\Export\" & CSVPath)
        'Dim data As String
        'Dim i As Integer
        'Dim j As Integer
        'Dim k As Integer
        'Dim columns As Integer = dt.Columns.Count
        'Dim rows As Integer = dt.Rows.Count
        'Dim last As Integer = columns - 1

        'For i = 0 To columns - 1
        '    data = FormatData(Column(i))
        '    sw.Write(data)
        '    If i <> last Then
        '        sw.Write(",")
        '    End If
        'Next
        'sw.Write(vbCrLf)

        'For j = 0 To rows - 1
        '    For k = 0 To columns - 1
        '        data = dt.Rows(j).Item(k).ToString
        '        data = FormatData("'" & data & "'") 'Added by mukesh 20130730
        '        sw.Write(data)
        '        If k <> last Then
        '            sw.Write(",")
        '        End If
        '    Next
        '    If j <> rows - 1 Then
        '        sw.Write(vbCrLf)
        '    End If
        'Next

        'sw.Close()
        'ExcelHelper.Instance.Create(dt, Request.PhysicalApplicationPath & "\Upload\Export\" & CSVPath)
        ExcelHelper.Instance.ExportToExcel(dt, Request.PhysicalApplicationPath & "Upload\Export\" & CSVPath)
        Response.Redirect("~/Upload/Export/" & CSVPath)
    End Sub

    'Populate Warehouse
    Private Sub subPopulateWarehouse()
        Dim objWhs As New clsWarehouses
        objWhs.PopulateWarehouse(dlWarehouses)
        dlWarehouses.Items.RemoveAt(0)
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = liWarehouseLocation
        dlWarehouses.Items.Insert(0, itm)
        objWhs = Nothing
    End Sub

    Private Shared Function FormatData(ByVal data As String) As String
        data = data.Trim
        data = data.Replace("""", """""")
        data = """" & data & """"
        Return data
    End Function

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        If Request.QueryString("type") = "" Then
            Dim WarehouseCode As String = dlWarehouses.SelectedValue
            Dim objDB As New clsDataClass
            Dim strSQL As String = "SELECT productID, '" & WarehouseCode & "' as Warehouse, prdUPCCode, prdOhdQty, prdDefectiveQty, prdName, prdIntID, prdExtID, prdBlock, "
            strSQL += "prdFloor, prdAisle, prdRack, prdBin, prdIsGlutenFree, prdSpicyLevel, prdIsContainsNuts, prdIsVegetarian" 'Edited by mukesh 20130718'
            If radYes.Checked = True Then
                strSQL += ", prdSalePricePerMinQty, prdEndUserSalesPrice, prdWebSalesPrice"
            End If
            strSQL += " FROM products p left join prdquantity q on p.productID = q.prdID and q.prdWhsCode = '" & WarehouseCode & "'"
            ' where p.prdIsKit=0;"
            Dim dt As DataTable = objDB.GetDataset(strSQL).Tables(0)
            objDB.CloseDatabaseConnection()
            DownloadCSV(Column, dt, WarehouseCode)
        ElseIf Request.QueryString("type") = "S" Then
            Dim WarehouseCode As String = dlWarehouses.SelectedValue
            Dim objDB As New clsDataClass
            Dim strSQL As String = " "
            strSQL += " SELECT DISTINCT p.ProductID,  p.prdName AS Name, pdFr.prdSmallDesc AS Description_Fr, pdEn.prdSmallDesc AS Description_En, pClthDesc.Collection AS Col_ID, pClthDesc.Style AS Style, "
            strSQL += " pClthDesc.Material AS MatID, '' AS  Gauge, '' TextureID, '' AS WebSite_CatgeoryID, ''  AS Website_SubCatg_ID, '' AS Neckline_ID, '' AS Extra_Catg_ID, '' AS Sleeve_ID, '' AS Silhouette_ID, "
            strSQL += " '' AS Trend_ID, '' AS Keywords_ID, pClthDesc.longueurLength AS LengthSize, pmDtl.prdIncrementLength AS LengthIncrement, pClthDesc.busteChest  AS Chest, pmDtl.prdIncrementChest AS Chest_Increment, "
            strSQL += " pClthDesc.epauleShoulder AS Shoulder, pmDtl.prdIncrementShoulder AS ShoulderIncrement, pClthDesc.bicep AS Biceps, pmDtl.prdIncrementBicep AS BicepsIncrement, "
            strSQL += " p.prdWeight AS ProdWgt, '' AS ProdWgtIncrement,"
            If radYes.Checked = True Then
                strSQL += " p.prdFOBPrice AS FOBPrice, p.prdLandedPrice AS LandedPrice, p.prdSalePricePerMinQty AS WholeSalePrice, p.prdEndUserSalesPrice AS RetailPrice, p.prdWebSalesPrice AS WebPrice, "
            End If
            strSQL += "  p.prdWeightPkg AS PackageWgt, p.prdWidthPkg AS PackageWidth, p.prdLengthPkg AS PackageLength, p.prdIsWeb AS IsWebActive, p.IsPOSMenu AS IsPOSMenu , '" & WarehouseCode & "' as Warehouse, "
            strSQL += " p.prdUPCCode, psz.Sizeen AS Size ,pClthDesc.Color AS ColorID "
            strSQL += "FROM PRODUCTS AS p "
            strSQL += "INNER JOIN prddescriptions AS pdEn ON pdEn.ID = p.ProductID AND pdEn.DescLang ='en' "
            strSQL += "INNER JOIN prddescriptions AS pdFr ON pdFr.ID = p.ProductID AND pdFr.DescLang ='fr' "
            strSQL += "INNER JOIN ProductClothDesc AS pClthDesc ON pClthDesc.ProductID = p.ProductID "
            strSQL += " LEFT OUTER JOIN ProdMasterDetail AS pmDtl ON pmDtl.MasterID = p.MasterID "
            'strSQL += " INNER JOIN productsize psz on psz.sizeID = p.SizeID "
            strSQL += " INNER JOIN productsize psz on psz.sizeID = pClthDesc.Size "
            strSQL += " LEFT JOIN prdquantity q on p.productID = q.prdID and q.prdWhsCode = '" & WarehouseCode & "'"
            strSQL += " where 1=1 /*AND p.prdIsKit=0*/  ORDER BY p.prdName; "

            Dim dt As DataTable = objDB.GetDataset(strSQL).Tables(0)
            objDB.CloseDatabaseConnection()
            Dim ds As New DataSet
            Dim query = From r In dt Group By key = r.Field(Of String)("Size") Into Group
            For Each grp In query
                Dim dColumn As New DataColumn()
                dColumn.ColumnName = "SKU-" + grp.key
                dt.Columns.Add(dColumn)

                Dim dColumnQty As New DataColumn()
                dColumnQty.ColumnName = "Qty-" + grp.key
                dt.Columns.Add(dColumnQty)
            Next

            For Each row As DataRow In dt.Rows
                Dim objPrdClothDes As New Product.ProductClothDesc()
                objPrdClothDes.ProductID = Convert.ToInt32(row("ProductID"))
                objPrdClothDes.getClothDesc(New DbHelper, 0)
                row("TextureID") = objPrdClothDes.Texture
                row("WebSite_CatgeoryID") = objPrdClothDes.WebSiteCatg
                row("Website_SubCatg_ID") = objPrdClothDes.WebSiteSubCatg
                row("Neckline_ID") = objPrdClothDes.Neckline
                row("Extra_Catg_ID") = objPrdClothDes.ExtraCatg
                row("Sleeve_ID") = objPrdClothDes.Sleeve
                row("Silhouette_ID") = objPrdClothDes.Silhouette
                row("Trend_ID") = objPrdClothDes.Trend
                row("Keywords_ID") = objPrdClothDes.KeyWord
                row("Gauge") = objPrdClothDes.Gauge
            Next row

            Dim dtCopyProduct As DataTable
            dtCopyProduct = dt.Clone()

            Dim queryGroupByName = From r In dt Group By key = r.Field(Of String)("Name") Into Group
            For Each grp In queryGroupByName
                Dim pName As String
                pName = grp.key
                Dim resultGroupByPname() As DataRow = dt.Select("Name  = '" + pName + "'")
                Dim iRowCount = 0
                For Each rowPDetail As DataRow In resultGroupByPname
                    If (iRowCount = 0) Then
                        Dim dRowPDesc As DataRow = dtCopyProduct.NewRow
                        dRowPDesc("Name") = Convert.ToString(rowPDetail("Name"))
                        dRowPDesc("Description_Fr") = Convert.ToString(rowPDetail("Description_Fr"))
                        dRowPDesc("Description_En") = Convert.ToString(rowPDetail("Description_En"))
                        dRowPDesc("Col_ID") = Convert.ToString(rowPDetail("Col_ID"))
                        dRowPDesc("Style") = Convert.ToString(rowPDetail("Style"))
                        dRowPDesc("MatID") = Convert.ToString(rowPDetail("MatID"))
                        dRowPDesc("Gauge") = Convert.ToString(rowPDetail("Gauge"))
                        dRowPDesc("TextureID") = Convert.ToString(rowPDetail("TextureID"))
                        dRowPDesc("WebSite_CatgeoryID") = Convert.ToString(rowPDetail("WebSite_CatgeoryID"))
                        dRowPDesc("Website_SubCatg_ID") = Convert.ToString(rowPDetail("Website_SubCatg_ID"))
                        dRowPDesc("Neckline_ID") = Convert.ToString(rowPDetail("Neckline_ID"))
                        dRowPDesc("Extra_Catg_ID") = Convert.ToString(rowPDetail("Extra_Catg_ID"))
                        dRowPDesc("Sleeve_ID") = Convert.ToString(rowPDetail("Sleeve_ID"))
                        dRowPDesc("Silhouette_ID") = Convert.ToString(rowPDetail("Silhouette_ID"))
                        dRowPDesc("Trend_ID") = Convert.ToString(rowPDetail("Trend_ID"))
                        dRowPDesc("Keywords_ID") = Convert.ToString(rowPDetail("Keywords_ID"))
                        dRowPDesc("LengthSize") = Convert.ToString(rowPDetail("LengthSize"))
                        dRowPDesc("LengthIncrement") = Convert.ToString(rowPDetail("LengthIncrement"))
                        dRowPDesc("Chest") = Convert.ToString(rowPDetail("Chest"))
                        dRowPDesc("Chest_Increment") = Convert.ToString(rowPDetail("Chest_Increment"))
                        dRowPDesc("Shoulder") = Convert.ToString(rowPDetail("Shoulder"))
                        dRowPDesc("ShoulderIncrement") = Convert.ToString(rowPDetail("ShoulderIncrement"))
                        dRowPDesc("Biceps") = Convert.ToString(rowPDetail("Biceps"))
                        dRowPDesc("BicepsIncrement") = Convert.ToString(rowPDetail("BicepsIncrement"))
                        If radYes.Checked = True Then
                            dRowPDesc("FOBPrice") = Convert.ToString(rowPDetail("FOBPrice"))
                            dRowPDesc("LandedPrice") = Convert.ToString(rowPDetail("LandedPrice"))
                            dRowPDesc("WholeSalePrice") = Convert.ToString(rowPDetail("WholeSalePrice"))
                            dRowPDesc("RetailPrice") = Convert.ToString(rowPDetail("RetailPrice"))
                            dRowPDesc("WebPrice") = Convert.ToString(rowPDetail("WebPrice"))
                        End If
                        dRowPDesc("ProdWgt") = Convert.ToString(rowPDetail("ProdWgt"))
                        dRowPDesc("ProdWgtIncrement") = Convert.ToString(rowPDetail("ProdWgtIncrement"))
                        dRowPDesc("PackageWgt") = Convert.ToString(rowPDetail("PackageWgt"))
                        dRowPDesc("PackageWidth") = Convert.ToString(rowPDetail("PackageWidth"))
                        dRowPDesc("PackageLength") = Convert.ToString(rowPDetail("PackageLength"))
                        dRowPDesc("IsWebActive") = Convert.ToString(rowPDetail("IsWebActive"))
                        dRowPDesc("IsPOSMenu") = Convert.ToString(rowPDetail("IsPOSMenu"))
                        dRowPDesc("Warehouse") = Convert.ToString(rowPDetail("Warehouse"))
                        dtCopyProduct.Rows.Add(dRowPDesc)
                        iRowCount = iRowCount + 1

                        Dim dtPDetail As DataTable = resultGroupByPname.CopyToDataTable()
                        Dim queryGroupbyColor = From r In dtPDetail Group By key = r.Field(Of Int32)("ColorID") Into Group
                        'Dim queryGroupbyColor = From r In dtPDetail Group By key = r.Field(Of Int64)("ColorID") Into Group
                        For Each grpColor In queryGroupbyColor
                            Dim sColorID As Int32 = Convert.ToInt32(grpColor.key)
                            Dim dvByPnameAndColor As DataView = dt.DefaultView
                            dvByPnameAndColor.RowFilter = "Name  = '" + pName + "' AND ColorID = " + Convert.ToString(sColorID)
                            Dim DtPnameAndColor As DataTable = dvByPnameAndColor.ToTable("UniqueColorID", True, "ColorID")
                            For Each rowPnameAndColor As DataRow In DtPnameAndColor.Rows
                                dRowPDesc = dtCopyProduct.NewRow
                                dRowPDesc("ColorID") = Convert.ToString(rowPnameAndColor("ColorID"))

                                Dim dvByPnameAndColorFilter As DataView = dt.DefaultView
                                dvByPnameAndColorFilter.RowFilter = "Name  = '" + pName + "' AND ColorID = " + Convert.ToString(sColorID)
                                Dim dtByPnameAndColor As DataTable = dvByPnameAndColorFilter.ToTable()
                                For Each DataRowSize As DataRow In dtByPnameAndColor.Rows
                                    Dim objProdQty As New ProductQuantity()
                                    objProdQty.PopulateObject(Convert.ToInt32(DataRowSize("ProductID")), Convert.ToString(dlWarehouses.SelectedValue))
                                    dRowPDesc("SKU-" + Convert.ToString(DataRowSize("Size"))) = Convert.ToString(DataRowSize("PrdUPCCode"))
                                    dRowPDesc("Qty-" + Convert.ToString(DataRowSize("Size"))) = Convert.ToString(objProdQty.PrdOhdQty)
                                    'objProdQty.PopulateObject(New DbHelper, Convert.ToInt32(DataRowSize("ProductID")), Convert.ToString(dlWarehouses.SelectedValue))
                                    'dRowPDesc("SKU-" + Convert.ToString(DataRowSize("Size"))) = Convert.ToString(DataRowSize("PrdUPCCode"))
                                    'dRowPDesc("Qty-" + Convert.ToString(DataRowSize("Size"))) = Convert.ToString("0")
                                Next
                                dtCopyProduct.Rows.Add(dRowPDesc)
                            Next
                        Next
                        Exit For
                    End If
                Next
            Next
            dtCopyProduct.Columns.Remove("ProductID")
            dtCopyProduct.Columns.Remove("prdUPCCode")
            dtCopyProduct.Columns.Remove("Size")
            Dim sCol As String = ""
            For Each column As DataColumn In dtCopyProduct.Columns
                If sCol = "" Then
                    sCol = column.ColumnName
                Else
                    sCol += "," + column.ColumnName
                End If
            Next column
            Column = sCol.Split(",")


            DownloadCSV(Column, dtCopyProduct, WarehouseCode)
        End If
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If CSVFile.HasFile Then
            Dim strFile As String = System.IO.Path.GetFileName(CSVFile.FileName).Trim
            Dim strFileArr() As String = Split((strFile), ".")
            Dim count As Int16 = strFileArr.Length - 1

            If strFileArr.Length = "1" Then
                lblMsg.Text = InvalidProductList
            End If

            'If strFileArr(count).ToLower = "csv" Then
            If (strFileArr(count).ToLower = "xlsx") Or strFileArr(count).ToLower = "csv" Then
                Try
                    Dim SaveFolder As String = Server.MapPath("~/Upload/CSVFiles/Products/")
                    Dim SaveLocation As String = SaveFolder & strFile
                    CSVFile.SaveAs(SaveLocation)

                    Dim objImport As clsImport = New clsImport
                    Dim objImportStyle As Import = New Import
                    Dim intRecCount As Integer
                    If Request.QueryString("type") = "" Then
                        intRecCount = objImportStyle.UpdateProduct(SaveFolder, strFile, rblUpdateType.SelectedItem.Value, dlWarehouses.SelectedItem.Value)
                    ElseIf Request.QueryString("type") = "S" Then
                        intRecCount = objImportStyle.UpdateStyleProduct(SaveFolder, strFile)
                    End If
                    errorMessage = objImportStyle.ImportErrorMessage
                    lblMsg.Text = intRecCount & " " & ProductUpdatedSuccess
                    If (rblUpdateType.SelectedItem.Value.ToUpper() = "RSU") Then
                        lblMsg.Text += " " + lblProductQtyToZero
                    End If
                Catch ex As Exception
                    lblMsg.Text = ex.Message
                End Try
            Else
                lblMsg.Text = InvalidProductList
            End If
        Else
            lblMsg.Text = InvalidProductList
        End If
    End Sub
End Class

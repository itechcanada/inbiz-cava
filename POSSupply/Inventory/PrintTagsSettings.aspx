﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="PrintTagsSettings.aspx.cs" Inherits="Inventory_PrintTagsSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div id="contentBottom" style="padding: 5px; height: 120px; overflow: auto;">
        <ul class="form">
            <li>
                <div class="lbl"><%=Resources.Resource.lblTag%>:
                </div>
                <div class="input">
                    <asp:TextBox ID="txtUpcCode" runat="server" />
                </div>
                <div class="clearBoth"></div>
            </li>
             <li>
                <div class="lbl">
                    Style:
                </div>
                <div class="input">
                    <asp:TextBox ID="txtName" runat="server" />
                </div>
                <div class="clearBoth"></div>
            </li>
            <li>
                <div class="lbl">
                    <%=Resources.Resource.lblPrice%>:
                </div>
                <div class="input">
                    <asp:TextBox ID="txtPrice" runat="server" />
                </div>
                <div class="clearBoth"></div>
            </li>            
            <li>
                <div class="lbl">
                    <%=Resources.Resource.lblNoofTags%>:
                </div>
                <div class="input">
                    <asp:TextBox ID="txtNoOfTags" runat="server" MaxLength="3" />
                </div>
                <div class="clearBoth"></div>
            </li>
        </ul>
        <asp:HiddenField ID="hdnCurrency" runat="server" />
        <asp:HiddenField ID="HiddenPrdColor" runat="server" />
        <asp:HiddenField ID="HiddenPrdSize" runat="server" />
    </div>
    <div style="text-align: right; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, lblPrint %>" 
            runat="server"  OnClientClick="Javascript:funInsert();" 
            onclick="btnSave_Click"  />
        <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
            CausesValidation="False" 
            OnClientClick=" jQuery.FrameDialog.closeDialog(); return false;" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">


        $("#<%=btnSave.ClientID%>").click(function () {
            //var pramsToPrint = { tag: "", name: "", price: "", cur: "", count: "" };
            var pramsToPrint = { tag: "", name: "", price: "", clr: "", count: "", size: "" };
            pramsToPrint.tag = $("#<%=txtUpcCode.ClientID%>").val();
            pramsToPrint.name = $("#<%=txtName.ClientID%>").val();
            pramsToPrint.price = $("#<%=txtPrice.ClientID%>").val();
            //pramsToPrint.cur = $("#<%=hdnCurrency.ClientID%>").val();
            pramsToPrint.clr = $("#<%=HiddenPrdColor.ClientID%>").val();
            pramsToPrint.count = $("#<%=txtNoOfTags.ClientID%>").val();
            pramsToPrint.size = $("#<%=HiddenPrdSize.ClientID%>").val();

            //var url = 'PrintTags.aspx?' + decodeURIComponent($.param(pramsToPrint));
            var url = '../Receiving/PrintTagsNew.aspx?' + decodeURIComponent($.param(pramsToPrint));
            window.open(url, 'Print', 'height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');
            //jQuery.FrameDialog.closeDialog();            
            return false;
        });
        //
    </script>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Inventory_ProductMasterColor : BasePage
{
    private string strTaxID = "";
    private SysTaxCode _objTax = new SysTaxCode();
    // On Page Load
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/AdminLogin.aspx");
        }

        if (!IsPagePostBack(jgdvTaxes)) //Instead of IsPostBack use this to prevent postback as fress requens in each page of jqgrid
        {
            //FillGroupTax();
            txtSearch.Focus();
        }

        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "deleteColor")
        {
            try
            {
                int iColorID = BusinessUtility.GetInt(Request.Form["ColorID"]);
                if (iColorID > 0)
                {
                    ProductColor objColor = new ProductColor();
                    objColor.ColorID = iColorID;
                    if (objColor.Delete(null, CurrentUser.UserID) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }

            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
    }
    //private void FillGroupTax()
    //{
    //    SysTaxCodeDesc objTG = new SysTaxCodeDesc();
    //    objTG.FillListControl(null, dlTaxGroup, new ListItem(Resources.Resource.liTaxGroup, "0"));
    //}
    //protected void lnkAddTaxes_Click(object sender, System.EventArgs e)
    //{
    //    Response.Redirect("~/Taxes/AddEditTaxes.aspx");
    //}

    protected void jgdvTaxes_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 2)
        {

            e.CellHtml = string.Format(@"<img src=""{0}"" />", ResolveUrl(string.Format("~/images/{0}", e.CellHtml)));// e.RowValues[idxDSStatusCol]

            //if (e.RowValues[1] == "1")
            //{

            //    e.CellHtml = string.Format(@"<img src=""{0}"" />", ResolveUrl(string.Format("~/images/{0}", "green.gif")));
            //}
            //else if (e.RowValues[1] == "0")
            //{
            //    e.CellHtml = string.Format(@"<img src=""{0}"" />", ResolveUrl(string.Format("~/images/{0}", "red.gif")));
            //}

        }
        if (e.ColumnIndex == 3)
        {
            int iColorID = BusinessUtility.GetInt(e.CellHtml);
            e.CellHtml = string.Format(@"<a class=""edit-Taxs""  href=""javascript:void(0);"" onclick=""editPopup('{0}')"">{1}</a>", iColorID, Resources.Resource.edit);
        }
        if (e.ColumnIndex == 4)
        {
            int iColorID = BusinessUtility.GetInt(e.CellHtml);
            e.CellHtml = string.Format(@"<a href=""{0}""  onclick=""deleteColor('{1}')"">{2}</a>", "javascript:void(0);", BusinessUtility.GetString(iColorID), Resources.Resource.delete);
        }
    }

    protected void jgdvTaxes_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        ProductColor objProductColor = new ProductColor();
        string search = Request.QueryString[txtSearch.ClientID];
        //int id = 0;
        //int.TryParse(Request.QueryString[dlTaxGroup.ClientID], out id);

        //if (Request.QueryString.AllKeys.Contains("IsDeleting") && Request.QueryString["IsDeleting"] == "1")
        //{
        //    int txGrp = 0;
        //    string[] arrKey = Request.QueryString["CollectionID"].Split('~');
        //    if (arrKey.Length > 1 && int.TryParse(arrKey[0], out txGrp) && txGrp > 0)
        //    {
        //        objCollection = new Collection();
        //        objCollection.CollectionID = BusinessUtility.GetInt(arrKey[1]);
        //        //_objTax.Delete(null, txGrp, arrKey[1]);
        //    }
        //}

        objProductColor = new ProductColor();
        objProductColor.SearchKey = search;
        //jgdvTaxes.DataSource = _objTax.GetAllTaxes(null, id, search);
        jgdvTaxes.DataSource = objProductColor.GetAllColorList(null, Globals.CurrentAppLanguageCode);
    }
}
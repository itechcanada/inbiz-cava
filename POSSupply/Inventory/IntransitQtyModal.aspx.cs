﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Inventory_IntransitQtyModal : BasePage
{
    Receiving _rcv = new Receiving();

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void grdQuantity_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        sdsQuantity.SelectCommand = _rcv.GetSql(sdsQuantity.SelectParameters, this.ProductID, this.WhsCode);
    }

    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    public string WhsCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["whscode"]);
        }
    }
}
<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="AssociateVendor.aspx.vb" Inherits="Inventory_AssociateVendor" %>


<%@ Register src="../Controls/CommonSearch/InventorySearch.ascx" tagname="InventorySearch" tagprefix="uc1" %>


<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>
    
    <uc1:InventorySearch ID="InventorySearch1" runat="server" />
    
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblHeading"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent">        
        <asp:UpdatePanel runat="server" ID="udpnlAssVendor">
            <ContentTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">                    
                    <tr height="25">
                        <td class="tdAlign" width="35%">
                            <asp:Label ID="Label1" Text="<%$ Resources:Resource, lblInventryVendor %>" CssClass="lblBold"
                                runat="server" />
                        </td>
                        <td width="1%">
                        </td>
                        <td align="left" width="64%">
                            <asp:DropDownList ID="dlVendor" CssClass="innerText" runat="server" Width="336px">
                            </asp:DropDownList>
                            <span class="style1">*</span>
                            <asp:CustomValidator ID="custvalVendor" SetFocusOnError="true" runat="server" ErrorMessage="<%$ Resources:Resource, custvalVendor %>"
                                ClientValidationFunction="funSelectVendor" Display="None"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                            <asp:Label ID="lblCostPrice" Text="<%$ Resources:Resource, lblPrdCostPrice %>" CssClass="lblBold"
                                runat="server" />
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtCostPrice" runat="server" CssClass="txtBoxPrd" /><span class="style1">
                                *</span>
                            <asp:CompareValidator ID="comvalCostPrice" EnableClientScript="true" SetFocusOnError="true"
                                ControlToValidate="txtCostPrice" Operator="DataTypeCheck" Type="double" Display="None"
                                ErrorMessage="<%$ Resources:Resource, comvalCostPrice %>" runat="server" />
                            <asp:RequiredFieldValidator ID="reqvalCostPrice" runat="server" ControlToValidate="txtCostPrice"
                                ErrorMessage="<%$ Resources:Resource, reqvalCostPrice %>" SetFocusOnError="true"
                                Display="None"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <table width="100%">
                                <tr>
                                    <td width="25%">
                                        <div class="buttonwrapper">
                                            <a id="imgCmdSave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                                style="min-width: 120px; text-align: center;">
                                                <%=Resources.Resource.cmdCssSubmit%></span></a></div>
                                    </td>
                                    <td width="75%" align="left">
                                        <div class="buttonwrapper">
                                            <a id="imgCmdReset" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                                <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                    <%=Resources.Resource.cmdCssReset%></span></a></div>
                                    </td>
                                </tr>
                            </table>
                            <%--<asp:ImageButton runat=server ID=imgCmdSave ImageUrl="~/images/submit.png"  />
                <asp:ImageButton runat=server ID=imgCmdReset  CausesValidation=false ImageUrl="~/images/Reset.png" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td height="5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="padding-right: 15px;" colspan="3">
                            <div style="height: 22px">
                                <asp:UpdateProgress runat="server" ID="Updateprogress" AssociatedUpdatePanelID="udpnlAssVendor"
                                    DisplayAfter="10">
                                    <ProgressTemplate>
                                        <img src="../Images/wait22trans.gif" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td align="center" colspan="3">
                            <asp:GridView ID="grdvAssVendor" runat="server" AllowSorting="True" DataSourceID="sqlsdAssVendor"
                                AllowPaging="True" PageSize="10" PagerSettings-Mode="Numeric" CellPadding="0"
                                Style="border-collapse: separate;" CssClass="view_grid650" 
                                GridLines="none" AutoGenerateColumns="False"
                                UseAccessibleHeader="False" DataKeyNames="prdVendorID" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="vendorName" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, grdvPrdVendorName %>"
                                        ReadOnly="True" SortExpression="vendorName">
                                        <ItemStyle Width="500px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="prdCostPrice" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false"
                                        HeaderText="<%$ Resources:Resource, grdvCostPrice %>" ReadOnly="True" SortExpression="prdCostPrice">
                                        <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdvDelete %>" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-ForeColor="white">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                                ToolTip="<%$ Resources:Resource, grdvDelete %>" CommandArgument='<%# Eval("prdVendorID") %>'
                                                CommandName="Delete" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="grid_footer" />
                                <RowStyle CssClass="grid_rowstyle" />
                                <PagerStyle CssClass="grid_footer" />
                                <HeaderStyle CssClass="grid_header" Height="26px" />
                                <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                <PagerSettings PageButtonCount="20" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="sqlsdAssVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <asp:ValidationSummary ID="sumvalAssociate" runat="server" ShowMessageBox="true"
                        ShowSummary="false" />
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
    </div>
    <script type="text/javascript" language="javascript">
        function funSelectVendor(source, arguments) {
            if (window.document.getElementById('<%=dlVendor.ClientID%>').value != "0") {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }
    </script>
</asp:Content>

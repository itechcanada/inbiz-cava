﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="ProductTagsModal.aspx.cs" Inherits="Inventory_ProductTagsModal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
<div id="contentBottom" style="padding: 5px; height: 220px; overflow: auto;">
    <asp:Repeater ID="rptTags" runat="server">
        <HeaderTemplate>
            <ol>
        </HeaderTemplate>
        <ItemTemplate>
            <li style="padding: 5px;"><a href="javascript:void(0);">
                <%# Eval("Tag")%></a></li>
        </ItemTemplate>
        <FooterTemplate>
            </ol>
        </FooterTemplate>
    </asp:Repeater>
    <asp:SqlDataSource ID="sdsTags" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
        ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>

    <%if (rptTags.Items.Count == 0)
      {
          %>
          <div style="text-align:center; padding:10px; color:Red;">
            <b> Tags Not Available!</b>  
          </div>          
          <%
      } %>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using Trirand.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class Inventory_mdApplyBlackOuts : BasePage
{
    ProductBlackOut _disc = new ProductBlackOut();    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdDiscounts))
        {           
            this.Initialize();
        }
    }   

    private int SingleRoomID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["sid"], out id);            
            return id;
        }
    }

    private int FirstRoomID
    {
        get {
            string rooms = Request.QueryString["rooms"];
            if (!string.IsNullOrEmpty(rooms))
            {
                 int[] roomsID = StringUtil.GetArrayFromJoindString(",", rooms);
                 return roomsID[0];
            }
            return 0;
        }
    }

    public void Initialize()
    {        
        btnReset.Style[HtmlTextWriterStyle.Display] = "none";
        DropDownHelper.FillBlackOutReasonDDL(ddlReason, new ListItem("", ""), Globals.CurrentAppLanguageCode);
        if (this.SingleRoomID <= 0)
        {
            string script = "$('.dicount_confirm').click(function (){";
            script += string.Format("return confirm('{0}');", Resources.Resource.msgBlackOutAddingConfirm.Replace("'", "\'"));
            script += "});";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "dicount_confirm", script, true);
        }

        //grdDiscounts.Columns[4].Visible = this.IsSingleRoomEdit;
        //grdDiscounts.Columns[5].Visible = this.IsSingleRoomEdit; //Disallow deleting in case of multiple
    }

    private List<BlackoutView> BlackoutDataSource
    {
        get
        {
            if (Session["BlackoutDataSource"] != null)
            {
                return (List<BlackoutView>)Session["BlackoutDataSource"];
            }
            return new List<BlackoutView>();
        }
    }

    protected void grdDiscounts_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        int id = this.SingleRoomID > 0 ? this.SingleRoomID : this.FirstRoomID;
        Session["BlackoutDataSource"] = _disc.GetAllByRoom(id, Globals.CurrentAppLanguageCode);
        grdDiscounts.DataSource = this.BlackoutDataSource;
    }

    //Apply discount By Date Range
    protected void btnApply_Click(object sender, EventArgs e)
    {        
        _disc.EndDate = BusinessUtility.GetDateTime(txtEndDate.Text, DateFormat.MMddyyyy);
        _disc.StartDate = BusinessUtility.GetDateTime(txtStartDate.Text, DateFormat.MMddyyyy);       
        _disc.Reason = BusinessUtility.GetInt(ddlReason.SelectedValue);

        if (_disc.StartDate > _disc.EndDate)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPleaseSelectValidDateRange);
            return;
        }
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString["rooms"]))
            {
                string rooms = Request.QueryString["rooms"];                
                int[] roomsID = StringUtil.GetArrayFromJoindString(",", rooms);
                if (roomsID.Length > 0)
                {                    
                    if (hdnID.Value.Length > 0)
                    {
                        _disc.UpdateBlackoutToRoom(hdnID.Value, roomsID);
                        Response.Redirect(Request.RawUrl);
                    }
                    else
                    {
                        _disc.ApplyBlackoutToRooms(this.BlackoutDataSource, roomsID);
                        Response.Redirect(QueryString.GetModifiedUrl("sid", roomsID[0].ToString()));           
                    }                    
                }
            }             
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.BLACK_OUT_DATE_REANGE_ALREADY_EXISTS)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgBlackoutDateRangeAlreadyExisists);
            }
        }
    }   

    protected void grdDiscounts_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 4)
        {
            //e.CellHtml = Resources.Resource.lblEdit;
            e.CellHtml = string.Format(@"<a href='javascript:;' onclick=""setSelectedItemData('{0}')"">{1}</a>", e.RowKey, Resources.Resource.lblEdit);
        }
        if (e.ColumnIndex == 5)
        {
            e.CellHtml = string.Format(@"<a href='javascript:;' onclick=""deleteItem('{0}')"">{1}</a>", e.RowKey, Resources.Resource.delete);
        }  
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl);
    }
    
    protected void grdDiscounts_DataRequested(object sender, JQGridDataRequestedEventArgs e)
    {
        //Session["AppliedDiscountCount"] = e.DataTable.Rows.Count;
    }    

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            try
            {
                BlackOutReason _blReason = new BlackOutReason();
                _blReason.ReasonEn = txtReasonEn.Text;
                _blReason.ReasonFr = txtReasonFr.Text;
                _blReason.IsActive = true;
                _blReason.Insert();

                if (_blReason.ID > 0)
                {
                    txtReasonEn.Text = string.Empty;
                    txtReasonFr.Text = string.Empty;
                    DropDownHelper.FillBlackOutReasonDDL(ddlReason, new ListItem("", ""), Globals.CurrentAppLanguageCode);
                    ddlReason.SelectedValue = _blReason.ID.ToString();
                }
            }
            catch 
            {
                
            }            
        }
    }

    private bool IsSingleRoomEdit
    {
        get
        {
            string str = Request.QueryString["rooms"];
            if (!string.IsNullOrEmpty(str))
            {
                return str.Split(',').Length == 1;
            }
            return false;
        }
    }

    protected string GetJsonArrayRoomID()
    {
        string rooms = Request.QueryString["rooms"];
        if (!string.IsNullOrEmpty(rooms))
        {
            int[] roomsID = StringUtil.GetArrayFromJoindString(",", rooms);
            return JsonConvert.SerializeObject(roomsID);
        }
        return JsonConvert.SerializeObject(new int[] { this.SingleRoomID });
    }
}
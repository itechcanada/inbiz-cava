Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Partial Class Inventory_ProductSalePrice
    Inherits BasePage
    Public objprdsalePrice As New clsProductSalesPrice
    Public objPrdDesc As New clsPrdDescriptions
    Protected Sub Inventory_ProductSalePrice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        lblHeading.Text = objPrdDesc.funProductName & lblPrdProductSalePrice
        If Not Page.IsPostBack Then
            If Request.QueryString("PrdID") <> "" Then
                objprdsalePrice.ProductID = Request.QueryString("prdID")
                fillGridViewProductSalePrice()
            End If
        End If
        If Session("UserModules").ToString.Contains("INROL") = True And (Session("UserModules").ToString.Contains("STK") = False Or Session("UserModules").ToString.Contains("ADM") = False) Then
            imgCmdSave.Visible = False
        End If
        txtSalePrice.Focus()
    End Sub
    Public Sub subFillData()
        objprdsalePrice.ProductID = Request.QueryString("prdID")
        objprdsalePrice.subGetProductSalesPriceInfo()
        txtFromQty.Text = objprdsalePrice.FromQty
        txtFromRushQty.Text = objprdsalePrice.FromRushQty
        txtRushSalePrice.Text = objprdsalePrice.RushSalesPrice
        txtSalePrice.Text = objprdsalePrice.SalesPrice
        txtToQty.Text = objprdsalePrice.ToQty
        txtToRushQuantity.Text = objprdsalePrice.ToRushQty
    End Sub
    Public Sub subSetData()
        objprdsalePrice.FromQty = txtFromQty.Text
        objprdsalePrice.FromRushQty = txtFromRushQty.Text
        objprdsalePrice.ProductID = Request.QueryString("prdID")
        objprdsalePrice.RushSalesPrice = txtRushSalePrice.Text
        objprdsalePrice.SalesPrice = txtSalePrice.Text
        objprdsalePrice.ToQty = txtToQty.Text
        objprdsalePrice.ToRushQty = txtToRushQuantity.Text
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
  

    Protected Sub imgCmdReset_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdReset.ServerClick
        txtFromQty.Text = ""
        txtToQty.Text = ""
        txtSalePrice.Text = ""
        txtFromRushQty.Text = ""
        txtToRushQuantity.Text = ""
        txtRushSalePrice.Text = ""
        lblMsg.Text = ""
    End Sub
    ' ////////////// Event Save button Click
    Protected Sub imgCmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdSave.ServerClick
        subSetData()
        If objprdsalePrice.isGivenRangeAlreadyExist Then
            lblMsg.Text = msgProductSalePriceAlreadyExist ' "Entered Quantity Range is already exist."
        Else
            If objprdsalePrice.funInsertProductSalesPrice Then
                lblMsg.Text = msgPrdSalePriceSaved
            End If
            fillGridViewProductSalePrice()
        End If
    End Sub

    ' ///// Fills the grid view for ProductSalePrice
    Protected Sub fillGridViewProductSalePrice()
        objprdsalePrice.ProductID = Request.QueryString("prdID")
        sdsProductSalesPrice.SelectCommand = objprdsalePrice.getGridViewFillerSQL()
    End Sub

    Protected Sub gvwProductSalePrice_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwProductSalePrice.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
            "confirm('" & msgProductSalePriceDelete & "')")
        End If
        'If Session("UserModules").ToString.Contains("INROL") = True And (Session("UserModules").ToString.Contains("STK") = False Or Session("UserModules").ToString.Contains("ADM") = False) Then
        '    gvwProductSalePrice.Columns(1).Visible = False
        'End If
    End Sub

    Protected Sub gvwProductSalePrice_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvwProductSalePrice.RowDeleting
        Dim strProductSalesPriceID As String = gvwProductSalePrice.DataKeys(e.RowIndex).Value.ToString()
        lblMsg.Text = msgProductSalePriceDeleted
        objprdsalePrice.ProductID = Request.QueryString("prdID")
        objprdsalePrice.ProductSalesPriceID = strProductSalesPriceID
        sdsProductSalesPrice.DeleteCommand = objprdsalePrice.deleteProductSalePriceRowSQL
        fillGridViewProductSalePrice()
    End Sub
End Class

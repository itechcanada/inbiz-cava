﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ProductSizeCatg.aspx.cs" Inherits="Inventory_ProductSizeCatg" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
    <script type="text/javascript">
        var _alertsPreLoaded = [];
        var _usersList = [];
    </script>
    <style type="text/css">
        /*.ui-menu .ui-menu-item { white-space:nowrap; padding:0 10px 0 0; }*/
        div.autocomplete_holder input[type=text]
        {
            width: 50px;
            margin: 0 0 2px 0;
            padding: 0 0 3px;
            position: relative;
            top: 0;
            float: left;
            border: none;
        }
        div.autocomplete_holder span
        {
            display: block;
            width: auto;
            margin: 0 3px 3px 0;
            padding: 3px 20px 4px 8px;
            position: relative;
            float: left;
            text-indent: 0;
            background-Size: #eee;
            border: 1px solid #333;
            -moz-border-radius: 7px;
            -webkit-border-radius: 7px;
            border-radius: 7px;
            Size: #333;
            font: normal 11px Verdana, Sans-serif;
        }
        div.autocomplete_holder span a
        {
            position: absolute;
            right: 8px;
            top: 2px;
            Size: #666;
            font: bold 12px Verdana, Sans-serif;
            text-decoration: none;
        }
        div.autocomplete_holder span a:hover
        {
            Size: #ff0000 !important;
        }
        
        div.autocomplete_holder span.add
        {
            background-Size: #d7ffd7;
        }
        div.autocomplete_holder span.add a
        {
            font-weight: bold;
            Size: Green;
        }
        div.autocomplete_holder span.add a:hover
        {
            Size: #ff0000 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div style="float: left; width: 100%;">
        <div style="float: left; width: 215px;">
            <div class="col1">
                <img src="../lib/image/icon/ico_admin.png" alt="" />
            </div>
            <div class="col2">
                <h1>
                    <%= Resources.Resource.Administration %></h1>
            </div>
        </div>
        <div style="width: 100%; padding-left: 3px;">
            <div style="float: right;">
                <input type="button" id="btnAddNewSizeGroup" value="<%=Resources.Resource.btnAddNewSizeGroup%>"
                    onclick='AddSizeGroup(0);' />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <h3>
        <asp:Label ID="Label2" Text="<%$Resources:Resource,lblSizeGroup%>" runat="server" />
    </h3>
    <div id="grid_wrapper" style="width: 100%;">
        <trirand:JQGrid runat="server" ID="grdAlerts" DataSourceID="sdsAlerts" Height="300px"
            AutoWidth="true" OnCellBinding="grdAlerts_CellBinding" OnDataRequesting="grdAlerts_DataRequesting">
            <Columns>
                <trirand:JQGridColumn DataField="SizeGroupID" HeaderText="<%$ Resources:Resource, lblSizeGroupID %>"
                    Width="50" Sortable="false" />
                <trirand:JQGridColumn DataField="SizeGroup" HeaderText="<%$ Resources:Resource, lblGroupName %>" />
                <trirand:JQGridColumn DataField="SizeGroup" HeaderText="<%$ Resources:Resource, lblSize %>"
                    Sortable="false" />
                <trirand:JQGridColumn DataField="IsActive" HeaderText="<%$ Resources:Resource, lblGrdActive %>"
                    Sortable="false" TextAlign="Center" Width="50" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="SizeGroupID"
                    Sortable="false" TextAlign="Center" Width="50" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="SizeGroupID"
                    Sortable="false" TextAlign="Center" Width="50" />
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <ClientSideEvents LoadComplete="loadComplete" BeforeRowSelect="beforeRowSelect" />
        </trirand:JQGrid>
        <asp:SqlDataSource ID="sdsAlerts" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
    <asp:Panel runat="server" ID="SearchPanel">
        <div class="searchBar">
            <div class="header">
                <div class="title">
                    <asp:Literal ID="ltSearchForm" Text="<%$Resources:Resource,lblSearchForm %>" runat="server" />
                </div>
                <div class="icon">
                    <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
            </div>
            <h4>
                <asp:Label ID="lblSearchKeyword" CssClass="filter-key" AssociatedControlID="txtStyle"
                    runat="server" Text="<%$ Resources:Resource, lblSizeGroup %>"></asp:Label></h4>
            <div class="inner">
                <asp:TextBox runat="server" Width="180px" ID="txtStyle" CssClass="filter-key"></asp:TextBox>
            </div>
            <div class="footer">
                <div class="submit">
                    <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                </div>
                <br />
            </div>
        </div>
        <br />
    </asp:Panel>
    <div class="clear">
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $grid = $("#<%=grdAlerts.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=grdAlerts.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }
        function loadComplete(data) {
            jqGridResize();
        }

        //prevent jq grid from row selection
        function beforeRowSelect(rowid, e) {
            return false;
        }

        function saveFieldData(source, key) {
            //alert($(source).val());.
            $tr = $(source).parent().parent();
            $sub = $(".msg_subject", $tr);
            $body = $(".msg_body", $tr);

            var dataToPost = {};
            dataToPost.editRowKey = key;
            dataToPost.msgSubject = $sub.val();
            dataToPost.msgBody = $body.val();

            for (var k in dataToPost) {
                $grid.setPostDataItem(k, dataToPost[k]);
            }

            $grid.trigger("reloadGrid");

            for (var k in dataToPost) {
                $grid.removePostDataItem(k);
            }
        }
    </script>
    <script type="text/javascript">

        jQuery(window).bind("focus", function (event) {
            $("#btnAddNewSizeGroup").removeClass("ui-state-focus");
        });

        function GridPost() {
            var filterArr = {};
            $grid.appendPostData(filterArr);
            $grid.trigger("reloadGrid", [{ page: 1}]);
            return false;
        }

        $(document).ready(function () {
            GridPost();
        });

        function ReloadStyle() {
            GridPost();
        }

        function AddSizeGroup(gropID) {
            var url = 'AddSizeGroup.aspx';
            var queryData = {};
            queryData.SizeGroupID = gropID;
            var t = "<%=Resources.Resource.lblSizeGroup%>";
            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 450,
                height: 360,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');
            return false;
        }


        function AddSize(styleID) {
            var url = 'AddSizeGroupSize.aspx';
            var queryData = {};
            queryData.StyleID = styleID;
            var t = "<%=Resources.Resource.lblAddSize%>";
            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 720,
                height: 600,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');
            return false;
        }


        function RemoveUsedSize(source, materialID, masterID) {
            if (confirm("<%=Resources.Resource.msgDeleteSize%>")) {
                var datatoPost = {};
                datatoPost.isAjaxCall = 1;
                datatoPost.callBack = "deleteMaterial";
                datatoPost.MasterID = masterID;
                datatoPost.MaterialID = materialID;
                $.post("ProductSizeCatg.aspx", datatoPost, function (data) {
                    if (data == "ok") {
                        GridPost();
                    }
                });
            }
        }


        function DeleteGroup(groupID) {
            //if (confirm("<%=Resources.Resource.msgDeleteMaterial%>")) {
            if (confirm("<%=Resources.Resource.msgDeleteGroup%>")) {
                var datatoPost = {};
                datatoPost.isAjaxCall = 1;
                datatoPost.callBack = "deleteGroup";
                datatoPost.GroupID = groupID;

                $.post("ProductSizeCatg.aspx", datatoPost, function (data) {
                    if (data == "ok") {
                        //$grid.trigger("reloadGrid");
                        GridPost();
                    }
                });
            }
        }

    </script>
</asp:Content>



﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdApplyDiscountNew.aspx.cs" Inherits="Inventory_mdApplyDiscountNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div style="padding: 5px; height:455px; overflow-y:scroll;">
        <div id="tabs">
            <ul>
                <li><a href="#divDateRange"><%=Resources.Resource.lblByDateRange%></a> </li>
                <li><a href="#divDay"><%=Resources.Resource.lblByDaty%></a> </li>
                <li><a href="#divPackage"><%=Resources.Resource.lblByPackage %></a> </li>
            </ul>
            <div id="divDateRange">
                <asp:Panel ID="pnlDateRange" runat="server" DefaultButton="btnApply">
                    <table border="0" cellpadding="2" cellspacing="2">
                        <tr>
                            <td class="chzn-rtl">
                                <asp:Label ID="lblStartDate" Text="<%$Resources:Resource, lblStartDate %>" runat="server" />(mm/DD/yyyy)
                            </td>
                            <td>
                                <asp:TextBox ID="txtStartDate" CssClass="datepicker" runat="server" Width="80px" />
                                <asp:RequiredFieldValidator ID="rfvStartDate" ErrorMessage="*" ControlToValidate="txtStartDate"
                                    runat="server" ValidationGroup="grp1" />
                            </td>
                            <td class="chzn-rtl">
                                <asp:Label ID="Label1" Text="<%$Resources:Resource, lblEndDate %>" runat="server" />(mm/DD/yyyy)
                            </td>
                            <td>
                                <asp:TextBox ID="txtEndDate" CssClass="datepicker" runat="server" Width="80px" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ControlToValidate="txtEndDate"
                                    runat="server" ValidationGroup="grp1" />
                            </td>
                            <td class="chzn-rtl">
                                <asp:Label ID="Label3" Text="<%$Resources:Resource, lblMinDays %>" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtMinDays" runat="server" Width="50px" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="*" ControlToValidate="txtMinDays"
                                    runat="server" ValidationGroup="grp1" />
                            </td>                            
                        </tr>
                        <tr>
                            <td class="chzn-rtl">
                                <asp:Label ID="Label2" Text="<%$Resources:Resource, lblUnitDiscountPercent %>" runat="server" />                                
                            </td>
                            <td>
                                <asp:TextBox ID="txtDiscount" CssClass="val_dis_1" runat="server" Width="50px" />
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="*" ControlToValidate="txtDiscount"
                            runat="server" ValidationGroup="grp1" />--%>
                            </td>
                            <td class="chzn-rtl">
                                Or
                                <asp:Label ID="Label4" Text="<%$Resources:Resource, lblUnitAbsoluteDiscount %>" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtDiscountAbsolute" CssClass="val_dis_1" runat="server" Width="50px" />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="chzn-rtl">
                                Or
                                <asp:Label ID="Label10" Text="<%$Resources:Resource, lblUnitAmount %>" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtFinalAmount" CssClass="val_dis_1" runat="server" Width="50px" />                                
                            </td> 
                            <td class="chzn-rtl">                                
                                Or <asp:Label ID="Label18" Text="<%$Resources:Resource, lblFinalAmount %>" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtPackagePrice" CssClass="val_dis_1" runat="server" Width="50px" /> 
                                <script type="text/javascript">
                                    $(".val_dis_1").keyup(function () {
                                        if ($.trim($(this).val()) != '') {
                                            $this = $(this);
                                            $this.addClass("current");
                                            $(".val_dis_1").each(function () {
                                                if (!$(this).hasClass("current")) {
                                                    $(this).val("");
                                                }
                                            });
                                            $this.removeClass("current");
                                        }
                                    });
                                </script>                               
                            </td>
                            <td></td>
                            <td></td>
                        </tr>                       
                        <tr>
                            <td class="chzn-rtl">
                                <asp:Label ID="lblApplyOn" Text="<%$Resources:Resource, lblApplyOn%>" runat="server" />
                            </td>
                            <td colspan="5">
                                <asp:RadioButtonList ID="rblApplyOn" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="<%$Resources:Resource,lblAllYears %>" Value="1" Selected="True" />
                                    <asp:ListItem Text="<%$Resources:Resource,lblDuringSelectedDateRange%>" Value="2" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>                        
                    </table>
                    <div class="div_command">
                        <asp:Button ID="btnApply" CssClass="dicount_confirm" Text="<%$Resources:Resource, lblApply %>"
                            runat="server" OnClick="btnApply_Click" ValidationGroup="grp1" />
                        <asp:Button ID="btnReset" Text="<%$Resources:Resource, lblClear %>" runat="server" CausesValidation="False"
                            OnClick="btnReset_Click" OnClientClick="resetPanel(true); return false;" />                        
                    </div>
                </asp:Panel>
            </div>
            <div id="divDay">
                <asp:Panel ID="pnlByDay" runat="server" DefaultButton="btnApply1">
                    <table border="0" cellpadding="2" cellspacing="2">
                        <tr>
                            <td class="chzn-rtl">
                                <asp:Label ID="Label5" Text="<%$Resources:Resource, lblStartDate %>" runat="server" />(mm/DD/yyyy)
                            </td>
                            <td>
                                <asp:TextBox ID="txtStartDate1" CssClass="datepicker" runat="server" Width="80px" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="*" ControlToValidate="txtStartDate1"
                                    runat="server" ValidationGroup="grp2" />
                            </td>
                            <td class="chzn-rtl">
                                <asp:Label ID="Label6" Text="<%$Resources:Resource, lblEndDate %>" runat="server" />(mm/DD/yyyy)
                            </td>
                            <td>
                                <asp:TextBox ID="txtEndDate1" CssClass="datepicker" runat="server" Width="80px" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="*" ControlToValidate="txtEndDate1"
                                    runat="server" ValidationGroup="grp2" />
                            </td>
                            <td class="chzn-rtl">
                                <asp:Label ID="Label8" Text="<%$Resources:Resource, lblMinDays %>" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtMinDays1" runat="server" Width="50px" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="*" ControlToValidate="txtMinDays1"
                                    runat="server" ValidationGroup="grp2" />
                            </td>                           
                        </tr>
                        <tr>
                            <td class="chzn-rtl">
                                <asp:Label ID="Label7" Text="<%$Resources:Resource, lblUnitDiscountPercent %>" runat="server" />                                
                            </td>
                            <td>
                                <asp:TextBox ID="txtDiscount1" CssClass="val_dis_2" runat="server" Width="50px" />
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="*" ControlToValidate="txtDiscount1"
                            runat="server" ValidationGroup="grp2" />--%>
                            </td>
                            <td class="chzn-rtl">
                                Or
                                <asp:Label ID="Label9" Text="<%$Resources:Resource, lblUnitAbsoluteDiscount %>" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtDiscountAbsolute1" CssClass="val_dis_2" runat="server" Width="50px" />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="chzn-rtl">
                                Or
                                <asp:Label ID="Label11" Text="<%$Resources:Resource, lblUnitAmount %>" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtFianlAmount1" CssClass="val_dis_2" runat="server" Width="50px" />
                                
                            </td>
                            <td class="chzn-rtl">                                
                                Or <asp:Label ID="Label17" Text="<%$Resources:Resource, lblFinalAmount %>" runat="server" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtPackagePrice1" CssClass="val_dis_2" runat="server" Width="50px" />   
                                <script type="text/javascript">
                                    $(".val_dis_2").keyup(function () {
                                        if ($.trim($(this).val()) != '') {
                                            $this = $(this);
                                            $this.addClass("current");
                                            $(".val_dis_2").each(function () {
                                                if (!$(this).hasClass("current")) {
                                                    $(this).val("");
                                                }
                                            });
                                            $this.removeClass("current");
                                        }
                                    });
                                </script>                             
                            </td>
                            <td></td>
                            <td></td>
                        </tr>                       
                        <tr>
                            <td class="chzn-rtl">
                                <asp:Label ID="Label12" Text="<%$Resources:Resource,lblApplyOn%>" runat="server" />
                            </td>
                            <td colspan="5">
                                <asp:RadioButtonList ID="rblApplyOn1" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="All Years" Value="1" Selected="True" />
                                    <asp:ListItem Text="During Selected Date Range Only" Value="2" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="chzn-rtl">
                                <asp:Label ID="Label13" Text="<%$Resources:Resource,lblDay%>" runat="server" />                                
                            </td>
                            <td colspan="5">
                                <asp:RadioButtonList ID="rblDays" runat="server" RepeatDirection="Horizontal">
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="Please select a day!"
                                    ControlToValidate="rblDays" Display="Dynamic" runat="server" ValidationGroup="grp2" />
                            </td>
                        </tr>                        
                    </table>
                    <div class="div_command">
                        <asp:Button ID="btnApply1" CssClass="dicount_confirm" Text="<%$Resources:Resource, lblApply %>"
                            runat="server" OnClick="btnApply1_Click" ValidationGroup="grp2" />
                        <asp:Button ID="btnReset1" Text="<%$Resources:Resource, lblClear %>" runat="server"
                            CausesValidation="False" OnClick="btnReset1_Click" OnClientClick="resetPanel(true); return false;" /></div>                   
                </asp:Panel>
            </div>
            <div id="divPackage">
                <asp:Panel ID="pnlPackage" runat="server" Visible="true">
                    <table border="0" cellpadding="2" cellspacing="2">
                        <tr>
                            <td class="chzn-rtl">
                                <asp:Label ID="Label14" Text="<%$Resources:Resource, lblStartDate %>" runat="server" />(mm/DD/yyyy)
                            </td>
                            <td>
                                <asp:TextBox ID="txtStartDate3" CssClass="datepicker" runat="server" Width="80px" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="*" ControlToValidate="txtStartDate3"
                                    runat="server" ValidationGroup="grp3" />
                            </td>
                            <td class="chzn-rtl">
                                <asp:Label ID="Label15" Text="<%$Resources:Resource, lblEndDate %>" runat="server" />(mm/DD/yyyy)
                            </td>
                            <td>
                                <asp:TextBox ID="txtEndDate3" CssClass="datepicker" runat="server" Width="80px" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="*" ControlToValidate="txtEndDate3"
                                    runat="server" ValidationGroup="grp3" />
                            </td>
                        </tr>
                        <%--<tr>
                            <td class="chzn-rtl">
                                <asp:Label ID="Label16" Text="<%$Resources:Resource,lblApplyOn%>" runat="server" />
                            </td>
                            <td colspan="3">
                                <asp:RadioButtonList ID="rblApplyOn3" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="All Years" Value="1" Selected="True" />
                                    <asp:ListItem Text="During Selected Date Range Only" Value="2" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>--%>
                        <tr>                            
                            <td class="chzn-rtl">                                
                                <asp:Label ID="Label19" Text="<%$Resources:Resource, lblPackageAmount %>" runat="server" />
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtPackagePrice3" runat="server" Width="50px" />                                
                            </td>
                        </tr>                        
                    </table>
                    <div class="div_command">
                        <asp:Button ID="btnApply3" CssClass="dicount_confirm" Text="<%$Resources:Resource, lblApply %>"
                            runat="server" ValidationGroup="grp3" onclick="btnApply3_Click" />
                        <asp:Button ID="btnReset3" Text="<%$Resources:Resource, lblClear %>" runat="server"
                            CausesValidation="False" OnClick="btnReset1_Click" OnClientClick="resetPanel(true); return false;" /></div>   
                </asp:Panel>
            </div>
        </div>        
        <asp:HiddenField ID="hdnID" runat="server" />
        <br style="line-height:2px;" />
        <div id="grid_wrapper">
            <trirand:JQGrid runat="server" ID="grdDiscounts" Height="180px"
                Width="100%" AutoWidth="true" PagerSettings-PageSize="50" OnDataRequesting="grdDiscounts_DataRequesting"
                OnCellBinding="grdDiscounts_CellBinding" 
                ondatarequested="grdDiscounts_DataRequested">
                <Columns>
                    <trirand:JQGridColumn DataField="RowIndex" PrimaryKey="true" Visible="false" />
                    <trirand:JQGridColumn DataField="RoomID" HeaderText="RoomID" Visible="false" />
                    <trirand:JQGridColumn DataField="DiscountCategory" HeaderText="<%$Resources:Resource, lblDiscountType%>" Editable="false"
                        Sortable="false" TextAlign="Center" />
                    <trirand:JQGridColumn DataField="StartDate" HeaderText="<%$Resources:Resource, lblStartDate%>" DataFormatString="{0:MMM dd yyyy}"
                        Editable="false" Sortable="false" TextAlign="Center" />
                    <trirand:JQGridColumn DataField="EndDate" HeaderText="<%$Resources:Resource, lblEndDate%>" DataFormatString="{0:MMM dd yyyy}"
                        Editable="false" Sortable="false" TextAlign="Center" />
                    <trirand:JQGridColumn DataField="Day" HeaderText="<%$Resources:Resource, lblDay%>" Editable="false" Sortable="false"
                        NullDisplayText="--" ConvertEmptyStringToNull="true" TextAlign="Center" />
                    <trirand:JQGridColumn DataField="PercentageDiscount" HeaderText="<%$Resources:Resource, lblDiscount2%>" DataFormatString="{0:F}%"
                        Editable="false" Sortable="false" TextAlign="Center" />
                    <trirand:JQGridColumn DataField="MinDays" HeaderText="<%$Resources:Resource, lblMinDays%>" Editable="false" Sortable="false"
                        TextAlign="Center" />
                    <trirand:JQGridColumn DataField="ApplyType" HeaderText="<%$Resources:Resource, lblApplyOn%>" Editable="false" Sortable="false"
                        TextAlign="Center" />
                    <trirand:JQGridColumn DataField="RowIndex" HeaderText="<%$Resources:Resource, lblEdit%>" Editable="false"
                        Sortable="false" TextAlign="Center" Visible="false" />  
                    <trirand:JQGridColumn DataField="RoomID" HeaderText="<%$Resources:Resource, Delete%>" Editable="false" Sortable="false"
                        TextAlign="Center" />                  
                </Columns>
                <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                    ShowDeleteButton="false" ShowSearchButton="false" />
                <SortSettings InitialSortColumn="" />
                <AppearanceSettings AlternateRowBackground="True" />
                <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="gridLoadComplete"
                    RowSelect="rowSelect" />
            </trirand:JQGrid>
            <asp:SqlDataSource ID="sdsDiscount" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        //Variables
        var $tabUi;
        var gridID = "<%=grdDiscounts.ClientID %>";
        $grid = $("#<%=grdDiscounts.ClientID %>");
        //Function To Resize the grid
        function resize_the_grid() {
            $grid.fluidGrid({ example: '#grid_wrapper', offset: -2 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $grid.onJqGridPaging();
        }


        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $grid.onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);


        function reloadGrid(event, ui) {
            $grid.trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function gridLoadComplete(data) {
            $(window).trigger("resize");
        }

        //$(".datapicker1").attr("autocomplete", "off");
        //$(".datapicker1").datepicker({ defaultDate: "<%=DateTime.Now.Month%>/<%=DateTime.Now.Day%>/<%=DateTime.Now.Year%>", changeMonth: true, hideIfNoPrevNext: true, maxDate: "12/31/<%=DateTime.Now.Year%>", minDate: "01/01/<%=DateTime.Now.Year%>" });

         $(function () {
            $tabUi = $("#tabs").tabs({
                select: function (event, ui) {
                    return $("#<%=hdnID.ClientID%>").val() == "";
                    /*var isValid = false;
                    switch (ui.index) {
                        case 0:
                            return true;
                        case 1:
                            return true;
                        case 2:
                            return true;
                        case 3:
                            return true;
                    }
                    return isValid;*/
                }
            });
        });

        function selectTab(tabIndex) {
            $tabUi.tabs("select", tabIndex);            
        }

        function togglePanel(val) {
            selectTab(val-1);
            /*if (val == 2) {
                $pnl1 = $("#<%=pnlDateRange.ClientID%>");
                $("input:text", $pnl1).each(function () {
                    $(this).attr("disabled", true).val("");
                });
                $("input:submit", $pnl1).each(function () {
                    $(this).button({ disabled: true });
                });

                $pnl2 = $("#<%=pnlByDay.ClientID%>");
                $("input:text", $pnl2).each(function () {
                    $(this).attr("disabled", false);
                });
                $("input:submit", $pnl2).each(function () {
                    $(this).button({ disabled: false });
                });
                $("input:radio", $("#<%=rblDays.ClientID%>")).each(function () {
                    $this = $(this);
                    $this.attr("disabled", false);
                });
            }
            else {
                $pnl1 = $("#<%=pnlDateRange.ClientID%>");
                $("input:text", $pnl1).each(function () {
                    $(this).attr("disabled", false);
                });
                $("input:submit", $pnl1).each(function () {
                    $(this).button({ disabled: false });
                });

                $pnl2 = $("#<%=pnlByDay.ClientID%>");
                $("input:text", $pnl2).each(function () {
                    $(this).attr("disabled", true).val("");
                });
                $("input:submit", $pnl2).each(function () {
                    $(this).button({ disabled: true });
                });
                $("input:radio", $("#<%=rblDays.ClientID%>")).each(function () {
                    $this = $(this);
                    $this.attr("disabled", true).attr('checked', false);
                });
            }
            $(".seq_1").val(1);*/
        }

        function resetPanel(isClearGridSelection) {
            if (isClearGridSelection) {
                $grid.jqGrid('resetSelection');
            }
            //$grid.jqGrid('resetSelection');
            $("#<%=btnReset.ClientID%>").hide();
            $("#<%=btnReset1.ClientID%>").hide();

            $pnl1 = $("#<%=pnlDateRange.ClientID%>");
            $("input:text", $pnl1).each(function () {
                $(this).attr("disabled", false).val("");
            });            

            $pnl2 = $("#<%=pnlByDay.ClientID%>");
            $("input:text", $pnl2).each(function () {
                $(this).attr("disabled", false).val("");
            });            
            $("input:radio", $("#<%=rblDays.ClientID%>")).each(function () {
                $this = $(this);
                $this.attr("disabled", false).attr('checked', false);
            });

            $pnl3 = $("#<%=pnlPackage.ClientID%>");
            $("input:text", $pnl3).each(function () {
                $(this).attr("disabled", false).val("");
            });            
            $("#<%=hdnID.ClientID%>").val("");
            //$(".seq_1").val(1);
        }

        function rowSelect(id) {  
            resetPanel(false);          
            setSelectedItemData(id);
        }

        function setSelectedItemData(id) {
            var postData = {};
            postData.itemID = id;
            $.ajax({
                type: "POST",
                url: "mdApplyDiscountNew.aspx/GetProductDiscountItem",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(postData),
                success: function (data) {
                    //alert(parseInt(data.d.DiscountType));
                    selectTab(data.d.DiscountCategory -1); 
                    $("#<%=hdnID.ClientID%>").val(data.d.RowIndex);
                    if (data.d.DiscountCategory == 1) {
                        $("#<%=txtStartDate.ClientID%>").datepicker("setDate", new Date(data.d.StartDate.match(/\d+/)[0] * 1));
                        $("#<%=txtEndDate.ClientID%>").datepicker("setDate", new Date(data.d.EndDate.match(/\d+/)[0] * 1));
                        if (data.d.PackagePrice > 0) {
                            $("#<%=txtPackagePrice.ClientID%>").val(data.d.PackagePrice);
                        }
                        else {
                            $("#<%=txtDiscount.ClientID%>").val(data.d.PercentageDiscount);
                        }
                        $("#<%=txtMinDays.ClientID%>").val(data.d.MinDays);
                        //$("#txtSeq.ClientID").val(data.d.Seq);

                        $("input:radio", $("#<%=rblApplyOn.ClientID%>")).each(function () {
                            $this = $(this);
                            if ($this.val() == data.d.ApplyType) {
                                $this.attr('checked', true);
                            }
                        });

                        //togglePanel(data.d.DiscountCategory);
                        $("#<%=btnReset.ClientID%>").show();
                        $("#<%=btnReset1.ClientID%>").hide();
                        $("#<%=btnReset3.ClientID%>").hide();
                    }
                    else if(data.d.DiscountCategory == 2) {
                        $("#<%=txtStartDate1.ClientID%>").datepicker("setDate", new Date(data.d.StartDate.match(/\d+/)[0] * 1));
                        $("#<%=txtEndDate1.ClientID%>").datepicker("setDate", new Date(data.d.EndDate.match(/\d+/)[0] * 1));                       
                        if (data.d.PackagePrice > 0) {
                            $("#<%=txtPackagePrice1.ClientID%>").val(data.d.PackagePrice);
                        }
                        else {
                            $("#<%=txtDiscount1.ClientID%>").val(data.d.PercentageDiscount);
                        }
                        $("#<%=txtMinDays1.ClientID%>").val(data.d.MinDays);
                        //$("#txtSeq1.ClientID").val(data.d.Seq);

                        $("input:radio", $("#<%=rblApplyOn1.ClientID%>")).each(function () {
                            $this = $(this);
                            if ($this.val() == data.d.ApplyType) {
                                $this.attr('checked', true);
                            }
                        });

                        $("input:radio", $("#<%=rblDays.ClientID%>")).each(function () {
                            $this = $(this);
                            if ($this.val() == data.d.Day) {
                                $this.attr('checked', true);
                            }
                        });

                        //togglePanel(data.d.DiscountCategory);
                        $("#<%=btnReset.ClientID%>").hide();
                        $("#<%=btnReset1.ClientID%>").show();
                        $("#<%=btnReset3.ClientID%>").hide();
                    }
                    else if(data.d.DiscountCategory == 3) {
                        $("#<%=txtStartDate3.ClientID%>").datepicker("setDate", new Date(data.d.StartDate.match(/\d+/)[0] * 1));
                        $("#<%=txtEndDate3.ClientID%>").datepicker("setDate", new Date(data.d.EndDate.match(/\d+/)[0] * 1));
                        $("#<%=txtPackagePrice3.ClientID%>").val(data.d.PackagePrice);                                                

                        $("#<%=btnReset.ClientID%>").hide();
                        $("#<%=btnReset1.ClientID%>").hide();
                        $("#<%=btnReset3.ClientID%>").show();
                    }
                },
                error: function (request, status, errorThrown) {
                    alert(status);
                }
            });

            return false;
        } 
        
        function deleteItem(id) {
            if (confirm("<%=Resources.Resource.msgDoYouWantToDelete%>")) {
                var postData = {};
                postData.itemID = id;
                postData.roomIds = <%=GetJsonArrayRoomID()%>;
                postData.productID = <%=ProductID%>;
                $.ajax({
                    type: "POST",
                    url: "mdApplyDiscountNew.aspx/DeleteDiscountItem",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(postData),
                    success: function (data) {
                        reloadGrid();
                    },
                    error: function (request, status, errorThrown) {
                        alert(status);
                    }
                });
            }
            return false;
        }   
    </script>
</asp:Content>


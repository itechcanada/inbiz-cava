﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Inventory_AddNewTexture : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MessageState.SetGlobalMessage(MessageType.Success, "");
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                if (BusinessUtility.GetString(Request.QueryString["Status"]) == "INDIVIDUAL")
                {
                    trIsAxctive.Visible = true;
                    if (BusinessUtility.GetInt(Request.QueryString["TextureID"]) > 0)
                    {
                        ProductTexture objProductTexture = new ProductTexture();
                        objProductTexture.TextureID = BusinessUtility.GetInt(Request.QueryString["TextureID"]);
                        var lst = objProductTexture.GetAllTextureList(null, Globals.CurrentAppLanguageCode);
                        foreach (var item in lst)
                        {
                            txtTextureNameEn.Text = item.TextureEn;
                            txtTextureNameFr.Text = item.TextureFr;
                            txtShortName.Text = item.ShortName;
                            rblstActive.SelectedValue = BusinessUtility.GetBool(item.IsActive) ? "1" : "0";
                        }

                    }
                }
                txtTextureNameFr.Focus();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            ProductTexture objProductTexture = new ProductTexture();
            objProductTexture.TextureEn = txtTextureNameEn.Text;
            objProductTexture.TextureFr = txtTextureNameFr.Text;
            objProductTexture.IsActive = BusinessUtility.GetInt(rblstActive.SelectedItem.Value);
            objProductTexture.TextureID = BusinessUtility.GetInt(Request.QueryString["TextureID"]);
            objProductTexture.ShortName = txtShortName.Text;
            if (objProductTexture.Save(null, CurrentUser.UserID) == true)
            {

                if (BusinessUtility.GetString(Request.QueryString["Status"]) == "INDIVIDUAL")
                {
                    if (BusinessUtility.GetInt(Request.QueryString["TextureID"]) > 0)
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgTextureUpdate);
                        Globals.RegisterReloadParentScript(this);
                        Globals.RegisterCloseDialogScript(this);
                        return;
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgTextureCreate);
                        Globals.RegisterReloadParentScript(this);
                        Globals.RegisterCloseDialogScript(this);
                        return;
                    }
                }

                //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated );
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.TextureCreated({0});", objProductTexture.TextureID), true);
            }
            else
            {
                //MessageState.SetGlobalMessage(MessageType.Failure, "Collection Creation failure");
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }

        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
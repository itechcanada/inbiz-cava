﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Inventory_ProductKit : System.Web.UI.Page
{
    ProductKit _kit = new ProductKit();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            DropDownHelper.FillProductKitDropdown(dlProduct, this.ProductID, Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.msgSelectProduct, "0"));
        }
    }

    private void SetData()
    {
        _kit.PrdID = this.ProductID;
        _kit.PrdIncludeID = BusinessUtility.GetInt(dlProduct.SelectedValue);
        _kit.PrdIncludeQty = BusinessUtility.GetDouble(txtProdQty.Text);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid) {
            SetData();

            OperationResult res = _kit.Insert();
            if (res == OperationResult.Success)
            {
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgPrdAdded);
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                }
                else
                {
                    Globals.RegisterCloseDialogScript(Page);
                }
            }
            else if (res == OperationResult.AlreadyExists)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPrdExist);
            }
        }
    }

    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AddNewSize.aspx.cs" Inherits="Inventory_AddNewSize" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="dialogAssignUser" style="padding: 5px;">
        <h3>
            <asp:Label ID="Label1" Text="<%$Resources:Resource, btnAddNewSize%>" runat="server" />
        </h3>
        <table>
            <tr>
                <td>
                    <%= Resources.Resource.lblSizeNameFrench%>
                </td>
                <td>
                    <asp:TextBox ID="txtSizeNameFr" runat="server" MaxLength="250" />
                </td>
            </tr>
            <tr>
                <td>
                    <%= Resources.Resource.lblSizeNameEnglish%>
                </td>
                <td>
                    <asp:TextBox ID="txtSizeNameEn" runat="server" MaxLength="250" />
                </td>
            </tr>
            <tr>
                <td>
                    <%= Resources.Resource.lblSeq%>
                </td>
                <td>
                    <asp:TextBox ID="txtSeq" runat="server" CssClass="numericTextField" MaxLength="4" />
                </td>
            </tr>
            <tr id="trIsAxctive" runat="server" visible="false">
                <td>
                    <asp:Label ID="lblIsProdActive" Text="<%$ Resources:Resource, lblIsActive %>" runat="server" />
                </td>
                <td>
                    <asp:RadioButtonList ID="rblstActive" runat="server" RepeatDirection="Horizontal"
                        CssClass="rbl_list">
                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Selected="true" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </div>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
            OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
</asp:Content>

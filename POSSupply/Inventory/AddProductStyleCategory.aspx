﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AddProductStyleCategory.aspx.cs" Inherits="Inventory_AddProductStyleCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="dialogAssignUser" style="padding: 5px;">
        <h3>
            <asp:Label ID="Label1" Text="<%$Resources:Resource, lblSelectCategory%>" runat="server" />
        </h3>
<%--        <div style="float: right; padding-bottom: 10px;">
            <input type="button" id="btnAddNewMaterial" value="<%=Resources.Resource.btnAddNewMaterial%>"
                onclick='AddNewMaterial();' />
        </div>--%>
        <asp:ListBox CssClass="modernized_select" ID="ddlUsers" runat="server" Width="600px" data-placeholder="<%$ Resources:Resource, lblWebCategory %>"
            SelectionMode="Multiple"></asp:ListBox>
    </div>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
            OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
<script type="text/javascript" >
    $(document).ready(function () {
        $("#contentBottom").css("height", "auto");
    });
</script>
</asp:Content>



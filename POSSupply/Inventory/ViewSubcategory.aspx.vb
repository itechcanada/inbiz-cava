Imports Resources.Resource
Partial Class ViewSubcategory
    Inherits BasePage
    Public objSubcategory As New clsSubcategory
    Private strsubcatID As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        lblMsg.Text = ""
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If
        If Not Page.IsPostBack Then
            subFillGrid()
        End If
        If Request.QueryString("Approve") <> "" Then
            lblHeading.Text = PendingForApproval
        Else
            lblHeading.Text = ViewSubCategories
        End If

        clsGrid.MaintainParams(Page, grdvAppSubCategory, SearchPanel, lblMsg, imgSearch)
    End Sub
    Public Sub subFillGrid()
        sqlsdAppAdmin.SelectCommand = objSubcategory.funFillGrid(Request.QueryString("Approve"))  'fill Sub-category Record
    End Sub
    Protected Sub grdvAppCategory_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdvAppSubCategory.PageIndexChanging
        subFillGrid()
    End Sub

    Protected Sub grdvAppSubCategory_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdvAppSubCategory.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
            "confirm('" & msgAreYouSureYouWantToDeleteTheSubCategory & " ')")
        End If
    End Sub
    Protected Sub grdvAppsubcategory_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdvAppSubCategory.RowDeleting
        strsubcatID = grdvAppSubCategory.DataKeys(e.RowIndex).Value.ToString()
        lblMsg.Text = msgSubCategoryHasBeenDeletedSuccessfully
        sqlsdAppAdmin.DeleteCommand = objSubcategory.funDeleteSubcategory(strsubcatID) 'Delete Category
        objSubcategory.SubcategoryID = strsubcatID
        objSubcategory.subGetSubCategoryInfo()
        Dim ObjDataClass As New clsDataClass()
        AdjustWebSeq(objSubcategory.SubcategoryWebSequence, ObjDataClass)
        subFillGrid()
    End Sub
    Public Sub AdjustWebSeq(ByVal oldWebSeq As String, ByVal objclsDataClass As clsDataClass)
        Dim qry As String
        qry = "UPDATE subcategory SET subcatWebSeq=subcatWebSeq-1 WHERE subcatWebSeq >" & oldWebSeq & " "
        objclsDataClass.SetData(qry)
    End Sub
    Protected Sub grdvAppsubCategory_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdvAppSubCategory.RowEditing
        Dim subcatID As String = grdvAppSubCategory.DataKeys(e.NewEditIndex).Value.ToString()
        If Request.QueryString("Approve") <> "" Then
            Response.Redirect("AddSubCategory.aspx?subcatID=" & subcatID & "&Approve=" & Request.QueryString("Approve"))
        Else
            Response.Redirect("AddSubCategory.aspx?subcatID=" & subcatID)
        End If
    End Sub

    Protected Sub grdvAppsubcategory_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdvAppSubCategory.Sorting
        subFillGrid()
    End Sub

    Protected Sub sqlsdAppAdmin_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqlsdAppAdmin.Selected
        If e.AffectedRows = 0 Then
            lblMsg.Text = NoDataFound
        End If
    End Sub
End Class

Imports System
Imports System.IO
Imports System.Data.Odbc
Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Configuration.ConfigurationManager

Partial Class INV_Print
    Inherits BasePage
    Private sHtmlData As String = ""
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            subClose()
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("prdID") <> "" Then
                subHTMLGen(Request.QueryString("PrdID"))
            End If
        End If
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    ' Sub routine to close popup
    Private Sub subClose()
        Response.Write("<script>window.opener.location.reload();</script>")
        Response.Write("<script>window.close();</script>")
    End Sub

    'Printing Content
    Private Sub subHTMLGen(ByVal strPrdID As String)

        Dim objPrd As New clsProducts
        Dim objPrdKit As New clsProductKits
        Dim objAssVendor As New clsPrdAssociateVendor
        Dim objPrn As New clsPrint
        Dim objCom As New clsCommon
        If strPrdID <> "" Then

            objPrd.ProductID = strPrdID
            objPrd.subGetProductsInfo()
            Dim vendors As String = objAssVendor.GetVendorNames(strPrdID)

            '//////////////////////////////////////////////////////
            sHtmlData += "<h1>" & prnAsmblyList & "</h1><hr><br/><br/>"
            sHtmlData += "<table border=0 cellpadding=5 cellspacing=0 width=100%>"
            sHtmlData += "  <tr><td align=left>" & prnPrdIntId & "&nbsp;<b>" & objPrd.PrdIntID & "</b></td>"
            sHtmlData += "<td align=left>" & prnPrdUPCCode & " &nbsp;<b>" & objPrd.PrdUPCCode & "</b></td>"
            sHtmlData += "<td align=left>" & prnVndorID & ":  &nbsp;<b>" & vendors & " </b></td></tr>"
            'sHtmlData += "<tr height=10px><td colspan=3></td></tr>"
            sHtmlData += "<tr><td colspan=3 align=left>" & prnProductName & "<b> &nbsp;" & objPrd.PrdName & "</b></td></tr>"
            'sHtmlData += "<tr height=10px><td colspan=3></td></tr>"
            sHtmlData += "<tr><td colspan=3 align=left><h5>" & prnProductKitList & "</h5></td></tr>"
            sHtmlData += "<tr>"
            sHtmlData += "<td colspan=3>"
            sHtmlData += "<table width=100% border=1 cellspacing=0 cellpadding=5>"
            sHtmlData += "<thead>"
            sHtmlData += "<th width=16% scope=col align=left>" & prnInternalId & "</th>"
            sHtmlData += "<th width=16% scope=col align=left>" & prnVndorID & "</th>"
            sHtmlData += "<th width=16% scope=col align=left>" & prnBarCode & "</th>"
            sHtmlData += "<th width=37% scope=col align=left>" & prnName & "</th>"
            sHtmlData += "<th width=15% scope=col align=right>" & prnNoOfUnits & "</th>"
            sHtmlData += "</thead>"

            Dim drItems As OdbcDataReader
            drItems = objPrdKit.GetProductKitsInfoByPrdId(strPrdID)

            While (drItems.Read)
                objPrd.ProductID = drItems("prdIncludeID").ToString
                objPrd.subGetProductsInfo()
                sHtmlData += "<tr>"
                sHtmlData += "<td align=left>" & objPrd.PrdIntID & "</td>"
                sHtmlData += "<td align=left>" & objAssVendor.GetVendorNames(objPrd.ProductID) & "</td>"
                sHtmlData += "<td align=left>" & objPrd.PrdUPCCode & "</td>"
                sHtmlData += "<td align=left>" & objPrd.PrdName & "</td>"
                sHtmlData += "<td align=right>" & drItems("prdIncludeQty").ToString & "</td>"
                sHtmlData += "</tr>"
            End While
            sHtmlData += "</table>"
            sHtmlData += "</td>"
            sHtmlData += "</tr>"
            sHtmlData += "</table>"
            lblData.ForeColor = Drawing.Color.Black
            lblData.Text = sHtmlData
        End If
    End Sub
End Class

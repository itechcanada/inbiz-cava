<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="ViewSubcategory.aspx.vb" Inherits="ViewSubcategory" %>

<%@ Import Namespace="Resources.Resource" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register src="../Controls/CommonSearch/CommonKeyword.ascx" tagname="CommonKeyword" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>

    <uc1:CommonKeyword ID="CommonKeyword1" runat="server" />

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblHeading"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <asp:UpdatePanel runat="server" ID="udpnlViewPrd">
            <ContentTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">                    
                    <tr>
                        <td align="center">
                            <asp:Panel runat="server" ID="SearchPanel" DefaultButton="imgSearch">
                                <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label></asp:Panel>
                            <asp:ImageButton ID="imgSearch" Visible="false" runat="server" AlternateText="Search"
                                ImageUrl="../images/search-btn.gif" />
                        </td>
                    </tr>
                    <tr>
                        <td height="10">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="grdvAppSubCategory" runat="server" AllowSorting="True" DataSourceID="sqlsdAppAdmin"
                                AllowPaging="True" PageSize="10" PagerSettings-Mode="Numeric" CellPadding="0"
                                GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
                                CssClass="view_grid650" UseAccessibleHeader="False" 
                                DataKeyNames="subcatId" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="subcatWebSeq" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, hdrWebSequence%>"
                                        ReadOnly="True" SortExpression="subcatWebSeq">
                                        <ItemStyle Width="30px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="catname" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, Category%>"
                                        ReadOnly="True" SortExpression="catname">
                                        <ItemStyle Width="140px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="subcatName" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, SubCategory%>"
                                        ReadOnly="True" SortExpression="subcatName">
                                        <ItemStyle Width="140px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="eBayCatgNo" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, eBayCatgNo%>"
                                        ReadOnly="True" SortExpression="eBayCatgNo">
                                        <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UserName" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, UpdatedBy%>"
                                        ReadOnly="True" SortExpression="UserName">
                                        <ItemStyle Width="140px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="subcatUpdatedOn" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, UpdatedOn%>"
                                        ReadOnly="True" SortExpression="subcatUpdatedOn">
                                        <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, hdrImage%>" HeaderStyle-ForeColor="white"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <a href='<% = ConfigurationManager.AppSettings("RelativePathForSubCategory") %><%# Container.DataItem("subcatImagePath")%>'
                                                target="_blank">
                                                <img src='../Thumbnail.ashx?p=<% = ConfigurationManager.AppSettings("RelativePathForSubCategory") %><%# Container.DataItem("subcatImagePath")%>&Cat=Subcat' /></a>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" Width="40px" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:ImageField SortExpression="subcatIsActive" HeaderStyle-ForeColor="white" HeaderText="<%$ Resources:Resource, hdrStatus%>"
                                        DataImageUrlField="subcatIsActive" DataImageUrlFormatString="~/Images/{0}">
                                        <ItemStyle HorizontalAlign="center" Width="40px" VerticalAlign="Middle" />
                                    </asp:ImageField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdEdit%>" HeaderStyle-ForeColor="white"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("subcatId") %>'
                                                CommandName="Edit" ImageUrl="~/images/edit_icon.png" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="40px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete%>" HeaderStyle-ForeColor="white"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                                CommandArgument='<%# Eval("subcatId") %>' CommandName="Delete" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="40px" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="grid_footer" />
                                <RowStyle CssClass="grid_rowstyle" />
                                <PagerStyle CssClass="grid_footer" />
                                <HeaderStyle CssClass="grid_header" Height="26px" />
                                <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                <PagerSettings PageButtonCount="20" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="sqlsdAppAdmin" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <br />
    <br />
</asp:Content>

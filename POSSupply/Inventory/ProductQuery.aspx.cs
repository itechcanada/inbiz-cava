﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using Trirand.Web.UI.WebControls;
using System.Data;

public partial class Inventory_ProductQuery : BasePage
{
    Product _prd = new Product();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(gvProducts))
        {
            //Security Check
            if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER))
            {
                Response.Redirect("~/Errors/AccessDenied.aspx");
            }

            if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
            {
                btnAdd.Visible = false;
            }

            ListItem lstBlank = new ListItem("", "");
            DropDownHelper.FillDropdown(dlSearch, "PD", "dlSh1", Globals.CurrentAppLanguageCode, null);
            dlSearch.Items.Insert(0, lstBlank);
            dlSearch.SelectedIndex = 1;
            //txtSearch.Focus();
            ProductCategory objProductSize = new ProductCategory();
            var vProdSrchCatg = objProductSize.GetSearchCategoryList(null, Globals.CurrentAppLanguageCode,Resources.Resource.All);
            lstProdCatg.DataSource = vProdSrchCatg;
            lstProdCatg.DataTextField = "CatgName";
            lstProdCatg.DataValueField = "CatgValue";
            lstProdCatg.DataBind();

            ProductColor objProductMaterial = new ProductColor();
            var vColorList = objProductMaterial.GetColorList(null, Globals.CurrentAppLanguageCode);
            lstColor.DataSource = vColorList;
            lstColor.DataTextField = "ColorName";
            lstColor.DataValueField = "ColorID";
            lstColor.DataBind();


            //Set Sections visivilayt by Product Type
            switch (this.ProductType)
            {
                case StatusProductType.Product:
                    break;
                case StatusProductType.Accommodation:
                    ltTitle.Text = Resources.Resource.lblViewAccommodation;
                    btnAdd.Text = Resources.Resource.lblAddAccommodation;
                    break;
                case StatusProductType.ServiceProduct:
                    ltTitle.Text = Resources.Resource.lblViewServiceProducts;
                    btnAdd.Text = Resources.Resource.lblAddServiceProduct;
                    break;
                case StatusProductType.AdminFee:
                    ltTitle.Text = Resources.Resource.lblManageAdminFee;
                    btnAdd.Text = Resources.Resource.lblManageAdminFee;
                    int adminFeeProductID = _prd.GetAdminFeeProductID();
                    if (adminFeeProductID > 0)
                    {
                        Response.Redirect(string.Format("Product.aspx?PrdID={0}&ptype={1}", adminFeeProductID, (int)StatusProductType.AdminFee));
                    }
                    break;
                case StatusProductType.ChildUnder12:
                    break;
                case StatusProductType.CourseProduct:
                    ltTitle.Text = Resources.Resource.lblViewCourses;
                    btnAdd.Text = Resources.Resource.lblAddNewCourse;
                    break;
            }

            // Get User Grid Height
            InbizUser _usr = new InbizUser();
            _usr.PopulateObject(CurrentUser.UserID);
            if (BusinessUtility.GetInt(_usr.UserGridHeight) > 0)
            {
                gvProducts.Height = _usr.UserGridHeight;
            }
        }

        ////Hide coulmns if they are not relavent to Accomodation
        //if (this.ProductType == StatusProductType.Accommodation)
        //{
        //    gvProducts.Columns[2].Visible = false;
        //    gvProducts.Columns[3].Visible = false;
        //    gvProducts.Columns[4].Visible = false;
        //    gvProducts.Columns[5].Visible = false;
        //    gvProducts.Columns[6].Visible = false;

        //    gvProducts.Columns[8].Visible = true;
        //    gvProducts.Columns[9].Visible = true;
        //    gvProducts.Columns[10].Visible = true;
        //    gvProducts.Columns[11].Visible = true;
        //    gvProducts.Columns[12].Visible = true;
        //    gvProducts.Columns[14].Visible = true;
        //}
        //else if (this.ProductType == StatusProductType.ServiceProduct || this.ProductType == StatusProductType.AdminFee)
        //{
        //    gvProducts.Columns[2].Visible = false;
        //    gvProducts.Columns[3].Visible = false;
        //    gvProducts.Columns[4].Visible = false;
        //    gvProducts.Columns[5].Visible = false;
        //    gvProducts.Columns[6].Visible = false;
        //    gvProducts.Columns[7].Visible = false;

        //    gvProducts.Columns[8].Visible = false;
        //    gvProducts.Columns[9].Visible = false;
        //    gvProducts.Columns[10].Visible = false;
        //    gvProducts.Columns[11].Visible = false;
        //    gvProducts.Columns[12].Visible = true;
        //    gvProducts.Columns[14].Visible = false;
        //}
        //else if (this.ProductType == StatusProductType.Product)
        //{
        //    //gvProducts.Columns[15].Visible = true;
        //}
        txtStyle.Focus();
        //dlSearch.Focus();
    }


    protected void Page_Init(object sender, EventArgs e)
    {
        Product objPrd = new Product();
        DataTable dtProdDtl = objPrd.SearchProductsQuerySql(null, this.ProductType, "", "", 0, Globals.CurrentAppLanguageCode, "", "", "", "", "", "", "", "", "", Resources.Resource.grdvDetails);
        int i = 0;
        foreach (DataColumn dColumn in dtProdDtl.Columns)
        {
            JQGridColumn col = new JQGridColumn();
            string sColHdr = BusinessUtility.GetString(dColumn.ColumnName);
            if (sColHdr.ToUpper() == "NAME")
            {
                sColHdr = Resources.Resource.grdvProductName;
            }
            else if (sColHdr.ToUpper() == "MATERIAL")
            {
                sColHdr = Resources.Resource.lblMaterial;
            }
            else if (sColHdr.ToUpper() == "COLOR")
            {
                sColHdr = Resources.Resource.lblColor;
            }
            else if (sColHdr.ToUpper() == "SIZE")
            {
                sColHdr = Resources.Resource.lblSize;
            }
            else if (sColHdr.ToUpper() == "DETAIL")
            {
                sColHdr = Resources.Resource.grdvDetails;
            }

            col.HeaderText = BusinessUtility.GetString(sColHdr);
            col.DataField = BusinessUtility.GetString(dColumn.ColumnName);
            if (i == 0)
            {
                col.PrimaryKey = true;
                col.Visible = false;
            }
            else
            {
                col.PrimaryKey = false;
                col.Visible = true;
            }
            col.Editable = false;
            col.Sortable = false;
            gvProducts.Columns.Add(col);
            i += 1;
        }
    }

    protected void gvProducts_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
    }

    private string GetInputHtml(Dictionary<string, string> attributes)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<input type='text' ");
        foreach (var item in attributes.Keys)
        {
            sb.AppendFormat("{0}='{1}' ", item, attributes[item]);
        }
        sb.Append("/>");
        return sb.ToString();
    }

    protected void gvProducts_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string txtStyle = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_txtStyle"]);
        string ddlCollectionSearch = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_ddlCollectionSearch"]);
        string ddlMaterialSearch = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_ddlMaterialSearch"]);
        string lstColor = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_lstColor"]);
        string lstSizeGroup = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_lstSizeGroup"]);
        string history = BusinessUtility.GetString(Request.QueryString["_history"]);
        string lstProdCatg = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_lstProdCatg"]);

        string lstProdKeyWord = "";
        int iCollctionID = BusinessUtility.GetInt(Request.QueryString["_Collection"]);
        if (Request.QueryString.AllKeys.Contains("editSeq"))
        {
            int productID = 0, newSeq = 0;
            int.TryParse(Request.QueryString["editRowKey"], out productID);
            int.TryParse(Request.QueryString["seq"], out newSeq);
            _prd.UpdateProductSequence(productID, newSeq);
        }

        if (Request.QueryString.AllKeys.Contains("_history"))
        {
            DataTable dtProdDtl = _prd.SearchProductsQuerySql(null, this.ProductType, Request.QueryString[dlSearch.ClientID], Request.QueryString[txtSearch.ClientID], this.VendorID, Globals.CurrentAppLanguageCode, BusinessUtility.GetString(iCollctionID), txtStyle, ddlCollectionSearch, ddlMaterialSearch, lstColor, lstSizeGroup, history, lstProdCatg, lstProdKeyWord, Resources.Resource.grdvDetails);
            gvProducts.DataSource = dtProdDtl;
        }
        else
        {
            DataTable dtProdDtl = _prd.SearchProductsQuerySql(null, this.ProductType, dlSearch.SelectedValue, txtSearch.Text, this.VendorID, Globals.CurrentAppLanguageCode, BusinessUtility.GetString(iCollctionID), txtStyle, ddlCollectionSearch, ddlMaterialSearch, lstColor, lstSizeGroup, history, lstProdCatg, lstProdKeyWord, Resources.Resource.grdvDetails);
            gvProducts.DataSource = dtProdDtl;
        }
    }

    public int VendorID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["vdrID"]);
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("~/Inventory/Product.aspx?ptype={0}", (int)this.ProductType));
    }

    public StatusProductType ProductType
    {
        get
        {
            return QueryStringHelper.GetProductType("ptype");
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="MoveInventory.aspx.cs" Inherits="Inventory_MoveInventory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
<style type="text/css">
    ul.form li div.lbl
    {
        width:100px;
    }
    ul.form li div.input {
    width: 100px;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <ul class="form">
        <li>
            <div class="lbl">
                <asp:Label ID="Label1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblFromWarehouse %>" />
                :
            </div>
            <div class="input">
                <asp:Label ID="lblFromWareHouseDetail" runat="server"></asp:Label>
            </div>
            <div class="lbl">
                <asp:Label ID="Label2" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblToWarehouse %>" />
                :
            </div>
            <div class="input">
                <asp:DropDownList ID="dlToWhs" runat="server" Width="180px">
                </asp:DropDownList>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblFloor" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, grdvOnHandQty %>" />
            </div>
            <div class="input">
                <asp:Label ID="lblOnHandQty" runat="server" CssClass="innerText" MaxLength="10" />
            </div>
            <div class="lbl">
                <asp:Label ID="lblQtyToMove" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblQtyToMove %>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtQtyToMove" runat="server"  CssClass="innerText numericTextField" MaxLength="10" Width="100px" />
                <asp:CompareValidator ID="comvalOnHandQuanPerLocation" runat="server" ControlToValidate="txtQtyToMove"
                    Display="None" EnableClientScript="true" ErrorMessage="<%$ Resources:Resource, comvalOnHandQuanPerLocation %>"
                    Operator="DataTypeCheck" SetFocusOnError="true" Type="double" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
    </ul>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnReset" Text="<%$Resources:Resource, btnCancel%>" runat="server"
            CausesValidation="false" OnClick="btnReset_Click" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
</asp:Content>

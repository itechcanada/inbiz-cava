﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.Web;

public partial class Inventory_delete : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!IsPostBack) {            
            if (!UserProfile.IsAuthenticated) {
                Globals.RegisterCloseDialogScript(this);
            }
        }        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        switch (this.CurrentCommand.ToLower()) { 
            case DeleteCommands.DELETE_PRODUCT:
                DeleteProduct();
                break;  
            case DeleteCommands.DELETE_PRODUCT_QTY:
                DeleteProductQty();
                break;
            case DeleteCommands.DELETE_CATEGORY:
                DeleteCategory();
                break;
            case DeleteCommands.DELETE_ATTRIBUTE:
                DeleteAttribute();
                break;
            case DeleteCommands.DELETE_PRODUCT_SALE_PRICE:
                DeleteProductSalePrice();
                break;
            case DeleteCommands.DELETE_PRD_VENDOR_ASSIGN:
                DeleteVendorAssigned();
                break;
        }
    }

   

    public string CurrentCommand {
        get {
            if (!string.IsNullOrEmpty(Request.QueryString["cmd"]))
            {
                return Request.QueryString["cmd"].ToLower();
            }
            return "";
        }
    }

    private int GetKey(int ofIndex) {
        string cleneStr = StringUtil.GetCleneJoinedString(",", this.KeysToDelete);
        int[] keys = StringUtil.GetArrayFromJoindString(",", cleneStr);
        try
        {
            return keys[ofIndex];
        }
        catch 
        {
            return 0;
        }
    }

    private string GetStringKey(int ofIndex){
        string cleneStr = StringUtil.GetCleneJoinedString(",", this.KeysToDelete);
        string[] keys = cleneStr.Split(',');
        try
        {
            return keys[ofIndex];
        }
        catch 
        {
            return string.Empty;
        }
    }

    public string KeysToDelete {
        get {
            if (!string.IsNullOrEmpty(Request.QueryString["keys"])) {
                return Request.QueryString["keys"];
            }
            return "";
        }
    }

    public string JsCallBackFunction {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }

    #region Delete Functions

    private void DeleteProduct()
    {
        //if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        //{
        //    Product prd = new Product();

        //    try
        //    {
        //        //check is bed occupied
        //        if (prd.IsOccupiedForFuture(this.GetKey(0)))
        //        {
        //            lblMessage.Text = Resources.Resource.msgProductDeleteConfirm;
        //            pnlConfirm.Visible = true;
        //            pnlDeleteCommand.Visible = false;
        //            return;
        //        }

        //        prd.Delete(this.GetKey(0));
        //        lblMessage.Text = Resources.Resource.msgDeleteSuccess;
        //        if (!string.IsNullOrEmpty(this.JsCallBackFunction))
        //        {
        //            Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
        //        }
        //    }
        //    catch
        //    {
        //        lblMessage.Text = "Critical Error!";
        //    }
        //}
        //else
        //{
        //    lblMessage.Text = "Access Denied!";
        //}

        //pnlOk.Visible = true;
        //pnlDeleteCommand.Visible = false;
    }

    private void DeleteProductQty()
    {
        //bool secure = CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER) == true || CurrentUser.IsInRole(RoleID.ADMINISTRATOR) == true;
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            ProductQuantity prdQty = new ProductQuantity();

            try
            {
                prdQty.Delete(this.GetKey(0));
                lblMessage.Text = Resources.Resource.msgDeleteSuccess;
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
                }
            }
            catch
            {
                lblMessage.Text = "Critical Error!";
            }
        }
        else
        {
            lblMessage.Text = "Access Denied!";
        }

        pnlOk.Visible = true;
        pnlDeleteCommand.Visible = false;
    }

    private void DeleteCategory()
    {
        //if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        //{
        Category categ = new Category();

        try
        {
            OperationResult res = categ.Delete(this.GetKey(0));
            if (res == OperationResult.Failuer)
            {
                lblMessage.Text = "Selected category has child category & can not be deleted!";
            }
            else
            {
                lblMessage.Text = Resources.Resource.msgDeleteSuccess;
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
                }
            }
        }
        catch
        {
            lblMessage.Text = "Critical Error!";
        }
        //}
        //else
        //{
        //    lblMessage.Text = "Access Denied!";
        //}

        pnlOk.Visible = true;
        pnlDeleteCommand.Visible = false;
    }

    private void DeleteAttribute()
    {
        //if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        //{
        Attributes attr = new Attributes();

        try
        {
            attr.Delete(this.GetKey(0));
            lblMessage.Text = Resources.Resource.msgDeleteSuccess;
            if (!string.IsNullOrEmpty(this.JsCallBackFunction))
            {
                Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
            }
        }
        catch
        {
            lblMessage.Text = "Critical Error!";
        }
        //}
        //else
        //{
        //    lblMessage.Text = "Access Denied!";
        //}

        pnlOk.Visible = true;
        pnlDeleteCommand.Visible = false;
    }

    private void DeleteProductSalePrice()
    {
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            ProductSalePrice prdSalePrice = new ProductSalePrice();

            try
            {
                prdSalePrice.Delete(this.GetKey(0));
                lblMessage.Text = Resources.Resource.msgDeleteSuccess;
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
                }
            }
            catch
            {
                lblMessage.Text = "Critical Error!";
            }
        }
        else
        {
            lblMessage.Text = "Access Denied!";
        }

        pnlOk.Visible = true;
        pnlDeleteCommand.Visible = false;
    }

    private void DeleteVendorAssigned()
    {
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            ProductAssociateVendor prdSalePrice = new ProductAssociateVendor();

            try
            {
                prdSalePrice.Delete(this.GetKey(0), this.GetKey(1));
                lblMessage.Text = Resources.Resource.msgDeleteSuccess;
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
                }
            }
            catch
            {
                lblMessage.Text = "Critical Error!";
            }
        }
        else
        {
            lblMessage.Text = "Access Denied!";
        }

        pnlOk.Visible = true;
        pnlDeleteCommand.Visible = false;
    }
    
    
    #endregion
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Product prd = new Product();

            try
            {               
                prd.Delete(this.GetKey(0));
                lblMessage.Text = Resources.Resource.msgDeleteSuccess;
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
                }
            }
            catch
            {
                lblMessage.Text = "Critical Error!";
            }
        }
        else
        {
            lblMessage.Text = "Access Denied!";
        }

        pnlConfirm.Visible = false;
        pnlOk.Visible = true;
        pnlDeleteCommand.Visible = false;
    }
}
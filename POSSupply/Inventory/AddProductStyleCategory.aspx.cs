﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Inventory_AddProductStyleCategory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                MessageState.SetGlobalMessage(MessageType.Success, "");
                ProductCategory objProductCategory = new ProductCategory();
                objProductCategory.CatgHrdID = 1;
                ddlUsers.DataSource = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
                ddlUsers.DataTextField = "CatgName";
                ddlUsers.DataValueField = "CatgID";
                ddlUsers.DataBind();

                objProductCategory.CatgHrdID = 1;
                var lst = objProductCategory.GetStyleCatg(dbHelp, BusinessUtility.GetInt(this.StyleID), Globals.CurrentAppLanguageCode);
                foreach (var item in lst)
                {
                    ListItem li = ddlUsers.Items.FindByValue(item.CatgID.ToString());
                    if (li != null)
                    {
                        li.Selected = true;
                    }
                }

                //string newMaterialID = BusinessUtility.GetString(Request.QueryString["MaterialID"]);
                //if (newMaterialID != "")
                //{
                //    string[] sMaterialID = newMaterialID.Split(',');

                //    foreach (string MaterialID in sMaterialID)
                //    {
                //        ListItem li = ddlUsers.Items.FindByValue(MaterialID);
                //        if (li != null)
                //        {
                //            li.Selected = true;
                //        }
                //    }
                //}

                ////Check for ajax call if New Material Add
                //if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "ajxNewMaterialCreated")
                //{
                //    try
                //    {
                //        int newMaterialID = BusinessUtility.GetInt( Request.Form["materialID"]);
                //        if (newMaterialID > 0)
                //        {
                //            ListItem li = ddlUsers.Items.FindByValue(newMaterialID.ToString());
                //            if (li != null)
                //            {
                //                li.Selected = true;
                //            }
                //        }
                //        Response.Write("ok");
                //    }
                //    catch
                //    {
                //        Response.Write("error");
                //    }

                //    Response.End();
                //    Response.SuppressContent = true;
                //}


            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    private string StyleID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["StyleID"]);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {

            ProductCategory al = new ProductCategory();
            var lst = new List<int>();
            int id = 0;
            foreach (ListItem item in ddlUsers.Items)
            {
                id = 0;
                if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lst.Contains(id))
                {
                    lst.Add(id);

                }
            }
            al.CatgHrdID = 1;
            if (al.AddCatgInStyle(BusinessUtility.GetInt(this.StyleID), lst.ToArray()) == true)
            {
                //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated );
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadStyle();"), true);
            }
            else
            {
                //MessageState.SetGlobalMessage(MessageType.Failure, "Collection Creation failure");
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }
            //{
            //    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgAlertsAssignedToUser);
            //    Globals.RegisterReloadParentScript(this);
            //}
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    //[WebMethod]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public static List<ProductCategory> GetMaterialList()
    //{
    //    ProductCategory objProductCategory = new ProductCategory();
    //    var t = objProductCategory.GetMaterialList(null, Globals.CurrentAppLanguageCode);
    //    return t;
    //}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Inventory_AddColorGroup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MessageState.SetGlobalMessage(MessageType.Success, "");
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                
                //if (BusinessUtility.GetString(Request.QueryString["Status"]) == "INDIVIDUAL")
                {
                    //trIsAxctive.Visible = true;
                    if (BusinessUtility.GetInt(Request.QueryString["ColorGroupID"]) > 0)
                    {
                        ProductColorGroup objProductColor = new ProductColorGroup();
                        objProductColor.ColorGroupID = BusinessUtility.GetInt(Request.QueryString["ColorGroupID"]);
                        var lst = objProductColor.GetAllColorGroupList(null, Globals.CurrentAppLanguageCode);
                        foreach (var item in lst)
                        {
                            txtColorNameEn.Text = item.ColorGroupNameEn;
                            txtColorNameFr.Text = item.ColorGroupNameFr;
                            //txtShortName.Text = item.ShortName;
                            rblstActive.SelectedValue = BusinessUtility.GetBool(item.ColorGroupIsActive) ? "1" : "0";
                        }

                    }
                }
                txtColorNameFr.Focus();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            ProductColorGroup objProductColor = new ProductColorGroup();
            objProductColor.ColorGroupNameEn = txtColorNameEn.Text;
            objProductColor.ColorGroupNameFr = txtColorNameFr.Text;
            //objCollection.ShortName = txtShortName.Text;
            objProductColor.ColorGroupIsActive = BusinessUtility.GetInt(rblstActive.SelectedItem.Value);
            //objProductColor.ColorID = BusinessUtility.GetInt(Request.QueryString["ColorID"]);
            if (objProductColor.SaveColorGroup(null, CurrentUser.UserID, BusinessUtility.GetInt(Request.QueryString["ColorGroupID"])) == true)
            {
                if (BusinessUtility.GetInt(Request.QueryString["ColorGroupID"]) > 0)
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgColorGroupUpdated);
                        Globals.RegisterReloadParentScript(this);
                        Globals.RegisterCloseDialogScript(this);
                        return;
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgColorGroupCreated);
                        Globals.RegisterReloadParentScript(this);
                        Globals.RegisterCloseDialogScript(this);
                        return;
                    }
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadStyle({0});", objProductColor.ColorGroupID), true);
            }
            else
            {
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }

        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
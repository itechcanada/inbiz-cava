Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Partial Class Inventory_ProductDescription
    Inherits BasePage
    Public objPrdDesc As New clsPrdDescriptions
    Public objProduct As New clsProducts
    Protected Sub Inventory_ProductDescription_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        lblHeading.Text = objPrdDesc.funProductName & lblPrdDescription
        If Not Page.IsPostBack Then
            If Request.QueryString("PrdID") <> "" Then
                subFillData()
            End If
        End If
        If Session("msg") <> "" Then
            lblMsg.Text = Session("msg").ToString
            Session.Remove("msg")
        End If
        If Session("UserModules").ToString.Contains("INROL") = True And (Session("UserModules").ToString.Contains("STK") = False Or Session("UserModules").ToString.Contains("ADM") = False) Then
            imgCmdSave.Visible = False
        End If
        txtProdName.Focus()
    End Sub
    Public Sub subFillData()
        objPrdDesc.Id = Request.QueryString("PrdID")
        If Request.QueryString("Lng") = "" Then
            objPrdDesc.DescLang = "en"
        Else
            objPrdDesc.DescLang = Request.QueryString("Lng")
        End If
        objPrdDesc.subGetPrdDescriptionsInfo()
        txtProdDescr.Text = objPrdDesc.PrdSmallDesc
        txtProdTechDescr.Text = objPrdDesc.PrdLargeDesc
        txtProdName.Text = objPrdDesc.PrdName
    End Sub
    Public Sub subSetData()
        objPrdDesc.Id = Request.QueryString("PrdID")
        objPrdDesc.PrdSmallDesc = txtProdDescr.Text
        objPrdDesc.PrdLargeDesc = txtProdTechDescr.Text
        objPrdDesc.PrdName = txtProdName.Text
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    Protected Sub imgCmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdSave.ServerClick
        subSetData()
        If Request.QueryString("Lng") = "" Then
            objPrdDesc.DescLang = "en"
            objPrdDesc.funUpdatePrdDescriptions()
            Session.Add("msg", msgPrdDesInFrench)
            Response.Redirect("ProductDescription.aspx?Lng=fr&PrdID=" & Request.QueryString("PrdID") & "&Update=yes" & "&Kit=" & Request.QueryString("Kit"))
        ElseIf Request.QueryString("Lng") = "fr" Then
            objPrdDesc.DescLang = "fr"
            objPrdDesc.funUpdatePrdDescriptions()
            Session.Add("msg", msgPrdDesInSpanish)
            Response.Redirect("ProductDescription.aspx?Lng=sp&PrdID=" & Request.QueryString("PrdID") & "&Update=yes" & "&Kit=" & Request.QueryString("Kit"))
        ElseIf Request.QueryString("Lng") = "sp" Then
            objPrdDesc.DescLang = "sp"
            objPrdDesc.funUpdatePrdDescriptions()
        End If
        'Session.Add("msg", msgPrdDescSaved)
        If Request.QueryString("Kit") = "1" Then
            Response.Redirect("ProductKit.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
        Else
            Response.Redirect("ProductQuantity.aspx?PrdID=" & Request.QueryString("PrdID") & "&Kit=" & Request.QueryString("Kit"))
        End If
    End Sub
End Class

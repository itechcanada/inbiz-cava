﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Inventory_New_ViewProduct : BasePage
{
    Product _prd = new Product();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(gvProducts))
        {
            //Security Check
            if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER))
            {
                Response.Redirect("~/Errors/AccessDenied.aspx");
            }

            if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
            {
                btnAdd.Visible = false;
            }

            ListItem lstBlank = new ListItem("", "");
            DropDownHelper.FillDropdown(dlSearch, "PD", "dlSh1", Globals.CurrentAppLanguageCode, null);
            dlSearch.Items.Insert(0, lstBlank);
            dlSearch.SelectedIndex = 1;
            //txtSearch.Focus();
            ProductCategory objProductSize = new ProductCategory();
            var vProdSrchCatg = objProductSize.GetSearchCategoryList(null, Globals.CurrentAppLanguageCode,Resources.Resource.All);
            lstProdCatg.DataSource = vProdSrchCatg;
            lstProdCatg.DataTextField = "CatgName";
            lstProdCatg.DataValueField = "CatgValue";
            lstProdCatg.DataBind();

            ProductColor objProductMaterial = new ProductColor();
            var vColorList = objProductMaterial.GetColorList(null, Globals.CurrentAppLanguageCode);
            lstColor.DataSource = vColorList;
            lstColor.DataTextField = "ColorName";
            lstColor.DataValueField = "ColorID";
            lstColor.DataBind();


            //Set Sections visivilayt by Product Type
            switch (this.ProductType)
            {
                case StatusProductType.Product:
                    break;
                case StatusProductType.Accommodation:
                    ltTitle.Text = Resources.Resource.lblViewAccommodation;
                    btnAdd.Text = Resources.Resource.lblAddAccommodation;
                    break;
                case StatusProductType.ServiceProduct:
                    ltTitle.Text = Resources.Resource.lblViewServiceProducts;
                    btnAdd.Text = Resources.Resource.lblAddServiceProduct;
                    break;
                case StatusProductType.AdminFee:
                    ltTitle.Text = Resources.Resource.lblManageAdminFee;
                    btnAdd.Text = Resources.Resource.lblManageAdminFee;
                    int adminFeeProductID = _prd.GetAdminFeeProductID();
                    if (adminFeeProductID > 0)
                    {
                        Response.Redirect(string.Format("Product.aspx?PrdID={0}&ptype={1}", adminFeeProductID, (int)StatusProductType.AdminFee));
                    }
                    break;
                case StatusProductType.ChildUnder12:
                    break;
                case StatusProductType.CourseProduct:
                    ltTitle.Text = Resources.Resource.lblViewCourses;
                    btnAdd.Text = Resources.Resource.lblAddNewCourse;
                    break;
            }


            // Get User Grid Height
            InbizUser _usr = new InbizUser();
            _usr.PopulateObject(CurrentUser.UserID);
            if (BusinessUtility.GetInt(_usr.UserGridHeight) > 0)
            {
                gvProducts.Height = _usr.UserGridHeight;
            }
        }
                
        txtSearch.Focus();
        //dlSearch.Focus();
    }

    protected void gvProducts_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        int idxDSStatusCol = 8;
        
        if (e.ColumnIndex == 4) //Format status column
        {
            e.CellHtml = string.Format(@"<img src=""{0}"" />", ResolveUrl(string.Format("~/images/{0}", e.RowValues[idxDSStatusCol])));
        }
        
        else if (e.ColumnIndex == 5) //Format edit link column
        {
            string editUrl = string.Format("Product.aspx?PrdID={0}", e.RowKey);
            e.CellHtml = string.Format(@"<a href=""{0}"">{1}</a>", editUrl, Resources.Resource.lblEdit);
        }
        else if (e.ColumnIndex == 6) //Format Delete Link
        {
            string delUrl = string.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_PRODUCT, e.RowKey, "reloadGrid");
            e.CellHtml = string.Format(@"<a class=""pop_delete"" href=""{0}"">{1}</a>", delUrl, Resources.Resource.CmdCssDelete);
        }
    }

    private string GetInputHtml(Dictionary<string, string> attributes)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<input type='text' ");
        foreach (var item in attributes.Keys)
        {
            sb.AppendFormat("{0}='{1}' ", item, attributes[item]);
        }
        sb.Append("/>");
        return sb.ToString();
    }

    protected void gvProducts_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {

        string txtStyle = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_txtStyle"]);
        string ddlCollectionSearch = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_ddlCollectionSearch"]);
        string ddlMaterialSearch = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_ddlMaterialSearch"]);
        string lstColor = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_lstColor"]);
        string lstSizeGroup = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_lstSizeGroup"]);
        string history = BusinessUtility.GetString(Request.QueryString["_history"]);
        string lstProdCatg = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_lstProdCatg"]);


        //ListBox objNewlstBox = new ListBox();
        //objNewlstBox = (ListBox)this.FindControl("ctl00_ctl00_cphFullWidth_cphLeft_lstProdCatg");
        //var selected = objNewlstBox.GetSelectedIndices().ToList();
        //var selectedValues = (from c in selected
        //                      select objNewlstBox.Items[c].Value).ToList();

        string lstProdKeyWord = "";//BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_lstProdKeyWord"]);
        int iCollctionID = BusinessUtility.GetInt(Request.QueryString["_Collection"]);
        if (Request.QueryString.AllKeys.Contains("editSeq"))
        {
            //To do update productg sequence
            int productID = 0, newSeq = 0;
            int.TryParse(Request.QueryString["editRowKey"], out productID);
            int.TryParse(Request.QueryString["seq"], out newSeq);
            _prd.UpdateProductSequence(productID, newSeq);
        }

        if (Request.QueryString.AllKeys.Contains("_history"))
        {
            sdsPropducts.SelectCommand = _prd.SearchProductsSql(sdsPropducts.SelectParameters, this.ProductType, Request.QueryString[dlSearch.ClientID], Request.QueryString[txtSearch.ClientID], this.VendorID, Globals.CurrentAppLanguageCode, BusinessUtility.GetString(iCollctionID), txtStyle, ddlCollectionSearch, ddlMaterialSearch, lstColor, lstSizeGroup, history, lstProdCatg, lstProdKeyWord);
        }
        else
        {
            sdsPropducts.SelectCommand = _prd.SearchProductsSql(sdsPropducts.SelectParameters, this.ProductType, dlSearch.SelectedValue, txtSearch.Text, this.VendorID, Globals.CurrentAppLanguageCode, BusinessUtility.GetString(iCollctionID), txtStyle, ddlCollectionSearch, ddlMaterialSearch, lstColor, lstSizeGroup, history, lstProdCatg, lstProdKeyWord);
        }
    }

    public int VendorID {
        get {
            return BusinessUtility.GetInt(Request.QueryString["vdrID"]);
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("~/Inventory/Product.aspx?ptype={0}", (int)this.ProductType));
    }

    public StatusProductType ProductType
    {
        get
        {
            return QueryStringHelper.GetProductType("ptype");
        }
    }
}
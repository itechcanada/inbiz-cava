﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Inventory_AddTexture : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                MessageState.SetGlobalMessage(MessageType.Success, "");
                ProductTexture objProductTexture = new ProductTexture();
                //InbizUser user = new InbizUser();
                //user.FillAllUsers(dbHelp, ddlUsers, null);
                //NewAlert al = new NewAlert();
                //objProductTexture.FillAllTexture(dbHelp, ddlUsers,null , Globals.CurrentAppLanguageCode);

                ddlUsers.DataSource = objProductTexture.GetTextureList(null, Globals.CurrentAppLanguageCode);
                ddlUsers.DataTextField = "TextureName";
                ddlUsers.DataValueField = "TextureID";
                ddlUsers.DataBind();

                var lst = objProductTexture.GetStyleTexture(dbHelp, BusinessUtility.GetInt(this.StyleID), Globals.CurrentAppLanguageCode);
                foreach (var item in lst)
                {
                    ListItem li = ddlUsers.Items.FindByValue(item.TextureID.ToString());
                    if (li != null)
                    {
                        li.Selected = true;
                    }
                }

                string newTextureID = BusinessUtility.GetString(Request.QueryString["TextureID"]);
                if (newTextureID != "")
                {
                    string[] sTextureID = newTextureID.Split(',');

                    foreach (string TextureID in sTextureID)
                    {
                        ListItem li = ddlUsers.Items.FindByValue(TextureID);
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    private string StyleID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["StyleID"]);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {

            ProductTexture al = new ProductTexture();
            var lst = new List<int>();
            int id = 0;
            foreach (ListItem item in ddlUsers.Items)
            {
                id = 0;
                if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lst.Contains(id))
                {
                    lst.Add(id);

                }
            }

            if (al.AddTextureInStyle(BusinessUtility.GetInt(this.StyleID), lst.ToArray()) == true)
            {
                //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated );
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadStyle();"), true);
            }
            else
            {
                //MessageState.SetGlobalMessage(MessageType.Failure, "Collection Creation failure");
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }
            //{
            //    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgAlertsAssignedToUser);
            //    Globals.RegisterReloadParentScript(this);
            //}
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ProductTexture> GetTextureList()
    {
        ProductTexture objProductTexture = new ProductTexture();
        var t = objProductTexture.GetTextureList(null, Globals.CurrentAppLanguageCode);
        return t;
    }
}
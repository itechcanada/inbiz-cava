﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Inventory_Product : BasePage
{
    private Control _contentControl;
    private Product _prd = new Product();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Security Check
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER))
        {
            Response.Redirect("~/Errors/AccessDenined.aspx");
        }
        if (this.ActiveSectionIndex == InventorySectionKey.ViewProducts)
        {
            Response.Redirect(this.GetSectionLink(InventorySectionKey.ViewProducts).Value);
        }
        BindNavigation();
        LoadUserControl(this.ActiveSectionIndex);
    }

    private void LoadUserControl(InventorySectionKey index)
    {
        string ctrlPath = this.InventorySections[index];

        _contentControl = Page.LoadControl(ctrlPath);
        phControl.Controls.Clear();
        phControl.Controls.Add(_contentControl);
        _contentControl.ID = "ctlContent";
        IProduct ctrl = (IProduct)_contentControl;

        if (ctrl.ProductID > 0)
        {
            switch (this.ProductType)
            {
                case StatusProductType.Accommodation:
                    ltTitle.Text = Resources.Resource.lblEditAccommodation;
                    btnAdd.Text = Resources.Resource.lblAddAccommodation;

                    prdPosCategory.Visible = false;
                    prdTagPrinting.Visible = false;
                    prdQty.Visible = false;
                    prdKit.Visible = false;
                    break;
                case StatusProductType.ServiceProduct:
                    ltTitle.Text = Resources.Resource.lblEditServiceProduct;
                    btnAdd.Text = Resources.Resource.lblAddServiceProduct;

                    prdPosCategory.Visible = false;
                    prdTagPrinting.Visible = false;
                    prdQty.Visible = false;
                    prdKit.Visible = false;
                    break;
                case StatusProductType.AdminFee:
                    ltTitle.Text = Resources.Resource.lblManageAdminFee;
                    btnAdd.Visible = false;

                    prdPosCategory.Visible = false;
                    prdTagPrinting.Visible = false;
                    prdQty.Visible = false;
                    prdKit.Visible = false;
                    break;
                case StatusProductType.ChildUnder12:
                    ltTitle.Text = Resources.Resource.lblEditProduct;
                    btnAdd.Visible = false;

                    prdPosCategory.Visible = false;
                    prdTagPrinting.Visible = false;
                    prdQty.Visible = false;
                    prdKit.Visible = false;
                    break;
                case StatusProductType.CourseProduct:
                    ltTitle.Text = Resources.Resource.lblEditCourse;
                    btnAdd.Visible = false;

                    prdPosCategory.Visible = false;
                    prdTagPrinting.Visible = false;
                    prdQty.Visible = false;
                    prdKit.Visible = false;
                    break;
                case StatusProductType.Product:
                default:
                    ltTitle.Text = Resources.Resource.lblEditProduct;
                    btnAdd.Text = Resources.Resource.lblAddProduct;
                    prdPosCategory.Visible = (index == 0 && this.ProductID > 0 && ConfigurationManager.AppSettings["ShowPrdPOSCatg"].ToUpper() == "TRUE");
                    mdAssignPosCateg.Url = string.Format("~/Inventory/AssignPOSCategoryModal.aspx?PrdID={0}", this.ProductID);

                    prdTagPrinting.Visible = (index == 0 && this.ProductID > 0);
                    mdTagPrinting.Url = string.Format("~/Inventory/PrintTagsSettings.aspx?PrdID={0}", this.ProductID);
                    break;
            }
        }
        else
        {
            switch (this.ProductType)
            {
                case StatusProductType.Accommodation:
                    ltTitle.Text = Resources.Resource.lblAddAccommodation;
                    btnAdd.Text = Resources.Resource.lblAddAccommodation;

                    prdPosCategory.Visible = false;
                    prdTagPrinting.Visible = false;
                    prdQty.Visible = false;
                    prdKit.Visible = false;
                    break;
                case StatusProductType.ServiceProduct:
                    ltTitle.Text = Resources.Resource.lblAddServiceProduct;
                    btnAdd.Text = Resources.Resource.lblAddServiceProduct;

                    prdPosCategory.Visible = false;
                    prdTagPrinting.Visible = false;
                    prdQty.Visible = false;
                    prdKit.Visible = false;
                    break;
                case StatusProductType.AdminFee:
                    ltTitle.Text = Resources.Resource.lblManageAdminFee;
                    btnAdd.Visible = false;

                    prdPosCategory.Visible = false;
                    prdTagPrinting.Visible = false;
                    prdQty.Visible = false;
                    prdKit.Visible = false;
                    break;
                case StatusProductType.CourseProduct:
                    ltTitle.Text = Resources.Resource.lblAddCourse;
                    btnAdd.Visible = false;

                    prdPosCategory.Visible = false;
                    prdTagPrinting.Visible = false;
                    prdQty.Visible = false;
                    prdKit.Visible = false;
                    break;
                case StatusProductType.Product:
                default:
                    ltTitle.Text = Resources.Resource.lblAddProduct;
                    btnAdd.Text = Resources.Resource.lblAddProduct;
                    break;
            }
        }
        ctrl.Initialize();

        prdKit.Visible = index == InventorySectionKey.ProductKit;
        mdProductKitContent.Url = string.Format("~/Inventory/ProductKit.aspx?PrdID={0}&jscallback={1}", this.ProductID, "reloadGrid");

    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("~/Inventory/Product.aspx?ptype={0}", (int)this.ProductType));
    }

    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    public StatusProductType ProductType
    {
        get
        {
            return QueryStringHelper.GetProductType("ptype");
        }
    }

    public void BindNavigation()
    {
        blstNav.Items.Clear();

        blstNav.Items.Add(this.GetSectionLink(InventorySectionKey.Details));
        if (this.ProductID > 0)
        {
            _prd.PopulateObject(this.ProductID);
            blstNav.Items.Add(this.GetSectionLink(InventorySectionKey.Description));
            if (this.ProductType == StatusProductType.Product)
            {
                if (IsResturant() == false)
                {
                    //blstNav.Items.Add(this.GetSectionLink(InventorySectionKey.ProductColors));
                }
                blstNav.Items.Add(this.GetSectionLink(InventorySectionKey.ProducImages));
                blstNav.Items.Add(this.GetSectionLink(InventorySectionKey.ProductQuantity));
                if (_prd.PrdIsKit)
                {
                    blstNav.Items.Add(this.GetSectionLink(InventorySectionKey.ProductKit));
                }
                blstNav.Items.Add(this.GetSectionLink(InventorySectionKey.ProductsAssocaitions));
                blstNav.Items.Add(this.GetSectionLink(InventorySectionKey.ProductSalesPrice));
                blstNav.Items.Add(this.GetSectionLink(InventorySectionKey.AssociateVendors));
            }
            else if (_prd.PrdType == (int)StatusProductType.Accommodation)
            {
                blstNav.Items.Add(this.GetSectionLink(InventorySectionKey.Ammenities));
                //blstNav.Items.Add(this.GetSectionLink(InventorySectionKey.ProductDiscount));
                //blstNav.Items.Add(this.GetSectionLink(InventorySectionKey.ProductBlackOut));
            }
            else if (_prd.PrdType == (int)StatusProductType.ChildUnder12 || _prd.PrdType == (int)StatusProductType.CourseProduct)
            {
                blstNav.Items.Add(this.GetSectionLink(InventorySectionKey.ProductQuantity));
            }
        }
        blstNav.Items.Add(this.GetSectionLink(InventorySectionKey.ViewProducts));
    }

    private InventorySectionKey ActiveSectionIndex
    {
        get
        {
            int section = 0;
            if (int.TryParse(Request.QueryString["section"], out section))
            {
                if (Enum.IsDefined(typeof(InventorySectionKey), section))
                {
                    return (InventorySectionKey)Enum.ToObject(typeof(InventorySectionKey), section);
                }
                else
                {
                    return InventorySectionKey.Details;
                }
            }
            return InventorySectionKey.Details;
        }
    }

    private ListItem GetSectionLink(InventorySectionKey sectionIndex)
    {
        ListItem li = new ListItem(this.InventorySectionsNames[sectionIndex], this.InventorySectionsUrl[sectionIndex]);
        if (sectionIndex == this.ActiveSectionIndex)
        {
            li.Attributes["class"] = "open";
        }
        return li;
    }

    private Dictionary<InventorySectionKey, string> InventorySections
    {
        get
        {
            Dictionary<InventorySectionKey, string> sec = new Dictionary<InventorySectionKey, string>();
            sec[InventorySectionKey.Details] = "~/Inventory/UserControls/ProductDetails.ascx";
            sec[InventorySectionKey.Description] = "~/Inventory/UserControls/ProductDescription.ascx";
            sec[InventorySectionKey.ProductColors] = "~/Inventory/UserControls/ProductColor.ascx";
            sec[InventorySectionKey.ProducImages] = "~/Inventory/UserControls/ProductImages.ascx";
            sec[InventorySectionKey.ProductQuantity] = "~/Inventory/UserControls/ProductQuantity.ascx";
            sec[InventorySectionKey.ProductKit] = "~/Inventory/UserControls/ProductKit.ascx";
            sec[InventorySectionKey.ProductsAssocaitions] = "~/Inventory/UserControls/ProductAssociation.ascx";
            sec[InventorySectionKey.ProductSalesPrice] = "~/Inventory/UserControls/ProductSalePrice.ascx";
            sec[InventorySectionKey.AssociateVendors] = "~/Inventory/UserControls/ProductVendor.ascx";
            sec[InventorySectionKey.Ammenities] = "~/Inventory/UserControls/ProductAttributes.ascx";
            //sec[InventorySectionKey.ProductDiscount] = "~/Inventory/UserControls/ProductDiscount.ascx";
            //sec[InventorySectionKey.ProductBlackOut] = "~/Inventory/UserControls/ProductBlackOuts.ascx";
            return sec;
        }
    }

    private Dictionary<InventorySectionKey, string> InventorySectionsUrl
    {
        get
        {
            Dictionary<InventorySectionKey, string> sec = new Dictionary<InventorySectionKey, string>();
            sec[InventorySectionKey.Details] = QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.Details).ToString());
            sec[InventorySectionKey.Description] = QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.Description).ToString());
            sec[InventorySectionKey.ProductColors] = QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.ProductColors).ToString());
            sec[InventorySectionKey.ProducImages] = QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.ProducImages).ToString());
            sec[InventorySectionKey.ProductQuantity] = QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.ProductQuantity).ToString());
            sec[InventorySectionKey.ProductKit] = QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.ProductKit).ToString());
            sec[InventorySectionKey.ProductsAssocaitions] = QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.ProductsAssocaitions).ToString());
            //sec[InventorySectionKey.ProductsAssocaitions] = "~/Inventory/ProductAssociation.aspx?PrdID=" + this.ProductID.ToString();
            sec[InventorySectionKey.ProductSalesPrice] = QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.ProductSalesPrice).ToString());
            //sec[InventorySectionKey.ProductSalesPrice] = "~/Inventory/ProductSalePrice.aspx?PrdID=" + this.ProductID.ToString();
            sec[InventorySectionKey.AssociateVendors] = QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.AssociateVendors).ToString());
            //sec[InventorySectionKey.AssociateVendors] = "~/Inventory/AssociateVendor.aspx?PrdID=" + this.ProductID.ToString();
            sec[InventorySectionKey.Ammenities] = QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.Ammenities).ToString());
            //sec[InventorySectionKey.ProductDiscount] = QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.ProductDiscount).ToString());
            //sec[InventorySectionKey.ProductBlackOut] = QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.ProductBlackOut).ToString());
            sec[InventorySectionKey.ViewProducts] = string.Format("~/Inventory/ViewProduct.aspx?ptype={0}", (int)this.ProductType);
            return sec;
        }
    }

    private Dictionary<InventorySectionKey, string> InventorySectionsNames
    {
        get
        {
            Dictionary<InventorySectionKey, string> sec = new Dictionary<InventorySectionKey, string>();
            switch (this.ProductType)
            {
                case StatusProductType.Accommodation:
                    sec[InventorySectionKey.Details] = Resources.Resource.lblAccommodationDetails;
                    sec[InventorySectionKey.Description] = Resources.Resource.lblDescription;
                    sec[InventorySectionKey.ProductColors] = Resources.Resource.btnprodcolor;
                    sec[InventorySectionKey.ProducImages] = Resources.Resource.btnprodimages;
                    sec[InventorySectionKey.ProductQuantity] = Resources.Resource.lblQuantityPerWarehouse;
                    sec[InventorySectionKey.ProductKit] = Resources.Resource.btnprodkit;
                    sec[InventorySectionKey.ProductsAssocaitions] = Resources.Resource.btnprodasso;
                    sec[InventorySectionKey.ProductSalesPrice] = Resources.Resource.btnprodsale;
                    sec[InventorySectionKey.AssociateVendors] = Resources.Resource.btnassovender;
                    sec[InventorySectionKey.Ammenities] = Resources.Resource.lblAmenities;
                    //sec[InventorySectionKey.ProductDiscount] = Resources.Resource.lblManageDiscount;
                    //sec[InventorySectionKey.ProductBlackOut] = Resources.Resource.lblManageBlackOut;
                    sec[InventorySectionKey.ViewProducts] = Resources.Resource.lblViewAccommodation;
                    break;
                case StatusProductType.ServiceProduct:
                    sec[InventorySectionKey.Details] = Resources.Resource.lblServiceDetails;
                    sec[InventorySectionKey.Description] = Resources.Resource.lblDescription;
                    sec[InventorySectionKey.ProductColors] = Resources.Resource.btnprodcolor;
                    sec[InventorySectionKey.ProducImages] = Resources.Resource.btnprodimages;
                    sec[InventorySectionKey.ProductQuantity] = Resources.Resource.lblQuantityPerWarehouse;
                    sec[InventorySectionKey.ProductKit] = Resources.Resource.btnprodkit;
                    sec[InventorySectionKey.ProductsAssocaitions] = Resources.Resource.btnprodasso;
                    sec[InventorySectionKey.ProductSalesPrice] = Resources.Resource.btnprodsale;
                    sec[InventorySectionKey.AssociateVendors] = Resources.Resource.btnassovender;
                    sec[InventorySectionKey.Ammenities] = Resources.Resource.lblAmenities;
                    //sec[InventorySectionKey.ProductDiscount] = Resources.Resource.lblManageDiscount;
                    //sec[InventorySectionKey.ProductBlackOut] = Resources.Resource.lblManageBlackOut;
                    sec[InventorySectionKey.ViewProducts] = Resources.Resource.lblViewServices;
                    break;
                case StatusProductType.AdminFee:
                    sec[InventorySectionKey.Details] = Resources.Resource.lblAdminFeeDetails;
                    sec[InventorySectionKey.Description] = Resources.Resource.lblDescription;
                    sec[InventorySectionKey.ProductColors] = Resources.Resource.btnprodcolor;
                    sec[InventorySectionKey.ProducImages] = Resources.Resource.btnprodimages;
                    sec[InventorySectionKey.ProductQuantity] = Resources.Resource.lblQuantityPerWarehouse;
                    sec[InventorySectionKey.ProductKit] = Resources.Resource.btnprodkit;
                    sec[InventorySectionKey.ProductsAssocaitions] = Resources.Resource.btnprodasso;
                    sec[InventorySectionKey.ProductSalesPrice] = Resources.Resource.btnprodsale;
                    sec[InventorySectionKey.AssociateVendors] = Resources.Resource.btnassovender;
                    sec[InventorySectionKey.Ammenities] = Resources.Resource.lblAmenities;
                    //sec[InventorySectionKey.ProductDiscount] = Resources.Resource.lblManageDiscount;
                    //sec[InventorySectionKey.ProductBlackOut] = Resources.Resource.lblManageBlackOut;
                    sec[InventorySectionKey.ViewProducts] = Resources.Resource.lblViewAdminFee;
                    break;
                case StatusProductType.CourseProduct:
                    sec[InventorySectionKey.Details] = Resources.Resource.lblCourseDetails;
                    sec[InventorySectionKey.Description] = Resources.Resource.lblDescription;
                    sec[InventorySectionKey.ProductColors] = Resources.Resource.btnprodcolor;
                    sec[InventorySectionKey.ProducImages] = Resources.Resource.btnprodimages;
                    sec[InventorySectionKey.ProductQuantity] = Resources.Resource.lblSpacesAvailable;
                    sec[InventorySectionKey.ProductKit] = Resources.Resource.btnprodkit;
                    sec[InventorySectionKey.ProductsAssocaitions] = Resources.Resource.btnprodasso;
                    sec[InventorySectionKey.ProductSalesPrice] = Resources.Resource.btnprodsale;
                    sec[InventorySectionKey.AssociateVendors] = Resources.Resource.btnassovender;
                    sec[InventorySectionKey.Ammenities] = Resources.Resource.lblAmenities;
                    //sec[InventorySectionKey.ProductDiscount] = Resources.Resource.lblManageDiscount;
                    //sec[InventorySectionKey.ProductBlackOut] = Resources.Resource.lblManageBlackOut;
                    sec[InventorySectionKey.ViewProducts] = Resources.Resource.lblViewCourses;
                    break;
                case StatusProductType.ChildUnder12:
                default:
                    sec[InventorySectionKey.Details] = Resources.Resource.btnProductDetail;
                    sec[InventorySectionKey.Description] = Resources.Resource.btnproddesc;
                    sec[InventorySectionKey.ProductColors] = Resources.Resource.btnprodcolor;
                    sec[InventorySectionKey.ProducImages] = Resources.Resource.btnprodimages;
                    sec[InventorySectionKey.ProductQuantity] = Resources.Resource.lblQuantityPerWarehouse;
                    sec[InventorySectionKey.ProductKit] = Resources.Resource.btnprodkit;
                    sec[InventorySectionKey.ProductsAssocaitions] = Resources.Resource.btnprodasso;
                    sec[InventorySectionKey.ProductSalesPrice] = Resources.Resource.btnprodsale;
                    sec[InventorySectionKey.AssociateVendors] = Resources.Resource.btnassovender;
                    sec[InventorySectionKey.Ammenities] = Resources.Resource.lblAmenities;
                    //sec[InventorySectionKey.ProductDiscount] = Resources.Resource.lblManageDiscount;
                    //sec[InventorySectionKey.ProductBlackOut] = Resources.Resource.lblManageBlackOut;
                    sec[InventorySectionKey.ViewProducts] = Resources.Resource.btnviewprod;
                    break;
            }

            return sec;
        }
    }

    private Boolean IsResturant()
    {
        return BusinessUtility.GetBool(BusinessUtility.GetInt(System.Configuration.ConfigurationManager.AppSettings["IsResturant"]));
    }

    #region Web Methods
    [System.Web.Services.WebMethod]
    public static ProductQuantity GetProductQuantityObject(int id)
    {
        ProductQuantity prdQty = new ProductQuantity();
        prdQty.PopulateObject(null, id);
        return prdQty;
    }
    #endregion
}
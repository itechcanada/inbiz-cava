﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using Trirand.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;

using Newtonsoft.Json;


public partial class Inventory_mdApplyDiscountNew : BasePage
{
    ProductDiscount _disc = new ProductDiscount();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdDiscounts))
        {
            this.Initialize();
        }
    }       

    private int[] RoomIDs
    {
        get
        {
            string rooms = Request.QueryString["rooms"];
            if (!string.IsNullOrEmpty(rooms))
            {
                return StringUtil.GetArrayFromJoindString(",", rooms); 
            }
            return new List<int>().ToArray();
        }
    }

    private int FirstRoomID {
        get {
            return this.RoomIDs.Length > 0 ? this.RoomIDs[0] : 0;
        }
    }

    protected int ProductID {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["productID"], out id);
            return id;
        }
    }

    public void Initialize()
    {
        btnReset1.Style[HtmlTextWriterStyle.Display] = "none";
        btnReset.Style[HtmlTextWriterStyle.Display] = "none";
        btnReset3.Style[HtmlTextWriterStyle.Display] = "none";

        if (this.RoomIDs.Length > 0)//If multiple room id need to confirm override message!
        {
            string script = "$('.dicount_confirm').click(function (){";
            script += string.Format("return confirm('{0}');", Resources.Resource.msgOverrideAllDisocunt);
            script += "});";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "dicount_confirm", script, true);
        }

        rblDays.Items.Clear();
        rblDays.Items.Add(new ListItem(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[(int)DayOfWeek.Monday], "Mon"));
        rblDays.Items.Add(new ListItem(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[(int)DayOfWeek.Tuesday], "Tue"));
        rblDays.Items.Add(new ListItem(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[(int)DayOfWeek.Wednesday], "Wed"));
        rblDays.Items.Add(new ListItem(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[(int)DayOfWeek.Thursday], "Thu"));
        rblDays.Items.Add(new ListItem(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[(int)DayOfWeek.Friday], "Fri"));
        rblDays.Items.Add(new ListItem(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[(int)DayOfWeek.Saturday], "Sat"));
        rblDays.Items.Add(new ListItem(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[(int)DayOfWeek.Sunday], "Sun"));

        txtDiscount.Focus();
    }

    private List<ProductDiscountView> DiscountDataSource
    {
        get
        {
            if (Session["DiscountDataSource"] != null)
            {
                return (List<ProductDiscountView>)Session["DiscountDataSource"];
            }
            return new List<ProductDiscountView>();
        }
        set {
            Session["DiscountDataSource"] = value;
        }
    }

    private void SetGridDataSource()
    {
        if (this.ProductID > 0)
        {
            this.DiscountDataSource = new ProductDiscount().GetAllDiscountByBed(this.ProductID);
        }
        else
        {
            this.DiscountDataSource = new ProductDiscount().GetAllDiscountByRoom(this.FirstRoomID);
        }
        grdDiscounts.DataSource = this.DiscountDataSource;
    }

    protected void grdDiscounts_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        this.SetGridDataSource();   
    }

    //Apply discount By Date Range
    protected void btnApply_Click(object sender, EventArgs e)
    {
        DateTime startDate = BusinessUtility.GetDateTime(txtStartDate.Text, DateFormat.MMddyyyy);
        DateTime endDate = BusinessUtility.GetDateTime(txtEndDate.Text, DateFormat.MMddyyyy);

        if (startDate >= endDate)
        {
              MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPleaseSelectValidDateRange);
            return;
        }       
        
        try
        {
            List<ProductDiscountView> currentList =  this.DiscountDataSource;
            int editIdx = -1;
            int.TryParse(hdnID.Value, out editIdx);
            if (editIdx > 0)
            {
                ProductDiscountView dv = currentList.Where(disc => disc.RowIndex == editIdx).FirstOrDefault();
                dv.ApplyType = (DiscountApplyOn)BusinessUtility.GetInt(rblApplyOn.SelectedValue);
                dv.Day = string.Empty;
                dv.DiscountCategory = DiscountCategory.DateRange;
                dv.EndDate = endDate;
                dv.MinDays = BusinessUtility.GetInt(txtMinDays.Text);
                dv.PercentageDiscount = this.GetPercentageDiscountForDateRange(dv.MinDays);
                dv.RowIndex = this.DiscountDataSource.Count + 1;
                dv.Seq = 1;
                //dv.PackagePrice = 0;
                dv.PackagePrice = BusinessUtility.GetDouble(txtPackagePrice.Text);
                dv.StartDate = startDate;
            }
            else
            {
                ProductDiscountView dv = new ProductDiscountView();
                dv.ApplyType = (DiscountApplyOn)BusinessUtility.GetInt(rblApplyOn.SelectedValue);
                dv.Day = string.Empty;
                dv.DiscountCategory = DiscountCategory.DateRange;
                dv.EndDate = endDate;
                dv.MinDays = BusinessUtility.GetInt(txtMinDays.Text);
                dv.PercentageDiscount = this.GetPercentageDiscountForDateRange(dv.MinDays);
                dv.RowIndex = this.DiscountDataSource.Count + 1;
                dv.Seq = 1;
                dv.PackagePrice = BusinessUtility.GetDouble(txtPackagePrice.Text);
                dv.StartDate = startDate;

                currentList.Add(dv);
            }
            ProductDiscount pd = new ProductDiscount();
            if (this.ProductID > 0)
            {
                pd.ApplyDiscountToProduct(currentList, this.ProductID);
            }
            else
            {
                pd.ApplyDiscoountToRooms(currentList, this.RoomIDs);
            }

            Response.Redirect(Request.RawUrl, false);
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.DISCOUNT_DETAIL_ALREADYE_EXISTS)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgDiscountAlreadyExists);
            }
        }
    }

    private double GetPercentageDiscountForDateRange(int minDays)
    {
        if (txtDiscount.Text.Trim().Length > 0)
        {
            return BusinessUtility.GetDouble(txtDiscount.Text);
        }
        else if (txtDiscountAbsolute.Text.Trim().Length > 0)
        {
            double firstBedPrice = new Rooms().GetPrice(this.FirstRoomID);
            double absAmt = BusinessUtility.GetDouble(txtDiscountAbsolute.Text);
            //return Math.Round(absAmt * 100 / firstBedPrice, 2);
            return absAmt * 100 / firstBedPrice;
        }
        else if (txtFinalAmount.Text.Trim().Length > 0)
        {
            double firstBedPrice = new Rooms().GetPrice(this.FirstRoomID);
            double calculated = BusinessUtility.GetDouble(txtFinalAmount.Text);
            //return Math.Round((firstBedPrice - calculated) * 100 / firstBedPrice, 2);
            return (firstBedPrice - calculated) * 100 / firstBedPrice;
        }
        else if (txtPackagePrice.Text.Trim().Length > 0)
        {
            double firstBedPrice = new Rooms().GetPrice(this.FirstRoomID);
            double calculated = BusinessUtility.GetDouble(txtPackagePrice.Text) / (double)minDays;
            return (firstBedPrice - calculated) * 100.00D / firstBedPrice;
        }
        return 0.0D;
    }

    private double GetPercentageDiscountForDay(int minDays)
    {
        if (txtDiscount1.Text.Trim().Length > 0)
        {
            return BusinessUtility.GetDouble(txtDiscount1.Text);
        }
        else if (txtDiscountAbsolute1.Text.Trim().Length > 0)
        {
            double firstBedPrice = new Rooms().GetPrice(this.FirstRoomID);
            double absAmt = BusinessUtility.GetDouble(txtDiscountAbsolute1.Text);
            return absAmt * 100.00D / firstBedPrice;
        }
        else if (txtFianlAmount1.Text.Trim().Length > 0)
        {
            double firstBedPrice = new Rooms().GetPrice(this.FirstRoomID);
            double calculated = BusinessUtility.GetDouble(txtFianlAmount1.Text);
            return (firstBedPrice - calculated) * 100.00D / firstBedPrice;
        }
        else if (txtPackagePrice1.Text.Trim().Length > 0)
        {
            double firstBedPrice = new Rooms().GetPrice(this.FirstRoomID);
            double calculated = BusinessUtility.GetDouble(txtPackagePrice1.Text) / (double)minDays;
            return (firstBedPrice - calculated) * 100.00D / firstBedPrice;
        }        
        return 0.0D;
    }

    private double GetPercentageDiscountOnPackagePrice(int minDays)
    {
        if (minDays > 0)
        {
            double firstBedPrice = new Rooms().GetPrice(this.FirstRoomID);
            double calculated = BusinessUtility.GetDouble(txtPackagePrice3.Text) / (double)minDays;
            return (firstBedPrice - calculated) * 100.00D / firstBedPrice;
        }
        return 0.0D;
    }

    //Apply Discount By Day
    protected void btnApply1_Click(object sender, EventArgs e)
    {
        DateTime startDate = BusinessUtility.GetDateTime(txtStartDate1.Text, DateFormat.MMddyyyy);
        DateTime endDate = BusinessUtility.GetDateTime(txtEndDate1.Text, DateFormat.MMddyyyy);

        if (startDate >= endDate)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPleaseSelectValidDateRange);
            return;
        }

        try
        {
            List<ProductDiscountView> currentList = this.DiscountDataSource;
            int editIdx = -1;
            int.TryParse(hdnID.Value, out editIdx);
            if (editIdx > 0)
            {
                ProductDiscountView dv = currentList.Where(disc => disc.RowIndex == editIdx).FirstOrDefault();
                dv.ApplyType = (DiscountApplyOn)BusinessUtility.GetInt(rblApplyOn1.SelectedValue);
                dv.Day = rblDays.SelectedValue;
                dv.DiscountCategory = DiscountCategory.DaySpecial;
                dv.EndDate = endDate;
                dv.MinDays = BusinessUtility.GetInt(txtMinDays1.Text);
                dv.PercentageDiscount = this.GetPercentageDiscountForDay(dv.MinDays);
                dv.RowIndex = this.DiscountDataSource.Count + 1;
                dv.Seq = 1;
                //dv.PackagePrice = 0;
                dv.PackagePrice = BusinessUtility.GetDouble(txtPackagePrice1.Text);
                dv.StartDate = startDate;
            }
            else
            {
                ProductDiscountView dv = new ProductDiscountView();
                dv.ApplyType = (DiscountApplyOn)BusinessUtility.GetInt(rblApplyOn1.SelectedValue);
                dv.Day = rblDays.SelectedValue;
                dv.DiscountCategory = DiscountCategory.DaySpecial;
                dv.EndDate = endDate;
                dv.MinDays = BusinessUtility.GetInt(txtMinDays1.Text);
                dv.PercentageDiscount = this.GetPercentageDiscountForDay(dv.MinDays);
                dv.RowIndex = this.DiscountDataSource.Count + 1;
                dv.Seq = 1;
                dv.PackagePrice = BusinessUtility.GetDouble(txtPackagePrice1.Text);
                dv.StartDate = startDate;

                currentList.Add(dv);
            }
            ProductDiscount pd = new ProductDiscount();
            if (this.ProductID > 0)
            {
                pd.ApplyDiscountToProduct(currentList, this.ProductID);
            }
            else
            {
                pd.ApplyDiscoountToRooms(currentList, this.RoomIDs);
            }

            Response.Redirect(Request.RawUrl, false);
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.DISCOUNT_DETAIL_ALREADYE_EXISTS)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgDiscountAlreadyExists);
            }
        }
    }

    protected void btnApply3_Click(object sender, EventArgs e)
    {
        DateTime startDate = BusinessUtility.GetDateTime(txtStartDate3.Text, DateFormat.MMddyyyy);
        DateTime endDate = BusinessUtility.GetDateTime(txtEndDate3.Text, DateFormat.MMddyyyy);

        if (startDate >= endDate)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPleaseSelectValidDateRange);
            return;
        }

        try
        {
            List<ProductDiscountView> currentList = this.DiscountDataSource;
            int editIdx = -1;
            int.TryParse(hdnID.Value, out editIdx);
            if (editIdx > 0)
            {
                ProductDiscountView dv = currentList.Where(disc => disc.RowIndex == editIdx).FirstOrDefault();
                dv.ApplyType = DiscountApplyOn.DuringSelectedDateRangeOnly;  //(DiscountApplyOn)BusinessUtility.GetInt(rblApplyOn3.SelectedValue);
                dv.Day = string.Empty;
                dv.DiscountCategory = DiscountCategory.PackageSpecial;
                dv.EndDate = endDate;
                dv.MinDays = endDate.Subtract(startDate).Days;
                dv.PercentageDiscount = this.GetPercentageDiscountOnPackagePrice(dv.MinDays);
                dv.RowIndex = this.DiscountDataSource.Count + 1;
                dv.Seq = 1;
                dv.PackagePrice = BusinessUtility.GetDouble(txtPackagePrice3.Text);
                dv.StartDate = startDate;
            }
            else
            {
                ProductDiscountView dv = new ProductDiscountView();
                dv.ApplyType = (DiscountApplyOn)BusinessUtility.GetInt(rblApplyOn1.SelectedValue);
                dv.Day = string.Empty;
                dv.DiscountCategory = DiscountCategory.PackageSpecial;
                dv.EndDate = endDate;
                dv.MinDays = endDate.Subtract(startDate).Days;
                dv.PercentageDiscount = this.GetPercentageDiscountOnPackagePrice(dv.MinDays);
                dv.RowIndex = this.DiscountDataSource.Count + 1;
                dv.Seq = 1;
                dv.PackagePrice = BusinessUtility.GetDouble(txtPackagePrice3.Text);
                dv.StartDate = startDate;

                currentList.Add(dv);
            }
            ProductDiscount pd = new ProductDiscount();
            if (this.ProductID > 0)
            {
                pd.ApplyDiscountToProduct(currentList, this.ProductID);
            }
            else
            {
                pd.ApplyDiscoountToRooms(currentList, this.RoomIDs);
            }

            Response.Redirect(Request.RawUrl, false);
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.DISCOUNT_DETAIL_ALREADYE_EXISTS)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgDiscountAlreadyExists);
            }
        }
    }

    protected void grdDiscounts_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 2) //format discount type
        {
            if (e.CellHtml.Trim() == ((int)DiscountCategory.DateRange).ToString())
            {
                e.CellHtml = Resources.Resource.lblDateRange;
            }
            else if (e.CellHtml.Trim() == ((int)DiscountCategory.PackageSpecial).ToString())
            {
                e.CellHtml = Resources.Resource.lblPackageSpecial;
            }
            else
            {
                e.CellHtml = Resources.Resource.lblDaySpecial;
            }
        }
        else if (e.ColumnIndex == 8)
        {
            if (e.CellHtml.Trim() == ((int)DiscountApplyOn.AllYears).ToString())
            {
                e.CellHtml = Resources.Resource.lblAllYears;
            }
            else
            {
                e.CellHtml = Resources.Resource.lblDuringSelectedDateRange;
            }
        }
        else if (e.ColumnIndex == 9)
        {
            e.CellHtml = Resources.Resource.lblEdit;
        }
        else if (e.ColumnIndex == 10)
        {
            e.CellHtml = string.Format(@"<a href='javascript:;' onclick=""deleteItem('{0}')"">{1}</a>", e.RowKey, Resources.Resource.delete);
        }
    }
   
    protected void btnReset1_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl);
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl);
    }
    protected void grdDiscounts_DataRequested(object sender, JQGridDataRequestedEventArgs e)
    {
        //Session["AppliedDiscountCount"] = e.DataTable.Rows.Count;
    }

    protected string GetJsonArrayRoomID()
    {
        return JsonConvert.SerializeObject(this.RoomIDs);
    }

    #region Product Discount Helper
    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static ProductDiscountView GetProductDiscountItem(int itemID)
    {
        if (HttpContext.Current.Session["DiscountDataSource"] != null)
        {
            List<ProductDiscountView> lst = (List<ProductDiscountView>)HttpContext.Current.Session["DiscountDataSource"];
            var resutl = lst.Where(disc => disc.RowIndex == itemID);
            return resutl.FirstOrDefault();
        }
        return new ProductDiscountView();
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static bool DeleteDiscountItem(int itemID, int[] roomIds, int productID)
    {
        try
        {
            if (HttpContext.Current.Session["DiscountDataSource"] != null)
            {
                List<ProductDiscountView> lst = (List<ProductDiscountView>)HttpContext.Current.Session["DiscountDataSource"];
                var result = lst.Where(disc => disc.RowIndex == itemID);
                var itm = result.FirstOrDefault();
                lst.Remove(itm);

                if (productID > 0)
                {
                    new ProductDiscount().ApplyDiscountToProduct(lst, productID);
                }
                else
                {
                    new ProductDiscount().ApplyDiscoountToRooms(lst, roomIds);
                }
            }

            return true;
        }
        catch
        {
            return false;
        }
    }
    #endregion    
}
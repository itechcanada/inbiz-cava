﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_BuildingEdit : BasePage
{
    Buildings _categ = new Buildings();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            BindData();
            txtCategoryEn.Focus();
        }        
    }

    private void BindData()
    {       
     
        if (this.BuildingID > 0)
        {
            _categ.PopulateObject(this.BuildingID);           
            
            txtCategoryEn.Text = _categ.BuildingTitleEn;
            txtCategoryFr.Text = _categ.BuildingTitleEn;
        }        
    }

    private void SetData() {
        _categ.BuildingID = this.BuildingID;
        _categ.BuildingTitleEn = txtCategoryEn.Text;
        _categ.BuildingTitleFr = txtCategoryFr.Text;
        _categ.IsActive = true;        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid) {
            SetData();
            if (this.BuildingID > 0) {
                _categ.Update();
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgRecordUpdatedSuccessfully);
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                }
                else
                {
                    Globals.RegisterCloseDialogScript(Page);
                }    
            }
            else
            {
                _categ.Insert();
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgRecordAddedSuccess);
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                }
                else
                {
                    Globals.RegisterCloseDialogScript(Page);
                }  
            }
        }
    }

    private int BuildingID {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["catid"], out id);
            return id;
        }
    }

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Inventory_AddMaterialGroup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MessageState.SetGlobalMessage(MessageType.Success, "");
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {

                //if (BusinessUtility.GetString(Request.QueryString["Status"]) == "INDIVIDUAL")
                {
                    //trIsAxctive.Visible = true;
                    if (BusinessUtility.GetInt(Request.QueryString["MaterialGroupID"]) > 0)
                    {
                        ProductMaterialGroup objProductMaterial = new ProductMaterialGroup();
                        objProductMaterial.MaterialGroupID = BusinessUtility.GetInt(Request.QueryString["MaterialGroupID"]);
                        var lst = objProductMaterial.GetAllMaterialGroupList(null, Globals.CurrentAppLanguageCode);
                        foreach (var item in lst)
                        {
                            txtMaterialNameEn.Text = item.MaterialGroupNameEn;
                            txtMaterialNameFr.Text = item.MaterialGroupNameFr;
                            //txtShortName.Text = item.ShortName;
                            rblstActive.SelectedValue = BusinessUtility.GetBool(item.MaterialGroupIsActive) ? "1" : "0";
                        }

                    }
                }
                txtMaterialNameFr.Focus();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            ProductMaterialGroup objProductMaterial = new ProductMaterialGroup();
            objProductMaterial.MaterialGroupNameEn = txtMaterialNameEn.Text;
            objProductMaterial.MaterialGroupNameFr = txtMaterialNameFr.Text;
            //objCollection.ShortName = txtShortName.Text;
            objProductMaterial.MaterialGroupIsActive = BusinessUtility.GetInt(rblstActive.SelectedItem.Value);
            //objProductMaterial.MaterialID = BusinessUtility.GetInt(Request.QueryString["MaterialID"]);
            if (objProductMaterial.SaveMaterialGroup(null, CurrentUser.UserID, BusinessUtility.GetInt(Request.QueryString["MaterialGroupID"])) == true)
            {
                if (BusinessUtility.GetInt(Request.QueryString["MaterialGroupID"]) > 0)
                {
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgMaterialGroupUpdate);
                    Globals.RegisterReloadParentScript(this);
                    Globals.RegisterCloseDialogScript(this);
                    return;
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgMaterialGroupCreate);
                    Globals.RegisterReloadParentScript(this);
                    Globals.RegisterCloseDialogScript(this);
                    return;
                }
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadStyle({0});", objProductMaterial.MaterialGroupID), true);
            }
            else
            {
                //MessageState.SetGlobalMessage(MessageType.Failure, "Collection Creation failure");
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }

        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
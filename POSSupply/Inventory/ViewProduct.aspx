﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeFile="ViewProduct.aspx.cs" Inherits="Inventory_New_ViewProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <asp:Literal ID="ltTitle" Text="<%$Resources:Resource, lblInvViewProduct %>" runat="server" />
        </b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <asp:Button ID="btnAdd" runat="server" Text="<%$Resources:Resource, lblAddNewProduct %>"
            OnClick="btnAdd_Click" />
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif"
            CssClass="csv_export" />
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        <%= Resources.Resource.lblSearchForm%></div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="Label1" runat="server" CssClass="filter-key" AssociatedControlID="dlSearch"
                        Text="<%$ Resources:Resource, lblSearchBy%>"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:DropDownList ID="dlSearch" runat="server" ValidationGroup="PrdSrch" Width="175px">
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" CssClass="filter-key" AssociatedControlID="txtSearch"
                        runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
                </div>
                <h4>
                    <asp:Label ID="Label2" CssClass="filter-key" AssociatedControlID="txtStyle" runat="server"
                        Text="<%$ Resources:Resource, lblStyle %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtStyle" CssClass="filter-key"></asp:TextBox>
                </div>
                <h4>
                    <asp:Label ID="Label3" CssClass="filter-key" AssociatedControlID="ddlCollectionSearch"
                        runat="server" Text="<%$ Resources:Resource, lblCollection %>"></asp:Label></h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlCollectionSearch" runat="server" CssClass="filter-key">
                        <asp:ListItem Text="All" Value="" Selected="True" />
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="Label4" CssClass="filter-key" AssociatedControlID="ddlMaterialSearch"
                        runat="server" Text="<%$ Resources:Resource, lblMaterial %>"></asp:Label></h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlMaterialSearch" runat="server" CssClass="filter-key">
                        <asp:ListItem Text="All" Value="" Selected="True" />
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="Label5" CssClass="filter-key" AssociatedControlID="lstColor" runat="server"
                        Text="<%$ Resources:Resource, lblPrdColor %>"></asp:Label></h4>
                <div class="inner">
                <asp:ListBox CssClass="modernized_select" ID="lstColor" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblPrdColor  %>"></asp:ListBox>
                    <%--<asp:ListBox ID="lstColor" runat="server" SelectionMode="Multiple" CssClass="filter-key">
                    </asp:ListBox>--%>
                </div>
                <h4>
                    <asp:Label ID="Label6" CssClass="filter-key" AssociatedControlID="lstSizeGroup" runat="server"
                        Text="<%$ Resources:Resource, lblGrdSizeGroup %>"></asp:Label></h4>
                <div class="inner">
                    <asp:ListBox ID="lstSizeGroup" runat="server" SelectionMode="Multiple" CssClass="filter-key">
                    </asp:ListBox>
                </div>
                
<%--                &nbsp;<asp:LinkButton ID="lnbAdvanceSearch" runat="server" Text="<%$ Resources:Resource, lblAdvanceProductSearch %>"></asp:LinkButton>
                <iCtrl:IframeDialog ID="mdWareHouse" TriggerControlID="lnbAdvanceSearch" Width="600"
                    Height="520" Url="ProductAdvanceSearch.aspx" Title="<%$ Resources:Resource, lblAdvanceProductSearch %>"
                    runat="server">
                </iCtrl:IframeDialog>--%>
                                <h4>
                    <asp:Label ID="Label7" CssClass="filter-key" AssociatedControlID="lstProdCatg" runat="server"
                        Text="<%$ Resources:Resource, lblGrdCategory %>"></asp:Label></h4>
                <div class="inner">
                <asp:ListBox CssClass="modernized_select" ID="lstProdCatg" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblGrdCategory  %>"></asp:ListBox>
                    <%--<asp:ListBox ID="lstProdCatg" runat="server" SelectionMode="Multiple" CssClass="filter-key">
                    </asp:ListBox>--%>
                </div>
                <%--                <h4>
                    <asp:Label ID="Label8" CssClass="filter-key" AssociatedControlID="lstProdKeyWord"
                        runat="server" Text="<%$ Resources:Resource, lblKeyword %>"></asp:Label></h4>
                <div class="inner">
                    <asp:ListBox ID="lstProdKeyWord" runat="server" CssClass="filter-key"></asp:ListBox>
                </div>--%>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%= Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="gvProducts" DataSourceID="sdsPropducts" Height="300px"
                    AutoWidth="True" OnCellBinding="gvProducts_CellBinding" OnDataRequesting="gvProducts_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="ProductID" Visible="false" PrimaryKey="True" />
                        <trirand:JQGridColumn DataField="prdName" HeaderText="<%$ Resources:Resource, grdvProductName %>"
                            Editable="false" Width="200" />
                        <%--<trirand:JQGridColumn DataField="ProductID" HeaderText="<%$ Resources:Resource, grdvVendor %>"
                            Editable="false">
                        </trirand:JQGridColumn>--%>
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdvOnHandQty %>" DataField="prdOhdQty"
                            Sortable="true" TextAlign="Center" />
                        <%--<trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdvDefectiveQty %>" DataField="prdDefectiveQty"
                            Sortable="true" TextAlign="Center" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdvAvailableQty %>" DataField="AvailableQty"
                            Sortable="true" TextAlign="Center" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdvInTransit %>" DataField="ProductID"
                            Sortable="true" TextAlign="Center" />--%>
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdvUPCCode %>" DataField="prdUPCCode"
                            Sortable="true" TextAlign="Center" />
                        <%--<trirand:JQGridColumn HeaderText="Building" DataField="Building" Sortable="true"
                            TextAlign="Center" Visible="false" />
                        <trirand:JQGridColumn HeaderText="Room" DataField="Room" Sortable="true" TextAlign="Center"
                            Visible="false" />--%>
                       <%-- <trirand:JQGridColumn HeaderText="Room Type" DataField="RoomType" Sortable="true"
                            TextAlign="Center" Visible="false" />--%>
                       <%-- <trirand:JQGridColumn HeaderText="Amenities" DataField="ProductID" Sortable="true"
                            TextAlign="Center" Visible="false" />--%>
                        <%--<trirand:JQGridColumn HeaderText="Price" DataField="prdEndUserSalesPrice" DataFormatString="{0:F}"
                            Sortable="true" TextAlign="Center" Visible="false" />--%>
                        <trirand:JQGridColumn HeaderText="Is Active" DataField="prdIsActive" Sortable="true"
                            TextAlign="Center" Width="70" />
                       <%-- <trirand:JQGridColumn DataField="prdSeq" HeaderText="<%$ Resources:Resource, lblSeq %>"
                            Editable="true" Width="50" TextAlign="Center" Visible="false" />--%>
                        <%--<trirand:JQGridColumn HeaderText="<%$ Resources:Resource, lblPhotographAdded %>"
                            DataField="ProductID" Sortable="false" TextAlign="Center" Width="150" Visible="false" />--%>
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdvEdit %>" DataField="ProductID"
                            Sortable="false" TextAlign="Center" Width="60" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="ProductID"
                            Sortable="false" TextAlign="Center" Width="60" Visible="false" />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents RowSelect="rowSelect" LoadComplete="loadComplete" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sdsPropducts" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
                <iCtrl:IframeDialog ID="mdDeleteUser" Width="400" Height="120" Title="Delete" Dragable="true"
                    TriggerSelectorClass="pop_delete" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
                    runat="server">
                </iCtrl:IframeDialog>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
    <script type="text/javascript">
        $grid = $("#<%=gvProducts.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=gvProducts.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        function loadComplete(data) {
            if (data.rows.length == 1 && data.total == 1) {
                location.href = 'Product.aspx?PrdID=' + data.rows[0].id + '&Kit=False&ptype=<%=Request.QueryString["ptype"]%>';
            }
            else {
                jqGridResize();
            }
        }

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function rowSelect(id) {
            //alert(jQuery("#" + gridID).getGridParam('selarrrow'));

        }

        function deleteRow(rowID) {
            // Get the currently selected row
            $('#' + gridID).setSelection(rowID);
            var rowKey = $('#' + gridID).getGridParam("selrow");
            $('#' + gridID).jqGrid('delGridRow', rowKey, {
                errorTextFormat: function (data) {
                    if (data.responseText.substr(0, 6) == "<html ") {
                        return jQuery(data.responseText).html();
                    }
                    else {
                        return "<%=Resources.Resource.msgProductCanNotBeDeleted%>";
                        // or
                        // return "Status: '" + data.statusText + "'. Error code: " +data.status;
                    }
                }
            });
        }

        function editSeq(source) {
            var grid = $("#<%=gvProducts.ClientID %>");
            grid.setSelection($(source).attr("row-key"));
            var dataToPost = {};
            dataToPost["editSeq"] = 1;
            dataToPost["editRowKey"] = $(source).attr("row-key");
            dataToPost["seq"] = $(source).val();

            for (var k in dataToPost) {
                grid.setPostDataItem(k, dataToPost[k]);
            }
            grid.trigger("reloadGrid");

            for (var k in dataToPost) {
                grid.removePostDataItem(k);
            }
        }

        $(document).ready(function () {
            loadSearchControl();
        });

        function loadSearchControl() {
            ReloadSearchConnectionList();
            ReloadSearchMaterialList();
//            ReloadSearchColorList();
            ReloadSearchCatgSizeList();
//            ReloadSearchCatgList();
        }

        function ReloadSearchConnectionList() {
            $.ajax({
                type: "POST",
                url: "productMaster.aspx/GetCollectionList",
                dataType: "json",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var addressList = data.d;
                    $('option', $("#<%=ddlCollectionSearch.ClientID%>")).remove();
                    $("#<%=ddlCollectionSearch.ClientID%>").append($("<option />").val("").text("<%=Resources.Resource.lblAll%>"));
                    for (var i = 0; i < addressList.length; i++) {
                        $("#<%=ddlCollectionSearch.ClientID%>").append($("<option />").val(addressList[i].CollectionID).text(addressList[i].ShortName));
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }

        function ReloadSearchMaterialList() {
            $.ajax({
                type: "POST",
                url: "productMaster.aspx/GetMaterialList",
                dataType: "json",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var addressList = data.d;
                    $('option', $("#<%=ddlMaterialSearch.ClientID%>")).remove();
                    $("#<%=ddlMaterialSearch.ClientID%>").append($("<option />").val("").text("<%=Resources.Resource.lblAll%>"));
                    for (var i = 0; i < addressList.length; i++) {
                        //$("#<%=ddlMaterialSearch.ClientID%>").append($("<option />").val(addressList[i].MaterialID).text(addressList[i].MaterialShortName));
                        $("#<%=ddlMaterialSearch.ClientID%>").append($("<option />").val(addressList[i].MaterialID).text(addressList[i].MaterialName));
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }

//        function ReloadSearchColorList() {
//            $.ajax({
//                type: "POST",
//                url: "productMaster.aspx/GetColorList",
//                dataType: "json",
//                data: "{}",
//                contentType: "application/json; charset=utf-8",
//                success: function (data) {
//                    var addressList = data.d;
//                    $('option', $("#<%=lstColor.ClientID%>")).remove();
//                    $("#<%=lstColor.ClientID%>").append($("<option />").val("0").text("<%=Resources.Resource.lblAll%>"));
//                    var sOption = '';
//                    for (var i = 0; i < addressList.length; i++) {
//                        //$("#<%=lstColor.ClientID%>").append($("<option />").val(addressList[i].ColorID).text(addressList[i].ShortName));
//                        $("#<%=lstColor.ClientID%>").append($("<option />").val(addressList[i].ColorID).text(addressList[i].ColorName));
//                    }
//                },
//                error: function (XMLHttpRequest, textStatus, errorThrown) {
//                }
//            });
//        }

        function ReloadSearchCatgSizeList() {
            $.ajax({
                type: "POST",
                url: "productMaster.aspx/GetCategorySizeList",
                dataType: "json",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var sSelGroup = '';
                    var addressList = data.d;
                    $('option', $("#<%=lstSizeGroup.ClientID%>")).remove();
                    $("#<%=lstSizeGroup.ClientID%>").append($("<option />").val("0").text("<%=Resources.Resource.lblAll%>"));
                    var sOption = '';
                    for (var i = 0; i < addressList.length; i++) {
                        var sGroupName = addressList[i].CategoryName;
                        if (sSelGroup == "") {
                            sSelGroup = sGroupName;
                            $("#<%=lstSizeGroup.ClientID%>").append($("<option />").val(addressList[i].CategoryName).text(addressList[i].CategoryName));
                        }
                        else if (sSelGroup != sGroupName) {
                            sSelGroup = sGroupName;
                            $("#<%=lstSizeGroup.ClientID%>").append($("<option />").val(addressList[i].CategoryName).text(addressList[i].CategoryName));
                        }

                        //$("#<%=lstSizeGroup.ClientID%>").append($("<option />").val(addressList[i].CategoryName).text(addressList[i].CategoryName));
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }

//        function ReloadSearchCatgList() {
//            $.ajax({
//                type: "POST",
//                url: "productMaster.aspx/GetProdCategoryList",
//                dataType: "json",
//                data: "{}",
//                contentType: "application/json; charset=utf-8",
//                success: function (data) {
//                    var sSelGroup = '';
//                    var addressList = data.d;
//                    $('option', $("#<%=lstProdCatg.ClientID%>")).remove();
//                    //                    $("#<%=lstProdCatg.ClientID%>").append($("<option />").val("").text("<%=Resources.Resource.lblAll%>"));
//                    var sOption = '';
//                    for (var i = 0; i < addressList.length; i++) {
//                        $("#<%=lstProdCatg.ClientID%>").append($("<option />").val(addressList[i].CatgValue).text(addressList[i].CatgName));
//                        //$("#<%=lstSizeGroup.ClientID%>").append($("<option />").val(addressList[i].CategoryName).text(addressList[i].CategoryName));
//                    }
//                },
//                error: function (XMLHttpRequest, textStatus, errorThrown) {
//                }
//            });
//        }

//        function AdvanceSearch(sWebSite) {
//            alert(sWebSite);
//        }

    </script>
</asp:Content>

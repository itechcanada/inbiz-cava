<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="ProductDescription.aspx.vb" Inherits="Inventory_ProductDescription" %>

<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>

<%@ Register src="../Controls/CommonSearch/InventorySearch.ascx" tagname="InventorySearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">       
        $('#divMainContainerTitle').corner();        
    </script>
        
    <uc1:InventorySearch ID="InventorySearch1" runat="server" />
        
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" Text="<%$ Resources:Resource, lblPrdDescription %>"
                                ID="lblHeading"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
    </div>
    <div class="divMainContent">
        <asp:UpdatePanel runat="server" ID="udpnlViewPrd">
            <ContentTemplate>
                <asp:Panel runat="server" ID="Panel14">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblMsg" runat="server" Font-Bold="true" Text="<%$ Resources:Resource, lblPrdDescriptionMsg %>"
                                        ForeColor="green"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblProdName" runat="server" Text="<%$ Resources:Resource, lblPrdDescName %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtProdName" runat="server" CssClass="innerText" Width="330px" MaxLength="250"></asp:TextBox><span
                                        class="style1"> *</span>
                                    <asp:RequiredFieldValidator ID="reqvalProdName" runat="server" Display="None" SetFocusOnError="true"
                                        ErrorMessage="<%$ Resources:Resource, reqvalPrdName %>" ControlToValidate="txtProdName"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                            <tr>
                                <td height="5">
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign" width="18%">
                                    <asp:Label ID="lblProdDescr" runat="server" Text="<%$ Resources:Resource, lblDescription %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td width="1%">
                                </td>
                                <td align="left" width="81%">
                                    <FTB:FreeTextBox ID="txtProdDescr" runat="server" Width="540px" AutoConfigure="Alternate"
                                        ToolbarBackgroundImage="false" ToolbarBackColor="#D2D2D5" GutterBackColor="#D2D2D5"
                                        BackColor="#D2D2D5" Height="150">
                                    </FTB:FreeTextBox>
                                    <asp:RequiredFieldValidator ID="reqvalProdDescr" runat="server" Display="None" SetFocusOnError="true"
                                        ErrorMessage="<%$ Resources:Resource, reqvalPrdDescr %>" ControlToValidate="txtProdDescr"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td height="5">
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lblPrdTechDescription %>"
                                        CssClass="lblBold"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <FTB:FreeTextBox ID="txtProdTechDescr" runat="server" Width="540px" AutoConfigure="Alternate"
                                        ToolbarBackgroundImage="false" ToolbarBackColor="#D2D2D5" GutterBackColor="#D2D2D5"
                                        BackColor="#D2D2D5" Height="150">
                                    </FTB:FreeTextBox>
                                </td>
                            </tr>
                            <asp:ValidationSummary ID="sumvalDesc" runat="server" ShowSummary="false" ShowMessageBox="true">
                            </asp:ValidationSummary>
                            <tr>
                                <td height="5">
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <div class="buttonwrapper">
                                        <a id="imgCmdSave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                            style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssSubmit%></span></a></div>
                                </td>
                            </tr>
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
    </div>
</asp:Content>

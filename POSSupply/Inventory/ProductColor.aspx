<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="ProductColor.aspx.vb" Inherits="Inventory_ProductColor" %>

<%@ Register src="../Controls/CommonSearch/InventorySearch.ascx" tagname="InventorySearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>
    
    <uc1:InventorySearch ID="InventorySearch1" runat="server" />
    
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" Text="<%$ Resources:Resource, lblPrdAddColor %>"
                                ID="lblHeading"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <asp:UpdatePanel runat="server" ID="udpnlPrdColor">
            <ContentTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">                    
                    <tr height="25">
                        <td class="tdAlign" width="35%">
                            <asp:Label ID="Label1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblPrdColor %>"></asp:Label>
                        </td>
                        <td width="1%">
                        </td>
                        <td align="left" width="64%">
                            <asp:CheckBoxList ID="chklstColor" runat="server" RepeatColumns="4" RepeatDirection="Horizontal">
                            </asp:CheckBoxList>
                            <asp:DropDownList ID="dlColor" runat="server" CssClass="innerText" Width="336px"
                                Visible="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <table width="100%">
                                <tr>
                                    <td width="25%">
                                        <div class="buttonwrapper">
                                            <a id="imgCmdSave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                                style="min-width: 120px; text-align: center;">
                                                <%=Resources.Resource.cmdCssSubmit%></span></a></div>
                                    </td>
                                    <td width="75%" align="left">
                                        <div class="buttonwrapper">
                                            <a id="imgCmdReset" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                                <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                    <%=Resources.Resource.cmdCssReset%></span></a></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-right: 15px" align="right" colspan="3">
                            <div style="height: 22px">
                                <asp:UpdateProgress ID="Updateprogress" runat="server" DisplayAfter="10" AssociatedUpdatePanelID="udpnlPrdColor">
                                    <ProgressTemplate>
                                        <img src="../Images/wait22trans.gif" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td align="center" colspan="3">
                            <asp:GridView ID="grdvPrdColor" runat="server" AllowSorting="True" DataSourceID="sqlsdPrdColor"
                                AllowPaging="True" PageSize="10" PagerSettings-Mode="Numeric" CellPadding="0"
                                GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
                                CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="prdColiCode"
                                Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="colDesc" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, grdvPrdColor %>"
                                        ReadOnly="True" SortExpression="colDesc">
                                        <ItemStyle Width="600px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdvDelete %>" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-ForeColor="white">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                                ToolTip="<%$ Resources:Resource, grdvDelete %>" CommandArgument='<%# Eval("prdColiCode") %>'
                                                CommandName="Delete" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="grid_footer" />
                                <RowStyle CssClass="grid_rowstyle" />
                                <PagerStyle CssClass="grid_footer" />
                                <HeaderStyle CssClass="grid_header" Height="26px" />
                                <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                <PagerSettings PageButtonCount="20" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="sqlsdPrdColor" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <asp:ValidationSummary ID="sumvalPrdColor" runat="server" ShowMessageBox="true" ShowSummary="false">
                    </asp:ValidationSummary>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
    </div>
    <script type="text/javascript" language="javascript">
 function funSelectColor(source, arguments)
     {  
        for(int i=0;i<window.document.getElementById('<%=chklstColor.ClientID%>').Items.Count;i++)
        {
            if (window.document.getElementById('<%=chklstColor.ClientID%>')Items(i).Selected )
            {
                arguments.IsValid=true;
   		    }
            arguments.IsValid=false;
        }
	 }
    </script>
</asp:Content>

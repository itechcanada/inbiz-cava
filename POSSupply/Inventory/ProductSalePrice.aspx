<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="ProductSalePrice.aspx.vb" Inherits="Inventory_ProductSalePrice" %>

<%@ Register src="../Controls/CommonSearch/InventorySearch.ascx" tagname="InventorySearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>
   
    <uc1:InventorySearch ID="InventorySearch1" runat="server" />
   
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" Text="<%$ Resources:Resource, lblPrdProductSalePrice %>"
                                ID="lblHeading"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
    </div>
    <div class="divMainContent">
        <asp:UpdatePanel runat="server" ID="udpnlPrdSalePrice">
            <ContentTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td height="10">
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                            <asp:Label ID="lblSalePrice" Text="<%$ Resources:Resource, lblPrdSalePrice %>" CssClass="lblBold"
                                runat="server" />
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtSalePrice" CssClass="innerText" runat="server" Width="330Px" /><span
                                class="style1"> *</span>
                            <asp:RequiredFieldValidator ID="reqvalSalePrice" runat="server" ControlToValidate="txtSalePrice"
                                ErrorMessage="<%$ Resources:Resource, reqvalSalePrice %>" SetFocusOnError="true"
                                Display="None"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="comvalSalePrice" EnableClientScript="true" SetFocusOnError="true"
                                ControlToValidate="txtSalePrice" Operator="DataTypeCheck" Type="Double" Display="None"
                                ErrorMessage="<%$ Resources:Resource, comvalSalePrice %>" runat="server" />
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign" width="35%">
                            <asp:Label ID="lblFromQty" Text="<%$ Resources:Resource, lblPrdFromQuantity %>" CssClass="lblBold"
                                runat="server" />
                        </td>
                        <td width="1%">
                        </td>
                        <td align="left" width="64%">
                            <asp:TextBox ID="txtFromQty" CssClass="innerText" runat="server" Width="330Px" /><span
                                class="style1"> *</span>
                            <asp:RequiredFieldValidator ID="reqvalFromQty" runat="server" ControlToValidate="txtFromQty"
                                ErrorMessage="<%$ Resources:Resource, reqvalFromQty %>" SetFocusOnError="true"
                                Display="None"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="compvalFromQty" EnableClientScript="true" SetFocusOnError="true"
                                ControlToValidate="txtFromQty" Operator="DataTypeCheck" Type="Double" Display="None"
                                ErrorMessage="<%$ Resources:Resource, compvalFromQty %>" runat="server" />
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                            <asp:Label ID="lblToQty" Text="<%$ Resources:Resource, lblPrdToQuantity %>" CssClass="lblBold"
                                runat="server" />
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtToQty" CssClass="innerText" runat="server" Width="330Px" /><span
                                class="style1"> *</span>
                            <asp:RequiredFieldValidator ID="reqvalToQty" runat="server" ControlToValidate="txtToQty"
                                ErrorMessage="<%$ Resources:Resource, reqvalToQty %>" SetFocusOnError="true"
                                Display="None"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="comvalToQty" EnableClientScript="true" SetFocusOnError="true"
                                ControlToValidate="txtToQty" Operator="DataTypeCheck" Type="Double" Display="None"
                                ErrorMessage="<%$ Resources:Resource, comvalToQty %>" runat="server" />
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                            <asp:Label ID="lblRushSalePrice" Text="<%$ Resources:Resource, lblPrdRushSalePrice %>"
                                CssClass="lblBold" runat="server" />
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtRushSalePrice" CssClass="innerText" runat="server" Width="330Px" />
                            <asp:CompareValidator ID="comvalRushSalePrice" EnableClientScript="true" SetFocusOnError="true"
                                ControlToValidate="txtRushSalePrice" Operator="DataTypeCheck" Type="Double" Display="None"
                                ErrorMessage="<%$ Resources:Resource, comvalRushSalePrice %>" runat="server" />
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                            <asp:Label ID="lblFromRushQty" Text="<%$ Resources:Resource, lblPrdFromRushQuantity %>"
                                CssClass="lblBold" runat="server" />
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtFromRushQty" CssClass="innerText" runat="server" Width="330Px" />
                            <asp:CompareValidator ID="comvalFromRushQty" EnableClientScript="true" SetFocusOnError="true"
                                ControlToValidate="txtFromRushQty" Operator="DataTypeCheck" Type="Double" Display="None"
                                ErrorMessage="<%$ Resources:Resource, comvalFromRushQty %>" runat="server" />
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                            <asp:Label ID="lblToRushQuantity" Text="<%$ Resources:Resource, lblPrdToRushQuantity %>"
                                CssClass="lblBold" runat="server" />
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtToRushQuantity" CssClass="innerText" runat="server" Width="330Px" />
                            <asp:CompareValidator ID="comvalToRushQuantity" EnableClientScript="true" SetFocusOnError="true"
                                ControlToValidate="txtToRushQuantity" Operator="DataTypeCheck" Type="Double"
                                Display="None" ErrorMessage="<%$ Resources:Resource, comvalToRushQuantity %>"
                                runat="server" />
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                        </td>
                        <td>
                            <asp:ValidationSummary ID="sumvalsalePriceProduct" runat="server" ShowMessageBox="true"
                                ShowSummary="false" />
                        </td>
                        <td align="left">
                            <table width="100%">
                                <tr>
                                    <td width="25%">
                                        <div class="buttonwrapper">
                                            <a id="imgCmdSave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                                style="min-width: 120px; text-align: center;">
                                                <%=Resources.Resource.cmdCssSubmit%></span></a></div>
                                    </td>
                                    <td width="75%" align="left">
                                        <div class="buttonwrapper">
                                            <a id="imgCmdReset" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                                <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                    <%=Resources.Resource.cmdCssReset%></span></a></div>
                                    </td>
                                </tr>
                            </table>
                            <%-- <asp:ImageButton runat=server ID=imgCmdSave ImageUrl="~/images/submit.png"  />
            <asp:ImageButton runat=server CausesValidation=false Visible=false ID=imgCmdReset ImageUrl="~/images/Reset.png" />                --%>
                        </td>
                    </tr>
                    <tr>
                        <td height="20">
                        </td>
                    </tr>
                    <tr valign="top">
                        <td align="center" colspan="3">
                            <asp:GridView ID="gvwProductSalePrice" runat="server" AllowSorting="True" DataSourceID="sdsProductSalesPrice"
                                AllowPaging="True" PageSize="10" PagerSettings-Mode="Numeric" CellPadding="0"
                                GridLines="none" AutoGenerateColumns="False" UseAccessibleHeader="False" Style="border-collapse: separate;"
                                CssClass="view_grid650" DataKeyNames="id" Width="100%" 
                                CausesValidation="false">
                                <Columns>
                                    <asp:BoundField DataField="fromQty" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false"
                                        HeaderText="<%$ Resources:Resource, gvwFromQty %>" ReadOnly="True" SortExpression="fromQty">
                                        <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="toQty" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false"
                                        HeaderText="<%$ Resources:Resource, gvwToQty %>" ReadOnly="True" SortExpression="toQty">
                                        <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="salesPrice" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false"
                                        HeaderText="<%$ Resources:Resource, gvwSalesPrice %>" ReadOnly="True" SortExpression="salesPrice">
                                        <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="fromRushQty" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false"
                                        HeaderText="<%$ Resources:Resource, gvwRushFromQty %>" ReadOnly="True" SortExpression="fromRushQty">
                                        <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="toRushQty" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false"
                                        HeaderText="<%$ Resources:Resource, gvwRushToQty %>" ReadOnly="True" SortExpression="toRushQty">
                                        <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="rushSalesPrice" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Wrap="false"
                                        HeaderText="<%$ Resources:Resource, gvwRushSalesPrice %>" ReadOnly="True" SortExpression="rushSalesPrice">
                                        <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdvDelete %>" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-ForeColor="white">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                                ToolTip="<%$ Resources:Resource, grdvDelete %>" CommandArgument='<%# Eval("id") %>'
                                                CommandName="Delete" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="grid_footer" />
                                <RowStyle CssClass="grid_rowstyle" />
                                <PagerStyle CssClass="grid_footer" />
                                <HeaderStyle CssClass="grid_header" Height="26px" />
                                <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                <PagerSettings PageButtonCount="20" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="sdsProductSalesPrice" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td height="20">
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
    </div>
</asp:Content>

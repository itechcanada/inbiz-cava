<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="AssignPrd.aspx.vb" Inherits="Inventory_AssignPrd" EnableEventValidation="false" %>

<%@ Register src="../Controls/CommonSearch/InventorySearch.ascx" tagname="InventorySearch" tagprefix="uc1" %>
<%@ Register src="../Controls/CommonSearch/CommonKeyword.ascx" tagname="CommonKeyword" tagprefix="uc2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>

    <uc2:CommonKeyword ID="CommonKeyword1" runat="server" />

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" Text="Assign Product" ID="lblHeading"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr id="trAllControl" runat="server">
                <td align="left">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Label runat="server" ID="lblErrorMsg" Style="color: Red;" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Label runat="server" ID="lblMsg" CssClass="lblBold" Style="color: Green;"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="tdAlign" width="30%">
                                <asp:Label ID="lblSubcatCatName" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, Category%>"></asp:Label>
                            </td>
                            <td align="left" width="70%">
                                <asp:DropDownList ID="dlCatName" AutoPostBack="true" Width="270px" runat="server">
                                </asp:DropDownList>
                                <span class="style1">*</span>
                                <asp:CustomValidator ID="custvalCatName" runat="server" ClientValidationFunction="funSelectCategory"
                                    Display="None" ErrorMessage="<%$ Resources:Resource, msgPleaseSelectCategoryName%>"
                                    SetFocusOnError="true"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="tdAlign" width="30%">
                                <asp:Label ID="lblsubCategory" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, SubCategory%>"></asp:Label>
                            </td>
                            <td align="left" width="70%">
                                <asp:DropDownList ID="dlSubcatName" AutoPostBack="true" Width="270px" runat="server">
                                </asp:DropDownList>
                                <span class="style1">*</span>
                                <asp:CustomValidator ID="custvalSubcatName" runat="server" ClientValidationFunction="funSelectSubcategory"
                                    Display="None" ErrorMessage="<%$ Resources:Resource, msgPleaseSelectSubcategoryName%>"
                                    SetFocusOnError="true"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="tdAlign">
                                <asp:Label runat="server" CssClass="lblBold" ID="lblProduct" Text="<%$ Resources:Resource, SelectProduct%>"></asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table width="100%" class="tblBorder" style="border: 0;">
                                    <tr>
                                        <td align="right" style="padding-right: 65px;">
                                            <asp:Label runat="server" CssClass="lblBold" ID="lblselect" Text="<%$ Resources:Resource, AllProducts%>"></asp:Label>
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left" style="padding-left: 50px;">
                                            <asp:Label runat="server" CssClass="lblBold" ID="Label1" Text="<%$ Resources:Resource, SelectedProducts%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" width="45%">
                                            <asp:ListBox ID="lstFromPrd" runat="server" onclick="funFrom()" Width="200px" Height="250px">
                                            </asp:ListBox>
                                        </td>
                                        <td width="10%" align="center">
                                            <input type="BUTTON" name="Move" value=">>" disabled="disabled" id="cmdMoveRight"
                                                language="javascript" style="width: 25px; background-color: LightGrey; font-size: 11px;
                                                font-weight: bold; height: 22px;" onclick="return cmdMoveRight_onclick()"><br />
                                            <br />
                                            <br />
                                            <input type="BUTTON" name="Move" value="<<" disabled="disabled" id="cmdMoveLeft"
                                                language="javascript" style="width: 25px; background-color: LightGrey; font-size: 11px;
                                                font-weight: bold; height: 22px;" onclick="return cmdMoveLeft_onclick()">
                                        </td>
                                        <td width="45%" align="left">
                                            <asp:ListBox ID="lstToPrd" onclick="funTo()" runat="server" Width="200px" Height="250px">
                                            </asp:ListBox>
                                            <asp:CustomValidator ID="custvalPrd" runat="server" Display="None" ClientValidationFunction="funselectPrd"
                                                ErrorMessage="<%$ Resources:Resource, msgPleaseSelectUser%>" Width="149px"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr height="30">
                            <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                                <table width="100%">
                                    <tr>
                                        <td width="30%">
                                        </td>
                                        <td width="20%">
                                            <div class="buttonwrapper">
                                                <a id="imgCmdSave" onclick="Javascript:funInsert();" runat="server" class="ovalbutton"
                                                    href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                        <%=Resources.Resource.cmdCssSubmit%></span></a></div>
                                        </td>
                                        <td width="50%" align="left">
                                            <div class="buttonwrapper">
                                                <a id="imgCmdReset" onclick="return cmdMoveLeft_onclick()" runat="server" causesvalidation="false"
                                                    class="ovalbutton" href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                        <%=Resources.Resource.cmdCssReset%></span></a></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hdnListValue" runat="server" />
        <asp:HiddenField ID="hdnListText" runat="server" />
        <asp:ValidationSummary ID="sumvalAssignPrd" runat="server" ShowMessageBox="true"
            ShowSummary="false" />
    </div>
    <br />
    <br />
    <script language="javascript">

        function funSelectCategory(source, arguments) {
            if (window.document.getElementById('<%=dlCatName.ClientID%>').value != "0") {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }

        function funSelectSubcategory(source, arguments) {
            if (window.document.getElementById('<%=dlSubcatName.ClientID%>').value != "0") {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }

        function funselectPrd(source, arguments) {
            if (document.getElementById('<%=lstToPrd.ClientID%>').options.length == 0) {
                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;
            }
        }

        function cmdMoveLeft_onclick() {
            var len = window.document.getElementById('<%=lstToPrd.ClientID%>').length - 1;
            for (i = len; i >= 0; i--) {
                window.document.getElementById('<%=lstFromPrd.ClientID%>').appendChild(window.document.getElementById('<%=lstToPrd.ClientID%>').item(i));
            }
        }

        function cmdMoveRight_onclick() {
            var len = window.document.getElementById('<%=lstFromPrd.ClientID%>').length - 1;
            for (i = len; i >= 0; i--) {
                window.document.getElementById('<%=lstToPrd.ClientID%>').appendChild(window.document.getElementById('<%=lstFromPrd.ClientID%>').item(i));
            }
        }
        function funFrom() {
            if (window.document.getElementById('<%=lstFromPrd.ClientID%>').selectedIndex != -1)

                window.document.getElementById('<%=lstToPrd.ClientID%>').appendChild(window.document.getElementById('<%=lstFromPrd.ClientID%>').item(window.document.getElementById('<%=lstFromPrd.ClientID%>').selectedIndex));
        }

        function funTo() {
            if (window.document.getElementById('<%=lstToPrd.ClientID%>').selectedIndex != -1)
                window.document.getElementById('<%=lstFromPrd.ClientID%>').appendChild(window.document.getElementById('<%=lstToPrd.ClientID%>').item(window.document.getElementById('<%=lstToPrd.ClientID%>').selectedIndex));
        }


        function funInsert() {
            var hdn = document.getElementById('<%=hdnListValue.ClientID%>');
            hdn.value = "";
            var hdn1 = document.getElementById('<%=hdnListText.ClientID%>');
            hdn1.value = "";

            var listBox = document.getElementById('<%=lstToPrd.ClientID%>');
            var elements = "";

            var elements1 = "";
            var intCount = listBox.options.length;
            //store the elements in a hidden input that we can get server side 
            for (i = 0; i < intCount; i++) {
                elements += listBox.options[i].value + '-';
                elements1 += listBox.options[i].text + '/';
            }
            hdn.value = elements;
            hdn1.value = elements1;

        }
    </script>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Inventory_ProductAdvanceSearch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                LoadDropDownValues();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    private void LoadDropDownValues()
    {
        ProductCategory objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = (int)ProductCategories.Website;
        var vWebsiteCategory = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        vWebsiteCategory.Insert(0, new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll /*+ " " + Resources.Resource.lblWebSiteSubCategory*/) });
        ddlCategory.DataSource = vWebsiteCategory;
        ddlCategory.DataTextField = "CatgName";
        ddlCategory.DataValueField = "CatgID";
        ddlCategory.DataBind();
        foreach (var item in vWebsiteCategory)
        {
            ListItem li = ddlCategory.Items.FindByValue("0");
            if (li != null)
            {
                li.Selected = true;
            }
        }


        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = (int)ProductCategories.Neckline;
        var vNickline = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        vNickline.Insert(0, new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll /*+ " " + Resources.Resource.lblNickLine*/) });
        ddlNickline.DataSource = vNickline;
        ddlNickline.DataTextField = "CatgName";
        ddlNickline.DataValueField = "CatgID";
        ddlNickline.DataBind();
        foreach (var item in vNickline)
        {
            ListItem li = ddlNickline.Items.FindByValue("0");
            if (li != null)
            {
                li.Selected = true;
            }
        }


        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = (int)ProductCategories.Extra;
        var vDetail = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        vDetail.Insert(0, new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll /*+ " " + Resources.Resource.lblExtraCategory*/) });
        ddlExtraCategory.DataSource = vDetail;
        ddlExtraCategory.DataTextField = "CatgName";
        ddlExtraCategory.DataValueField = "CatgID";
        ddlExtraCategory.DataBind();
        foreach (var item in vDetail)
        {
            ListItem li = ddlExtraCategory.Items.FindByValue("0");
            if (li != null)
            {
                li.Selected = true;
            }
        }



        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = (int)ProductCategories.Sleeve;
        var vSleeve = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        vSleeve.Insert(0, new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll /*+ " " + Resources.Resource.lblSleeve*/) });
        ddlSleeve.DataSource = vSleeve;
        ddlSleeve.DataTextField = "CatgName";
        ddlSleeve.DataValueField = "CatgID";
        ddlSleeve.DataBind();
        foreach (var item in vSleeve)
        {
            ListItem li = ddlSleeve.Items.FindByValue("0");
            if (li != null)
            {
                li.Selected = true;
            }
        }


        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = (int)ProductCategories.Silhouette;
        var vSilhouette = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        vSilhouette.Insert(0, new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll /*+ " " + Resources.Resource.lblSilhouette*/) });
        ddlSilhouette.DataSource = vSilhouette;
        ddlSilhouette.DataTextField = "CatgName";
        ddlSilhouette.DataValueField = "CatgID";
        ddlSilhouette.DataBind();
        foreach (var item in vSilhouette)
        {
            ListItem li = ddlSilhouette.Items.FindByValue("0");
            if (li != null)
            {
                li.Selected = true;
            }
        }


        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = (int)ProductCategories.Trend;
        var vTrend = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        vTrend.Insert(0, new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll /*+ " " + Resources.Resource.lblTrend*/) });
        ddlTrend.DataSource = vTrend;
        ddlTrend.DataTextField = "CatgName";
        ddlTrend.DataValueField = "CatgID";
        ddlTrend.DataBind();
        foreach (var item in vTrend)
        {
            ListItem li = ddlTrend.Items.FindByValue("0");
            if (li != null)
            {
                li.Selected = true;
            }
        }

        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = (int)ProductCategories.Keywords;
        var vKeyWords = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        vKeyWords.Insert(0, new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll /*+ " " + Resources.Resource.lblProductKeyWord*/) });
        ddlKeyWord.DataSource = vKeyWords;
        ddlKeyWord.DataTextField = "CatgName";
        ddlKeyWord.DataValueField = "CatgID";
        ddlKeyWord.DataBind();
        foreach (var item in vKeyWords)
        {
            ListItem li = ddlKeyWord.Items.FindByValue("0");
            if (li != null)
            {
                li.Selected = true;
            }
        }

        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = (int)ProductCategories.WebsiteSubCategory;
        var vWebsiteSubCategory = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        vWebsiteSubCategory.Insert(0, new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll /*+ " " + Resources.Resource.lblWebSiteSubCategory*/) });
        ddlWebSiteSubCatg.DataSource = vWebsiteSubCategory;
        ddlWebSiteSubCatg.DataTextField = "CatgName";
        ddlWebSiteSubCatg.DataValueField = "CatgID";
        ddlWebSiteSubCatg.DataBind();
        foreach (var item in vWebsiteSubCategory)
        {
            ListItem li = ddlWebSiteSubCatg.Items.FindByValue("0");
            if (li != null)
            {
                li.Selected = true;
            }
        }

        objProductCategory = new ProductCategory();
        objProductCategory.CatgHrdID = (int)ProductCategories.Gauge;
        var vGauge = objProductCategory.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        vGauge.Insert(0, new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll + " " + Resources.Resource.lblGauge) });
        ddlGauge.DataSource = vGauge;
        ddlGauge.DataTextField = "CatgName";
        ddlGauge.DataValueField = "CatgID";
        ddlGauge.DataBind();
        foreach (var item in vGauge)
        {
            ListItem li = ddlGauge.Items.FindByValue("0");
            if (li != null)
            {
                li.Selected = true;
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {


            //var lstNickLine = new List<int>();
            //string sNickLine = "";
            //int id = 0;

            //ProductCategory objPCategory = new ProductCategory();
            //List<ProductCategory> lstPC = new List<ProductCategory>();
            //objPCategory.CatgHrdID = (int)ProductCategories.Neckline;
            //lstPC = objPCategory.GetCategoryHeader(null, Globals.CurrentAppLanguageCode);
            //int lmtCount = 0;
            //foreach (ListItem item in ddlNickline.Items)
            //{
            //    id = 0;
            //    if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstNickLine.Contains(id))
            //    {
            //        lstNickLine.Add(id);
            //        if (sNickLine != "")
            //            sNickLine += " ," + BusinessUtility.GetString(id);
            //        else
            //        {
            //            sNickLine = BusinessUtility.GetString(id);
            //        }

            //    }
            //}
            //_prd.PrdNickLine = sNickLine;


            //var lstGauge = new List<int>();
            //string sGauge = "";
            //id = 0;
            //foreach (ListItem item in ddlGauge.Items)
            //{
            //    id = 0;
            //    if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstGauge.Contains(id))
            //    {
            //        lstGauge.Add(id);
            //        if (sGauge != "")
            //            sGauge += " ," + BusinessUtility.GetString(id);
            //        else
            //        {
            //            sGauge = BusinessUtility.GetString(id);
            //        }
            //    }
            //}
            //_prd.PrdGauge = sGauge;



            //var lstExtraCategory = new List<int>();
            //string sExtraCategory = "";
            //id = 0;
            //foreach (ListItem item in ddlExtraCategory.Items)
            //{
            //    id = 0;
            //    if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstExtraCategory.Contains(id))
            //    {
            //        lstExtraCategory.Add(id);
            //        if (sExtraCategory != "")
            //            sExtraCategory += " ," + BusinessUtility.GetString(id);
            //        else
            //        {
            //            sExtraCategory = BusinessUtility.GetString(id);
            //        }
            //    }
            //}
            //_prd.PrdExtraCategory = sExtraCategory;


            //var lstSleeve = new List<int>();
            //string sSleeve = "";
            //id = 0;
            //foreach (ListItem item in ddlSleeve.Items)
            //{
            //    id = 0;
            //    if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstSleeve.Contains(id))
            //    {
            //        lstSleeve.Add(id);
            //        if (sSleeve != "")
            //            sSleeve += " ," + BusinessUtility.GetString(id);
            //        else
            //        {
            //            sSleeve = BusinessUtility.GetString(id);
            //        }
            //    }
            //}
            //_prd.PrdSleeve = sSleeve;


            //var lstSilhouette = new List<int>();
            //string sSilhouette = "";
            //id = 0;
            //foreach (ListItem item in ddlSilhouette.Items)
            //{
            //    id = 0;
            //    if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstSilhouette.Contains(id))
            //    {
            //        lstSilhouette.Add(id);
            //        if (sSilhouette != "")
            //            sSilhouette += " ," + BusinessUtility.GetString(id);
            //        else
            //        {
            //            sSilhouette = BusinessUtility.GetString(id);
            //        }
            //    }
            //}
            //_prd.PrdSilhouette = sSilhouette;



            //var lstTrend = new List<int>();
            //string sTrend = "";
            //id = 0;
            //foreach (ListItem item in ddlTrend.Items)
            //{
            //    id = 0;
            //    if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstTrend.Contains(id))
            //    {
            //        lstTrend.Add(id);
            //        if (sTrend != "")
            //            sTrend += " ," + BusinessUtility.GetString(id);
            //        else
            //        {
            //            sTrend = BusinessUtility.GetString(id);
            //        }
            //    }
            //}
            //_prd.PrdTrend = sTrend;


            //var lstKeyWord = new List<int>();
            //string sKeyWord = "";
            //id = 0;
            //foreach (ListItem item in ddlKeyWord.Items)
            //{
            //    id = 0;
            //    if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstKeyWord.Contains(id))
            //    {
            //        lstKeyWord.Add(id);
            //        if (sKeyWord != "")
            //            sKeyWord += " ," + BusinessUtility.GetString(id);
            //        else
            //        {
            //            sKeyWord = BusinessUtility.GetString(id);
            //        }
            //    }
            //}
            //_prd.PrdKeyWord = sKeyWord;


            Globals.RegisterCloseDialogScript(Page, string.Format("parent.AdvanceSearch({0});", "5"), true);

            //Collection objCollection = new Collection();
            //objCollection.CollectionEn = txtCollectionNameEn.Text;
            //objCollection.CollectionFr = txtCollectionNameFr.Text;
            //objCollection.ShortName = txtShortName.Text;
            //objCollection.IsActive = BusinessUtility.GetInt(rblstActive.SelectedItem.Value);
            //objCollection.CollectionID = BusinessUtility.GetInt(Request.QueryString["CollectioID"]);
            //if (objCollection.Save(null, CurrentUser.UserID) == true)
            //{
            //    if (BusinessUtility.GetString(Request.QueryString["Status"]) == "INDIVIDUAL")
            //    {
            //        if (objCollection.CollectionID > 0)
            //        {
            //            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgCollectionUpdate);
            //            Globals.RegisterReloadParentScript(this);
            //            return;
            //        }
            //        else
            //        {
            //            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated);
            //            Globals.RegisterReloadParentScript(this);
            //            return;
            //        }
            //    }
            //    Globals.RegisterCloseDialogScript(Page, string.Format("parent.CollectionCreated({0});", objCollection.CollectionID), true);
            //}
            //else
            //{
            //    //MessageState.SetGlobalMessage(MessageType.Failure, "Collection Creation failure");
            //    Globals.RegisterReloadParentScript(this);
            //    Globals.RegisterCloseDialogScript(this);
            //}

        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
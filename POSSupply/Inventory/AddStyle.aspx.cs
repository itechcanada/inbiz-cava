﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Inventory_AddStyle : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MessageState.SetGlobalMessage(MessageType.Success, "");
            txtStyleName.Focus();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            ProductStyle objProductStyle = new ProductStyle();
            objProductStyle.StyleName = txtStyleName.Text;
            objProductStyle.CollectionID = CollectionID();

            if (objProductStyle.Save(null, CurrentUser.UserID) == true)
            {
                //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated );
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadStyle({0});", objProductStyle.StyleID), true);
            }
            else
            {
                //MessageState.SetGlobalMessage(MessageType.Failure, "Collection Creation failure");
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }

        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }


    private int CollectionID()
    {
        return BusinessUtility.GetInt(Request.QueryString["CollectionID"]);
    }

}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductKit.ascx.cs" Inherits="Inventory_UserControls_ProductKit" %>

<h3>
    <asp:Literal ID="ltSectionTitle" Text="" runat="server" />
</h3>
<div id="grid_wrapper" style="width: 100%;">    
    <trirand:JQGrid runat="server" ID="grdProductKit" DataSourceID="sdsProductKit" Height="200px"
        AutoWidth="True" ondatarequesting="grdProductKit_DataRequesting">
        <Columns>
            <trirand:JQGridColumn DataField="prdKitID" HeaderText="" PrimaryKey="True" Visible="false" />
            <trirand:JQGridColumn DataField="prdName" HeaderText="<%$ Resources:Resource, grdvProductName %>"
                Editable="false" />
            <trirand:JQGridColumn DataField="prdIntID" HeaderText="<%$ Resources:Resource, grdvProductIntID %>"
                Editable="false" />
            <trirand:JQGridColumn DataField="prdIncludeQty" HeaderText="<%$ Resources:Resource, grdvPrdQty %>"
                Editable="false" />           
            <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="prdKitID"
                Sortable="false" TextAlign="Center" Width="50">
            </trirand:JQGridColumn>
        </Columns>
        <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
            ShowDeleteButton="false" ShowSearchButton="false" />
        <SortSettings InitialSortColumn=""></SortSettings>
        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
        <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="gridLoadComplete" />
    </trirand:JQGrid>
    <iCtrl:IframeDialog ID="mdDelete" Width="400" Height="120" Title="Delete" Dragable="true"
        TriggerSelectorClass="pop_delete" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
        runat="server">
    </iCtrl:IframeDialog>   
    <asp:SqlDataSource ID="sdsProductKit" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
        ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
</div>

<script type="text/javascript">
    //Variables
    var gridID = "<%=grdProductKit.ClientID %>";

    //Function To Resize the grid
    function resize_the_grid() {
        $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
    }

    //Client side function before page change.
    function beforePageChange(pgButton) {
        $('#' + gridID).onJqGridPaging();
    }

    //Client side function on sorting
    function columnSort(index, iCol, sortorder) {
        $('#' + gridID).onJqGridSorting();
    }

    //Call Grid Resizer function to resize grid on page load.
    resize_the_grid();

    //Bind window.resize event so that grid can be resized on windo get resized.
    $(window).resize(resize_the_grid);

    function reloadGrid(event, ui) {
        $('#' + gridID).trigger("reloadGrid");
        //Call back Sync Message
        if (typeof getGlobalMessage == 'function') {
            getGlobalMessage();
        }
    }

    function gridLoadComplete(data) {
        $(window).trigger("resize");
    }        
</script>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductQuantity.ascx.cs" Inherits="Inventory_UserControls_ProductQuantity" %>

<style type="text/css">

select{width:179px;}
input,textarea,select{border:1px solid #797D7F;margin-bottom:0;}
</style>

<h3>
    <asp:Literal ID="ltSectionTitle" Text="" runat="server" />
</h3>
<asp:Panel ID="pnlEdit" runat="server" DefaultButton="btnSave">
    <ul class="form">
        <li>
            <div class="lbl">
                <asp:Label ID="Label1" runat="server" CssClass="lblBold" 
                    Text="<%$ Resources:Resource, lblPrdAvailableWarehouse %>" />
                *</div>
            <div class="input">
                <asp:DropDownList ID="dlWarehouses" runat="server" CssClass="innerText">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="<%$ Resources:Resource, custvalwarehouse %>" ControlToValidate="dlWarehouses"
                        runat="server" Text="*" />
            </div>
             <div class="lbl">
                <asp:Label ID="lblPrdOnHandQtyperLocation" runat="server" CssClass="lblBold" 
                    Text="<%$ Resources:Resource, lblPrdOnHandQtyperLocation %>" />
             </div>
            <div class="input">
                <asp:TextBox ID="txtOnHandQuanPerLocation" runat="server" CssClass="innerText" 
                    MaxLength="10" />
                <asp:CompareValidator ID="comvalOnHandQuanPerLocation" runat="server" 
                    ControlToValidate="txtOnHandQuanPerLocation" Display="None" 
                    EnableClientScript="true" 
                    ErrorMessage="<%$ Resources:Resource, comvalOnHandQuanPerLocation %>" 
                    Operator="DataTypeCheck" SetFocusOnError="true" Type="double" />
             </div>
            
            <div class="clearBoth">
            </div>
        </li>
         <li id="liPrd1" runat="server">
            <div class="lbl">
                <asp:Label ID="lblFloor" runat="server" CssClass="lblBold" 
                    Text="<%$ Resources:Resource, lblPrdFloor %>" />
             </div>
            <div class="input">
                <asp:TextBox ID="txtFloor" runat="server" CssClass="innerText" MaxLength="10" />
             </div>
            <div class="lbl">
                <asp:Label ID="lblAisie" runat="server" CssClass="lblBold" 
                    Text="<%$ Resources:Resource, lblPrdAisie %>" />
             </div>
            <div class="input">
                <asp:TextBox ID="txtAisie" runat="server" CssClass="innerText" MaxLength="10" />
             </div>
            <div class="clearBoth">
            </div>
        </li>
         <li id="liPrd2" runat="server">
            <div class="lbl">
                <asp:Label ID="lblRack" runat="server" CssClass="lblBold" 
                    Text="<%$ Resources:Resource, lblPrdRack %>" />
             </div>
            <div class="input">
                <asp:TextBox ID="txtRack" runat="server" CssClass="innerText" MaxLength="10" />
             </div>
            <div class="lbl">
                <asp:Label ID="lblBin" runat="server" CssClass="lblBold" 
                    Text="<%$ Resources:Resource, lblPrdBin %>" />
             </div>
            <div class="input">
                <asp:TextBox ID="txtBin" runat="server" CssClass="innerText" MaxLength="10" />
             </div>
            <div class="clearBoth">
            </div>
        </li>
         <li id="liPrd3" runat="server">
            <div class="lbl">
                <asp:Label ID="lblTaxCode" runat="server" CssClass="lblBold" 
                    Text="<%$ Resources:Resource, lblSelectTaxGroup %>" />
             </div>
            <div class="input">
                <asp:DropDownList ID="dlTaxCode" runat="server" CssClass="innerText">
                </asp:DropDownList>
             </div>
           <div class="lbl">
                <asp:Label ID="lblBlock" runat="server" CssClass="lblBold" 
                    Text="<%$ Resources:Resource, lblPrdBlock %>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtBlock" runat="server" CssClass="innerText" MaxLength="10" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
         <li id="liPrd4" runat="server">
            <div class="lbl">
                <asp:Label ID="lblPrdReservedQuantity" runat="server" CssClass="lblBold" 
                    Text="<%$ Resources:Resource, lblPrdReservedQuantity %>" />
             </div>
            <div class="input">
                <asp:TextBox ID="txtReservedQuantity" runat="server" CssClass="innerText" 
                    MaxLength="10" />
                <asp:CompareValidator ID="comvalReservedQuantity" runat="server" 
                    ControlToValidate="txtReservedQuantity" Display="None" 
                    EnableClientScript="true" 
                    ErrorMessage="<%$ Resources:Resource, comvalReservedQuantity %>" 
                    Operator="DataTypeCheck" SetFocusOnError="true" Type="double" />
             </div>
            <div class="lbl">
                <asp:Label ID="lblPrdReservedQtyforSO" runat="server" CssClass="lblBold" 
                    Text="<%$ Resources:Resource, lblPrdReservedQtyforSO %>" />
             </div>
            <div class="input">
                <asp:TextBox ID="txtResQuaforSO" runat="server" CssClass="innerText" 
                    MaxLength="10" />
                <asp:CompareValidator ID="comvalResQuaforSO" runat="server" 
                    ControlToValidate="txtResQuaforSO" Display="None" EnableClientScript="true" 
                    ErrorMessage="<%$ Resources:Resource, comvalResQuaforSO %>" 
                    Operator="DataTypeCheck" SetFocusOnError="true" Type="double" />
             </div>
            <div class="clearBoth">
            </div>
        </li>
         <li id="liPrd5" runat="server">
            <div class="lbl">
                <asp:Label ID="lblPrdDefectedQua" runat="server" CssClass="lblBold" 
                    Text="<%$ Resources:Resource, lblPrdDefectedQua %>" />
             </div>
            <div class="input">
                <asp:TextBox ID="txtDefectedQua" runat="server" CssClass="innerText" 
                    MaxLength="10" />
                <asp:CompareValidator ID="comvalDefectedQua" runat="server" 
                    ControlToValidate="txtDefectedQua" Display="None" EnableClientScript="true" 
                    ErrorMessage="<%$ Resources:Resource, comvalDefectedQua %>" 
                    Operator="DataTypeCheck" SetFocusOnError="true" Type="double" />
             </div>
            <div class="lbl"></div>
            <div class="input"></div>
            <div class="clearBoth">
            </div>
        </li>
    </ul>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" 
            onclick="btnSave_Click" />   
           <asp:Button ID="btnReset" Text="<%$Resources:Resource, btnReset%>" 
            runat="server"  CausesValidation="false" onclick="btnReset_Click" OnClientClick="reloadGrid(); return false;" />      
    </div>
</asp:Panel>

<div class="div_command"></div>
<div id="grid_wrapper" style="width: 100%;">    
    <trirand:JQGrid runat="server" ID="grdQuantity" DataSourceID="sdsQuantity" Height="200px"
        AutoWidth="True" ondatarequesting="grdQuantity_DataRequesting" 
        oncellbinding="grdQuantity_CellBinding">
        <Columns>
            <trirand:JQGridColumn DataField="id" HeaderText="" PrimaryKey="True" Visible="false" />
            <trirand:JQGridColumn DataField="WarehouseDescription" HeaderText="<%$ Resources:Resource, grdvWarehouse %>"
                Editable="false" />
            <trirand:JQGridColumn DataField="prdOhdQty" HeaderText="<%$ Resources:Resource, grdvOnHandQty %>"
                Editable="false" />
            <trirand:JQGridColumn DataField="prdDefectiveQty" HeaderText="<%$ Resources:Resource, grdvDefectiveQty %>"
                Editable="false" />
            <trirand:JQGridColumn DataField="AvailableQty" HeaderText="<%$ Resources:Resource, grdvAvailableQty %>"
                Editable="false" />
            <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdvInTransit %>" DataField="InTransit"
                Sortable="false" TextAlign="Center" />
            <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdTaxGroup %>" DataField="sysTaxCodeDescText"
                Sortable="false" TextAlign="Center" />
            <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, lblMove %>" DataField="id"
                Sortable="false" TextAlign="Center">
            </trirand:JQGridColumn>
            <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, lblViewTags %>" DataField="id"
                Sortable="false" TextAlign="Center">
            </trirand:JQGridColumn>
             <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdvEdit %>" DataField="id"
                Sortable="false" TextAlign="Center" Width="50">
            </trirand:JQGridColumn>
            <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="id"
                Sortable="false" TextAlign="Center" Width="50">
            </trirand:JQGridColumn>
        </Columns>
        <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
            ShowDeleteButton="false" ShowSearchButton="false" />
        <SortSettings InitialSortColumn=""></SortSettings>
        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
        <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="gridLoadComplete" RowSelect="rowSelect" />
    </trirand:JQGrid>   
    <iCtrl:IframeDialog ID="mdEdit" Width="750" Height="450" Title="Delete" Dragable="true"
        TriggerSelectorClass="pop_edit" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
        runat="server">
    </iCtrl:IframeDialog>
    <iCtrl:IframeDialog ID="mdInTransit" Width="600" Height="280" Title="In Transit" Dragable="true"
        TriggerSelectorClass="pop_intransit" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
        runat="server">
    </iCtrl:IframeDialog>
     <iCtrl:IframeDialog ID="mdTags" Width="400" Height="280" Title="<%$ Resources:Resource, lblProductTags %>" Dragable="true"
        TriggerSelectorClass="pop_tags" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
        runat="server">
    </iCtrl:IframeDialog>
     <iCtrl:IframeDialog ID="mdMoveItems" Width="600" Height="200" Title="<%$ Resources:Resource, lblMoveInventory %>" Dragable="true"
        TriggerSelectorClass="pop_move" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
        runat="server">
    </iCtrl:IframeDialog>
    <asp:SqlDataSource ID="sdsQuantity" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
        ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>   
    <asp:HiddenField ID="hdnIdToEdit" runat="server" /> 
</div>

<script type="text/javascript">
    //Variables
    var gridID = "<%=grdQuantity.ClientID %>";    

    //Function To Resize the grid
    function resize_the_grid() {
        $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
    }

    //Client side function before page change.
    function beforePageChange(pgButton) {
        $('#' + gridID).onJqGridPaging();
    }

    //Client side function on sorting
    function columnSort(index, iCol, sortorder) {
        $('#' + gridID).onJqGridSorting();
    }

    //Call Grid Resizer function to resize grid on page load.
    resize_the_grid();

    //Bind window.resize event so that grid can be resized on windo get resized.
    $(window).resize(resize_the_grid);

    function reloadGrid(event, ui) {
        resetForm();
        $('#' + gridID).trigger("reloadGrid");
        //Call back Sync Message
        if (typeof getGlobalMessage == 'function') {
            getGlobalMessage();
        }
    }

    function gridLoadComplete(data) {
        $(window).trigger("resize");
    }    

    function deleteQuantity(id) {
        if (confirm("Are you sure you want to delete?")) {
            var dataToPost = {};
            dataToPost.deleteItem = 1;
            dataToPost.idToDelete = id;
            $grid = $("#" + gridID);
            for (var k in dataToPost) {
                $grid.setPostDataItem(k, dataToPost[k]);
            }
            $grid.trigger("reloadGrid");

            for (var k in dataToPost) {
                $grid.removePostDataItem(k);
            }
        }
    }

    function resetForm() {
        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_dlWarehouses").val("");
        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtOnHandQuanPerLocation").val("");
        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtFloor").val("");
        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtAisie").val("");
        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtRack").val("");
        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtBin").val("");
        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_dlTaxCode").val("");
        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtBlock").val("");
        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtReservedQuantity").val("");
        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtResQuaforSO").val("");
        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtDefectedQua").val("");
        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_hdnIdToEdit").val("");
    }


    function rowSelect(id) {
        var dataToPost = {};
        dataToPost.id = id;
        var eleToBlock = $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_pnlEdit");
        masterBlockContentArea(eleToBlock , "Please wait...");
        $.ajax({
            type: "POST",
            url: 'Product.aspx/GetProductQuantityObject',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(dataToPost),
            success: function (data) {
                var qtyObj = data.d;
                if (qtyObj != null && qtyObj.Id > 0) {
                    $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_hdnIdToEdit").val(qtyObj.Id);
                    $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_dlWarehouses").val(qtyObj.PrdWhsCode);
                    $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtOnHandQuanPerLocation").val(qtyObj.PrdOhdQty);
                    $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtFloor").val(qtyObj.PrdFloor);
                    $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtAisie").val(qtyObj.PrdAisle);
                    $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtRack").val(qtyObj.PrdRack);
                    $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtBin").val(qtyObj.PrdBin);
                    $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_dlTaxCode").val(qtyObj.PrdTaxCode);
                    $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtBlock").val(qtyObj.PrdBlock);
                    $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtReservedQuantity").val(qtyObj.PrdQuoteRsv);
                    $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtResQuaforSO").val(qtyObj.PrdSORsv);
                    $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtDefectedQua").val(qtyObj.PrdDefectiveQty);
                }
                masterUnblockContentArea(eleToBlock);
            },
            error: function (request, status, errorThrown) {
                alert(status);
                masterUnblockContentArea(eleToBlock);
            }
        });
    }     
</script>

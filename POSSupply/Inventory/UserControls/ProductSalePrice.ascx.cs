﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Trirand.Web.UI.WebControls;


using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Inventory_UserControls_ProductSalePrice : System.Web.UI.UserControl, IProduct
{
    ProductSalePrice _prdSalePrice = new ProductSalePrice();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && grdProductSalePrice.AjaxCallBackMode != Trirand.Web.UI.WebControls.AjaxCallBackMode.None)
        {
            
        }
        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
        {
            btnSave.Visible = false;
            grdProductSalePrice.Columns[7].Visible = false;
        }
    }

    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    public void Initialize()
    {
        if (!IsPostBack && grdProductSalePrice.AjaxCallBackMode == AjaxCallBackMode.None)
        {
            ltSectionTitle.Text = Resources.Resource.lblPrdProductSalePrice;
            Product prd = new Product();
            prd.PopulateObject(this.ProductID);
            ltSectionTitle.Text = string.Format("{0} / {1}", prd.PrdName, Resources.Resource.lblPrdProductSalePrice);
            txtSalePrice.Focus();   
        }        
    }

    private void SetData()
    {
        _prdSalePrice.FromQty = BusinessUtility.GetInt(txtFromQty.Text);
        _prdSalePrice.FromRushQty = BusinessUtility.GetInt(txtFromRushQty.Text);
        _prdSalePrice.PrdSalesPriceID = this.ProductID;
        _prdSalePrice.RushSalesPrice = BusinessUtility.GetDouble(txtRushSalePrice.Text);
        _prdSalePrice.SalesPrice = BusinessUtility.GetDouble(txtSalePrice.Text);
        _prdSalePrice.ToQty = BusinessUtility.GetInt(txtToQty.Text);
        _prdSalePrice.ToRushQty = BusinessUtility.GetInt(txtToRushQuantity.Text);
    }

    protected void grdProductSalePrice_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        sdsProductSalePrice.SelectCommand = _prdSalePrice.GetSql(sdsProductSalePrice.SelectParameters, this.ProductID);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            this.SetData();
            _prdSalePrice.Insert();
            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgPrdSalePriceSaved);
            //Response.Redirect(Request.RawUrl, false);
            Response.Redirect(QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.AssociateVendors).ToString()), false);
        }
        catch(Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.PRODUCT_QTY_RANGE_ALREADY_EXISTS)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgProductSalePriceAlreadyExist);
            }
        }
    }
    protected void grdProductSalePrice_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 7) //Format Delete Url Link
        {
             string delUrl = string.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_PRODUCT_SALE_PRICE, e.RowKey, "reloadGrid");
            e.CellHtml = string.Format(@"<a class=""pop_delete"" href=""{0}"">{1}</a>", delUrl, Resources.Resource.CmdCssDelete);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.WebControls;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;

public partial class Inventory_UserControls_ProductDescription : System.Web.UI.UserControl, IProduct
{

    ProductDescription objPrdDesc = new ProductDescription();
    Product objProduct = new Product();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security Check
        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY) && CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER) == false && CurrentUser.IsInRole(RoleID.ADMINISTRATOR) == false)
        {
            btnSave.Visible = false;
        }
        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
        {
            btnSave.Visible = false;
        }

    }

    public void SetData()
    {
        objPrdDesc.Id = this.ProductID;
        objPrdDesc.DescLang = rblLang.SelectedValue;
        objPrdDesc.PrdSmallDesc = txtProdDescr.Text;
        objPrdDesc.PrdLargeDesc = txtProdTechDescr.Text;
        objPrdDesc.PrdName = txtProdName.Text;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            SetData();
            objPrdDesc.Update();

            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgPrdDescSaved);
            //Response.Redirect(Request.RawUrl);
            StatusProductType pt = QueryStringHelper.GetProductType("ptype");
            int iSec = (int)InventorySectionKey.ViewProducts;
            switch (pt)
            {
                case StatusProductType.Product:
                    if (IsResturant() == false)
                    {
                        // iSec = (int)InventorySectionKey.ProductColors;
                        iSec = (int)InventorySectionKey.ProducImages;
                    }
                    else
                    {
                        iSec = (int)InventorySectionKey.ProducImages;
                    }
                    break;
                case StatusProductType.Accommodation:
                    iSec = (int)InventorySectionKey.Ammenities;
                    break;
                case StatusProductType.ServiceProduct:
                case StatusProductType.AdminFee:
                case StatusProductType.ChildUnder12:
                    iSec = (int)InventorySectionKey.ViewProducts;
                    break;
                case StatusProductType.CourseProduct:
                    iSec = (int)InventorySectionKey.ProductQuantity;
                    break;
            }
            Response.Redirect(QueryString.GetModifiedUrl("section", iSec.ToString()), false);
        }
    }

    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    public void Initialize()
    {
        if (!IsPostBack)
        {
            ltSectionTitle.Text = "Description"; //Resources.Resource.lblPrdDescription;
            if (this.ProductID > 0)
            {
                objProduct.PopulateObject(this.ProductID);
                ltSectionTitle.Text = objProduct.PrdName + " / " + "Description"; //+ Resources.Resource.lblPrdDescription;
            }

            rblLang.SelectedValue = Globals.CurrentAppLanguageCode;

            objPrdDesc.PopulateObject(this.ProductID, rblLang.SelectedValue);
            objPrdDesc.Id = this.ProductID;
            objPrdDesc.DescLang = rblLang.SelectedValue;
            txtProdDescr.Text = objPrdDesc.PrdSmallDesc;
            txtProdTechDescr.Text = objPrdDesc.PrdLargeDesc;
            txtProdName.Text = objPrdDesc.PrdName;

            txtProdName.Focus();
        }
    }

    protected void rblLang_SelectedIndexChanged(object sender, EventArgs e)
    {
        objPrdDesc.PopulateObject(this.ProductID, rblLang.SelectedValue);
        objPrdDesc.Id = this.ProductID;
        objPrdDesc.DescLang = rblLang.SelectedValue;
        txtProdDescr.Text = objPrdDesc.PrdSmallDesc;
        txtProdTechDescr.Text = objPrdDesc.PrdLargeDesc;
        txtProdName.Text = objPrdDesc.PrdName;

        txtProdName.Focus();
    }

    private Boolean IsResturant()
    {
        return BusinessUtility.GetBool(BusinessUtility.GetInt(System.Configuration.ConfigurationManager.AppSettings["IsResturant"]));
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Inventory_UserControls_ProductVendor : System.Web.UI.UserControl, IProduct
{
    ProductAssociateVendor _prdAssVend = new ProductAssociateVendor();

    int _dsVendID = 0;
    int _dsPrdID = 1;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
        {
            btnSave.Visible = false;
            grdVendors.Columns[7].Visible = false;
        }
    }
    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    private void SetData()
    {
        _prdAssVend.PrdCostPrice = BusinessUtility.GetDouble(txtCostPrice.Text);
        _prdAssVend.PrdID = this.ProductID;
        _prdAssVend.PrdVendorID = BusinessUtility.GetInt(dlVendor.SelectedValue);
        _prdAssVend.NoOfUnits = BusinessUtility.GetInt(txtNoOfUnits.Text);
        _prdAssVend.VendorRefNo = txtVendorRefNo.Text;
    }

    public void Initialize()
    {
        ltSectionTitle.Text = Resources.Resource.lblPrdAssociate;
        Product prd = new Product();
        prd.PopulateObject(this.ProductID);
        ltSectionTitle.Text = string.Format("{0} / {1}", prd.PrdName, Resources.Resource.lblPrdAssVendor);
        DropDownHelper.FillVendors(dlVendor, new ListItem(""));       
    }

    protected void grdVendors_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains("toDelete"))
        {
            try
            {
                int pid = 0, vid = 0;
                int.TryParse(Request.QueryString["delProductID"], out pid);
                int.TryParse(Request.QueryString["delVendorID"], out vid);
                if (pid > 0 && vid > 0)
                {
                    _prdAssVend.Delete(pid, vid);
                }
                
            }
            finally { }
        }

        sdsPrdVendors.SelectCommand = _prdAssVend.GetSql(sdsPrdVendors.SelectParameters, this.ProductID);
    }

    protected void grdVendors_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 7)
        {
            e.CellHtml = string.Format(@"<a href=""javascript:;"" onclick=""deleteRecord({0}, {1});"">{2}</a>", e.RowValues[0], e.RowValues[1], Resources.Resource.CmdCssDelete);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                this.SetData();
                _prdAssVend.Save(dbHelp);
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgPrdAssociateVendorSaved);
                //Response.Redirect(Request.RawUrl, false);
                Response.Redirect(QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.ViewProducts).ToString()), false);
            }
            catch (Exception ex)
            {
                if (ex.Message == CustomExceptionCodes.VENDOR_ALREADY_ASSIGN_TO_PRODUCT)
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPrdAssociateVendorExist);
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
                }
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
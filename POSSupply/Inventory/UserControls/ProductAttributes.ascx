﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductAttributes.ascx.cs" Inherits="Inventory_UserControls_ProductAttributes" %>

<h3>
    <asp:Literal ID="ltSectionTitle" Text="" runat="server" />
</h3>
<asp:DataList ID="dlstProductAttributes" runat="server" 
    DataSourceID="sdsProductAttributes" RepeatColumns="4" RepeatDirection="Horizontal" 
    onitemdatabound="dlstProductAttributes_ItemDataBound" HorizontalAlign="Center">    
    <ItemTemplate>        
        <table border="0" cellpadding="2" cellspacing="2">
            <tr>
                <td valign="middle">
                    <input id="chkAttribute" type="checkbox" value='<%#Eval("AttributeID")%>' runat="server" />
                </td>
                <td valign="middle">
                    <label id="lblIcon" runat="server">
                        <asp:Image ID="imgIcon" ImageUrl="" runat="server"  />                       
                    </label>    
                </td>
                <td valign="middle">                                                     
                    <asp:Label AssociatedControlID="chkAttribute" ID="lblAttribute" Text='<%#Eval("AttributeDesc")%>' runat="server" />
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:DataList>
<asp:SqlDataSource ID="sdsProductAttributes" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
      ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
    <div style="text-align: right; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave %>" 
            runat="server" onclick="btnSave_Click" />
    </div>


﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductVendor.ascx.cs" Inherits="Inventory_UserControls_ProductVendor" %>

<h3>
    <asp:Literal ID="ltSectionTitle" Text="" runat="server" />
</h3>
<asp:Panel ID="pnlEdit" runat="server" DefaultButton="btnSave">
    <ul class="form">
        <li>
            <div class="lbl">
                <asp:Label ID="lblVendor" Text="<%$ Resources:Resource, lblInventryVendor %>" runat="server" />*
            </div>
            <div class="input">
                <asp:DropDownList ID="dlVendor" CssClass="innerText" runat="server" Width="336px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvVendor" runat="server" ControlToValidate="dlVendor"
                    ErrorMessage="<%$ Resources:Resource, custvalVendor %>" SetFocusOnError="true"
                    Display="None"></asp:RequiredFieldValidator>
            </div>           
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblCostPrice" Text="<%$ Resources:Resource, lblPrdCostPrice %>" CssClass="lblBold"
                    runat="server" />*
            </div>
            <div class="input">
                <asp:TextBox ID="txtCostPrice" runat="server" CssClass="txtBoxPrd numericTextField" />
                <%--<asp:CompareValidator ID="comvalCostPrice" EnableClientScript="true" SetFocusOnError="true"
                    ControlToValidate="txtCostPrice" Operator="DataTypeCheck" Type="double" Display="None"
                    ErrorMessage="<%$ Resources:Resource, comvalCostPrice %>" runat="server" />--%>
                <asp:RequiredFieldValidator ID="reqvalCostPrice" runat="server" ControlToValidate="txtCostPrice"
                    ErrorMessage="<%$ Resources:Resource, reqvalCostPrice %>" SetFocusOnError="true"
                    Display="None"></asp:RequiredFieldValidator>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">                
                <asp:Label ID="Label1" Text="<%$Resources:Resource,lblNoOfUnits %>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtNoOfUnits" runat="server" />
            </div>
            <div class="clearBoth"></div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="Label2" Text="<%$Resources:Resource,lblVendorRefNo %>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtVendorRefNo" runat="server" />
            </div>
            <div class="clearBoth"></div>
        </li>
    </ul>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave %>" runat="server"
            OnClick="btnSave_Click" />
    </div>
    <asp:ValidationSummary ID="sumvalAssociate" runat="server" ShowMessageBox="true"
                        ShowSummary="false" />
</asp:Panel>
<div id="grid_wrapper" style="width: 100%;">
    <trirand:JQGrid runat="server" ID="grdVendors" DataSourceID="sdsPrdVendors"
        Height="200px" AutoWidth="True" 
        OnDataRequesting="grdVendors_DataRequesting" 
        oncellbinding="grdVendors_CellBinding">
        <Columns>
            <trirand:JQGridColumn DataField="prdVendorID" HeaderText="" PrimaryKey="True" Visible="false" />
            <trirand:JQGridColumn DataField="vendorName" HeaderText="<%$ Resources:Resource, grdvPrdVendorName %>"
                Editable="false" />
            <trirand:JQGridColumn DataField="prdCostPrice" HeaderText="<%$ Resources:Resource, grdvCostPrice %>"
                Editable="false" TextAlign="Right"  DataFormatString="{0:F}" />
            <trirand:JQGridColumn DataField="NoOfUnits" HeaderText="<%$ Resources:Resource, lblNoOfUnits %>"
                Editable="false" TextAlign="Center" />
            <trirand:JQGridColumn DataField="PurchaseDate" HeaderText="<%$ Resources:Resource, lblPurchaseDate %>"
                Editable="false" TextAlign="Center" />
            <trirand:JQGridColumn DataField="PurchaseOrderNo" HeaderText="<%$ Resources:Resource, lblPurchaseOrderNo %>"
                Editable="false" TextAlign="Center" />
            <trirand:JQGridColumn DataField="VendorRefNo" HeaderText="<%$ Resources:Resource, lblVendorRefNo %>"
                Editable="false" />
            <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="prdVendorID"
                Sortable="false" TextAlign="Center" Width="50">
            </trirand:JQGridColumn>
        </Columns>
        <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
            ShowDeleteButton="false" ShowSearchButton="false" />
        <SortSettings InitialSortColumn=""></SortSettings>
        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
        <ClientSideEvents LoadComplete="loadComplete" />
    </trirand:JQGrid>
    <iCtrl:IframeDialog ID="mdDelete" Width="400" Height="120" Title="Delete" Dragable="true"
        TriggerSelectorClass="pop_delete" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
        runat="server">
    </iCtrl:IframeDialog>
    <asp:SqlDataSource ID="sdsPrdVendors" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
        ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
</div>
<asp:Panel ID="SearchPanel" runat="server">
</asp:Panel>
<script type="text/javascript">
    $grid = $("#<%=grdVendors.ClientID%>");
    $grid.initGridHelper({
        searchPanelID: "<%=SearchPanel.ClientID %>",
        searchButtonID: "btnSearch",
        gridWrapPanleID: "grid_wrapper"
    });

    function jqGridResize() {
        $("#<%=grdVendors.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
    }


    function loadComplete(data) {
        jqGridResize();
    }

    function deleteRecord(vendorID, productID) {
        if (confirm("Are you sure you want to delete?")) {
            var dataToPost = {};
            dataToPost.toDelete = true;
            dataToPost.delVendorID = vendorID;
            dataToPost.delProductID = productID;

            for (var k in dataToPost) {
                $grid.setPostDataItem(k, dataToPost[k]);
            }

            $grid.trigger("reloadGrid");

            for (var k in dataToPost) {
                $grid.removePostDataItem(k);
            }
        }
    }
        
</script>

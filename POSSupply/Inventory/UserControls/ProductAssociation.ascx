﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductAssociation.ascx.cs" Inherits="Inventory_UserControls_ProductAssociation" %>

<h3>
    <asp:Literal ID="ltSectionTitle" Text="" runat="server" />
</h3>
<asp:Panel ID="pnlEdit" runat="server" DefaultButton="btnSave">
    <ul class="form">
        <li>
            <div class="lbl">                
                <asp:Label ID="lblProducts" Text="<%$Resources:Resource, lblProducts %>" runat="server" />
            </div>
            <div class="input">
                <asp:ListBox ID="lbxProducts" CssClass="modernized_select" runat="server" Width="336px" SelectionMode="Multiple">
                </asp:ListBox>
            </div>
            <div class="clearBoth">
            </div>
        </li>
    </ul>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave %>" runat="server"
            OnClick="btnSave_Click" />
    </div>
</asp:Panel>

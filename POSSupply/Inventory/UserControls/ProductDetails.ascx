﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductDetails.ascx.cs"
    Inherits="Inventory_UserControls_ProductDetails" %>
<asp:Panel ID="pnlProduct" runat="Server" DefaultButton="btnSave">
    <h3>
        <asp:Literal ID="ltSectionTitle" Text="" runat="server" />
    </h3>
    <ul class="form">
        <li id="liAccomodation1" runat="server" visible="false">
            <div class="lbl">
                <asp:Label ID="lblBuilding" Text="Building" runat="server" />
            </div>
            <div class="input">
                <asp:DropDownList ID="ddlBuilding" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBuilding_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="lbl">
                <asp:Label ID="lblRoom" Text="Room" runat="server" />
            </div>
            <div class="input">
                <asp:DropDownList ID="ddlRoom" runat="server">
                </asp:DropDownList>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <h3>
                <%= Resources.Resource.lblProductDecription%>
            </h3>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblProdName" Text="<%$ Resources:Resource, lblPrdName %>" runat="server" />*
            </div>
            <div class="input">
                <asp:TextBox ID="txtProdName" runat="server" MaxLength="250" />
                <asp:RequiredFieldValidator ID="reqvalProdName" runat="server" ControlToValidate="txtProdName"
                    ErrorMessage="<%$ Resources:Resource, reqvalPrdName %>" Text="*" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </div>
            <div class="lbl">
                <asp:Label ID="lblBarcode" Text="<%$ Resources:Resource, lblPrdBarcodeNew %>" runat="server" />*
            </div>
            <div class="input" style="width: 350px;">
                <asp:TextBox ID="txtBarcode" runat="server" MaxLength="35" />
                <asp:Button ID="btnRefresh" Text="Generate UPC" runat="server" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="reqvalBarCode" runat="server" ControlToValidate="txtBarcode"
                    ErrorMessage="<%$ Resources:Resource, reqvalProdBarCode %>" SetFocusOnError="true">*</asp:RequiredFieldValidator>
                <script type="text/javascript">
                    var $barCodeTxtFld = $("#<%=txtBarcode.ClientID%>");
                    $("#<%=btnRefresh.ClientID%>").click(function () {
                        $this = $(this);
                        $.post("../ajax/barCodeAPI.aspx", { pType: "<%=SequenceType%>", pfx: "<%=SequencePfx%>", f: "<%=SequenceFormat%>" }, function (data) {
                            $barCodeTxtFld.val(data);
                            $this.hide();
                        });
                        return false;
                    });                    
                </script>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="Label2" Text="<%$ Resources:Resource, lblPrdSalesPriceperMinQty %>"
                    runat="server" />*
            </div>
            <div class="input">
                <asp:TextBox ID="txtSalPriceMinQua" MaxLength="20" runat="server" CssClass="numericTextField" />
                <%--<asp:CompareValidator ID="cmvSalPriceMinQua" EnableClientScript="true" SetFocusOnError="true"
                    ControlToValidate="txtSalPriceMinQua" Operator="DataTypeCheck"  Type="Double"
                    ErrorMessage="<%$ Resources:Resource, cmvPrdSalPriceMinQua %>" CultureInvariantValues="false" runat="server">*</asp:CompareValidator>--%>
                <asp:RequiredFieldValidator ID="reqvalSalPriceMinQua" runat="server" ControlToValidate="txtSalPriceMinQua"
                    ErrorMessage="<%$ Resources:Resource, reqvalSalPriceMinQua %>" SetFocusOnError="true">*</asp:RequiredFieldValidator>
            </div>
            <div class="lbl">
                <asp:Label ID="lblEndUserSalesPrice" Text="<%$ Resources:Resource, lblPrdEndUserSalesPrice %>"
                    runat="server" />*
            </div>
            <div class="input">
                <asp:TextBox ID="txtEndUserSalesPrice" runat="server" MaxLength="20" CssClass="numericTextField" />
                <%--<asp:CompareValidator ID="compvalProdPrice" EnableClientScript="true" SetFocusOnError="true"
                    ControlToValidate="txtEndUserSalesPrice" Operator="DataTypeCheck" Type="Double"
                    ErrorMessage="<%$ Resources:Resource, compvalPrdPrice %>" runat="server">*</asp:CompareValidator>--%>
                <asp:RequiredFieldValidator ID="reqvalEndUserSalesPrice" runat="server" ControlToValidate="txtEndUserSalesPrice"
                    ErrorMessage="<%$ Resources:Resource, reqvalEndUserSalesPrice %>" SetFocusOnError="true">*</asp:RequiredFieldValidator>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblInternalID" runat="server" Text="<%$ Resources:Resource, lblPrdInternalID %>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtInternalID" runat="server" MaxLength="20" />
            </div>
            <div class="lbl">
                <asp:Label ID="lblWebSalesPrice" Text="<%$ Resources:Resource, lblWebSalesPrice %>"
                    runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtWebSalesPrice" runat="server" MaxLength="10" CssClass="numericTextField" />
                <%--<asp:CompareValidator ID="compvalWebSalesPrice" EnableClientScript="true" SetFocusOnError="true"
                    ControlToValidate="txtWebSalesPrice" Operator="DataTypeCheck" Type="Double" ErrorMessage="<%$ Resources:Resource, CompvalPrdWebSalesPrice %>"
                    runat="server">*</asp:CompareValidator>--%>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liFOBLanded" runat="server">
            <div class="lbl">
                <asp:Label ID="Label35" runat="server" Text="<%$ Resources:Resource, lblFOBPrice %>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtFOBPrice" runat="server" MaxLength="10" CssClass="numericTextField" />
                <%--<asp:CompareValidator ID="CompareValidator2" EnableClientScript="true" SetFocusOnError="true"
                    ControlToValidate="txtFOBPrice" Operator="DataTypeCheck" Type="Double" ErrorMessage="<%$ Resources:Resource, CompvalPrdFobPrice %>"
                    runat="server">*</asp:CompareValidator>--%>
            </div>
            <div class="lbl">
                <asp:Label ID="Label36" Text="<%$ Resources:Resource, lblLandedPrice %>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtLandedPrice" runat="server" MaxLength="10" CssClass="numericTextField" />
                <%--                <asp:CompareValidator ID="CompareValidator1" EnableClientScript="true" SetFocusOnError="true"
                    ControlToValidate="txtLandedPrice" Operator="DataTypeCheck" Type="Double" ErrorMessage="<%$ Resources:Resource, CompvalPrdLandedPrice %>"
                    runat="server">*</asp:CompareValidator>--%>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblDefectiveQty" runat="server" Text="<%$ Resources:Resource, grdvDefectiveQty %>" />
            </div>
            <div class="input">
                <asp:Label ID="valDefectiveQty" runat="server" Text="" />
            </div>
            <div class="lbl">
                <asp:Label ID="lblAvailableQty" Text="<%$ Resources:Resource, grdvAvailableQty %>"
                    runat="server" />
            </div>
            <div class="input">
                <asp:Label ID="valAvailableQty" Text="" runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblInTransit" runat="server" Text="<%$ Resources:Resource, grdvInTransit %>" />
            </div>
            <div class="input">
                <asp:Label ID="valInTransit" runat="server" Text="" />
            </div>
            <div class="lbl">
                <asp:Label ID="lblInComingOrder" runat="server" Text="<%$ Resources:Resource, lblInComingOrder %>" />
            </div>
            <div class="input">
                <asp:Label ID="valInComingOrder" runat="server" Text="" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <h3>
                <%= Resources.Resource.lblPackeageDetails%>
            </h3>
        </li>
        <li id="liProduct1" runat="server" visible="false">
            <div class="lbl">
                <asp:Label ID="lblProdPkgHeight0" runat="server" Text="<%$Resources:Resource, lblProductWeight%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtWeight" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
                <asp:Label ID="lblUnitWeight" Text="" runat="server" />
            </div>
            <div class="lbl">
                <asp:Label ID="lblProdPkgLength0" runat="server" Text="<%$Resources:Resource, lblProductLength%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtLength" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
                <asp:Label ID="lblUnitLength" Text="" runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liProduct2" runat="server" visible="false" style="display: none;">
            <div class="lbl">
                <asp:Label ID="Label1" runat="server" Text="<%$Resources:Resource, lblWidth%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtWidth" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
                <asp:Label ID="lblUnitWidth" Text="" runat="server" />
            </div>
            <div class="lbl">
                <asp:Label ID="Label3" runat="server" Text="<%$Resources:Resource, lblPrdHeight%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtHeight" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
                <asp:Label ID="lblUnitHeight" Text="" runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liProduct3" runat="server" visible="false">
            <div class="lbl">
                <asp:Label ID="Label4" runat="server" Text="<%$Resources:Resource, lblPrdPackageWeight%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtPkgWeight" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
                <asp:Label ID="lblUnitPkgWeight" Text="" runat="server" />
            </div>
            <div class="lbl">
                <asp:Label ID="Label6" runat="server" Text="<%$Resources:Resource, lblPackageLenght%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtPkgLength" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
                <asp:Label ID="lblUnitPkgLength" Text="" runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liProduct4" runat="server" visible="false">
            <div class="lbl">
                <asp:Label ID="Label7" runat="server" Text="<%$Resources:Resource, lblPrdPackageWidth%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtPkgWidth" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
                <asp:Label ID="lblUnitPkgWidth" Text="" runat="server" />
            </div>
            <div class="lbl">
                <asp:Label ID="Label8" runat="server" Text="<%$Resources:Resource, lblPrdPackageHeight%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtPkgHeigth" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
                <asp:Label ID="lblUnitPkgHeight" Text="" runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <h3>
                <%= Resources.Resource.lblProductSize%>
            </h3>
        </li>
        <li id="liClothDesc4" runat="server">
            <div class="lbl">
                <asp:Label ID="Label26" runat="server" Text="<%$Resources:Resource, lblSize%>" />
            </div>
            <div class="input">
                <asp:DropDownList ID="ddlSize" runat="server">
                </asp:DropDownList>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liClothDesc5" runat="server">
            <div class="lbl">
                <%--<asp:Label ID="Label25" runat="server" Text="<%$Resources:Resource, lblJupesPantalons%>" />--%>
                <asp:Label ID="Label27" runat="server" Text="<%$Resources:Resource, lblLongueurLength%>" />
            </div>
            <div class="input">
                <%-- <asp:DropDownList ID="ddlJupesPantalons" runat="server">
                </asp:DropDownList>--%>
                <asp:TextBox ID="txtLongueurLength" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
            </div>
            <div class="lbl">
                <asp:Label ID="Label28" runat="server" Text="<%$Resources:Resource, lblEpauleShoulder%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtEpauleShoulder" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="Label29" runat="server" Text="<%$Resources:Resource, lblBusteChest%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtBusteChest" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
            </div>
            <div class="lbl">
                <asp:Label ID="Label30" runat="server" Text="<%$Resources:Resource, lblBicep%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtBicep" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <h3>
                <%= Resources.Resource.lblPOInformation%>
            </h3>
        </li>
        <li id="liProduct5" runat="server" visible="false">
            <div class="lbl">
                <asp:Label ID="lblMinQtyforPO" Text="<%$ Resources:Resource, lblPrdMinimumQtyforPO %>"
                    runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtMinQtyforPO" runat="server" CssClass="integerTextField" Width="70px" />
                <asp:CompareValidator ID="comvalMinQtyforPO" EnableClientScript="true" SetFocusOnError="true"
                    ControlToValidate="txtMinQtyforPO" Operator="DataTypeCheck" Type="double" ErrorMessage="<%$ Resources:Resource, comvalMinQtyforPO %>"
                    runat="server">*</asp:CompareValidator>
            </div>
            <div class="lbl">
                <asp:Label ID="lblQuaToOrd" Text="<%$ Resources:Resource, lblPrdQuaToOrd %>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtQuaToPOrd" runat="server" CssClass="integerTextField" Width="70px" />
                <asp:CompareValidator ID="comvalQuaToPOrd" EnableClientScript="true" SetFocusOnError="true"
                    ControlToValidate="txtQuaToPOrd" Operator="DataTypeCheck" Type="double" ErrorMessage="<%$ Resources:Resource, comvalQuaToPOrd %>"
                    runat="server">*</asp:CompareValidator>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liProduct7" runat="server" visible="false">
            <div class="lbl">
                <asp:Label ID="Label5" Text="<%$ Resources:Resource, lblPrdCreateAutoPOFlag %>" runat="server" />
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rdlstPOFlag" runat="server" RepeatDirection="Horizontal"
                    CssClass="rbl_list">
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Selected="true" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="lbl">
            </div>
            <div class="input">
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <h3>
                <%= Resources.Resource.lblWebSiteInfo%>
            </h3>
        </li>
        <li id="li2" runat="server">
            <div class="lbl">
                <asp:Label ID="Label37" runat="server" Text="<%$Resources:Resource, lblWebCategory%>" />
            </div>
            <div class="input">
                <asp:ListBox CssClass="modernized_select" ID="ddlCategory" runat="server" SelectionMode="Multiple"
                    data-placeholder="<%$ Resources:Resource, lblWebCategory %>"></asp:ListBox>
            </div>
            <div class="lbl">
                <asp:Label ID="Label38" runat="server" Text="<%$Resources:Resource, lblWebSiteSubCategory%>" />
            </div>
            <div class="input">
                <asp:ListBox CssClass="modernized_select" ID="ddlWebSiteSubCatg" runat="server" SelectionMode="Multiple"
                    data-placeholder="<%$ Resources:Resource, lblWebSiteSubCategory %>"></asp:ListBox>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <h3>
                <%= Resources.Resource.lblProductDetail%>
            </h3>
        </li>
        <%--Shown when Resturan Application start--%>
        <li id="liResturant1" runat="server" visible="false">
            <div class="lbl">
                <asp:Label ID="Label9" runat="server" Text="<%$Resources:Resource, lblGlutenFree%>" />
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rblGlutenFree" runat="server" RepeatDirection="Horizontal"
                    CssClass="rbl_list">
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0" Selected="true"></asp:ListItem>
                </asp:RadioButtonList>
                <asp:Label ID="Label10" Text="" runat="server" />
            </div>
            <div class="lbl">
                <asp:Label ID="Label11" runat="server" Text="<%$Resources:Resource, lblVegetarian%>" />
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rblVegetarian" runat="server" RepeatDirection="Horizontal"
                    CssClass="rbl_list">
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0" Selected="true"></asp:ListItem>
                </asp:RadioButtonList>
                <asp:Label ID="Label12" Text="" runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liResturant2" runat="server" visible="false">
            <div class="lbl">
                <asp:Label ID="Label13" runat="server" Text="<%$Resources:Resource, lblContainsNuts%>" />
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rblContainsNuts" runat="server" RepeatDirection="Horizontal"
                    CssClass="rbl_list">
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0" Selected="true"></asp:ListItem>
                </asp:RadioButtonList>
                <asp:Label ID="Label14" Text="" runat="server" />
            </div>
            <div class="lbl">
                <asp:Label ID="Label15" runat="server" Text="<%$Resources:Resource, lblCookedSushi%>" />
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rblCookedSushi" runat="server" RepeatDirection="Horizontal"
                    CssClass="rbl_list">
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0" Selected="true"></asp:ListItem>
                </asp:RadioButtonList>
                <asp:Label ID="Label16" Text="" runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liResturant3" runat="server" visible="false">
            <div class="lbl">
                <asp:Label ID="Label17" runat="server" Text="<%$Resources:Resource, lblSpicy%>" />
            </div>
            <div class="input">
                <asp:DropDownList ID="ddlSpicy" runat="server">
                    <asp:ListItem Text="<%$Resources:Resource, lblSpicy0%>" Value="0" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="<%$Resources:Resource, lblSpicyLevel1%>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$Resources:Resource, lblSpicyLevel2%>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$Resources:Resource, lblSpicyLevel3%>" Value="3"></asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="Label18" Text="" runat="server" />
            </div>
            <div class="lbl">
                &nbsp;
            </div>
            <div class="input">
                &nbsp;
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <%--Resturant Contorl End--%>
        <%--Cloth Description Control Strart--%>
        <li>
            <div class="lbl">
                <asp:Label ID="Label25" runat="server" Text="<%$Resources:Resource, lblCollection%>" />
            </div>
            <div class="input">
                <%--<asp:TextBox ID="txtTexture" runat="server" MaxLength="30" Width="70px" />--%>
                <asp:DropDownList ID="ddlCollection" OnSelectedIndexChanged="ddlCollection_OnSelectedIndexChanged"
                    runat="server">
                </asp:DropDownList>
            </div>
            <div class="lbl">
                <%--<asp:Label ID="Label40" runat="server" Text="<%$Resources:Resource, lblGauge%>" />--%>
            </div>
            <div class="input">
                <%-- <asp:TextBox ID="txtGauage" runat="server" CssClass="integerTextField" MaxLength="30"
                    Width="70px" />--%>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liClothDesc1" runat="server">
            <div class="lbl">
                <asp:Label ID="Label19" runat="server" Text="<%$Resources:Resource, lblStyle%>" />
            </div>
            <div class="input">
                <%--<asp:DropDownList ID="ddlStyle" runat="server">
                </asp:DropDownList>--%>
                <asp:TextBox ID="txtStyle" runat="server"></asp:TextBox>
            </div>
            <div class="lbl">
                <asp:Label ID="Label31" runat="server" Text="<%$Resources:Resource, lblMaterial%>" />
            </div>
            <div class="input">
                <asp:DropDownList ID="ddlMaterial" runat="server">
                </asp:DropDownList>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liClothDesc2" runat="server">
            <div class="lbl">
                <%--<asp:Label ID="Label21" runat="server" Text="<%$Resources:Resource, lblGauge%>" />--%>
                <asp:Label ID="Label23" runat="server" Text="<%$Resources:Resource, lblTexture%>" />
            </div>
            <div class="input">
                <asp:ListBox CssClass="modernized_select" ID="ddlTexture" runat="server" SelectionMode="Multiple"
                    data-placeholder="<%$ Resources:Resource, lblTexture %>"></asp:ListBox>
                <%--                <asp:DropDownList ID="ddlTexture" runat="server">
                </asp:DropDownList>--%>
            </div>
            <div class="lbl">
                <%--<asp:Label ID="Label21" runat="server" Text="<%$Resources:Resource, lblGauge%>" />--%>
                <asp:Label ID="Label21" runat="server" Text="<%$Resources:Resource, lblColor%>" />
            </div>
            <div class="input">
                <asp:DropDownList ID="ddlColor" runat="server">
                </asp:DropDownList>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liClothDesc6" runat="server">
            <div class="lbl">
                <asp:Label ID="Label20" runat="server" Text="<%$Resources:Resource, lblNeckline%>" />
            </div>
            <div class="input">
                <%--<asp:DropDownList ID="ddlNeckline" runat="server">
                </asp:DropDownList>--%>
                <asp:ListBox CssClass="modernized_select" ID="ddlNeckline" runat="server" SelectionMode="Multiple"
                    data-placeholder="<%$ Resources:Resource, lblNeckline %>"></asp:ListBox>
            </div>
            <div class="lbl">
                <asp:Label ID="Label32" runat="server" Text="<%$Resources:Resource, lblExtraCategory%>" />
            </div>
            <div class="input">
                <%--                <asp:DropDownList ID="ddlExtraCategory" runat="server">
                </asp:DropDownList>--%>
                <asp:ListBox CssClass="modernized_select" ID="ddlExtraCategory" runat="server" SelectionMode="Multiple"
                    data-placeholder="<%$ Resources:Resource, lblExtraCategory %>"></asp:ListBox>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liClothDesc3" runat="server">
            <div class="lbl">
                <asp:Label ID="Label22" runat="server" Text="<%$Resources:Resource, lblSleeve%>" />
            </div>
            <div class="input">
                <%--                <asp:DropDownList ID="ddlSleeve" runat="server">
                </asp:DropDownList>--%>
                <asp:ListBox CssClass="modernized_select" ID="ddlSleeve" runat="server" SelectionMode="Multiple"
                    data-placeholder="<%$ Resources:Resource, lblSleeve %>"></asp:ListBox>
            </div>
            <div class="lbl">
                <asp:Label ID="Label24" runat="server" Text="<%$Resources:Resource, lblSilhouette%>" />
            </div>
            <div class="input">
                <%--                <asp:DropDownList ID="ddlSilhouette" runat="server">
                </asp:DropDownList>--%>
                <asp:ListBox CssClass="modernized_select" ID="ddlSilhouette" runat="server" SelectionMode="Multiple"
                    data-placeholder="<%$ Resources:Resource, lblSilhouette %>"></asp:ListBox>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="li1" runat="server">
            <div class="lbl">
                <asp:Label ID="Label33" runat="server" Text="<%$Resources:Resource, lblTrend%>" />
            </div>
            <div class="input">
                <%--                <asp:DropDownList ID="ddlSleeve" runat="server">
                </asp:DropDownList>--%>
                <asp:ListBox CssClass="modernized_select" ID="ddlTrend" runat="server" SelectionMode="Multiple"
                    data-placeholder="<%$ Resources:Resource, lblTrend %>"></asp:ListBox>
            </div>
            <div class="lbl">
                <asp:Label ID="Label34" runat="server" Text="<%$Resources:Resource, lblProductKeyWord%>" />
            </div>
            <div class="input">
                <%--                <asp:DropDownList ID="ddlSilhouette" runat="server">
                </asp:DropDownList>--%>
                <asp:ListBox CssClass="modernized_select" ID="ddlKeyWord" runat="server" SelectionMode="Multiple"
                    data-placeholder="<%$ Resources:Resource, lblProductKeyWord %>"></asp:ListBox>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="li3" runat="server">
            <div class="lbl">
                <asp:Label ID="Label39" runat="server" Text="<%$Resources:Resource, lblAssociatedStyle%>" />
            </div>
            <div class="input">
                <%--                <asp:DropDownList ID="ddlSleeve" runat="server">
                </asp:DropDownList>--%>
                <asp:HiddenField ID="hdnMasterID" runat="server" />
                <asp:HiddenField ID="hdnCollectionID" runat="server" />
                <asp:ListBox CssClass="modernized_select" ID="ddlAssociatedStyle" runat="server"
                    SelectionMode="Multiple" data-placeholder="<%$ Resources:Resource, lblAssociatedStyle %>">
                </asp:ListBox>
            </div>
            <div class="lbl">
                <asp:Label ID="Label40" runat="server" Text="<%$Resources:Resource, lblGauge%>" />
            </div>
            <div class="input">
                <asp:ListBox CssClass="modernized_select" ID="ddlGauge" runat="server" SelectionMode="Multiple"
                    data-placeholder="<%$ Resources:Resource, lblGauge %>"></asp:ListBox>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <br />
        <li id="liClothDesc7" runat="server">
            <div class="lbl">
            </div>
            <div class="input">
                <%--<asp:TextBox ID="txtMaterial"  runat="server" MaxLength="30"
                    Width="70px" />--%>
            </div>
            <div class="lbl">
                <%-- <asp:Label ID="Label32" runat="server" Text="<%$Resources:Resource, lblMicron%>" />--%>
            </div>
            <div class="input">
                <%--<asp:TextBox ID="txtMicron" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />--%>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <%--Cloth Description Control Strart--%>
        <li>
            <div class="lbl">
                <asp:Label ID="lblProdSpecial" Text="<%$ Resources:Resource, lblPrdSpecial %>" runat="server" />
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rblstSpecial" runat="server" RepeatDirection="Horizontal"
                    CssClass="rbl_list">
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Selected="true" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="lbl">
                <asp:Label ID="lblISGiftCardProduct" Text="<%$ Resources:Resource, lblIsGiftCardProduct %>"
                    runat="server" />
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rdlstISGiftCardProduct" runat="server" RepeatDirection="Horizontal"
                    CssClass="rbl_list">
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Selected="true" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liProduct8" runat="server" visible="false">
            <div class="lbl" id="dvlblIsKit" runat="server">
                <asp:Label ID="lblIsKit" Text="<%$ Resources:Resource, lblPrdIsProductKit %>" runat="server" />
            </div>
            <div class="input" id="dvrblstProKit" runat="server">
                <asp:RadioButtonList ID="rblstProKit" runat="server" RepeatDirection="Horizontal"
                    CssClass="rbl_list">
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Selected="true" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="lbl">
                <asp:Label ID="lblisPOSMenu" Text="<%$ Resources:Resource, lblIsPOSMenu %>" runat="server" />
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rdlstISPOSMenu" runat="server" RepeatDirection="Horizontal"
                    CssClass="rbl_list">
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Selected="true" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblISWeb" Text="<%$ Resources:Resource, lblIsWeb %>" runat="server" />
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rdlstISWeb" runat="server" RepeatDirection="Horizontal"
                    CssClass="rbl_list">
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Selected="true" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="lbl">
                <asp:Label ID="lblIsProdActive" Text="<%$ Resources:Resource, lblIsActive %>" runat="server" />
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rblstActive" runat="server" RepeatDirection="Horizontal"
                    CssClass="rbl_list">
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Selected="true" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
            </div>
            <div class="input">
                <asp:CheckBox ID="chkIsNonStandard" runat="server" Text="<%$ Resources:Resource, lblNonStandard %>" />
            </div>
            <div class="lbl">
            </div>
            <div class="input">
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <h3>
                <%= Resources.Resource.lblProductTextDesc%>
            </h3>
        </li>
        <li id="liProduct10" runat="server" visible="false">
            <div class="lbl">
                <asp:Label ID="lblProdIncolTerms" Text="<%$ Resources:Resource, lblPrdIncolTerms %>"
                    runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtProdIncolTerms" runat="server" Rows="3" TextMode="MultiLine" />
                <asp:CustomValidator ID="custvalProdIncolTerms" SetFocusOnError="true" runat="server"
                    ErrorMessage="<%$ Resources:Resource, CustvalPrdIncolTerms %>" ClientValidationFunction="funMaxLengthIncolTerms">*</asp:CustomValidator>
            </div>
            <div class="lbl">
            </div>
            <div class="input">
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <asp:ValidationSummary ID="sumvalProduct" runat="server" ShowMessageBox="true" ShowSummary="false" />
        </li>
    </ul>
    <div style="clear: both;">
    </div>
    <div style="text-align: right; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;">
        <asp:Button ID="btnDelete" runat="server" CausesValidation="false" OnClick="btnDelete_Click"
            OnClientClick="return confimrDelete();" Text="<%$ Resources:Resource, delete %>" />
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, btnCancel%>" runat="server"
            CausesValidation="false" OnClick="btnCancel_Click" />
    </div>
</asp:Panel>
<script type="text/javascript" language="javascript">
    function funMaxLengthIncolTerms(source, arguments) {
        if (document.getElementById('<%= txtProdIncolTerms.ClientID%>').value.length >= 500) {
            arguments.IsValid = false;
        }
        else {
            arguments.IsValid = true;
        }
    }

    function confimrDelete() {
        return confirm("<%=Resources.Resource.confirmDelete%>");
    }
</script>

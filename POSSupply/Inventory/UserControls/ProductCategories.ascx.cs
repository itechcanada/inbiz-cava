﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.WebControls;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;

public partial class Inventory_UserControls_ProductCategories : System.Web.UI.UserControl,IProduct
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
        {
            grdProductCategory.Columns[3].Visible = false;
            grdProductCategory.Columns[4].Visible = false;
        }
    }
    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["section"], out pid);
            return pid;
        }
    }

    public void Initialize()
    {
        //ltSectionTitle.Text = Resources.Resource.lblMakePrdKit;
        ProductCategory prdCategory = new ProductCategory();
        if (this.ProductID == 0)
        {
            prdCategory.CatgHrdID = 1;
        }
        else
        {
            prdCategory.CatgHrdID = this.ProductID;
        }
        List<ProductCategory> lResult = new List<ProductCategory>();
        lResult = prdCategory.GetCategoryHeader(null, Globals.CurrentAppLanguageCode);
        if (lResult.Count > 0)
        {

            ltSectionTitle.Text = string.Format("{0}", lResult[0].CatgHdrName);
        }
        
    }
    protected void grdProductCategory_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        ProductCategory prdCategory = new ProductCategory();
        if (this.ProductID == 0)
        {
            prdCategory.CatgHrdID = 1;
        }
        else
        {
            prdCategory.CatgHrdID = this.ProductID;
        }
        grdProductCategory.DataSource = prdCategory.GetAllCategoryList(null, Globals.CurrentAppLanguageCode);
        grdProductCategory.DataBind();
        //sdsProductKit.SelectCommand = new ProductKit().GetSql(sdsProductKit.SelectParameters, this.CategoryHdrID, Globals.CurrentAppLanguageCode);
    }

    protected void grdProductCategory_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
       
        if (e.ColumnIndex == 2)
        {
            bool isActive = BusinessUtility.GetBool(e.CellHtml);// false;//= BusinessUtility.GetBool(e.RowKey);
            e.CellHtml = string.Format("<input type='checkbox' disabled='true' {0} />", isActive ? "checked='checked'" : "");
        }
        else if (e.ColumnIndex == 3) //Format edit link column
        {
            //string htm = string.Format(@"<a href=""javascript:;"" onclick=""EditProductCategory('{0}','{1}','{2}','{3}','{4}')"">{5}</a>", e.CellHtml, e.RowValues[0].ToString().Replace("'", ""), e.RowValues[4].ToString(), e.RowValues[8].ToString().Replace("'", ""), "&jscallback = reloadGrid", Resources.Resource.grdEdit);
            string htm = string.Format(@"<a href=""javascript:;"" onclick=""EditProductCategory('{0}','{1}','{2}','{3}','{4}')"">{5}</a>", e.CellHtml, e.RowValues[0].ToString().Replace("'", ""), this.ProductID, ltSectionTitle.Text, "&jscallback = reloadGrid", Resources.Resource.grdEdit);
            e.CellHtml = htm;
        }
        else if (e.ColumnIndex == 4) //Format Delete Link
        {
            int iMaterialID = BusinessUtility.GetInt(e.CellHtml);
            e.CellHtml = string.Format(@"<a href=""{0}""  onclick=""deleteCategory('{1}','{2}')"">{3}</a>", "javascript:void(0);", e.CellHtml, e.RowValues[4].ToString(), Resources.Resource.grdCMDelete);
        }
    }

  
}
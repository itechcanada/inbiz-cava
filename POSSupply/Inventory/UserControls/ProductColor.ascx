﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductColor.ascx.cs" Inherits="Inventory_UserControls_ProductColor" %>

<h3>
    <asp:Literal ID="ltSectionTitle" Text="" runat="server" />
</h3>
<asp:DataList ID="dlstProductColors" runat="server" 
    DataSourceID="sdsProductColors" RepeatColumns="4" RepeatDirection="Horizontal" 
    onitemdatabound="dlstProductColors_ItemDataBound" HorizontalAlign="Center">
    <ItemStyle Width="50px" />
    <ItemTemplate>        
        <table border="0" cellpadding="5" cellspacing="5">
            <tr>
                <td>
                    <input id="chkColor" type="checkbox" value='<%#Eval("colCode")%>' runat="server" />
                </td>
                <td>
                    <label id="lblColor" style='<%#string.Format("cursor:pointer;display:block; width:12px; height:12px; border:1px solid #ccc; background-color:{0}", Eval("colDesc"))%>' runat="server">
                        &nbsp;
                    </label>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:DataList>
<asp:SqlDataSource ID="sdsProductColors" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
      ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
    <div style="text-align: right; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave %>" 
            runat="server" onclick="btnSave_Click" />
    </div>


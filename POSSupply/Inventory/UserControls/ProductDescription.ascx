﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductDescription.ascx.cs" Inherits="Inventory_UserControls_ProductDescription" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>


<h3>
    <asp:Literal ID="ltSectionTitle" Text="" runat="server" />
</h3>
<asp:Panel runat="server" ID="Panel14" DefaultButton="btnSave">
    <table class="contentTableForm" width="100%" style="border: 0;" border="0">
        <tbody>
            <tr>
                <td valign="top" class="text">
                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lblLanguage %>"
                        CssClass="lblBold"></asp:Label>
                </td> 
                <td class="input">
                    <asp:RadioButtonList ID="rblLang" runat="server" RepeatDirection="Horizontal" 
                        AutoPostBack="True" onselectedindexchanged="rblLang_SelectedIndexChanged">
                        <asp:ListItem Selected="True" Value="en" Text="English" />
                        <asp:ListItem Value="fr" Text="French" />
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td valign="top" class="text">
                    <asp:Label ID="lblProdName" runat="server" Text="<%$ Resources:Resource, lblPrdDescName %>"
                        CssClass="lblBold"></asp:Label>
                </td>                
                <td class="input">
                    <asp:TextBox ID="txtProdName" runat="server" CssClass="innerText" Width="330px" MaxLength="250"></asp:TextBox><span
                        class="style1"> *</span>
                    <asp:RequiredFieldValidator ID="reqvalProdName" runat="server" Display="None" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:Resource, reqvalPrdName %>" ControlToValidate="txtProdName"></asp:RequiredFieldValidator>
                </td>
            </tr>            
            <tr>
                <td valign="top" class="text" >
                    <asp:Label ID="lblProdDescr" runat="server" Text="<%$ Resources:Resource, lblDescription %>"
                        CssClass="lblBold"></asp:Label>
                </td>
                
                <td class="input tbx_editor">
                    <%--<ftb:freetextbox id="txtProdDescr" runat="server" width="540px" autoconfigure="Alternate"
                        toolbarbackgroundimage="false" toolbarbackcolor="#D2D2D5" gutterbackcolor="#D2D2D5"
                        backcolor="#D2D2D5" height="150"></ftb:freetextbox>--%>
                    <asp:TextBox ID="txtProdDescr" runat="server" TextMode="MultiLine" Width="540px" Height="150px" />
                    <asp:RequiredFieldValidator ID="reqvalProdDescr" runat="server" Display="None" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:Resource, reqvalPrdDescr %>" ControlToValidate="txtProdDescr"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td valign="top" class="text">
                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lblPrdTechDescription %>"
                        CssClass="lblBold"></asp:Label>
                </td>              
                <td class="input tbx_editor">
                    <%--<ftb:freetextbox id="txtProdTechDescr" runat="server" width="540px" autoconfigure="Alternate"
                        toolbarbackgroundimage="false" toolbarbackcolor="#D2D2D5" gutterbackcolor="#D2D2D5"
                        backcolor="#D2D2D5" height="150"></ftb:freetextbox>--%>
                    <asp:TextBox ID="txtProdTechDescr" runat="server" TextMode="MultiLine" Width="540px" Height="150px" />
                </td>
            </tr>
            <asp:ValidationSummary ID="sumvalDesc" runat="server" ShowSummary="false" ShowMessageBox="true">
            </asp:ValidationSummary>                    
        </tbody>
    </table>
    <div style="text-align: right; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave %>" 
            runat="server" onclick="btnSave_Click" />
    </div>
</asp:Panel>

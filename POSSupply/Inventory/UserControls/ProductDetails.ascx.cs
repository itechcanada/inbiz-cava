﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Inventory_UserControls_ProductDetails : System.Web.UI.UserControl, IProduct
{
    Product _prd = new Product();
    Product.ProductClothDesc _prdClothDesc = new Product.ProductClothDesc();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Security Check
        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY) && CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER) == false && CurrentUser.IsInRole(RoleID.ADMINISTRATOR) == false)
        {
            btnSave.Visible = false;
        }
        if(CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
        {
            liFOBLanded.Visible = false;
            txtSalPriceMinQua.Enabled = false;
            txtEndUserSalesPrice.Enabled = false;
            txtWebSalesPrice.Enabled = false;
            btnSave.Visible = false;
            btnDelete.Visible = false;
        }
        if (!Page.IsPostBack)
        {
            txtProdName.Focus();
        }
    }

    public void SetData()
    {
        _prd.ProductID = this.ProductID;
        _prd.PrdIntID = txtInternalID.Text;
        _prd.PrdExtID = string.Empty;
        _prd.PrdUPCCode = txtBarcode.Text;
        if ((string.IsNullOrEmpty(txtBarcode.Text)))
        {
            if ((txtProdName.Text.Length > 3))
            {
                _prd.PrdUPCCode = txtProdName.Text.Substring(0, 3).ToUpper();
                _prd.PrdUPCCode += "9999";
            }
            else
            {
                _prd.PrdUPCCode = txtProdName.Text.ToUpper();
                _prd.PrdUPCCode += "9999";
            }
        }
        else
        {
            _prd.PrdUPCCode = txtBarcode.Text;

        }
        _prd.PrdExtID = txtBarcode.Text;
        _prd.PrdType = (int)this.ProductType; //rblProductType.SelectedValue;
        _prd.PrdName = txtProdName.Text;
        _prd.PrdIsKit = rblstProKit.SelectedValue == "1";
        _prd.PrdMinQtyPerSO = 0;//BusinessUtility.GetInt(txtPrdMinQtyforSO.Text);
        _prd.PrdWeight = string.Empty;
        _prd.PrdInoclTerms = txtProdIncolTerms.Text;
        _prd.PrdCreatedUserID = CurrentUser.UserID;
        _prd.PrdLastUpdatedByUserID = CurrentUser.UserID;
        _prd.PrdSalePricePerMinQty = BusinessUtility.GetDouble(txtSalPriceMinQua.Text);
        _prd.PrdEndUserSalesPrice = BusinessUtility.GetDouble(txtEndUserSalesPrice.Text);
        _prd.PrdIsSpecial = rblstSpecial.SelectedValue == "1";
        //if (string.IsNullOrEmpty(txtBlanketDis.Text))
        //{
        //    txtBlanketDis.Text = "0";
        //    txtDiscount.Text = "0";
        //}
        _prd.PrdMinQtyPOTrig = BusinessUtility.GetInt(txtMinQtyforPO.Text);
        _prd.PrdAutoPO = rdlstPOFlag.SelectedValue == "1";
        _prd.PrdPOQty = BusinessUtility.GetInt(txtQuaToPOrd.Text);
        _prd.PrdIsActive = this.ProductType == StatusProductType.Accommodation ? true : rblstActive.SelectedValue == "1";
        _prd.PrdIsWeb = rdlstISWeb.SelectedValue == "1";
        _prd.PrdComissionCode = string.Empty; //txtProComissioncode.Text;
        _prd.IsPOSMenu = rdlstISPOSMenu.SelectedValue == "1";
        _prd.PrdWebSalesPrice = BusinessUtility.GetDouble(txtWebSalesPrice.Text);
        _prd.PrdDiscountType = "P"; //rblstDiscountType.SelectedValue;
        _prd.PrdDiscount = 0;
        //if (rblstDiscountType.SelectedValue == "P")
        //{
        //    _prd.PrdDiscount = BusinessUtility.GetInt(txtDiscount.Text.TrimEnd('%'));
        //}
        //else
        //{
        //    _prd.PrdDiscount = BusinessUtility.GetInt(txtDiscount.Text);
        //}        
        _prd.PrdHeight = txtHeight.Text;
        _prd.PrdHeightPkg = txtPkgHeigth.Text;
        _prd.PrdLength = txtLength.Text;
        _prd.PrdLengthPkg = txtPkgLength.Text;
        _prd.PrdWidth = txtWidth.Text;
        _prd.PrdWidthPkg = txtPkgWidth.Text;
        _prd.PrdWeight = txtWeight.Text;
        _prd.PrdWeightPkg = txtPkgWeight.Text;
        _prd.PrdExtendedCategory = BusinessUtility.GetInt(ddlRoom.SelectedValue);
        // For Resturant Detail
        _prd.PrdIsGlutenFree = rblGlutenFree.SelectedValue == "1";
        _prd.PrdIsVegetarian = rblVegetarian.SelectedValue == "1";
        _prd.PrdIsContainsNuts = rblContainsNuts.SelectedValue == "1";
        _prd.PrdIsCookedSushi = rblCookedSushi.SelectedValue == "1";
        _prd.PrdSpicyLevel = BusinessUtility.GetInt(ddlSpicy.SelectedValue);
        _prd.PrdFOBPrice = BusinessUtility.GetDouble(txtFOBPrice.Text);
        _prd.PrdLandedPrice = BusinessUtility.GetDouble(txtLandedPrice.Text);
        //_prd.PrdAccommodationType = BusinessUtility.GetInt(ddlAccommodationType.SelectedValue);

        // Set Product Clot Desc
        _prdClothDesc.ProductID = this.ProductID;
        _prdClothDesc.MasterID = BusinessUtility.GetInt(hdnMasterID.Value);
        _prdClothDesc.CollectionID = BusinessUtility.GetInt(hdnCollectionID.Value);
        //_prdClothDesc.Style = ddlStyle.SelectedItem.Value;
        //_prdClothDesc.Neckline = ddlNeckline.SelectedItem.Value;
        //_prdClothDesc.Sleeve = ddlSleeve.SelectedItem.Value;
        //_prdClothDesc.Silhouette = ddlSilhouette.SelectedItem.Value;
        //_prdClothDesc.Size = ddlSize.SelectedItem.Value;
        //_prdClothDesc.JupesPantalons = ddlJupesPantalons.SelectedItem.Value;
        //_prdClothDesc.Gauge = BusinessUtility.GetInt(txtGauage.Text);
        //_prdClothDesc.Texture = BusinessUtility.GetString(txtTexture.Text);
        //_prdClothDesc.LongueurLength = BusinessUtility.GetInt(txtLongueurLength.Text);
        //_prdClothDesc.EpauleShoulder = BusinessUtility.GetInt(txtEpauleShoulder.Text);
        //_prdClothDesc.BusteChest = BusinessUtility.GetInt(txtBusteChest.Text);
        //_prdClothDesc.Bicep = BusinessUtility.GetInt(txtBicep.Text);
        //_prdClothDesc.Material = BusinessUtility.GetString(txtMaterial.Text);
        //_prdClothDesc.Micron = BusinessUtility.GetInt(txtMicron.Text);


        _prdClothDesc.Style = txtStyle.Text;
        _prdClothDesc.Size = BusinessUtility.GetInt(ddlSize.SelectedItem.Value);
        //_prdClothDesc.JupesPantalons = ddlJupesPantalons.SelectedItem.Value;
        //_prdClothDesc.Gauge = BusinessUtility.GetInt(txtGauge.Text);
        //_prdClothDesc.Texture = BusinessUtility.GetInt(ddlTexture.SelectedItem.Value);
        _prdClothDesc.LongueurLength = BusinessUtility.GetString(txtLongueurLength.Text);
        _prdClothDesc.EpauleShoulder = BusinessUtility.GetString(txtEpauleShoulder.Text);
        _prdClothDesc.BusteChest = BusinessUtility.GetString(txtBusteChest.Text);
        _prdClothDesc.Bicep = BusinessUtility.GetString(txtBicep.Text);
        _prdClothDesc.Material = BusinessUtility.GetInt(ddlMaterial.SelectedItem.Value);
        //_prdClothDesc.Micron = BusinessUtility.GetInt(txtMicron.Text);
        _prdClothDesc.CollectionID = BusinessUtility.GetInt(ddlCollection.SelectedItem.Value);
        _prdClothDesc.Color = BusinessUtility.GetInt(ddlColor.SelectedItem.Value);
        _prd.PrdIsGiftCardProduct = rdlstISGiftCardProduct.SelectedValue == "1";
        _prd.PrdIsNonStandard = chkIsNonStandard.Checked == true;


        //_prdClothDesc.Neckline = BusinessUtility.GetInt(ddlNeckline.SelectedItem.Value);
        //_prdClothDesc.Sleeve = BusinessUtility.GetInt(ddlSleeve.SelectedItem.Value);
        //_prdClothDesc.Silhouette = BusinessUtility.GetInt(ddlSilhouette.SelectedItem.Value);
        //_prdClothDesc.ExtraCatg = BusinessUtility.GetInt(ddlExtraCategory.SelectedItem.Value);


        var lstNickLine = new List<int>();
        string sNickLine = "";
        int id = 0;
        foreach (ListItem item in ddlNeckline.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstNickLine.Contains(id))
            {
                lstNickLine.Add(id);
                if (sNickLine != "")
                    sNickLine += " ," + BusinessUtility.GetString(id);
                else
                {
                    sNickLine = BusinessUtility.GetString(id);
                }
            }
        }
        _prdClothDesc.Neckline = sNickLine;



        var lstGauge = new List<int>();
        string sGauge = "";
        id = 0;
        foreach (ListItem item in ddlGauge.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstGauge.Contains(id))
            {
                lstGauge.Add(id);
                if (sGauge != "")
                    sGauge += " ," + BusinessUtility.GetString(id);
                else
                {
                    sGauge = BusinessUtility.GetString(id);
                }
            }
        }
        _prdClothDesc.Gauge = sGauge;



        var lstTexture = new List<int>();
        string sTexture = "";
        id = 0;
        foreach (ListItem item in ddlTexture.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstTexture.Contains(id))
            {
                lstTexture.Add(id);
                if (sTexture != "")
                    sTexture += " ," + BusinessUtility.GetString(id);
                else
                {
                    sTexture = BusinessUtility.GetString(id);
                }
            }
        }
        _prdClothDesc.Texture = sTexture;



        var lstExtraCatg = new List<int>();
        string sExtraCatg = "";
        id = 0;
        foreach (ListItem item in ddlExtraCategory.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstExtraCatg.Contains(id))
            {
                lstExtraCatg.Add(id);
                if (sExtraCatg != "")
                    sExtraCatg += " ," + BusinessUtility.GetString(id);
                else
                {
                    sExtraCatg = BusinessUtility.GetString(id);
                }
            }
        }
        _prdClothDesc.ExtraCatg = sExtraCatg;




        var lstSleeve = new List<int>();
        string sSleeve = "";
        id = 0;
        foreach (ListItem item in ddlSleeve.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstSleeve.Contains(id))
            {
                lstSleeve.Add(id);
                if (sSleeve != "")
                    sSleeve += " ," + BusinessUtility.GetString(id);
                else
                {
                    sSleeve = BusinessUtility.GetString(id);
                }
            }
        }
        _prdClothDesc.Sleeve = sSleeve;


        var lstSilhouette = new List<int>();
        string sSilhouette = "";
        id = 0;
        foreach (ListItem item in ddlSilhouette.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstSilhouette.Contains(id))
            {
                lstSilhouette.Add(id);
                if (sSilhouette != "")
                    sSilhouette += " ," + BusinessUtility.GetString(id);
                else
                {
                    sSilhouette = BusinessUtility.GetString(id);
                }
            }
        }
        _prdClothDesc.Silhouette = sSilhouette;


        var lstTrend = new List<int>();
        string sTrend = "";
        id = 0;
        foreach (ListItem item in ddlTrend.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstTrend.Contains(id))
            {
                lstTrend.Add(id);
                if (sTrend != "")
                    sTrend += " ," + BusinessUtility.GetString(id);
                else
                {
                    sTrend = BusinessUtility.GetString(id);
                }
            }
        }
        _prdClothDesc.Trend = sTrend;

        var lstKeyWord = new List<int>();
        string sKeyWord = "";
        id = 0;
        foreach (ListItem item in ddlKeyWord.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstKeyWord.Contains(id))
            {
                lstKeyWord.Add(id);
                if (sKeyWord != "")
                    sKeyWord += " ," + BusinessUtility.GetString(id);
                else
                {
                    sKeyWord = BusinessUtility.GetString(id);
                }
            }
        }
        _prdClothDesc.KeyWord = sKeyWord;



        var lstWebSiteCatg = new List<int>();
        string sWebSiteCatg = "";
        id = 0;
        foreach (ListItem item in ddlCategory.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstWebSiteCatg.Contains(id))
            {
                lstWebSiteCatg.Add(id);
                if (sWebSiteCatg != "")
                    sWebSiteCatg += " ," + BusinessUtility.GetString(id);
                else
                {
                    sWebSiteCatg = BusinessUtility.GetString(id);
                }
            }
        }
        _prdClothDesc.WebSiteCatg = sWebSiteCatg;



        var lstWebSiteSubCatg = new List<int>();
        string sWebSiteSubCatg = "";
        id = 0;
        foreach (ListItem item in ddlWebSiteSubCatg.Items)
        {
            id = 0;
            if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lstWebSiteSubCatg.Contains(id))
            {
                lstWebSiteSubCatg.Add(id);
                if (sWebSiteSubCatg != "")
                    sWebSiteSubCatg += " ," + BusinessUtility.GetString(id);
                else
                {
                    sWebSiteSubCatg = BusinessUtility.GetString(id);
                }
            }
        }
        _prdClothDesc.WebSiteSubCatg = sWebSiteSubCatg;

        var lstWebAssociateStyle = new List<string>();
        string sWebAssociateStyle = "";
        string sStyle = "";
        foreach (ListItem item in ddlAssociatedStyle.Items)
        {
            id = 0;
            if (item.Selected && !lstWebAssociateStyle.Contains(sStyle))
            {
                lstWebAssociateStyle.Add(sStyle);
                if (sWebAssociateStyle != "")
                    sWebAssociateStyle += " ," + BusinessUtility.GetString(id);
                else
                {
                    sWebAssociateStyle = BusinessUtility.GetString(id);
                }
            }
        }
        _prdClothDesc.WebAssociatedStyle = sWebAssociateStyle;

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //Product Bar Code Duplicay During Product Creation or Editing
        _prd.ProductID = this.ProductID;
        _prd.PrdUPCCode = txtBarcode.Text;
        if (_prd.IsProductUPCCodeDuplicate() == true)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, "Bar Code Already Exist...");
            return;
        }
        SetData();

        /*if (rblstDiscountType.SelectedValue == "P" & txtBlanketDis.Text != "0")
        {
            if (BusinessUtility.GetDouble(txtBlanketDis.Text.TrimEnd('%')) > 100)
            {
                MessageState.SetGlobalMessage(MessageType.Information, Resources.Resource.msgDiscountgreaterThen100);
                //"Discount should not be greater than 100%."
                txtBlanketDis.Focus();
                return;
            }
        }
        else
        {
            if (BusinessUtility.GetDouble(txtBlanketDis.Text.TrimEnd('%')) == 0)
            {
                MessageState.SetGlobalMessage(MessageType.Information, Resources.Resource.msgDicountNuValue);
                //"Please enter numeric value for Discount."
                txtBlanketDis.Focus();
                return;
            }
        }*/

        try
        {
            if (this.ProductID > 0)
            {
                _prd.Update(CurrentUser.UserID);
                _prdClothDesc.InsertUpdateClothDesc();
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgPrdUpdated);
                Response.Redirect(QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.Description).ToString()), false);
                //Response.Redirect(Request.RawUrl, false);
            }
            else
            {
                _prd.Insert(CurrentUser.UserID);
                _prdClothDesc.ProductID = _prd.ProductID;
                _prdClothDesc.InsertUpdateClothDesc();
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgPrdSaved);
                if (this.VendorID > 0)
                {
                    Response.Redirect(string.Format("~/Procurement/Create.aspx?vdrID={0}&ComID={1}&whsID={2}&BarCode={3}", this.VendorID, Request.QueryString["ComID"], Request.QueryString["whsID"], _prd.PrdUPCCode), false);
                }
                string url = string.Format("~/Inventory/Product.aspx?PrdID={0}&Kit={1}&section={2}&pType={3}", _prd.ProductID, _prd.PrdIsKit, (int)InventorySectionKey.Description, (int)this.ProductType);
                Response.Redirect(url, false);
            }
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.PRODUCT_ALREADY_EXISTS)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgProductNameAlreadyExists);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (this.VendorID > 0)
        {
            Response.Redirect(string.Format("~/Procurement/Create.aspx?vdrID={0}&ComID={1}&whsID={2}", this.VendorID, Request.QueryString["ComID"], Request.QueryString["whsID"]));
        }
        else
        {
            Response.Redirect(string.Format("~/Inventory/ViewProduct.aspx?pType={0}", (int)this.ProductType));
        }
    }

    private int VendorID
    {
        get
        {
            int vdrid = 0;
            int.TryParse(Request.QueryString["vdrID"], out vdrid);
            return vdrid;
        }
    }

    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    public StatusProductType ProductType
    {
        get
        {
            return QueryStringHelper.GetProductType("ptype");
        }
    }

    public string SequenceType
    {
        get
        {
            switch (this.ProductType)
            {
                case StatusProductType.Product:
                    return "Product";
                case StatusProductType.Accommodation:
                    return "Accommodation Product";
                case StatusProductType.ServiceProduct:
                    return "Service Product";
                case StatusProductType.AdminFee:
                    return "Admin Fee Product";
                case StatusProductType.ChildUnder12:
                    return "Children Product";
                case StatusProductType.CourseProduct:
                    return "Course Product";
                default:
                    return "";
            }
        }
    }

    public string SequencePfx
    {
        get
        {
            switch (this.ProductType)
            {
                case StatusProductType.Accommodation:
                    return "BED";
                case StatusProductType.ServiceProduct:
                    return "SVC";
                case StatusProductType.AdminFee:
                    return "";
                case StatusProductType.ChildUnder12:
                    return "";
                case StatusProductType.CourseProduct:
                    return "COU";
                case StatusProductType.Product:
                default:
                    return "";
            }
        }
    }

    public string SequenceFormat
    {
        get
        {
            switch (this.ProductType)
            {
                case StatusProductType.Accommodation:
                    return "{0}-{1:000000}";
                case StatusProductType.ServiceProduct:
                    return "{0}-{1:000000}";
                case StatusProductType.AdminFee:
                    return "{0}";
                case StatusProductType.ChildUnder12:
                    return "{0}";
                case StatusProductType.CourseProduct:
                    return "{0}-{1:000000}";
                case StatusProductType.Product:
                default:
                    return "{0:000000}";
            }
        }
    }

    public void Initialize()
    {
        if (!IsPostBack)
        {
            btnDelete.Visible = this.ProductID > 0;
            //if (this.ProductType == StatusProductType.AdminFee && this.ProductID <=0)
            //{
            //    int prdID = _prd.GetAdminFeeProductID();
            //    Response.Redirect(QueryString.GetModifiedUrl("PrdID", prdID.ToString()));
            //}

            ltSectionTitle.Text = Resources.Resource.btnProductDetail;

            DropDownHelper.FillBuildings(ddlBuilding, new ListItem("--Building--", "0"), Globals.CurrentAppLanguageCode);

            //DropDownHelper.FillDropdown(null, ddlStyle, "PD", "ddlSt", Globals.CurrentAppLanguageCode, new ListItem(""));
            //DropDownHelper.FillDropdown(null, ddlNeckline, "PD", "ddlNe", Globals.CurrentAppLanguageCode, new ListItem(""));
            ProductCategory objProductCatg = new ProductCategory();
            objProductCatg.CatgHrdID = 2;
            var lst = objProductCatg.GetCategoryList(null, Globals.CurrentAppLanguageCode);
            ListItem lstBlank = new ListItem();
            lstBlank.Text = "";
            lstBlank.Value = "";
            ddlNeckline.Items.Clear();
            ddlNeckline.Items.Add(lstBlank);
            foreach (var item in lst)
            {
                ListItem lstNew = new ListItem();
                lstNew.Text = item.CatgName;
                lstNew.Value = BusinessUtility.GetString(item.CatgID);
                ddlNeckline.Items.Add(lstNew);
            }


            objProductCatg = new ProductCategory();
            objProductCatg.CatgHrdID = 9;
            lst = objProductCatg.GetCategoryList(null, Globals.CurrentAppLanguageCode);
            ddlGauge.Items.Clear();

            ddlGauge.Items.Add(lstBlank);
            foreach (var item in lst)
            {
                ListItem lstNew = new ListItem();
                lstNew.Text = item.CatgName;
                lstNew.Value = BusinessUtility.GetString(item.CatgID);
                ddlGauge.Items.Add(lstNew);
            }


            objProductCatg = new ProductCategory();
            objProductCatg.CatgHrdID = 3;
            lst = objProductCatg.GetCategoryList(null, Globals.CurrentAppLanguageCode);
            ddlExtraCategory.Items.Clear();

            ddlExtraCategory.Items.Add(lstBlank);
            foreach (var item in lst)
            {
                ListItem lstNew = new ListItem();
                lstNew.Text = item.CatgName;
                lstNew.Value = BusinessUtility.GetString(item.CatgID);
                ddlExtraCategory.Items.Add(lstNew);
            }

            //DropDownHelper.FillDropdown(null, ddlSleeve, "PD", "ddlSl", Globals.CurrentAppLanguageCode, new ListItem(""));
            objProductCatg = new ProductCategory();
            objProductCatg.CatgHrdID = 4;
            lst = objProductCatg.GetCategoryList(null, Globals.CurrentAppLanguageCode);
            ddlSleeve.Items.Clear();

            ddlSleeve.Items.Add(lstBlank);
            foreach (var item in lst)
            {
                ListItem lstNew = new ListItem();
                lstNew.Text = item.CatgName;
                lstNew.Value = BusinessUtility.GetString(item.CatgID);
                ddlSleeve.Items.Add(lstNew);
            }


            //DropDownHelper.FillDropdown(null, ddlSilhouette, "PD", "ddlSi", Globals.CurrentAppLanguageCode, new ListItem(""));
            objProductCatg = new ProductCategory();
            objProductCatg.CatgHrdID = 5;
            lst = objProductCatg.GetCategoryList(null, Globals.CurrentAppLanguageCode);
            ddlSilhouette.Items.Clear();
            ddlSilhouette.Items.Add(lstBlank);
            foreach (var item in lst)
            {
                ListItem lstNew = new ListItem();
                lstNew.Text = item.CatgName;
                lstNew.Value = BusinessUtility.GetString(item.CatgID);
                ddlSilhouette.Items.Add(lstNew);
            }


            objProductCatg = new ProductCategory();
            objProductCatg.CatgHrdID = 6;
            lst = objProductCatg.GetCategoryList(null, Globals.CurrentAppLanguageCode);
            ddlTrend.Items.Clear();
            ddlTrend.Items.Add(lstBlank);
            foreach (var item in lst)
            {
                ListItem lstNew = new ListItem();
                lstNew.Text = item.CatgName;
                lstNew.Value = BusinessUtility.GetString(item.CatgID);
                ddlTrend.Items.Add(lstNew);
            }

            objProductCatg = new ProductCategory();
            objProductCatg.CatgHrdID = 7;
            lst = objProductCatg.GetCategoryList(null, Globals.CurrentAppLanguageCode);
            ddlKeyWord.Items.Clear();
            ddlKeyWord.Items.Add(lstBlank);
            foreach (var item in lst)
            {
                ListItem lstNew = new ListItem();
                lstNew.Text = item.CatgName;
                lstNew.Value = BusinessUtility.GetString(item.CatgID);
                ddlKeyWord.Items.Add(lstNew);
            }


            objProductCatg = new ProductCategory();
            objProductCatg.CatgHrdID = 1;
            lst = objProductCatg.GetCategoryList(null, Globals.CurrentAppLanguageCode);
            ddlCategory.Items.Clear();
            ddlCategory.Items.Add(lstBlank);
            foreach (var item in lst)
            {
                ListItem lstNew = new ListItem();
                lstNew.Text = item.CatgName;
                lstNew.Value = BusinessUtility.GetString(item.CatgID);
                ddlCategory.Items.Add(lstNew);
            }


            objProductCatg = new ProductCategory();
            objProductCatg.CatgHrdID = 8;
            lst = objProductCatg.GetCategoryList(null, Globals.CurrentAppLanguageCode);
            ddlWebSiteSubCatg.Items.Clear();
            ddlWebSiteSubCatg.Items.Add(lstBlank);
            foreach (var item in lst)
            {
                ListItem lstNew = new ListItem();
                lstNew.Text = item.CatgName;
                lstNew.Value = BusinessUtility.GetString(item.CatgID);
                ddlWebSiteSubCatg.Items.Add(lstNew);
            }


            ProductSize objProductSize = new ProductSize();
            var lstSize = objProductSize.GetSize(null, Globals.CurrentAppLanguageCode);

            //ddlSize.Items.Clear();
            //ddlSize.Items.Add(lstBlank);
            foreach (var item in lstSize)
            {
                ListItem lstNew = new ListItem();
                lstNew.Text = item.Size;
                lstNew.Value = BusinessUtility.GetString(item.SizeID);
                ddlSize.Items.Add(lstNew);
            }


            Collection objCollction = new Collection();
            var lstCollection = objCollction.GetCollectionList(null, Globals.CurrentAppLanguageCode);
            //ddlCollection.Items.Clear();
            //ddlCollection.Items.Add(lstBlank);
            foreach (var item in lstCollection)
            {
                ListItem lstNew = new ListItem();
                lstNew.Text = item.ShortName;
                lstNew.Value = BusinessUtility.GetString(item.CollectionID);
                ddlCollection.Items.Add(lstNew);
            }


            ProductTexture objTexture = new ProductTexture();
            var lstTexture = objTexture.GetTextureList(null, Globals.CurrentAppLanguageCode);
            //ddlTexture.Items.Clear();
            //ddlTexture.Items.Add(lstBlank);
            foreach (var item in lstTexture)
            {
                ListItem lstNew = new ListItem();
                lstNew.Text = item.TextureName;
                lstNew.Value = BusinessUtility.GetString(item.TextureID);
                ddlTexture.Items.Add(lstNew);
            }


            ProductColor objProductColor = new ProductColor();
            var lstColor = objProductColor.GetColorList(null, Globals.CurrentAppLanguageCode);
            //ddlColor.Items.Clear();
            //ddlColor.Items.Add(lstBlank);
            foreach (var item in lstColor)
            {
                ListItem lstNew = new ListItem();
                lstNew.Text = item.ColorName;
                lstNew.Value = BusinessUtility.GetString(item.ColorID);
                ddlColor.Items.Add(lstNew);
            }


            ProductMaterial objMaterial = new ProductMaterial();
            var lstMaterial = objMaterial.GetMaterialList(null, Globals.CurrentAppLanguageCode);
            //ddlMaterial.Items.Clear();
            //ddlMaterial.Items.Add(lstBlank);
            foreach (var item in lstMaterial)
            {
                ListItem lstNew = new ListItem();
                lstNew.Text = item.MaterialName;
                lstNew.Value = BusinessUtility.GetString(item.MaterialID);
                ddlMaterial.Items.Add(lstNew);
            }

            //DropDownHelper.FillDropdown(null, ddlSilhouette, "PD", "ddlSi", Globals.CurrentAppLanguageCode, new ListItem(""));
            //DropDownHelper.FillDropdown(null, ddlSize, "PD", "ddlSz", Globals.CurrentAppLanguageCode, new ListItem(""));
            //DropDownHelper.FillDropdown(null, ddlJupesPantalons, "PD", "ddlJu", Globals.CurrentAppLanguageCode, new ListItem(""));

            this.SetMassLengthUnitNames();

            if (this.ProductID > 0)
            {
                _prd.PopulateObject(this.ProductID);
                _prd.GetDefectiveAvailableQty(null, this.ProductID);
                valDefectiveQty.Text = _prd.DefectiveQty.ToString();
                valAvailableQty.Text = _prd.AvailableQty.ToString();
                valInTransit.Text = ProcessInventory.GetInTransitProductQuantity(this.ProductID,"").ToString();
                ltSectionTitle.Text = _prd.PrdName + " / " + Resources.Resource.btnProductDetail;
                valInComingOrder.Text = ProcessInventory.GetInComingOrdQuantity(this.ProductID, "").ToString();

                if (_prd.PrdExtendedCategory > 0)
                {
                    Rooms categ = new Rooms();
                    categ.PopulateObject(_prd.PrdExtendedCategory);
                    FillRoomsDdl(categ.BuildingID);
                    ddlBuilding.SelectedValue = categ.BuildingID.ToString();
                    ddlRoom.SelectedValue = _prd.PrdExtendedCategory.ToString();
                }

                txtInternalID.Text = _prd.PrdIntID;
                txtBarcode.Text = _prd.PrdUPCCode;
                txtProdName.Text = _prd.PrdName;
                rblstProKit.SelectedValue = _prd.PrdIsKit ? "1" : "0";
                //txtPrdMinQtyforSO.Text = _prd.PrdMinQtyPerSO.ToString();                        
                txtProdIncolTerms.Text = _prd.PrdInoclTerms;
                //txtSalPriceMinQua.Text = string.Format("{0:F}", _prd.PrdSalePricePerMinQty);
                //txtEndUserSalesPrice.Text = string.Format("{0:F}", _prd.PrdEndUserSalesPrice);
                 //txtSalPriceMinQua.Text =  _prd.PrdSalePricePerMinQty.ToString("#.00");
                txtSalPriceMinQua.Text = CurrencyFormat.GetAmountInUSCulture(_prd.PrdSalePricePerMinQty);
                txtEndUserSalesPrice.Text = CurrencyFormat.GetAmountInUSCulture(_prd.PrdEndUserSalesPrice);
                rblstSpecial.SelectedValue = _prd.PrdIsSpecial ? "1" : "0";
                //txtBlanketDis.Text = _prd.PrdDiscount.ToString();
                txtMinQtyforPO.Text = _prd.PrdMinQtyPOTrig.ToString();
                rdlstPOFlag.SelectedValue = _prd.PrdAutoPO ? "1" : "0";
                txtQuaToPOrd.Text = _prd.PrdPOQty.ToString();
                rblstActive.SelectedValue = _prd.PrdIsActive ? "1" : "0";
                rdlstISWeb.SelectedValue = _prd.PrdIsWeb ? "1" : "0";
                //txtProComissioncode.Text = _prd.PrdComissionCode;
                rdlstISPOSMenu.SelectedValue = _prd.IsPOSMenu ? "1" : "0";
                //txtWebSalesPrice.Text = string.Format("{0:F}", _prd.PrdWebSalesPrice);
                txtWebSalesPrice.Text = CurrencyFormat.GetAmountInUSCulture(_prd.PrdWebSalesPrice);
                //rblstDiscountType.SelectedValue = _prd.PrdDiscountType;
                //txtDiscount.Text = _prd.PrdDiscount.ToString();
                //if (rblstDiscountType.SelectedValue == "P")
                //{
                //    txtBlanketDis.Text = txtDiscount.Text + "%";
                //}
                txtWeight.Text = _prd.PrdWeight;
                txtPkgWeight.Text = _prd.PrdWeightPkg;
                txtHeight.Text = _prd.PrdHeight;
                txtPkgHeigth.Text = _prd.PrdHeightPkg;
                txtLength.Text = _prd.PrdLength;
                txtPkgLength.Text = _prd.PrdLengthPkg;
                txtWidth.Text = _prd.PrdWidth;
                txtPkgWidth.Text = _prd.PrdWidthPkg;


                // For Resturant Detail
                rblGlutenFree.SelectedValue = _prd.PrdIsGlutenFree ? "1" : "0";
                rblVegetarian.SelectedValue = _prd.PrdIsVegetarian ? "1" : "0";
                rblContainsNuts.SelectedValue = _prd.PrdIsContainsNuts ? "1" : "0";
                rblCookedSushi.SelectedValue = _prd.PrdIsCookedSushi ? "1" : "0";
                //txtFOBPrice.Text = string.Format("{0:F}", _prd.PrdFOBPrice);
                //txtLandedPrice.Text = string.Format("{0:F}", _prd.PrdLandedPrice);
                txtFOBPrice.Text = CurrencyFormat.GetAmountInUSCulture(_prd.PrdFOBPrice);
                txtLandedPrice.Text = CurrencyFormat.GetAmountInUSCulture(_prd.PrdLandedPrice);
                hdnCollectionID.Value = BusinessUtility.GetString(_prd.CollectionID);
                hdnMasterID.Value = BusinessUtility.GetString(_prd.MasterID);

                rdlstISGiftCardProduct.SelectedValue = _prd.PrdIsGiftCardProduct ? "1" : "0";
                chkIsNonStandard.Checked = _prd.PrdIsNonStandard;


                if (_prd.PrdSpicyLevel >= 0)
                {
                    ddlSpicy.SelectedValue = _prd.PrdSpicyLevel.ToString();
                }
                btnRefresh.Visible = false;
                //ddlAccommodationType.SelectedValue = _prd.PrdAccommodationType.ToString();

                // Get Product Cloth Desc

                Product.ProductClothDesc objprdClothdesc = new Product.ProductClothDesc();
                objprdClothdesc.ProductID = this.ProductID;
                objprdClothdesc.CollectionID = _prd.CollectionID;
                objprdClothdesc.MasterID = _prd.MasterID;
                objprdClothdesc.getClothDesc(null, this.ProductID);

                txtStyle.Text = objprdClothdesc.Style;

                if (ddlSize.Items.Count > 0)
                {
                    ddlSize.SelectedIndex = ddlSize.Items.IndexOf(ddlSize.Items.FindByValue(BusinessUtility.GetString(objprdClothdesc.Size)));
                    //ddlSize.SelectedValue  = BusinessUtility.GetString(objprdClothdesc.Size);
                }


                if (ddlCollection.Items.Count > 0)
                {
                    ddlCollection.SelectedIndex = ddlCollection.Items.IndexOf(ddlCollection.Items.FindByValue(BusinessUtility.GetString(objprdClothdesc.Collection)));
                    ddlCollection_OnSelectedIndexChanged(null, null);
                }




                if (ddlMaterial.Items.Count > 0)
                {
                    ddlMaterial.SelectedIndex = ddlMaterial.Items.IndexOf(ddlMaterial.Items.FindByValue(BusinessUtility.GetString(objprdClothdesc.Material)));
                }

                if (ddlTexture.Items.Count > 0)
                {
                    ddlTexture.SelectedIndex = ddlTexture.Items.IndexOf(ddlTexture.Items.FindByValue(BusinessUtility.GetString(objprdClothdesc.Texture)));
                }




                if (ddlColor.Items.Count > 0)
                {
                    ddlColor.SelectedIndex = ddlColor.Items.IndexOf(ddlColor.Items.FindByValue(BusinessUtility.GetString(objprdClothdesc.Color)));
                }


                string[] sNickline = BusinessUtility.GetString(objprdClothdesc.Neckline).Split(',');
                foreach (string item in sNickline)
                {
                    if (item != "")
                    {
                        ListItem li = ddlNeckline.Items.FindByValue(BusinessUtility.GetString(item));
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }
                }


                string[] sGauge = BusinessUtility.GetString(objprdClothdesc.Gauge).Split(',');
                foreach (string item in sGauge)
                {
                    if (item != "")
                    {
                        ListItem li = ddlGauge.Items.FindByValue(BusinessUtility.GetString(item));
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }
                }


                string[] sTexture = BusinessUtility.GetString(objprdClothdesc.Texture).Split(',');
                foreach (string item in sTexture)
                {
                    if (item != "")
                    {
                        ListItem li = ddlTexture.Items.FindByValue(BusinessUtility.GetString(item));
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }
                }


                string[] sExtraCatg = BusinessUtility.GetString(objprdClothdesc.ExtraCatg).Split(',');
                foreach (string item in sExtraCatg)
                {
                    if (item != "")
                    {
                        ListItem li = ddlExtraCategory.Items.FindByValue(BusinessUtility.GetString(item));
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }
                }

                string[] sSleeve = BusinessUtility.GetString(objprdClothdesc.Sleeve).Split(',');
                foreach (string item in sSleeve)
                {
                    if (item != "")
                    {
                        ListItem li = ddlSleeve.Items.FindByValue(BusinessUtility.GetString(item));
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }
                }

                string[] sSilhouette = BusinessUtility.GetString(objprdClothdesc.Silhouette).Split(',');
                foreach (string item in sSilhouette)
                {
                    if (item != "")
                    {
                        ListItem li = ddlSilhouette.Items.FindByValue(BusinessUtility.GetString(item));
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }
                }


                string[] sTrend = BusinessUtility.GetString(objprdClothdesc.Trend).Split(',');
                foreach (string item in sTrend)
                {
                    if (item != "")
                    {
                        ListItem li = ddlTrend.Items.FindByValue(BusinessUtility.GetString(item));
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }
                }


                string[] sKeyWord = BusinessUtility.GetString(objprdClothdesc.KeyWord).Split(',');
                foreach (string item in sKeyWord)
                {
                    if (item != "")
                    {
                        ListItem li = ddlKeyWord.Items.FindByValue(BusinessUtility.GetString(item));
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }
                }



                string[] sWebSiteCatg = BusinessUtility.GetString(objprdClothdesc.WebSiteCatg).Split(',');
                foreach (string item in sWebSiteCatg)
                {
                    if (item != "")
                    {
                        ListItem li = ddlCategory.Items.FindByValue(BusinessUtility.GetString(item));
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }
                }


                string[] sWebSiteSubCatg = BusinessUtility.GetString(objprdClothdesc.WebSiteSubCatg).Split(',');
                foreach (string item in sWebSiteSubCatg)
                {
                    if (item != "")
                    {

                        ListItem li = ddlWebSiteSubCatg.Items.FindByValue(BusinessUtility.GetString(item));
                        if (li != null)
                        {
                            li.Selected = true;
                        }

                    }
                }


                string[] sWebAssociatedStyle = BusinessUtility.GetString(objprdClothdesc.WebAssociatedStyle).Split(',');
                foreach (string item in sWebAssociatedStyle)
                {
                    if (item != "")
                    {
                        ListItem li = ddlAssociatedStyle.Items.FindByValue(BusinessUtility.GetString(item));
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }
                }


                //if (ddlJupesPantalons.Items.Count > 0)
                //{
                //    ddlJupesPantalons.SelectedIndex = ddlJupesPantalons.Items.IndexOf(ddlJupesPantalons.Items.FindByValue(objprdClothdesc.JupesPantalons));
                //}
                //txtGauage.Text = BusinessUtility.GetString(objprdClothdesc.Gauge);
                //txtTexture.Text = BusinessUtility.GetString(objprdClothdesc.Texture);
                txtLongueurLength.Text = BusinessUtility.GetString(objprdClothdesc.LongueurLength);
                txtEpauleShoulder.Text = BusinessUtility.GetString(objprdClothdesc.EpauleShoulder);
                txtBusteChest.Text = BusinessUtility.GetString(objprdClothdesc.BusteChest);
                txtBicep.Text = BusinessUtility.GetString(objprdClothdesc.Bicep);
                //txtMaterial.Text = BusinessUtility.GetString(objprdClothdesc.Material);
                //txtMicron.Text = BusinessUtility.GetString(objprdClothdesc.Micron);
            }
            else
            {
                //POS menu default selection while on add new product screeen
                rdlstISPOSMenu.SelectedValue = this.ProductType == StatusProductType.Product ? "1" : "0";
            }
        }

        //Set visivility by Product Type
        liAccomodation1.Visible = this.ProductType == StatusProductType.Accommodation;
        //liAccomodation2.Visible = this.ProductType == StatusProductType.Accommodation;
        liProduct10.Visible = this.ProductType == StatusProductType.Product;
        liProduct8.Visible = this.ProductType == StatusProductType.Product;

        if (IsResturant() == false)
        {
            liProduct1.Visible = this.ProductType == StatusProductType.Product;
            liProduct2.Visible = this.ProductType == StatusProductType.Product;
            liProduct3.Visible = this.ProductType == StatusProductType.Product;
            liProduct4.Visible = this.ProductType == StatusProductType.Product;
            liProduct5.Visible = this.ProductType == StatusProductType.Product;
            liProduct7.Visible = this.ProductType == StatusProductType.Product;
        }
        else if (IsResturant() == true)
        {
            liResturant1.Visible = true;
            liResturant2.Visible = true;
            liResturant3.Visible = true;
            dvlblIsKit.Visible = false;
            dvrblstProKit.Visible = false;
        }


        switch (this.ProductType)
        {
            case StatusProductType.Product:
                ltSectionTitle.Text = !string.IsNullOrEmpty(_prd.PrdName) ? _prd.PrdName + " / " + Resources.Resource.btnProductDetail : Resources.Resource.btnProductDetail;
                break;
            case StatusProductType.Accommodation:
                ltSectionTitle.Text = !string.IsNullOrEmpty(_prd.PrdName) ? _prd.PrdName + " / " + Resources.Resource.lblAccommodationDetails : Resources.Resource.lblAccommodationDetails;
                break;
            case StatusProductType.ServiceProduct:
                ltSectionTitle.Text = !string.IsNullOrEmpty(_prd.PrdName) ? _prd.PrdName + " / " + Resources.Resource.lblServiceDetails : Resources.Resource.lblServiceDetails;
                break;
            case StatusProductType.AdminFee:
                break;
            case StatusProductType.ChildUnder12:
                break;
            case StatusProductType.CourseProduct:
                ltSectionTitle.Text = !string.IsNullOrEmpty(_prd.PrdName) ? _prd.PrdName + " / " + Resources.Resource.lblCourseDetails : Resources.Resource.lblCourseDetails;
                break;
            default:
                break;
        }

    }

    protected void ddlCollection_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        // vCollection[0].CollectionID
        int iCollectionID = BusinessUtility.GetInt(ddlCollection.SelectedItem.Value);
        if (iCollectionID > 0)
        {
            ProductStyle objProductStyle = new ProductStyle();
            int styleID = objProductStyle.GetStyleID(this.ProductID);
            ddlAssociatedStyle.DataSource = objProductStyle.GetCollectionStyleList(null, styleID, BusinessUtility.GetInt(iCollectionID));
            ddlAssociatedStyle.DataTextField = "StyleName";
            ddlAssociatedStyle.DataValueField = "StyleName";
            ddlAssociatedStyle.DataBind();
        }
    }

    private void SetMassLengthUnitNames()
    {
        CompanyUnitType ut = new SysCompanyInfo().GetUnitType(null, CurrentUser.DefaultCompanyID);
        switch (ut)
        {
            case CompanyUnitType.Metric:
                lblUnitHeight.Text = Resources.Resource.lblLenghtMetricUnit;
                lblUnitLength.Text = Resources.Resource.lblLenghtMetricUnit;
                lblUnitPkgHeight.Text = Resources.Resource.lblLenghtMetricUnit;
                lblUnitPkgLength.Text = Resources.Resource.lblLenghtMetricUnit;
                lblUnitPkgWeight.Text = Resources.Resource.lblMassMetricUnit;
                lblUnitPkgWidth.Text = Resources.Resource.lblLenghtMetricUnit;
                lblUnitWeight.Text = Resources.Resource.lblMassMetricUnit;
                lblUnitWidth.Text = Resources.Resource.lblLenghtMetricUnit;
                break;
            case CompanyUnitType.English:
                lblUnitHeight.Text = Resources.Resource.lblLenghtEngUnit;
                lblUnitLength.Text = Resources.Resource.lblLenghtEngUnit;
                lblUnitPkgHeight.Text = Resources.Resource.lblLenghtEngUnit;
                lblUnitPkgLength.Text = Resources.Resource.lblLenghtEngUnit;
                lblUnitPkgWeight.Text = Resources.Resource.lblMassEngUnit;
                lblUnitPkgWidth.Text = Resources.Resource.lblLenghtEngUnit;
                lblUnitWeight.Text = Resources.Resource.lblMassEngUnit;
                lblUnitWidth.Text = Resources.Resource.lblLenghtEngUnit;
                break;
        }
    }

    protected void ddlBuilding_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillRoomsDdl(BusinessUtility.GetInt(ddlBuilding.SelectedValue));
        ddlRoom.Focus();
    }

    private void FillRoomsDdl(int buildingID)
    {
        //ProcessInventory.FillRooms(ddlRoom, buildingID, Globals.CurrentAppLanguageCode, new ListItem("--Room--", "0"));
        DropDownHelper.FillRooms(ddlRoom, new ListItem("--Room--", "0"), buildingID, Globals.CurrentAppLanguageCode);
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Product prd = new Product();
            try
            {
                //check is bed occupied
                if (!prd.IsOccupiedForFuture(this.ProductID))
                {
                    prd.Delete(this.ProductID);
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgDeleteSuccess);

                    string url = string.Format("~/Inventory/ViewProduct.aspx?pType={0}", (int)this.ProductType);
                    Response.Redirect(url, false);
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgProductCanNotBeDeleted);
                }
            }
            catch
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
            }
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblAccessDenied);
        }
    }

    private Boolean IsResturant()
    {
        return BusinessUtility.GetBool(BusinessUtility.GetInt(System.Configuration.ConfigurationManager.AppSettings["IsResturant"]));
    }
}
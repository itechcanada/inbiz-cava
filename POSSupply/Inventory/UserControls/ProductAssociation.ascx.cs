﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Inventory_UserControls_ProductAssociation : System.Web.UI.UserControl, IProduct
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
        {
            btnSave.Visible = false;
        }
    }

    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    public void Initialize()
    {
        ltSectionTitle.Text = Resources.Resource.lblPrdAssociate;
        Product prd = new Product();
        prd.PopulateObject(this.ProductID);
        if (this.ProductID > 0)
        {
            ltSectionTitle.Text = string.Format("{0} / {1}", prd.PrdName, Resources.Resource.lblPrdAssociate);
        }                

        lbxProducts.DataSource = ProcessInventory.GetProductListToAssign(this.ProductID, Globals.CurrentAppLanguageCode);
        lbxProducts.DataTextField = "Name";
        lbxProducts.DataValueField = "id";
        lbxProducts.DataBind();

        List<OptionList> lstAssigned = ProcessInventory.GetProductListAssigned(this.ProductID, Globals.CurrentAppLanguageCode);
        foreach (var item in lstAssigned)
        {
            ListItem li = lbxProducts.Items.FindByValue(item.id);
            if (li != null)
            {
                li.Selected = true;
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        List<int> lstSelectedItems = new List<int>();
        foreach (ListItem item in lbxProducts.Items)
        {
            if (item.Selected)
            {
                lstSelectedItems.Add(BusinessUtility.GetInt(item.Value));
            }
        }
        ProcessInventory.AssociateProducts(lstSelectedItems.ToArray(), this.ProductID);
        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgPrdAssociateSaved);
        //Response.Redirect(Request.RawUrl);
        Response.Redirect(QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.ProductSalesPrice).ToString()));
    }
}
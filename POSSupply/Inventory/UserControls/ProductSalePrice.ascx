﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductSalePrice.ascx.cs"
    Inherits="Inventory_UserControls_ProductSalePrice" %>
<h3>
    <asp:Literal ID="ltSectionTitle" Text="" runat="server" />
</h3>
<asp:Panel ID="pnlEditForm" runat="server" DefaultButton="btnSave">
    <ul class="form">
        <li>
            <div class="lbl">
                <asp:Label ID="lblSalePrice" Text="<%$ Resources:Resource, lblPrdSalePrice %>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtSalePrice" CssClass="innerText numericTextField" runat="server" /><span> *</span>
                <asp:RequiredFieldValidator ID="reqvalSalePrice" runat="server" ControlToValidate="txtSalePrice"
                    ErrorMessage="<%$ Resources:Resource, reqvalSalePrice %>" SetFocusOnError="true"
                    Display="None"></asp:RequiredFieldValidator>
                <%--<asp:CompareValidator ID="comvalSalePrice" EnableClientScript="true" SetFocusOnError="true"
                    ControlToValidate="txtSalePrice" Operator="DataTypeCheck" Type="Double" Display="None"
                    ErrorMessage="<%$ Resources:Resource, comvalSalePrice %>" runat="server" />--%>
            </div>
            <div class="lbl">
                <asp:Label ID="lblRushSalePrice" Text="<%$ Resources:Resource, lblPrdRushSalePrice %>"
                    runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtRushSalePrice" CssClass="innerText numericTextField" runat="server" />
                <%--<asp:CompareValidator ID="comvalRushSalePrice" EnableClientScript="true" SetFocusOnError="true"
                    ControlToValidate="txtRushSalePrice" Operator="DataTypeCheck" Type="Double" Display="None"
                    ErrorMessage="<%$ Resources:Resource, comvalRushSalePrice %>" runat="server" />--%>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblFromQty" Text="<%$ Resources:Resource, lblPrdFromQuantity %>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtFromQty" runat="server" CssClass="innerText" />
                <span>*</span>
                <asp:RequiredFieldValidator ID="reqvalFromQty" runat="server" ControlToValidate="txtFromQty"
                    Display="None" ErrorMessage="<%$ Resources:Resource, reqvalFromQty %>" SetFocusOnError="true"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="compvalFromQty" runat="server" ControlToValidate="txtFromQty"
                    Display="None" EnableClientScript="true" ErrorMessage="<%$ Resources:Resource, compvalFromQty %>"
                    Operator="DataTypeCheck" SetFocusOnError="true" Type="Double" />
            </div>
            <div class="lbl">
                <asp:Label ID="lblToQty" runat="server" Text="<%$ Resources:Resource, lblPrdToQuantity %>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtToQty" runat="server" CssClass="innerText" />
                <span>*</span>
                <asp:RequiredFieldValidator ID="reqvalToQty" runat="server" ControlToValidate="txtToQty"
                    Display="None" ErrorMessage="<%$ Resources:Resource, reqvalToQty %>" SetFocusOnError="true"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="comvalToQty" runat="server" ControlToValidate="txtToQty"
                    Display="None" EnableClientScript="true" ErrorMessage="<%$ Resources:Resource, comvalToQty %>"
                    Operator="DataTypeCheck" SetFocusOnError="true" Type="Double" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblFromRushQty" runat="server" Text="<%$ Resources:Resource, lblPrdFromRushQuantity %>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtFromRushQty" runat="server" CssClass="innerText" />
                <asp:CompareValidator ID="comvalFromRushQty" runat="server" ControlToValidate="txtFromRushQty"
                    Display="None" EnableClientScript="true" ErrorMessage="<%$ Resources:Resource, comvalFromRushQty %>"
                    Operator="DataTypeCheck" SetFocusOnError="true" Type="Double" />
            </div>
            <div class="lbl">
                <asp:Label ID="lblToRushQuantity" runat="server" Text="<%$ Resources:Resource, lblPrdToRushQuantity %>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtToRushQuantity" runat="server" CssClass="innerText" />
                <asp:CompareValidator ID="comvalToRushQuantity" runat="server" ControlToValidate="txtToRushQuantity"
                    Display="None" EnableClientScript="true" ErrorMessage="<%$ Resources:Resource, comvalToRushQuantity %>"
                    Operator="DataTypeCheck" SetFocusOnError="true" Type="Double" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
    </ul>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave %>" runat="server"
            OnClick="btnSave_Click" />
    </div>
    <asp:ValidationSummary ID="sumvalsalePriceProduct" runat="server" ShowMessageBox="true"
        ShowSummary="false" />
</asp:Panel>
<h3>
    <asp:Literal ID="ltListSalesPice" Text="Product Sale Prices" runat="server" />
</h3>
<div id="grid_wrapper" style="width: 100%;">
    <trirand:JQGrid runat="server" ID="grdProductSalePrice" DataSourceID="sdsProductSalePrice"
        Height="200px" AutoWidth="True" 
        OnDataRequesting="grdProductSalePrice_DataRequesting" 
        oncellbinding="grdProductSalePrice_CellBinding">
        <Columns>
            <trirand:JQGridColumn DataField="id" HeaderText="" PrimaryKey="True" Visible="false" />
            <trirand:JQGridColumn DataField="fromQty" HeaderText="<%$ Resources:Resource, gvwFromQty %>"
                Editable="false" />
            <trirand:JQGridColumn DataField="toQty" HeaderText="<%$ Resources:Resource, gvwToQty %>"
                Editable="false" />
            <trirand:JQGridColumn DataField="salesPrice" HeaderText="<%$ Resources:Resource, gvwSalesPrice %>"
                Editable="false" />
            <trirand:JQGridColumn DataField="fromRushQty" HeaderText="<%$ Resources:Resource, gvwRushFromQty %>"
                Editable="false" />
            <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, gvwRushToQty %>" DataField="toRushQty"
                Sortable="false" TextAlign="Center" />
            <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, gvwRushSalesPrice %>" DataField="rushSalesPrice"
                Sortable="false" TextAlign="Center" />
            <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="id"
                Sortable="false" TextAlign="Center" Width="50">
            </trirand:JQGridColumn>
        </Columns>
        <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
            ShowDeleteButton="false" ShowSearchButton="false" />
        <SortSettings InitialSortColumn=""></SortSettings>
        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
        <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="gridLoadComplete" />
    </trirand:JQGrid>
    <iCtrl:IframeDialog ID="mdDelete" Width="400" Height="120" Title="Delete" Dragable="true"
        TriggerSelectorClass="pop_delete" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
        runat="server">
    </iCtrl:IframeDialog>
    <asp:SqlDataSource ID="sdsProductSalePrice" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
        ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
</div>
<script type="text/javascript">
    //Variables
    var gridID = "<%=grdProductSalePrice.ClientID %>";

    //Function To Resize the grid
    function resize_the_grid() {
        $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
    }

    //Client side function before page change.
    function beforePageChange(pgButton) {
        $('#' + gridID).onJqGridPaging();
    }

    //Client side function on sorting
    function columnSort(index, iCol, sortorder) {
        $('#' + gridID).onJqGridSorting();
    }

    //Call Grid Resizer function to resize grid on page load.
    resize_the_grid();

    //Bind window.resize event so that grid can be resized on windo get resized.
    $(window).resize(resize_the_grid);

    function reloadGrid(event, ui) {
        $('#' + gridID).trigger("reloadGrid");
        //Call back Sync Message
        if (typeof getGlobalMessage == 'function') {
            getGlobalMessage();
        }
    }

    function gridLoadComplete(data) {
        $(window).trigger("resize");
    }        
</script>

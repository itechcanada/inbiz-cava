﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Inventory_UserControls_ProductColor : System.Web.UI.UserControl, IProduct
{
    ProductColor _pColor = new ProductColor();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Security Check
        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY) && CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER) == false && CurrentUser.IsInRole(RoleID.ADMINISTRATOR) == false)
        {
            btnSave.Visible = false;
        }

        if (!IsPostBack) { 

        }
    }
    protected void dlstProductColors_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item) {
            HtmlInputCheckBox chkColor = (HtmlInputCheckBox)e.Item.FindControl("chkColor");
            string selectedCode = BusinessUtility.GetString(DataBinder.Eval(e.Item.DataItem, "prdColiCode"));
            chkColor.Checked = !string.IsNullOrEmpty(selectedCode);
            HtmlContainerControl lblColor = (HtmlContainerControl)e.Item.FindControl("lblColor");
            lblColor.Attributes["for"] = chkColor.ClientID;
            lblColor.Attributes["title"] = BusinessUtility.GetString(DataBinder.Eval(e.Item.DataItem, "colDesc"));
        }
    }



    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    public void Initialize()
    {
        ltSectionTitle.Text = Resources.Resource.lblPrdAddColor;
        Product prd = new Product();
        prd.PopulateObject(this.ProductID);
        ltSectionTitle.Text = string.Format("{0} / {1}", prd.PrdName, Resources.Resource.lblPrdAddColor);
        sdsProductColors.SelectCommand = _pColor.GetSql(sdsProductColors.SelectParameters, this.ProductID);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        List<string> selectedColors = new List<string>();
        foreach (DataListItem item in dlstProductColors.Items)
        {
             HtmlInputCheckBox chkColor = (HtmlInputCheckBox)item.FindControl("chkColor");
             if (chkColor.Checked) {
                 selectedColors.Add(chkColor.Value);
             }
        }

        _pColor.Save(selectedColors.ToArray(), this.ProductID);
        Response.Redirect(QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.ProducImages).ToString()), false);
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductCategories.ascx.cs"
    Inherits="Inventory_UserControls_ProductCategories" %>
<h3>
    <asp:Literal ID="ltSectionTitle" Text="" runat="server" />
</h3>
<div id="grid_wrapper" style="width: 100%;">
    <trirand:JQGrid runat="server" ID="grdProductCategory" Height="200px" AutoWidth="True"
        OnCellBinding="grdProductCategory_CellBinding" OnDataRequesting="grdProductCategory_DataRequesting">
        <Columns>
            <trirand:JQGridColumn DataField="CatgID" HeaderText="<%$Resources:Resource, grdCategoryID %>"
                PrimaryKey="True" Visible="true" />
            <trirand:JQGridColumn DataField="CatgName" HeaderText="<%$ Resources:Resource, grdvProductName %>"
                Editable="false" />
            <trirand:JQGridColumn DataField="CatgIsActive" HeaderText="<%$ Resources:Resource, lblCMIsActive %>"
                Editable="false" />
            <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="CatgID"
                Sortable="false" TextAlign="Center" Width="50">
            </trirand:JQGridColumn>
            <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="CatgID"
                Sortable="false" TextAlign="Center" Width="50">
            </trirand:JQGridColumn>
        </Columns>
        <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
            ShowDeleteButton="false" ShowSearchButton="false" />
        <SortSettings InitialSortColumn=""></SortSettings>
        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
        <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="gridLoadComplete" />
    </trirand:JQGrid>
    <iCtrl:IframeDialog ID="mdDelete" Width="400" Height="120" Title="Delete" Dragable="true"
        TriggerSelectorClass="pop_delete" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
        runat="server">
    </iCtrl:IframeDialog>
    <asp:SqlDataSource ID="sdsProductKit" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
        ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
</div>
<script type="text/javascript">
    //Variables
    var gridID = "<%=grdProductCategory.ClientID %>";

    //Function To Resize the grid
    function resize_the_grid() {
        $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
    }

    //Client side function before page change.
    function beforePageChange(pgButton) {
        $('#' + gridID).onJqGridPaging();
    }

    //Client side function on sorting
    function columnSort(index, iCol, sortorder) {
        $('#' + gridID).onJqGridSorting();
    }

    //Call Grid Resizer function to resize grid on page load.
    resize_the_grid();

    //Bind window.resize event so that grid can be resized on windo get resized.
    $(window).resize(resize_the_grid);

    function reloadGrid(event, ui) {
        $('#' + gridID).trigger("reloadGrid");
        //Call back Sync Message
        if (typeof getGlobalMessage == 'function') {
            getGlobalMessage();
        }
    }

    function gridLoadComplete(data) {
        $(window).trigger("resize");
    }

    function EditProductCategory(categoryID, categoryName, categoryHrdID, catgHdrName) {
        var url = 'AddEditProductCategory.aspx';
        var queryData = {};
        queryData.CategoryID = categoryID;
        queryData.CategoryName = categoryName;
        queryData.CategoryHrdID = categoryHrdID;
        queryData.catgHdrName = catgHdrName;
        //alert(categoryHrdID);
        var t = catgHdrName;
        var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
            title: t,
            loadingClass: "loading-image",
            modal: true,
            width: 800,
            height: 400,
            autoOpen: false,
            closeOnEscape: true
        });
        $dialog.dialog('open');
        return false;
    }

   
</script>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductImages.ascx.cs" Inherits="Inventory_UserControls_ProductImages" %>

<h3>
    <asp:Literal ID="ltSectionTitle" Text="" runat="server" />
</h3>

<asp:DataList ID="dlstProductColors" DataSourceID="sdsPrductImages" runat="server"
    RepeatColumns="4" RepeatDirection="Horizontal" HorizontalAlign="Center" 
    onitemdatabound="dlstProductColors_ItemDataBound" 
    onitemcommand="dlstProductColors_ItemCommand">
    <ItemTemplate>
        <table border="0" cellpadding="3" cellspacing="2">
            <tr>
                <td valign="top">
                    <asp:HyperLink ID="hlImage" NavigateUrl="" runat="server" Target="_blank" >
                        <asp:Image ID="prdImage" ImageUrl="" runat="server" Width="120px" style="border:1px solid #ccc !important;"/>
                    </asp:HyperLink>                   
                </td>
                <td valign="top">
                    <asp:ImageButton ID="ibtnDelete" CommandName="del" CommandArgument='<%#Eval("prdImageID")%>' runat="server" ToolTip="Delete" CausesValidation="false"
                        ImageUrl="~/Images/delete_icon.png" OnClientClick="return confirm('Do you want to delete?')" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <a class="edit_pop" href='ProductImageEdit.aspx?imgID=<%#Eval("prdImageID")%>&prdID=<%#Eval("prdID")%>&imgType=<%#Eval("ProductImageType")%>&imgGroup=<%#Eval("ImageGroup")%>'><%=Resources.Resource.lblEdit%></a>                    
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:DataList>    
<asp:SqlDataSource ID="sdsPrductImages" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
      ProviderName="MySql.Data.MySqlClient" SelectCommand="" 
    onselected="sdsPrductImages_Selected"></asp:SqlDataSource>

<div style="text-align: center; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;">
    <asp:Panel ID="pnlAddNew" runat="server">
        <asp:HyperLink ID="hlAddImage" CssClass="edit_pop" NavigateUrl="" runat="server"
            Text="Add Product Image" />
    </asp:Panel>
    <iCtrl:IframeDialog ID="mdEdit" Width="650" Height="500" Title="Add Image" Dragable="true"
        TriggerSelectorClass="edit_pop" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
        runat="server">
    </iCtrl:IframeDialog>
    <asp:Panel ID="pnlUpload" runat="server" Visible="false">
        <asp:FileUpload runat="server" ID="fuImage" /><br />
        <br />
        <asp:Button ID="btnUpload" Text="Upload" runat="server" OnClick="btnUpload_Click" /></asp:Panel>
</div>

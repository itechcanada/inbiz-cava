﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.WebControls;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;

public partial class Inventory_UserControls_ProductKit : System.Web.UI.UserControl, IProduct
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    public void Initialize()
    {
        ltSectionTitle.Text = Resources.Resource.lblMakePrdKit;
        Product prd = new Product();
        prd.PopulateObject(this.ProductID);
        ltSectionTitle.Text = string.Format("{0} / {1}", prd.PrdName, Resources.Resource.lblMakePrdKit);
    }
    protected void grdProductKit_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        sdsProductKit.SelectCommand = new ProductKit().GetSql(sdsProductKit.SelectParameters, this.ProductID, Globals.CurrentAppLanguageCode);
    }
}
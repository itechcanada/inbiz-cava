﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Trirand.Web.UI.WebControls;

using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Inventory_UserControls_ProductQuantity : System.Web.UI.UserControl, IProduct
{
    ProductQuantity _prdQty = new ProductQuantity();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack  && grdQuantity.AjaxCallBackMode == AjaxCallBackMode.None)
        {
            
        }
        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
        {
            txtOnHandQuanPerLocation.Enabled = false;
            txtReservedQuantity.Enabled = false;
            txtResQuaforSO.Enabled = false;
            txtDefectedQua.Enabled = false;
            btnSave.Visible = false;
            grdQuantity.Columns[10].Visible = false;
        }
    }

    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    public void Initialize()
    {
        if (!IsPostBack && grdQuantity.AjaxCallBackMode == AjaxCallBackMode.None)
        {            
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                //Checkif only oner record Exist
                if (this.QuantiyID <= 0)
                {
                    int id = 0;
                    int allQty = _prdQty.GetProductQuantityCount(dbHelp, this.ProductID, out id);
                    if (allQty == 1 && id > 0)
                    {
                        //Session["EditKeyID"] = id;
                        //Response.Redirect(QueryString.GetModifiedUrl("PQtyID", id.ToString()), false);
                    }
                }

                SysTaxCodeDesc tg = new SysTaxCodeDesc();
                SysWarehouses wh = new SysWarehouses();                

                tg.FillListControl(dbHelp, dlTaxCode, new ListItem(""));
                wh.FillAllWharehouse(dbHelp, dlWarehouses, CurrentUser.UserDefaultWarehouse, 0, new ListItem(""));
                //dlTaxCode.Attributes["data-placeholder"] = Resources.Resource.liTaxGroup;                
                //dlWarehouses.Attributes["data-placeholder"] = Resources.Resource.liWarehouseLocation;

                ltSectionTitle.Text = Resources.Resource.lblPrdQuantity;
                Product prd = new Product();
                prd.PopulateObject(this.ProductID);
                ltSectionTitle.Text = string.Format("{0} / {1}", prd.PrdName, Resources.Resource.lblPrdQuantity);

                switch (this.ProductType)
                {
                    case StatusProductType.Product:
                        liPrd1.Visible = true;
                        liPrd2.Visible = true;
                        liPrd3.Visible = true;
                        liPrd4.Visible = true;
                        liPrd5.Visible = true;
                        break;
                    case StatusProductType.Accommodation:
                        break;
                    case StatusProductType.ServiceProduct:
                        break;
                    case StatusProductType.AdminFee:
                        break;
                    case StatusProductType.ChildUnder12:
                        break;
                    case StatusProductType.CourseProduct:
                        liPrd1.Visible = false;
                        liPrd2.Visible = false;
                        liPrd3.Visible = false;
                        liPrd4.Visible = false;
                        liPrd5.Visible = false;

                        grdQuantity.Columns[3].Visible = false;
                        grdQuantity.Columns[4].Visible = false;
                        grdQuantity.Columns[5].Visible = false;
                        grdQuantity.Columns[6].Visible = false;
                        grdQuantity.Columns[7].Visible = false;
                        lblPrdOnHandQtyperLocation.Text = Resources.Resource.lblSpacesAvailable;
                        ltSectionTitle.Text = string.Format("{0} / {1}", prd.PrdName, Resources.Resource.lblSpacesAvailable);
                        break;
                    default:
                        break;
                }

                //FillForm(dbHelp);
            }
            catch
            {

            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }       
    }

    private void FillForm(DbHelper dbHelp)
    {
        if (this.QuantiyID > 0)
        {
            _prdQty.PopulateObject(dbHelp, this.QuantiyID);
            dlWarehouses.SelectedValue = _prdQty.PrdWhsCode;
            dlWarehouses.Enabled = false;
        }

        ListItem li = dlTaxCode.Items.FindByValue(_prdQty.PrdTaxCode.ToString());
        if (li != null)
        {
            li.Selected = true;
        }

        txtAisie.Text = _prdQty.PrdAisle;
        txtBin.Text = _prdQty.PrdBin;
        txtBlock.Text = _prdQty.PrdBlock;
        txtDefectedQua.Text = string.Format("{0:F}", _prdQty.PrdDefectiveQty);
        txtFloor.Text = _prdQty.PrdFloor;
        txtOnHandQuanPerLocation.Text = string.Format("{0:F}", _prdQty.PrdOhdQty);
        txtRack.Text = _prdQty.PrdRack;
        txtReservedQuantity.Text = string.Format("{0:F}", _prdQty.PrdQuoteRsv);
        txtResQuaforSO.Text = string.Format("{0:F}", _prdQty.PrdSORsv);
        txtBlock.Focus();
    }

    private void SetData()
    {
        _prdQty.Id = this.QuantiyID;
        _prdQty.PrdAisle = txtAisie.Text;
        _prdQty.PrdBin = txtBin.Text;
        _prdQty.PrdBlock = txtBlock.Text;
        _prdQty.PrdDefectiveQty = BusinessUtility.GetDouble(txtDefectedQua.Text);
        _prdQty.PrdFloor = txtFloor.Text;
        _prdQty.PrdOhdQty = BusinessUtility.GetDouble(txtOnHandQuanPerLocation.Text);
        _prdQty.PrdQuoteRsv = BusinessUtility.GetDouble(txtReservedQuantity.Text);
        _prdQty.PrdRack = txtRack.Text;
        _prdQty.PrdSORsv = BusinessUtility.GetDouble(txtResQuaforSO.Text);
        _prdQty.PrdTaxCode = BusinessUtility.GetInt(dlTaxCode.SelectedValue);
        _prdQty.PrdWhsCode = dlWarehouses.SelectedValue;
        _prdQty.PrdID = this.ProductID;
    }

    private int QuantiyID
    {
        get
        {
            int id = 0;
            int.TryParse(hdnIdToEdit.Value, out id);            
            return id;
        }
    }

    protected void grdQuantity_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        //Check if reuqest is for deleting record
        if (Request.QueryString.AllKeys.Contains("deleteItem") && Request.QueryString["deleteItem"]  == "1")
        {
            int idtoDelete = 0;
            if (int.TryParse(Request.QueryString["idToDelete"], out idtoDelete) && idtoDelete > 0)
            {
                ProductQuantity prdQty = new ProductQuantity();
                try
                {
                    prdQty.Delete(idtoDelete);                                        
                }
                catch
                {
                    
                }
            }
        }

        sdsQuantity.SelectCommand = _prdQty.GetSql(sdsQuantity.SelectParameters, this.ProductID);
    }

    protected void grdQuantity_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        //int editColIndex = 8;
        //int deleteColIndex = 9;
        //int prdIDColIndex = 0;

        int dsPrdIdIndex = 1;
        int dsWshIndex = 2;
        int dsOnHndQtyIndex = 3;
        if (e.ColumnIndex == 5) //Intransit Pop Setup
        {
            double inTransQty = BusinessUtility.GetDouble(e.CellHtml); //_prdQty.GetInTransitQty(BusinessUtility.GetInt(e.RowValues[dsPrdIdIndex]), BusinessUtility.GetString(e.RowValues[dsWshIndex]));
            if (inTransQty > 0) {
                e.CellHtml = string.Format(@"<a class=""pop_intransit""  href=""IntransitQtyModal.aspx?PrdID={0}&whscode={1}"" >{2}</a>", e.RowValues[dsPrdIdIndex], e.RowValues[dsWshIndex], inTransQty);
            }
            else
            {
                e.CellHtml = string.Format(@"<a  href=""javascript:void(0);"" >{0}</a>", inTransQty);
            }            
        }
        else if (e.ColumnIndex == 7) //Tags popup setup
        {
            e.CellHtml = string.Format(@"<a class=""pop_move""  href=""MoveInventory.aspx?PrdID={0}&whscode={1}&WhsDesc={3}&onHndQty={4}"" >{2}</a>", e.RowValues[dsPrdIdIndex], e.RowValues[dsWshIndex], Resources.Resource.lblMove, BusinessUtility.GetString( e.RowValues[8]), BusinessUtility.GetString( e.RowValues[3]));
        }
        else if (e.ColumnIndex == 8) //Tags popup setup
        {
            e.CellHtml = string.Format(@"<a class=""pop_tags""  href=""ProductTagsModal.aspx?PrdID={0}&whscode={1}"" >{2}</a>", e.RowValues[dsPrdIdIndex], e.RowValues[dsWshIndex], Resources.Resource.lblViewTags);
        }
        else if(e.ColumnIndex == 9) //Edit popup setup
        {
            //e.CellHtml = string.Format(@"<a href=""Product.aspx?PrdID={0}&PQtyID={1}&ptype={2}&section={3}"" >Edit</a>", e.RowValues[dsPrdIdIndex], e.RowKey, Request.QueryString["ptype"], Request.QueryString["section"]);
            e.CellHtml = Resources.Resource.edit;
        }
        else if (e.ColumnIndex == 10) //Delete popup setup
        {
            //string delUrl = string.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_PRODUCT_QTY, e.RowKey, "reloadGrid");
            e.CellHtml = string.Format(@"<a class=""pop_delete"" href=""javascript:;"" onclick=""deleteQuantity({0});"">{1}</a>", e.RowKey, Resources.Resource.delete);
        }        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                SetData();
                int iSec = (int)InventorySectionKey.ViewProducts;

                if (this.QuantiyID > 0)
                {
                    if (_prdQty.Update(dbHelp))
                    {
                        InventoryMovment objIM = new InventoryMovment();
                        objIM.AddMovment(_prdQty.PrdWhsCode, CurrentUser.UserID, _prdQty.PrdID, BusinessUtility.GetString(InvMovmentSrc.PM), BusinessUtility.GetInt(_prdQty.PrdOhdQty), BusinessUtility.GetString(InvMovmentUpdateType.ORWT));
                    }
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgPrdQuantityUpdated);
                    //Response.Redirect(Request.RawUrl);                
                    switch (this.ProductType)
                    {
                        case StatusProductType.Product:
                            iSec = (int)InventorySectionKey.ProductsAssocaitions;
                            break;
                        case StatusProductType.Accommodation:
                        case StatusProductType.ServiceProduct:
                        case StatusProductType.AdminFee:
                        case StatusProductType.ChildUnder12:
                        case StatusProductType.CourseProduct:
                            iSec = (int)InventorySectionKey.ViewProducts;
                            break;
                    }
                    Response.Redirect(QueryString.GetModifiedUrl("section", iSec.ToString()), false);
                }
                else
                {
                    _prdQty.Insert(dbHelp);
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgPrdQuantityAdded);

                    switch (this.ProductType)
                    {
                        case StatusProductType.Product:
                            iSec = (int)InventorySectionKey.ProductsAssocaitions;
                            break;
                        case StatusProductType.Accommodation:
                        case StatusProductType.ServiceProduct:
                        case StatusProductType.AdminFee:
                        case StatusProductType.ChildUnder12:
                        case StatusProductType.CourseProduct:
                            iSec = (int)InventorySectionKey.ViewProducts;
                            break;
                    }
                    Response.Redirect(QueryString.GetModifiedUrl("section", iSec.ToString()), false);
                }
            }
            catch(Exception ex)
            {
                if (ex.Message == CustomExceptionCodes.ALREADY_EXISTS)
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgDuplicateWhs);
                }
                else
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }            
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl);
    }

    public StatusProductType ProductType
    {
        get
        {
            return QueryStringHelper.GetProductType("ptype");
        }
    }
    protected void grdQuantity_RowSelecting(object sender, JQGridRowSelectEventArgs e)
    {
        Session["EditKeyID"] = BusinessUtility.GetInt(e.RowKey);
        this.FillForm(null);
    }
}
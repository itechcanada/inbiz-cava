﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Inventory_UserControls_ProductAttributes : System.Web.UI.UserControl, IProduct
{
    ProductAttributes _pattr = new ProductAttributes();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Security Check
        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY) && CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER) == false && CurrentUser.IsInRole(RoleID.ADMINISTRATOR) == false)
        {
            btnSave.Visible = false;
        }

        if (!IsPostBack) { 

        }
    }
    protected void dlstProductAttributes_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item) {
            HtmlInputCheckBox chkAttribute = (HtmlInputCheckBox)e.Item.FindControl("chkAttribute");
            int prdID = BusinessUtility.GetInt(DataBinder.Eval(e.Item.DataItem, "ProductID"));
            chkAttribute.Checked = prdID == this.ProductID;
            HtmlContainerControl lblIcon = (HtmlContainerControl)e.Item.FindControl("lblIcon");
            lblIcon.Attributes["for"] = chkAttribute.ClientID;
            lblIcon.Attributes["title"] = BusinessUtility.GetString(DataBinder.Eval(e.Item.DataItem, "AttributeDesc"));

            string imageUrl = BusinessUtility.GetString(DataBinder.Eval(e.Item.DataItem, "AttributeImage"));
            Image imgIcon = (Image)e.Item.FindControl("imgIcon");
            imgIcon.ImageUrl = ImageUploadPath.ATTRIBUTE_IMAGE_ICONS + ThumbSizePrefix._32x32 + imageUrl;            

            //iF IMAGE NOT EXIST THEN HIDE IT
            lblIcon.Visible = !string.IsNullOrEmpty(imageUrl);         
        }
    }

    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    public void Initialize()
    {
        ltSectionTitle.Text = "Amenities";
        Product prd = new Product();
        prd.PopulateObject(this.ProductID);
        ltSectionTitle.Text = string.Format("{0} / {1}", prd.PrdName, "Amenities");
        sdsProductAttributes.SelectCommand = _pattr.GetSql(sdsProductAttributes.SelectParameters, this.ProductID, Globals.CurrentAppLanguageCode);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        List<int> selectedAttr = new List<int>();
        foreach (DataListItem item in dlstProductAttributes.Items)
        {
            HtmlInputCheckBox chkAttribute = (HtmlInputCheckBox)item.FindControl("chkAttribute");
             if (chkAttribute.Checked) {
                 selectedAttr.Add(BusinessUtility.GetInt(chkAttribute.Value));
             }
        }

        _pattr.Save(selectedAttr.ToArray(), this.ProductID);

        Response.Redirect(QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.ViewProducts).ToString()), false);
    }
}
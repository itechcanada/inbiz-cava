﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Inventory_UserControls_ProductImages : System.Web.UI.UserControl, IProduct
{
    int _imageCounter = 0;
    private string _productImageUplaodPath = ImageUploadPath.PRODUCT_IMAGES;
    ProductImages _prdImages = new ProductImages();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Security Check
        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY) && CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER) == false && CurrentUser.IsInRole(RoleID.ADMINISTRATOR) == false)
        {
            pnlUpload.Visible = false;
        }
        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
        {
            pnlUpload.Visible = false;
            pnlAddNew.Visible = false;
        }

    }

    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    public void Initialize()
    {
        ltSectionTitle.Text = Resources.Resource.lblPrdImages;
        hlAddImage.NavigateUrl = "~/Inventory/ProductImageEdit.aspx?prdID=" + this.ProductID;
        Product prd = new Product();
        prd.PopulateObject(this.ProductID);
        ltSectionTitle.Text = string.Format("{0} / {1}", prd.PrdName, Resources.Resource.lblPrdImages);
        sdsPrductImages.SelectCommand = _prdImages.GetSql(sdsPrductImages.SelectParameters, this.ProductID);
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (fuImage.HasFile && FileManager.IsValidImageFile(fuImage.FileName)) {
            string fileName = FileManager.GetRandomFileName(fuImage.FileName);
            string smallFileName = string.Format("{0}{1}", ThumbSizePrefix._100x100, fileName);

            _prdImages.PimgIsDefault = dlstProductColors.Items.Count == 0;
            _prdImages.PrdID = this.ProductID;
            _prdImages.PrdLargeImagePath = fileName;
            //_prdImages.PrdSmallImagePath = smallFileName;
            _prdImages.PrdSmallImagePath = fileName;

            fuImage.PostedFile.SaveAs(Server.MapPath(_productImageUplaodPath) + fileName);
            //FileManager.UploadImage(fuImage.PostedFile, _productImageUplaodPath, fileName); //Upload Original file
            //FileManager.UploadImage(fuImage.PostedFile, 100, 100, _productImageUplaodPath, smallFileName); //Thum Image

            _prdImages.Insert();

            //Response.Redirect(Request.RawUrl);
        }
        Response.Redirect(QueryString.GetModifiedUrl("section", ((int)InventorySectionKey.ProductQuantity).ToString()));
    }
    
    protected void dlstProductColors_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ((Image)e.Item.FindControl("prdImage")).ImageUrl = string.Format("{0}{1}", _productImageUplaodPath, DataBinder.Eval(e.Item.DataItem, "prdSmallImagePath"));
            ((HyperLink)e.Item.FindControl("hlImage")).NavigateUrl = string.Format("{0}{1}", _productImageUplaodPath, DataBinder.Eval(e.Item.DataItem, "prdLargeImagePath"));
            //CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY) && CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER) == false && CurrentUser.IsInRole(RoleID.ADMINISTRATOR) == false
            e.Item.FindControl("ibtnDelete").Visible = CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR);
            _imageCounter++;
        }
    }
    protected void dlstProductColors_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "del") {
            _prdImages.Delete(BusinessUtility.GetInt(e.CommandArgument));
            Response.Redirect(Request.RawUrl);
        }
    }
    protected void sdsPrductImages_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        if (e.AffectedRows > 0)
        {
            pnlAddNew.Visible = false;
        }
    }
}
Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Data.Odbc
Imports System.IO
Imports clsCommon
Partial Class Inventory_AddCategory
    Inherits BasePage
    Dim objCategory As New clsCategory()
    Dim objDataClass As New clsDataClass()
    Shared Flag As Boolean = False
    Dim newWebSeq As String
    Dim maxWebSeq As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        'To be assigned
        If Request.QueryString("CatID") <> "" Then
            lblHeading.Text = EditCategory
        Else
            lblHeading.Text = AddCategory
            trFree.Visible = False
        End If
     
        'This is for automatically selecting web sequence
        If Not IsPostBack Then
            dlWebSeq.Items.Clear()
            If objDataClass.GetScalarData("SELECT MAX(catWebSeq) FROM category where catStatus='1'") Is System.DBNull.Value Then
                dlWebSeq.Items.Add((maxWebSeq + 1).ToString)
            Else
                maxWebSeq = objDataClass.GetScalarData("SELECT MAX(catWebSeq) FROM category where catStatus='1'")
                Dim i As Integer
                For i = 0 To IIf(Request.QueryString("CatID") <> "", maxWebSeq - 1, maxWebSeq)
                    dlWebSeq.Items.Add((i + 1).ToString)
                    If i = maxWebSeq Then
                        dlWebSeq.SelectedValue = maxWebSeq + 1
                    End If
                Next
            End If
            hdnOldvalue.Value = maxWebSeq + 1
            '------------------------------------------------------------------
            'Fields catName-0,catImagePath-1,catWebSeq-2,catIsActive-3
            If Not Request.QueryString("CatID") Is Nothing Then
                fillRecordDetail()
            End If
            txtCatName.Focus()
        End If
    End Sub
    Protected Sub fillRecordDetail()
        objDataClass = New clsDataClass()
        Dim drCategory As OdbcDataReader = objDataClass.GetDataReader("SELECT" & _
        " catName,catImagePath,catIsActive,catWebSeq,catIsFreeText,catFreeText, " & _
        " eBayCatgNo, catOffImagePath, catLandingPageBannerPath FROM category WHERE catId=" & Request.QueryString("CatID") & " and catdescLang ='" + rblLanguage.SelectedValue + "'")
        While drCategory.Read
            txtCatName.Text = drCategory.Item("CatName").ToString
            dlWebSeq.SelectedValue = drCategory.Item("catWebSeq")
            hdnOldvalue.Value = dlWebSeq.SelectedValue
            If (drCategory.Item("catIsActive") = 1) Then
                rbYes.Checked = True
            Else
                rbNo.Checked = True
            End If
            If Not drCategory.Item("catImagePath").ToString = "" Then
                imgCat.Src = "../Thumbnail.ashx?p=" & ConfigurationManager.AppSettings("RelativePathForCategory") & drCategory.Item("catImagePath").ToString & "&Cat=Cat"
                aTag.HRef = ConfigurationManager.AppSettings("RelativePathForCategory") & drCategory.Item("catImagePath").ToString
                trCatImageUrl.Visible = True
                trCatImagePath.Visible = False
            End If
            If Not drCategory.Item("catOffImagePath").ToString = "" Then
                imgCatOff.Src = "../Thumbnail.ashx?p=" & ConfigurationManager.AppSettings("RelativePathForCategory") & drCategory.Item("catOffImagePath").ToString & "&Cat=Cat"
                aOffImage.HRef = ConfigurationManager.AppSettings("RelativePathForCategory") & drCategory.Item("catOffImagePath").ToString
                trCatOffImageUrl.Visible = True
                trCatOffImagePath.Visible = False
            End If
            If Not drCategory.Item("catLandingPageBannerPath").ToString = "" Then
                imgBanner.Src = ConfigurationManager.AppSettings("RelativePathForCategory") & drCategory.Item("catLandingPageBannerPath").ToString '"../Thumbnail.ashx?p=" & ConfigurationManager.AppSettings("RelativePathForCategory") & drCategory.Item("catImagePath").ToString & "&Cat=Cat"
                aBanner.HRef = ConfigurationManager.AppSettings("RelativePathForCategory") & drCategory.Item("catLandingPageBannerPath").ToString
                trLandingPageBannerUrl.Visible = True
                trLandingPageBanner.Visible = False
            End If
            If (drCategory.Item("catIsFreeText") = 1) Then
                rblstCategory.SelectedIndex = 1
                txtFreeText.Text = drCategory.Item("catFreeText").ToString
                trFree.Visible = True
            Else
                rblstCategory.SelectedIndex = 0
                txtFreeText.Text = ""
                trFree.Visible = False
            End If
            txteBayCatgNo.Text = drCategory.Item("eBayCatgNo").ToString
        End While
    End Sub
    Public Sub AdjustWebSeq(ByVal oldWebSeq As Integer, ByVal newWebSeq As Integer, ByVal objclsDataClass As clsDataClass)
        Dim qry As String
        If (oldWebSeq < newWebSeq) Then
            qry = "UPDATE category SET catWebSeq=catWebSeq-1 WHERE catWebSeq >" & oldWebSeq & " and catWebSeq <=" & newWebSeq
            objclsDataClass.SetData(qry)
        ElseIf oldWebSeq > newWebSeq Then
            qry = "UPDATE category SET catWebSeq=catWebSeq+1 WHERE catWebSeq >=" & newWebSeq & " and catWebSeq <" & oldWebSeq
            objclsDataClass.SetData(qry)
        End If
    End Sub
    Public Sub UploadFile(ByVal postedFile As HttpPostedFile, ByVal filePostFix As String)
        Dim StrCategoryPic As String = System.IO.Path.GetFileName(postedFile.FileName).Trim
        Dim strCategory() As String = Split((StrCategoryPic), ".")
        Dim count As Int16 = strCategory.Length - 1
        If strCategory.Length = "1" Then
            lblErrorMsg.Text += msgCMInvalidFileExtension + "<br>"
            Exit Sub
        End If
        If strCategory(count).ToLower = "jpg" Or strCategory(count).ToLower = "gif" Or strCategory(count).ToLower = "png" Or strCategory(count).ToLower = "jpeg" Or strCategory(count).ToLower = "bmp" Then
            Dim strPicture As String
            Dim catID As String
            If Request.QueryString("CatID") <> "" Then
                catID = Request.QueryString("CatID")
            Else
                catID = objCategory.funGetMaxCategoryID
            End If
            strPicture = catID
            'strPicture += "_" & Format(Now(), "HHmmss") & "." & strCategory(count)
            'strPicture = Replace(strPicture, " ", "_")
            'strPicture = Replace(strPicture, "/", "")
            'strPicture = Replace(strPicture, "\", "")
            strPicture &= filePostFix & "." & strCategory(count)
            Dim objDa As New iTECH.Library.DataAccess.MySql.DbHelper()
            Try
                If catID > 0 Then
                    Dim SaveLocation As String = Server.MapPath("~/Upload/Category/") & "\" & strPicture
                    postedFile.SaveAs(SaveLocation)
                    Dim sqlUpdate As String = String.Empty
                    If strPicture.Contains("_on") Then
                        sqlUpdate = "UPDATE category SET catImagePath='" & strPicture & "' WHERE catId=" & catID
                    ElseIf strPicture.Contains("_off") Then
                        sqlUpdate = "UPDATE category SET catOffImagePath='" & strPicture & "' WHERE catId=" & catID
                    ElseIf strPicture.Contains("_banner") Then
                        sqlUpdate = "UPDATE category SET catLandingPageBannerPath='" & strPicture & "' WHERE catId=" & catID
                    End If
                    If Not String.IsNullOrEmpty(strPicture) Then
                        objDa.ExecuteNonQuery(sqlUpdate, Data.CommandType.Text, Nothing)
                    End If
                End If
                'objCategory.CategoryImagePath = strPicture
            Catch ex As Exception
            Finally
                objDa.CloseDatabaseConnection()
            End Try
        Else
            lblErrorMsg.Text += msgUnrecognizedImagePleaseUploadImg + "<br>"
            Exit Sub
        End If
    End Sub

    Protected Sub lnkClickMore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClickMore.Click
        trClickHere.Visible = False
        trAllControl.Visible = True
        txtCatName.Text = ""
        Response.Redirect("~/Inventory/AddCategory.aspx")
    End Sub
    Protected Sub imgCatImageUrl_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCatImageUrl.Click
        trCatImageUrl.Visible = False
        trCatImagePath.Visible = True
        Dim strSql As String = "UPDATE category SET catImagePath='' WHERE catId=" & Request.QueryString("CatID")
        objDataClass.SetData(strSql)
    End Sub

    Protected Sub ibtnBanner_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnBanner.Click
        trLandingPageBannerUrl.Visible = False
        trLandingPageBanner.Visible = True
        Dim strSql As String = "UPDATE category SET catLandingPageBannerPath='' WHERE catId=" & Request.QueryString("CatID")
        objDataClass.SetData(strSql)
    End Sub

    Protected Sub ibtnOffImage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnOffImage.Click
        trCatOffImageUrl.Visible = False
        trCatOffImagePath.Visible = True
        Dim strSql As String = "UPDATE category SET catOffImagePath='' WHERE catId=" & Request.QueryString("CatID")
        objDataClass.SetData(strSql)
    End Sub

    Protected Sub rblstCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblstCategory.SelectedIndexChanged
        Select Case rblstCategory.SelectedIndex
            Case 0
                trFree.Visible = False
            Case 1
                trFree.Visible = True
        End Select
    End Sub
    ' Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    ' fill Detail when selected language changed.
    Protected Sub rblLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblLanguage.SelectedIndexChanged
        If Request.QueryString("CatID") <> "" Then
            fillRecordDetail()
        End If
    End Sub

    Protected Sub imgCmdReset_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdReset.ServerClick
        If Request.QueryString("CatID") <> "" Then
            Response.Redirect("~/Inventory/AddCategory.aspx?CatID=" & Request.QueryString("CatID") & "&Approve=" & Request.QueryString("Approve"))
        Else
            Response.Redirect("~/Inventory/AddCategory.aspx")
        End If
    End Sub

    Protected Sub imgCmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdSave.ServerClick
        Dim redirect As Boolean = False
        lblErrorMsg.Text = ""
        lblSuccess.Text = ""
        Flag = True
        If rbYes.Checked = True Then
            objCategory.IsActive = 1
        ElseIf rbNo.Checked = True Then
            objCategory.IsActive = 0
        End If

        If txtCatName.Text <> "" Then
            objCategory.CategoryName = Replace(Replace(txtCatName.Text, "'", "''").Trim, "\", "\\")
        Else
            objCategory.CategoryName = "sysNull"
        End If
        objCategory.CategoryUpdatedBy = Session("UserID")
        objCategory.CategoryUpdatedOn = Format(Date.Parse(Now()), "yyyy-MM-dd HH:mm:ss")
        objCategory.CategoryWebSequence = dlWebSeq.SelectedItem.ToString
        objCategory.CategoryIsFreeText = rblstCategory.SelectedValue
        If txteBayCatgNo.Text <> "" Then
            objCategory.eBayCatgNo = Replace(Replace(txteBayCatgNo.Text, "'", "''").Trim, "\", "\\")
        End If
        If rblstCategory.SelectedValue = 1 Then
            objCategory.CategoryFreeText = txtFreeText.Text
        Else
            objCategory.CategoryFreeText = ""
        End If
        Dim strCID As String = Request.QueryString("CatID")
        If objCategory.funDuplicateCategory(strCID) Then   'check Duplicate Category Name.
            lblErrorMsg.Text = msgThisCategoryNameAlreadyExists
            txtCatName.Focus()
            Exit Sub
        End If

        If Request.QueryString("CatID") Is Nothing Then
            Dim objclsDataClass As New clsDataClass()
            'To set CategoryImagePath

            '------------------------------------------------------------------------------
            'Adjust for websequence
            newWebSeq = objCategory.CategoryWebSequence
            AdjustWebSeq(hdnOldvalue.Value, newWebSeq, objclsDataClass)
            '-----------------------------------------------------------------------------
            objclsDataClass = New clsDataClass()
            If objCategory.funInsertCategory() = True Then 'insert the Category
                lblSuccess.Text = msgCategoryHasBeenAddedSuccessfully
                trClickHere.Visible = True
                trAllControl.Visible = False
            End If
        Else
            objCategory.CategoryID = Request.QueryString("CatID")
            Dim objclsDataClass As New clsDataClass()
            '------------------------------------------------------------------------------
            'Adjust for websequence
            Dim newWebSeq As String = dlWebSeq.SelectedItem.ToString
            AdjustWebSeq(hdnOldvalue.Value, newWebSeq, objclsDataClass)
            objCategory.DescLang = rblLanguage.SelectedValue
            objCategory.funUpdateCategory("No") 'Update the Category

            '-----------------------------------------------------------------------------
            'objCategory.DescLang = rblLanguage.SelectedValue
            'If objCategory.CategoryImagePath = "" Then
            '    objCategory.funUpdateCategory("No") 'Update the Category
            'Else
            '    objCategory.funUpdateCategory("Yes") 'Update the Category
            'End If            
            lblSuccess.Visible = True
            Session.Add("Msg", msgCategoryHasBeenUpdatedSuccessfully)
            redirect = True
        End If

        Dim strPicture As String
        strPicture = "_" & Format(Now(), "HHmmss")
        strPicture = Replace(strPicture, " ", "_")
        strPicture = Replace(strPicture, "/", "")
        strPicture = Replace(strPicture, "\", "")
        'On image Upload 
        If fileCatImagePath.HasFile Then
            UploadFile(fileCatImagePath.PostedFile, strPicture & "_on")
        End If
        'Off Image Upload
        If fuOffCatImage.HasFile Then
            UploadFile(fuOffCatImage.PostedFile, strPicture & "_off")
        End If
        'Banner Image Upload
        If fuBanner.HasFile Then
            UploadFile(fuBanner.PostedFile, strPicture & "_banner")
        End If
        If redirect Then
            Response.Redirect("~/Inventory/ViewCategory.aspx")
        End If
    End Sub
End Class

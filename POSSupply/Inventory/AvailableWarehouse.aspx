<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="AvailableWarehouse.aspx.vb" Inherits="Inventory_AvailableWarehouse" %>

<%@ Register src="../Controls/CommonSearch/CommonKeyword.ascx" tagname="CommonKeyword" tagprefix="uc1" %>
<%@ Register src="../Controls/CommonSearch/InventorySearch.ascx" tagname="InventorySearch" tagprefix="uc2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>
    
    <uc2:InventorySearch ID="InventorySearch1" runat="server" />
    
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" Text="<%$ Resources:Resource, lblPrdAvailableWarehouse %>"
                                ID="lblHeading"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
    </div>
    <div class="divMainContent">
        <asp:UpdatePanel runat="server" ID="udpnlAvaWhs">
            <ContentTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr height="25">
                        <td class="tdAlign" width="35%">
                            <asp:Label ID="Label1" Text="<%$ Resources:Resource, lblPrdAvailableWarehouse %>"
                                CssClass="lblBold" runat="server" />
                        </td>
                        <td width="1%">
                        </td>
                        <td align="left" width="64%">
                            <asp:DropDownList ID="dlWarehouses" runat="server" Width="336px" CssClass="innerText">
                            </asp:DropDownList>
                            <span class="style1">*</span>
                            <asp:CustomValidator ID="custvalwarehouse" SetFocusOnError="true" runat="server"
                                ErrorMessage="<%$ Resources:Resource, custvalwarehouse %>" ClientValidationFunction="funSelectWarehouses"
                                Display="None"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                            <asp:Label ID="lblBlock" Text="<%$ Resources:Resource, lblPrdBlock %>" CssClass="lblBold"
                                runat="server" />
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtBlock" MaxLength="10" CssClass="innerText" runat="server" Width="330Px" />
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                            <asp:Label ID="lblFloor" Text="<%$ Resources:Resource, lblPrdFloor %>" CssClass="lblBold"
                                runat="server" />
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtFloor" MaxLength="10" CssClass="innerText" runat="server" Width="330Px" />
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                            <asp:Label ID="lblAisie" Text="<%$ Resources:Resource, lblPrdAisie %>" CssClass="lblBold"
                                runat="server" />
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAisie" MaxLength="10" CssClass="innerText" runat="server" Width="330Px" />
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                            <asp:Label ID="lblRack" Text="<%$ Resources:Resource, lblPrdRack %>" CssClass="lblBold"
                                runat="server" />
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtRack" MaxLength="10" CssClass="innerText" runat="server" Width="330Px" />
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                            <asp:Label ID="lblBin" Text="<%$ Resources:Resource, lblPrdBin %>" CssClass="lblBold"
                                runat="server" />
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtBin" MaxLength="10" CssClass="innerText" runat="server" Width="330Px" />
                        </td>
                    </tr>
                    <tr height="25">
                        <td class="tdAlign">
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:ImageButton runat="server" ID="imgCmdSave" ImageUrl="~/images/submit.png" />
                            <asp:ImageButton runat="server" CausesValidation="false" ID="imgCmdReset" ImageUrl="~/images/Reset.png" />
                        </td>
                    </tr>
                    <tr>
                        <td height="5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="padding-right: 15px;" colspan="3">
                            <div style="height: 22px">
                                <asp:UpdateProgress runat="server" ID="Updateprogress" AssociatedUpdatePanelID="udpnlAvaWhs"
                                    DisplayAfter="10">
                                    <ProgressTemplate>
                                        <img src="../Images/wait22trans.gif" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td align="center" colspan="3">
                            <asp:GridView ID="grdvAvailWhs" runat="server" AllowSorting="True" DataSourceID="sqlsdAvailWhs"
                                AllowPaging="True" PageSize="10" PagerSettings-Mode="Numeric" CellPadding="0"
                                GridLines="none" AutoGenerateColumns="False" UseAccessibleHeader="False" Style="border-collapse: separate;"
                                CssClass="view_grid650" DataKeyNames="id" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="WarehouseDescription" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, grdvWarehouse %>"
                                        ReadOnly="True" SortExpression="WarehouseDescription">
                                        <ItemStyle Width="250px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="prdBlock" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, grdvBlock %>"
                                        ReadOnly="True" NullDisplayText="-" SortExpression="prdBlock">
                                        <ItemStyle Width="150px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="prdFloor" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, grdvFloor %>"
                                        NullDisplayText="-" ReadOnly="True" SortExpression="prdFloor">
                                        <ItemStyle Width="150px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdvEdit %>" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-ForeColor="white">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                                                CommandName="Edit" ToolTip="<%$ Resources:Resource, grdvEdit %>" ImageUrl="~/images/edit_icon.png" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdvDelete %>" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-ForeColor="white">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="false" ImageUrl="~/images/delete_icon.png"
                                                ToolTip="<%$ Resources:Resource, grdvDelete %>" CommandArgument='<%# Eval("id") %>'
                                                CommandName="Delete" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="grid_footer" />
                                <RowStyle CssClass="grid_rowstyle" />
                                <PagerStyle CssClass="grid_footer" />
                                <HeaderStyle CssClass="grid_header" Height="26px" />
                                <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                <PagerSettings PageButtonCount="20" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="sqlsdAvailWhs" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <asp:ValidationSummary ID="sumvalWhs" runat="server" ShowMessageBox="true" ShowSummary="false" />
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
        <script type="text/javascript" language="javascript">
            function funSelectWarehouses(source, arguments) {
                if (window.document.getElementById('<%=dlWarehouses.ClientID%>').value != "0") {
                    arguments.IsValid = true;
                }
                else {
                    arguments.IsValid = false;
                }
            }
        </script>
    </div>
</asp:Content>

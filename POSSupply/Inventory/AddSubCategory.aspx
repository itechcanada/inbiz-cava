<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="AddSubCategory.aspx.vb" Inherits="Inventory_AddSubCategory" %>

<%@ Register src="../Controls/CommonSearch/CommonKeyword.ascx" tagname="CommonKeyword" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>

    <uc1:CommonKeyword ID="CommonKeyword1" runat="server" />

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblHeading"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr height="40" runat="server" id="trClickHere" visible="false">
                <td align="center" colspan="3">
                    <asp:Label runat="server" ID="lblSuccess" Style="color: Green;" Font-Bold="true"></asp:Label>
                    <asp:LinkButton runat="server" ID="lnkClickMore" Text="Add More Sub-Category"></asp:LinkButton>
                </td>
            </tr>
            <tr id="trAllControl" runat="server">
                <td align="left">
                    <table width="100%" style="border: 0;" border="0" cellpadding="0" 
                        cellspacing="0">
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Label runat="server" ID="lblErrorMsg" Style="color: Red;" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="tdAlign" width="30%">
                                <asp:Label ID="lblLanguage" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, SelectLanguage%>"></asp:Label>
                            </td>
                            <td align="left" width="70%">
                                <asp:RadioButtonList ID="rblLanguage" runat="server" RepeatDirection="Horizontal"
                                    AutoPostBack="true">
                                    <asp:ListItem Text="<%$ Resources:Resource, English%>" Value="en" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:Resource, French%>" Value="fr"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:Resource, Spanish%>" Value="sp"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="tdAlign" width="30%">
                                <asp:Label ID="lblSubcatCatName" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, CategoryName%>"></asp:Label>
                            </td>
                            <td align="left" width="70%">
                                <asp:DropDownList ID="dlSubcatCatName" Width="338px" runat="server">
                                </asp:DropDownList>
                                <span class="style1">*</span>
                                <asp:CustomValidator ID="custvalCatName" runat="server" ClientValidationFunction="funSelectCategory"
                                    Display="None" ErrorMessage="<%$ Resources:Resource, msgPleaseSelectCategoryName%>"
                                    SetFocusOnError="true"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="tdAlign">
                                <asp:Label ID="lblSubCatName" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, SubCategoryName%>"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtSubCatName" runat="server" MaxLength="100" Width="330px"></asp:TextBox><span
                                    class="style1"> *</span>
                                <asp:RequiredFieldValidator ID="reqvalSubCatName" ControlToValidate="txtSubCatName"
                                    runat="server" ErrorMessage="<%$ Resources:Resource, msgPleaseEnterSubCategoryName%>"
                                    Display="none" />
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="tdAlign">
                                <asp:Label ID="lbleBayCatgNo" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, eBayCatgNo%>"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txteBayCatgNo" runat="server" MaxLength="20" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr height="30" id="trsubcatImagePath" runat="server">
                            <td class="tdAlign">
                                <asp:Label ID="lblsubcatImagePath" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblImage%>"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:FileUpload ID="filesubcatImagePath" runat="server" />
                            </td>
                        </tr>
                        <tr height="30" id="trsubcatImageUrl" runat="server" visible="false">
                            <td class="tdAlign">
                                <asp:Label ID="lblsubcatImageUrl" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, msgPleaseEnterSubCategoryName%>"></asp:Label><br>
                            </td>
                            <td align="left">
                                <a runat="server" id="aTag" target="_blank">
                                    <img runat="server" id="imgCat" /></a> &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="imgsubcatImageUrl" runat="server" ImageUrl="~/Images/delete_icon.png" />
                            </td>
                        </tr>
                        <tr height="30" runat="server" id="trIsActive">
                            <td class="tdAlign">
                                <asp:Label ID="lblIsActive" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, msgIsSubCategoryActive%>"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:RadioButton ID="rbYes" Checked="true" GroupName="rbIsActive" runat="server"
                                    Text="<%$ Resources:Resource, lblPrdYes%>" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:RadioButton ID="rbNo" GroupName="rbIsActive" runat="server" Text="<%$ Resources:Resource, No%>" />
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="tdAlign">
                                <asp:Label ID="lblWebSeq" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, WebSequence%>"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="dlWebSeq" Width="205px" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <asp:HiddenField runat="server" ID="hdnActive" />
                        <asp:HiddenField runat="server" ID="hdnOldvalue" />
                        <tr height="30">
                            <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                                <table width="100%">
                                    <tr>
                                        <td width="30%">
                                        </td>
                                        <td width="20%">
                                            <div class="buttonwrapper">
                                                <a id="imgCmdSave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                                    style="min-width: 120px; text-align: center;">
                                                    <%=Resources.Resource.cmdCssSubmit%></span></a></div>
                                        </td>
                                        <td width="50%" align="left">
                                            <div class="buttonwrapper">
                                                <a id="imgCmdReset" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                                    <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                        <%=Resources.Resource.cmdCssReset%></span></a></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="sumvalSubcategory" runat="server" ShowMessageBox="true"
            ShowSummary="false" />
    </div>
    <br />
    <br />
    <script language="javascript">

        function funSelectCategory(source, arguments) {
            if (window.document.getElementById('<%=dlSubcatCatName.ClientID%>').value != "0") {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }

        function funSubCategoryImage(source, arguments) {
            if (document.getElementById('<%=filesubcatImagePath.ClientID%>').value.length == 0)
                arguments.IsValid = false;
            else
                arguments.IsValid = true;
        }
      
    </script>
</asp:Content>

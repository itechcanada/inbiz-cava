﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Trirand.Web.UI.WebControls;

using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Inventory_MoveInventory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SysWarehouses wh = new SysWarehouses();
            wh.FillAllWharehouse(null, dlToWhs, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem("", "0"));
            dlToWhs.SelectedIndex = 0;

            // Remove From Warehouse code to ToWharehouse list
            ListItem removeItem = dlToWhs.Items.FindByValue(this.WhsCode);
            dlToWhs.Items.Remove(removeItem);

            lblFromWareHouseDetail.Text = this.WhsCodeDesc;
            lblOnHandQty.Text = BusinessUtility.GetString(this.OnHndQty);
            if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
            {
                txtQtyToMove.Enabled = false;
            }
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {

                if (ValidatePage())
                {
                    Product prd = new Product();
                    if (prd.MoveProductQtyToWhrHouse(this.ProductID, BusinessUtility.GetInt(txtQtyToMove.Text), this.WhsCode, BusinessUtility.GetString(dlToWhs.SelectedItem.Value), CurrentUser.UserID) == true)
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgQtyMoved);
                        Globals.RegisterReloadParentScript(this.Page);
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgQtyFailure);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == CustomExceptionCodes.ALREADY_EXISTS)
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgDuplicateWhs);
                }
                else
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Globals.RegisterCloseDialogScript(this);
    }

    private Boolean ValidatePage()
    {

        if (BusinessUtility.GetString(dlToWhs.SelectedItem.Text) == "")
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgSelectWhrHouseToMove);
            return false;
        }

        if (BusinessUtility.GetInt(txtQtyToMove.Text) == 0)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgValidQtyToMove);
            return false;
        }

        if (BusinessUtility.GetInt(txtQtyToMove.Text) > BusinessUtility.GetInt(lblOnHandQty.Text))
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgNotSufficientQtyToMove);
            return false;
        }

        return true;
    }


    public int ProductID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["PrdID"], out pid);
            return pid;
        }
    }

    public string WhsCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["whscode"]);
        }
    }

    public string WhsCodeDesc
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["WhsDesc"]);
        }
    }

    public string OnHndQty
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["onHndQty"]);
        }
    }
}
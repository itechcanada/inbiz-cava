﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Inventory_AddColor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                MessageState.SetGlobalMessage(MessageType.Success, "");
                ProductColor objProductColor = new ProductColor();
                //InbizUser user = new InbizUser();
                //user.FillAllUsers(dbHelp, ddlUsers, null);
                //NewAlert al = new NewAlert();
                //objProductColor.FillAllColor(dbHelp, ddlUsers,null , Globals.CurrentAppLanguageCode);

                ddlUsers.DataSource = objProductColor.GetColorList(null, Globals.CurrentAppLanguageCode);
                ddlUsers.DataTextField = "ColorName";
                ddlUsers.DataValueField = "ColorID";
                ddlUsers.DataBind();

                var lst = objProductColor.GetStyleColor(dbHelp, BusinessUtility.GetInt(this.StyleID), Globals.CurrentAppLanguageCode);
                foreach (var item in lst)
                {
                    ListItem li = ddlUsers.Items.FindByValue(item.ColorID.ToString());
                    if (li != null)
                    {
                        li.Selected = true;
                    }
                }

                string newColorID = BusinessUtility.GetString(Request.QueryString["ColorID"]);
                if (newColorID != "")
                {
                    string[] sColorID = newColorID.Split(',');

                    foreach (string ColorID in sColorID)
                    {
                        ListItem li = ddlUsers.Items.FindByValue(ColorID);
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }
                }

                ddlUsers.Focus();
                ////Check for ajax call if New Color Add
                //if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "ajxNewColorCreated")
                //{
                //    try
                //    {
                //        int newColorID = BusinessUtility.GetInt( Request.Form["ColorID"]);
                //        if (newColorID > 0)
                //        {
                //            ListItem li = ddlUsers.Items.FindByValue(newColorID.ToString());
                //            if (li != null)
                //            {
                //                li.Selected = true;
                //            }
                //        }
                //        Response.Write("ok");
                //    }
                //    catch
                //    {
                //        Response.Write("error");
                //    }

                //    Response.End();
                //    Response.SuppressContent = true;
                //}


            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    private string StyleID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["StyleID"]);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {

            ProductColor al = new ProductColor();
            var lst = new List<int>();
            int id = 0;
            foreach (ListItem item in ddlUsers.Items)
            {
                id = 0;
                if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lst.Contains(id))
                {
                    lst.Add(id);

                }
            }
            bool isStylePublished = false;

            if (IsPrductPublished(BusinessUtility.GetInt(this.StyleID)) == true)
            {
                isStylePublished = true;
            }

            al.IsStylePublished = isStylePublished;
            if (al.AddColorInStyle(BusinessUtility.GetInt(this.StyleID), lst.ToArray()) == true)
            {
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadStyle();"), true);
            }
            else
            {
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    public bool IsPrductPublished(int iMasterID)
    {
        ProductMasterHeader objProductMasterHeader = new ProductMasterHeader();
        objProductMasterHeader.MasterID = BusinessUtility.GetInt(iMasterID);
        var lstProductMasterHdr = objProductMasterHeader.GetMasterHeader(null, Globals.CurrentAppLanguageCode);

        if (lstProductMasterHdr.Count > 0)
        {
            if (lstProductMasterHdr[0].MasterPublished == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ProductColor> GetColorList()
    {
        ProductColor objProductColor = new ProductColor();
        var t = objProductColor.GetColorList(null, Globals.CurrentAppLanguageCode);
        return t;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_RoomView : BasePage
{
    Rooms _categ = new Rooms();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Security Check TO DO

        if (!IsPagePostBack(grdCategories)) { 

        }
    }

    protected void grdCategories_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        int idxDsRoomType = 2;
        if (e.ColumnIndex == 3)
        {
            int idVal = BusinessUtility.GetInt(e.RowValues[idxDsRoomType]);
            e.CellHtml = UIHelper.GetRoomType(idVal);
        }
        else if (e.ColumnIndex == 4)
        {
            Dictionary<string, string> attr = new Dictionary<string, string>();
            attr["id"] = string.Format("{0}_psingle", e.RowKey);
            attr["onchange"] = string.Format("editRow({0});", e.RowKey);
            attr["value"] = e.CellHtml;
            attr["style"] = "width:50px;";
            e.CellHtml = this.GetInputHtml(attr);
        }
        else if (e.ColumnIndex == 5)
        {
            Dictionary<string, string> attr = new Dictionary<string, string>();
            attr["id"] = string.Format("{0}_pcouple", e.RowKey);
            attr["onchange"] = string.Format("editRow({0});", e.RowKey);
            attr["value"] = e.CellHtml;
            attr["style"] = "width:50px;";
            e.CellHtml = this.GetInputHtml(attr);
        }
        else if (e.ColumnIndex == 6)
        {
            Dictionary<string, string> attr = new Dictionary<string, string>();
            attr["id"] = string.Format("{0}_seq", e.RowKey);
            attr["onchange"] = string.Format("editRow({0});", e.RowKey);
            attr["value"] = e.CellHtml;
            attr["style"] = "width:50px;";
            e.CellHtml = this.GetInputHtml(attr);
        }
        else if (e.ColumnIndex == 7)
        {
            Dictionary<string, string> attr = new Dictionary<string, string>();
            attr["roomid"] = e.RowKey;
            attr["class"] = "priceRoomLevel";
            double p = new Rooms().GetPrice(BusinessUtility.GetInt(e.RowKey));
            attr["value"] = string.Format("{0:F}", p);
            e.CellHtml = this.GetInputHtml(attr);
        }
        else if (e.ColumnIndex == 8)
        {
            e.CellHtml = string.Format(@"<a href=""javascript:void(0);"" onclick=""manageBlackOut({0});"" >{1}</a>", e.RowKey, Resources.Resource.lblManageBlackOut);
        }
        else if (e.ColumnIndex == 9)
        {
            e.CellHtml = string.Format(@"<a href=""javascript:void(0);"" onclick=""manageDiscount({0});"" >{1}</a>", e.RowKey, Resources.Resource.lblManageDiscount);
        }
        else if (e.ColumnIndex == 10)
        {
            e.CellHtml = string.Format(@"<input type=""checkbox"" disabled=""disabled"" {0} />", e.CellHtml.Trim().ToLower() == "true" ? @"checked=""checked""" : "");
        }
        else if (e.ColumnIndex == 11)
        {
            e.CellHtml = string.Format(@"<a class=""pop_edit""  href=""RoomEdit.aspx?catid={0}&jscallback={1}"" >{2}</a>", e.RowValues[0], "reloadGrid", Resources.Resource.lblEdit);
        }
        if (e.ColumnIndex == 12)
        {
            //string delUrl = string.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_CATEGORY, e.RowKey, "reloadGrid");
            //e.CellHtml = string.Format(@"<a class=""pop_delete"" href=""{0}"">{1}</a>", delUrl, Resources.Resource.delete);
            e.CellHtml = string.Format(@"<a href=""{0}"" onclick=""deleteRow({1});"">{2}</a>", "javascript:;", e.RowKey, Resources.Resource.delete);
        }
    }

    protected void grdCategories_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        //Custom Edit mode
        if (Request.QueryString.AllKeys.Contains<string>("editRowKey"))
        {
            int rowKey = 0;
            if (int.TryParse(Request.QueryString["editRowKey"], out rowKey) && rowKey > 0)
            {
                double ps = BusinessUtility.GetDouble(Request.QueryString["_psingle"]);
                double pc = BusinessUtility.GetDouble(Request.QueryString["_pcouple"]);
                int seq = BusinessUtility.GetInt(Request.QueryString["_seq"]);

                _categ.UpdateSequence(rowKey, seq, ps, pc);
            }
        }

        if (Request.QueryString.AllKeys.Contains<string>("editPriceRowKey"))
        {
            int rowKey = 0;
            if (int.TryParse(Request.QueryString["editPriceRowKey"], out rowKey) && rowKey > 0)
            {
                _categ.UpdatePrice(rowKey, BusinessUtility.GetDouble(Request.QueryString["newPrice"]));
            }
        }

        if (Request.QueryString.AllKeys.Contains<string>("_history")) {
            sdsCategories.SelectCommand = _categ.GetSql(sdsCategories.SelectParameters, Request.QueryString[txtSearch.ClientID], Globals.CurrentAppLanguageCode);
        }
        else
        {
            sdsCategories.SelectCommand = _categ.GetSql(sdsCategories.SelectParameters, txtSearch.Text, Globals.CurrentAppLanguageCode);
        }
    }
    protected void grdCategories_RowDeleting(object sender, Trirand.Web.UI.WebControls.JQGridRowDeleteEventArgs e)
    {
        e.Cancel = true;

        try
        {
            _categ.Delete(BusinessUtility.GetInt(e.RowKey));
        }
        catch 
        {
            throw;
        }
    }
    protected void grdCategories_RowEditing(object sender, Trirand.Web.UI.WebControls.JQGridRowEditEventArgs e)
    {
        e.Cancel = true;
        try
        {            
           
        }
        catch 
        {
            
        }
    }

    private string GetInputHtml(Dictionary<string, string> attributes)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<input type='text' ");
        foreach (var item in attributes.Keys)
        {
            sb.AppendFormat("{0}='{1}' ", item, attributes[item]);
        }
        sb.Append("/>");
        return sb.ToString();
    }
}
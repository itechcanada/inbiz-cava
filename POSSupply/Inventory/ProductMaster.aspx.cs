﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Inventory_ProductMaster : System.Web.UI.Page
{
    NewAlert _alert = new NewAlert();
    InbizUser _usr = new InbizUser();

    ProductMaterial objPRoductMaterial;
    ProductTexture objPRoductTexture;
    ProductColor objPRoductColor;
    ProductSize objPRoductSize;
    ProductCategory objPRoductCategory;

    protected void Page_Load(object sender, EventArgs e)
    {
        //Security 
        if (!IsPostBack)
        {
            MessageState.SetGlobalMessage(MessageType.Success, "");

            ProductCategory objProductSize = new ProductCategory();
            var vProdSrchCatg = objProductSize.GetSearchCategoryList(null, Globals.CurrentAppLanguageCode,Resources.Resource.All);
            lstProdCatg.DataSource = vProdSrchCatg;
            lstProdCatg.DataTextField = "CatgName";
            lstProdCatg.DataValueField = "CatgValue";
            lstProdCatg.DataBind();

            ProductColor objProductMaterial = new ProductColor();
            var vColorList = objProductMaterial.GetColorList(null, Globals.CurrentAppLanguageCode);
            lstColor.DataSource = vColorList;
            lstColor.DataTextField = "ColorName";
            lstColor.DataValueField = "ColorID";
            lstColor.DataBind();

            // Get User Grid Height
            InbizUser _usr = new InbizUser();
            _usr.PopulateObject(CurrentUser.UserID);
            if (BusinessUtility.GetInt(_usr.UserGridHeight) > 0)
            {
                grdAlerts.Height = _usr.UserGridHeight;
            }
        }
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }
        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
        {
            grdAlerts.Columns[6].Visible = false;
        }
        //Check for ajax call
        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "deleteMaterial")
        {
            try
            {
                int iMaterialID = BusinessUtility.GetInt(Request.Form["MaterialID"]);
                int iMasterID = BusinessUtility.GetInt(Request.Form["MasterID"]);
                if (iMaterialID > 0 && iMasterID > 0)
                {
                    objPRoductMaterial = new ProductMaterial();
                    if (objPRoductMaterial.DeleteMaterialInStyle(iMasterID, iMaterialID) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }

            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
        else if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "deleteTexture")
        {
            try
            {
                int iTextureID = BusinessUtility.GetInt(Request.Form["TextureID"]);
                int iMasterID = BusinessUtility.GetInt(Request.Form["MasterID"]);
                if (iTextureID > 0 && iMasterID > 0)
                {
                    objPRoductTexture = new ProductTexture();
                    if (objPRoductTexture.DeleteTextureInStyle(iMasterID, iTextureID) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }

            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
        else if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "deleteColor")
        {
            try
            {
                int iColorID = BusinessUtility.GetInt(Request.Form["ColorID"]);
                int iMasterID = BusinessUtility.GetInt(Request.Form["MasterID"]);
                if (iColorID > 0 && iMasterID > 0)
                {
                    objPRoductColor = new ProductColor();
                    if (objPRoductColor.DeleteColorInStyle(iMasterID, iColorID) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }

            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
        else if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "deleteCategorySize")
        {
            try
            {
                string sCategorySize = BusinessUtility.GetString(Request.Form["CategorySize"]);
                int iMasterID = BusinessUtility.GetInt(Request.Form["MasterID"]);
                if (sCategorySize != "" && iMasterID > 0)
                {
                    objPRoductSize = new ProductSize();
                    if (objPRoductSize.DeleteSizeCategoryInStyle(iMasterID, sCategorySize) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }

            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
        else if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "deleteCategory")
        {
            try
            {
                int sCategoryID = BusinessUtility.GetInt(Request.Form["CategoryID"]);
                int iMasterID = BusinessUtility.GetInt(Request.Form["MasterID"]);
                if (sCategoryID > 0 && iMasterID > 0)
                {
                    objPRoductCategory = new ProductCategory();
                    objPRoductCategory.CatgHrdID = 1;
                    if (objPRoductCategory.DeleteCatgInStyle(iMasterID, sCategoryID) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }

            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }


        Collection objCollection = new Collection();
        List<Collection> lResult = new List<Collection>();

        lResult.Add(new Collection { CollectionName = "", CollectionID = BusinessUtility.GetInt(0) });
        var lst = objCollection.GetCollectionList(null, Globals.CurrentAppLanguageCode);
        foreach (var item in lst)
        {
            lResult.Add(new Collection { CollectionName = item.ShortName, CollectionID = BusinessUtility.GetInt(item.CollectionID) });
        }

        ddlCollection.DataSource = lResult;
        ddlCollection.DataTextField = "CollectionName";
        ddlCollection.DataValueField = "CollectionID";
        ddlCollection.DataBind();
        ddlCollection.Focus();
    }

    int _rowIdx = -1;
    string htmlMaterial = @"<div id=""inputMaterial#CONTAINER_ID#"" class=""autocomplete_holder ui-helper-clearfix"">#MATERIAL_USED# <span class=""add"" #AddStyle#  onclick=""AddMaterial('#STYLEID#');"">@Add<a href=""javascript:;"">+</a></span></div>";
    //string htmlTexture = @"<div id=""inputColor#CONTAINER_ID#"" class=""autocomplete_holder ui-helper-clearfix"">#TEXTURE_USED# <span class=""add"" #AddStyle# onclick=""AddTexture('#STYLEID#');"">@Add<a href=""javascript:;"">+</a></span></div>";
    string htmlColor = @"<div id=""inputColor#CONTAINER_ID#"" class=""autocomplete_holder ui-helper-clearfix"">#COLOR_USED# <span class=""add"" #AddStyle# onclick=""AddColor('#STYLEID#');"">@Add<a href=""javascript:;"">+</a></span></div>";
    string htmlSizeGroup = @"<div id=""inputSizeGroup#CONTAINER_ID#"" class=""autocomplete_holder ui-helper-clearfix"">#SIZEGROUP_USED# <span class=""add"" #AddStyle# onclick=""AddSizeGroup('#STYLEID#');"">@Add<a href=""javascript:;"">+</a></span></div>";
    string htmlCategory = @"<div id=""inputCategory#CONTAINER_ID#"" class=""autocomplete_holder ui-helper-clearfix"">#CATEGORY_USED# <span class=""add"" #AddStyle# #style# onclick=""AddCategory('#STYLEID#');"">@Add<a href=""javascript:;"">+</a></span></div>";

    protected void grdAlerts_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 1)
        {
            int iStyleID = BusinessUtility.GetInt(e.CellHtml);
            string sAddStyle = "";
            string sRemoveStyle = "";

            e.CellHtml = htmlMaterial.Replace("#CONTAINER_ID#", e.RowIndex.ToString()).Replace("#STYLEID#", BusinessUtility.GetString(iStyleID)).Replace("@Add", Resources.Resource.lblAdd);
            objPRoductMaterial = new ProductMaterial();
            var v = objPRoductMaterial.GetStyleMaterial(null, iStyleID, Globals.CurrentAppLanguageCode);
            if (v.Count == 1)
            {
                sAddStyle = "style=display:none";
            }

            if (IsPrductPublished(iStyleID) == true)
            {
                sAddStyle = "style=display:none";
                sRemoveStyle = "style=display:none";
            }
            e.CellHtml = e.CellHtml.Replace("#AddStyle#", sAddStyle);

            string htm = string.Empty;

            foreach (var item in v)
            {
                htm += string.Format(@"<span>{0}<a href=""javascript:;"" #RemoveStyle# onclick=""RemoveUsedMaterial(this, {1}, '{2}')"">x</a></span>", item.MaterialName, item.MaterialID, iStyleID).Replace("#RemoveStyle#", sRemoveStyle);
            }
            e.CellHtml = e.CellHtml.Replace("#MATERIAL_USED#", htm);
        }
        else if (e.ColumnIndex == 2)
        {
            //int iStyleID = BusinessUtility.GetInt(e.CellHtml);
            //e.CellHtml = htmlTexture.Replace("#CONTAINER_ID#", e.RowIndex.ToString()).Replace("#STYLEID#", BusinessUtility.GetString(iStyleID)).Replace("@Add", Resources.Resource.lblAdd);
            //string sAddStyle = "";
            //string sRemoveStyle = "";
            //if (IsPrductPublished(iStyleID) == true)
            //{
            //    sAddStyle = "style=display:none";
            //    sRemoveStyle = "style=display:none";
            //}
            //e.CellHtml = e.CellHtml.Replace("#AddStyle#", sAddStyle);
            //string htm = string.Empty;
            //objPRoductTexture = new ProductTexture();
            //var v = objPRoductTexture.GetStyleTexture(null, iStyleID, Globals.CurrentAppLanguageCode);
            //foreach (var item in v)
            //{
            //    htm += string.Format(@"<span>{0}<a href=""javascript:;""  #RemoveStyle# onclick=""RemoveUsedTexture(this, {1}, '{2}')"">x</a></span>", item.TextureName, item.TextureID, iStyleID).Replace("#RemoveStyle#", sRemoveStyle); ;
            //}
            //e.CellHtml = e.CellHtml.Replace("#TEXTURE_USED#", htm);
        }
        else if (e.ColumnIndex == 3)
        {
            int iStyleID = BusinessUtility.GetInt(e.CellHtml);
            e.CellHtml = htmlColor.Replace("#CONTAINER_ID#", e.RowIndex.ToString()).Replace("#STYLEID#", BusinessUtility.GetString(iStyleID)).Replace("@Add", Resources.Resource.lblAdd);
            string sAddStyle = "";
            string sRemoveStyle = "";
            if (IsPrductPublished(iStyleID) == true)
            {
                //sAddStyle = "style=display:none";
                sRemoveStyle = "style=display:none";
            }
            e.CellHtml = e.CellHtml.Replace("#AddStyle#", sAddStyle);
            string htm = string.Empty;
            objPRoductColor = new ProductColor();
            var v = objPRoductColor.GetStyleColor(null, iStyleID, Globals.CurrentAppLanguageCode);
            foreach (var item in v)
            {
                htm += string.Format(@"<span>{0}<a href=""javascript:;""  #RemoveStyle# onclick=""RemoveUsedColor(this, {1}, '{2}')"">x</a></span>", item.ColorName, item.ColorID, iStyleID).Replace("#RemoveStyle#", sRemoveStyle);
            }
            e.CellHtml = e.CellHtml.Replace("#COLOR_USED#", htm);
        }
        else if (e.ColumnIndex == 4)
        {
            int iStyleID = BusinessUtility.GetInt(e.CellHtml);
            e.CellHtml = htmlSizeGroup.Replace("#CONTAINER_ID#", e.RowIndex.ToString()).Replace("#STYLEID#", BusinessUtility.GetString(iStyleID)).Replace("@Add", Resources.Resource.lblAdd);
            string sAddStyle = "";
            string sRemoveStyle = "";
            if (IsPrductPublished(iStyleID) == true)
            {
                sAddStyle = "style=display:none";
                sRemoveStyle = "style=display:none";
            }
            e.CellHtml = e.CellHtml.Replace("#AddStyle#", sAddStyle);
            string htm = string.Empty;
            objPRoductSize = new ProductSize();
            var v = objPRoductSize.GetStyleSizeCategory(null, iStyleID, Globals.CurrentAppLanguageCode);
            foreach (var item in v)
            {
                htm += string.Format(@"<span>{0}<a href=""javascript:;""  #RemoveStyle# onclick=""RemoveUsedSizeGroup(this, '{1}', '{2}')"">x</a></span>", item.CategoryName, item.CategoryName, iStyleID).Replace("#RemoveStyle#", sRemoveStyle); ;
            }
            e.CellHtml = e.CellHtml.Replace("#SIZEGROUP_USED#", htm);

        }
        else if (e.ColumnIndex == 5)
        {
            int iStyleID = BusinessUtility.GetInt(e.CellHtml);
            string sStyle = "";
            objPRoductCategory = new ProductCategory();
            objPRoductCategory.CatgHrdID = 1;
            var v = objPRoductCategory.GetStyleCatg(null, iStyleID, Globals.CurrentAppLanguageCode);
            objPRoductCategory.CatgHrdID = 1;
            var Hdr = objPRoductCategory.GetCategoryHeader(null, Globals.CurrentAppLanguageCode);
            if (v.Count == Hdr[0].CatgHdrSelNo)
            {
                sStyle = "style='display:none;'";
            }

            e.CellHtml = htmlCategory.Replace("#CONTAINER_ID#", e.RowIndex.ToString()).Replace("#STYLEID#", BusinessUtility.GetString(iStyleID)).Replace("#style#", sStyle).Replace("@Add", Resources.Resource.lblAdd);

            string sAddStyle = "";
            string sRemoveStyle = "";
            if (IsPrductPublished(iStyleID) == true)
            {
                sAddStyle = "style=display:none";
                sRemoveStyle = "style=display:none";
            }
            e.CellHtml = e.CellHtml.Replace("#AddStyle#", sAddStyle);
            string htm = string.Empty;

            foreach (var item in v)
            {
                htm += string.Format(@"<span>{0}<a href=""javascript:;""  #RemoveStyle# onclick=""RemoveUsedCategory(this, '{1}', '{2}')"">x</a></span>", item.CatgName, item.CatgID, iStyleID).Replace("#RemoveStyle#", sRemoveStyle); ;
            }
            e.CellHtml = e.CellHtml.Replace("#CATEGORY_USED#", htm);
        }


        else if (e.ColumnIndex == 6)
        {

            int iStyleID = BusinessUtility.GetInt(e.CellHtml);
            string htm = string.Format(@"<a href=""javascript:;"" onclick=""AddItemDetail('{0}')"">" + Resources.Resource.CmdCssEdit + "</a>", iStyleID);
            e.CellHtml = htm;
        }


        else if (e.ColumnIndex == 7)
        {
            bool isActive = BusinessUtility.GetBool(e.CellHtml);
            e.CellHtml = string.Format("<input type='checkbox' disabled='true' {0} />", isActive ? "checked='checked'" : "");
        }
        else if (e.ColumnIndex == 8)
        {
            bool isPublish = BusinessUtility.GetBool(e.CellHtml);
            bool isEnable = true;
            e.CellHtml = string.Format("<input type='checkbox' {1} {0} />", isPublish ? "checked='checked'" : "", isEnable ? "disabled='true'" : "");
        }
        else if (e.ColumnIndex == 9)
        {
            int iStyleID = BusinessUtility.GetInt(e.CellHtml);
            string iStyleName = e.RowKey;
            string htm = string.Format(@"<a href=""javascript:;"" onclick=""AddCopyCollectionStyle('{0}','{1}')"">" + Resources.Resource.lblGrdCopy + "</a>", iStyleID, iStyleName);
            e.CellHtml = htm;
        }
    }

    protected void grdAlerts_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        ProductStyle objProductStyle = new ProductStyle();
        string txtStyle = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_txtStyle"]);
        string ddlCollectionSearch = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_ddlCollectionSearch"]);
        string ddlMaterialSearch = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_ddlMaterialSearch"]);
        string lstColor = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_lstColor"]);
        string lstSizeGroup = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_lstSizeGroup"]);
        string history = BusinessUtility.GetString(Request.QueryString["_history"]);
        string lstProdCatg = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_lstProdCatg"]);
        string lstProdKeyWord = "";//BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_lstProdKeyWord"]);
        int iCollctionID = BusinessUtility.GetInt(Request.QueryString["_Collection"]);
        sdsAlerts.SelectCommand = objProductStyle.GetStyleSql(sdsAlerts.SelectParameters, BusinessUtility.GetString(iCollctionID), txtStyle, ddlCollectionSearch, ddlMaterialSearch, lstColor, lstSizeGroup, history, lstProdCatg, lstProdKeyWord);
    }


    public bool IsPrductPublished(int iMasterID)
    {
        ProductMasterHeader objProductMasterHeader = new ProductMasterHeader();
        objProductMasterHeader.MasterID = BusinessUtility.GetInt(iMasterID);
        var lstProductMasterHdr = objProductMasterHeader.GetMasterHeader(null, Globals.CurrentAppLanguageCode);

        if (lstProductMasterHdr.Count > 0)
        {
            if (lstProductMasterHdr[0].MasterPublished == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }


    protected void btnPublishStyles_OnClick(object sender, EventArgs e)
    {
        try
        {
            ProductMasterHeader objProductMasterHeader = new ProductMasterHeader();
            var lstMasterHdr = objProductMasterHeader.GetMasterHeader(null, Globals.CurrentAppLanguageCode);

            int iNoStylePublished = 0;
            foreach (var item in lstMasterHdr)
            {
                if (BusinessUtility.GetBool(item.MasterActive) == true && BusinessUtility.GetBool(item.MasterPublished) == false)
                {
                    ProductPublish objProductPublish = new ProductPublish();
                    objProductPublish.MasterID = BusinessUtility.GetInt(item.MasterID);
                    if (objProductPublish.Save(CurrentUser.UserID) == true)
                    {
                        iNoStylePublished += 1;
                    }
                }
            }

            if (iNoStylePublished > 0)
            {
                MessageState.SetGlobalMessage(MessageType.Information, BusinessUtility.GetString(iNoStylePublished) + "  " + Resources.Resource.msgItemPublished);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Information, Resources.Resource.msgProductNotFindToBePublish);
            }
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Collection> GetCollectionList()
    {
        Collection objCollection = new Collection();
        var t = objCollection.GetCollectionList(null, Globals.CurrentAppLanguageCode);
        return t;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ProductMaterial> GetMaterialList()
    {
        ProductMaterial objProductMaterial = new ProductMaterial();
        var t = objProductMaterial.GetMaterialList(null, Globals.CurrentAppLanguageCode);
        return t;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ProductColor> GetColorList()
    {
        ProductColor objProductMaterial = new ProductColor();
        var t = objProductMaterial.GetColorList(null, Globals.CurrentAppLanguageCode);
        return t;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ProductSize> GetCategorySizeList()
    {
        ProductSize objProductSize = new ProductSize();
        var t = objProductSize.GetProductCatSize(null, Globals.CurrentAppLanguageCode);
        return t;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ProductSize> GetSizeList()
    {
        ProductSize objProductSize = new ProductSize();
        var t = objProductSize.GetSize(null, Globals.CurrentAppLanguageCode);
        return t;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ProductCategory> GetProdCategoryList()
    {
        ProductCategory objProductSize = new ProductCategory();
        //objProductSize.CatgHrdID = (int)ProductCategories.Website;
        var t = objProductSize.GetSearchCategoryList(null, Globals.CurrentAppLanguageCode,Resources.Resource.All);

        //List<ProductCategory> lstPrdCatg = new List<ProductCategory>();

        //ProductCategory objProductSize = new ProductCategory();
        //objProductSize = new ProductCategory();
        //objProductSize.CatgHrdID = (int)ProductCategories.Website;
        //var vWebSite = objProductSize.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        //lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll + " " + Resources.Resource.lblWebsite) });
        //foreach (var vItem in vWebSite)
        //{
        //    lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(vItem.CatgID), CatgName = BusinessUtility.GetString(vItem.CatgName) });
        //}


        //objProductSize = new ProductCategory();
        //objProductSize.CatgHrdID = (int)ProductCategories.WebsiteSubCategory;
        //var vWebsiteSubCategory = objProductSize.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        //lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll + " " + Resources.Resource.lblWebSiteSubCategory) });
        //foreach (var vItem in vWebsiteSubCategory)
        //{
        //    lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(vItem.CatgID), CatgName = BusinessUtility.GetString(vItem.CatgName) });
        //}


        //objProductSize.CatgHrdID = (int)ProductCategories.Extra;
        //var vDetail = objProductSize.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        //lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll + " " + Resources.Resource.lblExtraCategory) });
        //foreach (var vItem in vDetail)
        //{
        //    lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(vItem.CatgID), CatgName = BusinessUtility.GetString(vItem.CatgName) });
        //}


        //objProductSize = new ProductCategory();
        //objProductSize.CatgHrdID = (int)ProductCategories.Gauge;
        //var vGauge = objProductSize.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        //lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll + " " + Resources.Resource.lblGauge) });
        //foreach (var vItem in vGauge)
        //{
        //    lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(vItem.CatgID), CatgName = BusinessUtility.GetString(vItem.CatgName) });
        //}


        //objProductSize = new ProductCategory();
        //objProductSize.CatgHrdID = (int)ProductCategories.Keywords;
        //var vKeyWords = objProductSize.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        //lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll + " " + Resources.Resource.lblProductKeyWord) });
        //foreach (var vItem in vKeyWords)
        //{
        //    lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(vItem.CatgID), CatgName = BusinessUtility.GetString(vItem.CatgName) });
        //}

        //objProductSize = new ProductCategory();
        //objProductSize.CatgHrdID = (int)ProductCategories.Neckline;
        //var vNickline = objProductSize.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        //lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll + " " + Resources.Resource.lblNickLine) });
        //foreach (var vItem in vNickline)
        //{
        //    lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(vItem.CatgID), CatgName = BusinessUtility.GetString(vItem.CatgName) });
        //}

        //objProductSize = new ProductCategory();
        //objProductSize.CatgHrdID = (int)ProductCategories.Silhouette;
        //var vSilhouette = objProductSize.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        //lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll + " " + Resources.Resource.lblSilhouette) });
        //foreach (var vItem in vSilhouette)
        //{
        //    lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(vItem.CatgID), CatgName = BusinessUtility.GetString(vItem.CatgName) });
        //}

        //objProductSize = new ProductCategory();
        //objProductSize.CatgHrdID = (int)ProductCategories.Sleeve;
        //var vSleeve = objProductSize.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        //lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll + " " + Resources.Resource.lblSleeve) });
        //foreach (var vItem in vSleeve)
        //{
        //    lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(vItem.CatgID), CatgName = BusinessUtility.GetString(vItem.CatgName) });
        //}

        //objProductSize = new ProductCategory();
        //objProductSize.CatgHrdID = (int)ProductCategories.Trend;
        //var vTrend = objProductSize.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        //lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(0), CatgName = BusinessUtility.GetString(Resources.Resource.lblAll + " " + Resources.Resource.lblTrend) });
        //foreach (var vItem in vTrend)
        //{
        //    lstPrdCatg.Add(new ProductCategory { CatgID = BusinessUtility.GetInt(vItem.CatgID), CatgName = BusinessUtility.GetString(vItem.CatgName) });
        //}
        return t;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ProductCategory> GetProdKeyWordList()
    {
        ProductCategory objProductSize = new ProductCategory();
        objProductSize.CatgHrdID = (int)ProductCategories.Keywords;
        var t = objProductSize.GetCategoryList(null, Globals.CurrentAppLanguageCode);
        return t;
    }

}
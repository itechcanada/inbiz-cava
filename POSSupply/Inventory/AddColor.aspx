﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AddColor.aspx.cs" Inherits="Inventory_AddColor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="dialogAssignUser" style="padding: 5px;">
        <h3>
            <asp:Label ID="Label1" Text="<%$Resources:Resource, lblSelectColor%>" runat="server" />
        </h3>
        <div style="float: right; padding-bottom: 10px;">
            <input type="button" id="btnAddNewColor" value="<%=Resources.Resource.btnAddNewColor%>"
                onclick='AddNewColor();' />
        </div>
        <asp:ListBox CssClass="modernized_select" ID="ddlUsers" runat="server" Width="600px" data-placeholder="<%$ Resources:Resource, lblColor %>"
            SelectionMode="Multiple"></asp:ListBox>
    </div>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
            OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        function AddNewColor() {
            var url = 'AddNewColor.aspx';
            var queryData = {};
            queryData.aName = "";
            var t = "<%=Resources.Resource.btnAddNewColor%>";
            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 620,
                height: 400,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');
            return false;
        }

        function ColorCreated(ColorID) {
            var urlLocation = window.location.href + "&ColorID=" + ColorID;
            window.location.href = urlLocation;
        }
    </script>
</asp:Content>


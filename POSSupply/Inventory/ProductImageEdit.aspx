﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="ProductImageEdit.aspx.cs" Inherits="Inventory_ProductImageEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">    
    <link href="../lib/scripts/jCrop/css/jquery.Jcrop.css" rel="stylesheet" type="text/css" />
    <script src="../lib/scripts/jCrop/js/jquery.Jcrop.min.js" type="text/javascript"></script>
    <style type="text/css">
        ul.form li div.lbl{width:200px !important;}        
    </style>
    <script type="text/javascript">
        var arrOfArray = <%=RectangleCoordinatesScriptData%>; 
        var isEarring = <%=IsEarring%>;

        //Boxer plugin
        $.widget("ui.boxer", $.ui.mouse, {

            _init: function () {
                this.element.addClass("ui-boxer");

                this.dragged = false;

                this._mouseInit();

                this.helper = $("<div />")
			.css({ border: '1px dotted black' })
			.addClass("ui-boxer-helper"); //.appendTo('body');
            },

            destroy: function () {
                this.element
			.removeClass("ui-boxer ui-boxer-disabled")
			.removeData("boxer")
			.unbind(".boxer");
                this._mouseDestroy();

                return this;
            },

            _mouseStart: function (event) {
                var self = this;

                this.opos = [event.pageX, event.pageY];

                if (this.options.disabled)
                    return;

                var options = this.options;

                this._trigger("start", event);

                $('body').append(this.helper);

                this.helper.css({
                    "z-index": 100,
                    "position": "absolute",
                    "left": event.clientX,
                    "top": event.clientY,
                    "width": 0,
                    "height": 0
                });
            },

            _mouseDrag: function (event) {
                var self = this;
                this.dragged = true;

                if (this.options.disabled)
                    return;

                var options = this.options;

                var x1 = this.opos[0], y1 = this.opos[1], x2 = event.pageX, y2 = event.pageY;
                if (x1 > x2) { var tmp = x2; x2 = x1; x1 = tmp; }
                if (y1 > y2) { var tmp = y2; y2 = y1; y1 = tmp; }
                this.helper.css({ left: x1, top: y1, width: x2 - x1, height: y2 - y1 });

                this._trigger("drag", event);

                return false;
            },

            _mouseStop: function (event) {
                var self = this;

                this.dragged = false;

                var options = this.options;

                var clone = this.helper.clone()
			.removeClass('ui-boxer-helper').appendTo(this.element);                
                this._trigger("stop", event, { box: clone });
                $(clone).resizable({stop: function () {
                    self._trigger("stop", event, { box: clone });
                }})
                .draggable({stop: function () {
                    self._trigger("stop", event, { box: clone });
                }});
                this.helper.remove();

                return false;
            }
        });
        $.extend($.ui.boxer, {
            defaults: $.extend({}, $.ui.mouse.defaults, {
                appendTo: 'body',
                distance: 0
            })
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <asp:Panel ID="pnlStep1" runat="server">
        <div style="padding: 5px 10px;">
            <ul class="form">
                <li style="display:none;">
                    <div class="lbl">
                        <asp:Label ID="lblSelectLang" runat="server" CssClass="lblBold" Text="Edit Product Image"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:Image ID="imgProduct" ImageUrl="" runat="server" />
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblCatName" runat="server" CssClass="lblBold" Text="Select Display Image"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:FileUpload ID="fuPrdDisplayImage" runat="server" />
                    </div>
                    <div class="clearBoth">
                    </div>
                </li> 
                <li>
                    <div class="lbl">
                        <asp:Label ID="Label10" runat="server" CssClass="lblBold" Text="Select 90(deg.) Image"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:FileUpload ID="fu90DegImage" runat="server" />
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="Label1" runat="server" CssClass="lblBold" Text="Select Zoom Image"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:FileUpload ID="fuPrdZoomImage" runat="server" />
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>   
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblImageGroup" Text="Select Group" runat="server" />
                    </div>
                    <div class="input">
                        <asp:DropDownList ID="ddlImageGroup" runat="server" Width="250px">
                            <asp:ListItem Value="normal" Text="Normal" />
                            <asp:ListItem Value="bracelet" Text="Bracelet" />
                            <asp:ListItem Value="necklace" Text="Necklace" />
                            <asp:ListItem Value="earring" Text="Earring" />
                            <asp:ListItem Value="charm" Text="Charm" />
                            <asp:ListItem Value="stopper" Text="Stopper" />
                        </asp:DropDownList>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>            
                <li>
                    <div class="lbl">
                        <asp:Label ID="Label3" runat="server" CssClass="lblBold" Text="Product Image Type"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:RadioButtonList ID="ddlImageType" runat="server">
                            <asp:ListItem Text="Normal Product Image" Value="normal" Selected="True" />
                            <asp:ListItem Text="Foundation" Value="foundation" />
                            <asp:ListItem Text="Charm/Beads" Value="charm" />
                        </asp:RadioButtonList>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
            </ul>
        </div>
        <div class="div-dialog-command" style="border-top: 1px solid #ccc; padding: 5px;">
            <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" OnClick="btnSave_Click" />
            <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
                CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlStep2" runat="server" Visible="false">        
        <div class="">
            <div>
                <asp:Label ID="Label2" Text="Define Rectangle Area To Drop" runat="server" Font-Bold="true" />
            </div>
            <div id="canvasDisplay" style="border: 1px #c30ea7 solid; width: 633px; height: 177px;
                margin-bottom: 8px; z-index: 0;">
                <div id="divCrop" style="overflow: hidden; height: 177px; width: 635px;">
                    <asp:Image ID="imgToCrop" ImageUrl="" runat="server" />
                </div>
                <br />
            </div>
        </div>
        <%--<div class="charmSection">
            <div style="border: 1px #c30ea7 solid; text-align:center;">
                <asp:Image ID="imgToCrop1" ImageUrl="" runat="server" />
            </div>
        </div>--%>
        <div style="text-align:center;">
             <asp:RadioButton ID="rbtnFoundation" Text="Foundation" runat="server" GroupName="iType"
                            onclick="toggelSection('foundationSection');"  Checked="true"/>
                        <asp:RadioButton ID="rbtnCharmOrBead" Text="Charm/Bead" runat="server" GroupName="iType"
                            onclick="toggelSection('charmSection');" />
                            <br />
            <div class="foundationSection">
                <div id="dropAreas">
                    <b>Draw a rectangle over image to define drop rectangle area!</b>
                </div>
                <div style="border: 1px solid #ccc;">
                    <asp:Label ID="lblX1" Text="X1" runat="server" Font-Bold="True" />
                    <asp:TextBox ID="txtX1" runat="server" Width="45px" MaxLength="4">0</asp:TextBox>
                    <asp:Label ID="Label9" Text="Y1" runat="server" Font-Bold="True" />
                    <asp:TextBox ID="txtY1" runat="server" Width="45px" MaxLength="4">0</asp:TextBox>
                    <asp:Label ID="Label8" Text="X2" runat="server" Font-Bold="True" />
                    <asp:TextBox ID="txtX2" runat="server" Width="45px" MaxLength="4">0</asp:TextBox>
                    <asp:Label ID="lblY2" Text="Y2" runat="server" Font-Bold="True" />
                    <asp:TextBox ID="txtY2" runat="server" Width="45px" MaxLength="4">0</asp:TextBox>
                    <input id="btnSet" type="button" value="Set" />
                    <asp:HiddenField ID="hdnRecData" runat="server" />
                </div>
                <br />
            </div>
        </div>
        <div>
            <ul class="form">               
                <li class="foundationSection">
                    <div class="lbl">
                        <asp:Label ID="Label7" Text="Coordinates" runat="server" Font-Bold="true" /></div>
                    <div class="input">
                        <span id="spCoordinates">
                            <asp:Literal ID="ltAreaCoordinates" Text="Not Set" runat="server" /></span>  
                            <input id="btnClear" type="button" value="Clear" />
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <%--<li>
                    <div class="lbl">
                        <asp:Label ID="lblImageGroup" Text="Select Group" runat="server" />
                    </div>
                    <div class="input">
                        <asp:DropDownList ID="ddlImageGroup" runat="server" Width="250px">
                            <asp:ListItem Value="bracelet" Text="Bracelet" />
                            <asp:ListItem Value="necklace" Text="Necklace" />
                            <asp:ListItem Value="earring" Text="Earring" />
                            <asp:ListItem Value="charm" Text="Charm" />
                            <asp:ListItem Value="stopper" Text="Stopper" />
                        </asp:DropDownList>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>--%>
                <li class="charmSection">
                    <div class="lbl">
                        <asp:Label ID="Label5" Text="Charm/Bead Width" runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtCharmWidth" Width="45px" runat="server" MaxLength="4" Text="5" />
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li class="charmSection">
                    <div class="lbl">
                        <asp:Label ID="Label6" Text="Charm/Bead Offset" runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtCharmOffset" Width="45px" runat="server" MaxLength="4" />
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
            </ul>
        </div>              
        <div class="div-dialog-command" style="border-top:1px solid #ccc; padding:5px;">
            <asp:Button ID="btnSave2" Text="<%$Resources:Resource, btnSave%>" 
                runat="server" onclick="btnSave2_Click"/>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlStep3" runat="server" Visible="false">
       <div>
            <asp:Label ID="Label4" Text="Charm/Beads Settings" runat="server" Font-Bold="true" />
        </div>
        <ul class="form">
            <li>
                
            </li>
            
        </ul>
        <div class="div-dialog-command" style="border-top:1px solid #ccc; padding:5px;">
            <asp:Button ID="btnSave3" Text="<%$Resources:Resource, btnSave%>" 
                runat="server" onclick="btnSave3_Click"/>
                
        </div>
    </asp:Panel>
    <div id="divConsole"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        //Push Coordinates                

        /*var imgCrop = $('#divCrop').Jcrop({
            onChange: showCoords,
            onSelect: showCoords
        });*/
        //$("<div />").draggable({ containment: "#divCrop" }).css({ "border": '1px solid #414141', 'width': '400px', 'height': '50px' }).resizable().appendTo($("#dropAreas"));
        //$("<div />").draggable({ containment: "#divCrop" }).css({ "border": '1px solid #414141', 'width': '400px', 'height': '50px' }).resizable().appendTo($("#dropAreas"));
        /*$("#divCrop").click(function (e) {
            var relativeXPosition = (e.pageX - this.offsetLeft); //offset -> method allows you to retrieve the current position of an element 'relative' to the document
            var relativeYPosition = (e.pageY - this.offsetTop);
            //alert(relativeXPosition);
            alert(relativeYPosition);
        });*/

        // Using the boxer plugin
        $('#divCrop').boxer({
            stop: function (event, ui) {
                var offset = ui.box.offset();
                ui.box.css({ border: '1px solid #666', background: 'orange', padding: '0.1em', opacity: .5 })
                .append($("<div />").css({ position: 'absolute', top: 0, right: 0, width: 10, opacity: 1, 'font-weight': 'bold' }).html("<a href='javascript:;'>X</a>").click(function () { $(this).parent().remove(); }));
                $('#<%=txtX1.ClientID%>').val(offset.left);
                $('#<%=txtX2.ClientID%>').val(offset.left + Math.round(ui.box.width()));
                $('#<%=txtY1.ClientID%>').val(offset.top);
                $('#<%=txtY2.ClientID%>').val(offset.top + Math.round(ui.box.height()));
                //$("#divConsole").html("");
                //$("#divConsole").append('x:' + offset.left + ', y:' + offset.top).append('<br>').append('w:' + ui.box.width() + ', h:' + ui.box.height()).append('<br>');
                //.append('x:' + offset.left + ', y:' + offset.top)
                //.append('<br>')
                //.append('w:' + ui.box.width() + ', h:' + ui.box.height());

            }
        });
        toggelSection();
        function toggelSection(section) {
            if ($("#<%=rbtnFoundation.ClientID%>").is(":checked")) {
                section = "foundationSection";
            }
            else {
                section = "charmSection";
            }
            if (section == "charmSection") {
                $(".charmSection").show();
                $(".foundationSection").hide();
            }
            else {
                $(".foundationSection").show();
                $(".charmSection").hide();
            }
        }
        function showCoords() {

        }

        $(window).load(function () {
            $("#btnSet").click(function () {
                var x1, x2, h;
                x1 = parseInt($('#<%=txtX1.ClientID%>').val());
                x2 = parseInt($('#<%=txtX2.ClientID%>').val());
                y1 = parseInt($('#<%=txtY1.ClientID%>').val());
                y2 = parseInt($('#<%=txtY2.ClientID%>').val());
                //alert(x1 + ' ' + x2 + ' ' + h);
                if (isNaN(x1) || isNaN(x2) || isNaN(y1) || isNaN(y2)) {
                    alert("Please provide valid Numeric Value!");
                    return false;
                }
                var arr = [];
                if (isEarring) {                   
                    arr.push(x1);
                    arr.push(y1);
                    arr.push(x1);
                    arr.push(y2);
                    arrOfArray.push(arr);
                }
                else {
                    arr.push(x1);
                    arr.push(y1);
                    arr.push(x2);
                    arr.push(y1);
                    arrOfArray.push(arr);
                }
                $("#<%=hdnRecData.ClientID%>").val(JSON.stringify(arrOfArray));
                $("#spCoordinates").html(JSON.stringify(arrOfArray));
                $('#<%=txtX1.ClientID%>').val("0");
                $('#<%=txtY1.ClientID%>').val("0");
                $('#<%=txtX2.ClientID%>').val("0");
                $('#<%=txtY2.ClientID%>').val("0");
                return false;
            });

            $("#btnClear").click(function () {
                arrOfArray = [];
                $("#spCoordinates").html(JSON.stringify(arrOfArray));
                $("#<%=hdnRecData.ClientID%>").val(JSON.stringify(arrOfArray));
            });
        });
    </script>
</asp:Content>


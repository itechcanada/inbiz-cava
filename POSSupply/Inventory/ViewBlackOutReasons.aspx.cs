﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using Trirand.Web.UI.WebControls;
using iTECH.WebControls;

public partial class Inventory_ViewBlackOutReasons : BasePage
{
    BlackOutReason _blReason = new BlackOutReason();

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void jgdvRegister_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        sqldsRegister.SelectCommand = _blReason.GetSql();
        sqldsRegister.DataBind();
    }
    protected void jgdvRegister_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 3)
        {
            e.CellHtml = string.Format(@"<a href=""{0}"" onclick=""{2}"">{1}</a>", "javascript:void(0)", Resources.Resource.lblEdit, string.Format("setSelectedRow({0}); $('#edit_ctl00_ctl00_cphFullWidth_cphMaster_jgdvRegister').trigger('click');", e.RowKey));
        }
        else if (e.ColumnIndex == 4)
        {
            e.CellHtml = string.Format(@"<a href=""{0}"" onclick=""{2}"">{1}</a>", "javascript:void(0)", Resources.Resource.delete, string.Format("setSelectedRow({0}); $('#del_ctl00_ctl00_cphFullWidth_cphMaster_jgdvRegister').trigger('click');", e.RowKey));
        }
    }

    protected void jgdvRegister_RowAdding(object sender, Trirand.Web.UI.WebControls.JQGridRowAddEventArgs e)
    {
        e.Cancel = true;

        try
        {
            _blReason.ReasonEn = e.RowData["ReasonEn"];
            _blReason.ReasonFr = e.RowData["ReasonFr"];
            _blReason.IsActive = true;
            _blReason.Insert();
        }
        catch 
        {
            
        }   
        sqldsRegister.SelectCommand = _blReason.GetSql();
        sqldsRegister.DataBind();
    }
    protected void jgdvRegister_RowEditing(object sender, JQGridRowEditEventArgs e)
    {
        e.Cancel = true;
        try
        {
            _blReason.ID = BusinessUtility.GetInt(e.RowKey);
            _blReason.ReasonEn = e.RowData["ReasonEn"];
            _blReason.ReasonFr = e.RowData["ReasonFr"];
            _blReason.IsActive = true;
            _blReason.Update();
        }
        catch
        {
            
        }       

        sqldsRegister.SelectCommand = _blReason.GetSql();
        sqldsRegister.DataBind();
    }
    protected void jgdvRegister_RowDeleting(object sender, JQGridRowDeleteEventArgs e)
    {
        e.Cancel = true;
        try
        {
            _blReason.Delete(BusinessUtility.GetInt(e.RowKey));
        }
        catch 
        {
            
        }

        sqldsRegister.SelectCommand = _blReason.GetSql();
        sqldsRegister.DataBind();
    }
}
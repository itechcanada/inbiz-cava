﻿<%@ Page Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="ProductSQUpdate.aspx.cs" Inherits="Inventory_ProductSQUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
    </div>
    <div style="float: left; padding-top: 12px;">
        <literal id="lblCollectionName" style="font-size: 20px; font-weight: bold;"></literal>
        <literal id="lblStyleName" style="font-size: 20px; font-weight: bold; padding-left: 40px;"></literal>
    </div>
    <div style="float: right;">
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        <asp:Literal ID="ltSearchForm" Text="<%$Resources:Resource,lblSearchForm %>" runat="server" />
                    </div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="Label2" runat="server" CssClass="filter-key" AssociatedControlID="ddlWarehouse"
                        Text="<%$ Resources:Resource, lblWhs%>"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="filter-key">
                        <asp:ListItem Value="" Selected="True" />
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="Label1" runat="server" CssClass="filter-key" AssociatedControlID="ddlCollection"
                        Text="<%$ Resources:Resource, lblCollection%>"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlCollection" runat="server" CssClass="filter-key">
                        <asp:ListItem Value="" Selected="True" />
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" CssClass="filter-key" AssociatedControlID="txtSearch"
                        runat="server" Text="<%$ Resources:Resource, lblGrdStyle %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div id="divCollectionStyleID" style="width: 100%;">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
    <script type="text/javascript">


        $(document).ready(function () {
            loadSearchControl();
        });

        $("#btnSearch").click(function () {
            var sSearchText = $("#<%=txtSearch.ClientID%>").val();
            var collectionName = $("#<%=ddlCollection.ClientID %> :selected").text();

            var sSearch = $("#<%=ddlCollection.ClientID %> :selected").val();
            var sWarehouse = $("#<%=ddlWarehouse.ClientID %> :selected").val();
            if (sWarehouse == "") {
                alert('<%=Resources.Resource.custvalPOWarehouse %>');
                $("#<%=ddlWarehouse.ClientID %>").focus();
                return;
            }
            else if (collectionName == "") {
                var altermsg = "<%=Resources.Resource.msgPleaseSelectCollection %>";
                alert(altermsg);
                $("#<%=ddlCollection.ClientID %>").focus();
                return;
            }
            else if (sSearchText == "") {
                var altermsg = "<%=Resources.Resource.msgPleaseEnterStyle %>";
                $("#<%=txtSearch.ClientID %>").focus();
                alert(altermsg);
                return;
            }
            if (sSearchText != "" && sSearch != "" && sWarehouse != "") {
                $.ajax({
                    type: "POST",
                    url: "ProductSQUpdate.aspx/GetStyleCollection",
                    dataType: "json",
                    data: "{ style: '" + sSearchText + "',collectionID: '" + sSearch + "',whs: '" + sWarehouse + "' }",
                    contentType: "application/json;",
                    success: function (data) {
                        var addressList = data.d;
                        if (addressList != "") {
                            $("#lblCollectionName").html(collectionName);
                            $("#lblStyleName").html(sSearchText);
                            var valhtml = addressList.split("*");
                            if (valhtml.length > 1) {
                                $("#divCollectionStyleID").html(valhtml[0]);
                                var frstprdID = valhtml[1];
                                if (frstprdID.length > 0) {
                                    $("#" + frstprdID).focus();

                                    $("#" + "ctl00_ctl00_MessageControl1_pnlMessage").hide();
                                }

                            }
                        }
                        else {
                            $("#lblCollectionName").html("");
                            $("#lblStyleName").html("");
                            $("#divCollectionStyleID").css('text-align', 'center');
                            $("#divCollectionStyleID").html("<font style='color:Red; font-Weight:bold; font-size:14px;'> " + "<%=Resources.Resource.msgPrdNotFound%>" + "</font>");
                        }
                        $("#btnSearch").removeClass("ui-state-focus");
                        addMouseHoverEvent();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    }
                });
            }
            else {
                $("#lblCollectionName").html("");
                $("#lblStyleName").html("");
                $("#divCollectionStyleID").html("");
            }
        });


        function updateCollStyle(vPids) {

            var csIDS = vPids.split("~");
            var csQtys = "";
            for (var i = 0; i < csIDS.length; i++) {
                var csQty = $("#qty" + csIDS[i]).val();
                csQtys[i] = csQty;
                if (i == 0) {
                    csQtys = csQty;
                }
                else {
                    csQtys += "~" + csQty;
                }
            }
            if (confirm('<%=Resources.Resource.msgConfrmUpdateQty%>') == true) {
                if (csIDS.length > 0 && csQtys.length > 0) {
                    var sWarehouse = $("#<%=ddlWarehouse.ClientID %> :selected").val();
                    $.ajax({
                        type: "POST",
                        url: "ProductSQUpdate.aspx/UpdateCollectionStyle",
                        dataType: "json",
                        data: "{ csIDs: '" + vPids + "',csQtys: '" + csQtys + "',whs: '" + sWarehouse + "' }",
                        contentType: "application/json;",
                        success: function (data) {
                            var result = data.d;
                            if (result == "success") {
                                alert("<%=Resources.Resource.msgPrdQuantityUpdated%>");
                            }
                            else {
                                alert("<%=Resources.Resource.InvoiceProcessError%>");

                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                        }
                    });
                }
            }
            else {
                return false;
            }
        }



        function loadSearchControl() {
            ReloadSearchConnectionList();
        }

        function ReloadSearchConnectionList() {
            $.ajax({
                type: "POST",
                url: "ProductSQUpdate.aspx/GetCollectionList",
                dataType: "json",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var addressList = data.d;
                    $('option', $("#<%=ddlCollection.ClientID%>")).remove();
                    $("#<%=ddlCollection.ClientID%>").append($("<option />").val("").text(""));
                    for (var i = 0; i < addressList.length; i++) {
                        $("#<%=ddlCollection.ClientID%>").append($("<option />").val(addressList[i].CollectionID).text(addressList[i].ShortName));
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }

        jQuery(window).bind("focus", function (event) {
            $("#btnSearch").removeClass("ui-state-focus");
//            $('.cssPqty').live().each(function () {
//                $(this).addClass('numericTextField');
//            });
        });

        function addMouseHoverEvent() {
            $(".updateQty").live('mouseover', function (e) {
                $($(this).addClass('ui-state-hover'));
            });

            $(".updateQty").live('mouseout', function (e) {
                $($(this).removeClass('ui-state-hover'));
            });
        }


        $(document).ready(function () {
            setGlobalMessage('Ram');
        });

        function setGlobalMessage(sMessage) {
            $.ajax({ url: '<%=ResolveUrl("~/Handlers/AsyncSetMessageHandler.ashx") %>',
                global: false,
                type: "POST",
                data: "{ 'message': '" + sMessage + "' }",
                dataType: "html",
                async: false,
                success: function (msg) {
                    if (msg != 404) {
                        if (typeof getGlobalMessage == 'function') {
                            getGlobalMessage();
                        }
                    }
                }
            });
        }


        function updateCollStylePrice(collectionID) {

            if (confirm('<%=Resources.Resource.msgConfrmUpdateQty%>') == true) {
                var vStyle = $("#btnUpdatePrice").attr("stlytvalue");
                var txtSMQ = $("#txtSMQ").val();
                var txtEUSP = $("#txtEUSP").val();
                var txtWSP = $("#txtWSP").val();
                var txtFOB = $("#txtFOB").val();
                var txtlnd = $("#txtlnd").val();

                    $.ajax({
                        type: "POST",
                        url: "ProductSQUpdate.aspx/UpdateCollectionStylePrice",
                        dataType: "json",
                        data: "{ collectionID: '" + collectionID + "',style: '" + vStyle + "',txtSMQ: '" + txtSMQ + "',txtEUSP: '" + txtEUSP + "',txtWSP: '" + txtWSP + "',txtFOB: '" + txtFOB + "',txtlnd: '" + txtlnd + "' }",
                        contentType: "application/json;",
                        success: function (data) {
                            var result = data.d;
                            if (result == "success") {
                                alert("<%=Resources.Resource.msgPrdPriceUpdated%>");
                            }
                            else {
                                alert("<%=Resources.Resource.InvoiceProcessError%>");

                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                        }
                    });
                
            }
            else {
                return false;
            }

        }

    </script>
</asp:Content>

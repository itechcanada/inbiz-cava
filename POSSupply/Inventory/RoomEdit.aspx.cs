﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_RoomEdit : BasePage
{
    Rooms _categ = new Rooms();
    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            BindData();
            txtCategoryEn.Focus();
        }        
    }

    private void BindData()
    {       
        DropDownHelper.FillDropdown(ddlRoomType, "CT", "dlSGp", Globals.CurrentAppLanguageCode, new ListItem("--Room Type--", "0"));
        DropDownHelper.FillBuildings(ddlParentCategory, new ListItem("--Building---", "0"), Globals.CurrentAppLanguageCode);        

        if (RoomID > 0)
        {
            _categ.PopulateObject(this.RoomID);
            ddlParentCategory.SelectedValue = _categ.BuildingID.ToString();
           
            ddlRoomType.SelectedValue = _categ.RoomType.ToString();
            txtCategoryEn.Text = _categ.RoomTitleEn;
            txtCategoryFr.Text = _categ.RoomTitleFr;
        }        
    }

    private void SetData() {
        _categ.BuildingID = BusinessUtility.GetInt(ddlParentCategory.SelectedValue);
        _categ.IsActive = true;
        _categ.PriorityCouple = 0D;
        _categ.ProritySingle = 0D;
        _categ.RoomID = this.RoomID;
        _categ.RoomTitleEn = txtCategoryEn.Text;
        _categ.RoomTitleFr = txtCategoryFr.Text;        
        _categ.RoomType = BusinessUtility.GetInt(ddlRoomType.SelectedValue);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid) {
            SetData();
            if (this.RoomID > 0) {
                _categ.Update();
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgRecordUpdatedSuccessfully);

                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                }
                else
                {
                    Globals.RegisterCloseDialogScript(Page);
                }       
            }
            else
            {
                _categ.Insert();
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgRecordAddedSuccess);
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                }
                else
                {
                    Globals.RegisterCloseDialogScript(Page);
                }   
            }
        }
    }

    private int RoomID {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["catid"], out id);
            return id;
        }
    }

    private string CategoryGroup
    {
        get
        {            
            return BusinessUtility.GetString(Request.QueryString["cg"]);
        }
    }

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Inventory_ProductMaterialGroup : System.Web.UI.Page
{
    NewAlert _alert = new NewAlert();
    InbizUser _usr = new InbizUser();

    ProductMaterialGroup objProductMaterialGroup;


    protected void Page_Load(object sender, EventArgs e)
    {
        //Security 
        if (!IsPostBack)
        {
            //MessageState.SetGlobalMessage(MessageType.Success, "");
        }
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        //Check for ajax call
        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "deleteMaterial")
        {
            try
            {
                int iMaterialID = BusinessUtility.GetInt(Request.Form["MaterialID"]);
                int iMasterID = BusinessUtility.GetInt(Request.Form["MasterID"]);
                if (iMaterialID > 0 && iMasterID > 0)
                {
                    ProductMaterialGroup objProductMaterialGroup = new ProductMaterialGroup();
                    if (objProductMaterialGroup.DeleteMaterialInGroup(iMasterID, iMaterialID) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }

            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "deleteGroup")
        {
            try
            {
                int iGroupID = BusinessUtility.GetInt(Request.Form["GroupID"]);
                if (iGroupID > 0)
                {
                    ProductMaterialGroup objProductMaterialGroup = new ProductMaterialGroup();
                    if (objProductMaterialGroup.DeleteMaterialGroup(null, CurrentUser.UserID, iGroupID) == true)
                    {
                        Response.Write("ok");
                    }
                    else
                    {
                        Response.Write("error");
                    }
                }
                else
                {
                    Response.Write("error");
                }

            }
            catch
            {
                Response.Write("error");
            }

            Response.End();
            Response.SuppressContent = true;
        }
    }

    int _rowIdx = -1;
    string htmlMaterial = @"<div id=""inputMaterial#CONTAINER_ID#"" class=""autocomplete_holder ui-helper-clearfix"">#MATERIAL_USED# <span class=""add""  onclick=""AddMaterial('#STYLEID#');"">@Add<a href=""javascript:;"">+</a></span></div>";

    protected void grdAlerts_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 2)
        {
            int iGroupID = BusinessUtility.GetInt(e.CellHtml);
            e.CellHtml = htmlMaterial.Replace("#CONTAINER_ID#", e.RowIndex.ToString()).Replace("#STYLEID#", BusinessUtility.GetString(iGroupID)).Replace("@Add", Resources.Resource.lblAdd);

            objProductMaterialGroup = new ProductMaterialGroup();
            var v = objProductMaterialGroup.GetGroupMaterial(null, iGroupID, Globals.CurrentAppLanguageCode);

            string htm = string.Empty;

            foreach (var item in v)
            {
                htm += string.Format(@"<span>{0}<a href=""javascript:;""  onclick=""RemoveUsedMaterial(this, {1}, '{2}')"">x</a></span>", item.MaterialName, item.MaterialID, iGroupID);
                //htm += string.Format(@"<span>{0}<a href=""javascript:;"" #RemoveStyle# onclick=""RemoveUsedMaterial(this, {1}, '{2}')"">x</a></span>", item.MaterialShortName, item.MaterialID, iStyleID).Replace("#RemoveStyle#", sRemoveStyle);
            }
            e.CellHtml = e.CellHtml.Replace("#MATERIAL_USED#", htm);
        }
        else if (e.ColumnIndex == 3)
        {
            //int iStyleID = BusinessUtility.GetInt(e.CellHtml);
            bool isActive = false;//BusinessUtility.GetBool(e.CellHtml); //false; //ProcessInventory.IsImageAdded(BusinessUtility.GetInt(e.RowKey));
            if (e.CellHtml == "1")
            {
                isActive = true;
            }

            e.CellHtml = string.Format("<input type='checkbox' disabled='true' {0} />", isActive ? "checked='checked'" : "");
        }
        else if (e.ColumnIndex == 4)
        {

            int iStyleID = BusinessUtility.GetInt(e.CellHtml);
            string htm = string.Format(@"<a href=""javascript:;"" onclick=""AddMaterialGroup('{0}')"">" + Resources.Resource.CmdCssEdit + "</a>", iStyleID);
            e.CellHtml = htm;//"<a onclick='AddItemDetail('#STYLEID#');'  >Edit</a>".Replace("#STYLEID#", BusinessUtility.GetString(iStyleID));
        }
        else if (e.ColumnIndex == 5)
        {

            int iStyleID = BusinessUtility.GetInt(e.CellHtml);
            string htm = string.Format(@"<a href=""javascript:;"" onclick=""DeleteGroup('{0}')"">" + Resources.Resource.delete + "</a>", iStyleID);
            e.CellHtml = htm;//"<a onclick='AddItemDetail('#STYLEID#');'  >Edit</a>".Replace("#STYLEID#", BusinessUtility.GetString(iStyleID));
        }



    }

    protected void grdAlerts_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        //NewAlert al = new NewAlert();
        ProductMaterialGroup objProductMaterialGroup = new ProductMaterialGroup();
        string txtStyle = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_txtStyle"]);
        objProductMaterialGroup.SearchKey = txtStyle;
        sdsAlerts.SelectCommand = objProductMaterialGroup.GetMaterialGroupSql(BusinessUtility.GetString(Globals.CurrentAppLanguageCode));
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
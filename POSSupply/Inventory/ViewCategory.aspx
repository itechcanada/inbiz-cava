<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="ViewCategory.aspx.vb" Inherits="Inventory_ViewCategory" %>

<%@ Import Namespace="Resources.Resource" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register src="../Controls/CommonSearch/CommonKeyword.ascx" tagname="CommonKeyword" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>

    <uc1:CommonKeyword ID="CommonKeyword1" runat="server" />

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblHeading"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <asp:UpdatePanel runat="server" ID="udpnlViewPrd">
            <ContentTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tbody>                       
                        <tr>
                            <td align="center">
                                <asp:Panel runat="server" ID="SearchPanel" DefaultButton="imgSearch">
                                    <asp:Label ID="lblMsg" runat="server" Font-Bold="true" ForeColor="green"></asp:Label>
                                    <asp:ImageButton ID="imgSearch" Visible="false" runat="server" AlternateText="Search"
                                        ImageUrl="../images/search-btn.gif" />
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td height="10">
                            </td>
                        </tr>
                        <%--<TR><TD align=left>
<TABLE id="Table1" cellSpacing=0 cellPadding=0 width="100%" border=0 runat="server"><TBODY><TR><TD align=left colSpan=3><asp:Label id="lblSearch" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblCatSearch %>"> </asp:Label> </TD></TR><TR><TD align=left width="21%"><asp:DropDownList id="dlSearch" runat="server" ValidationGroup="PrdSrch" Width="145px">
                        <asp:ListItem Value="CN" Text="<%$ Resources:Resource, liCatName %>"/>
                        <asp:ListItem Value="CI" Text="<%$ Resources:Resource, liCatID %>"/>
                    </asp:DropDownList> </TD><TD align=left width="29%"><asp:TextBox id="txtSearch" runat="server" CssClass="innerText" Width="200px"></asp:TextBox>&nbsp;&nbsp;&nbsp; </TD><TD align=left width="10%"><asp:ImageButton id="imgSearch" runat="server" ImageUrl="../images/search-btn.gif" AlternateText="Search" OnClick="imgSearch_Click"></asp:ImageButton> </TD><TD style="PADDING-RIGHT: 15px" align=right width="40%"><DIV style="HEIGHT: 22px"><asp:updateprogress id="Updateprogress" runat="server" DisplayAfter="10" AssociatedUpdatePanelID="udpnlViewPrd">
                  <ProgressTemplate>
                    <img src="../Images/wait22trans.gif" />
                  </ProgressTemplate>
                </asp:updateprogress> </DIV></TD></TR></TBODY></TABLE></TD></TR>--%>
                        <tr>
                            <td>
                                <asp:GridView ID="grdvAppCategory" runat="server" Width="100%" DataKeyNames="catid"
                                    UseAccessibleHeader="False" AutoGenerateColumns="False" GridLines="none" CellPadding="0"
                                    PagerSettings-Mode="Numeric" PageSize="10" AllowPaging="True" Style="border-collapse: separate;"
                                    CssClass="view_grid650" DataSourceID="sqlsdAppAdmin" AllowSorting="True">
                                    <Columns>
                                        <asp:BoundField DataField="catWebSeq" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, hdrWebSequence%>"
                                            ReadOnly="True" SortExpression="catWebSeq">
                                            <ItemStyle Width="30px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="catName" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, Category%>"
                                            ReadOnly="True" SortExpression="catName">
                                            <ItemStyle Width="190px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="eBayCatgNo" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, eBayCatgNo%>"
                                            ReadOnly="True" SortExpression="eBayCatgNo">
                                            <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="UserName" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, UpdatedBy%>"
                                            ReadOnly="True" SortExpression="UserName">
                                            <ItemStyle Width="190px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="catUpdatedOn" HeaderStyle-Wrap="false" HeaderText="<%$ Resources:Resource, UpdatedOn%>"
                                            ReadOnly="True" SortExpression="catUpdatedOn">
                                            <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, hdrImage%>" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-ForeColor="white">
                                            <ItemTemplate>
                                                <a href='<% = ConfigurationManager.AppSettings("RelativePathForCategory") %><%# Container.DataItem("catImagePath")%>'
                                                    id="aTag" target="_blank">
                                                    <img src='../Thumbnail.ashx?p=<% = ConfigurationManager.AppSettings("RelativePathForCategory") %><%# Container.DataItem("catImagePath")%>&Cat=Cat' />
                                                </a>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="center" Width="40px" ForeColor="white" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:ImageField SortExpression="catIsActive" HeaderStyle-ForeColor="white" HeaderText="<%$ Resources:Resource, hdrStatus%>"
                                            DataImageUrlField="catIsActive" DataImageUrlFormatString="~/Images/{0}">
                                            <ItemStyle HorizontalAlign="center" Width="40px" VerticalAlign="Middle" />
                                        </asp:ImageField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, grdEdit%>" HeaderStyle-ForeColor="white"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("catId") %>'
                                                    CommandName="Edit" ImageUrl="~/images/edit_icon.png" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="40px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete%>" HeaderStyle-ForeColor="white"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                                    CommandArgument='<%# Eval("catId") %>' CommandName="Delete" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="40px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="grid_footer" />
                                    <RowStyle CssClass="grid_rowstyle" />
                                    <PagerStyle CssClass="grid_footer" />
                                    <HeaderStyle CssClass="grid_header" Height="26px" />
                                    <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                    <PagerSettings PageButtonCount="20" />
                                </asp:GridView>
                                <asp:SqlDataSource ID="sqlsdAppAdmin" runat="server" ProviderName="System.Data.Odbc"
                                    ConnectionString="<%$ ConnectionStrings:ConnectionString %>"></asp:SqlDataSource>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <br />
    <br />
    <script language="javascript">
        function OpenWindow() {
            submit();
        }
    </script>
</asp:Content>

<%@ Page Language="VB" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="false"
    CodeFile="InventoryUpdate.aspx.vb" Inherits="Inventory_InventoryUpdate" %>

<%--MasterPageFile="~/AdminMaster.master"--%>
<%@ Import Namespace="Resources.Resource" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../Controls/CommonSearch/InventorySearch.ascx" TagName="InventorySearch"
    TagPrefix="uc1" %>
<asp:Content ID="cntTop" ContentPlaceHolderID="cphTop" runat="Server">
    <div>
        <div class="col1">
            <img src="../lib/image/icon/ico_admin.png" />
        </div>
        <div class="col2">
            <h1>
                Administration</h1>
            <b>Importing</b>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <h3>
            <%= Resources.Resource.lblInvDownload%>
        </h3>
    </div>
    <div class="divMainContent">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="39%" align="right">
                    <asp:Label ID="lblWarehouse" runat="server" Text="<%$ Resources:Resource, lblInvWarehouse %>"
                        CssClass="lblBold"></asp:Label>
                </td>
                <td width="2%">
                </td>
                <td width="59%" align="left">
                    <asp:DropDownList ID="dlWarehouses" runat="server" AutoPostBack="false" Width="250px"
                        CssClass="innerText">
                    </asp:DropDownList>
                    <span class="style1">*</span>
                    <asp:CustomValidator ID="custvalwarehouse" SetFocusOnError="true" runat="server"
                        ErrorMessage="<%$ Resources:Resource, custvalwarehouse %>" ClientValidationFunction="funSelectWarehouses"
                        Display="Dynamic"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblPrice" runat="server" Text="<%$ Resources:Resource, lblInvIncludePrice %>"
                        CssClass="lblBold"></asp:Label>
                </td>
                <td>
                </td>
                <td align="left">
                    <asp:RadioButton ID="radYes" runat="server" Text="<%$ Resources:Resource, lblYes %>"
                        CssClass="lblBold" GroupName="Price" />&nbsp;&nbsp;&nbsp;
                    <asp:RadioButton ID="radNo" runat="server" Text="<%$ Resources:Resource, lblNo %>"
                        CssClass="lblBold" GroupName="Price" Checked="true" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td align="left">
                    <br style="line-height: 5px;" />
                    <asp:Button ID="btnDownload" runat="server" CausesValidation="true" Text="<%$ Resources:Resource, btnInvDownload %>" />
                    <%-- <div class="buttonwrapper">
                        <a id="imgCmdDownload" runat="server" causesvalidation="true" class="ovalbutton"
                            href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                <%=Resources.Resource.btnInvDownload %></span></a></div>--%>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Label ID="lblNote" runat="server" Text="<%$ Resources:Resource, lblInvNote %>"
                        CssClass="lblBold" Style="font-size: 13px; color: Green;"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div id="div1" class="divMainContainerTitle">
        <h3>
            <%= Resources.Resource.lblInvUpload%>
        </h3>
    </div>
    <div class="divMainContent">
        <table width="100%" border="0" cellpadding="2" cellspacing="0">
            <tr>
                <td align="center" colspan="3">
                    <asp:Label ID="lblMsg" runat="server" CssClass="lblBold" Style="font-size: 13px;
                        color: Green;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="39%" align="right">
                    <asp:Label ID="lblCSV" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblInvSelectList %>"></asp:Label>
                </td>
                <td width="2%">
                </td>
                <td width="59%" align="left">
                    <asp:FileUpload ID="CSVFile" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:RadioButtonList ID="rblUpdateType" runat="server" RepeatDirection="Horizontal"
                        Visible="false">
                        <asp:ListItem Text="<%$ Resources:Resource, lblUpdateQtyOnly %>" Value="QTY" Selected="True" />
                        <asp:ListItem Text="<%$ Resources:Resource, lblResetAndUpdate %>" Value="RSU" />
                        <asp:ListItem Text="<%$ Resources:Resource, lblIncrementQtyOnly %>" Value="INC" />
                        <asp:ListItem Text="<%$ Resources:Resource, lblUpdateAllFields %>" Value="ALL" />
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td align="left">
                    <%--<br style="line-height:5px;" />--%>
                    <asp:Button ID="btnUpload" runat="server" CausesValidation="true" Text="<%$ Resources:Resource, btnInvUpload %>" />
                    <%--<div class="buttonwrapper">
                        <a id="imgCmdUpload" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                            <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                <%=Resources.Resource.btnInvUpload %></span></a></div>--%>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <%= errorMessage%>
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript" language="javascript">
        function funSelectWarehouses(source, arguments) {
            if (window.document.getElementById('<%=dlWarehouses.ClientID%>').value != "0") {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }

        function funConfirm() {
            if (window.document.getElementById('<%=dlWarehouses.ClientID%>').value != "0") {
                if (confirm("<%=Resources.Resource.msgCountUpdateWareHouse%>" +" " + window.document.getElementById('<%=dlWarehouses.ClientID%>').value + " ?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert("<%=Resources.Resource.custvalwarehouse%>");
                return false;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeft" runat="Server">
    <script language="javascript" type="text/javascript">
        //        $('#divMainContainerTitle').corner();        
    </script>
    <uc1:InventorySearch ID="InventorySearch1" runat="server" />
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="cphHead">
</asp:Content>

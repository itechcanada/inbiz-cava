﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="RoomEdit.aspx.cs" Inherits="Admin_RoomEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div id="contentBottom" style="padding: 5px; height: 120px; overflow: auto;">
         <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0" width="100%">            
             <tr id="trBuilding" runat="server">
                <td class="text">
                    <asp:Label ID="lblBuilding" Text="<%$Resources:Resource, lblBuilding%>" runat="server" />                    
                </td>
                <td>
                    <asp:DropDownList ID="ddlParentCategory" runat="server">                        
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="text">
                   <asp:Label ID="Label1" Text="<%$Resources:Resource, lblRoomType%>" runat="server" />
                </td>
                <td>
                    <asp:DropDownList ID="ddlRoomType" runat="server">                        
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ControlToValidate="ddlRoomType" SetFocusOnError="true"
                        runat="server" InitialValue="0" />
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="Label2" Text="<%$Resources:Resource, lblName%>" runat="server" /> (En)
                </td>
                <td>
                    <asp:TextBox ID="txtCategoryEn" runat="server" MaxLength="45" />
                    <asp:RequiredFieldValidator ID="rfvCategoryEn" ErrorMessage="*" ControlToValidate="txtCategoryEn" SetFocusOnError="true"
                        runat="server" />
                </td>
            </tr>
             <tr>
                <td class="text">
                    <asp:Label ID="Label3" Text="<%$Resources:Resource, lblName%>" runat="server" /> (Fr)
                </td>
                <td>
                    <asp:TextBox ID="txtCategoryFr" runat="server" MaxLength="45" />
                    <asp:RequiredFieldValidator ID="rfvCategoryFr" ErrorMessage="*" ControlToValidate="txtCategoryFr" SetFocusOnError="true"
                        runat="server" />
                </td>
            </tr>           
        </table>
    </div>

    <div class="div-dialog-command" style="border-top:1px solid #ccc; padding:5px;">
        <asp:Button Text="<%$Resources:Resource, btnSave %>" ID="btnSave" runat="server" onclick="btnSave_Click"/>
        <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
            CausesValidation="False" 
            OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


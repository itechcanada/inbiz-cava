﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="ProductQuery.aspx.cs" Inherits="Inventory_ProductQuery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <asp:Literal ID="ltTitle" Text="<%$Resources:Resource, lblProductQuery %>" runat="server" />
        </b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <asp:Button ID="btnAdd" runat="server" Text="<%$Resources:Resource, lblAddNewProduct %>"
            OnClick="btnAdd_Click" />
    </div>
    <div style="float: right;padding-top:15px;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif"
            CssClass="csv_export" />
         <font size='3px;'>  <b> <%= Resources.Resource.lblProductQueryLegend%></b></font>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        <%= Resources.Resource.lblSearchForm%></div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="Label1" runat="server" CssClass="filter-key" AssociatedControlID="dlSearch"
                        Text="<%$ Resources:Resource, lblSearchBy%>"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:DropDownList ID="dlSearch" runat="server" ValidationGroup="PrdSrch" Width="175px">
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" CssClass="filter-key" AssociatedControlID="txtSearch"
                        runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
                </div>
                <h4>
                    <asp:Label ID="Label2" CssClass="filter-key" AssociatedControlID="txtStyle" runat="server"
                        Text="<%$ Resources:Resource, lblStyle %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtStyle" CssClass="filter-key"></asp:TextBox>
                </div>
                <h4>
                    <asp:Label ID="Label3" CssClass="filter-key" AssociatedControlID="ddlCollectionSearch"
                        runat="server" Text="<%$ Resources:Resource, lblCollection %>"></asp:Label></h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlCollectionSearch" runat="server" CssClass="filter-key">
                        <asp:ListItem Text="All" Value="" Selected="True" />
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="Label4" CssClass="filter-key" AssociatedControlID="ddlMaterialSearch"
                        runat="server" Text="<%$ Resources:Resource, lblMaterial %>"></asp:Label></h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlMaterialSearch" runat="server" CssClass="filter-key">
                        <asp:ListItem Text="All" Value="" Selected="True" />
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="Label5" CssClass="filter-key" AssociatedControlID="lstColor" runat="server"
                        Text="<%$ Resources:Resource, lblPrdColor %>"></asp:Label></h4>
                <div class="inner">
                    <asp:ListBox CssClass="modernized_select" ID="lstColor" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblPrdColor  %>"></asp:ListBox>
                    <%--<asp:ListBox ID="lstColor" runat="server" SelectionMode="Multiple" CssClass="filter-key">
                    </asp:ListBox>--%>
                </div>
                <h4>
                    <asp:Label ID="Label6" CssClass="filter-key" AssociatedControlID="lstSizeGroup" runat="server"
                        Text="<%$ Resources:Resource, lblSize %>"></asp:Label></h4>
                <div class="inner">
                    <asp:ListBox ID="lstSizeGroup" runat="server" SelectionMode="Multiple" CssClass="filter-key">
                    </asp:ListBox>
                </div>
                <%--                &nbsp;<asp:LinkButton ID="lnbAdvanceSearch" runat="server" Text="<%$ Resources:Resource, lblAdvanceProductSearch %>"></asp:LinkButton>
                <iCtrl:IframeDialog ID="mdWareHouse" TriggerControlID="lnbAdvanceSearch" Width="600"
                    Height="520" Url="ProductAdvanceSearch.aspx" Title="<%$ Resources:Resource, lblAdvanceProductSearch %>"
                    runat="server">
                </iCtrl:IframeDialog>--%>
                <h4>
                    <asp:Label ID="Label7" CssClass="filter-key" AssociatedControlID="lstProdCatg" runat="server"
                        Text="<%$ Resources:Resource, lblGrdCategory %>"></asp:Label></h4>
                <div class="inner">
                    <asp:ListBox CssClass="modernized_select" ID="lstProdCatg" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblGrdCategory  %>"></asp:ListBox>
                    <%--<asp:ListBox ID="lstProdCatg" runat="server" SelectionMode="Multiple" CssClass="filter-key">
                    </asp:ListBox>--%>
                </div>
                <%--                <h4>
                    <asp:Label ID="Label8" CssClass="filter-key" AssociatedControlID="lstProdKeyWord"
                        runat="server" Text="<%$ Resources:Resource, lblKeyword %>"></asp:Label></h4>
                <div class="inner">
                    <asp:ListBox ID="lstProdKeyWord" runat="server" CssClass="filter-key"></asp:ListBox>
                </div>--%>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%= Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <div id="grid_wrapper" style="width: 100%;">
                <%--DataSourceID="sdsPropducts" Height="300px"--%>
                <trirand:JQGrid runat="server" ID="gvProducts" Height="300px" AutoWidth="True" OnCellBinding="gvProducts_CellBinding" 
                    OnDataRequesting="gvProducts_DataRequesting">
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents RowSelect="rowSelect" LoadComplete="loadComplete" />
                </trirand:JQGrid>
                <%--                <trirand:JQGrid runat="server" ID="gvProducts" Height="530px" AutoWidth="True" OnDataRequesting="gvProducts_DataRequesting"
                    OnCellBinding="gvProducts_CellBinding">
                    <Columns>
                    <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdvUPCCode %>" DataField="SKU#"
                            Sortable="true" TextAlign="Center" />
                    </Columns>
                    <PagerSettings PageSize="100" PageSizeOptions="[100,150,200]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" />
                    <ClientSideEvents LoadComplete="loadComplete" BeforeRowSelect="returnFalse" />
                </trirand:JQGrid>--%>
                <asp:SqlDataSource ID="sdsPropducts" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
                <iCtrl:IframeDialog ID="mdDeleteUser" Width="400" Height="120" Title="Delete" Dragable="true"
                    TriggerSelectorClass="pop_delete" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
                    runat="server">
                </iCtrl:IframeDialog>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
    <script type="text/javascript">
        $grid = $("#<%=gvProducts.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=gvProducts.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        function loadComplete(data) {
          
            if (data.rows.length == 1 && data.total == 1) {
//                location.href = 'Product.aspx?PrdID=' + data.rows[0].id + '&Kit=False&ptype=<%=Request.QueryString["ptype"]%>';
            }
            else {
                jqGridResize();
            }
           
        }

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function rowSelect(id) {
            //alert(jQuery("#" + gridID).getGridParam('selarrrow'));

        }

        function deleteRow(rowID) {
            // Get the currently selected row
            $('#' + gridID).setSelection(rowID);
            var rowKey = $('#' + gridID).getGridParam("selrow");
            $('#' + gridID).jqGrid('delGridRow', rowKey, {
                errorTextFormat: function (data) {
                    if (data.responseText.substr(0, 6) == "<html ") {
                        return jQuery(data.responseText).html();
                    }
                    else {
                        return "<%=Resources.Resource.msgProductCanNotBeDeleted%>";
                        // or
                        // return "Status: '" + data.statusText + "'. Error code: " +data.status;
                    }
                }
            });
        }

        function editSeq(source) {
            var grid = $("#<%=gvProducts.ClientID %>");
            grid.setSelection($(source).attr("row-key"));
            var dataToPost = {};
            dataToPost["editSeq"] = 1;
            dataToPost["editRowKey"] = $(source).attr("row-key");
            dataToPost["seq"] = $(source).val();

            for (var k in dataToPost) {
                grid.setPostDataItem(k, dataToPost[k]);
            }
            grid.trigger("reloadGrid");

            for (var k in dataToPost) {
                grid.removePostDataItem(k);
            }
        }

        $(document).ready(function () {
            loadSearchControl();
        });

        function loadSearchControl() {
            ReloadSearchConnectionList();
            ReloadSearchMaterialList();
            //            ReloadSearchColorList();
            ReloadSearchCatgSizeList();
            //            ReloadSearchCatgList();
        }

        function ReloadSearchConnectionList() {
            $.ajax({
                type: "POST",
                url: "productMaster.aspx/GetCollectionList",
                dataType: "json",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var addressList = data.d;
                    $('option', $("#<%=ddlCollectionSearch.ClientID%>")).remove();
                    $("#<%=ddlCollectionSearch.ClientID%>").append($("<option />").val("").text("<%=Resources.Resource.lblAll%>"));
                    for (var i = 0; i < addressList.length; i++) {
                        $("#<%=ddlCollectionSearch.ClientID%>").append($("<option />").val(addressList[i].CollectionID).text(addressList[i].ShortName));
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }

        function ReloadSearchMaterialList() {
            $.ajax({
                type: "POST",
                url: "productMaster.aspx/GetMaterialList",
                dataType: "json",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var addressList = data.d;
                    $('option', $("#<%=ddlMaterialSearch.ClientID%>")).remove();
                    $("#<%=ddlMaterialSearch.ClientID%>").append($("<option />").val("").text("<%=Resources.Resource.lblAll%>"));
                    for (var i = 0; i < addressList.length; i++) {
                        //$("#<%=ddlMaterialSearch.ClientID%>").append($("<option />").val(addressList[i].MaterialID).text(addressList[i].MaterialShortName));
                        $("#<%=ddlMaterialSearch.ClientID%>").append($("<option />").val(addressList[i].MaterialID).text(addressList[i].MaterialName));
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }

        //        function ReloadSearchColorList() {
        //            $.ajax({
        //                type: "POST",
        //                url: "productMaster.aspx/GetColorList",
        //                dataType: "json",
        //                data: "{}",
        //                contentType: "application/json; charset=utf-8",
        //                success: function (data) {
        //                    var addressList = data.d;
        //                    $('option', $("#<%=lstColor.ClientID%>")).remove();
        //                    $("#<%=lstColor.ClientID%>").append($("<option />").val("0").text("<%=Resources.Resource.lblAll%>"));
        //                    var sOption = '';
        //                    for (var i = 0; i < addressList.length; i++) {
        //                        //$("#<%=lstColor.ClientID%>").append($("<option />").val(addressList[i].ColorID).text(addressList[i].ShortName));
        //                        $("#<%=lstColor.ClientID%>").append($("<option />").val(addressList[i].ColorID).text(addressList[i].ColorName));
        //                    }
        //                },
        //                error: function (XMLHttpRequest, textStatus, errorThrown) {
        //                }
        //            });
        //        }

        function ReloadSearchCatgSizeList() {
            $.ajax({
                type: "POST",
                url: "productMaster.aspx/GetSizeList",
                dataType: "json",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var sSelGroup = '';
                    var addressList = data.d;
                    $('option', $("#<%=lstSizeGroup.ClientID%>")).remove();
                    $("#<%=lstSizeGroup.ClientID%>").append($("<option />").val("0").text("<%=Resources.Resource.lblAll%>"));
                    for (var i = 0; i < addressList.length; i++) {
                        $("#<%=lstSizeGroup.ClientID%>").append($("<option />").val(addressList[i].SizeID).text(addressList[i].Size));

                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }

        //        function AdvanceSearch(sWebSite) {
        //            alert(sWebSite);
        //        }

    </script>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Inventory_AddSizeGroup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MessageState.SetGlobalMessage(MessageType.Success, "");
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                {
                    if (BusinessUtility.GetInt(Request.QueryString["SizeGroupID"]) > 0)
                    {
                        ProductSizeGroup objProductSizeGroup = new ProductSizeGroup();
                        objProductSizeGroup.SizeGroupID = BusinessUtility.GetInt(Request.QueryString["SizeGroupID"]);
                        var lst = objProductSizeGroup.GetAllSizeGroupList(null, Globals.CurrentAppLanguageCode);
                        foreach (var item in lst)
                        {
                            txtSizeGroup.Text = item.SizeGroupName;
                            rblstActive.SelectedValue = BusinessUtility.GetBool(item.SizeGroupIsActive) ? "1" : "0";
                        }

                    }
                }
                txtSizeGroup.Focus();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            ProductSizeGroup objProductSizeGroup = new ProductSizeGroup();
            objProductSizeGroup.SizeGroupName = txtSizeGroup.Text;
            objProductSizeGroup.SizeGroupIsActive = BusinessUtility.GetInt(rblstActive.SelectedItem.Value);
            if (objProductSizeGroup.SaveSizeGroup(null, CurrentUser.UserID, BusinessUtility.GetInt(Request.QueryString["SizeGroupID"])) == true)
            {
                if (BusinessUtility.GetInt(Request.QueryString["SizeGroupID"]) > 0)
                {
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgGroupUpdated);
                    Globals.RegisterReloadParentScript(this);
                    Globals.RegisterCloseDialogScript(this);
                    return;
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgGroupCreated);
                    Globals.RegisterReloadParentScript(this);
                    Globals.RegisterCloseDialogScript(this);
                    return;
                }
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadStyle({0});", objProductSizeGroup.SizeGroupID), true);
            }
            else
            {
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }

        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
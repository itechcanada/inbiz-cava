﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="ProductKit.aspx.cs" Inherits="Inventory_ProductKit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div id="contentBottom" style="padding: 5px; height: 150px; overflow: auto;">
        <table class="contentTableForm" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="text">
                     <asp:Label ID="Label1" Text="<%$ Resources:Resource, lblPrdProductName %>" CssClass="lblBold"
                                runat="server" />*
                </td>
                <td class="input">
                    <asp:DropDownList ID="dlProduct" runat="server" Width="200px" 
                        CssClass="innerText">
                            </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvProduct" ErrorMessage="<%$ Resources:Resource, CustvalProd %>" ControlToValidate="dlProduct"
                        runat="server" InitialValue="0" Text="*" />
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblQuantity" Text="<%$ Resources:Resource, lblPrdQuantity %>" CssClass="lblBold"
                                runat="server" />*
                </td>
                <td class="input">
                    <asp:TextBox ID="txtProdQty" runat="server" MaxLength="20" CssClass="innerText" />
                    <asp:RequiredFieldValidator ID="reqvalProdQty" runat="server" ControlToValidate="txtProdQty"
                        ErrorMessage="<%$ Resources:Resource, reqvalProdQty %>" SetFocusOnError="true"
                        Display="None"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cmvProdQty" EnableClientScript="true" SetFocusOnError="true"
                        ControlToValidate="txtProdQty" Operator="DataTypeCheck" Type="Double" Display="None"
                        ErrorMessage="<%$ Resources:Resource, cmvProdQty %>" runat="server" />
                </td>
            </tr>
        </table>
    </div>
     <div style="text-align: right; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" 
             onclick="btnSave_Click" />
        <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
            CausesValidation="False" 
            OnClientClick=" jQuery.FrameDialog.closeDialog(); return false;" />
    </div>   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


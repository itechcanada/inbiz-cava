﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Inventory_ManageMinDays : BasePage
{
    ReservationSpecialDays _minDays = new ReservationSpecialDays();

    protected void Page_Load(object sender, EventArgs e)
    {
        txtNameEn.Focus();
        if (!IsPagePostBack(grdMinDays))
        {
            chkBuildings.DataSource = new Buildings().GetAllBuildings(Globals.CurrentAppLanguageCode);            
            chkBuildings.DataBind();
                      
        }
    }
    protected void grdMinDays_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        //Add/Edit/Delete
        try
        {
            if (Request.QueryString.AllKeys.Contains("_command"))
            {
                _minDays.SpecialDayID = BusinessUtility.GetInt(Request.QueryString["_minid"]);
                if (Request.QueryString["_command"] == "add")
                {
                    _minDays.IfCheckinDate = BusinessUtility.GetDateTime(Request.QueryString["_cindate"], DateFormat.MMddyyyy);
                    _minDays.IsActive = BusinessUtility.GetBool(Request.QueryString["_isactive"]);
                    _minDays.MinDaysToBeReserved = BusinessUtility.GetInt(Request.QueryString["_mindays"]);
                    _minDays.SpecialNameEn = BusinessUtility.GetString(Request.QueryString["_nameen"]);
                    _minDays.SpecialNameFr = BusinessUtility.GetString(Request.QueryString["_namefr"]);
                    _minDays.MessageToDisplayEn = BusinessUtility.GetString(Request.QueryString["_msgen"]);
                    _minDays.MessageToDisplayFr = BusinessUtility.GetString(Request.QueryString["_msgfr"]);
                    int id = 0;
                    string[] arrStr = BusinessUtility.GetString(Request.QueryString["_buildings"]).Split(',');
                    foreach (var item in arrStr)
                    {
                        id = BusinessUtility.GetInt(item);
                        if (id > 0)
                        {
                            _minDays.Buildings.Add(id);   
                        }                        
                    }
                    _minDays.Insert();
                }
                else if (Request.QueryString["_command"] == "edit")
                {
                    _minDays.IfCheckinDate = BusinessUtility.GetDateTime(Request.QueryString["_cindate"], DateFormat.MMddyyyy);
                    _minDays.IsActive = BusinessUtility.GetBool(Request.QueryString["_isactive"]);
                    _minDays.MinDaysToBeReserved = BusinessUtility.GetInt(Request.QueryString["_mindays"]);
                    _minDays.SpecialNameEn = BusinessUtility.GetString(Request.QueryString["_nameen"]);
                    _minDays.SpecialNameFr = BusinessUtility.GetString(Request.QueryString["_namefr"]);
                    _minDays.MessageToDisplayEn = BusinessUtility.GetString(Request.QueryString["_msgen"]);
                    _minDays.MessageToDisplayFr = BusinessUtility.GetString(Request.QueryString["_msgfr"]);
                    int id = 0;
                    string[] arrStr = BusinessUtility.GetString(Request.QueryString["_buildings"]).Split(',');
                    foreach (var item in arrStr)
                    {
                        id = BusinessUtility.GetInt(item);
                        if (id > 0)
                        {
                            _minDays.Buildings.Add(id);
                        }
                    }
                    _minDays.Update();
                }
                else if (Request.QueryString["_command"] == "delete")
                {
                    _minDays.Delete(_minDays.SpecialDayID);
                }
            }
        }
        finally
        {

        }

        if (Request.QueryString.AllKeys.Contains("_history"))
        {
            sdsMinDays.SelectCommand = _minDays.GetSql(sdsMinDays.SelectParameters, BusinessUtility.GetDateTime(Request.QueryString[txtSearch.ClientID], DateFormat.MMddyyyy), Globals.CurrentAppLanguageCode);
        }
        else
        {
            sdsMinDays.SelectCommand = _minDays.GetSql(sdsMinDays.SelectParameters, BusinessUtility.GetDateTime(txtSearch.Text, DateFormat.MMddyyyy), Globals.CurrentAppLanguageCode);
        }
    }

    protected void grdMinDays_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 2)
        {
            e.CellHtml = BusinessUtility.GetDateTime(e.RowValues[3]).ToString("MMM dd, yyyy");
        }
        else if(e.ColumnIndex == 5)
        {
            e.CellHtml = string.Format("<a href='javascript:;' onclick='setRowSelectedToUpdate({0})'>{1}</a>", e.RowKey, Resources.Resource.lblEdit);
        }
        else if (e.ColumnIndex == 6)
        {
            e.CellHtml = string.Format("<a href='javascript:;' onclick='deleteRow({0})'>{1}</a>", e.RowKey, Resources.Resource.delete);
        }
    }

    [System.Web.Services.WebMethod]
    public static ReservationSpecialDays GetSelectedItem(int id)
    {
        if (!CurrentUser.IsAutheticated) return null;
        ReservationSpecialDays rm = new ReservationSpecialDays();
        try
        {
            rm.PopulateObject(id);
            return rm;
        }
        finally
        {

        }
    }
}
Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Partial Class eComMetaTags
    Inherits BasePage
    Protected objMetaTag As New clseComMetaTags
    ' Page Load Event
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Session("msg") <> "" Then
            lblMsg.Text = Session("msg").ToString
            Session.Remove("msg")
        End If
        If Not Page.IsPostBack Then
            subFillData()
        End If
        txteComTitle.Focus()
    End Sub
    'Databse to User Interface
    Public Sub subFillData()
        objMetaTag.WhsCode = "--" ' Session("UserWarehouse").ToString
        objMetaTag.DescLang = rblLanguage.SelectedValue
        objMetaTag.subGeteComMetaTagInfoByWhsCode()
        txteComTitle.Text = objMetaTag.Title
        txteComKeyWords.Text = objMetaTag.Keywords
        txteComDesc.Text = objMetaTag.TagDesc
    End Sub
    'User Interface to Database
    Public Sub subSetData()
        objMetaTag.WhsCode = "--" ' Session("UserWarehouse").ToString
        objMetaTag.Title = txteComTitle.Text
        objMetaTag.Keywords = txteComKeyWords.Text
        objMetaTag.TagDesc = txteComDesc.Text
        objMetaTag.DescLang = rblLanguage.SelectedValue
    End Sub
    'fill detail if selected Language changed.
    Protected Sub rblLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblLanguage.SelectedIndexChanged
        lblMsg.Text = ""
        Session.Remove("msg")
        subFillData()
    End Sub
    'Saving the Info in Database
    Protected Sub imgCmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdSave.ServerClick
        subSetData()
        If objMetaTag.funGetDuplicateeComMetaTag() = True Then
            objMetaTag.TagID = objMetaTag.funGetTagID()
            objMetaTag.funUpdateeComMetaTag()
        Else
            objMetaTag.funInserteComMetaTag()
        End If
        Session.Add("msg", msgeComMetaTagUpdateSuccessfully)
        lblMsg.Text = Session("msg").ToString
    End Sub
End Class

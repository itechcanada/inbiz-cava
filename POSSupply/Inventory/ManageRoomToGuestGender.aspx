﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ManageRoomToGuestGender.aspx.cs" Inherits="Inventory_ManageRoomToGuestGender" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphTop" Runat="Server">
     <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <asp:Literal ID="ltTitle" Text="<%$Resources:Resource, lblAssociateSex%>" runat="server" /> </b>
    </div>
    <div style="float: left; padding-top: 12px;">        
       
    </div>
    <div style="float: right;">
        
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" Runat="Server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <h3>
        <asp:Label ID="lbl" Text="<%$Resources:Resource, lblAssociateSex%>" runat="server" />
    </h3>
    <div id="contentBottom" style="padding: 5px;">
         <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0" width="100%">            
            <tr id="trBuilding" runat="server">
                <td class="text">
                    <asp:Label ID="lblBuilding" Text="<%$Resources:Resource, lblBuilding%>" runat="server" />                    
                </td>
                <td>                    
                    <asp:DropDownList CssClass="modernized_select" ID="ddlBuilding" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlBuilding_SelectedIndexChanged" AppendDataBoundItems="true" Width="250px">   
                         <asp:ListItem Value="" Text="" />                        
                    </asp:DropDownList>
                </td>
            </tr>
             <tr>
                <td class="text">
                    <asp:Label ID="lblRoom" Text="<%$Resources:Resource, lblRoom%>" runat="server" />                               
                </td>
                <td>                    
                    <asp:ListBox CssClass="modernized_select" ID="ddlRoom" runat="server" 
                        Width="450px" AutoPostBack="false" SelectionMode="Multiple" 
                        onselectedindexchanged="ddlRoom_SelectedIndexChanged">                                          
                    </asp:ListBox>
                </td>
            </tr> 
            <tr>
                <td class="text">
                    <asp:Label ID="Label1" Text="<% $Resources:Resource, lblAllowtoReserveFor%>" runat="server" />
                </td>
                <td>
                    <asp:RadioButtonList ID="rblReserveFor" runat="server" 
                        RepeatDirection="Horizontal">                                                
                    </asp:RadioButtonList>
                    <div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ControlToValidate="rblReserveFor"
                            runat="server" Display="Dynamic" />
                    </div>
                </td>
            </tr>                  
        </table>
    </div>

    <div class="div-dialog-command" style="border-top:1px solid #ccc; padding:5px;">
        <asp:Button Text="<%$Resources:Resource, btnSave %>" ID="btnSave" runat="server" onclick="btnSave_Click"/>
        <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
            CausesValidation="False" 
            OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" Visible="false" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


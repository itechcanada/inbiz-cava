﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AddGroupMaterial.aspx.cs" Inherits="Inventory_AddGroupMaterial" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="dialogAssignUser" style="padding: 5px;">
        <h3>
            <asp:Label ID="Label1" Text="<%$Resources:Resource, lblSelectMaterial%>" runat="server" />
        </h3>
        <div style="float: right; padding-bottom: 10px;">
            <input type="button" id="btnAddNewMaterial" value="<%=Resources.Resource.btnAddNewMaterial%>"
                onclick='AddNewMaterial();' />
        </div>
        <asp:ListBox CssClass="modernized_select" ID="ddlUsers" runat="server" Width="600px" data-placeholder="<%$ Resources:Resource, lblMaterial %>"
            SelectionMode="Multiple"></asp:ListBox>
    </div>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
            OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        function AddNewMaterial() {
            var url = 'AddNewMaterial.aspx';
            var queryData = {};
            queryData.aName = "";
            var t = "<%=Resources.Resource.btnAddNewMaterial%>";
            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 420,
                height: 300,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');
            return false;
        }

        function MaterialCreated(MaterialID) {
            var urlLocation = window.location.href + "&MaterialID=" + MaterialID;
            window.location.href = urlLocation;
        }
    </script>
</asp:Content>


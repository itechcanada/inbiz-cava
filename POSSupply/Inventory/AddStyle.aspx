﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AddStyle.aspx.cs" Inherits="Inventory_AddStyle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="dialogAssignUser" style="padding: 5px;">
        <h3>
            <asp:Label ID="Label1" Text="<%$Resources:Resource, lblAddnewStyle%>" runat="server" />
        </h3>
        <table>
            <tr>
                <td>
                <%= Resources.Resource.lblStyleName%>
                </td>
                <td>
                    <asp:TextBox ID="txtStyleName" runat="server" MaxLength="250"/>
                </td>
            </tr>
        </table>
    </div>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
            OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">

</asp:Content>

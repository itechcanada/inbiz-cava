﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="ManageMinDays.aspx.cs" Inherits="Inventory_ManageMinDays" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <%=UIHelper.GetScriptTags("blockui", 1)%>
    <script type="text/javascript">
        function blockContentArea(elementToBlock, loadingMessage) {
            $(elementToBlock).block({
                message: '<div>' + loadingMessage + '</div>',
                css: { border: '3px solid #a00' }
            });
        }
        function unblockContentArea(elementToBlock) {
            $(elementToBlock).unblock();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
    </div>
    <div style="float: left; padding-top: 12px;">
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif"
            CssClass="csv_export" />
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <h3>
        <asp:Label ID="lblManageSpecials" Text="<%$Resources:Resource, lblManageSpecials%>"
            runat="server" />
    </h3>
    <div id="divForm" class="">
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="Label3" Text="Name(En)" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtNameEn" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="*" ControlToValidate="txtNameEn" SetFocusOnError="true"
                        runat="server" />
                </div>
                <div class="lbl">
                    <asp:Label ID="Label6" Text="Name(Fr)" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtNameFr" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="*" ControlToValidate="txtNameFr" SetFocusOnError="true"
                        runat="server" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="Label1" Text="<%$Resources:Resource, lblCheckIn%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCheckInDate" runat="server" CssClass="datepicker" MaxLength="15"
                        Width="70px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ControlToValidate="txtCheckInDate" SetFocusOnError="true"
                        runat="server" />
                </div>
                <div class="lbl">
                    <asp:Label ID="Label2" Text="<%$Resources:Resource, lblMinDays%>" runat="server" />
                </div>
                <div class="input">
                    <asp:DropDownList ID="ddlMinDays" runat="server" Width="50px">
                        <asp:ListItem Text="2" />
                        <asp:ListItem Text="3" />
                        <asp:ListItem Text="4" />
                        <asp:ListItem Text="5" />
                        <asp:ListItem Text="6" />
                        <asp:ListItem Text="7" />
                        <asp:ListItem Text="8" />
                        <asp:ListItem Text="9" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="11" />
                        <asp:ListItem Text="12" />
                        <asp:ListItem Text="13" />
                        <asp:ListItem Text="14" />
                        <asp:ListItem Text="15" />
                    </asp:DropDownList>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            
            <li style="display:none;">
                <div class="lbl">
                    <asp:Label ID="Label4" Text="Message(En)" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtMessageEn" runat="server" TextMode="MultiLine" 
                        Height="70px" Width="250px" />
                </div>
                <div class="lbl">
                    <asp:Label ID="Label5" Text="Message(Fr)" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtMessageFr" runat="server" TextMode="MultiLine" 
                        Height="70px" Width="250px" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="Label8" Text="Apply To Building(s)" runat="server" />
                </div>
                <div class="input">
                    <asp:DataList ID="chkBuildings" RepeatColumns="2" runat="server">
                        <ItemTemplate>
                            <input id="chkBuilding" type="checkbox" value='<%#Eval("BuildingID")%>' runat="server" />
                            <asp:Label ID="lblBui" AssociatedControlID="chkBuilding" Text='<%#Eval("BuildingTitle")%>' runat="server" />
                        </ItemTemplate>
                    </asp:DataList>                    
                    <%--<input type="button" id="bntTest"/>--%>
                    <script type="text/javascript">
                        $("#bntTest").click(function () {
                            var buil = [];
                            $("#<%=chkBuildings.ClientID%> input:checked").each(function () {
                                buil.push($(this).val());
                            });
                            alert(buil.join(","));
                        });
                    </script>
                </div>
                <div class="lbl">
                    <asp:Label ID="Label7" AssociatedControlID="chkIsActive" Text="<%$Resources:Resource, lblIsActive%>" runat="server" />                    
                </div>
                <div class="input">
                    <asp:CheckBox ID="chkIsActive" Text="" Checked="true"
                        runat="server" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
        <div class="div_command">
            <asp:HiddenField ID="hdnID" runat="server" Value="0" />
            <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" />
            <asp:Button ID="btnReset" Text="<%$Resources:Resource, Reset%>" runat="server" CausesValidation="false" />
        </div>
    </div>
    <div id="grid_wrapper" style="width: 100%;">
        <trirand:JQGrid runat="server" ID="grdMinDays" DataSourceID="sdsMinDays" Height="300px"
            AutoWidth="True" OnDataRequesting="grdMinDays_DataRequesting" OnCellBinding="grdMinDays_CellBinding">
            <Columns>
                <trirand:JQGridColumn DataField="SpecialDayID" HeaderText="" Editable="false" PrimaryKey="True"
                    Visible="false" />
                 <trirand:JQGridColumn DataField="Name" HeaderText="Name" Editable="false"
                    TextAlign="Center" />
                <trirand:JQGridColumn DataField="IfCheckinDate" HeaderText="Check-In Date" Editable="false"
                    TextAlign="Center" />
                <trirand:JQGridColumn DataField="MinDaysToBeReserved" HeaderText="Min Days" Editable="false"
                    TextAlign="Center" />
                <trirand:JQGridColumn DataField="IsActive" HeaderText="Is Active" Editable="false"
                    TextAlign="Center" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="SpecialDayID"
                    Sortable="false" TextAlign="Center" Width="50" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="SpecialDayID"
                    Sortable="false" TextAlign="Center" Width="50" />
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="loadComplete" />
        </trirand:JQGrid>
        <asp:SqlDataSource ID="sdsMinDays" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
        <iCtrl:IframeDialog ID="mdEdit" Width="400" Height="240" Title="Edit Building" Dragable="true"
            TriggerSelectorClass="pop_edit" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
            runat="server">
        </iCtrl:IframeDialog>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        Search form</div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label CssClass="filter-key" ID="lblSearchKeyword" AssociatedControlID="txtSearch"
                        runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="Search" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%= Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdMinDays.ClientID %>";
        var searchPanelID = "<%=SearchPanel.ClientID %>";


        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function deleteRow(rowID) {
            // Get the currently selected row
            $('#' + gridID).setSelection(rowID);
            var rowKey = $('#' + gridID).getGridParam("selrow");

        }

        function loadComplete() {
            resize_the_grid();
            getGlobalMessage();
            unblockContentArea($("#divForm"));
        }

        $("#<%=btnSave.ClientID%>").click(function () {
            var id = $("#<%=hdnID.ClientID%>").val();
            var grid = $("#" + gridID);
            if (Page_IsValid) {
                blockContentArea($("#divForm"), "Loading...");
                var dataToPost = {};
                dataToPost["_minid"] = id;
                dataToPost["_command"] = id > 0 ? "edit" : "add";
                dataToPost["_cindate"] = $("#<%=txtCheckInDate.ClientID%>").val();
                dataToPost["_mindays"] = $("#<%=ddlMinDays.ClientID%>").val();
                dataToPost["_isactive"] = $("#<%=chkIsActive.ClientID%>").is(":checked");
                dataToPost["_nameen"] = $("#<%=txtNameEn.ClientID%>").val();
                dataToPost["_namefr"] = $("#<%=txtNameFr.ClientID%>").val();
                dataToPost["_msgen"] = $("#<%=txtMessageEn.ClientID%>").val();
                dataToPost["_msgfr"] = $("#<%=txtMessageFr.ClientID%>").val();
                var buil = [];
                $("#<%=chkBuildings.ClientID%> input:checked").each(function () {
                    buil.push($(this).val());
                });
                dataToPost["_buildings"] = buil.join(",");
                for (var k in dataToPost) {
                    grid.setPostDataItem(k, dataToPost[k]);
                }
                grid.trigger("reloadGrid");
                for (var k in dataToPost) {
                    grid.removePostDataItem(k);
                }
                resetForm();
            }
            return false;
        });

        $("#<%=btnReset.ClientID%>").click(function () {
            resetForm();
            return false;
        });

        function resetForm() {
            var grid = $("#" + gridID);

            $("#<%=hdnID.ClientID%>").val("0");
            $("#<%=ddlMinDays.ClientID%>").val("2");
            $("#<%=txtCheckInDate.ClientID%>").val("");
            $("#<%=txtNameEn.ClientID%>").val("");
            $("#<%=txtNameFr.ClientID%>").val("");
            $("#<%=txtMessageEn.ClientID%>").val("");
            $("#<%=txtMessageFr.ClientID%>").val("");
            $("#<%=chkBuildings.ClientID%> input").each(function () {
                this.checked = false;
            });
            grid.jqGrid('resetSelection');
        }

        function setRowSelectedToUpdate(id) {
            var grid = $("#" + gridID);
            grid.setSelection(id);
            blockContentArea($("#divForm"), "Loading...");
            var dataToPost = {};
            dataToPost["id"] = id;
            $.ajax({
                type: "POST",
                url: "ManageMinDays.aspx/GetSelectedItem",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(dataToPost), //"{'roomid' : '" + $(this).val() + "'}",
                success: function (data) {
                    if (data.d == null) {
                        return;
                    }
                    $("#<%=txtCheckInDate.ClientID%>").datepicker("setDate", new Date(+data.d.IfCheckinDate.replace(/\/Date\((-?\d+)\)\//gi, "$1")));
                    $("#<%=ddlMinDays.ClientID%>").val(data.d.MinDaysToBeReserved);
                    $("#<%=chkIsActive.ClientID%>").attr("checked", data.d.IsActive);
                    $("#<%=hdnID.ClientID%>").val(data.d.SpecialDayID);
                    $("#<%=txtNameEn.ClientID%>").val(data.d.SpecialNameEn);
                    $("#<%=txtNameFr.ClientID%>").val(data.d.SpecialNameFr);
                    $("#<%=txtMessageEn.ClientID%>").val(data.d.MessageToDisplayEn);
                    $("#<%=txtMessageFr.ClientID%>").val(data.d.MessageToDisplayFr);
                    $("#<%=chkBuildings.ClientID%> input").each(function () {
                        this.checked = false;
                    });
                    for (var i = 0; i < data.d.Buildings.length; i++) {
                        $("#<%=chkBuildings.ClientID%> input").each(function () {
                            if (!this.checked) {
                                this.checked = this.value == data.d.Buildings[i];
                            }
                        });
                    }
                    unblockContentArea($("#divForm"));
                },
                error: function (request, status, errorThrown) {
                    alert(status);
                    unblockContentArea($("#divForm"));
                }
            });
        }

        function deleteRow(id) {            
            var grid = $("#" + gridID);
            if (confirm("Do you want to delete record?")) {                
                var dataToPost = {};
                dataToPost["_minid"] = id;
                dataToPost["_command"] = "delete";
                for (var k in dataToPost) {
                    grid.setPostDataItem(k, dataToPost[k]);
                }
                grid.trigger("reloadGrid");
                for (var k in dataToPost) {
                    grid.removePostDataItem(k);
                }
                resetForm();
            }
        }
        
    </script>
</asp:Content>

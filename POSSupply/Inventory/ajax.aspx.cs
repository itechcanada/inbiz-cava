﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.WebControls;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;

public partial class Inventory_ajax : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static List<OptionList>[] GetProductListToAssing(string productID) {
        List<OptionList>[] arrLst = new List<OptionList>[2];
        int prdID = 0;
        int.TryParse(productID, out prdID);
        if (prdID > 0) {
            arrLst[0] = ProcessInventory.GetProductListToAssign(prdID, Globals.CurrentAppLanguageCode);
            arrLst[1] = ProcessInventory.GetProductListAssigned(prdID, Globals.CurrentAppLanguageCode);
        }
        else
        {
            arrLst[0] = new List<OptionList>();
            arrLst[1] = new List<OptionList>();
        }

        return arrLst;
    }

    //#region Product Discount Helper
    //[System.Web.Services.WebMethod]
    //[System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    //public static ProductDiscount GetProductDiscount(int prdDiscountID)
    //{
    //    ProductDiscount dic = new ProductDiscount();
    //    dic.PopulateObject(prdDiscountID);

    //    return dic;
    //}

    //[System.Web.Services.WebMethod]
    //[System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    //public static bool DeleteProductDiscount(int prdDiscountID)
    //{
    //    try
    //    {
    //        ProductDiscount dic = new ProductDiscount();
    //        dic.Delete(prdDiscountID);

    //        return true;
    //    }
    //    catch
    //    {
    //        return false;
    //    }
    //}

    //[System.Web.Services.WebMethod]
    //[System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    //public static DiscountView GetProductDiscountByRoom(string itemID)
    //{
    //    ProductDiscount dis = new ProductDiscount();
    //    return dis.GetDiscountView(itemID);
    //}

    //[System.Web.Services.WebMethod]
    //[System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    //public static bool DeleteDiscountByRoom(string rowKey, int[] roomIds)
    //{
    //    try
    //    {
    //        ProductDiscount dic = new ProductDiscount();
    //        dic.DeleteByRoomID(rowKey, roomIds);

    //        return true;
    //    }
    //    catch
    //    {
    //        return false;
    //    }
    //}  
    //#endregion

    #region Product Black Out Helper
    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static ProductBlackOut GetProductBlackOut(int itemID)
    {
        ProductBlackOut dic = new ProductBlackOut();
        dic.PopulateObject(itemID);

        return dic;
    }
    

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static bool DeleteBlackOuts(int itemID)
    {
        try
        {
            ProductBlackOut dic = new ProductBlackOut();
            dic.Delete(itemID);

            return true;
        }
        catch
        {
            return false;
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static BlackoutView GetProductBlackOutByRoom(string itemID)
    {
        ProductBlackOut dic = new ProductBlackOut();
        return dic.GetBlackout(itemID);
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static bool DeleteBlackOutsByRoom(string rowKey, int[] roomIds)
    {
        try
        {
            ProductBlackOut dic = new ProductBlackOut();
            dic.DeleteByRoomID(rowKey, roomIds);

            return true;
        }
        catch
        {
            return false;
        }
    }   
    #endregion
}
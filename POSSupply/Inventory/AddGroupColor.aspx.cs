﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Inventory_AddGroupColor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                MessageState.SetGlobalMessage(MessageType.Success, "");
                ProductColor objProductColor = new ProductColor();
                //InbizUser user = new InbizUser();
                //user.FillAllUsers(dbHelp, ddlUsers, null);
                //NewAlert al = new NewAlert();
                //objProductColor.FillAllColor(dbHelp, ddlUsers,null , Globals.CurrentAppLanguageCode);

                ddlUsers.DataSource = objProductColor.GetColorList(null, Globals.CurrentAppLanguageCode);
                ddlUsers.DataTextField = "ColorName";
                ddlUsers.DataValueField = "ColorID";
                ddlUsers.DataBind();

                ProductColorGroup objColorGroup = new ProductColorGroup();

                var lst = objColorGroup.GetGroupColor(dbHelp, BusinessUtility.GetInt(this.StyleID), Globals.CurrentAppLanguageCode);
                foreach (var item in lst)
                {
                    ListItem li = ddlUsers.Items.FindByValue(item.ColorID.ToString());
                    if (li != null)
                    {
                        li.Selected = true;
                    }
                }

                string newColorID = BusinessUtility.GetString(Request.QueryString["ColorID"]);
                if (newColorID != "")
                {
                    string[] sColorID = newColorID.Split(',');

                    foreach (string ColorID in sColorID)
                    {
                        ListItem li = ddlUsers.Items.FindByValue(ColorID);
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }
                }

                ddlUsers.Focus();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    private string StyleID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["StyleID"]);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {

            ProductColorGroup al = new ProductColorGroup();
            var lst = new List<int>();
            int id = 0;
            foreach (ListItem item in ddlUsers.Items)
            {
                id = 0;
                if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lst.Contains(id))
                {
                    lst.Add(id);

                }
            }

            if (al.AddColorInGroup(BusinessUtility.GetInt(this.StyleID), lst.ToArray()) == true)
            {
                //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated );
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadStyle();"), true);
            }
            else
            {
                //MessageState.SetGlobalMessage(MessageType.Failure, "Collection Creation failure");
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ProductColor> GetColorList()
    {
        ProductColor objProductColor = new ProductColor();
        var t = objProductColor.GetColorList(null, Globals.CurrentAppLanguageCode);
        return t;
    }
}
<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="~/Inventory/eComManageWebPages.aspx.vb" Inherits="eComManageWebPages" ValidateRequest="false" %>

<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>

<%@ Register src="../Controls/CommonSearch/CommonKeyword.ascx" tagname="CommonKeyword" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
   <script language="javascript" type="text/javascript">
       $('#divMainContainerTitle').corner();        
    </script>

    <uc1:CommonKeyword ID="CommonKeyword1" runat="server" />

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal ID="lblHeading" runat="server" Text="<%$ Resources:Resource, lbleComManageWebpages %>"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
    </div>
    <div class="divMainContent">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td style="height: 15px">
                    </td>
                    <td style="height: 15px">
                    </td>
                    <td style="height: 15px" align="left">
                        <asp:Label ID="lblMsg" runat="server" Font-Bold="true" Text="" ForeColor="green"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="10">
                    </td>
                </tr>
                <tr height="25">
                    <td class="tdAlign">
                        <asp:Label ID="lblSelectLang" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, SelectLanguage%>"></asp:Label>
                    </td>
                    <td>
                    </td>
                    <td align="left">
                        <asp:RadioButtonList ID="rblLanguage" runat="server" RepeatDirection="Horizontal"
                            AutoPostBack="true">
                            <asp:ListItem Text="<%$ Resources:Resource, English%>" Value="en" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, French%>" Value="fr"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, Spanish%>" Value="sp"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td height="10">
                    </td>
                </tr>
                <tr>
                    <td class="tdAlign" width="18%">
                        <asp:Label ID="lblEditContent" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblEditContent %>"></asp:Label>
                    </td>
                    <td>
                    </td>
                    <td align="left">
                        <asp:RadioButtonList ID="rblWebPage" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
                            RepeatColumns="4">
                            <asp:ListItem Value="HomePage" Text="<%$ Resources:Resource, HomePage %>"> </asp:ListItem>
                            <asp:ListItem Value="AboutUs" Text="<%$ Resources:Resource, AboutUs %>"></asp:ListItem>
                            <asp:ListItem Value="ContactUs" Text="<%$ Resources:Resource, ContactUs %>"></asp:ListItem>
                            <asp:ListItem Value="PrivacyStatement" Text="<%$ Resources:Resource, PrivacyStatement %>"></asp:ListItem>
                            <asp:ListItem Value="TermsNConditions" Text="<%$ Resources:Resource, TermsNConditions %>"></asp:ListItem>
                            <asp:ListItem Value="WarehouseSale" Text="<%$ Resources:Resource, WarehouseSale %>"></asp:ListItem>
                            <asp:ListItem Value="eBayStoreHeader" Text="<%$ Resources:Resource, eBayStoreHeader %>"></asp:ListItem>
                            <asp:ListItem Value="eBayStoreFooter" Text="<%$ Resources:Resource, eBayStoreFooter %>"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr height="25">
                    <td class="tdAlign" width="18%">
                    </td>
                    <td width="1%">
                    </td>
                    <td align="left" width="81%">
                        <FTB:FreeTextBox ID="txtWebPage" runat="server" Width="540px" AutoConfigure="Alternate"
                            ToolbarBackgroundImage="false" ToolbarBackColor="#D2D2D5" GutterBackColor="#D2D2D5"
                            BackColor="#D2D2D5" Height="150">
                        </FTB:FreeTextBox>
                    </td>
                </tr>
                <tr>
                    <td height="5">
                    </td>
                </tr>
                <tr height="25">
                    <td class="tdAlign">
                    </td>
                    <td>
                    </td>
                    <td align="left">
                        <table width="100%">
                            <tr>
                                <td width="25%">
                                    <div class="buttonwrapper">
                                        <a id="imgCmdPreview" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                            style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssPreview%></span></a></div>
                                </td>
                                <td width="25%">
                                    <div class="buttonwrapper">
                                        <a id="imgCmdSave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                            style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssSubmit%></span></a></div>
                                </td>
                                <td width="25%" align="left">
                                    <div class="buttonwrapper">
                                        <a id="imgCmdReset" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                            <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                <%=Resources.Resource.cmdCssReset%></span></a></div>
                                </td>
                                <td width="25%">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Content>

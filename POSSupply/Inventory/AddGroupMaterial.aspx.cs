﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Inventory_AddGroupMaterial : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MessageState.SetGlobalMessage(MessageType.Success, "");
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                ProductMaterial objProductMaterial = new ProductMaterial();

                ddlUsers.DataSource = objProductMaterial.GetMaterialList(null, Globals.CurrentAppLanguageCode);
                ddlUsers.DataTextField = "MaterialName";
                ddlUsers.DataValueField = "MaterialID";
                ddlUsers.DataBind();

                ProductMaterialGroup objMaterialGroup = new ProductMaterialGroup();

                var lst = objMaterialGroup.GetGroupMaterial(dbHelp, BusinessUtility.GetInt(this.StyleID), Globals.CurrentAppLanguageCode);
                foreach (var item in lst)
                {
                    ListItem li = ddlUsers.Items.FindByValue(item.MaterialID.ToString());
                    if (li != null)
                    {
                        li.Selected = true;
                    }
                }

                string newMaterialID = BusinessUtility.GetString(Request.QueryString["MaterialID"]);
                if (newMaterialID != "")
                {
                    string[] sMaterialID = newMaterialID.Split(',');

                    foreach (string MaterialID in sMaterialID)
                    {
                        ListItem li = ddlUsers.Items.FindByValue(MaterialID);
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }
                }

                ddlUsers.Focus();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    private string StyleID
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["StyleID"]);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {

            ProductMaterialGroup al = new ProductMaterialGroup();
            var lst = new List<int>();
            int id = 0;
            foreach (ListItem item in ddlUsers.Items)
            {
                id = 0;
                if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lst.Contains(id))
                {
                    lst.Add(id);

                }
            }

            if (al.AddMaterialInGroup(BusinessUtility.GetInt(this.StyleID), lst.ToArray()) == true)
            {
                //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.MsgCollectionCreated );
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.ReloadStyle();"), true);
            }
            else
            {
                //MessageState.SetGlobalMessage(MessageType.Failure, "Collection Creation failure");
                Globals.RegisterReloadParentScript(this);
                Globals.RegisterCloseDialogScript(this);
            }
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<ProductMaterial> GetMaterialList()
    {
        ProductMaterial objProductMaterial = new ProductMaterial();
        var t = objProductMaterial.GetMaterialList(null, Globals.CurrentAppLanguageCode);
        return t;
    }
}
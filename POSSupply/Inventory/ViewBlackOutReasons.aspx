﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ViewBlackOutReasons.aspx.cs" Inherits="Inventory_ViewBlackOutReasons" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <%= Resources.Resource.lblImport%>:
            <%= Resources.Resource.lblCustomerUser%></b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <input type="button" value="Add New Reason" onclick="$('#add_ctl00_ctl00_cphFullWidth_cphMaster_jgdvRegister').trigger('click');"/>
        <%--<asp:Button ID="btnRegister" runat="server" Text="Add New Reason" OnClientClick="addRow(); return false;" />--%>        
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif"
            CssClass="csv_export" />
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div id="grid_wrapper" style="width: 100%;">
        <trirand:JQGrid runat="server" ID="jgdvRegister" DataSourceID="sqldsRegister" Height="300px"
            AutoWidth="True" OnCellBinding="jgdvRegister_CellBinding" 
            OnDataRequesting="jgdvRegister_DataRequesting" 
            onrowadding="jgdvRegister_RowAdding" 
            onrowediting="jgdvRegister_RowEditing" 
            onrowdeleting="jgdvRegister_RowDeleting">
            <Columns>
                <trirand:JQGridColumn DataField="ID" HeaderText="ID" Editable="false" PrimaryKey="True"
                    Width="30" TextAlign="Center" />
                <trirand:JQGridColumn DataField="ReasonEn" HeaderText="Reason (EN)" Editable="true" >
                    <EditClientSideValidators>
                        <trirand:RequiredValidator />
                    </EditClientSideValidators>
                </trirand:JQGridColumn>
                <trirand:JQGridColumn DataField="ReasonFr" HeaderText="Reason (FR)"  Editable="true">
                    <EditClientSideValidators>
                        <trirand:RequiredValidator />
                    </EditClientSideValidators>
                </trirand:JQGridColumn>
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="ID"
                    Sortable="false" TextAlign="Center" Width="50" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="ID"
                    Sortable="false" TextAlign="Center" Width="50" />
            </Columns>
            <EditDialogSettings CloseAfterEditing="True" Modal="True" />
            <AddDialogSettings CloseAfterAdding="True" Modal="True" />
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="true" ShowRefreshButton="True" ShowAddButton="True"
                ShowDeleteButton="True" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" />
        </trirand:JQGrid>
        <asp:SqlDataSource ID="sqldsRegister" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>                
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" Runat="Server">
    <div>
        <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        Search form</div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch" CssClass="filter-key-txt"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="Search" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%= Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=jgdvRegister.ClientID %>";
        var searchPanelID = "<%=SearchPanel.ClientID %>";


        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function setSelectedRow(key) {
            var grid = $('#' + gridID);
            grid.setSelection(key);
        }
    </script>

</asp:Content>


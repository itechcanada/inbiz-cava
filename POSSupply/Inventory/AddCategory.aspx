<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false" ValidateRequest="false"
    CodeFile="AddCategory.aspx.vb" Inherits="Inventory_AddCategory" %>

<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>

<%@ Register src="../Controls/CommonSearch/CommonKeyword.ascx" tagname="CommonKeyword" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>
    
    <uc1:CommonKeyword ID="CommonKeyword1" runat="server" />
    
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblHeading"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td style="width: 150px;">
                    
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr height="40" runat="server" id="trClickHere" visible="false">
                <td align="center" colspan="3">
                    <asp:Label runat="server" ID="lblSuccess" Style="color: Green;" Font-Bold="true"></asp:Label>
                    <asp:LinkButton runat="server" ID="lnkClickMore" Text="<%$ Resources:Resource, AddMoreCategory%>"></asp:LinkButton>
                </td>
            </tr>
            <tr id="trAllControl" runat="server">
                <td align="left">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                                <asp:Label runat="server" ID="lblErrorMsg" Style="color: Red;" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="tdAlign" width="28%">
                                <asp:Label ID="lblSelectLang" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, SelectLanguage%>"></asp:Label>
                            </td>
                            <td align="left" width="72%">
                                <asp:RadioButtonList ID="rblLanguage" runat="server" RepeatDirection="Horizontal"
                                    AutoPostBack="true">
                                    <asp:ListItem Text="<%$ Resources:Resource, English%>" Value="en" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:Resource, French%>" Value="fr"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:Resource, Spanish%>" Value="sp"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="tdAlign" width="28%">
                                <asp:Label ID="lblCatName" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, CategoryName%>"></asp:Label>
                            </td>
                            <td align="left" width="72%">
                                <asp:TextBox ID="txtCatName" runat="server" MaxLength="100" Width="330px"></asp:TextBox><span
                                    class="style1"> *</span>
                                <asp:RequiredFieldValidator ID="reqvalCatName" ControlToValidate="txtCatName" runat="server"
                                    ErrorMessage="<%$ Resources:Resource, msgPlzEntrCatName%>" Display="None" />
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="tdAlign" width="28%">
                                <asp:Label ID="lbleBayCatgNo" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, eBayCatgNo%>"></asp:Label>
                            </td>
                            <td align="left" width="72%">
                                <asp:TextBox ID="txteBayCatgNo" runat="server" MaxLength="20" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr height="30" runat="server" id="trIsActive">
                            <td class="tdAlign" width="23%">
                                <asp:Label ID="lblIsActive" CssClass="lblBold" runat="server" Text="<%$ Resources:Resource, IsCategoryActive%>"></asp:Label>
                            </td>
                            <td align="left" width="77%">
                                <asp:RadioButton ID="rbYes" GroupName="rbIsActive" Checked="true" runat="server"
                                    Text="Yes" />&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:RadioButton ID="rbNo" GroupName="rbIsActive" runat="server" Text="No" TextAlign="Right" />&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="tdAlign">
                                <asp:Label ID="lblWebSeq" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, WebSequence%>"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="dlWebSeq" Width="100px" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="tdAlign">
                                <asp:Label ID="lblSelCat" Text="<%$ Resources:Resource, lblInvCategory%>" runat="server"
                                    CssClass="lblBold" />
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rblstCategory" runat="server" RepeatDirection="Horizontal"
                                    AutoPostBack="true">
                                    <asp:ListItem Text="<%$ Resources:Resource, lblInvDefault%>" Value="0" Selected="True">
                                    </asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:Resource, lblInvFreeText%>" Value="1"></asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:CustomValidator ID="custvalIsDefault" runat="server" ErrorMessage="<%$ Resources:Resource, msgPlzEntrFreeTxt%>"
                                    ClientValidationFunction="funIsDefault" Display="None" Enabled="true"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr height="30" id="trFree" runat="server">
                            <td class="tdAlign" width="28%">
                                <asp:Label ID="lblFreeText" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblInvFreeText%>"></asp:Label>
                            </td>
                            <td align="left" width="72%">
                                <FTB:FreeTextBox ID="txtFreeText" runat="server" Height="150" BackColor="#D2D2D5"
                                    GutterBackColor="#D2D2D5" ToolbarBackColor="#D2D2D5" ToolbarBackgroundImage="false"
                                    Width="540px" AutoConfigure="Alternate">
                                </FTB:FreeTextBox>
                                <asp:CustomValidator ID="custvalFreeText" runat="server" ErrorMessage="<%$ Resources:Resource, PlzEntMax1500Words %>"
                                    ClientValidationFunction="funWC" Display="None" Enabled="true"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr height="30" id="trCatImagePath" runat="server">
                            <td class="tdAlign">
                                <asp:Label ID="lblCatImagePath" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblImage%>"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:FileUpload ID="fileCatImagePath" runat="server" />
                                <asp:CustomValidator ID="custCatImage" runat="server" ErrorMessage="<%$ Resources:Resource, msgPleaseUploadCategoryImage %>"
                                    ClientValidationFunction="funCategoryImage" Display="None" Enabled="false"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr height="30" id="trCatImageUrl" runat="server" visible="false">
                            <td class="tdAlign">
                                <asp:Label ID="lblCatImageUrl" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblImage%>"></asp:Label>
                            </td>
                            <td align="left">
                                <a runat="server" id="aTag" target="_blank">
                                    <img runat="server" id="imgCat" /></a> &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="imgCatImageUrl" runat="server" ImageUrl="~/Images/delete_icon.png" />
                            </td>
                        </tr>
                        <tr height="30" id="trCatOffImagePath" runat="server" style="display:none;">
                            <td class="tdAlign">
                                <asp:Label ID="Label2" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblImage%>"></asp:Label>(off)
                            </td>
                            <td align="left">
                                <asp:FileUpload ID="fuOffCatImage" runat="server" />
                                <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="<%$ Resources:Resource, msgPleaseUploadCategoryImage %>"
                                    ClientValidationFunction="funCategoryImage" Display="None" Enabled="false"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr height="30" id="trCatOffImageUrl" runat="server" visible="false" style="display:none;">
                            <td class="tdAlign">
                                <asp:Label ID="Label1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblImage%>"></asp:Label>(off)
                            </td>
                            <td align="left">
                                <a runat="server" id="aOffImage" target="_blank">
                                    <img runat="server" id="imgCatOff" /></a> &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="ibtnOffImage" runat="server" ImageUrl="~/Images/delete_icon.png" />
                            </td>
                        </tr>
                        <tr height="30" id="trLandingPageBanner" runat="server" style="display:none;">
                            <td class="tdAlign">
                                <asp:Label ID="Label3" runat="server" CssClass="lblBold" Text="Landing page banner"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:FileUpload ID="fuBanner" runat="server" />
                                <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="<%$ Resources:Resource, msgPleaseUploadCategoryImage %>"
                                    ClientValidationFunction="funCategoryImage" Display="None" Enabled="false"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr height="30" id="trLandingPageBannerUrl" runat="server" visible="false" style="display:none;">
                            <td class="tdAlign">
                                <asp:Label ID="Label4" runat="server" CssClass="lblBold" Text="Landing page banner"></asp:Label>
                            </td>
                            <td align="left">
                                <a runat="server" id="aBanner" target="_blank">
                                    <img runat="server" id="imgBanner" /></a> &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="ibtnBanner" runat="server" ImageUrl="~/Images/delete_icon.png" />
                            </td>
                        </tr>
                        <tr height="30">
                            <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                                <table width="100%">
                                    <tr>
                                        <td width="30%">
                                        </td>
                                        <td width="20%">
                                            <div class="buttonwrapper">
                                                <a id="imgCmdSave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                                    style="min-width: 120px; text-align: center;">
                                                    <%=Resources.Resource.cmdCssSubmit%></span></a></div>
                                        </td>
                                        <td width="50%" align="left">
                                            <div class="buttonwrapper">
                                                <a id="imgCmdReset" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                                    <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                        <%=Resources.Resource.cmdCssReset%></span></a></div>
                                        </td>
                                    </tr>
                                </table>
                                <asp:HiddenField runat="server" ID="hdnActive" />
                                <asp:HiddenField runat="server" ID="hdnOldvalue" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="sumvalCategory" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
    <br />
    <br />
    <script language="javascript">

        function funCategoryImage(source, arguments) {
            if (document.getElementById('<%=fileCatImagePath.ClientID%>').value.length == 0)
                arguments.IsValid = false;
            else
                arguments.IsValid = true;
        }
        function funIsDefault(source, arguments) {
            if (document.getElementById('<%=rblstCategory.ClientID%>_1').checked == true &&
    document.getElementById('<%=txtFreeText.ClientID%>').value == "") {
                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;
            }
        }
        function funWC(source, arguments) {
            var nData = wordCount(document.getElementById('<%=txtFreeText.ClientID%>').value);
            if (nData >= 1 && nData <= 1500) {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }
        function wordCount(str) {
            str = prepStr(str);
            var tempArray = str.split(' ').sort();
            return tempArray.length - 1;
        }

        // Define a function to format strings for easier manipulation
        function prepStr(str) {
            str = str.toLowerCase();
            str = str.replace(/<[^>]*>/g, "");
            str = str.replace(/['"-]/g, "");
            str = str.replace(/\W/g, " ");
            str = str.replace(/\s+/g, " ");

            return str;
        }
    </script>
</asp:Content>

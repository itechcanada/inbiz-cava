﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdApplyBlackOuts.aspx.cs" Inherits="Inventory_mdApplyBlackOuts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div style="padding: 5px; height: 360px; overflow: auto;">
        <asp:Panel ID="pnlDateRange" GroupingText="By Date Range" runat="server" DefaultButton="btnApply">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <asp:Label ID="lblStartDate" Text="<%$Resources:Resource, lblStartDate %>" runat="server" />                        
                    </td>
                    <td>
                        <asp:Label ID="Label1" Text="<%$Resources:Resource, lblEndDate %>" runat="server" />                        
                    </td>
                    <td>
                         <asp:Label ID="Label2" Text="<%$Resources:Resource, lblReason %>" runat="server" />                                   
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtStartDate" CssClass="datepicker" runat="server" Width="80px" />
                        <asp:RequiredFieldValidator ID="rfvStartDate" ErrorMessage="*" ControlToValidate="txtStartDate"
                            runat="server" ValidationGroup="grp1" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtEndDate" CssClass="datepicker" runat="server" Width="80px" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ControlToValidate="txtEndDate"
                            runat="server" ValidationGroup="grp1" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlReason" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="*" ControlToValidate="ddlReason"
                            runat="server" ValidationGroup="grp1" />
                    </td>
                    <td>
                        <asp:Button CssClass="dicount_confirm" ID="btnApply" Text="<%$Resources:Resource, lblApply %>" runat="server"
                            OnClick="btnApply_Click" ValidationGroup="grp1" />
                        <asp:Button ID="btnAddNewReaso" Text="<%$Resources:Resource, lblAddNewReason %>" runat="server" CssClass="p_opner" />
                        <asp:Button ID="btnReset" Text="<%$Resources:Resource, btnReset %>" runat="server" CausesValidation="False" OnClick="btnReset_Click"
                            OnClientClick="resetPanel(); return false;" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnID" runat="server" />
        </asp:Panel>
        <div id="grid_wrapper">
            <trirand:JQGrid runat="server" ID="grdDiscounts" Height="200px"
                Width="100%" AutoWidth="true" PagerSettings-PageSize="50" OnDataRequesting="grdDiscounts_DataRequesting"
                OnCellBinding="grdDiscounts_CellBinding">
                <Columns>
                    <trirand:JQGridColumn DataField="GridPrimaryKey" PrimaryKey="true" Visible="false" />                    
                    <trirand:JQGridColumn DataField="StartDate" HeaderText="<%$Resources:Resource, lblStartDate%>" DataFormatString="{0:MMM dd}"
                        Editable="false" Sortable="false" TextAlign="Center" />
                    <trirand:JQGridColumn DataField="EndDate" HeaderText="<%$Resources:Resource, lblEndDate%>" DataFormatString="{0:MMM dd}"
                        Editable="false" Sortable="false" TextAlign="Center" />
                    <trirand:JQGridColumn DataField="ReasonDesc" HeaderText="<%$Resources:Resource, lblReason%>" Editable="false"
                        Sortable="false" TextAlign="Center" />
                    <trirand:JQGridColumn DataField="RoomID" HeaderText="<%$Resources:Resource, Edit%>" Editable="false" Sortable="false"
                        TextAlign="Center" />  
                    <trirand:JQGridColumn DataField="RoomID" HeaderText="<%$Resources:Resource, Delete%>" Editable="false" Sortable="false"
                        TextAlign="Center" />                    
                </Columns>
                <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                    ShowDeleteButton="false" ShowSearchButton="false" />
                <SortSettings InitialSortColumn="" />
                <AppearanceSettings AlternateRowBackground="True" />
                <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="gridLoadComplete"
                    RowSelect="rowSelect" />
            </trirand:JQGrid>
            <asp:SqlDataSource ID="sdsDiscount" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
        </div>
    </div>
    <div id="divAddNewReason" title="<%=Resources.Resource.lblAddNewReason%>" class="ui-jqdialog-content">
        <asp:Panel ID="pnlAddReason" runat="server" DefaultButton="btnSave">
            <div style="width: 296px; overflow: auto; position: relative; height: 66px;">
                <table class="EditTable" border="0" cellpadding="0" cellspacing="0">
                    <tr style="display: none;" id="FormError">
                        <td colspan="2" class="ui-state-error">
                        </td>
                    </tr>
                    <tr class="tinfo" style="display: none;">
                        <td colspan="2" class="topinfo">
                        </td>
                    </tr>
                    <tr class="FormData" id="tr_ReasonEn">
                        <td class="CaptionTD">
                           <asp:Label ID="Label3" Text="<%$Resources:Resource, lblReason %>" runat="server" />
                             (EN)
                        </td>
                        <td class="DataTD">
                            &nbsp;<asp:TextBox ID="txtReasonEn" runat="server" CssClass="FormElement ui-widget-content ui-corner-all" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="*" ControlToValidate="txtReasonEn"
                                runat="server" ValidationGroup="grp2" SetFocusOnError="true" />
                        </td>
                    </tr>
                    <tr class="FormData" id="tr_ReasonFr">
                        <td class="CaptionTD">
                            <asp:Label ID="Label4" Text="<%$Resources:Resource, lblReason %>" runat="server" /> (FR)
                        </td>
                        <td class="DataTD">
                            &nbsp;<asp:TextBox ID="txtReasonFr" runat="server" CssClass="FormElement ui-widget-content ui-corner-all" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="*" ControlToValidate="txtReasonFr"
                                runat="server" ValidationGroup="grp2" SetFocusOnError="true" />
                        </td>
                    </tr>                    
                </table>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" class="EditTable">
                <tbody>
                    <tr>
                        <td colspan="2">
                            <hr style="margin: 1px" class="ui-widget-content" />
                        </td>
                    </tr>
                    <tr id="Act_Buttons">
                        <td class="EditButton">
                            <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" ValidationGroup="grp2" OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" Text="<%$Resources:Resource, btnCancel%>" CausesValidation="false" runat="server"
                                OnClientClick="$('#divAddNewReason').dialog('close'); return false;" />
                        </td>
                    </tr>
                    <tr class="binfo" style="display: none">
                        <td colspan="2" class="bottominfo">
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
    </div>
    <asp:HiddenField ID="editPopAutoOpen" runat="server" Value="0" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdDiscounts.ClientID %>";
        $grid = $("#<%=grdDiscounts.ClientID %>");

        //Function To Resize the grid
        function resize_the_grid() {
            $grid.fluidGrid({ example: '#grid_wrapper', offset: -2 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $grid.onJqGridPaging();
        }


        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $grid.onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);


        function reloadGrid(event, ui) {
            $grid.trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function gridLoadComplete(data) {
            $(window).trigger("resize");
        }

        function resetPanel() {
            $grid.jqGrid('resetSelection');
            $("#<%=btnReset.ClientID%>").hide();
            $("#<%=hdnID.ClientID%>").val("");
            $pnl1 = $("#<%=pnlDateRange.ClientID%>");
            $("input:text", $pnl1).each(function () {
                $(this).attr("disabled", false).val("");
            });
            $("input:submit", $pnl1).each(function () {
                $(this).button({ disabled: false });
            });
        }

        function rowSelect(id) {
            setSelectedItemData(id);
        }

        function setSelectedItemData(id) {
            var postData = {};
            postData.itemID = id;
            $.ajax({
                type: "POST",
                url: "ajax.aspx/GetProductBlackOutByRoom",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(postData),
                success: function (data) {
                    //alert(parseInt(data.d.DiscountType));
                    $("#<%=hdnID.ClientID%>").val(data.d.GridPrimaryKey);
                    $("#<%=txtStartDate.ClientID%>").datepicker("setDate", new Date(data.d.StartDate.match(/\d+/)[0] * 1));
                    $("#<%=txtEndDate.ClientID%>").datepicker("setDate", new Date(data.d.EndDate.match(/\d+/)[0] * 1));
                    $("#<%=ddlReason.ClientID%>").val(data.d.Reason);
                    $("#<%=ddlReason.ClientID%>").trigger("liszt:updated")
                    $("#<%=btnReset.ClientID%>").show();
                },
                error: function (request, status, errorThrown) {
                    alert(status);
                }
            });

            return false;
        }

        function deleteItem(id) {
            if (confirm("<%=Resources.Resource.msgDoYouWantToDelete%>")) {
                var postData = {};
                postData.rowKey = id;
                postData.roomIds = <%=GetJsonArrayRoomID()%>;
                $.ajax({
                    type: "POST",
                    url: "ajax.aspx/DeleteBlackOutsByRoom",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(postData),
                    success: function (data) {
                        reloadGrid();
                    },
                    error: function (request, status, errorThrown) {
                        alert(status);
                    }
                });
            }
            return false;
        }

        //Add new blackout reason popup
        $("#divAddNewReason").dialog({
            modal: true,
            autoOpen: $("#<%=editPopAutoOpen.ClientID%>").val() == "1",
            resizable: true,
            width: 320,
            height: 150
        }).parent().appendTo(jQuery("form:first"));
        $(".p_opner").click(function () {
            $this = $(this);
            $("#divAddNewReason").dialog("open");
            return false;
        });       
        
</script>
</asp:Content>


Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Partial Class Inventory_AssociateVendor
    Inherits BasePage
    Dim objAssVendor As New clsPrdAssociateVendor
    Public objPrdDesc As New clsPrdDescriptions
    Protected Sub Inventory_AssociateVendor_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        lblHeading.Text = objPrdDesc.funProductName & lblPrdAssVendor
        If Not Page.IsPostBack Then
            objAssVendor.subGetVendor(dlVendor)
            subFillGrid()
        End If
        If Session("UserModules").ToString.Contains("INROL") = True And (Session("UserModules").ToString.Contains("STK") = False Or Session("UserModules").ToString.Contains("ADM") = False) Then
            imgCmdReset.Visible = False
            imgCmdSave.Visible = False
        End If
        dlVendor.Focus()
    End Sub
    Public Sub subFillGrid()
        sqlsdAssVendor.SelectCommand = objAssVendor.subFillAssVendor()
    End Sub
    Protected Sub grdvAssVendor_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdvAssVendor.PageIndexChanging
        subFillGrid()
    End Sub
    Protected Sub grdvAssVendor_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdvAssVendor.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
            "confirm('" & msgPrdAssociateVendorDelete & "')")
        End If
        If Session("UserModules").ToString.Contains("INROL") = True And (Session("UserModules").ToString.Contains("STK") = False Or Session("UserModules").ToString.Contains("ADM") = False) Then
            grdvAssVendor.Columns(1).Visible = False
        End If
    End Sub
    Protected Sub grdvAssVendor_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdvAssVendor.RowDeleting
        Dim strAssVenID As String = grdvAssVendor.DataKeys(e.RowIndex).Value.ToString()
        lblMsg.Text = msgPrdAssociateVendorDeleted
        sqlsdAssVendor.DeleteCommand = objAssVendor.subAssvendorDelete(strAssVenID) 'Delete Associate Product
        subFillGrid()
    End Sub
    Protected Sub grdvAssVendor_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdvAssVendor.Sorting
        subFillGrid()
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    Protected Sub imgCmdReset_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdReset.ServerClick
        dlVendor.SelectedValue = 0
    End Sub
    Protected Sub imgCmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdSave.ServerClick
        objAssVendor.PrdID = Request.QueryString("prdID")
        objAssVendor.PrdVendorID = dlVendor.SelectedValue
        objAssVendor.PrdCostPrice = txtCostPrice.Text
        If objAssVendor.funDuplicateAssVendor Then
            lblMsg.Text = msgPrdAssociateVendorExist
            Exit Sub
        Else
            If objAssVendor.funInsertPrdAssociateVendor() Then
                lblMsg.Text = msgPrdAssociateVendorSaved
            End If
        End If
        subFillGrid()
    End Sub
End Class

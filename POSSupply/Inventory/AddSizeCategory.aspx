﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="AddSizeCategory.aspx.cs" Inherits="Inventory_AddSizeCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="dialogAssignUser" style="padding: 5px;">
        <h3>
            <asp:Label ID="Label1" Text="<%$Resources:Resource, lblSizeCategory%>" runat="server" />
        </h3>
        <div id="dvCategory" style="padding-left: 50px;">
        </div>
    </div>
    <div class="div_command">
        <input type="button" id="btnAddSizeCategory" value="<%=Resources.Resource.btnSave%>"
            onclick='AddSizeCategory();' />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
            OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            loadSize();
        });

        $.extend({
            getParamValue: function (paramName) {
                parName = paramName.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
                var pattern = '[\\?&]' + paramName + '=([^&#]*)';
                var regex = new RegExp(pattern);
                var matches = regex.exec(window.location.href);
                if (matches == null) return '';
                else return decodeURIComponent(matches[1].replace(/\+/g, ' '));
            }
        });

        function AddSizeCategory() {
            var n = jQuery(".checkboxClass:checked").length;
            var sSelectedValue = "";
            if (n > 0) {
                jQuery(".checkboxClass:checked").each(function () {
                    if (sSelectedValue != "") {
                        sSelectedValue = sSelectedValue + "~" + $(this).val();
                    }
                    else {
                        sSelectedValue = $(this).val();
                    }

                });
            }

            if (sSelectedValue != "") {
                var datatoPost = {};
                datatoPost.isAjaxCall = 1;
                datatoPost.callBack = "Save";
                datatoPost.styleID = $.getParamValue('StyleID');
                datatoPost.CategoryID = sSelectedValue;
                $.post("AddSizeCategory.aspx", datatoPost, function (data) {
                    if (data == "ok") {
                        parent.ReloadStyle(); jQuery.FrameDialog.closeDialog();
                    }
                });
            }
        }

        function loadSize() {
            $.ajax({
                type: "POST",
                url: "AddSizeCategory.aspx/GetCategorySizeList",
                dataType: "json",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var addressList = data.d;
                    var sSelGroup = '';
                    var sHTML = "";
                    var sTDHtml = "";
                    for (var i = 0; i < addressList.length; i++) {
                        var sGroupName = addressList[i].CategoryName;
                        var sSize = addressList[i].Size;

                        if (sSelGroup == "") {
                            sSelGroup = sGroupName;
                            sHTML = sHTML + "<tr><td><table><tr><td style='text-align: left;' ><input class='checkboxClass' id='chk" + i + "' type='checkbox'  name='chk" + i + "' value='" + sGroupName + "' /></td><td >   " + sGroupName + "</td></tr></table> </td></tr> "; //checked='checked'
                            sTDHtml = sTDHtml + "<td >&nbsp;</td>  <td> " + sSize + " </td> ";
                        }
                        else if (sSelGroup == sGroupName) {
                            sTDHtml = sTDHtml + "<td ></td><td> " + sSize + " </td>";
                        }
                        else if (sSelGroup != sGroupName) {
                            sSelGroup = sGroupName;
                            sHTML = sHTML + "<tr> " + sTDHtml + " </tr>";
                            sHTML = sHTML + "<tr><td style='text-align: left;'><input class='checkboxClass' id='chk" + i + "' type='checkbox'  name='chk" + i + "' value='" + sGroupName + "'/> " + sGroupName + " </td></tr> ";
                            sTDHtml = "";
                            sTDHtml = sTDHtml + "<td >&nbsp;</td><td> " + sSize + " </td>";
                        }
                    }
                    sHTML = '<table>' + sHTML + sTDHtml + "</table>";
                    $("#dvCategory").html(sHTML);

                    loadSavedCatgSize();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });

        }

        function loadSavedCatgSize() {
            $.ajax({
                type: "POST",
                url: "AddSizeCategory.aspx/GetSavedCategorySize",
                dataType: "json",
                data: "{sMasterID:" + $.getParamValue('StyleID') + "}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var addressList = data.d;
                    for (var i = 0; i < addressList.length; i++) {
                        jQuery(".checkboxClass").each(function () {

                            if ($(this).val() == addressList[i].CategoryName) {
                                $(this).attr("checked", 'checked');
                            }
                        });
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });

        }

        $(".checkboxClass:checked").live('change', function (e) {
            var sSelectedValue = $(this).val();

            jQuery(".checkboxClass:checked").each(function () {

                if ($(this).val() != sSelectedValue) {
                    $(this).removeAttr("checked");
                }
            });
        });




    </script>
</asp:Content>

Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Net
Imports System.IO
Partial Class eComManageWebPages
    Inherits BasePage
    Protected strHtml As String = ""
    Protected strFilePath As String = Server.MapPath("~/Upload/WebPages")
    'Page Load Event
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Session("msg") <> "" Then
            lblMsg.Text = Session("msg").ToString
            Session.Remove("msg")
        End If
        If Not IsPostBack Then
            rblWebPage.SelectedIndex = 0
            txtWebPage.Text = funGetCustomText()
        End If
    End Sub
    'To Change Text in rich text box when diffrent web pages selected.
    Protected Sub rblWebPage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblWebPage.SelectedIndexChanged
        lblMsg.Text = ""
        txtWebPage.Text = funGetCustomText()

        imgCmdPreview.Visible = True
        If rblWebPage.SelectedValue = "eBayStoreHeader" Or rblWebPage.SelectedValue = "eBayStoreFooter" Then
            imgCmdPreview.Visible = False
        End If
    End Sub
    'Reads content from file.
    Protected Function funGetCustomText() As String
        Dim str As String = ""
        Dim fileName As String = getFileName()
        Dim objStreamReader As StreamReader
        If File.Exists(strFilePath & "/" & fileName) = True Then
            objStreamReader = File.OpenText(strFilePath & "/" & fileName)
            str = objStreamReader.ReadToEnd()
            objStreamReader.Close()
        End If
        Return str
    End Function
    ' Gets fileName for reading by selected language
    Protected Function getFileName() As String
        If rblWebPage.SelectedValue = "eBayStoreHeader" Or rblWebPage.SelectedValue = "eBayStoreFooter" Then
            Return rblWebPage.SelectedValue + ".html"
        End If

        Dim fileName As String = ""
        If rblLanguage.SelectedValue = "en" Then
            fileName = rblWebPage.SelectedValue
        ElseIf rblLanguage.SelectedValue = "fr" Then
            fileName = rblWebPage.SelectedValue + "_FR"
        ElseIf rblLanguage.SelectedValue = "sp" Then
            fileName = rblWebPage.SelectedValue + "_SP"
        End If
        fileName += ".html"
        Return fileName
    End Function
    ' Selected Language change Event
    Protected Sub rblLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblLanguage.SelectedIndexChanged
        lblMsg.Text = ""
        Session.Remove("msg")
        txtWebPage.Text = funGetCustomText()
    End Sub
    'Priview Web Page of selected radio button
    Protected Sub imgCmdPreview_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdPreview.ServerClick
        lblMsg.Text = ""
        Dim stream_writer As New StreamWriter(strFilePath & "/Temp.html")
        stream_writer.Write(txtWebPage.Text)
        stream_writer.Close()

        Dim streComURL As String = ""
        streComURL = ConfigurationManager.AppSettings("eComWebPageURL").ToString()

        Dim strAboutUsPage = streComURL & "AboutUs.aspx"
        Dim strContactUsPage = streComURL & "ContactUs.aspx"
        Dim strHomePage = streComURL & "Category.aspx"
        Dim strPrivacyStatement = streComURL & "PrivacyPolicy.aspx"
        Dim strTermsConditions = streComURL & "TermsNConditions.aspx"
        Dim strWarehouseSale = streComURL & "Warehouse.aspx"

        Dim selectedPage As String = rblWebPage.SelectedValue

        If selectedPage = "AboutUs" Then
            strHtml = strAboutUsPage
        ElseIf selectedPage = "ContactUs" Then
            strHtml = strContactUsPage
        ElseIf selectedPage = "PrivacyStatement" Then
            strHtml = strPrivacyStatement
        ElseIf selectedPage = "TermsNConditions" Then
            strHtml = strTermsConditions
        ElseIf selectedPage = "WarehouseSale" Then
            strHtml = strWarehouseSale + "?cID=1&scID=1&pID=1&Prw=T"
        ElseIf selectedPage = "HomePage" Then
            strHtml = strHomePage
        End If
        If Not selectedPage = "WarehouseSale" Then
            strHtml = strHtml + "?Prw=T"
        End If
        Response.Write("<script>window.open('" & strHtml & "'); 'window.focus'; </script>")
    End Sub
    'Reset the all changes before saving.
    Protected Sub imgCmdReset_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdReset.ServerClick
        lblMsg.Text = ""
        txtWebPage.Text = funGetCustomText()
    End Sub
    'Submit button click event, data save in upload/Webpage directory as file form.
    Protected Sub imgCmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdSave.ServerClick
        Dim fileName As String = getFileName()
        Dim stream_writer As New StreamWriter(strFilePath & "/" & fileName)
        stream_writer.Write(txtWebPage.Text)
        stream_writer.Close()
        Session.Add("msg", msgeComWebPageUpdateSuccessfully)
        lblMsg.Text = Session("msg").ToString
    End Sub
End Class

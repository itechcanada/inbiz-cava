﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AmenityEdit.aspx.cs" Inherits="Admin_AmenityEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <asp:Panel ID="pnlMain" runat="server" DefaultButton="btnSave">
        <div id="contentBottom" style="padding: 5px; height: 290px; overflow: auto;">
            <ul class="form">
                <li>
                    <div class="lbl">
                        Amenity Description (En)
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtDescriptionEn" runat="server" />
                        <asp:RequiredFieldValidator ID="rfvDesc" ErrorMessage="*" ControlToValidate="txtDescriptionEn"
                            SetFocusOnError="true" runat="server" />
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        Amenity Description (Fr)
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtDescriptionFr" runat="server" />
                        <asp:RequiredFieldValidator ID="rfvDescFr" ErrorMessage="*" ControlToValidate="txtDescriptionFr"
                            SetFocusOnError="true" runat="server" />
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        Image Icon
                    </div>
                    <div class="input">
                        <asp:FileUpload ID="fuImage" runat="server" />
                        <asp:HiddenField ID="hdnImage" runat="server" />
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                    </div>
                    <div class="input">
                        <asp:Image ID="imgIcon" ImageUrl="" runat="server" Visible="false" />
                        <br />
                        <br />
                        <asp:ImageButton ID="ibtnDelete" CommandName="del" runat="server" ToolTip="Delete"
                            CausesValidation="false" ImageUrl="~/Images/delete_icon.png" OnClientClick="return confirm('Do you want to delete?')"
                            OnClick="ibtnDelete_Click" Visible="false" />
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
            </ul>
            <div style="clear: both;">
            </div>
        </div>
        <div class="div-dialog-command" style="border-top: 1px solid #ccc; padding: 5px;">
            <asp:Button Text="<%$Resources:Resource, btnSave %>" ID="btnSave" runat="server"
                OnClick="btnSave_Click" />
            <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
                CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


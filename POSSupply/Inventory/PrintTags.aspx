﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintTags.aspx.cs" Inherits="Inventory_PrintTags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>    
     <script src="../lib/scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-plugins/jquery-framedialog-1.1.2.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">    
    <asp:DataList ID="dlstTags" runat="server" BorderWidth="0px" CellPadding="5" RepeatColumns="3" RepeatDirection="Horizontal">
        <ItemStyle Height="95px" Width="240px" />
        <ItemTemplate>     
                   
            <font size="3" face='<%#ConfigurationManager.AppSettings["BarCodeFont"]%>'>*<%#Eval("Column1") %>*</font></br> 
            <font size="1.5" face="Arial"><%#Eval("Column2") %></font>
            <font size="2" face="Arial">&nbsp;<%#Eval("Column3") %> <b><%#Eval("Column4") %></b></font>            
            <%--<font size="3" face="IDAutomationHC39M">*90000123*</font></br> <font size="1.5" face="Arial">Bhagwat Geeta Book</font><font size="2" face="Arial">&nbsp;CDN <b>23.00</b></font>--%>
        </ItemTemplate>
    </asp:DataList>  
    
    <script language="javascript" type="text/javascript">
        var PrintCommandObject = null;

        function printPage() {
            if (PrintCommandObject) {
                try {
                    PrintCommandObject.ExecWB(6, 2);
                    PrintCommandObject.outerHTML = "";
                }
                catch (e) {
                    window.print();

                }
            }
            else {
                window.print();
            }
            var browserName = navigator.appName;
            if (navigator.appName != "Microsoft Internet Explorer") {
               window.close();
            }
            else {
               window.close();
            }
        }

        window.onload = function () {
            Minimize();
            if (navigator.appName == "Microsoft Internet Explorer") {
                // attach and initialize print command ActiveX object
                try {
                    var PrintCommand = '<object id="PrintCommandObject" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2" width="0" height="0"></object>';
                    document.body.insertAdjacentHTML('beforeEnd', PrintCommand);
                }
                catch (e) { }
            }
            setTimeout(printPage, 1);
        };


        function Minimize() {
            window.innerWidth = 10;
            window.innerHeight = 10;
            window.screenX = screen.width;
            window.screenY = screen.height;
            alwaysLowered = true;
        }

    </script>  
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Inventory_ProductImageEdit : BasePage
{
    ProductImages _prdImage = new ProductImages();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.FillForm();
        }
    }

    private void FillForm()
    {
        rbtnFoundation.Enabled = false;
        rbtnCharmOrBead.Enabled = false;
        switch (this.ProductImageType)
        {
            case ProductImages.PRODUCT_IMAGE_TYPE_FOUNDATION:
                pnlStep1.Visible = false;
                pnlStep2.Visible = true;
                pnlStep3.Visible = false;
                rbtnFoundation.Checked = true;
                rbtnCharmOrBead.Checked = false;
                break;
            case ProductImages.PRODUCT_IMAGE_TYPE_CHARM:
                pnlStep1.Visible = false;
                pnlStep2.Visible = true;
                pnlStep3.Visible = false;
                rbtnFoundation.Checked = false;
                rbtnCharmOrBead.Checked = true;
                break;
            default:
                pnlStep1.Visible = true;
                pnlStep2.Visible = false;
                pnlStep3.Visible = false;
                break;
        }

        if (this.ProductImageID > 0)
        {
            _prdImage.PopulatObject(this.ProductImageID);            

            imgProduct.ImageUrl = ImageUploadPath.PRODUCT_IMAGES + _prdImage.PrdSmallImagePath;
            imgToCrop.ImageUrl = ImageUploadPath.PRODUCT_IMAGES + _prdImage.PrdSmallImagePath;            
            //imgToCrop1.ImageUrl = ImageUploadPath.PRODUCT_IMAGES + _prdImage.PrdSmallImagePath;
            ddlImageType.SelectedValue = _prdImage.ProductImageType;
            txtCharmOffset.Text = _prdImage.CharmOffSet.ToString();
            txtCharmWidth.Text = _prdImage.CharmWidth.ToString();
            hdnRecData.Value = _prdImage.RectangleCoordinates;
            ltAreaCoordinates.Text = _prdImage.RectangleCoordinates;
            _prdImage.RectangleCoordinates = !string.IsNullOrEmpty(hdnRecData.Value) ? hdnRecData.Value : Newtonsoft.Json.JsonConvert.SerializeObject(new List<int>());
            ddlImageGroup.SelectedValue = _prdImage.ImageGroup;
            this.SetImagePosition(_prdImage.ImageGroup);
        }
    }

    private void SetImagePosition(string group) {
        double mainWidth = 634;
        double mainHeight = 177;
        double picWidth = -1;
        double picHeight = -1;
        double hOffset = 0;
        double vOffset = 0;
        switch (group)
        {
            case "bracelet":
                picHeight = 500;
                picWidth = 500;

                hOffset = Math.Round((mainWidth - picWidth) / 2);
                vOffset = Math.Round((mainHeight - picHeight) / 2);
                break;
            case "necklace":
                picHeight = 600;
                picWidth = 600;

                hOffset = Math.Round((mainWidth - picWidth) / 2);
                vOffset = Math.Round((mainHeight - picHeight) / 2);
                break;
            case "earring":
                picHeight = 150;
                picWidth = 150;

                hOffset = Math.Round((mainWidth - picWidth) / 2);
                vOffset = Math.Round((mainHeight - picHeight) / 2);
                break;
            case "charm":
            case "stopper":
            default:
                picHeight = 50;
                picWidth = 50;

                hOffset = Math.Round((mainWidth - picWidth) / 2);
                vOffset = Math.Round((mainHeight - picHeight) / 2);
                break;            
        }
        imgToCrop.Style[HtmlTextWriterStyle.MarginLeft] = hOffset + "px";
        imgToCrop.Style[HtmlTextWriterStyle.MarginTop] = vOffset + "px";
    }

    protected string RectangleCoordinatesScriptData {
        get {
            return !string.IsNullOrEmpty(hdnRecData.Value) ? hdnRecData.Value : Newtonsoft.Json.JsonConvert.SerializeObject(new List<int>());
        }
    }

    private bool ValidateCategoryImage() {
        bool check1 = false, check2 = false, check3 = false;
        check1 = fuPrdDisplayImage.HasFile && FileManager.IsValidImageFile(fuPrdDisplayImage.PostedFile.FileName);
        if (!check1)
        {
            return false;
        }
        check2 = !fuPrdZoomImage.HasFile || (fuPrdZoomImage.HasFile && FileManager.IsValidImageFile(fuPrdZoomImage.PostedFile.FileName));
        if (!check2)
        {
            return false;
        }
        check3 = !fu90DegImage.HasFile || (fu90DegImage.HasFile && FileManager.IsValidImageFile(fu90DegImage.PostedFile.FileName));
        return check1 && check2 && check3;
    }

    private string GetUploadedImageName(HttpPostedFile imgNormal) {
        string fileExt = System.IO.Path.GetExtension(imgNormal.FileName);        
        try
        {
            string fileName = BusinessUtility.GenerateRandomString(6, true) + fileExt;
            imgNormal.SaveAs(Server.MapPath(ImageUploadPath.PRODUCT_IMAGES) + fileName);
            return fileName;
        }
        catch 
        {
            return string.Empty;
        }        
    }    

    private int ProductImageID {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["imgID"], out id);
            return id;
        }
    }

    private int ProductID {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["prdID"], out id);
            return id;
        }
    }

    
    private string ProductImageType {
        get {
            switch (Request.QueryString["imgType"])
            {
                case ProductImages.PRODUCT_IMAGE_TYPE_FOUNDATION:
                    return ProductImages.PRODUCT_IMAGE_TYPE_FOUNDATION;
                case ProductImages.PRODUCT_IMAGE_TYPE_CHARM:
                    return ProductImages.PRODUCT_IMAGE_TYPE_CHARM;
                default:
                    return ProductImages.PRODUCT_IMAGE_TYPE_NORMAL;
            }                        
        }
    }

    private string ImageGroup {
        get {
            return Request.QueryString["imgGroup"];
        }
    }

    public string IsEarring {
        get {
            return (this.ImageGroup == "earring").ToString().ToLower();
        }
    }

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (this.ProductImageID > 0)
        {
            if (ValidateCategoryImage())
            {
                string smallFileName = this.GetUploadedImageName(fuPrdDisplayImage.PostedFile);
                string largeFileName = string.Empty;
                string img90Deg = string.Empty;
                if (fuPrdZoomImage.HasFile)
                {
                    largeFileName = this.GetUploadedImageName(fuPrdZoomImage.PostedFile);
                }
                if (fu90DegImage.HasFile)
                {
                    img90Deg = this.GetUploadedImageName(fu90DegImage.PostedFile);
                }
                if (!string.IsNullOrEmpty(smallFileName))
                {
                    if (string.IsNullOrEmpty(largeFileName))
                    {
                        largeFileName = smallFileName;
                    }
                    if (string.IsNullOrEmpty(img90Deg))
                    {
                        img90Deg = smallFileName;
                    }

                    _prdImage.Update(this.ProductImageID, ddlImageType.SelectedValue, smallFileName, largeFileName, img90Deg, true);

                    if (ddlImageType.SelectedValue == ProductImages.PRODUCT_IMAGE_TYPE_CHARM || ddlImageType.SelectedValue == ProductImages.PRODUCT_IMAGE_TYPE_FOUNDATION)
                    {
                        Response.Redirect(string.Format("ProductImageEdit.aspx?imgID={0}&prdID={1}&imgType={2}&imgGroup={3}", this.ProductImageID, this.ProductID, ddlImageType.SelectedValue, ddlImageGroup.SelectedValue));
                    }                   
                    else
                    {
                        Globals.RegisterCloseDialogScript(this);
                    }
                }
            }
        }
        else
        {
            if (ValidateCategoryImage())
            {
                string smallFileName = this.GetUploadedImageName(fuPrdDisplayImage.PostedFile);
                string largeFileName = string.Empty;
                string img90Deg = string.Empty;
                if (fuPrdZoomImage.HasFile)
                {
                    largeFileName = this.GetUploadedImageName(fuPrdZoomImage.PostedFile);
                }
                if (fu90DegImage.HasFile)
                {
                    img90Deg = this.GetUploadedImageName(fu90DegImage.PostedFile);
                }
                if (!string.IsNullOrEmpty(smallFileName))
                {
                    if (string.IsNullOrEmpty(largeFileName))
                    {
                        largeFileName = smallFileName;
                    }
                    if (string.IsNullOrEmpty(img90Deg))
                    {
                        img90Deg = smallFileName;
                    }
                    _prdImage.PimgIsDefault = true;
                    _prdImage.PrdID = this.ProductID;
                    _prdImage.PrdLargeImagePath = largeFileName;
                    _prdImage.PrdSmallImagePath = smallFileName;
                    _prdImage.ProductImageType = ddlImageType.SelectedValue;
                    _prdImage.ImageGroup = ddlImageGroup.SelectedValue;
                    _prdImage.Rotate90ImageUrl = img90Deg;
                    _prdImage.Insert();
                    if (_prdImage.PrdImageID > 0)
                    {
                        if (ddlImageType.SelectedValue == ProductImages.PRODUCT_IMAGE_TYPE_CHARM || ddlImageType.SelectedValue == ProductImages.PRODUCT_IMAGE_TYPE_FOUNDATION)
                        {
                            Response.Redirect(string.Format("ProductImageEdit.aspx?imgID={0}&prdID={1}&imgType={2}&imgGroup={3}", _prdImage.PrdImageID, this.ProductID, ddlImageType.SelectedValue, ddlImageGroup.SelectedValue));
                        }                        
                        else
                        {
                            Globals.RegisterReloadParentScript(this);
                        }
                    }
                }
            }
        }
    }

    protected void btnSave2_Click(object sender, EventArgs e)
    {
        int cW = BusinessUtility.GetInt(txtCharmWidth.Text);
        int co = BusinessUtility.GetInt(txtCharmOffset.Text);

        _prdImage.Update(this.ProductImageID, co, cW, hdnRecData.Value);
        Globals.RegisterReloadParentScript(this);
    }

    protected void btnSave3_Click(object sender, EventArgs e)
    {
        if (ProductImageID > 0)
        {
            //int cW = BusinessUtility.GetInt(txtCharmWidth.Text);
            //int co = BusinessUtility.GetInt(txtCharmOffset.Text);
            //_prdImage.Update(this.ProductImageID, co, cW, string.Empty);
            //Globals.RegisterReloadParentScript(this);
        }
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Shared/popup.master"  AutoEventWireup="true" CodeFile="AddEditProductCategory.aspx.cs" Inherits="Inventory_AddEditProductCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">

    <div id="contentBottom" style="padding: 5px; height: 200px; overflow: auto; text-align:center" onkeypress="return disableEnterKey(event)">
        <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        <br />
 
        <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
        <td class="text" style="font-weight:bold; font-size:20px;">
        <asp:Label ID="lblCategoryName" Text=" " runat="server"  />
        </td>
             <td></td> 
        </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblNameFrench" Text="<%$ Resources:Resource, lblNameFrench %>" runat="server" />
                </td>
                <td >
                    <asp:TextBox ID="txtNameFrench" runat = "server" Width="350px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label ID="lblNameEnglish" Text="<%$ Resources:Resource, lblNameEnglish %>" runat="server" />
                </td>
                <td  >
                    <asp:TextBox ID="txtNameEnglish" runat="server" Width="350px"  />
                </td>
            </tr>
            <tr>
            <td class="text">
            <asp:Label ID ="lblIsActive" runat="server" Text = "<%$ Resources:Resource, lblCMIsActive %>"></asp:Label>
            </td>
            <td>
            <asp:CheckBox ID ="chkIsActive" runat="server" />
            </td>
            </tr>
            <tr>
            <td class="text">
             <asp:Label ID="lblLastModifyBy" Text=" " runat="server" />
            </td>
            <td class="text">
             <asp:Label ID="lblLastModifyAt" Text=" " runat="server" />
            </td>
            </tr>
        </table>
    </div>
    <div class="div-dialog-command" style="border-top: 1px solid #ccc; padding: 5px;">
        <asp:Button Text="<%$Resources:Resource, btnSave %>" ID="btnSave" runat="server"
            ValidationGroup="RegisterUser" OnClick="btnSave_Click" />
        <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
            CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
    </div>
    <asp:ValidationSummary ID="valsAdminWarehouse" runat="server" ShowMessageBox="true"
        ShowSummary="false" ValidationGroup="RegisterUser" />

        <script type = "text/javascript">
            $(document).ready(function () {
                //$("#lblCategoryName").html($.getParamValue('CategoryName'));
            });

            $.extend({
                getParamValue: function (paramName) {
                    parName = paramName.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
                    var pattern = '[\\?&]' + paramName + '=([^&#]*)';
                    var regex = new RegExp(pattern);
                    var matches = regex.exec(window.location.href);
                    if (matches == null) return '';
                    else return decodeURIComponent(matches[1].replace(/\+/g, ' '));
                }
            });

        </script>
</asp:Content>

Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Partial Class Inventory_ProductColor
    Inherits BasePage
    Dim objPrdColorCode As New clsProductColorCode
    Dim objPrdColor As New clsProductColor
    Public objPrdDesc As New clsPrdDescriptions
    Protected Sub Inventory_ProductColor_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        lblHeading.Text = objPrdDesc.funProductName & lblPrdAddColor
        If Not Page.IsPostBack Then
            objPrdColorCode.PopulateColor(chklstColor)
            subFillGrid()
        End If
       
        If Session("UserModules").ToString.Contains("INROL") = True And (Session("UserModules").ToString.Contains("STK") = False Or Session("UserModules").ToString.Contains("ADM") = False) Then
            imgCmdSave.Visible = False
            imgCmdReset.Visible = False
        End If
    End Sub
    Public Sub subFillGrid()
        sqlsdPrdColor.SelectCommand = objPrdColor.subFillPrdColor
    End Sub
    Protected Sub grdvPrdColor_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdvPrdColor.PageIndexChanging
        subFillGrid()
    End Sub
    Protected Sub grdvPrdColor_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdvPrdColor.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
            "confirm('" & msgPrdColorDelete & "')")
        End If
        If Session("UserModules").ToString.Contains("INROL") = True And (Session("UserModules").ToString.Contains("STK") = False Or Session("UserModules").ToString.Contains("ADM") = False) Then
            grdvPrdColor.Columns(1).Visible = False
        End If
    End Sub
    Protected Sub grdvPrdColor_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdvPrdColor.RowDeleting
        Dim strColCode As String = grdvPrdColor.DataKeys(e.RowIndex).Value.ToString()
        lblMsg.Text = msgPrdColorDeleted
        sqlsdPrdColor.DeleteCommand = objPrdColor.subPrdColorDelete(strColCode) 'Delete Product color
        subFillGrid()
    End Sub
    Protected Sub grdvPrdColor_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdvPrdColor.Sorting
        subFillGrid()
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    Protected Sub imgCmdReset_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdReset.ServerClick
        chklstColor.ClearSelection()
    End Sub
    Protected Sub imgCmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCmdSave.ServerClick
        objPrdColor.ProductID = Request.QueryString("prdID")
        Dim strPrdCol As String = Session("color")
        If strPrdCol <> "" Then
            strPrdCol = strPrdCol.TrimStart(",")
            Session.Remove("color")
        End If
        objPrdColorCode.UpdateColor(chklstColor, Request.QueryString("prdID"))
        lblMsg.Text = msgPrdColorSaved
        subFillGrid()
    End Sub
End Class

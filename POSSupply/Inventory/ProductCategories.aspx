﻿<%@ Page Language="C#"  MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ProductCategories.aspx.cs" Inherits="Inventory_ProductCategories" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">   
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
    <%=UIHelper.GetScriptTags("blockui", 1)%>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
     <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <asp:Literal ID="ltTitle" Text="" runat="server" /> </b>
    </div>
    <div style="float: left; padding-top: 12px;">        
       <asp:Button ID="btnAdd" runat="server" Text=""  OnClientClick="return editPopup();" />
    </div>
    <div style="float: right; padding-top: 12px;"> 
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">    
     <asp:PlaceHolder ID="phControl" runat="server" /> 
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" Runat="Server">
    <ul id="custMenu" class="menu inbiz-left-menu collapsible">
        <li class="active"><a href="#"><%=Resources.Resource.lblProductCategories %></a>
            <asp:BulletedList ID="blstNav" DisplayMode="HyperLink" DataTextField="Text"
                DataValueField="Value" runat="server" style="display:block;">
            </asp:BulletedList>
        </li>
    </ul>
    <asp:HiddenField ID="hdnNexUrl" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">

        $.extend({
            getParamValue: function (paramName) {
                parName = paramName.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
                var pattern = '[\\?&]' + paramName + '=([^&#]*)';
                var regex = new RegExp(pattern);
                var matches = regex.exec(window.location.href);
                if (matches == null) return '';
                else return decodeURIComponent(matches[1].replace(/\+/g, ' '));
            }
        });

        function editPopup() {
            var url = "AddEditProductCategory.aspx";
            var queryData = {};
            queryData.CatgHdrID = $.getParamValue('section');
            queryData.Status = "INDIVIDUAL";
            var t = "<%=Resources.Resource.AddCategory%>";
           
            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 800,
                height: 400,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');

            return false;
        }

        function deleteCategory(CategoryID, CategoryHrdID) {

            if (confirm("<%=Resources.Resource.msgDeleteCategory%>")) {
                var datatoPost = {};
                datatoPost.isAjaxCall = 1;
                datatoPost.callBack = "deleteCategory";
                datatoPost.CategoryID = CategoryID;
                datatoPost.CategoryHrdID = CategoryHrdID;

                $.post("ProductCategories.aspx", datatoPost, function (data) {
                    if (data == "ok") {
//                        $grid.trigger("reloadGrid");
                        window.location.href = window.location.href;
                    }
                });
            }
        } 

    </script>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="ProductMaster.aspx.cs" Inherits="Inventory_ProductMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
    <script type="text/javascript">
        var _alertsPreLoaded = [];
        var _usersList = [];
    </script>
    <style type="text/css">
        /*.ui-menu .ui-menu-item { white-space:nowrap; padding:0 10px 0 0; }*/
        div.autocomplete_holder input[type=text]
        {
            width: 50px;
            margin: 0 0 2px 0;
            padding: 0 0 3px;
            position: relative;
            top: 0;
            float: left;
            border: none;
        }
        div.autocomplete_holder span
        {
            display: block;
            width: auto;
            margin: 0 3px 3px 0;
            padding: 3px 20px 4px 8px;
            position: relative;
            float: left;
            text-indent: 0;
            background-color: #eee;
            border: 1px solid #333;
            -moz-border-radius: 7px;
            -webkit-border-radius: 7px;
            border-radius: 7px;
            color: #333;
            font: normal 11px Verdana, Sans-serif;
        }
        div.autocomplete_holder span a
        {
            position: absolute;
            right: 8px;
            top: 2px;
            color: #666;
            font: bold 12px Verdana, Sans-serif;
            text-decoration: none;
        }
        div.autocomplete_holder span a:hover
        {
            color: #ff0000 !important;
        }
        
        div.autocomplete_holder span.add
        {
            background-color: #d7ffd7;
        }
        div.autocomplete_holder span.add a
        {
            font-weight: bold;
            color: Green;
        }
        div.autocomplete_holder span.add a:hover
        {
            color: #ff0000 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div style="float: left; width: 100%;">
        <div style="float: left; width: 215px;">
            <div class="col1">
                <img src="../lib/image/icon/ico_admin.png" alt="" />
            </div>
            <div class="col2">
                <h1>
                    <%= Resources.Resource.Administration %></h1>
            </div>
        </div>
        <div style="margin-left: 212px;">
            <h3>
                <asp:Label ID="Label1" Text="<%$Resources:Resource,lblProductMaster%>" runat="server" />
            </h3>
            <div style="width: 100%;">
                <div style="float: left; padding-top: 12px;">
                </div>
                <table cellspacing="0">
                    <tr>
                        <td>
                            <%= Resources.Resource.lblCollection%>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCollection" runat="server" CssClass="filter-key">
                                <asp:ListItem Selected="True" Text="" Value="" />
                                <asp:ListItem Text="2014 Summer" Value="1" />
                                <asp:ListItem Text="2014 Winter" Value="2" />
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:Button ID="btnCreateNewCollection" runat="server" Text="<%$Resources:Resource,btnCreationCollection%>" />
                            <iCtrl:IframeDialog ID="mdCreateCollection" TriggerControlID="btnCreateNewCollection"
                                Width="450" Height="360" Url="AddCollection.aspx" Title="<%$Resources:Resource,lblCollection%>"
                                runat="server">
                            </iCtrl:IframeDialog>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <input type="button" id="btnAddNewStyle" value="<%=Resources.Resource.btnAddNewStyle%>"
                                style="display: none;" onclick='AddStyle();' />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width: 100%; padding-left: 3px;">
                <div style="float: left;">
                    <b><span id="lblSelectedCollection" style="font-size: 22px;">&nbsp;</span></b>
                </div>
                <div style="float: right;">
                    <asp:Button ID="btnPublishStyles" runat="server" Text="<%$Resources:Resource,btnPublishAllActiveStyle%>"
                        OnClick="btnPublishStyles_OnClick" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="grid_wrapper" style="width: 100%;">
        <trirand:JQGrid runat="server" ID="grdAlerts" DataSourceID="sdsAlerts" Height="300px"
            AutoWidth="true" OnCellBinding="grdAlerts_CellBinding" OnDataRequesting="grdAlerts_DataRequesting">
            <Columns>
                <trirand:JQGridColumn DataField="Style" HeaderText="<%$ Resources:Resource, lblGrdStyle %>" />
                <trirand:JQGridColumn DataField="MasterID" HeaderText="<%$ Resources:Resource, lblGrdMaterial %>"
                    Sortable="false" />
                <trirand:JQGridColumn DataField="MasterID" HeaderText="<%$ Resources:Resource, lblGrdTexture %>"
                    Sortable="false" Visible="false" />
                <trirand:JQGridColumn DataField="MasterID" HeaderText="<%$ Resources:Resource, lblGrdColor %>"
                    Sortable="false" />
                <trirand:JQGridColumn DataField="MasterID" HeaderText="<%$ Resources:Resource, lblGrdSizeGroup %>"
                    Sortable="false" />
                <trirand:JQGridColumn DataField="MasterID" HeaderText="<%$ Resources:Resource, lblGrdCategory %>"
                    Sortable="false" Visible="false" />
                <trirand:JQGridColumn DataField="MasterID" HeaderText="<%$ Resources:Resource, lblGrdDetails %>"
                    Sortable="false" TextAlign="Center" Width="50" />
                <trirand:JQGridColumn DataField="IsActive" HeaderText="<%$ Resources:Resource, lblGrdActive %>"
                    Sortable="false" TextAlign="Center" Width="50" />
                <trirand:JQGridColumn DataField="IsPublished" HeaderText="<%$ Resources:Resource, lblGrdPublish %>"
                    Sortable="false" TextAlign="Center" Width="50" />
                <trirand:JQGridColumn DataField="MasterID" HeaderText="<%$ Resources:Resource, lblGrdCopy %>"
                    Sortable="false" TextAlign="Center" Width="50" />
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <ClientSideEvents LoadComplete="loadComplete" BeforeRowSelect="beforeRowSelect" />
        </trirand:JQGrid>
        <asp:SqlDataSource ID="sdsAlerts" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
    <asp:Panel runat="server" ID="SearchPanel">
        <div class="searchBar">
            <div class="header">
                <div class="title">
                    <asp:Literal ID="ltSearchForm" Text="<%$Resources:Resource,lblSearchForm %>" runat="server" />
                </div>
                <div class="icon">
                    <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
            </div>
            <h4>
                <asp:Label ID="lblSearchKeyword" CssClass="filter-key" AssociatedControlID="txtStyle"
                    runat="server" Text="<%$ Resources:Resource, lblStyle %>"></asp:Label></h4>
            <div class="inner">
                <asp:TextBox runat="server" Width="180px" ID="txtStyle" CssClass="filter-key"></asp:TextBox>
            </div>
            <h4>
                <asp:Label ID="Label2" CssClass="filter-key" AssociatedControlID="ddlCollectionSearch"
                    runat="server" Text="<%$ Resources:Resource, lblCollection %>"></asp:Label></h4>
            <div class="inner">
                <asp:DropDownList ID="ddlCollectionSearch" runat="server" CssClass="filter-key">
                    <asp:ListItem Text="All" Value="" Selected="True" />
                </asp:DropDownList>
            </div>
            <h4>
                <asp:Label ID="Label3" CssClass="filter-key" AssociatedControlID="ddlMaterialSearch"
                    runat="server" Text="<%$ Resources:Resource, lblMaterial %>"></asp:Label></h4>
            <div class="inner">
                <asp:DropDownList ID="ddlMaterialSearch" runat="server" CssClass="filter-key">
                    <asp:ListItem Text="All" Value="" Selected="True" />
                </asp:DropDownList>
            </div>
            <h4>
                <asp:Label ID="Label4" CssClass="filter-key" AssociatedControlID="lstColor" runat="server"
                    Text="<%$ Resources:Resource, lblPrdColor %>"></asp:Label></h4>
            <div class="inner">
                <%--<asp:ListBox ID="lstColor" runat="server" CssClass="filter-key"></asp:ListBox>--%>
                <asp:ListBox CssClass="modernized_select" ID="lstColor" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblPrdColor  %>"></asp:ListBox>
            </div>
            <h4>
                <asp:Label ID="Label5" CssClass="filter-key" AssociatedControlID="lstSizeGroup" runat="server"
                    Text="<%$ Resources:Resource, lblGrdSizeGroup %>"></asp:Label></h4>
            <div class="inner">
                <asp:ListBox ID="lstSizeGroup" runat="server" SelectionMode="Multiple" CssClass="filter-key"></asp:ListBox>
            </div>
            <h4>
                <asp:Label ID="Label6" CssClass="filter-key" AssociatedControlID="lstProdCatg" runat="server"
                    Text="<%$ Resources:Resource, lblGrdCategory %>"></asp:Label></h4>
            <div class="inner">
                <%--<asp:ListBox ID="lstProdCatg" runat="server" CssClass="filter-key"></asp:ListBox>--%>
                <asp:ListBox CssClass="modernized_select" ID="lstProdCatg" runat="server" SelectionMode="Multiple"
                        data-placeholder="<%$ Resources:Resource, lblGrdCategory  %>"></asp:ListBox>
            </div>
<%--            <h4>
                <asp:Label ID="Label7" CssClass="filter-key" AssociatedControlID="lstProdKeyWord"
                    runat="server" Text="<%$ Resources:Resource, lblKeyword %>"></asp:Label></h4>
            <div class="inner">
                <asp:ListBox ID="lstProdKeyWord" runat="server" CssClass="filter-key"></asp:ListBox>
            </div>--%>
            <div class="footer">
                <div class="submit">
                    <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                </div>
                <br />
            </div>
        </div>
        <br />
    </asp:Panel>
    <div class="clear">
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $grid = $("#<%=grdAlerts.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=grdAlerts.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }
        function loadComplete(data) {
            jqGridResize();
        }

        //prevent jq grid from row selection
        function beforeRowSelect(rowid, e) {
            return false;
        }

        
    </script>
    <script type="text/javascript">
        function CollectionCreated(collectionID) {
            try {
                $.ajax({
                    type: "POST",
                    url: "productMaster.aspx/GetCollectionList",
                    dataType: "json",
                    data: "{}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var addressList = data.d;
                        $('option', $("#<%=ddlCollection.ClientID%>")).remove();
                        $("#<%=ddlCollection.ClientID%>").append($("<option />").val("").text(""));
                        for (var i = 0; i < addressList.length; i++) {
                            $("#<%=ddlCollection.ClientID%>").append($("<option />").val(addressList[i].CollectionID).text(addressList[i].ShortName));
                        }
                        SetListSelectedValue('<%=ddlCollection.ClientID %>', collectionID);
                        $("#<%=ddlCollection.ClientID%>").trigger("change");
                        $("#<%=btnCreateNewCollection.ClientID%>").removeClass("ui-state-focus");
                        getGlobalMessage();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    }
                });
            }
            catch (e) {
            }
            loadSearchControl();
        }

        $("#<%=ddlCollection.ClientID%>").change(function () {
            var sText = $("#<%=ddlCollection.ClientID %> :selected").text();
            var sCollectionID = $("#<%=ddlCollection.ClientID %> :selected").val();
            $("#lblSelectedCollection").html(sText);
            $("#btnAddNewStyle").css("display", "block");
            GridPost();
        });


        jQuery(window).bind("focus", function (event) {
            $("#<%=btnCreateNewCollection.ClientID%>").removeClass("ui-state-focus");
            $("#btnAddNewStyle").removeClass("ui-state-focus");
        });

        function GridPost() {
            var sCollectionID = $("#<%=ddlCollection.ClientID %> :selected").val();
            var filterArr = {};
            filterArr["_Collection"] = sCollectionID;
            $grid.appendPostData(filterArr);
            $grid.trigger("reloadGrid", [{ page: 1}]);
            return false;
        }

        $(document).ready(function () {
            loadSearchControl();
            GridPost();
        });

        function loadSearchControl() {
            ReloadSearchConnectionList();
            ReloadSearchMaterialList();
//            ReloadSearchColorList();
            ReloadSearchCatgSizeList();
//            ReloadSearchCatgList();
//            ReloadSearchKeyWordList();
        }

        function ReloadSearchConnectionList() {
            $.ajax({
                type: "POST",
                url: "productMaster.aspx/GetCollectionList",
                dataType: "json",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var addressList = data.d;
                    $('option', $("#<%=ddlCollectionSearch.ClientID%>")).remove();
                    $("#<%=ddlCollectionSearch.ClientID%>").append($("<option />").val("").text("<%=Resources.Resource.lblAll%>"));
                    for (var i = 0; i < addressList.length; i++) {
                        $("#<%=ddlCollectionSearch.ClientID%>").append($("<option />").val(addressList[i].CollectionID).text(addressList[i].ShortName));
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }

        function ReloadSearchMaterialList() {
            $.ajax({
                type: "POST",
                url: "productMaster.aspx/GetMaterialList",
                dataType: "json",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var addressList = data.d;
                    $('option', $("#<%=ddlMaterialSearch.ClientID%>")).remove();
                    $("#<%=ddlMaterialSearch.ClientID%>").append($("<option />").val("").text("<%=Resources.Resource.lblAll%>"));
                    for (var i = 0; i < addressList.length; i++) {
                        $("#<%=ddlMaterialSearch.ClientID%>").append($("<option />").val(addressList[i].MaterialID).text(addressList[i].MaterialName));
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }

        function ReloadSearchColorList() {
            $.ajax({
                type: "POST",
                url: "productMaster.aspx/GetColorList",
                dataType: "json",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var addressList = data.d;
                    $('option', $("#<%=lstColor.ClientID%>")).remove();
                    $("#<%=lstColor.ClientID%>").append($("<option />").val("").text("<%=Resources.Resource.lblAll%>"));
                    var sOption = '';
                    for (var i = 0; i < addressList.length; i++) {
                        $("#<%=lstColor.ClientID%>").append($("<option />").val(addressList[i].ColorID).text(addressList[i].ColorName));
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }

        function ReloadSearchCatgSizeList() {
            $.ajax({
                type: "POST",
                url: "productMaster.aspx/GetCategorySizeList",
                dataType: "json",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var sSelGroup = '';
                    var addressList = data.d;
                    $('option', $("#<%=lstSizeGroup.ClientID%>")).remove();
                    $("#<%=lstSizeGroup.ClientID%>").append($("<option />").val("").text("<%=Resources.Resource.lblAll%>"));
                    var sOption = '';
                    for (var i = 0; i < addressList.length; i++) {
                        var sGroupName = addressList[i].CategoryName;
                        if (sSelGroup == "") {
                            sSelGroup = sGroupName;
                            $("#<%=lstSizeGroup.ClientID%>").append($("<option />").val(addressList[i].CategoryName).text(addressList[i].CategoryName));
                        }
                        else if (sSelGroup != sGroupName) {
                            sSelGroup = sGroupName;
                            $("#<%=lstSizeGroup.ClientID%>").append($("<option />").val(addressList[i].CategoryName).text(addressList[i].CategoryName));
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }


       

        function SetListSelectedValue(elementID, valueSelect) {
            var selectedArray = new Array();
            var selObj = document.getElementById(elementID);
            var i;
            var count = 0;
            for (i = 0; i < selObj.options.length; i++) {
                var sText = selObj.options[i].text;
                if (selObj.options[i].value == valueSelect) {
                    selObj.options[i].selected = true;
                }
            }
        }

        function ReloadStyle() {
            loadSearchControl();
            GridPost();
        }

        function AddStyle() {
            var sCollectionID = $("#<%=ddlCollection.ClientID %> :selected").val();
            var url = 'AddStyle.aspx';
            var queryData = {};
            queryData.CollectionID = sCollectionID;
            var t = "<%=Resources.Resource.lblStyle%>";

            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 450,
                height: 360,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');

            return false;
        }


        function AddMaterial(styleID) {
            var url = 'AddMaterial.aspx';
            var queryData = {};
            queryData.StyleID = styleID;
            var t = "<%=Resources.Resource.lblAddMaterial%>";
            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 720,
                height: 600,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');
            return false;
        }


        function RemoveUsedMaterial(source, materialID, masterID) {
            if (confirm("<%=Resources.Resource.msgDeleteMaterial%>")) {
                var datatoPost = {};
                datatoPost.isAjaxCall = 1;
                datatoPost.callBack = "deleteMaterial";
                datatoPost.MasterID = masterID;
                datatoPost.MaterialID = materialID;

                $.post("ProductMaster.aspx", datatoPost, function (data) {
                    if (data == "ok") {
                        GridPost();
                    }
                });
            }
        }


        function AddTexture(styleID) {
            var url = 'AddTexture.aspx';
            var queryData = {};
            queryData.StyleID = styleID;
            var t = "<%=Resources.Resource.lblAddTexture%>";
            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 720,
                height: 600,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');
            return false;
        }


        function RemoveUsedTexture(source, textureID, masterID) {
            if (confirm("<%=Resources.Resource.msgDeleteTexture%>")) {
                var datatoPost = {};
                datatoPost.isAjaxCall = 1;
                datatoPost.callBack = "deleteTexture";
                datatoPost.MasterID = masterID;
                datatoPost.TextureID = textureID;

                $.post("ProductMaster.aspx", datatoPost, function (data) {
                    if (data == "ok") {
                        $(source).parent().remove();
                    }
                });
            }
        }


        function AddColor(styleID) {
            var url = 'AddColor.aspx';
            var queryData = {};
            queryData.StyleID = styleID;
            var t = "<%=Resources.Resource.lblAddColor%>";
            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 720,
                height: 600,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');
            return false;
        }


        function RemoveUsedColor(source, colorID, masterID) {
            if (confirm("<%=Resources.Resource.msgDeleteColor%>")) {
                var datatoPost = {};
                datatoPost.isAjaxCall = 1;
                datatoPost.callBack = "deleteColor";
                datatoPost.MasterID = masterID;
                datatoPost.ColorID = colorID;

                $.post("ProductMaster.aspx", datatoPost, function (data) {
                    if (data == "ok") {
                        $(source).parent().remove();
                    }
                });
            }
        }


        function AddSizeGroup(styleID) {
            var url = 'AddSizeCategory.Aspx';
            var queryData = {};
            queryData.StyleID = styleID;
            var t = "<%=Resources.Resource.lblAddSizeCategory%>";
            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 720,
                height: 600,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');
            return false;
        }


        function RemoveUsedSizeGroup(source, categorySize, masterID) {
            if (confirm("<%=Resources.Resource.msgDeleteCategorySize%>")) {
                var datatoPost = {};
                datatoPost.isAjaxCall = 1;
                datatoPost.callBack = "deleteCategorySize";
                datatoPost.MasterID = masterID;
                datatoPost.CategorySize = categorySize;

                $.post("ProductMaster.aspx", datatoPost, function (data) {
                    if (data == "ok") {
                        $(source).parent().remove();
                    }
                });
            }
        }


        function AddCategory(styleID) {
            var url = 'AddProductStyleCategory.aspx';
            var queryData = {};
            queryData.StyleID = styleID;
            var t = "<%=Resources.Resource.lblSelectCategory%>";
            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 720,
                height: 600,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');
            return false;
        }


        function RemoveUsedCategory(source, categoryID, masterID) {
            if (confirm("<%=Resources.Resource.msgDeleteCategory%>")) {
                var datatoPost = {};
                datatoPost.isAjaxCall = 1;
                datatoPost.callBack = "deleteCategory";
                datatoPost.MasterID = masterID;
                datatoPost.CategoryID = categoryID;

                $.post("ProductMaster.aspx", datatoPost, function (data) {
                    if (data == "ok") {
                        $(source).parent().remove();
                    }
                });
            }
        }

        //AddCategory
        function AddItemDetail(styleID) {
            var url = 'AddEditProductDetail.aspx';
            var queryData = {};
            queryData.StyleID = styleID;
            var t = "<%=Resources.Resource.lblProductDetail%>";
            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 1000,
                height: 760,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');
            return false;
        }

        //CopyCollectionStyle
        function AddCopyCollectionStyle(styleID, styleName) {
            var url = 'CopyCollectionStyle.aspx';
            var queryData = {};
            queryData.StyleID = styleID;
            queryData.StyleOrgName = styleName;
            queryData.CollectionID = $("#<%=ddlCollection.ClientID %> :selected").val();
            queryData.StyleName = $("#lblSelectedCollection").text() + " -- " + styleName;
            var t = "<%=Resources.Resource.lblCopyCollectionStyle%>";
            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 450,
                height: 250,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');
            return false;
        }
    </script>
</asp:Content>

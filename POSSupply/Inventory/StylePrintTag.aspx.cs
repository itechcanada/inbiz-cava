﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;
using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
public partial class Inventory_StylePrintTag : System.Web.UI.Page
{
    Product _prd = new Product();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security Check
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.INVENTORY_MANAGER))
        {
            Response.Redirect("~/Errors/AccessDenined.aspx");
        }


        if (!IsPostBack)
        {
            Collection objCollection = new Collection();

            List<Collection> lResult = new List<Collection>();

            lResult.Add(new Collection { CollectionName = "", CollectionID = BusinessUtility.GetInt(0) });

            //var lst = objCollection.GetCollectionList(null, Globals.CurrentAppLanguageCode);
            //foreach (var item in lst)
            //{
            //    lResult.Add(new Collection { CollectionName = item.ShortName, CollectionID = BusinessUtility.GetInt(item.CollectionID) });
            //}

            //ddlCollection.DataSource = lResult;
            //ddlCollection.DataTextField = "CollectionName";
            //ddlCollection.DataValueField = "CollectionID";
            //ddlCollection.DataBind();

            SysWarehouses wh = new SysWarehouses();
            wh.FillAllWharehouse(null, ddlWarehouse, CurrentUser.UserDefaultWarehouse, 0, new ListItem(""));

        }
        ddlCollection.Focus();
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Collection> GetCollectionList()
    {
        Collection objCollection = new Collection();
        var t = objCollection.GetCollectionList(null, Globals.CurrentAppLanguageCode);
        return t;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string UpdateCollectionStyle(string csIDs, string csQtys, string whs)
    {
        try
        {
            string[] cstyleIDs = csIDs.Split('~');
            string[] cstyleQtys = csQtys.Split('~');
            if (cstyleIDs.Length == cstyleQtys.Length)
            {
                Product prd = new Product();
                for (int i = 0; i < cstyleIDs.Length; i++)
                {
                    prd.UpdatePrdOnHandQty(null, BusinessUtility.GetInt(cstyleIDs[i]), BusinessUtility.GetInt(cstyleQtys[i]), whs);
                }
            }
            //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgPrdQuantityUpdated);
            //Globals.RegisterReloadParentScript();
            return "success";
        }
        catch (Exception ex)
        {
            throw;
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetStyleCollection(string style, string collectionID, string whs)
    {
        Product prd = new Product();
        DataTable dt = prd.GetProductDetailStyle(null, style, StatusProductType.Product, Globals.CurrentAppLanguageCode, BusinessUtility.GetInt(collectionID), whs);
        if (dt != null && dt.Rows.Count > 0)
        {
            var result = from row in dt.AsEnumerable()
                         group row by row.Field<string>("Size") into grp
                         select new
                         {
                             Size = grp.Key,
                             MemberCount = grp.Count()
                         };

            string sHtmlHeading = "<tr> ";
            sHtmlHeading += "<td> </td>";
            string sHtmlContent = "";
            int iItemSizeCount = 0;

            foreach (var item in result)
            {
                sHtmlHeading += "<td> <b> " + BusinessUtility.GetString(item.Size) + " </b> </td>";
                iItemSizeCount = BusinessUtility.GetInt(item.MemberCount);
            }

            sHtmlHeading += "</tr>";

            DataTable dtProductColor = prd.GetProductDetailGroupbyColor(null, style, StatusProductType.Product, Globals.CurrentAppLanguageCode, BusinessUtility.GetInt(collectionID), whs);
            int i = 0;
            string sFirstProcutID = "";
            string sFirstPrdEndUserSalesPrice = "";
            int iCount = 0;
            string sPIDs = "";
            foreach (DataRow dRow in dtProductColor.Rows)
            //foreach (DataRow dRow in dt.Rows)
            {
                string sClass = "cssPqty";
                sHtmlContent += "<tr>";
                sHtmlContent += "<td>Color  <b> " + BusinessUtility.GetString(dRow["Color"]) + "</b> </br>  SKU #    </td> ";
                DataView datavw = new DataView();
                datavw = dt.DefaultView;
                datavw.RowFilter = "Color='" + BusinessUtility.GetString(dRow["Color"]) + "'";
                DataTable dtProduct = datavw.ToTable();
               

                if (dtProduct.Rows.Count > 0)
                {

                    foreach (DataRow drProduct in dtProduct.Rows)
                    {
                        if (iCount == 0)
                        {
                            sFirstProcutID = "qty" + BusinessUtility.GetString(drProduct["productID"]);
                            sFirstPrdEndUserSalesPrice = BusinessUtility.GetString(drProduct["EndUserSalesPrice"]);
                        }
                        iCount += 1;
                        //sHtmlContent += "<td><input id=" + "qty" + BusinessUtility.GetString(drProduct["productID"]) + " value ='" + BusinessUtility.GetString(drProduct["prdOhdQty"]) + "' pname='" + BusinessUtility.GetString(drProduct["prdName"]) + "' pbarcode='" + BusinessUtility.GetString(drProduct["prdUPCCode"]) + "'  class=" + sClass + " ppid=" + BusinessUtility.GetString(dRow["productID"]) + "   pid=" + BusinessUtility.GetString(drProduct["productID"]) + "  type='text'  CssClass='numericTextField'  style='width:100px;'/> </br> " + BusinessUtility.GetString(drProduct["prdUPCCode"]) + " </td> ";
                        sHtmlContent += "<td><input id=" + "qty" + BusinessUtility.GetString(drProduct["productID"]) + " value ='0' pname='" + BusinessUtility.GetString(drProduct["prdName"]) + "' pbarcode='" + BusinessUtility.GetString(drProduct["prdUPCCode"]) + "'  class=" + sClass + " ppid=" + BusinessUtility.GetString(dRow["productID"]) + "   pid=" + BusinessUtility.GetString(drProduct["productID"]) + "  type='text'  CssClass='numericTextField'  style='width:100px;'/> </br> " + BusinessUtility.GetString(drProduct["prdUPCCode"]) + " </td> ";

                        if (sPIDs == "")
                        {
                            sPIDs = BusinessUtility.GetString(drProduct["productID"]);
                        }
                        else
                        {
                            sPIDs += "~" + BusinessUtility.GetString(drProduct["productID"]);
                        }

                    }
                }
                //sHtmlContent += string.Format("<td style='vertical-align:top;'> <input  id=" + "btn" + BusinessUtility.GetString(dRow["productID"]) + " type=button value= '" + Resources.Resource.lblUpdate + "' onclick =  updateCollStyle('{0}') />  </td> ", sPIDs);
                sHtmlContent += "</tr>";
                i += 1;
            }

            string sHtmlTable = "<table style='border:1;width:100%;padding:5px;'> ";
            sHtmlTable += sHtmlHeading;
            sHtmlTable += sHtmlContent;
            sHtmlTable += "</table> ";

            return sHtmlTable + "*" + sFirstProcutID + "^" + sFirstPrdEndUserSalesPrice + "@" + sPIDs;

        }
        else
        {
            //return "<font style='color:Red; font-Weight:bold; font-size:14px; text-align: center; '> " + Resources.Resource.msgPrdNotFound + " </font> *";
            return "";
        }
    }


}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="StylePrintTag.aspx.cs" Inherits="Inventory_StylePrintTag" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
    </div>
    <div style="float: left; padding-top: 12px;">
        <literal id="lblCollectionName" style="font-size: 20px; font-weight: bold;"></literal>
        <literal id="lblStyleName" style="font-size: 20px; font-weight: bold; padding-left: 40px;"></literal>
    </div>
    <div style="float: right;">
    <span>
                <asp:Label ID="lblEndUserSalesPrice" Text="<%$ Resources:Resource, lblPrice %>"
                    runat="server" />
            </span>
             <span>
                <%--<asp:TextBox ID="txtEndUserSalesPrice" runat="server" MaxLength="20" CssClass="numericTextField" />--%>
                <input type="text" id  = "txtEndUserSalesPrice"MaxLength="20" CssClass="numericTextField" />
                </span>
     <span id="prdTagPrinting" runat="server">
            <asp:Button ID="btnPrintTags" Text="<%$Resources:Resource, lblPrintTags %>" runat="server" CausesValidation="false" />
        </span>
        <span id = prdQties></span>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        <asp:Literal ID="ltSearchForm" Text="<%$Resources:Resource,lblSearchForm %>" runat="server" />
                    </div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="Label2" runat="server" CssClass="filter-key" AssociatedControlID="ddlWarehouse"
                        Text="<%$ Resources:Resource, lblWhs%>"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="filter-key">
                        <asp:ListItem Value="" Selected="True" />
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="Label1" runat="server" CssClass="filter-key" AssociatedControlID="ddlCollection"
                        Text="<%$ Resources:Resource, lblCollection%>"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlCollection" runat="server" CssClass="filter-key">
                        <asp:ListItem Value="" Selected="True" />
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" CssClass="filter-key" AssociatedControlID="txtSearch"
                        runat="server" Text="<%$ Resources:Resource, lblGrdStyle %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div id="divCollectionStyleID" style="width:100%;">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
    <script type="text/javascript">


        $(document).ready(function () {
            loadSearchControl();
        });

        $("#btnSearch").click(function () {
            var sSearchText = $("#<%=txtSearch.ClientID%>").val();
            var collectionName = $("#<%=ddlCollection.ClientID %> :selected").text();

            var sSearch = $("#<%=ddlCollection.ClientID %> :selected").val();
            var sWarehouse = $("#<%=ddlWarehouse.ClientID %> :selected").val();
            if (sWarehouse == "") {
                alert('<%=Resources.Resource.custvalPOWarehouse %>');
                $("#<%=ddlWarehouse.ClientID %>").focus();
                return;
            }
            else if (collectionName == "") {
                var altermsg = "<%=Resources.Resource.msgPleaseSelectCollection %>";
                alert(altermsg);
                $("#<%=ddlCollection.ClientID %>").focus();
                return;
            }
            else if (sSearchText == "") {
                var altermsg = "<%=Resources.Resource.msgPleaseEnterStyle %>";
                $("#<%=txtSearch.ClientID %>").focus();
                alert(altermsg);
                return;
            }
            if (sSearchText != "" && sSearch != "" && sWarehouse != "") {
                $.ajax({
                    type: "POST",
                    url: "StylePrintTag.aspx/GetStyleCollection",
                    dataType: "json",
                    data: "{ style: '" + sSearchText + "',collectionID: '" + sSearch + "',whs: '" + sWarehouse + "' }",
                    contentType: "application/json;",
                    success: function (data) {
                        var addressList = data.d;
                        if (addressList != "") {
                            $("#lblCollectionName").html(collectionName);
                            $("#lblStyleName").html(sSearchText);
                            var valhtml = addressList.split("*");
                            if (valhtml.length > 1) {
                                $("#divCollectionStyleID").html(valhtml[0]);
                                var valSecondhtml = valhtml[1].split("^");
                                if (valSecondhtml.length > 1) {
                                    var frstprdID = valSecondhtml[0];
                                    if (frstprdID.length > 0) {
                                        $("#" + frstprdID).focus();
                                    }
                                    var valThirdhtml = valSecondhtml[1].split("@");
                                    if (valThirdhtml.length > 1) {
                                        $("#txtEndUserSalesPrice").val(valThirdhtml[0]);
                                        $("#prdQties").val(valThirdhtml[1]);
                                    }
                                }



                            }
                        }
                        else {
                            $("#lblCollectionName").html("");
                            $("#lblStyleName").html("");
                            $("#divCollectionStyleID").css('text-align', 'center');
                            $("#divCollectionStyleID").html("<font style='color:Red; font-Weight:bold; font-size:14px;'> " + "<%=Resources.Resource.msgPrdNotFound%>" + "</font>");
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    }
                });
            }
            else {
                $("#lblCollectionName").html("");
                $("#lblStyleName").html("");
                $("#divCollectionStyleID").html("");
            }
        });

        $("#<%=btnPrintTags.ClientID%>").click(function () {


            var url = '../Receiving/PrintTagsNew.aspx';

            var pPids = $("#prdQties").val();
            var csIDS = pPids.split("~");
            var csQtys = "";
            for (var i = 0; i < csIDS.length; i++) {
                var csQty = $("#qty" + csIDS[i]).val();
                csQtys[i] = csQty;
                if (i == 0) {
                    csQtys = csQty;
                }
                else {
                    csQtys += "~" + csQty;
                }
            }
            var vprice = $("#txtEndUserSalesPrice").val();
            url += "?Pids=" + pPids + "&pQties=" + csQtys + "&price=" + vprice;
            window.open(url, 'Print', 'height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');
            return false;
        });

        function updateCollStyle(vPids) {

            var csIDS = vPids.split("~");
            var csQtys = "";
            for (var i = 0; i < csIDS.length; i++) {
                var csQty = $("#qty" + csIDS[i]).val();
                csQtys[i] = csQty;
                if (i == 0) {
                    csQtys = csQty;
                }
                else {
                    csQtys += "~" + csQty;
                }
            }
            if (confirm('<%=Resources.Resource.msgConfrmUpdateQty%>') == true) {
                if (csIDS.length > 0 && csQtys.length > 0) {
                    var sWarehouse = $("#<%=ddlWarehouse.ClientID %> :selected").val();
                    $.ajax({
                        type: "POST",
                        url: "ProductSQUpdate.aspx/UpdateCollectionStyle",
                        dataType: "json",
                        data: "{ csIDs: '" + vPids + "',csQtys: '" + csQtys + "',whs: '" + sWarehouse + "' }",
                        contentType: "application/json;",
                        success: function (data) {
                            var result = data.d;
                            if (result == "success") {
                                alert("<%=Resources.Resource.msgPrdQuantityUpdated%>");
                            }
                            else {
                                alert("<%=Resources.Resource.InvoiceProcessError%>");

                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                        }
                    });
                }
            }
            else {
                return false;
            }
        }



        function loadSearchControl() {
            ReloadSearchConnectionList();
        }

        function ReloadSearchConnectionList() {
            $.ajax({
                type: "POST",
                url: "ProductSQUpdate.aspx/GetCollectionList",
                dataType: "json",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var addressList = data.d;
                    $('option', $("#<%=ddlCollection.ClientID%>")).remove();
                    $("#<%=ddlCollection.ClientID%>").append($("<option />").val("").text(""));
                    for (var i = 0; i < addressList.length; i++) {
                        $("#<%=ddlCollection.ClientID%>").append($("<option />").val(addressList[i].CollectionID).text(addressList[i].ShortName));
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }

    </script>
</asp:Content>




﻿//Help From http://alexmarandon.com/articles/web_widget_jquery/
(function () {
    // Localize jQuery variable
    var jQuery;
    /******** Load jQuery if not present *********/
    if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.4.2') {
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type", "text/javascript");
        script_tag.setAttribute("src",
            "http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js");
        if (script_tag.readyState) {
            script_tag.onreadystatechange = function () { // For old versions of IE
                if (this.readyState == 'complete' || this.readyState == 'loaded') {
                    scriptLoadHandler();
                }
            };
        } else {
            script_tag.onload = scriptLoadHandler;
        }
        // Try to find the head, otherwise default to the documentElement
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
    } else {
        // The jQuery version on the window is the one we want to use
        jQuery = window.jQuery;
        main();
    }

    /******** Called once jQuery has loaded ******/
    function scriptLoadHandler() {
        // Restore $ and window.jQuery to their previous values and store the
        // new jQuery in our local jQuery variable
        jQuery = window.jQuery.noConflict(true);
        // Call our main function
        main();
    }

    /******** Our main function ********/
    function main() {
        jQuery(document).ready(function ($) {
            /******* Load CSS *******/
            var css_link = $("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: "http://www.sivananda.org/camp/wp-content/themes/SimplePress/epanel/shortcodes/shortcodes.css?ver=1.6"
            });
            css_link.appendTo('head');

            /******* Load HTML *******/
            var jsonp_url = "http://localhost:7500/InbizWebDvlp/widget/widget_data.aspx?jsoncallback=?";
            $.getJSON(jsonp_url, function (data) {
                //alert(data);
                var html = "<h2>" + data.Title + "</h2>";
                html += "<div>" + data.Description + "</div>";
                if (data.Items.length > 0) {
                    for (var i = 0; i < data.Items.length; i++) {
                        html += "<p style='text-align: left;'>";
                        html += "<strong><br>" + data.Items[i].Title + "<br></strong>";
                        html += data.Items[i].Description;
                        html += "<p>";
                        html += "<div class='et-pricing clearfix'>";
                        for (var j = 0; j < data.Items[i].Rooms.length; j++) {
                            html += "<div class='pricing-table";
                            html += data.Items[i].IsSpecial ? " pricing-big'>" : "'>";
                            html += "<div class='pricing-heading'>";
                            html += "<h2 class='pricing-title'>" + data.Items[i].Rooms[j].Title + "</h2>";
                            html += "<p></p>";
                            html += "</div> <!-- end .pricing-heading -->";
                            html += "<div class='pricing-content'>";
                            html += "<div class='pricing-tcontent'>";
                            html += "<ul class='pricing'>";
                            html += "<li>" + data.Items[i].Rooms[j].Description + "</li>";
                            html += "<li><span>" + data.Items[i].Rooms[j].NoOfBeds + " bed</span></li>";
                            var amn = data.Items[i].Rooms[j].Amenities;
                            for (var k = 0; k < amn.length; k++) {
                                html += "<li><span>" + amn[k] + "</span></li>";
                            }
                            html += "</ul>";
                            html += "<span class='et-price'><span class='dollar-sign'>$</span>" + data.Items[i].Rooms[j].Price + "<sup></sup></span>";
                            html += "</div> <!-- end .pricing-tcontent -->";
                            html += "</div> <!-- end .pricing-content -->";
                            html += "<a target='_blank' class='join-button' href='http://www.sivananda.org/wpcamp/?page_id=1243'><span>Book now</span></a>";
                            html += "</div> <!-- end .pricing-table -->";
                        }
                        html += "</div>";
                    }
                }

                $('#inbiz_widget_container').html(html);
            });
        });
    }

})();  // We call our anonymous function immediately
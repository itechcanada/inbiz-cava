﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Newtonsoft.Json;
using iTECH.InbizERP.BusinessLogic;

public partial class widget_data : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Clear();
        Response.ContentType = "application/json";        
        
        //Init All Available Accommodations
        Accommodation acc1 = new Accommodation
        {
            Title = "SPECIAL WEEKEND GETAWAY",
            Description = "Valid from 1st October to 31st May<br> Not valid for May 20-23 Victoria weekend when the Special is for three nights: $200 shared room and $225 single room",
            Rooms = new List<AccommodationItem> { 
                new AccommodationItem{ 
                    Title="Shared rooms", 
                    Description="from <strong>FRIDAY 3:00 pm</strong> to <strong>SUNDAY 3:00 pm</strong>",
                    ItemID = 1,
                    Amenities = new List<string>{"private bathroom", "ecological building"},
                    NoOfBeds = 1,
                    Price = 130.00D
                },
                new AccommodationItem{ 
                    Title="Single room", 
                    Description="from <strong>FRIDAY 3:00 pm</strong> to <strong>SUNDAY 3:00 pm</strong>",
                    ItemID = 1,
                    Amenities = new List<string>{"shared bathroom", "privacy"},
                    NoOfBeds = 1,
                    Price = 150.00D
                }
            },
            IsSpecial = true
        };

        Accommodation acc2 = new Accommodation
        {
            Title = "YEAR-ROUND ACCOMMODATION",
            Description = "<strong>Rooms in the Lodge</strong><br><a href='http://www.sivananda.org/wpcamp/wp-content/uploads/2010/10/lodge-mothers.jpg'><img height='75' width='114' alt='' src='http://www.sivananda.org/wpcamp/wp-content/uploads/2010/10/lodge-mothers.jpg' title='lodge mothers' style='margin: 5px;' class='size-full wp-image-812 alignleft'></a>The Lodge is in the same building as the Reception, the sauna and the dining room. It is a comfortable ecological building made of straw bales. The underfloor heating system diffuses heat evenly throughout. The building has a comfortable seating area and roof terraces overlooking the gardens where guests can enjoy the outdoors when the weather permits.<br>",
            Rooms = new List<AccommodationItem> { 
                new AccommodationItem{ 
                    Title="Shared rooms", 
                    Description="<span><strong>per night</strong></span>",
                    ItemID = 1,
                    Amenities = new List<string>{"private bathroom", "ecological building"},
                    NoOfBeds = 1,
                    Price = 80.00D
                },
                new AccommodationItem{ 
                    Title="Shared rooms", 
                    Description="<span><strong>per week</strong></span>",
                    ItemID = 1,
                    Amenities = new List<string>{"private bathroom", "ecological building"},
                    NoOfBeds = 1,
                    Price = 510.00D
                },
                new AccommodationItem{ 
                    Title="Shared rooms", 
                    Description="<span><strong>2 weeks</strong></span>",
                    ItemID = 1,
                    Amenities = new List<string>{"private bathroom", "ecological building"},
                    NoOfBeds = 1,
                    Price = 1010.00D
                }
            },
            IsSpecial = false
        }; 

        Dictionary<string, object> data = new Dictionary<string, object>();
        data["Title"] = "Accommodation";
        data["Description"] = "";
        data["Items"] = new List<Accommodation> { acc1, acc2 };
        Response.Write(string.Format("{0}({1})", Request.QueryString["jsoncallback"], JsonConvert.SerializeObject(data)));
        Response.Flush();
        Response.SuppressContent = true;
    }    
}
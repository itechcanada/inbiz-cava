﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Data;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class NewReport_printgrid : System.Web.UI.Page
{
    protected string SearchCretria;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            grdPrint.DataSource = Session["_rpt_export"];
            grdPrint.DataBind();
            //PrintHelper.PrintWebControl(grdPrint);
            reportTitle.Text = BusinessUtility.GetString(Session["_rpt_title"]);
            SearchCretria = "Search Cretria";

            if (Session["_rpt_paramCollect"] != null)
            {
                SearchCretria = getSerchParamList();
            }
        }
    }

    private string getSerchParamList()
    {
        DataTable dtsearchParam = (DataTable)Session["_rpt_paramCollect"];
        StringBuilder sbSearchParam = new StringBuilder();
        sbSearchParam.Append("<table border=0 class='SearchCretria' style='width:800px;'><tr><td>");
        sbSearchParam.Append("<table border=0 class='SearchCretria'>");
        int irow = 0;
        int iMaxTd = 3;
        int iTdCount = 0;

        sbSearchParam.Append("<tr>");
        sbSearchParam.Append("<td colspan='3'  class='Heading'");
        sbSearchParam.Append("<span > Search By </span> ");
        sbSearchParam.Append("</td>");
        sbSearchParam.Append("<td colspan='3'>");
        sbSearchParam.Append("</td>");
        sbSearchParam.Append("</tr>");

        foreach (DataRow drSearchParam in dtsearchParam.Rows)
        {
            if (irow == 0)
                sbSearchParam.Append("<tr>");


            irow += 1;
            if (irow <= iMaxTd)
            {
                sbSearchParam.Append("<td><b>" + BusinessUtility.GetString(drSearchParam["ParamName"]) + "</b></td>" + "<td>" + BusinessUtility.GetString(drSearchParam["value"]) + "</td>");
                iTdCount += 1;
            }
            else if (irow > iMaxTd)
            {
                sbSearchParam.Append("<td><b>" + BusinessUtility.GetString(drSearchParam["ParamName"]) + "</b></td>" + "<td>" + BusinessUtility.GetString(drSearchParam["value"]) + "</td>");
                iTdCount += 1;
            }

            if (iTdCount == iMaxTd)
            {
                sbSearchParam.Append("</tr>");
                iTdCount = 0;
                irow = 0;
            }
        }


        //if (Session["_rpt_groupby"] != null)
        //{
        //    sbSearchParam.Append("<tr>");
        //    sbSearchParam.Append("<td colspan='5'  class='Heading'>");
        //    sbSearchParam.Append("<span > Group By </span> ");
        //    sbSearchParam.Append("</td>");
        //    sbSearchParam.Append("</tr>");

        //    DataTable dtGroupBy = (DataTable)Session["_rpt_groupby"];
        //    string sGroupBy = "";
        //    foreach (DataRow drGroup in dtGroupBy.Rows)
        //    {
        //        if (sGroupBy != "")
        //            sGroupBy += ",";

        //        sGroupBy += BusinessUtility.GetString(drGroup["Headertext"]);
        //    }
        //    sbSearchParam.Append("<tr>");
        //    sbSearchParam.Append("<td colspan='5' >");
        //    sbSearchParam.Append(sGroupBy);
        //    sbSearchParam.Append("</td>");
        //    sbSearchParam.Append("</tr>");
        //}


        sbSearchParam.Append("</table>");
        sbSearchParam.Append("</td><td valign='top' align='right'>");
        sbSearchParam.Append("<b>Report Generation Date/Time : </b> " + BusinessUtility.GetDateTimeString(DateTime.Now, DateFormat.MMddyyyyHHmmss));
        sbSearchParam.Append("</td></tr>");
        sbSearchParam.Append("<table>");


        return sbSearchParam.ToString();
    }

    protected void grdPrint_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        foreach (TableCell cell in e.Row.Cells)
        {
            cell.Text = Server.HtmlDecode(cell.Text);
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="printgrid.aspx.cs" Inherits="NewReport_printgrid" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
      <%--<link href="../POS/Css/style.css" rel="stylesheet" type="text/css" /> --%>
    <script type="text/javascript">
        function printpage() {
            window.print();
        }
</script>
    <style type="text/css" >
        .GridViewStyle
        {
            font-family:Lucida Sans Unicode,Lucida Grande,sans-serif;
            font-size:13px;font-weight:400;font-style:normal;font-size-adjust:none;color:#515151;text-transform:none;text-decoration:none;letter-spacing:normal;word-spacing:0;
            line-height:25px;text-align:left;vertical-align:middle;direction:ltr;text-overflow:clip;
            border-bottom:2px solid Gray ;font: normal 11px Tahoma, Arial;font-size:9px;    
        }
        .GridViewStyle a
        {
            color: #FFFFFF;
        }
        
         .GridViewStyle tr td
        {
            border-left:1px solid Gray;
            text-align:left;border-right-width: 1px; border-right-color: Gray; border-right-style: solid;line-height:25px;font: normal 11px Tahoma, Arial;font-size:9px;    
        }

        .GridViewHeaderStyle th
        {
            font-family:Lucida Sans Unicode,Lucida Grande,sans-serif;font-size:13px;font-weight:700;font-style:normal;font-size-adjust:none;color:#FFFEFD;text-transform:none;text-decoration:none;letter-spacing:normal;
            word-spacing:0;line-height:18px;text-align:center;vertical-align:middle;direction:ltr;text-overflow:clip;background-color:#787C7D;background-image:url("http://localhost:50206/InbizWebDvlp/lib/scripts/jquery.jqGrid-4.0.0/css/bck_SubTable.jpg");
            background-repeat:repeat-x;background-position:50% 100%;background-attachment:scroll;opacity:1;height:26px;top:auto;right:auto;bottom:auto;left:auto;margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;
            padding-top:0;padding-right:2px;padding-bottom:0;padding-left:2px;border-top-width:0;border-right-width:1px;border-bottom-width:0;border-left-width:0;border-top-color:#FFFEFD;border-right-color:#CCCCCC;border-bottom-color:#FFFEFD;
            border-left-color:#FFFEFD;border-top-style:none;border-right-style:solid;border-bottom-style:none;border-left-style:none;box-shadow:none;position:static;display:table-cell;visibility:visible;z-index:auto;overflow-x:hidden;
            overflow-y:hidden;white-space:nowrap;clip:auto;float:none;clear:none;cursor:auto;list-style-image:none;list-style-position:outside;list-style-type:disc;marker-offset:auto;
            font: normal 11px Tahoma, Arial; font-size:9px;    
        }
        .GridViewHeaderStyle
        {}
        
        .GridViewFooterStyle
        {
            background-color: #5D7B9D;
            font-weight: bold;
            color: White;
        }
        
        .GridViewRowStyle
        {
          background-color:#eee;color:#000;
        }
        
        .GridViewAlternatingRowStyle
        {
             background-color:#fff;color:#000;
        }
        
       .GridViewRowStyle td, .GridViewAlternatingRowStyle td
        {
            border: 1px solid #EBE9ED;
            
        }
        
        .GridViewSelectedRowStyle
        {
            background-color: #E2DED6;
            font-weight: bold;
            color: #333333;
            
        }
        
        .GridViewPagerStyle
        {
            background-color: #284775;
            color: #FFFFFF;
            border-top:1px solid Gray;
        }
        
        .GridViewPagerStyle table /* to center the paging links*/
        {
            margin: 0 auto 0 auto;
        }
        
        .GridViewStyle td, .GridViewStyle th
        {
        padding:2px;
        }
        
        .ReplortHeader
        {
           font-family:Lucida Sans Unicode,Lucida Grande,sans-serif;font-size:13px;font-weight:700;font-style:normal;font-size-adjust:none;color;#FFFEFD;text-transform:none;text-decoration:none;letter-spacing:normal;
           word-spacing:0;line-height:18px;text-align:start;vertical-align:baseline;direction:ltr;text-align:center;text-overflow:clip;background-color:#304467;background-image:url("http://localhost:50206/InbizWebDvlp/lib/css/inbiz/images/ui-bg_highlight-soft_25_304467_1x100.png");
           background-repeat:repeat-x;background-position:50% 50%;background-attachment:scroll;opacity:1;height:28.6667px;top:auto;right:auto;bottom:auto;left:auto;margin-top:0;margin-right:0;margin-bottom:5px;
           margin-left:0;padding-top:2px;padding-right:2px;padding-bottom:2px;padding-left:2px;border-top-width:1px;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;border-top-color:#DBDDDD;border-right-color:#DBDDDD;
           border-bottom-color:#DBDDDD;border-left-color:#DBDDDD;border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;box-shadow:none;position:static;display:block;visibility:visible;
           z-index:auto;overflow-x:visible;overflow-y:visible;white-space:normal;clip:auto;float:none;clear:none;cursor:auto;list-style-image:none;list-style-position:outside;list-style-type:disc;marker-offset:auto;font: normal 11px Tahoma, Arial;font-size:9px;    
        }
        .Heading
        {
           font-family:Lucida Sans Unicode,Lucida Grande,sans-serif;font-size:13px;font-weight:700;font-style:normal;font-size-adjust:none;color:#FFFEFD;text-transform:none;text-decoration:none;letter-spacing:normal; font: normal 11px Tahoma, Arial;font-size:9px;    
        }
        .SearchCretria
        {
            font-family:Lucida Sans Unicode,Lucida Grande,sans-serif;font-size:13px;font-style:normal;font-size-adjust:none;color:#FFFEFD;text-transform:none;text-decoration:none;letter-spacing:normal;font: normal 11px Tahoma, Arial;font-size:9px;    
        }
        
        
        body
{
    font: normal 11px Tahoma, Arial;
	color: black;
	font-size:9px;    
}
        
    </style>



</head>
<body onload="printpage()" >
    <form id="form1" runat="server" >
    <div >
    <div id="toolbar" class="ReplortHeader"  >
        <div style="text-align:center; padding: 5px;">
            <asp:Label ID="reportTitle" Text="" Font-Bold="true" runat="server" />
        </div>
    </div>
    <div style="">
    <%=SearchCretria %>
    </div>
    <div style="padding:5px;padding-right:5px;padding-left:5px;">
        <asp:GridView ID="grdPrint" runat="server"  CssClass="GridViewStyle" GridLines="Vertical" EnableTheming="true" OnRowDataBound="grdPrint_OnRowDataBound" Width="100%">
        <HeaderStyle CssClass="GridViewRowStyle"  />
        <AlternatingRowStyle CssClass="GridViewAlternatingRowStyle" />
        <RowStyle  CssClass="GridViewRowStyle"/>
        </asp:GridView>
    </div>
    
    </div>
    </form>

    
    
</body>
</html>


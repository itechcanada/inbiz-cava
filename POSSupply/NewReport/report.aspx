﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="report.aspx.cs" Inherits="NewReport_report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <style type="text/css">
        .evenTableRow
        {
            background-color: #eee;
            color: #000;
        }
        .oddTableRow
        {
            background-color: #fff;
            color: #000;
        }
        
        .ui-jqgrid tr.jqgrow td
        {
            font-weight: normal;
            overflow: hidden;
            white-space: pre;
            height: 22px;
            padding: 0 2px 0 2px;
            border-bottom-width: 0px;
            border-bottom-color: inherit;
            border-bottom-style: solid;
        }
        .ui-jqgrid tr.ui-row-ltr td
        {
            text-align: left;
            border-right-width: 1px;
            border-right-color: Gray;
            border-right-style: solid;
            line-height: 25px;
        }
    </style>
    <script type="text/javascript">
        var isValidateForm = true;
        (function ($) {
            $.fn.extend({
                initReportGridHelper: function (options) {
                    var defaults = {
                        searchPanelID: "pnlSearchID",
                        searchButtonID: "btnSearch",
                        gridWrapPanleID: "grid_wrapper",
                        fixedSearchOptions: {}
                    };
                    var options = $.extend(defaults, options);

                    var $grid = jQuery(this);
                    var $srcPnl = jQuery("#" + options.searchPanelID);
                    var $btnSrc = jQuery("#" + options.searchButtonID);

                    /*setTimeout(function () {
                    $grid.fluidGrid({ example: "#" + options.gridWrapPanleID, offset: options.resizeOffset });
                    $grid.setGridParam({
                    loadComplete: function (data) {
                    $grid.fluidGrid({ example: "#" + options.gridWrapPanleID, offset: options.resizeOffset });
                    }
                    })
                    }, 1000);*/

                    $srcPnl.keydown(function (e) {
                        if (e.keyCode == 13) {
                            $btnSrc.trigger("click");
                            return false;
                        }
                    });

                    $btnSrc.click(function () {
                        isValidateForm = false;
                        $(".cssreq", $srcPnl).each(function () {
                            if ($(this).val() == "") {
                                alert('Required ' + $(this).attr('txttitle'));
                                isValidateForm = true;
                                return;
                            }
                        });

                        if (isValidateForm == true) {
                            isValidateForm = false;
                            return;
                        }
                        var filterArr = {};
                        var iCount = 0;
                        $(".filter-key", $srcPnl).each(function () {
                            var ele = document.getElementById($(this).attr('for'));
                            if ($('#' + $(this).attr('for')).is('input[type=text]')) {
                                //filterArr[$(ele).attr("filterKey")] = ele.value;  
                                filterArr[$(ele).attr("id")] = ele.value;
                                iCount++;
                            }
                            else if ($('#' + $(this).attr('for')).is('select')) {
                                //filterArr[$(ele).attr("filterKey")] = ele.value;
                                //filterArr[$(ele).attr("id")] = ele.value;
                                var sSelectedValue = ''
                                $("select#" + $(this).attr('for') + " option:selected").each(function () {
                                    var val = $(this).val();
                                    if (val != 'undefined') {
                                        if (sSelectedValue == '') {
                                            sSelectedValue = val;
                                        }
                                        else {
                                            sSelectedValue += "," + val;
                                        }
                                    }
                                });
                                if (sSelectedValue == 'undefined') {
                                    sSelectedValue = '';
                                }
                                //                        alert(sSelectedValue);
                                filterArr[ele.id] = sSelectedValue;
                                iCount++;
                            }
                            else if ($('#' + $(this).attr('for')).is('input[type=radio]')) {
                                if (ele.checked) {
                                    //filterArr[$(ele).attr("filterKey")] = ele.value;
                                    filterArr[$(ele).attr("id")] = ele.value;
                                    iCount++;
                                }
                            }
                        });
                        filterArr["_history"] = "0";
                        $grid.appendPostData(filterArr);
                        $grid.trigger("reloadGrid", [{ page: 1}]);

                        return false;
                    });

                    $(window).resize(function () {
                        $grid.fluidGrid({ example: "#" + options.gridWrapPanleID, offset: options.resizeOffset });
                    });
                },
                jqRptResizeAfterLoad: function (gridWrapPanleID, resizeOffset) {
                    jQuery(this).fluidGrid({ example: "#" + gridWrapPanleID, offset: resizeOffset });
                },
                jqRptCustomSearch: function (objPostDictionaryData) {
                    jQuery(this).appendPostData(objPostDictionaryData);
                    jQuery(this).trigger("reloadGrid", [{ page: 1}]);
                }
            });
        })(jQuery);
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphLeft" runat="Server">
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="searchBar">
            <div class="header">
                <div class="title">
                    Search</div>
                <div class="icon">
                    <img alt="" src="../lib/image/iconSearch.png" style="width: 17px" /></div>
            </div>
            <asp:PlaceHolder ID="phSearchFilters" runat="server"></asp:PlaceHolder>
            <%--<asp:Repeater ID="rptSearchForm" runat="server">
                <ItemTemplate>
                    <h4>
                        <asp:Label ID="lblFieldTitle" CssClass="filter-key" runat="server" Text="Shipment Order From"
                            AssociatedControlID="txtFromDate"></asp:Label>
                    </h4>
                    <div class="inner">                        
                        <asp:TextBox ID="txtValue" runat="server" />
                        <asp:DropDownList ID="ddlValue" runat="server">                            
                        </asp:DropDownList>
                    </div>
                </ItemTemplate>
            </asp:Repeater>--%>
            <div class="footer">
                <div class="submit">
                    <asp:Button ID="btnSearch" Text="<%$Resources:Resource, Search%>" runat="server" />
                    <%--<input id="btnSearch" type="button" value="Search" />--%>
                </div>
                <br />
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <div id="toolbar" class="ui-widget-header ui-corner-all" style="margin-bottom: 5px;
                padding: 2px;">
                <div style="float: left; padding: 5px;">
                    <asp:Label ID="reportTitle" Text="" Font-Bold="true" ForeColor="#ffffff" runat="server" />
                </div>
                <div style="float: right; text-align: right;">
                    <asp:PlaceHolder ID="phCommands" runat="server"></asp:PlaceHolder>
                </div>
                <div style="clear: both">
                </div>
            </div>
            <%-- <div style="text-align: right; background-image: url(../lib/image/bck_menu.jpg);
                background-repeat: repeat-x; padding: 2px 0px; margin-bottom: 5px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="text-align: left; padding-left:12px;">
                            <asp:Label ID="reportTitle" Text="" Font-Bold="true" ForeColor="#ffffff" runat="server" />
                        </td>
                        <td>
                            <asp:PlaceHolder ID="phCommands" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </div>--%>
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="grdReport" Height="530px" AutoWidth="True" OnDataRequesting="grdReport_DataRequesting"
                    OnCellBinding="grdReport_CellBinding" OnDataRequested="grdReport_DataRequested">
                    <PagerSettings PageSize="100" PageSizeOptions="[100,150,200]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" />
                    <ClientSideEvents LoadComplete="loadComplete" BeforeRowSelect="returnFalse" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sdsReport" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $grid = $("#<%=grdReport.ClientID%>");
        $grid.initReportGridHelper({
            searchPanelID: "<%=pnlSearch.ClientID %>",
            searchButtonID: "<%=btnSearch.ClientID %>",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $grid.jqRptResizeAfterLoad("grid_wrapper", 0);
        }


        function loadComplete() {
            getGlobalMessage();
            jqGridResize();
            $('tr', $grid).removeClass("ui-widget-content");
            $('tr:nth-child(even)', $grid).addClass("evenTableRow");
            $('tr:nth-child(odd)', $grid).addClass("oddTableRow");
        }

        function customReportSorting(source) {
            var sortingData = {};
            sortingData.rptSortBy = $(source).val();
            $grid.appendPostData(sortingData);
            $grid.trigger("reloadGrid", [{ page: 1}]);
        }

        function customSortOrder(source) {
            var sortingData = {};
            sortingData.rptSortOrder = $(source).val();
            $grid.appendPostData(sortingData);
            $grid.trigger("reloadGrid", [{ page: 1}]);
        }

        function emailReportPopup() {
            var $dialog = jQuery.FrameDialog.create({
                url: "MailReport.aspx",
                title: "Email Report",
                loadingClass: "loading-image",
                modal: true,
                width: "550",
                height: "350",
                autoOpen: false
            });
            $dialog.dialog('open');
            return false;
        }

        function returnFalse() {
            return false;
        }

//        $(document).ready(function () {
//            alert("ram");
//            $('#<%=btnSearch.ClientID%>').trigger('click');
//        });
    </script>
</asp:Content>

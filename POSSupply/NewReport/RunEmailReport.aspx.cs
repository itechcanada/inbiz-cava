﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

using iTECH.Library.Utilities;
using iCtrl = iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;


using Trirand.Web.UI.WebControls;

public partial class NewReport_RunEmailReport : System.Web.UI.Page
{
    DataSet _reportDateSet = null;
    string _reportSql = "";
    bool _reportHasLanguageParameter = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        string reqPassword = Request.QueryString["ReportPassword"];
        string resPassword = System.Configuration.ConfigurationManager.AppSettings["ReportPassword"];
        if (string.IsNullOrEmpty(reqPassword) || !reqPassword.Equals(resPassword))
        {
            return;
        }

        if (this.ReportXmlPath == string.Empty)
        {
            return;
        }
        if (!File.Exists(Server.MapPath(this.ReportXmlPath)))
        {
            return;
        }
        _reportDateSet = new DataSet("ReportDataSource");
        _reportDateSet.ReadXml(Server.MapPath(this.ReportXmlPath));
        DataRow reportSettingRow = null;
        if (_reportDateSet.Tables["ReportSetting"] != null)
        {
            reportSettingRow = _reportDateSet.Tables["ReportSetting"].Rows[0];
            _reportHasLanguageParameter = BusinessUtility.GetBool(reportSettingRow["HasLanguageParameter"]);
            _reportSql = BusinessUtility.GetString(reportSettingRow["ReportSql"]);
        }
   

        if (_reportDateSet != null)
        {
            //Set connection to datasource if available
            if (_reportDateSet.Tables.Contains("Connection"))
            {
                DataTable dtConnection = _reportDateSet.Tables["Connection"];
                string conString = string.Empty;
                string conProvider = string.Empty;
                if (dtConnection.Rows.Count > 0)
                {
                    conString = BusinessUtility.GetString(dtConnection.Rows[0]["ConnectionString"]);
                    conProvider = BusinessUtility.GetString(dtConnection.Rows[0]["providerName"]);
                }
                if (!string.IsNullOrEmpty(conString))
                {
                    sdsReport.ConnectionString = conString;
                }
                if (!string.IsNullOrEmpty(conProvider))
                {
                    sdsReport.ProviderName = conProvider;
                }
            }
           
            List<object> lstParams = new List<object>();
            DataTable filterParams = _reportDateSet.Tables["ReportSqlParameter"];                        
            int iParamCounter = 1;
            foreach (DataRow row in filterParams.Rows)
            {
                string reqVal = Request.QueryString[string.Format("param_{0}", iParamCounter)];
                string valFormat = BusinessUtility.GetString(row["ValueFormatString"]);
                if (valFormat.Length <= 0)
                {
                    valFormat = "{0}";
                }

                if (!string.IsNullOrEmpty(reqVal))
                {
                    switch (BusinessUtility.GetString(row["Type"]).ToUpper())
                    {
                        case "DATE":
                            DateTime dtVal = BusinessUtility.GetDateTime(reqVal, DateFormat.MMddyyyy);
                            reqVal = string.Format(valFormat, dtVal.Date.ToString("yyyy-MM-dd HH:mm:ss").Replace("00:00:00", string.Empty));
                            break;
                    }

                    sdsReport.SelectParameters.Add("@" + BusinessUtility.GetString(row["Name"]), reqVal);
                }
                else
                {
                    reqVal = BusinessUtility.GetString(row["DefaultValue"]);
                    switch (BusinessUtility.GetString(row["Type"]).ToUpper())
                    {
                        case "DATE":
                            DateTime dtVal = this.GetRequestedDate(reqVal);
                            reqVal = string.Format(valFormat, dtVal.Date.ToString("yyyy-MM-dd HH:mm:ss").Replace("00:00:00", string.Empty));
                            break;
                    }

                    sdsReport.SelectParameters.Add("@" + BusinessUtility.GetString(row["Name"]), reqVal);
                }
                iParamCounter++;
            }
            if (_reportHasLanguageParameter)
            {
                sdsReport.SelectParameters.Add("@Language", "en");
            }

            sdsReport.SelectCommand = _reportSql;

            DataView dvUnGroupd = (DataView)sdsReport.Select(DataSourceSelectArguments.Empty);
            string sortCol = Request.QueryString.AllKeys.Contains("rptSortBy") ? Request.QueryString["rptSortBy"] : "";
            string sortBy = Request.QueryString.AllKeys.Contains("rptSortOrder") ? Request.QueryString["rptSortOrder"] : "ASC";
            if (sortCol.Length > 0)
            {
                dvUnGroupd.Sort = string.Format("{0} {1}", sortCol, sortBy);
            }

            //grdReport.DataSource = dvUnGroupd;
            //Session["REPORT_DATA_SESSION_TO_EMAIL"]
            var data = this.PerformGroupingAndSorting(dvUnGroupd);

            if (data.Rows.Count <= 0)
            {
                return;
            }
            string fileName = Server.MapPath("~/Upload/ReportExcel/");
            fileName += BusinessUtility.GenerateRandomString(5, true);
            fileName += ".xls";

            try
            {
                string emailsTo = Request.QueryString["emailTo"];
                string sub = Request.QueryString["sub"];
                string msg = Request.QueryString["msg"];
                if (CommonReport.CreateReportWorkBook(data, fileName))
                {
                    CommonReport.SendReport(emailsTo, sub, msg, fileName);                    
                }
            }
            catch 
            {                
            }
        }
    }     

    private string ReportXmlPath
    {
        get
        {
            string xmlFile = Request.QueryString["rpt"];
            if (!string.IsNullOrEmpty(xmlFile))
            {
                return string.Format("~/NewReport/xml/{0}", xmlFile);
            }
            return string.Format("~/NewReport/xml/{0}", "nofile.xml");
            //return CommonReport.GetReportConfigurationXmlPath(this.CurrentReport);            
        }
    }

    private ReportKey CurrentReport
    {
        get
        {
            int id = BusinessUtility.GetInt(Request.QueryString["rpt"]);
            if (Enum.IsDefined(typeof(ReportKey), id))
            {
                return (ReportKey)id;
            }
            return ReportKey.Default;
        }
    }

    private Trirand.Web.UI.WebControls.TextAlign GetTextAlignment(string str)
    {
        switch (str.ToLower().Trim())
        {
            case "center":
                return Trirand.Web.UI.WebControls.TextAlign.Center;
            case "right":
                return Trirand.Web.UI.WebControls.TextAlign.Right;
            case "left":
            default:
                return Trirand.Web.UI.WebControls.TextAlign.Left;
        }
    }

    private DateTime GetRequestedDate(string key)
    {
        switch (key)
        {
            case "TODAY":
                return DateTime.Today;
            case "NEXT_DAY":
                return DateTime.Today.AddDays(1);
            case "NEXT_WEEK":
                return DateTime.Today.AddDays(7);
            case "NEXT_2WEEK":
                return DateTime.Today.AddDays(14);
            case "PREVIOUS_WEEK":
                return DateTime.Today.AddDays(-7);
            case "PREVIOUS_WEEK_MON":
                DateTime dtMonday = DateTime.Today.AddDays(-7);
                while (dtMonday.DayOfWeek != DayOfWeek.Monday)
                {
                    dtMonday = dtMonday.AddDays(-1);
                }
                return dtMonday;
            case "PREVIOUS_WEEK_SUN":
                DateTime dtSunDay = DateTime.Today.AddDays(-7);
                while (dtSunDay.DayOfWeek != DayOfWeek.Sunday)
                {
                    dtSunDay = dtSunDay.AddDays(1);
                }
                return dtSunDay;
            case "MONTH_FIRST_DAY":
                return new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            case "MONTH_LAST_DAY":
                return new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1);
            case "PRE_MONTH_FIRST_DAY":
                return new DateTime(DateTime.Today.AddMonths(-1).Year, DateTime.Today.AddMonths(-1).Month, 1);
            case "PRE_MONTH_LAST_DAY":
                return new DateTime(DateTime.Today.AddMonths(-1).Year, DateTime.Today.AddMonths(-1).Month, 1).AddMonths(1).AddDays(-1);
            default:
                return DateTime.Today;
        }
    }

    private List<string> GetGroupColumns()
    {
        DataTable dt = _reportDateSet.Tables["ReportSetting"];
        if (dt != null && dt.Rows.Count > 0 && dt.Columns.Contains("GroupColumns"))
        {
            string str = BusinessUtility.GetString(dt.Rows[0]["GroupColumns"]);
            return new List<string>(str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
        }
        return new List<string>();
    }

    private List<string> GetSubTotalColumns()
    {
        DataTable dt = _reportDateSet.Tables["ReportSetting"];
        if (dt != null && dt.Rows.Count > 0 && dt.Columns.Contains("SubTotalColumns"))
        {
            string str = BusinessUtility.GetString(dt.Rows[0]["SubTotalColumns"]);
            return new List<string>(str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
        }
        return new List<string>();
    }

    private List<string> GetTotalColumns()
    {
        DataTable dt = _reportDateSet.Tables["ReportSetting"];
        if (dt != null && dt.Rows.Count > 0 && dt.Columns.Contains("TotalColumns"))
        {
            string str = BusinessUtility.GetString(dt.Rows[0]["TotalColumns"]);
            return new List<string>(str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
        }
        return new List<string>();
    }

    private string GetTotalLegendColumn()
    {
        DataTable dt = _reportDateSet.Tables["ReportSetting"];
        if (dt != null && dt.Rows.Count > 0 && dt.Columns.Contains("TotalLegendColumn"))
        {
            return BusinessUtility.GetString(dt.Rows[0]["TotalLegendColumn"]);
        }
        return string.Empty;
    }

    private string GetReportCountColumn()
    {
        DataTable dt = _reportDateSet.Tables["ReportSetting"];
        if (dt != null && dt.Rows.Count > 0 && dt.Columns.Contains("ShowRecordsCountColumn"))
        {
            return BusinessUtility.GetString(dt.Rows[0]["ShowRecordsCountColumn"]);
        }
        return string.Empty;
    }

    private string GetSubTotalLegend()
    {
        DataTable dt = _reportDateSet.Tables["ReportSetting"];
        if (dt != null && dt.Rows.Count > 0 && dt.Columns.Contains("SubTotalLegend"))
        {
            return BusinessUtility.GetString(dt.Rows[0]["SubTotalLegend"]);
        }
        return "Sub Total";
    }

    private DataTable PerformGroupingAndSorting(DataView unSortedData)
    {
        DataTable dtSource = unSortedData.ToTable();
        DataTable dtDest = dtSource.Clone();
        DataTable reportColumns = _reportDateSet.Tables["ReportColumn"];
        DataTable dtNoRepeat = _reportDateSet.Tables["NoRepeatColumns"];


        //Make all destination column type of string
        foreach (DataColumn item in dtDest.Columns)
        {
            item.DataType = typeof(string);
        }

        string totalLegendColumn = this.GetTotalLegendColumn();
        string compositKey = string.Empty;
        List<string> grpColumns = this.GetGroupColumns();
        List<string> sTotalColumns = this.GetSubTotalColumns();
        List<string> totalColumns = this.GetTotalColumns();
        string rptCountColumn = this.GetReportCountColumn();

        bool addSubTotalRows = sTotalColumns.Count > 0;
        bool addTotalRow = totalColumns.Count > 0;

        Dictionary<string, double> subTotal = new Dictionary<string, double>(); //SubTotal Column Name, sub Total
        Dictionary<string, double> total = new Dictionary<string, double>(); //Total Column Name, Total

        if (grpColumns.Count > 0 || sTotalColumns.Count > 0 || totalColumns.Count > 0)
        {
            int iCounter = 0;
            List<string> currentCompositKeys;
            string formatedCompostiKey = string.Empty;
            foreach (DataRow row in dtSource.Rows)
            {
                //Invoke Methods & Set Data to original table
                this.InvokeMethodsAndSetData(row);

                currentCompositKeys = new List<string>();
                foreach (var key in grpColumns)
                {
                    currentCompositKeys.Add(BusinessUtility.GetString(row[key]));
                }

                if (currentCompositKeys.Count <= 0)
                {
                    currentCompositKeys.Add(iCounter.ToString());
                }
                formatedCompostiKey = string.Join("_", currentCompositKeys.ToArray());

                //Add total it is not dependednt on groupping
                foreach (var tCol in totalColumns)
                {
                    if (total.Keys.Contains(tCol))
                    {
                        total[tCol] += BusinessUtility.GetDouble(row[tCol]);
                    }
                    else
                    {
                        total[tCol] = BusinessUtility.GetDouble(row[tCol]);
                    }
                }

                if (formatedCompostiKey != compositKey)
                {
                    //Initialize sub total if not exists 
                    foreach (var sCol in sTotalColumns)
                    {
                        if (!subTotal.Keys.Contains(sCol))
                        {
                            subTotal[sCol] = sCol.ToLower() == rptCountColumn.ToLower() ? 1.0D : BusinessUtility.GetDouble(row[sCol]);
                        }
                    }

                    //Add First Row And Last Row
                    if (iCounter == 0 && iCounter == dtSource.Rows.Count - 1)
                    {
                        dtDest.Rows.Add(row.ItemArray);
                        DataRow lstrow = dtDest.Rows[dtDest.Rows.Count - 1];
                        //format added data
                        string format = string.Empty;
                        foreach (DataRow rptRow in reportColumns.Rows)
                        {
                            format = string.Empty;
                            format = BusinessUtility.GetString(rptRow["DataFormat"]);
                            if (!string.IsNullOrEmpty(format))
                            {
                                lstrow[BusinessUtility.GetString(rptRow["DataField"])] = string.Format(format, row[BusinessUtility.GetString(rptRow["DataField"])]);
                            }
                        }

                        //Add Sub total Row
                        if (addSubTotalRows)
                        {
                            DataRow sTotalRow = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                sTotalRow[totalLegendColumn] = this.GetSubTotalText();
                            }
                            foreach (var sCol in sTotalColumns)
                            {
                                sTotalRow[sCol] = this.GetSummaryTotal(subTotal[sCol]);
                            }
                            dtDest.Rows.Add(sTotalRow);
                        }

                        //Add Total Row
                        if (addTotalRow)
                        {
                            DataRow totalRow = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                totalRow[totalLegendColumn] = this.GetTotalText();
                            }
                            foreach (var sCol in totalColumns)
                            {
                                totalRow[sCol] = this.GetSummaryTotal(total[sCol]);
                            }
                            dtDest.Rows.Add(totalRow);
                        }
                    }
                    else if (iCounter == 0)
                    {
                        dtDest.Rows.Add(row.ItemArray);
                        DataRow lstrow = dtDest.Rows[dtDest.Rows.Count - 1];

                        //format added data
                        string format = string.Empty;
                        foreach (DataRow rptRow in reportColumns.Rows)
                        {
                            format = string.Empty;
                            format = BusinessUtility.GetString(rptRow["DataFormat"]);
                            if (!string.IsNullOrEmpty(format))
                            {
                                lstrow[BusinessUtility.GetString(rptRow["DataField"])] = string.Format(format, row[BusinessUtility.GetString(rptRow["DataField"])]);
                            }
                        }

                        //Check If no Repead
                        this.ValidateAndStoreInNottoDuplicat(dtNoRepeat, lstrow);
                    }
                    else if (iCounter == dtSource.Rows.Count - 1)
                    {
                        //Add sub Total
                        if (addSubTotalRows)
                        {
                            DataRow sTotalRow = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                sTotalRow[totalLegendColumn] = this.GetSubTotalText();
                            }
                            foreach (var sCol in sTotalColumns)
                            {
                                sTotalRow[sCol] = this.GetSummaryTotal(subTotal[sCol]);
                            }
                            dtDest.Rows.Add(sTotalRow);
                        }
                        //Temp Reset Sub Total
                        foreach (var sCol in sTotalColumns)
                        {
                            subTotal.Remove(sCol);
                        }

                        //Add Last Row
                        dtDest.Rows.Add(row.ItemArray);
                        DataRow lstrow = dtDest.Rows[dtDest.Rows.Count - 1];
                        //format added data
                        string format = string.Empty;
                        foreach (DataRow rptRow in reportColumns.Rows)
                        {
                            format = string.Empty;
                            format = BusinessUtility.GetString(rptRow["DataFormat"]);
                            if (!string.IsNullOrEmpty(format))
                            {
                                lstrow[BusinessUtility.GetString(rptRow["DataField"])] = string.Format(format, row[BusinessUtility.GetString(rptRow["DataField"])]);
                            }
                        }

                        //Check If no Repead
                        this.ValidateAndStoreInNottoDuplicat(dtNoRepeat, lstrow);

                        //Again need to add sub total
                        //Add Sub Total
                        foreach (var sCol in sTotalColumns)
                        {
                            if (subTotal.Keys.Contains(sCol))
                            {
                                subTotal[sCol] += sCol.ToLower() == rptCountColumn.ToLower() ? 1.0D : BusinessUtility.GetDouble(row[sCol]);
                            }
                            else
                            {
                                subTotal[sCol] = sCol.ToLower() == rptCountColumn.ToLower() ? 1.0D : BusinessUtility.GetDouble(row[sCol]);
                            }
                        }

                        //Add another subtotal row
                        if (addSubTotalRows)
                        {
                            DataRow sTotalRowLast = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                sTotalRowLast[totalLegendColumn] = this.GetSubTotalText();
                            }
                            foreach (var sCol in sTotalColumns)
                            {
                                sTotalRowLast[sCol] = this.GetSummaryTotal(subTotal[sCol]);
                            }
                            dtDest.Rows.Add(sTotalRowLast);
                        }

                        //Add Finally Total Row
                        if (addTotalRow)
                        {
                            DataRow totalRow = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                totalRow[totalLegendColumn] = this.GetTotalText();
                            }
                            foreach (var sCol in totalColumns)
                            {
                                totalRow[sCol] = this.GetSummaryTotal(total[sCol]);
                            }
                            dtDest.Rows.Add(totalRow);
                        }
                    }
                    else
                    {
                        //Add Sub Total Row
                        if (addSubTotalRows)
                        {

                            DataRow sTotalRow = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                sTotalRow[totalLegendColumn] = this.GetSubTotalText();
                            }
                            foreach (var sCol in sTotalColumns)
                            {
                                sTotalRow[sCol] = this.GetSummaryTotal(subTotal[sCol]);
                            }
                            dtDest.Rows.Add(sTotalRow);
                        }

                        //Add New Row
                        dtDest.Rows.Add(row.ItemArray);
                        DataRow lstrow = dtDest.Rows[dtDest.Rows.Count - 1];
                        //format added data
                        string format = string.Empty;
                        foreach (DataRow rptRow in reportColumns.Rows)
                        {
                            format = string.Empty;
                            format = BusinessUtility.GetString(rptRow["DataFormat"]);
                            if (!string.IsNullOrEmpty(format))
                            {
                                lstrow[BusinessUtility.GetString(rptRow["DataField"])] = string.Format(format, row[BusinessUtility.GetString(rptRow["DataField"])]);
                            }
                        }

                        //Check If no Repead
                        this.ValidateAndStoreInNottoDuplicat(dtNoRepeat, lstrow);

                        //Reset Sub Total
                        foreach (var sCol in sTotalColumns)
                        {
                            subTotal.Remove(sCol);
                        }

                        //Add current to calculate sub total for next 
                        foreach (var sCol in sTotalColumns)
                        {
                            if (!subTotal.Keys.Contains(sCol))
                            {
                                subTotal[sCol] = sCol.ToLower() == rptCountColumn.ToLower() ? 1.0D : BusinessUtility.GetDouble(row[sCol]);
                            }
                        }
                    }
                }
                else
                {
                    //Add Row to destination & Hide data in rows to perform grouping                     
                    dtDest.Rows.Add(row.ItemArray);
                    DataRow lstrow = dtDest.Rows[dtDest.Rows.Count - 1];
                    foreach (var gCol in grpColumns)
                    {
                        lstrow[gCol] = string.Empty;
                    }

                    //format added data
                    string format = string.Empty;
                    foreach (DataRow rptRow in reportColumns.Rows)
                    {
                        format = string.Empty;
                        format = BusinessUtility.GetString(rptRow["DataFormat"]);
                        if (!string.IsNullOrEmpty(format))
                        {
                            lstrow[BusinessUtility.GetString(rptRow["DataField"])] = string.Format(format, row[BusinessUtility.GetString(rptRow["DataField"])]);
                        }
                    }

                    //Check If no Repead
                    this.ValidateAndStoreInNottoDuplicat(dtNoRepeat, lstrow);

                    //Add sub total & total
                    foreach (var sCol in sTotalColumns)
                    {
                        if (subTotal.Keys.Contains(sCol))
                        {
                            subTotal[sCol] += sCol.ToLower() == rptCountColumn.ToLower() ? 1.0D : BusinessUtility.GetDouble(row[sCol]);
                        }
                        else
                        {
                            subTotal[sCol] = sCol.ToLower() == rptCountColumn.ToLower() ? 1.0D : BusinessUtility.GetDouble(row[sCol]);
                        }
                    }

                    if (iCounter == dtSource.Rows.Count - 1)
                    {
                        if (addSubTotalRows)
                        {
                            DataRow sTotalRowLast = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                sTotalRowLast[totalLegendColumn] = this.GetSubTotalText();
                            }
                            foreach (var sCol in sTotalColumns)
                            {
                                sTotalRowLast[sCol] = this.GetSummaryTotal(subTotal[sCol]);
                            }
                            dtDest.Rows.Add(sTotalRowLast);
                        }

                        //Add Finally Total Row
                        if (addTotalRow)
                        {
                            DataRow totalRow = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                totalRow[totalLegendColumn] = this.GetTotalText();
                            }
                            foreach (var sCol in totalColumns)
                            {
                                totalRow[sCol] = this.GetSummaryTotal(total[sCol]);
                            }
                            dtDest.Rows.Add(totalRow);
                        }
                    }
                }

                compositKey = formatedCompostiKey;
                iCounter++;
            }

            return dtDest;
        }
        else
        {
            foreach (DataRow row in dtSource.Rows)
            {
                this.InvokeMethodsAndSetData(row);
            }

            return dtSource;
        }
    }

    private void ValidateAndStoreInNottoDuplicat(DataTable dtNotToDuplicat, DataRow newRow)
    {
        if (dtNotToDuplicat != null && dtNotToDuplicat.Rows.Count > 0 && newRow != null)
        {
            string[] keyColumns;
            string dataColumn;
            string previousRowValue;
            string previousRowValuFormat;
            List<string> lstData;
            foreach (DataRow item in dtNotToDuplicat.Rows)
            {
                lstData = new List<string>();
                keyColumns = BusinessUtility.GetString(item["KeyColumns"]).Split(',');
                dataColumn = BusinessUtility.GetString(item["DataColumn"]);
                previousRowValue = BusinessUtility.GetString(item["PreviousRowValue"]);
                previousRowValuFormat = BusinessUtility.GetString(item["PreviousRowValueFormat"]);

                foreach (var key in keyColumns)
                {
                    lstData.Add(BusinessUtility.GetString(newRow[key]));
                }
                //Set Current Value to Previous Row Data
                item["PreviousRowValue"] = string.Format(previousRowValuFormat, lstData.ToArray());

                //Set Empty Data
                if (previousRowValue.Equals(string.Format(previousRowValuFormat, lstData.ToArray())))
                {
                    newRow[dataColumn] = string.Empty;
                }
            }
        }
    }

    private void InvokeMethodsAndSetData(DataRow row)
    {
        DataTable dtMethods = _reportDateSet.Tables["MethodsToInvoke"];

        if (dtMethods != null && dtMethods.Rows.Count > 0)
        {
            string mName, vColumn;
            string[] ss;
            foreach (DataRow rr in dtMethods.Rows)
            {
                mName = BusinessUtility.GetString(rr["MethodName"]);
                vColumn = BusinessUtility.GetString(rr["SetValuetocolumn"]);
                ss = BusinessUtility.GetString(rr["ParameterColumns"]).Split(',');
                List<object> pValues = new List<object>();
                foreach (var item in ss)
                {
                    if (row.Table.Columns.Contains(item))
                    {
                        pValues.Add(row[item]);
                    }
                }
                //if (!string.IsNullOrEmpty(mName) && !string.IsNullOrEmpty(vColumn))
                //{
                //    switch (mName)
                //    {
                //        case "GetBalanceAmount":
                //            if (row.Table.Columns.Contains(vColumn) && ss != null && pValues.Count == 1)
                //            {
                //                row[vColumn] = this.GetBalanceAmount(pValues[0]);
                //            }
                //            break;
                //    }
                //}
            }
        }
    }

    private string GetSubTotalText()
    {
        return string.Format("<div style='display:block;background-image:none;margin-right:-2px;margin-left:-2px;height:100%; padding:2px 4px; text-align:right;background:#ccff99;'><b>{0}</b></div>", this.GetSubTotalLegend());
    }

    private string GetTotalText()
    {
        return "<div style='display:block;background-image:none;margin-right:-2px;margin-left:-2px;height:100%; padding:2px 4px; text-align:right;background:#ccff99;'><b>Total</b></div>";
    }

    private string GetSummaryTotal(double amt)
    {
        return string.Format("<div style='display:block;background-image:none;margin-right:-2px;margin-left:-2px;height:100%; padding:2px 4px; text-align:right;background:#ccff99;'><b>{0:F}</b></div>", amt);
    }
}
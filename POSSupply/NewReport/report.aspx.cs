﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Data;

using iTECH.Library.Utilities;
using iCtrl = iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;
using iTECH.InbizERP.BusinessLogic;

using Trirand.Web.UI.WebControls;
using System.Xml;
using System.IO;
using System.Globalization;
using System.Data.Odbc;
using System.Configuration;

public partial class NewReport_report : BasePage
{
    DataSet _reportDateSet = null;
    string _reportSql = "";
    bool _reportHasLanguageParameter = false;
    Button btnExport = new Button();
    Button btnPrint = new Button();
    Button btnEmail = new Button();
    Button btnXML = new Button();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (this.ReportXmlPath == string.Empty)
        {
            iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, "Report configuration source not found!");
            grdReport.Visible = false;
            return;
        }
        if (!File.Exists(Server.MapPath(this.ReportXmlPath)))
        {
            iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Failure, "Report configuration source not found!");
            grdReport.Visible = false;
            return;
        }
        _reportDateSet = new DataSet("ReportDataSource");
        _reportDateSet.ReadXml(Server.MapPath(this.ReportXmlPath));
        DataRow reportSettingRow = null;
        if (_reportDateSet.Tables["ReportSetting"] != null)
        {
            reportSettingRow = _reportDateSet.Tables["ReportSetting"].Rows[0];
            this.Title = BusinessUtility.GetString(reportSettingRow["ReportName"]);
            reportTitle.Text = BusinessUtility.GetString(reportSettingRow["ReportName"]);
            _reportHasLanguageParameter = BusinessUtility.GetBool(reportSettingRow["HasLanguageParameter"]);
            _reportSql = BusinessUtility.GetString(reportSettingRow["ReportSql"]);
        }

        //Build Column Modal
        DataTable reportColumns = _reportDateSet.Tables["ReportColumn"];
        if (reportColumns != null)
        {
            foreach (DataRow dr in reportColumns.Rows)
            {
                JQGridColumn col = new JQGridColumn();
                col.HeaderText = BusinessUtility.GetString(dr["HeaderText"]);
                col.DataField = BusinessUtility.GetString(dr["DataField"]);
                col.PrimaryKey = BusinessUtility.GetBool(dr["PrimaryKey"]);
                //col.Sortable = BusinessUtility.GetBool(dr["Sortable"]);
                col.Sortable = false;
                //col.DataFormatString = BusinessUtility.GetString(dr["DataFormat"]);
                col.Visible = BusinessUtility.GetBool(dr["Visible"]);
                if (reportColumns.Columns.Contains("TextAlign"))
                {
                    col.TextAlign = this.GetTextAlignment(BusinessUtility.GetString(dr["TextAlign"]));
                }
                grdReport.Columns.Add(col);
            }
        }

        if (grdReport.AjaxCallBackMode == Trirand.Web.UI.WebControls.AjaxCallBackMode.None)
        {
            //Build Commands 
            if (reportSettingRow != null)
            {
                if (reportSettingRow.Table.Columns.Contains("AllowXML"))
                {

                    if (BusinessUtility.GetBool(reportSettingRow["AllowXML"]))
                    {
                        btnXML.Text = "XML";
                        btnXML.Click += new EventHandler(btnDownloadXML_Click);
                        phCommands.Controls.Add(btnXML);
                    }
                }
                if (BusinessUtility.GetBool(reportSettingRow["AllowExportExcel"]))
                {
                    btnExport.Text = "Excel";
                    btnExport.Click += new EventHandler(btnExport_Click);
                    phCommands.Controls.Add(btnExport);
                }
                if (BusinessUtility.GetBool(reportSettingRow["AllowExportExcel"]))
                {
                    btnPrint.Text = "Print";
                    //btnPrint.Click +=new EventHandler(btnPrint_Click);
                    btnPrint.OnClientClick = "window.open('printgrid.aspx','PrintMe','height=300px,width=300px,scrollbars=1'); return false;";
                    phCommands.Controls.Add(btnPrint);
                }
                if (BusinessUtility.GetBool(reportSettingRow["AllowMail"]))
                {
                    btnEmail.Text = "Email";
                    btnEmail.OnClientClick = "return emailReportPopup();";
                    phCommands.Controls.Add(btnEmail);
                }
                if (reportSettingRow.Table.Columns.Contains("AutoLoad"))
                {
                    if (BusinessUtility.GetString(reportSettingRow["AutoLoad"]) != "")
                    {
                        Session["isAutoLoad"] = BusinessUtility.GetBool(reportSettingRow["AutoLoad"]);
                    }
                }
            }

            //Build Search Panel Starts here
            DataTable filterParams = _reportDateSet.Tables["ReportSqlParameter"];
            if (filterParams != null)
            {
                Boolean HasRequiredSearchParam = false;
                if (filterParams.Columns.Contains("Required"))
                {
                    HasRequiredSearchParam = true;
                }
                foreach (DataRow dr in filterParams.Rows)
                {
                    phSearchFilters.Controls.Add(new LiteralControl("<h4>"));
                    Label lbl = new Label();
                    lbl.ID = "lbl" + BusinessUtility.GetString(dr["Name"]);
                    lbl.Text = BusinessUtility.GetString(dr["Title"]);
                    lbl.AssociatedControlID = BusinessUtility.GetString(dr["Name"]);
                    lbl.CssClass = "filter-key";
                    phSearchFilters.Controls.Add(lbl);
                    phSearchFilters.Controls.Add(new LiteralControl("</h4>"));
                    phSearchFilters.Controls.Add(new LiteralControl("<div class='inner'>"));
                    string ctrlType = BusinessUtility.GetString(dr["ControlType"]);
                    string dType = BusinessUtility.GetString(dr["Type"]);
                    string dValue = BusinessUtility.GetString(dr["DefaultValue"]);
                    if (ctrlType == "TextBox" || ctrlType == "DatePicker")
                    {
                        TextBox tb = new TextBox();
                        tb.ID = BusinessUtility.GetString(dr["Name"]);
                        tb.Attributes["filterKey"] = BusinessUtility.GetString(dr["Name"]);
                        tb.Attributes["txttitle"] = BusinessUtility.GetString(dr["Title"]);
                        if (ctrlType == "DatePicker")
                        {
                            tb.CssClass = "datepicker";
                        }

                        if (HasRequiredSearchParam == true)
                        {
                            if (BusinessUtility.GetString(dr["Required"]) != "")
                            {
                                if (BusinessUtility.GetBool(dr["Required"]) == true)
                                {
                                    tb.CssClass = "cssreq";
                                }
                            }
                        }

                        if (dType.ToUpper() == "DATE")
                        {
                            tb.Text = BusinessUtility.GetDateTimeString(this.GetRequestedDate(dValue), DateFormat.MMddyyyy);
                        }
                        else
                        {
                            if ((BusinessUtility.GetString(dr["Name"]).ToUpper() == "CustID".ToUpper()) && (BusinessUtility.GetString(Request.QueryString["custid"]) != ""))
                            {

                                tb.Text = BusinessUtility.GetString(Request.QueryString["custid"]);
                            }
                            else
                            {
                                tb.Text = dValue;
                            }
                        }
                        phSearchFilters.Controls.Add(tb);
                    }
                    else if (BusinessUtility.GetString(dr["ControlType"]) == "DropDownList")
                    {
                        DropDownList dl = new DropDownList();
                        dl.ID = BusinessUtility.GetString(dr["Name"]);
                        dl.Attributes["filterKey"] = BusinessUtility.GetString(dr["Name"]);
                        string sql = BusinessUtility.GetString(dr["ControlSql"]);
                        if (sql != string.Empty)
                        {
                            DbHelper dbHelp = new DbHelper();
                            try
                            {
                                List<MySqlParameter> p = new List<MySqlParameter>();
                                if (sql.Contains("@Language"))
                                {
                                    p.Add(DbUtility.GetParameter("Language", "en", MyDbType.String));
                                }
                                dl.DataTextField = "DataTextField";
                                dl.DataValueField = "DataValueField";
                                dl.DataSource = dbHelp.GetDataTable(sql, CommandType.Text, p.ToArray());
                                dl.DataBind();
                            }
                            catch
                            {

                            }
                            //dl.DataSource = 
                        }

                        if ((dl.ID.ToUpper() == "sysRegCode".ToUpper()) && (BusinessUtility.GetString(Request.QueryString["regcode"]) != ""))
                        {
                            dl.SelectedValue = BusinessUtility.GetString(Request.QueryString["regcode"]);
                        }
                        else if ((dl.ID.ToUpper() == "WarehouseCode".ToUpper()) && (BusinessUtility.GetString(Request.QueryString["whscode"]) != ""))
                        {
                            dl.SelectedValue = BusinessUtility.GetString(Request.QueryString["whscode"]);
                        }
                        else
                        {
                            dl.SelectedValue = BusinessUtility.GetString(dr["DefaultValue"]);
                        }
                        phSearchFilters.Controls.Add(dl);
                    }
                    else if (BusinessUtility.GetString(dr["ControlType"]) == "ListBox")
                    {
                        //DropDownList dl = new DropDownList();
                        ListBox dl = new ListBox();
                        dl.ID = BusinessUtility.GetString(dr["Name"]);
                        dl.Attributes["filterKey"] = BusinessUtility.GetString(dr["Name"]);
                        dl.SelectionMode = ListSelectionMode.Multiple;
                        dl.CssClass = "modernized_select";
                        dl.Attributes["data-placeholder"] = BusinessUtility.GetString(dr["Title"]);
                        string sql = BusinessUtility.GetString(dr["ControlSql"]);
                        if (sql != string.Empty)
                        {
                            DbHelper dbHelp = new DbHelper();
                            try
                            {
                                List<MySqlParameter> p = new List<MySqlParameter>();
                                if (sql.Contains("@Language"))
                                {
                                    p.Add(DbUtility.GetParameter("Language", "en", MyDbType.String));
                                }
                                dl.DataTextField = "DataTextField";
                                dl.DataValueField = "DataValueField";
                                dl.DataSource = dbHelp.GetDataTable(sql, CommandType.Text, p.ToArray());
                                dl.DataBind();
                            }
                            catch
                            {

                            }
                        }
                        phSearchFilters.Controls.Add(dl);
                    }
                    phSearchFilters.Controls.Add(new LiteralControl("</div>"));
                }
            }

            DataTable sortColums = _reportDateSet.Tables["ReportSortColumn"];
            if (sortColums != null)
            {
                phSearchFilters.Controls.Add(new LiteralControl("<h4>"));
                Label lbl = new Label();
                lbl.ID = "lblSortBy";
                lbl.Text = "Sort By";
                lbl.AssociatedControlID = "SortBy";
                lbl.CssClass = "filter-key";
                phSearchFilters.Controls.Add(lbl);
                phSearchFilters.Controls.Add(new LiteralControl("</h4>"));
                phSearchFilters.Controls.Add(new LiteralControl("<div class='inner'>"));

                DropDownList dl = new DropDownList();
                dl.ID = "SortBy";
                dl.Attributes["onchange"] = "customReportSorting(this);";
                dl.DataTextField = "FieldDisplayName";
                dl.DataValueField = "FieldName";
                dl.DataSource = sortColums;
                dl.DataBind();
                phSearchFilters.Controls.Add(dl);
                phSearchFilters.Controls.Add(new LiteralControl("</div>"));

                phSearchFilters.Controls.Add(new LiteralControl("<h4>"));
                Label lblSo = new Label();
                lblSo.ID = "lblSortDir";
                lblSo.Text = "Sort Order";
                lblSo.AssociatedControlID = "SortOrder";
                lblSo.CssClass = "filter-key";
                phSearchFilters.Controls.Add(lblSo);
                phSearchFilters.Controls.Add(new LiteralControl("</h4>"));
                phSearchFilters.Controls.Add(new LiteralControl("<div class='inner'>"));
                DropDownList dl2 = new DropDownList();
                dl2.ID = "SortOrder";
                dl2.Attributes["onchange"] = "customSortOrder(this);";
                dl2.Items.Add(new ListItem("ASC", "ASC"));
                dl2.Items.Add(new ListItem("DESC", "DESC"));
                phSearchFilters.Controls.Add(dl2);
                phSearchFilters.Controls.Add(new LiteralControl("</div>"));
            }
            //Search panel ends
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (Session["_rpt_export"] != null)
        {
            if (((DataTable)Session["_rpt_export"]).Rows.Count > 0)
            {
                Export.ExportDetails((System.Data.DataTable)Session["_rpt_export"], Export.ExportFormat.Excel, "report.xls");

                //string sFileNme = DateTime.Now.ToString("yyyymmddhhmmss");
                //ExcelHelper.Instance.ExportToExcel((System.Data.DataTable)Session["_rpt_export"], Request.PhysicalApplicationPath + "Upload\\ReportExcel\\report" + sFileNme + ".xlsx");
                //Response.Redirect("~/Upload/ReportExcel/report" + sFileNme + ".xlsx");
            }
        }
    }

    #region DownloadQBXML
    protected void btnDownloadXML_Click(object sender, EventArgs e)
    {
        if (Session["_rpt_export"] != null)
        {
            if (((DataTable)Session["_rpt_export"]).Rows.Count > 0)
            {
                DownLoadXML((DataTable)Session["_rpt_export"]);
                //Response.Write("Ram Avtar");
                //Export.ExportDetails((System.Data.DataTable)Session["_rpt_export"], Export.ExportFormat.Excel, "report.xls");
            }
        }
    }

    private void DownLoadXML(DataTable dtInvoice)
    {
        Invoice _inv = new Invoice();
        Partners _cust = new Partners();
        Product _prd = new Product();
        InvoiceItems invItem = new InvoiceItems();
        DbHelper dbHelp = new DbHelper(true);
        if (dtInvoice.Rows.Count > 0)// 455
        {
            if (dtInvoice.Columns.Contains("InvoiceNo"))
            {
                var distinct = from r in dtInvoice.Rows.Cast<DataRow>()
                               group r by (string)r["InvoiceNo"] into g
                               select g.First();

                StringBuilder sbCustAddXML = new StringBuilder();
                StringBuilder sbItemAddXML = new StringBuilder();
                StringBuilder sbInvAddXML = new StringBuilder();
                foreach (var item in distinct)
                {
                    // Get Invoice Detail
                    int iInvoiceNo = BusinessUtility.GetInt(item["InvoiceNo"]);
                    _inv = new Invoice();
                    _inv.PopulateObject(dbHelp, BusinessUtility.GetInt(iInvoiceNo));

                    // Get Customer Detail
                    _cust = new Partners();
                    _cust.PopulateObject(dbHelp, BusinessUtility.GetInt(_inv.InvCustID));
                    string sCustName = BusinessUtility.GetString(_cust.PartnerLongName);
                    sbCustAddXML.Append(GetQBCustomerAddXML().Replace("#CustName#", sCustName).Replace("#CustPhone#", _cust.PartnerPhone));

                    // Get InvoiceItem
                    invItem = new InvoiceItems();
                    StringBuilder sbInvItemAddXML = new StringBuilder();
                    DataTable dtInvItem = invItem.GetInvoiceItemsDataTable(dbHelp, iInvoiceNo);
                    foreach (DataRow dRowInvItem in dtInvItem.Rows)
                    {
                        string sPName = BusinessUtility.GetString(dRowInvItem["prdName"]) + "-" + BusinessUtility.GetString(dRowInvItem["prdUPCCode"]);
                        sbItemAddXML.Append(GetQBItemAddXML().Replace("#ITEMNAME#", sPName));

                        //Collect Tax Applied on Item
                        StringBuilder sbItemTaxes = new StringBuilder();
                        if (BusinessUtility.GetString(dRowInvItem["TaxDesc1"]) != "")
                        {
                            sbItemTaxes.Append(GetQBSaleTaxXML().Replace("#TAXCODE#", BusinessUtility.GetString(dRowInvItem["TaxDesc1"])));
                        }
                        //if (BusinessUtility.GetString(dRowInvItem["TaxDesc2"]) != "")
                        //{
                        //    sbItemTaxes.Append(GetQBSaleTaxXML().Replace("#TAXCODE#", BusinessUtility.GetString(dRowInvItem["TaxDesc2"])));
                        //}
                        //if (BusinessUtility.GetString(dRowInvItem["TaxDesc3"]) != "")
                        //{
                        //    sbItemTaxes.Append(GetQBSaleTaxXML().Replace("#TAXCODE#", BusinessUtility.GetString(dRowInvItem["TaxDesc3"])));
                        //}
                        //if (BusinessUtility.GetString(dRowInvItem["TaxDesc4"]) != "")
                        //{
                        //    sbItemTaxes.Append(GetQBSaleTaxXML().Replace("#TAXCODE#", BusinessUtility.GetString(dRowInvItem["TaxDesc4"])));
                        //}
                        //if (BusinessUtility.GetString(dRowInvItem["TaxDesc5"]) != "")
                        //{
                        //    sbItemTaxes.Append(GetQBSaleTaxXML().Replace("#TAXCODE#", BusinessUtility.GetString(dRowInvItem["TaxDesc5"])));
                        //}

                        sbInvItemAddXML.Append(
                        GetQBInvItemAddXML().Replace("#ITEMNAME#", sPName)
                        .Replace("#QTY#", BusinessUtility.GetString(BusinessUtility.GetInt(dRowInvItem["invProductQty"])))
                        .Replace("#PRICE#", CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(dRowInvItem["invProductUnitPrice"])).Replace(",", ""))
                        .Replace("#AMOUNT#", CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(dRowInvItem["Amount"])).Replace(",", ""))
                        .Replace("#SALETAXES#", BusinessUtility.GetString(sbItemTaxes))
                        );
                    }

                    sbInvAddXML.Append(GetQBInvAddXML()
                        .Replace("#CUSTNAME#", sCustName)
                        .Replace("#INVNO#", BusinessUtility.GetString(BusinessUtility.GetInt(iInvoiceNo)))
                        .Replace("#INVITEM#", BusinessUtility.GetString(sbInvItemAddXML))
                        .Replace("#REQUESTID#", BusinessUtility.GetString(BusinessUtility.GetInt(iInvoiceNo))));
                }

                StringBuilder sbQBXMLContaint = new StringBuilder();
                sbQBXMLContaint.Append(BusinessUtility.GetString(sbCustAddXML));
                sbQBXMLContaint.Append(BusinessUtility.GetString(sbItemAddXML));
                sbQBXMLContaint.Append(BusinessUtility.GetString(sbInvAddXML));

                StringBuilder sbQBXML = new StringBuilder();
                sbQBXML.Append(GetQBXML().Replace("#XMLBODY#", BusinessUtility.GetString(sbQBXMLContaint)));

                XmlDocument qbXML = GetQBXMLBody();

                XmlNode prof = qbXML.CreateNode(XmlNodeType.Element, "QBXML", null);
                prof.InnerXml = BusinessUtility.GetString(sbQBXML);
                qbXML.AppendChild(prof);

                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(RemoveDiacritics(BusinessUtility.GetString(qbXML.OuterXml)));
                string sFileName = ("QBXML" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml").Replace(@"\\", @"\").Replace("-", "_").Replace(":", "_").Replace(" ", "_");
                string LabelFileName = (FeedExLabelPath + sFileName).Replace(@"\\", @"\");
                xdoc.Save(LabelFileName);
                string _path = LabelFileName;
                System.IO.FileInfo _file = new System.IO.FileInfo(_path);
                if (_file.Exists)
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + _file.Name);
                    Response.AddHeader("Content-Length", _file.Length.ToString());
                    Response.ContentType = "application/octet-stream";
                    Response.WriteFile(_file.FullName);
                    Response.End();
                }
            }
        }
    }

    static string RemoveDiacritics(string text)
    {
        var normalizedString = text.Normalize(NormalizationForm.FormD);
        var stringBuilder = new StringBuilder();

        foreach (var c in normalizedString)
        {
            var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
            if (unicodeCategory != UnicodeCategory.NonSpacingMark)
            {
                stringBuilder.Append(c);
            }
        }

        return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
    }

    private static string FeedExLabelPath
    {
        get
        {
            return HttpContext.Current.Request.PhysicalApplicationPath + BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["FeedExLablePath"]);
        }
    }

    public string GetQBCustomerAddXML()
    {
        StringBuilder sbQBCustomerAddXML = new StringBuilder();
        sbQBCustomerAddXML.Append("<CustomerAddRq ><CustomerAdd><Name>#CustName#</Name><Phone >#CustPhone#</Phone></CustomerAdd></CustomerAddRq>");
        return BusinessUtility.GetString(sbQBCustomerAddXML);
    }

    public string GetQBItemAddXML()
    {
        StringBuilder sbQBItemAddXML = new StringBuilder();
        sbQBItemAddXML.Append("<ItemNonInventoryAddRq >");
        sbQBItemAddXML.Append("<ItemNonInventoryAdd>");
        sbQBItemAddXML.Append("<Name>#ITEMNAME#</Name>");
        sbQBItemAddXML.Append("<SalesOrPurchase>");
        sbQBItemAddXML.Append("<AccountRef>");
        sbQBItemAddXML.Append("<FullName>" + this.ItemInventoryAccountType + "</FullName>");
        sbQBItemAddXML.Append("</AccountRef>");
        sbQBItemAddXML.Append("</SalesOrPurchase>");
        sbQBItemAddXML.Append("</ItemNonInventoryAdd>");
        sbQBItemAddXML.Append("</ItemNonInventoryAddRq>");
        return BusinessUtility.GetString(sbQBItemAddXML);
    }

    public string GetQBInvItemAddXML()
    {
        StringBuilder sbQBInvItemAddXML = new StringBuilder();
        sbQBInvItemAddXML.Append("<InvoiceLineAdd > ");
        sbQBInvItemAddXML.Append("<ItemRef>");
        sbQBInvItemAddXML.Append("<FullName >#ITEMNAME#</FullName>");
        sbQBInvItemAddXML.Append("</ItemRef>");
        sbQBInvItemAddXML.Append("<Quantity >#QTY#</Quantity>");
        sbQBInvItemAddXML.Append("<Rate >#PRICE#</Rate>");
        sbQBInvItemAddXML.Append("<Amount >#AMOUNT#</Amount>");
        //sbQBInvItemAddXML.Append("<!--<SalesTaxCodeRef>");
        //sbQBInvItemAddXML.Append("<ListID >IDTYPE</ListID>");
        //sbQBInvItemAddXML.Append("<FullName >STRTYPE</FullName>");
        //sbQBInvItemAddXML.Append("</SalesTaxCodeRef>-->");
        sbQBInvItemAddXML.Append("#SALETAXES#");
        sbQBInvItemAddXML.Append("</InvoiceLineAdd>        ");
        return BusinessUtility.GetString(sbQBInvItemAddXML);
    }

    public string GetQBSaleTaxXML()
    {
        StringBuilder sbQBSaleTaxXML = new StringBuilder();
        sbQBSaleTaxXML.Append("<SalesTaxCodeRef>");
        sbQBSaleTaxXML.Append("<FullName >#TAXCODE#</FullName>");
        sbQBSaleTaxXML.Append("</SalesTaxCodeRef>");
        return BusinessUtility.GetString(sbQBSaleTaxXML);
    }

    public string GetQBInvAddXML()
    {
        StringBuilder sbQBInvAddXML = new StringBuilder();
        sbQBInvAddXML.Append("<!-- STARTINV#REQUESTID# -->");
        sbQBInvAddXML.Append("<InvoiceAddRq requestID='#REQUESTID#'>");
        sbQBInvAddXML.Append("<InvoiceAdd > ");
        sbQBInvAddXML.Append("<CustomerRef> ");
        sbQBInvAddXML.Append("<FullName >#CUSTNAME#</FullName>");
        sbQBInvAddXML.Append("</CustomerRef>");
        sbQBInvAddXML.Append("<RefNumber >#INVNO#</RefNumber>");
        sbQBInvAddXML.Append("#INVITEM#");
        sbQBInvAddXML.Append("</InvoiceAdd>");
        sbQBInvAddXML.Append("</InvoiceAddRq>");
        sbQBInvAddXML.Append("<!-- ENDINV#REQUESTID# -->");
        return BusinessUtility.GetString(sbQBInvAddXML);
    }

    public XmlDocument GetQBXMLBody()
    {
        XmlDocument inputXMLDoc = new XmlDocument();
        inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", null, null));
        inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbxml", "version=\"2.0\""));
        return inputXMLDoc;
    }

    public string GetQBXML()
    {
        StringBuilder sbQBXMLBody = new StringBuilder();
        sbQBXMLBody.Append("<QBXMLMsgsRq onError='continueOnError'>");
        sbQBXMLBody.Append("#XMLBODY#");
        sbQBXMLBody.Append("</QBXMLMsgsRq>");
        return BusinessUtility.GetString(sbQBXMLBody);
    }

    private string ItemInventoryAccountType
    {
        get
        {
            return System.Configuration.ConfigurationSettings.AppSettings["ItemInventoryAccountType"];
        }
    }
    #endregion


    //protected void btnExport_Click(object sender, EventArgs e)
    //{
    //    if (Session["_rpt_export"] != null)
    //    {
    //        DataTable dtExcel = (System.Data.DataTable)Session["_rpt_export"];
    //        if (dtExcel.Rows.Count > 0)
    //        {
    //            string legendColumn = this.GetTotalLegendColumn();
    //            List<string> lstGroupColumns = this.GetGroupColumns();
    //            DataRow drPrevious = dtExcel.Rows[0];
    //            for (int i = 1; i < dtExcel.Rows.Count; i++)
    //            {
    //             if (string.IsNullOrEmpty(legendColumn))
    //                    break;
    //                if (lstGroupColumns.Count <= 0)
    //                    break;
    //                if (BusinessUtility.GetString(dtExcel.Rows[i][legendColumn]).Contains("Total"))
    //                    continue;
    //                foreach (string groupColumns in lstGroupColumns)
    //                {
    //                    if (string.IsNullOrEmpty(BusinessUtility.GetString(dtExcel.Rows[i][groupColumns])))
    //                        dtExcel.Rows[i][groupColumns] = drPrevious[groupColumns];
    //                }
    //                drPrevious = dtExcel.Rows[i];                    
    //            }
    //            Export.ExportDetails(dtExcel, Export.ExportFormat.Excel, "report.xls");                           
    //        }
    //    }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && grdReport.AjaxCallBackMode == Trirand.Web.UI.WebControls.AjaxCallBackMode.None)
        {
            // Get User Grid Height
            InbizUser _usr = new InbizUser();
            _usr.PopulateObject(CurrentUser.UserID);
            if (BusinessUtility.GetInt(_usr.UserGridHeight) > 0)
            {
                grdReport.Height = _usr.UserGridHeight;
            }
        }
    }

    protected void grdReport_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        Session["_rpt_paramCollect"] = null;

        if (_reportDateSet != null)
        {
            string conString = string.Empty;
            string conProvider = string.Empty;
            Dictionary<string, object> dParamsCol = new Dictionary<string, object>();

            //Set connection to datasource if available
            if (_reportDateSet.Tables.Contains("Connection"))
            {
                DataTable dtConnection = _reportDateSet.Tables["Connection"];

                if (dtConnection.Rows.Count > 0)
                {
                    conString = BusinessUtility.GetString(dtConnection.Rows[0]["ConnectionString"]);
                    conProvider = BusinessUtility.GetString(dtConnection.Rows[0]["providerName"]);
                }
                if (!string.IsNullOrEmpty(conString))
                {
                    sdsReport.ConnectionString = conString;
                }
                if (!string.IsNullOrEmpty(conProvider))
                {
                    sdsReport.ProviderName = conProvider;
                }
            }


            sdsReport.SelectParameters.Clear();
            List<object> lstParams = new List<object>();
            DataTable filterParams = _reportDateSet.Tables["ReportSqlParameter"];
            //DataTable whereClauses = _reportDateSet.Tables["ReportWhereClause"];
            string ctrlID = "ctl00_ctl00_cphFullWidth_cphLeft_{0}";
            DataTable dtParamCollect = ParameterTable();
            foreach (DataRow row in filterParams.Rows)
            {
                string reqVal = Request.QueryString[string.Format(ctrlID, row["Name"])];

                if (reqVal == null && (BusinessUtility.GetString(row["Name"]).ToUpper() == "CustID".ToUpper()))
                {
                    reqVal = BusinessUtility.GetString(Request.QueryString["custid"]);
                }
                string valFormat = BusinessUtility.GetString(row["ValueFormatString"]);
                if (valFormat.Length <= 0)
                {
                    valFormat = "{0}";
                }

                DataRow drParamCollect = dtParamCollect.NewRow();
                drParamCollect["ParamName"] = BusinessUtility.GetString(row["Title"]);
                drParamCollect["Value"] = BusinessUtility.GetString(reqVal);
                dtParamCollect.Rows.Add(drParamCollect);
                dtParamCollect.AcceptChanges();
                Session["_rpt_paramCollect"] = dtParamCollect;

                if (!string.IsNullOrEmpty(reqVal))
                {
                    switch (BusinessUtility.GetString(row["Type"]).ToUpper())
                    {
                        case "DATE":
                            DateTime dtVal = BusinessUtility.GetDateTime(reqVal, DateFormat.MMddyyyy);
                            reqVal = string.Format(valFormat, dtVal.Date.ToString("yyyy-MM-dd HH:mm:ss").Replace("00:00:00", string.Empty));
                            break;
                    }

                    //sdsReport.SelectParameters.Add("@" + BusinessUtility.GetString(row["Name"]), reqVal);
                    dParamsCol["@" + BusinessUtility.GetString(row["Name"])] = reqVal;
                }
                else
                {
                    reqVal = BusinessUtility.GetString(row["DefaultValue"]);
                    switch (BusinessUtility.GetString(row["Type"]).ToUpper())
                    {
                        case "DATE":
                            DateTime dtVal = this.GetRequestedDate(reqVal);
                            reqVal = string.Format(valFormat, dtVal.Date.ToString("yyyy-MM-dd HH:mm:ss").Replace("00:00:00", string.Empty));
                            break;
                    }

                    //sdsReport.SelectParameters.Add("@" + BusinessUtility.GetString(row["Name"]), reqVal);                    
                    dParamsCol["@" + BusinessUtility.GetString(row["Name"])] = reqVal;
                }
            }
            if (_reportHasLanguageParameter)
            {
                //sdsReport.SelectParameters.Add("@Language", "en");
                dParamsCol["@Language"] = "en";
            }

            // For Auto Load Grid
            if (BusinessUtility.GetString(Request.QueryString["_history"]) == "")
            {
                if (BusinessUtility.GetBool(Session["isAutoLoad"]) == false)
                {
                    //  _reportSql.LastIndexOf(";");
                    if ((_reportSql.Trim().LastIndexOf(";") > 0) && (_reportSql.Trim().LastIndexOf(";") == _reportSql.Trim().Length - 1))
                    {
                        _reportSql = _reportSql.Substring(0, _reportSql.Trim().LastIndexOf(";")); //+" LIMIT 0 ";
                        if (!_reportSql.Contains("LIMIT"))
                        {
                            _reportSql = _reportSql + " LIMIT 0 ";
                        }
                    }
                    else
                    {
                        if (!_reportSql.Contains("LIMIT"))
                        {
                            _reportSql = _reportSql + " LIMIT 0 ";
                        }
                    }
                    Session["isAutoLoad"] = true;
                }
            }
            sdsReport.SelectCommand = _reportSql;

            if (string.IsNullOrEmpty(conString)) //If null go far default connection string from webconfig
            {
                conString = System.Configuration.ConfigurationManager.ConnectionStrings["NewConnectionString"].ConnectionString;
            }

            OdbcConnection DbConnection = new OdbcConnection(ConfigurationManager.AppSettings["PioneerDBConnection"]);

            DataView dvUnGroupd = null;//


            Response.Write(_reportSql);
            DbConnection.Open();
            OdbcDataAdapter da = new OdbcDataAdapter(Convert.ToString(_reportSql), DbConnection);
            DataTable dtRestults = new DataTable();
            da.Fill(dtRestults);
            dvUnGroupd = dtRestults.DefaultView;

            if (conProvider == "System.Data.Odbc")
            {

            }
            else
            {
                //using (MySqlConnection con = new MySqlConnection(conString))
                //{
                //    MySqlCommand cmd = new MySqlCommand();
                //    cmd.Connection = con;
                //    cmd.CommandText = _reportSql;
                //    foreach (var item in dParamsCol.Keys)
                //    {
                //        cmd.Parameters.AddWithValue(item, dParamsCol[item]).IsNullable = true;
                //    }
                //    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                //    DataTable dtRestults = new DataTable();
                //    da.Fill(dtRestults);
                //    dvUnGroupd = dtRestults.DefaultView;
                //}
            }

            if (dvUnGroupd != null)
            {
                string sortCol = Request.QueryString.AllKeys.Contains("rptSortBy") ? Request.QueryString["rptSortBy"] : "";
                string sortBy = Request.QueryString.AllKeys.Contains("rptSortOrder") ? Request.QueryString["rptSortOrder"] : "ASC";
                if (sortCol.Length > 0)
                {
                    dvUnGroupd.Sort = string.Format("{0} {1}", sortCol, sortBy);
                }

                //grdReport.DataSource = dvUnGroupd;
                grdReport.DataSource = this.PerformGroupingAndSorting(dvUnGroupd);
            }
        }
    }

    private DataTable ParameterTable()
    {
        DataTable dtTable = new DataTable();
        dtTable.Columns.Add("ParamName", typeof(string));
        dtTable.Columns.Add("Value", typeof(string));
        return dtTable;
    }

    protected void grdReport_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {

    }

    protected void grdReport_DataRequested(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestedEventArgs e)
    {
        //Session["_rpt_export"] = e.DataTable;

        DataTable dtToExport = e.DataTable;
        DataTable reportColumns = _reportDateSet.Tables["ReportColumn"];

        string sColToRemove = "";
        foreach (DataColumn dc in dtToExport.Columns)
        {
            bool isColumnFind = false;

            foreach (DataRow dRow in reportColumns.Rows)
            {
                if (BusinessUtility.GetString(dRow["DataField"]) == BusinessUtility.GetString(dc.ColumnName))
                {
                    isColumnFind = true;
                }
            }

            if (isColumnFind == false)
            {
                if (sColToRemove == "")
                {
                    sColToRemove = dc.ColumnName;
                }
                else
                {
                    sColToRemove += "," + dc.ColumnName;
                }
            }
        }

        string[] sColToRemoveFromTabl = sColToRemove.Split(',');

        foreach (string str in sColToRemoveFromTabl)
        {
            if (dtToExport.Columns.Contains(str))
            {
                dtToExport.Columns.Remove(str);
                dtToExport.AcceptChanges();
            }

        }



        // Session["_rpt_export"] = e.DataTable;
        Session["_rpt_export"] = dtToExport;
        Session["_rpt_title"] = reportTitle.Text;
    }

    void btnPrint_Click(object sender, EventArgs e)
    {
        //ClientScript.RegisterStartupScript(this.GetType(), "onclick", "<script language=javascript></script>");
    }

    private string ReportXmlPath
    {
        get
        {
            string xmlFile = Request.QueryString["rpt"];
            if (!string.IsNullOrEmpty(xmlFile))
            {
                return string.Format("~/NewReport/xml/{0}", xmlFile);
            }
            return string.Format("~/NewReport/xml/{0}", "nofile.xml");
            //return CommonReport.GetReportConfigurationXmlPath(this.CurrentReport);            
        }
    }

    private ReportKey CurrentReport
    {
        get
        {
            int id = BusinessUtility.GetInt(Request.QueryString["rpt"]);
            if (Enum.IsDefined(typeof(ReportKey), id))
            {
                return (ReportKey)id;
            }
            return ReportKey.Default;
        }
    }

    private Trirand.Web.UI.WebControls.TextAlign GetTextAlignment(string str)
    {
        switch (str.ToLower().Trim())
        {
            case "center":
                return Trirand.Web.UI.WebControls.TextAlign.Center;
            case "right":
                return Trirand.Web.UI.WebControls.TextAlign.Right;
            case "left":
            default:
                return Trirand.Web.UI.WebControls.TextAlign.Left;
        }
    }

    private DateTime GetRequestedDate(string key)
    {
        switch (key)
        {
            case "TODAY":
                return DateTime.Today;
            case "NEXT_DAY":
                return DateTime.Today.AddDays(1);
            case "NEXT_WEEK":
                return DateTime.Today.AddDays(7);
            case "NEXT_2WEEK":
                return DateTime.Today.AddDays(14);
            case "PREVIOUS_WEEK":
                return DateTime.Today.AddDays(-7);
            case "PREVIOUS_WEEK_MON":
                DateTime dtMonday = DateTime.Today.AddDays(-7);
                while (dtMonday.DayOfWeek != DayOfWeek.Monday)
                {
                    dtMonday = dtMonday.AddDays(-1);
                }
                return dtMonday;
            case "PREVIOUS_WEEK_SUN":
                DateTime dtSunDay = DateTime.Today.AddDays(-7);
                while (dtSunDay.DayOfWeek != DayOfWeek.Sunday)
                {
                    dtSunDay = dtSunDay.AddDays(1);
                }
                return dtSunDay;
            case "MONTH_FIRST_DAY":
                return new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            case "MONTH_LAST_DAY":
                return new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1);
            case "PRE_MONTH_FIRST_DAY":
                return new DateTime(DateTime.Today.AddMonths(-1).Year, DateTime.Today.AddMonths(-1).Month, 1);
            case "PRE_MONTH_LAST_DAY":
                return new DateTime(DateTime.Today.AddMonths(-1).Year, DateTime.Today.AddMonths(-1).Month, 1).AddMonths(1).AddDays(-1);
            case "Current_Year_FIRST_DAY":
                return new DateTime(DateTime.Today.Year, 1, 1);
            default:
                return DateTime.Today;
        }
    }

    private List<string> GetGroupColumns()
    {
        DataTable dt = _reportDateSet.Tables["ReportSetting"];
        if (dt != null && dt.Rows.Count > 0 && dt.Columns.Contains("GroupColumns"))
        {
            string str = BusinessUtility.GetString(dt.Rows[0]["GroupColumns"]);
            return new List<string>(str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
        }
        return new List<string>();
    }

    private List<string> GetSubTotalColumns()
    {
        DataTable dt = _reportDateSet.Tables["ReportSetting"];
        if (dt != null && dt.Rows.Count > 0 && dt.Columns.Contains("SubTotalColumns"))
        {
            string str = BusinessUtility.GetString(dt.Rows[0]["SubTotalColumns"]);
            return new List<string>(str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
        }
        return new List<string>();
    }

    private List<string> GetTotalColumns()
    {
        DataTable dt = _reportDateSet.Tables["ReportSetting"];
        if (dt != null && dt.Rows.Count > 0 && dt.Columns.Contains("TotalColumns"))
        {
            string str = BusinessUtility.GetString(dt.Rows[0]["TotalColumns"]);
            return new List<string>(str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
        }
        return new List<string>();
    }

    private string GetTotalLegendColumn()
    {
        DataTable dt = _reportDateSet.Tables["ReportSetting"];
        if (dt != null && dt.Rows.Count > 0 && dt.Columns.Contains("TotalLegendColumn"))
        {
            return BusinessUtility.GetString(dt.Rows[0]["TotalLegendColumn"]);
        }
        return string.Empty;
    }

    private string GetReportCountColumn()
    {
        DataTable dt = _reportDateSet.Tables["ReportSetting"];
        if (dt != null && dt.Rows.Count > 0 && dt.Columns.Contains("ShowRecordsCountColumn"))
        {
            return BusinessUtility.GetString(dt.Rows[0]["ShowRecordsCountColumn"]);
        }
        return string.Empty;
    }

    private string GetSubTotalLegend()
    {
        DataTable dt = _reportDateSet.Tables["ReportSetting"];
        if (dt != null && dt.Rows.Count > 0 && dt.Columns.Contains("SubTotalLegend"))
        {
            return BusinessUtility.GetString(dt.Rows[0]["SubTotalLegend"]);
        }
        return "Sub Total";
    }

    private DataTable PerformGroupingAndSorting(DataView unSortedData)
    {
        DataTable dtSource = unSortedData.ToTable();
        DataTable dtDest = dtSource.Clone();
        DataTable reportColumns = _reportDateSet.Tables["ReportColumn"];
        DataTable dtNoRepeat = _reportDateSet.Tables["NoRepeatColumns"];


        //Make all destination column type of string
        foreach (DataColumn item in dtDest.Columns)
        {
            item.DataType = typeof(string);
        }

        string totalLegendColumn = this.GetTotalLegendColumn();
        string compositKey = string.Empty;
        List<string> grpColumns = this.GetGroupColumns();
        List<string> sTotalColumns = this.GetSubTotalColumns();
        List<string> totalColumns = this.GetTotalColumns();
        string rptCountColumn = this.GetReportCountColumn();

        bool addSubTotalRows = sTotalColumns.Count > 0;
        bool addTotalRow = totalColumns.Count > 0;

        Dictionary<string, double> subTotal = new Dictionary<string, double>(); //SubTotal Column Name, sub Total
        Dictionary<string, double> total = new Dictionary<string, double>(); //Total Column Name, Total

        if (grpColumns.Count > 0 || sTotalColumns.Count > 0 || totalColumns.Count > 0)
        {
            int iCounter = 0;
            List<string> currentCompositKeys;
            string formatedCompostiKey = string.Empty;
            foreach (DataRow row in dtSource.Rows)
            {
                //Invoke Methods & Set Data to original table
                this.InvokeMethodsAndSetData(row);

                currentCompositKeys = new List<string>();
                foreach (var key in grpColumns)
                {
                    currentCompositKeys.Add(BusinessUtility.GetString(row[key]));
                }

                if (currentCompositKeys.Count <= 0)
                {
                    currentCompositKeys.Add(iCounter.ToString());
                }
                formatedCompostiKey = string.Join("_", currentCompositKeys.ToArray());

                //Add total it is not dependednt on groupping
                foreach (var tCol in totalColumns)
                {
                    if (total.Keys.Contains(tCol))
                    {
                        total[tCol] += BusinessUtility.GetDouble(row[tCol]);
                    }
                    else
                    {
                        total[tCol] = BusinessUtility.GetDouble(row[tCol]);
                    }
                }

                if (formatedCompostiKey != compositKey)
                {
                    //Initialize sub total if not exists 
                    foreach (var sCol in sTotalColumns)
                    {
                        if (!subTotal.Keys.Contains(sCol))
                        {
                            subTotal[sCol] = sCol.ToLower() == rptCountColumn.ToLower() ? 1.0D : BusinessUtility.GetDouble(row[sCol]);
                        }
                    }

                    //Add First Row And Last Row
                    if (iCounter == 0 && iCounter == dtSource.Rows.Count - 1)
                    {
                        dtDest.Rows.Add(row.ItemArray);
                        DataRow lstrow = dtDest.Rows[dtDest.Rows.Count - 1];
                        //format added data
                        string format = string.Empty;
                        foreach (DataRow rptRow in reportColumns.Rows)
                        {
                            format = string.Empty;
                            format = BusinessUtility.GetString(rptRow["DataFormat"]);
                            if (!string.IsNullOrEmpty(format))
                            {
                                lstrow[BusinessUtility.GetString(rptRow["DataField"])] = string.Format(format, row[BusinessUtility.GetString(rptRow["DataField"])]);
                            }
                        }

                        //Add Sub total Row
                        if (addSubTotalRows)
                        {
                            DataRow sTotalRow = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                sTotalRow[totalLegendColumn] = this.GetSubTotalText();
                            }
                            foreach (var sCol in sTotalColumns)
                            {
                                sTotalRow[sCol] = this.GetSummaryTotal(subTotal[sCol], sCol);
                            }
                            dtDest.Rows.Add(sTotalRow);
                        }

                        //Add Total Row
                        if (addTotalRow)
                        {
                            DataRow totalRow = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                totalRow[totalLegendColumn] = this.GetTotalText();
                            }
                            foreach (var sCol in totalColumns)
                            {
                                totalRow[sCol] = this.GetSummaryTotal(total[sCol], sCol);
                            }
                            dtDest.Rows.Add(totalRow);
                        }
                    }
                    else if (iCounter == 0)
                    {
                        dtDest.Rows.Add(row.ItemArray);
                        DataRow lstrow = dtDest.Rows[dtDest.Rows.Count - 1];

                        //format added data
                        string format = string.Empty;
                        foreach (DataRow rptRow in reportColumns.Rows)
                        {
                            format = string.Empty;
                            format = BusinessUtility.GetString(rptRow["DataFormat"]);
                            if (!string.IsNullOrEmpty(format))
                            {
                                lstrow[BusinessUtility.GetString(rptRow["DataField"])] = string.Format(format, row[BusinessUtility.GetString(rptRow["DataField"])]);
                            }
                        }

                        //Check If no Repead
                        this.ValidateAndStoreInNottoDuplicat(dtNoRepeat, lstrow);
                    }
                    else if (iCounter == dtSource.Rows.Count - 1)
                    {
                        //Add sub Total
                        if (addSubTotalRows)
                        {
                            DataRow sTotalRow = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                sTotalRow[totalLegendColumn] = this.GetSubTotalText();
                            }
                            foreach (var sCol in sTotalColumns)
                            {
                                sTotalRow[sCol] = this.GetSummaryTotal(subTotal[sCol], sCol);
                            }
                            dtDest.Rows.Add(sTotalRow);
                        }
                        //Temp Reset Sub Total
                        foreach (var sCol in sTotalColumns)
                        {
                            subTotal.Remove(sCol);
                        }

                        //Add Last Row
                        dtDest.Rows.Add(row.ItemArray);
                        DataRow lstrow = dtDest.Rows[dtDest.Rows.Count - 1];
                        //format added data
                        string format = string.Empty;
                        foreach (DataRow rptRow in reportColumns.Rows)
                        {
                            format = string.Empty;
                            format = BusinessUtility.GetString(rptRow["DataFormat"]);
                            if (!string.IsNullOrEmpty(format))
                            {
                                lstrow[BusinessUtility.GetString(rptRow["DataField"])] = string.Format(format, row[BusinessUtility.GetString(rptRow["DataField"])]);
                            }
                        }

                        //Check If no Repead
                        this.ValidateAndStoreInNottoDuplicat(dtNoRepeat, lstrow);

                        //Again need to add sub total
                        //Add Sub Total
                        foreach (var sCol in sTotalColumns)
                        {
                            if (subTotal.Keys.Contains(sCol))
                            {
                                subTotal[sCol] += sCol.ToLower() == rptCountColumn.ToLower() ? 1.0D : BusinessUtility.GetDouble(row[sCol]);
                            }
                            else
                            {
                                subTotal[sCol] = sCol.ToLower() == rptCountColumn.ToLower() ? 1.0D : BusinessUtility.GetDouble(row[sCol]);
                            }
                        }

                        //Add another subtotal row
                        if (addSubTotalRows)
                        {
                            DataRow sTotalRowLast = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                sTotalRowLast[totalLegendColumn] = this.GetSubTotalText();
                            }
                            foreach (var sCol in sTotalColumns)
                            {
                                sTotalRowLast[sCol] = this.GetSummaryTotal(subTotal[sCol], sCol);
                            }
                            dtDest.Rows.Add(sTotalRowLast);
                        }

                        //Add Finally Total Row
                        if (addTotalRow)
                        {
                            DataRow totalRow = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                totalRow[totalLegendColumn] = this.GetTotalText();
                            }
                            foreach (var sCol in totalColumns)
                            {
                                totalRow[sCol] = this.GetSummaryTotal(total[sCol], sCol);
                            }
                            dtDest.Rows.Add(totalRow);
                        }
                    }
                    else
                    {
                        //Add Sub Total Row
                        if (addSubTotalRows)
                        {

                            DataRow sTotalRow = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                sTotalRow[totalLegendColumn] = this.GetSubTotalText();
                            }
                            foreach (var sCol in sTotalColumns)
                            {
                                sTotalRow[sCol] = this.GetSummaryTotal(subTotal[sCol], sCol);
                            }
                            dtDest.Rows.Add(sTotalRow);
                        }

                        //Add New Row
                        dtDest.Rows.Add(row.ItemArray);
                        DataRow lstrow = dtDest.Rows[dtDest.Rows.Count - 1];
                        //format added data
                        string format = string.Empty;
                        foreach (DataRow rptRow in reportColumns.Rows)
                        {
                            format = string.Empty;
                            format = BusinessUtility.GetString(rptRow["DataFormat"]);
                            if (!string.IsNullOrEmpty(format))
                            {
                                lstrow[BusinessUtility.GetString(rptRow["DataField"])] = string.Format(format, row[BusinessUtility.GetString(rptRow["DataField"])]);
                            }
                        }

                        //Check If no Repead
                        this.ValidateAndStoreInNottoDuplicat(dtNoRepeat, lstrow);

                        //Reset Sub Total
                        foreach (var sCol in sTotalColumns)
                        {
                            subTotal.Remove(sCol);
                        }

                        //Add current to calculate sub total for next 
                        foreach (var sCol in sTotalColumns)
                        {
                            if (!subTotal.Keys.Contains(sCol))
                            {
                                subTotal[sCol] = sCol.ToLower() == rptCountColumn.ToLower() ? 1.0D : BusinessUtility.GetDouble(row[sCol]);
                            }
                        }
                    }
                }
                else
                {
                    //Add Row to destination & Hide data in rows to perform grouping                     
                    dtDest.Rows.Add(row.ItemArray);
                    DataRow lstrow = dtDest.Rows[dtDest.Rows.Count - 1];
                    foreach (var gCol in grpColumns)
                    {
                        lstrow[gCol] = string.Empty;
                    }

                    //format added data
                    string format = string.Empty;
                    foreach (DataRow rptRow in reportColumns.Rows)
                    {
                        format = string.Empty;
                        format = BusinessUtility.GetString(rptRow["DataFormat"]);
                        if (!string.IsNullOrEmpty(format))
                        {
                            lstrow[BusinessUtility.GetString(rptRow["DataField"])] = string.Format(format, row[BusinessUtility.GetString(rptRow["DataField"])]);
                        }
                    }

                    //Check If no Repead
                    this.ValidateAndStoreInNottoDuplicat(dtNoRepeat, lstrow);

                    //Add sub total & total
                    foreach (var sCol in sTotalColumns)
                    {
                        if (subTotal.Keys.Contains(sCol))
                        {
                            subTotal[sCol] += sCol.ToLower() == rptCountColumn.ToLower() ? 1.0D : BusinessUtility.GetDouble(row[sCol]);
                        }
                        else
                        {
                            subTotal[sCol] = sCol.ToLower() == rptCountColumn.ToLower() ? 1.0D : BusinessUtility.GetDouble(row[sCol]);
                        }
                    }

                    if (iCounter == dtSource.Rows.Count - 1)
                    {
                        if (addSubTotalRows)
                        {
                            DataRow sTotalRowLast = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                sTotalRowLast[totalLegendColumn] = this.GetSubTotalText();
                            }
                            foreach (var sCol in sTotalColumns)
                            {
                                sTotalRowLast[sCol] = this.GetSummaryTotal(subTotal[sCol], sCol);
                            }
                            dtDest.Rows.Add(sTotalRowLast);
                        }

                        //Add Finally Total Row
                        if (addTotalRow)
                        {
                            DataRow totalRow = dtDest.NewRow();
                            if (!string.IsNullOrEmpty(totalLegendColumn))
                            {
                                totalRow[totalLegendColumn] = this.GetTotalText();
                            }
                            foreach (var sCol in totalColumns)
                            {
                                totalRow[sCol] = this.GetSummaryTotal(total[sCol], sCol);
                            }
                            dtDest.Rows.Add(totalRow);
                        }
                    }
                }

                compositKey = formatedCompostiKey;
                iCounter++;
            }

            return dtDest;
        }
        else
        {
            foreach (DataRow row in dtSource.Rows)
            {
                this.InvokeMethodsAndSetData(row);

                dtDest.Rows.Add(row.ItemArray);

                string format = string.Empty;
                foreach (DataRow rptRow in reportColumns.Rows)
                {
                    format = string.Empty;
                    format = BusinessUtility.GetString(rptRow["DataFormat"]);
                    if (!string.IsNullOrEmpty(format))
                    {
                        dtDest.Rows[dtDest.Rows.Count - 1][BusinessUtility.GetString(rptRow["DataField"])] = string.Format(format, row[BusinessUtility.GetString(rptRow["DataField"])]);
                    }
                }
            }

            return dtDest;
        }
    }

    private void ValidateAndStoreInNottoDuplicat(DataTable dtNotToDuplicat, DataRow newRow)
    {
        if (dtNotToDuplicat != null && dtNotToDuplicat.Rows.Count > 0 && newRow != null)
        {
            string[] keyColumns;
            string dataColumn;
            string previousRowValue;
            string previousRowValuFormat;
            List<string> lstData;
            foreach (DataRow item in dtNotToDuplicat.Rows)
            {
                lstData = new List<string>();
                keyColumns = BusinessUtility.GetString(item["KeyColumns"]).Split(',');
                dataColumn = BusinessUtility.GetString(item["DataColumn"]);
                previousRowValue = BusinessUtility.GetString(item["PreviousRowValue"]);
                previousRowValuFormat = BusinessUtility.GetString(item["PreviousRowValueFormat"]);

                foreach (var key in keyColumns)
                {
                    lstData.Add(BusinessUtility.GetString(newRow[key]));
                }
                //Set Current Value to Previous Row Data
                item["PreviousRowValue"] = string.Format(previousRowValuFormat, lstData.ToArray());

                //Set Empty Data
                if (previousRowValue.Equals(string.Format(previousRowValuFormat, lstData.ToArray())))
                {
                    newRow[dataColumn] = string.Empty;
                }
            }
        }
    }

    private void InvokeMethodsAndSetData(DataRow row)
    {
        DataTable dtMethods = _reportDateSet.Tables["MethodsToInvoke"];

        if (dtMethods != null && dtMethods.Rows.Count > 0)
        {
            string mName, vColumn;
            string[] ss;
            foreach (DataRow rr in dtMethods.Rows)
            {
                mName = BusinessUtility.GetString(rr["MethodName"]);
                vColumn = BusinessUtility.GetString(rr["SetValuetocolumn"]);
                ss = BusinessUtility.GetString(rr["ParameterColumns"]).Split(',');
                List<object> pValues = new List<object>();
                foreach (var item in ss)
                {
                    if (row.Table.Columns.Contains(item))
                    {
                        pValues.Add(row[item]);
                    }
                }
                if (!string.IsNullOrEmpty(mName) && !string.IsNullOrEmpty(vColumn))
                {
                    switch (mName)
                    {
                        case "GetBalanceAmount":
                            if (row.Table.Columns.Contains(vColumn) && ss != null && pValues.Count == 1)
                            {
                                //row[vColumn] = this.GetBalanceAmount(pValues[0]);
                            }
                            break;
                        case "test":
                            if (row.Table.Columns.Contains(vColumn) && ss != null && pValues.Count == 1)
                            {
                                this.GetTestData(pValues[0], pValues[1]);
                            }
                            break;
                    }
                }
            }
        }
    }

    private string GetSubTotalText()
    {
        return string.Format("<div style='display:block;background-image:none;margin-right:-2px;margin-left:-2px;height:100%; padding:2px 4px; text-align:right;background:#ccff99;'><b>{0}</b></div>", this.GetSubTotalLegend());
    }

    private string GetTotalText()
    {
        return "<div style='display:block;background-image:none;margin-right:-2px;margin-left:-2px;height:100%; padding:2px 4px; text-align:right;background:#ccff99;'><b>Total</b></div>";
    }

    private string GetSummaryTotal(double amt, string columnName)
    {
        DataTable reportColumns = _reportDateSet.Tables["ReportColumn"];
        DataRow[] dr = null;
        if (reportColumns != null && reportColumns.Rows.Count > 0)
        {
            dr = reportColumns.Select(string.Format("DataField='{0}'", columnName));
            if (dr != null && dr.Length > 0 && dr[0]["DataFormat"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(dr[0]["DataFormat"])))
            {
                return string.Format("<div style='display:block;background-image:none;margin-right:-2px;margin-left:-2px;height:100%; padding:2px 4px; text-align:right;background:#ccff99;'><b>{0}</b></div>", string.Format(dr[0]["DataFormat"].ToString(), amt));
            }
        }
        return string.Format("<div style='display:block;background-image:none;margin-right:-2px;margin-left:-2px;height:100%; padding:2px 4px; text-align:right;background:#ccff99;'><b>{0:F}</b></div>", amt);
    }

    #region Methods to invoke during databind
    public string GetTestData(object o1, object o2)
    {
        return "100";
    }
    #endregion
}
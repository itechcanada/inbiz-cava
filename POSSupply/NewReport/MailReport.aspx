﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Shared/popup.master" CodeFile="MailReport.aspx.cs" Inherits="NewReport_MailReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
<style type="text/css" media="screen">
    .label{padding-left:40px;padding-right:10px;font-weight:700;width:70px;vertical-align:top;}
    .Field{padding-left:10px;padding-right:10px;}
    
</style>
    <div id="contentBottom" style="padding:5px; height:150px; overflow:auto;">
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
        <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0" width="100%" >
            <tr>
                <td class="label" ><%--class="text"--%>
                    <asp:Label Text="To"  ID="lblTo" runat="server" />*
                </td>
                <td class="Field"><%--class="input"--%>
                    <asp:TextBox ID="txtMailTo" runat="server"  Width="100%" />
                    <asp:RequiredFieldValidator  ID="reqvalMailTo" ErrorMessage="Mail ID Required" runat="server" ControlToValidate="txtMailTo"
                        Display="None"></asp:RequiredFieldValidator>
                       <%-- <asp:RegularExpressionValidator ID="regexMailTo" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                        ControlToValidate="txtMailTo" ErrorMessage="Invalid Email Format" Display="None"></asp:RegularExpressionValidator>--%>
                </td>
               
            </tr>
            <tr>
                <td class="label">
                    <asp:Label Text="Subject"  ID="lblSubject" runat="server"/>*
                </td>
                <td class="Field">
                    <asp:TextBox ID="txtMailSubject" runat="server" MaxLength="50" Width="100%"  />
                    <asp:RequiredFieldValidator ID="reqvaltxtSubject" runat="server" ControlToValidate="txtMailSubject"
                        ErrorMessage="Subject Required" SetFocusOnError="true"
                        Display="None"> </asp:RequiredFieldValidator>
                </td>
               
            </tr>
             <tr>
                <td class="label">
                    <asp:Label Text="Message"  ID="lblMessage" runat="server" />*
                </td>
                <td class="Field">
                    <asp:TextBox ID="txtMailMessage" runat="server" TextMode="MultiLine" Width="100%" Height="80px"  />
                    <asp:RequiredFieldValidator ID="reqvaltxtMessage" runat="server" ControlToValidate="txtMailMessage"
                        ErrorMessage="Message Required" SetFocusOnError="true"
                        Display="None"> </asp:RequiredFieldValidator>
                </td>
            </tr>
            
        </table>        
    </div>
   
        <font color='red'><b>Note : </b></font>To Send Multiple E-Mail use spreated by ","
        <div class="div-dialog-command">
            <asp:Button Text="Send"  ID="btnSave" runat="server" OnClick="btnSend_Click" />
            <asp:Button Text="Cancel"  ID="btnCancel" runat="server" CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
            <div class="clear">
            </div>
        </div>
        <asp:ValidationSummary ID="valsAdminWarehouse" runat="server" ShowMessageBox="true"
            ShowSummary="false" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphScripts" runat="Server">    
    <script type="text/javascript">
      
    </script>
</asp:Content>
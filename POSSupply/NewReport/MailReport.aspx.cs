﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;
//using Microsoft.Office.Interop.Excel;



using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class NewReport_MailReport : BasePage
{
    //On Page Load
    private string sAttachedFile = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        txtMailTo.Focus();
    }

    protected void btnSend_Click(object sender, System.EventArgs e)
    {
        if (Session["_rpt_export"] == null)
        {
            return;
        }
        if (Page.IsValid)
        {
            // Code To Send Mail
            try
            {
                DataTable rptData = (DataTable)Session["_rpt_export"];
                if (rptData.Rows.Count <= 0)
                {
                    MessageState.SetGlobalMessage(MessageType.Information, "Record Not Found...");
                    return;
                }

                string fileName = Server.MapPath("~/Upload/ReportExcel/");
                fileName += BusinessUtility.GenerateRandomString(5, true);
                fileName += ".xls";

                if (CommonReport.CreateReportWorkBook(rptData, fileName))
                {
                    CommonReport.SendReport(txtMailTo.Text, txtMailSubject.Text, txtMailMessage.Text, fileName);
                    ClosePopup();
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Information, "Mail Not Send...");
                }

                //string sFileName = GetPath() + GetFileName();
                //sAttachedFile = sFileName;
                //if (CreatingWorkbook(((DataTable)Session["_rpt_export"]), sFileName) == true)
                //{
                //    string[] MailID = txtMailTo.Text.Split(',');

                //    foreach (string EmailID in MailID)
                //    {
                //        if (EmailID != "")
                //        {
                //            string strMailFrom = System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"];
                //            string strMailTo = BusinessUtility.GetString(EmailID); //txtMailTo.Text;
                //            string strSub = txtMailSubject.Text;
                //            string strMsg = "";
                //            strMsg = txtMailMessage.Text;

                //            //clsCommon obj = new clsCommon();                           
                //            if (EmailHelper.SendEmail(strMailFrom, strMailTo, strMsg, strSub, sAttachedFile))
                //            {
                //                ClosePopup();
                //            }                            
                //        }
                //    }
                //}
                //else
                //{
                //    MessageState.SetGlobalMessage(MessageType.Information, "Mail Not Send...");
                //}
            }

            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Information, ex.Message);
            }
        }
    }
    

    //private string GetPath()
    //{
    //    string dirPath = Server.MapPath("~/Upload/ReportExcel/");
    //    DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
    //   FileInfo[]  fileList =    dirInfo.GetFiles();
    //   foreach (FileInfo finfo in fileList)
    //   {
    //       string sFilePath = dirPath + finfo.Name;
    //       if (isFileOpenOrReadOnly(ref sFilePath) != true)
    //       {
    //           finfo.Delete();
    //       }
    //   }

    //    if (!dirInfo.Exists)
    //    {
    //        dirInfo.Create();
    //    }

    //    string strFullPath = dirPath;
    //    return strFullPath;
    //}

    //private string GetFileName()
    //{
    //    string sRandomString = BusinessUtility.GenerateRandomString(5,true);
    //    string sReportTitle=  BusinessUtility.GetString(Session["_rpt_title"]).Replace(" ","_");
    //    string sFileExtension = this.GetDocType(); 
    //    string sFileName = sReportTitle + "_" + sRandomString + sFileExtension;
    //    return sFileName;
    //}

    //private string GetDocType()
    //{
    //    return ".xls";//.xls,doc
    //}

    /// <summary>
    /// method to check not only if a file is already open, but if the
    /// but also if read and write permissions exist
    /// </summary>
    /// <param name="file">the file we wish to check</param>
    /// <returns></returns>
    ///
    //public bool isFileOpenOrReadOnly(ref string file)
    //{
    //    try
    //    {
    //        //first make sure it's not a read only file
    //        if ((File.GetAttributes(file) & FileAttributes.ReadOnly) != FileAttributes.ReadOnly)
    //        {
    //            //first we open the file with a FileStream
    //            using (FileStream stream = new FileStream(file, FileMode.OpenOrCreate, FileAccess.Read, FileShare.None))
    //            {
    //                try
    //                {
    //                    stream.ReadByte();
    //                    return false;
    //                }
    //                catch (IOException)
    //                {
    //                    return true;
    //                }
    //                finally
    //                {
    //                    stream.Close();
    //                    stream.Dispose();
    //                }
    //            }
    //        }
    //        else
    //            return true;
    //    }
    //    catch (IOException)
    //    {
    //        return true;
    //    }
    //}

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }

    private void ClosePopup()
    {
        if (!string.IsNullOrEmpty(this.JsCallBackFunction))
        {
            Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
        }
        else
        {
            Globals.RegisterCloseDialogScript(Page);
        }
    }
}
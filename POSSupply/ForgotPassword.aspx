﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="ForgotPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Inbiz Forgot Password</title>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/scripts/jquery-1.5.1.min.js"></script>
    <script type="text/javascript" src="lib/scripts/jquery-ui-1.8.11.custom.min.js"></script>
    <link href="lib/css/login.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("button, input:submit", "").button();
        });    	   
    </script> 
</head>
<body>
   <div id="outer">
        <div id="middle">
            <div id="inner" class="inner">
                <div class="glow">
                    <div class="content">
                        <div class="logo">
                        </div>
                        <%--  <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut auctor, purus at condimentum
                            cursus, orci elit felis.
                            <br />
                            <br />
                            <asp:Label ID="lblMsg" runat="server" ForeColor="red" CssClass="lblBold"></asp:Label>
                        </p>--%>
                        <div style="height: 30px">
                            <p>
                                <asp:Label ID="lblMsg" runat="server" ForeColor="red" CssClass="lblBold" />
                            </p>
                        </div>
                        <br />
                        <br />
                        <form id="form1" runat="server">
                        <table>
                            <tr>
                                <td class="text">
                                    <asp:Label ID="lblID" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblLoginID %>"></asp:Label>
                                </td>
                                <td class="input">
                                    <asp:TextBox ID="txtLoginID" Width="290px" runat="server" MaxLength="75"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvalLoginID" runat="server" ControlToValidate="txtLoginID"
                                        ErrorMessage="<%$ Resources:Resource, reqvalLoginID %>" SetFocusOnError="true"
                                        Display="None"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="text">
                                    <asp:Label ID="lblPwd" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblEmailID %>"></asp:Label>
                                </td>
                                <td class="input">
                                    <asp:TextBox ID="txtEmailID" Width="290px" runat="server" MaxLength="100">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvalEmail" runat="server" ControlToValidate="txtEmailID"
                                        ErrorMessage="<%$ Resources:Resource, PlzEntEmailID %>" SetFocusOnError="true"
                                        Display="None"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revalEmailId" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ControlToValidate="txtEmailID" ErrorMessage="<%$ Resources:Resource, ValidEmailID %>"
                                        SetFocusOnError="true" Display="None"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td class="forgetLink">
                                    <a href="AdminLogin.aspx">
                                        <%= Resources.Resource.lbllogin %></a>                                   
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnLogin" runat="server" 
                                        Text="<%$ Resources:Resource, cmdCssSubmit%>" onclick="btnLogin_Click" />
                                </td>
                                <td>                                    
                                </td>
                            </tr>
                        </table>
                        <asp:Panel runat="server" ID="pnlMain" DefaultButton="btnLogin">
                            <asp:ValidationSummary ID="valsDefault" runat="server" DisplayMode="List" ShowMessageBox="true"
                                ShowSummary="false" />
                        </asp:Panel>                        
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

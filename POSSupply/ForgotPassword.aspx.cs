﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.Web;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql; 

public partial class ForgotPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        InbizUser iUser = new InbizUser();
        try
        {
            string newPassword = string.Empty;
            if (iUser.ResetPassword(null, txtLoginID.Text, txtEmailID.Text, out newPassword))
            {
                //Send Email new password to user
                string fromAddress = System.Configuration.ConfigurationManager.AppSettings["NoReplyEmail"];
                lblMsg.Text = Resources.Resource.msgSuccPwdSent;
                string msg = Resources.Resource.lblResetPwdMsg + " " + newPassword;

                EmailHelper.SendEmail(fromAddress, txtEmailID.Text, msg, Resources.Resource.PasswordReset, false);
                lblMsg.Text = Resources.Resource.msgSuccPwdSent;
                txtEmailID.Text = string.Empty;
                txtLoginID.Text = string.Empty;
            }
            else
            {
                lblMsg.Text = Resources.Resource.PlzEntValidLoginIDEmailID;
            }
        }
        catch(Exception ex)
        {
            lblMsg.Text = ex.Message;            
        }
        finally
        { 
            
        }
    }   
}
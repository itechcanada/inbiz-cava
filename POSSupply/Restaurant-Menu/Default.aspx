﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Restaurant_Menu_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Welcome To Restaurant - Menu</title>
    <meta charset="utf-8" />
    <meta name='viewport' content='width=device-width,initial-scale=1,maximum-scale=1'
        userscale='no' />
    <link href="css/style.css" type="text/css" rel="stylesheet" />

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', '<%=System.Configuration.ConfigurationManager.AppSettings["JavaScriptCode"] %>', 'itechcanada.com');
            ga('send', 'pageview');

    </script>

    <script type="text/javascript">
        var source = "";
        source = '<%= Request.QueryString["source"] %>';
        function PlaceNewOrder(lang) {
            var url = "  MenuOrder.aspx?lang=";
            if (lang == "en")
                url += "en";
            else
                url += "fr";
            if (source == "1")
                url += "&source=1";
            window.location.href = url;
        }
    </script>
</head>
<body style="background-color: #de6b37;">
    <form id="form1" runat="server">
    <div style="width: 100%; height: 100%; display: block; text-align: center; color: white;
        font-family: monterey_btregular; float: left; width: 100%;">
        <div style="margin: 12% auto 0 0; font-size: 110px; float: left; width: 100%;">
            Welcome! &nbsp; Bienvenue!
        </div>
        <div style="float: left; clear: both; width: 100%; height: 200px; display: block;
            margin: 0px auto; text-align: center;">
            <div style="background-color: #d9c5ad; width: 200px; height: 200px; display: block;
                margin: 10px auto 0px auto; border-radius: 100px;">
                <a href="MenuOrder.aspx?lang=en" style="text-decoration: none; color: inherit;">
                    <div style="padding: 25px 33px; color: #de6b37; font-size: 50px;">
                        <img src="../Upload/companylogo/CZMCWIJJ.png" alt=" Restaurant" style="vertical-align: middle:" />
                    </div>
                </a>
            </div>
        </div>
        <div style="float: left; margin: 1% auto 0 0; clear: both; width: 100%; font-size: 90px">
            <a href="javascript:;" onclick='PlaceNewOrder("en");' style="text-decoration: none;
                color: inherit; margin: 0 2% 0 0;">Go to Menu
                <img src="images/icon-next.png" alt="" />
            </a><a href="javascript:;" onclick='PlaceNewOrder("fr");' style="text-decoration: none;
                color: inherit; margin: 0 0 0 2%;">Aller au menu
                <img src="images/icon-next.png" alt="" />
            </a>
        </div>
    </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewThankyou.aspx.cs" Inherits="Restaurant_Menu_NewThankyou" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Thank you - Place Order</title>
    <meta charset="utf-8">
    <meta name='viewport' content='width=device-width,initial-scale=1,maximum-scale=1'
        userscale='no'>
    <link href="css/style.css" type="text/css" rel="stylesheet" />
    <script src="../scripts/jquery.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-ui-1.8.11.custom.min.js" type="text/javascript"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<%=System.Configuration.ConfigurationManager.AppSettings["JavaScriptCode"] %>', 'itechcanada.com');
        ga('send', 'pageview');

    </script>
    <script type="text/javascript">
        var source = "";
        source = '<%= Request.QueryString["source"] %>';
        var currentAppLanguageCode = '<%= iTECH.InbizERP.BusinessLogic.Globals.CurrentAppLanguageCode %>';
        var postData = window.name.split('&==&');
        var vCompanyName='';

        var transID = "";
        var tableNote = "";
        if (postData.length > 1)
            tableNote = postData[0];
        if (postData.length > 2)
            transID = postData[1];
        if (transID == "")
            transID = '<%=TransitionIDForWeb %>;';
        if (tableNote == "")
            transID = '<%=TableNoteForWeb %>;';
        //Transition();

        $(document).ready(function () {
            CompanyInfo();
            SetProfileDetails();
            var imageUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["Thankyou_Barcode"] %>'
            imageUrl += tableNote;
            $('#divOrderPlaceCode').html("<img src='" + imageUrl + "' alt='" + tableNote + "' />");
            
        });
        var emailID = "";
        var custName = "";
        function SetProfileDetails() {
            $.ajax({
                type: "POST",
                url: "Restaurant-Menu.asmx/PartnerDetails",
                data: "{partnerID:'<%=UserID %>',logInID:'<%=EmailID %>',deviceID:'<%=DeviceID %>',token:'" + $('#<%=hdnToken.ClientID%>').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var JsonPartner = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (JsonPartner.ResponseCode == 1 && JsonPartner.Status == "OK") {
                        emailID = JsonPartner.Partners.PartnerEmail;
                        custName = JsonPartner.Partners.PartnerLongName;
                        SendEmail();
                        
                    }
                },
                error: function (xhr, err) { emailID = '<%=EmailID %>'; SendEmail() }
            });
        }
        function SendEmail() {
            var custEmailID = emailID;
            var subject = vCompanyName + " - " + '<%=Resources.Resource.lblOrderPlaced %>'; //Order Placed"; //"Chuchai - Order Placed";lblOrderPlaced
            var message = $("#divEmail").html();
            var dataToPost = {};
            dataToPost.email = emailID;
            dataToPost.subject = subject;
            dataToPost.message = message;
            $.ajax({
                type: "POST",
                url: '../Handlers/MailHandler.ashx',
                data: $.param(dataToPost),
                success: function (response) { },
                error: function (response, status, error) { }
            });
        }


        function CompanyInfo() {

            $.ajax({
                type: "POST",
                url: "NewThankyou.aspx/CompanyInfo",
                data: "{companyID:'5'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var returnData = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (returnData.ResponseCode == 1 && returnData.Status == "OK") {
                        vCompanyName = returnData.CompanyName;
                    }
                    else if (returnData.ResponseCode == -1) {
                        alert("Company infomartion not getting.");
                    }
                    else { alert(FormatUserErrorMessage(returnData.ResponseCode)); }
                },
                error: function (xhr, err) {
                    alert(FormatUserErrorMessage("-3") + " \n " + FormatErrorMessage(xhr, err));
                }
            });
        }

        var jsonTrans;
        function Transition() {
            $.ajax({
                type: "POST",
                url: '<%= ResolveUrl("~/Restaurant-Menu/Restaurant-Menu.asmx/Transaction") %> ',
                data: "{transID:'" + transID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    jsonTrans = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    ShowOrderDetails();
                    setInterval(function () { window.location.href = GetUrl(); }, 60 * 1000 * 2);
                },
                error: function (response, status, error) {
                    alert(response);
                }
            });
        }
        function ShowOrderDetails() {
            var trans = jsonTrans.Transition;
            var divHTML;
            if (trans.length == 1) {
                var i = 0;
                divHTML = "<div style=' min-height: 380px; top:0; width:100%;'> <table border='0' cellpadding='5px' cellspacing='5px' width='100%'>";
                divHTML += "<tr style='height: 10px;'><td colspan='4'></td></tr>";
                divHTML += " <tr style='height: 10px;'><td colspan='4'></td></tr> <tr><td colspan='2' style='text-align:left;'>";
                divHTML += " <strong><b>ORDER NO : " + trans[i].TransID + "<br />" + trans[i].Date + "</b></strong></td><td colspan='2'  style='text-align:right;'>";
                divHTML += "<strong><b style='font-size: 15px;'>" + trans[i].TableNote + "</b></strong></td></tr><tr style='height: 10px;'>";
                divHTML += "<td colspan='4'></td></tr><tr><td><strong><b> No.</b></strong></td><td><strong><b>  Desc</b></strong>";
                divHTML += "</td><td><strong><b>QTY</b></strong></td><td><strong><b>Unit Price</b></strong></td></tr>";

                var orderDetails = trans[i].OrderDetails;
                for (j = 0; j < orderDetails.length; j++) {
                    divHTML += " <tr><td>" + Number(j + 1) + "</td><td ><b>" + orderDetails[j].ProductName + "</b>";
                    divHTML += "</td><td><b>" + orderDetails[j].Qty + "</b></td><td>$" + Number(orderDetails[j].UnitPrice).toFixed(2) + "</td></tr>";
                }
                divHTML += " </table></div>";
            }
            $('#divOrderDetails').html(divHTML);
        }
        var showPrintOption = true;
        //        setInterval(function () { if (showPrintOption == true && jsonTrans != "") { printDiv(); showPrintOption = false; } }, 60 * 1000 * 0.10);
        function printDiv() {
            var printContents = $('#divOrderDetailsParent').html();
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
        function GetUrl() {
            var url = "NewMenuOrder.aspx?lang=";
            if (currentAppLanguageCode == "en")
                url += "en";
            else
                url += "fr";
            if (source == "1")
                url += "&source=1";
            return url;
        }
        function PlaceNewOrder() {
            window.location.href = GetUrl();

        }
    </script>
</head>
<body style="background-color: #de6b37; height: 100%">
    <form id="form1" runat="server">
    <div style="padding-top: 50px; float: left; width: 100%; display: none;" id="divOrderDetailsParent">
        <div style="width: 35%; min-height: 450px; float: left; position: relative; margin-right: 1%;
            margin-left: 2%; background-color: White; font-family: Arial; font-size: 11px;
            color: Black;" id="divOrderDetails">
        </div>
    </div>
    <div style="clear: both;">
    </div>
    <div style="width: 100%; height: 100%; display: block; text-align: center; color: white;
        font-family: ArcherProBold; float: left; width: 100%;">
        <div id="divEmail">
            <div style="margin: 8% auto 0 0; font-size: 55px; float: left; width: 100%;">
                <%=Resources.Resource.lblThankyou %>
            </div>
            <div style="margin: 0 auto 60px 0; font-size: 24px; float: left; width: 100%;">
                <%=Resources.Resource.lblOrderHasPlaced %>
                <div style="clear: both;">
                </div>
                <div style="font-size: 24px;">
                <%--<p style="font-size:14px;">
                    <%=ConfigurationManager.AppSettings["SignUpText"]%>
                    </p>--%>
                    <%=Resources.Resource.lblYourConfirmationCode %>

                </div>
                <div id="divOrderPlaceCode" style="font-size: 30px; text-align: center; color: Black;
                    font-family: Arial;">
                </div>
            </div>
        </div>
        <div style="float: left; clear: both; width: 100%; font-size: 29px;">
            <a href="javascript:;" onclick="PlaceNewOrder();" style="text-decoration: none; color: inherit;">
                <%=Resources.Resource.lblNewOrderPlaced %>
                <img src="images/icon-next.png" alt="" />
            </a>
        </div>
    </div>
     <asp:HiddenField ID="hdnToken" runat="server" />
    <div style="clear: both;">
    </div> 
    </form>
</body>
</html>

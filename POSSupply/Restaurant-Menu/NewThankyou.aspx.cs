﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using System.Web.Script.Serialization;
using System.Configuration;
using iTECH.Library.Utilities;
using System.Net;
using iTECH.Library.DataAccess.MySql;

using iTECH.WebService.Utility;

public partial class Restaurant_Menu_NewThankyou : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(TransitionID) && !string.IsNullOrEmpty(TableNote))
        {
            Session["TransitionID"] = TransitionID;
            Session["TableNote"] = TableNote;
            Session["IsTransactionSuccess"] = false;
            List<object> lstTransition = new CategoryWithProduct().GetTransition(null, BusinessUtility.GetInt(TransitionID), 1);
            double subTotal = 0.0D;
            double total = 0.0D;
            foreach (Dictionary<object, object> dictResult in lstTransition)
            {
                if (TransitionID == BusinessUtility.GetString(dictResult["TransID"]))
                {
                    if (isPartialPayThroughLoyalPoint == "Yes")
                    {
                        subTotal = Math.Round(BusinessUtility.GetDouble(dictResult["SubTotal"]), 2) - BusinessUtility.GetDouble(amtToUpdateLP);
                        total = Math.Round(BusinessUtility.GetDouble(dictResult["Total"]), 2) - BusinessUtility.GetDouble(amtToUpdateLP);
                        break;
                    }
                    else
                    {
                        subTotal = Math.Round(BusinessUtility.GetDouble(dictResult["SubTotal"]), 2);
                        total = Math.Round(BusinessUtility.GetDouble(dictResult["Total"]), 2);
                        break;
                    }
                }

            }
            Session["PaymentToPay"] = total;
            Session["SubTotalAmt"] = subTotal;
            string url = "";
            string urlpaymentDetails = "";
            urlpaymentDetails += string.Format("&L_PAYMENTREQUEST_0_NAME{0}={1}", 0, "Chuchai Restaurant Paiement/Payment");
            urlpaymentDetails += string.Format("&L_PAYMENTREQUEST_0_AMT{0}={1}", 0, Math.Round(BusinessUtility.GetDouble(subTotal), 2));
            urlpaymentDetails += string.Format("&L_PAYMENTREQUEST_0_QTY{0}={1}", 0, 1);

            url += "https://api-3t.sandbox.paypal.com/nvp";
            // url += "https://api-3t.paypal.com/nvp";
            url += string.Format("?USER={0}", Constants.API_USERNAME);
            url += string.Format("&PWD={0}", Constants.API_PASSWORD);
            url += string.Format("&SIGNATURE={0}", Constants.API_SIGNATURE);
            url += string.Format("&VERSION={0}", Constants.API_VERSION);
            url += string.Format("&PAYMENTREQUEST_0_PAYMENTACTION={0}", Constants.PAYMENTACTION_TYPE);

            url += string.Format("&PAYMENTREQUEST_0_CURRENCYCODE={0}", Constants.API_CURRENCYCODE);

            url += string.Format("&PAYMENTREQUEST_0_AMT={0}", Math.Round(total, 2));
            url += string.Format("&PAYMENTREQUEST_0_ITEMAMT={0}", Math.Round(subTotal, 2));
            url += string.Format("&PAYMENTREQUEST_0_TAXAMT={0}", Math.Round(total, 2) - Math.Round(subTotal, 2));

            url += string.Format("&METHOD={0}", APIMETHOD.SETDETAILS);
            string sSiteVirtualDirectory = Request.Url.GetLeftPart(UriPartial.Authority) + VirtualPathUtility.ToAbsolute("~");
            url += string.Format("&RETURNURL={0}", sSiteVirtualDirectory + "/Restaurant-Menu/NewThankyou.aspx");
            url += string.Format("&CANCELURL={0}", sSiteVirtualDirectory + "/Restaurant-Menu/NewMenuOrder.aspx?lang=" + Globals.CurrentAppLanguageCode);

            url += !string.IsNullOrEmpty(urlpaymentDetails) ? urlpaymentDetails : "";

            if (Globals.CurrentAppLanguageCode != "en")
                url = url.Replace(",", ".");

            Dictionary<string, string> dictResponseData = GetResponseDataFromAPI(url);
            string token = dictResponseData.ContainsKey(ResponseAPIData.TOKEN) ? dictResponseData[ResponseAPIData.TOKEN] : "";
            string ACK = dictResponseData.ContainsKey(ResponseAPIData.ACK) ? dictResponseData[ResponseAPIData.ACK] : ""; ;
            if (ACK == ACKTyp.SUCCESS && !dictResponseData.ContainsKey(ResponseAPIData.ERROR_CODE) && !string.IsNullOrEmpty(token))
                Response.Redirect("https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" + token, false);
            //  Response.Redirect("https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" + token, false);
        }
        else
            if (!string.IsNullOrEmpty(TOKEN) && !string.IsNullOrEmpty(PAYERID) && Session["PaymentToPay"] != null && Session["SubTotalAmt"] != null) //
            {
                string url = "";
                int calculatedPoint = 0;
                // url += "https://api-3t.paypal.com/nvp";
                url += "https://api-3t.sandbox.paypal.com/nvp";
                url += string.Format("?USER={0}", Constants.API_USERNAME);
                url += string.Format("&PWD={0}", Constants.API_PASSWORD);
                url += string.Format("&SIGNATURE={0}", Constants.API_SIGNATURE);
                url += string.Format("&VERSION={0}", Constants.API_VERSION);
                url += string.Format("&PAYMENTREQUEST_0_PAYMENTACTION={0}", Constants.PAYMENTACTION_TYPE);
                url += string.Format("&METHOD={0}", APIMETHOD.PAYMET);
                if (Globals.CurrentAppLanguageCode != "en")
                    url += string.Format("&PAYMENTREQUEST_0_AMT={0}", Math.Round(BusinessUtility.GetDouble(Session["PaymentToPay"]), 2)).Replace(",", ".");
                else
                    url += string.Format("&PAYMENTREQUEST_0_AMT={0}", Math.Round(BusinessUtility.GetDouble(Session["PaymentToPay"]), 2));
                url += string.Format("&PAYERID={0}", this.PAYERID);
                url += string.Format("&TOKEN={0}", this.TOKEN);
                url += string.Format("&PAYMENTREQUEST_0_CURRENCYCODE={0}", Constants.API_CURRENCYCODE);

                Dictionary<string, string> dictResponseData = GetResponseDataFromAPI(url);
                string ACK = dictResponseData.ContainsKey(ResponseAPIData.ACK) ? dictResponseData[ResponseAPIData.ACK] : "";
                if (ACK == ACKTyp.SUCCESS && !dictResponseData.ContainsKey(ResponseAPIData.ERROR_CODE))
                {
                    Session["IsTransactionSuccess"] = true;
                    // to do on success..
                    new CategoryWithProduct().UpdateTrasitionPaidAmount(BusinessUtility.GetInt(Session["TransitionID"]), BusinessUtility.GetDouble(Session["PaymentToPay"]));
                    //Added by mukesh 20130829
                    SysWarehouses _whs = new SysWarehouses();
                    double amtPerPoint = _whs.GetAmtPerPoint(System.Configuration.ConfigurationManager.AppSettings["WebSaleWhsCode"]);

                    if (amtPerPoint > 0)
                    {
                        calculatedPoint = BusinessUtility.GetInt(BusinessUtility.GetDouble(Session["SubTotalAmt"]) / amtPerPoint);
                    }
                    
                    DbHelper dbHelp = new DbHelper(true);
                    Partners part = new Partners();
                    part.PopulateObject(BusinessUtility.GetInt(this.UserID));
                    LoyalPartnerHistory lph = new LoyalPartnerHistory();
                    lph.LoyalCategoryID = part.PartnerLoyalCategoryID;
                    lph.LoyalPartnerID = part.PartnerID;
                    lph.PointAuto = false;
                    lph.Points = calculatedPoint;
                    lph.PointsAddedBy = BusinessUtility.GetInt(this.UserID);
                    lph.PointsAddedOn = DateTime.Now;
                    lph.PosTransactionID = BusinessUtility.GetInt(Session["TransitionID"]);
                    lph.Insert(dbHelp);
                    //Added end
                    string urlTo = "NewThankyou.aspx?lang=" + Globals.CurrentAppLanguageCode;
                    Response.Redirect(urlTo);
                }
                else
                {
                    string urlTo = "NewMenuOrder.aspx?lang=" + Globals.CurrentAppLanguageCode;
                    Response.Redirect(urlTo);
                }
            }
        if (((isPayThroughLoyalPoint == "Yes") || (isPartialPayThroughLoyalPoint == "Yes")) && !string.IsNullOrEmpty(TransitionID))
        {
            try
            {
                SysWarehouses _whs = new SysWarehouses();
                int calculatedPoint = 0;
                double amtPerPoint = _whs.GetLoyalAmtPerPoint(System.Configuration.ConfigurationManager.AppSettings["WebSaleWhsCode"]);
                if (amtPerPoint > 0)
                {
                    calculatedPoint = BusinessUtility.GetInt(BusinessUtility.GetDouble(amtToUpdateLP) / amtPerPoint);
                }
                Partners part = new Partners();
                part.PopulateObject(BusinessUtility.GetInt(UserID));
                LoyalPartnerHistory lph = new LoyalPartnerHistory();
                lph.LoyalCategoryID = part.PartnerLoyalCategoryID;
                lph.LoyalPartnerID = part.PartnerID;
                lph.PointAuto = false;
                lph.Points = (-1) * calculatedPoint;
                lph.PointsAddedBy = BusinessUtility.GetInt(UserID);
                lph.PointsAddedOn = DateTime.Now;
                lph.PosTransactionID = BusinessUtility.GetInt(TransitionID);
                lph.Insert(null);
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }

    public static Dictionary<string, string> GetResponseDataFromAPI(string url)
    {
        Dictionary<string, string> dictResponseData = new Dictionary<string, string>();
        using (WebClient client = new WebClient())
        {
            string responseFromServer = HttpContext.Current.Server.UrlDecode(client.DownloadString(url));
            string[] andOperatorData = responseFromServer.Split('&');
            foreach (string equalStringData in andOperatorData)
            {
                string[] equalOperatorData = equalStringData.Split('=');
                if (equalOperatorData.Length >= 2)
                    dictResponseData.Add(equalOperatorData[0], equalOperatorData[1]);
            }
        }
        return dictResponseData;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        hdnToken.Value = TokenNo;

        if (!Page.IsPostBack)
        {

      //Response.Write(Request.Url + "&url=false");
            //if (string.IsNullOrEmpty(BusinessUtility.GetString(Request.QueryString["url"])))
            //{
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Call my function", "SetProfileDetails();", true);
              //  Response.Redirect(Request.Url + "&url=false");
            //}
            //SetProfileDetails();
        }
    }
    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
    private bool _isValidToken = true;
    protected bool IsValidToken
    {
        get
        {
            if (Session["TokenResult"] != null)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["ResponseCode"] != null ? iTECH.Library.Utilities.BusinessUtility.GetInt(dictData["ResponseCode"]) == 1 && _isValidToken : false;
            }
            else return false;
        }
        set { _isValidToken = value; }
    }
    protected string UserID
    {
        get
        {
            if (IsValidToken)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["UserID"] != null ? dictData["UserID"].ToString() : "1";
            }
            else return "0";
        }
    }
    protected string EmailID
    {
        get
        {
            if (IsValidToken)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["EmailID"] != null ? dictData["EmailID"].ToString() : "admin";
            }
            else return "admin";
        }
    }
    protected string TokenNo
    {
        get
        {
            if (IsValidToken)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["TokenNo"] != null ? dictData["TokenNo"].ToString() : "";
            }
            else return "";
        }
    }
    protected string DeviceID { get { return ConfigurationManager.AppSettings["Resturant_DeviceID"] != null ? ConfigurationManager.AppSettings["Resturant_DeviceID"] : "Rest_123456"; } }

    private string TransitionID
    {
        get { return Request.QueryString["TransitionID"] != null ? Request.QueryString["TransitionID"] : ""; }
    }
    private string TableNote { get { return Request.QueryString["tableNote"] != null ? Request.QueryString["tableNote"] : ""; } }
    protected string TransitionIDForWeb { get { return Session["TransitionID"] != null ? BusinessUtility.GetString(Session["TransitionID"]) : "0"; } }
    protected string TableNoteForWeb { get { return Session["TableNote"] != null ? BusinessUtility.GetString(Session["TableNote"]) : ""; } }
    private string PAYERID { get { return Request.QueryString["PayerID"] != null ? Request.QueryString["PayerID"] : ""; } }
    private string TOKEN { get { return Request.QueryString["token"] != null ? Request.QueryString["token"] : ""; } }

    private string isPayThroughLoyalPoint
    {
        get { return Request.QueryString["isPayThroughLoyalPoint"] != null ? Request.QueryString["isPayThroughLoyalPoint"] : "false"; }
    }
    private string isPartialPayThroughLoyalPoint
    {
        get { return Request.QueryString["isPartialPayThroughLoyalPoint"] != null ? Request.QueryString["isPartialPayThroughLoyalPoint"] : "false"; }
    }
    private string amtToUpdateLP
    {
        get { return Request.QueryString["amtToUpdateLP"] != null ? Request.QueryString["amtToUpdateLP"] : "0.0"; }
    }


    [System.Web.Services.WebMethod]
    public static string CompanyInfo(string companyID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            clsCompanyInfo cls = new clsCompanyInfo();
            cls.CompanyID = companyID;
            cls.getCompanyInfo();
            dictResult.OkStatus(ResponseCode.SUCCESS);
            dictResult.Add("CompanyName", cls.CompanyName);
        }
        catch (Exception ex)
        {
            dictResult.ErrorStatus(ResponseCode.UNSUCCESS);
        }
        return dictResult.ResultInInJSONFormat;
    }

    public void PartialPaythrLP(double amtToUpdateLP)
    {
        try
        {
            SysWarehouses _whs = new SysWarehouses();
            int calculatedPoint = 0;
            double amtPerPoint = _whs.GetLoyalAmtPerPoint(System.Configuration.ConfigurationManager.AppSettings["WebSaleWhsCode"]);
            if (amtPerPoint > 0)
            {
                calculatedPoint = BusinessUtility.GetInt(BusinessUtility.GetDouble(amtToUpdateLP) / amtPerPoint);
            }
            Partners part = new Partners();
            part.PopulateObject(BusinessUtility.GetInt(UserID));
            LoyalPartnerHistory lph = new LoyalPartnerHistory();
            lph.LoyalCategoryID = part.PartnerLoyalCategoryID;
            lph.LoyalPartnerID = part.PartnerID;
            lph.PointAuto = false;
            lph.Points = (-1) * calculatedPoint;
            lph.PointsAddedBy = BusinessUtility.GetInt(UserID);
            lph.PointsAddedOn = DateTime.Now;
            lph.PosTransactionID = BusinessUtility.GetInt(TransitionID);
            lph.Insert(null);
        }
        catch (Exception ex)
        {
            throw;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using iTECH.InbizERP.BusinessLogic;
using iTECH.WebControls;
using iTECH.Library.Utilities;
using System.Data;
using System.Web.Script.Serialization;

public partial class Restaurant_Menu_MenuOrder : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["TokenResult"] == null || !IsValidToken)
            {
                Restaurant_Menu rest_menu = new Restaurant_Menu();
                string loginID = ConfigurationManager.AppSettings["Resturant_LoginID"] != null ? ConfigurationManager.AppSettings["Resturant_LoginID"] : "admin";
                string password = ConfigurationManager.AppSettings["Resturant_Password"] != null ? ConfigurationManager.AppSettings["Resturant_Password"] : "juni0124";
                object jsonObject = new JavaScriptSerializer().Deserialize<object>(rest_menu.Authentication(loginID, password, DeviceID));
                Session["TokenResult"] = jsonObject;
                _isValidToken = true;
            }
        }
        catch { IsValidToken = false; }
    }
    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
    private bool _isValidToken = true;
    protected bool IsValidToken
    {
        get
        {
            if (Session["TokenResult"] != null)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["ResponseCode"] != null ? iTECH.Library.Utilities.BusinessUtility.GetInt(dictData["ResponseCode"]) == 1 && _isValidToken : false;
            }
            else return false;
        }
        set { _isValidToken = value; }
    }
    protected string UserID
    {
        get
        {
            if (IsValidToken)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["UserID"] != null ? dictData["UserID"].ToString() : "1";
            }
            else return "0";
        }
    }
    protected string EmailID
    {
        get
        {
            if (IsValidToken)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["EmailID"] != null ? dictData["EmailID"].ToString() : "admin";
            }
            else return "admin";
        }
    }
    protected string TokenNo
    {
        get
        {
            if (IsValidToken)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["TokenNo"] != null ? dictData["TokenNo"].ToString() : "";
            }
            else return "";
        }
    }
    protected string DeviceID { get { return ConfigurationManager.AppSettings["Resturant_DeviceID"] != null ? ConfigurationManager.AppSettings["Resturant_DeviceID"] : "Rest_123456"; } }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using iTECH.InbizERP.BusinessLogic;

using iTECH.WebService.Utility;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;

public partial class Restaurant_Menu_SignUp : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hdnToken.Value = TokenNo;
    }
    protected override void InitializeCulture()
    {

        string sLan = BusinessUtility.GetString(ConfigurationManager.AppSettings["MobAppLang"]);
        string sLangSelected = BusinessUtility.GetString(Request.QueryString["lang"]);


        if (sLangSelected != "")
        {
            if (sLangSelected == "en")
            {
                Globals.SetCultureInfo(AppCulture.ENGLISH_CANADA);
            }
            else
            {
                Globals.SetCultureInfo(AppCulture.FRENCH_CANADA);
            }
            return;
        }
        if (sLan == "")
        {
            Globals.SetCultureInfo(Globals.CurrentCultureName);
            base.InitializeCulture();
        }
        else
        {
            if (sLan == "en")
            {
                Globals.SetCultureInfo(AppCulture.ENGLISH_CANADA);
            }
            else
            {
                Globals.SetCultureInfo(AppCulture.FRENCH_CANADA);
            }
        }
    }
    private bool _isValidToken = true;
    protected bool IsValidToken
    {
        get
        {
            if (Session["TokenResult"] != null)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["ResponseCode"] != null ? iTECH.Library.Utilities.BusinessUtility.GetInt(dictData["ResponseCode"]) == 1 && _isValidToken : false;
            }
            else return false;
        }
        set { _isValidToken = value; }
    }
    protected string UserID
    {
        get
        {
            if (IsValidToken)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["UserID"] != null ? dictData["UserID"].ToString() : "1";
            }
            else return "0";
        }
    }
    protected string EmailID
    {
        get
        {
            if (IsValidToken)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["EmailID"] != null ? dictData["EmailID"].ToString() : "admin";
            }
            else return "admin";
        }
    }
    protected string TokenNo
    {
        get
        {
            if (IsValidToken)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["TokenNo"] != null ? dictData["TokenNo"].ToString() : "";
            }
            else return "";
        }
    }
    protected string DeviceID { get { return ConfigurationManager.AppSettings["Resturant_DeviceID"] != null ? ConfigurationManager.AppSettings["Resturant_DeviceID"] : "Rest_123456"; } }

    [System.Web.Services.WebMethod]
    public static string CompanyInfo(string companyID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            clsCompanyInfo cls = new clsCompanyInfo();
            cls.CompanyID = companyID;
            cls.getCompanyInfo();
            dictResult.OkStatus(ResponseCode.SUCCESS);
            dictResult.Add("CompanyName", cls.CompanyName);
        }
        catch (Exception ex)
        {
            dictResult.ErrorStatus(ResponseCode.UNSUCCESS);
        }
        return dictResult.ResultInInJSONFormat;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using System.Configuration;
using System.Web.Script.Serialization;
using iTECHFacebook;
using iTECH.Library.Utilities;


public partial class Restaurant_Menu_Token_WebService : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string lang = Request.QueryString["lang"] != null ? Request.QueryString["lang"] : "en";
        string source = Request.QueryString["source"] != null ? Request.QueryString["source"] : "";
        try
        {
            if (Request.QueryString["loginID"] != null && Request.QueryString["pwd"] != null && !IsValidToken && Request.QueryString["Address"] != null)
            {
                Restaurant_Menu rest_menu = new Restaurant_Menu();
                string loginID = Request.QueryString["loginID"];
                string password = Request.QueryString["pwd"];
                object jsonObject = new JavaScriptSerializer().Deserialize<object>(rest_menu.Authentication(loginID, password, DeviceID));
                Session["TokenResult"] = jsonObject;
                _isValidToken = true;
                string url = Request.QueryString["Address"] != null ? Request.QueryString["Address"] + ".aspx" : "SignUp.aspx";
                url += "?lang=" + lang + (!string.IsNullOrEmpty(source) ? "&source=" + source : "");
                Response.Write("1" + jsonObject);
                //Response.Redirect(url, false);
            }
            else
                if (Request.QueryString["FBLogin"] != null)
                {
                    iTECHFacebook.Facebook fbObject = new Facebook();
                    fbObject.FacebookAppKey = BusinessUtility.GetString(ConfigurationManager.AppSettings["FB_AppID"]);
                    fbObject.FacebookAppSecret = BusinessUtility.GetString(ConfigurationManager.AppSettings["FB_AppSecretCode"]);
                    fbObject.RedirectUrl = BusinessUtility.GetString(ConfigurationManager.AppSettings["FB_CallBackURL"]); ;
                    fbObject.FacebookUserLogIn();
                    //string callback_url = BusinessUtility.GetString(ConfigurationManager.AppSettings["FB_CallBackURL"]);
                    //string url = "https://www.facebook.com/dialog/oauth?client_id=" + BusinessUtility.GetString(ConfigurationManager.AppSettings["FB_AppID"]) + "&scope=email,user_location,user_hometown&redirect_uri=" + callback_url;
                    // Response.Redirect(url);
                }
                //    else
                //if (Request.QueryString["code"] != null)
                //{
                //    string callback_url = BusinessUtility.GetString(ConfigurationManager.AppSettings["FB_CallBackURL"]);
                //    string url = "https://graph.facebook.com/oauth/access_token?client_id=" + BusinessUtility.GetString(ConfigurationManager.AppSettings["FB_AppID"]) + "&client_secret=" + BusinessUtility.GetString(ConfigurationManager.AppSettings["FB_AppSecretCode"]) +"&code="+Request.QueryString["code"]+"&redirect_uri="+ callback_url;
                //    Response.Redirect(url);
                //}
                //else
                //    if (Request.QueryString["token"] != null)
                //    {
                //        string callback_url = BusinessUtility.GetString(ConfigurationManager.AppSettings["FB_CallBackURL"]);
                //        string url = "https://graph.facebook.com/me/?access_token=" + Request.QueryString["token"] + "&fields=id,name,first_name,last_name,email,picture,work";
                //        Response.Redirect(url);
                //    }
                else
                    throw new Exception("Access Denied");
        }
        catch
        {
            IsValidToken = false;
            Session.RemoveAll();
            Session.Clear();
            Session.Abandon();
            string url = "SignUp.aspx";
            url += "?lang=" + lang + (!string.IsNullOrEmpty(source) ? "&source=" + source : "");
            Response.Redirect(url, false);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
    private bool _isValidToken = true;
    protected bool IsValidToken
    {
        get
        {
            if (Session["TokenResult"] != null)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["ResponseCode"] != null ? iTECH.Library.Utilities.BusinessUtility.GetInt(dictData["ResponseCode"]) == 1 && _isValidToken : false;
            }
            else return false;
        }
        set { _isValidToken = value; }
    }
    protected string UserID
    {
        get
        {
            if (IsValidToken)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["UserID"] != null ? dictData["UserID"].ToString() : "1";
            }
            else return "0";
        }
    }
    protected string EmailID
    {
        get
        {
            if (IsValidToken)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["EmailID"] != null ? dictData["EmailID"].ToString() : "admin";
            }
            else return "admin";
        }
    }
    protected string TokenNo
    {
        get
        {
            if (IsValidToken)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["TokenNo"] != null ? dictData["TokenNo"].ToString() : "";
            }
            else return "";
        }
    }
    protected string DeviceID { get { return ConfigurationManager.AppSettings["Resturant_DeviceID"] != null ? ConfigurationManager.AppSettings["Resturant_DeviceID"] : "Rest_123456"; } }
}
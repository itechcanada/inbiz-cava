﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewMenuOrder.aspx.cs" Inherits="Restaurant_Menu_NewMenuOrder" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Restaurant - Menu Choice your Order </title>
    <meta charset="utf-8" />
    <meta name='viewport' content='width=device-width,initial-scale=1,maximum-scale=1'
        userscale='no' />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="apple-touch-icon-precomposed" href="images/icon_jqm.png" />
    <script type="text/javascript" src="JS/jquery.min.js"></script>
    <link href="css/Mobile_style.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/jquery.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-ui-1.8.11.custom.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="JS/jquery-ui.css" />
    <script src="JS/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="js/jquery-ui.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-plugins/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-plugins/jquery.blockUI.js" type="text/javascript"></script>
    <%--  <script type="text/javascript">
        page_popup_bubble = "#index";
    </script>--%>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<%=System.Configuration.ConfigurationManager.AppSettings["JavaScriptCode"] %>', 'itechcanada.com');
        ga('send', 'pageview');

    </script>
    <script type="text/javascript">
        var JsonObject;
        var currentAppLanguageCode = '<%= iTECH.InbizERP.BusinessLogic.Globals.CurrentAppLanguageCode %>';
        var source = "";
        source = '<%= Request.QueryString["source"] %>';
        var screenWidth = 1024;
        $(document).ready(function () {
            var resourceText1 = ("<%=Resources.Resource.lblAdditionalRequest %>");
            $("#txtAdditionalRequest").attr("placeholder", resourceText1.replace("'", "\'"));

            if ('<%= IsValidToken %>' == "True") {
                PageOnLoad();
                SetMenuName();
                SetProfileDetails();
                $.ajax({
                    type: "POST",
                    url: "Restaurant-Menu.asmx/CategoryANDProduct",
                    data: "{lang:'" + currentAppLanguageCode + "',logInID:'<%= EmailID %>',deviceID:'<%= DeviceID %>',token:'" + $('#<%=hdnToken.ClientID%>').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        JsonObject = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (JsonObject.ResponseCode == 1 && JsonObject.Status == "OK") {
                            var listCategory = JsonObject.Category.length > 0 ? JsonObject.Category : "";
                            var innerHTMLValue = "";
                            $("#divCategory").html("");

                            var vViewFavList = '';
                            vViewFavList += "<div class='button-container' style='clear:both;'>";
                            vViewFavList += "<a href='javascript:;'  onclick='ShowFavourateProduct();'>" + "<%=Resources.Resource.lblViewFavourateProductList %>" + "</a>";
                            vViewFavList += "</div>";
                            $("#divCategory").append(vViewFavList);

                            for (i = 0; i < listCategory.length; i++) {
                                innerHTMLValue = "<div class='button-container' style='clear:both;'>";
                                innerHTMLValue += "<a href='javascript:;'  onclick='ShowProduct(\"" + listCategory[i].SubCategoryID + "\");'>" + listCategory[i].SubCategory + "</a>";
                                innerHTMLValue += "</div>";
                                $("#divCategory").append(innerHTMLValue);
                                if (i == 0) { ShowProduct(listCategory[i].SubCategoryID); }
                            }
                        }
                        else { $("#divCategory").html("<b>" + FormatUserErrorMessage(JsonObject.ResponseCode) + "</b>"); }
                    },
                    error: function (xhr, err) {
                        $("#divCategory").html("<b>" + FormatUserErrorMessage("-3") + "<br/>" + FormatErrorMessage(xhr, err) + "</b>");
                    }
                });
                GetShopTime();
            }
            else $("#divCategory").html("<b>" + FormatUserErrorMessage("-3") + "<br/>" + FormatUserErrorMessage("-2") + "</b>");
        });

        function SetProfileDetails() {
            $.ajax({
                type: "POST",
                url: "Restaurant-Menu.asmx/PartnerDetails",
                data: "{partnerID:'<%=UserID %>',logInID:'<%=EmailID %>',deviceID:'<%=DeviceID %>',token:'" + $('#<%=hdnToken.ClientID%>').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var JsonPartner = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (JsonPartner.ResponseCode == 1 && JsonPartner.Status == "OK") {
                        $("#txtName").val(JsonPartner.Partners.PartnerLongName);
                        $("#txtEmailID").val(JsonPartner.Partners.PartnerEmail);
                        $("#txtPhoneNo").val(JsonPartner.Partners.PartnerPhone);
                        document.getElementById("txtEmailID").readOnly = true;
                    }
                    else { alert(FormatUserErrorMessage(JsonPartner.ResponseCode)); }
                },
                error: function (xhr, err) {
                    alert(FormatUserErrorMessage("-3") + "\n" + FormatErrorMessage(xhr, err));
                }
            });

        }
        function PageOnLoad() {
            $('input[placeholder], textarea[placeholder]').css("font-family", "MS Shell Dlg");
            $('input[placeholder], textarea[placeholder]').css("font-size", "13px");
            $('input[placeholder]').css("height", "30px");
            screenWidth = $(document).width();
            if (Number(screenWidth) < Number(340)) {
                $(".content .category .button-container a").css("font-size", "38px");
                $(".content .order-area h1").css("font-size", "48px");
                $(".header .btm-content p").css("font-size", "38px");
                $(".header .btm-content p a").css("font-size", "38px");
                $("header .btm-content p").css("font-size", "38px");
                $("header .btm-content p a").css("font-size", "38px");

                $(".content .order-content-container .my-order p").css("font-size", "13px");
                $(".content .order-content-container .my-order p strong").css("font-size", "18px");
                $(".content .order-content-container .my-form p").css("font-size", "13px");
                $(".content .order-content-container .my-form p strong").css("font-size", "18px");
                $(".content #item-list ul li h2").css("font-size", "18px");
                $(".content #item-list ul li p").css("font-size", "13px");
            }
            $(".ui-loader").hide();
            $(".ui-loader").css('top', '100px');
            $(".ui-btn-inner").hide();
            $("#popup-container").hide();
            if (source == "1")
                $(".my-form").hide();
            $("#pShowShopHHMM").hide();
        }

        function FormatUserErrorMessage(responseCode) {
            var returnValue = "";
            if (responseCode == "-3")
                returnValue = "<%=Resources.Resource.lblWeAreHavingTechProb%>";
            else if (responseCode == "-2")
                returnValue = "<%=Resources.Resource.lblInVaildToken %>";
            else if (responseCode == "-1")
                returnValue = "<%=Resources.Resource.lblOperationUnsuccess%>";
            else
                returnValue = "<%=Resources.Resource.lblUnKnowError%>";
            return returnValue.replace("'", "\'");
        }

        function FormatErrorMessage(jqXHR, exception) {
            var returnValue = "";
            if (Number(jqXHR.status) == 0)
                returnValue = ("<%=Resources.Resource.lblErrorNetworkConnection %>");
            else if (Number(jqXHR.status) == 404)
                returnValue = ("<%=Resources.Resource.lblErrorPageNotfound %>");
            else if (Number(jqXHR.status) == 500)
                returnValue = ("<%=Resources.Resource.lblErrorInternalServerError %>");
            else if (exception == 'parsererror')
                returnValue = ("<%=Resources.Resource.lblErrorRequestedJSONParseFailed %>");
            else if (exception == 'timeout')
                returnValue = ("<%=Resources.Resource.lblErrorTimeOut %>");
            else if (exception == 'abort')
                returnValue = ("<%=Resources.Resource.lblErrorAjaxRequestAborted %>");
            else
                returnValue = ("<%=Resources.Resource.lblUnKnowError %> \n" + jqXHR.responseText);
            return returnValue.replace("'", "\'");
        }


        var imagePath = '<%= System.Configuration.ConfigurationManager.AppSettings["Restaurant_Image_Path"] %>';
        function AddedProduct(product, subCategoryID) {
            $("#popup-container").hide();
            var divValue = " <ul>";
            for (var i = 0; i < product.length; i++) {
                var isShow = false;
                //var prdselectioncount = 0;
                var showOption = "";
                divValue += " <li ><div style='float:left; width:12%; padding-right:3px; margin-right:10px;'> <a href='javascript:;' style='cursor:pointer;' onclick='ShowPopUP(" + product[i].ProductID + "," + subCategoryID + ");'>";
                var productImagePath = imagePath + product[i].SmallImage;
                divValue += " <img src='" + productImagePath + "' alt='' class='round-img'    /></a> </div>";

                divValue += "<div style='float:right; width:3%;cursor:pointer;'  onclick='AddQtyToMyOrder(" + product[i].ProductID + "," + subCategoryID + ");'> <img style='margin-top:10px; height:30px;' class='flr' src='images/icon-add.png' alt='' /> </div>";
                divValue += "<div style='float:right; width:3%; margin-right:25px;cursor:pointer;'  onclick='AddItemToFav(" + product[i].ProductID + "," + subCategoryID + ");'> <img style='margin-top:10px;height:30px;' class='flr' src='images/favicon.png' alt='' /> </div>";
                divValue += " <div style='float:left; text-align:left; width:55%; cursor:pointer;'  onclick='AddQtyToMyOrder(" + product[i].ProductID + "," + subCategoryID + ");'> ";
                divValue += " <h2>" + product[i].Name + "<sup><b> <span style='color:Green;font-weight:bold;font-family:Tahoma;font-size:15px;' class='order-number' id='spanMyOrder1" + product[i].ProductID + "' ></span></b> </sup></h2>";
                divValue += "<p style='text-align: justify;'>" + product[i].SmallDesc + "</p>";

                if (product[i].IsGlutenFree == "True") {
                    showOption = "Gluten Free";
                    isShow = true;
                }
                if (product[i].IsVegetarian == "True") {
                    if (showOption != "")
                        showOption += ",  ";
                    showOption += "Vegetarian";
                    isShow = true;
                }
                if (product[i].IsContainsNuts == "True") {
                    if (showOption != "")
                        showOption += ",  ";
                    showOption += "Contains Nuts";
                    isShow = true;
                }
                if (product[i].IsCookedSushi == "True") {
                    if (showOption != "")
                        showOption += ",  ";
                    showOption += "Cooked Sushi";
                    isShow = true;
                }
                if (Number(product[i].SpicyLevel) > 0) {
                    if (showOption != "")
                        showOption += ",  ";
                    showOption += "Spicy Level " + product[i].SpicyLevel;
                    isShow = true;
                }
                divValue += "<div style='clear:both;'></div> ";
                if (isShow)
                    divValue += "<p style='float:left; text-align: justify;'>" + showOption + "</p>";
                divValue += "<div style='clear:both;'></div> ";
                divValue += " <p style='float:left;text-align:left;'> Price  $ " + Number(product[i].SalesPrice).toFixed(2) + "</p></div>";
                divValue += "<div style='clear:both;'></div> ";
                divValue += "</li>";
            }
            divValue += " </ul>";
            return divValue;
        }
        var isProductDetails = false;
        function ShowProduct(subCategoryID) {
            var productHTML = "";
            var listCategory = JsonObject.Category.length > 0 ? JsonObject.Category : "";
            for (i = 0; i < listCategory.length; i++) {
                if (Number(listCategory[i].SubCategoryID) == Number(subCategoryID)) {
                    productHTML = AddedProduct(listCategory[i].Products, subCategoryID);
                    break;
                }
            }

            $(".divShowProduct").html(productHTML);
            if (isProductDetails == true)
                ShowProductDetails();
            isProductDetails = true;
        }

        function AddQtyToMyOrder(productID, subCategoryID) {
            totalMuOrderCount++;
            $(".spanTotalMyOrder").html(totalMuOrderCount);
            var addMyOrder = "";
            if (!(document.getElementById('spanMyOrder' + productID) == null)) {
                var qty = $('#spanMyOrder' + productID).text();
                $('#spanMyOrder' + productID).text(Number(qty) + 1);
                $('#spanMyOrder1' + productID).text(Number(qty) + 1);
                var modifyDivHTML = "<div class='my-order' id='divMyOrder" + productID + "' >" + $("#divMyOrder" + productID).html() + " </div";
                $('#divMyOrder' + productID).remove()
                var htmlData = $(".divShowMyOrder").html();
                $(".divShowMyOrder").html(modifyDivHTML);
                $(".divShowMyOrder").append(htmlData);
            }
            else {
                var listCategory = JsonObject.Category.length > 0 ? JsonObject.Category : "";
                for (i = 0; i < listCategory.length; i++) {
                    if (Number(listCategory[i].SubCategoryID) == Number(subCategoryID)) {
                        var productArray = listCategory[i].Products;
                        for (j = 0; j < productArray.length; j++) {
                            if (Number(productArray[j].ProductID) == Number(productID)) {
                                addMyOrder = " <div class='my-order' id='divMyOrder" + productArray[j].ProductID + "' ><p class='flt order-content'> <strong> ";
                                addMyOrder += productArray[j].Name + "</strong> </p>";
                                addMyOrder += "<p class='flr order-right-area'><span class='order-number' id='spanMyOrder" + productArray[j].ProductID + "' >1</span>";
                                addMyOrder += "<a href='javascript:;'><img src='images/icon-less.png' alt='' onclick='RemoveQtyFromMyOrder(" + productID + ")' /> </a></p> <div class='clr'></div>   </div";
                                var qty1 = $('#spanMyOrder1' + productID).text();
                                $('#spanMyOrder1' + productID).text(Number(qty1) + 1);

                                break;
                            }
                        }
                        break;
                    }
                }
                var htmlData = $(".divShowMyOrder").html();
                $(".divShowMyOrder").html(addMyOrder);
                $(".divShowMyOrder").append(htmlData);
            }

            SetMenuName();
            Close();
        }
        function RemoveQtyFromMyOrder(productID) {
            if (!(document.getElementById('spanMyOrder' + productID) == null)) {
                var qty = $('#spanMyOrder' + productID).text();
                if (Number(qty) > 0) {
                    $('#spanMyOrder' + productID).text(Number(qty) - 1);
                    if (Number($('#spanMyOrder' + productID).text()) == 0)
                        $('#divMyOrder' + productID).remove();
                }
                else
                    $('#divMyOrder' + productID).remove();
            }
            totalMuOrderCount--;
            SetMenuName();
            GetReqGrdQuery();
            $("#ltrTotalAmt").html("$" + Number(totalAmount).toFixed(2));
        }
        function ShowPopUP(productID, subCategoryID) {
            var popContaints = "";
            var listCategory = JsonObject.Category.length > 0 ? JsonObject.Category : "";
            for (i = 0; i < listCategory.length; i++) {
                if (Number(listCategory[i].SubCategoryID) == Number(subCategoryID)) {
                    var productArray = listCategory[i].Products;
                    for (j = 0; j < productArray.length; j++) {
                        if (Number(productArray[j].ProductID) == Number(productID)) {
                            var productImagePath = imagePath + productArray[j].LargeImage
                            popContaints = "<img src='" + productImagePath + "' alt='' id='popup-img' />";
                            popContaints += "<div id='content-popup' ><a href='javascript:;' class='cross-icon '><img src='images/icon-cross.png' alt='' onclick='Close();' /></a>";
                            popContaints += "<div style='cursor:pointer;' onclick='AddQtyToMyOrder(" + productArray[j].ProductID + "," + subCategoryID + ");'>";
                            popContaints += "<h2>" + productArray[j].Name + "</h2><ul>";
                            popContaints += "<li style='text-align: justify;'>" + productArray[j].SmallDesc + "</li>";
                            popContaints += "<li> Price $ " + Number(productArray[j].SalesPrice).toFixed(2) + "</li></ul>";
                            popContaints += "<p><a href='javascript:;'><img src='images/icon-add.png' alt='' /></a></p> </div>";
                            popContaints += "</div>";
                            break;
                        }
                    }
                    break;
                }
            }
            $("#popup-container").html(popContaints);
            $("#popup-container").show();
        }
        function Close() { $("#popup-container").hide(); }

        // save function
        function SaveTransition() {

            var txtPhoneNo = $("#txtPhoneNo").val();
            var txtEmailID = $("#txtEmailID").val();
            var txtName = $("#txtName").val();
            var whsCode = '<%=System.Configuration.ConfigurationManager.AppSettings["WebSaleWhsCode"] %>';
            var txtAdditionalRequest = $("#txtAdditionalRequest").val();
            var txtPickUpDate = "";
            //            txtPickUpDate = GetPictUpDate();
            //            var currentDate = new Date();
            //            if ((currentPickupDate < currentDate) && source != "1") {
            //                alert("Pick Up Date Time should be greater than current Date and Time");
            //                return;
            //            }
            var returnValue = "";
            var productDetails = GetReqGrdQuery();
            if (productDetails == null || productDetails == "") {
                returnValue = ("<%=Resources.Resource.lblAddedItemInMyOrder %>");
                alert(returnValue.replace("'", "\'"));
            }
            else {
                if ((txtName == null || txtName == "") && !(source == "1")) {
                    returnValue = ("<%=Resources.Resource.RfvYourName %>");
                    alert(returnValue.replace("'", "\'"));
                    return;
                }
                if ((txtPhoneNo == null || txtPhoneNo == "") && !(source == "1")) {
                    returnValue = ("<%=Resources.Resource.RfvYourPhone %>");
                    alert(returnValue.replace("'", "\'"));
                    return;
                }
                if (source == "1") {
                    txtName = "House";
                    txtPickUpDate = "";
                }
                $.ajax({
                    type: "POST",
                    url: "Restaurant-Menu.asmx/TransactionWithCustDetails",
                    data: "{userID:'<%= UserID %>',regCode:'HIT',totalAmount:'" + totalAmount + "',totalSubAmount:'" + subtotalAmount + "',whsCode:'" + whsCode + "',productDetails:'" + productDetails + "',tableNote:'" + txtName + "-',custPhoneNo:'" + txtPhoneNo + "',custPickUpDateTime:'" + txtPickUpDate + "',custAdditionalRequest:'" + txtAdditionalRequest + "',custEmailID:'" + txtEmailID + "',deviceID:'<%=DeviceID %>',token:'" + $('#<%=hdnToken.ClientID%>').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var returnData = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        var url = "javascript:;";
                        if (Number(returnData.ResponseCode) == 1 && returnData.Status == "OK")
                            url = "NewThankyou.aspx?lang=" + currentAppLanguageCode;
                        else
                            url = "NewMenuOrder.aspx?lang=" + currentAppLanguageCode;
                        totalMuOrderCount = 0;
                        SetMenuName();
                        var postDate = "";
                        var urlAdded = "";
                        if (returnData.TableNote != null) {
                            postDate += returnData.TableNote;
                            urlAdded = "&tableNote=" + returnData.TableNote
                        }
                        if (returnData.TransitionID != null) {
                            postDate += "&==&" + returnData.TransitionID;
                            urlAdded += "&TransitionID=" + returnData.TransitionID;
                        }

                        $(".divShowMyOrder").empty();
                        $(".divShowMyOrderList").empty();

                        if (source == "1")
                            if (source == "1")
                                url += "&source=1";
                        window.name = postDate;
                        if (isPayThroughPayPal == true) {
                            if (isPartialPayThroughLoyalPoint == true) {
                                window.location.href = url + urlAdded + "&isPartialPayThroughLoyalPoint=Yes" + "&amtToUpdateLP=" + partialPaidAmount;
                            }
                            else {
                                window.location.href = url + urlAdded;
                            }
                        }
                        else {
                            if (isPayThroughLoyalPoint == true) {
                                window.location.href = url + "&isPayThroughLoyalPoint=Yes" + "&TransitionID=" + returnData.TransitionID + "&amtToUpdateLP=" + subtotalAmount;
                            }
                            else {
                                if (isPartialPayThroughLoyalPoint == true) {
                                    window.location.href = url + "&isPartialPayThroughLoyalPoint=Yes" + "&TransitionID=" + returnData.TransitionID + "&amtToUpdateLP=" + partialPaidAmount;
                                }
                                else {
                                    window.location.href = url;
                                }
                            }
                        }
                    },
                    error: function (xhr, err) {
                        returnValue = ("<%=Resources.Resource.POSCashTransactionFailed %>");
                        alert(FormatUserErrorMessage("-3") + "\n " + FormatErrorMessage(xhr, err) + " \n " + returnValue.replace("'", "\'"));
                        var url = "NewMenuOrder.aspx?lang=" + currentAppLanguageCode;
                        if (source == "1")
                            if (source == "1")
                                url += "&source=1";
                        window.location.href = url;
                    }
                });
            }
        }

        var totalAmount = 0.0;
        var subtotalAmount = 0.0;
        var partialPaidAmount = 0.0;
        var taxDetails = "";
        function GetReqGrdQuery() {
            totalAmount = 0.0;
            taxDetails = "";
            subtotalAmount = 0.0;
            var prQuery = ""
            var taxAmount = 0.0;
            var listCategory = JsonObject.Category.length > 0 ? JsonObject.Category : "";
            for (i = 0; i < listCategory.length; i++) {
                var productArray = listCategory[i].Products;
                for (j = 0; j < productArray.length; j++) {
                    var productID = productArray[j].ProductID;
                    if (!(document.getElementById('spanMyOrder' + productID) == null) && !(document.getElementById('divMyOrder' + productID) == null)) {
                        var countLoop = 0;
                        var taxItem = "";
                        var productTax = productArray[j].ProductTax;
                        for (var pt = 0; pt < productTax.length; pt++) {
                            if (taxDetails != "")
                                taxDetails += "^";
                            if (taxItem != "")
                                taxItem += "^";
                            countLoop++;
                            taxItem += productTax[pt].TaxDesc + "~" + productTax[pt].Tax;
                            taxDetails += productTax[pt].TaxDesc + "~" + productTax[pt].Tax;

                            if (productTax[pt].IsOnTotal != "" && productTax[pt].IsOnTotal != null) {
                                if (productTax[pt].IsOnTotal == "False")
                                    taxAmount += (Number(productArray[j].SalesPrice) * Number($('#spanMyOrder' + productID).text()) * productTax[pt].Tax) / 100;
                                else
                                    taxAmount += productTax[pt].Tax;
                            }
                        }
                        for (var countTax = 0; countTax < 4 - countLoop; countTax++) {
                            if (countLoop > 0)
                                taxItem += "^";
                            taxItem += "No Tax~0.0";
                        }
                        prQuery += productID + "~" + $('#spanMyOrder' + productID).text() + "~" + productArray[j].SalesPrice + "~" + Number(productArray[j].SalesPrice) * Number($('#spanMyOrder' + productID).text()) + "__taxGrp__" + taxItem + "__item__";
                        subtotalAmount += Number(productArray[j].SalesPrice) * Number($('#spanMyOrder' + productID).text());
                        //totalAmount += Number(productArray[j].SalesPrice) * Number($('#spanMyOrder' + productID).text()) ;
                    }
                }
            }
            totalAmount = subtotalAmount + Number(taxAmount)
            return prQuery;
        }
        function load_xml_content_string(xmlData) {
            var dataStr = xmlData.documentElement.childNodes[0].nodeValue
            if (dataStr.indexOf("<") == 0) {
                dataStr = dataStr.substr(dataStr.indexOf(">") + 1, dataStr.length - 1);
            }
            return dataStr;
        }
        function ShowMenu() {
            $("#divProduct").hide();
            $("#divMyOrder").hide();
            $("#divMenu").show();
        }
        function ShowProductDetails() {
            $("#divProduct").show();
            $("#divMenu").hide();
            $("#divMyOrder").hide();
        }
        function ShowMyOrder() {

            if (totalMuOrderCount != 0) {
                $("#divProduct").hide();
                $("#divMenu").hide();
                $("#divMyOrder").show();
                GetReqGrdQuery();
                $("#ltrTotalAmt").html("$" + Number(totalAmount).toFixed(2));
            }
            else {
                alert("<%=Resources.Resource.lblMinQtyReq %>");
            }
        }
        var totalMuOrderCount = 0;
        $(document).ready(function () {
            $("#divProduct").hide();
            $("#divMyOrder").hide();
            $("#divMenu").show();
            $(".spanTotalMyOrder").html(totalMuOrderCount);

        });
        function Reset() {
            totalMuOrderCount = 0;
            SetMenuName();
            $(".divShowMyOrder").empty();
            $(".spanTotalMyOrder").html(totalMuOrderCount);
        }
        function ChangeCulture() {
            var url = "newmenuorder.aspx?lang="
            if (currentAppLanguageCode == "en")
                url += "fr";
            else
                url += "en";
            if (source == "1")
                url += "&source=1";
            window.location.href = url;
        }
        $(document).ready(function () {
            $('#pMySetting').click(function () { });
        });
        function MyProfile() {
            var url = "signUp.aspx?lang="
            if (currentAppLanguageCode == "en")
                url += "en";
            else
                url += "fr";
            if (source == "1")
                url += "&source=1";
            window.location.href = url;
        }
        function SetMenuName() {
            var menuHTML = "<div class='btm-content flt'>";
            if (totalMuOrderCount == 0) {
                //menuHTML += "<p > " + '<%=Resources.Resource.lblMenu %>' + " </p></div>";
                menuHTML += "<p id='pMySetting' onclick='MyProfile()' style='cursor:pointer;'> " + '<%=Resources.Resource.lblMyProfile%>' + " </p></div>";
                menuHTML += "<div class='btm-content flr'><p onclick='ChangeCulture()' style='text-align:right; padding-right:5px;'>";
                menuHTML += "<a href='javascript:;'><span style='float:left;' >";
                //                if (currentAppLanguageCode == "en")
                //                    menuHTML += "Française";
                //                else
                //                    menuHTML += "English";
                menuHTML += "</span></a></p> </div>";
                $("#divSelectMenu").html(menuHTML);
                $(".spanCheckOut").html('<%=Resources.Resource.lblCheckout %>');
                $(".spanCheckOut").css('font-size', '25px');
            }
            else {
                menuHTML += "<p  onclick='Reset();'> <a href='javascript:;'>";
                menuHTML += '<%=Resources.Resource.btnCancel %>' + "</a> </p> </div>";
                menuHTML += "<div class='btm-content flr'> <p onclick='ShowMyOrder()' style='text-align:right; padding-right:5px;'>";
                menuHTML += "<a href='javascript:;'><span style='float:left;' class='spanCheckOut'>";
                menuHTML += '<%=Resources.Resource.lblCheckout %>' + "</span>";
                menuHTML += " <sup><b> <span  style='float:left; padding-left:5px;' class='spanTotalMyOrder'></span></b> </sup> ";
                menuHTML += "<span style='float:left;'> &nbsp;<img src='images/icon-next.png' alt='' /></span> </a></p> </div>";
                $("#divSelectMenu").html(menuHTML);
                $(".spanCheckOut").html("<b>" + $(".spanCheckOut").html() + "</b");
                $(".spanCheckOut").css('font-size', '27px');
            }
            $(".spanTotalMyOrder").html(totalMuOrderCount);
        }
        $('input[placeholder], textarea[placeholder]').placeholder();

        var jsonShopTime = "";
        function GetShopTime() {
            $.ajax({
                type: "POST",
                url: 'Restaurant-Menu.asmx/ShopTime',
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    jsonShopTime = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    var txtPickUpDate = $("#txtPickUp");
                    ShowShopTime(txtPickUpDate.val());
                },
                error: function (xhr, err) {
                    alert(FormatUserErrorMessage("-3") + "<br/>" + FormatErrorMessage(xhr, err));
                }
            });
        }

        function ShowShopTime(selectDate) {
            var date = new Date(selectDate);
            var day = date.getDay();
            $("#pShowShopHHMM").show();
            $("#spanShopTime").html("");
            $("#ddlHH").html("");
            var dateCurrent = new Date();
            dateCurrent.setMinutes(dateCurrent.getMinutes() + 15, 0, 0);
            var currentHH = dateCurrent.getHours();
            var currentMM = dateCurrent.getMinutes();
            var selectIndx = "0";
            if (currentMM <= 15)
                selectIndx = "1";
            else {
                if (currentMM > 15 && currentMM <= 30)
                    selectIndx = "2";
                else {
                    if (currentMM > 30 && currentMM <= 45)
                        selectIndx = "3";
                    else {
                        currentHH = currentHH + 1;
                        selectIndx = "0";
                    }
                }
            }

            for (var i = 0; i < jsonShopTime.ShopTime.length; i++) {
                if (jsonShopTime.ShopTime[i].Day == (day + 1)) {
                    if ($("#spanShopTime").html() != "") {
                        $("#spanShopTime").append(" OR " + jsonShopTime.ShopTime[i].TimeFrom + " - ")
                    }
                    else
                        $("#spanShopTime").append("[ " + jsonShopTime.ShopTime[i].TimeFrom + " - ")

                    $("#spanShopTime").append(jsonShopTime.ShopTime[i].TimeTo)
                    var timeFrom = Number(jsonShopTime.ShopTime[i].TimeFrom.split(':')[0]);
                    var timeTo = Number(jsonShopTime.ShopTime[i].TimeTo.split(':')[0]);

                    while (timeFrom < timeTo) {
                        var optionddlHH = "";
                        optionddlHH = "<option value='" + timeFrom + "' ";
                        if (Number(timeFrom) == Number(currentHH))
                            optionddlHH += "selected='selected'";
                        optionddlHH += " >" + timeFrom + "</option>";
                        $("#ddlHH").append(optionddlHH);
                        timeFrom = timeFrom + 1;
                    }
                    var ddlMM = document.getElementById("ddlMM").selectedIndex = selectIndx;
                }
            }
            $("#spanShopTime").append(" ]");
        }
        var currentPickupDate;
        function GetPictUpDate() {
            var pickUpDate = "";
            var txtPickUpDate = $("#txtPickUp");
            if (txtPickUpDate.val() != "" && txtPickUpDate.val() != null) {
                pickUpDate = txtPickUpDate.val();
                var ddlHH = document.getElementById("ddlHH");
                var ddlHHOption = ddlHH.options[ddlHH.selectedIndex].text;
                var ddlMM = document.getElementById("ddlMM");
                var ddlMMOption = ddlMM.options[ddlMM.selectedIndex].text;
                currentPickupDate = new Date(pickUpDate);
                pickUpDate += " " + ddlHHOption;
                pickUpDate += ":" + ddlMMOption + ":00";
                currentPickupDate.setHours(ddlHHOption, ddlMMOption, 00, 00);
            }
            return pickUpDate;
        }
    </script>
    <script type="text/javascript">
        $(function () {
            var date = new Date();
            $("#txtPickUp").datepicker({
                buttonImage: 'images/calender.png',
                buttonImageOnly: true,
                showOn: 'both',
                autoSize: true,
                dateFormat: 'yy-mm-dd',
                onSelect: function (dateText) {
                    ShowShopTime(dateText);
                }
            });
            $('#txtPickUp').datepicker('setDate', 'today');

        });

        $(document).ready(function () {
            $(".ui-datepicker-trigger").css("padding", "0 20px 0 5px");
            $(".ui-datepicker-trigger").css("cursor", "pointer");
        });
        function limitText(limitField, limitCount, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                limitCount.value = limitNum - limitField.value.length;
            }
        }

        
    </script>
</head>
<body id="body">
    <div data-role="page" id="index">
        <form id="form1" runat="server">
        <div id="divMenu">
            <div id="main-container">
                <header>           
           <div  id="divSelectMenu" >
                     
            </div>
            
          </header>
                <section class="content">
        	<article class="category">
             <center>
               <div id="divCategory">
               <div style="font-size:40px;font-family: MS Shell Dlg; ">
               <%=Resources.Resource.lblLoading %>
               </div>
                </div>  
             </center>          
            </article>            
        </section>
            </div>
        </div>
        <div id="divProduct">
            <div class="main-container">
                <header>
        	<div class="btm-content flt" >
            	<p onclick="ShowMenu();">
                	<a href="javascript:;"><img src="images/icon-pre.png" alt="" />
               <%=Resources.Resource.lblMenu %></a>
                </p>
            </div>
           <div class="btm-content flr" >
            <p onclick="ShowMyOrder()" style="text-align:right; padding-right:5px;">
                	<a href="javascript:;"> 
                     <span style="float:left;" class="spanCheckOut" > <%=Resources.Resource.lblCheckout %></span> 
                     <sup  ><b> <span style="float:left; padding-left:5px;" class="spanTotalMyOrder"></span></b></sup>
                    
                   <span style="float:right;"> &nbsp;<img src="images/icon-next.png" alt="" /></span>
                    </a>
                </p>             
            </div>
          </header>
                <section class="content">
        	<div id="item-list">   
            <center>
            <div class="divShowProduct" ></div>
                <div id="popup-container"></div>
              </center>            
            </div>
        </section>
            </div>
        </div>
        <div id="divMyOrder">
            <div class="main-container">
                <header>
        	<div class="btm-content flt" >
            	<p onclick="ShowMenu();">
                	<a href="javascript:;"><img src="images/icon-pre.png" alt="" />
               <%= Resources.Resource.lblBack%></a>
                </p> </div>
                   <div class="btm-content flr" >
            <p onclick="PaymentOption();" style="text-align:right; padding-right:5px;" >
                	<a href="javascript:;"><%= Resources.Resource.lblPlaceMyOrder%>&nbsp;
                    <img src="images/icon-next.png" alt="" /></a>
                </p>             
            </div>              
          </header>
                <section class="content">
         <article class="order-area">
              <div class="order-content-container">
                 <div class="my-form">
                 <p class="flt txt-area">
                       <input type="text"  class="form-input"  id="txtEmailID" placeholder="Your Email ID to notify you, when order is ready" /><b> <span class="spanRed">*</span></b>
                    </p> 
                    <div class="clr"></div>
                   <br />
                    <p class="flt txt-area">
                      <input type="text"  class="form-input"  id="txtName" placeholder="Your Name " /> <b> <span class="spanRed">*</span></b>
                     </p> 
                    <div class="clr"></div>
                    <br /> 
                    <p class="flt txt-area">
                      <input type="text"  class="form-input"  id="txtPhoneNo" placeholder="Your Phone No.. if we need to call you " /> <b> <span class="spanRed">*</span></b>
                    </p> 
                    <div class="clr"></div>
                    <br />              
                    
                    <p class="flt txt-area"   style="font-family:MS Shell Dlg; word-break:break-all; font-size:13px">
                    <b><%=Resources.Resource.lblPickUpTime%> </b> </p>                  
                    <div class="clr"></div>
                 <div style="display:none;">
                  <br />
                    <p class="flt txt-area">
                    <b> Pick up at</b> <span id="spanShopTime"></span>    
                    </p>
                    <div class="clr"></div>
                    <br />
                     <p class="flt txt-area" >
                       <input type="text"  class="form-input"  id="txtPickUp" placeholder="Pick up your Time"  style="width:90px;" />       
                      </p>    
                       <p class="flt txt-area" id="pShowShopHHMM">
                       <span style="float:left;width:75px; padding-right:5px;">
                       <select id="ddlHH" style="width:60px; height:30px;" class="form-input">
                        </select> 
                        </span>
                       <span style="float:left;">
                        <select id="ddlMM" style="width:55px; height:30px;" class="form-input" >
                         <option value="00" selected="selected">00</option>
                         <option value="15"  >15</option>
                         <option value="30" >30</option>
                         <option value="45" >45</option>
                        </select>
                        </span>
                      </p>
                         <div class="clr"></div>
                   </div>
                    <br />              
                    <p class="flt txt-area">                     
                       <textarea  style="height: 64px;" id="txtAdditionalRequest" class="form-input" placeholder=""
                       name="AdditionalRequest"
                       onkeydown="limitText(this.form.AdditionalRequest,this.form.countdown,100);"
                        onkeyup="limitText(this.form.AdditionalRequest,this.form.countdown,100);"
                       ></textarea>
                       <br />                        
                        <input readonly="readonly" type="text" name="countdown" size="3" value="100" style="border: 0px;
                            width: 25px; display:none;" />
                        <%--characters left.--%>
                    </p> 
                     <div class="clr"></div>
                      <p class="flt txt-area">
                     <span class="spanRed" style="font-family:MS Shell Dlg;font-size:14px"><b></b><%= Resources.Resource.lblFieldRequired %></span>
                     </p>
                      <div class="clr"></div>
                      <br />
                      <p class="flt order-content">
                    <strong><%= Resources.Resource.lblTotal%>
                            <literal id="ltrTotalAmt"></literal> </strong> </p>  
                      <div class="clr"></div>
                 </div> 
              </div>
            	<h1 style="color:#de6b37;"><%= Resources.Resource.lblMyOrder %></h1>
                <div class="order-content-container"> 
                <div class="divShowMyOrder" ></div>
                <div class="clr"></div>           
                  <div class="header">       
                  <center>
                     <div class="btm-content" >                      
                       <p onclick="PaymentOption();" >
                         <a href="javascript:;"><%= Resources.Resource.lblPlaceMyOrder%>&nbsp;
                               <img src="images/icon-next.png" alt="" />
                          </a>
                       </p>                               
                   </div>    
                   </center>                                   
                 </div>                                                                                                        
             </div>
          </article>
        </section>
            </div>
        </div>
        <asp:HiddenField ID="hdnToken" runat="server" />
        <div id="divPop" style="display: none; background-color: rgba(10,10,10,.4); text-align: center;
            position: absolute; top: 0; width: 100%; height: 100%;">
            <%--height: 100%; width: 100%; --%>
            <div style="width: 100%; display: none;">
                <image src="../Images/cancel1.png" style="float: right; cursor: pointer;" onclick="closedialog()"></image>
            </div>
            <div id="divPopinner" style="margin: auto; top: 42%; width: 80%; display: block;
                background-color: White; position: relative;">
                <div style="color: white; font-family: ArcherProBold; font-size: 27px; font-weight: normal;
                    text-align: center; padding: 20px 0;">
                    <div style="background-color: #DE6B37; width: 80%; cursor: pointer; margin: auto;"
                        id="divPayNow">
                        <%= Resources.Resource.lblPayNow%>&nbsp;
                        <img src="images/icon-next.png" alt="" />
                    </div>
                    <div class="clr" style="height: 20px;">
                    </div>
                    <div style="background-color: #DE6B37; width: 80%; cursor: pointer; margin: auto;"
                        id="divPayAtKiosk">
                        <%= Resources.Resource.lblPayAtKiosk%>&nbsp;
                        <img src="images/icon-next.png" alt="" />
                    </div>
                    <div class="clr" style="height: 20px;">
                    </div>
                    <div style="background-color: #DE6B37; width: 80%; cursor: pointer; margin: auto;"
                        id="divPayLoyaltyPoints">
                        <%= Resources.Resource.lblPayLoyaltyPoints%>&nbsp;
                        <img src="images/icon-next.png" alt="" />
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">

            function closedialog() {
                $("#divPop").hide();
            }

            var isPayThroughPayPal = false;
            var isPayThroughLoyalPoint = false;
            var isPartialPayThroughLoyalPoint = false;
            $(document).ready(function () {

                //blockContentArea($("#divPop"), "Loading...");  //Screen Blocking start

                $("#divPayNow").click(function () {

                    $("#divPop").hide();
                    isPayThroughLoyalPoint = false; isPayThroughPayPal = true; SaveTransition();
                });
                $("#divPayAtKiosk").click(function () {
                    $("#divPop").hide();
                    isPayThroughLoyalPoint = false; isPayThroughPayPal = false; SaveTransition();
                });
                $("#divPayLoyaltyPoints").click(function () {

                    IsValidLoyalPoints();
                });



            });
            function IsValidLoyalPoints() {
                //var rStatus = "false";
                GetReqGrdQuery();
                $.ajax({
                    type: "POST",
                    url: "NewMenuOrder.aspx/IsValidLoyalPoints",
                    data: "{partnerID:'<%=UserID %>',amtToCheckLoyalPoints:" + totalAmount + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var JsonPartner = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (JsonPartner.ResponseCode == 1 && JsonPartner.Status == "OK") {
                            if (JsonPartner.IsValidStatus == "false") {
                                var resourceText = ("<%=Resources.Resource.lblNotEnoughLoyaltyPoint %>").replace("@avlpoint", JsonPartner.avlpoint).replace("@minpoint", JsonPartner.minpoint);
                                alert(resourceText);
                            }
                            else if (JsonPartner.IsValidStatus == "true") {
                                $("#divPop").hide();
                                isPayThroughLoyalPoint = true;
                                isPayThroughPayPal = false;
                                SaveTransition();
                            }
                            else {
                                //var resourceText = ("<%=Resources.Resource.lblNotEnoughLoyaltyPoint %>").replace("@avlpoint", JsonPartner.avlpoint).replace("@minpoint", JsonPartner.minpoint);
                                //alert(resourceText);
                                alert("Amount " + JsonPartner.unPaidAmt + " left for paying");
                                isPartialPayThroughLoyalPoint = true;
                                partialPaidAmount = JsonPartner.paidAmt;
                            }
                        }
                        else { alert(FormatUserErrorMessage(JsonPartner.ResponseCode)); }
                    },
                    error: function (xhr, err) {
                        alert(FormatUserErrorMessage("-3") + "\n" + FormatErrorMessage(xhr, err));
                    }
                });
            }

            function PaymentOption() {

                //IsValidLoyalPoints();
                var appPaymentOption = '<%=System.Configuration.ConfigurationManager.AppSettings["AppPaymentOption"] %>';
                if (Number(appPaymentOption) == 1) { $("#divPayNow").hide(); $("#divPayNow").next("div").hide(); $("#divPayLoyaltyPoints").hide(); $("#divPayLoyaltyPoints").prev("div").hide(); }
                if (Number(appPaymentOption) == 3) { $("#divPayAtKiosk").hide(); $("#divPayLoyaltyPoints").hide(); $("#divPayLoyaltyPoints").prev("div").hide(); $("#divPayAtKiosk").prev("div").hide(); }
                if (Number(appPaymentOption) == 4) { $("#divPayNow").hide(); $("#divPayNow").next("div").hide(); $("#divPayAtKiosk").hide(); $("#divPayLoyaltyPoints").prev("div").hide(); }
                if (Number(appPaymentOption) == 5) { $("#divPayNow").hide(); $("#divPayNow").next("div").hide(); }
                $("#divPop").css("display", "block");
                showDivinCentre('divPopinner');
                $("#divPop").css("height", $(document).height());
                ScrollTop();
            }


            function showDivinCentre(divIDToHide) {

                var divWidth = $("#" + divIDToHide).css("width").replace("px", '');
                var divHeight = $("#" + divIDToHide).css("height").replace("px", '');
                var divId = divIDToHide; // id of the div that you want to show in center

                // Get the x and y coordinates of the center in output browser's window 
                var centerX, centerY;
                if (self.innerHeight) {
                    centerX = self.innerWidth;
                    centerY = self.innerHeight;
                }
                else if (document.documentElement && document.documentElement.clientHeight) {
                    centerX = document.documentElement.clientWidth;
                    centerY = document.documentElement.clientHeight;
                }
                else if (document.body) {
                    centerX = document.body.clientWidth;
                    centerY = document.body.clientHeight;
                }

                var offsetLeft = (centerX - divWidth) / 2;
                var offsetTop = (centerY - divHeight) / 2;

                // The initial width and height of the div can be set in the
                // style sheet with display:none; divid is passed as an argument to // the function
                var ojbDiv = document.getElementById(divId);

                ojbDiv.style.position = 'absolute';
                ojbDiv.style.top = offsetTop + 'px';
                ojbDiv.style.left = offsetLeft + 'px';
                ojbDiv.style.display = "block";

            }

            function AddItemToFav(productID) {
                $.ajax({
                    type: "POST",
                    url: "Restaurant-Menu.asmx/AddFavourateProduct",
                    data: "{partnerID:'<%=UserID %>',productID:'" + productID + "',logInID:'<%=EmailID %>',deviceID:'<%=DeviceID %>',token:'" + $('#<%=hdnToken.ClientID%>').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var JsonPartner = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (JsonPartner.ResponseCode == 1 && JsonPartner.Status == "OK") {
                            alert("<%=Resources.Resource.lblProductAddedFavList %>");
                        }
                        else { alert(FormatUserErrorMessage(JsonPartner.ResponseCode)); }
                    },
                    error: function (xhr, err) {
                        alert(FormatUserErrorMessage("-3") + "\n" + FormatErrorMessage(xhr, err));
                    }
                });
            }

            function RemoveItemToFav(productID) {
                $.ajax({
                    type: "POST",
                    url: "Restaurant-Menu.asmx/RemoveFavourateProduct",
                    data: "{partnerID:'<%=UserID %>',productID:'" + productID + "',logInID:'<%=EmailID %>',deviceID:'<%=DeviceID %>',token:'" + $('#<%=hdnToken.ClientID%>').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var JsonPartner = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (JsonPartner.ResponseCode == 1 && JsonPartner.Status == "OK") {
                            alert("<%=Resources.Resource.lblProductRemoveFavList %>");
                            ShowFavourateProduct();
                        }
                        else { alert(FormatUserErrorMessage(JsonPartner.ResponseCode)); }
                    },
                    error: function (xhr, err) {
                        alert(FormatUserErrorMessage("-3") + "\n" + FormatErrorMessage(xhr, err));
                    }
                });
            }

            function ShowFavourateProduct() {
                $.ajax({
                    type: "POST",
                    url: "Restaurant-Menu.asmx/FavourateProduct",
                    data: "{partnerID:'<%=UserID %>',lang:'" + currentAppLanguageCode + "',logInID:'<%= EmailID %>',deviceID:'<%= DeviceID %>',token:'" + $('#<%=hdnToken.ClientID%>').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var favProducts = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (favProducts.ResponseCode == 1 && favProducts.Status == "OK") {
                            var productHTML = "";
                            productHTML = AddedFavourateProduct(favProducts.FavourateProduct);
                            $(".divShowProduct").html(productHTML);
                            if (isProductDetails == true)
                                ShowProductDetails();
                            isProductDetails = true;
                        }
                        else { $("#divCategory").html("<b>" + FormatUserErrorMessage(favProducts.ResponseCode) + "</b>"); }
                    },
                    error: function (xhr, err) {
                        $("#divCategory").html("<b>" + FormatUserErrorMessage("-3") + "<br/>" + FormatErrorMessage(xhr, err) + "</b>");
                    }
                });
            }

            function AddedFavourateProduct(product) {
                $("#popup-container").hide();
                var divValue = " <ul>";
                for (var i = 0; i < product.length; i++) {
                    var isShow = false;
                    var showOption = "";
                    divValue += " <li ><div style='float:left; width:12%; margin-right:10px; padding-right:3px;'> <a href='javascript:;' style='cursor:pointer;' onclick='ShowPopUP(" + product[i].ProductID + "," + product[i].SubCategoryID + ");'>";
                    var productImagePath = imagePath + product[i].SmallImage;
                    divValue += " <img src='" + productImagePath + "' alt='' class='round-img'    /></a> </div>";
                    divValue += "<div style='float:right; width:3%;cursor:pointer;'  onclick='AddQtyToMyOrder(" + product[i].ProductID + "," + product[i].SubCategoryID + ");'> <img style='margin-top:10px;max-height:30px; max-width:32px;' class='flr' src='images/icon-add.png' alt='' /> </div>";
                    divValue += "<div style='float:right; width:3%;margin-right:25px;cursor:pointer;'  onclick='RemoveItemToFav(" + product[i].ProductID + ");'> <img style='margin-top:10px;max-height:35px; max-width:30px;' class='flr' src='images/unfavourateicon.png' alt='' /> </div>"; // 20112013
                    divValue += " <div style='float:left; text-align:left; width:55%; cursor:pointer;'  onclick='AddQtyToMyOrder(" + product[i].ProductID + "," + product[i].SubCategoryID + ");'> ";
                    divValue += " <h2>" + product[i].Name + "<sup><b> <span style='color:Green;font-weight:bold;font-family:Tahoma;font-size:15px;' class='order-number' id='spanMyOrder1" + product[i].ProductID + "' ></span></b> </sup></h2>";
                    divValue += "<p style='text-align: justify;'>" + product[i].SmallDesc + "</p>";

                    if (product[i].IsGlutenFree == "True") {
                        showOption = "Gluten Free";
                        isShow = true;
                    }
                    if (product[i].IsVegetarian == "True") {
                        if (showOption != "")
                            showOption += ",  ";
                        showOption += "Vegetarian";
                        isShow = true;
                    }
                    if (product[i].IsContainsNuts == "True") {
                        if (showOption != "")
                            showOption += ",  ";
                        showOption += "Contains Nuts";
                        isShow = true;
                    }
                    if (product[i].IsCookedSushi == "True") {
                        if (showOption != "")
                            showOption += ",  ";
                        showOption += "Cooked Sushi";
                        isShow = true;
                    }
                    if (Number(product[i].SpicyLevel) > 0) {
                        if (showOption != "")
                            showOption += ",  ";
                        showOption += "Spicy Level " + product[i].SpicyLevel;
                        isShow = true;
                    }
                    divValue += "<div style='clear:both;'></div> ";
                    if (isShow)
                        divValue += "<p style='float:left; text-align: justify;'>" + showOption + "</p>";
                    divValue += "<div style='clear:both;'></div> ";
                    divValue += " <p style='float:left;text-align:left;'> Price  $ " + Number(product[i].SalesPrice).toFixed(2) + "</p></div>";
                    divValue += "<div style='clear:both;'></div> ";
                    divValue += "</li>";
                }
                divValue += " </ul>";
                return divValue;
            }

        </script>
        </form>
    </div>
</body>
<script type="text/javascript">
    function blockContentArea(elementToBlock, loadingMessage) {
        $(elementToBlock).block({
            message: '<div>' + loadingMessage + '</div>',
            css: { border: '3px solid #a00' }
        });
    }


    function ScrollTop() {
        $("html, body").animate({ scrollTop: 0 }, 0);
        return false;
    } 
</script>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MenuOrder.aspx.cs" Inherits="Restaurant_Menu_MenuOrder" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Restaurant - Menu Choice your Order </title>
    <meta charset="utf-8" />
    <meta name='viewport' content='width=device-width,initial-scale=1,maximum-scale=1'
        userscale='no' />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/jquery.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-ui-1.8.11.custom.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="JS/jquery-ui.css" />

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', '<%=System.Configuration.ConfigurationManager.AppSettings["JavaScriptCode"] %>', 'itechcanada.com');
            ga('send', 'pageview');

    </script>

    <script type="text/javascript">
        var JsonObject;
        var currentAppLanguageCode = "en";
        currentAppLanguageCode = '<%= iTECH.InbizERP.BusinessLogic.Globals.CurrentAppLanguageCode %>';
        var source = "";
        source = '<%= Request.QueryString["source"] %>';
        $(document).ready(function () {
            if (source == "1") {
                $(".my-form").hide();
                $("#left-content1 #order-content-container1").css("height", "380px");
            }
            $('input[placeholder], textarea[placeholder]').css("font-family", "MS Shell Dlg");
            $('input[placeholder], textarea[placeholder]').css("font-size", "13px");

            if ('<%= IsValidToken %>' == "True") {
                $("#pShowShopHHMM").hide();
                $("#popup-container").hide();
                $.ajax({
                    type: "POST",
                    url: "Restaurant-Menu.asmx/CategoryANDProduct",
                    data: "{lang:'" + currentAppLanguageCode + "',logInID:'<%= EmailID %>',deviceID:'<%= DeviceID %>',token:'<%= TokenNo %>'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        JsonObject = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (JsonObject.ResponseCode == 1 && JsonObject.Status == "OK") {
                            var listCategory = JsonObject.Category.length > 0 ? JsonObject.Category : "";
                            var innerHTMLValue = "";
                            $("#divCategory").html("");
                            for (i = 0; i < listCategory.length; i++) {
                                if (i % 2 == 0) {
                                    innerHTMLValue = "<figure class='button-container' style='clear:both;'>";
                                    innerHTMLValue += "<a href='javascript:;'  onclick='ShowProduct(\"" + listCategory[i].SubCategoryID + "\");'>" + listCategory[i].SubCategory + "</a>";
                                }
                                else {
                                    innerHTMLValue += "<a href='javascript:;'  class='margin-right' onclick='ShowProduct(\"" + listCategory[i].SubCategoryID + "\");'>" + listCategory[i].SubCategory + "</a>";
                                    innerHTMLValue += "</figure>";
                                    $("#divCategory").append(innerHTMLValue);
                                }
                                if (i == 0) { ShowProduct(listCategory[i].SubCategoryID); }
                            }
                            if (Number(listCategory.length) % 2 != 0) {
                                innerHTMLValue += "<span class='margin-right' ></span></figure>";
                                $("#divCategory").append(innerHTMLValue);
                            }
                        }
                        else { $("#divCategory").html("<b>" + FormatUserErrorMessage(JsonObject.ResponseCode) + "</b>"); }
                    },
                    error: function (xhr, err) {
                        $("#divCategory").html("<b>" + FormatUserErrorMessage("-3") + "<br/>" + FormatErrorMessage(xhr, err) + "</b>");
                    }
                });
                GetShopTime();
            }
            else $("#divCategory").html("<b>" + FormatUserErrorMessage("-3") + "<br/>" + FormatUserErrorMessage("-2") + "</b>");
        });

        function FormatUserErrorMessage(responseCode) {
            var returnValue = "";
            if (responseCode == "-3")
                returnValue = "<%=Resources.Resource.lblWeAreHavingTechProb%>";
            else if (responseCode == "-2")
                returnValue = "<%=Resources.Resource.lblInVaildToken %>";
            else if (responseCode == "-1")
                returnValue = "<%=Resources.Resource.lblOperationUnsuccess%>";
            else
                returnValue = "<%=Resources.Resource.lblUnKnowError%>";
            return returnValue.replace("'", "\'");
        }

        function FormatErrorMessage(jqXHR, exception) {
            var returnValue = "";
            if (Number(jqXHR.status) == 0)
                returnValue = ("<%=Resources.Resource.lblErrorNetworkConnection %>");
            else if (Number(jqXHR.status) == 404)
                returnValue = ("<%=Resources.Resource.lblErrorPageNotfound %>");
            else if (Number(jqXHR.status) == 500)
                returnValue = ("<%=Resources.Resource.lblErrorInternalServerError %>");
            else if (exception == 'parsererror')
                returnValue = ("<%=Resources.Resource.lblErrorRequestedJSONParseFailed %>");
            else if (exception == 'timeout')
                returnValue = ("<%=Resources.Resource.lblErrorTimeOut %>");
            else if (exception == 'abort')
                returnValue = ("<%=Resources.Resource.lblErrorAjaxRequestAborted %>");
            else
                returnValue = ("<%=Resources.Resource.lblUnKnowError %> \n" + jqXHR.responseText);
            return returnValue.replace("'", "\'");
        }

        var imagePath = '<%= System.Configuration.ConfigurationManager.AppSettings["Restaurant_Image_Path"] %>';
        function AddedProduct(product, subCategoryID) {
            $("#popup-container").hide();
            var divValue = " <ul>";
            for (var i = 0; i < product.length; i++) {
                divValue += " <li ><div style='float:left; width:12%;'> <a href='javascript:;' style='cursor:pointer;' onclick='ShowPopUP(" + product[i].ProductID + "," + subCategoryID + ");'>";
                var productImagePath = imagePath + product[i].SmallImage;
                divValue += " <img src='" + productImagePath + "' alt='' class='round-img'    /></a> </div>";

                divValue += "<div style='float:right; width:8%;cursor:pointer;'  onclick='AddQtyToMyOrder(" + product[i].ProductID + "," + subCategoryID + ");'> <img style='margin-top:10px;' class='flr' src='images/icon-add.png' alt='' /> </div>";
                divValue += " <div style='float:right; width:79%; cursor:pointer;'  onclick='AddQtyToMyOrder(" + product[i].ProductID + "," + subCategoryID + ");'> ";
                divValue += " <h2>" + product[i].Name + "</h2>";
                divValue += "<p style='text-align: justify;'>" + product[i].SmallDesc + "</p>";
                divValue += " <br/><p> Price  $ " + Number(product[i].SalesPrice).toFixed(2) + "</p></div>";
                divValue += "<div style='clear:both;'></div> ";
                divValue += "</li>";
            }
            divValue += " </ul>";
            return divValue;
        }
        function ShowProduct(subCategoryID) {
            var productHTML = "";
            var listCategory = JsonObject.Category.length > 0 ? JsonObject.Category : "";
            for (i = 0; i < listCategory.length; i++) {
                if (Number(listCategory[i].SubCategoryID) == Number(subCategoryID)) {
                    productHTML = AddedProduct(listCategory[i].Products, subCategoryID);
                    break;
                }
            }
            $(".divShowProduct").html(productHTML);
        }

        function AddQtyToMyOrder(productID, subCategoryID) {
            var addMyOrder = "";
            if (!(document.getElementById('spanMyOrder' + productID) == null)) {
                var qty = $('#spanMyOrder' + productID).text();
                $('#spanMyOrder' + productID).text(Number(qty) + 1);

                var modifyDivHTML = "<div class='my-order' id='divMyOrder" + productID + "' >" + $("#divMyOrder" + productID).html() + " </div";
                $('#divMyOrder' + productID).remove()
                var htmlData = $(".divShowMyOrder").html();
                $(".divShowMyOrder").html(modifyDivHTML);
                $(".divShowMyOrder").append(htmlData);
            }
            else {
                var listCategory = JsonObject.Category.length > 0 ? JsonObject.Category : "";
                for (i = 0; i < listCategory.length; i++) {
                    if (Number(listCategory[i].SubCategoryID) == Number(subCategoryID)) {
                        var productArray = listCategory[i].Products;
                        for (j = 0; j < productArray.length; j++) {
                            if (Number(productArray[j].ProductID) == Number(productID)) {
                                addMyOrder = " <div class='my-order' id='divMyOrder" + productArray[j].ProductID + "' ><p class='flt order-content'> <strong> ";
                                addMyOrder += productArray[j].Name + "</strong> </p>";
                                addMyOrder += "<p class='flr order-right-area'><span class='order-number' id='spanMyOrder" + productArray[j].ProductID + "' >1</span>";
                                addMyOrder += "<a href='javascript:;'><img src='images/icon-less.png' alt='' onclick='RemoveQtyFromMyOrder(" + productID + ")' /> </a></p> <div class='clr'></div>   </div";
                                break;
                            }
                        }
                        break;
                    }
                }
                var htmlData = $(".divShowMyOrder").html();
                $(".divShowMyOrder").html(addMyOrder);
                $(".divShowMyOrder").append(htmlData);
            }
            Close();
        }
        function RemoveQtyFromMyOrder(productID) {
            if (!(document.getElementById('spanMyOrder' + productID) == null)) {
                var qty = $('#spanMyOrder' + productID).text();
                if (Number(qty) > 0) {
                    $('#spanMyOrder' + productID).text(Number(qty) - 1);
                    if (Number($('#spanMyOrder' + productID).text()) == 0)
                        $('#divMyOrder' + productID).remove();
                }
                else {
                    $('#divMyOrder' + productID).remove();
                }
            }
        }
        function ShowPopUP(productID, subCategoryID) {
            var popContaints = "";
            var listCategory = JsonObject.Category.length > 0 ? JsonObject.Category : "";
            for (i = 0; i < listCategory.length; i++) {
                if (Number(listCategory[i].SubCategoryID) == Number(subCategoryID)) {
                    var productArray = listCategory[i].Products;
                    for (j = 0; j < productArray.length; j++) {
                        if (Number(productArray[j].ProductID) == Number(productID)) {
                            var productImagePath = imagePath + productArray[j].LargeImage
                            popContaints = "<img src='" + productImagePath + "' alt='' id='popup-img' />";
                            popContaints += "<div id='content-popup'><a href='javascript:;' class='cross-icon '><img src='images/icon-cross.png' alt='' onclick='Close();' /></a>";
                            popContaints += "<h2>" + productArray[j].Name + "</h2><ul>";
                            popContaints += "<li style='text-align: justify;'>" + productArray[j].SmallDesc + "</li>";
                            popContaints += "<li> Price $ " + Number(productArray[j].SalesPrice).toFixed(2) + "</li></ul>";
                            popContaints += "<p><a href='javascript:;'><img src='images/icon-add.png' alt='' onclick='AddQtyToMyOrder(" + productArray[j].ProductID + "," + subCategoryID + ");'/></a></p> </div>";
                            break;
                        }
                    }
                    break;
                }
            }
            $("#popup-container").html(popContaints);
            $("#popup-container").show();
        }
        function Close() { $("#popup-container").hide(); }

        // save function
        function SaveTransition() {

            var txtPhoneNo = $("#txtPhoneNo").val();
            var txtEmailID = $("#txtEmailID").val();
            var txtName = $("#txtName").val();
            var whsCode = '<%=System.Configuration.ConfigurationManager.AppSettings["WebSaleWhsCode"] %>';
            var txtAdditionalRequest = $("#txtAdditionalRequest").val();
            var txtPickUpDate = "";
            //            txtPickUpDate = GetPictUpDate();
            //            var currentDate = new Date();
            //            if ((currentPickupDate < currentDate) && source != "1") {
            //                alert("Pick Up Date Time should be greater than current Date and Time");
            //                return;
            //            }
            var returnValue = "";
            var productDetails = GetReqGrdQuery();
            if (productDetails == null || productDetails == "") {
                returnValue = ("<%=Resources.Resource.lblAddedItemInMyOrder %>");
                alert(returnValue.replace("'", "\'"));
            }
            else {
                if ((txtName == null || txtName == "") && !(source == "1")) {
                    returnValue = ("<%=Resources.Resource.RfvYourName %>");
                    alert(returnValue.replace("'", "\'"));
                    return;
                }
                if ((txtPhoneNo == null || txtPhoneNo == "") && !(source == "1")) {
                    returnValue = ("<%=Resources.Resource.RfvYourPhone %>");
                    alert(returnValue.replace("'", "\'"));
                    return;
                }
                if (source == "1") {
                    txtName = "House";
                    txtPickUpDate = "";
                }

                $.ajax({
                    type: "POST",
                    url: "Restaurant-Menu.asmx/TransactionWithCustDetails",
                    data: "{userID:'<%= UserID %>',regCode:'HIT',totalAmount:'" + totalAmount + "',totalSubAmount:'" + subtotalAmount + "',whsCode:'" + whsCode + "',productDetails:'" + productDetails + "',tableNote:'" + txtName + "-',custPhoneNo:'" + txtPhoneNo + "',custPickUpDateTime:'" + txtPickUpDate + "',custAdditionalRequest:'" + txtAdditionalRequest + "',custEmailID:'" + txtEmailID + "',deviceID:'<%=DeviceID %>'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        var returnData = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        var url = "javascript:;";
                        if (Number(returnData.ResponseCode) == 1 && returnData.Status == "OK")
                            url = "Thankyou.aspx?lang=" + currentAppLanguageCode;
                        else
                            url = "MenuOrder.aspx?lang=" + currentAppLanguageCode;

                        var postDate = "";
                        if (returnData.TableNote != null)
                            postDate += returnData.TableNote;
                        if (returnData.TransitionID != null)
                            postDate += "&==&" + returnData.TransitionID;

                        $(".divShowMyOrder").empty();
                        $(".divShowMyOrderList").empty();

                        if (source == "1")
                            if (source == "1")
                                url += "&source=1";
                        window.name = postDate;
                        window.location.href = url;
                    },
                    error: function (xhr, err) {
                        returnValue = ("<%=Resources.Resource.POSCashTransactionFailed %>");
                        alert("<b>" + FormatUserErrorMessage("-3") + "\n " + FormatErrorMessage(xhr, err) + "</b> \n " + returnValue.replace("'", "\'"));
                        var url = "MenuOrder.aspx?lang=" + currentAppLanguageCode;
                        if (source == "1")
                            if (source == "1")
                                url += "&source=1";
                        window.location.href = url;
                    }
                });
            }
        }
        var totalAmount = 0.0;
        var subtotalAmount = 0.0;
        var taxDetails = "";
        function GetReqGrdQuery() {
            totalAmount = 0.0;
            taxDetails = "";
            subtotalAmount = 0.0;
            var prQuery = ""
            var taxAmount = 0.0;
            var listCategory = JsonObject.Category.length > 0 ? JsonObject.Category : "";
            for (i = 0; i < listCategory.length; i++) {
                var productArray = listCategory[i].Products;
                for (j = 0; j < productArray.length; j++) {
                    var productID = productArray[j].ProductID;
                    if (!(document.getElementById('spanMyOrderList' + productID) == null) && !(document.getElementById('divMyOrderList' + productID) == null)) {
                        var countLoop = 0;
                        var taxItem = "";
                        var productTax = productArray[j].ProductTax;
                        for (var pt = 0; pt < productTax.length; pt++) {
                            if (taxDetails != "")
                                taxDetails += "^";
                            if (taxItem != "")
                                taxItem += "^";
                            countLoop++;
                            taxItem += productTax[pt].TaxDesc + "~" + productTax[pt].Tax;
                            taxDetails += productTax[pt].TaxDesc + "~" + productTax[pt].Tax;
                            if (productTax[pt].IsOnTotal == "False")
                                taxAmount += (Number(productArray[j].SalesPrice) * Number($('#spanMyOrderList' + productID).text()) * productTax[pt].Tax) / 100;
                            else
                                taxAmount += productTax[pt].Tax;
                        }
                        for (var countTax = 0; countTax < 4 - countLoop; countTax++) {
                            if (countLoop > 0)
                                taxItem += "^";
                            taxItem += "No Tax~0.0";
                        }
                        prQuery += productID + "~" + $('#spanMyOrderList' + productID).text() + "~" + productArray[j].SalesPrice + "~" + Number(productArray[j].SalesPrice) * Number($('#spanMyOrderList' + productID).text()) + "__taxGrp__" + taxItem + "__item__";
                        subtotalAmount += Number(productArray[j].SalesPrice) * Number($('#spanMyOrderList' + productID).text());
                        totalAmount += Number(productArray[j].SalesPrice) * Number($('#spanMyOrderList' + productID).text()) + Number(taxAmount);
                    }
                }
            }
            return prQuery;
        }
        function load_xml_content_string(xmlData) {
            var dataStr = xmlData.documentElement.childNodes[0].nodeValue
            if (dataStr.indexOf("<") == 0) {
                dataStr = dataStr.substr(dataStr.indexOf(">") + 1, dataStr.length - 1);
            }
            return dataStr;
        }
        function RemoveQtyFromMyOrderList(productID) {
            if (!(document.getElementById('spanMyOrderList' + productID) == null)) {
                var qty = $('#spanMyOrderList' + productID).text();
                if (Number(qty) > 0) {
                    $('#spanMyOrderList' + productID).text(Number(qty) - 1);
                    if (Number($('#spanMyOrderList' + productID).text()) == 0)
                        $('#divMyOrderList' + productID).remove();
                }
                else {
                    $('#divMyOrderList' + productID).remove();
                }
            }
        }
        $("#divOrderConfirm").hide();
        function ShowMyOrderList() {
            $("#divDetails").hide();
            $(".divShowMyOrderList").empty();
            AddProductFromMyOrderToMyOrderList();
            $("#divOrderConfirm").show();
        }
        function ShowMyOrder() {
            $("#divDetails").show();
            $("#divOrderConfirm").hide();
        }
        function AddProductFromMyOrderToMyOrderList() {
            var addMyOrderList = "";
            var listCategory = JsonObject.Category.length > 0 ? JsonObject.Category : "";
            for (i = 0; i < listCategory.length; i++) {
                var productArray = listCategory[i].Products;
                for (j = 0; j < productArray.length; j++) {
                    if (!(document.getElementById('spanMyOrder' + productArray[j].ProductID) == null) && !(document.getElementById('divMyOrder' + productArray[j].ProductID) == null)) {
                        addMyOrderList = " <div class='my-order' id='divMyOrderList" + productArray[j].ProductID + "' ><p class='flt order-content'> <strong> ";
                        addMyOrderList += productArray[j].Name + "</strong> </p>";
                        addMyOrderList += "<p class='flr order-right-area'><span class='order-number' id='spanMyOrderList" + productArray[j].ProductID + "' >" + $('#spanMyOrder' + productArray[j].ProductID).text() + "</span>";
                        addMyOrderList += "<a href='javascript:;'><img src='images/icon-less.png' alt='' onclick='RemoveQtyFromMyOrderList(" + productArray[j].ProductID + ")' /> </a></p> <div class='clr'></div>   </div";
                        $(".divShowMyOrderList").append(addMyOrderList);
                    }
                }
            }
        }
        var jsonShopTime = "";
        function GetShopTime() {
            $.ajax({
                type: "POST",
                url: 'Restaurant-Menu.asmx/ShopTime',
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    jsonShopTime = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    var txtPickUpDate = $("#txtPickUp");
                    ShowShopTime(txtPickUpDate.val());
                },
                error: function (response, status, error) {
                    jsonShopTime = (typeof response.d) == 'string' ? eval('(' + error.d + ')') : jsonShopTime;
                }
            });
        }

        function ShowShopTime(selectDate) {
            var date = new Date(selectDate);
            var day = date.getDay();
            $("#pShowShopHHMM").show();
            $("#spanShopTime").html("");
            $("#ddlHH").html("");
            var dateCurrent = new Date();
            dateCurrent.setMinutes(dateCurrent.getMinutes() + 15, 0, 0);
            var currentHH = dateCurrent.getHours();
            var currentMM = dateCurrent.getMinutes();
            var selectIndx = "0";
            if (currentMM <= 15)
                selectIndx = "1";
            else {
                if (currentMM > 15 && currentMM <= 30)
                    selectIndx = "2";
                else {
                    if (currentMM > 30 && currentMM <= 45)
                        selectIndx = "3";
                    else {
                        currentHH = currentHH + 1;
                        selectIndx = "0";
                    }
                }
            }

            for (var i = 0; i < jsonShopTime.ShopTime.length; i++) {
                if (jsonShopTime.ShopTime[i].Day == (day + 1)) {
                    if ($("#spanShopTime").html() != "") {
                        $("#spanShopTime").append(" OR " + jsonShopTime.ShopTime[i].TimeFrom + " - ")
                    }
                    else
                        $("#spanShopTime").append("[ " + jsonShopTime.ShopTime[i].TimeFrom + " - ")

                    $("#spanShopTime").append(jsonShopTime.ShopTime[i].TimeTo)
                    var timeFrom = Number(jsonShopTime.ShopTime[i].TimeFrom.split(':')[0]);
                    var timeTo = Number(jsonShopTime.ShopTime[i].TimeTo.split(':')[0]);

                    while (timeFrom < timeTo) {
                        var optionddlHH = "";
                        optionddlHH = "<option value='" + timeFrom + "' ";
                        if (Number(timeFrom) == Number(currentHH))
                            optionddlHH += "selected='selected'";
                        optionddlHH += " >" + timeFrom + "</option>";
                        $("#ddlHH").append(optionddlHH);
                        timeFrom = timeFrom + 1;
                    }
                    var ddlMM = document.getElementById("ddlMM").selectedIndex = selectIndx;
                }
            }
            $("#spanShopTime").append(" ]");
        }
        var currentPickupDate;
        function GetPictUpDate() {
            var pickUpDate = "";
            var txtPickUpDate = $("#txtPickUp");
            if (txtPickUpDate.val() != "" && txtPickUpDate.val() != null) {
                pickUpDate = txtPickUpDate.val();
                var ddlHH = document.getElementById("ddlHH");
                var ddlHHOption = ddlHH.options[ddlHH.selectedIndex].text;
                var ddlMM = document.getElementById("ddlMM");
                var ddlMMOption = ddlMM.options[ddlMM.selectedIndex].text;
                currentPickupDate = new Date(pickUpDate);
                pickUpDate += " " + ddlHHOption;
                pickUpDate += ":" + ddlMMOption + ":00";
                currentPickupDate.setHours(ddlHHOption, ddlMMOption, 00, 00);
            }
            return pickUpDate;
        }
    </script>
    <script type="text/javascript">
        $(function () {
            var date = new Date();
            $("#txtPickUp").datepicker({
                buttonImage: 'images/calender.png',
                buttonImageOnly: true,
                showOn: 'both',
                autoSize: true,
                dateFormat: 'yy-mm-dd',
                onSelect: function (dateText) {
                    ShowShopTime(dateText);
                }
            });
            $('#txtPickUp').datepicker('setDate', 'today');

        });

        $(document).ready(function () {
            $(".ui-datepicker-trigger").css("padding", "0 20px 0 5px");
            $(".ui-datepicker-trigger").css("cursor", "pointer");
        });
        function limitText(limitField, limitCount, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divDetails">
        <div id="main-container">
            <section id="left-content">
        	<article id="category">
            <div id="divCategory">
             <div style="font-size:80px;font-family: monterey_btregular; ">
               Loading.......
               </div>
            </div>            
            </article>    
            <article id="order-area">
            	<h1 style="color:#de6b37;"><%= Resources.Resource.lblMyOrder %></h1>
                <figure id="order-content-container"> 
                <div class="divShowMyOrder" ></div>
                    <div class="clr"></div>
                </figure>
            </article>
        </section>
            <section id="right-content">
        	<figure id="item-list">               
            <div class="divShowProduct" ></div>
                <div id="popup-container">
	           	    
                </div>
            </figure>
        </section>
            <footer>
        	<div class="btm-content flt" style="width:55%;">
            	<p>
                	<a href="Default.aspx">
                    <img src="images/icon-pre.png" alt="" />
               <%= Resources.Resource.lblHome %> &nbsp;/&nbsp;<%= Resources.Resource.lblCancelMyOrder %> </a>
                </p>
            </div>
            <div class="btm-content flr" style="width:35%;">
                 <p onclick="ShowMyOrderList();" style="text-align:right; padding-right:10px;">
                	<a href="javascript:;"><%= Resources.Resource.lblConfirmMyOrder%>&nbsp;
                    <img src="images/icon-next.png" alt="" />
                    </a>
                </p>
                
            </div>
        </footer>
        </div>
    </div>
    <div id="divOrderConfirm" style="display: none;">
        <div id="main-container1">
            <section id="left-content1">   
            <center>
                <div class="order-content-container">
                 <div class="my-form">
                    <p class="flt txt-area">
                      <input type="text"  class="form-input"  id="txtName" placeholder="Your Name" /> <b> <span class="spanRed">*</span></b>
                     </p> 
                    <div class="clr"></div>
                    <br /> 
                    <p class="flt txt-area">
                      <input type="text"  class="form-input"  id="txtPhoneNo" placeholder="Your Phone No.. if we need to call you" /> <b> <span class="spanRed">*</span></b>
                    </p> 
                    <div class="clr"></div>
                    <br />              
                    <p class="flt txt-area">
                       <input type="text"  class="form-input"  id="txtEmailID" placeholder="Your Email ID to notify you, when order is ready" />
                    </p> 
                     <div class="clr"></div>
                   <br />
                    <p class="flt txt-area"   style="font-family:MS Shell Dlg; word-break:break-all; font-size:13px">
                    <b>Regular Pick - Up time for today  between</b> </p>
                   <div class="clr"></div>
                <p class="flt txt-area"   style="font-family:MS Shell Dlg; word-break:break-all;font-size:13px">
                 <b>  10:00 - 21:30 </b>                    </p>
                    <div class="clr"></div>
                     <div style="display:none;">
                    <br />
                    <p class="flt txt-area">
                    <b> Pick up at</b> <span id="spanShopTime"></span>    
                    </p>
                    <div class="clr"></div>
                   
                    <br />
                     <p class="flt txt-area">
                       <input type="text"  class="form-input"  id="txtPickUp" placeholder="Pick up your Time"  style="width:90px;" />       
                      </p>    
                      
                       <p class="flt txt-area" id="pShowShopHHMM">
                       <span style="float:left;width:75px; padding-right:5px;">
                       <select id="ddlHH" style="width:60px; height:30px;" class="form-input">
                        </select> 
                        </span>
                       <span style="float:left;">
                        <select id="ddlMM" style="width:55px; height:30px;" class="form-input" >
                         <option value="00" selected="selected">00</option>
                         <option value="15"  >15</option>
                         <option value="30" >30</option>
                         <option value="45" >45</option>
                        </select>
                        </span>
                      </p>
                         <div class="clr"></div>
                          </div>
                    <br />              
                    <p class="flt txt-area">                     
                         <textarea  style="height: 64px;" id="txtAdditionalRequest" class="form-input" placeholder="Additional Request with Pickup Time (Max 100 characters)"
                       name="AdditionalRequest"
                       onkeydown="limitText(this.form.AdditionalRequest,this.form.countdown,100);"
                        onkeyup="limitText(this.form.AdditionalRequest,this.form.countdown,100);"
                       ></textarea>
                       <br />                        
                        <input readonly="readonly" type="text" name="countdown" size="3" value="100" style="border: 0px;
                            width: 25px;display:none;" />
                        <%--characters left.--%>
                    </p> 
                    <div class="clr"></div>
                      <p class="flt txt-area">
                     <span class="spanRed" style="font-family:MS Shell Dlg;font-size:14px"><b></b> <%= Resources.Resource.lblFieldRequired %> </span>
                     </p>
                      <div class="clr"></div>
                 </div> 
              </div>
              </center>     	 

            <article id="order-area1"> 
            	 <h1 style="color:#de6b37;"><%= Resources.Resource.lblMyOrder%> </h1>
                <figure id="order-content-container1"> 
                <div class="divShowMyOrderList" ></div>
                    <div class="clr"></div>
                </figure>
            </article>
        </section>
            <footer>
        	<div class="btm-content flt" style="width:55%;">
            	<p onclick="ShowMyOrder();">
                	<a href="javascript:;"><img src="images/icon-pre.png" alt="" />
               <%= Resources.Resource.lblHome %> &nbsp;/&nbsp;<%= Resources.Resource.lblCancelMyOrder %> </a>
                </p>
            </div>
           <div class="btm-content flr" style="width:35%;">
            <p onclick="SaveTransition();" style="text-align:right; padding-right:10px;">
                	<a href="javascript:;"><%= Resources.Resource.lblPlaceMyOrder%>&nbsp;
                    <img src="images/icon-next.png" alt="" /></a>
                </p>             
            </div>
        </footer>
        </div>
    </div>
    </form>
</body>
</html>

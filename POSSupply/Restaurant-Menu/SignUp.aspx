﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignUp.aspx.cs" Inherits="Restaurant_Menu_SignUp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sign Up </title>
    <meta charset="utf-8" />
    <meta name='viewport' content='width=device-width,initial-scale=1,maximum-scale=1'
        userscale='no' />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <%--<link href="css/style.css" type="text/css" rel="stylesheet" />--%>
    <link rel="apple-touch-icon-precomposed" href="images/icon_jqm.png" />
    <script type="text/javascript" src="JS/jquery.min.js"></script>
    <link href="css/Mobile_style.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/jquery.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="../lib/scripts/jquery-ui-1.8.11.custom.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="JS/jquery-ui.css" />
    <script src="JS/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="js/jquery-ui.js" type="text/javascript"></script>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', '<%=System.Configuration.ConfigurationManager.AppSettings["JavaScriptCode"] %>', 'itechcanada.com');
            ga('send', 'pageview');

    </script>

    <script type="text/javascript">
        var currentAppLanguageCode = '<%= iTECH.InbizERP.BusinessLogic.Globals.CurrentAppLanguageCode %>';
        var source = "";
        source = '<%= Request.QueryString["source"] %>';

        function PlaceNewOrder(lang) {
            var url = "MenuOrder.aspx?lang=";
            if (lang == "en")
                url += "en";
            else
                url += "fr";
            if (source == "1")
                url += "&source=1";
            window.location.href = url;
        }
        function GetUrlParameter() {
            var url = "lang=";
            if (currentAppLanguageCode == "en")
                url += "en";
            else
                url += "fr";
            if (source == "1")
                url += "&source=1";
            return url;
        }
        function FormatUserErrorMessage(responseCode) {
            var returnValue = "";
            if (responseCode == "-3")
                returnValue = "<%=Resources.Resource.lblWeAreHavingTechProb%>";
            else if (responseCode == "-2")
                returnValue = "<%=Resources.Resource.lblInVaildToken %>";
            else if (responseCode == "-1")
                returnValue = "<%=Resources.Resource.lblOperationUnsuccess%>";
            else
                returnValue = "<%=Resources.Resource.lblUnKnowError%>";
            return returnValue.replace("'", "\'");
        }

        function FormatErrorMessage(jqXHR, exception) {
            var returnValue = "";
            if (Number(jqXHR.status) == 0)
                returnValue = ("<%=Resources.Resource.lblErrorNetworkConnection %>");
            else if (Number(jqXHR.status) == 404)
                returnValue = ("<%=Resources.Resource.lblErrorPageNotfound %>");
            else if (Number(jqXHR.status) == 500)
                returnValue = ("<%=Resources.Resource.lblErrorInternalServerError %>");
            else if (exception == 'parsererror')
                returnValue = ("<%=Resources.Resource.lblErrorRequestedJSONParseFailed %>");
            else if (exception == 'timeout')
                returnValue = ("<%=Resources.Resource.lblErrorTimeOut %>");
            else if (exception == 'abort')
                returnValue = ("<%=Resources.Resource.lblErrorAjaxRequestAborted %>");
            else
                returnValue = ("<%=Resources.Resource.lblUnKnowError %> \n" + jqXHR.responseText);
            return returnValue.replace("'", "\'");
        }
        var resourceText = "";
        $(document).ready(function () {
            if (currentAppLanguageCode == "fr")
                $("#BLangauage").html("English");
            else
                $("#BLangauage").html("Français");
            $("#BLangauage").click(function () {
                var url = "signUp.aspx?lang=";
                if (currentAppLanguageCode == "en")
                    url += "fr";
                else
                    url += "en";
                if (source == "1")
                    url += "&source=1";
                window.location.href = url;
            });

            $('input[placeholder], textarea[placeholder]').css("font-family", "MS Shell Dlg");
            $('input[placeholder], textarea[placeholder]').css("font-size", "15px")
            $('input[placeholder], textarea[placeholder]').css("width", "260px")
            SetPlaceHolderValue()
            //$("#divLogIn").hide();
            $("#divLoginOption").hide();
            $("#bodyID").css('background-color', '#D9C5AD');
            $("#divSignUpMySetting").hide();
            $('#divSignUp').click(function () {
                $("#divLoginOption").hide();
                $("#divLogIn").show();
                $("#bodyID").css('background-color', '#D9C5AD');
            });
            $('#pBack').click(function () {
                $("#divLoginOption").show();
                $("#divLogIn").hide();
                $("#bodyID").css('background-color', '#de6b37');
            });
            $('#pLogIn').click(function () {
                $("#divLogIn").hide();
                $("#divSignUpMySetting").show();
            });
            $('#pSignUpBack').click(function () {
                $("#divLogIn").show();
                $("#divSignUpMySetting").hide();
            });
            $('#pLogInToWebSite').click(function () {
                var txtLogInID = $("#txtLoginID").val();
                var txtPassword = $("#txtPassword").val();
                if (txtLogInID == null || txtLogInID == "") {
                    resourceText = ("<%=Resources.Resource.RfvLoginIDOrEmailID %>");
                    alert(resourceText.replace("'", "\'"));
                    return;
                }
                if (txtPassword == null || txtPassword == "") {
                    resourceText = ("<%=Resources.Resource.RfvPassword %>");
                    alert(resourceText.replace("'", "\'"));
                    return;
                }
                window.location.href = "Token-WebService.aspx?loginID=" + txtLogInID + "&pwd=" + txtPassword + "&Address=NewMenuOrder&" + GetUrlParameter();
            });
            function validateForm(emailID) {
                var x = emailID;
                var atpos = x.indexOf("@");
                var dotpos = x.lastIndexOf(".");
                if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                    var msg = "Not a valid e-mail address ";
                    if (currentAppLanguageCode == "fr")
                        msg = "Pas une adresse e-mail valide";
                    alert(msg);
                    return false;
                }
                else
                    return true;
            }
            $('#pSignUp').click(function () {
                var txtSignUpName = $("#txtSignUpName").val();
                var txtEmailID = $("#txtEmailID").val();
                var txtSignUpPassword = $("#txtSignUpPassword").val();
                var txtConfirmPassword = $("#txtConfirmPassword").val();
                var txtSignUpPhone = $("#txtSignUpPhone").val();
                if (txtSignUpName == null || txtSignUpName == "") {
                    resourceText = ("<%=Resources.Resource.RfvName %>");
                    alert(resourceText.replace("'", "\'"));
                    return;
                }
                if (txtEmailID == null || txtEmailID == "") {
                    resourceText = ("<%=Resources.Resource.RfvLoginIDOrEmailID %>");
                    alert(resourceText.replace("'", "\'"));
                    return;
                }
                if (txtSignUpPassword == null || txtSignUpPassword == "") {
                    resourceText = ("<%=Resources.Resource.RfvPassword %>");
                    alert(resourceText.replace("'", "\'"));
                    return;
                }
                if (txtConfirmPassword == null || txtConfirmPassword == "") {
                    resourceText = ("<%=Resources.Resource.RfvConfirmPassword %>");
                    alert(resourceText.replace("'", "\'"));
                    return;
                }
                if (txtConfirmPassword != txtSignUpPassword) {
                    resourceText = ("<%=Resources.Resource.MsgPasswordNotMatched %>");
                    alert(resourceText.replace("'", "\'"));
                    return;
                }
                if (txtSignUpPhone == null || txtSignUpPhone == "") {
                    resourceText = ("<%=Resources.Resource.RfvPhone %>");
                    alert(resourceText.replace("'", "\'"));
                    return;
                }
                if (validateForm(txtEmailID)) {
                }
                else
                    return;

                $.ajax({
                    type: "POST",
                    url: "Restaurant-Menu.asmx/SignUp",
                    data: "{lang :'" + currentAppLanguageCode + "',name:'" + txtSignUpName + "',logInID:'" + txtEmailID + "',password:'" + txtSignUpPassword + "',phone:'" + txtSignUpPhone + "',address:'" + $("#txtSignUpAddress").val() + "',city:'" + $("#txtSignUpCity").val() + "',postalCode:'" + $("#txtSignUpPostalCode").val() + "',state:'" + $("#txtSignUpState").val() + "',country:'" + $("#txtSignUpCountry").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var returnData = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (returnData.ResponseCode == 1 && returnData.Status == "OK") {
                            window.location.href = "Token-WebService.aspx?loginID=" + txtEmailID + "&pwd=" + txtSignUpPassword + "&Address=NewMenuOrder&" + GetUrlParameter();
                        }
                        else if (returnData.ResponseCode == -1) {
                            resourceText = ("<%=Resources.Resource.MsgEmailIDExists %>");
                            alert(resourceText.replace("'", "\'"));
                        }
                        else { alert(FormatUserErrorMessage(returnData.ResponseCode)); }
                    },
                    error: function (xhr, err) {
                        alert(FormatUserErrorMessage("-3") + " \n " + FormatErrorMessage(xhr, err));
                    }
                });
            });

            $('#bLogOut').click(function () {
                window.location.href = "Token-WebService.aspx?" + GetUrlParameter();
            });
            $('#bGoMenuPage').click(function () {
                window.location.href = "NewMenuOrder.aspx?" + GetUrlParameter();
            });

            $("#divSave").hide();
            $("#divLogOut").hide();
            $("#divForgetPassword").hide();
            var IsValidToken = '<%=IsValidToken %>';
            if (IsValidToken == "True") {
                LoyaltyPoint();
                $("#divSignUpMySetting").show();
                $("#divLoginOption").hide();
                $("#divLogIn").hide();
                $("#bodyID").css('background-color', '#D9C5AD');
                $("#divPassHide").hide();
                $("#divSave").show();
                $("#divSignUpButton").hide();
                resourceText = ("<%=Resources.Resource.lblMyProfile %>");
                $("#pMyProfile").html("<b>" + resourceText.replace("'", "\'") + "</b>");
                $("#divLogOut").show();
                document.getElementById("txtEmailID").readOnly = true;
                $.ajax({
                    type: "POST",
                    url: "Restaurant-Menu.asmx/PartnerDetails",
                    data: "{partnerID:'<%=UserID %>',logInID:'<%=EmailID %>',deviceID:'<%=DeviceID %>',token:'" + $('#<%=hdnToken.ClientID%>').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var JsonPartner = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (JsonPartner.ResponseCode == 1 && JsonPartner.Status == "OK") {
                            $("#txtSignUpName").val(JsonPartner.Partners.PartnerLongName);
                            $("#txtEmailID").val(JsonPartner.Partners.PartnerEmail);
                            $("#txtSignUpPhone").val(JsonPartner.Partners.PartnerPhone);
                            $("#txtSignUpAddress").val(JsonPartner.Address.AddressLine1);
                            $("#txtSignUpCity").val(JsonPartner.Address.AddressCity);
                            $("#txtSignUpPostalCode").val(JsonPartner.Address.AddressPostalCode);
                            $("#txtSignUpState").val(JsonPartner.Address.AddressState);
                            $("#txtSignUpCountry").val(JsonPartner.Address.AddressCountry);
                        }
                        else { alert(FormatUserErrorMessage(JsonPartner.ResponseCode)); }
                    },
                    error: function (xhr, err) {
                        alert(FormatUserErrorMessage("-3") + "\n" + FormatErrorMessage(xhr, err));
                    }
                });
            }
        });

        $(document).ready(function () {
            CompanyInfo();
            RegisterInfo();
            
            $('#divChangePassword').hide();
            $('#pChangePassword').click(function () {
                $('#divSignUpMySetting').hide();
                $('#divChangePassword').show();
            });
            $('#pChangePasswordBack').click(function () {
                $('#divChangePassword').hide();
                $('#divSignUpMySetting').show();
            });

            $('#pChangePasswordSave').click(function () {
                var txtChangeOldPassword = $("#txtChangeOldPassword").val();
                var txtChangeNewPassword = $("#txtChangeNewPassword").val();
                var txtChangeConfirmPassword = $("#txtChangeConfirmPassword").val();

                if (txtChangeOldPassword == null || txtChangeOldPassword == "") {
                    resourceText = ("<%=Resources.Resource.RfvOldPassword %>");
                    alert(resourceText.replace("'", "\'"));
                    return;
                }
                if (txtChangeNewPassword == null || txtChangeNewPassword == "") {
                    resourceText = ("<%=Resources.Resource.RfvNewPassword %>");
                    alert(resourceText.replace("'", "\'"));
                    return;
                }
                if (txtChangeConfirmPassword == null || txtChangeConfirmPassword == "") {
                    resourceText = ("<%=Resources.Resource.RfvConfirmNewPassword %>");
                    alert(resourceText.replace("'", "\'"));
                    return;
                }
                if (txtChangeConfirmPassword != txtChangeNewPassword) {
                    resourceText = ("<%=Resources.Resource.MsgNewPasswordAndConfirmPasswordNotMAtched %>");
                    alert(resourceText.replace("'", "\'"));
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "Restaurant-Menu.asmx/ChangePassword",
                    data: "{partnerID :'<%=UserID %>',oldPassword:'" + txtChangeOldPassword + "',newPassword:'" + txtChangeNewPassword + "',logInID:'<%=EmailID %>',deviceID:'<%=DeviceID %>',token:'" + $('#<%=hdnToken.ClientID%>').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var returnData = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (returnData.ResponseCode == 1 && returnData.Status == "OK") {
                            resourceText = ("<%=Resources.Resource.MsgPasswordChanged %>");
                            alert(resourceText.replace("'", "\'"));
                            $('#divChangePassword').hide();
                            $('#divSignUpMySetting').show();
                        }
                        else if (returnData.ResponseCode == -1) {
                            resourceText = ("<%=Resources.Resource.MsgPasswordNotMatched %>");
                            alert(resourceText.replace("'", "\'"));
                        }
                        else { alert(FormatUserErrorMessage(returnData.ResponseCode)); }
                    },
                    error: function (xhr, err) {
                        alert(FormatUserErrorMessage("-3") + FormatErrorMessage(xhr, err));
                    }
                });
            });


            $('#pSave').click(function () {
                var txtSignUpName = $("#txtSignUpName").val();
                var txtEmailID = $("#txtEmailID").val();
                var txtSignUpPassword = $("#txtSignUpPassword").val();
                var txtConfirmPassword = $("#txtConfirmPassword").val();
                var txtSignUpPhone = $("#txtSignUpPhone").val();
                if (txtSignUpName == null || txtSignUpName == "") {
                    resourceText = ("<%=Resources.Resource.RfvName %>");
                    alert(resourceText.replace("'", "\'"));
                    return;
                }
                if (txtSignUpPhone == null || txtSignUpPhone == "") {
                    resourceText = ("<%=Resources.Resource.RfvPhone %>");
                    alert(resourceText.replace("'", "\'"));
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "Restaurant-Menu.asmx/UpdatePartner",
                    data: "{partnerID :'<%=UserID %>',name:'" + txtSignUpName + "',logInID:'" + txtEmailID + "',phone:'" + txtSignUpPhone + "',address:'" + $("#txtSignUpAddress").val() + "',city:'" + $("#txtSignUpCity").val() + "',postalCode:'" + $("#txtSignUpPostalCode").val() + "',state:'" + $("#txtSignUpState").val() + "',country:'" + $("#txtSignUpCountry").val() + "',deviceID:'<%=DeviceID %>',token:'" + $('#<%=hdnToken.ClientID%>').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var returnData = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (returnData.ResponseCode == 1 && returnData.Status == "OK") {
                            resourceText = ("<%=Resources.Resource.MsgSaveSuccessfully %>");
                            alert(resourceText.replace("'", "\'"));
                        }
                        else if (returnData.ResponseCode == -1) {
                            resourceText = ("<%=Resources.Resource.MsgEmailIDExists %>");
                            alert(resourceText.replace("'", "\'"));
                        }
                        else { alert(FormatUserErrorMessage(returnData.ResponseCode)); }
                    },
                    error: function (xhr, err) {
                        alert(FormatUserErrorMessage("-3") + FormatErrorMessage(xhr, err));
                    }
                });
            });
            $('#pResetPassword').click(function () {
                $("#divLogIn").hide();
                $("#divForgetPassword").show();
            });
            $('#pForgetBack').click(function () {
                $("#divLogIn").show();
                $("#divForgetPassword").hide();
            });

            $('#pForgetPasswordSend').click(function () {
                var txtForgetLoginID = $("#txtForgetLoginID").val();
                if (txtForgetLoginID == "" || txtForgetLoginID == null) {
                    resourceText = ("<%=Resources.Resource.RfvLoginIDOrEmailID %>");
                    alert(resourceText.replace("'", "\'"));
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "Restaurant-Menu.asmx/PartnerForgetPassword",
                    data: "{lang :'" + currentAppLanguageCode + "', loginID :'" + txtForgetLoginID + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var returnData = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (returnData.ResponseCode == 1 && returnData.Status == "OK") {
                            resourceText = ("<%=Resources.Resource.MsgPasswordResetSuccessfully %>");
                            alert(resourceText.replace("'", "\'"));
                            $("#divLogIn").show();
                            $("#divForgetPassword").hide();
                        }
                        else if (returnData.ResponseCode == -1) {
                            resourceText = ("<%=Resources.Resource.MsgEmailIDOrLoginIDNotExists %>");
                            alert(resourceText.replace("'", "\'"));
                            return;
                        }
                        else { alert(FormatUserErrorMessage(returnData.ResponseCode)); }
                    },
                    error: function (xhr, err) {
                        alert(FormatUserErrorMessage("-3") + FormatErrorMessage(xhr, err));
                    }
                });
            });
        });
        function SetPlaceHolderValue() {
            resourceText = ("<%=Resources.Resource.lblLoginIDOrEmailID %>");
            $("#txtLoginID").attr("placeholder", resourceText.replace("'", "\'"));
            $("#txtForgetLoginID").attr("placeholder", resourceText.replace("'", "\'"));
            resourceText = ("<%=Resources.Resource.lblPassword %>");
            $("#txtPassword").attr("placeholder", resourceText.replace("'", "\'"));
            resourceText = ("<%=Resources.Resource.lblEmailID_LoginID %>");
            $("#txtEmailID").attr("placeholder", resourceText.replace("'", "\'"));
            resourceText = ("<%=Resources.Resource.lblPassword %>");
            $("#txtSignUpPassword").attr("placeholder", resourceText.replace("'", "\'"));
            resourceText = ("<%=Resources.Resource.lblConfirmPassword %>");
            $("#txtConfirmPassword").attr("placeholder", resourceText.replace("'", "\'"));
            resourceText = ("<%=Resources.Resource.lblDistName %>");
            $("#txtSignUpName").attr("placeholder", resourceText.replace("'", "\'"));
            resourceText = ("<%=Resources.Resource.lblDistPhone %>");
            $("#txtSignUpPhone").attr("placeholder", resourceText.replace("'", "\'"));
            resourceText = ("<%=Resources.Resource.lblHQAddressLine1 %>");
            $("#txtSignUpAddress").attr("placeholder", resourceText.replace("'", "\'"));
            resourceText = ("<%=Resources.Resource.lblHQAddressCity %>");
            $("#txtSignUpCity").attr("placeholder", resourceText.replace("'", "\'"));
            resourceText = ("<%=Resources.Resource.lblHQAddressPostalCode %>");
            $("#txtSignUpPostalCode").attr("placeholder", resourceText.replace("'", "\'"));
            resourceText = ("<%=Resources.Resource.lblHQAddressState %>");
            $("#txtSignUpState").attr("placeholder", resourceText.replace("'", "\'"));
            resourceText = ("<%=Resources.Resource.lblHQAddressCountry %>");
            $("#txtSignUpCountry").attr("placeholder", resourceText.replace("'", "\'"));
            resourceText = ("<%=Resources.Resource.lblOldPassword %>");
            $("#txtChangeOldPassword").attr("placeholder", resourceText.replace("'", "\'"));
            resourceText = ("<%=Resources.Resource.lblNewPassword %>");
            $("#txtChangeNewPassword").attr("placeholder", resourceText.replace("'", "\'"));
            resourceText = ("<%=Resources.Resource.lblConfirmNewPassword %>");
            $("#txtChangeConfirmPassword").attr("placeholder", resourceText.replace("'", "\'"));
        }

        function CompanyInfo() {

            $.ajax({
                type: "POST",
                url: "SignUp.aspx/CompanyInfo",
                data: "{companyID:'5'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var returnData = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (returnData.ResponseCode == 1 && returnData.Status == "OK") {
                        $("#ltrCompanyName").html(returnData.CompanyName);
                    }
                    else if (returnData.ResponseCode == -1) {
                        alert("Company infomartion not getting.");
                    }
                    else { alert(FormatUserErrorMessage(returnData.ResponseCode)); }
                },
                error: function (xhr, err) {
                    alert(FormatUserErrorMessage("-3") + " \n " + FormatErrorMessage(xhr, err));
                }
            });
        }

        function RegisterInfo() {

            $.ajax({
                type: "POST",
                url: "Restaurant-Menu.asmx/CompanyInfo",
                data: "{RegCode:'HIT'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var returnData = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (returnData.ResponseCode == 1 && returnData.Status == "OK") {
                        var sRegisterInfo = "";
                        if (returnData.Tel != "" && returnData.Address != "") {
                            sRegisterInfo = "<%=Resources.Resource.liCMPhoneNumber %>" + " : " + returnData.Tel + "<br/>" + "<%=Resources.Resource.lblAddress %>" + ":" + returnData.Address;
                        }
                        else if (returnData.Tel != "") {
                            sRegisterInfo = "<%=Resources.Resource.liCMPhoneNumber %>" + " : " + returnData.Tel;
                        }
                        else {
                            sRegisterInfo = "<%=Resources.Resource.lblAddress %>" + ":" + returnData.Address;
                        }
                        $("#ltrCompanyContactInfo").html(sRegisterInfo);
                    }
                    else if (returnData.ResponseCode == -1) {
                        alert("Register infomartion not getting.");
                    }
                    else { alert(FormatUserErrorMessage(returnData.ResponseCode)); }
                },
                error: function (xhr, err) {
                    alert(FormatUserErrorMessage("-3") + " \n " + FormatErrorMessage(xhr, err));
                }
            });
        }

        function LoyaltyPoint() {

            $.ajax({
                type: "POST",
                url: "Restaurant-Menu.asmx/AvailablePoints",
                data: "{location:'',userID:'<%=UserID %>',logInID:'<%=EmailID %>',deviceID:'<%=DeviceID %>',token:'" + $('#<%=hdnToken.ClientID%>').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var returnData = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (returnData.ResponseCode == 1 && returnData.Status == "OK") {
                        //var sRegisterInfo = "";
                        $("#ltrLoyaltyPoints").html(returnData.Points); 
                    }
                    else if (returnData.ResponseCode == -1) {
                        alert("Loyalty Point infomartion not getting.");
                    }
                    else { alert(FormatUserErrorMessage(returnData.ResponseCode)); }
                },
                error: function (xhr, err) {
                    alert(FormatUserErrorMessage("-3") + " \n " + FormatErrorMessage(xhr, err));
                }
            });
        }
        
    </script>
</head>
<body style="background-color: #de6b37;" id="bodyID">
    <form id="form1" runat="server">
    <div style="width: 100%; height: 100%; display: block; color: white; font-family: monterey_btregular;
        float: left; width: 100%;">
        <div id="divLoginOption" style="display: none;">
            <div style="margin: 12% 1% 0; font-size: 40px; float: left; width: 98%; cursor: pointer;
                background-color: #3B5998;">
                <img src="../images/facebook.png" alt="" style="height: 38px; padding-top: 10px;
                    width: 48px;" />
                Login with Facebook
                <img src="images/icon-next.png" alt="" style="float: right; margin-top: 20px; margin-right: 5px" />
            </div>
            <div id="divSignUp" style="margin: 3% 1% 0; font-size: 40px; float: left; width: 98%;
                cursor: pointer; background-color: #563131; text-align: left; padding-top: 10px;">
                <span style="margin-left: 45px;">Login to Umi Sushi</span>
                <img src="images/icon-next.png" alt="" style="float: right; margin-top: 10px; margin-right: 5px" />
            </div>
            <div style="margin: 3% 1% 0; font-size: 40px; float: left; width: 98%; cursor: pointer;
                text-align: left; padding-top: 10px;">
                <span style="margin-left: 45px;">Continue as Guest</span>
                <img src="images/icon-next.png" alt="" style="float: right; margin-top: 10px; margin-right: 5px" />
            </div>
        </div>
        <div id="divLogIn" style="float: left; width: 100%;">
            <div class="main-container content order-area">
                <div class="order-content-container" style="margin-top: -80px;">
                    <div class="my-form">
                   
                        <p class="txt-area" style="font-family: MS Shell Dlg; word-break: break-all;
                            font-size: 27px; text-align:center;">
                            <literal id="ltrCompanyName"></literal>
                        </p>
                        <p class="txt-area" style="font-family: MS Shell Dlg; word-break: break-all;
                            font-size: 12px; text-align:center;">
                            <literal id="ltrCompanyContactInfo"></literal>
                        </p>
                        <div class="clr">
                        </div>                       
                        <p class=" txt-area" style="font-family: MS Shell Dlg; word-break: break-all;
                            font-size: 13px; text-align:center;" >
                           <%-- <%=ConfigurationManager.AppSettings["SignUpText"]%>--%>
                        </p>                        
                        <br />  <br />
                        <div class="clr">
                        </div>
                        <p class="flt txt-area">
                            <input type="text" class="form-input" id="txtLoginID" placeholder="" />
                            <b><span class="spanRed">*</span></b>
                        </p>
                        <div class="clr">
                        </div>
                        <br />
                        <p class="flt txt-area">
                            <input type="password" class="form-input" id="txtPassword" placeholder="" />
                            <b><span class="spanRed">*</span></b>
                        </p>
                        <div class="clr">
                        </div>
                        <br />
                        <p class="flt txt-area" style="font-family: MS Shell Dlg; word-break: break-all;
                            font-size: 13px; cursor: pointer;" id="pLogIn">
                            <%=Resources.Resource.lblSignInForAccount %>
                        </p>
                        <div class="clr">
                        </div>
                        <br />
                        <p class="flt txt-area" style="font-family: MS Shell Dlg; word-break: break-all;
                            font-size: 13px; width: 100%; cursor: pointer;">
                            <b style="float: left;" id="pResetPassword"><a style="color: #de6b37;">
                                <%=Resources.Resource.lblForgotPassword %>
                            </a></b><b style="float: right;" id="BLangauage"><a style="color: #de6b37;"></a>
                            </b>
                            <div class="clr">
                            </div>
                        </p>
                        <div class="clr">
                        </div>
                        <br />
                        <div class="header">
                            <div class="btm-content">
                                <%--<p style="float: left" id="pBack">
                                    <a href="javascript:;">
                                        <img src="images/icon-pre.png" alt="" />&nbsp;
                                        <%= Resources.Resource.lblBack%>
                                    </a>
                                </p>--%>
                                <center>
                                    <p style="" id="pLogInToWebSite">
                                        <a href="javascript:;">
                                            <%=Resources.Resource.lblLog_In%>
                                            &nbsp;
                                            <img src="images/icon-next.png" alt="" />
                                        </a>
                                    </p>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clr">
        </div>
        <div id="divSignUpMySetting" style="float: left; width: 100%;">
            <div class="main-container content order-area">
                <div style="margin-top: -120px; top: 0;">
                    <div id="divLogOut" class="header">
                        <div class="btm-content">
                            <p style="float: left" id="bGoMenuPage">
                                <a href="javascript:;">
                                    <img src="images/icon-pre.png" alt="" />&nbsp;
                                    <%=Resources.Resource.lblGoToMenu %>
                                </a>
                            </p>
                            <p style="float: right" id="bLogOut">
                                <a href="javascript:;">
                                    <%=Resources.Resource.lblLogout%>
                                    &nbsp;<img src="images/icon-next.png" alt="" />
                                </a>
                            </p>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="order-content-container" style="margin-top: 30px;">
                        <div class="my-form">
                            <p class="flt txt-area" style="font-family: MS Shell Dlg; word-break: break-all;
                                font-size: 27px" id='pMyProfile'>
                                <b>
                                    <%=Resources.Resource.lblSignUp %></b>
                            </p>
                            <div class="clr">
                            </div>
                            <br />
                            <p class="flt txt-area">
                                <input type="text" class="form-input" id="txtEmailID" placeholder="" />
                                <b><span class="spanRed">*</span></b>
                            </p>
                            <div class="clr">
                            </div>
                            <br />
                            <div id="divPassHide">
                                <p class="flt txt-area">
                                    <input type="password" class="form-input" id="txtSignUpPassword" placeholder="" />
                                    <b><span class="spanRed">*</span></b>
                                </p>
                                <div class="clr">
                                </div>
                                <br />
                                <p class="flt txt-area">
                                    <input type="password" class="form-input" id="txtConfirmPassword" placeholder="" />
                                    <b><span class="spanRed">*</span></b>
                                </p>
                                <div class="clr">
                                </div>
                                <br />
                            </div>
                            <div class="clr">
                            </div>
                            <p class="flt txt-area">
                                <input type="text" class="form-input" id="txtSignUpName" placeholder="" />
                                <b><span class="spanRed">*</span></b>
                            </p>
                            <div class="clr">
                            </div>
                            <br />
                            <p class="flt txt-area">
                                <input type="text" class="form-input" id="txtSignUpPhone" placeholder="" />
                                <b><span class="spanRed">*</span></b>
                            </p>
                            <div class="clr">
                            </div>
                            <div style="display:none;">
                            <br />
                            <p class="flt txt-area">
                                <textarea style="height: 64px;" id="txtSignUpAddress" class="form-input" placeholder=""></textarea>
                            </p>
                            <div class="clr">
                            </div>
                            <br />
                            <p class="flt txt-area">
                                <input type="text" class="form-input" id="txtSignUpCity" placeholder="" />
                            </p>
                            <div class="clr">
                            </div>
                            <br />
                            <p class="flt txt-area">
                                <input type="text" class="form-input" id="txtSignUpPostalCode" placeholder="" />
                            </p>
                            <div class="clr">
                            </div>
                            <br />
                            <p class="flt txt-area">
                                <input type="text" class="form-input" id="txtSignUpState" placeholder="" />
                            </p>
                            <div class="clr">
                            </div>
                            <br />
                            <p class="flt txt-area">
                                <input type="text" class="form-input" id="txtSignUpCountry" placeholder="" />
                            </p>
                            <div class="clr">
                            </div>
                            </div>
                            <p class="flt txt-area">
                                <span class="spanRed" style="font-family: MS Shell Dlg; font-size: 14px"><b></b>
                                    <%= Resources.Resource.lblFieldRequired %></span>
                            </p>
                            <div class="clr">
                            </div>
                            <br />
                             <p class="flt txt-area" style="font-family: MS Shell Dlg; word-break: break-all;
                                font-size: 27px">
                                <b>
                                    <%= Resources.Resource.lblLoyaltyPoints%>
                                    <literal id="ltrLoyaltyPoints"></literal></b>
                            </p>
                            
                            <div class="clr">
                            </div>
                            <br />
                        </div>
                    </div>
                    <div id="divSignUpButton" class="header" style="bottom: 0; width: 100%;">
                        <div class="btm-content">
                            <p style="float: left" id="pSignUpBack">
                                <a href="javascript:;">
                                    <img src="images/icon-pre.png" alt="" />&nbsp;
                                    <%= Resources.Resource.lblBack%>
                                </a>
                            </p>
                            <p style="float: right" id="pSignUp">
                                <a href="javascript:;">
                                    <%=Resources.Resource.lblContinueSignUp%>&nbsp;
                                    <img src="images/icon-next.png" alt="" />
                                </a>
                            </p>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divSave" class="header" style="bottom: 0; width: 100%;">
                        <div class="btm-content">
                            <p style="float: left" id="pChangePassword">
                                <a href="javascript:;">
                                    <img src="images/icon-pre.png" alt="" />&nbsp;
                                    <%=Resources.Resource.btnChangesPassword%>
                                </a>
                            </p>
                            <p style="float: right" id="pSave">
                                <a href="javascript:;">
                                    <%=Resources.Resource.btnSave%>
                                    &nbsp;<img src="images/icon-next.png" alt="" />
                                </a>
                            </p>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                </div>
            </div>
            <div class="clr">
            </div>
        </div>
        <div id="divForgetPassword" style="float: left; width: 100%; cursor: pointer;">
            <div class="main-container content order-area">
                <div class="order-content-container" style="margin-top: -80px;">
                    <div class="my-form">
                        <p class="flt txt-area" style="font-family: MS Shell Dlg; word-break: break-all;
                            font-size: 27px">
                            <b>
                                <%=Resources.Resource.lblForgetPwd%></b>
                        </p>
                        <br />
                        <div class="clr">
                        </div>
                        <p class="flt txt-area">
                            <input type="text" class="form-input" id="txtForgetLoginID" placeholder="" />
                            <b><span class="spanRed">*</span></b>
                        </p>
                        <div class="clr">
                        </div>
                        <br />
                    </div>
                </div>
                <div class="header" style="bottom: 0;">
                    <div class="btm-content">
                        <p style="float: left" id="pForgetBack">
                            <a href="javascript:;">
                                <img src="images/icon-pre.png" alt="" />&nbsp;
                                <%= Resources.Resource.lblBack%>
                            </a>
                        </p>
                        <p style="float: right" id="pForgetPasswordSend">
                            <a href="javascript:;">
                                <%= Resources.Resource.cmdCssSubmit%>&nbsp;
                                <img src="images/icon-next.png" alt="" />
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div id="divChangePassword" style="float: left; width: 100%;">
            <div class="main-container content order-area">
                <div class="order-content-container" style="margin-top: -80px;">
                    <div class="my-form">
                        <p class="flt txt-area" style="font-family: MS Shell Dlg; word-break: break-all;
                            font-size: 27px">
                            <b>
                                <%= Resources.Resource.btnChangesPassword%></b>
                        </p>
                        <br />
                        <div class="clr">
                        </div>
                        <p class="flt txt-area">
                            <input type="password" class="form-input" id="txtChangeOldPassword" placeholder="" />
                            <b><span class="spanRed">*</span></b>
                        </p>
                        <div class="clr">
                        </div>
                        <br />
                        <p class="flt txt-area">
                            <input type="password" class="form-input" id="txtChangeNewPassword" placeholder="" />
                            <b><span class="spanRed">*</span></b>
                        </p>
                        <div class="clr">
                        </div>
                        <br />
                        <p class="flt txt-area">
                            <input type="password" class="form-input" id="txtChangeConfirmPassword" placeholder="" />
                            <b><span class="spanRed">*</span></b>
                        </p>
                        <div class="clr">
                        </div>
                        <br />
                    </div>
                </div>
                <div class="header">
                    <div class="btm-content">
                        <p style="float: left" id="pChangePasswordBack">
                            <a href="javascript:;">
                                <img src="images/icon-pre.png" alt="" />&nbsp;
                                <%= Resources.Resource.lblBack%>
                            </a>
                        </p>
                        <p style="float: right" id="pChangePasswordSave">
                            <a href="javascript:;">
                                <%= Resources.Resource.cmdCssSubmit%>&nbsp;
                                <img src="images/icon-next.png" alt="" />
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <asp:HiddenField ID="hdnToken" runat="server" />           
    </form>
</body>
</html>

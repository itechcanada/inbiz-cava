﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using System.Web.Script.Serialization;
using System.Configuration;

using iTECH.Library.Utilities;
using System.Net;
using System.Web.Services;
using Newtonsoft.Json;
using iTECH.WebService.Utility;
using iTECH.Library.DataAccess.MySql;

using System.Web.Script.Services;
public partial class Restaurant_Menu_NewMenuOrder : System.Web.UI.Page
{
    //Added by mukesh 20130802
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["IsTransactionSuccess"] != null)
        {
            if (!BusinessUtility.GetBool(Session["IsTransactionSuccess"]) && (Session["TransitionID"] != null) && (Session["TableNote"] != null) && (Session["PaymentToPay"] != null))// && !string.IsNullOrEmpty(TOKEN))
            {
                new clsPOSTransaction().funTransactionDelete(BusinessUtility.GetString(Session["TransitionID"]));

            }
        }
        Session["TableNote"] = null;
        Session["PaymentToPay"] = null;
        Session["IsTransactionSuccess"] = null;
        Session["TransitionID"] = null;
    }
    //Added end
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            string lang = Request.QueryString["lang"] != null ? Request.QueryString["lang"] : "en";
            string source = Request.QueryString["source"] != null ? Request.QueryString["source"] : "";
            hdnToken.Value = TokenNo;
            if (Session["TokenResult"] == null || !IsValidToken)
            {
                string url = "SignUp.aspx";
                url += "?lang=" + lang + (!string.IsNullOrEmpty(source) ? "&source=" + source : "");
                Response.Redirect(url, false);
            }
        }
        catch { IsValidToken = false; }
    }
    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
    private bool _isValidToken = true;
    protected bool IsValidToken
    {
        get
        {
            if (Session["TokenResult"] != null)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["ResponseCode"] != null ? iTECH.Library.Utilities.BusinessUtility.GetInt(dictData["ResponseCode"]) == 1 && _isValidToken : false;
            }
            else return false;
        }
        set { _isValidToken = value; }
    }
    protected string UserID
    {
        get
        {
            if (IsValidToken)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["UserID"] != null ? dictData["UserID"].ToString() : "1";
            }
            else return "0";
        }
    }
    protected string EmailID
    {
        get
        {
            if (IsValidToken)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["EmailID"] != null ? dictData["EmailID"].ToString() : "admin";
            }
            else return "admin";
        }
    }
    protected string TokenNo
    {
        get
        {
            if (IsValidToken)
            {
                Dictionary<string, Object> dictData = (Dictionary<string, Object>)Session["TokenResult"];
                return dictData["TokenNo"] != null ? dictData["TokenNo"].ToString() : "";
            }
            else return "";
        }
    }
    protected string DeviceID { get { return ConfigurationManager.AppSettings["Resturant_DeviceID"] != null ? ConfigurationManager.AppSettings["Resturant_DeviceID"] : "Rest_123456"; } }

    [System.Web.Services.WebMethod]
    public static string IsValidLoyalPoints(string partnerID, string amtToCheckLoyalPoints)
    {
        ResultDictionary dictResult = new ResultDictionary();
        try
        {
            SysWarehouses _whs = new SysWarehouses();
            int calculatedPoint = 0;
            double amtPerPoint = _whs.GetLoyalAmtPerPoint(System.Configuration.ConfigurationManager.AppSettings["WebSaleWhsCode"]);
            if (amtPerPoint > 0)
            {
                calculatedPoint = BusinessUtility.GetInt(BusinessUtility.GetDouble(amtToCheckLoyalPoints) / amtPerPoint);
            }
            LoyalPartnerHistory plh = new LoyalPartnerHistory();
            int userInHandPoint = BusinessUtility.GetInt(plh.GetPartnerLoyaltyPoints(null, BusinessUtility.GetInt(partnerID)));
            dictResult.OkStatus(ResponseCode.SUCCESS);
            if ((userInHandPoint >= _whs.MinPointsForRedemption))
            {
                if (userInHandPoint >= calculatedPoint)
                {
                    dictResult.Add("IsValidStatus", "true");
                    dictResult.Add("avlpoint", userInHandPoint);
                    dictResult.Add("minpoint", _whs.MinPointsForRedemption);
                    dictResult.Add("paidAmt", amtToCheckLoyalPoints);
                }
                else//To Allow partial payment
                {
                    dictResult.Add("IsValidStatus", "partial");
                    double inhandAmt = Math.Round((userInHandPoint * amtPerPoint),2);
                    double unPaidAmt = Math.Round(BusinessUtility.GetDouble(amtToCheckLoyalPoints) - inhandAmt,2);
                    dictResult.Add("unPaidAmt", unPaidAmt);
                    dictResult.Add("paidAmt", inhandAmt);
                    //dictResult.Add("avlpoint", userInHandPoint);
                    //dictResult.Add("minpoint", _whs.MinPointsForRedemption);
                }
            }
            else
            {
                dictResult.Add("IsValidStatus", "false");
                dictResult.Add("avlpoint", userInHandPoint);
                dictResult.Add("minpoint", _whs.MinPointsForRedemption);
            }
            //dictResult.Add("IsValidLoyalPoints", BusinessUtility.GetString(amtToCheckLoyalPoints));
            //return BusinessUtility.GetString(hdnSubTotalAmt.Value);   

        }
        catch (Exception ex)
        {
            throw;
        }
        return dictResult.ResultInInJSONFormat;
    }
}
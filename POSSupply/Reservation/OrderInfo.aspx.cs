﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;
using iTECH.WebControls;

using Newtonsoft.Json;

using jq = Trirand.Web.UI.WebControls;

//public partial class Reservation_OrderInfo : BasePage
public partial class Reservation_OrderInfo : BasePage
{
    double _orderTotal = 0;
    int _noOfNights = 0;
    double _individualBedPrice = 0;
    bool isValidRange = false;

    string _errorMessage = string.Empty;
    //public bool IsPagePostBack(params jq.JQGrid[] grids)
    //{
    //    bool isPostBack = IsPostBack;
    //    foreach (jq.JQGrid item in grids)
    //    {
    //        if (item.AjaxCallBackMode != jq.AjaxCallBackMode.None)
    //        {
    //            isPostBack = true;
    //            break;
    //        }
    //    }
    //    return isPostBack;
    //}
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdBeds)) {
            if (!string.IsNullOrEmpty(this.SelectedData))
            {
                DropDownHelper.FillDropdown(ddlCurrentReservation, "CU", "dlGST", Globals.CurrentAppLanguageCode, null);  
                //Fill Courses
                //DropDownHelper.FillCourses(ddlCourses, new ListItem("Select Course", ""), Globals.CurrentAppLanguageCode);
            }
            
            //Init Temp Data Source
            bool applyPrice = ddlCurrentReservation.SelectedValue.ToString() == ((int)StatusGuestType.Guest).ToString();
            //ProcessReservation.BedCart = new List<Bed>();
            Dictionary<string, int[]> data = JsonConvert.DeserializeObject<Dictionary<string, int[]>>(this.SelectedData);
            ReservationCartHelper.InitCart(data, applyPrice, Globals.CurrentAppLanguageCode);

            //Check if checkin date is require to validate min no of days being reserved
            /*List<int> lRomType;
            ReservationSpecialDays spDays = new ReservationSpecialDays();
            foreach (var item in ReservationCartHelper.CurrentCart.Items)
            {
                lRomType = new List<int>();
                lRomType.Add(item.RoomType);
                int minDaysBeingReserved = item.CheckOutDate.Subtract(item.CheckInDate).Days;
                if (!spDays.IsValidChecInkDate(item.CheckInDate, minDaysBeingReserved, lRomType))
                {
                    string msg = string.Empty;
                    if (item.CheckInDate == spDays.IfCheckinDate)
                    {
                        msg = string.Format(Resources.Resource.msgMinDaysRequired, item.CheckInDate, item.CheckOutDate.Subtract(item.CheckInDate).Days, spDays.MinDaysToBeReserved);
                    }
                    else
                    {
                        msg = string.Format(Resources.Resource.msgMinDaysRequired1, item.CheckInDate, item.CheckOutDate.Subtract(item.CheckInDate).Days);
                        //msg = string.Format(Resources.Resource.msgMinDaysRequired, item.CheckInDate, item.CheckOutDate.Subtract(item.CheckInDate).Days, spDays.MinDaysToBeReserved);
                    }

                    string title = Globals.CurrentAppLanguageCode == AppLanguageCode.FR ? spDays.SpecialNameFr : spDays.SpecialNameEn;
                    title += string.Format(" (" + Resources.Resource.msgDateForNights + ")", spDays.IfCheckinDate, spDays.MinDaysToBeReserved);

                    //string msg = Globals.CurrentAppLanguageCode == AppLanguageCode.FR ? spDays.MessageToDisplayFr : spDays.MessageToDisplayEn;
                    //string title = Globals.CurrentAppLanguageCode == AppLanguageCode.FR ? spDays.SpecialNameFr : spDays.SpecialNameEn;
                    MessageState.SetGlobalMessage(MessageType.Failure, string.Format("<b>{0}<b/><br />{1}", title, msg));
                    Globals.RegisterCloseDialogScript(this, "parent.getGlobalMessage();", true);
                    break;
                }  
            }*/           
            
                                 

            //ProcessReservation.GetSelectedBeds(data, applyPrice, Globals.CurrentAppLanguageCode);
            //DataTable dtTemp = Globals.ToDataTable<Bed>(ProcessReservation.BedCart, true);

            //Add Error Data Coulumn to Hold error during jqgrid reuqext process 
            //dtTemp.Columns.Add("ErrorData").AllowDBNull = true;

            //Session["TempSelectedRanges"] = dtTemp;

            if (ReservationCartHelper.CurrentCart.Items.Count <= 0)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPleaseSelectValidBedsAndDateRanges);
                Globals.RegisterCloseDialogScript(this, "parent.getGlobalMessage();", true);
            }
        }
    }

    private bool IsCartLoaded
    {
        get {
            return hdnCartLoaded.Value == "1";
        }        
    }

    private DataTable TempSource
    {
        get {
            return (DataTable)Session["TempSelectedRanges"];
        }
        set {
            Session["TempSelectedRanges"] = value;
        }
    }    

    public string SelectedData
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["data"]);
        }
    }        
   
   
    protected void btnReserveNow_Click(object sender, EventArgs e)
    {
        Globals.RegisterParentRedirectPageUrl(this, string.Format("CheckOut.aspx?rtype={0}", ddlCurrentReservation.SelectedValue));        
    }

    protected void btnAddABed_Click(object sender, EventArgs e)
    {
        Response.Redirect("default.aspx");
    }

    protected string GetRoomType(object idVal)
    { 
        return Globals.GetRoomType(BusinessUtility.GetInt(idVal));
    }

    protected void grdBeds_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 1)
        {
            e.CellHtml = string.Format("<b>{0}</b>/{1}/{2}/{3}", this.GetRoomType(e.RowValues[4]), e.RowValues[2], e.RowValues[3], e.RowValues[1]);
        }
        else if (e.ColumnIndex == 2)
        {
            e.CellHtml = string.Format(@"<input id=""{1}_fDate"" class=""jq_datepicker"" type=""text"" value=""{0}"" style=""width:90px"" rowKey=""{1}"" />", BusinessUtility.GetDateTimeString((DateTime)e.RowValues[5], DateFormat.MMddyyyy), e.RowKey);
        }
        else if (e.ColumnIndex == 3)
        {
            e.CellHtml = string.Format(@"<input id=""{1}_tDate"" class=""jq_datepicker"" type=""text"" value=""{0}"" style=""width:90px"" rowKey=""{1}"" />", BusinessUtility.GetDateTimeString((DateTime)e.RowValues[6], DateFormat.MMddyyyy), e.RowKey);
        }
        else if (e.ColumnIndex == 5)
        {
            e.CellHtml = string.Format(@"<input id=""{0}_price""  type=""text"" value=""{1}"" style=""width:50px"" rowKey=""{0}"" onchange='editPrice(this);' /><a class='inbiz_icon icon_save_24x24' >Save</a>", e.RowKey, e.CellHtml);
        }
        else if (e.ColumnIndex == 7)
        {
            e.CellHtml = string.Format(@"<a href=""javascript:void(0);"" onclick=""editRow({1});"">{0}</a>", "Save", e.RowKey);
        }
    }

    protected void grdBeds_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        //var r = Request.QueryString.AllKeys;
        DataTable dtTemp = this.TempSource;
        try
        {
            if (Request.QueryString.AllKeys.Contains("editRowKey"))
            {
                int rowKey = 0;
                if (int.TryParse(Request.QueryString["editRowKey"], out rowKey) && rowKey > 0)
                {
                    DateTime fDate = BusinessUtility.GetDateTime(Request.QueryString["editFDate"], DateFormat.MMddyyyy);
                    DateTime tDate = BusinessUtility.GetDateTime(Request.QueryString["editTDate"], DateFormat.MMddyyyy);

                    if (fDate == DateTime.MinValue || tDate == DateTime.MinValue || fDate >= tDate)
                    {
                        ReservationCartHelper.SetError(CustomExceptionCodes.INVALID_DATE_RANGE);
                        //throw new Exception(CustomExceptionCodes.INVALID_DATE_RANGE);
                        grdBeds.DataSource = ReservationCartHelper.CurrentCart.Items; 
                        return;
                    }

                    //ProcessReservation.EditSelectedBed(rowKey, fDate, tDate, dtTemp);
                    ReservationCartHelper.EditCartItem(rowKey, fDate, tDate);
                    
                    bool applyPrice = Request.QueryString["gstType"] == ((int)StatusGuestType.Guest).ToString();
                    //ProcessReservation.UpdateCartBedPrice(applyPrice, Globals.CurrentAppLanguageCode, dtTemp);
                    ReservationCartHelper.UpdateCartItemPrice(applyPrice, Globals.CurrentAppLanguageCode);

                    //this.TempSource = dtTemp;
                }
            }
            else if(Request.QueryString.AllKeys.Contains("applyPriceKey"))//applyPriceKey
            {
                bool applyPrice = Request.QueryString["applyPriceKey"] == ((int)StatusGuestType.Guest).ToString();

                //ProcessReservation.UpdateCartBedPrice(applyPrice, Globals.CurrentAppLanguageCode, dtTemp);
                //this.TempSource = dtTemp;

                ReservationCartHelper.UpdateCartItemPrice(applyPrice, Globals.CurrentAppLanguageCode);
            }
            else if (Request.QueryString.AllKeys.Contains("updatePriceRowKey"))
            {
                int rowKey = 0;
                double price = 0.0D;
                double.TryParse(Request.QueryString["priceToUpdate"], out price);
                if (int.TryParse(Request.QueryString["updatePriceRowKey"], out rowKey) && rowKey > 0)
                {
                    ReservationCartHelper.UpdateCartItemPrice(rowKey, price);
                }
            }
        }
        catch(Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.INVALID_DATE_RANGE)
            {
                //MessageState.SetGlobalMessage(MessageType.Information, "Invalid Date Range!");
                _errorMessage = Resources.Resource.msgInvalidDateRange;
            }
            else if (ex.Message == CustomExceptionCodes.ACCOMMODATION_NOT_AVAILABLE)
            {
                //MessageState.SetGlobalMessage(MessageType.Information, "Accomodation not available during selected date range!");
                _errorMessage = Resources.Resource.msgAccommodationNotAvaialbleDuringDateRange;
            }
        }

        grdBeds.DataSource = ReservationCartHelper.CurrentCart.Items;        
    }
    protected void grdBeds_DataRequested(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestedEventArgs e)
    {
        double total = 0.0D;
        foreach (DataRow item in e.DataTable.Rows)
        {
            total += BusinessUtility.GetDouble(item["TotalBedPrice"]);
        }

        grdBeds.Columns.FromDataField("TotalBedPrice").FooterValue = string.Format("Total: {0:F}", total); 
       
        //Set Error Message if Any
        if (!string.IsNullOrEmpty(ReservationCartHelper.CurrentCart.DataError))
        {
            if (ReservationCartHelper.CurrentCart.DataError == CustomExceptionCodes.INVALID_DATE_RANGE)
            {               
                grdBeds.Columns.FromDataField("ErrorData").FooterValue = Resources.Resource.msgInvalidDateRange;
            }
            else if (ReservationCartHelper.CurrentCart.DataError == CustomExceptionCodes.ACCOMMODATION_NOT_AVAILABLE)
            {               
                grdBeds.Columns.FromDataField("ErrorData").FooterValue = Resources.Resource.msgAccommodationNotAvaialbleDuringDateRange;
            }           
        }        
    }
}
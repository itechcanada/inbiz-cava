﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="GridResponce.aspx.cs" Inherits="Reservation_GridResponce" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">    
    <style type="text/css">
        .cell_selected{background-color:#5CAD00 !important;}
        .jqgrid_custom_cell{display:block;background-image:none;margin-right:-2px;margin-left:-2px;height:100%; padding:2px 4px;}
         .col_first{background:none repeat scroll 0 0 #D0DAFD;}
         .col_second{background:none repeat scroll 0 0 #EFF2FF;}
         .col_date{background:none repeat scroll 0 0 #FFFFFF;}
         .hide{display:none;}
         
         .cell_reserved{}/*{background-color:#C20329; color:#fff !important; font-weight:bold; cursor:pointer;}*/
        .cell_reserved_staff{background-color:#FFFF64; color:#000 !important; font-weight:normal;cursor:pointer;}
        .cell_reserved_spguest{background-color:#C7FF88; color:#000 !important; font-weight:normal;cursor:pointer;}
        .cell_reserved_cupart{background-color:#C7FF88; color:#000 !important; font-weight:normal;cursor:pointer;}

        .cell_reserved_guest_m{background-color:#3399FF; color:#fff !important; font-weight:normal; cursor:pointer;}
        .cell_reserved_guest_f{background-color:#FFAEFF; color:#fff !important; font-weight:normal; cursor:pointer;}
        .cell_reserved_guest_c{background-color:#9900CC; color:#fff !important; font-weight:normal; cursor:pointer;}
        
        .cell_black_out{background-color:#000; color:#fff !important; font-weight:normal; cursor:pointer;}
        .room_alter{background: #9BACFF;}
        .unpaid{border:2px solid red; margin-right:0px;margin-left:0px; padding:0px !important;}
        
        .td_hack {width: 100%;height: 100%;position: relative; margin-right:-2px;margin-left:-2px;padding:2px 4px;line-height:10px; font-size:9px;}        
        .td_overlay {position: absolute;top: 0;left: 0;width: 100%;height: 100%;background: #dfd;opacity: .50;filter:Alpha(Opacity=50);}
        .moving_reservedCell{opacity: .50;filter:Alpha(Opacity=50);}
    </style>

    <script type="text/javascript">
        //extend array prototype inorder to support index of function in IE browsers
        if (!Array.prototype.indexOf) {
            Array.prototype.indexOf = function (obj, start) {
                for (var i = (start || 0), j = this.length; i < j; i++) {
                    if (this[i] === obj) { return i; }
                }
                return -1;
            }
        }

        //===========================================================================
        // Provides a Dictionary object for client-side java scripts
        //===========================================================================
        function Lookup(key) {
            return (this[key]);
        }
        function Add() {
            for (c = 0; c < Add.arguments.length; c += 2) {
                // Add the property 
                this[Add.arguments[c]] = Add.arguments[c + 1];
                // And add it to the keys array
                var idx = this.Keys.indexOf(Add.arguments[c]);
                if (idx == -1) {
                    this.Keys[this.Keys.length] = Add.arguments[c];
                }
            }
        }

        function Delete() {
            for (c = 0; c < Delete.arguments.length; c++) {
                this[Delete.arguments[c]] = null;

            }
            // Adjust the keys (not terribly efficient) 
            var keys = new Array()
            for (var i = 0; i < this.Keys.length; i++) {
                if (this[this.Keys[i]] != null)
                    keys[keys.length] = this.Keys[i];
            }
            this.Keys = keys;
        }

        function Dictionary() {
            this.Add = Add;
            this.Lookup = Lookup;
            this.Delete = Delete;
            this.Keys = new Array();
        }
        //===========================================================================
        // Ends Disctionary Object
        //===========================================================================    

        //Temp variables to support parent page variables
        var partialMovingData = { reservationItemID: -1, fDate: -1, tDate: -1, targetFDate: -1, targetTDate: -1, bedID: -1, targetBedID: -1, roomDesc: "", targetRoomDesc: "", startCellIdx: -1, endCellIdx: -1, targetStartCellIdx: -1, targetEndCellIdx: -1, arrDatesToBeMoved: [], arrDatesToBeMovedOn: [] };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div id="gridResponceWrapper">
        <trirand:JQGrid runat="server" ID="grdReport" Height="550px" AutoWidth="true" OnDataRequesting="grdReport_DataRequesting"
            OnCellBinding="grdReport_CellBinding" OnDataRequested="grdReport_DataRequested">
            <PagerSettings PageSize="25" PageSizeOptions="[25,50,100]" 
                ScrollBarPaging="True" PagingMessage="test" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" />
            <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort"
                LoadComplete="gridLoadComplete" CellSelect="gridCellSelect" 
                GridInitialized="gridInited" />
        </trirand:JQGrid>
    </div>        
    <asp:TextBox ID="txtSerealizedSearchFilter" runat="server" CssClass="hide" />
    <asp:HiddenField ID="hdnCurrentIntervalDisplay" runat="server" />
    <div id="descContent">
        <input class="btn_focus" type="button" style="display:none;"/>
    </div>        
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server"> 
     <!--Bed Selection-->
    <script type="text/javascript">
//        arrBeds = new Dictionary();
//        rowClicks = new Dictionary();
//        rowClicks.Add("ROW_ID", "");
//        rowClicks.Add("START_CELL_IDX", -1);
//        rowClicks.Add("END_CELL_IDX", -1);

//        function resetEventsHistory() {
//            rowClicks["ROW_ID"] = -1;
//            rowClicks["START_CELL_IDX"] = -1;
//            rowClicks["END_CELL_IDX"] = -1;
//        }

        function setScrollPosition() {
            parent.setScrollPosition($(".ui-jqgrid-bdiv").scrollTop());            
        }
        //ui-jqgrid-bdiv
    </script>
  
    <script type="text/javascript">
        //Variables
        $jqGridFrame = $("#gridResponceWrapper");
        $jqGridCal = $("#<%=grdReport.ClientID%>");       

        //Function To Resize the grid
        function resize_the_grid() {
            //Set grid_wrapper width according to view port
            $jqGridFrame.width(parent.$jqGridFrame.width() - 2);            
            $jqGridCal.fluidGrid({ example: '#gridResponceWrapper', offset: -0 });
            $("tr.ui-jqgrid-labels th:eq(3), tr.jqgfirstrow td:eq(3)").width(120);
            $("tr.ui-jqgrid-labels th:eq(4), tr.jqgfirstrow td:eq(4)").width(100);           
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $jqGridCal.onJqGridPaging();
            alert(pgButton);           
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $jqGridCal.onJqGridSorting();            
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.        
        //$('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: $jqGridCal.attr("id") });

        function gridLoadComplete(data) {
            resize_the_grid();
            parent.setDateRangeDispalay($("#<%=hdnCurrentIntervalDisplay.ClientID%>").val());

            //Set selected dates as presereved
            for (var i = 0; i < parent.arrBeds.Keys.length; i++) {                
                var arrCells = parent.arrBeds.Lookup(parent.arrBeds.Keys[i]);
                for (var key in arrCells) {
                    var cellID = parent.arrBeds.Keys[i] + "_" + arrCells[key];
                    $cell = $("#" + cellID, $jqGridCal).parent();
                    if ($cell != null) {
                        $attrContainer = $cell.find(".td_hack").eq(0);                        
                        if (!$attrContainer.hasClass("cell_selected")) {                           
                            $attrContainer.addClass("cell_selected");
                        }                        
                    }
                }                
            }
            if (parent.isReservationDateMoving && parent.partialMovingData.reservationItemID > 0) {
                if (parent.partialMovingData.arrDatesToBeMoved.length > 0) {
                    for (var i = 0; i < parent.partialMovingData.arrDatesToBeMoved.length; i++) {
                        var cellID = parent.partialMovingData.bedID + "_" + parent.partialMovingData.arrDatesToBeMoved[i];
                        $cell = $("#" + cellID, $jqGridCal).parent();
                        if ($cell != null) {
                            $attrContainer = $cell.find(".td_hack").eq(0);
                            if (!$attrContainer.hasClass("moving_reservedCell")) {
                                $attrContainer.addClass("moving_reservedCell");
                            }
                            if (i == 0) {
                                parent.partialMovingData.startCellIdx = $cell.index();
                            }
                            else if (i == parent.partialMovingData.arrDatesToBeMoved.length - 1) {
                                 parent.partialMovingData.endCellIdx = $cell.index();
                            }
                        }                        
                    }                    
                }
                if (parent.partialMovingData.arrDatesToBeMovedOn.length > 0) {
                    for (var i = 0; i < parent.partialMovingData.arrDatesToBeMovedOn.length; i++) {
                        var cellID = parent.partialMovingData.targetBedID + "_" + parent.partialMovingData.arrDatesToBeMovedOn[i];
                        $cell = $("#" + cellID, $jqGridCal).parent();
                        if ($cell != null) {
                            $attrContainer = $cell.find(".td_hack").eq(0);
                            if (!$attrContainer.hasClass("cell_selected")) {
                                $attrContainer.addClass("cell_selected");
                            }
                            if (i == 0) {
                                parent.partialMovingData.targetStartCellIdx = $cell.index();
                            }
                            else if (i == parent.partialMovingData.arrDatesToBeMoved.length - 1) {
                                parent.partialMovingData.targetEndCellIdx = $cell.index();
                            }
                        }
                    }
                }
            }


            var roomID = -1;
            var altClass = "room_alter";
            $(".room_display", $jqGridCal).each(function () {
                if ($(this).attr("room-id") != roomID) {                    
                    altClass = altClass.length > 0 ? "" : "room_alter";
                }
                $(this).addClass(altClass);
                roomID = $(this).attr("room-id");
            });
        }

        function gridInited() {            
            if (parent.gridScrollPos > 0) {
                var srl = parent.gridScrollPos;
                parent.gridScrollPos = 0;
                $(".ui-jqgrid-bdiv").scrollTop(srl)                                
            }            
        }



        /*$(".ui-jqgrid-bdiv").live("scroll", function () {            
            parent.gridScrollPos = $(this).scrollTop();
        });*/   

        function setFilterOptions(data) {
            //To Do set filter options
            if (data) {
                //alert(JSON.stringify(data));
                for (var key in data) {
                    $jqGridCal.setPostDataItem(key, data[key]);
                    //alert(key + ' - ' + data[key]);
                }
               
                $jqGridCal.trigger("reloadGrid", [{ page: 1}]);
            }                      
        }                
    </script>
    <!--Bed Selection Helper-->
    <script type="text/javascript">
        function gridCellSelect(rowid, iCol, cellcontent, e) {
            if (iCol <= 4) {
                return;
            }            
           
            //Check if in moving mode then need to prevent selecting empty cell if reservation item id not found
            if (parent.isReservationDateMoving) {                
                moveReservationSelection(rowid, iCol, cellcontent, e);                
            }
            else {
                makeReservationSelection(rowid, iCol, cellcontent, e);
            }
        }

        function makeReservationSelection(rowid, iCol, cellcontent, e) {
            if (iCol <= 4) {
                return;
            }
            $row = $("#" + rowid, $jqGridCal);
            $cell = $("td", $row).eq(iCol);

            //Main Attributes Container
            $attrContainer = $cell.find(".td_hack").eq(0);

            //Check for blackouts
            var isBlackout = $attrContainer.attr("isblackout") != undefined && $attrContainer.attr("isblackout") == "true";
            if (isBlackout) {
                $attrContainer.removeClass("cell_black_out");
            }


            var toggle = true;
            if ($attrContainer.attr("isreserved") != undefined && $attrContainer.attr("isreserved") == "true") {
                toggle = false;
            }
            
            parent.arrBeds.Add(rowid, []);

            if (parent.rowClicks["ROW_ID"] != rowid) {
                parent.resetEventsHistory();
            }
            //if not find reserved cell
            if (toggle) {
                //if cell being selected
                if (!$attrContainer.hasClass("cell_selected")) {
                    $attrContainer.addClass("cell_selected");

                    parent.rowClicks["ROW_ID"] = rowid;
                    if (parent.rowClicks["START_CELL_IDX"] < 0) {
                        parent.rowClicks["END_CELL_IDX"] = -1;
                        parent.rowClicks["START_CELL_IDX"] = iCol;                        
                    }
                    else if (parent.rowClicks["END_CELL_IDX"] < 0) {
                        parent.rowClicks["END_CELL_IDX"] = iCol;
                        //alert(parent.rowClicks["START_CELL_IDX"] + "-" + parent.rowClicks["END_CELL_IDX"]);
                        //Set cells selected
                        if (parent.rowClicks["START_CELL_IDX"] < parent.rowClicks["END_CELL_IDX"]) {
                            $("td", $row).slice(parent.rowClicks["START_CELL_IDX"] + 1, parent.rowClicks["END_CELL_IDX"]).each(function () {
                                var l_lnRes = $('.td_hack', this).hasClass("cell_reserved"); //$(this).find(".cell_reserved").length;
                                var l_lnSel = $('.td_hack', this).hasClass("cell_selected"); //$(this).find(".cell_selected").length;
                                //var l_lnbo = $(this).find(".cell_black_out").length;
                                if (!l_lnRes && !l_lnSel) {
                                    $('.td_hack', this).addClass("cell_selected");
                                    $('.td_hack', this).removeClass("cell_black_out");
                                }
                            });
                        }
                        else {
                            $("td", $row).slice(parent.rowClicks["END_CELL_IDX"] + 1, parent.rowClicks["START_CELL_IDX"]).each(function () {
                                var l_lnRes = $('.td_hack', this).hasClass("cell_reserved"); //$(this).find(".cell_reserved").length;
                                var l_lnSel = $('.td_hack', this).hasClass("cell_selected"); //$(this).find(".cell_selected").length;
                                //var l_lnbo = $(this).find(".cell_black_out").length;
                                if (!l_lnRes && !l_lnSel) {
                                    $('.td_hack', this).addClass("cell_selected");
                                    $('.td_hack', this).removeClass("cell_black_out");
                                }
                            });
                        }
                        parent.resetEventsHistory();
                    }
                    else {
                        parent.resetEventsHistory();
                    }

                    //reset date range
                    var loc_arrDates = [];
                    $(".cell_selected", $row).each(function () {
                        loc_arrDates.push($(this).attr("cell_date_val"));
                    });
                    parent.arrBeds[rowid] = loc_arrDates;
                }
                else {//else find already selected
                    $attrContainer.removeClass("cell_selected");
                    if (isBlackout) {
                        $attrContainer.addClass("cell_black_out");
                    }
                    parent.resetEventsHistory();

                    var loc_arrDates = [];
                    $(".cell_selected", $row).each(function () {
                        loc_arrDates.push($(this).attr("cell_date_val"));
                    });
                    parent.arrBeds[rowid] = loc_arrDates;
                }
            }
            else {
                var soid = parseInt($attrContainer.attr("soid"));
                //$digContent = $(".desc_dialog", $cell);
                if (soid > 0) {
                    //parent.location.href = 'redirect.aspx?soid=' + soid;                        
                    var overlay = '<div class="td_overlay">'
                                        + '<img id="imgLoader" src="images/ajax-loader.gif" style="vertical-align:middle;" />'
                                        + '</div>';
                    $attrContainer.append(overlay);
                    $('#descContent').load('desc.aspx?soid=' + soid, {}, function (responseText, textStatus, XMLHttpRequest) {
                        $("button, input:submit, input:button", this).button();
                        $(this).dialog({
                            title: soid,
                            width: 300,
                            height: 180,
                            position: {
                                my: 'right',
                                at: 'left',
                                of: $cell
                            }
                        });
                        $attrContainer.find('.td_overlay').remove();
                    });
                }
            }
            parent.setDataToSubmit();
        }

        function moveReservationSelection(rowid, iCol, cellcontent, e) {            
            if (iCol <= 4) {
                return;
            }
            $row = $("#" + rowid, $jqGridCal);
            $cell = $("td", $row).eq(iCol);

            //Main Attributes Container
            $attrContainer = $cell.find(".td_hack").eq(0);

            //Check for blackouts
            var isBlackout = $attrContainer.attr("isblackout") != undefined && $attrContainer.attr("isblackout") == "true";
            if (isBlackout) {
                $attrContainer.removeClass("cell_black_out");
            }

            var isReserved = $attrContainer.attr("isreserved") != undefined && $attrContainer.attr("isreserved") == "true";                                    

            //Check if in moving mode then need to prevent selecting empty cell if reservation item id not found
            if (parent.isReservationDateMoving && parent.partialMovingData.reservationItemID <= 0) {                
                if ($attrContainer.hasClass("invoiced")) {
                    alert("Selected reservation was Invoiced & can not be moved further!");
                    return;
                }
                if ($attrContainer.attr("res_item_id") == undefined || $attrContainer.attr("res_item_id") <= 0) {
                    alert("Please select valid reserved date range to be moved!");
                    return;
                }            
                parent.partialMovingData.reservationItemID = $attrContainer.attr("res_item_id");
                parent.partialMovingData.startCellIdx = iCol;
                parent.partialMovingData.bedID = $attrContainer.attr("row_id");
                parent.partialMovingData.fDate = $attrContainer.attr("cell_date_val");
                parent.partialMovingData.arrDatesToBeMoved.push(parent.partialMovingData.fDate);
                $('.td_hack', $cell).addClass("moving_reservedCell");
                //alert("You have selected reserved bed to move, now click on available start date cell to move!");
                parent.setTitle(); //Set help title
                return;
            }
            else if (parent.isReservationDateMoving && parent.partialMovingData.tDate <= 0) {
                var toggle = parent.partialMovingData.reservationItemID > 0 && parent.partialMovingData.fDate > 0 && parent.partialMovingData.tDate > 0;
                var rID = $attrContainer.attr("row_id");
                if (rID != parent.partialMovingData.bedID || $attrContainer.attr("res_item_id") != parent.partialMovingData.reservationItemID) {
                    alert("Please select valid reserved date range to be moved!");
                    return;
                }
                if (!toggle) {
                    var idxClicked = iCol;
                    var dateClicked = $attrContainer.attr("cell_date_val");
                    if (idxClicked < parent.partialMovingData.startCellIdx) {
                        idxClicked = parent.partialMovingData.startCellIdx;
                        parent.partialMovingData.startCellIdx = iCol;
                        parent.partialMovingData.endCellIdx = idxClicked;
                        dateClicked = parent.partialMovingData.fDate;
                        parent.partialMovingData.fDate = $attrContainer.attr("cell_date_val");
                        parent.partialMovingData.tDate = dateClicked;
                    }
                    else {
                        parent.partialMovingData.endCellIdx = idxClicked;
                        parent.partialMovingData.tDate = dateClicked;
                    }
                    parent.partialMovingData.arrDatesToBeMoved = [];
                    $("td", $row).slice(parent.partialMovingData.startCellIdx, parent.partialMovingData.endCellIdx + 1).each(function () {
                        $('.td_hack', this).addClass("moving_reservedCell");
                        parent.partialMovingData.arrDatesToBeMoved.push($('.td_hack', this).attr("cell_date_val"));
                    });
                    parent.setTitle(); //Set Help title
                    return;
                }
                else {
                    alert("Please select valid reserved date range to be moved!");
                    return;
                }
            }
            else if (parent.isReservationDateMoving && parent.partialMovingData.targetFDate <= 0) {
                if (isReserved && $attrContainer.attr("res_item_id") != parent.partialMovingData.reservationItemID) { //means reserver by other
                    alert("Please select valid date range to be selected reservation moved on!");
                    return;
                }
                parent.partialMovingData.targetStartCellIdx = iCol;
                parent.partialMovingData.targetBedID = $attrContainer.attr("row_id");
                parent.partialMovingData.targetFDate = $attrContainer.attr("cell_date_val");
                parent.partialMovingData.arrDatesToBeMovedOn.push(parent.partialMovingData.targetFDate);
                $('.td_hack', $cell).addClass("cell_selected");
                if (isReserved) {
                    //$('.td_hack', $cell).removeClass
                }
                //alert("You have selected reserved bed to move, now click on available start date cell to move!");
                //Set Help title
                parent.setTitle();
                return;
            }
            else if (parent.isReservationDateMoving && parent.partialMovingData.targetTDate <= 0) {
                var rID = $attrContainer.attr("row_id");
                if (rID != parent.partialMovingData.targetBedID) {
                    alert("Please select valid date range to be selected reservation moved on!");
                    return;
                }
                if (isReserved && $attrContainer.attr("res_item_id") != undefined && $attrContainer.attr("res_item_id") != parent.partialMovingData.reservationItemID) {
                    alert("Please select valid date range to be selected reservation moved on!");
                    return;
                }
                var idxClicked = iCol;
                var dateClicked = $attrContainer.attr("cell_date_val");
                if (idxClicked < parent.partialMovingData.targetStartCellIdx) {
                    idxClicked = parent.partialMovingData.targetStartCellIdx;
                    parent.partialMovingData.targetStartCellIdx = iCol;
                    parent.partialMovingData.targetEndCellIdx = idxClicked;
                    dateClicked = parent.partialMovingData.targetFDate;
                    parent.partialMovingData.targetFDate = $attrContainer.attr("cell_date_val");
                    parent.partialMovingData.targetTDate = dateClicked;
                }
                else {
                    parent.partialMovingData.targetEndCellIdx = idxClicked;
                    parent.partialMovingData.targetTDate = dateClicked;
                }               
                var checkOverlapping = false;
                $("td", $row).slice(parent.partialMovingData.targetStartCellIdx, parent.partialMovingData.targetEndCellIdx + 1).each(function () {
                    if ($('.td_hack', this).attr("res_item_id") != undefined && $('.td_hack', this).attr("res_item_id") != parent.partialMovingData.reservationItemID) {
                        alert("Your are trying to overlapping Reservation Item with other Reservation Item that is not allowed!");
                        checkOverlapping = true;
                        return false;
                    }                    
                });
                if (!checkOverlapping) {
                    parent.partialMovingData.arrDatesToBeMovedOn = [];
                    $("td", $row).slice(parent.partialMovingData.targetStartCellIdx, parent.partialMovingData.targetEndCellIdx + 1).each(function () {
                        $('.td_hack', this).addClass("cell_selected");
                        parent.partialMovingData.arrDatesToBeMovedOn.push($('.td_hack', this).attr("cell_date_val"));
                    });

                    parent.movePartialReservation();
                }
                else {
                    parent.partialMovingData.targetEndCellIdx = -1;
                    parent.partialMovingData.targetTDate = -1;
                    parent.partialMovingData.targetStartCellIdx = -1;
                    parent.partialMovingData.targetFDate = -1;
                    $(".td_hack", $jqGridCal).removeClass("cell_selected");
                }
                parent.setTitle(); //Set help title            
                return;
            }
        }

        //Rest Selection in Grid
        function resetSelectionInGrid() {
            $(".td_hack", $jqGridCal).removeClass("cell_selected");
            $(".td_hack", $jqGridCal).removeClass("moving_reservedCell");
            $(".td_hack", $jqGridCal).each(function () {
                var isReserved = $(this).attr("isreserved") != undefined && $(this).attr("isreserved") == "true";
                var isBlackout = $(this).attr("isblackout") != undefined && $(this).attr("isblackout") == "true";
                if (isReserved) {
                    $(this).addClass("cell_reserved");
                }
                if (isBlackout) {
                    $(this).addClass("cell_black_out");
                }
            });
        }

        function reloadGridOnScrollPos() {
            setScrollPosition();            
            $jqGridCal.trigger("reloadGrid");
        }

        function test() {
            var v = [];
            var r = [];
            var t = [];
            $(".td_hack").each(function () {
                if ($(this).attr("isreserved") == "true") {
                    t.push($(this).attr("soid"));
                    if (v.indexOf($(this).attr("soid")) == -1) {
                        v.push($(this).attr("soid"));
                    }
                    if (r.indexOf($(this).attr("res_item_id")) == -1) {
                        r.push($(this).attr("res_item_id"));
                    }                   
                }
            });
            //alert(v.join(","));
            alert("Total Guests:" + r.length + "\n Total Nights: " + t.length);
        }
    </script>    
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/fullWidth.master" AutoEventWireup="true" CodeFile="ReservationCalendar.aspx.cs" Inherits="Reservation_ReservationCalendar" %>
<%@ Import Namespace="iTECH.InbizERP.BusinessLogic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <%=UIHelper.GetScriptTags("blockui,momentjs", 1)%>    
    <script src="../lib/scripts/jquery-plugins/jquery.addplaceholder.js" type="text/javascript"></script>              
    <script type="text/javascript">
        //extend array prototype inorder to support index of function in IE browsers
        if (!Array.prototype.indexOf) {
            Array.prototype.indexOf = function (obj, start) {
                for (var i = (start || 0), j = this.length; i < j; i++) {
                    if (this[i] === obj) { return i; }
                }
                return -1;
            }
        }

        //===========================================================================
        // Provides a Dictionary object for client-side java scripts
        //===========================================================================
        function Lookup(key) {
            return (this[key]);
        }
        function Add() {
            for (c = 0; c < Add.arguments.length; c += 2) {
                // Add the property 
                this[Add.arguments[c]] = Add.arguments[c + 1];
                // And add it to the keys array
                var idx = this.Keys.indexOf(Add.arguments[c]);
                if (idx == -1) {
                    this.Keys[this.Keys.length] = Add.arguments[c];
                }
            }
        }

        function Delete() {
            for (c = 0; c < Delete.arguments.length; c++) {
                this[Delete.arguments[c]] = null;

            }
            // Adjust the keys (not terribly efficient) 
            var keys = new Array()
            for (var i = 0; i < this.Keys.length; i++) {
                if (this[this.Keys[i]] != null)
                    keys[keys.length] = this.Keys[i];
            }
            this.Keys = keys;
        }

        function Dictionary() {
            this.Add = Add;
            this.Lookup = Lookup;
            this.Delete = Delete;
            this.Keys = new Array();
        }
        //===========================================================================
        // Ends Disctionary Object
        //===========================================================================        

        function blockContentArea(elementToBlock, loadingMessage) {
            $(elementToBlock).block({
                message: '<div>' + loadingMessage + '</div>',
                css: { border: '3px solid #a00' }
            });
        }
        function unblockContentArea(elementToBlock) {
            $(elementToBlock).unblock();
        }


        //Global varaibles
        var _isRequireReload = false;
        var isReservationDateMoving = false;                
        var reservationItemID = -1;        
        var gridScrollPos = 0;
        var bedIDToMoveOn = 0;
        var startDateToMoveOn = "";

        var partialMovingData = { reservationItemID: -1, fDate: -1, tDate: -1, targetFDate: -1, targetTDate: -1, bedID: -1, targetBedID: -1, roomDesc: "", targetRoomDesc: "", startCellIdx:-1, endCellIdx:-1, targetStartCellIdx: -1, targetEndCellIdx:-1, arrDatesToBeMoved: [], arrDatesToBeMovedOn: [] };

        function resetMovingData() {
            partialMovingData.fDate = -1;
            partialMovingData.tDate = -1;
            partialMovingData.reservationItemID = -1;
            partialMovingData.targetFDate = -1;
            partialMovingData.targetTDate = -1;
            partialMovingData.bedID = -1;
            partialMovingData.targetBedID = -1;
            partialMovingData.roomDesc = "";
            partialMovingData.targetRoomDesc = "";
            partialMovingData.startCellIdx = -1;
            partialMovingData.endCellIdx = -1;
            partialMovingData.targetStartCellIdx = -1;
            partialMovingData.targetEndCellIdx = -1;
            partialMovingData.arrDatesToBeMoved = [];
            partialMovingData.arrDatesToBeMovedOn = [];
        }

        function setTitle() {
            if (isReservationDateMoving) {
                $("#spReservationMode").hide();
                if (partialMovingData.fDate < 0) {
                    $("#spMoveMode_0").show();
                    $("#spMoveMode_1").hide();
                    $("#spMoveMode_2").hide();
                    $("#spMoveMode_3").hide();
                    $("#spMoveMode_4").hide();
                }
                else if (partialMovingData.tDate < 0) {
                    $("#spMoveMode_0").hide();
                    $("#spMoveMode_1").show();
                    $("#spMoveMode_2").hide();
                    $("#spMoveMode_3").hide();
                    $("#spMoveMode_4").hide();
                }
                else if (partialMovingData.targetFDate < 0) {
                    $("#spMoveMode_0").hide();
                    $("#spMoveMode_1").hide();
                    $("#spMoveMode_2").show();
                    $("#spMoveMode_3").hide();
                    $("#spMoveMode_4").hide();
                }
                else if (partialMovingData.targetTDate < 0) {
                    $("#spMoveMode_0").hide();
                    $("#spMoveMode_1").hide();
                    $("#spMoveMode_2").hide();
                    $("#spMoveMode_3").show();
                    $("#spMoveMode_4").hide();
                }
                else {
                    $("#spMoveMode_0").hide();
                    $("#spMoveMode_1").hide();
                    $("#spMoveMode_2").hide();
                    $("#spMoveMode_3").hide();
                    $("#spMoveMode_4").show();
                }
            }
            else {
                $("#spReservationMode").show();
                $("#spMoveMode_0").hide();
                $("#spMoveMode_1").hide();
                $("#spMoveMode_2").hide();
                $("#spMoveMode_3").hide();
                $("#spMoveMode_4").hide();
            }

            $('.divTitle').animate({ backgroundColor: 'gold' }, 250, function () {
                $(this).animate({ backgroundColor: '#fff' }, 2000);
            }
                        );
        }
    </script>
    <style type="text/css">
         .paint{background-color:#5CAD00;}
         .jqgrid_custom_cell{display:block;background-image:none;margin-right:-2px;margin-left:-2px;height:100%; padding:4px;}
         .col_first{background:none repeat scroll 0 0 #D0DAFD;}
         .col_second{background:none repeat scroll 0 0 #EFF2FF;}
         .col_date{background:none repeat scroll 0 0 #FFFFFF;}
         .hide{display:none;}
         #toolbar {padding: 2px 4px;}
         
        .cell_reserved{}/*{background-color:#C20329; color:#fff !important; font-weight:bold; cursor:pointer;}*/
        .cell_reserved_staff{background-color:#FFFF64; color:#fff !important; font-weight:bold;cursor:pointer;}
        .cell_reserved_spguest{background-color:#C7FF88; color:#fff !important; font-weight:bold;cursor:pointer;}
        .cell_reserved_cupart{background-color:#C7FF88; color:#fff !important; font-weight:bold;cursor:pointer;}

        .cell_reserved_guest_m{background-color:#3399FF; color:#fff !important; font-weight:bold; cursor:pointer;}
        .cell_reserved_guest_f{background-color:#FFAEFF; color:#fff !important; font-weight:bold; cursor:pointer;}
        .cell_reserved_guest_c{background-color:#9900CC; color:#fff !important; font-weight:bold; cursor:pointer;}
        
        .cell_black_out{background-color:#000; color:#fff !important; font-weight:bold; cursor:pointer;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div style="text-align: right; margin-bottom: 5px;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td valign="top" style="text-align:left;">
                    <div class="divTitle" style="margin: 0px; font-weight: bold; font-size: 12px; margin-left:220px;">                        
                        <span id="spReservationMode">
                            <%=Resources.Resource.lblReservationMode%>
                        </span>
                        <span id="spMoveMode_0" style="display: none;"><%=Resources.Resource.lblMoveModeTitle0%></span> 
                        <span id="spMoveMode_1" style="display: none;"><%=Resources.Resource.lblMoveModeTitle1%></span> 
                        <span id="spMoveMode_2" style="display: none;"><%=Resources.Resource.lblMoveModeTitle2%></span>
                        <span id="spMoveMode_3" style="display: none;"><%=Resources.Resource.lblMoveModeTitle3%></span>
                        <span id="spMoveMode_4" style="display: none;"><%=Resources.Resource.lblMoveModeTitle4%></span>
                    </div>
                </td>
                <td valign="top">
                    <span id="spReserve">
                        <asp:Button ID="btnClearAll" Text="<%$Resources:Resource, lblClearAll %>" runat="server" />
                        <asp:Button ID="btnReserve" Text="<%$Resources:Resource, lblReserve %>" runat="server" />
                        <asp:Button ID="btnMove" Text="<%$Resources:Resource, lblMoveOrCancelReservation %>"
                            runat="server" OnClick="btnMove_Click" />
                    </span><span id="spMoving" style="display: none;">
                        <asp:Button ID="btnClearAll2" Text="<%$Resources:Resource, lblClearAll %>" runat="server" />
                        <asp:Button ID="btnMoveReservation" Text="<%$Resources:Resource, lblMove%>" runat="server"
                            OnClientClick="movePartialReservation(); return false;" />
                        <asp:Button ID="btnPartialCancel" runat="server" Text="<%$Resources:Resource, lblCancelReservation%>"
                            OnClientClick="cancelPartialReservation(); return false;" />
                        <asp:Button ID="btnCancelMove" Text="<%$Resources:Resource, lblCancelMoving %>" runat="server" />
                    </span>
                </td>
            </tr>
        </table>
    </div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="top" style="width: 220px;">
                <div class="cls_datepicker">
                </div>
                <table id="tblSearch" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="pnlSearch" runat="server">
                                <h2>
                                    <asp:Label ID="lblFilterOption" Text="<%$Resources:Resource, lblFilterOptions %>" runat="server" />
                                </h2>
                                <div style="margin: 5px 0px;">
                                    <asp:Label ID="lblBuilidings" AssociatedControlID="ddlBuildings" CssClass="hide filter-key" Text="" runat="server" />
                                    <asp:DropDownList ID="ddlBuildings" runat="server" Width="200px" data-placeholder="[All Buildings]" CssClass="val_filter modernized_select" search-key="building">
                                        <asp:ListItem Value="" Text="" />
                                    </asp:DropDownList>
                                </div>
                                <div style="margin: 5px 0px; display:none;">
                                    <asp:Label ID="Label1" AssociatedControlID="ddlRooms" CssClass="hide filter-key" Text="" runat="server" />
                                    <asp:DropDownList ID="ddlRooms" runat="server" Width="200px" data-placeholder="[All Rooms]" CssClass="val_filter modernized_select" search-key="room">
                                        <asp:ListItem Value="" Text="" />
                                    </asp:DropDownList>
                                </div>
                                <div style="margin: 5px 0px; display:none;">
                                    <asp:Label ID="Label2" AssociatedControlID="ddlBeds" CssClass="hide filter-key" Text="" runat="server" />
                                    <asp:DropDownList ID="ddlBeds" runat="server" Width="200px" data-placeholder="[All Beds]" CssClass="val_filter modernized_select" search-key="bed">
                                        <asp:ListItem Value="" Text="" />
                                    </asp:DropDownList>
                                </div>
                                <div style="margin: 5px 0px; height: 100px; width: 200px; overflow-y: scroll; border: 1px solid #797D7F">
                                    <asp:Repeater ID="rptAmenities" DataSourceID="sdsAttributes" runat="server">
                                        <ItemTemplate>
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <input id="chkAmenity" class="filter_amenity" type="checkbox" value='<%#Eval("AttributeID")%>'
                                                            runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblAmenity" AssociatedControlID="chkAmenity" Text='<%#Eval("AttributeName")%>'
                                                            runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:SqlDataSource ID="sdsAttributes" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                                        ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
                                </div>
                                <!--Hidden Search Filters-->
                                <div class="hide">                                    
                                    <asp:Label ID="Label3" AssociatedControlID="txtAmenities" CssClass="hide filter-key" Text="" runat="server" />
                                    <asp:TextBox ID="txtAmenities" runat="server"  CssClass="val_filter" search-key="amenities" />                                                                        
                                </div>
                                <div style="margin: 5px 0px; text-align: right; width: 200px;">
                                    <input id="btnAvailable" type="button" value="Available" />                                    
                                    <input id="btnSearch" type="button" value="Search" />
                                </div>
                            </asp:Panel>
                            <div style="margin: 5px 0px;">
                                <asp:Panel ID="pnlGuestSearch" DefaultButton="btnGuestSearch" runat="server">
                                    <asp:TextBox ID="txtSearch" CssClass="search_guest_staff" runat="server" Width="170px"
                                        ph="Search Guest Or Staff" Style="padding: 4px 2px;" />
                                    <asp:LinkButton ID="lnkSearchGuest" Text="" CssClass="inbiz_icon icon_search_24" runat="server" OnClick="btnGuestSearch_Click" />
                                    <asp:Button ID="btnGuestSearch" Text="" runat="server" 
                                        onclick="btnGuestSearch_Click" style="display:none;" />
                                </asp:Panel>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 2px;">
                            <div class="cell_reserved_guest_f" style="width: 15px; height: 15px; border: 1px solid #ccc !important;">
                                &nbsp;</div>
                        </td>
                        <td style="padding: 2px;">
                             <asp:Label ID="Label4" Text="<%$Resources:Resource, lblReserrvedForFemailGuest %>" runat="server" />
                            
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 2px;">
                            <div class="cell_reserved_guest_m" style="width: 15px; height: 15px; border: 1px solid #ccc !important;">
                                &nbsp;</div>
                        </td>
                        <td style="padding: 2px;">
                            <asp:Label ID="Label5" Text="<%$Resources:Resource, lblReservedForMailGuest %>" runat="server" />                            
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 2px;">
                            <div class="cell_reserved_guest_c" style="width: 15px; height: 15px; border: 1px solid #ccc !important;">
                                &nbsp;</div>
                        </td>
                        <td style="padding: 2px;">
                            <asp:Label ID="Label6" Text="<%$Resources:Resource, lblReservedForCouple %>" runat="server" />                            
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 2px;">
                            <div class="cell_reserved_staff" style="width: 15px; height: 15px; border: 1px solid #ccc !important;">
                                &nbsp;</div>
                        </td>
                        <td style="padding: 2px;">
                            <asp:Label ID="Label7" Text="<%$Resources:Resource, lblReservedForStaff %>" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 2px;">
                            <div class="cell_reserved_spguest" style="width: 15px; height: 15px; border: 1px solid #ccc !important;">
                                &nbsp;</div>
                        </td>
                        <td style="padding: 2px;">
                            <asp:Label ID="Label8" Text="<%$Resources:Resource, lblReservedForSpecialGuest %>" runat="server" />                            
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 2px;">
                            <div class="cell_black_out" style="width: 15px; height: 15px; border: 1px solid #ccc !important;">
                                &nbsp;</div>
                        </td>
                        <td style="padding: 2px;">
                            <asp:Label ID="Label9" Text="<%$Resources:Resource, lblBlackoutDates %>" runat="server" />                            
                        </td>
                    </tr>
                </table>
            </td>
            <td id="tdRightColmn" valign="top">                
                <div>
                   <div id="toolbar" class="ui-widget-header ui-corner-all">
                         <div style="float: left;">
                            <button id="btnToday" onclick="shiftDates(false, 0); return false;" style="margin:0px">
                                <span class="label" style="color:#fff;text-shadow:none;"><%=Resources.Resource.lblToday%></span> 
                            </button>
	                        <button id="beginning" onclick="shiftDates(true, -1); return false;" style="margin:0px 0px 0px 5px;">first</button> 
	                        <button id="rewind"  onclick="shiftDates(false, -1); return false;" style="margin:0px 0px 0px 5px;">previous</button> 	                     	                    
	                        <button id="forward" onclick="shiftDates(false, 1); return false;" style="margin:0px 0px 0px 5px;">next</button> 
	                        <button id="end" onclick="shiftDates(true, 1); return false;" style="margin:0px 5px 0px 5px;">last</button> 
                            <span id="dislayDateRange" class="display_date_range"></span>
	                      </div>	                                          
	                    <div style="float: right; text-align: right;">
                            <asp:RadioButtonList ID="repeat" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                <asp:ListItem Text="<%$Resources:Resource, lblDayView%>" Value="day" />
                                <asp:ListItem Text="<%$Resources:Resource, lblWeekView%>"  Value="rweek" />
                                <asp:ListItem Text="<%$Resources:Resource, lbl2WeekView%>" Selected="True" Value="2week" />
                                <asp:ListItem Text="<%$Resources:Resource, lblMonth%>" Value="month" />
                            </asp:RadioButtonList>	                        
                        </div>
                        <div style="clear: both">
                        </div>
                    </div>                    
                    <div>
                        <iframe id="grid_wrapper" frameborder="0" marginheight="0" marginwidth="0" src=""  style="width: 600px; height:630px;"></iframe>
                        <asp:HiddenField ID="hdnToday" runat="server" Value="" />    
                        <asp:HiddenField ID="hdnCurrentView" runat="server" Value="2week" />
                        <asp:TextBox CssClass="hide" ID="txtDataToSubmit" runat="server" TextMode="MultiLine" />   
                        <asp:HiddenField ID="hdnScrollPos" runat="server" Value="0" />                                                
                    </div>                    
                </div>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnBuildingID" runat="server" />
    <asp:HiddenField ID="hdnRoomID" runat="server" />
    <asp:HiddenField ID="hdnBedID" runat="server" />
    <input id="soOnCal" type="button" value="Test" style="display:none;"/>
   
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" Runat="Server">
     <!--Bed Selection-->
    <script type="text/javascript">            
        arrBeds = new Dictionary();
        rowClicks = new Dictionary();
        rowClicks.Add("ROW_ID", "");
        rowClicks.Add("START_CELL_IDX", -1);
        rowClicks.Add("END_CELL_IDX", -1);

        function resetEventsHistory() {
            rowClicks["ROW_ID"] = -1;
            rowClicks["START_CELL_IDX"] = -1;
            rowClicks["END_CELL_IDX"] = -1;
        }

        function setScrollPosition(pos) {
            gridScrollPos = pos;
        }      
    </script>

    <!--Grid Script-->
    <script type="text/javascript">                    
        //Variables
        $jqGridFrame = $("#grid_wrapper");             
        var searchPanelID = "<%=pnlSearch.ClientID %>";

        //Function To Resize the grid
        function resize_the_grid() {
            //Set grid_wrapper width according to view port
            $jqGridFrame.width($(window).width() - 260);          
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);        

        function gridLoadComplete(data) {
            //$('#' + gridID).remapColumns();
            resize_the_grid();
        }

        $("#btnSearch").click(function () {
            var filterData = {};
            //Reset previous selection
            $("#<%=hdnBuildingID.ClientID%>").val("0");
            $("#<%=hdnRoomID.ClientID%>").val("0");
            $("#<%=hdnBedID.ClientID%>").val("0");
            $(".val_filter").each(function () {
                //alert($(this).attr("search-key"));
                filterData[$(this).attr("search-key")] = $(this).val();
            });
            filterData["showAvailable"] = 0;            
            $jqGridFrame[0].contentWindow.setFilterOptions(filterData);
        });

        $("#btnAvailable").click(function () {
            var filterData = {};
            //Reset previous selection
            $("#<%=hdnBuildingID.ClientID%>").val("0");
            $("#<%=hdnRoomID.ClientID%>").val("0");
            $("#<%=hdnBedID.ClientID%>").val("0");
            $(".val_filter").each(function () {
                filterData[$(this).attr("search-key")] = $(this).val();
            });
            filterData["showAvailable"] = 1;
            $jqGridFrame[0].contentWindow.setFilterOptions(filterData);
        });        
    </script>    
    <script type="text/javascript">
        $txtDataToSubmit = $("#<%=txtDataToSubmit.ClientID%>");       
        $txtAmenities = $("#<%=txtAmenities.ClientID%>");
        $ddl_buldings = $("#<%=ddlBuildings.ClientID%>");
        $ddl_rooms = $("#<%=ddlRooms.ClientID%>");
        $ddl_beds = $("#<%=ddlBeds.ClientID%>");
        $hdnToday = $("#<%=hdnToday.ClientID%>");
        $hdnView = $("#<%=hdnCurrentView.ClientID%>");
        $btnMove = $("#<%=btnMove.ClientID%>");
        $btnCancelMove = $("#<%=btnCancelMove.ClientID%>");      
        $btnReserve = $("#<%=btnReserve.ClientID%>");
        $btnClearAll = $("#<%=btnClearAll.ClientID%>");
        $btnPartialCancel = $("#<%=btnPartialCancel.ClientID%>");
        $btnClearAll2 = $("#<%=btnClearAll2.ClientID%>");

        /*$date_picker = $(".cls_datepicker").datepicker($.extend(datePickerDefaults).extend({
            onSelect: function (dateText, inst) {
                reloadCalendar();                
            }
        }));*/
        $date_picker = $(".cls_datepicker").datepicker($.extend($.datepicker.regional[_jqdplang]).extend({
            onSelect: function (dateText, inst) {
                reloadCalendar();
            }
        }));   
            

        //Set Hidden Search filters values
        $(".filter_amenity").click(function () {
            var arrAmenity = [];
            $(".filter_amenity").each(function () {
                if (this.checked) {
                    arrAmenity.push($(this).val());
                }
            });
            $txtAmenities.val(arrAmenity.join(','));
            $("#btnSearch").trigger("click");
        });       

        $("#<%=repeat.ClientID%> :radio").click(function () {
            if ($hdnView.val() != $(this).val()) {
                $hdnView.val($(this).val());
                reloadCalendar();
            }            
        });

        function reloadCalendar() {
            resetEventsHistory();
            var prm = {};
            $("#<%=repeat.ClientID%> :radio").each(function () {
                if ($(this).is(':checked')) {
                    prm.view = $(this).val();
                }                
            });                                                         
            var dateObj = $date_picker.datepicker('getDate');
            prm.selectedDate = moment(dateObj).format("MM/DD/YYYY");
            prm['in_room'] = $("#<%=hdnRoomID.ClientID%>").val();            
            $(".val_filter").each(function () {
                if ($(this).val() != "") {
                    prm['in_' + $(this).attr("search-key")] = $(this).val();
                }                
            });
            $jqGridFrame.attr("src", "GridResponce.aspx?" + jQuery.param(prm));
            if (typeof $jqGridFrame[0].contentWindow.setScrollPosition == "function") {
                $jqGridFrame[0].contentWindow.setScrollPosition();
            }            
        }

        //move next or previous
        function shiftDates(isIntervalShift, shiftDirection) {
            var currentview = $hdnView.val();
            if (shiftDirection == 0) {
                $date_picker.datepicker("setDate", new Date());
            }
            else if (isIntervalShift) {
                var dateObj = $date_picker.datepicker('getDate');
                var dateToSet;
                if (currentview == 'day') {
                    if (shiftDirection == 1) {
                        dateToSet = moment(dateObj).add('days', 1);
                    }
                    else {
                        dateToSet = moment(dateObj).subtract('days', 1);
                    }                    
                }
                else if (currentview == 'month') {
                    if (shiftDirection == 1) {
                        dateToSet = moment(dateObj).add('months', 1);
                    }
                    else {
                        dateToSet = moment(dateObj).subtract('months', 1);
                    }                   
                }
                else if (currentview == '2week') {
                    if (shiftDirection == 1) {
                        dateToSet = moment(dateObj).add('days', 14);
                    }
                    else {
                        dateToSet = moment(dateObj).subtract('days', 14);
                    }                    
                }
                else {//Default rweek
                    if (shiftDirection == 1) {
                        dateToSet = moment(dateObj).add('days', 7);
                    }
                    else {
                        dateToSet = moment(dateObj).subtract('days', 7);
                    }
                }
                $date_picker.datepicker("setDate", new Date(dateToSet));                
            }
            else {
                if (shiftDirection == -1) {
                    var dateObj = $date_picker.datepicker('getDate');
                    var dateToSet = moment(dateObj).subtract('days', 1);
                    $date_picker.datepicker("setDate", new Date(dateToSet));                    
                }
                else {
                    var dateObj = $date_picker.datepicker('getDate');
                    var dateToSet = moment(dateObj).add('days', 1);
                    $date_picker.datepicker("setDate", new Date(dateToSet));                    
                }
            }
            reloadCalendar();
        }

        function setDateRangeDispalay(display) {
            $("#dislayDateRange").html(display);
        }

        function setDataToSubmit() {
            $txtDataToSubmit.val(JSON.stringify(arrBeds));
        }         
    </script>

    <!--Ajax Calls-->
    <script type="text/javascript">
        $ddl_buldings.change(function () {
            $("#btnSearch").trigger("click");
            var dataToPost = {};
            dataToPost.buildingid = $(this).val();
            $("#<%=hdnBuildingID.ClientID%>").val($(this).val());
            $("#<%=hdnRoomID.ClientID%>").val("0");
            $("#<%=hdnBedID.ClientID%>").val("0");
            $.ajax({
                type: "POST",
                url: "ajax.aspx/GetRooms",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(dataToPost), //"{'buildingid' : '" + $(this).val() + "'}",
                success: function (data) {
                    $('option', $ddl_rooms).remove();
                    $('option', $ddl_beds).remove();
                    $ddl_rooms.append($("<option />").val("").text(""));
                    $ddl_beds.append($("<option />").val("").text(""));
                    $.each(data.d, function () {
                        $ddl_rooms.append($("<option />").val(this.id).text(this.Name));
                    });                    
                    $ddl_rooms.trigger("liszt:updated");
                    $ddl_beds.trigger("liszt:updated");
                },
                error: function (request, status, errorThrown) {
                    alert(status);
                }
            });
        });

        $ddl_rooms.change(function () {
            $("#btnSearch").trigger("click");
            var dataToPost = {};
            dataToPost.roomid = $(this).val();
            $("#<%=hdnRoomID.ClientID%>").val($(this).val());
            $("#<%=hdnBedID.ClientID%>").val("0");
            $.ajax({
                type: "POST",
                url: "ajax.aspx/GetBeds",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(dataToPost), //"{'roomid' : '" + $(this).val() + "'}",
                success: function (data) {
                    $('option', $ddl_beds).remove();
                    $ddl_beds.append($("<option />").val("").text(""));
                    $.each(data.d, function () {
                        $ddl_beds.append($("<option />").val(this.id).text(this.Name));
                    });
                    $ddl_beds.trigger("liszt:updated");
                },
                error: function (request, status, errorThrown) {
                    alert(status);
                }
            });
        });

        $ddl_beds.change(function () {
            $("#btnSearch").trigger("click");
            $("#<%=hdnBedID.ClientID%>").val($(this).val());
        });

        $btnMove.click(function () {
            $("#spMoving").toggle();
            $("#spReserve").toggle();
            clearAllSelection();
            resetMovingData();
            isReservationDateMoving = true;
            setTitle();
            return false;
        });

        function moveReservation() {
            if (arrBeds.Keys.length <= 0) {
                alert("<%=Resources.Resource.msgPleaseSelectStartDateToMoveReservation%>");
                return false;
            }           
            var postData = {};
            postData.reservationItemID = reservationItemID;
            postData.bedIDToMoveOn = bedIDToMoveOn;
            postData.startDateToMoveOn = startDateToMoveOn;
            blockContentArea($('body'), "Moving...");
            $.ajax({
                type: "POST",
                url: "ajax.aspx/MoveReservation",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(postData),
                success: function (data) {
                    unblockContentArea($('body'));
                    if (data.d == "SUCCESS") {                        
                        alert("<%=Resources.Resource.msgReservationMovedSuccessfully%>");
                    }
                    else if (data.d == "INVALID") {
                        alert("<%=Resources.Resource.msgPleaseSelectValidStartDatetoMove%>");
                    }
                    else if (data.d == "NA") {
                        alert("<%=Resources.Resource.msgAccommodationNotAvailable%>");
                    }
                    else {
                        alert('Error!');
                    }                    
                    
                    reloadCalendar();                    
                },
                error: function (request, status, errorThrown) {
                    alert(status);                   
                }
            });
        }

        $btnCancelMove.click(function () {
            $("#spMoving").toggle();
            $("#spReserve").toggle();
            isReservationDateMoving = false;
            clearAllSelection();
            resetMovingData();
            setTitle();
            return false;
        });

        $(document).ready(function () {
            if (isReservationDateMoving) {
                $btnMove.trigger("click");
            }
        });
    </script>
    
    <!--Too Bar Heler Scripts-->
    <script type="text/javascript">
        $(function () {
            $("#beginning").button({
                text: false,
                icons: {
                    primary: "ui-icon-seek-start"
                }
            });
            $("#rewind").button({
                text: false,
                icons: {
                    primary: "ui-icon-seek-prev"
                }
            });
            $("#forward").button({
                text: false,
                icons: {
                    primary: "ui-icon-seek-next"
                }
            });
            $("#end").button({
                text: false,
                icons: {
                    primary: "ui-icon-seek-end"
                }
            });
            
            $("#<%=repeat.ClientID%>").buttonset();
        });

        $btnClearAll.click(function () {
            clearAllSelection();
            resetMovingData();
            setTitle();
            return false;
        });


        $btnClearAll2.click(function () {
            clearAllSelection();
            resetMovingData();
            setTitle();
            return false;
        });

        function clearAllSelection() {
            if (typeof $jqGridFrame[0].contentWindow.resetSelectionInGrid == "function") {
                $jqGridFrame[0].contentWindow.resetSelectionInGrid();
            }
            
            resetEventsHistory();
            for (var i = 0; i < arrBeds.Keys.length; i++) {
                arrBeds[arrBeds.Keys[i]] = [];
            }
            $txtDataToSubmit.val("");
        }
        

        //dialog to process reservation
        $btnReserve.live("click", function (e) {
            e.preventDefault();
            $this = $(this);
            if ($txtDataToSubmit.val() == "") {
                alert("<%=Resources.Resource.msgPleaseSelectValidDateRange%>");
            }
            else {
                $dialog = jQuery.FrameDialog.create({ url: 'OrderInfo.aspx?data=' + $txtDataToSubmit.val(),
                    title: "<%=Resources.Resource.lblBookingInformation%>",
                    loadingClass: "loading-image",
                    modal: true,
                    width: 950,
                    height: 510,
                    autoOpen: false,
                    close: function (ev, ui) {
                        $(this).dialog('destroy');
                    }
                });
                $dialog.dialog('open'); return false;
            }
        });        
    </script>
    <script type="text/javascript">
        $(document).ready(function () {            
            reloadCalendar();
        });

        function cancelPartialReservation() {
            if (partialMovingData.reservationItemID <= 0 || partialMovingData.bedID <= 0 || partialMovingData.fDate <= 0
                || partialMovingData.tDate <= 0) {
                alert("<%=Resources.Resource.msgPleaseSelectValidDateRange%>");
            }
            else {
                var dataToPost = {};
                dataToPost.rid = partialMovingData.reservationItemID;
                dataToPost.old_bed_id = partialMovingData.bedID;
                dataToPost.new_bed_id = partialMovingData.targetBedID;
                dataToPost.cancel_from = partialMovingData.fDate;
                dataToPost.cancel_to = partialMovingData.tDate;
                dataToPost.checkin = partialMovingData.targetFDate;
                dataToPost.checkout = partialMovingData.targetTDate;
                dataToPost.action = "cancel";

                $dialog = jQuery.FrameDialog.create({ url: 'mdPartialReservationMove.aspx?' + $.param(dataToPost),
                    title: "<%=Resources.Resource.lblMovingReservation%>",
                    loadingClass: "loading-image",
                    modal: true,
                    width: 750,
                    height: 510,
                    autoOpen: false,
                    close: function (ev, ui) {
                        $(this).dialog('destroy');
                        if (_isRequireReload) {
                            reloadGridOnScrollPos();
                            _isRequireReload = false;
                        }
                    }
                });
                $dialog.dialog('open'); return false;
            }
        }

        function movePartialReservation() {
            if (partialMovingData.reservationItemID <= 0 || partialMovingData.bedID <= 0 || partialMovingData.fDate <= 0
                || partialMovingData.targetBedID <= 0 || partialMovingData.targetFDate <= 0 ||
                partialMovingData.targetTDate <= 0 || partialMovingData.tDate <=0) {
                alert("<%=Resources.Resource.msgPleaseSelectValidDateRange%>");
            }
            else {
                var dataToPost = {};
                dataToPost.rid = partialMovingData.reservationItemID;
                dataToPost.old_bed_id = partialMovingData.bedID;
                dataToPost.new_bed_id = partialMovingData.targetBedID;
                dataToPost.moving_from = partialMovingData.fDate;
                dataToPost.moving_to = partialMovingData.tDate;
                dataToPost.checkin = partialMovingData.targetFDate;
                dataToPost.checkout = partialMovingData.targetTDate;
                dataToPost.action = "move";

                $dialog = jQuery.FrameDialog.create({ url: 'mdPartialReservationMove.aspx?' + $.param(dataToPost),
                    title: "<%=Resources.Resource.lblMovingReservation%>",
                    loadingClass: "loading-image",
                    modal: true,
                    width: 750,
                    height: 510,
                    autoOpen: false,
                    close: function (ev, ui) {
                        $(this).dialog('destroy');
                        if (_isRequireReload) {
                            reloadGridOnScrollPos();
                            _isRequireReload = false;
                        }
                    }
                });
                $dialog.dialog('open'); return false;
            }
        }

        function reloadGridOnScrollPos() {
            clearAllSelection();
            resetMovingData(); 
            $jqGridFrame[0].contentWindow.reloadGridOnScrollPos();
            return false;
        }

        $(".search_guest_staff").addPlaceholder();
    </script>
     <script type="text/javascript">
         $("#soOnCal").click(function () {
             $jqGridFrame[0].contentWindow.test();
         });
    </script>
</asp:Content>


﻿<%@ WebHandler Language="C#" Class="datePickerLocals" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Text;
using System.IO;
using System.Web.UI;
using System.Globalization;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public class datePickerLocals : IHttpHandler, IRequiresSessionState
{
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/javascript";
        string local = Globals.CurrentAppLanguageCode;
        StringBuilder scriptVariable = new StringBuilder();
        scriptVariable.Append("var datePickerDefaults = {");
        switch (local)
        {
            case AppLanguageCode.EN:
                break;
            case AppLanguageCode.FR:
                scriptVariable.Append("closeText: 'Fermer',")
                    .Append("prevText: '&#x3c;Préc',")
                    .Append("nextText: 'Suiv&#x3e;',")
                    .Append("currentText: 'Courant',")
                    .Append("monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],")
                    .Append("monthNamesShort: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aoû', 'Sep', 'Oct', 'Nov', 'Déc'],")
                    .Append("dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],")
                    .Append("dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],")
                    .Append(" dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa']");                
                break;
        }
        scriptVariable.Append("};");
        context.Response.Write(scriptVariable);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}
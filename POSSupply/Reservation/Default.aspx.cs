﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

using Newtonsoft.Json;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;
using System.Text;

public partial class Reservation_Default : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Redirect("ReservationCalendar.aspx?" + Request.QueryString); //New Reservation Calendar logic page
    } 
}
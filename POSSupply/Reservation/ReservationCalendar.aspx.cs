﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using Trirand.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;

using iTECH.WebControls;

using Newtonsoft.Json;

public partial class Reservation_ReservationCalendar : BasePage
{
    SearchFilter _searchFilter = new SearchFilter();
    ReservationItems _rItems = new ReservationItems();

    protected void Page_Load(object sender, EventArgs e)
    {       
        //If not postback
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                _searchFilter.Clear();

                //txtSelectedDate.Text = BusinessUtility.GetDateTimeString(DateTime.Today, DateFormat.MMddyyyy);

                if (this.OrderID > 0 && this.ReservationID > 0 && this.ReservationItemID > 0 && this.BedID > 0)
                {
                    //btnReserve.Visible = false;
                    //btnMove.Visible = true;
                    //_searchFilter["bed"] = this.BedID.ToString();

                    _rItems.PopulateObject(dbHelp, this.ReservationItemID);
                    string jsDateObj = string.Format("new Date({0}, {1}, {2})", _rItems.CheckInDate.Year, _rItems.CheckInDate.Month - 1, _rItems.CheckInDate.Day);
                    //Globals.RegisterScript(this, string.Format(@"$date_picker.datepicker(""setDate"", {0});", jsDateObj));
                    string script = string.Empty; //string.Format("locationHrefOnSuccess = '{0}';", ResolveUrl(string.Format("~/Sales/SalesEdit.aspx?SOID={0}", this.OrderID)));
                    script += string.Format("isReservationDateMoving = true;");
                    //script += string.Format("reservationItemID = {0};" , this.ReservationItemID);
                    script += string.Format(@"$date_picker.datepicker(""setDate"", {0});", jsDateObj);
                    script += "reloadCalendar();";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "setdate", script, true);

                    hdnBedID.Value = this.BedID.ToString();
                    int roomID = 0;
                    Bed b = new Bed();
                    hdnBuildingID.Value = b.GetBuildingID(dbHelp, this.BedID, out roomID).ToString();
                    hdnRoomID.Value = roomID.ToString();
                }
                else if (this.OrderID > 0)
                {
                    Reservation rsv = new Reservation();
                    ReservationItems ri = rsv.GetFirstReservationItem(dbHelp, this.OrderID);
                    if (ri.ReservationItemID > 0)
                    {
                        hdnRoomID.Value = ri.BedID.ToString();
                        string jsDateObj = string.Format("new Date({0}, {1}, {2})", ri.CheckInDate.Year, ri.CheckInDate.Month - 1, ri.CheckInDate.Day);
                        string script = string.Format(@"$date_picker.datepicker(""setDate"", {0});", jsDateObj);
                        script += "reloadCalendar();";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "setdate", script, true);

                        int roomID = 0;
                        Bed b = new Bed();
                        hdnBuildingID.Value = b.GetBuildingID(dbHelp, ri.BedID, out roomID).ToString();
                        hdnRoomID.Value = roomID.ToString();
                    }
                }

                //Fill buildings              
                DropDownHelper.FillBuildings(ddlBuildings, new ListItem("", ""), Globals.CurrentAppLanguageCode);

                //Fill Amenity
                Attributes attr = new Attributes();
                sdsAttributes.SelectCommand = attr.GetSql(sdsAttributes.SelectParameters, string.Empty, Globals.CurrentAppLanguageCode);
            }
            catch
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    public int BedID {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["bedid"], out id);
            return id;  
        }
    }

    public int ReservationID {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["rid"], out id);
            return id;
        }
    }

    public int ReservationItemID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["ritmid"], out id);
            return id;
        }
    }

    public int OrderID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["soid"], out id);
            return id;
        }
    }

    protected void btnMove_Click(object sender, EventArgs e)
    {        
       
    }
    protected void btnGuestSearch_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("~/Partner/ViewCustomer.aspx?pType=E&gType=1&includeStaff=true&sk={0}", Server.UrlEncode(txtSearch.Text)));
    }
}
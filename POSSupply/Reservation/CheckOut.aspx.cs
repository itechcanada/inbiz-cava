﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Newtonsoft.Json;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

using iTECH.Library.DataAccess.MySql;

public partial class Reservation_CheckOut : BasePage
{
    Partners _primaryCust = new Partners();    

    protected void Page_Load(object sender, EventArgs e)
    {        
        if (ReservationCartHelper.CurrentCart.Items.Count <= 0) {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPleaseSelectBeds);
            Response.Redirect("Default.aspx");            
        }

        BindNavigation();        

        //CurrentCustomer Client Object
        ClientScript.RegisterClientScriptBlock(this.GetType(), "objClientGuest", string.Format("var _objClientGuest={0};", JsonConvert.SerializeObject(new Partners())), true);

        //CurrentCustomer Client Object
        ClientScript.RegisterClientScriptBlock(this.GetType(), "objClientGuestAddress", string.Format("var _objClientGuestAddress={0};", JsonConvert.SerializeObject(new Addresses())), true);

        if (!IsPostBack)
        {
            DropDownHelper.FillDropdown(rblMember, "CU", "dlMem", Globals.CurrentAppLanguageCode, null);

            if (this.ReservationCartItem != null)
            {
                ltAccommodationType.Text = Globals.GetRoomType(this.ReservationCartItem.RoomType);
                ltCheckIn.Text = BusinessUtility.GetDateTimeString(this.ReservationCartItem.CheckInDate, DateFormat.MMddyyyy);
                ltCheckOut.Text = BusinessUtility.GetDateTimeString(this.ReservationCartItem.CheckOutDate, DateFormat.MMddyyyy);
            }

            if (this.IsLastGuest)
            {
                btnNext.Visible = false;
                btnAddServices.Visible = true;
                btnReceivePayment.Visible = true;
            }                      
            else
            {
                btnNext.Visible = true;                
                btnAddServices.Visible = false;                
                btnReceivePayment.Visible = false;
                pnlCustomerInfo.DefaultButton = btnNext.ID;
            }

            if (this.GuestIndex >= 1)
            {
                btnReserveRest.Visible = true;
            }
            else
            {
                btnReserveRest.Visible = false;
            }
            btnReserve.Visible = !btnNext.Visible;
            ltSectionTitle.Text = string.Format("{0} - Guest {1}", Resources.Resource.lblPersonalInformation, this.GuestIndex + 1);
            chkCouple.Checked = this.IsCouple;
            chkCouple.Enabled = !this.IsCouple;
            txtFirstName.Focus();
        }  
    }    

    public void BindNavigation()
    {
        blstNav.Items.Clear();
        //QueryString.GetModifiedUrl("section", "0")

        string sql = string.Format("SELECT '{0}' AS Text, '{1}' AS Value ", "Personal Information", "javascript:void(0);");
        //sql += string.Format("UNION SELECT '{0}' AS Text, '{1}' AS Value ", "Accommodation Details", "javascript:void(0);");
        //sql += string.Format("UNION SELECT '{0}' AS Text, '{1}' AS Value ", "Payment Details", "javascript:void(0);");
        //sql += string.Format("UNION SELECT '{0}' AS Text, '{1}' AS Value ", "Release of reliability", "javascript:void(0);");


        sdsNavigation.SelectCommand = sql;
        blstNav.DataBind();
    }


    public void Initialize()
    {
        //DropDownHelper.FillDropdown(ddlRelationship1, "CU", "dlRel", Globals.CurrentAppLanguageCode, new ListItem("--Select--", "0"));
        //DropDownHelper.FillDropdown(ddlRelationship2, "CU", "dlRel", Globals.CurrentAppLanguageCode, new ListItem("--Select--", "0"));
        DropDownHelper.FillDropdown(rblMember, "CU", "dlMem", Globals.CurrentAppLanguageCode, null);

        if (this.PrimaryPartnerID > 0)
        {
            _primaryCust.PopulateObject(this.PrimaryPartnerID);
            Addresses billToAddress = _primaryCust.GetBillToAddress(_primaryCust.PartnerID, _primaryCust.PartnerType);

            txtAddressLine1.Text = billToAddress.AddressLine1;
            txtAddressLine2.Text = billToAddress.AddressLine2;
            txtCity.Text = billToAddress.AddressCity;
            txtState.Text = billToAddress.AddressState;
            txtCountry.Text = billToAddress.AddressCountry;
            txtPostalCode.Text = billToAddress.AddressPostalCode;


            liRelationToPrimaryGuest.Visible = true;
            liReservationNote.Visible = false;
        }
        else
        {
            liRelationToPrimaryGuest.Visible = false;
        }

        txtFirstName.Focus();
    }

    public int SearchedGuestID
    {
        get
        {
            int gid = 0;
            int.TryParse(hdnSelectedGuestID.Value, out gid);
            return gid;
        }
    }

    public int GuestIndex
    {
        get
        {
            int gi = 0;
            int.TryParse(Request.QueryString["gi"], out gi);
            return gi;
        }
    }

    public int PrimaryPartnerID
    {
        get
        {
            int pguest = 0;
            int.TryParse(Request.QueryString["pcust"], out pguest);
            return pguest;
        }
    }    

    public RsvCartItem ReservationCartItem
    {
        get
        {
            if (this.GuestIndex < ReservationCartHelper.CurrentCart.Items.Count)
            {
                return ReservationCartHelper.CurrentCart.Items[this.GuestIndex];
            }
            return null;
        }
    }

    public StatusGuestType ReserveFor
    {
        get
        {
            return QueryStringHelper.GetGuestType("rType");
        }
    }

    #region Old Logic
    private bool IsCouple
    {
        get {
            return Request.QueryString["iscouple"] == "1";
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            if (this.GuestIndex == 0)
            {
                ReservationCartHelper.CurrentCart.ReservationNotes = txtReservationNote.Text;
            }

            Partners cust = SavePartnerInfo(dbHelp);
            string isc = !this.IsCouple && chkCouple.Checked ? "1" : "0";

            if (cust.PartnerID > 0)
            {
                ReservationCartHelper.UpdateCartItem(this.ReservationCartItem.CartItemID, cust.PartnerID, cust.ExtendedProperties.Sex, txtRelationship.Text, chkCouple.Checked);
                int pCustID = this.GuestIndex == 0 ? cust.PartnerID : this.PrimaryPartnerID;
                Response.Redirect(string.Format("~/Reservation/CheckOut.aspx?rtype={0}&section=0&pcust={1}&gi={2}&iscouple={3}", (int)this.ReserveFor, pCustID, this.GuestIndex + 1, isc), false);
            }
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.DUPLICATE_KEY)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgEmailIDAlreadyExists);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }
    

    private int PlaceOrder(DbHelper dbHelp)
    {        
        int soid = 0;
        try
        {
            if (this.GuestIndex == 0)
            {
                ReservationCartHelper.CurrentCart.ReservationNotes = txtReservationNote.Text;
            }

            //Check beds avaialability for the last time before creating reservation
            if (ProcessReservation.IsAccommodationAvailalbe(dbHelp, ReservationCartHelper.CurrentCart.Items, false) == false)
            {
                return 0;
            }

            Partners primaryPartner = new Partners();
            primaryPartner.PopulateObject(dbHelp, ReservationCartHelper.CurrentCart.Items[0].GuestID); //Make first guest as Primary Partner

            //Make Reservation 
            Reservation rsv = new Reservation();
            rsv.PrimaryPartnerID = primaryPartner.PartnerID;
            rsv.ReservationStatus = (int)StatusReservation.New;
            rsv.ReserveFor = (int)this.ReserveFor;
            rsv.ReservationNote = ReservationCartHelper.CurrentCart.ReservationNotes;
            rsv.Insert(dbHelp, CurrentUser.UserID);

            //Insert Reservation Items
            ReservationItems ri = new ReservationItems();
            ri.AddList(dbHelp, ReservationCartHelper.CurrentCart.Items, rsv.ReservationID);

            Orders ord = new Orders();
            SysCompanyInfo cinf = new SysCompanyInfo();
            cinf.PopulateObject(CurrentUser.DefaultCompanyID, dbHelp);

            ord.OrdCompanyID = CurrentUser.DefaultCompanyID;
            ord.OrdCreatedBy = CurrentUser.UserID;
            ord.OrdCreatedFromIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
            ord.OrdCurrencyCode = primaryPartner.PartnerCurrencyCode;
            ord.OrdCurrencyExRate = SysCurrencies.GetRelativePrice(ord.OrdCurrencyCode);
            ord.OrdCustID = primaryPartner.PartnerID;
            ord.OrdCustType = Globals.GetPartnerType(primaryPartner.PartnerType);
            ord.OrderTypeCommission = (int)OrderCommission.Reservation;
            ord.OrdLastUpdateBy = CurrentUser.UserID;
            ord.OrdSalesRepID = CurrentUser.UserID;
            ord.OrdSaleWeb = false;
            ord.OrdShippingTerms = cinf.CompanyShpToTerms;
            ord.OrdShpBlankPref = false;
            ord.OrdShpCode = string.Empty;
            ord.OrdShpCost = 0;
            ord.OrdShpDate = DateTime.Now;
            ord.OrdShpTrackNo = string.Empty;
            ord.OrdShpWhsCode = CurrentUser.UserDefaultWarehouse;
            ord.OrdStatus = SOStatus.IN_PROGRESS;
            ord.OrdType = StatusSalesOrderType.QUOTATION;
            ord.OrdVerified = true;
            ord.OrdVerifiedBy = CurrentUser.UserID;
            ord.QutExpDate = DateTime.Now;
            ord.OrdNetTerms = string.Empty;
            ord.OrderRejectReason = string.Empty;
            ord.OrdComment = rsv.ReservationNote;
            ord.Insert(dbHelp, CurrentUser.UserID); //Insert Order

            soid = ord.OrdID;
            if (soid > 0)
            {
                OrderItems processItems = new OrderItems();
                processItems.AddReservationItems(dbHelp, soid, rsv.ReservationID);

                rsv.UpdateReservationStatus(dbHelp, rsv.ReservationID, soid, StatusReservation.Processed);

                //To do add into log
                new OrderActionLog().AddLog(dbHelp, CurrentUser.UserID, soid, "Reservation created", ord.OrdComment);
            }

            return soid;
        }
        catch
        {
            throw;
        }       
    }

    private Partners SavePartnerInfo(DbHelper dbHelp)
    {        
        Partners cust = new Partners();
        try
        {
            //Save Customer Information in CRM

            Addresses billToAddress = new Addresses();
            Addresses shipToAddress = new Addresses();
            List<Addresses> lstAddress = new List<Addresses>();
            if (this.SearchedGuestID > 0)
            {
                cust.PopulateObject(dbHelp, this.SearchedGuestID);
                billToAddress = cust.GetBillToAddress(dbHelp, cust.PartnerID, cust.PartnerType);
                shipToAddress = cust.GetShipToAddress(dbHelp, cust.PartnerID, cust.PartnerType);

                cust.PartnerEmail = txtEmailAddress.Text;
                cust.PartnerLongName = string.Format("{0} {1}", txtLastName.Text, txtFirstName.Text);
                cust.PartnerNote = txtCustomerNote.Text;
                cust.PartnerPhone = txtTelephone.Text;

                billToAddress.AddressCity = txtCity.Text;
                billToAddress.AddressCountry = txtCountry.Text;
                billToAddress.AddressLine1 = txtAddressLine1.Text;
                billToAddress.AddressLine2 = txtAddressLine2.Text;
                billToAddress.AddressPostalCode = txtPostalCode.Text;
                billToAddress.AddressState = txtState.Text;
                billToAddress.AddressRef = AddressReference.END_CLIENT;
                billToAddress.AddressType = AddressType.BILL_TO_ADDRESS;
                lstAddress.Add(billToAddress);

                shipToAddress.AddressCity = txtCity.Text;
                shipToAddress.AddressCountry = txtCountry.Text;
                shipToAddress.AddressLine1 = txtAddressLine1.Text;
                shipToAddress.AddressLine2 = txtAddressLine2.Text;
                shipToAddress.AddressPostalCode = txtPostalCode.Text;
                shipToAddress.AddressState = txtState.Text;
                shipToAddress.AddressRef = AddressReference.END_CLIENT;
                shipToAddress.AddressType = AddressType.SHIP_TO_ADDRESS;
                lstAddress.Add(shipToAddress);


                cust.ExtendedProperties.Age = 0;
                cust.ExtendedProperties.DOB = BusinessUtility.GetDateTime(txtDOB.Text, DateFormat.MMddyyyy);
                cust.ExtendedProperties.EmergencyAltTelephone = txtAlterTelephone.Text;
                cust.ExtendedProperties.EmergencyContactName = txtEmName.Text;
                cust.ExtendedProperties.EmergencyRelationship = txtEmRelationship.Text;
                cust.ExtendedProperties.EmergencyTelephone = txtEmTelephone.Text;
                cust.ExtendedProperties.FirstName = txtFirstName.Text;
                cust.ExtendedProperties.IsMember = rblMember.SelectedValue;
                cust.ExtendedProperties.LastName = txtLastName.Text;
                cust.ExtendedProperties.MembershipID = txtMembership.Text;
                cust.ExtendedProperties.ReceiveNewsLetters = chkNewsLetter.Checked;
                cust.ExtendedProperties.GuestType = (int)this.ReserveFor;
                cust.ExtendedProperties.Sex = rblSex.SelectedValue;
                cust.ExtendedProperties.SpirtualName = txtSpritualName.Text;

                string dulicateEmailAddress = System.Configuration.ConfigurationManager.AppSettings["DuplicatEmailAddress"];
                cust.PartnerEmail = cust.PartnerEmail.ToLower().Trim() == dulicateEmailAddress.ToLower().Trim() ? string.Format("{0:yyyyMMddhhmmss}_{1}", DateTime.Now, dulicateEmailAddress) : cust.PartnerEmail;
                cust.Update(dbHelp, CurrentUser.UserID, lstAddress);
            }
            else
            {
                string currencyCode = string.Empty;
                int taxCode = 0;
                if (this.PrimaryPartnerID > 0)
                {
                    _primaryCust.PopulateObject(dbHelp, this.PrimaryPartnerID);
                    currencyCode = _primaryCust.PartnerCurrencyCode;
                    taxCode = _primaryCust.PartnerTaxCode;
                }
                else
                {
                    SysCompanyInfo ci = new SysCompanyInfo();
                    ci.PopulateObject(CurrentUser.DefaultCompanyID, dbHelp);
                    currencyCode = ci.CompanyBasCur;

                    SysWarehouses whs = new SysWarehouses();
                    whs.PopulateObject(CurrentUser.UserDefaultWarehouse, dbHelp);
                    taxCode = whs.WarehouseRegTaxCode;
                }

                cust.PartnerActive = true;
                cust.PartnerEmail = txtEmailAddress.Text;
                cust.PartnerLongName = string.Format("{0} {1}", txtLastName.Text, txtFirstName.Text);
                cust.PartnerNote = txtCustomerNote.Text;
                cust.PartnerPhone = txtTelephone.Text;
                cust.PartnerStatus = StatusPartnerStatus.ACTIVE;
                cust.PartnerTaxCode = taxCode;
                cust.PartnerType = (int)PartnerTypeIDs.EndClient;
                cust.PartnerCurrencyCode = currencyCode;


                billToAddress.AddressCity = txtCity.Text;
                billToAddress.AddressCountry = txtCountry.Text;
                billToAddress.AddressLine1 = txtAddressLine1.Text;
                billToAddress.AddressLine2 = txtAddressLine2.Text;
                billToAddress.AddressPostalCode = txtPostalCode.Text;
                billToAddress.AddressState = txtState.Text;
                billToAddress.AddressRef = AddressReference.END_CLIENT;
                billToAddress.AddressType = AddressType.BILL_TO_ADDRESS;
                lstAddress.Add(billToAddress);

                shipToAddress.AddressCity = txtCity.Text;
                shipToAddress.AddressCountry = txtCountry.Text;
                shipToAddress.AddressLine1 = txtAddressLine1.Text;
                shipToAddress.AddressLine2 = txtAddressLine2.Text;
                shipToAddress.AddressPostalCode = txtPostalCode.Text;
                shipToAddress.AddressState = txtState.Text;
                shipToAddress.AddressRef = AddressReference.END_CLIENT;
                shipToAddress.AddressType = AddressType.SHIP_TO_ADDRESS;
                lstAddress.Add(shipToAddress);

                cust.ExtendedProperties.Age = 0;
                cust.ExtendedProperties.DOB = BusinessUtility.GetDateTime(txtDOB.Text, DateFormat.MMddyyyy);
                cust.ExtendedProperties.EmergencyAltTelephone = txtAlterTelephone.Text;
                cust.ExtendedProperties.EmergencyContactName = txtEmName.Text;
                cust.ExtendedProperties.EmergencyRelationship = txtEmRelationship.Text;
                cust.ExtendedProperties.EmergencyTelephone = txtEmTelephone.Text;
                cust.ExtendedProperties.FirstName = txtFirstName.Text;
                cust.ExtendedProperties.IsMember = rblMember.SelectedValue;
                cust.ExtendedProperties.LastName = txtLastName.Text;
                cust.ExtendedProperties.MembershipID = txtMembership.Text;
                cust.ExtendedProperties.ReceiveNewsLetters = chkNewsLetter.Checked;
                cust.ExtendedProperties.GuestType = (int)this.ReserveFor;
                cust.ExtendedProperties.Sex = rblSex.SelectedValue;
                cust.ExtendedProperties.SpirtualName = txtSpritualName.Text;

                string dulicateEmailAddress = System.Configuration.ConfigurationManager.AppSettings["DuplicatEmailAddress"];
                cust.PartnerEmail = cust.PartnerEmail.ToLower().Trim() == dulicateEmailAddress.ToLower().Trim() ? string.Format("{0:yyyyMMddhhmmss}_{1}", DateTime.Now, dulicateEmailAddress) : cust.PartnerEmail;
                cust.Insert(dbHelp, CurrentUser.UserID, lstAddress);
            }

            return cust;
        }
        catch
        {
            throw;
        }        
    }

    private bool IsLastGuest
    {
        get {
            return this.GuestIndex + 1 == ReservationCartHelper.CurrentCart.Items.Count;
        }
    }
    #endregion

    protected void btnSearchCustomer_Click(object sender, EventArgs e)
    {

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Reservation/default.aspx");
    }
    protected void btnAddServices_Click(object sender, EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            if (this.IsLastGuest)
            {
                Partners cust = SavePartnerInfo(dbHelp);
                if (cust.PartnerID > 0)
                {
                    ReservationCartHelper.UpdateCartItem(this.ReservationCartItem.CartItemID, cust.PartnerID, cust.ExtendedProperties.Sex, txtRelationship.Text, chkCouple.Checked);

                    int soid = PlaceOrder(dbHelp);
                    if (soid > 0)
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgReservationSuccessfull);
                        Response.Redirect(string.Format("~/Sales/SalesEdit.aspx?SOID={0}", soid), false);
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgAccommodationNoLongerAvailable);
                        Response.Redirect("~/Reservation/Default.aspx", false);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.DUPLICATE_KEY)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgEmailIDAlreadyExists);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    protected void btnReceivePayment_Click(object sender, EventArgs e)
    {
        //Do similar to Add Services Command
        btnAddServices_Click(sender, e);
    }

    protected void btnReserveRest_Click(object sender, EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            int gstIdx = this.GuestIndex;
            int totalGuestToAdd = ReservationCartHelper.CurrentCart.Items.Count - this.GuestIndex;

            //Check if Primary guest availalbe from querystnng id or ( Guest information has been saved for Reservation)
            if (this.PrimaryPartnerID > 0 && this.ReservationCartItem != null)
            {
                //Get Primary Partner 
                Partners primaryPart = new Partners();
                primaryPart.PopulateObject(dbHelp, this.PrimaryPartnerID);

                foreach (var item in ReservationCartHelper.CurrentCart.Items)
                {
                    if (item.GuestID <= 0)
                    {
                        ReservationCartHelper.UpdateCartItem(item.CartItemID, primaryPart.PartnerID, primaryPart.ExtendedProperties.Sex, string.Empty, chkCouple.Checked);
                    }
                }

                int soid = PlaceOrder(dbHelp);
                if (soid > 0)
                {
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgReservationSuccessfull);
                    Response.Redirect("~/Reservation/Default.aspx?soid=" + soid, false);
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgAccommodationNoLongerAvailable);
                    Response.Redirect("~/Reservation/Default.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    protected void btnReserve_Click(object sender, EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {          
            if (this.IsLastGuest)
            {
                Partners cust = SavePartnerInfo(dbHelp);
                if (cust.PartnerID > 0)
                {
                    ReservationCartHelper.UpdateCartItem(this.ReservationCartItem.CartItemID, cust.PartnerID, cust.ExtendedProperties.Sex, txtRelationship.Text, chkCouple.Checked);

                    int soid = PlaceOrder(dbHelp);
                    if (soid > 0)
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgReservationSuccessfull);
                        Response.Redirect("~/Reservation/Default.aspx?soid=" + soid, false);
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgAccommodationNoLongerAvailable);
                        Response.Redirect("~/Reservation/Default.aspx", false);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.DUPLICATE_KEY)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgEmailIDAlreadyExists);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }
}
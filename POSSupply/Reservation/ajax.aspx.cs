﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

using Newtonsoft.Json;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Reservation_ajax : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static int[] SearchGuests(string fName, string lName, string email, string phone, string rid)
    {
        try
        {
            return ProcessReservation.SearchedGuestIDs(fName, lName, email, phone, BusinessUtility.GetInt(rid));
        }
        catch 
        {
            return new List<int>().ToArray();
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static object[] GetGuest(string guestid)
    {
        object[] arrData = new object[2];
        Partners cu = new Partners();
        try
        {            
            cu.PopulateObject(BusinessUtility.GetInt(guestid));
            arrData[0] = cu;
            arrData[1] = cu.GetBillToAddress(cu.PartnerID, cu.PartnerType);
        }
        catch
        {
            arrData[0] = cu;
            arrData[1] = new Addresses();
        }
        return arrData;
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static List<OptionList> GetRooms(string buildingid)
    {
        List<OptionList> lResult = new List<OptionList>();
        try
        {
            lResult = new Rooms().GetRooms(BusinessUtility.GetInt(buildingid), Globals.CurrentAppLanguageCode);
            OptionList rItem = new OptionList();
            //rItem.id = "";
            //rItem.Name = "";
            //lResult.Insert(0, rItem);
            return lResult;
        }
        catch
        {
            return lResult;
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    public static List<OptionList> GetBeds(string roomid)
    {
        List<OptionList> lResult = new List<OptionList>();
        try
        {
            lResult = new Bed().GetBeds(BusinessUtility.GetInt(roomid), Globals.CurrentAppLanguageCode);
            //OptionList rItem = new OptionList();
            //rItem.id = "";
            //rItem.Name = "";
            //lResult.Insert(0, rItem);
            return lResult;
        }
        catch
        {
            return lResult;
        }
    }

    //[System.Web.Services.WebMethod]
    //[System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)]
    //public static string MoveReservation(int reservationItemID, int bedIDToMoveOn, string startDateToMoveOn)
    //{
    //    DbHelper dbHelp = new DbHelper(true);
    //    try
    //    {
    //        DateTime dtStartDate = BusinessUtility.GetDateTime(startDateToMoveOn, "yyyyMMdd");

    //        Reservation rsv = new Reservation();
    //        ReservationItems rItems = new ReservationItems();
    //        rItems.PopulateObject(dbHelp, reservationItemID);
    //        rsv.PopulateObject(dbHelp, rItems.ReservationID);
    //        int totalDaysToAdd = rItems.TotalNights;
    //        bool applyPricing = true;
    //        if (rsv.ReserveFor == (int)StatusGuestType.CourseParticipants || rsv.ReserveFor == (int)StatusGuestType.SpecialGuest || rsv.ReserveFor == (int)StatusGuestType.Staff)
    //        {
    //            applyPricing = false;
    //        }

    //        if (dtStartDate != DateTime.MinValue && bedIDToMoveOn > 0)
    //        {
    //            DateTime dtEndDate = dtStartDate.AddDays(totalDaysToAdd);  //Moving complete dte range                         

    //            if (ProcessReservation.IsAccommodationAvailalbe(dbHelp, dtStartDate, dtEndDate, bedIDToMoveOn, reservationItemID, false))
    //            {
    //                Bed bd = new Bed();
    //                bd.BedID = bedIDToMoveOn;
    //                bd.FromDate = dtStartDate;
    //                bd.ToDate = dtEndDate;
    //                bd.PopulateBedInfo(dbHelp, bd, Globals.CurrentAppLanguageCode);
    //                if (applyPricing)
    //                {
    //                    ProductDiscount pDis = new ProductDiscount();
    //                    bd.BedPrice = pDis.CalculateAccommodationPrice(dbHelp, bedIDToMoveOn, dtStartDate, dtEndDate);
    //                }
    //                else
    //                {
    //                    bd.BedPrice = 0.0D;
    //                }

    //                rItems.CheckInDate = dtStartDate;
    //                rItems.BedID = bedIDToMoveOn;
    //                rItems.CheckOutDate = dtEndDate;
    //                rItems.PricePerDay = bd.BedPrice;
    //                rItems.MoveReservation(dbHelp); //Update Reservation Item

    //                //Update Order Item
    //                OrderItems oi = new OrderItems();
    //                oi.PopulateReservationOrderItem(dbHelp, reservationItemID);
    //                oi.OrderItemDesc = bd.ToString();
    //                oi.OrdProductID = bd.BedID;
    //                oi.OrdProductUnitPrice = bd.BedPrice;
    //                oi.MoveReservation(dbHelp);

    //                //Update Invoice Item
    //                InvoiceItems ii = new InvoiceItems();
    //                ii.PopulateReservationInvoiceItem(dbHelp, reservationItemID);
    //                if (ii.InvItemID > 0)
    //                {
    //                    ii.InvProdIDDesc = bd.ToString();
    //                    ii.InvProductID = bd.BedID;
    //                    ii.InvProductUnitPrice = bd.BedPrice;
    //                    ii.MoveReservation(dbHelp);
    //                }

    //                return "SUCCESS";
    //            }
    //            else
    //            {
    //                return "NA";
    //            }
    //        }

    //        return "INVALID";
    //    }
    //    catch
    //    {
    //        return "ERROR";
    //    }
    //}
   
}
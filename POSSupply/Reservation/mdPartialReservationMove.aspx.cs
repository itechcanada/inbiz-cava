﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;

public partial class Reservation_mdPartialReservationMove : BasePage
{
    int _wholeOrderIdToCancel = 0;
    string _priceFormat = @"<span style=""font-size:14px; font-weight:bold;"">{0}</span> {1:F}";    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            switch (this.ActionType)
            {
                case "cancel":
                    FillPartialCancelPreviewForm();
                    break;
                case "move":                    
                    FillPartialMovingPreviewForm();
                    break;
                case "whole":
                    hdnOrderID.Value = Convert.ToString(Request.QueryString["oid"]);                    
                    if (this.OrderID > 0)
                    {
                        pnlCancelWhole.Visible = true;
                        pnlMove.Visible = false;
                        pnlPartialCancel.Visible = false;
                        Product prd = this.GetAdminFeeProduct(null);
                        txtAdminFeeWhole.Text = string.Format("{0:F}", prd.PrdEndUserSalesPrice);
                    }                    
                    break;
            }
        }
    }

    private void FillPartialMovingPreviewForm()
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            pnlPartialCancel.Visible = false;
            pnlCancelWhole.Visible = false;
            pnlMove.Visible = this.ReservationItemID > 0 && this.OldBedID > 0 && this.NewBedID > 0 && this.NewCheckinDate > 0 && this.NewCheckoutDate > 0;
            if (!pnlMove.Visible)
            {
                return;
            }

            Reservation rsv = new Reservation();
            ReservationItems rsvItem = new ReservationItems();
            Bed newBed = new Bed();
            Bed oldBed = new Bed();
            Orders ord = new Orders();
            OrderItems oi = new OrderItems();
            ProductDiscount prdDiscount = new ProductDiscount();
            Partners part = new Partners();

            Product prd = this.GetAdminFeeProduct(null);
            txtAdminFeeMove.Text = string.Format("{0:F}", prd.PrdEndUserSalesPrice);

            rsvItem.PopulateObject(dbHelp, this.ReservationItemID);
            part.PopulateObject(rsvItem.GuestID);
            valGuestMove.Text = part.PartnerLongName;
            txtAccommodationCostMove.Text = string.Format("{0:F} for {1} night(s)", rsvItem.PricePerDay * (double)rsvItem.TotalNights, rsvItem.TotalNights);

            hdnReservationID.Value = rsvItem.ReservationID.ToString();
            rsv.PopulateObject(dbHelp, this.ReservationID);
            hdnOrderID.Value = rsv.SoID.ToString();
            oi.PopulateReservationOrderItem(dbHelp, this.ReservationItemID);

            DateTime movingFrom = BusinessUtility.GetDateTime(this.MovingFromDate.ToString(), "yyyyMMdd");
            DateTime movinTo = BusinessUtility.GetDateTime(this.MovingToDate.ToString(), "yyyyMMdd");
            movinTo = movinTo.AddDays(1);
            DateTime cinDate = BusinessUtility.GetDateTime(this.NewCheckinDate.ToString(), "yyyyMMdd");
            DateTime coutDate = BusinessUtility.GetDateTime(this.NewCheckoutDate.ToString(), "yyyyMMdd");
            coutDate = coutDate.AddDays(1);

            oldBed.BedID = this.OldBedID;
            oldBed.FromDate = movingFrom;
            oldBed.ToDate = movinTo;
            oldBed.PopulateBedInfo(dbHelp, oldBed, Globals.CurrentAppLanguageCode);
            if (rsv.ReserveFor != (int)StatusGuestType.Guest || rsv.ReserveFor != (int)StatusGuestType.None)
            {
                oldBed.BedPrice = 0.0D;
            }
            else
            {
                oldBed.BedPrice = prdDiscount.CalculateAccommodationPrice(dbHelp, oldBed.BedID, movingFrom, movinTo);
            }

            ltMovingFrom.Text = string.Format("<b>{0}/{1}/{2}</b><br>", oldBed.BuildingTitle, oldBed.RoomTitle, oldBed.BedTitle);
            ltMovingFrom.Text += oldBed.ToString().Replace(Environment.NewLine, "<br>");

            newBed.BedID = this.NewBedID;
            newBed.FromDate = cinDate;
            newBed.ToDate = coutDate;
            newBed.PopulateBedInfo(dbHelp, newBed, Globals.CurrentAppLanguageCode);
            if (rsv.ReserveFor != (int)StatusGuestType.Guest || rsv.ReserveFor != (int)StatusGuestType.None)
            {
                newBed.BedPrice = 0.0D;
            }
            else
            {
                newBed.BedPrice = prdDiscount.CalculateAccommodationPrice(dbHelp, newBed.BedID, cinDate, coutDate);
            }

            ltMovingTo.Text = string.Format("<b>{0}/{1}/{2}</b><br>", newBed.BuildingTitle, newBed.RoomTitle, newBed.BedTitle);
            ltMovingTo.Text += newBed.ToString().Replace(Environment.NewLine, "<br>");
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private void FillPartialCancelPreviewForm()
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            int oldBedID = this.OldBedID;

            pnlMove.Visible = false;
            pnlCancelWhole.Visible = false;
            pnlPartialCancel.Visible = this.ReservationItemID > 0;
            if (!pnlPartialCancel.Visible)
            {
                return;
            }

            Reservation rsv = new Reservation();
            ReservationItems rsvItem = new ReservationItems();
            Bed newBed = new Bed();
            Bed oldBed = new Bed();
            Orders ord = new Orders();
            OrderItems oi = new OrderItems();
            ProductDiscount prdDiscount = new ProductDiscount();
            Partners part = new Partners();

            Product prd = this.GetAdminFeeProduct(null);
            txtAdminFeeCancel.Text = string.Format("{0:F}", prd.PrdEndUserSalesPrice);

            rsvItem.PopulateObject(dbHelp, this.ReservationItemID);

            part.PopulateObject(rsvItem.GuestID);
            valGuestCancel.Text = part.PartnerLongName;
            txtAccommodationCostCancel.Text = string.Format("{0:F} for {1} night(s)", rsvItem.PricePerDay * (double)rsvItem.TotalNights, rsvItem.TotalNights);

            oldBedID = rsvItem.BedID;

            hdnOldBedID.Value = oldBedID.ToString();
            hdnReservationID.Value = rsvItem.ReservationID.ToString();
            rsv.PopulateObject(dbHelp, this.ReservationID);
            hdnOrderID.Value = rsv.SoID.ToString();
            oi.PopulateReservationOrderItem(dbHelp, this.ReservationItemID);
            //hdnOrderItemID.Value = _oi.OrderItemID.ToString();

            //If not found in querystring then needs to cancel actual date range(whole reservation item)
            if (this.CancelFromDate <= 0)
            {
                hdnCancelFrom.Value = rsvItem.CheckInDate.ToString("yyyyMMdd");
            }
            if (this.CancelToDate <= 0)
            {
                hdnCancelTo.Value = rsvItem.CheckOutDate.AddDays(-1).ToString("yyyyMMdd");
            }

            DateTime cancelFrom = BusinessUtility.GetDateTime(this.CancelFromDate.ToString(), "yyyyMMdd");
            DateTime cancelTo = BusinessUtility.GetDateTime(this.CancelToDate.ToString(), "yyyyMMdd");
            cancelTo = cancelTo.AddDays(1);


            oldBed.BedID = oldBedID;
            oldBed.FromDate = cancelFrom;
            oldBed.ToDate = cancelTo;
            oldBed.PopulateBedInfo(dbHelp, oldBed, Globals.CurrentAppLanguageCode);


            ltCancelAcc.Text = string.Format("<b>{0}/{1}/{2}</b><br>", oldBed.BuildingTitle, oldBed.RoomTitle, oldBed.BedTitle);
            ltCancelAcc.Text += oldBed.ToString().Replace(Environment.NewLine, "<br>");
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private Product GetAdminFeeProduct(DbHelper dbHelp)
    {
        bool closeConnection = false;
        if (dbHelp == null)
        {
            dbHelp = new DbHelper();
            closeConnection = true;
        }
        try
        {
            Product prd = new Product();
            prd.PopulateAdminFeeProduct(dbHelp);
            return prd;
        }
        catch
        {
            throw;
        }
        finally
        {
            if (closeConnection)
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    public int ReservationItemID {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["rid"], out id);
            return id;
        }
    }

    public int OldBedID {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["old_bed_id"], out id);
            return id;
        }
    }

    public int NewBedID {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["new_bed_id"], out id);
            return id;
        }
    }

    public int MovingFromDate {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["moving_from"], out id);
            return id;
        }
    }

    public int MovingToDate
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["moving_to"], out id);
            return id;
        }
    }

    private int CancelFromDate {
        get {
            int id = 0;
            if (!int.TryParse(Request.QueryString["cancel_from"], out id)) {
                int.TryParse(hdnCancelFrom.Value, out id);
            }
            return id;
        }
    }

    private int CancelToDate
    {
        get
        {
            int id = 0;
            if (!int.TryParse(Request.QueryString["cancel_to"], out id))
            {
                int.TryParse(hdnCancelTo.Value, out id);
            }
            return id;
        }
    }

    public int NewCheckinDate {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["checkin"], out id);
            return id; 
        }
    }

    public int NewCheckoutDate {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["checkout"], out id);
            return id;
        }
    }

    public int ReservationID {
        get {
            int id = 0;
            int.TryParse(hdnReservationID.Value, out id);
            return id;
        }
    }

    public int OrderID
    {
        get
        {
            int id = 0;
            int.TryParse(hdnOrderID.Value, out id);
            return id;
        }
    }

    public string ActionType {
        get {
            return Request.QueryString["action"];
        }
    }

    private int OrderItemID
    {
        get
        {
            int oid = 0;
            int.TryParse(Request.QueryString["oitmid"], out oid);
            return oid;
        }
    }

    protected void btnMove_Click(object sender, EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);        

        double adminFee = 0.00D;
        if (!double.TryParse(txtAdminFeeMove.Text, out adminFee))
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidAdminFeeEntered);
            return;
        }

        Reservation rsv = new Reservation();
        ReservationItems rsvItem = new ReservationItems();        
        Orders ord = new Orders();
        OrderItems oi = new OrderItems();
        try
        {
            rsvItem.PopulateObject(dbHelp, this.ReservationItemID);
            rsv.PopulateObject(dbHelp, this.ReservationID);
            oi.PopulateReservationOrderItem(dbHelp, this.ReservationItemID);

            DateTime movingFrom = BusinessUtility.GetDateTime(this.MovingFromDate.ToString(), "yyyyMMdd");
            DateTime movinTo = BusinessUtility.GetDateTime(this.MovingToDate.ToString(), "yyyyMMdd");
            movinTo = movinTo.AddDays(1);
            DateTime cinDate = BusinessUtility.GetDateTime(this.NewCheckinDate.ToString(), "yyyyMMdd");
            DateTime coutDate = BusinessUtility.GetDateTime(this.NewCheckoutDate.ToString(), "yyyyMMdd");
            coutDate = coutDate.AddDays(1);
            List<SplitReservationRange> spllitedData = new List<SplitReservationRange>();

            //Case complete date range of reservation item is moving
            if (movingFrom == rsvItem.CheckInDate && movinTo == rsvItem.CheckOutDate)
            {
                //To do to move as it is by canceling complete reservation item & move it to other bed for same duration
            }
            else if (movingFrom > rsvItem.CheckInDate && movinTo == rsvItem.CheckOutDate) //Partial date range is moving in between 
            {
                //Neet to split existing reservation item
                //Convert.ToInt32((this.ToDate.Date - this.FromDate.Date).TotalDays)                
                spllitedData.Add(new SplitReservationRange { BedID = rsvItem.BedID, CheckinDate = rsvItem.CheckInDate, CheckoutDate = rsvItem.CheckInDate.AddDays(Convert.ToInt32((movingFrom.Date - rsvItem.CheckInDate.Date).TotalDays)) });               
            }
            else if (movingFrom == rsvItem.CheckInDate && movinTo < rsvItem.CheckOutDate)
            {
                spllitedData.Add(new SplitReservationRange { BedID = rsvItem.BedID, CheckinDate = movinTo, CheckoutDate = rsvItem.CheckOutDate });               
            }
            else if (movingFrom > rsvItem.CheckInDate && movinTo < rsvItem.CheckOutDate)
            {
                spllitedData.Add(new SplitReservationRange { BedID = rsvItem.BedID, CheckinDate = rsvItem.CheckInDate, CheckoutDate = rsvItem.CheckInDate.AddDays(Convert.ToInt32((movingFrom.Date - rsvItem.CheckInDate.Date).TotalDays)) });
                spllitedData.Add(new SplitReservationRange { BedID = rsvItem.BedID, CheckinDate = movinTo, CheckoutDate = rsvItem.CheckOutDate });                
            }            

            //Add new date range data to same dictionary            
            spllitedData.Add(new SplitReservationRange { BedID = this.NewBedID, CheckinDate = cinDate, CheckoutDate = coutDate });

            //Total before canceling & moving so that we can identify is refund / credit require or not
            TotalSummary tsBefore = CalculationHelper.GetOrderTotal(dbHelp, this.OrderID);
            hdnTotalPaidBefore.Value = tsBefore.GrandTotal.ToString();
            MoveReservationItem(dbHelp, rsvItem.ReservationID, rsvItem.ReservationItemID, spllitedData);

            //To Do to Add Action Log            
            new OrderActionLog().AddLog(dbHelp, CurrentUser.UserID, this.OrderID, "Reservation partially moved.", "");

            TotalSummary tsAfter = CalculationHelper.GetOrderTotal(dbHelp, this.OrderID);
            
            if(tsAfter.OutstandingAmount < 0)
            {
                Response.Redirect(string.Format("~/AccountsReceivable/mdReceive.aspx?oid={0}&rcvType={1}&amtToRefund={2}&adminFee={3}", this.OrderID, "refund", (-1.00D * tsAfter.OutstandingAmount), adminFee), false);
            }
            else
            {
                Globals.RegisterCloseDialogScript(this, "parent.reloadGridOnScrollPos();", true);
            }                     
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.ACCOMMODATION_NOT_AVAILABLE)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgAccommodationNoLongerAvailable);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private void MoveReservationItem(DbHelper dbHelp, int reservationID, int rsvItemID, List<SplitReservationRange> splitedDateRange)
    {        
        string sqlUpdateOrderItem = "UPDATE orderitems SET ordProductUnitPrice=0, ordIsItemCanceled=1 WHERE orderItemID=@orderItemID";
        string sqlUpdateRsvItem = "UPDATE z_reservation_items SET PricePerDay=0, IsCanceled=1 WHERE ReservationItemID=@ReservationItemID";        
        try
        {
            OrderItems oldOrderItem = new OrderItems();
            ReservationItems oldItem = new ReservationItems();
            oldItem.PopulateObject(dbHelp, rsvItemID);
            oldOrderItem.PopulateReservationOrderItem(dbHelp, rsvItemID);
            Reservation rsv = new Reservation();
            rsv.PopulateObject(dbHelp, oldItem.ReservationID);
            

            //Insert into Reservation Item & in order Item            
            ProductDiscount prdDisc = new ProductDiscount();
            bool isAccommodationAvailable = true;
            foreach (var item in splitedDateRange)
            {
                if (!ProcessReservation.IsAccommodationAvailalbe(dbHelp, item.CheckinDate, item.CheckoutDate, item.BedID, oldItem.ReservationItemID))
                {
                    isAccommodationAvailable = false;
                    break;
                }               
            }
            if (isAccommodationAvailable)
            {
                OrderItems oi;
                ReservationItems ri;
                foreach (var item in splitedDateRange)
                {
                    double price = 0;
                    int totalDays = Convert.ToInt32((item.CheckoutDate - item.CheckinDate).TotalDays);

                    if (rsv.ReserveFor == (int)StatusGuestType.Guest)
                    {
                        price = prdDisc.CalculateAccommodationPrice(dbHelp, item.BedID, item.CheckinDate, item.CheckoutDate);
                    }

                    ri = new ReservationItems();
                    ri.BedID = item.BedID;
                    ri.CheckInDate = item.CheckinDate;
                    ri.CheckOutDate = item.CheckoutDate;
                    ri.GuestID =  oldItem.GuestID;
                    ri.IsCanceled = false;
                    ri.IsCouple = oldItem.IsCouple;
                    ri.PricePerDay = price;
                    ri.RelationshipToPrimaryGuest =  oldItem.RelationshipToPrimaryGuest;
                    ri.ReservationID = oldItem.ReservationID;
                    ri.Sex = oldItem.Sex;
                    ri.Insert(dbHelp);

                    Bed b = new Bed();
                    b.BedID = ri.BedID;
                    b.BedPrice = ri.PricePerDay;
                    b.FromDate = ri.CheckInDate;
                    b.ToDate = ri.CheckOutDate;
                    b.PopulateBedInfo(dbHelp, b, Globals.CurrentAppLanguageCode);                    
                    if (oldOrderItem.OrderItemID > 0)
                    {
                        oi = new OrderItems();
                        oi.OrderItemDesc = b.ToString();
                        oi.OrdGuestID = ri.GuestID;
                        oi.OrdID = oldOrderItem.OrdID;
                        oi.OrdIsItemCanceled = false;
                        oi.OrdProductDiscount = oldOrderItem.OrdProductDiscount;
                        oi.OrdProductDiscountType = oldOrderItem.OrdProductDiscountType;
                        oi.OrdProductID = ri.BedID;
                        oi.OrdProductQty = ri.TotalNights;
                        oi.OrdProductTaxGrp = oldOrderItem.OrdProductTaxGrp;
                        oi.OrdProductUnitPrice = ri.PricePerDay;
                        oi.OrdReservationItemID = ri.ReservationItemID;
                        oi.Insert(dbHelp);
                    }
                }

                //Update old item to make it as canceld
                if (oldOrderItem.OrderItemID > 0)
                {
                    dbHelp.ExecuteNonQuery(sqlUpdateOrderItem, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("orderItemID", oldOrderItem.OrderItemID, MyDbType.Int)
                    });
                }
                dbHelp.ExecuteNonQuery(sqlUpdateRsvItem, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ReservationItemID", rsvItemID, MyDbType.Int)
                });

                double adminFee = 0.00D;
                double.TryParse(txtAdminFeeMove.Text, out adminFee);

                this.AddAdminFee(dbHelp, adminFee);
            }
            else
            {
                throw new Exception(CustomExceptionCodes.ACCOMMODATION_NOT_AVAILABLE);
            }
        }
        catch
        {
            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    protected void btnPartialCancel_Click(object sender, EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);

        double adminFee = 0.00D;
        if (!double.TryParse(txtAdminFeeCancel.Text, out adminFee))
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidAdminFeeEntered);
            return;
        }

        Reservation _rsv = new Reservation();
        ReservationItems _rsvItem = new ReservationItems();
        OrderItems _oi = new OrderItems();

        try
        {
            _rsvItem.PopulateObject(dbHelp, this.ReservationItemID);
            _rsv.PopulateObject(dbHelp, this.ReservationID);
            _oi.PopulateReservationOrderItem(dbHelp, this.ReservationItemID);

            DateTime cancelFrom = BusinessUtility.GetDateTime(this.CancelFromDate.ToString(), "yyyyMMdd");
            DateTime cancelTo = BusinessUtility.GetDateTime(this.CancelToDate.ToString(), "yyyyMMdd");
            cancelTo = cancelTo.AddDays(1);

            List<SplitReservationRange> spllitedData = new List<SplitReservationRange>();

            //Case complete date range of reservation item is moving
            if (cancelFrom == _rsvItem.CheckInDate && cancelTo == _rsvItem.CheckOutDate)
            {
                //To do to move as it is by canceling complete reservation item & move it to other bed for same duration                
            }
            else if (cancelFrom > _rsvItem.CheckInDate && cancelTo == _rsvItem.CheckOutDate) //Partial date range is moving in between 
            {
                //Neet to split existing reservation item
                //Convert.ToInt32((this.ToDate.Date - this.FromDate.Date).TotalDays)                
                spllitedData.Add(new SplitReservationRange { BedID = _rsvItem.BedID, CheckinDate = _rsvItem.CheckInDate, CheckoutDate = _rsvItem.CheckInDate.AddDays(Convert.ToInt32((cancelFrom.Date - _rsvItem.CheckInDate.Date).TotalDays)) });
            }
            else if (cancelFrom == _rsvItem.CheckInDate && cancelTo < _rsvItem.CheckOutDate)
            {
                spllitedData.Add(new SplitReservationRange { BedID = _rsvItem.BedID, CheckinDate = cancelTo, CheckoutDate = _rsvItem.CheckOutDate });
            }
            else if (cancelFrom > _rsvItem.CheckInDate && cancelTo < _rsvItem.CheckOutDate)
            {
                spllitedData.Add(new SplitReservationRange { BedID = _rsvItem.BedID, CheckinDate = _rsvItem.CheckInDate, CheckoutDate = _rsvItem.CheckInDate.AddDays(Convert.ToInt32((cancelFrom.Date - _rsvItem.CheckInDate.Date).TotalDays)) });
                spllitedData.Add(new SplitReservationRange { BedID = _rsvItem.BedID, CheckinDate = cancelTo, CheckoutDate = _rsvItem.CheckOutDate });
            }

            //Total before canceling & moving so that we can identify is refund / credit require or not
            TotalSummary tsBefore = CalculationHelper.GetOrderTotal(dbHelp, this.OrderID);
            hdnTotalPaidBefore.Value = tsBefore.GrandTotal.ToString();
            PartialCancelReservationItem(dbHelp, _rsvItem.ReservationID, _rsvItem.ReservationItemID, spllitedData);

            //To Do to Add Action Log            
            new OrderActionLog().AddLog(dbHelp, CurrentUser.UserID, this.OrderID, "Reservation partially canceled.", "");

            TotalSummary tsAfter = CalculationHelper.GetOrderTotal(dbHelp, this.OrderID);

            if (tsAfter.OutstandingAmount < 0)
            {
                //Redirect to refund page
                Response.Redirect(string.Format("~/AccountsReceivable/mdReceive.aspx?oid={0}&rcvType={1}&amtToRefund={2}&adminFee={3}", this.OrderID, "refund", (-1.00D * tsAfter.OutstandingAmount), adminFee));
            }
            else
            {
                Globals.RegisterCloseDialogScript(this, "parent.reloadGridOnScrollPos();", true);
            }
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.ACCOMMODATION_NOT_AVAILABLE)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgAccommodationNoLongerAvailable);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }
        finally {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private void PartialCancelReservationItem(DbHelper dbHelp, int reservationID, int rsvItemID, List<SplitReservationRange> splitedDateRange)
    {
        string sqlUpdateOrderItem = "UPDATE orderitems SET ordProductUnitPrice=0, ordIsItemCanceled=1 WHERE orderItemID=@orderItemID";
        string sqlUpdateRsvItem = "UPDATE z_reservation_items SET PricePerDay=0, IsCanceled=1 WHERE ReservationItemID=@ReservationItemID";        
        try
        {
            OrderItems oldOrderItem = new OrderItems();
            ReservationItems oldItem = new ReservationItems();
            oldItem.PopulateObject(dbHelp, rsvItemID);
            oldOrderItem.PopulateReservationOrderItem(dbHelp, rsvItemID);
            Reservation rsv = new Reservation();
            rsv.PopulateObject(dbHelp, oldItem.ReservationID);


            //Insert into Reservation Item & in order Item            
            ProductDiscount prdDisc = new ProductDiscount();
            bool isAccommodationAvailable = true;
            foreach (var item in splitedDateRange)
            {
                if (!ProcessReservation.IsAccommodationAvailalbe(dbHelp, item.CheckinDate, item.CheckoutDate, item.BedID, oldItem.ReservationItemID))
                {
                    isAccommodationAvailable = false;
                    break;
                }
            }
            if (isAccommodationAvailable)
            {
                OrderItems oi;
                ReservationItems ri;
                foreach (var item in splitedDateRange)
                {
                    double price = 0;
                    int totalDays = Convert.ToInt32((item.CheckoutDate - item.CheckinDate).TotalDays);
                    if (rsv.ReserveFor == (int)StatusGuestType.Guest)
                    {
                        price = prdDisc.CalculateAccommodationPrice(dbHelp, item.BedID, item.CheckinDate, item.CheckoutDate);
                    }
                    ri = new ReservationItems();
                    ri.BedID = item.BedID;
                    ri.CheckInDate = item.CheckinDate;
                    ri.CheckOutDate = item.CheckoutDate;
                    ri.GuestID = oldItem.GuestID;
                    ri.IsCanceled = false;
                    ri.IsCouple = oldItem.IsCouple;
                    ri.PricePerDay = price;
                    ri.RelationshipToPrimaryGuest = oldItem.RelationshipToPrimaryGuest;
                    ri.ReservationID = oldItem.ReservationID;
                    ri.Sex = oldItem.Sex;
                    ri.Insert(dbHelp);

                    Bed b = new Bed();
                    b.BedID = ri.BedID;
                    b.BedPrice = ri.PricePerDay;
                    b.FromDate = ri.CheckInDate;
                    b.ToDate = ri.CheckOutDate;
                    b.PopulateBedInfo(dbHelp, b, Globals.CurrentAppLanguageCode);
                    if (oldOrderItem.OrderItemID > 0)
                    {
                        oi = new OrderItems();
                        oi.OrderItemDesc = b.ToString();
                        oi.OrdGuestID = ri.GuestID;
                        oi.OrdID = oldOrderItem.OrdID;
                        oi.OrdIsItemCanceled = false;
                        oi.OrdProductDiscount = oldOrderItem.OrdProductDiscount;
                        oi.OrdProductDiscountType = oldOrderItem.OrdProductDiscountType;
                        oi.OrdProductID = ri.BedID;
                        oi.OrdProductQty = ri.TotalNights;
                        oi.OrdProductTaxGrp = oldOrderItem.OrdProductTaxGrp;
                        oi.OrdProductUnitPrice = ri.PricePerDay;
                        oi.OrdReservationItemID = ri.ReservationItemID;
                        oi.Insert(dbHelp);
                    }
                }

                //Update old item to make it as canceld
                if (oldOrderItem.OrderItemID > 0)
                {
                    dbHelp.ExecuteNonQuery(sqlUpdateOrderItem, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("orderItemID", oldOrderItem.OrderItemID, MyDbType.Int)
                });
                }
                dbHelp.ExecuteNonQuery(sqlUpdateRsvItem, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ReservationItemID", rsvItemID, MyDbType.Int)
                });
                
                double adminFee = 0.00D;
                double.TryParse(txtAdminFeeCancel.Text, out adminFee);

                this.AddAdminFee(dbHelp, adminFee);

                if (this.OrderItemID > 0)
                {
                    SalesCartHelper.SetItemAsCanceled(this.OrderItemID);
                }
            }
            else
            {
                throw new Exception(CustomExceptionCodes.ACCOMMODATION_NOT_AVAILABLE);
            }
        }
        catch
        {
            throw;
        }        
    }

    protected void btnCancelWhole_Click(object sender, EventArgs e)
    {
        double adminFee = 0.00D;
        if (!double.TryParse(txtAdminFeeWhole.Text, out adminFee))
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidAdminFeeEntered);
            return;
        }

        if (this.OrderID > 0)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                TotalSummary tsBefore = CalculationHelper.GetOrderTotal(dbHelp, this.OrderID);

                //Reservation ORDER Canceld
                Orders ord = new Orders();
                ord.CancelReservationOrder(this.OrderID, txtReasonToCancel.Text);

                //Apply Admin Fee to order
                this.AddAdminFee(dbHelp, adminFee);

                //Get total after cancle & applying admin fee
                TotalSummary tsAfter = CalculationHelper.GetOrderTotal(dbHelp, this.OrderID);

                //To Do to Add Action Log            
                new OrderActionLog().AddLog(dbHelp, CurrentUser.UserID, this.OrderID, "Reservation canceled.", "");

                if (tsAfter.OutstandingAmount < 0)
                {
                    //Redirect to refund page
                    Response.Redirect(string.Format("~/AccountsReceivable/mdReceive.aspx?oid={0}&rcvType={1}&amtToRefund={2}&adminFee={3}", this.OrderID, "refund", (-1.00D * tsAfter.OutstandingAmount), adminFee), false);
                }
                else
                {
                    Globals.RegisterCloseDialogScript(this, "parent.reloadGridOnScrollPos();", true);
                }
            }
            catch
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    private void AddAdminFee(DbHelper dbHelp, double adminFee)
    {        
        try
        {
            Orders ord = new Orders();
            OrderItems oi = new OrderItems();
            Product prd = this.GetAdminFeeProduct(dbHelp);
            ord.PopulateObject(dbHelp, this.OrderID);            
            if (prd.ProductID > 0)
            {
                oi.OrderItemDesc = prd.PrdName;
                oi.OrdGuestID = ord.OrdCustID;
                oi.OrdID = ord.OrdID;
                oi.OrdIsItemCanceled = false;
                oi.OrdProductDiscount = 0;
                oi.OrdProductDiscountType = "P";
                oi.OrdProductID = prd.ProductID;
                oi.OrdProductQty = 1;
                oi.OrdProductUnitPrice = adminFee;
                oi.OrdReservationItemID = 0;
                oi.Insert(dbHelp);
            }
        }
        finally 
        {
            
        }
    }
    
    private class SplitReservationRange
    {
        public int BedID { get; set; }
        public DateTime CheckinDate { get; set; }
        public DateTime CheckoutDate { get; set; }
    }
}
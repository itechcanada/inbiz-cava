﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;

public partial class Reservation_desc : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(5000);
        if (!IsPostBack)
        {
            btnEdit.Attributes["onclick"] = string.Format("parent.location.href='redirect.aspx?soid={0}'; return false;", this.OrderID);
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                string sqlGuest = "SELECT COUNT(ri.GuestID) FROM z_reservation r INNER JOIN z_reservation_items ri ON ri.ReservationID = r.ReservationID WHERE r.SoID=@SoID AND IFNULL(ri.IsCanceled, 0)=0";
                if (this.OrderID > 0)
                {
                    object val = dbHelp.GetValue(sqlGuest, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("SoID", this.OrderID, MyDbType.Int)
                    });
                    lblTotalGuest.Text = Convert.ToString(val);                    
                    TotalSummary ts = CalculationHelper.GetOrderTotal(this.OrderID);
                    lblTotalAmount.Text = string.Format("{0} {1:F}", ts.CurrencyCode, ts.GrandTotal);
                    lblTotalBalance.Text = string.Format("{0} {1:F}", ts.CurrencyCode, ts.OutstandingAmount);                    
                }  
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }            
        }        
    }

    public int OrderID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["soid"], out id);
            return id;
        }
    }
}
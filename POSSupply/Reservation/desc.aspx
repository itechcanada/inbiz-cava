﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="desc.aspx.cs" Inherits="Reservation_desc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 250px">
            <tr>
                <td style="width: 150px">
                    <%=Resources.Resource.lblTotalGuest %>
                </td>
                <td style="width: 2px;">
                    :
                </td>
                <td>
                    <asp:Label ID="lblTotalGuest" Text="" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 150px">
                    <%=Resources.Resource.lblTotalAmount %>
                </td>
                <td style="width: 2px;">
                    :
                </td>
                <td>
                     <asp:Label ID="lblTotalAmount" Text="" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 150px">
                    <%=Resources.Resource.lblTotalBalance %>
                </td>
                <td style="width: 2px;">
                    :
                </td>
                <td>
                    <asp:Label ID="lblTotalBalance" Text="" runat="server" />
                </td>
            </tr>
            <%--<tr>
                <td>
                    <%=Resources.Resource.lblCheckIn %>
                </td>
                <td style="width: 2px;">
                    :
                </td>
                <td>
                    <asp:Label ID="lblCheckIn" Text="" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <%=Resources.Resource.lblCheckout %>
                </td>
                <td style="width: 2px;">
                    :
                </td>
                <td>
                   <asp:Label ID="lblCheckout" Text="" runat="server" />
                </td>
            </tr>--%>
        </table>
        <div class="div_command" style="padding-right: 0px;">
            <asp:Button ID="btnEdit" Text="<%$Resources:Resource, lblEditCheckoutReceive%>" runat="server" />            
        </div>
    </div>
    </form>
</body>
</html>

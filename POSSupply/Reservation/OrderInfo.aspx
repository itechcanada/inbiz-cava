﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="OrderInfo.aspx.cs" Inherits="Reservation_OrderInfo" %>
<%@ Import Namespace="iTECH.InbizERP.BusinessLogic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <style type="text/css">
        .tblBed{border-collapse:collapse;}
        .tblBed td{border:1px solid #ccc; padding:5px;}
    </style>
    <%=UIHelper.GetScriptTags("blockui,momentjs", 1)%>  

    <script type="text/javascript">
        function blockContentArea(elementToBlock) {
            $(elementToBlock).block({
                message: '<div>Loading...</div>',
                css: { border: '3px solid #a00' }
            });
        }
        function unblockContentArea(elementToBlock) {
            $(elementToBlock).unblock();
        }
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="contentBottom">
        <div style="text-align: right; margin: 5px 0px;">
            <%--<span id="spCourse" style="display:none;">
                <asp:Label ID="lblSelectCourse" Text="Select Course" runat="server" />
                <asp:DropDownList ID="ddlCourses" runat="server">                
                </asp:DropDownList>
            </span> --%>           
            <asp:Label ID="Label1" Text="<%$Resources:Resource, lblCurrentReservationFor%>" runat="server" />
            <asp:DropDownList ID="ddlCurrentReservation" runat="server" AutoPostBack="false">
            </asp:DropDownList>           
            <%--<script type="text/javascript">
                $("#<%=ddlCurrentReservation.ClientID%>").change(function () {
                    if ($(this).val() == "<%=(int)StatusGuestType.CourseParticipants%>") {
                        $("#spCourse").show();
                    }
                    else {
                        $("#spCourse").hide();
                    }
                });
            </script>--%>
        </div>
        <div class="" style="padding:5px;">
            <div id="grid_wrapper">
                <trirand:JQGrid runat="server" ID="grdBeds" Height="200px" AutoWidth="True" OnCellBinding="grdBeds_CellBinding"
                    OnDataRequesting="grdBeds_DataRequesting" 
                    ondatarequested="grdBeds_DataRequested">
                    <Columns>
                        <trirand:JQGridColumn DataField="CartItemID" HeaderText="CartItemID" Editable="false"
                            PrimaryKey="True" Width="30" TextAlign="Center" Visible="false" Sortable="false" />
                        <trirand:JQGridColumn DataField="RoomType" HeaderText="<%$Resources:Resource, lblRoomTypeBuilding%>" Editable="false" TextAlign="Center" Sortable="false">
                        </trirand:JQGridColumn>                        
                        <trirand:JQGridColumn DataField="CheckInDate" HeaderText="<%$Resources:Resource, lblCheckIn%>" DataFormatString="{0:MM/dd/yyyy}"  TextAlign="Center" Width="90" Sortable="false">                            
                        </trirand:JQGridColumn>
                        <trirand:JQGridColumn DataField="CheckOutDate" HeaderText="<%$Resources:Resource, lblCheckout %>" DataFormatString="{0:MM/dd/yyyy}" TextAlign="Center" Width="90" Sortable="false">
                        </trirand:JQGridColumn>
                        <trirand:JQGridColumn DataField="TotalNights" HeaderText="<%$Resources:Resource, lblTotalNights%>" Editable="false" TextAlign="Center" Width="70" Sortable="false">
                        </trirand:JQGridColumn>
                        <trirand:JQGridColumn DataField="BedPrice" HeaderText="<%$Resources:Resource, lblPricePerDay%>" Editable="false" DataFormatString="{0:F}" TextAlign="Right" Width="70" Sortable="false">
                        </trirand:JQGridColumn>
                        <trirand:JQGridColumn DataField="TotalBedPrice" HeaderText="<%$Resources:Resource, lblTotal %>" Editable="false" DataFormatString="{0:F}" 
                         TextAlign="Right" Width="70" Sortable="false">
                        </trirand:JQGridColumn>
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField=""
                            Sortable="false" TextAlign="Center" Width="50" Visible="false" />
                        <trirand:JQGridColumn HeaderText="ErrorData" DataField="ErrorData"
                            Sortable="false" TextAlign="Center" Width="50" Visible="false" />
                    </Columns>                    
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowRefreshButton="True" ShowAddButton="false" ShowEditButton="false"
                         ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" 
                        ShowFooter="True" />                      
                    <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" 
                        LoadComplete="grdLoadComplete" />
                </trirand:JQGrid>
                <trirand:JQDatePicker runat="server" ID="dpCheckInDate" DateFormat="mm/dd/yyyy" DisplayMode="ControlEditor"
                    ShowOn="Focus" ChangeMonth="True" ChangeYear="True" Duration="100" />
                <trirand:JQDatePicker runat="server" ID="dpCheckoutDate" 
                    DateFormat="mm/dd/yyyy" DisplayMode="ControlEditor"
                    ShowOn="Focus" ChangeMonth="True" ChangeYear="True" Duration="100"  />   
                             
            </div>
        </div>        
        <div style="padding:10px; margin-top:5px; border-top:2px solid #ccc; font-size:18px; font-weight:bold; display:none;">
            Sub Total: 
            <asp:Literal ID="ltOrderTotal" Text="" runat="server" />
        </div>
        <div style="text-align: right; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;">
            <asp:Button ID="btnAddABed" Text="Add a Bed" runat="server" 
                 onclick="btnAddABed_Click" Visible="false" />
            <asp:Button ID="btnReserveNow" Text="Reserve Now!" runat="server" 
                 onclick="btnReserveNow_Click" />
            <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
            CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
        </div>
        <asp:HiddenField ID="hdnBeds" Value="0" runat="server" />
        <asp:SqlDataSource ID="sdsBedInfo" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient"></asp:SqlDataSource>
    </div>
    <asp:HiddenField ID="hdnCartLoaded" runat="server" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        function validateAndUpdateItem(containerID) {            
            var dataToPost = {};
            $container = $("#" + containerID);
            blockContentArea($container);
            dataToPost.itemIndex = $(".cart_id", $container).eq(0).val();
            dataToPost.cInDate = $(".fdate", $container).eq(0).val();
            dataToPost.cOutDate = $(".todate", $container).eq(0).val();
            $totalDaysField = $(".total_days", $container).eq(0);                
            $.ajax({
                type: "POST",
                url: "OrderInfo.aspx/ValidateAndUpdateCartItem",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(dataToPost),
                success: function (data) {
                    if (data.d == "INVALID_DATE_RANGE") {
                        alert("Invalid Date Range!");
                    }
                    else if (data.d == "ACCOMMODATION_NOT_AVAILABLE") {
                        alert("Accommodation not avaialbe during selected date range!");
                    }
                    else if (data.d == "0") {
                        alert("Error!");
                    }
                    else {
                        $totalDaysField.val(data.d);
                    }
                    unblockContentArea($container);
                },
                error: function (request, status, errorThrown) {
                    alert(status);
                }
            });
        }
    </script>

    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdBeds.ClientID %>";        


        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);
       

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function grdLoadComplete(data) {            
            resize_the_grid();            
//            $(".jq_datepicker").datepicker({ changeMonth: true,
//                changeYear: true
            //            });
            $(".jq_datepicker").datepicker($.extend($.datepicker.regional[_jqdplang]).extend({
                changeMonth: true,
                changeYear: true,
                onSelect: function (dateText, inst) {
                    editRow($(this).attr("rowKey"));
                }
            })); 
            if (data.userdata.ErrorData != "") {
                alert(data.userdata.ErrorData);
            }            
        }

        function setSelectedRow(key) {
            var grid = $('#' + gridID);
            grid.setSelection(key);
        }

        function editRow(key) {
            var grid = $('#' + gridID);
            grid.setSelection(key);

            var dataToPost = {};           
            dataToPost["editRowKey"] = key;
            dataToPost["editFDate"] = $("#" + key + "_fDate").val();
            dataToPost["editTDate"] = $("#" + key + "_tDate").val();
            dataToPost["gstType"] = $("#<%=ddlCurrentReservation.ClientID%>").val();

            for (var k in dataToPost) {
                grid.setPostDataItem(k, dataToPost[k]);
            }
            
            grid.trigger("reloadGrid", [{ page: 1}]);


            //alert(JSON.stringify(dataToPost));

            for (var k in dataToPost) {
                //alert(dataToPost[k]);
                //grid.setPostDataItem(k, dataToPost[k]); 
                grid.removePostDataItem(k);         
            }
        }

        function editPrice(ele) {
            var grid = $('#' + gridID);
            grid.setSelection($(ele).attr("rowKey"));

            var dataToPost = {};
            dataToPost["updatePriceRowKey"] = $(ele).attr("rowKey");
            dataToPost["priceToUpdate"] = $(ele).val();

            for (var k in dataToPost) {
                grid.setPostDataItem(k, dataToPost[k]);
            }

            grid.trigger("reloadGrid", [{ page: 1}]);
            for (var k in dataToPost) {               
                grid.removePostDataItem(k);
            }
        }

        $("#<%=ddlCurrentReservation.ClientID%>").change(function () {
            var grid = $('#' + gridID);
            grid.setPostDataItem("applyPriceKey", $(this).val());
            grid.trigger("reloadGrid", [{ page: 1}]);
            grid.removePostDataItem("applyPriceKey");    
        });
    </script>
</asp:Content>


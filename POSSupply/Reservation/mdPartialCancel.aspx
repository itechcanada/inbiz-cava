﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdPartialCancel.aspx.cs" Inherits="Reservation_mdPartialCancel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <style type="text/css">
        ul.form li div.lbl{width:150px !important;}        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
     <div style="padding: 5px 10px;">
         <asp:Panel ID="pnlPartialCancel" runat="server" DefaultButton="btnMove">
             <ul class="form">
                 <li>
                     <div class="lbl">                        
                         <asp:Label ID="Label1" Text="<%$Resources:Resource, lblPartialCancel%>" runat="server" />:
                     </div>
                     <div class="input">
                         <asp:Literal ID="ltCancelAcc" Text="" runat="server" />
                     </div>
                     <div class="clearBoth">
                     </div>
                 </li>                 
             </ul>
             <p>
                <b><%=Resources.Resource.notAdminFeeWillBeApplied%></b>
             </p>
             <div class="div_command">                
                 <asp:Button ID="btnPartialCancel" Text="<%$Resources:Resource, lblCancelSelectedDateReservation%>" runat="server" OnClick="btnPartialCancel_Click" />
             </div>
         </asp:Panel>
     </div>
     <asp:HiddenField ID="hdnReservationID" runat="server" />
     <asp:HiddenField ID="hdnOrderID" runat="server" />
     <%--<asp:HiddenField ID="hdnOrderItemID" runat="server" />--%>
     <asp:HiddenField ID="hdnTotalPaidBefore" runat="server" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
     <script type="text/javascript">
         $("#<%=btnPartialCancel.ClientID%>").click(function () {
             parent._isRequireReload = true;
         });
     </script>
</asp:Content>


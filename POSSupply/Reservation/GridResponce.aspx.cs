﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using Trirand.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;

public partial class Reservation_GridResponce : BasePage
{
    SearchFilter _searchFilter = new SearchFilter();
    int _currentRow = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.FindControl("pnlMessageScript").Visible = false;

        //Following Statmements Needs to execute on every request
        DateTime[] datesColumns = this.GetAgendaDays();

        this.BuildJqGridMedatoryColumns();
        DateTime fDate = DateTime.MinValue;
        DateTime tDate = DateTime.MinValue;
        foreach (var item in datesColumns)
        {
            JQGridColumn col = new JQGridColumn();
            col.HeaderText = string.Format("{0} {1}", this.GetWeekDayName(item), this.GetDayString(item)).Trim();
            col.DataField = "";
            col.PrimaryKey = false;
            col.Sortable = false;
            col.Visible = true;
            col.CssClass = "col_date";
            col.Width = 70;
            col.TextAlign = Trirand.Web.UI.WebControls.TextAlign.Center;
            grdReport.Columns.Add(col);

            if (fDate == DateTime.MinValue)
            {
                fDate = item;
            }
            tDate = item;
        }

        //If not postback
        if (grdReport.AjaxCallBackMode == AjaxCallBackMode.None)
        {
            if (fDate == tDate)
            {
                hdnCurrentIntervalDisplay.Value = string.Format("{0:dddd MMMM dd, yyyy}", fDate);
            }
            else if (fDate.Year != tDate.Year)
            {
                hdnCurrentIntervalDisplay.Value = string.Format("{0:MMM dd, yyyy} - {1:MMM dd, yyyy}", fDate, tDate);
            }
            else if (fDate.Month != tDate.Month)
            {
                hdnCurrentIntervalDisplay.Value = string.Format("{0:MMM dd} - {1:MMM dd, yyyy}", fDate, tDate);
            }
            else
            {
                hdnCurrentIntervalDisplay.Value = string.Format("{0:MMM dd} - {1:dd, yyyy}", fDate, tDate);
            }
        }
    }

    private void BuildJqGridMedatoryColumns()
    {
        //Primary Key Column
        JQGridColumn colBedID = new JQGridColumn();
        colBedID.HeaderText = "BedID";
        colBedID.DataField = "BedID";
        colBedID.PrimaryKey = true;
        colBedID.Sortable = false;
        colBedID.Visible = false;
        grdReport.Columns.Add(colBedID);

        JQGridColumn colRoomID = new JQGridColumn();
        colRoomID.HeaderText = "RoomID";
        colRoomID.DataField = "RoomID";
        colRoomID.PrimaryKey = false;
        colRoomID.Sortable = false;
        colRoomID.Visible = false;
        grdReport.Columns.Add(colRoomID);

        JQGridColumn colBuildingID = new JQGridColumn();
        colBuildingID.HeaderText = "BuildingID";
        colBuildingID.DataField = "BuildingID";
        colBuildingID.PrimaryKey = false;
        colBuildingID.Sortable = false;
        colBuildingID.Visible = false;
        grdReport.Columns.Add(colBuildingID);

        //This Column Need to be formated on cell binding
        JQGridColumn colBuildingRoom = new JQGridColumn();
        colBuildingRoom.HeaderText = "Building & Rooms";
        colBuildingRoom.DataField = "BedID";
        colBuildingRoom.PrimaryKey = false;
        colBuildingRoom.Sortable = false;
        colBuildingRoom.Visible = true;
        colBuildingRoom.CssClass = "col_first";
        colBuildingRoom.Resizable = false;
        colBuildingRoom.TextAlign = Trirand.Web.UI.WebControls.TextAlign.Center;
        grdReport.Columns.Add(colBuildingRoom);

        JQGridColumn colBed = new JQGridColumn();
        colBed.HeaderText = "Beds";
        colBed.DataField = "BedTitle";
        colBed.PrimaryKey = false;
        colBed.Sortable = false;
        colBed.Visible = true;
        colBed.CssClass = "col_second";
        colBed.TextAlign = Trirand.Web.UI.WebControls.TextAlign.Center;
        colBed.Resizable = false;
        grdReport.Columns.Add(colBed);
    }

    #region Date Members
    public DateTime[] GetAgendaDays()
    {
        switch (this.View.ToLower())
        {
            case CalendarView.ROLLING_WEEK:
                return GetRollingWeekView();
            case CalendarView.WEEK:
                return GetWeekDays();
            case CalendarView.TWO_WEEK:
                return GetTwoWeekDays();
            case CalendarView.MONTH:
                return GetMonthDays();
            case CalendarView.DAY:
                return new DateTime[] { this.SelectedDate };
            default:
                //return new DateTime[] { this.SelectedDate };
                return GetWeekDays();
        }
    }

    public string GetCssClass(DateTime dt)
    {
        if (dt.Date == DateTime.Today && (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday))
        {
            return "today_weekend";
        }
        if (dt.Date == DateTime.Today)
        {
            return "today";
        }
        else if (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
        {
            return "weekend";
        }
        return "";
    }

    //Mon to Sun
    public DateTime[] GetWeekDays()
    {
        DateTime offSetDate = this.SelectedDate;
        DateTime monday = offSetDate.AddDays(-(int)offSetDate.DayOfWeek).AddDays((int)DayOfWeek.Monday);

        return new DateTime[] { 
            monday.AddDays(0),
            monday.AddDays(1),
            monday.AddDays(2),
            monday.AddDays(3),
            monday.AddDays(4),
            monday.AddDays(5),
            monday.AddDays(6)
        };
    }

    //Mon to Sun
    public DateTime[] GetTwoWeekDays()
    {
        DateTime offSetDate = this.SelectedDate;
        //DateTime monday = offSetDate.AddDays(-(int)offSetDate.DayOfWeek).AddDays((int)DayOfWeek.Monday);

        return new DateTime[] { 
            offSetDate.AddDays(0),
            offSetDate.AddDays(1),
            offSetDate.AddDays(2),
            offSetDate.AddDays(3),
            offSetDate.AddDays(4),
            offSetDate.AddDays(5),
            offSetDate.AddDays(6),
            offSetDate.AddDays(7),
            offSetDate.AddDays(8),
            offSetDate.AddDays(9),
            offSetDate.AddDays(10),
            offSetDate.AddDays(11),
            offSetDate.AddDays(12),
            offSetDate.AddDays(13)
        };
    }

    public DateTime[] GetMonthDays()
    {
        DateTime offSetDate = this.SelectedDate;
        List<DateTime> lstDates = new List<DateTime>();
        int daysInMonth = DateTime.DaysInMonth(offSetDate.Year, offSetDate.Month);
        for (int i = 1; i <= daysInMonth; i++)
        {
            lstDates.Add(new DateTime(offSetDate.Year, offSetDate.Month, i));
        }
        return lstDates.ToArray();
    }

    public DateTime[] GetRollingWeekView()
    {
        DateTime selDate = this.SelectedDate;

        return new DateTime[] { 
            selDate.Date.AddDays(0),
            selDate.Date.AddDays(1),
            selDate.Date.AddDays(2),
            selDate.Date.AddDays(3),
            selDate.Date.AddDays(4),
            selDate.Date.AddDays(5),
            selDate.Date.AddDays(6)
        };
    }

    public string GetDayString(DateTime d)
    {
        //return string.Format("{0}/{1}", d.Month, d.Day);
        switch (this.View.ToLower())
        {
            case CalendarView.MONTH:
                return string.Format("{0}", d.Day);
            case CalendarView.ROLLING_WEEK:
            case CalendarView.WEEK:
            case CalendarView.TWO_WEEK:
            case CalendarView.DAY:
            default:
                return string.Format("{0}/{1}", d.Month, d.Day);
        }
    }

    public string GetWeekDayName(DayOfWeek d)
    {

        return CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[(int)d];
    }

    public string GetWeekDayName(DateTime d)
    {
        //return CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[(int)d.DayOfWeek];
        switch (this.View.ToLower())
        {
            case CalendarView.MONTH:
                //return string.Empty;
                return CultureInfo.CurrentCulture.DateTimeFormat.GetShortestDayName(d.DayOfWeek).Substring(0, 1);
            case CalendarView.ROLLING_WEEK:
            case CalendarView.WEEK:
            case CalendarView.TWO_WEEK:
            case CalendarView.DAY:
            default:
                return CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[(int)d.DayOfWeek];
        }
    }
    #endregion

    public string View
    {
        get
        {
            switch (Request.QueryString["view"])
            {
                case CalendarView.DAY:
                    return CalendarView.DAY;
                case CalendarView.MONTH:
                    return CalendarView.MONTH;
                case CalendarView.ROLLING_WEEK:
                    return CalendarView.ROLLING_WEEK;
                case CalendarView.TWO_WEEK:
                    return CalendarView.TWO_WEEK;
                default:
                    return CalendarView.DEFAULT;
            }
        }
    }

    public int BuildingID
    {
        get
        {
            int id = 0;
            if (this.HasKeyInRequest("building"))
            {
                int.TryParse(Request.QueryString["building"], out id);
            }
            else if (this.HasKeyInRequest("in_building"))
            {
                int.TryParse(Request.QueryString["in_building"], out id);
            }
            //else if (_searchFilter["building"] != null)
            //{
            //    int.TryParse(_searchFilter["building"], out id);
            //}
            return id;
        }
    }

    public int RoomID
    {
        get
        {
            int id = 0;
            if (this.HasKeyInRequest("room"))
            {
                int.TryParse(Request.QueryString["room"], out id);
            }
            else if (this.HasKeyInRequest("in_room"))
            {
                int.TryParse(Request.QueryString["in_room"], out id);
            }
            //else if (_searchFilter["room"] != null)
            //{
            //    int.TryParse(_searchFilter["room"], out id);
            //}            
            return id;
        }
    }

    public int BedID
    {
        get
        {
            int id = 0;
            if (this.HasKeyInRequest("bed"))
            {
                int.TryParse(Request.QueryString["bed"], out id);
            }
            else if (this.HasKeyInRequest("in_bed"))
            {
                int.TryParse(Request.QueryString["in_bed"], out id);
            }
            //else if (_searchFilter["bed"] != null)
            //{
            //    int.TryParse(_searchFilter["bed"], out id);
            //}            
            return id;
        }
    }

    public string Ammenities
    {
        get
        {
            if (this.HasKeyInRequest("amenities"))
                return BusinessUtility.GetString(Request.QueryString["amenities"]);
            else if (this.HasKeyInRequest("in_amenities"))
                return BusinessUtility.GetString(Request.QueryString["in_amenities"]);
            return string.Empty;
        }
    }

    public string GetDateString(DateTime dt)
    {
        return string.Format("{0:yyyyMMdd}", dt);
    }

    public DateTime SelectedDate
    {
        get
        {
            DateTime dt = DateTime.Today;
            if (this.HasKeyInRequest("selectedDate"))
            {
                dt = BusinessUtility.GetDateTime(Request.QueryString["selectedDate"], DateFormat.MMddyyyy);
            }
            //else if (_searchFilter["selectedDate"] != null)
            //{
            //    dt = BusinessUtility.GetDateTime(_searchFilter["selectedDate"], DateFormat.MMddyyyy);
            //}

            return dt;
        }
    }

    public DateTime AvailOn
    {
        get
        {
            if (this.HasKeyInRequest("showAvailable") && Request.QueryString["showAvailable"] == "1")
            {
                return this.SelectedDate;
            }
            return DateTime.MinValue;
        }
    }

    private bool IsFilterCommand
    {
        get
        {
            return Request.QueryString.AllKeys.Contains<string>("_history");
        }
    }

    private struct CalendarView
    {
        public const string DAY = "day";
        public const string ROLLING_WEEK = "rweek";
        public const string WEEK = "week";
        public const string TWO_WEEK = "2week";
        public const string MONTH = "month";
        public const string DEFAULT = "2week";
    }

    int soFlag = -1;
    protected void grdReport_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        int dsRoomIDIdx = 1;
        int sourceDateArrColIdx = 6;

        if (e.ColumnIndex == 3)
        {            
            e.CellHtml = string.Format(@"<span class=""{0}"" room-id={3}>{1} - {2}</span>", "jqgrid_custom_cell room_display", e.RowValues[5], e.RowValues[4], e.RowValues[dsRoomIDIdx]);
        }        
        if (e.ColumnIndex > 4)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                Dictionary<string, string> divAttr = new Dictionary<string, string>();
                List<DateTime> arrDates = (List<DateTime>)e.RowValues[sourceDateArrColIdx];

                divAttr["id"] = string.Format("{0}_{1}", e.RowKey, this.GetDateString(arrDates[e.ColumnIndex - 5]));
                divAttr["class"] = "td_hack";
                divAttr["row_id"] = e.RowKey;
                divAttr["cell_idx"] = e.ColumnIndex.ToString();
                divAttr["cell_date_val"] = this.GetDateString(arrDates[e.ColumnIndex - 5]);

                //e.CellHtml += string.Format(@"<span id=""{2}_{1}"" class=""{0}"" row_id=""{2}"" cell_idx=""{3}"" cell_date_val=""{1}"">{1}</span>", "date_cell hide", this.GetDateString(arrDates[e.ColumnIndex - 5]), e.RowKey, e.ColumnIndex);

                string blackOutReason = this.GetBlackoutReason(dbHelp, BusinessUtility.GetInt(e.RowKey), (DateTime)arrDates[e.ColumnIndex - 5]);

                //Validate is date reserved
                //DataTable dtBedInfo = ProcessReservation.GetBedInfo(arrDates[e.ColumnIndex - 5], BusinessUtility.GetInt(e.RowKey));
                ReservationDescription desc = this.GetReservationDescription(dbHelp, arrDates[e.ColumnIndex - 5], BusinessUtility.GetInt(e.RowKey));
                StatusGuestType reservedFor = StatusGuestType.None;

                //List<string> lstTdClass = new List<string>();

                if (desc != null)
                {
                    reservedFor = (StatusGuestType)desc.ReserveFor;
                    divAttr["isReserved"] = "true";
                    //lstTdClass.Add("jqgrid_custom_cell");
                    //lstTdClass.Add("cell_reserved");
                    switch (reservedFor)
                    {
                        case StatusGuestType.Staff:
                            divAttr["class"] += " cell_reserved_staff";
                            //lstTdClass.Add("cell_reserved_staff");
                            break;
                        case StatusGuestType.Guest:
                        case StatusGuestType.CourseParticipants:
                            if (desc.Sex.ToUpper() == "M" && !desc.IsCouple)
                            {
                                divAttr["class"] += " cell_reserved_guest_m";
                                //lstTdClass.Add("cell_reserved_guest_m");
                            }
                            else if (desc.Sex.ToUpper() == "F" && !desc.IsCouple)
                            {
                                divAttr["class"] += " cell_reserved_guest_f";
                                //lstTdClass.Add("cell_reserved_guest_f");
                            }
                            else if (desc.IsCouple)
                            {
                                divAttr["class"] += " cell_reserved_guest_c";
                                //lstTdClass.Add("cell_reserved_guest_c");
                            }
                            break;
                        case StatusGuestType.SpecialGuest:
                            divAttr["class"] += " cell_reserved_spguest";
                            //divAttr["class"] += " cell_reserved_cupart";
                            //lstTdClass.Add("cell_reserved_cupart");
                            break;
                        //case StatusGuestType.CourseParticipants:
                        //divAttr["class"] += " cell_reserved_spguest";
                        //lstTdClass.Add("cell_reserved_spguest");
                        // break;
                    }

                    string custName = desc.SpirtualName;
                    if (string.IsNullOrEmpty(custName))
                    {
                        custName = desc.FirstName;
                    }
                    if (string.IsNullOrEmpty(custName))
                    {
                        custName = desc.LastName;
                    }

                    string idText = custName.Length > 8 ? custName.Substring(0, 6) + ".." : custName;
                    idText = idText.Length > 0 ? idText : string.Format("RES-{0}", desc.SOID);
                    //string tdClass = string.Join(" ", lstTdClass.ToArray());
                    if (desc.SOID > 0 && soFlag != desc.SOID)
                    {
                        TotalSummary lTotal = CalculationHelper.GetOrderTotal(desc.SOID);
                        if (lTotal.OutstandingAmount > 0)
                        {
                            divAttr["class"] += " unpaid";
                        }
                    }

                    if (desc.IsInvoiceCreated)
                    {
                        divAttr["class"] += " invoiced";
                    }
                    divAttr["soid"] = desc.SOID.ToString();
                    e.CellHtml += string.Format(@"<div class=""desc_dialog"" style=""display:none;"" title=""{0}""></div>", desc.SOID);
                    //Set Blackout as hidden
                    if (!string.IsNullOrEmpty(blackOutReason))
                    {
                        blackOutReason = blackOutReason.Length > 8 ? blackOutReason.Substring(0, 8) : blackOutReason;
                        divAttr["isBlackOut"] = "true";
                        divAttr["blackOutReason"] = blackOutReason;
                        //e.CellHtml += string.Format(@"<span class=""{0}"" style='display:none;'>{1}</span>", "jqgrid_custom_cell cell_black_out", blackOutReason);
                        //return;
                    }
                    else
                    {
                        divAttr["isBlackOut"] = "false";
                    }
                    divAttr["res_item_id"] = desc.ReservationItemID.ToString();
                    //e.CellHtml += string.Format("<span res_item_id='{0}' class='reservation_id_holder' style='display:none;'></span>", desc.ReservationItemID);
                    e.CellHtml += idText;
                    string course = this.GetCourse(dbHelp, desc.SOID);
                    if (!string.IsNullOrEmpty(course))
                    {
                        e.CellHtml += string.Format("<br>{0}", course);
                    }
                }
                else
                {
                    //Set Blackout as visible
                    if (!string.IsNullOrEmpty(blackOutReason))
                    {
                        blackOutReason = blackOutReason.Length > 8 ? blackOutReason.Substring(0, 8) : blackOutReason;
                        divAttr["class"] += " cell_black_out";
                        divAttr["blackOutReason"] = blackOutReason;
                        e.CellHtml += blackOutReason;
                        divAttr["isBlackOut"] = "true";

                        //e.CellHtml += string.Format(@"<span class=""{0}"">{1}</span>", "jqgrid_custom_cell cell_black_out", blackOutReason);
                        //return;
                    }
                    else
                    {
                        divAttr["isBlackOut"] = "false";
                    }
                }

                e.CellHtml = this.GetDivHtml(divAttr, e.CellHtml);
            }
            catch
            {

            }
            finally {
                dbHelp.CloseDatabaseConnection();
            }                                   
        }
    }

    private string GetDivHtml(Dictionary<string, string> attr, string innerHtml)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append("<div ");
        foreach (var item in attr.Keys)
        {
            sb.AppendFormat(@"{0}=""{1}""", item, attr[item]);
        }
        sb.Append(">").Append(innerHtml).Append("</div>");
        return sb.ToString();
    }

    protected void grdReport_DataRequested(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestedEventArgs e)
    {
             
    }

    protected void grdReport_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {                      
        grdReport.DataSource = this.GetBedsToReserve(this.BuildingID, this.RoomID, this.BedID, this.Ammenities, this.GetAgendaDays(), this.AvailOn);
        grdReport.DataBind();
    }

    private List<ReservationCalendar> GetBedsToReserve(int buildingid, int roomid, int bedid, string amenities, DateTime[] datesToBind, DateTime availOn)
    {
        string pAmenity = string.Empty;
        int countAmt = 0;
        if (!string.IsNullOrEmpty(amenities))
        {
            pAmenity = StringUtil.GetCleneJoinedString(",", amenities);
            countAmt = pAmenity.Split(',').Length;
        }

        DbHelper dbHelp = new DbHelper(true);
        List<ReservationCalendar> lstData = new List<ReservationCalendar>();
        string sql = "SELECT v.BedID, v.BedTitle, v.BuildingID,  " + Globals.GetFieldName("v.Building", Globals.CurrentAppLanguageCode) + " AS BuildingTitle, ";
        sql += " v.RoomID,  " + Globals.GetFieldName("v.Room", Globals.CurrentAppLanguageCode) + " AS RoomTitle, v.SeqBuilding,";
        sql += " v.SeqRoom, v.SeqBed";        
        sql += " FROM vw_beds v ";
        if (countAmt > 0)
        {
            sql += " LEFT OUTER JOIN z_product_attributes a ON a.ProductID = v.BedID";
        }
        sql += " WHERE 1=1";
        List<MySqlParameter> p = new List<MySqlParameter>();               

        if (bedid > 0)
        {
            sql += " AND v.BedID=@BedID";
            p.Add(DbUtility.GetParameter("BedID", bedid, MyDbType.Int));
        }
        else if (roomid > 0)
        {
            sql += " AND v.RoomID=@RoomID";
            p.Add(DbUtility.GetParameter("RoomID", roomid, MyDbType.Int));
        }
        else if (buildingid > 0)
        {
            sql += " AND v.BuildingID=@BuildingID";
            p.Add(DbUtility.GetParameter("BuildingID", buildingid, MyDbType.Int));
        }        
        
        
        if (countAmt > 0)
        {
            sql += string.Format(" AND a.AttributeID IN ({0})", pAmenity);
            sql += " GROUP BY a.ProductID";
            sql += string.Format(" HAVING COUNT(a.ProductID) = {0}", countAmt);
            //sql += string.Format(" AND v.BedID IN (SELECT ProductID FROM z_product_attributes WHERE AttributeID IN ({0}))", pAmenity);
        }
              
        sql += " ORDER BY v.SeqBuilding, v.SeqRoom, v.SeqBed";
        MySqlDataReader dr = null;
        try
        {
            dr = dbHelp.GetDataReader(sql, CommandType.Text, p.ToArray());
            while (dr.Read())
            {
                ReservationCalendar rsvRow = new ReservationCalendar();
                rsvRow.BedID = BusinessUtility.GetInt(dr["BedID"]);
                rsvRow.RoomID = BusinessUtility.GetInt(dr["RoomID"]);
                rsvRow.BuildingID = BusinessUtility.GetInt(dr["BuildingID"]);
                rsvRow.BedTitle = BusinessUtility.GetString(dr["BedTitle"]);
                rsvRow.RoomTitle = BusinessUtility.GetString(dr["RoomTitle"]);
                rsvRow.BuildingTitle = BusinessUtility.GetString(dr["BuildingTitle"]);
                foreach (var item in datesToBind)
                {
                    rsvRow.ListDateColumns.Add(item);
                }
                lstData.Add(rsvRow);
            }
            if (dr!= null && !dr.IsClosed)
            {
                dr.Close();
                dr.Dispose();
            }
            if (availOn != DateTime.MinValue)
            {
                List<ReservationCalendar> itemsIndexToRemove = new List<ReservationCalendar>();                
                foreach (var item in lstData)
                {
                    if (!ProcessReservation.IsAccommodationAvailalbe(dbHelp, availOn, availOn.AddDays(1), item.BedID, -1))
                    {
                        itemsIndexToRemove.Add(item);
                    }                    
                }

                foreach (var item in itemsIndexToRemove)
                {
                    lstData.Remove(item);
                }
            }               

            return lstData;
        }
        catch
        {

            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection(dr);         
        }
    }

    private bool HasKeyInRequest(string ky)
    {        
        foreach (string key in Request.QueryString.AllKeys)
        {
            if (key.ToLower().Trim() == ky.ToLower().Trim())
            {
                return true;
            }      
        }
        return false;
    }

    private string GetBlackoutReason(DbHelper dbHelp, int bedID, DateTime dateToValidate)
    {
        string sql = "SELECT st." + Globals.GetFieldName("Reason", Globals.CurrentAppLanguageCode) + " AS ReasonDesc FROM z_product_blackouts pb ";
        sql += " INNER JOIN z_blackout_reasons st ON st.ID = pb.Reason ";
        sql += " WHERE pb.ProductID = @ProductID AND @DateValidate BETWEEN pb.StartDate AND pb.EndDate LIMIT 1";
        //DbHelper dbHelp = new DbHelper();
        try
        {
            object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] {                     
                    DbUtility.GetParameter("ProductID", bedID, MyDbType.Int),
                    DbUtility.GetParameter("DateValidate", dateToValidate, MyDbType.DateTime)
                });

            return BusinessUtility.GetString(val);
        }
        catch
        {
            throw;
        }
        finally
        {
            //dbHelp.CloseDatabaseConnection();
        }
    }

    private ReservationDescription GetReservationDescription(DbHelper dbHelp, DateTime onDate, int bedID)
    {
        string sql = "SELECT r.ReserveFor, r.SoID, ri.TotalNights, ri.GuestID, ri.IsCouple, IF(IFNULL(ri.Sex, '') = '', ce.Sex, ri.Sex) AS Sex, ce.LastName, ce.FirstName, ce.SpirtualName,ri.ReservationItemID, ";
        sql += " CASE IFNULL(i.invID, 0) WHEN 0 THEN 0 ELSE 1 END AS IsInvoiceCreated";
        sql += " FROM z_reservation_items ri INNER JOIN z_reservation r ON ";
        sql += " r.ReservationID=ri.ReservationID INNER JOIN z_customer_extended ce ON ce.PartnerID=ri.GuestID INNER JOIN partners p ON p.PartnerID=ce.PartnerID AND p.PartnerActive=1";
        sql += " LEFT OUTER JOIN invoices i ON i.invForOrderNo=r.SoID";
        sql += " WHERE ri.IsCanceled = 0 AND ";
        sql += " (ri.CheckInDate <= @Sdate AND ri.CheckOutDate > @Sdate) AND r.ReservationStatus=@ReservationStatus AND ri.BedID=@BedID AND r.SoID > 0";

        //DbHelper dbHelp = new DbHelper(true);
        ReservationDescription rsvDesc = null;
        MySqlDataReader dr = null;        
        try
        {            
            dr = dbHelp.GetDataReader(sql, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("Sdate",  onDate, MyDbType.DateTime),
                    DbUtility.GetParameter("ReservationStatus", (int)StatusReservation.Processed, MyDbType.Int),
                    DbUtility.GetParameter("BedID", bedID, MyDbType.Int)
                });
            if (dr.Read())
            {
                rsvDesc = new ReservationDescription();
                rsvDesc.FirstName = BusinessUtility.GetString(dr["FirstName"]);
                rsvDesc.GuestID = BusinessUtility.GetInt(dr["GuestID"]);
                rsvDesc.IsCouple = BusinessUtility.GetBool(dr["IsCouple"]);
                rsvDesc.LastName = BusinessUtility.GetString(dr["LastName"]);
                rsvDesc.ReserveFor = BusinessUtility.GetInt(dr["ReserveFor"]);
                rsvDesc.Sex = BusinessUtility.GetString(dr["Sex"]);
                rsvDesc.SOID = BusinessUtility.GetInt(dr["SOID"]);
                rsvDesc.SpirtualName = BusinessUtility.GetString(dr["SpirtualName"]);
                rsvDesc.TotalNights = BusinessUtility.GetInt(dr["TotalNights"]);
                rsvDesc.ReservationItemID = BusinessUtility.GetInt(dr["ReservationItemID"]);
                rsvDesc.IsInvoiceCreated = BusinessUtility.GetBool(dr["IsInvoiceCreated"]);
            }
            return rsvDesc;
        }
        catch
        {
            throw;
        }
        finally
        {
            //dbHelp.CloseDatabaseConnection(dr);        
            if (dr != null && !dr.IsClosed)
            {
                dr.Close();
                dr.Dispose();
            }
        }
    }

    /// <summary>
    /// Assuming that all guest in reservation have been reserved for same course
    /// </summary>
    /// <param name="dbHelp"></param>
    /// <param name="reservationItemID"></param>
    /// <returns></returns>
    private string GetCourse(DbHelper dbHelp, int soID)
    {
        string sql = "SELECT pd.prdName FROM products p INNER JOIN prddescriptions pd ON pd.id=p.productID AND pd.descLang='en'";
        sql += " INNER JOIN orderitems oi ON oi.ordProductID=p.ProductID";
        sql += string.Format(" WHERE p.prdIsActive=1 AND p.prdType={0} AND oi.ordID=@OrderID", (int)StatusProductType.CourseProduct);
        sql += " LIMIT 1";
        try
        {
            object val = dbHelp.GetValue(sql, CommandType.Text, new MySqlParameter[] { 
                DbUtility.GetParameter("OrderID", soID, MyDbType.Int)
            });
            return BusinessUtility.GetString(val);
        }
        catch 
        {            
            throw;
        }        
    }
}

//private DataTable GetDataBedsToReserve(int buildingid, int roomid, int bedid, string amenities, DateTime[] datesToBind, DateTime availOn)
//    {
//        string pAmenity = string.Empty;
//        int countAmt = 0;
//        if (!string.IsNullOrEmpty(amenities))
//        {
//            pAmenity = StringUtil.GetCleneJoinedString(",", amenities);
//            countAmt = pAmenity.Split(',').Length;
//        }

//        DbHelper dbHelp = new DbHelper(true);
        
//        string sql = "SELECT v.BedID, v.BedTitle, v.BuildingID,  " + Globals.GetFieldName("v.Building", Globals.CurrentAppLanguageCode) + " AS BuildingTitle, ";
//        sql += " v.RoomID,  " + Globals.GetFieldName("v.Room", Globals.CurrentAppLanguageCode) + " AS RoomTitle, v.SeqBuilding,";
//        sql += " v.SeqRoom, v.SeqBed, '' AS BuildingAndRoomDesc";
//        //string.Format(@"<span class=""{0}"" room-id={1}>{2} - {3}</span>", "jqgrid_custom_cell room_display", r["RoomID"], r["BuildingTitle"], r["RoomTitle"]);
//        List<string> lstDynamicColumns = new List<string>();
//        foreach (var dat in datesToBind)
//        {
//            //lstDynamicColumns.Add(string.Format("STR_TO_DATE('{0:yyyy-MM-dd HH:mm:ss}', '%Y-%m-%d %H:%i:%s') AS 'C_{0:yyyyMMdd}'", dat));
//            lstDynamicColumns.Add(string.Format("'{0:yyyy-MM-dd HH:mm:ss}' AS 'C_{0:yyyyMMdd}'", dat));
//        }
//        if (lstDynamicColumns.Count > 0)
//        {
//            sql += "," + string.Join(",", lstDynamicColumns.ToArray());
//        }        
//        sql += " FROM vw_beds v ";
//        if (countAmt > 0)
//        {
//            sql += " LEFT OUTER JOIN z_product_attributes a ON a.ProductID = v.BedID";
//        }
//        sql += " WHERE 1=1";
//        List<MySqlParameter> p = new List<MySqlParameter>();

//        if (bedid > 0)
//        {
//            sql += " AND v.BedID=@BedID";
//            p.Add(DbUtility.GetParameter("BedID", bedid, MyDbType.Int));
//        }
//        else if (roomid > 0)
//        {
//            sql += " AND v.RoomID=@RoomID";
//            p.Add(DbUtility.GetParameter("RoomID", roomid, MyDbType.Int));
//        }
//        else if (buildingid > 0)
//        {
//            sql += " AND v.BuildingID=@BuildingID";
//            p.Add(DbUtility.GetParameter("BuildingID", buildingid, MyDbType.Int));
//        }


//        if (countAmt > 0)
//        {
//            sql += string.Format(" AND a.AttributeID IN ({0})", pAmenity);
//            sql += " GROUP BY a.ProductID";
//            sql += string.Format(" HAVING COUNT(a.ProductID) = {0}", countAmt);
//            //sql += string.Format(" AND v.BedID IN (SELECT ProductID FROM z_product_attributes WHERE AttributeID IN ({0}))", pAmenity);
//        }

//        sql += " ORDER BY v.SeqBuilding, v.SeqRoom, v.SeqBed";
//        MySqlDataReader dr = null;
//        try
//        {
//            dbHelp.OpenDatabaseConnection();
//            DataTable dtData = dbHelp.GetDataTable(sql, CommandType.Text, p.ToArray());
//            List<DataRow> lstRowsToRemove = new List<DataRow>();
//            Dictionary<string, string> divAttr;
//            ReservationDateInfo dInfo;
//            string innerHtml;
//            DateTime startTime = DateTime.Now;
//            DateTime endTime = DateTime.Now;
//            int soidFlag = 0;
//            TotalSummary ts;
//            foreach (DataRow r in dtData.Rows)
//            {
//                r["BuildingAndRoomDesc"] = string.Format(@"<span class=""{0}"" room-id={1}>{2} - {3}</span>", "jqgrid_custom_cell room_display", r["RoomID"], r["BuildingTitle"], r["RoomTitle"]);
//                for (int i = 10; i < dtData.Columns.Count; i++)
//                {
//                    innerHtml = string.Empty;
//                    divAttr = new Dictionary<string, string>();
//                    dInfo = new ReservationDateInfo();
//                    dInfo.CellDateValue = BusinessUtility.GetDateTime(r[i].ToString(), "yyyy-MM-dd HH:mm:ss");
//                    dInfo.CellID = string.Format("{0}_{1}", r["BedID"], string.Format("{0:yyyyMMdd}", dInfo.CellDateValue));
//                    dInfo.CellCssClass += "td_hack";
//                    dInfo.RowID = BusinessUtility.GetString(r["BedID"]);
//                    dInfo.CellIndex = i;
//                    dInfo.CellDateValueString = string.Format("{0:yyyyMMdd}", dInfo.CellDateValue);
//                    this.GetDateInfo(dbHelp, dInfo);

//                    //if (dInfo.CellSoID > 0 && dInfo.CellSoID != soidFlag)
//                    //{
//                    //    ts = CalculationHelper.GetOrderTotal(dbHelp, dInfo.CellSoID);
//                    //    dInfo.IsPaid = ts.OutstandingAmount == 0;
//                    //}

//                    divAttr["id"] = dInfo.CellID;                    
//                    divAttr["row_id"] = dInfo.RowID;
//                    divAttr["cell_idx"] = dInfo.CellIndex.ToString();
//                    divAttr["cell_date_val"] = dInfo.CellDateValueString;
                   
//                    if (dInfo.IsCellReserved)
//                    {
//                        divAttr["isReserved"] = dInfo.IsCellReserved.ToString().ToLower();

//                        switch ((StatusGuestType)dInfo.CellGuestType)
//                        {
//                            case StatusGuestType.Staff:
//                                dInfo.CellCssClass += " cell_reserved_staff";                                
//                                break;
//                            case StatusGuestType.Guest:
//                            case StatusGuestType.CourseParticipants:
//                                if (dInfo.GuestSex.ToUpper() == "M" && !dInfo.IsReservedForCouple)
//                                {
//                                    dInfo.CellCssClass += " cell_reserved_guest_m";                                    
//                                }
//                                else if (dInfo.GuestSex.ToUpper() == "F" && !dInfo.IsReservedForCouple)
//                                {
//                                    dInfo.CellCssClass += " cell_reserved_guest_f";                                    
//                                }
//                                else if (dInfo.IsReservedForCouple)
//                                {
//                                    dInfo.CellCssClass += " cell_reserved_guest_c";                                    
//                                }
//                                break;
//                            case StatusGuestType.SpecialGuest:
//                                dInfo.CellCssClass += " cell_reserved_spguest";                               
//                                break;                            
//                        }

//                        if (dInfo.CellSoID > 0 && dInfo.IsPaid)
//                        {
//                             dInfo.CellCssClass += " unpaid";        
//                        }

//                        if (dInfo.CellSoID > 0 && dInfo.IsInvoiced)
//                        {
//                            dInfo.CellCssClass += " invoiced";
//                        }
//                        //divAttr["soid"] = desc.SOID.ToString();
//                        if (dInfo.CellSoID > 0)
//                        {
//                            divAttr["soid"] = dInfo.CellSoID.ToString();
//                            innerHtml += string.Format(@"<div class=""desc_dialog"" style=""display:none;"" title=""{0}""></div>", dInfo.CellSoID);
//                        }
//                        divAttr["isBlackOut"] = dInfo.IsBlackout.ToString().ToLower();
                                                                        
//                        //Set Blackout as hidden
//                        if (dInfo.IsBlackout)
//                        {                            
//                            divAttr["blackOutReason"] = dInfo.CellBlackoutReason;                            
//                        }
//                        if (dInfo.ReservationItemID > 0)
//                        {
//                            divAttr["res_item_id"] = dInfo.ReservationItemID.ToString();
//                        }
//                        innerHtml += dInfo.GuestNameSubString;                        
//                    }
//                    else
//                    {
//                        //Set Blackout as visible
//                        if (dInfo.IsBlackout)
//                        {
//                            dInfo.CellCssClass += " cell_black_out";
//                            divAttr["blackOutReason"] = dInfo.CellBlackoutReason;
//                            innerHtml += dInfo.GuestNameSubString;                              
//                        }                        
//                    }

//                    divAttr["class"] = dInfo.CellCssClass;
//                    innerHtml = this.GetDivHtml(divAttr, innerHtml);
//                    r[i] = innerHtml;                   
//                }                
//            }
//            endTime = DateTime.Now;
//            var t = startTime;
//            return dtData;
//        }
//        catch
//        {

//            throw;
//        }
//        finally
//        {
//            dbHelp.CloseDatabaseConnection(dr);
//        }
//    }

//private void GetDateInfo(DbHelper dbHelp, ReservationDateInfo info)
//    {
//        if (info.CellDateValue == DateTime.MinValue)
//        {
//            return;
//        }

//        string sqlBlackOutReason = "SELECT st." + Globals.GetFieldName("Reason", Globals.CurrentAppLanguageCode) + " AS ReasonDesc FROM z_product_blackouts pb ";
//        sqlBlackOutReason += " INNER JOIN z_blackout_reasons st ON st.ID = pb.Reason ";
//        sqlBlackOutReason += " WHERE pb.ProductID = @ProductID AND @DateValidate BETWEEN pb.StartDate AND pb.EndDate LIMIT 1";

//        string sqlDesc = "SELECT r.ReserveFor, r.SoID, ri.TotalNights, ri.GuestID, ri.IsCouple, ce.Sex, ce.LastName, ce.FirstName, ce.SpirtualName,ri.ReservationItemID, ";
//        sqlDesc += " CASE IFNULL(i.invID, 0) WHEN 0 THEN 0 ELSE 1 END AS IsInvoiceCreated";
//        sqlDesc += " FROM z_reservation_items ri INNER JOIN z_reservation r ON ";
//        sqlDesc += " r.ReservationID=ri.ReservationID INNER JOIN z_customer_extended ce ON ce.PartnerID=ri.GuestID INNER JOIN partners p ON p.PartnerID=ce.PartnerID AND p.PartnerActive=1";
//        sqlDesc += " LEFT OUTER JOIN invoices i ON i.invForOrderNo=r.SoID";
//        sqlDesc += " WHERE ri.IsCanceled = 0 AND ";
//        sqlDesc += " (ri.CheckInDate <= @Sdate AND ri.CheckOutDate > @Sdate) AND r.ReservationStatus=@ReservationStatus AND ri.BedID=@BedID AND r.SoID > 0";

//        try
//        {
//            object val = dbHelp.GetValue(sqlBlackOutReason, CommandType.Text, new MySqlParameter[] {                     
//                    DbUtility.GetParameter("ProductID", info.RowID, MyDbType.Int),
//                    DbUtility.GetParameter("DateValidate", info.CellDateValue, MyDbType.DateTime)
//                });
//            if (val != DBNull.Value && val != null)
//            {
//                info.CellBlackoutReason = BusinessUtility.GetString(val);
//                info.IsBlackout = true;
//            }

//            info.CellBlackoutReason = info.CellBlackoutReason.Length > 8 ? info.CellBlackoutReason.Substring(0, 8) : info.CellBlackoutReason;
           
//            using (MySqlDataReader dr =  dbHelp.GetDataReader(sqlDesc, CommandType.Text, new MySqlParameter[] { 
//                    DbUtility.GetParameter("Sdate", info.CellDateValue, MyDbType.DateTime),
//                    DbUtility.GetParameter("ReservationStatus", (int)StatusReservation.Processed, MyDbType.Int),
//                    DbUtility.GetParameter("BedID", info.RowID, MyDbType.Int)
//                }))
//            {
//                string fName = string.Empty, lName = string.Empty, sName = string.Empty;
//                if (dr.Read())
//                {
//                    info.IsCellReserved = true;
//                    info.CellGuestType = BusinessUtility.GetInt(dr["ReserveFor"]);
//                    info.CellGuestID = BusinessUtility.GetInt(dr["GuestID"]);
//                    fName = BusinessUtility.GetString(dr["FirstName"]);
//                    lName = BusinessUtility.GetString(dr["LastName"]);
//                    sName = BusinessUtility.GetString(dr["SpirtualName"]);
//                    info.CellSoID = BusinessUtility.GetInt(dr["SOID"]);
//                    info.ReservationItemID = BusinessUtility.GetInt(dr["ReservationItemID"]);
//                    info.IsInvoiced = BusinessUtility.GetBool(dr["IsInvoiceCreated"]);
//                    info.IsReservedForCouple = BusinessUtility.GetBool(dr["IsCouple"]);
//                    info.GuestSex = BusinessUtility.GetString(dr["Sex"]);
//                }
//                dr.Close();
//                dr.Dispose();
                
//                if (!string.IsNullOrEmpty(sName))
//                {
//                    info.GuestNameSubString = sName.Length > 8 ? sName.Substring(0, 6) + ".." : sName;
//                }
//                else if (!string.IsNullOrEmpty(fName))
//                {
//                    info.GuestNameSubString = fName.Length > 8 ? fName.Substring(0, 6) + ".." : fName;
//                }
//                else if (!string.IsNullOrEmpty(lName))
//                {
//                    info.GuestNameSubString = lName.Length > 8 ? lName.Substring(0, 6) + ".." : lName;
//                }
//            }
//        }
//        catch
//        {

//            throw;
//        }
//        finally
//        { 
//        }
//    }

//    private class ReservationDateInfo
//    {
//        public string CellID { get; set; }
//        public string CellCssClass { get; set; }
//        public string RowID { get; set; }
//        public int CellIndex { get; set; }
//        public DateTime CellDateValue { get; set; }
//        public string CellDateValueString { get; set; }        
//        public string CellBlackoutReason { get; set; }
//        public int CellGuestType { get; set; }
//        public string GuestNameSubString { get; set; }
//        public string GuestSex { get; set; }
//        public int CellSoID { get; set; }       
//        public int ReservationItemID { get; set; }
//        public int CellGuestID { get; set; }

//        public bool IsInvoiced { get; set; }
//        public bool IsPaid { get; set; }
//        public bool IsBlackout { get; set; }
//        public bool IsReservedForCouple { get; set; }
//        public bool IsCellReserved { get; set; }

//        public ReservationDateInfo()
//        {
//            this.CellBlackoutReason = string.Empty;
//            this.CellCssClass = string.Empty;
//            this.CellDateValue = DateTime.MinValue;
//            this.CellDateValueString = string.Empty;
//            this.CellGuestType = 0;
//            this.CellID = string.Empty;
//            this.CellIndex = -1;
//            this.CellSoID = 0;
//            this.GuestNameSubString = string.Empty;
//            this.IsBlackout = false;
//            this.IsCellReserved = false;
//            this.IsInvoiced = false;
//            this.IsPaid = false;
//            this.ReservationItemID = 0;
//            this.RowID = string.Empty;
//            this.IsReservedForCouple = false;
//        }
//    }
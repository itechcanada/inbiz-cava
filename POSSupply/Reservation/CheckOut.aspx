﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="CheckOut.aspx.cs" Inherits="Reservation_CheckOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script src="../lib/scripts/json2.js" type="text/javascript"></script>
    <script type="text/javascript">
        var srcFilter = {};
    </script>
    <script type="text/javascript">
        function setGuestInformation() {
            $("#<%=txtFirstName.ClientID %>").val(_objClientGuest.ExtendedProperties.FirstName);
            $("#<%=txtLastName.ClientID %>").val(_objClientGuest.ExtendedProperties.LastName);
            $("#<%=txtSpritualName.ClientID %>").val(_objClientGuest.ExtendedProperties.SpirtualName);
            $('input:radio', $("#<%=rblSex.ClientID %>")).each(function () {
                if ($(this).val() == _objClientGuest.ExtendedProperties.Sex) {                    
                    this.checked = true;
                }
                else {
                    this.checked = false;
                }
            });
            var dt = new Date(+_objClientGuest.ExtendedProperties.DOB.replace(/\/Date\((-?\d+)\)\//gi, "$1"));
            //$("#<%=txtDOB.ClientID %>").val(("0" + (dt.getMonth() + 1)).slice(-2) + "/" + ("0" + dt.getDate()).slice(-2) + "/" + dt.getFullYear());
            $("#<%=txtDOB.ClientID %>").datepicker("setDate", new Date(+_objClientGuest.ExtendedProperties.DOB.replace(/\/Date\((-?\d+)\)\//gi, "$1")));
            $("#<%=txtTelephone.ClientID %>").val(_objClientGuest.PartnerPhone);
            $("#<%=txtEmailAddress.ClientID %>").val(_objClientGuest.PartnerEmail);
            $("#<%=txtAddressLine1.ClientID %>").val(_objClientGuestAddress.AddressLine1);
            $("#<%=txtAddressLine2.ClientID %>").val(_objClientGuestAddress.AddressLine2);
            $("#<%=txtCity.ClientID %>").val(_objClientGuestAddress.AddressCity);
            $("#<%=txtState.ClientID %>").val(_objClientGuestAddress.AddressState);
            $("#<%=txtCountry.ClientID %>").val(_objClientGuestAddress.AddressCountry);
            $("#<%=txtPostalCode.ClientID %>").val(_objClientGuestAddress.AddressPostalCode);
            $("#<%=txtEmName.ClientID %>").val(_objClientGuest.ExtendedProperties.EmergencyContactName);
            $("#<%=txtEmTelephone.ClientID %>").val(_objClientGuest.ExtendedProperties.EmergencyTelephone);
            $("#<%=txtAlterTelephone.ClientID %>").val(_objClientGuest.ExtendedProperties.EmergencyAltTelephone);
            $("#<%=txtEmRelationship.ClientID %>").val(_objClientGuest.ExtendedProperties.EmergencyRelationship);
            $('input:radio', $("#<%=rblMember.ClientID %>")).each(function () {
                if ($(this).val() == _objClientGuest.ExtendedProperties.IsMember) {
                    this.checked = true;
                }
                else {
                    this.checked = false;
                }
            });

            if (_objClientGuest.ExtendedProperties.ReceiveNewsLetters) {
                $("#<%=chkNewsLetter.ClientID %>").attr('checked', 'checked');
            }
            else {
                $("#<%=chkNewsLetter.ClientID %>").attr('checked', '');
            }
            $("#<%=hdnSelectedGuestID.ClientID %>").val(_objClientGuest.PartnerID);
            $("#<%=txtCustomerNote.ClientID %>").val(_objClientGuest.PartnerNote);
            $("#<%=txtFirstName.ClientID %>").focus();
        }
        function fillSearchFilterData() {
            srcFilter.fName = $("#<%=txtFirstName.ClientID %>").val();
            srcFilter.lName = $("#<%=txtLastName.ClientID %>").val();
            srcFilter.email = $("#<%=txtEmailAddress.ClientID %>").val();
            srcFilter.phone = $("#<%=txtTelephone.ClientID %>").val();
            srcFilter.rid = '<%=Request.QueryString["rid"]%>';
        }

        function restCustomerForm() {
            $("#<%=txtFirstName.ClientID %>").val("");
            $("#<%=txtLastName.ClientID %>").val("");
            $("#<%=txtSpritualName.ClientID %>").val("");
            $("#<%=txtDOB.ClientID %>").val("");
            $("#<%=txtTelephone.ClientID %>").val("");
            $("#<%=txtEmailAddress.ClientID %>").val("");
            $("#<%=txtAddressLine1.ClientID %>").val("");
            $("#<%=txtAddressLine2.ClientID %>").val("");
            $("#<%=txtCity.ClientID %>").val("");
            $("#<%=txtState.ClientID %>").val("");
            $("#<%=txtCountry.ClientID %>").val("");
            $("#<%=txtPostalCode.ClientID %>").val("");
            $("#<%=txtEmName.ClientID %>").val("");
            $("#<%=txtEmTelephone.ClientID %>").val("");
            $("#<%=txtAlterTelephone.ClientID %>").val("");
            $("#<%=txtAlterTelephone.ClientID %>").val("");
            $("#<%=txtEmRelationship.ClientID %>").val("");
            $("#<%=txtCustomerNote.ClientID %>").val("");
            $("#<%=hdnSelectedGuestID.ClientID %>").val("");
            $("#<%=txtFirstName.ClientID %>").focus();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <asp:Literal ID="ltTitle" Text="Checkout" runat="server" />
        </b>
    </div>
    <div style="float: left;">
        <div id="divSearchButton" runat="server">             
            <input id="btnSearchCustomer" type="button" value="<%=Resources.Resource.lblSearchCustomer%>" />
            <input id="btnRestSearch" type="button" value="<%=Resources.Resource.Reset%>" /></div>
    </div>
    <div style="float: right;">        
        <asp:Button ID="btnAddServices" Text="<%$Resources:Resource, lblAddServicesAndDiscount%>" runat="server" OnClick="btnAddServices_Click"/>
            <%--<asp:Button ID="btnGenerateInvoice" Text="Generate Invoice" runat="server" OnClick="btnGenerateInvoice_Click"
                meta:resourcekey="btnGenerateInvoiceResource1" />--%>                
            <asp:Button ID="btnReceivePayment" Text="<%$Resources:Resource, btnReceivePayment %>" runat="server"
                OnClick="btnReceivePayment_Click" />
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <asp:HiddenField ID="hdnSelectedGuestID" runat="server" />
    <h3>
        <asp:Literal ID="ltSectionTitle" Text="Personal Information - Guest 1" runat="server"
            meta:resourcekey="ltSectionTitleResource1" />
    </h3>
    <asp:Panel ID="pnlCustomerInfo" runat="server" DefaultButton="btnNext" meta:resourcekey="pnlCustomerInfoResource1">
        <div style="padding: 2px; margin: 2px; background-color: #FFECB5; border: 1px solid #E5C365;">
            <table border="0" cellpadding="3" cellspacing="3">
                <tr>
                    <td style="font-weight: bold;">                        
                        <asp:Label ID="lblAccomodationType" Text="<%$Resources:Resource,lblAccommodationType %>" runat="server" meta:resourcekey="lblAccomodationTypeResource1" />:
                    </td>
                    <td>
                        
                        <asp:Literal ID="ltAccommodationType" runat="server" meta:resourcekey="ltAccommodationTypeResource1" />
                    </td>
                    <td style="font-weight: bold;">
                      
                        <asp:Label ID="lblCheckIn" Text="<%$Resources:Resource,lblCheckIn %>" runat="server" meta:resourcekey="lblCheckInResource1" />:
                    </td>
                    <td>
                        <asp:Literal ID="ltCheckIn" runat="server" meta:resourcekey="ltCheckInResource1" />
                    </td>
                    <td style="font-weight: bold;">                        
                        <asp:Label ID="lblCheckOut" Text="<%$Resources:Resource, lblCheckout %>" runat="server" meta:resourcekey="lblCheckOutResource1" />:
                    </td>
                    <td>
                        <asp:Literal ID="ltCheckOut" runat="server" meta:resourcekey="ltCheckOutResource1" />
                    </td>
                </tr>
            </table>
        </div>
        <ul class="form">
            <li>
                <div class="lbl">
                    
                    <asp:Label ID="lblFirstName" Text="<%$Resources:Resource, lblFName %>" runat="server" meta:resourcekey="lblFirstNameResource1" />*
                </div>
                <div class="input">
                    <asp:TextBox ID="txtFirstName" runat="server" MaxLength="45" CssClass="searchTrigger" onchange='_objClientGuest.FirstName=this.value;' />
                    <asp:RequiredFieldValidator ID="rfvFirstName" ControlToValidate="txtFirstName" SetFocusOnError="True"
                        runat="server" ErrorMessage="*" meta:resourcekey="rfvFirstNameResource1"></asp:RequiredFieldValidator>
                </div>
                <div class="lbl">
                    <asp:Label ID="lblLastName" Text="<%$Resources:Resource, lblLName %>" runat="server" meta:resourcekey="lblLastNameResource1" />*
                </div>
                <div class="input">
                    <asp:TextBox ID="txtLastName" CssClass="searchTrigger" runat="server" MaxLength="45" onchange='_objClientGuest.LastName=this.value;'
                        meta:resourcekey="txtLastNameResource1" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ControlToValidate="txtLastName"
                        SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator12Resource1"></asp:RequiredFieldValidator>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                
                    <asp:Label ID="lblSpiritualName" Text="<%$Resources:Resource, lblSpiritualName %>" runat="server" meta:resourcekey="lblSpiritualNameResource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtSpritualName" runat="server" MaxLength="45" meta:resourcekey="txtSpritualNameResource1" />
                </div>
                <div class="lbl">                    
                    <asp:Label ID="lblSex" Text="<%$Resources:Resource, lblSex %>" runat="server" meta:resourcekey="lblSexResource1" />*
                </div>
                <div class="input" style="width: 130px;">
                    
                    <asp:RadioButtonList ID="rblSex" runat="server" RepeatDirection="Horizontal" meta:resourcekey="rblSexResource1">
                        <asp:ListItem Value="M" Selected="True" Text="<%$Resources:Resource, lblMale%>"></asp:ListItem>
                        <asp:ListItem Value="F" Text="<%$Resources:Resource, lblFemale%>"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="lbl" style="text-align: left;">
                    
                    <asp:CheckBox ID="chkCouple" Text="<%$Resources:Resource,lblIsCouple %>" runat="server" meta:resourcekey="chkCoupleResource1" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li id="liRelationToPrimaryGuest" runat="server">
                <div class="lbl">                    
                    <asp:Label ID="lblRelationshipToGuest1" Text="Relationship to Guest1" runat="server"
                        meta:resourcekey="lblRelationshipToGuest1Resource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtRelationship" runat="server" MaxLength="45" meta:resourcekey="txtRelationshipResource1" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ControlToValidate="txtRelationship"
                        SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator10Resource1"
                        Visible="false"></asp:RequiredFieldValidator>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">                    
                    <asp:Label ID="lblDob" Text="<%$Resources:Resource,lblDateOfBirth %>" runat="server" meta:resourcekey="lblDobResource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtDOB" CssClass="dob_datepicker" runat="server" meta:resourcekey="txtDOBResource1" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    
                    <asp:Label ID="lblTelephoneNumber" Text="<%$Resources:Resource,lblTelephone %>" runat="server" meta:resourcekey="lblTelephoneNumberResource1" />*
                </div>
                <div class="input">
                    <asp:TextBox ID="txtTelephone" CssClass="searchTrigger" runat="server" MaxLength="15" onchange='_objClientGuest.TelephoneNo=this.value;'
                        meta:resourcekey="txtTelephoneResource1" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtTelephone"
                        SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                </div>
                <div class="lbl">                    
                    <asp:Label ID="lblEmailAddress" Text="<%$Resources:Resource, Email%>" runat="server" meta:resourcekey="lblEmailAddressResource1" />*
                </div>
                <div class="input" style="width:350px;">
                    <asp:TextBox ID="txtEmailAddress" CssClass="searchTrigger" runat="server" MaxLength="45" onchange='_objClientGuest.EmailAddress=this.value;'
                        meta:resourcekey="txtEmailAddressResource1" />
                    <asp:Button ID="btnNoEmail" Text="<%$Resources:Resource, lblNoEmail%>" runat="server" CausesValidation="false" />
                    <script type="text/javascript">
                        $("#<%=btnNoEmail.ClientID%>").click(function () {
                            $("#<%=txtEmailAddress.ClientID%>").val('<%=ConfigurationManager.AppSettings["DuplicatEmailAddress"]%>');
                            return false;
                        });
                    </script>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtEmailAddress"
                        SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="RequiredFieldValidator3Resource1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revalSecEmail" runat="server" ControlToValidate="txtEmailAddress"
                        ErrorMessage="<%$ Resources:Resource, revalCMSecEmail %>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        meta:resourcekey="revalSecEmailResource1"></asp:RegularExpressionValidator>
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
        <h3>
            <asp:Literal ID="ltAddress" Text="Address" runat="server" meta:resourcekey="ltAddressResource1" />
        </h3>
        <ul class="form">
            <li>
                <div class="lbl">                    
                    <asp:Label ID="lblAddressLine1" Text="<%$Resources:Resource, lblAddressLine1%>" runat="server" meta:resourcekey="lblAddressLine1Resource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtAddressLine1" runat="server" MaxLength="45" meta:resourcekey="txtAddressLine1Resource1" />
                </div>
                <div class="lbl">                    
                    <asp:Label ID="lblAddressLine2" Text="<%$Resources:Resource,lblAddressLine2 %>" runat="server" meta:resourcekey="lblAddressLine2Resource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtAddressLine2" runat="server" MaxLength="45" meta:resourcekey="txtAddressLine2Resource1" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">                    
                    <asp:Label ID="lblCity" Text="<%$Resources:Resource, lblCity%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCity" runat="server" MaxLength="45" meta:resourcekey="txtCityResource1" />
                </div>
                <div class="lbl">                    
                    <asp:Label ID="lblState" Text="<%$Resources:Resource, lblAddressState %>" runat="server" meta:resourcekey="lblStateResource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtState" runat="server" MaxLength="45" meta:resourcekey="txtStateResource1" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">                    
                    <asp:Label ID="lblPostalCode" Text="<%$Resources:Resource, lblPostalCode%>" runat="server" meta:resourcekey="lblPostalCodeResource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtPostalCode" runat="server" MaxLength="45" meta:resourcekey="txtPostalCodeResource1" />
                </div>
                <div class="lbl">                    
                    <asp:Label ID="lblCountry" Text="<%$Resources:Resource,lblCountry %>" runat="server" meta:resourcekey="lblCountryResource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCountry" runat="server" MaxLength="45" meta:resourcekey="txtCountryResource1" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
        <h3>            
            <asp:Literal ID="ltEmergencyContact" Text="<%$Resources:Resource, lblEmergencyContact%>" runat="server" meta:resourcekey="ltEmergencyContactResource1" />
        </h3>
        <ul class="form">
            <li>
                <div class="lbl">                    
                    <asp:Label ID="lblName" Text="<%$Resources:Resource, lblName%>" runat="server" meta:resourcekey="lblNameResource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtEmName" runat="server" MaxLength="45" meta:resourcekey="txtEmNameResource1" />
                </div>
                <div class="lbl">                    
                    <asp:Label ID="lblRelationship" Text="<%$Resources:Resource,lblRelationship %>" runat="server" meta:resourcekey="lblRelationshipResource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtEmRelationship" runat="server" MaxLength="45" meta:resourcekey="txtEmRelationshipResource1" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">                                        
                    <asp:Label ID="lblTelephoneNo" Text="<%$Resources:Resource, lblTelephone%>" runat="server" meta:resourcekey="lblTelephoneNoResource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtEmTelephone" runat="server" MaxLength="15" meta:resourcekey="txtEmTelephoneResource1" />
                </div>
                <div class="lbl">                    
                    <asp:Label ID="lblAltPhoneNo" Text="<%$Resources:Resource,lblAlternatePhoneNo %>" runat="server" meta:resourcekey="lblAltPhoneNoResource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtAlterTelephone" runat="server" MaxLength="45" meta:resourcekey="txtAlterTelephoneResource1" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
        <h3>            
            <asp:Literal ID="ltOtherInfo" Text="<%$Resources:Resource, lblOtherInfo %>" runat="server" meta:resourcekey="ltOtherInfoResource1" />
        </h3>
        <ul class="form">
            <li>
                <div class="lbl" style="width: 300px">                    
                    <asp:Label ID="lblAreYouMemberOfSivananda" Text="<%$Resources:Resource,msgAreYouaMemberofSivanandaYogaVedantaCenter %>"
                        runat="server" meta:resourcekey="lblAreYouMemberOfSivanandaResource1" />
                </div>
                <div class="input" style="width: 400px">
                    <asp:RadioButtonList ID="rblMember" runat="server" RepeatDirection="Horizontal" meta:resourcekey="rblMemberResource1">
                    </asp:RadioButtonList>
                </div>
                <div class="clearBoth">
                </div>
                <div class="lbl">                    
                    <asp:Label ID="lblMemberID" Text="<%$Resources:Resource, lblMemberID%>" runat="server" meta:resourcekey="lblMemberIDResource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtMembership" runat="server" meta:resourcekey="txtMembershipResource1" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li id="liReservationNote" runat="server">
                <div class="lbl">                    
                    <asp:Label ID="lblReservationNotes" Text="<%$Resources:Resource, lblReservationNotes %>" runat="server" meta:resourcekey="lblReservationNotesResource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtReservationNote" runat="server" TextMode="MultiLine" Width="250px"
                        Height="100px" meta:resourcekey="txtReservationNoteResource1" />
                </div>
                <div class="lbl">                    
                    <asp:Label ID="lblCustomerNotes" Text="<%$Resources:Resource, lblCustomerNotes%>" runat="server" meta:resourcekey="lblCustomerNotesResource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCustomerNote" runat="server" TextMode="MultiLine" Width="250px"
                        Height="100px" meta:resourcekey="txtCustomerNoteResource1" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                </div>
                <div>                    
                    <asp:CheckBox ID="chkNewsLetter" Font-Bold="True" Text="<%$Resources:Resource, lblIWoulLikeToReceiveNewsLetter%>"
                        runat="server" meta:resourcekey="chkNewsLetterResource1" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
        <div style="text-align: right; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;">            
            <asp:Button ID="btnNext" Text="<%$Resources:Resource,lblNext%>" runat="server" OnClick="btnNext_Click" meta:resourcekey="btnNextResource1" />            
            <asp:Button ID="btnReserve" Text="<%$Resources:Resource, lblReserve %>" runat="server" 
                onclick="btnReserve_Click" />
            <asp:Button ID="btnReserveRest" Text="<%$Resources:Resource,lblReserveRestForMainGuest%>" runat="server"
                OnClick="btnReserveRest_Click" Visible="false" CausesValidation="false" OnClientClick="return confirm('You are going to reserve all remaining accommodation for main Guest!');" />
            
            <asp:Button ID="btnCancel" Text="<%$ Resources:Resource, btnCancel %>" runat="server"
                OnClick="btnCancel_Click" CausesValidation="False" meta:resourcekey="btnCancelResource1" />
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
    <asp:BulletedList ID="blstNav" DataSourceID="sdsNavigation" CssClass="left_nav" DisplayMode="HyperLink"
        DataTextField="Text" DataValueField="Value" runat="server">
    </asp:BulletedList>
    <asp:SqlDataSource ID="sdsNavigation" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
        ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" runat="Server">
    <asp:Panel ID="pnlPopup" runat="server">
        <script type="text/javascript">
            //dialog to process reservation
            // string url = ResolveUrl(string.Format("~/Common/mdSearchGuest.aspx"))
            $("#btnSearchCustomer").click(function () {
                $this = $(this);
                $this.button({ disabled: true });
                srcFilter.gtype = '<%=(int)this.ReserveFor%>';
                srcFilter.opt = 'pick_guest';
                fillSearchFilterData();

                $.ajax({
                    type: "POST",
                    url: "ajax.aspx/SearchGuests",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(srcFilter),
                    success: function (data) {
                        if (data != null && data != undefined && data.d.length == 1) {
                            setCurrentGuest(data.d[0]);
                        }
                        else if (data.d.length > 1) {
                            $dialog = jQuery.FrameDialog.create({ url: '<%= ResolveUrl(string.Format("~/Common/mdSearchGuest.aspx"))%>?' + decodeURIComponent($.param(srcFilter)),
                                title: "Select Guest",
                                loadingClass: "loading-image",
                                modal: true,
                                width: 800,
                                height: 510,
                                autoOpen: true,
                                close: function (ev, ui) {
                                    $(this).dialog('destroy');
                                }
                            });
                            $dialog.dialog('open');
                        }
                        else {
                            alert("Result(s) not found!");
                        }
                        $this.button({ disabled: false });
                    },
                    error: function (request, status, errorThrown) {
                        alert(status);
                        $this.button({ disabled: false });
                    }
                });

                /* */
            });

            function setCurrentGuest(guestid) {
                var gid = {};
                gid.guestid = guestid;
                $.ajax({
                    type: "POST",
                    url: "ajax.aspx/GetGuest",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(gid),
                    success: function (data) {
                        _objClientGuest = data.d[0];
                        _objClientGuestAddress = data.d[1];
                        setGuestInformation();
                    },
                    error: function (request, status, errorThrown) {
                        alert(status);
                    }
                });
            }

            $("#btnRestSearch").click(function () {
                restCustomerForm();
            });

            $(".searchTrigger").keypress(function (e) {
                if (e.keyCode == 13) {
                    $("#btnSearchCustomer").trigger("click");
                }
            }); 
            
            function goToAddNewGuestPage(pType, gType, riID) {
                par = {};
                par.pType = pType;
                par.gType = gType;                
                location.href = '<%=ResolveUrl("~/Partner/CustomerEdit.aspx")%>' + "?" + $.param(par);
                return false;
            }               
        </script>
    </asp:Panel>
</asp:Content>

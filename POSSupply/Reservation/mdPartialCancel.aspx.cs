﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;


public partial class Reservation_mdPartialCancel : BasePage
{
    string _priceFormat = @"<span style=""font-size:14px; font-weight:bold;"">{0}</span> {1:F}";
    //Reservation _rsv = new Reservation();
    //ReservationItems _rsvItem = new ReservationItems();
    //Bed _newBed = new Bed();
    //Bed _oldBed = new Bed();
    //Orders _ord = new Orders();
    //OrderItems _oi = new OrderItems();
    //ProductDiscount _prdDiscount = new ProductDiscount();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (this.ReservationItemID > 0 && this.OldBedID > 0)
            {
                FillPartialCancelPreviewForm();
            }
        }
    }

    private void FillPartialCancelPreviewForm()
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            Reservation rsv = new Reservation();
            ReservationItems rsvItem = new ReservationItems();
            Bed newBed = new Bed();
            Bed oldBed = new Bed();
            Orders ord = new Orders();
            OrderItems oi = new OrderItems();
            ProductDiscount prdDiscount = new ProductDiscount();

            rsvItem.PopulateObject(dbHelp, this.ReservationItemID);
            hdnReservationID.Value = rsvItem.ReservationID.ToString();
            rsv.PopulateObject(dbHelp, this.ReservationID);
            hdnOrderID.Value = rsv.SoID.ToString();
            oi.PopulateReservationOrderItem(dbHelp, this.ReservationItemID);
            //hdnOrderItemID.Value = _oi.OrderItemID.ToString();

            DateTime cancelFrom = BusinessUtility.GetDateTime(this.MovingFromDate.ToString(), "yyyyMMdd");
            DateTime cancelTo = BusinessUtility.GetDateTime(this.MovingToDate.ToString(), "yyyyMMdd");
            cancelTo = cancelTo.AddDays(1);


            oldBed.BedID = this.OldBedID;
            oldBed.FromDate = cancelFrom;
            oldBed.ToDate = cancelTo;
            oldBed.PopulateBedInfo(dbHelp, oldBed, Globals.CurrentAppLanguageCode);


            ltCancelAcc.Text = string.Format("<b>{0}/{1}/{2}</b><br>", oldBed.BuildingTitle, oldBed.RoomTitle, oldBed.BedTitle);
            ltCancelAcc.Text += oldBed.ToString().Replace(Environment.NewLine, "<br>");
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }

    }

    public int ReservationItemID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["rid"], out id);
            return id;
        }
    }

    public int OldBedID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["old_bed_id"], out id);
            return id;
        }
    }

    

    public int MovingFromDate
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["moving_from"], out id);
            return id;
        }
    }

    public int MovingToDate
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["moving_to"], out id);
            return id;
        }
    }

    public int ReservationID
    {
        get
        {
            int id = 0;
            int.TryParse(hdnReservationID.Value, out id);
            return id;
        }
    }

    public int OrderID
    {
        get
        {
            int id = 0;
            int.TryParse(hdnOrderID.Value, out id);
            return id;
        }
    }

    //private int OrderItemID {
    //    get
    //    {
    //        int id = 0;
    //        int.TryParse(hdnOrderItemID.Value, out id);
    //        return id;
    //    }
    //}

    protected void btnPartialCancel_Click(object sender, EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);

        Reservation _rsv = new Reservation();
        ReservationItems _rsvItem = new ReservationItems();        
        OrderItems _oi = new OrderItems();

        try
        {
            _rsvItem.PopulateObject(dbHelp, this.ReservationItemID);
            _rsv.PopulateObject(dbHelp, this.ReservationID);
            _oi.PopulateReservationOrderItem(dbHelp, this.ReservationItemID);

            DateTime movingFrom = BusinessUtility.GetDateTime(this.MovingFromDate.ToString(), "yyyyMMdd");
            DateTime movinTo = BusinessUtility.GetDateTime(this.MovingToDate.ToString(), "yyyyMMdd");
            movinTo = movinTo.AddDays(1);

            List<SplitReservationRange> spllitedData = new List<SplitReservationRange>();

            //Case complete date range of reservation item is moving
            if (movingFrom == _rsvItem.CheckInDate && movinTo == _rsvItem.CheckOutDate)
            {
                //To do to move as it is by canceling complete reservation item & move it to other bed for same duration                
            }
            else if (movingFrom > _rsvItem.CheckInDate && movinTo == _rsvItem.CheckOutDate) //Partial date range is moving in between 
            {
                //Neet to split existing reservation item
                //Convert.ToInt32((this.ToDate.Date - this.FromDate.Date).TotalDays)                
                spllitedData.Add(new SplitReservationRange { BedID = _rsvItem.BedID, CheckinDate = _rsvItem.CheckInDate, CheckoutDate = _rsvItem.CheckInDate.AddDays(Convert.ToInt32((movingFrom.Date - _rsvItem.CheckInDate.Date).TotalDays)) });
            }
            else if (movingFrom == _rsvItem.CheckInDate && movinTo < _rsvItem.CheckOutDate)
            {
                spllitedData.Add(new SplitReservationRange { BedID = _rsvItem.BedID, CheckinDate = movinTo, CheckoutDate = _rsvItem.CheckOutDate });
            }
            else if (movingFrom > _rsvItem.CheckInDate && movinTo < _rsvItem.CheckOutDate)
            {
                spllitedData.Add(new SplitReservationRange { BedID = _rsvItem.BedID, CheckinDate = _rsvItem.CheckInDate, CheckoutDate = _rsvItem.CheckInDate.AddDays(Convert.ToInt32((movingFrom.Date - _rsvItem.CheckInDate.Date).TotalDays)) });
                spllitedData.Add(new SplitReservationRange { BedID = _rsvItem.BedID, CheckinDate = movinTo, CheckoutDate = _rsvItem.CheckOutDate });
            }

            //Total before canceling & moving so that we can identify is refund / credit require or not
            TotalSummary tsBefore = CalculationHelper.GetOrderTotal(dbHelp, this.OrderID);
            hdnTotalPaidBefore.Value = tsBefore.GrandTotal.ToString();
            PartialCancelReservationItem(dbHelp, _rsvItem.ReservationID, _rsvItem.ReservationItemID, spllitedData);

            TotalSummary tsAfter = CalculationHelper.GetOrderTotal(this.OrderID);

            if (tsAfter.OutstandingAmount < 0)
            {
                //Redirect to refund page
                Response.Redirect(string.Format("~/AccountsReceivable/mdReceive.aspx?oid={0}&rcvType={1}&amtToRefund={2}", this.OrderID, "refund", (-1.00D * tsAfter.OutstandingAmount)));
            }
            else
            {
                Globals.RegisterCloseDialogScript(this, "parent.reloadGridOnScrollPos();", true);
            }
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.ACCOMMODATION_NOT_AVAILABLE)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgAccommodationNoLongerAvailable);
            }
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private void PartialCancelReservationItem(DbHelper dbHelp, int reservationID, int rsvItemID, List<SplitReservationRange> splitedDateRange)
    {
        string sqlUpdateOrderItem = "UPDATE orderitems SET ordProductUnitPrice=0, ordIsItemCanceled=1 WHERE orderItemID=@orderItemID";
        string sqlUpdateRsvItem = "UPDATE z_reservation_items SET PricePerDay=0, IsCanceled=1 WHERE ReservationItemID=@ReservationItemID";        
        try
        {
            OrderItems oldOrderItem = new OrderItems();
            ReservationItems oldItem = new ReservationItems();
            oldItem.PopulateObject(dbHelp, rsvItemID);
            oldOrderItem.PopulateReservationOrderItem(dbHelp, rsvItemID);
            Reservation rsv = new Reservation();
            rsv.PopulateObject(dbHelp, oldItem.ReservationID);


            //Insert into Reservation Item & in order Item            
            ProductDiscount prdDisc = new ProductDiscount();
            bool isAccommodationAvailable = true;
            foreach (var item in splitedDateRange)
            {
                if (!ProcessReservation.IsAccommodationAvailalbe(dbHelp, item.CheckinDate, item.CheckoutDate, item.BedID, oldItem.ReservationItemID))
                {
                    isAccommodationAvailable = false;
                    break;
                }
            }
            if (isAccommodationAvailable)
            {
                OrderItems oi;
                ReservationItems ri;
                foreach (var item in splitedDateRange)
                {
                    double price = 0;
                    int totalDays = Convert.ToInt32((item.CheckoutDate - item.CheckinDate).TotalDays);
                    if (rsv.ReserveFor == (int)StatusGuestType.Guest)
                    {
                        price = prdDisc.CalculateAccommodationPrice(dbHelp, item.BedID, item.CheckinDate, item.CheckoutDate);
                    }
                    ri = new ReservationItems();
                    ri.BedID = item.BedID;
                    ri.CheckInDate = item.CheckinDate;
                    ri.CheckOutDate = item.CheckoutDate;
                    ri.GuestID = oldItem.GuestID;
                    ri.IsCanceled = false;
                    ri.IsCouple = oldItem.IsCouple;
                    ri.PricePerDay = price;
                    ri.RelationshipToPrimaryGuest = oldItem.RelationshipToPrimaryGuest;
                    ri.ReservationID = oldItem.ReservationID;
                    ri.Sex = oldItem.Sex;
                    ri.Insert(dbHelp);

                    Bed b = new Bed();
                    b.BedID = ri.BedID;
                    b.BedPrice = ri.PricePerDay;
                    b.FromDate = ri.CheckInDate;
                    b.ToDate = ri.CheckOutDate;
                    b.PopulateBedInfo(dbHelp, b, Globals.CurrentAppLanguageCode);
                    if (oldOrderItem.OrderItemID > 0)
                    {
                        oi = new OrderItems();
                        oi.OrderItemDesc = b.ToString();
                        oi.OrdGuestID = ri.GuestID;
                        oi.OrdID = oldOrderItem.OrdID;
                        oi.OrdIsItemCanceled = false;
                        oi.OrdProductDiscount = oldOrderItem.OrdProductDiscount;
                        oi.OrdProductDiscountType = oldOrderItem.OrdProductDiscountType;
                        oi.OrdProductID = ri.BedID;
                        oi.OrdProductQty = ri.TotalNights;
                        oi.OrdProductTaxGrp = oldOrderItem.OrdProductTaxGrp;
                        oi.OrdProductUnitPrice = ri.PricePerDay;
                        oi.OrdReservationItemID = ri.ReservationItemID;
                        oi.Insert(dbHelp);
                    }
                }

                //Update old item to make it as canceld
                if (oldOrderItem.OrderItemID > 0)
                {
                    dbHelp.ExecuteNonQuery(sqlUpdateOrderItem, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("orderItemID", oldOrderItem.OrderItemID, MyDbType.Int)
                });
                }
                dbHelp.ExecuteNonQuery(sqlUpdateRsvItem, CommandType.Text, new MySqlParameter[] { 
                    DbUtility.GetParameter("ReservationItemID", rsvItemID, MyDbType.Int)
                });

                this.AddAdminFee(dbHelp);
            }
            else
            {
                throw new Exception(CustomExceptionCodes.ACCOMMODATION_NOT_AVAILABLE);
            }
        }
        catch
        {
            throw;
        }
        finally
        {
            
        }
    }

    private void AddAdminFee(DbHelper dbHelp)
    {        
        try
        {
            Orders ord = new Orders();
            OrderItems oi = new OrderItems();
            Product prd = new Product();
            ord.PopulateObject(dbHelp, this.OrderID);
            prd.PopulateAdminFeeProduct(dbHelp);
            if (prd.ProductID > 0)
            {
                oi.OrderItemDesc = prd.PrdName;
                oi.OrdGuestID = ord.OrdCustID;
                oi.OrdID = ord.OrdID;
                oi.OrdIsItemCanceled = false;
                oi.OrdProductDiscount = 0;
                oi.OrdProductDiscountType = "P";
                oi.OrdProductID = prd.ProductID;
                oi.OrdProductQty = 1;
                oi.OrdProductUnitPrice = prd.PrdEndUserSalesPrice;
                oi.OrdReservationItemID = 0;
                oi.Insert(dbHelp);
            }
        }
        finally
        {
            
        }
    }

    private class SplitReservationRange
    {
        public int BedID { get; set; }
        public DateTime CheckinDate { get; set; }
        public DateTime CheckoutDate { get; set; }
    }
}
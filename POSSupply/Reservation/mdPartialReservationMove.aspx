﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdPartialReservationMove.aspx.cs" Inherits="Reservation_mdPartialReservationMove" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <style type="text/css">
        ul.form li div.lbl{width:150px !important;}        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
     <div style="padding: 5px 10px;">
         <asp:Panel ID="pnlPartialCancel" runat="server" DefaultButton="btnMove">
             <ul class="form">
                 <li>
                     <div class="lbl">
                         <asp:Label ID="lblGuest" Text="<%$Resources:Resource, lblGuestName%>" runat="server" />
                     </div>
                     <div class="input">
                         <asp:Label ID="valGuestCancel" Text="" runat="server" />
                     </div>
                     <div class="clearBoth">
                     </div>
                 </li>
                 <li>
                     <div class="lbl">                           
                         <asp:Label ID="Label3" Text="<%$Resources:Resource, Cancel%>" runat="server" />:
                     </div>
                     <div class="input">
                         <asp:Literal ID="ltCancelAcc" Text="" runat="server" />
                     </div>
                     <div class="clearBoth">
                     </div>
                 </li>
                 <li>
                     <div class="lbl">
                         <asp:Label ID="Label7" Text="Accommodation Cost" runat="server" />
                     </div>
                     <div class="input">
                         <asp:Label ID="txtAccommodationCostCancel" Text="" runat="server" />                         
                     </div>
                     <div class="clearBoth">
                     </div>
                 </li>
                 <li>
                     <div class="lbl">
                         <asp:Label ID="Label4" Text="<%$Resources:Resource, lblAdminFee%>" runat="server" />
                     </div>
                     <div class="input">
                         <asp:TextBox ID="txtAdminFeeCancel" runat="server" MaxLength="8" />
                     </div>
                     <div class="clearBoth">
                     </div>
                 </li>
             </ul>             
             <div class="div_command">                
                 <asp:Button ID="btnPartialCancel" Text="<%$Resources:Resource, lblCancelSelectedDateReservation%>" runat="server" OnClick="btnPartialCancel_Click" />
             </div>
         </asp:Panel>
         <asp:Panel ID="pnlMove" runat="server" DefaultButton="btnMove">
             <ul class="form">
                <li>
                     <div class="lbl">
                         <asp:Label ID="Label6" Text="<%$Resources:Resource, lblGuestName%>" runat="server" />
                     </div>
                     <div class="input">
                         <asp:Label ID="valGuestMove" Text="" runat="server" />
                     </div>
                     <div class="clearBoth">
                     </div>
                 </li>
                 <li>
                     <div class="lbl">
                         <asp:Label ID="Label1" Text="<%$Resources:Resource, lblGuestMovingFrom %>" runat="server" />:
                     </div>
                     <div class="input">
                         <asp:Literal ID="ltMovingFrom" Text="" runat="server" />
                     </div>
                     <div class="clearBoth">
                     </div>
                 </li>
                 <li>
                     <div class="lbl">
                         <asp:Label ID="Label8" Text="Accommodation Cost" runat="server" />
                     </div>
                     <div class="input">
                         <asp:Label ID="txtAccommodationCostMove" Text="" runat="server" />                         
                     </div>
                     <div class="clearBoth">
                     </div>
                 </li>
                 <li>
                     <div class="lbl">
                         <asp:Label ID="Label2" Text="<%$Resources:Resource, lblGuestMovingTo %>" runat="server" />:
                     </div>
                     <div class="input">
                         <asp:Literal ID="ltMovingTo" Text="" runat="server" />
                     </div>
                     <div class="clearBoth">
                     </div>
                 </li>                 
                 <li>
                     <div class="lbl">
                         <asp:Label ID="Label5" Text="<%$Resources:Resource, lblAdminFee%>" runat="server" />
                     </div>
                     <div class="input">
                         <asp:TextBox ID="txtAdminFeeMove" runat="server" MaxLength="8" />
                     </div>
                     <div class="clearBoth">
                     </div>
                 </li>
             </ul>             
             <div class="div_command">
                 <asp:Button ID="btnMove" Text="<%$Resources:Resource, lblMove%>" runat="server" OnClick="btnMove_Click" />
             </div>
         </asp:Panel>
         <asp:Panel ID="pnlCancelWhole" runat="server">
             <ul class="form">
                 <li>
                     <div class="lbl">
                         <asp:Label ID="lblReasontoCancel" Text="<%$Resources:Resource,lblGiveReasontoCancel%>"
                             runat="server" />
                     </div>
                     <div class="input">
                         <asp:TextBox ID="txtReasonToCancel" runat="server" TextMode="MultiLine" Width="300px"
                             Height="80px" />
                     </div>
                     <div class="clearBoth">
                     </div>
                 </li>
                 <li>
                     <div class="lbl">
                         <asp:Label ID="Label9" Text="<%$Resources:Resource, lblAdminFee%>" runat="server" />
                     </div>
                     <div class="input">
                         <asp:TextBox ID="txtAdminFeeWhole" runat="server" MaxLength="8" />
                     </div>
                     <div class="clearBoth">
                     </div>
                 </li>
             </ul>
             <div class="div_command">
                 <asp:Button ID="btnCancelWhole" Text="<%$Resources:Resource, lblCancelReservation%>"
                     runat="server" OnClick="btnCancelWhole_Click" />
             </div>
         </asp:Panel>
     </div>
     <asp:HiddenField ID="hdnReservationID" runat="server" />
     <asp:HiddenField ID="hdnOrderID" runat="server" />
     <asp:HiddenField ID="hdnTotalPaidBefore" runat="server" />
     <asp:HiddenField ID="hdnOldBedID" runat="server" />
     <asp:HiddenField ID="hdnCancelFrom" runat="server" />
     <asp:HiddenField ID="hdnCancelTo" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        $("#<%=btnMove.ClientID%>").click(function () {
            parent._isRequireReload = true;
        });

        $("#<%=btnPartialCancel.ClientID%>").click(function () {
            parent._isRequireReload = true;
        });
    </script>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

using Newtonsoft.Json;

public partial class Sample : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //DateTime fDate = new DateTime(2012, 4,1);
        //DateTime tDate = new DateTime(2012, 4, 10);
        //Response.Write(string.Format("{0} = {1}", (tDate - fDate).TotalDays, Convert.ToInt32((tDate - fDate).TotalDays)));
    }

    //Initialize Culture
    protected override void InitializeCulture()
    {
        if (Session["Language"]== null)
        {
            Session["Language"] = "en-CA";
        }

        System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Session["Language"].ToString());
    }

    private string FacebookLogin()
    {
        string appId = "302562493147466";
        string cookieValue, accessToken, uid, url, userInformation, email = null;
        Regex getValues;
        Match infoMatch;
        MatchCollection valuesCollection;
        WebClient client;

        try
        {
            // Get the cookie
            cookieValue = Request.Cookies["fbs_" + appId].Value;

            // Get the values
            getValues = new Regex(@"(?<==)[^&]+");
            valuesCollection = getValues.Matches(cookieValue);

            /* The access_token and uid parameters are the first,
respectively last, in the cookie */
            accessToken = valuesCollection[0].ToString();
            uid = valuesCollection[5].ToString().Replace(@"""", "");

            // Build the URL and download it
            url = "https://graph.facebook.com/" + uid +
                   "?access_token=" + accessToken;
            client = new WebClient();
            userInformation = client.DownloadString(url);

            // Get the email address
            getValues = new Regex("(?<=\"email\":\")(.+?)(?=\")");
            infoMatch = getValues.Match(userInformation);
            email = infoMatch.Value;
        }
        catch (Exception) { }

        return email;
    }

}
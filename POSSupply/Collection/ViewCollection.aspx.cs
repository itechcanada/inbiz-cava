﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Collection_ViewCollection : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UIHelper.SetJqGridDefaults(grdCollection);
        if (!IsPagePostBack(grdCollection))
        {
            DropDownHelper.FillDropdown(dlCreatedDate, "AR", "dlSh2", Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.liARRcvdVia, ""));
        }
    }

    protected void grdCollection_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        InvoiceCollection col = new InvoiceCollection();
        string sBy = Request.QueryString[ddlSearch.ClientID] == null ? "NA" : Request.QueryString[ddlSearch.ClientID];
        int dayRange = 0;
        int.TryParse(Request.QueryString[dlCreatedDate.SelectedValue], out dayRange);
        string custname = Request.QueryString[txtCustomerName.ClientID];
        string custPhone = Request.QueryString[txtCustPhoneNo.ClientID];
        int invNo = 0;
        int.TryParse(Request.QueryString[txtInvoiceNo.ClientID], out invNo);
        string assignTo = Request.QueryString[txtAssign.ClientID];
        sdsCollection.SelectCommand = col.GetAllInvoiceToBeCollection(sdsCollection.SelectParameters, sBy, dayRange, custname, custPhone, invNo, assignTo, CurrentUser.UserID, CurrentUser.IsSalesRistricted);
    }

    protected void grdCollection_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 5)
        {
            switch (e.CellHtml)
            {
                case InvoicesStatus.SUBMITTED:
                    e.CellHtml = Resources.Resource.liINSubmitted;
                    break;
                case InvoicesStatus.RE_SUBMITTED:
                    e.CellHtml = Resources.Resource.liINReSubmitted;
                    break;
                case InvoicesStatus.PAYMENT_RECEIVED:
                    e.CellHtml = Resources.Resource.liINPaymentReceived;
                    break;
                case InvoicesStatus.PARTIAL_PAYMENT_RECEIVED:
                    e.CellHtml = Resources.Resource.liINPartialPaymentReceived;
                    break;              
            }            
        }
        else if (e.ColumnIndex == 7)
        {
            e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
        }
        else if (e.ColumnIndex == 8)
        {
            e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
        }
    }
}
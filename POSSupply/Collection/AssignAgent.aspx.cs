﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Collection_AssignAgent : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownHelper.FillCollectionAgentUser(dlCollectionAgent, new ListItem(Resources.Resource.liSelectCollectionAgent, ""));
        }
    }

    protected void btnAssignAgents_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                InvoiceCollection col;
                CollectionLog colLog;
                int hh, mm;
                int.TryParse(txtHH.Text, out hh);
                int.TryParse(txtMM.Text, out mm);
                string strDate = string.Format("{0} {1:00}:{2:00} {3}", txtDate.Text, hh, mm, ddlTT.SelectedValue);
                foreach (var item in this.InvoiceIDs)
                {
                    col = new InvoiceCollection();
                    col.ColInvID = item;
                    col.ColNotes = txtNote.Text;
                    col.ColStatus = "1";
                    col.ColUserAssignedByManager = CurrentUser.UserID;
                    col.ColUserAssignedTo = BusinessUtility.GetInt(dlCollectionAgent.SelectedValue);
                    if (col.Insert(dbHelp, CurrentUser.UserID))
                    {
                        colLog = new CollectionLog();
                        colLog.ColFollowupUserID = CurrentUser.UserID;
                        colLog.ColHeaderID = item;
                        colLog.ColLogText = txtNote.Text;
                        colLog.ColFollowup = BusinessUtility.GetDateTime(strDate, "MM/dd/yyyy hh:mm tt");
                        //colLog.ColFollowup = colLog.ColFollowup.ToUniversalTime();
                        colLog.Insert(dbHelp);

                        try
                        {
                            AlertHelper.SetCollectionAgentAlert(dbHelp, colLog.ColFollowup, item, col.ColUserAssignedTo, CurrentUser.UserID);
                        }
                        catch { }                                                
                    }
                }

                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                }
                else
                {
                    Globals.RegisterCloseDialogScript(Page);
                }  
            }
            catch(Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError + ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    private int[] InvoiceIDs
    {
        get
        {
            string strArr = Request.QueryString["invid"];
            if (!string.IsNullOrEmpty(strArr))
            {
                string[] arr = strArr.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                return Array.ConvertAll<string, int>(arr, a => BusinessUtility.GetInt(a));
            }
            return new List<int>().ToArray();
        }
    }

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }
}
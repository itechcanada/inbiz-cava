﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AssignAgent.aspx.cs" Inherits="Collection_AssignAgent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <ul class="form">
        <li>
             <div class="lbl">
                <asp:Label ID="lblARCollectionAgent" CssClass="lblBold" Text="<%$ Resources:Resource, lblARCollectionAgent%>"
                                                    runat="server" />
            </div>
            <div class="input">
                <asp:DropDownList ID="dlCollectionAgent" runat="server" Width="192px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ControlToValidate="dlCollectionAgent"
                    runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="Label2" CssClass="lblBold" Text="<%$ Resources:Resource, grdCAFollowupDateTime%>"
                                                    runat="server" />                
            </div>
            <div class="input">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="padding-right:2px;">
                            <asp:TextBox ID="txtDate" runat="server" Width="75px" CssClass="datepicker" />
                        </td>
                        <td style="padding:0 2px;">
                            <asp:TextBox ID="txtHH" runat="server" Width="25px" CssClass="integerTextField" MaxLength="2" />
                        </td>
                        <td style="padding:0 2px;">
                            <asp:TextBox ID="txtMM" runat="server" Width="25px" CssClass="integerTextField" MaxLength="2" />
                        </td>
                        <td style="padding:0 2px;">
                            <asp:DropDownList runat="Server" ID="ddlTT" Width="45">
                                <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:0 2px;">
                            (mm/dd/yyyy)
                        </td>
                        <td style="padding:0 2px;">
                            (hh)
                        </td>
                        <td style="padding:0 2px;">
                            (mm)
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="Label1" CssClass="lblBold" Text="<%$ Resources:Resource, lblARNote%>"
                                                    runat="server" />
            </div>
            <div class="input" style="width:300px;">
               <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="3" Width="250px">
                                            </asp:TextBox>
            </div>
            <div class="clearBoth">
            </div>
        </li>
    </ul>
    <div class="div_command">                  
        <asp:Button ID="btnAssignAgents" Text="<%$Resources:Resource, btnAssignAgents %>" 
            runat="server" onclick="btnAssignAgents_Click" />         
        <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
            CausesValidation="False" 
            OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" /> 
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>



﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">

    protected override void OnInit(EventArgs e)
    {
        if (Request.Cookies.AllKeys.Contains("timezoneoffset"))
        {
            Session["timezoneoffset"] = Request.Cookies["timezoneoffset"].Value;
        }
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Write("<br> Utc Now converted: ");
        Response.Write(DateTime.UtcNow.ToClientTime());
        Response.Write("<br> Now converted: ");
        Response.Write(DateTime.Now.ToClientTime());
        Response.Write("<br> Utc Now: ");
        Response.Write(DateTime.UtcNow);
        Response.Write("<br> Now: ");
        Response.Write(DateTime.Now);
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        DateTime dt;
        int mm, dd, yyyy, hh, m;
        int.TryParse(txtMM.Text, out mm);
        int.TryParse(txtDD.Text, out dd);
        int.TryParse(txtYYYY.Text, out yyyy);
        int.TryParse(txtTimeHH.Text, out hh);
        int.TryParse(txtTimeMM.Text, out m);

        dt = iTECH.Library.Utilities.BusinessUtility.GetDateTime(string.Format("{0:00}{1:00}{2}{3:00}{4:00}{5}", mm, dd, yyyy, hh, m, ddlAmPm.SelectedValue), "MMddyyyyhhmmtt");
        TestClass.SendAppointment("hitendram@itechcanada.com", "hitendram@itechcanada.com", "", "Meet Him", "Montreal", dt.ToUniversalTime(), dt.AddHours(1).ToUniversalTime());

        /*Appointment ap = new Appointment();
        ap.AttendeeList.Add("itechcanada.inc@gmail.com");
        ap.AttendeeList.Add("hitendram@itechcanada.com");
        ap.EndDate = dt.AddHours(3);
        ap.Location = "Bhopal";
        ap.MeetingGUID = Guid.NewGuid();
        ap.OrganizerEmail = "hitendram@itechcanada.com";
        ap.OrganizerName = "Hitendra";
        ap.Password = "erp890";
        ap.Port = 2525;
        ap.ServerName = "mail.itechcanada.com";
        ap.StartDate = dt;
        ap.Subject = "Test Meeting";
        ap.Summary = "Please join this meeting";
        ap.UserName = "erp@itechcanada.com";
        //Response.Write(ap.MeetingGUID);
        ap.EmailAppointment();*/
    }
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="lib/scripts/jquery-1.5.1.min.js"></script>
    <script src="lib/scripts/jquery-plugins/jquery.cookie.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            setTimezoneCookie();
        });

        function setTimezoneCookie() {
            var timezone_cookie = "timezoneoffset";

            // if the timezone cookie not exists create one.
            if (!$.cookie(timezone_cookie)) {

                // check if the browser supports cookie
                var test_cookie = 'test cookie';
                $.cookie(test_cookie, true);

                // browser supports cookie
                if ($.cookie(test_cookie)) {

                    // delete the test cookie
                    $.cookie(test_cookie, null);

                    // create a new cookie 
                    $.cookie(timezone_cookie, new Date().getTimezoneOffset());

                    // re-load the page
                    location.reload();
                }
            }
            // if the current timezone and the one stored in cookie are different
            // then store the new timezone in the cookie and refresh the page.
            else {

                var storedOffset = parseInt($.cookie(timezone_cookie));
                var currentOffset = new Date().getTimezoneOffset();

                // user may have changed the timezone
                if (storedOffset !== currentOffset) {
                    $.cookie(timezone_cookie, new Date().getTimezoneOffset());
                    location.reload();
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    </div>
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <asp:TextBox ID="txtMM" runat="server" Width="40px" />
            </td>
            <td>
                <asp:TextBox ID="txtDD" runat="server" Width="40px" />
            </td>
            <td>
                <asp:TextBox ID="txtYYYY" Text="2012" runat="server" Enabled="false" Width="60px" />
            </td>
            <td>
                <asp:TextBox ID="txtTimeHH" runat="server" Width="40px" />
            </td>
            <td>
                <asp:TextBox ID="txtTimeMM" runat="server" Width="40px" />
            </td>
            <td>
                <asp:DropDownList ID="ddlAmPm" runat="server">
                    <asp:ListItem Text="AM" />
                    <asp:ListItem Text="PM" />
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btnSend" runat="server" Text="Send Meeting Request" OnClick="btnSend_Click" />
            </td>
        </tr>
        <tr>
            <td>
                MM
            </td>
            <td>
                DD
            </td>
            <td>
                YYYY
            </td>
            <td>
                hh
            </td>
            <td>
                mm
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

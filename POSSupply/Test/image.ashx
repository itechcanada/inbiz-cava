﻿<%@ WebHandler Language="C#" Class="image" %>

using System;
using System.Web;

public class image : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        System.Threading.Thread.Sleep(5000);
        context.Response.ContentType = "image/jpeg";
        context.Response.WriteFile(context.Server.MapPath("~/Upload/Product/1424_high_1_174852.jpg"));
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}
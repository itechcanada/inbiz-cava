﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="Test" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">  
    <%=UIHelper.GetScriptTags("blockui", 1)%>   
    <script type="text/javascript">
        function blockContentArea(elementToBlock, loadingMessage) {
            $(elementToBlock).block({
                message: '<div>' + loadingMessage + '</div>',
                css: { border: '3px solid #a00' }
            });
        }
        function unblockContentArea(elementToBlock) {
            $(elementToBlock).unblock();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
   
    <p>
        &lt;</p>
    
    <p>
        &gt;</p>
    <asp:TextBox ID="txtSample" runat="server" />
    <asp:RequiredFieldValidator ID="rfvSample" ErrorMessage="*" ControlToValidate="txtSample"
        runat="server" />
    <asp:Button ID="btnDisable" Text="Disable" runat="server" 
        onclick="btnDisable_Click" />
    <asp:Button ID="btnTestMessage" Text="Test Message" runat="server" 
        onclick="btnTestMessage_Click" CausesValidation="false" />
    <script type="text/javascript">        
        $("#<%=btnDisable.ClientID%>").click(function () {
            if (Page_IsValid) {
                blockContentArea($("body"), "<div>Please wait while processing your payment.</div><div>Do not refresh page or press back button</div>");
            }
        });
    </script>
    <a id="ajaxSecurityCheck" href="#">Ajax Security Check</a>
    <script type="text/javascript">
        $("#ajaxSecurityCheck").click(function () {
            var dataToPost = {};
            dataToPost["id"] = 10;
            $.ajax({
                type: "POST",
                url: "http://localhost:6212/InbizWebDvlp/Inventory/ManageMinDays.aspx/GetSelectedItem",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(dataToPost), //"{'roomid' : '" + $(this).val() + "'}",
                success: function (data) {
                    if (data.d == null) {
                        alert("Authentication Failed!");
                        return;
                    }
                    alert(data.d);
                },
                error: function (request, status, errorThrown) {
                    alert(status);
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" Runat="Server">      
    <%--<asp:Button ID="btnSendEvent" runat="server" Text="Send" 
        onclick="btnSendEvent_Click" />   --%>
    <asp:Literal ID="ltScript" Text="" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" Runat="Server">
   <%-- <asp:HyperLink ID="hlTest" NavigateUrl="" runat="server">
        <asp:image ID="imgTest" imageurl="" runat="server" />
    </asp:HyperLink>--%>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" Runat="Server">   
   <%-- <script type="text/javascript">
        window.onload = function () {
            location.href = "default.aspx?" + location.search.substring(1);
        }
    </script>--%>
</asp:Content>


﻿<%@ Page Language="C#" %>
<%@ Import Namespace="MySql.Data.MySqlClient" %>
<%@ Import Namespace="iTECH.InbizERP.BusinessLogic" %>
<%@ Import Namespace="iTECH.Library.DataAccess.MySql" %>
<%@ Import Namespace="iTECH.Library.Utilities" %>
<%@ Import Namespace="System.Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        this.ImportRoomsAndBeds();
    }

    private void ImportRoomsAndBeds()
    {
        DbTransactionHelper dbHelp = new DbTransactionHelper();
        try
        {
            string sqlRoom = "INSERT INTO z_accommodation_rooms (BuildingID, RoomType, RoomTitleEn, RoomTitleFr, ProritySingle, PriorityCouple, Seq, IsActive)";
            sqlRoom += " VALUES(40, 5, 'Camp{0}', 'Camp{0}', 1, 1, {0}, 1)";
            string sqlProduct = "INSERT INTO products (prdIntID, prdExtID, prdType, prdUPCCode, prdName, prdIsKit, prdMinQtyPerSO, prdCreatedUserID, prdCreatedOn, prdSalePricePerMinQty, prdEndUserSalesPrice, ";
            sqlProduct += "prdIsSpecial, prdDiscount, prdMinQtyPOTrig, prdAutoPO, prdPOQty, prdIsActive, prdIsWeb, prdComissionCode, isPOSMenu, prdCategory, prdSubcategory, prdWebSalesPrice, ";
            sqlProduct += "prdExtendedCategory, prdSeq)";
            sqlProduct += " VALUES ('CAMP{0}-1', 'CAMP{0}-1', 2, 'CAMP{0}-1', 'CAMP{0}-1', 0, 0,  1, '{1:yyyy-MM-dd HH:mm:ss}', 0, 45, ";
            sqlProduct += "0, 0, 0, 0, 0, 1, 0, NULL, 0, 0, 0, 45, {2}, 1)";
            string sqlPrdDesc = "INSERT INTO prddescriptions VALUES ({0}, 'en', 'CAMP{1}-1', 'CAMP{1}-1', 'CAMP{1}-1'),({0}, 'fr', 'CAMP{1}-1', 'CAMP{1}-1', 'CAMP{1}-1'), ({0}, 'es', 'CAMP{1}-1', 'CAMP{1}-1', 'CAMP{1}-1');";
            int rid = 0, pid = 0;
            dbHelp.BeginTransaction();
            for (int i = 56; i <= 100; i++)
            {
                rid = 0;
                pid = 0;
                dbHelp.ExecuteNonQuery(string.Format(sqlRoom, i), CommandType.Text, null);
                rid = dbHelp.GetLastInsertID();
                if (rid <= 0)
                {
                    throw new Exception("Room was not inesrted!");
                }
                dbHelp.ExecuteNonQuery(string.Format(sqlProduct, i, DateTime.Now, rid), CommandType.Text, null);
                pid = dbHelp.GetLastInsertID();
                if (pid <= 0)
                {
                    throw new Exception("Product was not inesrted!");
                }
                dbHelp.ExecuteNonQuery(string.Format(sqlPrdDesc, pid, i), CommandType.Text, null);
            }

            dbHelp.CommitTransaction();
        }
        catch (Exception ex)
        {
            try
            {
                dbHelp.RollBackTransaction();
            }
            finally
            {
                Response.Write(ex.Message);
                Response.Write("<br>");
            }
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }   
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>

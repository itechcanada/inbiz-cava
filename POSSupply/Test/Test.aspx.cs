﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using System.IO;
using System.Net;
using System.Net.Mail;

public partial class Test : System.Web.UI.Page
{
    public string GetUrl(object o1, object o2) {
        return string.Format("~/default.aspx?id={0}&rid={1}", o1, o2);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(5000);
        //DateTime dt1 = DateTime.Now;
        //DateTime dt2 = DateTime.Now.AddYears(4);

        //int diffYear = dt2.Year - dt1.Year;

        //DateTime dt = new DateTime(2013, 2, 28);

        //DbHelper dbHelp = new DbHelper(true);
        //double unitPrice = this.CalculateAccommodationPrice(dbHelp, 4741, 80, new DateTime(2012, 5, 12), new DateTime(2012, 5, 28));

        //double d = Math.Round(5.96D);
        //Response.Write(d);

        //string ss = "Amit or Hitendra";
        //string[] arr = ss.Split(new string[] { " or " }, StringSplitOptions.RemoveEmptyEntries);
        //Response.Write(string.Format("{0:yyyyMMddhhmmss}_{1}", DateTime.Now, "TEST"));

        //Uri uri = new Uri("http://www.youtube.com/watch?v=lJ3XV_6hJac&feature=g-logo-xit");
        //var t = HttpUtility.ParseQueryString(uri.Query);
        //ltScript.Text = t["v"];
    }
       
    protected void rptTest_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "download")
        {
            Response.Redirect(string.Format("~/default.aspx?oid={1}", e.CommandArgument));
        }
    }
    protected void btnSendEvent_Click(object sender, EventArgs e)
    {
        
    }

    protected void btnDisable_Click(object sender, EventArgs e)
    {
        
    }

    protected void btnTestMessage_Click(object sender, EventArgs e)
    {
        MessageState.SetGlobalMessage(MessageType.Information, "Test Info Message!");
    }
}
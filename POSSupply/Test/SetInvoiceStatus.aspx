﻿<%@ Page Language="C#" %>
<%@ Import Namespace="iTECH.InbizERP.BusinessLogic" %>
<%@ Import Namespace="iTECH.Library.Utilities" %>
<%@ Import Namespace="iTECH.WebControls" %>
<%@ Import Namespace="iTECH.Library.DataAccess.MySql" %>
<%@ Import Namespace="MySql.Data.MySqlClient" %>
<%@ Import Namespace="System.Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetTaxInOrderFirstTime();
            //MysqlFunctionTest();
            //DbHelper dbHelp = new DbHelper(true);
            //Response.Write("<br>");
            //Response.Write(dbHelp.GetValue("SELECT  getAmount(239.975)", CommandType.Text, null));
            //dbHelp.CloseDatabaseConnection();

            //this.SetTaxInOrderFirstTime();
            
        }               
    }

    private void MysqlFunctionTest()
    {
        DbHelper dbHelp = new DbHelper();
        try
        {
            /*//string sqlGetTax = "SELECT getTaxCalculated(@Amount, @Tax1, @TxOnTotal1,@Tax2, @TxOnTotal2,@Tax3, @TxOnTotal3,@Tax4, @TxOnTotal4,@Tax5, @TxOnTotal5)";
            string sqlGetTax = "SELECT getTaxCalculated(99, 3.5, 1,5, 1,9.75, 1,0, 0,0, 0)";
            Tax tx = new Tax();
            tx.Tax1 = 3.5D;
            tx.Tax2 = 5.00D;
            tx.Tax3 = 9.75D;
            tx.TaxOnTotal1 = true;
            tx.TaxOnTotal2 = true;
            tx.TaxOnTotal3 = true;
            double txAmt = 0.00D;
            double amount = 99.00D;
            txAmt = BusinessUtility.GetDouble(dbHelp.GetValue("SELECT getTaxCalculated(@Amount, @Tax1,  @TaxOnTotal1, @Tax2, @TaxOnTotal2, @Tax3, @TaxOnTotal3,@Tax4, @TaxOnTotal4,@Tax5, @TaxOnTotal5)", CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("Amount", amount, MyDbType.Double),
                        DbUtility.GetParameter("Tax1", tx.Tax1, MyDbType.Double),
                        DbUtility.GetParameter("Tax2", tx.Tax2, MyDbType.Double),
                        DbUtility.GetParameter("Tax3", 0, MyDbType.Double),
                        DbUtility.GetParameter("Tax4", 0, MyDbType.Double),
                        DbUtility.GetParameter("Tax5", 0, MyDbType.Double),
                        DbUtility.GetIntParameter("TaxOnTotal1", 1),
                        DbUtility.GetIntParameter("TaxOnTotal2", 1),
                        DbUtility.GetParameter("TaxOnTotal3", false, MyDbType.Boolean),
                        DbUtility.GetParameter("TaxOnTotal4", false, MyDbType.Boolean),
                        DbUtility.GetParameter("TaxOnTotal5", false, MyDbType.Boolean)
                    }));

            txAmt = BusinessUtility.GetDouble(dbHelp.GetValue(sqlGetTax, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("Amount", amount, MyDbType.Double),
                        DbUtility.GetParameter("Tax1", tx.Tax1, MyDbType.Double),
                        DbUtility.GetParameter("Tax2", tx.Tax2, MyDbType.Double),
                        DbUtility.GetParameter("Tax3", 0, MyDbType.Double),
                        DbUtility.GetParameter("Tax4", 0, MyDbType.Double),
                        DbUtility.GetParameter("Tax5", 0, MyDbType.Double),
                        this.GetBooleanParameter("TaxOnTotal1", tx.TaxOnTotal1),
                        this.GetBooleanParameter("TaxOnTotal2", tx.TaxOnTotal2),
                        DbUtility.GetParameter("TaxOnTotal3", 0, MyDbType.Int),
                        DbUtility.GetParameter("TaxOnTotal4", 0, MyDbType.Int),
                        DbUtility.GetParameter("TaxOnTotal5", 0, MyDbType.Int)
                    }));
            txAmt = BusinessUtility.GetDouble(dbHelp.GetValue(sqlGetTax, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("Amount", amount, MyDbType.Double),
                        DbUtility.GetParameter("Tax1", 3.5, MyDbType.Double),
                        DbUtility.GetParameter("Tax2", 5, MyDbType.Double),
                        DbUtility.GetParameter("Tax3", 0, MyDbType.Double),
                        DbUtility.GetParameter("Tax4", 0, MyDbType.Double),
                        DbUtility.GetParameter("Tax5", 0, MyDbType.Double),
                        DbUtility.GetParameter("TaxOnTotal1", tx.TaxOnTotal1 ? 1 : 0, MyDbType.Int),
                        DbUtility.GetParameter("TaxOnTotal2", tx.TaxOnTotal2 ? 1 : 0, MyDbType.Int),
                        DbUtility.GetParameter("TaxOnTotal3", 0, MyDbType.Int),
                        DbUtility.GetParameter("TaxOnTotal4", 0, MyDbType.Int),
                        DbUtility.GetParameter("TaxOnTotal5", 0, MyDbType.Int)
                    }));
            txAmt = BusinessUtility.GetDouble(dbHelp.GetValue(sqlGetTax, CommandType.Text, new MySqlParameter[] { 
                        DbUtility.GetParameter("Amount", amount, MyDbType.Double),
                        DbUtility.GetParameter("Tax1", tx.Tax1, MyDbType.Double),
                        DbUtility.GetParameter("Tax2", tx.Tax2, MyDbType.Double),
                        DbUtility.GetParameter("Tax3", tx.Tax3, MyDbType.Double),
                        DbUtility.GetParameter("Tax4", 0, MyDbType.Double),
                        DbUtility.GetParameter("Tax5", 0, MyDbType.Double),
                        DbUtility.GetParameter("TaxOnTotal1", tx.TaxOnTotal1 ? 1 : 0, MyDbType.Int),
                        DbUtility.GetParameter("TaxOnTotal2", tx.TaxOnTotal2 ? 1 : 0, MyDbType.Int),
                        DbUtility.GetParameter("TaxOnTotal3", tx.TaxOnTotal3 ? 1 : 0, MyDbType.Int),
                        DbUtility.GetParameter("TaxOnTotal4", 0, MyDbType.Int),
                        DbUtility.GetParameter("TaxOnTotal5", 0, MyDbType.Int)
                    }));  */          
        }
        catch
        {

            throw;
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private MySqlParameter GetBooleanParameter(string name, bool val) {
        MySqlParameter p = new MySqlParameter(BusinessUtility.GetParameterName(name), MySqlDbType.Bit);
        p.Value = val;
        return p;
    }

    private void SetTaxInOrderFirstTime()
    { 
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            Dictionary<int, int> dicItemTax; //itemID, taxgroupID
            string sql = "SELECT orderItemID,ordProductTaxGrp FROM orderitems";
            using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
            {
                dicItemTax = new Dictionary<int, int>();
                while (dr.Read())
                {
                    dicItemTax[BusinessUtility.GetInt(dr["orderItemID"])] = BusinessUtility.GetInt(dr["ordProductTaxGrp"]);
                }                
            }
            OrderItems oi = new OrderItems();
            foreach (var item in dicItemTax.Keys)
            {
                oi.UpdateTax(dbHelp, item, dicItemTax[item]);
            }

            sql = "SELECT orderItemProcID,sysTaxCodeDescID FROM orderitemprocess";
            using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
            {
                dicItemTax = new Dictionary<int, int>();
                while (dr.Read())
                {
                    dicItemTax[BusinessUtility.GetInt(dr["orderItemProcID"])] = BusinessUtility.GetInt(dr["sysTaxCodeDescID"]);
                }
            }

            OrderItemProcess oip = new OrderItemProcess();
            foreach (var item in dicItemTax.Keys)
            {
                oip.UpdateTax(dbHelp, item, dicItemTax[item]);
            }

            sql = "SELECT invItemID,invProductTaxGrp FROM invoiceitems";
            using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
            {
                dicItemTax = new Dictionary<int, int>();
                while (dr.Read())
                {
                    dicItemTax[BusinessUtility.GetInt(dr["invItemID"])] = BusinessUtility.GetInt(dr["invProductTaxGrp"]);
                }
            }

            InvoiceItems ii = new InvoiceItems();
            foreach (var item in dicItemTax.Keys)
            {
                ii.UpdateTax(dbHelp, item, dicItemTax[item]);
            }

            sql = "SELECT invItemProcID,sysTaxCodeDescID FROM invitemprocess";
            using (var dr = dbHelp.GetDataReader(sql, CommandType.Text, null))
            {
                dicItemTax = new Dictionary<int, int>();
                while (dr.Read())
                {
                    dicItemTax[BusinessUtility.GetInt(dr["invItemProcID"])] = BusinessUtility.GetInt(dr["sysTaxCodeDescID"]);
                }
            }

            InvoiceItemProcess iip = new InvoiceItemProcess();
            foreach (var item in dicItemTax.Keys)
            {
                iip.UpdateTax(dbHelp, item, dicItemTax[item]);
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }
    
    private void SetInvoiceStatus()
    {
        /*DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.OpenDatabaseConnection();
                string sql = "SELECT * FROM invoices";
                DataTable dt = dbHelp.GetDataTable(sql, CommandType.Text, null);
                int invID = 0;
                TotalSummary ts;
                List<string> invToSetPaid = new List<string>();
                List<string> invToSetPartialPaid = new List<string>();
                List<string> invToSetSubmitted = new List<string>();
                foreach (DataRow item in dt.Rows)
                {
                    invID = BusinessUtility.GetInt(item["invID"]);
                    ts = CalculationHelper.GetInvoiceTotal(dbHelp, invID);
                    if (ts.TotalAmountReceived > 0 || ts.GetTotalWriteOff() > 0)
                    {
                        if (ts.GrandTotal <= ts.TotalAmountReceived)
                        {
                            invToSetPaid.Add(invID.ToString());
                        }
                        else
                        {
                            invToSetPartialPaid.Add(invID.ToString());
                        }                        
                    }
                    else if(ts.OutstandingAmount > 0)
                    {
                        invToSetSubmitted.Add(invID.ToString());
                    }
                }
                string sqlUpdate = "UPDATE invoices SET invStatus=@Status WHERE invID IN({0})";
                if (invToSetPaid.Count > 0)
                {
                    dbHelp.ExecuteNonQuery(string.Format(sqlUpdate, string.Join(",", invToSetPaid.ToArray())), CommandType.Text, new MySql.Data.MySqlClient.MySqlParameter[] { 
                        DbUtility.GetParameter("Status", InvoicesStatus.PAYMENT_RECEIVED, MyDbType.String)
                    });
                }
                if (invToSetPartialPaid.Count > 0)
                {
                    dbHelp.ExecuteNonQuery(string.Format(sqlUpdate, string.Join(",", invToSetPartialPaid.ToArray())), CommandType.Text, new MySql.Data.MySqlClient.MySqlParameter[] { 
                        DbUtility.GetParameter("Status", InvoicesStatus.PARTIAL_PAYMENT_RECEIVED, MyDbType.String)
                    });
                }
                if (invToSetSubmitted.Count > 0)
                {
                    dbHelp.ExecuteNonQuery(string.Format(sqlUpdate, string.Join(",", invToSetSubmitted.ToArray())), CommandType.Text, new MySql.Data.MySqlClient.MySqlParameter[] { 
                        DbUtility.GetParameter("Status", InvoicesStatus.SUBMITTED, MyDbType.String)
                    });
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }*/
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>

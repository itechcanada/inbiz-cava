﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Test_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
    <script type="text/javascript">
        // Boxer plugin
        $.widget("ui.boxer", $.ui.mouse, {

            _init: function () {
                this.element.addClass("ui-boxer");

                this.dragged = false;

                this._mouseInit();

                this.helper = $("<div />")
			.css({ border: '1px dotted black' })
			.addClass("ui-boxer-helper"); //.appendTo('body');
            },

            destroy: function () {
                this.element
			.removeClass("ui-boxer ui-boxer-disabled")
			.removeData("boxer")
			.unbind(".boxer");
                this._mouseDestroy();

                return this;
            },

            _mouseStart: function (event) {
                var self = this;

                this.opos = [event.pageX, event.pageY];

                if (this.options.disabled)
                    return;

                var options = this.options;

                this._trigger("start", event);

                $('body').append(this.helper);

                this.helper.css({
                    "z-index": 100,
                    "position": "absolute",
                    "left": event.clientX,
                    "top": event.clientY,
                    "width": 0,
                    "height": 0
                });
            },

            _mouseDrag: function (event) {
                var self = this;
                this.dragged = true;

                if (this.options.disabled)
                    return;

                var options = this.options;

                var x1 = this.opos[0], y1 = this.opos[1], x2 = event.pageX, y2 = event.pageY;
                if (x1 > x2) { var tmp = x2; x2 = x1; x1 = tmp; }
                if (y1 > y2) { var tmp = y2; y2 = y1; y1 = tmp; }
                this.helper.css({ left: x1, top: y1, width: x2 - x1, height: y2 - y1 });

                this._trigger("drag", event);

                return false;
            },

            _mouseStop: function (event) {
                var self = this;

                this.dragged = false;

                var options = this.options;

                var clone = this.helper.clone()
			.removeClass('ui-boxer-helper').appendTo(this.element);
                $(clone).resizable().draggable();
                this._trigger("stop", event, { box: clone });

                this.helper.remove();

                return false;
            }
        });
        $.extend($.ui.boxer, {
            defaults: $.extend({}, $.ui.mouse.defaults, {
                appendTo: 'body',
                distance: 0
            })
        });

       
    </script>
    <style type="text/css">
        #canvas { height: 500px; background: white; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="canvas"></div>

         <input type="button" value="Ram" onclick="CallService()"/>

    <div style="display:none;">
        <asp:Image runat="server" ID="img1" ImageUrl="~/Test/ImageManager.ashx?src=S_Girardeau_Alumia_1.jpg&width=300" />
       <%-- <asp:Image runat="server" ID="Image1" ImageUrl="~/Test/ImageManager.ashx?src=S_Girardeau_Alumia_1.jpg&width=200" />
        <asp:Image runat="server" ID="Image2" ImageUrl="~/Test/ImageManager.ashx?src=S_Girardeau_Alumia_1.jpg" />
        <asp:Image runat="server" ID="Image3" ImageUrl="~/Test/S_Girardeau_Alumia_1.jpg" />--%>

        <asp:FileUpload ID="fuTest" runat="server" />
        <asp:Button ID="btnUpload" Text="Upload" runat="server" 
            onclick="btnUpload_Click" />


           
    </div>
    <script type="text/javascript">
        // Using the boxer plugin
        $('#canvas').boxer({
            stop: function (event, ui) {
                var offset = ui.box.offset();
                ui.box.css({ border: '1px solid white', background: 'orange', padding: '0.5em' })
			.append('x:' + offset.left + ', y:' + offset.top)
			.append('<br>')
			.append('w:' + ui.box.width() + ', h:' + ui.box.height());
            }
        });



        function CallService()
        {
          var JSONObject = new Array();
//            $('.cssPqty').each(function () {
                var obj = new Object();
                obj.emailID = "rahulp@itechcanada.com";
                JSONObject.push(obj);

                var obj1 = new Object();
                obj1.emailID = "mukesh@itechcanada.com";
                JSONObject.push(obj1);

//            });

            $.ajax({
                type: "POST",
                //url: "mdSearchProduct.aspx/SaveProductDetails",
                url: "https://live2.itechcanada.com/Sporteaz/SporteazServices.asmx/AddFriends",
                dataType: "json",
                //data: JSON.stringify("{ Sporteaz.BusinessLogic.friendsEmail: "+ JSONObject +" ,'userID':'8','tokenNo':'cAEAAIlCg6zaWFLAzPgp9NzjLW5XuZNoORFeoz7Y1ereBjPmsYjWoppUnMFywngDzvSfOsDG6WI8uhvXhP\/LqJTJyc2hEb9S6eQbxKla5FlpP\/YYDrzo830Dxdnt4U2tyddRNKaXz6r725Fsm0F8Q04tirS+lnpNyssTmLzEfNNo6nOOy2YP8opQugjR9Vhl1iD3b8D8Vpv70GwxIiNfRw1HXZqk1OwYfw751H7LhI75QKSHV+oH6M+cgKEm+Ure1+SnVEzs4fqyOOub84krfclh3\/DpiGEGq1104BPzQJlCOdn4KypRsnNnBkleBi\/MN3NwrpfPBdQ3igo+6h2cQyHblHeXxey+i8\/hIYNxgBpngQNoeCn+IxD9KYDeLn6Tf8EhGkoCgH\/WWvRhOW37KTXf\/7eWiiI2kwy0ZDiRmIeiBuiMqyARpDyYzSHyC7p3Ce4VmEsnj5v4JObBePAE8Mn9iOrchowY7ihhw9Dfcy3sKzqKSolYiceyOXqVnD1EgC84kStB2LZQG6uDd3Tt+hCwiyFiJiD1tfhZeWQh76uDatgDJP17EN3hSZXUY6Z3V9KYbSDZ0Nl836kCXlGqpJFiExsoBtJKk\/SWl1ZM8UBdUkEFEPPUe4HFd6jyNXi9Xt0tZAy7rYXqlC3LfAmtIuK\/\/C8=','emailID':'test@test.com','deviceID':'5402b9acc8a529b5'}"),
                data: JSON.stringify("{'emailID':'test@test.com','deviceID':'5402b9acc8a529b5','tokenNo':'cAEAAIlCg6zaWFLAzPgp9NzjLW5XuZNoORFeoz7Y1ereBjPmsYjWoppUnMFywngDzvSfOsDG6WI8uhvXhP\/LqJTJyc2hEb9S6eQbxKla5FlpP\/YYDrzo830Dxdnt4U2tyddRNKaXz6r725Fsm0F8Q04tirS+lnpNyssTmLzEfNNo6nOOy2YP8opQugjR9Vhl1iD3b8D8Vpv70GwxIiNfRw1HXZqk1OwYfw751H7LhI75QKSHV+oH6M+cgKEm+Ure1+SnVEzs4fqyOOub84krfclh3\/DpiGEGq1104BPzQJlCOdn4KypRsnNnBkleBi\/MN3NwrpfPBdQ3igo+6h2cQyHblHeXxey+i8\/hIYNxgBpngQNoeCn+IxD9KYDeLn6Tf8EhGkoCgH\/WWvRhOW37KTXf\/7eWiiI2kwy0ZDiRmIeiBuiMqyARpDyYzSHyC7p3Ce4VmEsnj5v4JObBePAE8Mn9iOrchowY7ihhw9Dfcy3sKzqKSolYiceyOXqVnD1EgC84kStB2LZQG6uDd3Tt+hCwiyFiJiD1tfhZeWQh76uDatgDJP17EN3hSZXUY6Z3V9KYbSDZ0Nl836kCXlGqpJFiExsoBtJKk\/SWl1ZM8UBdUkEFEPPUe4HFd6jyNXi9Xt0tZAy7rYXqlC3LfAmtIuK\/\/C8=','userID':'8',friendsEmail: '"+ JSONObject +"'}"),
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "success") {
                        parent.reloadGrid();
                        jQuery.FrameDialog.closeDialog();
                    }


                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }


    </script


    </form>
</body>
</html>

<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="ViewOrderDetails.aspx.vb" Inherits="Partner_ViewOrderDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="Resources.resource" %>

<%@ Register src="../Controls/CommonSearch/CRMSearch.ascx" tagname="CRMSearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();               
    </script>
    <uc1:CRMSearch ID="CRMSearch1" runat="server" />
</asp:Content>

<asp:Content ID="cntViewOrderDetails" ContentPlaceHolderID="cphMaster" runat="Server">
    
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                   
                </td>
            </tr>
        </table>
    </div>

    <br />
    <div class="divMainContent">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="center" colspan="2">
                    <asp:UpdatePanel ID="upnlMsg" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr runat="server" visible="false">
                <td height="20" colspan="2" align="right">
                    <table align="right">
                        <tr>
                            <td width="400px">
                                &nbsp;
                            </td>
                            <td>
                                <asp:ImageButton runat="server" ID="imgFax" ImageUrl="~/Images/fax.png" Width="48"
                                    Height="48" />
                            </td>
                            <td>
                                <asp:ImageButton runat="server" ID="imgMail" ImageUrl="~/Images/email.jpg" Width="48"
                                    Height="48" />
                            </td>
                            <td>
                                <asp:ImageButton runat="server" ID="imgPrint" CausesValidation="false" ImageUrl="~/Images/Printer1.png"
                                    Width="48" Height="48" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="20" colspan="2">
                    <table width="100%">
                        <tr>
                            <td width="20%">
                                <asp:Label ID="lblSONumber" Text="<%$ Resources:Resource, lblSOQuotationNo %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td width="30%">
                                <asp:Label ID="lblSOID" CssClass="lblBold" runat="server" />
                            </td>
                            <td width="2%">
                            </td>
                            <td align="left" width="18%">
                                <asp:Label ID="lblSOPOComName" Text="<%$ Resources:Resource, lblSOPOComName %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblSOPOComNameTitle" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblSODate1" Text="<%$ Resources:Resource, lblSOOrderDate %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblSODate" CssClass="lblBold" runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblSOPOwhs" Text="<%$ Resources:Resource, lblSOPOwhs %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblSOPOwhsName" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblSOQEDate1" Text="<%$ Resources:Resource, lblSOQutExpDate %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblSOQEDate" CssClass="lblBold" runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblCustomerName1" Text="<%$ Resources:Resource, lblSOCustomerName %>"
                                    CssClass="lblBold" runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblCustomerName" CssClass="lblBold" runat="server" />
                                <asp:HiddenField ID="hdnCustId" runat="server"></asp:HiddenField>
                                <asp:HiddenField ID="hdnCustTyp" runat="server"></asp:HiddenField>
                                <asp:HiddenField ID="hdnCurExRate" EnableViewState="false" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label2" Text="<%$ Resources:Resource, lblSOPOStatus %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblStatus" CssClass="lblBold" runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:Label ID="Label7" Text="<%$ Resources:Resource, lblARContactNm %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblContactName" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label6" Text="<%$ Resources:Resource, lblCreatedBy %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblCreatedBy" CssClass="lblBold" runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:Label ID="Label8" Text="<%$ Resources:Resource, lblARContactPhone %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblContactPhoneNo" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label5" Text="<%$ Resources:Resource, lblApprovedBy %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblApprovedBy" CssClass="lblBold" runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:Label ID="Label3" Text="<%$ Resources:Resource, POFax %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblFax" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr id="trAddress" runat="server" visible="false">
                            <td align="left">
                                <asp:Label ID="Label1" Text="<%$ Resources:Resource, lblSOReceivingAddress %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblAddress" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="15">
                            <td colspan="2">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <asp:GridView ID="grdOrdItems" runat="server" AllowSorting="false" DataSourceID="sqldsOrdItems"
                        AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0"
                        GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
                        CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="orderItemID"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="ordProductID" HeaderText="<%$ Resources:Resource, POProductID %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff" Visible="false">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="prdIntID" HeaderText="<%$ Resources:Resource, POInternalID %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="prdExtID" HeaderText="<%$ Resources:Resource, POExternalID %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="prdUPCCode" HeaderText="<%$ Resources:Resource, grdPOUPCCode %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="prdName" HeaderText="<%$ Resources:Resource, POProduct %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="200px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdPOShippingAddress %>" HeaderStyle-ForeColor="#ffffff"
                                HeaderStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <div>
                                        <%#funAddressListing(Eval("ordShpWhsCode"))%>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="90px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="sysTaxCodeDescText" NullDisplayText="--" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="<%$ Resources:Resource, POTaxGrp %>" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordProductDiscount" NullDisplayText="0" HeaderStyle-HorizontalAlign="Right"
                                HeaderText="<%$ Resources:Resource, grdSaleDiscount %>" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordProductQty" HeaderText="<%$ Resources:Resource, POQuantity %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordProductUnitPrice" HeaderText="<%$ Resources:Resource, POUnitPrice %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordCurrencyCode" HeaderText="" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="amount" HeaderText="<%$ Resources:Resource, POAmount %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Right">
                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordShpWhsCode" Visible="false" HeaderText="<%$ Resources:Resource, POWarehouseCode %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, POConfirm %>" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-ForeColor="#ffffff" Visible="false">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" Visible="false"
                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                        CommandArgument='<%# Eval("orderItemID") %>' CommandName="Delete" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <div style="display: none;">
                                        <asp:TextBox runat="server" Width="0px" ID="txtDisType" Text='<%# Eval("ordProductDiscountType") %>'>
                                        </asp:TextBox>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="0" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="grid_footer" />
                        <RowStyle CssClass="grid_rowstyle" />
                        <PagerStyle CssClass="grid_footer" />
                        <HeaderStyle CssClass="grid_header" Height="26px" />
                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                        <PagerSettings PageButtonCount="20" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqldsOrdItems" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                </td>
            </tr>
            <tr runat="server" id="trCommission">
                <td colspan="7" align="right" style="padding-right: 10px;">
                    <asp:HiddenField runat="server" ID="hdnSaleAgentID" />
                    <asp:HiddenField runat="server" ID="hdnOrderID" />
                    <br />
                    <asp:Label ID="lblAgent" runat="server" Text="<%$ Resources:Resource, lblCommissionAgentName %>"
                        CssClass="lblBold"></asp:Label>&nbsp;&nbsp;
                    <asp:Label ID="lblCommAgentName" runat="server" Text="" CssClass="lblBold"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblCom" runat="server" Text="<%$ Resources:Resource, lblSalesCommission %>"
                        CssClass="lblBold"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:Label ID="lblCommission"
                            runat="server" Text="" CssClass="lblBold"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="top" align="left">
                    <br />
                    <asp:Label ID="lblProcessList" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOProcessItemList %>"
                        runat="server" Visible="false" /><br />
                    <asp:GridView ID="grdAddProcLst" runat="server" AllowSorting="False" AllowPaging="True"
                        PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0" GridLines="none" AutoGenerateColumns="False"
                        DataSourceID="sqldsAddProcLst" Style="border-collapse: separate;" CssClass="view_grid650"
                        UseAccessibleHeader="False" DataKeyNames="orderItemProcID" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="orderItemProcID" HeaderStyle-ForeColor="#ffffff" HeaderText="ID:"
                                Visible="false" ReadOnly="True" SortExpression="orderItemProcID">
                                <ItemStyle Width="70px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcCode" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdProcessCode %>"
                                Visible="false" ReadOnly="True" SortExpression="ordItemProcCode">
                                <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ProcessDescription" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdProcessDescription %>"
                                ReadOnly="True" SortExpression="ProcessDescription">
                                <ItemStyle Width="250px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcFixedPrice" HeaderText="<%$ Resources:Resource, grdProcessFixedCost %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcPricePerHour" HeaderText="<%$ Resources:Resource, grdProcessCostPerHour %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcPricePerUnit" HeaderText="<%$ Resources:Resource, grdProcessCostPerUnit %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcHours" HeaderText="<%$ Resources:Resource, grdSOTotalHour %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcUnits" HeaderText="<%$ Resources:Resource, grdSOTotalUnit %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ProcessCost" HeaderText="<%$ Resources:Resource, grdSOProcessCost %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdEdit %>" Visible="false"
                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("orderItemProcID") %>'
                                        CommandName="Edit" ImageUrl="~/images/edit_icon.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" Visible="false"
                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                        CausesValidation="false" CommandArgument='<%# Eval("orderItemProcID") %>' CommandName="Delete" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="grid_footer" />
                        <RowStyle CssClass="grid_rowstyle" />
                        <PagerStyle CssClass="grid_footer" />
                        <HeaderStyle CssClass="grid_header" Height="26px" />
                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                        <PagerSettings PageButtonCount="20" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqldsAddProcLst" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                </td>
            </tr>
            <tr height="15">
                <td colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div id="divTerms" runat="server" align="center">
                        <table width="100%" style="border: 0;" border="0" align="center">
                            <tr>
                                <td colspan="7" align="right">
                                    <table align="right" cellpadding="3">
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="lblSubTotal1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOSubTotal %>">
                                                </asp:Label>
                                            </td>
                                            <td align="Right">
                                                <asp:Label ID="lblSubTotal" runat="server" CssClass="lblBold"></asp:Label>
                                            </td>
                                        </tr>
                                        <%=TaxString %>
                                        <tr>
                                            <td align="left">
                                                <asp:Label ID="lblAmount1" Text="<%$ Resources:Resource, lblSOTotal %>" CssClass="lblBold"
                                                    runat="server" />
                                            </td>
                                            <td align="Right">
                                                <asp:Label ID="lblAmount" CssClass="lblBold" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <% If Session("UserModules").ToString.Contains("SAL") = True Or Session("UserModules").ToString.Contains("ADM") = True Then%>
                            <tr height="25">
                                <td class="tdAlign" width="23%">
                                    <asp:Label ID="lblShpToAddress" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShpToAddress %>"
                                        runat="server" />
                                </td>
                                <td width="1%">
                                </td>
                                <td align="left" width="28%">
                                    <asp:TextBox ID="txtShpToAddress" runat="server" TextMode="MultiLine" Rows="5">
                                    </asp:TextBox>
                                    <asp:CustomValidator ID="custValShpToAddress" runat="server" SetFocusOnError="true"
                                        ErrorMessage="<%$ Resources:Resource, msgSOPlzEntShpToAddress %>" ClientValidationFunction="funCheckShpToAddress"
                                        Display="None">
                                    </asp:CustomValidator>
                                </td>
                                <td width="1%">
                                </td>
                                <td class="tdAlign" width="24%">
                                    <asp:Label ID="lblBillToAddress" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOBillToAddress %>"
                                        runat="server" />
                                </td>
                                <td width="1%">
                                </td>
                                <td align="left" width="22%">
                                    <asp:TextBox ID="txtBillToAddress" runat="server" TextMode="MultiLine" Rows="5">
                                    </asp:TextBox>
                                    <asp:CustomValidator ID="custvalBillToAddress" runat="server" SetFocusOnError="true"
                                        ErrorMessage="<%$ Resources:Resource, msgSOPlzEntBillToAddress %>" ClientValidationFunction="funCheckBillToAddress"
                                        Display="None">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblTerms" CssClass="lblBold" Text="<%$ Resources:Resource, POShippingTerms%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtTerms" runat="server" TextMode="MultiLine" Rows="3">
                                    </asp:TextBox>
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="lblNetTerms" CssClass="lblBold" Text="<%$ Resources:Resource, lblSONetTerms%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtNetTerms" runat="server">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblResellerShipBlankPref" CssClass="lblBold" Text="<%$ Resources:Resource, lblResellerShipBlankPref%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:RadioButtonList ID="rblstShipBlankPref" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="<%$ Resources:Resource, lblYes%>" Value="1">
                                        </asp:ListItem>
                                        <asp:ListItem Text="<%$ Resources:Resource, lblNo%>" Value="0" Selected="True">
                                        </asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="lblISWeb" Text="<%$ Resources:Resource, lblSOSalesISWeb%>" CssClass="lblBold"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:RadioButtonList ID="rblstISWeb" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1">
                                        </asp:ListItem>
                                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0" Selected="True">
                                        </asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblShpWarehouse" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingWarehouse %>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="dlShpWarehouse" runat="server" Width="155px">
                                    </asp:DropDownList>
                                    <asp:CustomValidator ID="custvalShpWarehouse" runat="server" SetFocusOnError="true"
                                        ErrorMessage="<%$ Resources:Resource, lblSOPlzSelShippingWarehouse %>" ClientValidationFunction="funCheckShippingWarehouseCode"
                                        Display="None">
                                    </asp:CustomValidator>
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="lblShpTrackNo" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingTrackNo %>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtShpTrackNo" runat="server" Width="150px" MaxLength="25" onkeypress="return disableEnterKey(event)">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr height="25" id="trShippingCost" runat="server" visible="false">
                                <td class="tdAlign">
                                    <asp:Label ID="lblShpCost" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingCost %>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left" colspan="5" valign="middle">
                                    <asp:TextBox ID="txtShpCost" runat="server" Width="150px" MaxLength="6" onkeypress="return disableEnterKey(event)">
                                    </asp:TextBox>
                                    <asp:CompareValidator ID="comvalShpCost" EnableClientScript="true" SetFocusOnError="true"
                                        ControlToValidate="txtShpCost" Operator="DataTypeCheck" Type="double" Display="None"
                                        ErrorMessage="<%$ Resources:Resource, lblSOPlzEntNumericValueInShippingCost %>"
                                        runat="server" />
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblShpDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingDate %>">
                                    </asp:Label>
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtShippingDate" Enabled="true" runat="server" Width="90px" MaxLength="15"
                                        onkeypress="return disableEnterKey(event)">
                                    </asp:TextBox>
                                    <asp:ImageButton ID="imgCalShpDate" CausesValidation="false" runat="server" ToolTip="Click to show calendar"
                                        ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                                    <%=lblPODateFormat %>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender4" TargetControlID="txtShippingDate"
                                        runat="server" PopupButtonID="imgCalShpDate" Format="MM/dd/yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RequiredFieldValidator ID="reqvalShpDate" runat="server" ControlToValidate="txtShippingDate"
                                        ErrorMessage="<%$ Resources:Resource, lblSOPlzEntShippingDate %>" SetFocusOnError="true"
                                        Display="None">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="lblShpCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingCode %>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtShpCode" runat="server" Width="150px" MaxLength="15" onkeypress="return disableEnterKey(event)">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblNotes" CssClass="lblBold" Text="<%$ Resources:Resource, PONotes%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left" colspan="5">
                                    <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Rows="3" Width="550px">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblCustPO" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOCustomerPO%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtCustPO" runat="server" Width="130px" MaxLength="35" onkeypress="return disableEnterKey(event)">
                                    </asp:TextBox>
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="lblPOStatus" CssClass="lblBold" Text="<%$ Resources:Resource, lblStatus%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="dlStatus" runat="server" Width="135px">
                                        <%--  <asp:ListItem Value="" Text="<%$ Resources:Resource, liSelectOrderStatus %>" />
                    <asp:ListItem Value="N" Text="<%$ Resources:Resource, liCreated %>" />
                    <asp:ListItem Value="A" Text="<%$ Resources:Resource, liApproved %>"/>
                    <asp:ListItem Value="P" Text="<%$ Resources:Resource, liInProcess %>"/>
                    <asp:ListItem Value="H" Text="<%$ Resources:Resource, liHeld %>"/>
                    <asp:ListItem Value="S" Text="<%$ Resources:Resource, liShipped %>"/>
                   <asp:ListItem Value="D" Text="<%$ Resources:Resource, liClosedInvoiced %>"/>
                    <asp:ListItem Value="I" Text="<%$ Resources:Resource, liInvoiced %>"/> 
                    <asp:ListItem Value="C" Text="<%$ Resources:Resource, liClosed %>"/>--%>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnPreStatus" runat="server" />
                                    <span class="style1">*</span>
                                    <asp:CustomValidator ID="custvalStatus" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, msgSOPlzSelStatus %>"
                                        ClientValidationFunction="funCheckStatus" Display="None">
                                    </asp:CustomValidator>
                                    <%-- <asp:CustomValidator ID="custvalStatusAndPO" runat="server" SetFocusOnError="true"
                 ErrorMessage="<%$ Resources:Resource, msgSOPlzEntCustomerPO %>" 
                 ClientValidationFunction="funCheckStatusAndCustPO" Display="None" >
                 </asp:CustomValidator>--%>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblQutExpDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOQutExpDate %>">
                                    </asp:Label>
                                </td>
                                <td>
                                </td>
                                <td align="left" valign="middle">
                                    <asp:TextBox ID="txtQutExpDate" Enabled="true" runat="server" Width="90px" MaxLength="15"
                                        onkeypress="return disableEnterKey(event)">
                                    </asp:TextBox>
                                    <asp:ImageButton ID="imgCalQutExpDate" CausesValidation="false" runat="server" ToolTip="Click to show calendar"
                                        ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                                    <%=lblPODateFormat %>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender5" TargetControlID="txtQutExpDate"
                                        runat="server" PopupButtonID="imgCalQutExpDate" Format="MM/dd/yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RequiredFieldValidator ID="reqvalQutExpDate" runat="server" ControlToValidate="txtQutExpDate"
                                        ErrorMessage="<%$ Resources:Resource, lblSOPlzEntQutExpDate %>" SetFocusOnError="true"
                                        Display="None">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <% End If%>
                        </table>
                    </div>
                </td>
            </tr>
            <% If Session("UserModules").ToString.Contains("SAL") = True Then%>
            <tr height="30" id="trCurrency" runat="server" visible="false">
                <td class="tdAlign">
                    <asp:Label ID="lblPOCurrencyCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOCurrencyCode%>"
                        runat="server" />
                </td>
                <td class="tdAlignLeft">
                    <asp:DropDownList ID="dlCurrencyCode" runat="server" Width="135px">
                    </asp:DropDownList>
                    <span class="style1">*</span>
                    <asp:CustomValidator ID="custvalCurrencyCode" runat="server" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:Resource, custvalPOCurrencyCode%>" ClientValidationFunction="funCheckCurrencyCode"
                        Display="None">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr height="30" id="trExchageRate" runat="server" visible="false">
                <td class="tdAlign">
                    <asp:Label ID="lblPOExRate" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOExRate%>"
                        runat="server" />
                </td>
                <td class="tdAlignLeft">
                    <asp:TextBox ID="txtExRate" runat="server" Width="130px" MaxLength="5">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td height="10">
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <table width="100%">
                        <tr>
                            <td height="20" align="center" width="40%">
                            </td>
                            <td width="60%">
                                <div class="buttonwrapper">
                                    <a id="cmdBack" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                        <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdInvBack%></span></a></div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="30" valign="middle" runat="server" visible="false">
                <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                    <asp:Button runat="server" ID="cmdSave" CssClass="imgSave" />&nbsp;
                    <% If (Session("UserModules").ToString.Contains("QOA") = True Or Session("UserModules").ToString.Contains("ADM") = True) Then%>
                    <asp:Button runat="server" ID="cmdEditSO" Style="width: 80px; height: 26px; border-width: 0px;
                        background: url(../Images/edit_icon.png);" CausesValidation="false" />
                    <% End If%>
                    &nbsp;
                    <asp:Button runat="server" ID="cmdGnratInvoice" CssClass="imgIntGenerateInvoice" />&nbsp;&nbsp;
                    <asp:Button runat="server" ID="cmdReset" Style="width: 80px; height: 26px; border-width: 0px;
                        background: url(../Images/Back.png);" CausesValidation="false" />&nbsp;&nbsp;
                    <asp:Button runat="server" ID="cmdCpyOrder" CssClass="imgDupOrder" OnClick="cmdCpyOrder_Click" />
                </td>
            </tr>
            <% Else%>
            <tr height="30">
                <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                    <table width="100%">
                        <tr>
                            <td height="20" align="center" width="40%">
                            </td>
                            <td width="60%">
                                <div class="buttonwrapper">
                                    <a id="CmdcssBack2" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                        <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdInvBack%></span></a></div>
                            </td>
                        </tr>
                    </table>
                    <%-- <asp:HyperLink ID="hlnkTemplate" runat="server" Target=_parent NavigateUrl="Default.aspx" Text="">
                <img id="img1" runat="server" alt="" src="../images/Back.png" border=0 />
            </asp:HyperLink>--%>
                </td>
            </tr>
            <% End If%>
        </table>
        <div id="divMail" runat="server" style="background: url(../images/popupbg.jpg); display: none;
            position: absolute; left: 572px; top: 170px; width: 395px; border: 3px; border-color: black;">
            <table width="100%" cellpadding="4px" style="border-color: #356083; border-style: solid;
                border-width: thin; background: url(../images/popupBgHdr.jpg);">
                <tr>
                    <td width='80%' align="left" valign="middle">
                        <span class='lblBold'>
                            <%=lblPOMail %></span>
                    </td>
                    <td width="20%" align="right">
                        <a href="javascript:funClosePopUp('<%=divMail.ClientID %>');">
                            <img border="0" src='../Images/closeprd.png' width="24" height="24" /></a>
                    </td>
            </table>
            <table width="100%" class='' style="border-color: #356083; border-style: solid; border-width: thin;
                border-top: 0px;">
                <tr>
                    <td class="tdAlign">
                        <asp:Label ID="lblMailTo" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOMailTo%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtMailTo" runat="server" Width="300px" ValidationGroup="mail">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdAlign">
                        <asp:Label ID="lblMailSubject" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOSubject%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtMailSubject" runat="server" Width="300px" ValidationGroup="mail">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdAlign">
                        <asp:Label ID="lblMailMessage" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOMessage%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtMailMessage" runat="server" TextMode="MultiLine" Rows="4" Width="300px"
                            ValidationGroup="mail">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                        <asp:Button runat="server" ID="cmdMail" CssClass="imgSend" ValidationGroup="mail" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="divFax" runat="server" style="background: url(../images/popupbg.jpg); display: none;
            position: absolute; left: 572px; top: 170px; width: 395px; border: 3px; border-color: black;">
            <table width="100%" cellpadding="4px" style="border-color: #356083; border-style: solid;
                border-width: thin; background: url(../images/popupBgHdr.jpg);">
                <tr>
                    <td width='80%' align="left" valign="middle">
                        <span class='lblBold'>
                            <%=lblPOFax %></span>
                    </td>
                    <td width="20%" align="right">
                        <a href="javascript:funClosePopUp('<%=divFax.ClientID %>');">
                            <img border="0" src='../Images/closeprd.png' width="24" height="24" /></a>
                    </td>
                </tr>
            </table>
            <table width="100%" class='' style="border-color: #356083; border-style: solid; border-width: thin;
                border-top: 0px;">
                <tr>
                    <td class="tdAlign">
                        <asp:Label ID="lblFaxTo" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOFaxTo%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtFaxTo" runat="server" Width="300px" ValidationGroup="Fax">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdAlign">
                        <asp:Label ID="lblAttention" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOAttention%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtAttention" runat="server" Width="300px" ValidationGroup="Fax">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdAlign">
                        <asp:Label ID="lblFaxSubject" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOSubject%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtFaxSubject" runat="server" Width="300px" ValidationGroup="Fax">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdAlign">
                        <asp:Label ID="lblFaxMessage" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOMessage%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtFaxMessage" runat="server" TextMode="MultiLine" Rows="4" Width="300px"
                            ValidationGroup="Fax">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                        <asp:Button runat="server" ID="cmdFax" CssClass="imgSend" ValidationGroup="Fax" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:ValidationSummary ID="valsSales" runat="server" ShowMessageBox="true" ShowSummary="false" />
        <asp:ValidationSummary ID="valsMail" runat="server" ShowMessageBox="true" ValidationGroup="mail"
            ShowSummary="false" />
        <asp:ValidationSummary ID="valsFax" runat="server" ShowMessageBox="true" ShowSummary="false"
            ValidationGroup="Fax" />
    </div>
    <br />
    <br />
    <script type="text/javascript" language="javascript">

        function funConfirm(id) {
            var frm = document.forms[0];
            for (i = 0; i < frm.elements.length; i++) {
                if (frm.elements[i].type == "checkbox") {
                    if (frm.elements[i].checked == false) {
                        alert('<% =Resources.Resource.POConfirmItems %>');
                        return false;
                    }
                }
            }
            return true;
        }
        function funCheckCurrencyCode(source, args) {
            if (document.getElementById('<%=dlCurrencyCode.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
        function funCheckShippingWarehouseCode(source, args) {
            if (document.getElementById('<%=dlShpWarehouse.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
        function funCheckStatus(source, args) {
            if (document.getElementById('<%=dlStatus.ClientID%>').selectedIndex == 0) {
                document.getElementById('<%=dlStatus.ClientID%>').focus();
                args.IsValid = false;
            }
        }
        function funCheckStatusAndCustPO(source, args) {
            if (document.getElementById('<%=dlStatus.ClientID%>').value == "P" &&
        document.getElementById('<%=txtCustPO.ClientID%>').value == "") {
                document.getElementById('<%=txtCustPO.ClientID%>').focus();
                args.IsValid = false;
            }
        }
        function funCheckShpToAddress(source, args) {
            if (document.getElementById('<%=txtShpToAddress.ClientID%>').value == "") {
                args.IsValid = false;
            }
        }
        function funCheckBillToAddress(source, args) {
            if (document.getElementById('<%=txtBillToAddress.ClientID%>').value == "") {
                args.IsValid = false;
            }
        }
        function openPDF(strOpen) {
            open(strOpen, "", 'height=365,width=810,left=180,top=150,resizable=yes,location=no,scrollbars=yes,menubar=yes,toolbar=yes,status=no');
            return false;
        }
        function funClosePopUp(strOption) {
            document.getElementById(strOption).style.display = "none";
        }
        function popUp(strOption) {
            document.getElementById(strOption).style.display = "block";
            return false;
        }
        function popAlert(strOption) {
            alert(strOption);
            return false;
        }
    </script>
</asp:Content>

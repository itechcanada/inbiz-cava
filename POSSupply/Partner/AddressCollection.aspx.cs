﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Trirand.Web.UI.WebControls;

using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

using Newtonsoft.Json;

public partial class Partner_AddressCollection : System.Web.UI.Page
{
    Addresses _address = new Addresses();
    CountryStateTaxGroup _country = new CountryStateTaxGroup();
    DbHelper dbHelp = new DbHelper(true);

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack && grdAddress.AjaxCallBackMode == AjaxCallBackMode.None)
        if (!IsPostBack)
        {
            MessageState.SetGlobalMessage(MessageType.Success, "");
            _country.PopulateListControlWithCountry(dbHelp, ddlCountry, new ListItem(""));
            _country.PopulateListControlWithStateCodes(dbHelp, ddlCountry.SelectedValue, ddlState, new ListItem(""));
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "_countryStateList", string.Format("var _countryStateList={0};", JsonConvert.SerializeObject(_country.GetAllCountryStateList(dbHelp))), true);
            hdnSrc.Value = this.AddressSrc;
            hdnCtrl.Value = this.TargetCtrl;
            hdnAddType.Value = this.AddressType;
        }
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "_countryStateList", string.Format("var _countryStateList={0};", JsonConvert.SerializeObject(_country.GetAllCountryStateList(dbHelp))), true);
        if (this.AddressSrc == "cust")
        {
            grdAddress.Columns[11].Visible = false;
        }
       
    }

    public int CustomerID
    {
        get
        {
            int customerID = 0;
            int.TryParse(Request.QueryString["custID"], out customerID);
            return customerID;
        }
    }

    public string AddressType
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["addType"]);
        }
    }

    private int AddressID
    {
        get
        {
            int id = 0;
            int.TryParse(hdnIdToEdit.Value, out id);
            return id;
        }
    }

    public string AddressSrc
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["src"]);
        }
    }

    protected void grdAddress_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        sdsAddress.SelectCommand = _address.GetSql(sdsAddress.SelectParameters, this.CustomerID, this.AddressType);
    }

    protected void grdAddress_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 10)
        {
            int iPartnerID = BusinessUtility.GetInt(e.RowKey);
            string sHtml = string.Format("<div class='submit' style='width:100%;text-align:center;'> <a href='#'  id='" + "btn" + BusinessUtility.GetString(iPartnerID) + "' name='" + "btn" + BusinessUtility.GetString(iPartnerID) + "'   onclick =  rowSelect('{0}') >" + Resources.Resource.edit + "</a>  </div>", BusinessUtility.GetString(iPartnerID));
            e.CellHtml = sHtml;
        }
        else if (e.ColumnIndex == 11)
        {
            int iPartnerID = BusinessUtility.GetInt(e.RowKey);
            string sHtml = string.Format("<div class='submit' style='width:100%;text-align:center;'> <input type='button' class='updateQty'  id='" + "btn" + BusinessUtility.GetString(iPartnerID) + "' name='" + "btn" + BusinessUtility.GetString(iPartnerID) + "'  value= '" + Resources.Resource.lnkSelectAddress + "' onclick =  SelectAddress('{0}') /> </div>", BusinessUtility.GetString(iPartnerID));
            e.CellHtml = sHtml;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                Partners objPartners = new Partners();
                objPartners.PopulateObject(this.CustomerID);



                _address.AddressCity = txtCity.Text;
                _address.AddressCountry = txtCountry.Text;
                _address.AddressLine1 = txtAddressLine1.Text;
                _address.AddressLine2 = txtAddressLine2.Text;
                _address.AddressLine3 = string.Empty;
                _address.AddressPostalCode = txtPostalCode.Text;
                _address.AddressState = txtState.Text;
                if ((objPartners.PartnerType == (int)PartnerTypeIDs.Distributer))
                {
                    _address.AddressRef = AddressReference.DISTRIBUTER;
                }
                else
                {
                    _address.AddressRef = AddressReference.END_CLIENT;
                }
                _address.AddressSourceID = this.CustomerID;
                _address.AddressType = this.AddressType;
                _address.AddressID = this.AddressID;
                _address.FirstName = txtFirstName.Text;
                _address.LastName = txtLastName.Text;
                _address.SaveCustomerAddresse();

                MessageState.SetGlobalMessage(MessageType.Information, Resources.Resource.lblAddressSaved);
                ResetPage();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
            }
        }
    }

    protected void ResetPage()
    {
        ddlCountry.SelectedValue = "";
        ddlState.SelectedValue = "";
        txtAddressLine1.Text = "";
        txtAddressLine2.Text = "";
        txtCity.Text = "";
        txtCountry.Text = "";
        txtState.Text = "";
        txtPostalCode.Text = "";
        txtFirstName.Text = "";
        txtLastName.Text = "";
        hdnIdToEdit.Value = "";
        //txtFirstName.Focus();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        ResetPage();
    }

    public StatusProductType ProductType
    {
        get
        {
            return QueryStringHelper.GetProductType("ptype");
        }
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlState.Items.Clear();
        if (ddlCountry.SelectedIndex > 0)
        {
            _country.PopulateListControlWithStateCodes(null, ddlCountry.SelectedValue, ddlState, new ListItem(""));
        }
    }

    private string PartnerType
    {
        get
        {
            return QueryStringHelper.GetPartnerType("pType");
        }
    }

    private int PartnerTypeID
    {
        get
        {
            switch (this.PartnerType)
            {
                case StatusCustomerTypes.END_CLINET:
                    return (int)PartnerTypeIDs.EndClient;
                default:
                    return (int)PartnerTypeIDs.Distributer;
            }
        }
    }

    public string AddressRef
    {
        get
        {
            switch (this.PartnerType)
            {
                case StatusCustomerTypes.END_CLINET:
                    return AddressReference.END_CLIENT;
                default:
                    return AddressReference.DISTRIBUTER;
            }
        }
    }

    private string TargetCtrl
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["ctrl"]);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }


    #region Web Methods
    [System.Web.Services.WebMethod]
    public static Addresses GetCustomerAddress(int id)
    {
        Addresses objAddress = new Addresses();
        objAddress.GetAddress(id);
        return objAddress;
    }
    #endregion
}
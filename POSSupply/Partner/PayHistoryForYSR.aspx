<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PayHistoryForYSR.aspx.vb"
    Inherits="Partner_PayHistoryForYSR" %>

<%@ Import Namespace="Resources.Resource" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <%=prjCompanyTitle%></title>
    <link href="../Css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div align="left">
        <table width="790" border="0" class="table">
            <tr>
                <td class="titleBgColor" height="20" align="center" colspan="3">
                    <asp:Label CssClass="lblBold" runat="server" ID="lblTitle" Text="<%$ Resources:Resource, lblYSRTitle %>"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="10">
                </td>
            </tr>
            <tr>
                <td width="15%" align="right">
                    <asp:Label CssClass="lblBold" runat="server" ID="lblCName" Text="<%$ Resources:Resource, lblCompanyName %>"></asp:Label>
                </td>
                <td width="1%">
                </td>
                <td width="84%" align="left">
                    <asp:Label runat="server" ForeColor="black" ID="lblComName"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="10">
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="3">
                    <asp:Panel runat="server" ID="pnlStatus" GroupingText="<%$ Resources:Resource, lblYSRStatus %>"
                        Width="780">
                        <asp:GridView ID="grdStatus" runat="server" AllowSorting="True" DataSourceID="sqldsStatus"
                            AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="3"
                            EmptyDataText="<%$ Resources:Resource, lblYSRNoDataFound %>" GridLines="both"
                            AutoGenerateColumns="False" UseAccessibleHeader="False" DataKeyNames="companyID"
                            Width="765px">
                            <Columns>
                                <asp:BoundField DataField="statusdttm" HeaderText="<%$ Resources:Resource, grdYSRCreatedDate %>"
                                    ReadOnly="True" DataFormatString="{0: dd MMM yyyy}" SortExpression="statusdttm">
                                    <ItemStyle Width="130px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="statusCode" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grdYSRCode %>"
                                    ReadOnly="True" SortExpression="statusCode">
                                    <ItemStyle Width="130px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="statusComp" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grdYSRStatusComp %>"
                                    ReadOnly="True" SortExpression="statusComp">
                                    <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CompanyID" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grdYSRCompanyID %>"
                                    ReadOnly="True" SortExpression="CompanyID">
                                    <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="statusAgent" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grdYSRstatusAgent %>"
                                    ReadOnly="True" SortExpression="statusAgent">
                                    <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="statusVerifiedBy" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grdYSRstatusVerifiedBy %>"
                                    ReadOnly="True" SortExpression="statusVerifiedBy">
                                    <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="statusTape" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grdYSRstatusTape %>"
                                    ReadOnly="True" SortExpression="statusTape">
                                    <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="statusCollector" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grdYSRstatusCollector %>"
                                    ReadOnly="True" SortExpression="statusCollector">
                                    <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                            </Columns>
                            <FooterStyle CssClass="grdFooterStyle" />
                            <RowStyle ForeColor="#333333" />
                            <EditRowStyle BackColor="#999999" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <PagerStyle BackColor="#5F6062" ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle"
                                CssClass="lblBold" />
                            <HeaderStyle BackColor="#5F6062" ForeColor="White" Font-Bold="true" HorizontalAlign="Left"
                                VerticalAlign="Middle" Height="15" />
                            <AlternatingRowStyle BackColor="#EDEDEE" ForeColor="#284775" />
                            <PagerSettings PageButtonCount="20" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqldsStatus" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td height="20">
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="3">
                    <asp:Panel runat="server" ID="pnlPayment" GroupingText="<%$ Resources:Resource, lblYSRPayment %>"
                        Width="780">
                        <asp:GridView ID="grdPayament" runat="server" AllowSorting="True" DataSourceID="sqldsPayment"
                            AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="3"
                            EmptyDataText="<%$ Resources:Resource, lblYSRNoDataFound %>" GridLines="both"
                            AutoGenerateColumns="False" UseAccessibleHeader="False" DataKeyNames="companyID"
                            Width="765px">
                            <Columns>
                                <asp:BoundField DataField="payInvNo" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grdYSRpayInvNo %>"
                                    ReadOnly="True" SortExpression="payInvNo">
                                    <ItemStyle Width="90px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="payDateTm" HeaderText="<%$ Resources:Resource, grdYSRPayDate %>"
                                    ReadOnly="True" DataFormatString="{0: dd MMM yyyy}" SortExpression="payDateTm">
                                    <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="payAppDesc" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grdYSRpayAppDesc %>"
                                    ReadOnly="True" SortExpression="payAppDesc">
                                    <ItemStyle Width="110px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="payCollector" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grdYSRpayCollector %>"
                                    ReadOnly="True" SortExpression="payCollector">
                                    <ItemStyle Width="90px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="payRef" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grdYSRpayRef %>"
                                    ReadOnly="True" SortExpression="payRef">
                                    <ItemStyle Width="90px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="payForm" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grdYSRpayForm %>"
                                    ReadOnly="True" SortExpression="payForm">
                                    <ItemStyle Width="90px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="payAmt" HeaderStyle-HorizontalAlign="right" HeaderText="<%$ Resources:Resource, grdYSRpayInvAmt %>"
                                    ReadOnly="True" SortExpression="payAmt">
                                    <ItemStyle Width="90px" Wrap="true" HorizontalAlign="right" />
                                </asp:BoundField>
                                <asp:BoundField DataField="payTrans" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grdYSRpayTrans %>"
                                    ReadOnly="True" SortExpression="payTrans">
                                    <ItemStyle Width="90px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="payUser" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grdYSRpayUser %>"
                                    ReadOnly="True" SortExpression="payUser">
                                    <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                            </Columns>
                            <FooterStyle CssClass="grdFooterStyle" />
                            <RowStyle ForeColor="#333333" />
                            <EditRowStyle BackColor="#999999" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <PagerStyle BackColor="#5F6062" ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle"
                                CssClass="lblBold" />
                            <HeaderStyle BackColor="#5F6062" ForeColor="White" Font-Bold="true" HorizontalAlign="Left"
                                VerticalAlign="Middle" Height="15" />
                            <AlternatingRowStyle BackColor="#EDEDEE" ForeColor="#284775" />
                            <PagerSettings PageButtonCount="20" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="sqldsPayment" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Imports System.Data.Odbc
Partial Class Partner_ViewContact
    Inherits BasePage
    Public objDataclass As New clsDataClass
    Private clsStatus As New clsSysStatus
    ' On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If

        lblMsg.Text = ""
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If
        
        lblTitle.Text = CMTitleContacts

        If Not Page.IsPostBack Then
            clsStatus.subGetStatus(ddlStatus, "CU", "dlSch")
            clsStatus.subGetStatus(dlSearch, "CO", "dlSh1")
            subGetQuerryString()
            subFillGrid()
        End If

        clsGrid.MaintainParams(Page, grdContact, pnlSearchName, lblMsg, imgSearch)

        clsCSV.ExportCSV(Page, "Contact", grdContact, New Integer() {1, 4, 6, 7, 8}, imgCsv)

        txtSearch.Focus()
    End Sub
    'get the value 
    Public Sub subGetQuerryString()
        If Session("ContSearch") <> "" Or Session("ContStatus") <> "1" Or Session("ContSearchtxt") <> "" Then
            ddlStatus.SelectedValue = Session("ContStatus")
            dlSearch.SelectedValue = Session("ContSearch")
            txtSearch.Text = Session("ContSearchtxt")
        End If
        Session.Remove("ContStatus")
        Session.Remove("ContSearch")
        Session.Remove("ContSearchtxt")
    End Sub
    'Populate Grid
    Public Sub subFillGrid()
        Dim objC As New clsPartnerContacts
        sqldsContact.SelectCommand = objC.funFillGrid(ddlStatus.SelectedValue, 0, dlSearch.SelectedValue, txtSearch.Text)  'fill Contacts Record
        objC = Nothing
    End Sub
    'On Page Index Change
    Protected Sub grdContact_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdContact.PageIndexChanging
        subFillGrid()
    End Sub
    ' On Row Command
    Protected Sub grdContact_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdContact.RowCommand
        If e.CommandName = "contact" Then
            subSetSession()
            Dim strValue() As String = e.CommandArgument.ToString.Split(":")
            Response.Redirect("AddEditContact.aspx?PartnerID=" & strValue(1) & "&ContactID=" & strValue(0) & "&disable=yes")
        End If
    End Sub

    'On Row Data Bound
    Protected Sub grdContact_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdContact.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & CMConfirmDeleteContact & " ')")
            Dim lnkContactName As LinkButton = CType(e.Row.FindControl("lnkContactName"), LinkButton)
            Dim txtContactName As TextBox = CType(e.Row.FindControl("txtContactName"), TextBox)
            lnkContactName.Text = txtContactName.Text
        End If
    End Sub
    'Deleting
    Protected Sub grdContact_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdContact.RowDeleting
        Dim objC As New clsPartnerContacts
        Dim strContactID As String = ""
        strContactID = grdContact.DataKeys(e.RowIndex).Value.ToString()
        sqldsContact.DeleteCommand = objC.funDeleteContact(strContactID) 'Delete Contact
        objC = Nothing
        subFillGrid()
        Session.Add("Msg", CMContactDeletedSuccessfully)
        lblMsg.Text = CMContactDeletedSuccessfully
    End Sub
    'Editing
    Protected Sub grdContact_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdContact.RowEditing
        Dim strContactID As String = grdContact.DataKeys(e.NewEditIndex).Value.ToString()
        Dim txtPartnerID As TextBox = CType(grdContact.Rows(e.NewEditIndex).FindControl("txtPartnerID"), TextBox)
        subSetSession()

        Response.Redirect("~/partner/AddEditContact.aspx?PartnerID=" & txtPartnerID.Text & "&ContactID=" & strContactID & "&mp=1")
    End Sub
    'Set the values in Session variables
    Public Sub subSetSession()
        Session("ContStatus") = ddlStatus.SelectedValue
        Session("ContSearch") = dlSearch.SelectedValue
        Session("ContSearchtxt") = txtSearch.Text.Trim
    End Sub
    'Sorting
    Protected Sub grdContact_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdContact.Sorting
        subFillGrid()
    End Sub
    ' Sql Data Source Event Track
    Protected Sub sqldsContact_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsContact.Selected
        If e.AffectedRows = 0 Then
            lblMsg.Text = CMNoDataFound
        End If
    End Sub
    ' On Search Image Click
    Protected Sub imgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
        subFillGrid()
        lblMsg.Text = ""
        txtSearch.Focus()
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
End Class

Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Data.Odbc
Partial Class Partner_Notes
    Inherits BasePage
    Private objPart As New clsPartners
    Public objDataclass As New clsDataClass
    Private objLead As New clsleadLog
    Private objSO As New clsOrders
    Private objActRcv As New clsAccountReceivable
    Private objCL As New clsCollectionLog
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Not Page.IsPostBack Then
            If Request.QueryString("PartnerID") <> "" Then
                'subPartnerFillGrid()
                subSaleActivityFillGrid()
                subSOFillGrid()
                subARFillGrid()
                subColFillGrid()
                objPart.PartnerID = Request.QueryString("PartnerID")
                objPart.getPartnersInfo()
                lblCustomerName.Text = objPart.PartnerLongName
                lblNotes.Text = objPart.PartnerNote
            End If
        End If
        lblHeading.Text = prnNote
        lblPartnerHeading.Text = lblCustomerNotes
        lblTitleLogDetails.Text = lblSalesActivityFollowup
        lblSo.Text = lblSalesOrdernote
        lblAR.Text = lblAccountRecieableHistory
        lblCol.Text = lblCollectionNote
    End Sub
    'Populate Grid
    Public Sub subPartnerFillGrid()
        sqldsPartner.SelectCommand = objPart.funFillGrid("", "", "", "", Request.QueryString("PartnerID"))  'fill User Record
    End Sub
    'Populate Grid
    Public Sub subSaleActivityFillGrid()
        objLead.AssignLeadsID = Request.QueryString("PartnerID")
        sqldsLeadsLog.SelectCommand = objLead.funFillGrid()  'fill Orders
    End Sub
    'Populate Grid
    Public Sub subSOFillGrid()
        sqldsOrder.SelectCommand = objSO.funFillGridForAllOrders("", "", "")  'fill Oorders
    End Sub

    Protected Sub grdLeadsLog_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdLeadsLog.PageIndexChanging
        subSaleActivityFillGrid()
    End Sub
    'On Row Data Bound
    Protected Sub grdLeadsLog_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdLeadsLog.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim lblLog As Label = CType(e.Row.FindControl("lblLog"), Label)
            lblLog.Text = CType(e.Row.FindControl("lblLog"), Label).Text
            lblLog.Text = lblLog.Text.Replace(Environment.NewLine, "<br />")
        End If
    End Sub
    'Populate Grid
    Public Sub subARFillGrid()
        sqldsAR.SelectCommand = objActRcv.funARNotes(Request.QueryString("PartnerID"))  'fill Orders
    End Sub
    'Populate Grid
    Public Sub subColFillGrid()
        sqldsCol.SelectCommand = objCL.funFillGrid(Request.QueryString("PartnerID"))  'fill Orders
    End Sub

    Protected Sub grdCol_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdCol.PageIndexChanging
        subColFillGrid()
    End Sub
    'On Row Data Bound
    Protected Sub grdCol_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdCol.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim lblLog As Label = CType(e.Row.FindControl("lblLog"), Label)
            lblLog.Text = CType(e.Row.FindControl("lblLog"), Label).Text
            lblLog.Text = lblLog.Text.Replace(Environment.NewLine, "<br />")
        End If
    End Sub
    Protected Sub sqldsAR_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsAR.Selected
        If e.AffectedRows = 0 Then
            lblARMsg.Text = NoDataFound
        End If
    End Sub
    Protected Sub sqldsCol_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsCol.Selected
        If e.AffectedRows = 0 Then
            lblColMsg.Text = NoDataFound
        End If
    End Sub
    Protected Sub sqldsLeadsLog_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsLeadsLog.Selected
        If e.AffectedRows = 0 Then
            lblLeads.Text = NoDataFound
        End If
    End Sub
    Protected Sub sqldsOrder_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsOrder.Selected
        If e.AffectedRows = 0 Then
            lblOrder.Text = NoDataFound
        End If
    End Sub
    Protected Sub sqldsPartner_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsPartner.Selected
        If e.AffectedRows = 0 Then
            lblPartner.Text = NoDataFound
        End If
    End Sub

    Protected Sub grdAR_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAR.PageIndexChanging
        subARFillGrid()
    End Sub

    Protected Sub grdAR_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAR.Sorting
        subARFillGrid()
    End Sub

    Protected Sub grdCol_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdCol.Sorting
        subColFillGrid()
    End Sub

    Protected Sub grdLeadsLog_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdLeadsLog.Sorting
        subSaleActivityFillGrid()
    End Sub

    Protected Sub grdOrder_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdOrder.PageIndexChanging
        subSOFillGrid()
    End Sub

    Protected Sub grdOrder_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdOrder.Sorting
        subSOFillGrid()
    End Sub

    Protected Sub grdPartner_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdPartner.PageIndexChanging
        subPartnerFillGrid()
    End Sub

    Protected Sub grdPartner_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdPartner.Sorting
        subPartnerFillGrid()
    End Sub

    Protected Sub cmdBack_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.ServerClick
        'subQueryString()        
        If Request.QueryString.AllKeys.Contains("returnUrl") Then
            Response.Redirect(Request.QueryString("returnUrl"))
        Else
            subQueryString()
        End If
    End Sub

    Protected Sub cmdBack2_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack2.ServerClick
        'subQueryString()
        If Request.QueryString.AllKeys.Contains("returnUrl") Then
            Response.Redirect(Request.QueryString("returnUrl"))
        Else
            subQueryString()
        End If
    End Sub
    Public Sub subQueryString()
        If Request.QueryString("AR") = "1" Then
            Response.Redirect("~/AccountsReceivable/InvoicePayment.aspx" & Request.UrlReferrer.Query)
        ElseIf Request.QueryString("CD") = "1" Then
            Response.Redirect("~/CollectionAgent/ViewFollowuplog.aspx" & Request.UrlReferrer.Query)
        ElseIf Request.QueryString("IN") = "1" Then
            Response.Redirect("~/Invoice/ViewInvoiceDetails.aspx" & Request.UrlReferrer.Query)
        ElseIf Request.QueryString("SA") = "1" Then
            Response.Redirect("~/Sales/Approval.aspx" & Request.UrlReferrer.Query)
        Else
            Response.Redirect("~/partner/AddEditCustomer.aspx" & Request.UrlReferrer.Query)
        End If
    End Sub
End Class

<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="AddEditContact.aspx.vb" Inherits="Partner_AddEditContact" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="Resources.Resource" %>
<%@ Register src="../Controls/CommonSearch/CRMSearch.ascx" tagname="CRMSearch" tagprefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();
        $('#divMainContainerTitle1').corner();
        $('#divMainContainerTitle2').corner();
        $('#divMainContainerTitle3').corner();
        $('#divMainContainerTitle4').corner();
        $('#divMainContainerTitle5').corner();
    </script>
    <uc1:CRMSearch ID="CRMSearch1" runat="server" />
</asp:Content>
<asp:Content ID="cntAddEditContact" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal></h2>
                </td>
                <td style="float: right;">
                    <div class="buttonwrapper">
                        <a id="cmdBackContactView" visible="false" runat="server" causesvalidation="false"
                            class="ovalbutton" href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                <%=Resources.Resource.cmdCssBack%>
                            </span></a>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="divMainContent">
        <table id="tblCrMoPart" runat="server" style="width: 100%;">
            <tr>
                <td colspan="3">
                    <asp:UpdatePanel ID="upnlMsg" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                            <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td valign="top" style="text-align: left; width: 120px;">
                    <asp:Label ID="lblCreated" CssClass="lblBold" runat="server" /><br />
                    <asp:Label ID="lblCreatedDate" CssClass="lblSimple" runat="server" /><br />
                    <asp:Label ID="lblCreatedBy" CssClass="lblSimple" runat="server" />
                </td>
                <td valign="top" style="text-align: left; width: 120px;">
                    <asp:Label ID="lblModified" CssClass="lblBold" runat="server" /><br />
                    <asp:Label ID="lblModifiedDate" CssClass="lblSimple" runat="server" /><br />
                    <asp:Label ID="lblModifiedBy" CssClass="lblSimple" runat="server" />
                </td>
            </tr>
        </table>
        <table style="width: 100%;">
            <tr>
                <td valign="top">
                    <table width="100%" style="border: 0;" bgcolor="#FFFFFF" class="table">
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblPN" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMPartnerName%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:Label ID="lblPartnerName" CssClass="lblBold" runat="server" Font-Size="12px" />
                            </td>
                        </tr>
                        <tr class="trheight" id="trActive" runat="server" visible="false">
                            <td class="tdAlign">
                                <asp:Label ID="lblIsActive" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMIsActive%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:RadioButtonList ID="rblstActive" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="<%$ Resources:Resource, lblCMYes%>" Value="1" Selected="True">
                                    </asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:Resource, lblCMNo%>" Value="0"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td width="28%" class="tdAlign">
                                <asp:Label ID="lblSalutation" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMSalutation%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft" width="72%">
                                <asp:TextBox ID="txtSalutation" runat="server" MaxLength="10" Width="200px" />
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td width="28%" class="tdAlign">
                                <asp:Label ID="lblLastName" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMLastName%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft" width="72%">
                                <asp:TextBox ID="txtLastName" runat="server" MaxLength="35" Width="200px" />
                                <span class="style1">*</span>
                                <asp:RequiredFieldValidator ID="reqvalLastName" runat="server" ControlToValidate="txtLastName"
                                    ErrorMessage="<%$ Resources:Resource, reqvalCMLastName%>" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblFirstName" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMFirstName%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtFirstName" runat="server" Width="200px" MaxLength="35">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <img alt="Address Line1" src="../Images/Adress1.png" /><asp:Label ID="lblAddLine1"
                                    CssClass="lblBold" Visible="false" Text="<%$ Resources:Resource, lblCMAddLine1%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtAddLine1" runat="server" Width="200px" MaxLength="35">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblAddLine2" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMAddLine2%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtAddLine2" runat="server" Width="200px" MaxLength="35">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblCity" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMCity%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtCity" runat="server" Width="200px" MaxLength="35">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr class="trheight" id="trDistrict" runat="server" visible="false">
                            <td class="tdAlign">
                                <asp:Label ID="lblDistrict" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMDistrict%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtDistrict" runat="server" Width="200px" MaxLength="35">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblProv" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMProv%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtProv" runat="server" Width="200px" MaxLength="35">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblCountry" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMCountry%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtCountry" runat="server" Width="200px" MaxLength="35">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblPostalCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMPostalCode%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtPostalCode" runat="server" Width="200px" MaxLength="10">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td width="28%" class="tdAlign">
                                <img alt='Email' src="../Images/email.png" />
                            </td>
                            <td class="tdAlignLeft" width="72%">
                                <asp:TextBox ID="txtEmail" runat="server" MaxLength="75" Width="200px" />
                                <asp:RegularExpressionValidator ID="revalEmail" runat="server" ControlToValidate="txtEmail"
                                    Display="None" ErrorMessage="<%$ Resources:Resource, revalCMEmail%>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td width="28%" class="tdAlign">
                                <asp:Label ID="lblSecEmail" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMSecEmail%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft" width="72%">
                                <asp:TextBox ID="txtSecEmail" runat="server" MaxLength="75" Width="200px" />
                                <asp:RegularExpressionValidator ID="revalSecEmail" runat="server" ControlToValidate="txtSecEmail"
                                    Display="None" ErrorMessage="<%$ Resources:Resource, revalCMSecEmail%>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblSex" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMSex%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:RadioButtonList ID="rblstSex" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="<%$ Resources:Resource, liCMFeMale%>" Value="F" Selected="True">
                                    </asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:Resource, liCMMale%>" Value="M"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="left" bgcolor="#FFFFFF" width="50%" valign="top">
                    <table width="100%" style="border: 0;" bgcolor="#FFFFFF" class="table">
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblLangPref" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMLangPref%>">
                                </asp:Label>
                            </td>
                            <td class="tdAlignLeft">
                                <asp:RadioButtonList ID="rblstLangPref" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="<%$ Resources:Resource, liCMFrench%>" Value="fr" Selected="True">
                                    </asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:Resource, liCMEnglish%>" Value="en"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td width="28%" class="tdAlign">
                                <asp:Label ID="lblTitleFr" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMTitleFr%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft" width="72%">
                                <asp:TextBox ID="txtTitleFr" runat="server" MaxLength="50" Width="200px" />
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td width="28%" class="tdAlign">
                                <asp:Label ID="lblTitleEn" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMTitleEN%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft" width="72%">
                                <asp:TextBox ID="txtTitleEn" runat="server" MaxLength="50" Width="200px" />
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <img alt="Phone" src="../Images/phone.png" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtPhone" runat="server" Width="200px" MaxLength="20" />
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblPhoneExt" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMPhoneExt%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtPhoneExt" runat="server" Width="200px" MaxLength="10" />
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblHomePhone" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMHomePhone%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtHomePhone" runat="server" Width="200px" MaxLength="20" />
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblCellPhone" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMCellPhone%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtCellPhone" runat="server" Width="200px" MaxLength="20" />
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblPagerNo" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMPagerNo%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtPagerNo" runat="server" Width="200px" MaxLength="20" />
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td width="28%" class="tdAlign">
                                <img alt="Fax" src="../Images/fax.png" />
                            </td>
                            <td class="tdAlignLeft" width="72%">
                                <asp:TextBox ID="txtFax" runat="server" MaxLength="20" Width="200px" />
                            </td>
                        </tr>
                        <tr class="trheight" visible="false" runat="server">
                            <td class="tdAlign">
                                <asp:Label ID="lblContactSpecialization" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMContactSpecialization%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:DropDownList ID="dlContactSpecialization" CssClass="innerText" runat="server"
                                    Width="200px">
                                    <asp:ListItem Text="<%$ Resources:Resource, liCMSelectContactSpecialization%>" Value="0" />
                                    <asp:ListItem Text="<%$ Resources:Resource, liCMInstitution%>" Value="1" />
                                    <asp:ListItem Text="<%$ Resources:Resource, liCMIndividual%>" Value="2" />
                                    <asp:ListItem Text="<%$ Resources:Resource, liCMEvent%>" Value="3" />
                                </asp:DropDownList>
                                <span class="style1">*</span>
                                <asp:CustomValidator ID="custvalContactSpecialization" SetFocusOnError="true" runat="server"
                                    ErrorMessage="<%$ Resources:Resource, custvalCMContactSpecialization %>" ClientValidationFunction="funSelectContactSpecialization"
                                    Display="None">
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblDOB" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMDOB%>"
                                    runat="server" /><br />
                                &nbsp;
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtDOB" runat="server" MaxLength="10" Width="150px" />
                                <asp:ImageButton ID="imgDOB" CausesValidation="false" runat="server" ToolTip="Click to show calendar"
                                    ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                                <%=lblCMDOBFormat %>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" TargetControlID="txtDOB" runat="server"
                                    PopupButtonID="imgDOB" Format="MM/dd/yyyy">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblContactPrimary" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMContactPrimary%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:RadioButtonList ID="rblstContactPrimary" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="<%$ Resources:Resource, lblCMYes%>" Value="1">
                                    </asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:Resource, lblCMNo%>" Value="0" Selected="True">
                                    </asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr class="trheight" valign="top">
                            <td class="tdAlign">
                                <asp:Label ID="lblNotes" CssClass="lblBold" Visible="true" Text="<%$ Resources:Resource, lblCMNotes%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft" valign="top">
                                <asp:TextBox ID="txtNotes" runat="server" TextMode="multiLine" Visible="true" Rows="3"
                                    MaxLength="250" Width="80%" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="trheight">
                <td align="right" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;
                    padding-right: 5px;">
                    <table width="100%">
                        <tr>
                            <td height="20" align="center" width="25%">
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="cmdsave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                        style="min-width: 120px; text-align: center;">
                                        <%=Resources.Resource.cmdCssSave%>
                                    </span></a>
                                </div>
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="cmdBack" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                        <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssBack%>
                                        </span></a>
                                </div>
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="cmdReset" runat="server" visible="false" causesvalidation="false" class="ovalbutton"
                                        href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssReset%>
                                        </span></a>
                                </div>
                            </td>
                            <td height="20" align="center" width="15%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <br />
    </div>
    <div id="divMainContainerTitle1" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitleCommunication" Text="<%$ Resources:Resource, TitleCMCommunication %>"></asp:Literal></h2>
                </td>
                <td>
                    <asp:UpdateProgress runat="server" ID="upMaster" DisplayAfter="10">
                        <ProgressTemplate>
                            <img src="../Images/wait22trans.gif" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
                <td>
                    <div class="buttonwrapper">
                        <a id="cmdAddCommunication" runat="server" causesvalidation="false" class="ovalbutton"
                            href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                <%=Resources.Resource.btnEmailLogActivity%>
                            </span></a>
                    </div>
                </td>
                <td style="width: 50px;">
                    <asp:ImageButton ID="imgAlert" runat="server" ImageUrl="~/images/Alert.png" CommandName="Delete" />
                </td>
            </tr>
        </table>
    </div>
    <div class="divMainContent">
        <table id="tblCommunication" runat="server" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td height="2" colspan="4">
                </td>
            </tr>
            <tr>
                <td colspan="4" valign="top" align="left">
                    <asp:UpdatePanel ID="upnlGridCommunication" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="grdCommunication" runat="server" AllowSorting="True" DataSourceID="sqldsCommunication"
                                EmptyDataText="<%$ Resources:Resource, CMNoDataFound %>" AllowPaging="True" PageSize="15"
                                PagerSettings-Mode="Numeric" CellPadding="3" GridLines="both" AutoGenerateColumns="False"
                                ShowHeader="true" CssClass="table" UseAccessibleHeader="False" DataKeyNames="ComID"
                                Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="ComDateTime" HeaderText="<%$ Resources:Resource, grdCMCommunicationDate %>"
                                        ReadOnly="True" DataFormatString="{0: dd MMM yyyy HH:mm}" SortExpression="ComDateTime">
                                        <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PartnerLongName" HeaderText="<%$ Resources:Resource, grdCMPartnerName %>"
                                        ReadOnly="True" SortExpression="PartnerLongName">
                                        <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ContactName" HeaderText="<%$ Resources:Resource, grdCMContactName %>"
                                        ReadOnly="True" SortExpression="ContactName">
                                        <ItemStyle Width="300px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ComType" HeaderText="<%$ Resources:Resource, grdCMCommunicationType %>"
                                        ReadOnly="True" SortExpression="ComType">
                                        <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:HyperLinkField Text="<%$ Resources:Resource, grdCMCommunicationDetails %>" HeaderText="<%$ Resources:Resource, grdCMCommunicationMessage %>"
                                        DataNavigateUrlFormatString="ViewMessage.aspx?CommID={0}&ContactID={1}&CommType={2}"
                                        SortExpression="ComID" DataNavigateUrlFields="ComID, ComContactID, ComType">
                                        <ItemStyle Width="150px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:HyperLinkField>
                                    <asp:TemplateField HeaderText="" Visible="false" HeaderStyle-ForeColor="#ffffff"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgViewComm" runat="server" CommandArgument='<%# Eval("ComID") %>'
                                                CausesValidation="false" CommandName="ViewComm" ImageUrl="~/images/CommFollowUp.png" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" Visible="false" HeaderStyle-ForeColor="#ffffff"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgAlert" runat="server" CommandArgument='<%# Eval("ComID") %>'
                                                CausesValidation="false" CommandName="Edit" ImageUrl="~/images/Alert.png" />
                                            <div id="divLst" runat="server">
                                                <asp:Label ID="lblAD" runat="server" CssClass="lblBold" />
                                            </div>
                                            <div style="display: none;">
                                                <asp:TextBox ID="txtCommID" runat="server" Text='<%# Eval("ComID") %>' Width="0">
                                                </asp:TextBox>
                                                <asp:TextBox ID="txtContactID" runat="server" Text='<%# Eval("ComContactID") %>'
                                                    Width="0">
                                                </asp:TextBox>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="120px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdCMDelete %>" Visible="false"
                                        HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDeleteContact" runat="server" ImageUrl="~/images/delete_icon.png"
                                                CommandArgument='<%# Eval("ComID") %>' CommandName="Delete" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="grdFooterStyle" />
                                <RowStyle ForeColor="#333333" />
                                <EditRowStyle BackColor="#999999" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <PagerStyle BackColor="#5F6062" ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle"
                                    CssClass="lblBold" />
                                <HeaderStyle BackColor="#5F6062" ForeColor="White" Font-Bold="true" HorizontalAlign="Left"
                                    VerticalAlign="Middle" Height="15" />
                                <AlternatingRowStyle BackColor="#EDEDEE" ForeColor="#284775" />
                                <PagerSettings PageButtonCount="20" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="sqldsCommunication" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsPartner" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
    <br />
    <br />
    <script language="javascript">

        function funSelectContactSpecialization(source, arguments) {
            if (window.document.getElementById('<%=dlContactSpecialization.ClientID%>').value != "0") {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }

        function funEnterContactTitle(source, arguments) {
            if (window.document.getElementById('<%=txtTitleFr.ClientID%>').value == "" &&
                window.document.getElementById('<%=txtTitleEn.ClientID%>').value == "") {
                arguments.IsValid = false;
                window.document.getElementById('<%=txtTitleFr.ClientID%>').focus();
                return;
            }
            else {
                arguments.IsValid = true;
            }
        }
    </script>
</asp:Content>

<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="SelectPartner.aspx.vb" Inherits="Partner_SelectPartner" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="Resources.Resource" %>

<%@ Register src="../Controls/CommonSearch/CRMSearch.ascx" tagname="CRMSearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();              
    </script>
    <uc1:CRMSearch ID="CRMSearch1" runat="server" />
</asp:Content>

<asp:Content ID="cntSelectPartner" ContentPlaceHolderID="cphMaster" runat="Server">
    
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal>
                    </h2>
                </td>
                <td style="text-align: right;">
                </td>               
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <asp:UpdatePanel ID="upnlMsg" runat="server">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="trheight" >
                        <td class="tdAlignLeft" colspan="5">
                            <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td height="2" colspan="5">
                        </td>
                    </tr>
                    <tr class="trheight">
                        <td colspan="5">
                            <asp:Panel runat="server" ID="SearchPanel" DefaultButton="imgSearch">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="28%" class="tdAlign">
                                            <asp:Label ID="lblSelectPartner" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMSelectCustomer%>"
                                                runat="server" />
                                        </td>
                                        <td width="1%">
                                        </td>
                                        <td class="tdAlignLeft" width="33%">
                                            <asp:HiddenField ID="hdnCustID" runat="server"></asp:HiddenField>
                                            <asp:TextBox ID="txtPartner" runat="server" Width="220px" MaxLength="250" />
                                            <ajaxToolkit:AutoCompleteExtender ID="acExPartner" runat="server" TargetControlID="txtPartner"
                                                MinimumPrefixLength="1" EnableCaching="true" ServicePath="~/FilterService.asmx"
                                                ServiceMethod="GetPartner">
                                            </ajaxToolkit:AutoCompleteExtender>
                                            <asp:RequiredFieldValidator ID="reqvalPartner" runat="server" ControlToValidate="txtPartner"
                                                ErrorMessage="<%$ Resources:Resource, msgCMCustomerName%>" SetFocusOnError="true"
                                                Display="None">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" width="8%">
                                            <asp:ImageButton ID="imgSearch" runat="server" CausesValidation="false" AlternateText="Search"
                                                ImageUrl="../images/search-btn.gif" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td align="right" width="30%">
                                            <asp:UpdateProgress runat="server" ID="upMaster" AssociatedUpdatePanelID="upnlMsg"
                                                DisplayAfter="10">
                                                <ProgressTemplate>
                                                    <img src="../Images/wait22trans.gif" alt=""/>
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <%--Grid view Start for Customer--%>
                    <tr>
                        <td align="center" colspan="5">
                            <asp:GridView ID="grdCustomer" runat="server" AllowSorting="True" DataSourceID="sqldsCustomer"
                                AllowPaging="True" PageSize="10" PagerSettings-Mode="Numeric" CellPadding="0"
                                GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
                                CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="PartnerID"
                                Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, lblCustName %>" HeaderStyle-ForeColor="#ffffff"
                                        HeaderStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPartnerName" ForeColor="black"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="250px" Wrap="true" HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, lblHistory %>" ReadOnly="True"
                                        SortExpression="TotalOrders">
                                        <ItemStyle Width="350px" Wrap="true" HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="" HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ToolTip="Select" ID="imgSelect" runat="server" CommandArgument='<%# Eval("PartnerID") %>'
                                                CommandName="Edit" CausesValidation="false" ImageUrl="~/images/arrow_24.png" />
                                            <div style="display: none;">
                                                <asp:TextBox runat="server" Width="0px" ID="txtPartnerAcronyme" Text='<%# Eval("PartnerAcronyme") %>'>
                                                </asp:TextBox>
                                                <asp:TextBox runat="server" Width="0px" ID="txtPartnerName" Text='<%# Eval("PartnerLongName") %>'>
                                                </asp:TextBox>
                                                <asp:TextBox runat="server" Width="0px" ID="txtTotalOrders" Text='<%# Eval("TotalOrders") %>'>
                                                </asp:TextBox>
                                                <asp:TextBox runat="server" Width="0px" ID="txtTotalInvoices" Text='<%# Eval("TotalInvoices") %>'>
                                                </asp:TextBox>
                                                <asp:TextBox runat="server" Width="0px" ID="txtTotalAmtRcvd" Text='<%# Eval("TotalAmtRcvd") %>'>
                                                </asp:TextBox>
                                                <asp:TextBox runat="server" Width="0px" ID="txtLastAmtRcvd" Text='<%# Eval("LastAmtRcvd") %>'>
                                                </asp:TextBox>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="100px" />
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="" DataField="PartnerLongName" ReadOnly="True">
                                        <ItemStyle Width="0px" Wrap="true" HorizontalAlign="Left" ForeColor="white" Font-Size="0"
                                            VerticalAlign="Top" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle CssClass="grid_footer" />
                                <RowStyle CssClass="grid_rowstyle" />
                                <PagerStyle CssClass="grid_footer" />
                                <HeaderStyle CssClass="grid_header" Height="26px" />
                                <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                <PagerSettings PageButtonCount="20" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="sqldsCustomer" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <%--Grid view End for Customer--%>
                    <tr class="trheight">
                        <td align="left" colspan="5">
                            <table width="100%">
                                <tr>
                                    <td height="20" align="center" width="35%">
                                    </td>
                                    <td width="20%">
                                        <div class="buttonwrapper">
                                            <a id="cmdSave" runat="server" visible="false" class="ovalbutton" href="#"><span
                                                class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                <%=Resources.Resource.cmdCssNext%></span></a></div>
                                    </td>
                                    <td width="20%">
                                        <div class="buttonwrapper">
                                            <a id="cmdBack" runat="server" visible="false" causesvalidation="false" class="ovalbutton"
                                                href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                    <%=Resources.Resource.cmdCssBack%></span></a></div>
                                        <div class="buttonwrapper">
                                            <a id="cmdReset" runat="server" visible="false" causesvalidation="false" class="ovalbutton"
                                                href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                    <%=Resources.Resource.cmdCssReset%></span></a></div>
                                    </td>
                                    <td height="20" align="center" width="25%">
                                    </td>
                                </tr>
                            </table>                            
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:ValidationSummary ID="valsPartner" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
    <br />
    <br />
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="CustomerEdit.aspx.cs" Inherits="Partner_CustomerEdit" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <%=UIHelper.GetScriptTags("blockui", 1)%>  
    <script type="text/javascript">
        function blockContentArea(elementToBlock, loadingMessage) {
            $(elementToBlock).block({
                message: '<div>' + loadingMessage + '</div>',
                css: { border: '3px solid #a00' }
            });
        }
        function unblockContentArea(elementToBlock) {
            $(elementToBlock).unblock();
        }
        function jqGridResize() {
        }
    </script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
     <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <asp:Literal ID="ltTitle" Text="" runat="server" /> </b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <asp:Button ID="btnAdd" runat="server" Text="Add New Customer" 
            CausesValidation="false" onclick="btnAdd_Click" Visible="false" /> 
           
        <span id="spUploadImage" runat="server" visible="false">
            <asp:Button ID="btnUploadImage" Text="Upload File(s)"
                runat="server" CausesValidation="false" Visible="false" />
            <iCtrl:IframeDialog ID="mdUploadImage" TriggerSelectorClass="upload_file_pop" TriggerSelectorMode="Class" UrlSelector="FromProperty"
                Width="570" Height="400" Url="" Title="Upload File(s)" runat="server">
            </iCtrl:IframeDialog></span>           
    </div>
    <div style="float: right;">
        
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">         
     <asp:PlaceHolder ID="phControl" runat="server" /> 
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" Runat="Server">    
    <ul id="custMenu" class="menu inbiz-left-menu collapsible">
        <li class="active"><a href="#">Action</a>
            <asp:BulletedList ID="blstNav" DisplayMode="HyperLink" runat="server" style="display:block;">
            </asp:BulletedList>
        </li>
    </ul>   
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" Runat="Server">
   
</asp:Content>

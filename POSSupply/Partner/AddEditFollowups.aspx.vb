Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Partial Class Partner_AddEditFollowups
    Inherits BasePage
    Private objCust As New clsExtUser
    Private objPrtCon As New clsPartnerContacts
    Private objAdd As New clsAddress
    Private objActivityLog As New clsPartnerActivityLog
    Dim strPartner As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If

        If Not Page.IsPostBack Then
            txtHour.Text = "12"
            txtMin.Text = "00"
            txtDate.Text = Now.ToString("MM/dd/yyyy")
            If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
                strPartner = Convert.ToString(Request.QueryString("PartnerID"))
                objCust.CustID = strPartner
                objCust.getCustomerInfo()
                lblCustomerName.Text = objCust.CustName
                hdnPatID.Value = objCust.CustID
                objPrtCon.subPartnerContactsInfo(hdnPatID.Value)
                If objPrtCon.ContactFirstName <> "" Then
                    lblContactName.Text = objPrtCon.ContactFirstName
                Else
                    lblContactName.Text = "--"
                End If
                lblContactTitle.Text = objPrtCon.ContactTitleEn & "<br/>" & objPrtCon.ContactTitleFr
                txtContactTitle.Text = lblContactTitle.Text
                If lblContactTitle.Text = "<br/>" Then
                    lblContactTitle.Text = "--"
                End If
                If objCust.CustPhone <> "" Then
                    lblPhone.Text = objCust.CustPhone
                Else
                    lblPhone.Text = "--"
                End If
                If objCust.CustFax <> "" Then
                    lblFax.Text = objCust.CustFax
                Else
                    lblFax.Text = "--"
                End If
                If objCust.CustEmail <> "" Then
                    lblEmail.Text = objCust.CustEmail
                Else
                    lblEmail.Text = "--"
                End If
                txtNotes.Text = objCust.PartnerNote
                txtAddress.Text = objAdd.funPopulateAddress(objCust.CustType, "S", objCust.CustID)
            End If
            subFillSR()
            subFillGrid()
            txtFollowupDateTime.Text = DateTime.Now.ToString("MM/dd/yyyy")
        End If
        lblCustomerDtl.Text = TitleCMCustomerDtl
        lblTitleLogDetails.Text = lblFollowupLogDetails
    End Sub
    ' To Populate Sales Representatives
    Private Sub subFillSR()
        Dim objP As New clsPartners
        objP.PopulateSR(chklstSalesRep)
        objP = Nothing
    End Sub
    'Populate Grid
    Public Sub subFillGrid()
        objActivityLog.PartnerId = Request.QueryString("PartnerID")
        sqldsActivityLog.SelectCommand = objActivityLog.GetFillGridAllActivityByPartnerIdCommand()  'fill Orders
    End Sub
    Protected Sub grdActivityLog_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdActivityLog.PageIndexChanging
        subFillGrid()
    End Sub
    'On Row Data Bound
    Protected Sub grdActivityLog_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdActivityLog.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim lblLog As Label = CType(e.Row.FindControl("lblLog"), Label)
            lblLog.Text = CType(e.Row.FindControl("lblLog"), Label).Text
            lblLog.Text = lblLog.Text.Replace(Environment.NewLine, "<br />")

            Dim lblAssignedTo As Label = CType(e.Row.FindControl("lblAssignedTo"), Label)
            lblAssignedTo.Text = objActivityLog.GetAllAssignedRepresentatives(DataBinder.Eval(e.Row.DataItem, "PartnerActivityId").ToString)
        End If
    End Sub
    'Populate Object
    Private Sub subSetData()
        objActivityLog.PartnerId = Convert.ToString(Request.QueryString("PartnerID"))
        objActivityLog.FollowUpDate = DateTime.ParseExact(txtFollowupDateTime.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd") & " " & fundate(txtHour.Text.Trim, ddlTime.SelectedValue) & ":" & funMin(txtMin.Text.Trim) & ":00"
        objActivityLog.ActivityCreatedBy = Session("UserID")
        objActivityLog.LogText = txtNote.Text
        objActivityLog.ActivityCreatedDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
        objActivityLog.ActivityLastModified = Now.ToString("yyyy-MM-dd HH:mm:ss")
        objActivityLog.ActivityLastModifyBy = Session("UserID")
    End Sub
    Public Function fundate(ByVal intHour As String, ByVal strType As String) As String
        Dim intValue As String
        If strType = "PM" Then
            If intHour <= 11 Then
                intValue = intHour + 12
            Else
                intValue = intHour
            End If
        ElseIf strType = "AM" Then
            If intHour = "12" Then
                intValue = "00"
            ElseIf intHour = "11" Then
                intValue = intHour
            End If
            If intHour <= 9 Then
                intValue = "0" & intHour
            End If
        End If
        Return intValue
    End Function
    Public Function funMin(ByVal intMin As String) As String
        Dim intValue As String
        If intMin <= 9 Then
            If intMin = "00" Then
                intValue = intMin
            Else
                intValue = "0" & intMin
            End If
        Else
            intValue = intMin
        End If
        Return intValue
    End Function

    Protected Sub CmdAddLog_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdAddLog.ServerClick
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            subSetData()

            Dim retIdentity As String
            retIdentity = String.Empty
            If objActivityLog.InsertLog(retIdentity) = True Then
                GenerateAlert(retIdentity)
                lblMsg.Text = msgLogaddedsuccessfully
                txtNote.Text = ""
                txtFollowupDateTime.Text = DateTime.Now.ToString("MM/dd/yyyy")
                divLogDetails.Visible = True
                chklstSalesRep.ClearSelection()
            End If
            subFillGrid()
        End If
    End Sub
    Protected Sub sqldsActivityLog_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsActivityLog.Selected
        If e.AffectedRows = 0 Then
            divLogDetails.Visible = False
        Else
            divLogDetails.Visible = True
        End If
    End Sub
    Protected Sub grdActivityLog_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdActivityLog.Sorting
        subFillGrid()
    End Sub
    Protected Sub cmdBack_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.ServerClick
        'Response.Redirect("~/Partner/ActivityCustomers.aspx")
        If Request.QueryString.AllKeys.Contains("returnUrl") Then
            Response.Redirect(Request.QueryString("returnUrl"))
        Else
            Response.Redirect("~/Partner/ActivityCustomers.aspx")
        End If
    End Sub
    ' Inserts Alert for TeleMarketing followup
    Protected Sub GenerateAlert(ByVal activityId As String)
        Dim objAlert As New clsAlerts
        subSetAlertData(objAlert, Session("UserID"))
        objAlert.insertAlerts()

        For Each myItem As ListItem In chklstSalesRep.Items
            If myItem.Selected = True Then
                objActivityLog.AssingSalesRepresentative(activityId, myItem.Value)
                subSetAlertData(objAlert, myItem.Value)
                objAlert.insertAlerts()
            End If
        Next
    End Sub
    'Set Data for Alert
    Private Sub subSetAlertData(ByRef objA As clsAlerts, ByVal alertUserId As String)
        Dim objPartner As New clsPartners
        objA.AlertPartnerContactID = objActivityLog.PartnerId   ' Partner ID
        objA.AlertComID = Session("UserID")
        objA.AlertDateTime = objActivityLog.FollowUpDate
        objA.AlertUserID = alertUserId 'Followup userID
        Dim strNote As String = ""
        strNote += alertLblCMFollowup & "<br><br>" 'alrtlblSalesActivityFollowupActivity & "<br><br>" 'You have a Sales-Activities Followup Activity.
        strNote += alrtlblPartnerNo & " " & objActivityLog.PartnerId & "<br>" 'Partner No. 
        objPartner.PartnerID = objActivityLog.PartnerId
        objPartner.getPartnersInfo()
        strNote += alrtlblCustmer & " " & objPartner.PartnerLongName & "<br>" 'Customer:
        strNote += objActivityLog.LogText & "<br><br>"
        Dim objUsr As New clsUser
        objUsr.UserID = Session("UserID")
        objUsr.getUserInfo()
        strNote += alrtlblCreatedBy & " " & objUsr.UserSalutation & " " & objUsr.UserFirstName & " " & objUsr.UserLastName 'Created by:
        objUsr = Nothing
        objA.AlertNote = strNote
        objA.AlertRefType = "CFL"
    End Sub
    Protected Sub cmdCustomerDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCustomerDetail.ServerClick
        'Session("FromLeadLogs") = "?PartnerID=" & Request.QueryString("PartnerID")
        'Response.Redirect("~/Partner/AddEditCustomer.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&LeadLog=1")
        If Request.QueryString.AllKeys.Contains("returnUrl") Then
            Response.Redirect(Request.QueryString("returnUrl"))
        Else

        End If
    End Sub
End Class

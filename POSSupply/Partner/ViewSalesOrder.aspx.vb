Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports clsCommon
Partial Class Partner_ViewSalesOrder
    Inherits BasePage
    Private strSOID As String = ""
    Private objSO As New clsOrders
    Private clsStatus As New clsSysStatus
    ' On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If
        lblTitle.Text = lblSOViewSalesOrder '"View Sales Order"
        If Not Page.IsPostBack Then
            clsStatus.subGetStatus(dlSearch, "SO", "dlSh1", "NO")
            clsStatus.subGetStatus(dlPOStatus, "SO", "SOSts")
            If Request.QueryString("dflt") <> "Y" Then
                subFillGrid()
            End If
        End If

        clsGrid.MaintainParams(Page, grdOrder, SearchPanel, lblMsg, imgSearch)

        clsCSV.ExportCSV(Page, "SO", grdOrder, New Integer() {8}, imgCsv)

        txtSearch.Focus()
    End Sub
    'Populate Grid
    Public Sub subFillGrid()
        sqldsOrder.SelectCommand = objSO.funFillGridForAllOrders("", "", "")  'fill Oorders
    End Sub
    'On Page Index Change
    Protected Sub grdOrder_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdOrder.PageIndexChanging
        subSearch()
        'subFillGrid()
    End Sub
    'On Row Data Bound
    Protected Sub grdOrder_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdOrder.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            e.Row.Cells(3).Text = String.Format("{0:F}", funCalcTax(e.Row.Cells(0).Text))
            Dim strData As String = ""
            Select Case e.Row.Cells(5).Text
                Case "N"
                    strData = liCreated
                Case "A"
                    strData = liApproved
                Case "P"
                    strData = liInProcess
                Case "H"
                    strData = liHeld
                Case "S"
                    strData = liShipped
                Case "Z"
                    strData = liShippingHeld
                Case "I"
                    strData = liInvoiced
                Case "C"
                    strData = liClosed
                Case "D"
                    strData = liClosedInvoiced
            End Select
            e.Row.Cells(5).Text = strData
        End If
    End Sub
    ' Calculate Tax
    Private Function funCalcTax(ByVal SOID As String) As Double
        Dim objSOItems As New clsOrderItems
        Dim drSOItems As Data.Odbc.OdbcDataReader
        Dim objPrn As New clsPrint
        drSOItems = objSOItems.GetDataReader(objSOItems.funFillGrid(SOID))
        Dim dblSubTotal As Double = 0
        Dim dblTax As Double = 0
        Dim dblProcessTotal As Double = 0
        Dim dblTotal As Double = 0
        Dim strTaxArray(,) As String
        Dim dblDiscountApplied As Double = 0
        Dim dblAmount As Double = 0
        Dim strWhs As String
        While drSOItems.Read
            Dim strTax As ArrayList
            Dim strTaxItem As String
            If Not drSOItems("sysTaxCodeDescText") Is DBNull.Value AndAlso drSOItems("sysTaxCodeDescText").ToString <> "0" Then
                strTax = objPrn.funCalcTax(drSOItems("ordProductID"), drSOItems("ordShpWhsCode"), drSOItems("amount"), "", drSOItems("ordCustID"), drSOItems("ordCustType"), drSOItems("ordProductTaxGrp"))
            Else
                strTax = objPrn.funCalcTax(drSOItems("ordProductID"), drSOItems("ordShpWhsCode"), drSOItems("amount"), "", drSOItems("ordCustID"), drSOItems("ordCustType"))
            End If
            strWhs = drSOItems("ordShpWhsCode")
            Dim intI As Integer = 0
            While strTax.Count - 1 >= intI
                strTaxItem = strTax.Item(intI).ToString
                strTaxArray = objPrn.funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                intI += 1
            End While
            dblSubTotal = dblSubTotal + CDbl(drSOItems("amount"))
        End While
        Dim i As Integer = 0
        Dim j As Integer = 0

        Dim objSOIP As New clsOrderItemProcess
        objSOIP.OrdID = SOID
        dblProcessTotal = CDbl(objSOIP.getProcessCostForOrderNo())
        strTaxArray = objPrn.funTotalProcessTax(dblProcessTotal, strWhs, strTaxArray)

        If Not strTaxArray Is Nothing Then
            j = strTaxArray.Length / 2
        End If
        While i < j
            dblTax += strTaxArray(1, i)
            i += 1
        End While

        dblTotal = dblTotal + dblSubTotal + dblProcessTotal + dblTax
        drSOItems.Close()
        objSOItems.CloseDatabaseConnection()
        objSOIP.CloseDatabaseConnection()
        objSOIP = Nothing
        objPrn = Nothing
        Return dblTotal
    End Function
    'Deleting
    Protected Sub grdOrder_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdOrder.RowDeleting
        strSOID = grdOrder.DataKeys(e.RowIndex).Value.ToString()
        lblMsg.Text = SalesOrderDeletedSuccessfully '"Sales Order Deleted Successfully." 'ODeleteSucess
        objSO.OrdID = strSOID
        objSO.funDeleteOrderItemProcess() 'Delete Sales Order Item Process
        objSO.funDeleteOrderItems() 'Delete Order Items
        sqldsOrder.DeleteCommand = objSO.funDeleteOrder() 'Delete Order
        subFillGrid()
    End Sub
    'Editing
    Protected Sub grdOrder_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdOrder.RowEditing
        strSOID = grdOrder.DataKeys(e.NewEditIndex).Value.ToString()
        If Request.QueryString("Col") <> "" Then
            Response.Redirect("~/Partner/ViewOrderDetails.aspx?SOID=" & strSOID & "&PartnerID=" & Request.QueryString("PartnerID") & "&Col=" & Request.QueryString("Col"))
        Else
            Response.Redirect("~/Partner/ViewOrderDetails.aspx?SOID=" & strSOID & "&PartnerID=" & Request.QueryString("PartnerID"))
        End If

    End Sub
    'Sorting
    Protected Sub grdOrder_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdOrder.Sorting
        subFillGrid()
    End Sub
    ' Sql Data Source Event Track
    Protected Sub sqldsOrder_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsOrder.Selected
        If e.AffectedRows = 0 And lblMsg.Text = "" Then
            lblMsg.Text = NoDataFound
        End If
    End Sub
    ' Search Sub Routine
    Private Sub subSearch()
        sqldsOrder.SelectCommand = objSO.funFillGridForAllOrders(dlSearch.SelectedValue, funRemove(txtSearch.Text), dlPOStatus.SelectedValue)
        lblMsg.Text = ""
    End Sub
    ' On Search Image Click
    Protected Sub imgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
        If txtSearch.Text <> "" Then
            subSearch()
        Else
            lblMsg.Text = PlzEntSearchCriteria
            txtSearch.Focus()
        End If
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    ' On Text Change
    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        subSearch()
    End Sub
    Protected Sub cmdBack_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.ServerClick
        If Request.QueryString("returnUrl") <> "" Then
            Response.Redirect(Request.QueryString("returnUrl"))
        End If        
        If Request.QueryString("Col") <> "" Then
            Response.Redirect("~/CollectionAgent/ViewFollowUpLog.aspx?SOID=" & Request.QueryString("Col"))
        Else
            Response.Redirect("~/Partner/AddEditCustomer.aspx?PartnerID=" & Request.QueryString("PartnerID"))
        End If
    End Sub
End Class

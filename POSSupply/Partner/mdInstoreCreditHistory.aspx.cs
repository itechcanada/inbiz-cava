﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Partner_mdInstoreCreditHistory : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void grdAccHst_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
    }

    protected void grdAccHst_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (this.CustID > 0)
        {
            CustomerCredit objCustCrdt = new CustomerCredit();
            grdAccHst.DataSource = objCustCrdt.GetInstoreCreditHistory(this.CustID);
            grdAccHst.DataBind();
        }

    }

    public int CustID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["custID"], out id);
            return id;
        }
    }
}
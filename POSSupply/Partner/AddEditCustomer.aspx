<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="AddEditCustomer.aspx.vb" Inherits="Admin_AddEditCustomer" ValidateRequest="false" %>

<%@ Import Namespace="Resources.Resource" %>
<%@ Register src="../Controls/CommonSearch/CRMSearch.ascx" tagname="CRMSearch" tagprefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();
        $('#divMainContainerTitle1').corner();
        $('#divMainContainerTitle2').corner();
        $('#divMainContainerTitle3').corner();
        $('#divMainContainerTitle4').corner();
        $('#divMainContainerTitle5').corner();
    </script>
    <uc1:CRMSearch ID="CRMSearch1" runat="server" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal></h2>
                </td>
                <td style="float: right;">
                    <div class="buttonwrapper divCommandButton">
                        <a id="cmdBackPartnerview" visible="false" runat="server" causesvalidation="false"
                            class="ovalbutton" href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                <%=Resources.Resource.cmdCssBack%>
                            </span></a>
                    </div>
                    <% If ConfigurationManager.AppSettings("YellowSR").ToLower = "yes" And Request.QueryString("PartnerID") <> "" Then%>
                    <div class="buttonwrapper divCommandButton">
                        <a id="cmdCssStsPayHistory" runat="server" causesvalidation="false" class="ovalbutton"
                            href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                <%=Resources.Resource.cmdCssStsPayHistory%>
                            </span></a>
                    </div>
                    <% End If%>
                    <% If Request.QueryString("PartnerID") <> "" And (Session("UserModules").ToString.Contains("SAL") = True Or Session("UserModules").ToString.Contains("QOA") = True Or Session("UserModules").ToString.Contains("ADM") = True) Then%>
                    <div class="buttonwrapper divCommandButton">
                        <a id="cmdSOHistory" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                            <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                <%=Resources.Resource.btnSOHistory%>
                            </span></a>
                    </div>
                    <% End If%>
                    <% If Request.QueryString("PartnerID") <> "" Then%>
                    <div class="buttonwrapper divCommandButton">
                        <a id="cmdInvoiceHistory" runat="server" causesvalidation="false" class="ovalbutton"
                            href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                <%=Resources.Resource.btnColInvoiceHistory%>
                            </span></a>
                    </div>
                    <div class="buttonwrapper divCommandButton">
                        <a id="cmdARHistory" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                            <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                <%=Resources.Resource.cmdCssARHistory%>
                            </span></a>
                    </div>
                    <div class="buttonwrapper divCommandButton">
                        <a id="cmdNotes" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                            <span class="ovalbutton" style="min-width: 90px; text-align: center;">
                                <%=Resources.Resource.prnNote%>
                            </span></a>
                    </div>
                    <div class="buttonwrapper divCommandButton">
                        <a id="cmbAddActivity" runat="server" causesvalidation="false" class="ovalbutton"
                            href="#"><span class="ovalbutton" style="min-width: 90px; text-align: center;">
                                <%=Resources.Resource.cmdAddActivity%>
                            </span></a>
                    </div>
                    <% End If%>
                    <div style="clear: both;">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="divMainContent">
        <asp:HiddenField runat="Server" ID="hdnLead" />
        <asp:HiddenField ID="hdnCustType" runat="server" />
        <table cellspacing="0" cellpadding="0" style="width: 100%;">
            <tr runat="server" id="trClickHere" visible="false">
                <td align="center" colspan="2">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                </td>
            </tr>
            <tr id="trAllControl" runat="server">
                <td align="left" bgcolor="#FFFFFF">
                    <table width="100%" style="border: 0;" bgcolor="#FFFFFF" class="table">
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td height="5" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table width="100%" style="border: 0;" bgcolor="#FFFFFF" class="table">
                                    <tr height="25">
                                        <td class="tdAlign" width="27%">
                                            <asp:Label ID="lblCustName" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMLongName %>"
                                                runat="server" />
                                        </td>
                                        <td width="1%">
                                        </td>
                                        <td align="left" width="24%">
                                            <asp:TextBox ID="txtCustName" runat="server" CssClass="txtBoxPrd" MaxLength="250" />
                                            <span class="style1">*</span>
                                            <asp:RequiredFieldValidator ID="reqvalCustName" runat="server" ControlToValidate="txtCustName"
                                                ErrorMessage="<%$ Resources:Resource, reqvalCMLongName %>" SetFocusOnError="true"
                                                Display="None">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td width="1%">
                                        </td>
                                        <td class="tdAlign" width="22%">
                                            <asp:Label ID="lblAcronyme" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMAcronyme%>"
                                                runat="server" Visible="false" />
                                        </td>
                                        <td width="1%">
                                        </td>
                                        <td align="left" width="24%">
                                            <asp:TextBox ID="txtAcronyme" runat="server" CssClass="txtBoxPrd" MaxLength="75" Visible="false" />
                                        </td>
                                    </tr>
                                    <tr height="25">
                                        <td class="tdAlign">
                                            <img src="../Images/phone.png" alt="Phone" />
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDistPhone" runat="server" CssClass="txtBoxPrd" MaxLength="15" />
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlign">
                                            <img src="../Images/email.png" alt="Email" />
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDistEmail" runat="server" MaxLength="65" CssClass="txtBoxPrd">
                                            </asp:TextBox>
                                            <asp:RegularExpressionValidator ID="revalDistributorEmail" runat="server" ControlToValidate="txtDistEmail"
                                                SetFocusOnError="true" Display="None" ErrorMessage="<%$ Resources:Resource, revalDistributorEmail%>"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                            </asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr height="25">
                                        <td class="tdAlign">
                                            <img src="../Images/website.png" alt="Web Site" />
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlignLeft">
                                            <asp:TextBox ID="txtWebsite" runat="server" CssClass="txtBoxPrd" MaxLength="75" />
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlign">
                                            <asp:Label ID="lblAdditionalPhoneNo" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource,lblAdditionalPhoneNo%>"></asp:Label>
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtAdditionalPhoneNo" runat="server" MaxLength="20" CssClass="txtBoxPrd"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr height="25">
                                        <td class="tdAlign" >
                                            <%--<asp:Label ID="lblcompanyID" CssClass="lblBold" Text="<%$ Resources:Resource, lblSelCompanyID%>"
                                                runat="server" />&nbsp;--%>
                                             <img src="../Images/fax1.png" alt="Fax" />
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlignLeft">
                                           <%-- &nbsp;<asp:DropDownList ID="dlCompanyName" runat="server" CssClass="txtBoxPrd" Width="155px" >
                                            </asp:DropDownList>
                                            <span class="style1" >*</span>
                                            <asp:CustomValidator ID="custvalCompanyName" runat="server" SetFocusOnError="true"
                                                ErrorMessage="<%$ Resources:Resource, custvalCompanyName%>" ClientValidationFunction="funCheckCompanyName"
                                                Display="None">
                                            </asp:CustomValidator>--%>
                                            <asp:TextBox ID="txtDistFax" runat="server" CssClass="txtBoxPrd" MaxLength="15" />
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlign">
                                           
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            
                                        </td>
                                    </tr>
                                    <tr height="25" id="Tr1" runat="server" visible="false">
                                        <td class="tdAlign">
                                            <asp:Label ID="lblDistDiscount" CssClass="lblBold" Text="<%$ Resources:Resource, lblDistDiscount %>"
                                                runat="server" />&nbsp;
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDistDiscount" runat="server" MaxLength="2" CssClass="txtBoxPrd">
                                            </asp:TextBox>%
                                            <asp:CompareValidator ID="compvalDistDiscount" EnableClientScript="true" SetFocusOnError="true"
                                                ControlToValidate="txtDistDiscount" Operator="DataTypeCheck" Type="Double" Display="None"
                                                ErrorMessage="<%$ Resources:Resource, compvalDiscount %>" runat="server" />
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlign">
                                            <asp:Label ID="lblDistCustomerID" CssClass="lblBold" Text="<%$ Resources:Resource, lblDistCustomerID%>"
                                                runat="server" />
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDistCustomerID" runat="server" CssClass="txtBoxPrd" MaxLength="10" />
                                            <span class="style1">*</span>
                                            <asp:RequiredFieldValidator ID="reqvalDistCustomerID" runat="server" ControlToValidate="txtDistCustomerID"
                                                Display="None" ErrorMessage="<%$ Resources:Resource, reqvalDistributorCustomerID%>"
                                                SetFocusOnError="true">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr height="25">
                                        <td class="tdAlign">
                                            <asp:Label ID="lblDistCommissionCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblDistCommissionCode%>"
                                                runat="server" Visible="false" />&nbsp;
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDistCommissionCode" runat="server" CssClass="txtBoxPrd" MaxLength="10" Visible="false" />
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlign">
                                            <asp:Label ID="lblDistInvoiceNetTerms" CssClass="lblBold" Text="<%$ Resources:Resource, lblDistInvoiceNetTerms%>"
                                                runat="server" />
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDistInvoiceNetTerms" runat="server" CssClass="txtBoxPrd" MaxLength="4" />
                                        </td>
                                    </tr>
                                    <tr height="25">
                                        <td class="tdAlign">
                                            <asp:Label ID="lblDistType" CssClass="lblBold" Text="<%$ Resources:Resource, lblDistType%>"
                                                runat="server" />&nbsp;
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            &nbsp;<asp:DropDownList ID="dlDistType" runat="server" CssClass="txtBoxPrd" Width="155px">
                                                <%-- <asp:ListItem Value="G" Text="<%$ Resources:Resource, Gold %>" />
                    <asp:ListItem Value="S" Text="<%$ Resources:Resource, Silver %>"/>--%>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlign">
                                            <asp:Label ID="lblDistCourierCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblDistCourierCode%>"
                                                runat="server" Visible="false" />
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDistCourierCode" runat="server" CssClass="txtBoxPrd" MaxLength="20" Visible="false" />
                                        </td>
                                    </tr>
                                    <tr height="25">
                                        <td class="tdAlign">
                                            <asp:Label ID="lblDistStatus" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblDistributorStatus %>" Visible="false"></asp:Label>&nbsp;
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            &nbsp;<asp:DropDownList ID="dlDistStatus" runat="server" CssClass="txtBoxPrd" Width="155px" Visible="false">
                                                <%--<asp:ListItem Value="A" Text="<%$ Resources:Resource, Active %>" />
                    <asp:ListItem Value="W" Text="<%$ Resources:Resource, WaitingForValidation %>"/>
                    <asp:ListItem Value="X" Text="<%$ Resources:Resource, Expired %>"/>--%>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlign">
                                            <asp:Label ID="lblSelectTaxGroup" CssClass="lblBold" Text="<%$ Resources:Resource, lblSelectTaxGroup %>"
                                                runat="server" />
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="dlTaxGroup" runat="server" CssClass="txtBoxPrd" Width="155px">
                                            </asp:DropDownList>
                                            <span class="style1">*</span>
                                            <asp:CustomValidator ID="custvalTaxGroup" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, custvalSelTaxGroup%>"
                                                ClientValidationFunction="funCheckSelTaxGroup" Display="None">
                                            </asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr height="25">
                                        <td class="tdAlign">
                                            <br />
                                            <asp:Label ID="lblDistShipBlankPref" CssClass="lblBold" Text="<%$ Resources:Resource, lblDistShipBlankPref%>"
                                                runat="server" Visible="false" />&nbsp;
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            &nbsp;<asp:RadioButtonList ID="rblstDistShipBlankPref" runat="server" RepeatDirection="Horizontal" Visible="false">
                                                <asp:ListItem Text="<%$ Resources:Resource, lblYes%>" Value="1">
                                                </asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:Resource, lblNo%>" Value="0" Selected="True">
                                                </asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlign">
                                            <asp:Label ID="lblDistCurrencyCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblDistCurrencyCode%>"
                                                runat="server" />
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="dlCurrencyCode" runat="server" CssClass="txtBoxPrd" Width="155px">
                                            </asp:DropDownList>
                                            <span class="style1">*</span>
                                            <asp:CustomValidator ID="custvalDistCurrencyCode" runat="server" SetFocusOnError="true"
                                                ErrorMessage="<%$ Resources:Resource, custvalResellerCurrencyCode%>" ClientValidationFunction="funCheckCurrencyCode"
                                                Display="None">
                                            </asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr height="25">
                                        <td class="tdAlign">
                                            <asp:Label ID="lblCustomerType" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblCustomerType%>">
                                            </asp:Label>&nbsp;
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            &nbsp;<asp:RadioButtonList ID="rblstCustomerType" runat="server" RepeatDirection="Vertical"
                                                CssClass="lblSimple">
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlign">
                                            <asp:Label ID="lblValidated" CssClass="lblBold" Text="<%$ Resources:Resource, lblIsValidated%>"
                                                runat="server" Visible="false" />
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:RadioButtonList ID="rblstValidated" runat="server" RepeatDirection="Horizontal" Visible="false">
                                                <asp:ListItem Text="<%$ Resources:Resource, lblYes%>" Value="1">
                                                </asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:Resource, lblNo%>" Value="0" Selected="True">
                                                </asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr height="25" runat="server" id="trSaleRep" valign="top">
                                        <td class="tdAlign">
                                            <asp:Label ID="lblLanguage" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblLanguage %>"></asp:Label>&nbsp;
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlignLeft">
                                            &nbsp;<asp:DropDownList ID="ddlLanguage" runat="server" CssClass="txtBoxPrd" Width="155px">
                                                <asp:ListItem Value="en" Text="<%$ Resources:Resource, English %>" />
                                                <asp:ListItem Value="fr" Text="<%$ Resources:Resource, French %>" />
                                                <asp:ListItem Value="sp" Text="<%$ Resources:Resource, Spanish %>" />
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlign">
                                            <asp:Label ID="lblSalesRep" CssClass="lblBold" Text="<%$ Resources:Resource, lblSalesRep%>"
                                                runat="server" />
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <div id="divSR" runat="server" style="height: 100px; width: 150px; border-width: 1px;
                                                border-color: Gray; border-style: solid; overflow-y: auto;">
                                                <% If chklstSalesRep.Items.Count = 0 Then%>
                                                <%= NOSRAvail %>
                                                <% End If%>
                                                <asp:CheckBoxList ID="chklstSalesRep" runat="server" RepeatDirection="vertical">
                                                </asp:CheckBoxList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr height="25">
                                        <td class="tdAlign">
                                            <asp:Label ID="Label1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblDesc1%>" />&nbsp;
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlignLeft">
                                            &nbsp;<asp:TextBox ID="txtDesc1" runat="server" CssClass="txtBoxPrd" MaxLength="70" />
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlign">
                                            <asp:Label ID="lblIsActive" CssClass="lblBold" Text="<%$ Resources:Resource, lblIsActive%>"
                                                runat="server" />
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:RadioButtonList ID="rblstActive" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="<%$ Resources:Resource, lblYes%>" Value="1" Selected="True">
                                                </asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:Resource, lblNo%>" Value="0"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr height="25">
                                        <td class="tdAlign">
                                            <asp:Label ID="lblNotes" CssClass="lblBold" Visible="true" Text="<%$ Resources:Resource, lblCMNotes%>"
                                                runat="server" />&nbsp;
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            &nbsp;<asp:TextBox ID="txtNotes" runat="server" TextMode="multiLine" Rows="3" MaxLength="250"
                                                CssClass="txtBoxPrd" />
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlign">
                                            <asp:Label ID="lblDesc1" CssClass="lblBold" Text="<%$ Resources:Resource, lblDesc2%>"
                                                runat="server" /><br />
                                            <br />
                                            <asp:Label ID="Label3" CssClass="lblBold" Visible="true" Text="<%$ Resources:Resource, lblCreditAvailable%>"
                                                runat="server" />
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDesc2" runat="server" CssClass="txtBoxPrd" MaxLength="70" />
                                            <br />
                                            <br />
                                            <asp:TextBox ID="txtCreditAvailable" runat="server" MaxLength="15" CssClass="txtBoxPrd" />
                                        </td>
                                    </tr>
                                    <tr height="25" valign="top">
                                        <td class="tdAlign">
                                            <asp:Label ID="lblInvoicePreference" CssClass="lblBold" Text="<%$ Resources:Resource, lblInvoicePreference %>"
                                                runat="server" />
                                            &nbsp;
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlignLeft">
                                            &nbsp;
                                            <asp:DropDownList ID="ddlInvoicePreference" CssClass="txtBoxPrd" runat="server">
                                                <asp:ListItem Selected="True" Value="E">Email</asp:ListItem>
                                                <asp:ListItem Value="M">Mail</asp:ListItem>
                                                <asp:ListItem Value="F">Fax</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdAlign">
                                            <br />
                                            <asp:Label ID="lblRejectedReason" Visible="false" CssClass="lblBold" Text="<%$ Resources:Resource, lblRejectedReason%>"
                                                runat="server" />
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:CompareValidator ID="compvalCreditAvailable" EnableClientScript="true" SetFocusOnError="true"
                                                ControlToValidate="txtCreditAvailable" Operator="DataTypeCheck" Type="Double"
                                                Display="None" ErrorMessage="<%$ Resources:Resource, compvalCreditAvailable %>"
                                                runat="server" />
                                            <asp:TextBox ID="txtRejectedReason" Visible="false" runat="server" TextMode="multiLine"
                                                Height="24" CssClass="txtBoxPrd" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="display:none;">
                <td colspan="2">
                    <div class="divMainContainerTitle">
                        <h2>
                            <%= Resources.Resource.lblCustomerCategories%>
                        </h2>
                    </div>
                    <div class="divMainContent">
                        <table border="0" cellpadding="0" cellspacing="0" width="600px" align="center">
                            <tr>
                                <td valign="middle" width="40px">
                                    <a id="ancAddCategory" href="javascript:void(0);">
                                        <img title="Click to add new category" alt="Add new category" src="../Images/add_categ.png"
                                            style="width: 24px; height: 24px; vertical-align: middle;" /></a>
                                </td>
                                <td align="left" valign="middle">
                                    <asp:Label ID="Label4" CssClass="lblBold" Text="Click to add new category." runat="server" />
                                </td>
                            </tr>
                            <tr id="divAddCategory">
                                <td align="left" colspan="2">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="lblCategoryNameEn" CssClass="lblBold" runat="server" Text="Category Name"></asp:Label>&nbsp;&nbsp;
                                                </td>
                                                <td align="left" style="width:215px;">
                                                    <asp:TextBox ID="txtCategoryNameEn" MaxLength="45" runat="server" Width="200Px"></asp:TextBox>
                                                </td>
                                                <td style="width:35px;">
                                                    <a id="ancSaveCateg" href="javascript:void(0);">
                                                        <img src="../Images/save_add2.png" title="Save" alt="Save" style="width: 24px; height: 24px;" />
                                                    </a>
                                                </td>
                                                <td style="width:35px;">
                                                    <a id="ancClose" href="javascript:void(0);">
                                                        <img alt="Cancel" src="../Images/save_cancel.png" title="Cancel" style="width: 24px; height: 24px;" />
                                                    </a>
                                                </td>
                                                <td>
                                                    <span id="msgbox" style="display:none; color:#000;"></span>
                                                </td>
                                            </tr>
                                        </table>
                                </td>
                            </tr>                            
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="600px">
                            <tr>
                                <td colspan="4" valign="middle">
                                    
                                </td>
                            </tr>                            
                            <tr style="height:24px;">
                                <td align="left" style="width:200px;">
                                    <asp:Label ID="lblAllCateg"  CssClass="lblBold" Text="All Categories"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblAssignedCateg" CssClass="lblBold" Text=" Assigned Categories"
                                        runat="server" />
                                </td>
                                <td>

                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <asp:ListBox ID="lBoxAllCateg" runat="server" Height="150px" Width="199px" SelectionMode="Multiple">                                        
                                    </asp:ListBox>
                                </td>
                                <td valign="middle" align="center" width="80px">
                                    <input type="button" id="btnAssingCat"  value="&gt;&gt;" style="width:50px" /><br />
                                    <input type="button"  id="unAssignCat"  value="&lt;&lt;" style="width:50px" /><br />  
                                    <asp:HiddenField ID="hdnAssignedValues" Value="" runat="server" />
                                </td>
                                <td valign="top">
                                    <asp:ListBox ID="lBoxAssignedCateg" runat="server" Height="150px" Width="199px" SelectionMode="Multiple">
                                    </asp:ListBox>
                                </td>
                                <td valign="top">
                                    <p style="margin-left:5px;">Note *: By pressing �ctrl� key can select multiple items. </p>
                                </td>
                            </tr>
                        </table>                       
                    </div>
                </td>
            </tr>
            <%--Bill To Address Start--%>
            <tr>
                <td height="20" align="left" colspan="2">
                    <div id="divMainContainerTitle1" class="divMainContainerTitle">
                        <h2>
                            <asp:Literal runat="server" ID="lblBillToAddress" Text="<%$ Resources:Resource, lblBillToAddress%>"></asp:Literal></h2>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" style="border: 0;" bgcolor="#FFFFFF" class="table">
                        <tr>
                            <td class="tdAlign" width="27%">
                            </td>
                            <td width="1%">
                            </td>
                            <td align="left" width="24%">
                            </td>
                            <td width="1%">
                            </td>
                            <td class="tdAlign" width="22%">
                            </td>
                            <td width="1%">
                            </td>
                            <td align="left" width="24%">
                            </td>
                        </tr>
                        <tr height="25">
                            <td class="tdAlign">
                                <asp:Label ID="lblBillToAddressLine1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressLine1%>">
                                </asp:Label>
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtBillToAddressLine1" runat="server" MaxLength="35" CssClass="txtBoxPrd">
                                </asp:TextBox>
                                <span class="style1">*</span>
                                <asp:RequiredFieldValidator ID="reqvalHQAddressLine1" runat="server" ControlToValidate="txtBillToAddressLine1"
                                    ErrorMessage="<%$ Resources:Resource, reqvalBillToAddressLine1 %>" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td>
                            </td>
                            <td class="tdAlign">
                                <asp:Label ID="lblBillToAddressLine2" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressLine2%>">
                                </asp:Label>
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtBillToAddressLine2" runat="server" MaxLength="35" CssClass="txtBoxPrd">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr height="25">
                            <td class="tdAlign">
                                <asp:Label ID="lblBillToAddressLine3" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressLine3%>">
                                </asp:Label>
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtBillToAddressLine3" runat="server" MaxLength="35" CssClass="txtBoxPrd">
                                </asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td class="tdAlign">
                                <asp:Label ID="lblBillToAddressCity" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressCity%>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtBillToAddressCity" runat="server" CssClass="txtBoxPrd" MaxLength="25" />
                                <span class="style1">*</span>
                                <asp:RequiredFieldValidator ID="reqvalAddressCity" runat="server" ControlToValidate="txtBillToAddressCity"
                                    ErrorMessage="<%$ Resources:Resource, PlzEntBillAddresCity%>" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr height="25">
                            <td class="tdAlign">
                                <asp:Label ID="lblBillToAddressState" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressState%>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtBillToAddressState" runat="server" CssClass="txtBoxPrd" MaxLength="25" />
                                <span class="style1">*</span>
                                <asp:RequiredFieldValidator ID="reqvalHQAddressState" runat="server" ControlToValidate="txtBillToAddressState"
                                    ErrorMessage="<%$ Resources:Resource, PlzEntBillToState %>" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td>
                            </td>
                            <td class="tdAlign">
                                <asp:Label ID="lblBillToAddressCountry" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressCountry%>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtBillToAddressCountry" runat="server" CssClass="txtBoxPrd" MaxLength="25" />
                            </td>
                        </tr>
                        <%--<tr height="30" id="trBillToCountryState" runat="server" visible="false">
                            <td class="tdAlign">
                                <asp:Label ID="lblBillToCountryState" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToCountryState %>">
                                </asp:Label>
                            </td>
                            <td class="tdAlignLeft">
                                <asp:UpdatePanel ID="upnlBillToState" runat="server" UpdateMode="Conditional">
                                    <contenttemplate>
                                                           
                    <asp:DropDownList ID="dlBillToCountry" runat="server" style="width:135px;" 
                        AutoPostBack="true">
                        <asp:ListItem Value="0" Text="<%$ Resources:Resource, SelectBillToCountry %>">
                        </asp:ListItem>
                    </asp:DropDownList>
                    
                    <asp:DropDownList ID="dlBillToState" runat="server" Style="width:135px;">
                        <asp:ListItem Value="0" Text="<%$ Resources:Resource, SelectBillToState %>"></asp:ListItem>
                    </asp:DropDownList>
                    <span class="style1"> *</span>
                    </contenttemplate>
                                    <triggers>
                    <asp:AsyncPostBackTrigger ControlID="dlCountry" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="dlBillToCountry" EventName="SelectedIndexChanged" />
                    </triggers>
                                </asp:UpdatePanel>
                                <asp:CustomValidator ID="custvalBillToCountry" runat="server" ClientValidationFunction="funBillToCountry"
                                    Display="None" ErrorMessage="<%$ Resources:Resource, PlzSelBillToCountry %>"> 
                                </asp:CustomValidator>
                                <asp:CustomValidator ID="custvalBillToState" runat="server" ClientValidationFunction="funBillToState"
                                    Display="None" ErrorMessage="<%$ Resources:Resource, PlzSelBillToState %>"> 
                                </asp:CustomValidator>
                            </td>
                        </tr>--%>
                        <tr height="25">
                            <td class="tdAlign">
                                <asp:Label ID="lblBillToAddressPostalCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressPostalCode %>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtBillToAddressPostalCode" runat="server" CssClass="txtBoxPrd"
                                    MaxLength="10" />
                                <span class="style1">*</span>
                                <asp:RequiredFieldValidator ID="reqvalAddressPostalCode" runat="server" ControlToValidate="txtBillToAddressPostalCode"
                                    ErrorMessage="<%$ Resources:Resource, PlzEntBillAddressPostalCode%>" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td>
                            </td>
                            <td class="tdAlign">
                            </td>
                            <td>
                            </td>
                            <td align="left">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%--Bill To Address End--%>
            <%--Ship To Address Start--%>
            <tr>
                <td height="20" align="left" colspan="2">
                    <div id="divMainContainerTitle2" class="divMainContainerTitle">
                        <h2>
                            <asp:Literal runat="server" ID="lblShpToAddress" Text="<%$ Resources:Resource, lblShpToAddress%>"></asp:Literal></h2>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" style="border: 0;" bgcolor="#FFFFFF" class="table">
                        <tr height="25">
                            <td class="tdAlign" width="27%">
                                <asp:Label ID="lblchkBillToAddress" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, chkBillToAddress%>">
                                </asp:Label>
                            </td>
                            <td width="1%">
                            </td>
                            <td align="left" width="24%">
                                <asp:CheckBox ID="chkBillToAddress" runat="server" Text="" onclick="funCopyBillToAddress()" />
                            </td>
                            <td width="1%">
                            </td>
                            <td class="tdAlign" width="22%">
                            </td>
                            <td width="1%">
                            </td>
                            <td align="left" width="24%">
                            </td>
                        </tr>
                        <tr height="25">
                            <td class="tdAlign">
                                <asp:Label ID="lblShpToAddressLine1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressLine1%>">
                                </asp:Label>
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtShpToAddressLine1" runat="server" MaxLength="35" CssClass="txtBoxPrd">
                                </asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td class="tdAlign">
                                <asp:Label ID="lblShpToAddressLine2" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressLine2%>">
                                </asp:Label>
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtShpToAddressLine2" runat="server" MaxLength="35" CssClass="txtBoxPrd">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr height="25">
                            <td class="tdAlign">
                                <asp:Label ID="lblShpToAddressLine3" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressLine3%>">
                                </asp:Label>
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtShpToAddressLine3" runat="server" MaxLength="35" CssClass="txtBoxPrd">
                                </asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td class="tdAlign">
                                <asp:Label ID="lblShpToAddressCity" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressCity%>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtShpToAddressCity" runat="server" CssClass="txtBoxPrd" MaxLength="25" />
                            </td>
                        </tr>
                        <tr height="25">
                            <td class="tdAlign">
                                <asp:Label ID="lblShpToAddressState" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressState%>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtShpToAddressState" runat="server" CssClass="txtBoxPrd" MaxLength="25" />
                            </td>
                            <td>
                            </td>
                            <td class="tdAlign">
                                <asp:Label ID="lblShpToAddressCountry" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressCountry%>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtShpToAddressCountry" runat="server" CssClass="txtBoxPrd" MaxLength="25" />
                            </td>
                        </tr>
                        <tr height="25">
                            <td class="tdAlign">
                                <asp:Label ID="lblShpToAddressPostalCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressPostalCode %>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtShpToAddressPostalCode" runat="server" CssClass="txtBoxPrd" MaxLength="10" />
                            </td>
                            <td>
                            </td>
                            <td class="tdAlign">
                            </td>
                            <td>
                            </td>
                            <td align="left">
                            </td>
                        </tr>
                        <%--<tr height="30" id="trShpToCountryState" runat="server" visible="false">
                            <td class="tdAlign">
                                <asp:Label ID="lblShpToCountryState" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToCountryState %>">
                                </asp:Label>
                            </td>
                            <td class="tdAlignLeft">
                                <asp:UpdatePanel ID="upnlShpToState" runat="server" UpdateMode="Conditional">
                                    <contenttemplate>
                                                           
                    <asp:DropDownList ID="dlShpToCountry" runat="server" style="width:135px;" 
                        AutoPostBack="true">
                        <asp:ListItem Value="0" Text="<%$ Resources:Resource, SelectShpToCountry %>">
                        </asp:ListItem>
                    </asp:DropDownList>
                    
                    <asp:DropDownList ID="dlShpToState" runat="server" Style="width:135px;">
                        <asp:ListItem Value="0" Text="<%$ Resources:Resource, SelectShpToState %>"></asp:ListItem>
                    </asp:DropDownList>
                    <span class="style1"> *</span>
                    </contenttemplate>
                                    <triggers>
                    <asp:AsyncPostBackTrigger ControlID="dlCountry" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="dlShpToCountry" EventName="SelectedIndexChanged" />
                    </triggers>
                                </asp:UpdatePanel>
                                <asp:CustomValidator ID="custvalShpToCountry" runat="server" ClientValidationFunction="funShpToCountry"
                                    Display="None" ErrorMessage="<%$ Resources:Resource, PlzSelShpToCountry %>"> 
                                </asp:CustomValidator>
                                <asp:CustomValidator ID="custvalShpToState" runat="server" ClientValidationFunction="funShpToState"
                                    Display="None" ErrorMessage="<%$ Resources:Resource, PlzSelShpToState %>"> 
                                </asp:CustomValidator>
                            </td>
                        </tr>--%>
                    </table>
                </td>
            </tr>
            <%--Head Quater Address Start--%>
            <tr style="display:none;">
                <td height="20" align="left" colspan="2">
                    <div id="divMainContainerTitle3" class="divMainContainerTitle">
                        <h2>
                            <asp:Literal runat="server" ID="lblHeadQuaterAddress" Text="<%$ Resources:Resource, lblHeadQuaterAddress%>"></asp:Literal></h2>
                    </div>
                </td>
            </tr>
            <tr style="display:none;">
                <td colspan="2">
                    <table width="100%" style="border: 0;" bgcolor="#FFFFFF" class="table">
                        <tr height="25">
                            <td class="tdAlign" width="27%">
                                <asp:Label ID="Label2" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, chkHeadOfficeToAddress%>">
                                </asp:Label>
                            </td>
                            <td width="1%">
                            </td>
                            <td align="left" width="24%">
                                <asp:CheckBox ID="chkHeadOffice" runat="server" Text="" onclick="funCopyShipToAddress()" />
                            </td>
                            <td width="1%">
                            </td>
                            <td class="tdAlign" width="22%">
                            </td>
                            <td width="1%">
                            </td>
                            <td align="left" width="24%">
                            </td>
                        </tr>
                        <tr height="25">
                            <td class="tdAlign" width="27%">
                                <asp:Label ID="lblAddressLine1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblHQAddressLine1%>">
                                </asp:Label>
                            </td>
                            <td width="1%">
                            </td>
                            <td align="left" width="24%">
                                <asp:TextBox ID="txtAddressLine1" runat="server" MaxLength="35" CssClass="txtBoxPrd">
                                </asp:TextBox>
                            </td>
                            <td width="1%">
                            </td>
                            <td class="tdAlign" width="22%">
                                <asp:Label ID="lblAddressLine2" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblHQAddressLine2%>">
                                </asp:Label>
                            </td>
                            <td width="1%">
                            </td>
                            <td align="left" width="24%">
                                <asp:TextBox ID="txtAddressLine2" runat="server" MaxLength="35" CssClass="txtBoxPrd">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr height="25">
                            <td class="tdAlign">
                                <asp:Label ID="lblAddressLine3" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblHQAddressLine3%>">
                                </asp:Label>
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtAddressLine3" runat="server" MaxLength="35" CssClass="txtBoxPrd">
                                </asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td class="tdAlign">
                                <asp:Label ID="lblAddressCity" CssClass="lblBold" Text="<%$ Resources:Resource, lblHQAddressCity%>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtAddressCity" runat="server" CssClass="txtBoxPrd" MaxLength="25" />
                            </td>
                        </tr>
                        <tr height="25">
                            <td class="tdAlign">
                                <asp:Label ID="lblAddressState" CssClass="lblBold" Text="<%$ Resources:Resource, lblHQAddressState%>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtAddressState" runat="server" CssClass="txtBoxPrd" MaxLength="25" />
                            </td>
                            <td>
                            </td>
                            <td class="tdAlign">
                                <asp:Label ID="lblAddressCountry" CssClass="lblBold" Text="<%$ Resources:Resource, lblHQAddressCountry%>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtAddressCountry" runat="server" CssClass="txtBoxPrd" MaxLength="25" />
                            </td>
                        </tr>
                        <%--<tr height="30" id="trHQCountryState" runat="server" visible="false">
                            <td class="tdAlign">
                                <asp:Label ID="lblCountryState" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblHQCountryState %>">
                                </asp:Label>
                            </td>
                            <td class="tdAlignLeft">
                                <asp:UpdatePanel ID="upnlState" runat="server" UpdateMode="Conditional">
                                    <contenttemplate>
                                                           
                    <asp:DropDownList ID="dlCountry" runat="server" style="width:135px;" 
                        AutoPostBack="true">
                        <asp:ListItem Value="0" Text="<%$ Resources:Resource, SelectHQCountry %>">
                        </asp:ListItem>
                    </asp:DropDownList>
                    
                    <asp:DropDownList ID="dlState" runat="server" Style="width:135px;">
                        <asp:ListItem Value="0" Text="<%$ Resources:Resource, SelectHQState %>">
                        </asp:ListItem>
                    </asp:DropDownList>
                    <span class="style1"> *</span>
                    </contenttemplate>
                                    <triggers>
                    <asp:AsyncPostBackTrigger ControlID="dlCountry" EventName="SelectedIndexChanged" />
                    </triggers>
                                </asp:UpdatePanel>
                                <asp:CustomValidator ID="custvalHQCountry" runat="server" ClientValidationFunction="funCountry"
                                    Display="None" ErrorMessage="<%$ Resources:Resource, PlzSelHQCountry %>"> 
                                </asp:CustomValidator>
                                <asp:CustomValidator ID="custvalHQState" runat="server" ClientValidationFunction="funState"
                                    Display="None" ErrorMessage="<%$ Resources:Resource, PlzSelHQState %>"> 
                                </asp:CustomValidator>
                            </td>
                        </tr>--%>
                        <tr height="30">
                            <td class="tdAlign">
                                <asp:Label ID="lblAddressPostalCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblHQAddressPostalCode %>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtAddressPostalCode" runat="server" CssClass="txtBoxPrd" MaxLength="10" />
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="30">
                <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                    <table width="100%">
                        <tr>
                            <td height="20" align="center" width="30%">
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="cmdsave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                        style="min-width: 120px; text-align: center;">
                                        <%=Resources.Resource.cmdCssSave%>
                                    </span></a>
                                </div>
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="cmdBack" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                        <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssBack%>
                                        </span></a>
                                </div>
                                <div class="buttonwrapper">
                                    <a id="cmdReset" visible="false" runat="server" causesvalidation="false" class="ovalbutton"
                                        href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssCancel%>
                                        </span></a>
                                </div>
                            </td>
                            <td width="30%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table id="tblEvent" runat="server" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td height="20" align="left">
                    <div id="divMainContainerTitle4" class="divMainContainerTitle">
                        <table width="100%">
                            <tr>
                                <td width="75%">
                                    <h2>
                                        <asp:Literal runat="server" ID="lblTitleEvent" Text="<%$ Resources:Resource, CMTitleEvents%>"></asp:Literal></h2>
                                </td>
                                <td width="5%">
                                    <asp:UpdateProgress runat="server" ID="upEvent" DisplayAfter="10" AssociatedUpdatePanelID="upnlGridEvent">
                                        <ProgressTemplate>
                                            <img src="../Images/wait22trans.gif" />
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                                <td width="10%">
                                    <div class="buttonwrapper">
                                        <a id="cmdAddEvent" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                            <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                <%=Resources.Resource.cmdCssAdd%>
                                            </span></a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td valign="top" align="left">
                    <div class="divMainContent">
                        <asp:UpdatePanel ID="upnlGridEvent" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="grdEvent" runat="server" AllowSorting="True" DataSourceID="sqldsEvent"
                                    AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="3"
                                    GridLines="both" AutoGenerateColumns="False" ShowHeader="true" CssClass="table"
                                    UseAccessibleHeader="False" DataKeyNames="PartnerServiceLogID" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="PartnerServiceLogID" Visible="false" HeaderText="<%$ Resources:Resource, grdCMEventID %>"
                                            ReadOnly="True" SortExpression="PartnerServiceLogID">
                                            <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PartnerEvenLongName" HeaderText="<%$ Resources:Resource, grdCMEventName %>"
                                            ReadOnly="True" SortExpression="PartnerEvenLongName">
                                            <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PartnerSvcDateTime" HeaderText="<%$ Resources:Resource, grdCMEventDateTime %>"
                                            ReadOnly="True" SortExpression="PartnerSvcDateTime">
                                            <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PartnerSvcLog" HeaderText="<%$ Resources:Resource, grdCMEventDetails %>"
                                            ReadOnly="True" SortExpression="PartnerSvcLog">
                                            <ItemStyle Width="300px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PartnerNumberOfVisitors" HeaderText="<%$ Resources:Resource, grdCMEventNoOfVisitors %>"
                                            ReadOnly="True" SortExpression="PartnerNumberOfVisitors">
                                            <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, grdCMEdit %>" HeaderStyle-ForeColor="#ffffff"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEditEvent" runat="server" CommandArgument='<%# Eval("PartnerServiceLogID") %>'
                                                    CommandName="Edit" ImageUrl="~/images/edit_icon.png" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="30px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, grdCMDelete %>" HeaderStyle-ForeColor="#ffffff"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgDeleteEvent" runat="server" ImageUrl="~/images/delete_icon.png"
                                                    CommandArgument='<%# Eval("PartnerServiceLogID") %>' CommandName="Delete" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="30px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="grdFooterStyle" />
                                    <RowStyle ForeColor="#333333" />
                                    <EditRowStyle BackColor="#999999" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <PagerStyle BackColor="#5F6062" ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle"
                                        CssClass="lblBold" />
                                    <HeaderStyle BackColor="#5F6062" ForeColor="White" Font-Bold="true" HorizontalAlign="Left"
                                        VerticalAlign="Middle" Height="15" />
                                    <AlternatingRowStyle BackColor="#EDEDEE" ForeColor="#284775" />
                                    <PagerSettings PageButtonCount="20" />
                                </asp:GridView>
                                <asp:SqlDataSource ID="sqldsEvent" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                            </ContentTemplate>
                            <Triggers>
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table id="tblContacts" runat="server" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td align="left">
                    <div id="divMainContainerTitle5" class="divMainContainerTitle">
                        <table width="100%">
                            <tr>
                                <td width="75%">
                                    <h2>
                                        <asp:Literal runat="server" ID="lblTitleContacts" Text="<%$ Resources:Resource, CMTitleContacts%>"></asp:Literal></h2>
                                </td>
                                <td width="5%">
                                    <asp:UpdateProgress runat="server" ID="upContact" DisplayAfter="10" AssociatedUpdatePanelID="upnlGridContact">
                                        <ProgressTemplate>
                                            <img src="../Images/wait22trans.gif" />
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                                <td width="10%">
                                    <div class="buttonwrapper">
                                        <a id="cmdAddContacts" runat="server" causesvalidation="false" class="ovalbutton"
                                            href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                <%=Resources.Resource.cmdCssAdd%>
                                            </span></a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td valign="top" align="left">
                    <div class="divMainContent">
                        <asp:UpdatePanel ID="upnlGridContact" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="grdContact" runat="server" AllowSorting="True" DataSourceID="sqldsContact"
                                    AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="3"
                                    GridLines="both" AutoGenerateColumns="False" ShowHeader="true" CssClass="table"
                                    UseAccessibleHeader="False" DataKeyNames="ContactID" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="ContactID" Visible="false" HeaderText="<%$ Resources:Resource, grdCMContactID %>"
                                            ReadOnly="True" SortExpression="ContactID">
                                            <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ContactName" HeaderText="<%$ Resources:Resource, grdCMContactName %>"
                                            ReadOnly="True" SortExpression="ContactName">
                                            <ItemStyle Width="300px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ContactTitleEn" HeaderText="<%$ Resources:Resource, grdCMContactTitleEn %>"
                                            ReadOnly="True" SortExpression="ContactTitleEn">
                                            <ItemStyle Width="300px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ContactDOB" Visible="false" HeaderText="<%$ Resources:Resource, grdCMContactDOB %>"
                                            ReadOnly="True" DataFormatString="{0: MM/dd/yyyy}" SortExpression="ContactDOB">
                                            <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ContactEmail" HeaderText="<%$ Resources:Resource, grdCMContactEmail %>"
                                            ReadOnly="True" SortExpression="ContactEmail">
                                            <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ContactPhone" HeaderText="<%$ Resources:Resource, grdCMContactPhone %>"
                                            ReadOnly="True" SortExpression="ContactPhone">
                                            <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ContactPhoneExt" HeaderText="<%$ Resources:Resource, grdCMContactPhoneExt %>"
                                            ReadOnly="True" SortExpression="ContactPhoneExt">
                                            <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, grdCMEdit %>" HeaderStyle-ForeColor="#ffffff"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEditContact" runat="server" CommandArgument='<%# Eval("ContactID") %>'
                                                    CommandName="Edit" ImageUrl="~/images/edit_icon.png" />
                                                <div style="display: none;">
                                                    <asp:TextBox runat="server" Width="0px" ID="txtContactName" Text='<%# Eval("ContactLastName") & " " & Eval("ContactFirstName") %>'></asp:TextBox>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="30px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, grdCMDelete %>" HeaderStyle-ForeColor="#ffffff"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgDeleteContact" runat="server" ImageUrl="~/images/delete_icon.png"
                                                    CommandArgument='<%# Eval("ContactID") %>' CommandName="Delete" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="30px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="grdFooterStyle" />
                                    <RowStyle ForeColor="#333333" />
                                    <EditRowStyle BackColor="#999999" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <PagerStyle BackColor="#5F6062" ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle"
                                        CssClass="lblBold" />
                                    <HeaderStyle BackColor="#5F6062" ForeColor="White" Font-Bold="true" HorizontalAlign="Left"
                                        VerticalAlign="Middle" Height="15" />
                                    <AlternatingRowStyle BackColor="#EDEDEE" ForeColor="#284775" />
                                    <PagerSettings PageButtonCount="20" />
                                </asp:GridView>
                                <asp:SqlDataSource ID="sqldsContact" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                            </ContentTemplate>
                            <Triggers>
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsAdminDistributor" runat="server" ShowMessageBox="true"
            ShowSummary="false" />
        <br />
        <br />
        <script language="javascript" type="text/javascript">
            function openYellowPage(strOpen) {
                var browserWidth = 0;
                browserWidth = (document.documentElement.clientWidth - 820) / 2;
                window.open(strOpen, 'History', 'height=600,width=820,left=' + browserWidth + ',top=30,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');
                return false;
            }


            function funCheckCurrencyCode(source, args) {
                if (document.getElementById('<%=dlCurrencyCode.ClientID%>').selectedIndex == 0) {
                    args.IsValid = false;
                }
            }


            function funCheckSelTaxGroup(source, args) {
                if (document.getElementById('<%=dlTaxGroup.ClientID%>').selectedIndex == 0) {
                    args.IsValid = false;
                }
            }





            function funCopyShipToAddress() {
                if (window.document.getElementById('<%=chkHeadOffice.ClientID%>').checked == 1) {
                    window.document.getElementById('<%=txtAddressLine1.ClientID%>').value = window.document.getElementById('<%=txtShpToAddressLine1.ClientID%>').value;
                    window.document.getElementById('<%=txtAddressLine2.ClientID%>').value = window.document.getElementById('<%=txtShpToAddressLine2.ClientID%>').value;
                    window.document.getElementById('<%=txtAddressLine3.ClientID%>').value = window.document.getElementById('<%=txtShpToAddressLine3.ClientID%>').value;
                    window.document.getElementById('<%=txtAddressCity.ClientID%>').value = window.document.getElementById('<%=txtShpToAddressCity.ClientID%>').value;
                    window.document.getElementById('<%=txtAddressCountry.ClientID%>').value = window.document.getElementById('<%=txtShpToAddressCountry.ClientID%>').value;
                    window.document.getElementById('<%=txtAddressState.ClientID%>').value = window.document.getElementById('<%=txtShpToAddressState.ClientID%>').value;
                    window.document.getElementById('<%=txtAddressPostalCode.ClientID%>').value = window.document.getElementById('<%=txtShpToAddressPostalCode.ClientID%>').value;
                }
                else {
                    window.document.getElementById('<%=txtAddressLine1.ClientID%>').value = "";
                    window.document.getElementById('<%=txtAddressLine2.ClientID%>').value = "";
                    window.document.getElementById('<%=txtAddressLine3.ClientID%>').value = "";
                    window.document.getElementById('<%=txtAddressCity.ClientID%>').value = "";
                    window.document.getElementById('<%=txtAddressCountry.ClientID%>').value = "";
                    window.document.getElementById('<%=txtAddressState.ClientID%>').value = "";
                    window.document.getElementById('<%=txtAddressPostalCode.ClientID%>').value = "";
                }
            }
            function funCopyBillToAddress() {
                if (window.document.getElementById('<%=chkBillToAddress.ClientID%>').checked == 1) {
                    window.document.getElementById('<%=txtShpToAddressLine1.ClientID%>').value = window.document.getElementById('<%=txtBillToAddressLine1.ClientID%>').value;
                    window.document.getElementById('<%=txtShpToAddressLine2.ClientID%>').value = window.document.getElementById('<%=txtBillToAddressLine2.ClientID%>').value;
                    window.document.getElementById('<%=txtShpToAddressLine3.ClientID%>').value = window.document.getElementById('<%=txtBillToAddressLine3.ClientID%>').value;
                    window.document.getElementById('<%=txtShpToAddressCity.ClientID%>').value = window.document.getElementById('<%=txtBillToAddressCity.ClientID%>').value;
                    window.document.getElementById('<%=txtShpToAddressCountry.ClientID%>').value = window.document.getElementById('<%=txtBillToAddressCountry.ClientID%>').value;
                    window.document.getElementById('<%=txtShpToAddressState.ClientID%>').value = window.document.getElementById('<%=txtBillToAddressState.ClientID%>').value;
                    window.document.getElementById('<%=txtShpToAddressPostalCode.ClientID%>').value = window.document.getElementById('<%=txtBillToAddressPostalCode.ClientID%>').value;
                }
                else {
                    window.document.getElementById('<%=txtShpToAddressLine1.ClientID%>').value = "";
                    window.document.getElementById('<%=txtShpToAddressLine2.ClientID%>').value = "";
                    window.document.getElementById('<%=txtShpToAddressLine3.ClientID%>').value = "";
                    window.document.getElementById('<%=txtShpToAddressCity.ClientID%>').value = "";
                    window.document.getElementById('<%=txtShpToAddressCountry.ClientID%>').value = "";
                    window.document.getElementById('<%=txtShpToAddressState.ClientID%>').value = "";
                    window.document.getElementById('<%=txtShpToAddressPostalCode.ClientID%>').value = "";
                }
            }
        </script>
    </div>

    <script type="text/javascript">
        var msgBox = $("#msgbox");
        var txtCatName = $("#<%=txtCategoryNameEn.ClientID %>");


        $(document).ready(function () {
            $("#divAddCategory").hide();
        });

        $("#ancAddCategory").click(function () {
            $("#divAddCategory").fadeIn(200);
            $("#divAddCategory").show();
        });

        $("#ancClose").click(function () {
            $("#divAddCategory").hide();
        });

        $("#ancSaveCateg").click(function () {
            //Check data available or not
            if (txtCatName.val().trim() == "") {
                msgBox.fadeTo(200, 0.1, function () //start fading the messagebox
                {
                    //add message and change the class of the box and start fading
                    $(this).html('Category name field required.').addClass('c_messageboxerror').fadeTo(900, 1);
                });

                return false;
            }
            //remove all the class add the messagebox classes and start fading
            msgBox.removeClass().addClass('c_messagebox').text('Saving...').fadeIn("slow");

            $.post("AddCategory.aspx", { cName: txtCatName.val() }, function (data) {
                if (data == 'NO') {
                    msgBox.fadeTo(200, 0.1, function () //start fading the messagebox
                    {
                        //add message and change the class of the box and start fading
                        $(this).html('Please provide valid Category Name.').addClass('c_messageboxerror').fadeTo(900, 1);
                    });
                }
                else {
                    var tVal = txtCatName.val();
                    txtCatName.val("");
                    msgBox.fadeTo(200, 0.1, function ()  //start fading the messagebox
                    {
                        //add message and change the class of the box and start fading
                        $(this).html('Category added.').addClass('c_messageboxok').fadeTo(900, 1);
                    });

                    var eleID = "<%=lBoxAllCateg.ClientID %>";
                    addOption(eleID, tVal, data);
                }
            });
        });

        $("#btnAssingCat").click(function () {
            $("#<%=lBoxAllCateg.ClientID %>  option:selected").appendTo("#<%=lBoxAssignedCateg.ClientID %>");
            KeepAssignedValues();
        });

        $("#unAssignCat").click(function () {
            $("#<%=lBoxAssignedCateg.ClientID %>  option:selected").appendTo("#<%=lBoxAllCateg.ClientID %>");
            KeepAssignedValues();
        });

        $("body").click(function () {
            msgBox.fadeTo(200, 1);
            msgBox.hide();

        });

        function KeepAssignedValues() {
            var selectValues = "";
            $("#<%=lBoxAssignedCateg.ClientID %> option[value='']").remove();
            $("#<%=lBoxAssignedCateg.ClientID %> option").each(function () {
                selectValues = selectValues + $(this).val() + ",";                
            });
            $("#<%=hdnAssignedValues.ClientID %>").val(selectValues.substring(0, selectValues.length - 1));
        }

        function addOption(selectboxID, text, value) {
            $("#" + selectboxID).append('<option value="' + value + '">' + text + '</option>');            
        }
    </script>
</asp:Content>

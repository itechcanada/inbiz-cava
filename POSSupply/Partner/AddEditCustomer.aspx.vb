Imports System.Threading
Imports System.Collections.Generic
Imports System.Globalization
Imports AjaxControlToolkit

Imports iTECH.InbizERP.BusinessLogic

Partial Class Admin_AddEditCustomer
    Inherits BasePage
    Dim objAdd As New clsAddress
    Dim objPST As New clsPartnerSelType
    Dim objC As New clsPartnerContacts
    Private clsStatus As New clsSysStatus
    Protected Sub Admin_AddEditCustomer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If

        lblMsg.Text = ""
        lblError.Text = ""
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If
        If Request.QueryString("disable") = "yes" Or Request.QueryString("Module") = "TM" Then
            'pnlPartner.Enabled = False
            cmdAddContacts.Visible = False
            cmdAddEvent.Visible = False
            cmdBack.Visible = False
            cmdReset.Visible = False
            cmdsave.Visible = False
            cmdBackPartnerview.Visible = True
            grdEvent.Enabled = False
            grdContact.Enabled = False
            cmdSOHistory.Visible = False
            cmdInvoiceHistory.Visible = False
            cmdARHistory.Visible = False
            cmdCssStsPayHistory.Visible = False
        End If
       
        If (Request.QueryString("adCst") = "Y") Then
            cmdReset.Visible = True
        End If
        lblError.Text = ""

        If Not IsPostBack Then
            clsStatus.subGetStatus(dlDistStatus, "CU", "dlDis")
            clsStatus.subGetStatus(dlDistType, "CU", "dlTyp")
            lblTitle.Text = Resources.Resource.TitleCMAddCustomer
            subPopulatePartnerType()
            subPopulateCurrency()
            subPopulateTaxGroup()
            'subGetCountryName()
            Dim objCompany As New clsCompanyInfo
            If Request.QueryString("PartnerID") <> "" Then
                lblTitle.Text = Resources.Resource.TitleCMEditCustomer
                subFillRecord()
                subGetSRUser()
                subFillEvent()
                subFillContact()
                If ConfigurationManager.AppSettings("YellowSR").ToLower = "yes" Then
                    Me.cmdCssStsPayHistory.Attributes.Add("OnClick", "return openYellowPage('PayHistoryForYSR.aspx?ComName=" & txtCustName.Text & "')")
                End If
            Else
                tblEvent.Visible = False
                tblContacts.Visible = False
                subFillAddress()
                cmdBack.Visible = False
            End If

            If Request.QueryString("Col") <> "" Then
                cmdAddContacts.Visible = False
                cmdAddEvent.Visible = False
                cmdBack.Visible = True
                cmdReset.Visible = False
                cmdsave.Visible = False
                cmdBackPartnerview.Visible = False
                cmdSOHistory.Visible = False
                cmdInvoiceHistory.Visible = False
                cmdARHistory.Visible = False
                cmdCssStsPayHistory.Visible = False
                lblTitle.Text = Resources.Resource.TitleCMCustomerDtl
            End If
            FillCategoryDataList()
        End If
        If Request.QueryString("Module") = "TM" Then
            lblTitle.Text = Resources.Resource.TitleCMAddCustomer
            cmdBack.Visible = True
            cmdsave.Visible = True
            cmdBackPartnerview.Visible = False
            tblEvent.Visible = False
            tblContacts.Visible = False
        End If
        If Request.QueryString("OEnt") = "1" Or Request.QueryString("LeadLog") = "1" Then
            cmdAddContacts.Visible = True
            cmdAddEvent.Visible = False
            cmdBack.Visible = True
            cmdReset.Visible = False
            cmdsave.Visible = True
            cmdBackPartnerview.Visible = True
            grdEvent.Enabled = False
            grdContact.Enabled = True
            cmdSOHistory.Visible = False
            cmdInvoiceHistory.Visible = False
            cmdARHistory.Visible = False
            cmdCssStsPayHistory.Visible = False
        End If
        txtCustName.Focus()
    End Sub
    Public Function FunValueForYellowSRV() As String
        'Dim objContact As New clsPartnerContacts
        'Dim strQuery As String = "CustRef=" & Request.QueryString("PartnerID") & "&CustBusName=" & txtCustName.Text & ""
        'strQuery += "&CustAddr1=" & txtAddressLine1.Text & "&CustAddr2=" & txtAddressLine2.Text & ""
        'strQuery += "&CustCity=" & txtAddressCity.Text & "&CustState=" & txtAddressState.Text & ""
        'strQuery += "&CustCountry=" & txtAddressCountry.Text & "&CustZipCode=" & txtAddressPostalCode.Text & ""
        'strQuery += "&CustPhone1=" & txtDistPhone.Text & "&CustPhone2=" & txtPhoneNo2.Text & "&CustFax=" & txtDistFax.Text & ""
        'strQuery += "&CustEmail=" & txtDistEmail.Text & "&CustWebSite=" & txtWebsite.Text & ""
        'strQuery += "&CustCatgEN=" & txtDesc1.Text & "&CustCatgES=" & txtDesc2.Text & "&CustContactName=" & objContact.funContactPerSonName(Request.QueryString("PartnerID")) & ""
        ''strQuery = Server.UrlEncode(strQuery)
        'Return strQuery
    End Function
    'Fill Event Grid
    Private Sub subFillEvent()
        Dim objEvent As New clsPartnerEventLogs
        objEvent.PartnerID = Request.QueryString("PartnerID")
        sqldsEvent.SelectCommand = objEvent.funFillGrid
        objEvent = Nothing
    End Sub
    'Fill Contact Grid
    Private Sub subFillContact()
        Dim objContact As New clsPartnerContacts
        objContact.ContactPartnerID = Request.QueryString("PartnerID")
        sqldsContact.SelectCommand = objContact.funFillGrid
        objContact = Nothing
    End Sub
    Public Sub subFillAddress()
        Dim objCompany As New clsCompanyInfo
        objCompany.GetCompanyAddress()
        txtAddressCity.Text = objCompany.CompanyCity
        txtAddressState.Text = objCompany.CompanyState
        txtAddressCountry.Text = objCompany.CompanyCountry
        If objCompany.CompanySalesRepRestricted = "1" Then
            subFillSR()
        Else
            lblSalesRep.Visible = False
            divSR.Visible = False
        End If
        txtBillToAddressCity.Text = txtAddressCity.Text
        txtBillToAddressState.Text = txtAddressState.Text
        txtBillToAddressCountry.Text = txtAddressCountry.Text
        txtShpToAddressCity.Text = txtAddressCity.Text
        txtShpToAddressState.Text = txtAddressState.Text
        txtShpToAddressCountry.Text = txtAddressCountry.Text
    End Sub
    'Populate Controls
    Public Sub subFillRecord()

        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            Dim sPID As String = Convert.ToString(Request.QueryString("PartnerID"))
            Dim objP As New clsPartners
            objP.PartnerID = sPID
            objP.getPartnersInfo()

            txtCustName.Text = objP.PartnerLongName
            txtDistPhone.Text = objP.PartnerPhone
            txtAdditionalPhoneNo.Text = objP.PartnerAdditionPhone
            txtDistFax.Text = objP.PartnerFax
            txtDistEmail.Text = objP.PartnerEmail
            txtWebsite.Text = objP.PartnerWebsite
            rblstActive.SelectedValue = objP.PartnerActive
            txtAcronyme.Text = objP.PartnerAcronyme
            txtNotes.Text = objP.PartnerNote
            If objP.PartnerInvoicePreference = "Email" Then
                ddlInvoicePreference.SelectedValue = "E"
            ElseIf objP.PartnerInvoicePreference = "Mail" Then

                ddlInvoicePreference.SelectedValue = "M"
            ElseIf objP.PartnerInvoicePreference = "Fax" Then
                ddlInvoicePreference.SelectedValue = "F"

            End If

            txtCreditAvailable.Text = objP.PartnerCreditAvailable

            Dim objDR As Data.Odbc.OdbcDataReader
            'Populate Selection Type
            Dim objPST As New clsPartnerSelType
            Dim strCustType As String = ""
            objPST.PartnerSelPartID = sPID
            objDR = objPST.GetDataReader(objPST.funFillGrid())
            While objDR.Read
                For idx As Integer = 0 To rblstCustomerType.Items.Count - 1
                    If objDR.Item("PartnerSelTypeID").ToString = rblstCustomerType.Items(idx).Value Then
                        rblstCustomerType.Items(idx).Selected = True
                        strCustType = objDR.Item("PartnerSelTypeID").ToString
                        hdnLead.Value = strCustType
                    End If
                Next
            End While
            objDR.Close()
            objPST = Nothing

            rblstValidated.SelectedValue = objP.PartnerValidated

            txtDistCommissionCode.Text = objP.PartnerCommissionCode
            txtDistDiscount.Text = objP.PartnerDiscount
            dlCurrencyCode.SelectedValue = objP.PartnerCurrencyCode
            txtDistInvoiceNetTerms.Text = objP.PartnerInvoiceNetTerms
            rblstDistShipBlankPref.SelectedValue = objP.PartnerShipBlankPref
            txtDistCourierCode.Text = objP.PartnerCourierCode
            ddlLanguage.SelectedValue = objP.PartnerLang
            dlDistStatus.SelectedValue = objP.PartnerStatus
            dlTaxGroup.SelectedValue = objP.PartnerValue

            If Convert.ToString(objP.PartnerTaxCode) <> "" Then
                dlTaxGroup.SelectedValue = objP.PartnerTaxCode
            Else
                dlTaxGroup.SelectedValue = 0
            End If
            If strCustType = "1" Then
                strCustType = "D"
            ElseIf strCustType = "2" Then
                strCustType = "E"
            ElseIf strCustType = "3" Then
                strCustType = "R"
            ElseIf strCustType = "4" Then
                strCustType = "L"

                If objP.PartnerActive = 0 Then
                    lblRejectedReason.Visible = True
                    txtRejectedReason.Visible = True
                    txtRejectedReason.Text = objP.INActiveReason
                End If

            End If

            'If Request.QueryString("Module") = "TM" Then
            '    strCustType = "L"
            'End If
            hdnCustType.Value = strCustType
            subSetHQAddControl(strCustType, sPID)
            subSetBillToAddControl(strCustType, sPID)
            subSetShpToAddControl(strCustType, sPID)
            objP = Nothing
        End If
    End Sub
    'Populate HQ Address Controls
    Private Sub subSetHQAddControl(ByVal strCustType As String, ByVal custID As String, Optional ByVal strEdit As String = "")
        objAdd.getAddressInfo(strCustType, "H", custID)
        If strEdit = "" Then
            txtAddressLine1.Text = objAdd.AddressLine1
            txtAddressLine2.Text = objAdd.AddressLine2
            txtAddressLine3.Text = objAdd.AddressLine3
            txtAddressCity.Text = objAdd.AddressCity
            txtAddressState.Text = objAdd.AddressState
            txtAddressCountry.Text = objAdd.AddressCountry
            txtAddressPostalCode.Text = objAdd.AddressPostalCode
        End If
    End Sub
    'Populate BillTo Address Controls
    Private Sub subSetBillToAddControl(ByVal strCustType As String, ByVal VdrID As String, Optional ByVal strEdit As String = "")
        objAdd.getAddressInfo(strCustType, "B", VdrID)
        If strEdit = "" Then
            txtBillToAddressLine1.Text = objAdd.AddressLine1
            txtBillToAddressLine2.Text = objAdd.AddressLine2
            txtBillToAddressLine3.Text = objAdd.AddressLine3
            txtBillToAddressCity.Text = objAdd.AddressCity
            txtBillToAddressState.Text = objAdd.AddressState
            txtBillToAddressCountry.Text = objAdd.AddressCountry
            txtBillToAddressPostalCode.Text = objAdd.AddressPostalCode
        End If
    End Sub
    'Populate ShpTo Address Controls
    Private Sub subSetShpToAddControl(ByVal strCustType As String, ByVal VdrID As String, Optional ByVal strEdit As String = "")
        objAdd.getAddressInfo(strCustType, "S", VdrID)
        If strEdit = "" Then
            txtShpToAddressLine1.Text = objAdd.AddressLine1
            txtShpToAddressLine2.Text = objAdd.AddressLine2
            txtShpToAddressLine3.Text = objAdd.AddressLine3
            txtShpToAddressCity.Text = objAdd.AddressCity
            txtShpToAddressState.Text = objAdd.AddressState
            txtShpToAddressCountry.Text = objAdd.AddressCountry
            txtShpToAddressPostalCode.Text = objAdd.AddressPostalCode
        End If
    End Sub
    'Populate HQ Address Object
    Private Sub subSetHQAddress(ByVal VdrID As String)
        'objAdd.AddressType = "H"
        'objAdd.AddressSourceID = VdrID
        'objAdd.AddressLine1 = txtAddressLine1.Text
        'objAdd.AddressLine2 = txtAddressLine2.Text
        'objAdd.AddressLine3 = txtAddressLine3.Text
        'objAdd.AddressCity = txtAddressCity.Text
        'objAdd.AddressState = txtAddressState.Text
        'objAdd.AddressCountry = txtAddressCountry.Text
        'objAdd.AddressPostalCode = txtAddressPostalCode.Text
        subSetBillToAddress(VdrID)
        objAdd.AddressType = "H"
    End Sub
    'Populate BillTo Address Object
    Private Sub subSetBillToAddress(ByVal VdrID As String)
        objAdd.AddressType = "B"
        objAdd.AddressSourceID = VdrID
        If txtBillToAddressLine1.Text = "" Then
            objAdd.AddressLine1 = txtAddressLine1.Text
        Else
            objAdd.AddressLine1 = txtBillToAddressLine1.Text
        End If
        If txtBillToAddressLine2.Text = "" Then
            objAdd.AddressLine2 = txtAddressLine2.Text
        Else
            objAdd.AddressLine2 = txtBillToAddressLine2.Text
        End If
        If txtBillToAddressLine3.Text = "" Then
            objAdd.AddressLine3 = txtAddressLine3.Text
        Else
            objAdd.AddressLine3 = txtBillToAddressLine3.Text
        End If
        If txtBillToAddressCity.Text = "" Then
            objAdd.AddressCity = txtAddressCity.Text
        Else
            objAdd.AddressCity = txtBillToAddressCity.Text
        End If
        If txtBillToAddressState.Text = "" Then
            objAdd.AddressState = txtAddressState.Text
        Else
            objAdd.AddressState = txtBillToAddressState.Text
        End If
        If txtBillToAddressCountry.Text = "" Then
            objAdd.AddressCountry = txtAddressCountry.Text
        Else
            objAdd.AddressCountry = txtBillToAddressCountry.Text
        End If
        If txtBillToAddressPostalCode.Text = "" Then
            objAdd.AddressPostalCode = txtAddressPostalCode.Text
        Else
            objAdd.AddressPostalCode = txtBillToAddressPostalCode.Text
        End If
    End Sub
    'Populate ShpTo Address Object
    Private Sub subSetShpToAddress(ByVal VdrID As String)
        objAdd.AddressType = "S"
        objAdd.AddressSourceID = VdrID

        If txtShpToAddressLine1.Text = "" Then
            objAdd.AddressLine1 = txtAddressLine1.Text
        Else
            objAdd.AddressLine1 = txtShpToAddressLine1.Text
        End If
        If txtShpToAddressLine2.Text = "" Then
            objAdd.AddressLine2 = txtAddressLine2.Text
        Else
            objAdd.AddressLine2 = txtShpToAddressLine2.Text
        End If
        If txtShpToAddressLine3.Text = "" Then
            objAdd.AddressLine3 = txtAddressLine3.Text
        Else
            objAdd.AddressLine3 = txtShpToAddressLine3.Text
        End If
        If txtShpToAddressCity.Text = "" Then
            objAdd.AddressCity = txtAddressCity.Text
        Else
            objAdd.AddressCity = txtShpToAddressCity.Text
        End If
        If txtShpToAddressState.Text = "" Then
            objAdd.AddressState = txtAddressState.Text
        Else
            objAdd.AddressState = txtShpToAddressState.Text
        End If
        If txtShpToAddressCountry.Text = "" Then
            objAdd.AddressCountry = txtAddressCountry.Text
        Else
            objAdd.AddressCountry = txtShpToAddressCountry.Text
        End If
        If txtShpToAddressPostalCode.Text = "" Then
            objAdd.AddressPostalCode = txtAddressPostalCode.Text
        Else
            objAdd.AddressPostalCode = txtShpToAddressPostalCode.Text
        End If
    End Sub
    Public Sub subGetSRUser()
        Dim objCompany As New clsCompanyInfo
        objCompany.GetCompanyAddress()
        If objCompany.CompanySalesRepRestricted = "1" Then
            subFillUserSR(Request.QueryString("PartnerID"))
        Else
            lblSalesRep.Visible = False
            divSR.Visible = False
        End If
    End Sub
    'Populate Country List
    'Public Sub subGetCountryName()
    '    Dim objAdr As New clsAddress
    '    objAdr.subPopulateCountry(dlCountry)
    '    objAdr.subPopulateCountry(dlBillToCountry)
    '    objAdr.subPopulateCountry(dlShpToCountry)
    '    objAdr = Nothing
    'End Sub
    'Populate Partner Type
    Private Sub subPopulatePartnerType()
        Dim objPT As New clsPartnerType
        objPT.PopulatePartnerType(rblstCustomerType)
        For Each lt As ListItem In rblstCustomerType.Items
            If (lt.Value = "1") Then
                lt.Selected = True
            Else
                lt.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "none")
            End If
        Next
        objPT = Nothing
    End Sub
    'Populate Currency
    Private Sub subPopulateCurrency()
        Dim objCur As New clsCurrencies
        Dim objCompany As New clsCompanyInfo
        objCur.PopulateCurrency(dlCurrencyCode)
        dlCurrencyCode.SelectedValue = objCompany.funGetCompanyCurrency()
        objCur = Nothing
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    'Populate Tax Group
    Private Sub subPopulateTaxGroup()
        Dim objTG As New clsTaxCodeDesc
        Dim objWhs As New clsWarehouses
        objTG.PopulateTaxGroup(dlTaxGroup)
        objWhs.WarehouseCode = Session.Item("UserWarehouse")
        objWhs.getWarehouseInfo()
        If Convert.ToString(objWhs.WarehouseTaxCode) <> "" Then
            dlTaxGroup.SelectedValue = objWhs.WarehouseTaxCode
        Else
            dlTaxGroup.SelectedValue = 0
        End If
        objTG = Nothing
    End Sub
    'Routine to clear address controls
    Private Sub subClearAddress()
        txtAddressLine1.Text = ""
        txtAddressLine2.Text = ""
        txtAddressLine3.Text = ""
        txtAddressCity.Text = ""
        txtAddressState.Text = ""
        txtAddressCountry.Text = ""
        'dlCountry.SelectedValue = 0
        'dlState.SelectedValue = 0
        txtAddressPostalCode.Text = ""
        txtBillToAddressLine1.Text = ""
        txtBillToAddressLine2.Text = ""
        txtBillToAddressLine3.Text = ""
        txtBillToAddressCity.Text = ""
        txtBillToAddressState.Text = ""
        txtBillToAddressCountry.Text = ""
        'dlBillToCountry.SelectedValue = 0
        'dlBillToState.SelectedValue = 0
        txtBillToAddressPostalCode.Text = ""
        txtShpToAddressLine1.Text = ""
        txtShpToAddressLine2.Text = ""
        txtShpToAddressLine3.Text = ""
        txtShpToAddressCity.Text = ""
        txtShpToAddressState.Text = ""
        txtShpToAddressCountry.Text = ""
        'dlShpToCountry.SelectedValue = 0
        'dlShpToState.SelectedValue = 0
        txtShpToAddressPostalCode.Text = ""
        txtCreditAvailable.Text = ""
        txtAdditionalPhoneNo.Text = ""
    End Sub
    Private Sub subFillSR()
        Dim objP As New clsPartners
        objP.PopulateSR(chklstSalesRep)
        objP = Nothing
    End Sub
    Private Sub subFillUserSR(ByVal DistID As String)
        Dim objP As New clsPartners
        objP.PopulateUserSR(chklstSalesRep, DistID, hdnCustType.Value, Request.QueryString("Module"))
        objP = Nothing
    End Sub
    Private Sub subSaveSR(ByVal DistID As String, ByVal strCustType As String)
        Dim objP As New clsPartners
        objP.InsertSR(chklstSalesRep, DistID, strCustType)
        objP = Nothing
    End Sub
    Private Sub subUpdateSR(ByVal DistID As String, ByVal strCustType As String, ByVal strLead As String)
        Dim objP As New clsPartners
        objP.UpdateSR(chklstSalesRep, DistID, strCustType, strLead)
        objP = Nothing
    End Sub
    'Populate Object
    Private Sub subSetData(ByRef objP As clsPartners)
        objP.PartnerLongName = txtCustName.Text
        objP.PartnerPhone = txtDistPhone.Text
        objP.PartnerAdditionPhone = txtAdditionalPhoneNo.Text
        objP.PartnerFax = txtDistFax.Text
        objP.PartnerEmail = txtDistEmail.Text
        objP.PartnerWebsite = txtWebsite.Text
        objP.PartnerActive = rblstActive.SelectedValue

        objP.PartnerType = "1"
        objP.PartnerAcronyme = txtAcronyme.Text
        objP.PartnerNote = txtNotes.Text
        objP.PartnerInvoicePreference = ddlInvoicePreference.SelectedItem.Text
        objP.PartnerValidated = rblstValidated.SelectedValue
        objP.PartnerTaxCode = dlTaxGroup.SelectedValue
        objP.PartnerCommissionCode = txtDistCommissionCode.Text
        objP.PartnerDiscount = txtDistDiscount.Text.Trim
        objP.PartnerCurrencyCode = dlCurrencyCode.SelectedValue
        objP.PartnerInvoiceNetTerms = txtDistInvoiceNetTerms.Text
        objP.PartnerShipBlankPref = rblstDistShipBlankPref.SelectedValue
        objP.PartnerCourierCode = txtDistCourierCode.Text
        objP.PartnerLang = ddlLanguage.SelectedValue
        objP.PartnerStatus = dlDistStatus.SelectedValue
        objP.PartnerValue = dlTaxGroup.SelectedValue
        objP.PartnerCreditAvailable = txtCreditAvailable.Text

        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            objP.PartnerLastUpdatedBy = Session("UserID")
            objP.PartnerLastUpdatedOn = Now.ToString("yyyy-MM-dd HH:mm:ss")
        Else
            objP.PartnerCreatedBy = Session("UserID")
            objP.PartnerLastUpdatedBy = Session("UserID")
            objP.PartnerCreatedOn = Now.ToString("yyyy-MM-dd HH:mm:ss")
            objP.PartnerLastUpdatedOn = Now.ToString("yyyy-MM-dd HH:mm:ss")
        End If
    End Sub
    'Populate HQ Address Object
    Private Sub subSetContactAddress(ByVal sID As String)
        objAdd.AddressRef = "C"
        objAdd.AddressType = "B"
        objAdd.AddressSourceID = sID
        objAdd.AddressLine1 = txtBillToAddressLine1.Text
        objAdd.AddressLine2 = txtBillToAddressLine2.Text
        objAdd.AddressLine3 = txtBillToAddressLine3.Text
        objAdd.AddressCity = txtBillToAddressCity.Text
        objAdd.AddressState = txtBillToAddressState.Text
        objAdd.AddressCountry = txtBillToAddressCountry.Text
        objAdd.AddressPostalCode = txtBillToAddressPostalCode.Text
    End Sub
    Private Sub subSetData(ByVal strPartnerID As String)
        Dim strF = "", strL As String = ""
        Dim strName() As String
        objC.ContactPartnerID = strPartnerID
        objC.ContactSex = "M"
        If txtCustName.Text <> "" Then
            strName = txtCustName.Text.Split(" ")
            If strName.Length > 1 Then
                strF = strName(1)
                strL = strName(0)
            Else
                strL = strName(0)
            End If
        End If

        objC.ContactFirstName = strF
        objC.ContactLastName = strL
        objC.ContactEmail = txtDistEmail.Text
        objC.ContactActive = rblstActive.SelectedValue
        objC.ContactLangPref = ddlLanguage.SelectedValue
        objC.ContactPhone = txtDistPhone.Text
        objC.ContactFax = txtDistFax.Text
        objC.ContactSelSpecialization = "0"
        objC.ContactNote = txtNotes.Text
        objC.ContactPrimary = "1"
        objC.ContactDOB = CDate(Date.Now).ToString("yyyy-MM-dd")
        If Convert.ToString(Request.QueryString("ContactID")) <> "" Then
            objC.ContactLastUpdatedBy = Session("UserID")
            objC.ContactLastUpdatedOn = Now.ToString("yyyy-MM-dd HH:mm:ss")
        Else
            objC.ContactCreatedBy = Session("UserID")
            objC.ContactLastUpdatedBy = Session("UserID")
            objC.ContactCreatedOn = Now.ToString("yyyy-MM-dd HH:mm:ss")
            objC.ContactLastUpdatedOn = Now.ToString("yyyy-MM-dd HH:mm:ss")
        End If
    End Sub
    'On Event Page Index Change
    Protected Sub grdEvent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdEvent.PageIndexChanging
        subFillEvent()
    End Sub
    'On Event Row Data Bound
    Protected Sub grdEvent_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdEvent.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDeleteEvent"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & Resources.Resource.CMConfirmDeleteEvent & " ')")
            If Request.QueryString("Col") <> "" Or Request.QueryString("disable") = "yes" Or Request.QueryString("OEnt") = "1" Then
                grdEvent.Columns(5).Visible = False
                grdEvent.Columns(6).Visible = False
            End If
        End If
    End Sub
    'On Event Row Deleting
    Protected Sub grdEvent_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdEvent.RowDeleting
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            Dim objPEL As New clsPartnerEventLogs
            Dim strPartnerID As String = ""
            strPartnerID = Convert.ToString(Request.QueryString("PartnerID"))
            Dim strLogID As String = ""
            strLogID = grdEvent.DataKeys(e.RowIndex).Value.ToString()
            sqldsEvent.DeleteCommand = objPEL.funDeleteEvent(strLogID, strPartnerID)
            subFillRecord()
            subFillContact()
            subFillEvent()
            Session.Add("Msg", Resources.Resource.CMPartnerEventDeletedSuccessfully)
            lblMsg.Text = Resources.Resource.CMPartnerEventDeletedSuccessfully
            objPEL = Nothing
        End If
    End Sub
    'On Event Row Editing
    Protected Sub grdEvent_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdEvent.RowEditing
        Dim strLogID As String = ""
        strLogID = grdEvent.DataKeys(e.NewEditIndex).Value.ToString()
        Response.Redirect("AddEditEvent.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&LogID=" & strLogID)
    End Sub
    ' On Event Sorting
    Protected Sub grdEvent_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdEvent.Sorting
        subFillEvent()
    End Sub
    'On Contact Page Index Change
    Protected Sub grdContact_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdContact.PageIndexChanging
        subFillContact()
    End Sub
    'On Contact Row DataBound
    Protected Sub grdContact_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdContact.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDeleteContact"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & Resources.Resource.CMConfirmDeleteContact & " ')")
            Dim txtContactName As TextBox = CType(e.Row.FindControl("txtContactName"), TextBox)
            e.Row.Cells(1).Text = txtContactName.Text
            If Request.QueryString("Col") <> "" Or Request.QueryString("disable") = "yes" Then
                grdContact.Columns(7).Visible = False
                grdContact.Columns(8).Visible = False
            End If
        End If
    End Sub
    'On Contact Row Deleting
    Protected Sub grdContact_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdContact.RowDeleting
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            Dim strContactID As String = ""
            strContactID = grdContact.DataKeys(e.RowIndex).Value.ToString()
            Dim objC As New clsPartnerContacts
            sqldsContact.DeleteCommand = objC.funDeleteContact(strContactID) 'Delete Contact
            subFillRecord()
            subFillContact()
            subFillEvent()
            Session.Add("Msg", Resources.Resource.CMContactDeletedSuccessfully)
            lblMsg.Text = Resources.Resource.CMContactDeletedSuccessfully
            objC = Nothing
        End If
    End Sub
    'On Contact Row Editing
    Protected Sub grdContact_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdContact.RowEditing
        Dim strContactID As String = ""
        strContactID = grdContact.DataKeys(e.NewEditIndex).Value.ToString()
        If Request.QueryString("LeadLog") = "1" Then
            Response.Redirect("AddEditContact.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&ContactID=" & strContactID & "&LeadLog=" & Request.QueryString("LeadLog"))
        Else
            Response.Redirect("AddEditContact.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&ContactID=" & strContactID & "&OEnt=" & Request.QueryString("OEnt"))
        End If
    End Sub
    'On Contact Row Sorting
    Protected Sub grdContact_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdContact.Sorting
        subFillContact()
    End Sub
    Protected Sub cmdSOHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSOHistory.ServerClick
        Response.Redirect("~/Partner/ViewSalesOrder.aspx?PartnerID=" & Request.QueryString("PartnerID"))
    End Sub
    Protected Sub cmdInvoiceHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdInvoiceHistory.ServerClick
        Response.Redirect("~/Common/ViewInvoice.aspx?PartnerID=" & Request.QueryString("PartnerID"))
    End Sub
    'On Add Contacts
    Protected Sub cmdAddContacts_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddContacts.ServerClick
        If Request.QueryString("LeadLog") = "1" Then
            Response.Redirect("AddEditContact.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&LeadLog=" & Request.QueryString("LeadLog"))
        Else
            Response.Redirect("AddEditContact.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&OEnt=" & Request.QueryString("OEnt"))
        End If
    End Sub
    ' On Add Events
    Protected Sub cmdAddEvent_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddEvent.ServerClick
        Response.Redirect("AddEditEvent.aspx?PartnerID=" & Request.QueryString("PartnerID"))
    End Sub
    'On Back
    Protected Sub cmdBack_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.ServerClick
        If Request.QueryString("Col") <> "" Then
            Response.Redirect("~/CollectionAgent/ViewFollowUpLog.aspx?SOID=" & Request.QueryString("Col"))
        ElseIf Request.QueryString("Module") = "TM" Then
            Response.Redirect("~/TeleMarketing/LeadsActivities.aspx?SFU=" & Request.QueryString("SFU") & "&LP=" & Request.QueryString("LP"))
        ElseIf Request.QueryString("OEnt") = "1" Then
            Response.Redirect("~/Sales/Approval.aspx" & Session("OrderEntry"))
        ElseIf Request.QueryString("LeadLog") = "1" Then
            Response.Redirect("~/TeleMarketing/viewLeadsLog.aspx?PartnerID=" & Request.QueryString("PartnerID"))
        Else
            Response.Redirect("ViewCustomer.aspx")
        End If
    End Sub
    ' On Add Activity
    Protected Sub cmbAddActivity_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAddActivity.ServerClick
        Response.Redirect("~/Partner/AddEditFollowups.aspx?PartnerId=" & Request.QueryString("PartnerID"))
    End Sub

    Protected Sub cmdBackPartnerview_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBackPartnerview.ServerClick
        If Request.QueryString("OEnt") = "1" Then
            Response.Redirect("~/Sales/Approval.aspx" & Session("OrderEntry"))
        ElseIf Request.QueryString("LeadLog") = "1" Then
            Response.Redirect("~/TeleMarketing/viewLeadsLog.aspx?PartnerID=" & Request.QueryString("PartnerID"))
        Else
            Response.Redirect("~/Partner/ViewCustomer.aspx")
        End If
    End Sub
    Protected Sub cmdReset_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReset.ServerClick
        If (Request.QueryString("adCst") = "Y") Then
            Response.Redirect("~/Sales/Generate.aspx")
        End If
    End Sub
    Protected Sub cmdsave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.ServerClick
        If Not Page.IsValid Then
            Exit Sub
        End If
        Dim objPartner As New clsPartners
        subSetData(objPartner)
        Dim strPartnerID As String = ""
        Dim strContactID As String = ""
        Dim strCust As String
        Dim blnCustType As Boolean = False
        For idx As Integer = 0 To rblstCustomerType.Items.Count - 1
            If rblstCustomerType.Items(idx).Selected = True Then
                strCust = rblstCustomerType.Items(idx).Value
                blnCustType = True
            End If
        Next

        If blnCustType = False Then
            lblError.Text = Resources.Resource.msgCMSelCustType
            Exit Sub
        End If
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            strPartnerID = Convert.ToString(Request.QueryString("PartnerID"))
            'If Request.QueryString("Module") = "TM" Then
            '    subSetHQAddControl("L", strPartnerID, "1") 'TeleMarketing
            'Else

            'End If

            objPartner.PartnerID = strPartnerID
            If objPartner.updatePartners() Then 'Update 
                'subSetHQAddress(strPartnerID)

                For idx As Integer = 0 To rblstCustomerType.Items.Count - 1
                    objPST.PartnerSelPartID = strPartnerID
                    objPST.PartnerSelTypeID = rblstCustomerType.Items(idx).Value
                    If rblstCustomerType.Items(idx).Selected = True Then
                        'If Request.QueryString("Module") = "TM" Then 'TeleMarketing
                        '    objPST.PartnerSelTypeID = "4"
                        '    objPST.deletePartnerSelType()
                        '    objPST.PartnerSelTypeID = rblstCustomerType.Items(idx).Value
                        '    objPST.insertPartnerSelType()
                        'Else
                        If objPST.funCheckRecord() = False Then
                            objPST.insertPartnerSelType()
                        Else
                            objPST.updatePartnerSelType()
                        End If
                        ' End If
                    Else
                    objPST.deletePartnerSelType()
                    End If
                Next
                objPST.getPartnerSelTypeInfo()
                serRef()
                'If Request.QueryString("Module") = "TM" Then
                '    objAdd.AddressRef = "L"
                'End If
                subSetHQAddControl(hdnCustType.Value, strPartnerID, "1")

                If objAdd.funCheckDuplicateAddress() = True Then
                    objAdd.AddressID = objAdd.funGetAddressID()
                    subSetHQAddress(strPartnerID)
                    serRef()
                    objAdd.updateAddress()
                Else
                    subSetHQAddress(strPartnerID)
                    serRef()
                    objAdd.insertAddress()
                End If
                'subSetBillToAddress(strPartnerID)
                subSetBillToAddControl(hdnCustType.Value, strPartnerID, "1")
                'If Request.QueryString("Module") = "TM" Then
                '    objAdd.AddressRef = "L"
                'End If
                If objAdd.funCheckDuplicateAddress() = True Then
                    objAdd.AddressID = objAdd.funGetAddressID()
                    subSetBillToAddress(strPartnerID)
                    serRef()
                    objAdd.updateAddress()
                Else
                    subSetBillToAddress(strPartnerID)
                    serRef()
                    objAdd.insertAddress()
                End If
                ' subSetShpToAddress(strPartnerID)
                subSetShpToAddControl(hdnCustType.Value, strPartnerID, "1")

                'If Request.QueryString("Module") = "TM" Then
                '    objAdd.AddressRef = "L"
                'End If
                If objAdd.funCheckDuplicateAddress() = True Then
                    objAdd.AddressID = objAdd.funGetAddressID()
                    subSetShpToAddress(strPartnerID)
                    serRef()
                    objAdd.updateAddress()
                Else
                    subSetShpToAddress(strPartnerID)
                    serRef()
                    objAdd.insertAddress()
                End If
                'If Request.QueryString("Module") = "TM" Then
                '    subUpdateSR(strPartnerID, objAdd.AddressRef, "L")
                'Else
                'subUpdateSR(strPartnerID, objAdd.AddressRef, "")

                Dim objP As New clsPartners
                If Request.QueryString("Module") = "TM" Then
                    subSaveSR(strPartnerID, objAdd.AddressRef)
                Else
                    objP.UpdateSR(chklstSalesRep, strPartnerID, objAdd.AddressRef, hdnCustType.Value)
                End If

                objP = Nothing

                'End If

                If objPST.PartnerSelTypeID = "2" Then
                    subSetData(strPartnerID)
                    'If Request.QueryString("Module") = "TM" Then
                    '    objC.IsSubscribeEmail = 1
                    '    If objC.insertPartnerContacts(strContactID) = True Then 'Insert Partner
                    '        If strPartnerID <> "" Then
                    '            subSetContactAddress(strContactID)
                    '            objAdd.insertAddress()
                    '        End If
                    '    End If
                    'Else
                    objC.ContactPartnerID = strPartnerID
                    objC.ContactID = objC.funGetContactID()
                    strContactID = objC.ContactID
                    objC.funUpdateContactsforEndClient()
                    objAdd.getAddressInfo("C", "B", strContactID)
                    If objAdd.funCheckDuplicateAddress() = True Then
                        objAdd.AddressID = objAdd.funGetAddressID()
                        subSetContactAddress(strContactID)
                        objAdd.updateAddress()
                    Else
                        subSetContactAddress(strContactID)
                        objAdd.insertAddress()
                    End If
                    ' End If
                End If
                AssignCategories(Request.QueryString("PartnerID"))
                Session.Add("Msg", Resources.Resource.CMCustomerUpdatedSuccessfully)
            Else
                Session.Add("Msg", Resources.Resource.OperationFailed)
            End If

            If Request.QueryString("Module") = "TM" Then
                Session.Add("Msg", Resources.Resource.CMleadUpdatedSuccessfully)
                serRef()
                If hdnLead.Value = 4 Then
                    If objAdd.AddressRef <> "E" Then
                        objC.ContactPartnerID = strPartnerID
                        strContactID = objC.funGetContactID()
                        Response.Redirect("AddEditContact.aspx?PartnerID=" & strPartnerID & "&CustType=" & objAdd.AddressRef & "&Module=TM&ContactID=" & strContactID & "&SFU=" & Request.QueryString("SFU") & "&LP=" & Request.QueryString("LP"))
                    End If
                End If
                Dim objCompany As New clsCompanyInfo
                objCompany.GetCompanyAddress()
                Response.Redirect("~/Sales/Create.aspx?custID=" & strPartnerID & "&custName=" & objPartner.PartnerLongName & "&ComID=" & objCompany.CompanyID & "&whsID=" & Session("UserWarehouse") & "&Module=" & Request.QueryString("Module") & "&SFU=" & Request.QueryString("SFU") & "&LP=" & Request.QueryString("LP"))
                'Response.Redirect("~/TeleMarketing/LeadsActivities.aspx")
            End If

            If Request.QueryString("OEnt") = "1" Then
                Response.Redirect("~/Sales/Approval.aspx" & Session("OrderEntry"))
            ElseIf Request.QueryString("LeadLog") = "1" Then
                Response.Redirect("~/TeleMarketing/viewLeadsLog.aspx?PartnerID=" & Request.QueryString("PartnerID"))
            Else
                Response.Redirect("~/Partner/ViewCustomer.aspx")
            End If
            Exit Sub
        Else
            If objPartner.insertPartners(strPartnerID) = True Then 'Insert Partner

                Try
                    AssignCategories(strPartnerID)
                Catch ex As Exception

                End Try

                For idx As Integer = 0 To rblstCustomerType.Items.Count - 1
                    If rblstCustomerType.Items(idx).Selected = True Then
                        objPST.PartnerSelPartID = strPartnerID
                        objPST.PartnerSelTypeID = rblstCustomerType.Items(idx).Value
                        objPST.insertPartnerSelType()
                    End If
                Next
                serRef()
                If strPartnerID <> "" Then
                    subSetHQAddress(strPartnerID)
                    objAdd.insertAddress()
                    subSetBillToAddress(strPartnerID)
                    objAdd.insertAddress()
                    subSetShpToAddress(strPartnerID)
                    objAdd.insertAddress()
                    subSaveSR(strPartnerID, objAdd.AddressRef)
                    If objPST.PartnerSelTypeID = "2" Then
                        subSetData(strPartnerID) 'got contact
                        objC.IsSubscribeEmail = 1
                        If objC.insertPartnerContacts(strContactID) = True Then 'Insert Partner
                            If strPartnerID <> "" Then
                                subSetContactAddress(strContactID)
                                objAdd.insertAddress()
                            End If
                        End If
                    End If
                End If
                If (Request.QueryString("adCst") = "Y") And strCust = 2 Then
                    Session.Add("Msg", Resources.Resource.CMCustomerAddedSuccessfully)
                    Response.Redirect("~/Sales/Generate.aspx?cID=" & strPartnerID & "&cName=" & txtCustName.Text)
                End If
                If strCust = 1 Or strCust = 3 Then
                    If strCust = 1 Then
                        strCust = "D"
                    ElseIf strCust = 3 Then
                        strCust = "R"
                    End If
                    If (Request.QueryString("adCst") = "Y") Then
                        Response.Redirect("AddEditContact.aspx?PartnerID=" & strPartnerID & "&CustType=" & strCust & "&adCst=" & Request.QueryString("adCst"))
                    End If
                    Response.Redirect("AddEditContact.aspx?PartnerID=" & strPartnerID & "&CustType=" & strCust)
                End If
                Session.Add("Msg", Resources.Resource.CMCustomerAddedSuccessfully)
                Response.Redirect("~/Partner/ViewCustomer.aspx")
                Exit Sub
            End If
        End If
    End Sub
    Public Sub serRef()
        If objPST.PartnerSelTypeID = "1" Then
            objAdd.AddressRef = "D"
        ElseIf objPST.PartnerSelTypeID = "2" Then
            objAdd.AddressRef = "E"
        ElseIf objPST.PartnerSelTypeID = "3" Then
            objAdd.AddressRef = "R"
        ElseIf objPST.PartnerSelTypeID = "4" Then
            objAdd.AddressRef = "L"
        End If
    End Sub
    Protected Sub cmdARHistory_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdARHistory.ServerClick
        Response.Redirect("~/AccountsReceivable/ViewAccountsReceivable.aspx?CustName=" & txtCustName.Text & "&PartnerID=" & Request.QueryString("PartnerID"))
    End Sub
    Protected Sub cmdNotes_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNotes.ServerClick
        Response.Redirect("~/partner/Notes.aspx" & Request.UrlReferrer.Query)
    End Sub

    Private Sub FillCategoryDataList()
        lBoxAllCateg.Items.Clear()
        lBoxAssignedCateg.Items.Clear()

        Dim objCateg As New BLLCategories
        Dim lang As Integer = ApplicationLanguage.English
        If Not Session("Language") Is Nothing AndAlso Session("Language") = "fr-CA" Then
            lang = ApplicationLanguage.French
        End If

        Dim dtCategories As New System.Data.DataTable()
        dtCategories = objCateg.GetAllCategories(lang)

        If Not String.IsNullOrEmpty(Request.QueryString("PartnerID")) Then
            Dim custID As Integer
            Integer.TryParse(Request.QueryString("PartnerID"), custID)

            Dim categList As New List(Of Integer)
            categList = objCateg.GetAllassignedCategories(custID)


            For Each row As System.Data.DataRow In dtCategories.Rows
                If categList.Contains(Integer.Parse(row("CategoryID"))) Then
                    lBoxAssignedCateg.Items.Add(New ListItem(row("TextDescription").ToString(), row("CategoryID").ToString()))
                Else
                    lBoxAllCateg.Items.Add(New ListItem(row("TextDescription").ToString(), row("CategoryID").ToString()))
                End If
            Next
        Else
            lBoxAllCateg.DataSource = objCateg.GetAllCategories(lang)
            lBoxAllCateg.DataTextField = "TextDescription"
            lBoxAllCateg.DataValueField = "CategoryID"
            lBoxAllCateg.DataBind()
        End If
    End Sub

    Private Sub AssignCategories(ByVal partnerId As String)
        Dim custID As Integer = 0
        Dim objCateg As New BLLCategories
        Dim categList As New List(Of Integer)

        If Integer.TryParse(partnerId, custID) Then
            If hdnAssignedValues.Value.Length > 0 Then
                For Each lItem As String In hdnAssignedValues.Value.Split(",")
                    categList.Add(CType(lItem, Integer))
                Next
            End If

            objCateg.AssignCategoriesToCustomer(categList.ToArray(), custID)
        End If
    End Sub
End Class

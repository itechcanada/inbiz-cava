Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Data.Odbc

Partial Class Partner_ReadAlert
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If IsPostBack Then
            Exit Sub
        End If
        If Request.QueryString("id") <> "" Then
            RemoveAlert(Request.QueryString("id"))
        End If
        If Request.QueryString("id") <> "" Then
            GenerateAlert(Request.QueryString("id"))
        End If
    End Sub

    Public Sub RemoveAlert(ByVal id As String)
        Dim strSql As String = "Update alerts set AlertViewCount=1, AlertLastViewedOn=now() where AlertID=" & id & " "
        Dim objDB As New clsDataClass
        objDB.SetData(strSql)
    End Sub

    Public Sub GenerateAlert(ByVal id As String)
        Dim strSql As String = "SELECT AlertID, PartnerID, ContactID, PartnerLongName, concat(ContactLastName, ' ', ContactFirstName) as ContactName, AlertNote, AlertDateTime FROM alerts inner join partnercontacts on ContactID=AlertPartnerContactID inner join partners on PartnerID=ContactPartnerID where AlertID='" & id & "'"

        Dim objDB As New clsDataClass
        Dim dr As OdbcDataReader = objDB.GetDataReader(strSql)
        While dr.Read
            PartnerName.Text = dr.Item("PartnerLongName").ToString()
            ContactName.Text = dr.Item("ContactName").ToString()
            AlertTime.Text = dr.Item("AlertDateTime").ToString()
            AlertNote.Text = dr.Item("AlertNote").ToString()
            imgContact.PostBackUrl = "AddEditContact.aspx?PartnerID=" & dr.Item("PartnerID").ToString() & "&ContactID=" & dr.Item("ContactID").ToString()
        End While

        dr.Close()
        objDB.CloseDatabaseConnection()
    End Sub

    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
End Class

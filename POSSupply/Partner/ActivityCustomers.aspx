<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false" CodeFile="ActivityCustomers.aspx.vb" Inherits="Partner_ActivityCustomers" title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="Resources.Resource" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">   
    <script language="javascript" type="text/javascript">
        $('#divSearchTitle').corner();
        $('#<%=pnlSearchForm.ClientID %>').corner();
        $('#divMainContainerTitle').corner();        
    </script>
    
    <div id="divSearchTitle" class="divSectionTitle"><h2> <%=Resources.Resource.lblSearchOptions%> </h2></div>
    <asp:Panel runat="server" ID="pnlSearchForm" CssClass="divSectionContent" DefaultButton="imgSearch">
        <table id="Table1" cellpadding="1" cellspacing="1" border="0" width="100%" runat="server">
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lblStatus %>"></asp:Label>
                    <br class="brMargin5"/>
                    <asp:DropDownList ID="ddlStatus" runat="server" Width="175px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"></asp:Label>
                    <br class="brMargin5"/>
                    <asp:DropDownList ID="dlSearchCatg" runat="server" Width="175px" 
                        ValidationGroup="PrdSrch">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword%>"></asp:Label>
                    <br class="brMargin5"/>
                    <asp:TextBox runat="server" Width="170px" ID="txtSearch"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="imgSearch" runat="server" AlternateText="Search" ImageUrl="../images/search-btn.gif" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <h2>
                        <asp:Literal ID="lblTitle" runat="server"></asp:Literal>
                    </h2>
                </td>
                <td align="right">
                    <asp:UpdateProgress runat="server" ID="upMaster" DisplayAfter="10">
                        <ProgressTemplate>
                            <img src="../Images/wait22trans.gif" alt="" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent" onkeypress="return disableEnterKey(event)">
        <table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
            <tr>
                <td>
                    <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                    <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="2" align="right" style="padding-right: 10px;">
                    <asp:Label ID="Label1" ForeColor="black" CssClass="lblBold" runat="server" Text="<%$ Resources:Resource, lblLeads %>"></asp:Label>&nbsp;&nbsp;&nbsp;
                    <asp:Image ID="Image1" ImageUrl="~/Images/blue.JPG" runat="server" />
                </td>
            </tr>
            <tr>
                <td height="350" valign="top">
                    <asp:GridView ID="grdPartner" runat="server" AllowSorting="True" DataSourceID="sqldsPartner"
                        AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0"
                        GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
                        CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="PartnerID"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="PartnerID" Visible="false" HeaderText="<%$ Resources:Resource, grdCMcustomerID %>"
                                ReadOnly="True" SortExpression="PartnerID">
                                <ItemStyle Width="75px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, lblCustName %>" HeaderStyle-ForeColor="#ffffff"
                                HeaderStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblPartnerName" ForeColor="black"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="PartnerEmail" HeaderText="<%$ Resources:Resource, grdCMEmail %>"
                                ReadOnly="True" SortExpression="PartnerEmail">
                                <ItemStyle Width="140px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdCMPhone %>" HeaderStyle-ForeColor="#ffffff"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%--  <table width="100PX" cellpadding=0 cellspacing=0 border=0 style="border-width:0;">
                          <tr>
                           <td  align="Center">--%>
                                    <a runat="server" style="color: Black;" id="aPhone1">
                                        <img src='<% = ConfigurationManager.AppSettings("skypeImage") %>' style="border-color: White;
                                            border-width: 0;" id="img1" alt='<%# Container.DataItem("PartnerPhone")%>' /></a>&nbsp;&nbsp;
                                    <%--   </td> 
                           <td  align="Center">--%>
                                    <a runat="server" style="color: Black;" id="aPhone2">
                                        <img src='<% = ConfigurationManager.AppSettings("skypeImage") %>' style="border-color: White;
                                            border-width: 0;" id="img2" alt='<%# Container.DataItem("PartnerPhone2")%>' /></a>
                                    <br />
                                    <%--   </td>    
                          </tr>
                          <tr>
                           <td align="Center">--%>
                                    <%# Container.DataItem("PartnerPhone")%>
                                    &nbsp;&nbsp;
                                    <%--</td>
                           <td align="Center">--%>
                                    <%# Container.DataItem("PartnerPhone2")%>
                                    <%-- </td>
                          </tr>
                          
                        </table>--%>
                                </ItemTemplate>
                                <ItemStyle Width="70px" Wrap="true" HorizontalAlign="center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, btnActivities %>" HeaderStyle-HorizontalAlign="Left"
                                HeaderStyle-ForeColor="white">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="lnkActivities" Font-Bold="true" CommandName="Activities"
                                        CommandArgument='<%# Eval("PartnerID") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" Width="40px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdHDLastFollowupDate %>"
                                HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="white" SortExpression="LastFollowUpDate">
                                <ItemTemplate>
                                    <%#Eval("LastFollowUpDate", "{0:MM/dd/yyyy hh:mm tt}")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdHDLastActivityDate %>"
                                HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="white" SortExpression="LastActivityDate">
                                <ItemTemplate>
                                    <%#Eval("LastActivityDate" , "{0:MM/dd/yyyy hh:mm tt}")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" Width="80px" />
                            </asp:TemplateField>
                            <asp:ImageField HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="center"
                                HeaderText="" DataImageUrlField="custType" DataImageUrlFormatString="~/images/{0}">
                                <ItemStyle HorizontalAlign="center" Width="0px" VerticalAlign="Middle" />
                            </asp:ImageField>
                            <asp:ImageField HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="center"
                                HeaderText="<%$ Resources:Resource, grdCMPartnerStatus %>" DataImageUrlField="PartnerActive"
                                DataImageUrlFormatString="~/images/{0}">
                                <ItemStyle HorizontalAlign="center" Width="0px" VerticalAlign="Middle" />
                            </asp:ImageField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, lblReviewLog %>" HeaderStyle-ForeColor="#ffffff"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgLog" runat="server" ImageUrl="~/images/view_24.png" ToolTip="<%$ Resources:Resource, lblReviewLog %>"
                                        CommandArgument='<%# Eval("PartnerID") %>' CommandName="ViewLog" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="grid_footer" />
                        <RowStyle CssClass="grid_rowstyle" />
                        <PagerStyle CssClass="grid_footer" />
                        <HeaderStyle CssClass="grid_header" Height="26px" />
                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                        <PagerSettings PageButtonCount="20" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqldsPartner" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>


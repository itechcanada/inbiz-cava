Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Partial Class Partner_PayHistoryForYSR
    Inherits BasePage
    Dim objPayYSR As New clsPayHistoryForYSR
    Protected Sub Partner_PayHistoryForYSR_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            subClose()
            Exit Sub
        End If
        lblComName.Text = Request.QueryString("ComName")
        If Not Page.IsPostBack Then
            subFillGridForStatus()
            subFillGridForPayment()
        End If
    End Sub
    'Populate Grid
    Public Sub subFillGridForStatus()
        sqldsStatus.SelectCommand = objPayYSR.funFillGridForStatus(Request.QueryString("ComName"))  'fill Status Record
    End Sub
    'Populate Grid
    Public Sub subFillGridForPayment()
        sqldsPayment.SelectCommand = objPayYSR.funFillGridForPaymentHistory(Request.QueryString("ComName"))  'fill Status Record
    End Sub
    Protected Sub grdStatus_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdStatus.PageIndexChanging
        subFillGridForStatus()
    End Sub
    Protected Sub grdStatus_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdStatus.Sorting
        subFillGridForStatus()
    End Sub
    Protected Sub grdPayament_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdPayament.PageIndexChanging
        subFillGridForPayment()
    End Sub
    Protected Sub grdPayament_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdPayament.Sorting
        subFillGridForPayment()
    End Sub
    ' Sub routine to close popup
    Private Sub subClose()
        Response.Write("<script>window.opener.location.reload();</script>")
        Response.Write("<script>window.close();</script>")
    End Sub
End Class

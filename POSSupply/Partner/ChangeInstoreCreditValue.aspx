﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="ChangeInstoreCreditValue.aspx.cs" Inherits="Partner_ChangeInstoreCreditValue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="contentBottom" style="padding:5px; height:100px; overflow:auto;">
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
        <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0">
            
            <tr>
                <td class="text">
                     <asp:Label ID="lblInstoreCredit" CssClass="lblBold" Text="<%$ Resources:Resource, lblInstoreCredit%>"
                        runat="server" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtAvailableCredit" runat="server" MaxLength="13" CssClass="numericTextField" />
                </td>
            </tr>
            
        </table>        
    </div>
    
        <br />
        <div class="div-dialog-command">
            <asp:Button Text="<%$Resources:Resource, btnSave %>"  ID="btnSave" runat="server" OnClick="btnSave_Click" />
            <asp:Button Text="<%$Resources:Resource, btnCancel %>"  ID="btnCancel" runat="server" CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
            <div class="clear">
            </div>
        </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
       
        </script>
</asp:Content>
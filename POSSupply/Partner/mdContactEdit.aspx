﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="mdContactEdit.aspx.cs" Inherits="Partner_mdContactEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" Runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="top">
                <ul class="form">
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblPN" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMPartnerName%>"
                                runat="server" />
                        </div>
                        <div class="input">
                            <asp:Label ID="lblPartnerName" CssClass="lblBold" runat="server" Font-Size="12px" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblIsActive" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMIsActive%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:RadioButtonList ID="rblstActive" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="<%$ Resources:Resource, lblCMYes%>" Value="1" Selected="True">
                                </asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, lblCMNo%>" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblSalutation" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMSalutation%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtSalutation" runat="server" MaxLength="10" Width="200px" /></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblLastName" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMLastName%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtLastName" runat="server" MaxLength="35" Width="200px" />
                            <span class="style1">*</span>
                            <asp:RequiredFieldValidator ID="reqvalLastName" runat="server" ControlToValidate="txtLastName"
                                ErrorMessage="<%$ Resources:Resource, reqvalCMLastName%>" SetFocusOnError="true"
                                Display="None">
                            </asp:RequiredFieldValidator></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblFirstName" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMFirstName%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtFirstName" runat="server" Width="200px" MaxLength="35">
                            </asp:TextBox></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblAddLine1" CssClass="lblBold" Visible="false" Text="<%$ Resources:Resource, lblCMAddLine1%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtAddLine1" runat="server" Width="200px" MaxLength="35">
                            </asp:TextBox></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblAddLine2" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMAddLine2%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtAddLine2" runat="server" Width="200px" MaxLength="35">
                            </asp:TextBox></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblCity" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMCity%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtCity" runat="server" Width="200px" MaxLength="35">
                            </asp:TextBox></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblDistrict" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMDistrict%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtDistrict" runat="server" Width="200px" MaxLength="35">
                            </asp:TextBox></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblProv" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMProv%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtProv" runat="server" Width="200px" MaxLength="35">
                            </asp:TextBox></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblCountry" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMCountry%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtCountry" runat="server" Width="200px" MaxLength="35">
                            </asp:TextBox></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblPostalCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMPostalCode%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtPostalCode" runat="server" Width="200px" MaxLength="10">
                            </asp:TextBox></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="75" Width="200px" />
                            <asp:RegularExpressionValidator ID="revalEmail" runat="server" ControlToValidate="txtEmail"
                                Display="None" ErrorMessage="<%$ Resources:Resource, revalCMEmail%>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                            </asp:RegularExpressionValidator></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblSecEmail" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMSecEmail%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtSecEmail" runat="server" MaxLength="75" Width="200px" />
                            <asp:RegularExpressionValidator ID="revalSecEmail" runat="server" ControlToValidate="txtSecEmail"
                                Display="None" ErrorMessage="<%$ Resources:Resource, revalCMSecEmail%>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                            </asp:RegularExpressionValidator></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblSex" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMSex%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:RadioButtonList ID="rblstSex" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="<%$ Resources:Resource, liCMFeMale%>" Value="F" Selected="True">
                                </asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, liCMMale%>" Value="M"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                </ul>
            </td>
            <td valign="top">
                <ul class="form">
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblLangPref" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMLangPref%>">
                            </asp:Label></div>
                        <div class="input">
                            <asp:RadioButtonList ID="rblstLangPref" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="<%$ Resources:Resource, liCMFrench%>" Value="fr" Selected="True">
                                </asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, liCMEnglish%>" Value="en"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblTitleFr" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMTitleFr%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtTitleFr" runat="server" MaxLength="50" Width="200px" /></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblTitleEn" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMTitleEN%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtTitleEn" runat="server" MaxLength="50" Width="200px" /></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtPhone" runat="server" Width="200px" MaxLength="20" /></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblPhoneExt" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMPhoneExt%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtPhoneExt" runat="server" Width="200px" MaxLength="10" /></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblHomePhone" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMHomePhone%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtHomePhone" runat="server" Width="200px" MaxLength="20" /></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblCellPhone" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMCellPhone%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtCellPhone" runat="server" Width="200px" MaxLength="20" /></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblPagerNo" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMPagerNo%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtPagerNo" runat="server" Width="200px" MaxLength="20" /></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtFax" runat="server" MaxLength="20" Width="200px" /></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblContactSpecialization" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMContactSpecialization%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:DropDownList ID="dlContactSpecialization" CssClass="innerText" runat="server"
                                Width="200px">
                                <asp:ListItem Text="<%$ Resources:Resource, liCMSelectContactSpecialization%>" Value="0" />
                                <asp:ListItem Text="<%$ Resources:Resource, liCMInstitution%>" Value="1" />
                                <asp:ListItem Text="<%$ Resources:Resource, liCMIndividual%>" Value="2" />
                                <asp:ListItem Text="<%$ Resources:Resource, liCMEvent%>" Value="3" />
                            </asp:DropDownList>
                            <span class="style1">*</span>
                            <asp:CustomValidator ID="custvalContactSpecialization" SetFocusOnError="true" runat="server"
                                ErrorMessage="<%$ Resources:Resource, custvalCMContactSpecialization %>" ClientValidationFunction="funSelectContactSpecialization"
                                Display="None">
                            </asp:CustomValidator>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblDOB" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMDOB%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtDOB" CssClass="" runat="server" MaxLength="10" Width="150px" /></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblContactPrimary" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMContactPrimary%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:RadioButtonList ID="rblstContactPrimary" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="<%$ Resources:Resource, lblCMYes%>" Value="1">
                                </asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, lblCMNo%>" Value="0" Selected="True">
                                </asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblNotes" CssClass="lblBold" Visible="true" Text="<%$ Resources:Resource, lblCMNotes%>"
                                runat="server" /></div>
                        <div class="input">
                            <asp:TextBox ID="txtNotes" runat="server" TextMode="multiLine" Visible="true" Rows="3"
                                MaxLength="250" Width="80%" /></div>
                        <div class="clearBoth">
                        </div>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphLeft" Runat="Server">
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


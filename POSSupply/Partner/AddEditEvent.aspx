<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="AddEditEvent.aspx.vb" Inherits="Partner_AddEditEvent" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="Resources.Resource" %>

<%@ Register src="../Controls/CommonSearch/CRMSearch.ascx" tagname="CRMSearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>
    <uc1:CRMSearch ID="CRMSearch1" runat="server" />
</asp:Content>

<asp:Content ID="cntAddEditEvent" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal></h2>
                </td>
                <td style="text-align:right;">
                   
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <div align="right" id="divCrtMod" runat="server" style="vertical-align: top;">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td width="70%" class="tdAlignLeft">
                        <asp:UpdatePanel ID="upnlMsg" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                                <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td width="30%" align="right"  valign="top">
                        <table style="border: 0;"  class="table">
                            <tr>
                                <td height="5">
                                </td>
                            </tr>
                            <tr class="trheight">
                                <td class="tdAlignLeft">
                                    <asp:Label ID="lblCreated" CssClass="lblBold" runat="server" /><br />
                                    <asp:Label ID="lblCreatedDate" CssClass="lblSimple" runat="server" /><br />
                                    <asp:Label ID="lblCreatedBy" CssClass="lblSimple" runat="server" />
                                </td>
                                <td width="10px">
                                    &nbsp;
                                </td>
                                <td class="tdAlignLeft">
                                    <asp:Label ID="lblModified" CssClass="lblBold" runat="server" /><br />
                                    <asp:Label ID="lblModifiedDate" CssClass="lblSimple" runat="server" /><br />
                                    <asp:Label ID="lblModifiedBy" CssClass="lblSimple" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
        </div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td align="left"  width="50%" valign="top">
                    <table width="100%" style="border: 0;"  class="table">
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td width="28%" class="tdAlign">
                                <asp:Label ID="lblShortName" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMShortName%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft" width="72%">
                                <asp:TextBox ID="txtShortName" runat="server" MaxLength="10" Width="150px" />
                                <span class="style1">*</span>
                                <asp:RequiredFieldValidator ID="reqvalShortName" runat="server" ControlToValidate="txtShortName"
                                    ErrorMessage="<%$ Resources:Resource, reqvalCMShortName%>" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblEventDate" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMEventDate%>"
                                    runat="server" /><br />
                                &nbsp;
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtEventDateTime" runat="server" MaxLength="10" Width="150px" />
                                <asp:ImageButton ID="imgEventDate" CausesValidation="false" runat="server" ToolTip="Click to show calendar"
                                    ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                                <span class="style1">*</span><br />
                                <%=lblCMEventDateFormat %>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" TargetControlID="txtEventDateTime"
                                    runat="server" PopupButtonID="imgEventDate" Format="MM/dd/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:RequiredFieldValidator ID="reqvalEventDateTime" runat="server" ControlToValidate="txtEventDateTime"
                                    ErrorMessage="<%$ Resources:Resource, reqvalCMEventDate%>" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                                <div style="display: none;">
                                    <asp:TextBox ID="txtDate" Enabled="true" runat="server" Width="0px" MaxLength="19">
                                    </asp:TextBox>
                                </div>
                                <asp:CustomValidator ID="custvalEventDate" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<%$ Resources:Resource, msgCMPlzEntEvtDateGTCurDate%>" Enabled="false"
                                    ClientValidationFunction="funCheckEventDate" Display="None">
                                </asp:CustomValidator>
                                <asp:CompareValidator ID="cmpvalEventDate" EnableClientScript="true" SetFocusOnError="true"
                                    ControlToValidate="txtEventDateTime" ControlToCompare="txtDate" Operator="GreaterThanEqual"
                                    Type="Date" Display="None" Enabled="false" ErrorMessage="<%$ Resources:Resource, msgCMPlzEntEvtDateGTCurDate %>"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblEventTime" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMEventTime%>"
                                    runat="server" /><br />
                                &nbsp;
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtEventTime" runat="server" Width="150px" MaxLength="8" />
                                <span class="style1">*</span><br />
                                <%=lblCMEventTimeFormat %>
                                <ajaxToolkit:AutoCompleteExtender ID="acExEventTime" runat="server" TargetControlID="txtEventTime"
                                    MinimumPrefixLength="1" EnableCaching="true" ServicePath="~/FilterService.asmx"
                                    ServiceMethod="GetTime">
                                </ajaxToolkit:AutoCompleteExtender>
                                <asp:RequiredFieldValidator ID="reqvalEventTime" runat="server" ControlToValidate="txtEventTime"
                                    ErrorMessage="<%$ Resources:Resource, reqvalCMEventTime%>" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="left"  width="50%" valign="top">
                    <table width="100%" style="border: 0;"  class="table">
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblLongName" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMLongName%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtLongName" runat="server" Width="200px" MaxLength="250">
                                </asp:TextBox><span class="style1"> *</span>
                                <asp:RequiredFieldValidator ID="reqvalLongName" runat="server" ControlToValidate="txtLongName"
                                    ErrorMessage="<%$ Resources:Resource, reqvalCMLongName%>" SetFocusOnError="true"
                                    Display="None"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblEventType" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMEventType%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:DropDownList ID="dlEventType" CssClass="innerText" runat="server" Width="155px">
                                </asp:DropDownList>
                                <span class="style1">*</span>
                                <asp:CustomValidator ID="custvalPartnerType" SetFocusOnError="true" runat="server"
                                    ErrorMessage="<%$ Resources:Resource, custvalCMEventType %>" ClientValidationFunction="funSelectEventType"
                                    Display="None">
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblNoOfVisitors" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMNoOfVisitors%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtNoOfVisitors" runat="server" MaxLength="6" Width="100px" />
                                <asp:CompareValidator ID="comvalNVisitor" EnableClientScript="true" SetFocusOnError="true"
                                    ControlToValidate="txtNoOfVisitors" Operator="DataTypeCheck" Type="Integer" Display="None"
                                    ErrorMessage="<%$ Resources:Resource, msgCMPlzEntNumericValueInNoOfVisitors %>"
                                    runat="server" />
                                <asp:CompareValidator ID="compvalNVisiGT0" EnableClientScript="true" SetFocusOnError="true"
                                    ControlToValidate="txtNoOfVisitors" Operator="GreaterThanEqual" ValueToCompare="0"
                                    ErrorMessage="<%$ Resources:Resource, msgCMPlzEntNoOfVisitorsGTE0 %>" Display="None"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="trheight">
                <td class="tdAlignLeft" colspan="2">
                    &nbsp;
                    <asp:Label ID="lblNotes" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMNotes%>"
                        runat="server" /><br />
                    <asp:TextBox ID="txtNotes" runat="server" TextMode="multiLine" Rows="5" MaxLength="250"
                        Width="100%" />
                </td>
            </tr>
            <tr class="trheight">
                <td class="tdAlignLeft" colspan="2" valign="top">
                    <table style="border: 0;" class="table">
                        <tr>
                            <td height="5" colspan="3">
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlignLeft" valign="top" width="33%">
                                <asp:Label ID="lblAverageDemographic" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMAverageDemographic%>" />
                                <br />
                                <asp:DataList ID="dlstVisitors" runat="server" DataKeyField="EventVistorID" CellSpacing="0"
                                    Width="100%" DataSourceID="sqldsVisitor" GridLines="None" RepeatColumns="1" RepeatDirection="Vertical">
                                    <HeaderTemplate>
                                        <table cellpadding="2" cellspacing="0" border="0" >
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="trheight">
                                            <td class="tdAlign">
                                                <asp:Label ID="lblAD" runat="server" Text='<%# Eval("VisitorDesc") %>' CssClass="lblBold">
                                                </asp:Label>
                                                <asp:HiddenField ID="hdnAD" runat="server" Value='<%# Eval("EventVistorID") %>' />
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td class="tdAlignLeft">
                                                <asp:TextBox ID="txtAD" runat="server" MaxLength="3" Text="" Width="80px">
                                                </asp:TextBox>
                                                <asp:CompareValidator ID="comvalAD" EnableClientScript="true" SetFocusOnError="true"
                                                    ControlToValidate="txtAD" Operator="DataTypeCheck" Type="Integer" Display="None"
                                                    ErrorMessage="<%$ Resources:Resource, msgCMPlzEntNumericValueInAverageDemographic %>"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                    <ItemStyle VerticalAlign="Top" Width="80px" />
                                </asp:DataList>
                                <asp:SqlDataSource ID="sqldsVisitor" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                            </td>
                            <td class="tdAlignLeft" width="33%" valign="top">
                                <asp:Label ID="lblEducationLevel" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMEducationLevel%>" />
                                <br />
                                <asp:DataList ID="dlstEduLvl" runat="server" DataKeyField="PartnerEvenEduLvlID" CellSpacing="0"
                                    Width="100%" DataSourceID="sqldsEduLvl" GridLines="None" RepeatColumns="1" RepeatDirection="Vertical">
                                    <HeaderTemplate>
                                        <table cellpadding="2" cellspacing="0" border="0" >
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="trheight">
                                            <td class="tdAlign">
                                                <asp:Label ID="lblEduLvl" runat="server" Text='<%# Eval("EventEduLvlDesc") %>' CssClass="lblBold">
                                                </asp:Label>
                                                <asp:HiddenField ID="hdnEduLvl" runat="server" Value='<%# Eval("PartnerEvenEduLvlID") %>' />
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td class="tdAlignLeft">
                                                <asp:TextBox ID="txtEduLvl" runat="server" MaxLength="3" Text="" Width="80px">
                                                </asp:TextBox>
                                                <asp:CompareValidator ID="comvalEduLvl" EnableClientScript="true" SetFocusOnError="true"
                                                    ControlToValidate="txtEduLvl" Operator="DataTypeCheck" Type="Integer" Display="None"
                                                    ErrorMessage="<%$ Resources:Resource, msgCMPlzEntNumericValueInEducationLevel %>"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                    <ItemStyle VerticalAlign="Top" Width="80px" />
                                </asp:DataList>
                                <asp:SqlDataSource ID="sqldsEduLvl" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                            </td>
                            <td class="tdAlignLeft" width="33%" valign="top">
                                <asp:Label ID="lblSector" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMSector%>" />
                                <br />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="txtSector" runat="server" TextMode="multiLine" Rows="5" MaxLength="250"
                                    Width="300" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="trheight">
                <td align="right" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;
                    padding-right: 5px;">
                    <table width="100%">
                        <tr>
                            <td height="20" align="center" width="25%">
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="cmdSave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                        style="min-width: 120px; text-align: center;">
                                        <%=Resources.Resource.cmdCssSave%></span></a></div>
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="cmdBack" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                        <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssBack%></span></a></div>
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="cmdReset" runat="server" visible="false" causesvalidation="false" class="ovalbutton"
                                        href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssCancel%></span></a></div>
                            </td>
                            <td height="20" align="center" width="15%">
                            </td>
                        </tr>
                    </table>
                    <%--<asp:Button runat="server" ID="cmdSave" CssClass="imgSave"  />&nbsp;&nbsp;
            <asp:Button runat="server" ID="cmdReset" CssClass="imgReset" 
            CausesValidation="false" Visible="false" />
            <asp:Button runat="server" ID="cmdBack" CssClass="imgBack" 
            CausesValidation="false"  />--%>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsPartner" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
    <br />
    <br />
    <script language="javascript" type="text/javascript">

        function funSelectEventType(source, arguments) {
            if (window.document.getElementById('<%=dlEventType.ClientID%>').value != "0") {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }
        function funCheckEventDate(source, args) {
            var curdate = new Date();

            if (window.document.getElementById('<%=txtEventDateTime.ClientID%>').value > curdate) {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }
    </script>
</asp:Content>

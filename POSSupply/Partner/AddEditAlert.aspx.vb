Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Partial Class Partner_AddEditAlert
    Inherits BasePage
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If

        lblError.Text = ""
        lblTitle.Text = CMTitleAlerts
        If Not IsPostBack Then
            txtDate.Text = Now.ToString("MM/dd/yyyy HH:mm:ss")
            If Request.QueryString("ContactID") <> "" Then
                divCrtMod.Style("display") = "block"
                subFillRecord()
            Else
                divCrtMod.Style("display") = "block"
            End If
        End If
    End Sub
    'Populate Object
    Private Sub subSetData(ByRef objA As clsAlerts)
        If Convert.ToString(Request.QueryString("CommID")) <> "" Then
            objA.AlertPartnerContactID = Convert.ToString(Request.QueryString("ContactID"))
            objA.AlertComID = Convert.ToString(Request.QueryString("CommID"))
            objA.AlertDateTime = DateTime.ParseExact(txtAlertDateTime.Text & " " & txtAlertTime.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd HH:mm:ss")
            objA.AlertUserID = Session("UserID")
            objA.AlertNote = txtNotes.Text
        End If
    End Sub
    'Populate Controls
    Public Sub subFillRecord()
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            
        End If
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    Protected Sub cmdBack_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.ServerClick
        Dim strPartnerID As String = ""
        strPartnerID = Convert.ToString(Request.QueryString("PartnerID"))
        Dim strContactID As String = ""
        strContactID = Convert.ToString(Request.QueryString("ContactID"))
        Response.Redirect("AddEditContact.aspx?PartnerID=" & strPartnerID & "&ContactID=" & strContactID & "&mp=" & Request.QueryString("mp"))
    End Sub
    ' Insert/Update Alert
    Protected Sub cmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.ServerClick
        If Not Page.IsValid Then
            Exit Sub
        End If
        lblError.Text = ""

        If txtAlertTime.Text = "" Then
            lblError.Text = reqvalCMAlertTime
            Exit Sub
        Else
            Dim strDT As String = ""
            strDT = txtAlertDateTime.Text & " " & txtAlertTime.Text
            If IsDate(strDT) = False Then
                lblError.Text = reqvalCMValidAlertTime
                Exit Sub
            End If
        End If
        If txtAlertDateTime.Text <> "" Then
            If IsDate(txtAlertDateTime.Text) = True Then
                Dim strDT As String = ""
                strDT = txtAlertDateTime.Text & " " & txtAlertTime.Text
                If CDate(strDT) < Now Then
                    lblError.Text = msgCMPlzEntAlertDateGTCurDate
                    Exit Sub
                End If
            End If
        End If

        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            Dim objAlert As New clsAlerts
            subSetData(objAlert)
            Dim strPartnerID As String = ""
            Dim strCommID As String = ""
            Dim strContactID As String = ""

            strPartnerID = Convert.ToString(Request.QueryString("PartnerID"))
            strCommID = Convert.ToString(Request.QueryString("CommID"))
            strContactID = Convert.ToString(Request.QueryString("ContactID"))
            If Convert.ToString(Request.QueryString("CommID")) <> "" Then

                'Insert Section
                If objAlert.insertAlerts = True Then 'Insert Partner



                    Session.Add("Msg", CMAlertAddedSuccessfully)
                    Response.Redirect("AddEditContact.aspx?PartnerID=" & strPartnerID & "&ContactID=" & strContactID & "&mp=" & Request.QueryString("mp"))
                    Exit Sub
                End If
            End If
        End If
    End Sub
    Protected Sub cmdReset_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReset.ServerClick
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            subFillRecord()
            Exit Sub
        End If

        txtAlertDateTime.Text = ""
    End Sub
End Class


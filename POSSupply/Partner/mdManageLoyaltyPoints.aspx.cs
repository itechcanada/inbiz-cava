﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using Trirand.Web.UI.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Common_mdManageLoyaltyPoints : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //txtPoints.Focus();
            //trProductsGrid.Visible = !(this.PointOption == "add");           
        }
    }

    public int PartnerID
    {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["partnerid"], out id);
            return id;
        }
    }

    public string PointOption {
        get {
            return BusinessUtility.GetString(Request.QueryString["pointOption"]);
        }
    }

    protected void grdLoyalty_CellBinding(object sender, JQGridCellBindEventArgs e)
    {

    }

    protected void grdLoyalty_DataRequesting(object sender, JQGridDataRequestEventArgs e)
    {
        grdLoyalty.DataSource = new LoyalProductLevelHeader().GetLoyalProducts(null, this.PointOption);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            double points = BusinessUtility.GetDouble(txtPoints.Text);
            if (this.PartnerID <= 0)
            {
                return;
            }
            if ((this.PointOption == "redeem" && points > 0) || (this.PointOption == "add" && points < 0))
            {                
                points = -1.00D * points;
            }           
            
            int productID = BusinessUtility.GetInt(hdnProductIDtoRedeem.Value);
            Partners part = new Partners();
            part.PopulateObject(dbHelp, this.PartnerID);

            LoyalPartnerHistory lph = new LoyalPartnerHistory();

            double partAvailPoints = lph.GetPartnerLoyaltyPoints(dbHelp, this.PartnerID);
            //Validate if guest have not enough points to redeem
            if (this.PointOption == "redeem" && partAvailPoints < (-1.00D * points))
            {
                MessageState.SetGlobalMessage(MessageType.Failure, "Guest have not enough points!");
                return;
            }
            
            lph.LoyalCategoryID = part.PartnerLoyalCategoryID;
            lph.LoyalPartnerID = part.PartnerID;
            lph.Notes = txtNotes.Text;
            lph.PointAuto = false;
            lph.Points = points;
            lph.PointsAddedBy = CurrentUser.UserID;
            lph.PointsAddedOn = DateTime.Now;
            lph.ProductID = productID;

            lph.Insert(dbHelp);
            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.lblPointsAddeddSuccessfully);

            Globals.RegisterReloadParentScript(this);
        }
        catch(Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }       
    }
}
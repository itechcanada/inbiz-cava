<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="Notes.aspx.vb" Inherits="Partner_Notes" %>

<%@ Register src="../Controls/CommonSearch/CRMSearch.ascx" tagname="CRMSearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();
        $('#divMainContainerTitle1').corner();
        $('#divMainContainerTitle2').corner();
        $('#divMainContainerTitle3').corner();
        $('#divMainContainerTitle4').corner();          
    </script>
    <uc1:CRMSearch ID="CRMSearch1" runat="server" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
   <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblHeading"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                   
                </td>
                <td style="width:150px;">
                     <div class="buttonwrapper">
                        <a id="cmdBack" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                            <span class="ovalbutton" style="min-width: 90px; text-align: center;">
                                <%=Resources.Resource.cmdCssBack%></span></a></div>
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent">
         <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td colspan="2" align="left" style="padding: 5px">
                    <asp:Label CssClass="lblBold" Visible="false" runat="server" ID="lblPartnerHeading"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right" width="12%" style="padding: 5px">
                    <asp:Label ID="lblCustomerName1" Text="<%$ Resources:Resource, lblSOCustomerName %>"
                        CssClass="lblBold" runat="server" />
                </td>
                <td align="left" style="padding: 5px">
                    <asp:Label ID="lblCustomerName" CssClass="lblBold" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right" style="padding: 5px" width="12%">
                    <asp:Label ID="Label1" Text="<%$ Resources:Resource, lblCMNotes %>" CssClass="lblBold"
                        runat="server" />
                </td>
                <td align="left" style="padding: 5px">
                    <asp:Label ID="lblNotes" CssClass="lblBold" runat="server" />
                </td>
            </tr>
             <tr>
                <td height="5" colspan="2" align="Center" style="padding: 5px">
                    <asp:Label runat="server" ForeColor="green" Font-Bold="True" ID="lblPartner"></asp:Label>
                </td>
            </tr>
        </table>

        <asp:GridView ID="grdPartner" runat="server" AllowSorting="True" Visible="false"
            DataSourceID="sqldsPartner" AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric"
            CellPadding="0" GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
            CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="PartnerID"
            Width="100%">
            <Columns>
                <asp:BoundField DataField="PartnerID" HeaderText="<%$ Resources:Resource, grdCMcustomerID %>"
                    ReadOnly="True" SortExpression="PartnerID">
                    <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="PartnerNote" HeaderText="<%$ Resources:Resource, prnNote %>"
                    ReadOnly="True" SortExpression="PartnerNote">
                    <ItemStyle Width="550px" Wrap="true" HorizontalAlign="Left" />
                </asp:BoundField>
            </Columns>
            <FooterStyle CssClass="grid_footer" />
            <RowStyle CssClass="grid_rowstyle" />
            <PagerStyle CssClass="grid_footer" />
            <HeaderStyle CssClass="grid_header" Height="26px" />
            <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
            <PagerSettings PageButtonCount="20" />
        </asp:GridView>
        <asp:SqlDataSource ID="sqldsPartner" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            ProviderName="System.Data.Odbc"></asp:SqlDataSource>
    </div>

    <div id="divMainContainerTitle1" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitleLogDetails"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                   
                </td>                
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <div style="text-align:center;">
            <asp:Label runat="server" ForeColor="green" Font-Bold="True" ID="lblLeads"></asp:Label>
        </div>
        <asp:GridView ID="grdLeadsLog" runat="server" AllowSorting="True" DataSourceID="sqldsLeadsLog"
            AllowPaging="True" PageSize="10" PagerSettings-Mode="Numeric" CellPadding="0"
            GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
            CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="LeadsLogID"
            Width="100%">
            <Columns>
                <asp:BoundField DataField="AssignLeadsID" HeaderText="<%$ Resources:Resource, grdCMPartnerID %>"
                    ReadOnly="True" SortExpression="AssignLeadsID">
                    <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="<%$ Resources:Resource, prnNote %>" HeaderStyle-ForeColor="#ffffff"
                    HeaderStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblLog" ForeColor="black" runat="server" Text='<%# Eval("LeadsLogText") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="left" Width="550px" />
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="grid_footer" />
            <RowStyle CssClass="grid_rowstyle" />
            <PagerStyle CssClass="grid_footer" />
            <HeaderStyle CssClass="grid_header" Height="26px" />
            <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
            <PagerSettings PageButtonCount="20" />
        </asp:GridView>
        <asp:SqlDataSource ID="sqldsLeadsLog" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            ProviderName="System.Data.Odbc"></asp:SqlDataSource>
    </div>

    <div id="divMainContainerTitle2" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblSo"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                   
                </td>                
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <div style="text-align: center;">
            <asp:Label runat="server" ForeColor="green" Font-Bold="True" ID="lblOrder"></asp:Label>
        </div>
        <asp:GridView ID="grdOrder" runat="server" AllowSorting="True" DataSourceID="sqldsOrder"
            AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0"
            GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
            CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="ordID" Width="100%">
            <Columns>
                <asp:BoundField DataField="ordID" HeaderText="<%$ Resources:Resource, grdSOOrderNo %>"
                    ReadOnly="True" SortExpression="ordID">
                    <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="ordComment" HeaderText="<%$ Resources:Resource, prnNote %>"
                    ReadOnly="True" SortExpression="ordComment">
                    <ItemStyle Width="550px" Wrap="true" HorizontalAlign="Left" />
                </asp:BoundField>
            </Columns>
            <FooterStyle CssClass="grid_footer" />
            <RowStyle CssClass="grid_rowstyle" />
            <PagerStyle CssClass="grid_footer" />
            <HeaderStyle CssClass="grid_header" Height="26px" />
            <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
            <PagerSettings PageButtonCount="20" />
        </asp:GridView>
        <asp:SqlDataSource ID="sqldsOrder" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            ProviderName="System.Data.Odbc"></asp:SqlDataSource>
    </div>

    <div id="divMainContainerTitle3" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblAR"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                   
                </td>                
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <div style="text-align: center;">
            <asp:Label runat="server" ForeColor="green" Font-Bold="True" ID="lblARMsg"></asp:Label>
        </div>
        <asp:GridView ID="grdAR" runat="server" AllowSorting="True" DataSourceID="sqldsAR"
            AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0"
            GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
            CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="ARInvoiceNo"
            Width="100%">
            <Columns>
                <asp:BoundField DataField="InvRefNo" HeaderText="<%$ Resources:Resource, grdINInvoiceNo %>"
                    ReadOnly="True" SortExpression="InvRefNo" HeaderStyle-ForeColor="#ffffff">
                    <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="ARNote" HeaderText="<%$ Resources:Resource, prnNote %>"
                    ReadOnly="True" SortExpression="ARNote" HeaderStyle-ForeColor="#ffffff">
                    <ItemStyle Width="550px" Wrap="true" HorizontalAlign="Left" />
                </asp:BoundField>
            </Columns>
            <FooterStyle CssClass="grid_footer" />
            <RowStyle CssClass="grid_rowstyle" />
            <PagerStyle CssClass="grid_footer" />
            <HeaderStyle CssClass="grid_header" Height="26px" />
            <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
            <PagerSettings PageButtonCount="20" />
        </asp:GridView>
        <asp:SqlDataSource ID="sqldsAR" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            ProviderName="System.Data.Odbc"></asp:SqlDataSource>
    </div>

     <div id="divMainContainerTitle4" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblCol"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                   
                </td>                
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <div style="text-align: center;">
            <asp:Label runat="server" ForeColor="green" Font-Bold="True" ID="lblColMsg"></asp:Label>
        </div>
        <asp:GridView ID="grdCol" runat="server" AllowSorting="True" DataSourceID="sqldsCol"
            AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0"
            GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
            CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="CollectionLogID"
            Width="100%">
            <Columns>
                <asp:BoundField DataField="InvRefNo" HeaderText="<%$ Resources:Resource, grdINInvoiceNo %>"
                    ReadOnly="True" SortExpression="InvRefNo">
                    <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="<%$ Resources:Resource, grdCAFollowupLogText %>" HeaderStyle-ForeColor="#ffffff"
                    HeaderStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblLog" ForeColor="black" runat="server" Text='<%# Eval("ColLogText") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="left" Width="550px" />
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="grid_footer" />
            <RowStyle CssClass="grid_rowstyle" />
            <PagerStyle CssClass="grid_footer" />
            <HeaderStyle CssClass="grid_header" Height="26px" />
            <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
            <PagerSettings PageButtonCount="20" />
        </asp:GridView>
        <asp:SqlDataSource ID="sqldsCol" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            ProviderName="System.Data.Odbc"></asp:SqlDataSource>

        <div class="buttonwrapper" style="text-align:center;">
            <a id="cmdBack2" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                <span class="ovalbutton" style="min-width: 90px; text-align: center;">
                    <%=Resources.Resource.cmdCssBack%></span></a></div>
    </div>
    <br />
</asp:Content>

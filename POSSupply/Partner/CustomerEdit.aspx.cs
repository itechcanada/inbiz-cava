﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections.Specialized;

using iTECH.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Partner_CustomerEdit :BasePage
{
    private Control _contentControl;

    protected void Page_Load(object sender, EventArgs e)
    {        
        //Security Check
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.CRM_MANAGER))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        if (!IsPostBack) {
            if (this.PartnerType == StatusCustomerTypes.END_CLINET && this.GuestType == StatusGuestType.Staff)
            {
                btnAdd.Text = Resources.Resource.lblAddStaffMember;
            }
            else if (this.PartnerType == StatusCustomerTypes.END_CLINET && (this.GuestType == StatusGuestType.Guest || this.GuestType == StatusGuestType.SpecialGuest || this.GuestType == StatusGuestType.CourseParticipants))
            {
                btnAdd.Text = Resources.Resource.lblAddGuestMember;
            }

            if (this.PartnerID > 0)
            {
                spUploadImage.Visible = true;
                mdUploadImage.Url = string.Format("~/Common/mdSysFiles.aspx?refid={0}&tblref={1}", this.PartnerID, TableRef.CUSTOMER);
            }

            
        }

        BuildMenu();
        LoadUserControl(this.ActiveSectionIndex);    
    }

    private void BuildMenu()
    {
        if (!IsPostBack)
        {
            blstNav.Items.Add(new ListItem(Resources.Resource.lblCustomerDetails, QueryString.GetModifiedUrl("section", ((int)CRMSectionKey.CustomerDetails).ToString())));
            if (this.PartnerID > 0)
            {
                //if (this.PartnerType == StatusCustomerTypes.DISTRIBUTER)
                //{
                //    blstNav.Items.Add(new ListItem(Resources.Resource.lblCustomerContacts, QueryString.GetModifiedUrl("section", ((int)CRMSectionKey.CustomerDetails).ToString())));
                //}

                //if (CurrentUser.IsInRole(RoleID.SALES_MANAGER) || CurrentUser.IsInRole(RoleID.QUOTATION_APPROVER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
                //{
                //    if (this.GuestType == StatusGuestType.CourseParticipants || this.GuestType == StatusGuestType.Guest ||
                //        this.GuestType == StatusGuestType.SpecialGuest || this.GuestType == StatusGuestType.Staff)
                //    {
                //        blstNav.Items.Add(new ListItem(Resources.Resource.lblSOHistory, QueryString.GetModifiedUrl("section", ((int)CRMSectionKey.CustomerSoHistory).ToString())));
                //    }
                //    else
                //    {
                //        blstNav.Items.Add(new ListItem(Resources.Resource.btnCreateOrder, ResolveUrl("~/Sales/SalesEdit.aspx") + string.Format("?custID={0}&custName={1}&ComID={2}&whsID={3}", this.PartnerID, "", CurrentUser.DefaultCompanyID, CurrentUser.UserDefaultWarehouse)));
                //        blstNav.Items.Add(new ListItem(Resources.Resource.lblSOHistory, string.Format("~/Sales/ViewSalesOrder.aspx?PartnerID={0}&returnUrl={1}", this.PartnerID, Server.UrlEncode(Request.RawUrl))));
                //    }
                //}
                blstNav.Items.Add(new ListItem(Resources.Resource.btnCreateOrder, ResolveUrl("~/Sales/SalesEdit.aspx") + string.Format("?custID={0}&custName={1}&ComID={2}&whsID={3}", this.PartnerID, "", CurrentUser.DefaultCompanyID, CurrentUser.UserDefaultWarehouse)));
                if (this.PartnerType != StatusCustomerTypes.DISTRIBUTER)
                {
                    blstNav.Items.Add(new ListItem(Resources.Resource.lblCustomerContacts, QueryString.GetModifiedUrl("section", ((int)CRMSectionKey.CustomerContacts).ToString())));
                }
                blstNav.Items.Add(new ListItem(Resources.Resource.lblSOHistory, string.Format("~/Sales/ViewSalesOrder.aspx?PartnerID={0}&returnUrl={1}", this.PartnerID, Server.UrlEncode(Request.RawUrl))));
                blstNav.Items.Add(new ListItem(Resources.Resource.lblInvoiceHistory, string.Format("~/Invoice/ViewInvoice.aspx?PartnerID={0}&returnUrl={1}", this.PartnerID, Server.UrlEncode(Request.RawUrl))));
                blstNav.Items.Add(new ListItem(Resources.Resource.lblARHistory, string.Format("~/AccountsReceivable/ViewAccountsReceivable.aspx?CustName={0}&PartnerID={1}&returnUrl={2}", "", this.PartnerID, Server.UrlEncode(Request.RawUrl))));
                blstNav.Items.Add(new ListItem(Resources.Resource.lblLoyaltyProgram, QueryString.GetModifiedUrl("section", ((int)CRMSectionKey.CustomerLoyaltyProgram).ToString())));
                blstNav.Items.Add(new ListItem(Resources.Resource.lblCustomerNotes, QueryString.GetModifiedUrl("section", ((int)CRMSectionKey.CustomerNotes).ToString())));
                blstNav.Items.Add(new ListItem(Resources.Resource.lblCustomerActivities, QueryString.GetModifiedUrl("section", ((int)CRMSectionKey.CustomerActivity).ToString())));
                ListItem liUpFile = new ListItem("Upload File(s)", "javascript:void(0);");
                liUpFile.Attributes.Add("class", "upload_file_pop");
                blstNav.Items.Add(liUpFile);
                //blstNav.Items.Add(new ListItem(Resources.Resource.lblCustomerNotes, string.Format("~/partner/Notes.aspx?PartnerID={0}&returnUrl={1}", this.PartnerID, Server.UrlEncode(Request.RawUrl))));
                //blstNav.Items.Add(new ListItem(Resources.Resource.lblCustomerActivities, string.Format("~/Partner/AddEditFollowups.aspx?PartnerId={0}&returnUrl={1}", this.PartnerID, Server.UrlEncode(Request.RawUrl))));
                blstNav.Items.Add(new ListItem(Resources.Resource.lblViewCustomer, string.Format("~/Partner/ViewCustomer.aspx?pType={0}&gType={1}", this.PartnerType, (int)this.GuestType)));
            }
        }
        foreach (ListItem item in blstNav.Items)
        {
            if (!item.Value.Equals("javascript:void(0);"))
            {
                Uri u = new Uri(Request.Url.GetLeftPart(UriPartial.Authority) + ResolveUrl(item.Value));
                NameValueCollection nvc = HttpUtility.ParseQueryString(u.Query);
                if (nvc.AllKeys.Contains("section") && BusinessUtility.GetInt(nvc["section"]) == (int)this.ActiveSectionIndex)
                {
                    item.Attributes["class"] = "open";
                    break;
                }
            }            
        }
    }

    private void LoadUserControl(CRMSectionKey index)
    {
        try
        {
            string ctrlPath = this.CRMSections[index];

            _contentControl = Page.LoadControl(ctrlPath);
            phControl.Controls.Clear();
            phControl.Controls.Add(_contentControl);
            _contentControl.ID = "ctlContent";
            ICustomer ctrl = (ICustomer)_contentControl;

            if (ctrl.PartnerID > 0)
            {
                ltTitle.Text = Resources.Resource.lblEditCustomer;
            }
            else
            {
                ltTitle.Text = Resources.Resource.lblAddCustomer;
            }
            ctrl.Initialize();
        }
        catch(Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError + ": " +  ex.Message);
        }
    }           

    public int PartnerID
    {
        get
        {
            int custid = 0;
            int.TryParse(Request.QueryString["custid"], out custid);
            return custid;
        }
    }

    private StatusGuestType GuestType
    {
        get
        {
            return QueryStringHelper.GetGuestType("gType");
        }
    }

    private string PartnerType
    {
        get
        {
            return QueryStringHelper.GetPartnerType("pType");
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("CustomerEdit.aspx?pType={0}&gType={1}", this.PartnerType, (int)this.GuestType));
    }

    private CRMSectionKey ActiveSectionIndex
    {
        get
        {
            int section = 0;
            if (int.TryParse(Request.QueryString["section"], out section))
            {
                if (Enum.IsDefined(typeof(CRMSectionKey), section))
                {
                    return (CRMSectionKey)Enum.ToObject(typeof(CRMSectionKey), section);
                }
                else
                {
                    return CRMSectionKey.CustomerDetails;
                }
            }
            return CRMSectionKey.CustomerDetails;
        }
    }    

    private Dictionary<CRMSectionKey, string> CRMSections
    {
        get
        {
            Dictionary<CRMSectionKey, string> sec = new Dictionary<CRMSectionKey, string>();
            sec[CRMSectionKey.CustomerDetails] = "~/Partner/UserControls/CustomerDetails.ascx"; //HostSetting.GetSectionUrl(AppSectionID.UcCustomerEdit);
            sec[CRMSectionKey.CustomerActivity] = "~/Partner/UserControls/CustomerActivities.ascx";
            //sec[CRMSectionKey.CustomerContacts] = "~/Partner/UserControls/CustomerDetails.ascx";
            sec[CRMSectionKey.CustomerContacts] = "~/Partner/UserControls/CustomerContacts.ascx";
            sec[CRMSectionKey.CustomerNotes] = "~/Partner/UserControls/CustomerNotes.ascx";
            sec[CRMSectionKey.CustomerSoHistory] = "~/Partner/UserControls/SOHistory.ascx";
            sec[CRMSectionKey.CustomerLoyaltyProgram] = "~/Partner/UserControls/CustomerLoyaltyProgram.ascx";
            return sec;
        }
    }


    #region Web Methods
    [System.Web.Services.WebMethod]
    public static PartnerContacts GetPartnerContacts(int id)
    {
        PartnerContacts objPtnrContact = new PartnerContacts();
        objPtnrContact.GetContactDetail(id);
        return objPtnrContact;
    }
    #endregion
}
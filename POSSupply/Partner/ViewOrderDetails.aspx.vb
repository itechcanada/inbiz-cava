Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Partial Class Partner_ViewOrderDetails
    Inherits BasePage
    Protected TaxString As String
    Private strSOID As String = ""
    Private objSO As New clsOrders
    Private objSOItems As New clsOrderItems
    Private objSOIP As New clsOrderItemProcess
    Private objCust As New clsExtUser
    Private objOrderType As New clsOrderType
    Private clsStatus As New clsSysStatus

    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        lblTitle.Text = lblSOViewSalesOrderDetails '"View Sales Order Details"
        If Convert.ToString(Request.QueryString("SOID")) <> "" And (Request.QueryString("gnIn") = "Y") Then
            lblMsg.Text = Session.Item("Msg")
        ElseIf Convert.ToString(Request.QueryString("SOID")) <> "" Then
            strSOID = Convert.ToString(Request.QueryString("SOID"))
        End If
        If Not Page.IsPostBack Then
            clsStatus.subGetStatus(dlStatus, "SO", "SOSts")
            If Convert.ToString(Request.QueryString("Msg")) = "1" Then
                lblMsg.Text = msgSOApprovedSuccessfully
            End If
            cmdGnratInvoice.Visible = False
            subPopulateCurrency()

            If Convert.ToString(Request.QueryString("SOID")) <> "" Then
                strSOID = Convert.ToString(Request.QueryString("SOID"))
                subFillGrid()
                objSO.OrdID = strSOID
                objSO.getOrders()
                subPopulateWarehouse(objSO.ordCompanyID)

                subPopulateTerms()

                hdnSaleAgentID.Value = objSO.OrdSalesRepID
                hdnOrderID.Value = objSO.ordercommission
                lblSODate.Text = IIf(objSO.OrdCreatedOn <> "", CDate(objSO.OrdCreatedOn).ToString("MMM-dd yyyy"), "--")
                lblSOID.Text = objSO.OrdID
                objCust.CustID = objSO.OrdCustID
                objCust.CustType = objSO.OrdCustType
                objCust.getCustomerInfoByType()
                txtTerms.Text = objSO.ordShippingTerms
                txtNetTerms.Text = objSO.ordNetTerms
                dlCurrencyCode.SelectedValue = objSO.OrdCurrencyCode
                txtBillToAddress.Text = funPopulateAddress(objCust.CustType, "B", objCust.CustID).Replace("<br>", vbCrLf)
                txtShpToAddress.Text = funPopulateAddress(objCust.CustType, "S", objCust.CustID).Replace("<br>", vbCrLf)
                lblAddress.Text = funPopulateAddress(objCust.CustType, "S", objCust.CustID)
                lblCustomerName.Text = objCust.CustName
                hdnCustId.Value = objCust.CustID
                hdnCustTyp.Value = objCust.CustType
                'lblTerms.Text = objCust.CustNetTerms
                lblFax.Text = objCust.CustFax
                Dim c As New clsCurrencies
                lblAmount.Text = objSO.OrdCurrencyCode & " " & CDbl(objSO.Amount) + CDbl(objSOIP.getProcessCostForOrderNo())
                txtNotes.Text = objSO.OrdComment
                Dim intI As Int16 = 0
                Dim IntJ As Int16 = 0
                If objSO.OrdStatus = "A" Then
                    IntJ = 1
                ElseIf objSO.OrdStatus = "P" Then
                    IntJ = 2
                ElseIf objSO.OrdStatus = "H" Then
                    IntJ = 3
                ElseIf objSO.OrdStatus = "S" Then
                    IntJ = 4
                ElseIf objSO.OrdStatus = "D" Or objSO.OrdStatus = "C" Then
                    IntJ = 5
                End If

                Dim objUser As New clsUser
                If objSO.OrdStatus = "N" Then
                    lblApprovedBy.Text = "--"
                Else
                    lblApprovedBy.Text = objUser.funUserName(objSO.OrdVerifiedBy)
                End If
                lblCreatedBy.Text = objUser.funUserName(objSO.OrdSalesRepID)

                Dim objPrtCon As New clsPartnerContacts
                objPrtCon.subPartnerContactsInfo(objCust.CustID)
                lblContactName.Text = objPrtCon.ContactFirstName
                lblContactPhoneNo.Text = objPrtCon.ContactPhone


                While intI < IntJ
                    dlStatus.Items.RemoveAt(1)
                    intI += 1
                End While
                dlStatus.SelectedValue = objSO.OrdStatus
                lblStatus.Text = dlStatus.SelectedItem.Text
                hdnPreStatus.Value = objSO.OrdStatus
                rblstShipBlankPref.SelectedValue = objSO.OrdShpBlankPref
                rblstISWeb.SelectedValue = objSO.OrdSaleWeb
                If objSO.OrdShpDate <> "" Then
                    txtShippingDate.Text = CDate(objSO.OrdShpDate).ToString("MM/dd/yyyy")
                End If
                txtShpTrackNo.Text = objSO.OrdShpTrackNo
                txtShpCost.Text = objSO.OrdShpCost
                txtShpCode.Text = objSO.OrdShpCode
                dlCurrencyCode.SelectedValue = objSO.OrdCurrencyCode
                txtExRate.Text = objSO.OrdCurrencyExRate
                txtCustPO.Text = objSO.OrdCustPO
                If objSO.QutExpDate <> "" Then
                    txtQutExpDate.Text = CDate(objSO.QutExpDate).ToString("MM/dd/yyyy")
                End If

                dlShpWarehouse.SelectedValue = objSO.OrdShpWhsCode
                subCalcTax()
                Dim strDocTyp As String = funStatusDocType()
                Me.imgPrint.Attributes.Add("OnClick", "return openPDF('ShowPdf.aspx?SOID=" & Request.QueryString("SOID") & "&DocTyp=" & strDocTyp & "')")
                Me.imgMail.Attributes.Add("OnClick", "return popUp('" & divMail.ClientID & "');")
                Me.imgFax.Attributes.Add("OnClick", "return popUp('" & divFax.ClientID & "');")
                Me.cmdFax.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & ConfirmFaxNumber & " ')")
                txtFaxTo.Text = lblFax.Text
                txtAttention.Text = lblCustomerName.Text
                txtFaxSubject.Text = strDocTyp & "-" & lblSOID.Text
                txtMailTo.Text = objCust.CustEmail
                txtMailSubject.Text = strDocTyp & "-" & lblSOID.Text
                'For Invoice Generation

                If (objSO.OrdStatus = "A") Then
                    subApproved()
                End If
                If (objSO.OrdStatus <> "N") And (Session("UserModules").ToString.Contains("INV") = True Or Session("UserModules").ToString.Contains("ADM") = True) Then
                    cmdGnratInvoice.Visible = True
                End If
                If (Session("UserModules").ToString.Contains("QOA") = False And Session("UserModules").ToString.Contains("ADM") = False) Then
                    dlStatus.Enabled = False
                End If
            End If
        Else
            If Convert.ToString(Request.QueryString("SOID")) <> "" And strSOID <> "" Then
                subCalcTax()
            End If
        End If
    End Sub
    'Populate Address
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)
        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + "<br>"
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + "<br>"
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + "<br>"
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + "<br>"
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + "<br>"
            Else
                strAddress += objAdr.AddressState + "<br>"
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + "<br>"
            Else
                strAddress += objAdr.AddressCountry + "<br>"
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += lblPostalCode & " " + objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function
    ' Sql Data Source Event Track
    Protected Sub sqldsOrdItems_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsOrdItems.Selected
        subCalcAmount()
    End Sub
    'Populate Grid
    Public Sub subFillGrid()
        sqldsOrdItems.SelectCommand = objSOItems.funFillGrid(strSOID)  'fill Order Items
        objSOIP.OrdID = strSOID
        objSOItems.OrdID = strSOID
        sqldsAddProcLst.SelectCommand = objSOIP.funFillGridForProcess 'fill Order Items Process
    End Sub
    'On Row Data Bound
    Protected Sub grdOrdItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdOrdItems.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & SOItemConfirmDelete & " ')")
            e.Row.Cells(9).Text = String.Format("{0:F}", CDbl(e.Row.Cells(9).Text))
            Dim objCur As New clsCurrencies
            e.Row.Cells(11).Text = String.Format("{0:F}", CDbl(e.Row.Cells(11).Text)) & objCur.funBaseCurConversion(objSO.OrdCurrencyExRate, e.Row.Cells(11).Text)
            Dim txtDisType As TextBox = CType(e.Row.Cells(14).FindControl("txtDisType"), TextBox)
            If txtDisType.Text = "P" Then
                e.Row.Cells(7).Text = e.Row.Cells(7).Text & "%"
            End If
        End If
    End Sub
    'Deleting
    Protected Sub grdOrdItems_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdOrdItems.RowDeleting
        Dim strItemID As String = grdOrdItems.DataKeys(e.RowIndex).Value.ToString()
        objSOItems.OrderItemID = strItemID
        sqldsOrdItems.DeleteCommand = objSOItems.funDeleteSOItems()
        lblMsg.Text = msgSOOrderItemDeletedSuccessfully
        subFillGrid()
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    'Populate Object
    Private Sub subSetData(ByRef objS As clsOrders)
        If dlStatus.SelectedValue = "" Then
            objS.OrdType = objS.OrdType
        ElseIf dlStatus.SelectedValue = "N" Then
            objS.OrdType = "QT"
        ElseIf dlStatus.SelectedValue = "A" Then
            objS.OrdType = "SO"
        ElseIf dlStatus.SelectedValue = "P" Then
            objS.OrdType = "WO"
        Else
            objS.OrdType = objS.OrdType
        End If
        objS.OrdCustType = objS.OrdCustType
        objS.OrdCustID = objS.OrdCustID
        objS.OrdCreatedOn = CDate(objS.OrdCreatedOn).ToString("yyyy-MM-dd hh:mm:ss")
        objS.OrdCreatedFromIP = objS.OrdCreatedFromIP
        objS.OrdStatus = dlStatus.SelectedValue
        If dlStatus.SelectedValue = "V" Then
            objS.OrdVerified = 1
            objS.OrdVerifiedBy = Session("UserID").ToString
        Else
            objS.OrdVerified = objS.OrdVerified
            objS.OrdVerifiedBy = objS.OrdVerifiedBy
        End If
        objS.OrdSalesRepID = objS.OrdSalesRepID
        objS.OrdSaleWeb = rblstISWeb.SelectedValue
        objS.OrdShpBlankPref = rblstShipBlankPref.SelectedValue
        objS.OrdLastUpdateBy = Session("UserID").ToString
        objS.OrdLastUpdatedOn = Now.ToString("yyyy-MM-dd hh:mm:ss")
        objS.OrdShpCode = txtShpCode.Text
        objS.OrdShpWhsCode = dlShpWarehouse.SelectedValue
        objS.OrdShpCost = txtShpCost.Text
        'objS.ordCompanyID = Request.QueryString("ComID")
        If txtShippingDate.Text <> "" Then
            If IsDate(DateTime.ParseExact(txtShippingDate.Text, "MM/dd/yyyy", Nothing)) = True Then
                objS.OrdShpDate = DateTime.ParseExact(txtShippingDate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
            Else
                If IsDate(objS.OrdShpDate) = True Then
                    objS.OrdShpDate = CDate(objS.OrdShpDate).ToString("yyyy-MM-dd hh:mm:ss")
                Else
                    objS.OrdShpDate = ""
                End If
            End If
        Else
            If IsDate(objS.OrdShpDate) = True Then
                objS.OrdShpDate = CDate(objS.OrdShpDate).ToString("yyyy-MM-dd hh:mm:ss")
            Else
                objS.OrdShpDate = ""
            End If
        End If
        objS.OrdShpTrackNo = txtShpTrackNo.Text
        objS.OrdCurrencyCode = dlCurrencyCode.SelectedValue
        objS.OrdCurrencyExRate = txtExRate.Text
        objS.OrdComment = txtNotes.Text
        objS.OrdCustPO = txtCustPO.Text

        If txtQutExpDate.Text <> "" Then
            If IsDate(DateTime.ParseExact(txtQutExpDate.Text, "MM/dd/yyyy", Nothing)) = True Then
                objS.QutExpDate = DateTime.ParseExact(txtQutExpDate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
            Else
                If IsDate(objS.QutExpDate) = True Then
                    objS.QutExpDate = CDate(objS.QutExpDate).ToString("yyyy-MM-dd hh:mm:ss")
                Else
                    objS.QutExpDate = ""
                End If
            End If
        Else
            If IsDate(objS.QutExpDate) = True Then
                objS.QutExpDate = CDate(objS.QutExpDate).ToString("yyyy-MM-dd hh:mm:ss")
            Else
                objS.QutExpDate = ""
            End If
        End If
    End Sub
    'On Submit
    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        If Not Page.IsValid Then
            'Exit Sub
        End If
        If dlStatus.SelectedValue = "P" And txtCustPO.Text = "" Then
            'lblMsg.Text = msgSOPlzEntCustomerPO '"Please enter Customer PO."
            'txtCustPO.Focus()
            'Exit Sub
        ElseIf dlStatus.SelectedItem.Text = "" Then
            lblMsg.Text = msgSOPlzSelStatus '"Please select Order Status."
            dlStatus.Focus()
            Exit Sub
        End If
        objSO.OrdID = Request.QueryString("SOID")
        objSO.getOrdersInfo()
        subSetData(objSO)
        If dlStatus.SelectedValue = "P" Then
            If objSO.updateOrders() = True Then
                'Update quantity QuoteRsv and SORsv
                If hdnPreStatus.Value <> "S" Then
                    If hdnPreStatus.Value = "I" Or hdnPreStatus.Value = "P" Or hdnPreStatus.Value = "C" Or hdnPreStatus.Value = "Z" Then
                        'Skip the update and invoice process
                    Else
                        objSO.updateProductQuantityOnProcess()
                        'Invoice Process Start
                        'funInvoiceProcess()
                        'Invoice Process End
                        Session.Add("Msg", OrderUpdatedSuccessfully)
                    End If
                Else
                    Session.Add("Msg", OrderUpdatedSuccessfully)
                End If
            Else
                Session.Add("Msg", OrderNotUpdatedSuccessfully)
            End If
            'For Shipped status
            'Devendra 14_07_09 start
        ElseIf dlStatus.SelectedValue = "S" And hdnPreStatus.Value <> "S" Then
            If (hdnPreStatus.Value = "P" Or hdnPreStatus.Value = "Z") Then
                objSO.OrdStatus = "D"
                If objSO.updateOrders() = True Then
                    'Update quantity SORsv and OnHand
                    objSO.updateProductQuantityOnShipping()
                    'Invoice Process Start
                    funInvoiceProcess()
                    'Invoice Process End
                    Session.Add("Msg", OrderShippedSuccessfully)
                Else
                    Session.Add("Msg", OrderNotShippedSuccessfully)
                End If
            End If
            'Devendra 14_07_09 End
        Else
            If objSO.updateOrders() = True Then
                Session.Add("Msg", OrderUpdatedSuccessfully)
            Else
                Session.Add("Msg", OrderNotUpdatedSuccessfully)
            End If
        End If
        Response.Redirect("ViewSalesOrder.aspx")
    End Sub
    ' On Back
    Protected Sub cmdBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReset.Click
        Response.Redirect("ViewSalesOrder.aspx")
    End Sub

    'Get Address Listing
    Protected Function funAddressListing(ByVal strID As String)
        Dim strData As String = ""
        If strID <> "" Then
            Dim objWhs As New clsWarehouses
            objWhs.WarehouseCode = strID
            objWhs.getWarehouseInfo()
            strData = "<table align='left' width=100% cellspacing='0' cellpadding='0' border='0' style='border:0px'><tr><td align='left'>"
            If objWhs.WarehouseCode <> "" Then
                'strData += "<tr><td align='left'>" + objWhs.WarehouseCode + "</td></tr>"
            End If
            If objWhs.WarehouseDescription <> "" Then
                strData += objWhs.WarehouseDescription + "<br/>"
            End If
            If objWhs.WarehouseAddressLine1 <> "" Then
                strData += objWhs.WarehouseAddressLine1 + "<br/>"
            End If
            If objWhs.WarehouseAddressLine2 <> "" Then
                strData += objWhs.WarehouseAddressLine2 + "<br/>"
            End If
            If objWhs.WarehouseCity <> "" Then
                strData += objWhs.WarehouseCity + "<br/>"
            End If
            If objWhs.WarehouseState <> "" Then
                strData += objWhs.WarehouseState + "<br/>"
            End If
            If objWhs.WarehouseCountry <> "" Then
                strData += objWhs.WarehouseCountry + "<br/>"
            End If
            If objWhs.WarehousePostalCode <> "" Then
                strData += objWhs.WarehousePostalCode + "<br/>"
            End If
            strData += "</td></tr></table>"
            objWhs = Nothing
        End If
        Return strData
    End Function
    'Populate Currency
    Private Sub subPopulateCurrency()
        Dim objCur As New clsCurrencies
        objCur.PopulateCurrency(dlCurrencyCode)
        objCur = Nothing
    End Sub
    'Populate Warehouse
    Private Sub subPopulateWarehouse(ByVal strCompanyID As String)
        Dim objWhs As New clsWarehouses
        objWhs.PopulateWarehouse(dlShpWarehouse, strCompanyID)
        objWhs = Nothing
    End Sub
    'Populate Terms
    Private Sub subPopulateTerms()
        Dim objComp As New clsCompanyInfo
        objComp.CompanyID = objSO.ordCompanyID
        objComp.getCompanyInfo()
        'txtTerms.Text = objComp.CompanyShpToTerms
        lblSOPOComNameTitle.Text = objComp.CompanyName
        Dim objWhs As New clsWarehouses
        objWhs.WarehouseCompanyID = objComp.CompanyID
        lblSOPOwhsName.Text = objWhs.funGetWhsName
        objComp = Nothing
        objWhs = Nothing
    End Sub
    'On Row Data Bound
    Protected Sub grdAddProcLst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAddProcLst.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & SOProcessItemConfirmDelete & " ')")
            e.Row.Cells(3).Text = String.Format("{0:F}", CDbl(e.Row.Cells(3).Text))
            e.Row.Cells(4).Text = String.Format("{0:F}", CDbl(e.Row.Cells(4).Text))
            e.Row.Cells(5).Text = String.Format("{0:F}", CDbl(e.Row.Cells(5).Text))
            e.Row.Cells(8).Text = String.Format("{0:F}", CDbl(e.Row.Cells(8).Text))
        End If
    End Sub
    ' On Row Deleting
    Protected Sub grdAddProcLst_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdAddProcLst.RowDeleting
        Dim strItemID As String = grdAddProcLst.DataKeys(e.RowIndex).Value.ToString()
        objSOIP.OrderItemProcID = strItemID
        sqldsAddProcLst.DeleteCommand = objSOIP.funDeleteSOItemProcess()
        lblMsg.Text = msgSOProcessDeletedSuccessfully '"Process Deleted Successfully"
        subFillGrid()
    End Sub
    ' Sql Data Source Event Track
    Protected Sub sqldsAddProcLst_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsAddProcLst.Selected
        subCalcAmount()
    End Sub
    ' Calcualte Amount
    Private Sub subCalcAmount()
        objSOIP.OrdID = strSOID
        objSOItems.OrdID = strSOID
        If grdAddProcLst.Rows.Count > 0 Then
            lblProcessList.Visible = True
        Else
            lblProcessList.Visible = False
        End If
    End Sub
    ' Calculate Tax
    Private Sub subCalcTax()
        lblSubTotal.Text = "0"
        Dim drSOItems As Data.Odbc.OdbcDataReader
        Dim objPrn As New clsPrint
        Dim objCur As New clsCurrencies
        drSOItems = objSOItems.GetDataReader(objSOItems.funFillGrid(strSOID))
        Dim dblSubTotal As Double = 0
        Dim dblTax As Double = 0
        Dim dblProcessTotal As Double = 0
        Dim dblTotal As Double = 0
        Dim dblDiscountApplied As Double = 0
        Dim dblAmount As Double = 0
        Dim strTaxArray(,) As String
        While drSOItems.Read
            Dim strTax As ArrayList
            Dim strTaxItem As String
            strTax = objPrn.funCalcTax(drSOItems("ordProductID"), drSOItems("ordShpWhsCode"), drSOItems("amount"), "", drSOItems("ordCustID"), drSOItems("ordCustType"), drSOItems("ordProductTaxGrp").ToString)
            Dim intI As Integer = 0
            While strTax.Count - 1 >= intI
                strTaxItem = strTax.Item(intI).ToString
                strTaxArray = objPrn.funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                intI += 1
            End While
            dblSubTotal = CDbl(dblSubTotal) + CDbl(drSOItems("amount"))
            lblSubTotal.Text = String.Format("{0:F}", dblSubTotal) 'dblSubTotal.ToString
        End While
        Dim i As Integer = 0
        Dim j As Integer = 0

        objSOItems.OrdID = strSOID
        dblProcessTotal = CDbl(objSOIP.getProcessCostForOrderNo())
        strTaxArray = objPrn.funTotalProcessTax(dblProcessTotal, dlShpWarehouse.SelectedValue, strTaxArray)

        If Not strTaxArray Is Nothing Then
            j = strTaxArray.Length / 2
        End If
        TaxString = ""
        While i < j
            TaxString &= "<tr><td align=left><b>" & strTaxArray(0, i) & ":</b></td><td align=right><b>" & dlCurrencyCode.SelectedValue & " " & String.Format("{0:F}", strTaxArray(1, i)) & "</b></td></tr>"
            dblTax += strTaxArray(1, i)
            i += 1
        End While

        dblTotal = dblTotal + dblSubTotal + dblProcessTotal + dblTax
        lblAmount.Text = String.Format("{0:F}", dblTotal)
        Dim objUser As New clsUser
        Dim strCommission As String = objOrderType.funOderCommission(hdnOrderID.Value, hdnSaleAgentID.Value, lblSubTotal.Text)
        If strCommission = 0 Then
            trCommission.Visible = False
        End If
        lblCommAgentName.Text = objUser.funUserName(hdnSaleAgentID.Value)
        lblCommission.Text = dlCurrencyCode.SelectedValue & " " & strCommission
        lblSubTotal.Text = dlCurrencyCode.SelectedValue & " " & lblSubTotal.Text
        lblAmount.Text = dlCurrencyCode.SelectedValue & " " & lblAmount.Text & " " & objCur.funBaseCurConversion(txtExRate.Text, dblTotal)
        drSOItems.Close()
        objSOItems.CloseDatabaseConnection()
        objPrn = Nothing
    End Sub
    ' On Send Mail Clicks
    Protected Sub cmdMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdMail.Click
        If Request.QueryString("SOID") <> "" Then
            Dim strDocTyp As String = funStatusDocType()
            Dim objPrn As New clsPrint(Request.QueryString("SOID"))
            Dim strPath As String = ""
            strPath = objPrn.PrintRequestDetail(Request.QueryString("SOID"), strDocTyp)
            strPath = strPath.Substring(strPath.LastIndexOf("\") + 1)
            strPath = Request.PhysicalApplicationPath & "PDF\" & strPath
            Dim strMailFrom As String = Session("EmailID").ToString
            Dim strMailTo As String = txtMailTo.Text
            Dim strSub As String = txtMailSubject.Text
            Dim strMsg As String = ""
            strMsg = txtMailMessage.Text
            Dim obj As New clsCommon
            If obj.SendMail(strSub, strMsg, strMailTo, strMailFrom, strPath) = True Then
                Dim objDoc As New clsDocuments
                objDoc.SysDocType = strDocTyp
                objDoc.SysDocDistType = "E"
                objDoc.SysDocDistDateTime = Now.ToString("yyyy-MM-dd hh:mm:ss")
                objDoc.SysDocPath = strPath
                objDoc.SysDocEmailTo = strMailTo
                objDoc.SysDocFaxToPhone = ""
                objDoc.SysDocFaxToAttention = ""
                objDoc.SysDocBody = strMsg
                objDoc.SysDocSubject = strSub
                objDoc.insertDocuments()
                objDoc = Nothing
            End If
            obj = Nothing
        End If
    End Sub
    ' On Send Fax Clicks
    Protected Sub cmdFax_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFax.Click
        If Request.QueryString("SOID") <> "" Then
            Dim strDocTyp As String = funStatusDocType()
            Dim objPrn As New clsPrint(Request.QueryString("SOID"))
            Dim strPath As String = ""
            strPath = objPrn.PrintRequestDetail(Request.QueryString("SOID"), strDocTyp)
            strPath = strPath.Substring(strPath.LastIndexOf("\") + 1)
            strPath = Request.PhysicalApplicationPath & "PDF\" & strPath
            Dim strFaxInfo() As String
            Dim objCI As New clsCompanyInfo
            objSO.OrdID = Request.QueryString("SOID")
            objSO.getOrders()
            objCI.CompanyID = objSO.ordCompanyID
            strFaxInfo = objCI.funGetFirstCompanyFaxInfo().Split("~")
            Dim strMailFrom As String = Session("EmailID").ToString
            Dim strMailTo As String = ""
            Dim strSub As String = txtFaxSubject.Text
            Dim strMsg As String = ""
            If txtFaxTo.Text.Contains(strFaxInfo(0)) = False Then
                strMailTo = objCI.funFaxNumbers(txtFaxTo.Text) & "@" & strFaxInfo(0)
            Else
                strMailTo = txtFaxTo.Text
            End If
            If strFaxInfo(1).Contains("username:") Then
                strMsg += strFaxInfo(1) & vbCrLf
            End If
            If strFaxInfo(2).Contains("code:") Then
                strMsg += strFaxInfo(2) & vbCrLf
            End If
            strMsg += "Attention: " & txtAttention.Text & vbCrLf
            strMsg += "Message: "
            strMsg += txtFaxMessage.Text
            Dim obj As New clsCommon
            If obj.SendFax(strSub, strMsg, strMailTo, strMailFrom, strPath) = True Then
                Dim objDoc As New clsDocuments
                objDoc.SysDocType = strDocTyp
                objDoc.SysDocDistType = "F"
                objDoc.SysDocDistDateTime = Now.ToString("yyyy-MM-dd hh:mm:ss")
                objDoc.SysDocPath = strPath
                objDoc.SysDocEmailTo = ""
                objDoc.SysDocFaxToPhone = strMailTo
                objDoc.SysDocFaxToAttention = txtAttention.Text
                objDoc.SysDocBody = txtFaxMessage.Text
                objDoc.SysDocSubject = strSub
                objDoc.insertDocuments()
                objDoc = Nothing
            End If
            obj = Nothing
        End If
    End Sub
    'Return Status Document Type
    Private Function funStatusDocType() As String
        Dim sStatus As String = ""
        Select Case hdnPreStatus.Value
            Case "N", "A", "V", "H"
                sStatus = "QO"
            Case "P", "I", "S", "C", "Z"
                sStatus = "SO"
            Case Else
                sStatus = "QO"
        End Select
        Return sStatus
    End Function
    'Invoice Process 
    Private Function funInvoiceProcess() As Boolean
        Dim bFlag As Boolean = False
        If Request.QueryString("SOID") <> "" Then
            objSO.OrdID = Request.QueryString("SOID")
            objSO.getOrdersInfo()
            Dim objInv As New clsInvoices
            subSetInvoiceData(objInv, objSO)
            If objInv.funCheckInvoiceForOrderNo = False Then
                If objInv.insertInvoices() = True Then
                    Dim strInvID As String
                    strInvID = objInv.funGetInvoiceID()
                    objInv.funUpdateInvRefNo(objInv.InvRefType, objInv.InvCompanyID, strInvID)
                    Dim objInvItem As New clsInvoiceItems
                    'Populate Invoice Item
                    bFlag = objInvItem.funPopulateInvoiceItem(Request.QueryString("SOID"), strInvID)
                    objInvItem = Nothing
                    'Populate Invoice Item Process
                    Dim objINIP As New clsInvItemProcess
                    bFlag = objINIP.funPopulateInvoiceItemProcess(Request.QueryString("SOID"), strInvID)
                End If
            End If
        End If
        Return bFlag
    End Function
    'Populate Invoice Object
    Private Sub subSetInvoiceData(ByRef objIN As clsInvoices, ByRef objS As clsOrders)
        objIN.InvCustType = objS.OrdCustType
        objIN.InvCustID = objS.OrdCustID
        objIN.InvCreatedOn = Now.ToString("yyyy-MM-dd hh:mm:ss")
        objIN.InvStatus = "S"
        objIN.InvComment = objS.OrdComment
        objIN.InvLastUpdateBy = Session("UserID").ToString
        objIN.InvLastUpdatedOn = Now.ToString("yyyy-MM-dd hh:mm:ss")
        objIN.InvShpCost = objS.OrdShpCost
        objIN.InvCurrencyCode = objS.OrdCurrencyCode
        objIN.InvCurrencyExRate = objS.OrdCurrencyExRate
        objIN.InvCustPO = objS.OrdCustPO
        objIN.InvShpWhsCode = objS.OrdShpWhsCode
        objIN.InvForOrderNo = objS.OrdID
        objIN.InvCompanyID = objS.ordCompanyID
        objIN.InvRefType = "IV"
        objIN.InvCommission = objS.ordercommission
        objIN.InvRefNo = 0
    End Sub

    Protected Sub cmdGnratInvoice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGnratInvoice.Click
        ' Response.Redirect("~/Invoice/GenerateInvoice.aspx?gnIn=Y&SOID=" + Request.QueryString("SOID"))
        If Not Page.IsValid Then
            'Exit Sub
        End If
        If (dlStatus.SelectedValue = "P" Or dlStatus.SelectedValue = "A") And txtCustPO.Text = "" Then
            'lblMsg.Text = msgSOPlzEntCustomerPO '"Please enter Customer PO."
            'txtCustPO.Focus()
            'Exit Sub
        ElseIf dlStatus.SelectedItem.Text = "" Then
            lblMsg.Text = msgSOPlzSelStatus '"Please select Order Status."
            dlStatus.Focus()
            Exit Sub
        End If
        objSO.OrdID = Request.QueryString("SOID")
        objSO.getOrdersInfo()
        If objSO.OrdStatus = "A" Then
            objSO.OrdStatus = "P"
            objSO.OrdCustPO = txtCustPO.Text
            'objSO.funApproveOrder()
            subSetData(objSO)
            If objSO.updateOrders() = True Then
                'Update quantity SORsv and OnHand
                objSO.updateProductQuantityOnProcess()
                'Invoice Process Start
                funInvoiceProcess()
                'Invoice Process End
                objSO.OrdStatus = "P"
                objSO.funUpdateOrder()
                Session.Add("Msg", OrderShippedSuccessfully)
                Dim objInv As New clsInvoices
                objInv.InvForOrderNo = objSO.OrdID
                Response.Redirect("~/invoice/ViewInvoiceDetails.aspx?SOID=" & objInv.funGetInvoiceID & "&ComID=" & objSO.ordCompanyID)
            Else
                Session.Add("Msg", OrderNotShippedSuccessfully)
            End If
        End If
        If objSO.OrdStatus = "P" Then
            If funInvoiceProcess() = True Then
                objSO.OrdStatus = "P"
                objSO.funUpdateOrder()
                Session.Add("Msg", InvoiceGeneratedSuccessfully)
                Dim objInv As New clsInvoices
                objInv.InvForOrderNo = objSO.OrdID
                Response.Redirect("~/invoice/ViewInvoiceDetails.aspx?SOID=" & objInv.funGetInvoiceID & "&ComID=" & objSO.ordCompanyID)
            Else
                lblMsg.Text = InvoiceProcessError 'InvoiceCouldNotCreatedSuccessfully
            End If
        Else
            lblMsg.Text = OrderStatusIsNotInProcess '"Order status is not ""In Process""."
        End If
    End Sub
    'View Disabled when Approved
    Private Sub subApproved()
        txtTerms.ReadOnly = True
        txtNetTerms.ReadOnly = True
        dlCurrencyCode.Enabled = False
        txtBillToAddress.ReadOnly = True
        txtShpToAddress.ReadOnly = True
        txtNotes.ReadOnly = True
        rblstShipBlankPref.Enabled = False
        rblstISWeb.Enabled = False
        txtShpTrackNo.ReadOnly = True
        txtShpCost.ReadOnly = True
        txtShpCode.ReadOnly = True
        dlCurrencyCode.Enabled = False
        txtExRate.ReadOnly = True
        dlShpWarehouse.Enabled = False
    End Sub

    Protected Sub cmdCpyOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        objSO.OrdID = Request.QueryString("SOID")
        objSO.getOrdersInfo()
        Response.Redirect("Create.aspx?SOID=" & Request.QueryString("SOID") & "&custID=" & objSO.OrdCustID & "&custName=" & objSO.OrdCustName & "&ComID=" & objSO.ordCompanyID & "&whsID=" & objSO.OrdShpWhsCode)
    End Sub
    'Devendra 17_07_09 start
    Protected Sub subInventoryUpdation()
        If objSO.OrdStatus = "P" Then
            If objSO.updateOrders() = True Then
                'Update quantity QuoteRsv and SORsv
                If hdnPreStatus.Value <> "S" Then
                    If hdnPreStatus.Value = "I" Or hdnPreStatus.Value = "P" Or hdnPreStatus.Value = "C" Or hdnPreStatus.Value = "Z" Then
                        'Skip the update and invoice process
                    Else
                        objSO.updateProductQuantityOnProcess()
                        Session.Add("Msg", OrderUpdatedSuccessfully)
                    End If
                Else
                    Session.Add("Msg", OrderUpdatedSuccessfully)
                End If
            Else
                Session.Add("Msg", OrderNotUpdatedSuccessfully)
            End If
            objSO.OrdStatus = "S"
            hdnPreStatus.Value = "P"
            'For Shipped status
        End If
        If objSO.OrdStatus = "S" And hdnPreStatus.Value <> "S" Then
            If (hdnPreStatus.Value = "P" Or hdnPreStatus.Value = "Z") Then
                objSO.OrdStatus = "D"

            End If
        Else
            If objSO.updateOrders() = True Then
                Session.Add("Msg", OrderUpdatedSuccessfully)
            Else
                Session.Add("Msg", OrderNotUpdatedSuccessfully)
            End If
        End If
    End Sub 'Devendra 17_07_09 End
    Protected Sub cmdEditSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdEditSO.Click
        Response.Redirect("~/Sales/Approval.aspx?SOID=" & Request.QueryString("SOID"))
    End Sub
    Protected Sub cmdBack_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.ServerClick
        If Request.QueryString("Col") <> "" Then
            Response.Redirect("~/Partner/ViewSalesOrder.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&Col=" & Request.QueryString("Col"))
        Else
            Response.Redirect("~/Partner/ViewSalesOrder.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&SOID=" & Request.QueryString("SOID"))
        End If
    End Sub

    Protected Sub CmdcssBack2_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdcssBack2.ServerClick
        If Request.QueryString("Col") <> "" Then
            Response.Redirect("~/Partner/ViewSalesOrder.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&Col=" & Request.QueryString("Col"))
        Else
            Response.Redirect("~/Partner/ViewSalesOrder.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&SOID=" & Request.QueryString("SOID"))
        End If
    End Sub
End Class

<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false" CodeFile="ViewContact.aspx.vb" Inherits="Partner_ViewContact" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="Resources.Resource" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divSearchTitle').corner();
        $('#<%=pnlSearchForm.ClientID %>').corner();
        $('#divMainContainerTitle').corner();        
    </script>
   
    <div id="divSearchTitle" class="divSectionTitle">
        <h2>
            <%=Resources.Resource.lblSearchOptions%>
        </h2>
    </div>
    <asp:Panel runat="server" ID="pnlSearchForm" CssClass="divSectionContent" DefaultButton="imgSearch">
        <asp:UpdatePanel ID="upnlSearch" runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlSearchName" DefaultButton="imgSearch">
                    <table>
                        <%--<tr>
                            <td>
                                <asp:Label CssClass="lblBold" runat="server" ID="lblSearch" Text="<%$ Resources:Resource, lblCMContactSearchCmst%>"></asp:Label>
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lblStatus %>"></asp:Label>
                    <br class="brMargin5"/>
                                <asp:DropDownList ID="ddlStatus" runat="server" Width="175px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"></asp:Label>
                    <br class="brMargin5"/>
                                <asp:DropDownList ID="dlSearch" runat="server" Width="175px">
                                </asp:DropDownList><br />
                                <asp:CustomValidator ID="custvalSearch" SetFocusOnError="true" runat="server" ErrorMessage="<%$ Resources:Resource, custvalCMSelectSearchBy %>"
                                    ClientValidationFunction="funSelectSearch" Display="None" Enabled="true">
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword%>"></asp:Label>
                    <br class="brMargin5"/>
                                <asp:TextBox runat="server" Width="170px" ID="txtSearch"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--<asp:Button ID="imgSearch" runat="server" Text="<%$Resources:Resource, lblSearch %>" />--%>
                                <asp:ImageButton ID="imgSearch" runat="server" AlternateText="Search" ImageUrl="../images/search-btn.gif" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="cntViewContact" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <h2>
                        <asp:Literal ID="lblTitle" runat="server"></asp:Literal>
                    </h2>
                </td>
                <td align="right">
                    <asp:UpdateProgress runat="server" ID="upMaster" DisplayAfter="10">
                        <ProgressTemplate>
                            <img src="../Images/wait22trans.gif" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
    </div>
    <div id="divMainContent" class="divMainContent" onkeypress="return disableEnterKey(event)">
        <table style="width: 100%;">
            <tr>
                <td>
                    <asp:UpdatePanel ID="upnlMsg" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                            <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td width="20%" align="right">
                    <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Search" ImageUrl="../images/xls.png" />
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="2" height="350" valign="top">
                    <asp:UpdatePanel ID="upnlGrid" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="grdContact" runat="server" AllowSorting="True" DataSourceID="sqldsContact"
                                AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0"
                                GridLines="none" AutoGenerateColumns="False" ShowHeader="true" Style="border-collapse: separate;"
                                CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="ContactID"
                                Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="PartnerLongName" HeaderText="<%$ Resources:Resource, grdCMCustomerName %>"
                                        ReadOnly="True" SortExpression="PartnerLongName">
                                        <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ContactName" HeaderText="<%$ Resources:Resource, grdCMContactName %>"
                                        ReadOnly="True" SortExpression="ContactName" Visible="false">
                                        <ItemStyle Width="300px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdCMContactName %>" HeaderStyle-HorizontalAlign="Left"
                                        HeaderStyle-ForeColor="white">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lnkContactName" Font-Bold="true" CommandName="contact"
                                                CommandArgument='<%# Eval("ContactID") & ":" & Eval("ContactPartnerID") %>' CausesValidation="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" Width="300px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ContactEmail" HeaderText="<%$ Resources:Resource, grdCMContactEmail %>"
                                        ReadOnly="True" SortExpression="ContactEmail">
                                        <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ContactDOB" Visible="false" HeaderText="<%$ Resources:Resource, grdCMContactDOB %>"
                                        ReadOnly="True" DataFormatString="{0: MM/dd/yyyy}" SortExpression="ContactDOB">
                                        <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ContactCellPhone" HeaderText="<%$ Resources:Resource, grdCMContactCellPhone %>"
                                        ReadOnly="True" SortExpression="ContactCellPhone">
                                        <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:ImageField HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="center"
                                        HeaderText="<%$ Resources:Resource, grdCMContactStatus %>" DataImageUrlField="ContactActive"
                                        DataImageUrlFormatString="~/images/{0}">
                                        <ItemStyle HorizontalAlign="center" Width="0px" VerticalAlign="Middle" />
                                    </asp:ImageField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdCMEdit %>" HeaderStyle-ForeColor="#ffffff"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("ContactID") %>'
                                                CommandName="Edit" CausesValidation="false" ImageUrl="~/images/edit_icon.png" />
                                            <div style="display: none;">
                                                <asp:TextBox runat="server" Width="0px" ID="txtContactName" Text='<%# Eval("ContactLastName") & " " & Eval("ContactFirstName") %>'></asp:TextBox>
                                                <asp:TextBox ID="txtPartnerID" runat="server" Text='<%# Eval("ContactPartnerID") %>'
                                                    Width="0">
                                                </asp:TextBox>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdCMDelete %>" HeaderStyle-ForeColor="#ffffff"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                                CommandArgument='<%# Eval("ContactID") %>' CommandName="Delete" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="grid_footer" />
                                <RowStyle CssClass="grid_rowstyle" />
                                <PagerStyle CssClass="grid_footer" />
                                <HeaderStyle CssClass="grid_header" Height="26px" />
                                <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                <PagerSettings PageButtonCount="20" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="sqldsContact" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                        </ContentTemplate>
                        <Triggers>
                            <%--<asp:AsyncPostBackTrigger ControlID="imgSearch" EventName="OnClick" />--%>
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsContact" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
    <br />
    <br />
    <script language="javascript">
        function funSelectSearch(source, arguments) {
            if (window.document.getElementById('<%=dlSearch.ClientID%>').value != "" &&
                    window.document.getElementById('<%=txtSearch.ClientID%>').value == "") {
                window.document.getElementById('<%=txtSearch.ClientID%>').focus();
                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;
            }
        }
    </script>
</asp:Content>


<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="ViewMessage.aspx.vb" Inherits="Partner_ViewMessage" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<%@ Import Namespace="Resources.Resource" %>

<%@ Register src="../Controls/CommonSearch/CRMSearch.ascx" tagname="CRMSearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>
    <uc1:CRMSearch ID="CRMSearch1" runat="server" />
</asp:Content>

<asp:Content ID="cntViewMessage" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <h2>
                        <asp:Literal ID="lblTitle" runat="server"></asp:Literal>
                    </h2>
                </td>
                <td align="right">
                    <asp:UpdateProgress runat="server" ID="upMaster" DisplayAfter="10">
                        <ProgressTemplate>
                            <img src="../Images/wait22trans.gif" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
    </div>
    <div class="divMainContent" onkeypress="return disableEnterKey(event)">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="center" colspan="2">
                    <asp:UpdatePanel ID="upnlMsg" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                            <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr class="trheight">
                <td class="tdAlign">
                    <asp:Label ID="lblDescription" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMDescription%>"
                        runat="server" />
                </td>
                <td class="tdAlignLeft">
                    <FTB:FreeTextBox ID="txtEmailMessage" runat="server" Height="150" BackColor="#D2D2D5"
                        GutterBackColor="#D2D2D5" ToolbarBackColor="#D2D2D5" ToolbarBackgroundImage="false"
                        Width="540px" AutoConfigure="Alternate">
                    </FTB:FreeTextBox>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr class="trheight">
                <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;
                    padding-right: 5px;">
                    <table width="100%">
                        <tr>
                            <td height="20" align="center" width="50%">
                            </td>
                            <td width="50%">
                                <div class="buttonwrapper">
                                    <a id="cmdBack" onclick="history.back(-1);" runat="server" causesvalidation="false"
                                        class="ovalbutton" href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssBack%></span></a></div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsPartner" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
    <br />
    <br />
</asp:Content>

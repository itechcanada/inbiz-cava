<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false" CodeFile="AddCommunication.aspx.vb" Inherits="Partner_AddCommunication" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<%@ Import Namespace="Resources.Resource" %>

<%@ Register src="../Controls/CommonSearch/CRMSearch.ascx" tagname="CRMSearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>
   <uc1:CRMSearch ID="CRMSearch1" runat="server" />
</asp:Content>
<asp:Content ID="cntAddCommunication" ContentPlaceHolderID="cphMaster" runat="Server">
    
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal></h2>
                </td>
                <td style="text-align:right;">
                    <asp:UpdateProgress runat="server" ID="upComm" DisplayAfter="10">
                        <ProgressTemplate>
                            <img src="../Images/wait22trans.gif" alt="" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <table border="0" cellpadding="0" cellspacing="0" style="width:100%;">            
            <tr>
                <td align="left" bgcolor="#FFFFFF">
                    <table border="0" cellpadding="0" cellspacing="0" style="width:100%;">     
                        <tr>
                            <td align="center" colspan="2">
                                <asp:UpdatePanel ID="upnlMsg" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                                        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td width="20%" class="tdAlign" valign="top">
                                <asp:Label ID="lblSelectCommunication" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMCommType%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft" width="80%" valign="top">
                                <asp:DropDownList ID="dlCommunicationType" runat="server" CssClass="lblSimple">
                                    <asp:ListItem Text="<%$ Resources:Resource, liCMSelectCommType%>" Value="0" />
                                    <asp:ListItem Text="<%$ Resources:Resource, liCMCommEmail%>" Value="1" />
                                    <asp:ListItem Text="<%$ Resources:Resource, liCMCommFax%>" Value="2" />
                                    <asp:ListItem Text="<%$ Resources:Resource, liCMCommPhone%>" Value="3" />
                                </asp:DropDownList>
                                <span class="style1">*</span>
                                <asp:CustomValidator ID="custvalCommType" SetFocusOnError="true" runat="server" ErrorMessage="<%$ Resources:Resource, custvalCMCommType %>"
                                    ClientValidationFunction="funSelectCommType" Display="None">
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblEmailSubject" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMCommSubject%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtSubject" runat="server" Width="540px" MaxLength="35" />
                                <span class="style1">*</span>
                                <asp:RequiredFieldValidator ID="reqvalSubject" runat="server" ControlToValidate="txtSubject"
                                    ErrorMessage="<%$ Resources:Resource, reqvalCMCommSubject%>" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td width="20%" class="tdAlign" valign="top">
                                <asp:Label ID="lblEmailMessage" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMCommMessage%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft" width="80%">
                                <FTB:FreeTextBox ID="txtEmailMessage" runat="server" Height="150" BackColor="#D2D2D5"
                                    GutterBackColor="#D2D2D5" ToolbarBackColor="#D2D2D5" ToolbarBackgroundImage="false"
                                    Width="540px" AutoConfigure="Alternate">
                                </FTB:FreeTextBox>
                                <asp:RequiredFieldValidator ID="reqvalEmailMessage" runat="server" ControlToValidate="txtEmailMessage"
                                    SetFocusOnError="true" Display="None" ErrorMessage="<%$ Resources:Resource, reqvalCMCommMessage%>">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblAttachFile" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMAttachFile%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:FileUpload runat="server" ID="fileAttach" />
                            </td>
                        </tr>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblEmailFrom" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMCommMessageFrom%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtEmailFrom" runat="server" Width="540px" MaxLength="35" />
                                <span class="style1">*</span>
                                <asp:RequiredFieldValidator ID="reqvalEmailFrom" runat="server" ControlToValidate="txtEmailFrom"
                                    ErrorMessage="<%$ Resources:Resource, reqvalCMCommFrom%>" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td height="5" colspan="2">
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td align="right" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                                <div id="divSend" runat="server" style="display: block;">
                                    <table width="100%">
                                        <tr>
                                            <td height="20" align="center" width="25%">
                                            </td>
                                            <td width="20%">
                                                <div class="buttonwrapper">
                                                    <a id="cmdSend" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                                        style="min-width: 120px; text-align: center;">
                                                        <%=Resources.Resource.cmdCssSave%></span></a></div>
                                            </td>
                                            <td width="20%">
                                                <div class="buttonwrapper">
                                                    <a id="cmdBack" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                                        <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                            <%=Resources.Resource.cmdCssBack%></span></a></div>
                                            </td>
                                            <td width="20%">
                                                <div class="buttonwrapper">
                                                    <a id="cmdReset" runat="server" visible="false" causesvalidation="false" class="ovalbutton"
                                                        href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                            <%=Resources.Resource.cmdCssCancel%></span></a></div>
                                            </td>
                                            <td height="20" align="center" width="15%">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsMassMail" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
    <br />
    <br />
    <script language="javascript" type="text/javascript">
        function funSelectCommType(source, arguments) {
            if (window.document.getElementById('<%=dlCommunicationType.ClientID%>').value != "0") {
                arguments.IsValid = true;
            }
            else {
                window.document.getElementById('<%=dlCommunicationType.ClientID%>').focus();
                arguments.IsValid = false;
            }
        }
        function funClosePopUp(strOption) {
            document.getElementById(strOption).style.display = "none";
            document.getElementById('<%=divSend.ClientID%>').style.display = "block";
        }
        function popUp(strOption) {
            document.getElementById(strOption).style.display = "block";
            document.getElementById('<%=divSend.ClientID%>').style.display = "none";
            return false;
        }
    </script>
</asp:Content>
 
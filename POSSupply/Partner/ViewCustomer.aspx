﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="ViewCustomer.aspx.cs" Inherits="Partner_ViewCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <%--<%= Resources.Resource.TitleCMViewCustomer%>--%>
            <asp:Literal id="liAdmin" runat="server"></asp:Literal>
        </b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <asp:Button ID="btnAdd" runat="server" Text="Add New Customer" OnClick="btnAdd_Click" />
    </div>
    <div style="float: right;">
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="grid_wrapper" style="width: 100%;">
        <trirand:JQGrid runat="server" ID="grdCustomers" DataSourceID="sdsCustomers" Height="300px"
            AutoWidth="True" OnCellBinding="grdCustomers_CellBinding" OnDataRequesting="grdCustomers_DataRequesting">
            <Columns>
                <trirand:JQGridColumn DataField="PartnerID" HeaderText="<%$ Resources:Resource, grdCMcustomerID %>"
                    PrimaryKey="True" Visible="false" />
                <trirand:JQGridColumn DataField="PartnerAcronyme" HeaderText="<%$ Resources:Resource, grdCMAcronyme %>"
                    Editable="false" Visible="false" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdCMCustomerName %>" DataField="PartnerLongName"
                    Sortable="true" TextAlign="Left" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdCMEmail %>" DataField="PartnerEmail"
                    Sortable="true" TextAlign="Left" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdCMPhone %>" DataField="PartnerPhone"
                    Sortable="true" TextAlign="Left" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdCMFax %>" DataField="PartnerFax"
                    Sortable="true" TextAlign="Center" Visible="false" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdvStatus %>" DataField="PartnerID"
                    Sortable="true" TextAlign="Center" Width="60" />
                <trirand:JQGridColumn HeaderText="<%$Resources:Resource, lblRecentOrder%>" DataField="PartnerID"
                    Sortable="true" TextAlign="Center" Width="60" />
                <trirand:JQGridColumn HeaderText="<%$Resources:Resource, lblAvailableCredit%>" DataField="AvailableCredit"
                    Sortable="true" TextAlign="Right" Width="60" />
                <trirand:JQGridColumn HeaderText="<%$Resources:Resource, lblAmountDue%>" DataField="AmountDue"
                    Sortable="true" TextAlign="Right" Width="60" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdvEdit %>" DataField="PartnerID"
                    Sortable="false" TextAlign="Center" Width="60" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="PartnerID"
                    Sortable="false" TextAlign="Center" Width="60" Visible="false" />
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <ClientSideEvents LoadComplete="loadComplete" />
        </trirand:JQGrid>
        <asp:SqlDataSource ID="sdsCustomers" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
        <iCtrl:IframeDialog ID="mdDeleteUser" Width="400" Height="120" Title="Delete" Dragable="true"
            TriggerSelectorClass="pop_delete" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
            runat="server">
        </iCtrl:IframeDialog>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        Search form</div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="Label1" runat="server" CssClass="filter-key" AssociatedControlID="ddlStatus"
                        Text="<%$ Resources:Resource, lblStatus%>"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlStatus" runat="server" Width="175px">
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="lblField" CssClass="filter-key" AssociatedControlID="ddlSearchFields"
                        runat="server" Text="<%$Resources:Resource, lblSearchFields %>">            
                    </asp:Label>
                </h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlSearchFields" runat="server" Width="175px">
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" CssClass="filter-key" AssociatedControlID="txtSearch"
                        runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="Search" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%= Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $grid = $("#<%=grdCustomers.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=grdCustomers.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        function loadComplete(data) {
            if (data.rows.length == 1 && data.total == 1) {
                location.href = 'CustomerEdit.aspx?custid=' + data.rows[0].id + '&pType=<%=Request.QueryString["pType"] %>&gType=<%=Request.QueryString["gType"] %>';
            }
            else if (data.rows.length == 0) {
                if ($("#<%=txtSearch.ClientID%>").val() != "") {
                    var qData = {};
                    qData.pType = '<%=Request.QueryString["pType"] %>';
                    qData.gType = '<%=Request.QueryString["gType"] %>';
                    qData.smartGvId = $("#<%=txtSearch.ClientID%>").val();
                    location.href = 'CustomerEdit.aspx?' + $.param(qData);
                }
            }
            else {
                jqGridResize();
            }
        }       
    </script>
</asp:Content>

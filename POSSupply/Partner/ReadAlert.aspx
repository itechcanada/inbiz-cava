<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false" CodeFile="ReadAlert.aspx.vb" Inherits="Partner_ReadAlert" %>

<%@ Register src="../Controls/CommonSearch/CRMSearch.ascx" tagname="CRMSearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();              
    </script>
    <uc1:CRMSearch ID="CRMSearch1" runat="server" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle" Text="<%$ Resources:Resource, lblAlertDetails %>"></asp:Literal></h2>
                </td>
                <td style="text-align: right;">
                </td>               
            </tr>
        </table>
    </div>
    <div class="divMainContent">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td width="30%" class="tdAlign">
                    <asp:Label ID="lblPartnerName" runat="server" Text="<%$ Resources:Resource, lblAlertPartnerName %>"
                        CssClass="lblBold" Style="font-size: 13px;"></asp:Label>
                </td>
                <td width="70%" class="tdAlignLeft">
                    <asp:Label ID="PartnerName" runat="server" CssClass="lblSimple" Style="font-size: 13px;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="5">
                </td>
            </tr>
            <tr>
                <td class="tdAlign">
                    <asp:Label ID="lblContactName" runat="server" Text="<%$ Resources:Resource, lblAlertContactName %>"
                        CssClass="lblBold" Style="font-size: 13px;"></asp:Label>
                </td>
                <td class="tdAlignLeft">
                    <asp:Label ID="ContactName" runat="server" CssClass="lblSimple" Style="font-size: 13px;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="5">
                </td>
            </tr>
            <tr>
                <td class="tdAlign">
                    <asp:Label ID="lblAlertDate" runat="server" Text="<%$ Resources:Resource, lblAlertDate %>"
                        CssClass="lblBold" Style="font-size: 13px;"></asp:Label>
                </td>
                <td class="tdAlignLeft">
                    <asp:Label ID="AlertTime" runat="server" CssClass="lblSimple" Style="font-size: 13px;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="5">
                </td>
            </tr>
            <tr>
                <td class="tdAlign">
                    <asp:Label ID="lblNote" runat="server" Text="<%$ Resources:Resource, lblAlertNote %>"
                        CssClass="lblBold" Style="font-size: 13px;"></asp:Label>
                </td>
                <td class="tdAlignLeft">
                    <asp:Label ID="AlertNote" runat="server" CssClass="lblSimple" Style="font-size: 13px;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="20">
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:ImageButton ID="imgContact" runat="server" ImageUrl="~/Images/ViewContact.gif" />
                </td>
            </tr>
            <tr>
                <td height="20">
                </td>
            </tr>
        </table>
    </div>
</asp:Content>


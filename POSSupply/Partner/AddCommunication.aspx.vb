Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Partial Class Partner_AddCommunication
    Inherits BasePage
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If

        lblError.Text = ""
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If
        If Request.QueryString("PartnerID") <> "" And Request.QueryString("ContactID") <> "" Then

        Else
            Session.Add("Msg", msgCMNoCommunicationContactAvailable)
            lblError.Text = msgCMNoCommunicationContactAvailable
            Exit Sub
        End If
        If Not IsPostBack Then
            txtSubject.Focus()
            lblTitle.Text = TitleCMCommunication
            subFillRecord()
        End If
    End Sub
    'Populate Controls
    Public Sub subFillRecord()
        txtEmailFrom.Text = Session("EmailID").ToString
    End Sub
    ' Reset Controls
    Private Sub SubResetControls()
        dlCommunicationType.SelectedValue = 0
        txtSubject.Text = ""
        txtEmailMessage.Text = ""
        txtEmailFrom.Text = ""
        subFillRecord()
    End Sub
    'Populate Object
    Private Sub subSetData(ByRef objCC As clsContactCommunication)

    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    'Upload File
    Private Function funUploadFile(ByRef sFilePathName As String) As Boolean
        Dim bFlag As Boolean = False
        If fileAttach.HasFile Then
            Dim StrUploadeImg As String = fileAttach.FileName.Trim
            Dim strImage() As String = Split((StrUploadeImg), ".")
            Dim count As Int16 = strImage.Length - 1
            If strImage.Length = "1" Then
                lblMsg.Text = ""
                lblError.Text = msgCMInvalidFileExtension
                fileAttach.Focus()
                bFlag = False
                Exit Function
            End If
            Dim strFN As String = strImage(0) & "_" & Now.ToString("yyyyMMddHHmmss") & "." & strImage(count)
            Try
                Dim strSrc As String = ""
                strSrc = Server.MapPath("~") & "\Upload\Mail\"

                Dim SaveLocation As String = strSrc & strFN
                sFilePathName = SaveLocation
                fileAttach.SaveAs(SaveLocation)
                bFlag = True
            Catch ex As Exception

            End Try

        End If
        Return bFlag
    End Function
    ' Go Back
    Protected Sub cmdBack_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.ServerClick
        Response.Redirect("AddEditContact.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&ContactID=" & Request.QueryString("ContactID") & "&mp=" & Request.QueryString("mp"))
    End Sub
    ' Send Mail
    Protected Sub cmdSend_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSend.ServerClick
        If Not Page.IsValid Then
            'Exit Sub
        End If

        Dim objMasEml As New clsMassEmail
        Dim strMailFrom As String = txtEmailFrom.Text
        Dim strSub As String = txtSubject.Text
        Dim strMsg As String = txtEmailMessage.Text
        Dim strName As String = ""
        Dim strData As String = ""
        Dim strFirstName As String = ""
        Dim strLastName As String = ""
        'strMailFrom = Session("EmailID").ToString

        Dim strFN As String = ""
        If funUploadFile(strFN) = False Then
            If strFN <> "" Then
                lblError.Text = msgCMPleaseAttachFileAgain
                Exit Sub
            End If
        End If

        strName = ""
        strFirstName = ""
        strLastName = ""
        strMsg = txtEmailMessage.Text

        Dim objPC As New clsPartnerContacts
        objPC.ContactID = Request.QueryString("ContactID")
        objPC.getPartnerContactsInfo()
        strFirstName = objPC.ContactFirstName
        strLastName = objPC.ContactLastName
        strData = objPC.ContactEmail
        objPC = Nothing
        Dim strMailTo As String = strData
        If dlCommunicationType.SelectedValue = "1" Then
            objMasEml.funCustomizedMail(strSub, strMsg, strMailTo, strMailFrom, strFirstName, strLastName, strFN)
        End If

        strMsg = strMsg.Replace("&lt;FIRST NAME&gt;", strFirstName)
        strMsg = strMsg.Replace("&lt;LAST NAME&gt;", strLastName)
        Dim objCC As New clsContactCommunication
        objCC.ComContactID = Request.QueryString("ContactID")
        objCC.ComType = dlCommunicationType.SelectedValue
        objCC.ComMotive = strSub
        objCC.ComDateTime = Now().ToString("yyyy-MM-dd HH:mm:ss")
        Select Case dlCommunicationType.SelectedValue
            Case "1"
                objCC.ComServiceLog = ""
                objCC.ComEmailMessage = strMsg
            Case "2", "3"
                objCC.ComServiceLog = strMsg
                objCC.ComEmailMessage = ""
        End Select
        objCC.ComCreatedBy = Session("UserID")
        objCC.insertContactCommunication()
        objCC = Nothing
        Select Case dlCommunicationType.SelectedValue
            Case "1"
                Session.Add("Msg", CMCommMailSendSuccessfully)
                lblMsg.Text = CMCommMailSendSuccessfully
            Case "2", "3"
                Session.Add("Msg", CMCommAddedSuccessfully)
                lblMsg.Text = CMCommAddedSuccessfully
        End Select
        SubResetControls()
        Response.Redirect("AddEditContact.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&ContactID=" & Request.QueryString("ContactID") & "&mp=" & Request.QueryString("mp"))
    End Sub
End Class


﻿Imports iTECH.InbizERP.BusinessLogic

Partial Class Partner_AddCategory
    Inherits System.Web.UI.Page

    Private objCategories As New BLLCategories

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.Form("cName") IsNot Nothing And Request.Form("cName") <> "" Then
            objCategories.CategoryName.Add(ApplicationLanguage.English, Request.Form("cName"))
            objCategories.CategoryName.Add(ApplicationLanguage.French, Request.Form("cName"))
            objCategories.IsActive = True

            Try
                If objCategories.Insert() Then
                    Dim catID As Integer = objCategories.GetMaxID()
                    ltResult.Text = catID.ToString()
                End If
            Catch ex As Exception
                ltResult.Text = "NO"
            End Try
        Else
            ltResult.Text = "NO"
        End If
    End Sub
End Class

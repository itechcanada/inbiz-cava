﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.Web;

public partial class Partner_delete : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!IsPostBack) {            
            if (!UserProfile.IsAuthenticated) {
                Globals.RegisterCloseDialogScript(this);
            }
        }        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        switch (this.CurrentCommand.ToLower()) { 
            case DeleteCommands.DELETE_CUSTOMER:
                DeleteCustomer();
                break;            
        }
    }             

    public string CurrentCommand {
        get {
            if (!string.IsNullOrEmpty(Request.QueryString["cmd"]))
            {
                return Request.QueryString["cmd"].ToLower();
            }
            return "";
        }
    }

    private int GetKey(int ofIndex) {
        string cleneStr = StringUtil.GetCleneJoinedString(",", this.KeysToDelete);
        int[] keys = StringUtil.GetArrayFromJoindString(",", cleneStr);
        try
        {
            return keys[ofIndex];
        }
        catch 
        {
            return 0;
        }
    }

    private string GetStringKey(int ofIndex){
        string cleneStr = StringUtil.GetCleneJoinedString(",", this.KeysToDelete);
        string[] keys = cleneStr.Split(',');
        try
        {
            return keys[ofIndex];
        }
        catch 
        {
            return string.Empty;
        }
    }

    public string KeysToDelete {
        get {
            if (!string.IsNullOrEmpty(Request.QueryString["keys"])) {
                return Request.QueryString["keys"];
            }
            return "";
        }
    }

    public string JsCallBackFunction {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }

    #region Delete Functions    
    public void DeleteCustomer()
    {
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            try
            {
                Partners part = new Partners();
                part.Delete(this.GetKey(0), false);
                lblMessage.Text = Resources.Resource.msgDeleteSuccess;
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
                }
                pnlDeleteCommand.Visible = false;
                pnlOk.Visible = true;
            }
            catch 
            {
                lblMessage.Text = "Critical Error!";
            }           
        }
        else
        {
            lblMessage.Text = Resources.Resource.lblAccessDenied;
        }
    }
    #endregion
}
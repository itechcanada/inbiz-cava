<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false" CodeFile="AddEditFollowups.aspx.vb" Inherits="Partner_AddEditFollowups" title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="AjaxControlToolkit" %>
<%@ Import Namespace="Resources.Resource" %>

<%@ Register src="../Controls/CommonSearch/CRMSearch.ascx" tagname="CRMSearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();
        $('#divMainContainerTitle1').corner();     
    </script>
    <uc1:CRMSearch ID="CRMSearch1" runat="server" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div align="center">
        <table style="width:100%">
            <tr valign="top">
                <td width="45%">
                    <div id="divMainContainerTitle" class="divMainContainerTitle">
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td style="width: 300px;">
                                    <h2>
                                        <asp:Literal runat="server" ID="lblCustomerDtl"></asp:Literal></h2>
                                </td>
                                <td style="text-align: right;">
                                </td>
                                <td style="width: 150px;">
                                    <div class="buttonwrapper">
                                        <a id="cmdCustomerDetail" runat="server" causesvalidation="false" class="ovalbutton"
                                            href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                <%=Resources.Resource.btnColCustomerDetail%>
                                            </span></a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <asp:HiddenField ID="hdnPatID" runat="server" />
                    </div>
                    <div class="divMainContent">
                        <table width="100%">
                            <tr>
                                <td class="tdAlign" width="36%">
                                    <asp:Label ID="lblCustomerName1" Text="<%$ Resources:Resource, lblSOCustomerName %>"
                                        CssClass="lblBold" runat="server" />
                                </td>
                                <td width="1%">
                                </td>
                                <td align="left" width="63%">
                                    <asp:Label ID="lblCustomerName" CssClass="lblSimple" runat="server" />
                                </td>
                            </tr>
                            <tr height="20">
                                <td class="tdAlign">
                                    <asp:Label ID="lblARPhone" CssClass="lblBold" Text="<%$ Resources:Resource, lblPhone%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblPhone" runat="server" CssClass="lblSimple"></asp:Label>
                                </td>
                            </tr>
                            <tr height="20">
                                <td class="tdAlign">
                                    <asp:Label ID="lblARFax" Text="<%$ Resources:Resource, lblFax %>" CssClass="lblBold"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblFax" CssClass="lblSimple" runat="server" />
                                </td>
                            </tr>
                            <tr height="20">
                                <td class="tdAlign">
                                    <asp:Label ID="lblAREmail" CssClass="lblBold" Text="<%$ Resources:Resource, lblEmail%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblEmail" runat="server" CssClass="lblSimple">
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr height="20">
                                <td class="tdAlign">
                                    <asp:Label ID="Label2" CssClass="lblBold" Text="<%$ Resources:Resource, lblColContactName%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblContactName" runat="server" CssClass="lblSimple"></asp:Label>
                                </td>
                            </tr>
                            <tr height="26" valign="top">
                                <td class="tdAlign">
                                    <asp:Label ID="Label4" Text="<%$ Resources:Resource, lblColContactTitle %>" CssClass="lblBold"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox Visible="false" runat="server" ID="txtContactTitle" TextMode="MultiLine"
                                        Rows="2" Width="200px" CssClass="lblSimple"></asp:TextBox>
                                    <asp:Label ID="lblContactTitle" CssClass="lblSimple" runat="server" />
                                </td>
                            </tr>
                            <tr height="20" valign="top">
                                <td class="tdAlign">
                                    <asp:Label ID="Label1" Text="<%$ Resources:Resource, POAddress %>" CssClass="lblBold"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox runat="server" ID="txtAddress" TextMode="MultiLine" Rows="4" Width="200px"
                                        CssClass="lblSimple"></asp:TextBox>
                                    <asp:Label ID="lblAddress" Visible="false" CssClass="lblBold" runat="server" />
                                </td>
                            </tr>
                            <tr height="20" valign="top">
                                <td class="tdAlign">
                                    <asp:Label ID="Label5" Text="<%$ Resources:Resource, lblCMNotes %>" CssClass="lblBold"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox runat="server" ID="txtNotes" TextMode="MultiLine" Rows="3" Width="200px"
                                        CssClass="lblSimple"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td height="9">
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td width="1%">
                </td>
                <td width="54%">
                    <div id="divMainContainerTitle1" class="divMainContainerTitle">
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td style="width: 300px;">
                                    <h2>
                                        <asp:Literal runat="server" ID="Literal1" Text="<%$ Resources:Resource, TitleAddLog %>"></asp:Literal></h2>
                                </td>
                                <td style="text-align: right;">
                                </td>
                                <td style="width: 150px;">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="divMainContent">
                        <table width="100%">
                            <tr>
                                <td height="4">
                                </td>
                            </tr>
                            <tr>
                                <td class="tdAlign" width="28%">
                                    <asp:Label ID="lblCAFollowupDateTime" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblFollowupDate %>">
                                    </asp:Label>
                                </td>
                                <td class="tdAlignLeft" width="72%">
                                    <asp:TextBox ID="txtFollowupDateTime" Enabled="true" runat="server" Width="90px"
                                        MaxLength="20">
                                    </asp:TextBox>
                                    <asp:ImageButton ID="imgAmtRcvdDate" CausesValidation="false" runat="server" ToolTip="Click to show calendar"
                                        ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                                    <%=lblPODateFormat %>
                                    <span class="style1">*</span>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender4" TargetControlID="txtFollowupDateTime"
                                        runat="server" PopupButtonID="imgAmtRcvdDate" Format="MM/dd/yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RequiredFieldValidator ID="reqvalAmtRcvdDateTime" runat="server" ControlToValidate="txtFollowupDateTime"
                                        ErrorMessage="<%$ Resources:Resource, msgCAPlzEntCAFollowupDate %>" SetFocusOnError="true"
                                        Display="None">
                                    </asp:RequiredFieldValidator>
                                    <div style="display: none;">
                                        <asp:TextBox ID="txtDate" Enabled="true" runat="server" Width="0px" MaxLength="15"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td height="4">
                                </td>
                            </tr>
                            <tr>
                                <td class="tdAlign">
                                    <asp:Label ID="Label3" CssClass="lblBold" Text="<%$ Resources:Resource, lblFollowupTime%>"
                                        runat="server" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHour" runat="server" Width="40"></asp:TextBox>
                                    <asp:TextBox ID="txtMin" runat="server" Width="40"></asp:TextBox>
                                    <asp:DropDownList runat="Server" ID="ddlTime" Width="45">
                                        <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                    </asp:DropDownList>
                                    (hh:mm)<span class="style1"> *</span>
                                    <asp:RequiredFieldValidator ID="reqValHour" runat="server" ControlToValidate="txtHour"
                                        ErrorMessage="<%$ Resources:Resource, reqValHour %>" SetFocusOnError="true" Display="None">
                                    </asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgVHour" runat="server" ControlToValidate="txtHour" Type="Integer"
                                        ErrorMessage="<%$ Resources:Resource, rgVHour%>" MaximumValue="12" MinimumValue="1"
                                        Display="None"></asp:RangeValidator>
                                    <asp:RangeValidator ID="rgVMin" runat="server" ControlToValidate="txtMin" Type="Integer"
                                        ErrorMessage="<%$ Resources:Resource, rgVMin%>" MaximumValue="59" MinimumValue="0"
                                        Display="None"></asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td height="4">
                                </td>
                            </tr>
                            <tr valign="top">
                                <td class="tdAlign">
                                    <asp:Label ID="lblARNote" CssClass="lblBold" Text="<%$ Resources:Resource, lblARNote%>"
                                        runat="server" />
                                </td>
                                <td class="tdAlignLeft">
                                    <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="9" Width="250px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td height="4">
                                </td>
                            </tr>
                            <tr valign="top">
                                <td class="tdAlign">
                                    <asp:Label ID="Label6" CssClass="lblBold" Text="<%$ Resources:Resource, FollowUpAssignedTo%>"
                                        runat="server" />
                                </td>
                                <td class="tdAlignLeft">
                                    <asp:CheckBoxList ID="chklstSalesRep" runat="server" RepeatDirection="vertical">
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td height="4">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <table width="100%">
                                        <tr>
                                            <td height="20" align="center" width="40%">
                                            </td>
                                            <td width="60%">
                                                <asp:ValidationSummary ID="valsAddlog" runat="server" ShowMessageBox="true" ShowSummary="false" />
                                                <div class="buttonwrapper">
                                                    <a id="CmdAddLog" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                                        style="min-width: 120px; text-align: center;">
                                                        <%=Resources.Resource.cmdColAddLog%>
                                                    </span></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Button ID="btnAddlog" runat="server" Style="display: none;" />
                                </td>
                            </tr>
                            <tr>
                                <td height="7">
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr height="10">
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr height="10">
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        
                        <div runat="server" id="divLogDetails">
                            <div class="divMainContainerTitle">
                                <h2>
                                     <asp:Literal runat="server" ID="lblTitleLogDetails"></asp:Literal>
                                </h2>
                            </div>
                            <div class="divMainContent">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td colspan="7" valign="top" align="center">
                                            <asp:GridView ID="grdActivityLog" runat="server" AllowSorting="True" DataSourceID="sqldsActivityLog"
                                                AllowPaging="True" PageSize="10" PagerSettings-Mode="Numeric" CellPadding="0"
                                                GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
                                                CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="PartnerActivityId"
                                                Width="100%">
                                                <Columns>
                                                    <asp:BoundField DataField="PartnerId" HeaderText="<%$ Resources:Resource, grdCMPartnerID %>"
                                                        ReadOnly="True" SortExpression="PartnerId">
                                                        <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="FollowUpDate" HeaderText="<%$ Resources:Resource, grdCAFollowupDateTime %>"
                                                        ReadOnly="True" SortExpression="FollowUpDate">
                                                        <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ActivityCreatedDate" HeaderText="<%$ Resources:Resource, grdHDActivityCreateDate %>"
                                                        ReadOnly="True" SortExpression="ActivityCreatedDate">
                                                        <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="UserName" HeaderText="<%$ Resources:Resource, grdCreatedPerson %>"
                                                        ReadOnly="True" SortExpression="UserName">
                                                        <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdCAFollowupLogText %>" HeaderStyle-ForeColor="#ffffff"
                                                        HeaderStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLog" ForeColor="black" runat="server" Text='<%# Eval("LogText") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="left" Width="200px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="left"
                                                        HeaderText="<%$ Resources:Resource, FollowUpAssignedTo %>">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAssignedTo" ForeColor="black" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="left" Width="200px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle CssClass="grid_footer" />
                                                <RowStyle CssClass="grid_rowstyle" />
                                                <PagerStyle CssClass="grid_footer" />
                                                <HeaderStyle CssClass="grid_header" Height="26px" />
                                                <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                                <PagerSettings PageButtonCount="20" />
                                            </asp:GridView>
                                            <asp:SqlDataSource ID="sqldsActivityLog" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr height="10">
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="7">
                        <table width="100%">
                            <tr>
                                <td height="20" align="center" width="40%">
                                </td>
                                <td width="60%">
                                    <div class="buttonwrapper">
                                        <a id="cmdBack" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                            <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                <%=Resources.Resource.cmdInvBack%>
                                            </span></a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
    </div>
    <br />
</asp:Content>


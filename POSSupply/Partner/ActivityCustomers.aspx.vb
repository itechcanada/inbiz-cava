Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Imports System.Data.Odbc

Partial Class Partner_ActivityCustomers
    Inherits BasePage
    Private strPartnerID As String = ""
    Private objPart As New clsPartners
    Private clsStatus As New clsSysStatus
    Protected Sub Admin_ViewCustomer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        lblMsg.Text = ""
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If
        lblTitle.Text = TitleCMViewCustomer

        If Not Page.IsPostBack Then
            clsStatus.subGetStatus(ddlStatus, "CU", "dlSch")
            clsStatus.subGetStatus(dlSearchCatg, "CU", "dlSh2")
            subGetQuerryString()
            subFillGrid()
        End If

        clsGrid.MaintainParams(Page, grdPartner, pnlSearchForm, lblMsg, imgSearch)

        txtSearch.Focus()
    End Sub
    Public Sub subGetQuerryString()
        If Session("Searchtxt") <> "" Or Session("Status") <> "1" Then
            txtSearch.Text = Session("Searchtxt")
            ddlStatus.SelectedValue = Session("Status")
        End If
        Session.Remove("Searchtxt")
        Session.Remove("Status")
    End Sub
    'Populate Grid
    Public Sub subFillGrid()
        sqldsPartner.SelectCommand = objPart.funFillActivityCustomersGrid(ddlStatus.SelectedValue, txtSearch.Text, "", dlSearchCatg.SelectedValue)  'fill User Record
    End Sub
    'On Page Index Change
    Protected Sub grdPartner_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdPartner.PageIndexChanging
        subFillGrid()
    End Sub

    Protected Sub grdPartner_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdPartner.RowCommand
        If e.CommandName = "Activities" Then
            subSetSession()
            Response.Redirect("~/Partner/AddEditFollowups.aspx?PartnerId=" & e.CommandArgument.ToString)
        ElseIf e.CommandName = "ViewLog" Then
            subSetSession()
            Response.Redirect("~/Partner/AddEditFollowups.aspx?PartnerId=" & e.CommandArgument.ToString)
        End If
    End Sub
    ' Set the values in Session variables
    Public Sub subSetSession()
        Session("Status") = ddlStatus.SelectedValue
        Session("Searchtxt") = txtSearch.Text.Trim
    End Sub
    'On Row Data Bound
    Protected Sub grdPartner_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdPartner.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim lblPartnerName As Label = CType(e.Row.FindControl("lblPartnerName"), Label)
            lblPartnerName.Text = DataBinder.Eval(e.Row.DataItem, "PartnerAcronyme") & "<BR/>" & DataBinder.Eval(e.Row.DataItem, "PartnerLongName") & "<BR/>" & funGetCustomerAddress(DataBinder.Eval(e.Row.DataItem, "PartnerID").ToString)
            lblPartnerName.Text = lblPartnerName.Text.Replace(Environment.NewLine, "<br />")

            Dim aPhn1 As HtmlAnchor = CType(e.Row.FindControl("aPhone1"), HtmlAnchor)
            Dim aPhn2 As HtmlAnchor = CType(e.Row.FindControl("aPhone2"), HtmlAnchor)

            aPhn1.HRef = "callto://" & DataBinder.Eval(e.Row.DataItem, "PartnerPhone")
            aPhn2.HRef = "callto://" & DataBinder.Eval(e.Row.DataItem, "PartnerPhone2")

            Dim lnkActivities As LinkButton = CType(e.Row.FindControl("lnkActivities"), LinkButton)
            Dim objActivity As New clsPartnerActivityLog
            objActivity.PartnerId = DataBinder.Eval(e.Row.DataItem, "PartnerID").ToString
            lnkActivities.Text = "(" & objActivity.GetActivitiesCountByPartnerId().ToString & ")" ' To Do Here To get All Actifity folloups for Customer.

            If lnkActivities.Text = "(0)" Then
                lnkActivities.OnClientClick = "alert('Customer has 0 activities.'); return false;"
            End If

        End If
    End Sub


    Private Function funGetCustomerAddress(ByVal strCustID As String) As String
        Dim strAddress As String
        Dim objCust As New clsExtUser
        objCust.CustID = strCustID
        objCust.getCustomerInfo()
        strAddress = funPopulateAddress(objCust.CustType, "B", strCustID).Replace("<br>", vbCrLf)
        Return strAddress
    End Function
    'Populate Address
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        Dim objPrtCon As New clsPartnerContacts
        objPrtCon.subPartnerContactsInfo(sSource)

        If objPrtCon.ContactFirstName <> "" Then
            strAddress += lblContact & " " & objPrtCon.ContactFirstName + "<br />"
        End If

        objAdr.getAddressInfo(sRef, sType, sSource)

        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + ","
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + ","
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3
        End If
        strAddress += "<br>"
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + ","
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + "<br />"
            Else
                strAddress += objAdr.AddressState + "<br />"
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + "<br />"
            Else
                strAddress += objAdr.AddressCountry + "<br />"
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += "Postal Code: " + objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function

    'Sorting
    Protected Sub grdPartner_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdPartner.Sorting
        subFillGrid()
    End Sub
    ' Sql Data Source Event Track
    Protected Sub sqldsPartner_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsPartner.Selected
        If e.AffectedRows = 0 Then
            lblMsg.Text = CMNoDataFound
        End If
    End Sub
    ' On Search Image Click
    Protected Sub imgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
        subFillGrid()
        lblMsg.Text = ""
        txtSearch.Focus()
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
End Class


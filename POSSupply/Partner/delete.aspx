﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="delete.aspx.cs" Inherits="Partner_delete" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div style="padding: 5px; height:20px; overflow: auto;">
        <h1>
            <asp:Literal ID="lblMessage" Text="Are you sure you want to delete?" runat="server" />            
        </h1>        
    </div>    
    <asp:Panel ID="pnlDeleteCommand" runat="server">
        <div class="div-dialog-command">
            <asp:Button  ID="btnSave" runat="server" Text="Delete" onclick="btnSave_Click" />
            <asp:Button Text="<%$Resources:Resource, btnCancel %>"  ID="btnCancel" runat="server" CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
        </div>
    </asp:Panel>
    <asp:Panel CssClass="div-dialog-command" ID="pnlOk" runat="server" Visible="false">
        <asp:Button ID="btnOk" runat="server" CausesValidation="False" Text="Ok" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
    
</asp:Content>


Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Partial Class Partner_AddEditEvent
    Inherits BasePage
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        lblMsg.Text = ""
        lblError.Text = ""
        If Not IsPostBack Then
            lblTitle.Text = TitleCMEvents
            txtDate.Text = Now.ToString("MM/dd/yyyy HH:mm:ss")
            subPopulateEventType()
            subPopulateVisitor()
            subPopulateEducationLevel()
            If Request.QueryString("PartnerID") <> "" Then
                divCrtMod.Style("display") = "block"
                lblTitle.Text = TitleCMEvents
                subFillRecord()
            Else
                divCrtMod.Style("display") = "none"
            End If
        End If
    End Sub
    'Populate Object
    Private Sub subSetData(ByRef objPEL As clsPartnerEventLogs)
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            objPEL.PartnerID = Convert.ToString(Request.QueryString("PartnerID"))
            objPEL.PartnerSvcDateTime = DateTime.ParseExact(txtEventDateTime.Text & " " & txtEventTime.Text, "MM/dd/yyyy HH:mm:ss", Nothing).ToString("yyyy-MM-dd HH:mm:ss")
            objPEL.PartnerEventType = dlEventType.SelectedValue
            objPEL.PartnerSvcLog = txtNotes.Text
            objPEL.PartnerEventShortName = txtShortName.Text
            objPEL.PartnerEvenLongName = txtLongName.Text
            objPEL.PartnerNumberOfVisitors = txtNoOfVisitors.Text
        End If
    End Sub
    'Populate Controls
    Public Sub subFillRecord()
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            Dim sPID As String = Convert.ToString(Request.QueryString("PartnerID"))
            Dim objP As New clsPartners
            objP.PartnerID = sPID
            objP.getPartnersInfo()
            lblCreated.Text = lblCMCreated
            lblCreatedDate.Text = CDate(objP.PartnerCreatedOn).ToString("MMMM dd, yyyy")
            lblCreatedBy.Text = objP.funGetCreatedBy(objP.PartnerCreatedBy)
            lblModified.Text = lblCMModified
            lblModifiedDate.Text = CDate(objP.PartnerLastUpdatedOn).ToString("MMMM dd, yyyy")
            lblModifiedBy.Text = objP.funGetModifiedBy(objP.PartnerLastUpdatedBy)
            objP = Nothing
            If Convert.ToString(Request.QueryString("LogID")) <> "" Then
                Dim sLogID As String = Convert.ToString(Request.QueryString("LogID"))
                Dim objPEL As New clsPartnerEventLogs
                objPEL.PartnerServiceLogID = sLogID
                objPEL.getPartnerEventLogsInfo()
                txtEventDateTime.Text = CDate(objPEL.PartnerSvcDateTime).ToString("MM/dd/yyyy")
                txtEventTime.Text = CDate(objPEL.PartnerSvcDateTime).ToString("HH:mm:ss")
                dlEventType.SelectedValue = objPEL.PartnerEventType
                txtNotes.Text = objPEL.PartnerSvcLog
                txtShortName.Text = objPEL.PartnerEventShortName
                txtLongName.Text = objPEL.PartnerEvenLongName
                txtNoOfVisitors.Text = objPEL.PartnerNumberOfVisitors
                objPEL = Nothing

                Dim objDR As Data.Odbc.OdbcDataReader
                'Populate Event Selection Visitor
                Dim objPESV As New clsPartnerEvtSelVst
                objPESV.PartnerEvtSvcLogID = sLogID

                objDR = objPESV.GetDataReader(objPESV.funFillGrid())
                While objDR.Read
                    For idx As Integer = 0 To dlstVisitors.Items.Count - 1
                        Dim hdnAD As HiddenField = CType(dlstVisitors.Items(idx).FindControl("hdnAD"), HiddenField)
                        Dim txtAD As TextBox = CType(dlstVisitors.Items(idx).FindControl("txtAD"), TextBox)

                        If objDR.Item("PartnerEventVistorTxtID").ToString = hdnAD.Value Then
                            txtAD.Text = objDR.Item("PartnerEvtVstVal").ToString
                        End If
                    Next
                End While
                objDR.Close()
                objPESV = Nothing

                'Populate Event Selection Education Level
                Dim objPESE As New clsPartnerEvtSelEdu
                objPESE.PartnerEvtSvcLogID = sLogID

                objDR = objPESE.GetDataReader(objPESE.funFillGrid())
                While objDR.Read
                    For idx As Integer = 0 To dlstEduLvl.Items.Count - 1
                        Dim hdnEduLvl As HiddenField = CType(dlstEduLvl.Items(idx).FindControl("hdnEduLvl"), HiddenField)
                        Dim txtEduLvl As TextBox = CType(dlstEduLvl.Items(idx).FindControl("txtEduLvl"), TextBox)

                        If objDR.Item("PartnerEvenEduLvlTxtID").ToString = hdnEduLvl.Value Then
                            txtEduLvl.Text = objDR.Item("PartnerEvtEduVal").ToString
                        End If
                    Next
                End While
                objDR.Close()
                objPESE = Nothing

                'Populate Event Sector
                Dim objPES As New clsPartnerEvtSector
                objPES.PartnerEvtSvcLogID = sLogID
                objPES.getPartnerEvtSectorWithRefToLogID()
                txtSector.Text = objPES.PartnerEvtSectorVal
                objPES = Nothing
            End If
        End If
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    'Populate EventType
    Private Sub subPopulateEventType()
        Dim objPET As New clsPartnerEventTypes
        objPET.PopulateEventTypes(dlEventType)
        objPET = Nothing
    End Sub
    'Populate Visitor
    Public Sub subPopulateVisitor()
        Dim objPEV As New clsPartnerEventVisitors
        sqldsVisitor.SelectCommand = objPEV.funFillGrid
        sqldsVisitor.DataBind()
    End Sub
    'Populate EducationLevel
    Public Sub subPopulateEducationLevel()
        Dim objPEEL As New clsPartnerEventEduLvl
        sqldsEduLvl.SelectCommand = objPEEL.funFillGrid
        sqldsEduLvl.DataBind()
    End Sub

    Protected Sub cmdBack_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.ServerClick
        Dim strPartnerID As String = ""
        strPartnerID = Convert.ToString(Request.QueryString("PartnerID"))
        Response.Redirect("AddEditCustomer.aspx?PartnerID=" & strPartnerID)
    End Sub
    ' Reset Controls
    Protected Sub cmdReset_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReset.ServerClick
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            subFillRecord()
            Exit Sub
        End If
        txtShortName.Text = ""
        txtLongName.Text = ""
        dlEventType.SelectedValue = 0
        txtNoOfVisitors.Text = ""
        txtEventDateTime.Text = ""
        subPopulateEventType()
    End Sub
    ' Insert/Update Partner Event
    Protected Sub cmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.ServerClick
        If Not Page.IsValid Then
            Exit Sub
        End If

        lblError.Text = ""

        If txtEventTime.Text = "" Then
            lblError.Text = reqvalCMEventTime
            Exit Sub
        Else
            Dim strDT As String = ""
            strDT = txtEventDateTime.Text & " " & txtEventTime.Text
            If IsDate(strDT) = False Then
                lblError.Text = reqvalCMValidEventTime
                Exit Sub
            End If
        End If

        If dlstVisitors.Items.Count > 0 Then
            Dim nSumAD As Integer = 0
            For idx As Integer = 0 To dlstVisitors.Items.Count - 1
                Dim hdnAD As HiddenField = CType(dlstVisitors.Items(idx).FindControl("hdnAD"), HiddenField)
                Dim txtAD As TextBox = CType(dlstVisitors.Items(idx).FindControl("txtAD"), TextBox)
                If txtAD.Text <> "" Then
                    nSumAD += CInt(txtAD.Text)
                End If
            Next
            If nSumAD <> 100 Then
                lblError.Text = msgCMSumAverageDemographic
                Exit Sub
            End If
        End If

        If dlstEduLvl.Items.Count > 0 Then
            Dim nSumEL As Integer = 0
            For idx As Integer = 0 To dlstEduLvl.Items.Count - 1
                Dim hdnEduLvl As HiddenField = CType(dlstEduLvl.Items(idx).FindControl("hdnEduLvl"), HiddenField)
                Dim txtEduLvl As TextBox = CType(dlstEduLvl.Items(idx).FindControl("txtEduLvl"), TextBox)
                If txtEduLvl.Text <> "" Then
                    nSumEL += CInt(txtEduLvl.Text)
                End If
            Next
            If nSumEL <> 100 Then
                lblError.Text = msgCMSumEducationLevel
                Exit Sub
            End If
        End If

        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            Dim objPEvtLog As New clsPartnerEventLogs
            subSetData(objPEvtLog)
            Dim strPartnerID As String = ""
            Dim strLogID As String = ""
            strPartnerID = Convert.ToString(Request.QueryString("PartnerID"))
            If Convert.ToString(Request.QueryString("LogID")) <> "" Then
                strLogID = Convert.ToString(Request.QueryString("LogID"))

                'Update Section
                objPEvtLog.PartnerServiceLogID = strLogID
                objPEvtLog.updatePartnerEventLogs()

                For idx As Integer = 0 To dlstVisitors.Items.Count - 1
                    Dim objPESV As New clsPartnerEvtSelVst
                    Dim hdnAD As HiddenField = CType(dlstVisitors.Items(idx).FindControl("hdnAD"), HiddenField)
                    Dim txtAD As TextBox = CType(dlstVisitors.Items(idx).FindControl("txtAD"), TextBox)
                    objPESV.PartnerEvtSvcLogID = strLogID
                    objPESV.PartnerEventVistorTxtID = hdnAD.Value
                    objPESV.PartnerEvtVstVal = txtAD.Text
                    If txtAD.Text <> "" Then
                        If objPESV.funCheckRecord() = False Then
                            objPESV.insertPartnerEvtSelVst()
                        Else
                            objPESV.updatePartnerEvtSelVstWRTLogID()
                        End If
                    Else
                        objPESV.deletePartnerEvtSelVst()
                    End If
                    objPESV = Nothing
                Next

                For idx As Integer = 0 To dlstEduLvl.Items.Count - 1
                    Dim objPESE As New clsPartnerEvtSelEdu
                    Dim hdnEduLvl As HiddenField = CType(dlstEduLvl.Items(idx).FindControl("hdnEduLvl"), HiddenField)
                    Dim txtEduLvl As TextBox = CType(dlstEduLvl.Items(idx).FindControl("txtEduLvl"), TextBox)
                    objPESE.PartnerEvtSvcLogID = strLogID
                    objPESE.PartnerEvenEduLvlTxtID = hdnEduLvl.Value
                    objPESE.PartnerEvtEduVal = txtEduLvl.Text
                    If txtEduLvl.Text <> "" Then
                        If objPESE.funCheckRecord() = False Then
                            objPESE.insertPartnerEvtSelEdu()
                        Else
                            objPESE.updatePartnerEvtSelEduWRTLogID()
                        End If
                    Else
                        objPESE.deletePartnerEvtSelEdu()
                    End If
                    objPESE = Nothing
                Next

                Dim objPES As New clsPartnerEvtSector
                objPES.PartnerEvtSvcLogID = strLogID
                objPES.getPartnerEvtSectorWithRefToLogID()
                objPES.PartnerEvtSectorVal = txtSector.Text
                If objPES.funCheckRecord = False Then
                    objPES.insertPartnerEvtSector()
                Else
                    objPES.updatePartnerEvtSector()
                End If
                objPES = Nothing

                Session.Add("Msg", CMPartnerEventUpdatedSuccessfully)
                Response.Redirect("AddEditCustomer.aspx?PartnerID=" & strPartnerID)
                Exit Sub
            Else
                'Insert Section
                If objPEvtLog.insertPartnerEventLogs(strLogID) = True Then 'Insert Partner

                    For idx As Integer = 0 To dlstVisitors.Items.Count - 1
                        Dim hdnAD As HiddenField = CType(dlstVisitors.Items(idx).FindControl("hdnAD"), HiddenField)
                        Dim txtAD As TextBox = CType(dlstVisitors.Items(idx).FindControl("txtAD"), TextBox)
                        If txtAD.Text <> "" Then
                            Dim objPESV As New clsPartnerEvtSelVst
                            objPESV.PartnerEvtSvcLogID = strLogID
                            objPESV.PartnerEventVistorTxtID = hdnAD.Value
                            objPESV.PartnerEvtVstVal = txtAD.Text
                            objPESV.insertPartnerEvtSelVst()
                            objPESV = Nothing
                        End If
                    Next
                    For idx As Integer = 0 To dlstEduLvl.Items.Count - 1
                        Dim hdnEduLvl As HiddenField = CType(dlstEduLvl.Items(idx).FindControl("hdnEduLvl"), HiddenField)
                        Dim txtEduLvl As TextBox = CType(dlstEduLvl.Items(idx).FindControl("txtEduLvl"), TextBox)
                        If txtEduLvl.Text <> "" Then
                            Dim objPESE As New clsPartnerEvtSelEdu
                            objPESE.PartnerEvtSvcLogID = strLogID
                            objPESE.PartnerEvenEduLvlTxtID = hdnEduLvl.Value
                            objPESE.PartnerEvtEduVal = txtEduLvl.Text
                            objPESE.insertPartnerEvtSelEdu()
                            objPESE = Nothing
                        End If
                    Next
                    If txtSector.Text <> "" Then
                        Dim objPES As New clsPartnerEvtSector
                        objPES.PartnerEvtSvcLogID = strLogID
                        objPES.PartnerEvtSectorVal = txtSector.Text
                        objPES.insertPartnerEvtSector()
                        objPES = Nothing
                    End If
                    Session.Add("Msg", CMPartnerEventAddedSuccessfully)
                    Response.Redirect("AddEditCustomer.aspx?PartnerID=" & strPartnerID)
                    Exit Sub
                End If
            End If
        End If
    End Sub
End Class


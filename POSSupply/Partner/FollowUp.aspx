<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false" CodeFile="FollowUp.aspx.vb" Inherits="Partner_FollowUp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="Resources.Resource" %>
<asp:Content ID="cntFollowUp" ContentPlaceHolderID="cphMaster" Runat="Server">

<div align=center >
    <div align="right">
    <table width=750 class="table" style="border:0;">
    <tr>
        <td width=70% class="tdAlignLeft">
            <asp:UpdatePanel ID="upnlMsg" runat="server" UpdateMode=Always>
            <ContentTemplate>
                <asp:Label ID="lblMsg" runat=server ForeColor=green Font-Bold=true></asp:Label>&nbsp;&nbsp;
            <asp:Label ID="lblError" runat=server ForeColor=Red Font-Bold=true></asp:Label>
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>
        <td width=30% align="right" bgcolor="#FFFFFF" valign="top">
        <table style="border:0;" bgcolor="#FFFFFF" class="table" 
        id="tblModPart" runat="server" visible="false">
        <tr><td height=5></td></tr>
        <tr class="trheight">
            <td class="tdAlignLeft" valign="top">
                <asp:Label ID="lblCreated" CssClass="lblBold" runat="server"/><br />
                <asp:Label ID="lblCreatedDate" CssClass="lblSimple" runat="server"/><br />
                <asp:Label ID="lblCreatedBy" CssClass="lblSimple" runat="server"/>
            </td>
            <td width=10px>&nbsp;</td>
            <td class="tdAlignLeft" valign="top">
                <asp:Label ID="lblModified" CssClass="lblBold" runat="server"/><br />
                <asp:Label ID="lblModifiedDate" CssClass="lblSimple" runat="server"/><br />
                <asp:Label ID="lblModifiedBy" CssClass="lblSimple" runat="server"/>
            </td>
        </tr>
        </table>
    </td>
    </tr>
    </table>
    </div><br />
    <table width=750 class="table" >
    <tr>
        <td height=20 align=left class="titleBgColor" colspan=2>
            <asp:Label runat=server CssClass="lblBold" ID=lblTitle ></asp:Label>
        </td>
    </tr>
    <tr><td height=5></td></tr>
    <tr>
        
        <td colspan=2 width=30% align="right" bgcolor="#FFFFFF" valign="top">
        <table style="border:0;" bgcolor="#FFFFFF" class="table" id="tblCrMoPart" 
        runat="server" visible="false">
        <tr><td height=5></td></tr>
        <tr class="trheight">
            <td class="tdAlignLeft" valign="top">
                <asp:Label ID="lblCntCreated" CssClass="lblBold" runat="server"/><br />
                <asp:Label ID="lblCntCreatedDate" CssClass="lblSimple" runat="server"/><br />
                <asp:Label ID="lblCntCreatedBy" CssClass="lblSimple" runat="server"/>
            </td>
            <td width=10px>&nbsp;</td>
            <td class="tdAlignLeft" valign="top">
                <asp:Label ID="lblCntModified" CssClass="lblBold" runat="server"/><br />
                <asp:Label ID="lblCntModifiedDate" CssClass="lblSimple" runat="server"/><br />
                <asp:Label ID="lblCntModifiedBy" CssClass="lblSimple" runat="server"/>
            </td>
        </tr>
        </table>
    </td>
    </tr>
    <tr>
    <td align=left bgcolor="#FFFFFF" width=50% valign="top">
        <table width=100% style="border:0;" bgcolor="#FFFFFF" class="table">
        <tr><td height=5></td></tr>
        <tr class="trheight">
            <td width=28% class="tdAlign">
                <asp:Label ID="lblFollowUpText" CssClass="lblBold" 
                Text="<%$ Resources:Resource, lblCMFollowUpText%>" 
                runat="server"/>
            </td>
            <td class="tdAlignLeft" width=72%>
                <asp:TextBox ID="txtFollowUpText" runat="server" MaxLength="250" Width="200px" 
                TextMode=MultiLine Rows=4/>
            </td>
        </tr>
        <tr class="trheight">
            <td class="tdAlign">
                <asp:Label ID="lblFollowUpDate" CssClass="lblBold" 
                Text="<%$ Resources:Resource, lblCMFollowUpDate%>" runat="server"/><br />&nbsp;
            </td>
            <td class="tdAlignLeft">
                <asp:TextBox ID="txtFollowUpDateTime" runat="server" MaxLength="19" Width="150px"/>
                <asp:ImageButton ID="imgFollowUpDate" CausesValidation=false runat=server 
            ToolTip="Click to show calendar" ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" /> <span class="style1"> *</span><br /><%=lblCMFollowUpDateFormat %>
            <ajaxToolkit:CalendarExtender ID="CalendarExtender4" TargetControlID=txtFollowUpDateTime 
            runat="server" PopupButtonID="imgFollowUpDate" Format="MM/dd/yyyy">
            </ajaxToolkit:CalendarExtender>
                <asp:RequiredFieldValidator ID="reqvalFollowUpDateTime" runat="server" 
                ControlToValidate="txtFollowUpDateTime" 
                ErrorMessage="<%$ Resources:Resource, reqvalCMFollowUpDateTime%>" 
                SetFocusOnError=true Display="None">
                </asp:RequiredFieldValidator>
                <div style="display:none;">
                <asp:TextBox ID="txtDate" Enabled=true runat="server" Width="0px" MaxLength=19>
                </asp:TextBox>
                </div>
                <asp:CustomValidator ID="custvalFollowUpDate" runat="server" SetFocusOnError="true"
                 ErrorMessage="<%$ Resources:Resource, msgCMPlzEntFollowUpDateGTCurDate%>" Enabled=false 
                 ClientValidationFunction="funCheckFollowUpDate" Display="None" >
                 </asp:CustomValidator>
                <asp:CompareValidator ID="cmpvalFollowUpDate" EnableClientScript="true" SetFocusOnError="true"
                ControlToValidate="txtFollowUpDateTime" ControlToCompare="txtDate"
                 Operator="GreaterThanEqual" Type=Date Display="None" Enabled=false
                ErrorMessage="<%$ Resources:Resource, msgCMPlzEntFollowUpDateGTCurDate %>" runat="server" />
            </td>
        </tr>
        </table>
    </td>
    <td align="left" bgcolor="#FFFFFF" width=50% valign="top">
        <table width=100% style="border:0;" bgcolor="#FFFFFF" class="table">
        <tr><td height=5></td></tr>
        <tr class="trheight">
            <td width=28% class="tdAlign">
                <asp:Label ID="lblFollowUpEmail" CssClass="lblBold" 
                Text="<%$ Resources:Resource, lblCMFollowUpEmail%>" 
                runat="server"/>
            </td>
            <td class="tdAlignLeft" width=72%>
                <asp:TextBox ID="txtFollowUpEmail" runat="server" MaxLength="250" Width="200px" 
                TextMode=MultiLine Rows=4/>
            </td>
        </tr>
        <tr class="trheight">
            <td class="tdAlign">
                <asp:Label ID="lblFollowUpTime" CssClass="lblBold" 
                Text="<%$ Resources:Resource, lblCMFollowUpTime%>" runat="server"/><br />&nbsp;
            </td>
            <td class="tdAlignLeft">
                <asp:TextBox ID="txtFollowUpTime" runat="server" Width="150px" MaxLength="8" /> <span class="style1"> *</span><br /><%=lblCMFollowUpTimeFormat %>
                <ajaxtoolkit:AutoCompleteExtender ID="acExFollowUpTime" runat="server"
                  TargetControlID="txtFollowUpTime" MinimumPrefixLength="1" EnableCaching="true" 
                  ServicePath="~/FilterService.asmx" ServiceMethod="GetTime" >
                </ajaxtoolkit:AutoCompleteExtender>
                <asp:RequiredFieldValidator ID="reqvalFollowUpTime" runat="server" 
                ControlToValidate="txtFollowUpTime" 
                ErrorMessage="<%$ Resources:Resource, reqvalCMFollowUpTime%>" 
                SetFocusOnError=true Display="None">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        </table>
    </td>
    </tr>
    <tr class="trheight">
        <td align=right colspan=2 style="border-top:solid 1px #E6EDF5;padding-top:8px;padding-right:5px;">
            <asp:Button runat="server" ID="cmdSave" CssClass="imgSave"  />&nbsp;&nbsp;
            <asp:Button runat="server" ID="cmdReset" CssClass="imgReset" 
            CausesValidation="false" Visible="false"  />
            <asp:Button runat="server" ID="cmdBack" CssClass="imgBack" 
            CausesValidation="false"  />
        </td>
    </tr>
    </table><br /><br />
    <table width=750 class="table" id="tblCommunication" runat="server" cellpadding=0 cellspacing=0>
    <tr>
        <td class="titleBgColor" height=20 align="left">
            <asp:Label CssClass="lblBold" runat=server ID="lblTitleCommunication" 
             text="<%$ Resources:Resource, TitleCMFollowUp %>"></asp:Label>
        </td>
        <td class="titleBgColor" height=20 align="right">
            <asp:updateprogress runat="server" ID="upMaster" DisplayAfter="10" >
            <ProgressTemplate>
                <img src="../Images/wait22trans.gif" />
            </ProgressTemplate>
            </asp:updateprogress>
        </td>
        <td class="titleBgColor" align=right style="padding-right:5px;" >
            <asp:Button runat="server" ID="cmdAddCommunication" CssClass="imgAdd" Visible="false" />
        </td> 
    </tr>
    <tr><td height=2 colspan=3></td></tr>
    <tr>
        <td colspan=3 valign="top" align="left">
        <asp:UpdatePanel ID="upnlGridCommunication" runat="server" >
        <ContentTemplate>
            <asp:GridView ID="grdCommunication" runat="server" AllowSorting="True" 
             DataSourceID="sqldsCommunication" 
             EmptyDataText="<%$ Resources:Resource, CMNoDataFound %>"
             AllowPaging="True" PageSize="15" PagerSettings-Mode=Numeric CellPadding="3" 
             GridLines="both" AutoGenerateColumns="False"  ShowHeader="true" CssClass="table"
             UseAccessibleHeader="False" DataKeyNames="ContactComFollowUpID" width=745px>
                <Columns>
                    <asp:BoundField DataField="ComFollowUpDateTime"  
                    HeaderText="<%$ Resources:Resource, grdCMFollowUpDate %>"
                     ReadOnly="True" DataFormatString="{0: dd MMM yyyy HH:mm}" 
                     SortExpression="ComFollowUpDateTime" >
                        <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="PartnerLongName"  
                    HeaderText="<%$ Resources:Resource, grdCMPartnerName %>"
                     ReadOnly="True" SortExpression="PartnerLongName" >
                        <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ContactName" 
                    HeaderText="<%$ Resources:Resource, grdCMContactName %>" 
                    ReadOnly="True" SortExpression="ContactName">
                        <ItemStyle Width="300px" Wrap="true" HorizontalAlign="Left"/>
                    </asp:BoundField>
                    <asp:BoundField DataField="ComFollowUpText" 
                    HeaderText="<%$ Resources:Resource, grdCMFollowUpText %>" 
                    ReadOnly="True" SortExpression="ComFollowUpText">
                        <ItemStyle Width="300px" Wrap="true" HorizontalAlign="Left"/>
                    </asp:BoundField>
                    <asp:BoundField DataField="ComFollowUpEmail" 
                    HeaderText="<%$ Resources:Resource, grdCMFollowUpEmail %>" 
                    ReadOnly="True" SortExpression="ComFollowUpEmail">
                        <ItemStyle Width="300px" Wrap="true" HorizontalAlign="Left"/>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdCMDelete %>" visible=false 
                    HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign=Center>
                        <ItemTemplate>
                            <asp:ImageButton ID="imgDeleteContact" runat="server" 
                            ImageUrl="~/images/delete_icon.png"
                            CommandArgument='<%# Eval("ContactComFollowUpID") %>'
                            CommandName="Delete" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                    </asp:TemplateField>                
                </Columns>
                <FooterStyle CssClass="grdFooterStyle" />
                <RowStyle ForeColor="#333333" />
                <EditRowStyle BackColor="#999999" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <PagerStyle BackColor="#5F6062" ForeColor="White" HorizontalAlign="Left" 
                VerticalAlign="Middle" CssClass="lblBold"/>
                <HeaderStyle BackColor="#5F6062"  ForeColor="White" Font-Bold="true"
                HorizontalAlign="Left" VerticalAlign="Middle" Height="15" />
                <AlternatingRowStyle BackColor="#EDEDEE" ForeColor="#284775" />
                <PagerSettings PageButtonCount="20" />
            </asp:GridView>
            <asp:SqlDataSource ID="sqldsCommunication" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
            ProviderName="System.Data.Odbc">
            </asp:SqlDataSource>
            </ContentTemplate>
            <Triggers>
                
            </Triggers>
            </asp:UpdatePanel>
        </td>
    </tr> 
    </table>
    <asp:ValidationSummary ID="valsPartner" runat=server ShowMessageBox=true ShowSummary=false />
</div>
    <br /><br />
    <script language="javascript">
        
        
    </script>
</asp:Content>
 
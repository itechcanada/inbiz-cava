﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Partner_ChangeInstoreCreditValue : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security check for authorization
        if (!CurrentUser.IsInRole(RoleID.ACCOUNT))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        if (!IsPostBack)
        {
            if (this.CustID > 0)
                {
                    CustomerCredit objCustCrdt = new CustomerCredit();
                    txtAvailableCredit.Text = objCustCrdt.GetAvailableCredit(this.CustID).ToString("N2");
            }
            txtAvailableCredit.Focus();
        }
    }

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                if (this.CustID > 0)
                {
                    CustomerCredit objCustCrdt = new CustomerCredit();
                    double oldAvailableValue = objCustCrdt.GetAvailableCredit(this.CustID);

                    if (oldAvailableValue > BusinessUtility.GetDouble(txtAvailableCredit.Text))
                    {
                        
                        objCustCrdt.CustomerID = this.CustID;
                        objCustCrdt.CreditAmount = BusinessUtility.GetDouble((-1) * (oldAvailableValue - BusinessUtility.GetDouble(txtAvailableCredit.Text)));
                        objCustCrdt.CreditVia = 19;
                        objCustCrdt.RefundType = "C";
                        objCustCrdt.WhsCode = CurrentUser.UserDefaultWarehouse;

                        if (objCustCrdt.InsertHistory(null, CurrentUser.UserID, this.CustID,oldAvailableValue))
                        {
                            objCustCrdt.Insert(CurrentUser.UserID);
                        }

                        Globals.RegisterCloseDialogScript(Page, string.Format("parent.setInstoreCreditAmt({0});", objCustCrdt.GetAvailableCredit(this.CustID)), true);
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInstoreCreditLessThanEqual);
                        return;
                    }
                }
            }
            catch
            {

            }
        }
    }

    private int CustID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["custID"]);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
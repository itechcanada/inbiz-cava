Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Partial Class Partner_ViewMessage
    Inherits BasePage

    ' On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If

        lblMsg.Text = ""
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If

        lblTitle.Text = TitleCMCommunication

        If Not Page.IsPostBack Then
            subPopulateControls()
        End If
    End Sub
    ' Populate Controls
    Private Sub subPopulateControls()
        If Convert.ToString(Request.QueryString("CommID")) <> "" Then
            Dim objCC As New clsContactCommunication
            objCC.ComID = Convert.ToString(Request.QueryString("CommID"))
            objCC.getContactCommunicationInfo()
            Select Case objCC.ComType
                Case 1, 4
                    txtEmailMessage.Text = objCC.ComEmailMessage
                Case Else
                    txtEmailMessage.Text = objCC.ComServiceLog
            End Select
        End If
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
End Class

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SOHistory.ascx.cs" Inherits="Partner_UserControls_SOHistory" %>
<h3>
    <asp:Label ID="lblGuestHistory" Text="Guest Personal Information" runat="server" />
</h3>
<table border="0" cellpadding="5" cellspacing="5">
    <tr>
        <td>
            <asp:Label ID="lblGuestName" Text="Guest Name" runat="server" 
                Font-Bold="True" />
        </td>
        <td>
            <asp:Label ID="valGuestName" Text="" runat="server" />
        </td>
        <td>
            <asp:Label ID="lblPhone" Text="Phone #" runat="server" Font-Bold="True" />
        </td>
        <td>
            <asp:Label ID="valPhone" Text="" runat="server" />
        </td>
        <td>
            <asp:Label ID="lblStatus" Text="Status" runat="server" Font-Bold="True" />
        </td>
        <td>
            <asp:Label ID="valStatus" Text="" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblRecentOrderDate" Text="Recent Order Date" runat="server" 
                Font-Bold="True" />
        </td>
        <td>
            <asp:Label ID="valRecentOrderDate" Text="" runat="server" />
        </td>
        <td>
            <asp:Label ID="lblEmail" Text="Email" runat="server" Font-Bold="True" />
        </td>
        <td>
            <asp:Label ID="valEmail" Text="" runat="server" />
        </td>
        <td>
            <asp:Label ID="lblSex" Text="Sex" runat="server" Font-Bold="True" />
        </td>
        <td>
            <asp:Label ID="valSex" Text="" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblAddress" Text="Address" runat="server" Font-Bold="True" />
        </td>
        <td>
            <asp:Label ID="valAddress" Text="" runat="server" />
        </td>
        <td>
            <asp:Label ID="lblCity" Text="City" runat="server" Font-Bold="True" />
        </td>
        <td>
            <asp:Label ID="valCity" Text="" runat="server" />
        </td>
        <td>
            <asp:Label ID="lblStateProvince" Text="State/Province" runat="server" 
                Font-Bold="True" />
        </td>
        <td>
            <asp:Label ID="valState" Text="" runat="server" />
        </td>
    </tr>
</table>
<h3>
    <asp:Label ID="lblSoHistory" Text="Sales History" runat="server" />
</h3>
<div id="grid_wrapper" style="width: 100%;">
    <trirand:JQGrid runat="server" ID="grdSales" 
        Height="300px" AutoWidth="True" oncellbinding="grdSales_CellBinding" 
        ondatarequesting="grdSales_DataRequesting" 
        ondatarequested="grdSales_DataRequested">
        <Columns>            
            <trirand:JQGridColumn DataField="OrderID" HeaderText="<%$ Resources:Resource, grdSOOrderNo %>"
                TextAlign="Center" />    
            <trirand:JQGridColumn DataField="OrderDate" HeaderText="Order Date"
                Editable="false" TextAlign="Center" DataFormatString="{0:MM/dd/yyyy}" />        
            <trirand:JQGridColumn DataField="NoOfBeds" HeaderText="# of Beds"
                Editable="false" TextAlign="Center" />   
            <trirand:JQGridColumn DataField="NoOfNights" HeaderText="# of Nights" Editable="false"  TextAlign="Center"/>
            <trirand:JQGridColumn DataField="Building" HeaderText="Building" Editable="false" TextAlign="Left" />
            <trirand:JQGridColumn DataField="Beds" HeaderText="Beds" Editable="false" TextAlign="Left" />
            <trirand:JQGridColumn DataField="CheckInDate" HeaderText="Check-in Date" Editable="false" TextAlign="Center" DataFormatString="{0:MM/dd/yyyy}" />               
            <trirand:JQGridColumn DataField="OrderAmount" HeaderText="Order Amount"
                Editable="false" TextAlign="Right" DataFormatString="{0:F}" />
            <trirand:JQGridColumn DataField="ReceivedAmount" HeaderText="Received Amount" Editable="false" TextAlign="Right" DataFormatString="{0:F}" />
            <trirand:JQGridColumn DataField="Balance" HeaderText="Balance" Editable="false" TextAlign="Right" DataFormatString="{0:F}" />
            <trirand:JQGridColumn DataField="OrderID" HeaderText="<%$ Resources:Resource, grdSOOrderNo %>"
                PrimaryKey="True" TextAlign="Center" Visible="false" /> 
        </Columns>
        <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
            ShowDeleteButton="false" ShowSearchButton="false" />
        <SortSettings InitialSortColumn="CheckInDate" InitialSortDirection="Desc"></SortSettings>
        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" ShowFooter="true" />
        <ClientSideEvents LoadComplete="loadComplete" />
    </trirand:JQGrid>
    <asp:SqlDataSource ID="sqldsSalesOrderView" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
        ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
</div>

<script type="text/javascript">
    //Variables
    var gridID = "<%=grdSales.ClientID %>";
    $grd = $("#<%=grdSales.ClientID %>");    

    //Function To Resize the grid
    function resize_the_grid() {
        $grd.fluidGrid({ example: '#grid_wrapper', offset: -0 });
    }
    
    //Call Grid Resizer function to resize grid on page load.
    resize_the_grid();
    //Bind window.resize event so that grid can be resized on windo get resized.
    $(window).resize(resize_the_grid);

    function loadComplete() {
        resize_the_grid();
    }
</script>

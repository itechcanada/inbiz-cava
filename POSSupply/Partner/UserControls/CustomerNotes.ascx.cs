﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Newtonsoft.Json;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Partner_UserControls_CustomerNotes : BaseUserControl, ICustomer
{
    Partners objPart = new Partners();
    public clsDataClass objDataclass = new clsDataClass();
    private clsleadLog objLead = new clsleadLog();
    private clsOrders objSO = new clsOrders();
    private clsAccountReceivable objActRcv = new clsAccountReceivable();
    private clsCollectionLog objCL = new clsCollectionLog();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Initialize()
    {
        subSaleActivityFillGrid();
        subSOFillGrid();
        subARFillGrid();
        subColFillGrid();

        if (!IsPagePostBack(grdArHistory, grdCollectionNotes, grdOrderNotes, grdSalesActivity))
        {
            if (this.PartnerID > 0)
            {
                objPart.PopulateObject(this.PartnerID);
                lblCustomerName.Text = objPart.PartnerLongName;
                lblNotes.Text = objPart.PartnerNote;
            }
        }                                      
    }

    public int PartnerID
    {
        get
        {
            int custid = 0;
            int.TryParse(Request.QueryString["custid"], out custid);
            return custid;
        }
    }   
    
   
    //Populate Grid
    public void subSaleActivityFillGrid()
    {
        objLead.AssignLeadsID = this.PartnerID.ToString();
        sqldsLeadsLog.SelectCommand = objLead.funFillGrid();
        //fill Orders
    }
    //Populate Grid
    public void subSOFillGrid()
    {
        sqldsOrder.SelectCommand = objSO.funFillGridForAllOrders("", "", "");
        //fill Oorders
    }

    protected void grdLeadsLog_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        subSaleActivityFillGrid();
    }
    //On Row Data Bound
    protected void  grdLeadsLog_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        if ((e.Row.RowType == DataControlRowType.DataRow))
        {
            Label lblLog = (Label)e.Row.FindControl("lblLog");
            lblLog.Text = ((Label)e.Row.FindControl("lblLog")).Text;
            lblLog.Text = lblLog.Text.Replace(Environment.NewLine, "<br />");
        }
    }
    //Populate Grid
    public void subARFillGrid()
    {
        sqldsAR.SelectCommand = objActRcv.funARNotes(this.PartnerID.ToString());
        //fill Orders
    }
    //Populate Grid
    public void subColFillGrid()
    {
        sqldsCol.SelectCommand = objCL.funFillGrid(this.PartnerID.ToString());
        //fill Orders
    }

    protected void grdCol_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        subColFillGrid();
    }
    //On Row Data Bound
    protected void grdCol_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        if ((e.Row.RowType == DataControlRowType.DataRow))
        {
            Label lblLog = (Label)e.Row.FindControl("lblLog");
            lblLog.Text = ((Label)e.Row.FindControl("lblLog")).Text;
            lblLog.Text = lblLog.Text.Replace(Environment.NewLine, "<br />");
        }
    }      
}
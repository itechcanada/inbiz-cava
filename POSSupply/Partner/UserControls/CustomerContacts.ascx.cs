﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Trirand.Web.UI.WebControls;

using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Partner_UserControls_CustomerContacts : System.Web.UI.UserControl, ICustomer
{
    PartnerContacts pContacts = new PartnerContacts();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && grdQuantity.AjaxCallBackMode == AjaxCallBackMode.None)
        {
            txtFirstName.Focus();
        }
    }

    public void Initialize()
    {
        try
        {
            if (!IsPostBack && grdQuantity.AjaxCallBackMode == AjaxCallBackMode.None)
            {
                DropDownHelper.FillDropdown(ddlContactType, "CU", "Ctyp", Globals.CurrentAppLanguageCode, null);
                ltSectionTitle.Text = Resources.Resource.lblCustomerContacts;
            }
        }
        catch
        {
            throw;
        }
    }

    private void SetData()
    {
        pContacts.ContactPartnerID = this.PartnerID;
        pContacts.ContactSalutation = txtSalutation.Text;
        pContacts.ContactLastName = txtLastName.Text;
        pContacts.ContactFirstName = txtFirstName.Text;
        pContacts.ContactTitleFr = txtContactTitleFr.Text;
        pContacts.ContactTitleEn = txtContactTitleEn.Text;
        pContacts.ContactPhone = txtTelephone.Text;
        pContacts.ContactPhoneExt = txtPhoneExt.Text;
        pContacts.ContactEmail = txtEmailAddress.Text;
        pContacts.ContactSecEmail = txtEmailAddress2.Text;
        pContacts.ContactFax = txtFax.Text;
        pContacts.ContactDOB = BusinessUtility.GetDateTime(txtDOB.Text, DateFormat.MMddyyyy);
        pContacts.ContactSex = ddlGender.SelectedItem.Value;
        pContacts.ContactLangPref = rblstLangPref.SelectedItem.Value;
        pContacts.ContactHomePhone = txtHomePhone.Text;
        pContacts.ContactCellPhone = txtCellPhone.Text;
        pContacts.ContactActive = rblstIsActive.SelectedValue == "1";
        pContacts.ContactPrimary = rblstContactPrimary.SelectedValue == "1";
        pContacts.ContactNote = txtNotes.Text;
        pContacts.IsSubscribeEmail = chkIsSubscribeMail.Checked == true;
        pContacts.FaceBook = txtFacebook.Text;
        pContacts.Twitter = txtTiwtter.Text;
        pContacts.LinkedIn = txtLinkedIn.Text;
        pContacts.ContactType = ddlContactType.SelectedItem.Value;
    }

    public int PartnerID
    {
        get
        {
            int custid = 0;
            int.TryParse(Request.QueryString["custid"], out custid);
            return custid;
        }
    }

    private StatusGuestType GuestType
    {
        get
        {
            return QueryStringHelper.GetGuestType("gType");
        }
    }

    private string PartnerType
    {
        get
        {
            return QueryStringHelper.GetPartnerType("pType");
        }
    }

    private int PartnerTypeID
    {
        get
        {
            switch (this.PartnerType)
            {
                case StatusCustomerTypes.END_CLINET:
                    return (int)PartnerTypeIDs.EndClient;
                default:
                    return (int)PartnerTypeIDs.Distributer;
            }
        }
    }

    public string AddressRef
    {
        get
        {
            switch (this.PartnerType)
            {
                case StatusCustomerTypes.END_CLINET:
                    return AddressReference.END_CLIENT;
                default:
                    return AddressReference.DISTRIBUTER;
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                if (txtFirstName.Text == "")
                    return;

                SetData();
                if (this.PartnerID > 0)
                {
                    if (BusinessUtility.GetInt(hdnIdToEdit.Value) == 0)
                    {
                        if (pContacts.Insert(CurrentUser.UserID) == true)
                        {
                            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.lblContactSaved);
                            //Response.Redirect(Request.Url.AbsoluteUri);
                            ResetPage();
                        }
                        else
                        {
                            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblContactFailure);
                        }
                    }
                    else
                    {
                        pContacts.ContactID = BusinessUtility.GetInt(hdnIdToEdit.Value);
                        if (pContacts.Update(CurrentUser.UserID) == true)
                        {
                            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.lblContactSaved);
                            ResetPage();
                        }
                        else
                        {
                            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblContactFailure);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == CustomExceptionCodes.DUPLICATE_KEY)
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgEmailIDAlreadyExists);
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
                }
            }
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.Url.AbsoluteUri);
    }

    private void ResetPage()
    {
        hdnIdToEdit.Value = "0";
        txtFirstName.Text = "";
        txtLastName.Text = "";
        txtDOB.Text = "";
        txtCellPhone.Text = "";
        txtPhoneExt.Text = "";
        txtTelephone.Text = "";
        txtHomePhone.Text = "";
        txtEmailAddress.Text = "";
        txtEmailAddress2.Text = "";
        txtSalutation.Text = "";
        txtFacebook.Text = "";
        txtLinkedIn.Text = "";
        txtNotes.Text = "";
        txtTiwtter.Text = "";
        txtContactTitleFr.Text = "";
        txtContactTitleEn.Text = "";
        txtFax.Text = "";
        rblstIsActive.SelectedValue = "1";
        rblstLangPref.SelectedValue = "fr";
        rblstContactPrimary.SelectedValue = "0";
        ddlGender.SelectedValue = "M";
        ddlContactType.SelectedIndex = 0;
        chkIsSubscribeMail.Checked = false;
    }

    protected void grdQuantity_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        //Check if reuqest is for deleting record
        if (Request.QueryString.AllKeys.Contains("deleteItem") && Request.QueryString["deleteItem"] == "1")
        {
            int idtoDelete = 0;
            if (int.TryParse(Request.QueryString["idToDelete"], out idtoDelete) && idtoDelete > 0)
            {
                ProductQuantity prdQty = new ProductQuantity();
                try
                {
                    prdQty.Delete(idtoDelete);
                }
                catch
                {

                }
            }
        }
        grdQuantity.DataSource = pContacts.GetContactList(this.PartnerID, Globals.CurrentAppLanguageCode);
    }

    protected void grdQuantity_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        ////int editColIndex = 8;
        ////int deleteColIndex = 9;
        ////int prdIDColIndex = 0;

        //int dsPrdIdIndex = 1;
        //int dsWshIndex = 2;
        //if (e.ColumnIndex == 5) //Intransit Pop Setup
        //{
        //    double inTransQty = BusinessUtility.GetDouble(e.CellHtml); //_prdQty.GetInTransitQty(BusinessUtility.GetInt(e.RowValues[dsPrdIdIndex]), BusinessUtility.GetString(e.RowValues[dsWshIndex]));
        //    if (inTransQty > 0)
        //    {
        //        e.CellHtml = string.Format(@"<a class=""pop_intransit""  href=""IntransitQtyModal.aspx?PrdID={0}&whscode={1}"" >{2}</a>", e.RowValues[dsPrdIdIndex], e.RowValues[dsWshIndex], inTransQty);
        //    }
        //    else
        //    {
        //        e.CellHtml = string.Format(@"<a  href=""javascript:void(0);"" >{0}</a>", inTransQty);
        //    }
        //}
        //else if (e.ColumnIndex == 7) //Tags popup setup
        //{
        //    e.CellHtml = string.Format(@"<a class=""pop_tags""  href=""ProductTagsModal.aspx?PrdID={0}&whscode={1}"" >{2}</a>", e.RowValues[dsPrdIdIndex], e.RowValues[dsWshIndex], "View Tags");
        //}
        if (e.ColumnIndex == 7) //Edit popup setup
        {
            //e.CellHtml = string.Format(@"<a href=""Product.aspx?PrdID={0}&PQtyID={1}&ptype={2}&section={3}"" >Edit</a>", e.RowValues[dsPrdIdIndex], e.RowKey, Request.QueryString["ptype"], Request.QueryString["section"]);
            e.CellHtml = "<a href='#' >"+ Resources.Resource.edit +"</a>" ;
            //e.CellHtml = Resources.Resource.edit;
        }
        //else if (e.ColumnIndex == 9) //Delete popup setup
        //{
        //    //string delUrl = string.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_PRODUCT_QTY, e.RowKey, "reloadGrid");
        //    e.CellHtml = string.Format(@"<a class=""pop_delete"" href=""javascript:;"" onclick=""deleteQuantity({0});"">{1}</a>", e.RowKey, Resources.Resource.delete);
        //}
    }



}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerActivities.ascx.cs" Inherits="Partner_UserControls_CustomerActivities" %>

<h3>
    <asp:Label ID="Label1" Text="<%$ Resources:Resource, TitleAddLog %>" runat="server" />
</h3>
<ul class="form">
    <li>
        <div class="lbl">
            <asp:Label ID="lblCAFollowupDateTime" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblFollowupDate %>">
            </asp:Label></div>
        <div class="input">
            <asp:TextBox ID="txtFollowupDateTime" CssClass="datepicker" Enabled="true" runat="server"
                Width="90px" MaxLength="20">
            </asp:TextBox>
            <%=Resources.Resource.lblPODateFormat%>
            <asp:RequiredFieldValidator ID="reqvalAmtRcvdDateTime" runat="server" ControlToValidate="txtFollowupDateTime"
                ErrorMessage="<%$ Resources:Resource, msgCAPlzEntCAFollowupDate %>" SetFocusOnError="true"
                Display="None">
            </asp:RequiredFieldValidator>
        </div>
        <div class="clearBoth">
        </div>
    </li>
    <li>
        <div class="lbl">
            <asp:Label ID="Label3" CssClass="lblBold" Text="<%$ Resources:Resource, lblFollowupTime%>"
                runat="server" />
        </div>
        <div class="input">
            <asp:TextBox ID="txtHour" runat="server" Width="40"></asp:TextBox>
            <asp:TextBox ID="txtMin" runat="server" Width="40"></asp:TextBox>
            <asp:DropDownList runat="Server" ID="ddlTime" Width="45">
                <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
            </asp:DropDownList>
            (hh:mm)<span class="style1"> *</span>
            <asp:RequiredFieldValidator ID="reqValHour" runat="server" ControlToValidate="txtHour"
                ErrorMessage="<%$ Resources:Resource, reqValHour %>" SetFocusOnError="true" Display="None"> </asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgVHour" runat="server" ControlToValidate="txtHour" Type="Integer"
                ErrorMessage="<%$ Resources:Resource, rgVHour%>" MaximumValue="12" MinimumValue="1"
                Display="None"></asp:RangeValidator>
            <asp:RangeValidator ID="rgVMin" runat="server" ControlToValidate="txtMin" Type="Integer"
                ErrorMessage="<%$ Resources:Resource, rgVMin%>" MaximumValue="59" MinimumValue="0"
                Display="None"></asp:RangeValidator>
        </div>
        <div class="clearBoth">
        </div>
    </li>
    <li>
        <div class="lbl">
            <asp:Label ID="lblARNote" CssClass="lblBold" Text="<%$ Resources:Resource, lblARNote%>"
                runat="server" /></div>
        <div class="input">
            <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="9" Width="250px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNote"
                ErrorMessage="Notes Required!" SetFocusOnError="true" Display="None"> </asp:RequiredFieldValidator>
            </div>
        <div class="clearBoth">
        </div>
    </li>
    <li>
        <div class="lbl">
            <asp:Label ID="Label2" CssClass="lblBold" Text="<%$ Resources:Resource, FollowUpAssignedTo%>"
                runat="server" /></div>
        <div class="input">
            <asp:CheckBoxList ID="chklstSalesRep" runat="server" RepeatDirection="vertical">
            </asp:CheckBoxList>
        </div>
        <div class="clearBoth">
        </div>
    </li>
</ul>
<div class="div_command">
    <asp:ValidationSummary ID="valsAddlog" runat="server" ShowMessageBox="true" ShowSummary="false" />
    <asp:Button ID="btnAddLog" Text="<%$Resources:Resource, cmdColAddLog%>" 
        runat="server" onclick="btnAddLog_Click" />
</div>	                                       
<br />
<h3>
    <asp:Label ID="Label4" Text="<%$Resources:Resource, lblFollowupLogDetails%>" runat="server" />
</h3>   
<div id="grdWrapper">
        <trirand:JQGrid runat="server" ID="grdActivityLog" DataSourceID="sqldsActivityLog"
            Height="150px" AutoWidth="True" oncellbinding="grdActivityLog_CellBinding">
            <Columns>
                <trirand:JQGridColumn DataField="PartnerActivityId" Visible="false"
                    PrimaryKey="True" Width="100" />
                <trirand:JQGridColumn DataField="PartnerId" HeaderText="<%$ Resources:Resource, grdCMPartnerID %>"
                   Width="100" />
                <trirand:JQGridColumn DataField="FollowUpDate" HeaderText="<%$ Resources:Resource, grdCAFollowupDateTime %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="ActivityCreatedDate" HeaderText="<%$ Resources:Resource, grdHDActivityCreateDate %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="UserName" HeaderText="<%$ Resources:Resource, grdCreatedPerson %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="LogText" HeaderText="<%$ Resources:Resource, grdCAFollowupLogText %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="PartnerId" HeaderText="<%$ Resources:Resource, FollowUpAssignedTo %>"
                    Editable="false" />
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <ClientSideEvents LoadComplete="resize_the_grid" />
        </trirand:JQGrid>
        <asp:SqlDataSource ID="sqldsActivityLog" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            ProviderName="System.Data.Odbc"></asp:SqlDataSource>
    </div>             
<script type="text/javascript">
    //Function To Resize the grid
    function resize_the_grid() {
        $("#<%=grdActivityLog.ClientID%>").fluidGrid({ example: "#grdWrapper", offset: -0 });       
    }

    //Call Grid Resizer function to resize grid on page load.
    $(document).ready(resize_the_grid);

    //Bind window.resize event so that grid can be resized on windo get resized.
    $(window).resize(resize_the_grid);
    window.onload = resize_the_grid;
</script>
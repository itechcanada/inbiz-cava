﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Partner_UserControls_CRM_Menu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            blstNav.Items.Add(new ListItem(Resources.Resource.lblCustomerDetails, QueryString.GetModifiedUrl("section", ((int)CRMSectionKey.CustomerDetails).ToString())));
            if (this.PartnerID > 0)
            {
                if (this.PartnerType == StatusCustomerTypes.DISTRIBUTER)
                {
                    blstNav.Items.Add(new ListItem(Resources.Resource.lblCustomerContacts, QueryString.GetModifiedUrl("section", ((int)CRMSectionKey.CustomerDetails).ToString())));
                }
                if (CurrentUser.IsInRole(RoleID.SALES_MANAGER) || CurrentUser.IsInRole(RoleID.QUOTATION_APPROVER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
                {
                    if (this.GuestType == StatusGuestType.CourseParticipants || this.GuestType == StatusGuestType.Guest ||
                        this.GuestType == StatusGuestType.SpecialGuest || this.GuestType == StatusGuestType.Staff)
                    {
                        blstNav.Items.Add(new ListItem(Resources.Resource.lblSOHistory, QueryString.GetModifiedUrl("section", ((int)CRMSectionKey.CustomerSoHistory).ToString())));
                    }
                    else
                    {
                        blstNav.Items.Add(new ListItem(Resources.Resource.lblSOHistory, string.Format("~/Sales/ViewSalesOrder.aspx?PartnerID={0}&returnUrl={1}", this.PartnerID, Server.UrlEncode(Request.RawUrl))));
                    }
                }
                blstNav.Items.Add(new ListItem(Resources.Resource.lblInvoiceHistory, string.Format("~/Invoice/ViewInvoice.aspx?PartnerID={0}&returnUrl={1}", this.PartnerID, Server.UrlEncode(Request.RawUrl))));
                blstNav.Items.Add(new ListItem(Resources.Resource.lblARHistory, string.Format("~/AccountsReceivable/ViewAccountsReceivable.aspx?CustName={0}&PartnerID={1}&returnUrl={2}", "", this.PartnerID, Server.UrlEncode(Request.RawUrl))));
                blstNav.Items.Add(new ListItem(Resources.Resource.lblCustomerNotes, QueryString.GetModifiedUrl("section", ((int)CRMSectionKey.CustomerNotes).ToString())));
                blstNav.Items.Add(new ListItem(Resources.Resource.lblCustomerActivities, QueryString.GetModifiedUrl("section", ((int)CRMSectionKey.CustomerActivity).ToString())));
                //blstNav.Items.Add(new ListItem(Resources.Resource.lblCustomerNotes, string.Format("~/partner/Notes.aspx?PartnerID={0}&returnUrl={1}", this.PartnerID, Server.UrlEncode(Request.RawUrl))));
                //blstNav.Items.Add(new ListItem(Resources.Resource.lblCustomerActivities, string.Format("~/Partner/AddEditFollowups.aspx?PartnerId={0}&returnUrl={1}", this.PartnerID, Server.UrlEncode(Request.RawUrl))));
                blstNav.Items.Add(new ListItem(Resources.Resource.lblViewCustomer, string.Format("~/Partner/ViewCustomer.aspx?pType={0}&gType={1}", this.PartnerType, (int)this.GuestType)));                
            }            
        }
        foreach (ListItem item in blstNav.Items)
        {
            Uri u = new Uri(Request.Url.GetLeftPart(UriPartial.Authority) + ResolveUrl(item.Value));
            NameValueCollection nvc = HttpUtility.ParseQueryString(u.Query);
            if (nvc.AllKeys.Contains("section") && BusinessUtility.GetInt(nvc["section"]) == this.SectionID)
            {
                item.Attributes["class"] = "active";
                break;
            }
        }
    }   

    private StatusGuestType GuestType
    {
        get
        {
            return QueryStringHelper.GetGuestType("gType");
        }
    }

    private string PartnerType
    {
        get
        {
            return QueryStringHelper.GetPartnerType("pType");
        }
    }

    public int PartnerID
    {
        get
        {
            int custid = 0;
            int.TryParse(Request.QueryString["custid"], out custid);
            return custid;
        }
    }

    private int SectionID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["section"], out id);
            return id;
        }
    }
}
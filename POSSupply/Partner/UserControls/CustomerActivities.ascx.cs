﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Newtonsoft.Json;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Partner_UserControls_CustomerActivities : BaseUserControl, ICustomer
{
    private Partners objCust = new Partners();
    private clsPartnerContacts objPrtCon = new clsPartnerContacts();
    private clsAddress objAdd = new clsAddress();
    private clsPartnerActivityLog objActivityLog = new clsPartnerActivityLog();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Initialize()
    {
        subFillGrid();
        if (!IsPagePostBack(grdActivityLog))
        {
            txtHour.Text = "12";
			txtMin.Text = "00";			
			if (this.PartnerID > 0) 
            {
                objCust.PopulateObject(this.PartnerID);
												
				
			}
			subFillSR();
			subFillGrid();
            txtFollowupDateTime.Text = BusinessUtility.GetDateTimeString(DateTime.Now, DateFormat.MMddyyyy);

            txtNote.Focus();
		}
       
    }

    // To Populate Sales Representatives
    private void subFillSR()
    {
        clsPartners objP = new clsPartners();
        objP.PopulateSR(chklstSalesRep);
        objP = null;
    }

    //Populate Grid
    public void subFillGrid()
    {
        objActivityLog.PartnerId = this.PartnerID.ToString();
        sqldsActivityLog.SelectCommand = objActivityLog.GetFillGridAllActivityByPartnerIdCommand();
        //fill Orders
    }

    public int PartnerID
    {
        get
        {
            int custid = 0;
            int.TryParse(Request.QueryString["custid"], out custid);
            return custid;
        }
    }

    //Populate Object
    private void subSetData()
    {
        objActivityLog.PartnerId = this.PartnerID.ToString();
        objActivityLog.FollowUpDate = BuildActivityDate();
        objActivityLog.ActivityCreatedBy = CurrentUser.UserID.ToString();
        objActivityLog.LogText = txtNote.Text;
        objActivityLog.ActivityCreatedDate = BusinessUtility.GetDateTimeString(DateTime.Now, "yyyy-MM-dd HH:mm:ss");
        objActivityLog.ActivityLastModified =BusinessUtility.GetDateTimeString(DateTime.Now, "yyyy-MM-dd HH:mm:ss");
        objActivityLog.ActivityLastModifyBy = CurrentUser.UserID.ToString();
    }

    private string BuildActivityDate()
    { 
        string dateTxt = txtFollowupDateTime.Text + " " + fundate(BusinessUtility.GetInt(txtHour.Text), ddlTime.SelectedValue) + ":" + funMin(BusinessUtility.GetInt(txtMin.Text)) + ":00";
        return BusinessUtility.GetDateTimeString(BusinessUtility.GetDateTime(dateTxt, "MM/dd/yyyy HH:mm:ss"), "yyyy-MM-dd HH:mm:ss"); 
    }

    public string fundate(int intHour, string strType)
    {
        int intValue = 0;
        if (strType == "PM")
        {
            if (intHour <= 11)
            {
                intValue = intHour + 12;
            }
            else
            {
                intValue = intHour;
            }
        }
        else if (strType == "AM")
        {
            if (intHour == 12)
            {
                intValue = 0;
            }
            else if (intHour == 11)
            {
                intValue = intHour;
            }
            if (intHour <= 9)
            {
                intValue = intHour;
            }
        }
        return intValue.ToString("00");
    }
    public string funMin(int intMin)
    {
        int intValue = 0;
        if (intMin <= 9)
        {
            if (intMin == 0)
            {
                intValue = intMin;
            }
            else
            {
                intValue = intMin;
            }
        }
        else
        {
            intValue = intMin;
        }
        return intValue.ToString("00");
    }

    protected void btnAddLog_Click(object sender, EventArgs e)
    {
        if (this.PartnerID > 0)
        {
            subSetData();

            int id = objActivityLog.InsertLog();
            if (id > 0)
            {
                GenerateAlert(id.ToString());
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgLogaddedsuccessfully);
                txtNote.Text = "";
                txtFollowupDateTime.Text = BusinessUtility.GetDateTimeString(DateTime.Now, DateFormat.MMddyyyy);                
                chklstSalesRep.ClearSelection();
            }            
        }
    }

    // Inserts Alert for TeleMarketing followup
    protected void GenerateAlert(string activityId)
    {
        clsAlerts objAlert = new clsAlerts();
        subSetAlertData(objAlert, CurrentUser.UserID.ToString());
        objAlert.insertAlerts();

        foreach (ListItem myItem in chklstSalesRep.Items)
        {
            if (myItem.Selected == true)
            {
                objActivityLog.AssingSalesRepresentative(activityId, myItem.Value);
                subSetAlertData(objAlert, myItem.Value);
                objAlert.insertAlerts();
            }
        }
    }
    //Set Data for Alert
    private void subSetAlertData(clsAlerts objA, string alertUserId)
    {
        clsPartners objPartner = new clsPartners();
        objA.AlertPartnerContactID = objActivityLog.PartnerId;
        // Partner ID
        objA.AlertComID = CurrentUser.UserID.ToString();
        objA.AlertDateTime = objActivityLog.FollowUpDate;
        objA.AlertUserID = alertUserId;
        //Followup userID
        string strNote = "";
        strNote += Resources.Resource.alertLblCMFollowup + "<br><br>";
        //alrtlblSalesActivityFollowupActivity & "<br><br>" 'You have a Sales-Activities Followup Activity.
        strNote += Resources.Resource.alrtlblPartnerNo + " " + objActivityLog.PartnerId + "<br>";
        //Partner No. 
        objPartner.PartnerID = objActivityLog.PartnerId;
        objPartner.getPartnersInfo();
        strNote += Resources.Resource.alrtlblCustmer + " " + objPartner.PartnerLongName + "<br>";
        //Customer:
        strNote += objActivityLog.LogText + "<br><br>";
        clsUser objUsr = new clsUser();
        objUsr.UserID = CurrentUser.UserID.ToString();
        objUsr.getUserInfo();
        strNote += Resources.Resource.alrtlblCreatedBy + " " + objUsr.UserSalutation + " " + objUsr.UserFirstName + " " + objUsr.UserLastName;
        //Created by:
        objUsr = null;
        objA.AlertNote = strNote;
        objA.AlertRefType = "CFL";
    }
    protected void grdActivityLog_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 5)
        {
            e.CellHtml = e.CellHtml.Replace(Environment.NewLine, "<br>");
        }
        else if (e.ColumnIndex == 6)
        {
            e.CellHtml = objActivityLog.GetAllAssignedRepresentatives(e.RowKey.ToString());
        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerNotes.ascx.cs"
    Inherits="Partner_UserControls_CustomerNotes" %>
<h3>
    <asp:Literal ID="ltSectionTitle" Text="<%$Resources:Resource, prnNote%>" runat="server" />
</h3>
 <ul class="form">
    <li>
        <div class="lbl"><asp:Label ID="lblCustomerName1" Text="<%$ Resources:Resource, lblSOCustomerName %>"
                    CssClass="lblBold" runat="server" /></div>
        <div class="input"><asp:Label ID="lblCustomerName" CssClass="lblBold" runat="server" /></div>
        <div class="clearBoth"></div>
    </li>
    <li>
        <div class="lbl"><asp:Label ID="Label1" Text="<%$ Resources:Resource, lblCMNotes %>" CssClass="lblBold"
                    runat="server" /></div>
        <div class="input"><asp:Label ID="lblNotes" CssClass="lblBold" runat="server" /></div>
        <div class="clearBoth"></div>
    </li>
 </ul>   
 <br />
 <h3>
    <asp:Literal runat="server" ID="lblTitleLogDetails" Text="<%$Resources:Resource, lblSalesActivityFollowup %>"></asp:Literal>
 </h3>
  <div id="grdWrapper1">
     <trirand:JQGrid runat="server" ID="grdSalesActivity" 
         DataSourceID="sqldsLeadsLog" Height="150px"
         AutoWidth="True">
         <Columns>
             <trirand:JQGridColumn DataField="AssignLeadsID" HeaderText="<%$ Resources:Resource, grdCMcustomerID %>"
                 PrimaryKey="True" Width="100" />
             <trirand:JQGridColumn DataField="LeadsLogText" HeaderText="<%$ Resources:Resource, prnNote %>"
                 Editable="false" />            
         </Columns>
         <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
         <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
             ShowDeleteButton="false" ShowSearchButton="false" />
         <SortSettings InitialSortColumn=""></SortSettings>
         <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
         <ClientSideEvents  LoadComplete="resize_the_grid" />  
     </trirand:JQGrid>
     <asp:SqlDataSource ID="sqldsLeadsLog" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
 </div>
 <br />
<h3>
    <asp:Literal runat="server" ID="lblSo" Text="<%$Resources:Resource,lblSalesOrdernote%>"></asp:Literal>
</h3>
<div id="grdWrapper2">
     <trirand:JQGrid runat="server" ID="grdOrderNotes" DataSourceID="sqldsOrder" Height="150px"
         AutoWidth="True">
         <Columns>
             <trirand:JQGridColumn DataField="ordID" HeaderText="<%$ Resources:Resource, grdSOOrderNo %>"
                 PrimaryKey="True" Width="100" />
             <trirand:JQGridColumn DataField="ordComment" HeaderText="<%$ Resources:Resource, prnNote %>"
                 Editable="false" />            
         </Columns>
         <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
         <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
             ShowDeleteButton="false" ShowSearchButton="false" />
         <SortSettings InitialSortColumn=""></SortSettings>
         <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
         <ClientSideEvents  LoadComplete="resize_the_grid" />  
     </trirand:JQGrid>
     <asp:SqlDataSource ID="sqldsOrder" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
</div>
<br />
<h3>
    <asp:Literal runat="server" ID="lblAR" Text="<%$Resources:Resource, lblAccountRecieableHistory %>"></asp:Literal>
</h3>
<div id="grdWrapper3">
    <trirand:JQGrid runat="server" ID="grdArHistory" DataSourceID="sqldsAR" Height="150px"
         AutoWidth="True">
         <Columns>
             <trirand:JQGridColumn DataField="InvRefNo" HeaderText="<%$ Resources:Resource, grdINInvoiceNo %>"
                 PrimaryKey="True"  Width="100" />
             <trirand:JQGridColumn DataField="ARNote" HeaderText="<%$ Resources:Resource, prnNote %>"
                 Editable="false" />            
         </Columns>
         <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
         <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
             ShowDeleteButton="false" ShowSearchButton="false" />
         <SortSettings InitialSortColumn=""></SortSettings>
         <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />         
         <ClientSideEvents  LoadComplete="resize_the_grid" />  
     </trirand:JQGrid>
     <asp:SqlDataSource ID="sqldsAR" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
</div>
<br />
<h3>
    <asp:Literal runat="server" ID="lblCol" Text="<%$Resources:Resource,lblCollectionNote%>"></asp:Literal>
</h3>
<div id="grdWrapper4">
    <trirand:JQGrid runat="server" ID="grdCollectionNotes" DataSourceID="sqldsCol" Height="150px"
         AutoWidth="True">
         <Columns>
             <trirand:JQGridColumn DataField="InvRefNo" HeaderText="<%$ Resources:Resource, grdINInvoiceNo %>"
                 PrimaryKey="True" Width="100" />
             <trirand:JQGridColumn DataField="ColLogText" HeaderText="<%$ Resources:Resource, grdCAFollowupLogText %>"
                 Editable="false" />            
         </Columns>
         <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
         <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
             ShowDeleteButton="false" ShowSearchButton="false" />
         <SortSettings InitialSortColumn=""></SortSettings>
         <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" /> 
         <ClientSideEvents  LoadComplete="resize_the_grid" />        
     </trirand:JQGrid>
     <asp:SqlDataSource ID="sqldsCol" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
</div>

<script type="text/javascript">    
    //Function To Resize the grid
    function resize_the_grid() {
        $("#<%=grdSalesActivity.ClientID%>").fluidGrid({ example: "#grdWrapper1", offset: -0 });
        $("#<%=grdOrderNotes.ClientID%>").fluidGrid({ example: "#grdWrapper2", offset: -0 });
        $("#<%=grdArHistory.ClientID%>").fluidGrid({ example: "#grdWrapper3", offset: -0 });
        $("#<%=grdCollectionNotes.ClientID%>").fluidGrid({ example: "#grdWrapper4", offset: -0 });       
    }

    //Call Grid Resizer function to resize grid on page load.
    $(document).ready(resize_the_grid);    

    //Bind window.resize event so that grid can be resized on windo get resized.
    $(window).resize(resize_the_grid);
    window.onload = resize_the_grid;
</script>


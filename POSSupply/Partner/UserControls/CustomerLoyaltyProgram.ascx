﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerLoyaltyProgram.ascx.cs" Inherits="Partner_UserControls_CustomerLoyaltyProgram" %>
 <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
<h3>
    <asp:Literal ID="ltSectionTitle" runat="server" />
</h3>
<asp:Panel ID="pnlGrid" runat="server">
    <table border="0" cellpadding="5" cellspacing="5">
        <tr>
            <td>
                <asp:Label ID="lblCustName" Text="<%$Resources:Resource,lblCustomerName %>" runat="server" Font-Bold="true" />
            </td>
            <td>
                <asp:Label ID="valCustName" Text="" runat="server" />
            </td>
            <td>
                <asp:Label ID="Label1" Text="<%$Resources:Resource,lblSmartGiveID %>" runat="server" Font-Bold="true" />
            </td>
            <td>
                <asp:Label ID="valSmartGivID" Text="" runat="server" />
            </td>
            <td>
                <asp:Label ID="lblPoints" Text="<%$Resources:Resource,lblPoints %>" runat="server" Font-Bold="true" />
            </td>
            <td>
                <asp:Label ID="valPoints" Text="" runat="server" />
            </td>
        </tr>
    </table>
    <div id="grid_wrapper" style="width: 100%;">
        <trirand:JQGrid runat="server" ID="grdLoyalty" Height="120px" AutoWidth="True" 
            oncellbinding="grdLoyalty_CellBinding" 
            ondatarequesting="grdLoyalty_DataRequesting" >
            <Columns>
                <trirand:JQGridColumn DataField="Points" HeaderText="<%$ Resources:Resource, lblPoints %>"
                    TextAlign="Center" />
                <trirand:JQGridColumn DataField="PointsAddedOn" HeaderText="<%$ Resources:Resource, lblAddedOn %>" Editable="false"
                    TextAlign="Center" DataFormatString="{0:MM/dd/yyyy}" />
                <trirand:JQGridColumn DataField="PointsAddedBy" HeaderText="<%$ Resources:Resource, lblAddedBy %>" Editable="false"
                    TextAlign="Center" />
                <trirand:JQGridColumn DataField="InvoiceNo" HeaderText="<%$ Resources:Resource, grdINInvoiceNo %>" Editable="false"
                    TextAlign="Center" />
                <trirand:JQGridColumn DataField="OrderNo" HeaderText="<%$ Resources:Resource, lblOrderNumber %>" Editable="false"
                    TextAlign="Left" />
                <trirand:JQGridColumn DataField="PosTransactionID" HeaderText="<%$ Resources:Resource, lblPosTransactionID %>" Editable="false" TextAlign="Left" />                
                <trirand:JQGridColumn DataField="LoyalHistoryID" HeaderText=""
                    PrimaryKey="True" TextAlign="Center" Visible="false" />
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />            
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" ShowFooter="true" />
            <ClientSideEvents LoadComplete="jqGridResize" />
        </trirand:JQGrid>        
    </div>
    <br />
    <div class="div_command">
        <asp:Button ID="btnAdd" Text="<%$ Resources:Resource, lblAdd %>" runat="server" CausesValidation="false" OnClientClick="return pointPopup('add');" />
        <asp:Button ID="btnRedeem" Text="<%$ Resources:Resource, lblRedeem %>" runat="server" CausesValidation="false" OnClientClick="return pointPopup('redeem');"/>
        <asp:HyperLink NavigateUrl="~/Common/mdManageLoyaltyPoints.aspx" runat="server" />
    </div>
</asp:Panel>
<script type="text/javascript">
    $grid = $("#<%=grdLoyalty.ClientID%>");
    $grid.initGridHelper({
        searchPanelID: "=SearchPanel.ClientID %>",
        searchButtonID: "btnSearch",
        gridWrapPanleID: "grid_wrapper"
    });

    function jqGridResize() {
        $("#<%=grdLoyalty.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
    }

    function pointPopup(opt) {
        var url = 'mdManageLoyaltyPoints.aspx';
        var queryData = {};
        queryData.partnerID = '<%=this.PartnerID%>'
        queryData.pointOption = opt;
        var t = "";
        var height = 0;
        if (opt == "add") {
            t = "<%=Resources.Resource.lblAdd%>";
            height = 550;
        }
        else {
            t = "<%=Resources.Resource.lblRedeem%>";
            height = 550;
        }

        var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
            title: t,
            loadingClass: "loading-image",
            modal: true,
            width: 620,
            height: height,
            autoOpen: false,
            closeOnEscape: true
        });
        $dialog.dialog('open');

        return false;
    }   
</script>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerDetails.ascx.cs"
    Inherits="Partner_UserControls_CustomerDetails" %>
<%@ Register Src="../../Controls/new/CustomFields.ascx" TagName="CustomFields" TagPrefix="uc1" %>
<h3>
    <asp:Literal ID="ltSectionTitle" runat="server" meta:resourcekey="ltSectionTitleResource1" />
</h3>
<asp:Panel ID="pnlCustomerInfo" runat="server" DefaultButton="btnSave">
    <asp:Panel ID="pnlPartnerHistory" runat="server">
        <table class="mGrid" border="0" cellpadding="0" cellspacing="0" style="width: 100%; border-collapse: collapse;">
            <tr class="grid_header">
                <td>
                    <asp:Label ID="Label9" Text="<%$Resources:Resource, grdvStatus%>" runat="server"
                        Font-Bold="true" />
                </td>
                <td>
                    <asp:Label ID="Label5" Text="<%$Resources:Resource, lblRecentOrder%>" runat="server"
                        Font-Bold="true" />
                </td>
                <td>
                    <asp:Label ID="Label16" Text="<%$Resources:Resource, lblInstoreCredit%>" runat="server"
                        Font-Bold="true" />
                </td>
                <td>
                    <asp:Label ID="lblhdgTotalSales" Text="<%$Resources:Resource, lblTotalSales%>"
                        runat="server" Font-Bold="true" />
                </td>
                <td>
                    <asp:Label ID="lblAnnualSales" Text="<%$Resources:Resource, lblAnnualSales%>" runat="server"
                        Font-Bold="true" />
                </td>
                <td id="tdHdgCrdt" runat="server" visible="false">
                    <asp:Label ID="Label6" Text="<%$Resources:Resource, lblAvailableCredit%>" runat="server"
                        Font-Bold="true" />
                </td>
                <td id="tdHdgAmtDue" runat="server" visible="false">
                    <asp:Label ID="Label7" Text="<%$Resources:Resource, lblAmountDue%>" runat="server"
                        Font-Bold="true" />
                </td>
                <td>
                    <asp:Label ID="Label8" Text="<%$Resources:Resource, lblPoints%>" runat="server" Font-Bold="true" />
                </td>
            </tr>
            <tr class="grid_rowstyle">
                <td>
                    <asp:Label ID="ltStatus" runat="server" />
                </td>
                <td>
                    <asp:HyperLink ID="hlRecentOrder" runat="server" />
                </td>
                <td>
                    <asp:HyperLink ID="ltInstoreCredit" runat="server" />
                    <%--<asp:HyperLink ID="hlIncrdHistory" runat="server" Text="ram" />--%>
                    <a id='showHistoryLink' runat="server" onclick='ShowInsCrdHistory();'>
                        <img src='../Images/HistroyImage.jpg' /></a>
                </td>
                <td>
                    <asp:Label ID="ltTotalSales" runat="server" />
                </td>
                <td>
                    <asp:Label ID="ltAnnualSales" runat="server" />
                </td>
                <td id="tdCrdt" runat="server" visible="false">
                    <asp:Label ID="ltAvailableCredit" runat="server" />
                </td>
                <td id="tdAmtDue" runat="server" visible="false">
                    <asp:Label ID="ltAmountDue" runat="server" />
                </td>
                <td>
                    <asp:Label ID="ltPoints" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ul class="form">
        <li>
            <asp:ValidationSummary ID="sumvalProduct" runat="server" ShowMessageBox="true" ShowSummary="false" />
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="Label4" Text="<%$ Resources:Resource, grdCMCustomerID %>" runat="server" />
            </div>
            <div class="input">
                <%--<asp:TextBox ID="txtCustActonym" runat="server" MaxLength="100" />--%>
                <asp:Label ID="lblPartID" Text="" Font-Bold="true" runat="server" />
                <asp:HiddenField ID="hdnPartID" runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liGuest1" runat="server">
            <div class="lbl">
                <asp:Label ID="lblLastName" Text="<%$Resources:Resource, lblLName %>" runat="server"
                    meta:resourcekey="lblLastNameResource1" />*
            </div>
            <div class="input">
                <asp:TextBox ID="txtLastName" runat="server" MaxLength="45" meta:resourcekey="txtLastNameResource1" />
                <asp:RequiredFieldValidator ID="rfvLastName" ControlToValidate="txtLastName" SetFocusOnError="True"
                    runat="server" ErrorMessage="<%$Resources:Resource, reqLastName %>" Text="*" meta:resourcekey="rfvLastNameResource1"></asp:RequiredFieldValidator>
            </div>
            <div class="lbl">
                <asp:Label ID="lblFirstName" Text="<%$Resources:Resource, lblFName%>" runat="server" /><%--*--%>
            </div>
            <div class="input">
                <asp:TextBox ID="txtFirstName" runat="server" MaxLength="45" meta:resourcekey="txtFirstNameResource1" />
                <%--<asp:RequiredFieldValidator ID="rfvFirstName" ControlToValidate="txtFirstName" SetFocusOnError="True"
                    runat="server" ErrorMessage="*" meta:resourcekey="rfvFirstNameResource1"></asp:RequiredFieldValidator>--%>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liDistributer1" runat="server">
            <div class="lbl">
                <asp:Label ID="lblCustName" Text="<%$ Resources:Resource, lblCMLongName %>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtPartnerFullName" runat="server" MaxLength="100" />
                <asp:RequiredFieldValidator ID="rfvFullPartnerName" ControlToValidate="txtPartnerFullName"
                    SetFocusOnError="True" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
            </div>
            <div class="lbl">
                <asp:Label ID="lblWebsite" Text="<%$Resources:Resource, lblWebsite%>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtWebsite" runat="server" meta:resourcekey="txtWebsiteResource1" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liGuest2" runat="server">
            <div class="lbl">
                <asp:Label ID="lblSpirtualName" Text="<%$Resources:Resource, lblSmartGiveID %>" runat="server" />
                <%--lblSpiritualName--%>
            </div>
            <div class="input">
                <asp:TextBox ID="txtSpritualName" runat="server" MaxLength="45" meta:resourcekey="txtSpritualNameResource1" />
            </div>
            <div class="lbl">
                <asp:Label ID="lblSex" Text="<%$Resources:Resource, lblSex %>" runat="server" meta:resourcekey="lblSexResource1" />*
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rblSex" runat="server" RepeatDirection="Horizontal" meta:resourcekey="rblSexResource1">
                    <asp:ListItem Value="M" Selected="True" Text="<%$Resources:Resource, lblMale%>"></asp:ListItem>
                    <asp:ListItem Value="F" Text="<%$Resources:Resource, lblFemale %>"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liGuest3" runat="server">
            <div class="lbl">
                <%-- <asp:Label ID="lblDOB" Text="<%$Resources:Resource, lblDateOfBirth%>" runat="server"
                    meta:resourcekey="lblDOBResource1" />--%>
            </div>
            <div class="input">
                <%-- <asp:TextBox ID="txtDOB" CssClass="dob_datepicker" runat="server" meta:resourcekey="txtDOBResource1" />--%>
            </div>
            <div class="lbl">
                <asp:Label ID="lblNationality" Text="<%$Resources:Resource, lblNationality%>" runat="server"
                    meta:resourcekey="lblNationalityResource1" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtNationality" runat="server" meta:resourcekey="txtNationalityResource1" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblTelephoneNumber" Text="<%$Resources:Resource,lblTelephone %>" runat="server" />*
            </div>
            <div class="input">
                <asp:TextBox ID="txtTelephone" runat="server" MaxLength="245" ph="999-999-9999" CssClass="placeholder" />
                <asp:CustomValidator ID="cumpTelephone" ControlToValidate="txtTelephone" ClientValidationFunction="validatePhoneFormat"
                    Display="Dynamic" runat="server" ErrorMessage="!" SetFocusOnError="true" ValidateEmptyText="true">
                </asp:CustomValidator>
                <asp:RequiredFieldValidator ID="rfvTelephone" ControlToValidate="txtTelephone" SetFocusOnError="True"
                    runat="server" ErrorMessage="<%$Resources:Resource, reqPhone %>" Text="*" meta:resourcekey="rfvTelephoneResource1"></asp:RequiredFieldValidator>
            </div>
            <div class="lbl">
                <asp:Label ID="lblAdditionalTelephone" Text="<%$Resources:Resource, lblAdditionalPhoneNo%>"
                    runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtPhone2" runat="server" MaxLength="15" ph="999-999-9999" CssClass="placeholder" />
                <asp:CustomValidator ID="cumpAdditionalPhone" ControlToValidate="txtPhone2" ClientValidationFunction="validateAdditionalPhoneFormat"
                    Display="Dynamic" runat="server" ErrorMessage="!" SetFocusOnError="true">
                </asp:CustomValidator>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblEmailAddress" Text="<%$Resources:Resource, lblEmail%>" runat="server"
                    meta:resourcekey="lblEmailAddressResource1" /><span id="spnMailReq" runat="server">*</span>
            </div>
            <div class="input">
                <asp:TextBox ID="txtEmailAddress" runat="server" MaxLength="245" meta:resourcekey="txtEmailAddressResource1" />
                <asp:RequiredFieldValidator ID="rfvEmailAddress" ControlToValidate="txtEmailAddress"
                    SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="rfvEmailAddressResource1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="rfvSecEmail" runat="server" ControlToValidate="txtEmailAddress"
                    ErrorMessage="<%$ Resources:Resource, revalCMSecEmail %>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    meta:resourcekey="rfvSecEmailResource1" Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
            <div class="lbl">
                <asp:Label ID="lblFax" Text="<%$Resources:Resource, lblFax %>" runat="server" meta:resourcekey="lblFaxResource1" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtFax" runat="server" meta:resourcekey="txtFaxResource1" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="li1" runat="server">
            <div class="lbl">
                <asp:Label ID="lblDOB" Text="<%$Resources:Resource, lblDateOfBirth%>" runat="server"
                    meta:resourcekey="lblDOBResource1" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtDOB" CssClass="dob_datepicker" runat="server" meta:resourcekey="txtDOBResource1" />
            </div>
            <div class="lbl">
                <asp:Label ID="Label12" Text="<%$Resources:Resource, lblGender%>" runat="server" />
            </div>
            <div class="input">
                <asp:DropDownList ID="ddlGender" runat="server">
                    <asp:ListItem Text="<%$Resources:Resource, lblMale%>" Value="M"></asp:ListItem>
                    <asp:ListItem Text="<%$Resources:Resource, lblFemale%>" Value="F" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="li2" runat="server">
            <div class="lbl">
                <asp:Label ID="Label13" Text="<%$Resources:Resource, lblFaceBook%>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtFacebook" runat="server" />
            </div>
            <div class="lbl">
                <asp:Label ID="Label14" Text="<%$Resources:Resource, lblTiwtter%>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtTiwtter" runat="server" MaxLength="350" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblEmailAddress2" Text="<%$Resources:Resource, lblEmail2%>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtEmailAddress2" runat="server" MaxLength="245" />
                <%--                <asp:RequiredFieldValidator ID="rfvEmailAddress" ControlToValidate="txtEmailAddress2"
                    SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="rfvEmailAddressResource1"></asp:RequiredFieldValidator>--%>
                <asp:RegularExpressionValidator ID="rfvSecEmail2" runat="server" ControlToValidate="txtEmailAddress2"
                    ErrorMessage="<%$ Resources:Resource, revalCMSecEmail %>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    meta:resourcekey="rfvSecEmailResource1" Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
            <div class="lbl">
                <asp:Label ID="Label21" Text="<%$Resources:Resource, lblCustomerSince%>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtCustSince" CssClass="dob_datepicker" runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="Label22" Text="<%$Resources:Resource, grdWarehouseCode%>" runat="server" />
            </div>
            <div class="input">
                <asp:DropDownList ID="dlWarehouse" runat="server" Width="180px" ValidationGroup="lstSrch">
                </asp:DropDownList>
            </div>
            <div class="lbl">
                &nbsp;
            </div>
            <div class="input">
                &nbsp;
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <h3>
                <%= Resources.Resource.lblAlert%>
            </h3>
        </li>
        <li id="li3" runat="server">
            <div class="lbl">
                <asp:Label ID="Label15" runat="server" Text="<%$Resources:Resource, lblAlert%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtAlert" runat="server" TextMode="MultiLine" MaxLength="10" onkeyup="return MaxLengthValdation(this.id, 200);" />
            </div>
            <div class="lbl">
                &nbsp;
            </div>
            <div class="input">
                &nbsp;
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liSizeHdg" runat="server">
            <h3>
                <%= Resources.Resource.lblProductSize%>
            </h3>
        </li>
        <li id="liClothDesc5" runat="server">
            <div class="lbl">
                <asp:Label ID="Label27" runat="server" Text="<%$Resources:Resource, lblLongueurLength%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtLength" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
            </div>
            <div class="lbl">
                <asp:Label ID="Label28" runat="server" Text="<%$Resources:Resource, lblEpauleShoulder%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtShoulder" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liClothDesc6" runat="server">
            <div class="lbl">
                <asp:Label ID="Label29" runat="server" Text="<%$Resources:Resource, lblBusteChest%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtChest" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
            </div>
            <div class="lbl">
                <asp:Label ID="Label30" runat="server" Text="<%$Resources:Resource, lblBicep%>" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtBicep" CssClass="numericTextField" runat="server" MaxLength="30"
                    Width="70px" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li style="display: none;">
            <div class="lbl">
                <asp:Label ID="lblInvoicePreference" Text="<%$ Resources:Resource, lblInvoicePreference %>"
                    runat="server" meta:resourcekey="lblInvoicePreferenceResource1" />
            </div>
            <div class="input">
                <asp:DropDownList ID="ddlInvoicePreference" CssClass="txtBoxPrd" runat="server" meta:resourcekey="ddlInvoicePreferenceResource1">
                    <asp:ListItem Selected="True" Value="E" meta:resourcekey="ListItemResource6">Email</asp:ListItem>
                    <asp:ListItem Value="M" meta:resourcekey="ListItemResource7">Mail</asp:ListItem>
                    <asp:ListItem Value="F" meta:resourcekey="ListItemResource8">Fax</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div id="divGuest1" style="float: left;" runat="server">
                <div class="lbl">
                    <asp:Label ID="lblGuestType" Text="<%$Resources:Resource, lblGuestType %>" runat="server"
                        meta:resourcekey="lblGuestTypeResource1" />
                </div>
                <div class="input">
                    <asp:DropDownList ID="rblIsSpecialGuest" runat="server" meta:resourcekey="rblIsSpecialGuestResource1">
                    </asp:DropDownList>
                </div>
                <div class="clearBoth">
                </div>
            </div>
            <div class="clearBoth">
            </div>
        </li>
    </ul>
    <asp:Panel ID="pnlAditionalInformation" runat="server">
        <h3>
            <asp:Literal ID="Literal1" Text="<%$Resources:Resource,lblAdditionalInformation %>"
                runat="server" />
        </h3>
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="lblSelectTaxGroup" Text="<%$ Resources:Resource, lblSelectTaxGroup %>"
                        runat="server" meta:resourcekey="lblSelectTaxGroupResource1" />
                </div>
                <div class="input">
                    <asp:DropDownList ID="dlTaxGroup" runat="server" meta:resourcekey="dlTaxGroupResource1">
                    </asp:DropDownList>
                    <%--<asp:RequiredFieldValidator ID="rfvTaxGroup" ControlToValidate="dlTaxGroup"
                    SetFocusOnError="True" runat="server" ErrorMessage="*" InitialValue="0" 
                    meta:resourcekey="rfvTaxGroupResource1"></asp:RequiredFieldValidator>--%>
                </div>
                <div class="lbl">
                    <asp:Label ID="lblDistCurrencyCode" Text="<%$ Resources:Resource, lblDistCurrencyCode %>"
                        runat="server" meta:resourcekey="lblDistCurrencyCodeResource1" />
                </div>
                <div class="input">
                    <asp:DropDownList ID="dlCurrencyCode" runat="server" meta:resourcekey="dlCurrencyCodeResource1">
                    </asp:DropDownList>
                    <%--<asp:RequiredFieldValidator ID="rfvCurrency" ControlToValidate="dlCurrencyCode"
                    SetFocusOnError="True" runat="server" ErrorMessage="*" InitialValue="0" 
                    meta:resourcekey="rfvCurrencyResource1"></asp:RequiredFieldValidator>--%>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblInvoiceNetTerms" Text="<%$Resources:Resource, lblInvoiceNetTerms%>"
                        runat="server" meta:resourcekey="lblInvoiceNetTermsResource1" />
                </div>
                <div class="input">
                    <%--<asp:TextBox ID="txtInvoiceNetTerms" runat="server" 
                    meta:resourcekey="txtInvoiceNetTermsResource1" />--%>
                    <asp:DropDownList ID="ddlNetTerms" runat="server" Enabled="true">
                    </asp:DropDownList>
                </div>
                <div class="lbl">
                    <asp:Label ID="lblLanguage" Text="<%$Resources:Resource, lblLanguage%>" runat="server"
                        meta:resourcekey="lblLanguageResource1" />
                </div>
                <div class="input">
                    <asp:DropDownList ID="ddlLang" runat="server" meta:resourcekey="ddlLangResource1">
                        <asp:ListItem Value="en" Text="<%$Resources:Resource, lblEnglish%>" />
                        <asp:ListItem Value="fr" Text="<%$Resources:Resource, lblFrench%>" />
                        <asp:ListItem Value="sp" Text="<%$Resources:Resource, Spanish%>" />
                    </asp:DropDownList>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="Label10" Text="<%$Resources:Resource, lblDiscPer%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtDiscPer" runat="server" MaxLength="4" CssClass="numericTextField" />
                    <asp:RangeValidator runat="server" ID="rngDate" ControlToValidate="txtDiscPer" Type="Integer"
                        MinimumValue="-99" MaximumValue="99" ErrorMessage="<%$Resources:Resource, errMsgDiscPer%>" />
                </div>
                <div class="lbl">
                    <asp:Label ID="Label11" Text="<%$Resources:Resource, lblCreditLimit%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtAvailableCredit" runat="server" MaxLength="13" CssClass="numericTextField" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li id="liLegacy" runat="server">
                <div class="lbl">
                    <asp:Label ID="lblLeagcyOpnAR" Text="<%$Resources:Resource, lblLeagcyOpnAR%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtLeagcyOpnAR" runat="server" MaxLength="20" />
                </div>
                <div class="lbl">
                    <asp:Label ID="lblLeagcyTotalSale" Text="<%$Resources:Resource, lblLeagcyTotalSale%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtLeagcyTotalSale" runat="server" MaxLength="20" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
    </asp:Panel>
    <ul id="ulStaffMemberInfo" class="form" runat="server">
        <li>
            <div class="lbl">
                <asp:Label ID="lblWorkExperience" Text="<%$Resources:Resource, lblWorkExperience %>"
                    runat="server" meta:resourcekey="lblWorkExperienceResource1" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtWorkEx" runat="server" TextMode="MultiLine" Width="250px" Height="70px"
                    meta:resourcekey="txtWorkExResource1" />
            </div>
            <div class="lbl">
                <asp:Label ID="Illnesses" Text="<%$Resources:Resource, lblIllnesses %>" runat="server"
                    meta:resourcekey="IllnessesResource1" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtIllnesses" runat="server" TextMode="MultiLine" Width="250px"
                    Height="70px" meta:resourcekey="txtIllnessesResource1" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblMedicationResource1" Text="<%$Resources:Resource, lblMedication%>"
                    runat="server" meta:resourcekey="lblMedicationResource1" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtMedication" runat="server" TextMode="MultiLine" Width="250px"
                    Height="70px" meta:resourcekey="txtMedicationResource1" />
            </div>
            <div class="lbl">
                <asp:Label ID="lblExplainWhyYouWishToJoin" Text="<%$Resources:Resource, lblWhyDoYouWishToJoinStaff%>"
                    runat="server" meta:resourcekey="lblExplainWhyYouWishToJoinResource1" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtResonToJoin" runat="server" TextMode="MultiLine" Width="250px"
                    Height="70px" meta:resourcekey="txtResonToJoinResource1" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblHaveYouPartOfStaffBefore" Text="<%$Resources:Resource, lblHaveYouBeenPartOfStaffBefore%>"
                    runat="server" meta:resourcekey="lblHaveYouPartOfStaffBeforeResource1" />
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rblHaveYouPartOfStaffBefore" RepeatDirection="Horizontal"
                    runat="server" meta:resourcekey="rblHaveYouPartOfStaffBeforeResource1">
                    <asp:ListItem Text="<%$Resources:Resource, lblNo%>" Value="0" Selected="True" meta:resourcekey="ListItemResource9" />
                    <asp:ListItem Text="<%$Resources:Resource, lblYes%>" Value="1" meta:resourcekey="ListItemResource10" />
                </asp:RadioButtonList>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <%=Resources.Resource.lblStatus %>
                <asp:Label ID="lblStatus" Text="<%$Resources:Resource, lblStatus%>" runat="server"
                    meta:resourcekey="lblStatusResource1" />
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal" meta:resourcekey="rblStatusResource1">
                </asp:RadioButtonList>
            </div>
            <div class="clearBoth">
            </div>
        </li>
    </ul>
    <asp:Panel ID="pnlBillToAddress" runat="server">
        <h3>
            <asp:Literal ID="ltAddress" Text="<%$Resources:Resource, lblBillToAddress%>" runat="server"
                meta:resourcekey="ltAddressResource1" />&nbsp;&nbsp;&nbsp;<input type="button" id="btnAddBillAddr"
                    runat="server" value="..." style="font-size: 14px; padding: 0.1em 0em;" visible="false"
                    onclick="ShowAddress('B', 'D');" />
        </h3>
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="Label19" Text="<%$Resources:Resource, lblCustFirstName %>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtBillFirstName" runat="server" MaxLength="45" />
                </div>
                <div class="lbl">
                    <asp:Label ID="Label20" Text="<%$Resources:Resource, lblCustLastName %>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtBillLastName" runat="server" MaxLength="45" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblAddressLine1" Text="<%$Resources:Resource, lblAddressLine1 %>"
                        runat="server" meta:resourcekey="lblAddressLine1Resource1" />
                    *
                </div>
                <div class="input">
                    <asp:TextBox ID="txtAddressLine1" runat="server" MaxLength="45" meta:resourcekey="txtAddressLine1Resource1" />
                    <asp:RequiredFieldValidator ID="rfvAddressLine1" ControlToValidate="txtAddressLine1"
                        SetFocusOnError="True" runat="server" ErrorMessage="<%$Resources:Resource, reqBillToAdd1 %>" Text="*"></asp:RequiredFieldValidator>
                    <asp:HiddenField ID="hdnBillAddID" runat="server" Value="0" />
                </div>
                <div class="lbl">
                    <asp:Label ID="lblAddressLine2" Text="<%$Resources:Resource, lblAddressLine2 %>"
                        runat="server" meta:resourcekey="lblAddressLine2Resource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtAddressLine2" runat="server" MaxLength="45" meta:resourcekey="txtAddressLine2Resource1" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblCity" Text="<%$Resources:Resource, lblCity%>" runat="server" meta:resourcekey="lblCityResource1" />
                    *
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCity" runat="server" MaxLength="45" meta:resourcekey="txtCityResource1" />
                    <asp:RequiredFieldValidator ID="rfvCity" ControlToValidate="txtCity" SetFocusOnError="True"
                        runat="server" ErrorMessage="<%$Resources:Resource, reqBillToCity %>" Text="*"></asp:RequiredFieldValidator>
                </div>
                <div class="lbl">
                    <asp:Label ID="lblCountry" Text="<%$Resources:Resource, lblCountry%>" runat="server"
                        meta:resourcekey="lblCountryResource1" />
                    *
                </div>
                <div class="input">
                    <asp:DropDownList ID="ddlCountry" runat="server" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtCountry" runat="server" MaxLength="45" meta:resourcekey="txtCountryResource1"
                        Style="display: none;" />
                    <asp:RequiredFieldValidator ID="rfvCountry" ControlToValidate="ddlCountry" SetFocusOnError="True"
                        runat="server" ErrorMessage="<%$Resources:Resource, reqBillToCountry %>" Text="*"></asp:RequiredFieldValidator>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblPostalCode" Text="<%$Resources:Resource, lblPostalCode%>" runat="server"
                        meta:resourcekey="lblPostalCodeResource1" />
                    *
                </div>
                <div class="input">
                    <asp:TextBox ID="txtPostalCode" runat="server" MaxLength="15" meta:resourcekey="txtPostalCodeResource1" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txtPostalCode"
                        SetFocusOnError="True" runat="server" ErrorMessage="<%$Resources:Resource, reqBillToPostalCode %>" Text="*"></asp:RequiredFieldValidator>
                </div>
                <div class="lbl">
                    <asp:Label ID="lblState" Text="<%$Resources:Resource, lblStateOrProvince %>" runat="server"
                        meta:resourcekey="lblStateResource1" />
                    *
                </div>
                <div class="input">
                    <asp:DropDownList ID="ddlState" runat="server">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtState" runat="server" MaxLength="45" meta:resourcekey="txtStateResource1"
                        Style="display: none;" />
                    <asp:RequiredFieldValidator ID="rfvState" ControlToValidate="txtState" SetFocusOnError="True"
                        runat="server" ErrorMessage="<%$Resources:Resource, reqBillToState %>" Text="*"></asp:RequiredFieldValidator>
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
    </asp:Panel>
    <asp:Panel ID="pnlShipToAddress" runat="server">
        <h3>
            <asp:Literal ID="ltShipToAddress" Text="<%$Resources:Resource, lblShpToAddress%>"
                runat="server" meta:resourcekey="ltShipToAddressResource1" />&nbsp;&nbsp;&nbsp;<input
                    type="button" id="BtnShpAddress" runat="server" visible="false" value="..." style="font-size: 14px; padding: 0.1em 0em;"
                    onclick="ShowAddress('S', 'D');" />
        </h3>
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="lblChkBillToAddress" runat="server" Text="<%$ Resources:Resource, chkBillToAddress %>"
                        meta:resourcekey="lblChkBillToAddressResource1"></asp:Label>
                </div>
                <div class="input">
                    <input id="chkSimilarAddress" type="checkbox" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="Label17" Text="<%$Resources:Resource, lblCustFirstName %>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtShipFirstName" runat="server" MaxLength="45" />
                </div>
                <div class="lbl">
                    <asp:Label ID="Label18" Text="<%$Resources:Resource, lblCustLastName %>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtShipLastName" runat="server" MaxLength="45" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblAddressLine1_2" Text="<%$Resources:Resource, lblAddressLine1%>"
                        runat="server" meta:resourcekey="lblAddressLine1_2Resource1" />
                    *
                </div>
                <div class="input">
                    <asp:TextBox ID="txtSAddressLine1" runat="server" MaxLength="45" meta:resourcekey="txtSAddressLine1Resource1" />
                    <asp:RequiredFieldValidator ID="rfvAddressLine1_2" ControlToValidate="txtSAddressLine1"
                        SetFocusOnError="True" runat="server" ErrorMessage="<%$Resources:Resource, reqShipToAdd1 %>" Text="*"></asp:RequiredFieldValidator>
                    <asp:HiddenField ID="hdnShpAddID" runat="server" Value="0" />
                </div>
                <div class="lbl">
                    <asp:Label ID="lblAddressLine2_2" Text="<%$Resources:Resource, lblAddressLine2%>"
                        runat="server" meta:resourcekey="lblAddressLine2_2Resource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtSAddressLine2" runat="server" MaxLength="45" meta:resourcekey="txtSAddressLine2Resource1" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblCity2" Text="<%$Resources:Resource, lblCity%>" runat="server" meta:resourcekey="lblCity2Resource1" />
                    *
                </div>
                <div class="input">
                    <asp:TextBox ID="txtSCity" runat="server" MaxLength="45" meta:resourcekey="txtSCityResource1" />
                    <asp:RequiredFieldValidator ID="rfvCity2" ControlToValidate="txtSCity" SetFocusOnError="True"
                        runat="server" ErrorMessage="<%$Resources:Resource, reqShipToCity %>" Text="*"></asp:RequiredFieldValidator>
                </div>
                <div class="lbl">
                    <asp:Label ID="lblCountry2" Text="<%$Resources:Resource, lblCountry%>" runat="server" />
                    *
                </div>
                <div class="input">
                    <asp:TextBox ID="txtSCountry" runat="server" MaxLength="45" meta:resourcekey="txtSCountryResource1"
                        Style="display: none;" />
                    <asp:DropDownList ID="ddlSCountry" runat="server" OnSelectedIndexChanged="ddlSCountry_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvCountry2" ControlToValidate="ddlSCountry" SetFocusOnError="True"
                        runat="server" ErrorMessage="<%$Resources:Resource, reqShipToCountry %>" Text="*"></asp:RequiredFieldValidator>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblPostalCode2" Text="<%$Resources:Resource, lblPostalCode%>" runat="server"
                        meta:resourcekey="lblPostalCode2Resource1" />
                    *
                </div>
                <div class="input">
                    <asp:TextBox ID="txtSPostalCode" runat="server" MaxLength="15" meta:resourcekey="txtSPostalCodeResource1" />
                    <asp:RequiredFieldValidator ID="rfvPostalCode2" ControlToValidate="txtSPostalCode"
                        SetFocusOnError="True" runat="server" ErrorMessage="<%$Resources:Resource, reqShipToPostalCode %>" Text="*"></asp:RequiredFieldValidator>
                </div>
                <div class="lbl">
                    <asp:Label ID="lblState2" Text="<%$Resources:Resource, lblStateOrProvince %>" runat="server"
                        meta:resourcekey="lblState2Resource1" />
                    *
                </div>
                <div class="input">
                    <asp:TextBox ID="txtSState" runat="server" MaxLength="45" Style="display: none;" />
                    <asp:DropDownList ID="ddlSState" runat="server" OnSelectedIndexChanged="ddlSState_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ControlToValidate="txtSState"
                        SetFocusOnError="True" runat="server" ErrorMessage="<%$Resources:Resource, reqShipToState %>" Text="*"></asp:RequiredFieldValidator>
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
    </asp:Panel>
    <asp:Panel ID="pnlBillAddress" runat="server">
        <h3>
            <asp:Literal ID="ltPBillAddress" Text="<%$Resources:Resource, lblBillToAddress%>"
                runat="server" />
        </h3>
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="lblPAddressLine1" Text="<%$Resources:Resource, lblAddressLine1 %>"
                        runat="server" meta:resourcekey="lblAddressLine1Resource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtPAddressLine1" runat="server" MaxLength="45" />
                </div>
                <div class="lbl">
                    <asp:Label ID="lblPAddressLine2" Text="<%$Resources:Resource, lblAddressLine2 %>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtPAddressLine2" runat="server" MaxLength="45" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblPCity" Text="<%$Resources:Resource, lblCity%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtPCity" runat="server" MaxLength="45" />
                </div>
                <div class="lbl">
                    <asp:Label ID="lblPCountry" Text="<%$Resources:Resource, lblCountry%>" runat="server" />
                </div>
                <div class="input">
                    <asp:DropDownList ID="ddlPCountry" runat="server" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtPCountry" runat="server" MaxLength="45" Style="display: none;" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblPPostalCode" Text="<%$Resources:Resource, lblPostalCode%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtPPostalCode" runat="server" MaxLength="15" />
                </div>
                <div class="lbl">
                    <asp:Label ID="lblPStateOrProvince" Text="<%$Resources:Resource, lblStateOrProvince %>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:DropDownList ID="ddlPState" runat="server">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtPState" runat="server" MaxLength="45" Style="display: none;" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
    </asp:Panel>
    <asp:Panel ID="pnlShipAddress" runat="server">
        <h3>
            <asp:Literal ID="ltPShpAddress" Text="<%$Resources:Resource, lblShpToAddress%>" runat="server" />
        </h3>
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="lblPBillAddress" runat="server" Text="<%$ Resources:Resource, chkBillToAddress %>"></asp:Label>
                </div>
                <div class="input">
                    <input id="chkSimilarPOSAddress" type="checkbox" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblPSAddressLine1" Text="<%$Resources:Resource, lblAddressLine1%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtPSAddressLine1" runat="server" MaxLength="45" />
                </div>
                <div class="lbl">
                    <asp:Label ID="lblPSAddressLine2" Text="<%$Resources:Resource, lblAddressLine2%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtPSAddressLine2" runat="server" MaxLength="45" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblPSCity" Text="<%$Resources:Resource, lblCity%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtPSCity" runat="server" MaxLength="45" />
                </div>
                <div class="lbl">
                    <asp:Label ID="lblPSCountry" Text="<%$Resources:Resource, lblCountry%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtPSCountry" runat="server" MaxLength="45" Style="display: none;" />
                    <asp:DropDownList ID="ddlPSCountry" runat="server" OnSelectedIndexChanged="ddlSCountry_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblPSPostalCode" Text="<%$Resources:Resource, lblPostalCode%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtPSPostalCode" runat="server" MaxLength="15" />
                </div>
                <div class="lbl">
                    <asp:Label ID="lblPSStateProvince" Text="<%$Resources:Resource, lblStateOrProvince %>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtPSState" runat="server" MaxLength="45" Style="display: none;" />
                    <asp:DropDownList ID="ddlPSState" runat="server" OnSelectedIndexChanged="ddlSState_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
    </asp:Panel>
    <h3 id="liPrimaryContact3" runat="server">
        <asp:Literal ID="ltEmerGencyContact" Text="<%$Resources:Resource, lblEmergencyContact%>"
            runat="server" />
    </h3>
    <ul class="form">
        <li id="liPrimaryContact1" runat="server">
            <div class="lbl">
                <asp:Label ID="lblName" Text="<%$Resources:Resource, lblName%>" runat="server" /><%--*--%>
            </div>
            <div class="input">
                <asp:TextBox ID="txtEmName" runat="server" MaxLength="45" meta:resourcekey="txtEmNameResource1" />
                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtEmName"
                    SetFocusOnError="True" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
            </div>
            <div class="lbl">
                <asp:Label ID="lblRelationship" Text="<%$Resources:Resource, lblRelationship%>" runat="server"
                    meta:resourcekey="lblRelationshipResource1" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtEmRelationship" runat="server" MaxLength="45" meta:resourcekey="txtEmRelationshipResource1" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liPrimaryContact2" runat="server">
            <div class="lbl">
                <asp:Label ID="lblTelephoneNo" Text="<%$Resources:Resource, lblTelephone%>" runat="server"
                    meta:resourcekey="lblTelephoneNoResource1" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtEmTelephone" runat="server" MaxLength="15" ph="999-999-9999"
                    CssClass="placeholder" />
                <asp:CustomValidator ID="cumpEmTelephone" ControlToValidate="txtEmTelephone" ClientValidationFunction="validateEmPhoneFormat"
                    Display="Dynamic" runat="server" ErrorMessage="!" SetFocusOnError="true">
                </asp:CustomValidator>
            </div>
            <div class="lbl">
                <asp:Label ID="lblAdditionalPhoneNo" Text="<%$Resources:Resource, lblAdditionalPhoneNo%>"
                    runat="server" meta:resourcekey="lblAdditionalPhoneNoResource1" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtAlterTelephone" runat="server" MaxLength="45" ph="999-999-9999"
                    CssClass="placeholder" />
                <asp:CustomValidator ID="CustomValidator1" ControlToValidate="txtAlterTelephone"
                    ClientValidationFunction="validateAdditionalContactPhoneFormat" Display="Dynamic"
                    runat="server" ErrorMessage="!" SetFocusOnError="true">
                </asp:CustomValidator>
            </div>
            <div class="clearBoth">
            </div>
        </li>
    </ul>
    <asp:Panel ID="pnlSalesRepresentatives" runat="server">
        <h3>
            <asp:Label ID="Label1" Text="<%$ Resources:Resource, lblSalesRep%>" runat="server" />
        </h3>
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="lblSalesRep" Text="<%$ Resources:Resource, lblSalesRep%>" runat="server" />
                </div>
                <div class="input">
                    <asp:ListBox ID="lbxSalesRepresentativs" SelectionMode="Multiple" runat="server"
                        Width="400px" CssClass="modernized_select"></asp:ListBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
    </asp:Panel>
    <asp:Panel ID="pnlAssignCategories" runat="server">
        <h3>
            <asp:Label ID="Label2" Text="<%$ Resources:Resource, lblCustomerCategories%>" runat="server" />
        </h3>
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="Label3" Text="<%$ Resources:Resource, lblCustomerCategories%>" runat="server" />
                </div>
                <div class="input">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:ListBox ID="lBoxAssignedCateg" runat="server" Width="400px" SelectionMode="Multiple"
                                    CssClass="modernized_select"></asp:ListBox>
                            </td>
                            <td>
                                <asp:Button ID="lnkAddCategory" Text="<%$Resources:Resource, AddCategory%>" runat="server"
                                    CausesValidation="false" OnClick="lnkAddCategory_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
    </asp:Panel>
    <asp:Panel ID="pnlOtherInformationTab" runat="server">
        <h3>
            <asp:Literal ID="ltOtherInformation" Text="<%$Resources:Resource, lblOtherInfo%>"
                runat="server" meta:resourcekey="ltOtherInformationResource1" />
        </h3>
        <ul class="form">
            <li>
                <div class="lbl" style="width: 300px">
                    <asp:Label ID="lblAreYouAMember" Text="<%$Resources:Resource, lblAreYouAMemberOfSivananda%>"
                        runat="server" meta:resourcekey="lblAreYouAMemberResource1" />
                </div>
                <div class="input" style="width: 400px">
                    <asp:RadioButtonList ID="rblMember" runat="server" RepeatDirection="Horizontal" meta:resourcekey="rblMemberResource1">
                    </asp:RadioButtonList>
                </div>
                <div class="clearBoth">
                </div>
                <div class="lbl">
                    <asp:Label ID="lblMemberID" Text="<%$Resources:Resource, lblMemberID%>" runat="server"
                        meta:resourcekey="lblMemberIDResource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtMembership" runat="server" meta:resourcekey="txtMembershipResource1" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblCustomerNote" Text="<%$Resources:Resource, lblCustomerNotes%>"
                        runat="server" meta:resourcekey="lblCustomerNoteResource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCustomerNote" runat="server" TextMode="MultiLine" Width="250px"
                        Height="70px" meta:resourcekey="txtCustomerNoteResource1" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                </div>
                <div>
                    <asp:CheckBox ID="chkNewsLetter" Font-Bold="True" Text="<%$Resources:Resource, lblIWoulLikeToReceiveNewsLetter%>"
                        runat="server" meta:resourcekey="chkNewsLetterResource1" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
    </asp:Panel>
    <asp:Panel ID="pnlSocialMedia" runat="server">
        <h3>
            <asp:Literal ID="ltSocialMedia" Text="<%$Resources:Resource, lblSocialMedia%>" runat="server" />
        </h3>
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="lblEnablesocialPlugin" Text="<%$Resources:Resource, lblEnableSocialPlugin%>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:CheckBox ID="chkEnableSocialPlugin" Text="" runat="server" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
    </asp:Panel>
    <asp:Panel ID="pnlCustomFields" runat="server">
        <uc1:CustomFields ID="CustomFields1" runat="server" />
    </asp:Panel>
    <div style="text-align: right; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;">
        <asp:Button ID="btnDelete" Text="<%$ Resources:Resource, delete %>" runat="server"
            CausesValidation="false" OnClientClick="return confimrDelete();" OnClick="btnDelete_Click" />
        <asp:Button ID="btnSave" Text="<%$ Resources:Resource, btnSave %>" runat="server"
            OnClick="btnSave_Click" meta:resourcekey="btnSaveResource1" />
        <asp:Button ID="btnCancel" Text="<%$ Resources:Resource, btnCancel %>" runat="server"
            CausesValidation="False" OnClick="btnCancel_Click" meta:resourcekey="btnCancelResource1" />
    </div>
    <asp:HiddenField ID="hdnShowHistory" runat="server" Value="0" />
</asp:Panel>
<script type="text/javascript">
    $("#chkSimilarAddress").click(function () {
        if (this.checked) {
            $("#<%=txtSAddressLine1.ClientID%>").val($("#<%=txtAddressLine1.ClientID%>").val());
            $("#<%=txtSAddressLine2.ClientID%>").val($("#<%=txtAddressLine2.ClientID%>").val());
            $("#<%=txtSCity.ClientID%>").val($("#<%=txtCity.ClientID%>").val());
            $("#<%=txtSState.ClientID%>").val($("#<%=txtState.ClientID%>").val());
            $("#<%=txtSCountry.ClientID%>").val($("#<%=txtCountry.ClientID%>").val());
            $("#<%=txtSPostalCode.ClientID%>").val($("#<%=txtPostalCode.ClientID%>").val());
            $("#<%=ddlSCountry.ClientID%>").val($("#<%=txtCountry.ClientID%>").val());
            $("#<%=ddlSCountry.ClientID%>").trigger("change");
            $("#<%=ddlSState.ClientID%>").val($("#<%=txtSState.ClientID%>").val());
            $("#<%=ddlSState.ClientID%>").trigger("change");

            $("#<%=txtShipFirstName.ClientID%>").val($("#<%=txtBillFirstName.ClientID%>").val());
            $("#<%=txtShipLastName.ClientID%>").val($("#<%=txtBillLastName.ClientID%>").val());
        }
    });
    $("#chkSimilarPOSAddress").click(function () {
        if (this.checked) {
            $("#<%=txtPSAddressLine1.ClientID%>").val($("#<%=txtPAddressLine1.ClientID%>").val());
            $("#<%=txtPSAddressLine2.ClientID%>").val($("#<%=txtPAddressLine2.ClientID%>").val());
            $("#<%=txtPSCity.ClientID%>").val($("#<%=txtPCity.ClientID%>").val());
            $("#<%=txtPSState.ClientID%>").val($("#<%=txtPState.ClientID%>").val());
            $("#<%=txtPSCountry.ClientID%>").val($("#<%=txtPCountry.ClientID%>").val());
            $("#<%=txtPSPostalCode.ClientID%>").val($("#<%=txtPPostalCode.ClientID%>").val());
            $("#<%=ddlPSCountry.ClientID%>").val($("#<%=txtPCountry.ClientID%>").val());
            $("#<%=ddlPSCountry.ClientID%>").trigger("change");
            $("#<%=ddlPSState.ClientID%>").val($("#<%=txtPSState.ClientID%>").val());
            $("#<%=ddlPSState.ClientID%>").trigger("change");
        }
    });
    $("#<%=txtPartnerFullName.ClientID%>").focus();

    $("#<%=btnSave.ClientID%>").click(function () {
        var isPOS = $.getParamValue('InvokeSrc');
        if (isPOS == "POS") {
            //            var s = $("#<%=ddlPState.ClientID%>").val();
            //            if (s == "") {
            //                return false;
            //            }
            //            var ss = $("#<%=ddlPSState.ClientID%>").val();
            //            if (ss == "") {
            //                return false;
            //            }
        }
        else {
            var s = $("#<%=ddlState.ClientID%>").val();
            if (s == "") {
                return false;
            }
            var ss = $("#<%=ddlSState.ClientID%>").val();
            if (ss == "") {
                return false;
            }
        }
        if (Page_IsValid) {
            blockContentArea($("body"), "<div>Please wait...</div>");
        }
        return Page_IsValid;
    });

    function confimrDelete() {
        return confirm("<%=Resources.Resource.confirmDelete%>");
    }

    $(".placeholder").addPlaceholder();

    function validatePhoneFormat(source, args) {
        var first12 = $("#<%=txtTelephone.ClientID%>").val();
        var chk_phone = /^[2-9]\d{2}-\d{3}-\d{4}$/;
        //alert(chk_phone.test(first12.substring(0,12)));   
        args.IsValid = first12.length > 0 && first12.length >= 12 && chk_phone.test(first12.substring(0, 12));
    }

    function validateAdditionalPhoneFormat(source, args) {
        var first12 = $("#<%=txtPhone2.ClientID%>").val();
        var chk_phone = /^[2-9]\d{2}-\d{3}-\d{4}$/;
        //alert(chk_phone.test(first12.substring(0,12)));   
        args.IsValid = first12.length > 0 && first12.length >= 12 && chk_phone.test(first12.substring(0, 12));
    }

    function validateEmPhoneFormat(source, args) {
        var first12 = $("#<%=txtEmTelephone.ClientID%>").val();
        var chk_phone = /^[2-9]\d{2}-\d{3}-\d{4}$/;
        //alert(chk_phone.test(first12.substring(0,12)));   
        args.IsValid = first12.length > 0 && first12.length >= 12 && chk_phone.test(first12.substring(0, 12));
    }

    function validateAdditionalContactPhoneFormat(source, args) {
        var first12 = $("#<%=txtAlterTelephone.ClientID%>").val();
        var chk_phone = /^[2-9]\d{2}-\d{3}-\d{4}$/;
        //alert(chk_phone.test(first12.substring(0,12)));   
        args.IsValid = first12.length > 0 && first12.length >= 12 && chk_phone.test(first12.substring(0, 12));
    }

    $("#<%=ddlSCountry.ClientID%>").change(function () {
        var country = $(this).val();
        $("#<%=txtSCountry.ClientID%>").val(country);
        $('option', $("#<%=ddlSState.ClientID%>")).remove();
        $("#<%=ddlSState.ClientID%>").append($("<option />").val("").text(""));
        for (var i = 0; i < _countryStateList.length; i++) {
            if (_countryStateList[i].Country == country) {
                $("#<%=ddlSState.ClientID%>").append($("<option />").val(_countryStateList[i].StateCode).text(_countryStateList[i].StateCode));
            }
        }
    });

    $("#<%=ddlPSCountry.ClientID%>").change(function () {
        var country = $(this).val();
        $("#<%=txtPSCountry.ClientID%>").val(country);
        $('option', $("#<%=ddlPSState.ClientID%>")).remove();
        $("#<%=ddlPSState.ClientID%>").append($("<option />").val("").text(""));
        for (var i = 0; i < _countryStateList.length; i++) {
            if (_countryStateList[i].Country == country) {
                $("#<%=ddlPSState.ClientID%>").append($("<option />").val(_countryStateList[i].StateCode).text(_countryStateList[i].StateCode));
            }
        }
    });

    $("#<%=ddlCountry.ClientID%>").change(function () {
        var country = $(this).val();
        $("#<%=txtCountry.ClientID%>").val(country);
        $('option', $("#<%=ddlState.ClientID%>")).remove();
        $("#<%=ddlState.ClientID%>").append($("<option />").val("").text(""));
        for (var i = 0; i < _countryStateList.length; i++) {
            if (_countryStateList[i].Country == country) {
                $("#<%=ddlState.ClientID%>").append($("<option />").val(_countryStateList[i].StateCode).text(_countryStateList[i].StateCode));
            }
        }
    });

    $("#<%=ddlPCountry.ClientID%>").change(function () {
        var country = $(this).val();
        $("#<%=txtPCountry.ClientID%>").val(country);
        $('option', $("#<%=ddlPState.ClientID%>")).remove();
        $("#<%=ddlPState.ClientID%>").append($("<option />").val("").text(""));
        for (var i = 0; i < _countryStateList.length; i++) {
            if (_countryStateList[i].Country == country) {
                $("#<%=ddlPState.ClientID%>").append($("<option />").val(_countryStateList[i].StateCode).text(_countryStateList[i].StateCode));
            }
        }
    });

    $("#<%=ddlSState.ClientID%>").live("change", function () {
        var country = $("#<%=ddlSCountry.ClientID%>").val();
        var state = $(this).val();
        $("#<%=txtSState.ClientID%>").val(state);
        for (var i = 0; i < _countryStateList.length; i++) {
            if (_countryStateList[i].Country == country && _countryStateList[i].StateCode == state) {
                $("#<%=dlTaxGroup.ClientID%>").val(_countryStateList[i].StateGroupTaxCode);
                return;
            }
        }
    });

    $("#<%=ddlPSState.ClientID%>").live("change", function () {
        var country = $("#<%=ddlPSCountry.ClientID%>").val();
        var state = $(this).val();
        $("#<%=txtPSState.ClientID%>").val(state);
        for (var i = 0; i < _countryStateList.length; i++) {
            if (_countryStateList[i].Country == country && _countryStateList[i].StateCode == state) {
                $("#<%=dlTaxGroup.ClientID%>").val(_countryStateList[i].StateGroupTaxCode);
                return;
            }
        }
    });

    $("#<%=ddlState.ClientID%>").live("change", function () {
        var state = $(this).val();
        $("#<%=txtState.ClientID%>").val(state);
    });

    $("#<%=ddlPState.ClientID%>").live("change", function () {
        var state = $(this).val();
        $("#<%=txtPState.ClientID%>").val(state);
    });

    $.extend({
        getParamValue: function (paramName) {
            parName = paramName.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
            var pattern = '[\\?&]' + paramName + '=([^&#]*)';
            var regex = new RegExp(pattern);
            var matches = regex.exec(window.location.href);
            if (matches == null) return '';
            else return decodeURIComponent(matches[1].replace(/\+/g, ' '));
        }
    });

    $(document).ready(function () {
        $("#<%=showHistoryLink.ClientID%>").hide();

        if ($("#<%=hdnShowHistory.ClientID%>").val() == "1") {
            $("#<%=showHistoryLink.ClientID%>").show();
        }

        var isPOS = $.getParamValue('InvokeSrc');
        if (isPOS == "POS") {
            //            $("#<%=pnlBillToAddress.ClientID %>").toggle();
            //            $("#<%=pnlShipToAddress.ClientID %>").toggle();

            $("#<%=pnlBillAddress.ClientID %>").toggle();
            $("#<%=pnlShipAddress.ClientID %>").toggle();

            ValidatorEnable(document.getElementById('<%=rfvAddressLine1.ClientID%>'), false);
            ValidatorEnable(document.getElementById('<%=rfvCity.ClientID%>'), false);
            ValidatorEnable(document.getElementById('<%=rfvCountry.ClientID%>'), false);
            ValidatorEnable(document.getElementById('<%=RequiredFieldValidator6.ClientID%>'), false);
            ValidatorEnable(document.getElementById('<%=rfvState.ClientID%>'), false);
            ValidatorEnable(document.getElementById('<%=rfvAddressLine1_2.ClientID%>'), false);
            ValidatorEnable(document.getElementById('<%=rfvCity2.ClientID%>'), false);
            ValidatorEnable(document.getElementById('<%=rfvCountry2.ClientID%>'), false);
            ValidatorEnable(document.getElementById('<%=rfvPostalCode2.ClientID%>'), false);
            ValidatorEnable(document.getElementById('<%=RequiredFieldValidator13.ClientID%>'), false);
        }
        else {
            $("#<%=pnlBillAddress.ClientID %>").toggle();
            $("#<%=pnlShipAddress.ClientID %>").toggle();
        }

    });

    function MaxLengthValdation(ctrlID, maxLength) {

        //get the limit from maxlength attribute
        var limit = maxLength; // parseInt($(this).attr('maxlength'));
        //get the current text inside the textarea
        var text = $("#" + ctrlID).val();
        //count the number of characters in the text
        var chars = text.length;

        //check if there are more characters then allowed
        if (chars > limit) {
            //and if there are use substr to get the text before the limit
            var new_text = text.substr(0, limit);

            //and change the current text with the new text
            $("#" + ctrlID).val(new_text);
        }


    }


    function ShowAddress(addressType, pType) {
        var custID = $("#<%=hdnPartID.ClientID%>").val();
        var $dialog = jQuery.FrameDialog.create({
            url: "AddressCollection.aspx?addType=" + addressType + "&custID=" + custID + "&pType=" + pType + "&src=" + "cust",
            title: "<%=Resources.Resource.lblCustomerAddress%>",
            loadingClass: "loading-image",
            modal: true,
            width: "900",
            height: "500",
            autoOpen: false
        });
        $dialog.dialog('open'); return false;
    }

    $("#<%=ltInstoreCredit.ClientID %>").click(function () {
        var custID = $("#<%=hdnPartID.ClientID%>").val();
        var pageTitle = "<%=Resources.Resource.lblInstoreCreditHistory%>";

        var $dialog = jQuery.FrameDialog.create({
            url: "ChangeInstoreCreditValue.aspx?custID=" + custID,
            title: pageTitle,
            loadingClass: "loading-image",
            modal: true,
            width: "800",
            height: "350",
            autoOpen: false
        });
        $dialog.dialog('open'); return false;

        return false;
    });

    function setInstoreCreditAmt(ablCreditAmt) {
        $("#<%=ltInstoreCredit.ClientID%>").html(ablCreditAmt);
        $("#<%=showHistoryLink.ClientID%>").show();
    }

    function ShowInsCrdHistory() {
        var custID = $("#<%=hdnPartID.ClientID%>").val();
        var pageTitle = "<%=Resources.Resource.lblInstoreCreditHistory%>";

        var $dialog = jQuery.FrameDialog.create({
            url: "mdInstoreCreditHistory.aspx?custID=" + custID,
            title: pageTitle,
            loadingClass: "loading-image",
            modal: true,
            width: "800",
            height: "350",
            autoOpen: false
        });
        $dialog.dialog('open'); return false;

        return false;
    }

</script>

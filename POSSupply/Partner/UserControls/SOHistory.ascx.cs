﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using Newtonsoft.Json;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Partner_UserControls_SOHistory : System.Web.UI.UserControl, ICustomer
{
    private string strSOID = "";
    private Orders _objOrder = new Orders();
    int _currentRowIdx = -1;
     
    // On Page Load
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public int PartnerID
    {
        get
        {
            int custid = 0;
            int.TryParse(Request.QueryString["custid"], out custid);
            return custid;
        }
    }

    public void Initialize()
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            if (this.PartnerID > 0)
            {
                Partners part = new Partners();
                part.PopulateObject(dbHelp, this.PartnerID);
                Addresses addr = part.GetBillToAddress(dbHelp, this.PartnerID, (int)PartnerTypeIDs.EndClient);
                int orderID = part.GetRecentOrderID(dbHelp, this.PartnerID);

                Orders ord = new Orders();
                ord.PopulateObject(dbHelp, orderID);

                valAddress.Text = addr.AddressLine1;
                valCity.Text = addr.AddressCity;
                valEmail.Text = part.PartnerEmail;
                valGuestName.Text = part.PartnerLongName;
                valPhone.Text = part.PartnerPhone;
                valRecentOrderDate.Text = BusinessUtility.GetDateTimeString(ord.OrdCreatedOn, DateFormat.MMddyyyy);
                valSex.Text = part.ExtendedProperties.Sex == "M" ? Resources.Resource.lblMale : Resources.Resource.lblFemale;
                valState.Text = addr.AddressState;
                valStatus.Text = part.PartnerActive ? Resources.Resource.lblActive : Resources.Resource.lblInActive;
            }
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }

    }

    protected void grdSales_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 0)
        {
            string strurl = ResolveUrl(string.Format("~/Sales/SalesEdit.aspx?SOID={0}", e.RowKey));
            e.CellHtml = string.Format(@"<a href=""{0}"">{1}</a>", strurl, e.CellHtml);
        }        
    }

    protected void grdSales_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        grdSales.DataSource = _objOrder.GetGuestOrders(this.PartnerID, Globals.CurrentAppLanguageCode);        
    }
    protected void grdSales_DataRequested(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestedEventArgs e)
    {
        double total = 0.0D;
        foreach (DataRow item in e.DataTable.Rows)
        {
            total += BusinessUtility.GetDouble(item["OrderAmount"]);
        }

        grdSales.Columns.FromDataField("OrderAmount").FooterValue = string.Format("<span style='font-size:14px;'>Total: {0:F}</span>", total); 
    }
}
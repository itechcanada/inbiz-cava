﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Data;

using Newtonsoft.Json;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;
using iTECH.WebControls;

public partial class Partner_UserControls_CustomerLoyaltyProgram : System.Web.UI.UserControl, ICustomer
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public int PartnerID
    {
        get
        {
            int custid = 0;
            int.TryParse(Request.QueryString["custid"], out custid);
            return custid;
        }
    }

    public void Initialize()
    {
        if (grdLoyalty.AjaxCallBackMode == Trirand.Web.UI.WebControls.AjaxCallBackMode.None)
        {
            ltSectionTitle.Text = Resources.Resource.lblLoyaltyProgram;

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                Partners part = new Partners();
                LoyalPartnerHistory plh = new LoyalPartnerHistory();
                part.PopulateObject(dbHelp, this.PartnerID);

                double points = plh.GetPartnerLoyaltyPoints(dbHelp, this.PartnerID);
                valCustName.Text = part.PartnerLongName;
                UIHelper.FormatNumberLabel(valPoints, null, points);

                valSmartGivID.Text = string.IsNullOrEmpty(part.ExtendedProperties.SpirtualName) ? "N/A" : part.ExtendedProperties.SpirtualName;
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
    protected void grdLoyalty_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {

    }
    protected void grdLoyalty_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        grdLoyalty.DataSource = new LoyalPartnerHistory().GetPartnerLoyaltyHistory(null, this.PartnerID);
    }
}
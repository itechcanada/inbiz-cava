﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerContacts.ascx.cs"
    Inherits="Partner_UserControls_CustomerContacts" %>
<h3>
    <asp:Literal ID="ltSectionTitle" runat="server" />
</h3>
<asp:Panel ID="pnlCustomerContact" runat="server">
    <div id="grid_wrapper" style="width: 100%;">
        <trirand:JQGrid runat="server" ID="grdQuantity" Height="100px" AutoWidth="True" OnDataRequesting="grdQuantity_DataRequesting"
            OnCellBinding="grdQuantity_CellBinding">
            <Columns>
                <trirand:JQGridColumn DataField="ContactID" HeaderText="" PrimaryKey="True" Visible="false" />
                <trirand:JQGridColumn DataField="ContactType" HeaderText="<%$ Resources:Resource, lblContactType %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="ContactTitle" HeaderText="<%$ Resources:Resource, lblContactTitle %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="ContactFirstName" HeaderText="<%$ Resources:Resource, lblFName %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="ContactLastName" HeaderText="<%$ Resources:Resource, lblLName %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="ContactPhone" HeaderText="<%$ Resources:Resource, lblTelephone %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="ContactEmail" HeaderText="<%$ Resources:Resource, lblEmail %>"
                    Editable="false" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdvEdit %>" DataField="ContactID"
                    Sortable="false" TextAlign="Center" Width="50">
                </trirand:JQGridColumn>
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="ContactID"
                    Sortable="false" TextAlign="Center" Width="50" Visible="false">
                </trirand:JQGridColumn>
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="gridLoadComplete"
                RowSelect="rowSelect" />
        </trirand:JQGrid>
        <asp:HiddenField ID="hdnIdToEdit" runat="server" />
    </div>
    <ul class="form">
        <asp:HiddenField ID="hdnConactID" runat="server" Value="0" />
        <li id="liName" runat="server">
            <div class="lbl">
                <asp:Label ID="lblFirstName" Text="<%$Resources:Resource, lblFName%>" runat="server" />*
            </div>
            <div class="input">
                <asp:TextBox ID="txtFirstName" runat="server" MaxLength="35" />
                <asp:RequiredFieldValidator ID="rfvFirstName" ControlToValidate="txtFirstName" SetFocusOnError="True"
                    runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
            </div>
            <div class="lbl">
                <asp:Label ID="lblLastName" Text="<%$Resources:Resource, lblLName %>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtLastName" runat="server" MaxLength="35" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
  <li id="liLContactTitle" runat="server">
            <div class="lbl">
                <asp:Label ID="lblContactTitleFr" Text="<%$Resources:Resource, lblCMTitleFr%>" runat="server" /><%--*--%>
            </div>
            <div class="input">
                <asp:TextBox ID="txtContactTitleFr" runat="server" MaxLength="50" />
            </div>
            <div class="lbl">
                <asp:Label ID="lblContactTitleEn" Text="<%$Resources:Resource, lblCMTitleEN %>" runat="server" /><%--*--%>
            </div>
            <div class="input">
                <asp:TextBox ID="txtContactTitleEn" runat="server" MaxLength="50" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="li1" runat="server">
            <div class="lbl">
                <asp:Label ID="lblDOB" Text="<%$Resources:Resource, lblDateOfBirth%>" runat="server"
                    meta:resourcekey="lblDOBResource1" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtDOB" CssClass="dob_datepicker" runat="server" meta:resourcekey="txtDOBResource1" />
            </div>
            <div class="lbl">
                <asp:Label ID="Label12" Text="<%$Resources:Resource, lblGender%>" runat="server" />
            </div>
            <div class="input">
                <asp:DropDownList ID="ddlGender" runat="server">
                    <asp:ListItem Text="<%$Resources:Resource, lblMale%>" Value="M"></asp:ListItem>
                    <asp:ListItem Text="<%$Resources:Resource, lblFemale%>" Value="F"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liPhoneMail" runat="server">
            <div class="lbl">
                <asp:Label ID="lblTelephoneNumber" Text="<%$Resources:Resource,lblTelephone %>" runat="server" />*
            </div>
            <div class="input">
                <asp:TextBox ID="txtTelephone" runat="server" MaxLength="20" ph="999-999-9999" CssClass="placeholder" />
                <asp:CustomValidator ID="cumpTelephone" ControlToValidate="txtTelephone" ClientValidationFunction="validatePhoneFormat"
                    Display="Dynamic" runat="server" ErrorMessage="!" SetFocusOnError="true" ValidateEmptyText="true">
                </asp:CustomValidator>
                <asp:RequiredFieldValidator ID="rfvTelephone" ControlToValidate="txtTelephone" SetFocusOnError="True"
                    runat="server" ErrorMessage="*" meta:resourcekey="rfvTelephoneResource1"></asp:RequiredFieldValidator>
            </div>
            <div class="lbl">
                <asp:Label ID="Label1" Text="<%$Resources:Resource,lblCMPhoneExt %>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtPhoneExt" runat="server" MaxLength="10" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liHomeCellPhone" runat="server">
            <div class="lbl">
                <asp:Label ID="lblHomePhone" Text="<%$Resources:Resource,lblCMHomePhone %>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtHomePhone" runat="server" MaxLength="20" ph="999-999-9999" CssClass="placeholder" />
                <%--<asp:CustomValidator ID="cumpHomePhone" ControlToValidate="txtHomePhone" ClientValidationFunction="validatePhoneFormat"
                    Display="Dynamic" runat="server" ErrorMessage="!" SetFocusOnError="true" ValidateEmptyText="true">
                </asp:CustomValidator>--%>
            </div>
            <div class="lbl">
                <asp:Label ID="lblCellPhone" Text="<%$Resources:Resource,lblCMCellPhone %>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtCellPhone" runat="server" MaxLength="20" ph="999-999-9999" CssClass="placeholder" />
                <%--<asp:CustomValidator ID="cumpCellPhone" ControlToValidate="txtCellPhone" ClientValidationFunction="validatePhoneFormat"
                    Display="Dynamic" runat="server" ErrorMessage="!" SetFocusOnError="true" ValidateEmptyText="true">
                </asp:CustomValidator>--%>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liGuest2" runat="server">
            <div class="lbl">
                <asp:Label ID="lblEmailAddress" Text="<%$Resources:Resource, lblEmail%>" runat="server"
                    meta:resourcekey="lblEmailAddressResource1" />*
            </div>
            <div class="input">
                <asp:TextBox ID="txtEmailAddress" runat="server" MaxLength="75" meta:resourcekey="txtEmailAddressResource1" />
                <asp:RequiredFieldValidator ID="rfvEmailAddress" ControlToValidate="txtEmailAddress"
                    SetFocusOnError="True" runat="server" ErrorMessage="*" meta:resourcekey="rfvEmailAddressResource1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="rfvSecEmail" runat="server" ControlToValidate="txtEmailAddress"
                    ErrorMessage="<%$ Resources:Resource, revalCMSecEmail %>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    meta:resourcekey="rfvSecEmailResource1" Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
            <div class="lbl">
                <asp:Label ID="lblEmailAddress2" Text="<%$Resources:Resource, lblEmail2%>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtEmailAddress2" runat="server" MaxLength="75" />
                <asp:RegularExpressionValidator ID="rfvSecEmail2" runat="server" ControlToValidate="txtEmailAddress2"
                    ErrorMessage="<%$ Resources:Resource, revalCMSecEmail %>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    meta:resourcekey="rfvSecEmailResource1" Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="liSalutation" runat="server">
            <div class="lbl">
                <asp:Label ID="lblSalutation" Text="<%$Resources:Resource, lblCMSalutation%>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtSalutation" runat="server" MaxLength="10" />
            </div>
            <div class="lbl">
                <asp:Label ID="lblFax" Text="<%$Resources:Resource, lblFax %>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtFax" runat="server" MaxLength="20" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
      
        <li id="liFBTwit" runat="server">
            <div class="lbl">
                <asp:Label ID="Label13" Text="<%$Resources:Resource, lblFaceBook%>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtFacebook" runat="server" MaxLength="350" />
            </div>
            <div class="lbl">
                <asp:Label ID="Label14" Text="<%$Resources:Resource, lblTiwtter%>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtTiwtter" runat="server" MaxLength="350" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="li2" runat="server">
            <div class="lbl">
                <asp:Label ID="lblLinkedIN" Text="<%$Resources:Resource, lblLinkedIN%>" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtLinkedIn" runat="server" MaxLength="350" />
            </div>
            <div class="lbl">
                <asp:Label ID="lblContactType" Text="<%$Resources:Resource, lblContactType%>" runat="server" />
            </div>
            <div class="input">
                <asp:DropDownList ID="ddlContactType" runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblIsActive" CssClass="lblBold" Text="<%$ Resources:Resource, lblIsActive%>"
                    runat="server" />
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rblstIsActive" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Text="<%$ Resources:Resource, lblCMYes%>" Value="1" Selected="True">
                    </asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lblCMNo%>" Value="0">
                    </asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="lbl">
                <asp:Label ID="lblContactPrimary" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMContactPrimary%>"
                    runat="server" />
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rblstContactPrimary" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Text="<%$ Resources:Resource, lblCMYes%>" Value="1">
                    </asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lblCMNo%>" Value="0" Selected="True">
                    </asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblLangPref" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMLangPref%>">
                </asp:Label>
            </div>
            <div class="input">
                <asp:RadioButtonList ID="rblstLangPref" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Text="<%$ Resources:Resource, liCMFrench%>" Value="fr" Selected="True">
                    </asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, liCMEnglish%>" Value="en"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="lbl">
                &nbsp;
            </div>
            <div class="input">
            <asp:CheckBox ID="chkIsSubscribeMail" runat="server" Text="<%$ Resources:Resource, lblIsSubscribeMail%>" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li id="li3" runat="server">
            <div class="lbl">
                <asp:Label ID="lblNotes" CssClass="lblBold" Visible="true" Text="<%$ Resources:Resource, lblCMNotes%>"
                    runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" MaxLength="10" Width="600px" onkeyup="return MaxLengthValdation(this.id, 200);" /><br />
            </div>
            <div class="clearBoth">
            </div>
        </li>
    </ul>
    <div style="text-align: right; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;">
        <asp:Button ID="btnSave" Text="<%$ Resources:Resource, btnSave %>" runat="server"
            OnClick="btnSave_Click" meta:resourcekey="btnSaveResource1" />
        <asp:Button ID="btnCancel" Text="<%$ Resources:Resource, btnCancel %>" runat="server"
            CausesValidation="False" OnClick="btnCancel_Click" meta:resourcekey="btnCancelResource1" />
    </div>
    <script language="javascript">
        function MaxLengthValdation(ctrlID, maxLength) {

            //get the limit from maxlength attribute
            var limit = maxLength; // parseInt($(this).attr('maxlength'));
            //get the current text inside the textarea
            var text = $("#" + ctrlID).val();
            //count the number of characters in the text
            var chars = text.length;

            //check if there are more characters then allowed
            if (chars > limit) {
                //and if there are use substr to get the text before the limit
                var new_text = text.substr(0, limit);

                //and change the current text with the new text
                $("#" + ctrlID).val(new_text);
            }


        }
    </script>
    <script type="text/javascript">
        function validatePhoneFormat(source, args) {
            var first12 = $("#<%=txtTelephone.ClientID%>").val();
            var chk_phone = /^[2-9]\d{2}-\d{3}-\d{4}$/;
            //alert(chk_phone.test(first12.substring(0,12)));   
            args.IsValid = first12.length > 0 && first12.length >= 12 && chk_phone.test(first12.substring(0, 12));
        }

        $(".placeholder").addPlaceholder();

        //Variables
        var gridID = "<%=grdQuantity.ClientID %>";

        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        function reloadGrid(event, ui) {
            resetForm();
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function gridLoadComplete(data) {
            $(window).trigger("resize");
        }

        function deleteQuantity(id) {
            if (confirm("Are you sure you want to delete?")) {
                var dataToPost = {};
                dataToPost.deleteItem = 1;
                dataToPost.idToDelete = id;
                $grid = $("#" + gridID);
                for (var k in dataToPost) {
                    $grid.setPostDataItem(k, dataToPost[k]);
                }
                $grid.trigger("reloadGrid");

                for (var k in dataToPost) {
                    $grid.removePostDataItem(k);
                }
            }
        }

        function resetForm() {
            $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_dlWarehouses").val("");
            $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtOnHandQuanPerLocation").val("");
            $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtFloor").val("");
            $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtAisie").val("");
            $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtRack").val("");
            $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtBin").val("");
            $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_dlTaxCode").val("");
            $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtBlock").val("");
            $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtReservedQuantity").val("");
            $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtResQuaforSO").val("");
            $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtDefectedQua").val("");
            $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_hdnIdToEdit").val("");
        }


        function rowSelect(id) {
            var dataToPost = {};
            dataToPost.id = id;
            $.ajax({
                type: "POST",
                url: 'CustomerEdit.aspx/GetPartnerContacts',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(dataToPost),
                success: function (data) {
                    var qtyObj = data.d;
                    if (qtyObj != null && qtyObj.ContactID > 0) {
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_hdnIdToEdit").val(qtyObj.ContactID);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtFirstName").val(qtyObj.ContactFirstName);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtLastName").val(qtyObj.ContactLastName);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtDOB").val(qtyObj.DOB);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtTelephone").val(qtyObj.ContactPhone);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtPhoneExt").val(qtyObj.ContactPhoneExt);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtHomePhone").val(qtyObj.ContactHomePhone);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtCellPhone").val(qtyObj.ContactCellPhone);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtEmailAddress").val(qtyObj.ContactEmail);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtEmailAddress2").val(qtyObj.ContactSecEmail);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtSalutation").val(qtyObj.ContactSalutation);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtContactTitleFr").val(qtyObj.ContactTitleFr);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtContactTitleEn").val(qtyObj.ContactTitleEn);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtFacebook").val(qtyObj.FaceBook);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtTiwtter").val(qtyObj.Twitter);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtFax").val(qtyObj.ContactFax);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtLinkedIn").val(qtyObj.LinkedIn);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_txtNotes").val(qtyObj.ContactNote);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_ddlGender").val(qtyObj.ContactSex);
                        $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_ddlContactType").val(qtyObj.ContactType);

                        if (qtyObj.ContactActive == true) {
                            $('#<%=rblstIsActive.ClientID %>').find("input[value='1']").attr("checked", "checked");
                        }
                        else {
                            $('#<%=rblstIsActive.ClientID %>').find("input[value='0']").attr("checked", "checked");
                        }

                        if (qtyObj.ContactPrimary == true) {
                            $('#<%=rblstContactPrimary.ClientID %>').find("input[value='1']").attr("checked", "checked");
                        }
                        else {
                            $('#<%=rblstContactPrimary.ClientID %>').find("input[value='0']").attr("checked", "checked");
                        }

                        if (qtyObj.ContactLangPref != "") {
                            $('#<%=rblstLangPref.ClientID %>').find("input[value='" + qtyObj.ContactLangPref + "']").attr("checked", "checked");
                        }

                        if (qtyObj.IsSubscribeEmail == true) {
                            $('#<%=chkIsSubscribeMail.ClientID %>').attr("checked", "checked");
                        }
                        else {
                            $('#<%=chkIsSubscribeMail.ClientID %>').removeAttr("checked");
                        }
                    }
                },
                error: function (request, status, errorThrown) {
                    alert(status);
                }
            });
        }
    </script>
</asp:Panel>

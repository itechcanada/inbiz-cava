﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Data;

using Newtonsoft.Json;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using MySql.Data.MySqlClient;
using iTECH.WebControls;

public partial class Partner_UserControls_CustomerDetails : System.Web.UI.UserControl, ICustomer
{
    Partners _cust = new Partners();
    Addresses _billToAddress = new Addresses();
    Addresses _shipToAddress = new Addresses();
    CountryStateTaxGroup _country = new CountryStateTaxGroup();
    NetTerm _netTerm = new NetTerm();
    SysCompanyInfo _comInfo = new SysCompanyInfo();

    protected void Page_Load(object sender, EventArgs e)
    {
        //txtFirstName.Focus();
        txtLastName.Focus();
        if (Request.QueryString.AllKeys.Contains("create_POS") && Request.QueryString["create_POS"] == "1" || Request.QueryString.AllKeys.Contains("InvokeSrc") && Request.QueryString["InvokeSrc"] == "POS")
        {
            rfvAddressLine1.Enabled = false;
            rfvCity.Enabled = false;
            rfvCountry.Enabled = false;
            RequiredFieldValidator6.Enabled = false;
            rfvState.Enabled = false;
            rfvAddressLine1_2.Enabled = false;
            rfvCity2.Enabled = false;
            rfvCountry2.Enabled = false;
            rfvPostalCode2.Enabled = false;
            RequiredFieldValidator13.Enabled = false;
        }
    }

    public void Initialize()
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "_countryStateList", string.Format("var _countryStateList={0};", JsonConvert.SerializeObject(_country.GetAllCountryStateList(dbHelp))), true);
            if (!IsPostBack)
            {
                btnDelete.Visible = this.PartnerID > 0;

                CustomFields1.Initialize(this.PartnerID, CustomFieldApplicableSection.CustomerAddEdit);

                //FillGuest Type Dropdown
                rblIsSpecialGuest.Items.Clear();
                rblIsSpecialGuest.Items.Add(new ListItem(Resources.Resource.lblGuest, ((int)StatusGuestType.Guest).ToString()));
                rblIsSpecialGuest.Items.Add(new ListItem(Resources.Resource.lblSpecialGuest, ((int)StatusGuestType.SpecialGuest).ToString()));
                rblIsSpecialGuest.Items.Add(new ListItem(Resources.Resource.lblCourseParticipants, ((int)StatusGuestType.CourseParticipants).ToString()));
                ListItem liGuestType = rblIsSpecialGuest.Items.FindByValue(((int)this.GuestType).ToString());

                //DropDownHelper.FillDropdown(ddlEmergencyRel, "CU", "dlRel", Globals.CurrentAppLanguageCode, new ListItem("--Select--", "0"));        
                DropDownHelper.FillDropdown(rblMember, "CU", "dlMem", Globals.CurrentAppLanguageCode, null);
                DropDownHelper.FillDropdown(rblStatus, "CU", "dlSts", Globals.CurrentAppLanguageCode, null);
                DropDownHelper.FillCurrencyDropDown(dlCurrencyCode, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.lstSelectCurrency, "0"));
                DropDownHelper.FillTaxGroup(dlTaxGroup, CurrentUser.UserDefaultWarehouse, new ListItem(Resources.Resource.liTaxGroup, "0"));
                DropDownHelper.FillSalesRepresentatives(lbxSalesRepresentativs, null);

                SysWarehouses wh = new SysWarehouses();
                wh.FillWharehouse(dbHelp, dlWarehouse, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));

                _netTerm.PopulateListControl(dbHelp, ddlNetTerms, new ListItem(""));
                _country.PopulateListControlWithCountry(dbHelp, ddlCountry, new ListItem(""));
                _country.PopulateListControlWithCountry(dbHelp, ddlSCountry, new ListItem(""));
                _country.PopulateListControlWithCountry(dbHelp, ddlPCountry, new ListItem(""));
                _country.PopulateListControlWithCountry(dbHelp, ddlPSCountry, new ListItem(""));
                lblPartID.Text = this.PartnerID <= 0 ? _cust.GetMaxPartnerID(dbHelp).ToString() : string.Empty;
                hdnPartID.Value = lblPartID.Text;

                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "_countryStateList", string.Format("var _countryStateList={0};", JsonConvert.SerializeObject(_country.GetAllCountryStateList(dbHelp))), true);


                this.FillCategoryDataList();

                if (CurrentUser.DefaultCompanyID > 0)
                {
                    _comInfo.PopulateObject(CurrentUser.DefaultCompanyID, dbHelp);

                    //Validate phone format only if checked in Company
                    cumpTelephone.Visible = _comInfo.RestrictedPhoneFormat;
                    cumpAdditionalPhone.Visible = _comInfo.RestrictedPhoneFormat;
                    cumpEmTelephone.Visible = _comInfo.RestrictedPhoneFormat;
                    rfvTelephone.Visible = !cumpTelephone.Visible;
                }

                if (this.PartnerID > 0)
                {
                    BtnShpAddress.Visible = true;
                    btnAddBillAddr.Visible = true;

                    _cust.PopulateObject(dbHelp, this.PartnerID);
                    //_billToAddress = _cust.GetBillToAddress(dbHelp, _cust.PartnerID, _cust.PartnerType);
                    //_shipToAddress = _cust.GetShipToAddress(dbHelp, _cust.PartnerID, _cust.PartnerType);

                    //Addresses billtoAddress = new Addresses();
                    _billToAddress.GetCustomerAddress(_cust.PartnerID, AddressType.BILL_TO_ADDRESS);

                    //Addresses shipToAddress = new Addresses();
                    _shipToAddress.GetCustomerAddress(_cust.PartnerID, AddressType.SHIP_TO_ADDRESS);

                    ddlCountry.SelectedValue = _billToAddress.AddressCountry;
                    //_country.PopulateListControlWithStateCodes(dbHelp, ddlCountry.SelectedValue, ddlState, new ListItem(""));
                    _country.PopulateListControlWithStateCodes(dbHelp, ddlCountry.SelectedValue, ddlState, new ListItem(""));
                    ddlState.SelectedValue = _billToAddress.AddressState;

                    ddlSCountry.SelectedValue = _shipToAddress.AddressCountry;
                    //_country.PopulateListControlWithStateCodes(dbHelp, ddlSCountry.SelectedValue, ddlSState, new ListItem(""));
                    _country.PopulateListControlWithStateCodes(dbHelp, ddlSCountry.SelectedValue, ddlSState, new ListItem(""));
                    ddlSState.SelectedValue = _shipToAddress.AddressState;

                    lblPartID.Text = _cust.PartnerID.ToString();
                    hdnPartID.Value = lblPartID.Text;
                    txtAvailableCredit.Text = BusinessUtility.GetString(_cust.PartnerCreditAvailable);
                    txtAddressLine1.Text = _billToAddress.AddressLine1;
                    txtAddressLine2.Text = _billToAddress.AddressLine2;
                    txtCity.Text = _billToAddress.AddressCity;
                    txtCountry.Text = _billToAddress.AddressCountry;
                    txtState.Text = _billToAddress.AddressState;
                    txtPostalCode.Text = _billToAddress.AddressPostalCode;
                    txtBillFirstName.Text = _billToAddress.FirstName;
                    txtBillLastName.Text = _billToAddress.LastName;
                    hdnBillAddID.Value = BusinessUtility.GetString(_billToAddress.AddressID);

                    txtSAddressLine1.Text = _shipToAddress.AddressLine1;
                    txtSAddressLine2.Text = _shipToAddress.AddressLine2;
                    txtSCity.Text = _shipToAddress.AddressCity;
                    txtSCountry.Text = _shipToAddress.AddressCountry;
                    txtSState.Text = _shipToAddress.AddressState;
                    txtSPostalCode.Text = _shipToAddress.AddressPostalCode;
                    txtShipFirstName.Text = _shipToAddress.FirstName;
                    txtShipLastName.Text = _shipToAddress.LastName;
                    hdnShpAddID.Value = BusinessUtility.GetString(_shipToAddress.AddressID);


                    txtPartnerFullName.Text = _cust.PartnerLongName;
                    txtCustomerNote.Text = _cust.PartnerNote;
                    txtEmailAddress.Text = _cust.PartnerEmail;
                    txtTelephone.Text = _cust.PartnerPhone;
                    txtCustomerNote.Text = _cust.PartnerNote;
                    txtDiscPer.Text = BusinessUtility.GetString(_cust.PartnerDiscount);

                    //txtInvoiceNetTerms.Text = _cust.PartnerInvoiceNetTerms;
                    ListItem li = ddlNetTerms.Items.FindByText(_cust.PartnerInvoiceNetTerms);
                    if (li != null)
                    {
                        li.Selected = true;
                    }
                    txtPhone2.Text = _cust.PartnerPhone2;
                    txtWebsite.Text = _cust.PartnerWebsite;
                    txtFax.Text = _cust.PartnerFax;

                    ddlInvoicePreference.SelectedValue = !string.IsNullOrEmpty(_cust.PartnerInvoicePreference) ? _cust.PartnerInvoicePreference : StatusInvoicePrefrences.DEFAULT;
                    ddlLang.SelectedValue = !string.IsNullOrEmpty(_cust.PartnerLang) ? _cust.PartnerLang : AppLanguageCode.DEFAULT;
                    dlCurrencyCode.SelectedValue = _cust.PartnerCurrencyCode;
                    dlTaxGroup.SelectedValue = _cust.PartnerTaxCode.ToString();
                    rblStatus.SelectedValue = _cust.PartnerStatus;

                    txtWorkEx.Text = _cust.ExtendedProperties.WorkExperience;
                    txtResonToJoin.Text = _cust.ExtendedProperties.ReasonToJoinStaff;
                    txtMedication.Text = _cust.ExtendedProperties.Medication;
                    txtIllnesses.Text = _cust.ExtendedProperties.Illnesses;
                    txtDOB.Text = BusinessUtility.GetDateTimeString(_cust.ExtendedProperties.DOB, DateFormat.MMddyyyy);
                    txtAlterTelephone.Text = _cust.ExtendedProperties.EmergencyAltTelephone;
                    txtEmName.Text = _cust.ExtendedProperties.EmergencyContactName;
                    txtEmTelephone.Text = _cust.ExtendedProperties.EmergencyTelephone;
                    txtFirstName.Text = _cust.ExtendedProperties.FirstName;
                    txtLastName.Text = _cust.ExtendedProperties.LastName;
                    txtMembership.Text = _cust.ExtendedProperties.MembershipID;
                    txtSpritualName.Text = _cust.ExtendedProperties.SpirtualName;
                    txtNationality.Text = _cust.ExtendedProperties.Nationality;
                    chkNewsLetter.Checked = _cust.ExtendedProperties.ReceiveNewsLetters;
                    txtEmRelationship.Text = _cust.ExtendedProperties.EmergencyRelationship;
                    rblHaveYouPartOfStaffBefore.SelectedValue = _cust.ExtendedProperties.IsPartOfStaffInPast ? "1" : "0";
                    rblSex.SelectedValue = _cust.ExtendedProperties.Sex;
                    chkEnableSocialPlugin.Checked = _cust.ExtendedProperties.EnableSocialPlugins;
                    txtEmailAddress2.Text = _cust.PartnerEmail2;
                    //ddlGender.SelectedValue = _cust.PartnerGender;
                    ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByValue(BusinessUtility.GetString(_cust.PartnerGender)));
                    txtFacebook.Text = _cust.FaceBook;
                    txtTiwtter.Text = _cust.Tiwtter;
                    txtLength.Text = _cust.Length;
                    txtShoulder.Text = _cust.Shoulder;
                    txtChest.Text = _cust.Chest;
                    txtBicep.Text = _cust.Bicep;
                    txtAlert.Text = _cust.Alert;
                    txtCustSince.Text = BusinessUtility.GetDateTimeString(_cust.PartnerSince, DateFormat.MMddyyyy);
                    txtLeagcyOpnAR.Text = _cust.legacyOpenAR;
                    txtLeagcyTotalSale.Text = _cust.legacyTotalSales;
                    if (_cust.Warehousecode != "")
                    {
                        dlWarehouse.SelectedIndex = dlWarehouse.Items.IndexOf(dlWarehouse.Items.FindByValue(BusinessUtility.GetString(_cust.Warehousecode)));
                    }

                    //txtCustActonym.Text = _cust.PartnerAcronyme;
                    if (liGuestType != null)
                    {
                        liGuestType.Selected = true;
                    }

                    if (!string.IsNullOrEmpty(_cust.ExtendedProperties.IsMember))
                    {
                        rblMember.SelectedValue = _cust.ExtendedProperties.IsMember;
                    }

                    //Retreive Sales Rep 
                    List<int> lstRep = _cust.GetCustomerSalesRep(this.PartnerID);
                    foreach (ListItem item in lbxSalesRepresentativs.Items)
                    {
                        if (lstRep.Contains(BusinessUtility.GetInt(item.Value)))
                        {
                            item.Selected = true;
                        }
                    }

                    ltStatus.Text = _cust.PartnerActive ? @"<img src=""../images/green.gif"">" : @"<img src=""../images/red.gif"">";
                    int recetOrder = _cust.GetRecentOrderID(dbHelp, this.PartnerID);
                    hlRecentOrder.Text = recetOrder > 0 ? recetOrder.ToString() : "N/A";
                    if (recetOrder > 0)
                    {
                        hlRecentOrder.NavigateUrl = string.Format("~/Sales/SalesEdit.aspx?SOID={0}", recetOrder);
                    }
                    double amtDue, availCredit, points;
                    amtDue = _cust.CustomerDueAmount(dbHelp, this.PartnerID);
                    UIHelper.FormatCurrencyLabel(ltAmountDue, amtDue);


                    if (this.PartnerType != StatusCustomerTypes.DISTRIBUTER)
                    {
                        amtDue += BusinessUtility.GetDouble(_cust.legacyOpenAR);
                    }

                    //ltAmountDue.Text = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(amtDue), _cust.PartnerCurrencyCode);
                    ltAmountDue.Text = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(amtDue), _comInfo.CompanyBasCur);

                    availCredit = _cust.CustomerCreditAvailable(dbHelp, this.PartnerID);
                    //UIHelper.FormatCurrencyLabel(ltAvailableCredit, availCredit);
                    //ltAvailableCredit.Text = string.Format("{0:C}", availCredit);
                    //ltAvailableCredit.Text = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(_cust.PartnerCreditAvailable) - BusinessUtility.GetDouble(amtDue), _cust.PartnerCurrencyCode);
                    ltAvailableCredit.Text = CurrencyFormat.GetReplaceCurrencySymbol(CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(_cust.PartnerCreditAvailable) - BusinessUtility.GetDouble(amtDue), _comInfo.CompanyBasCur));

                    points = new LoyalPartnerHistory().GetPartnerLoyaltyPoints(dbHelp, this.PartnerID);
                    UIHelper.FormatNumberLabel(ltPoints, null, points);
                    pnlPartnerHistory.Visible = true;


                    CustomerCredit objCustCrdt = new CustomerCredit();
                    //ltInstoreCredit.Text = CurrencyFormat.GetCompanyCurrencyFormat(objCustCrdt.GetAvailableCredit(this.PartnerID), _cust.PartnerCurrencyCode);
                    ltInstoreCredit.Text = CurrencyFormat.GetCompanyCurrencyFormat(objCustCrdt.GetAvailableCredit(this.PartnerID), _comInfo.CompanyBasCur);
                    if (objCustCrdt.ExistInstoreCreditHistory(null, this.PartnerID))
                    {
                        hdnShowHistory.Value = "1";

                        //showHistoryLink.Visible = true;
                        //hlIncrdHistory.Visible = true;
                        //hlIncrdHistory.Text = string.Format(@"<a id=""showHistoryLink"" onclick=""ShowInsCrdHistory();""  ><img src=""../Images/HistroyImage.jpg"" /></a>");
                    }
                    
                    Invoice objInvoice = new Invoice();
                    double custTotalSales = objInvoice.GetCustTotalSales(this.PartnerID);
                    if (this.PartnerType != StatusCustomerTypes.DISTRIBUTER)
                    {
                        custTotalSales += BusinessUtility.GetDouble(_cust.legacyTotalSales);
                    }
                    //ltTotalSales.Text = CurrencyFormat.GetCompanyCurrencyFormat(custTotalSales, _cust.PartnerCurrencyCode);
                    ltTotalSales.Text = CurrencyFormat.GetCompanyCurrencyFormat(custTotalSales, _comInfo.CompanyBasCur);

                    DateTime dtFrom = BusinessUtility.GetDateTime("01/01/" + DateTime.Now.Year, DateFormat.ddMMyyyy);
                    DateTime dtTo = BusinessUtility.GetDateTime("31/12/" + DateTime.Now.Year, DateFormat.ddMMyyyy);
                    double custAnnualSales = objInvoice.GetCustTotalSales(this.PartnerID, dtFrom, dtTo);
                    //ltAnnualSales.Text = CurrencyFormat.GetCompanyCurrencyFormat(custAnnualSales, _cust.PartnerCurrencyCode);
                    ltAnnualSales.Text = CurrencyFormat.GetCompanyCurrencyFormat(custAnnualSales, _comInfo.CompanyBasCur);
                }
                else
                {
                    //Set Searched Smart Give ID to Speritual Field Name
                    //txtSpritualName.Text = BusinessUtility.GetString(Request.QueryString["smartGvId"]);
                    txtCustSince.Text = BusinessUtility.GetDateTimeString(DateTime.Now, DateFormat.MMddyyyy);
                    if (CurrentUser.DefaultCompanyID > 0)
                    {
                        ddlCountry.SelectedValue = _comInfo.CompanyCountry;
                        _country.PopulateListControlWithStateCodes(dbHelp, ddlCountry.SelectedValue, ddlState, new ListItem(""));
                        ddlState.SelectedValue = _comInfo.CompanyState;
                        txtCountry.Text = _comInfo.CompanyCountry;
                        txtState.Text = _comInfo.CompanyState;

                        ddlPCountry.SelectedValue = _comInfo.CompanyCountry;
                        _country.PopulateListControlWithStateCodes(dbHelp, ddlPCountry.SelectedValue, ddlPState, new ListItem(""));
                        ddlPState.SelectedValue = _comInfo.CompanyState;
                        txtPCountry.Text = _comInfo.CompanyCountry;
                        txtPState.Text = _comInfo.CompanyState;

                        ddlSCountry.SelectedValue = _comInfo.CompanyCountry;
                        _country.PopulateListControlWithStateCodes(dbHelp, ddlSCountry.SelectedValue, ddlSState, new ListItem(""));
                        ddlSState.SelectedValue = _comInfo.CompanyState;
                        txtSCountry.Text = _comInfo.CompanyCountry;
                        txtSState.Text = _comInfo.CompanyState;

                        ddlPSCountry.SelectedValue = _comInfo.CompanyCountry;
                        _country.PopulateListControlWithStateCodes(dbHelp, ddlPSCountry.SelectedValue, ddlPSState, new ListItem(""));
                        ddlPSState.SelectedValue = _comInfo.CompanyState;
                        txtPSCountry.Text = _comInfo.CompanyCountry;
                        txtPSState.Text = _comInfo.CompanyState;

                        //Populate State Tax Code
                        _country.PopulateObject(dbHelp, _comInfo.CompanyCountry, _comInfo.CompanyState);

                        if (_country.StateGroupTaxCode > 0)
                        {
                            dlTaxGroup.SelectedValue = _country.StateGroupTaxCode.ToString();
                            dlCurrencyCode.SelectedValue = _comInfo.CompanyBasCur;
                        }
                    }
                    pnlPartnerHistory.Visible = false;
                }

                //Set Section visivility according
                switch (this.PartnerType)
                {
                    case StatusCustomerTypes.DISTRIBUTER:
                        ulStaffMemberInfo.Visible = false;
                        liDistributer1.Visible = true;
                        //liDistributer2.Visible = true;
                        //liDistributer3.Visible = true;
                        pnlAditionalInformation.Visible = true;
                        //liGuest1.Visible = false;
                        liGuest1.Visible = true;
                        liGuest2.Visible = false;
                        liGuest3.Visible = false;
                        divGuest1.Visible = false;
                        ltEmerGencyContact.Text = Resources.Resource.lblPrimaryContact;
                        pnlOtherInformationTab.Visible = false;
                        pnlSalesRepresentatives.Visible = true;
                        pnlAssignCategories.Visible = true;
                        pnlSocialMedia.Visible = false;//true;
                        lblCustName.Visible = false;
                        txtPartnerFullName.Visible = false;
                        rfvFullPartnerName.Enabled = false;
                        tdHdgCrdt.Visible = false;
                        tdHdgAmtDue.Visible = false;
                        tdCrdt.Visible = false;
                        tdAmtDue.Visible = false;
                        rfvEmailAddress.Enabled = false;
                        spnMailReq.Visible = false;
                        liLegacy.Visible = false;

                        txtPartnerFullName.Focus();
                        //txtCustActonym.Focus();
                        break;
                    default:
                        //ulStaffMemberInfo.Visible = this.GuestType == StatusGuestType.Staff;
                        //liDistributer1.Visible = false;
                        //pnlAditionalInformation.Visible = false;
                        ////liDistributer2.Visible = false;
                        ////liDistributer3.Visible = false;
                        //liGuest1.Visible = true;
                        //liGuest2.Visible = true;
                        //liGuest3.Visible = true;
                        //ltEmerGencyContact.Text = Resources.Resource.lblEmergencyContact;
                        //divGuest1.Visible = liGuestType != null;
                        //pnlOtherInformationTab.Visible = true;
                        //pnlSalesRepresentatives.Visible = false;
                        //pnlAssignCategories.Visible = false;
                        //pnlSocialMedia.Visible = false;
                        //txtFirstName.Focus();
                        ulStaffMemberInfo.Visible = false;
                        liDistributer1.Visible = true;
                        li1.Visible = false;
                        liSizeHdg.Visible = false;
                        liClothDesc5.Visible = false;
                        liClothDesc6.Visible = false;
                        //liDistributer2.Visible = true;
                        //liDistributer3.Visible = true;
                        pnlAditionalInformation.Visible = true;
                        //liGuest1.Visible = false;
                        liGuest1.Visible = true;
                        liGuest2.Visible = false;
                        liGuest3.Visible = false;
                        divGuest1.Visible = false;
                        ltEmerGencyContact.Text = Resources.Resource.lblPrimaryContact;
                        pnlOtherInformationTab.Visible = false;
                        pnlSalesRepresentatives.Visible = true;
                        pnlAssignCategories.Visible = true;
                        pnlSocialMedia.Visible = false;//true;
                        liPrimaryContact1.Visible = false;
                        liPrimaryContact2.Visible = false;
                        liPrimaryContact3.Visible = false;
                        liGuest1.Visible = false;
                        spnMailReq.Visible = true;

                        tdHdgCrdt.Visible = true;
                        tdHdgAmtDue.Visible = true;
                        tdCrdt.Visible = true;
                        tdAmtDue.Visible = true;
                        liLegacy.Visible = true;
                        txtPartnerFullName.Focus();
                        break;
                }

                switch (this.GuestType)
                {
                    case StatusGuestType.None:
                        //ltSectionTitle.Text = Resources.Resource.lblCustomerInfo;
                        ltSectionTitle.Text = Resources.Resource.lblVIPInformation;
                        break;
                    case StatusGuestType.Guest:
                        ltSectionTitle.Text = Resources.Resource.lblDistributorInformation;
                        //ltSectionTitle.Text = Resources.Resource.lblGuestInformation;
                        //pnlBillToAddress.Visible = false;
                        //pnlShipToAddress.Visible = false;
                        break;
                    case StatusGuestType.SpecialGuest:
                        ltSectionTitle.Text = Resources.Resource.lblGuestInformation;
                        pnlBillToAddress.Visible = false;
                        pnlShipToAddress.Visible = false;
                        break;
                    case StatusGuestType.Staff:
                        ltSectionTitle.Text = Resources.Resource.lblStaffInformation;
                        ltAddress.Text = Resources.Resource.lblAddress;
                        pnlBillToAddress.Visible = false;
                        pnlShipToAddress.Visible = false;
                        pnlOtherInformationTab.Visible = false;
                        break;
                    case StatusGuestType.CourseParticipants:
                        ltSectionTitle.Text = Resources.Resource.lblGuestInformation;
                        break;
                }
                //liTaxCur.Visible = (this.GuestType != StatusGuestType.Guest && this.GuestType != StatusGuestType.CourseParticipants && this.GuestType != StatusGuestType.SpecialGuest && this.GuestType != StatusGuestType.Staff);
                txtPartnerFullName.Focus();
                //txtCustActonym.Focus();
            }
        }
        catch
        {
            throw;
        }
    }

    private void SetData()
    {
        _cust.PartnerActive = true;
        _cust.PartnerAdditionPhone = txtPhone2.Text;
        _cust.PartnerCommissionCode = string.Empty;
        //_cust.PartnerAcronyme = txtCustActonym.Text;
        _cust.PartnerCourierCode = string.Empty;
        _cust.PartnerCreatedBy = CurrentUser.UserID;
        //_cust.PartnerCreditAvailable = 0D;
        _cust.PartnerCreditAvailable = BusinessUtility.GetDouble(txtAvailableCredit.Text);
        _cust.PartnerCurrencyCode = dlCurrencyCode.SelectedValue;
        _cust.PartnerDesc1 = string.Empty;
        _cust.PartnerDesc2 = string.Empty;
        _cust.PartnerDiscount = BusinessUtility.GetInt(txtDiscPer.Text);
        _cust.PartnerEmail = txtEmailAddress.Text;
        _cust.PartnerFax = txtFax.Text;
        _cust.PartnerID = this.PartnerID;
        _cust.PartnerInvoiceNetTerms = ddlNetTerms.SelectedIndex > -1 ? ddlNetTerms.SelectedItem.Text : string.Empty; //txtInvoiceNetTerms.Text;
        _cust.PartnerInvoicePreference = ddlInvoicePreference.SelectedValue;
        _cust.PartnerLang = ddlLang.SelectedValue;
        _cust.PartnerLastUpdatedBy = CurrentUser.UserID;
        _cust.PartnerLocked = false;
        _cust.PartnerLongName = this.GetCustomerName(); //!string.IsNullOrEmpty(txtPartnerFullName.Text.Trim()) ? txtPartnerFullName.Text : string.Format("{0} {1}", txtLastName.Text, txtFirstName.Text);
        _cust.PartnerNote = txtCustomerNote.Text;
        _cust.PartnerPhone = txtTelephone.Text;
        _cust.PartnerPhone2 = txtPhone2.Text;
        _cust.PartnerShipBlankPref = false;
        _cust.PartnerStatus = rblStatus.SelectedValue;
        _cust.PartnerTaxCode = BusinessUtility.GetInt(dlTaxGroup.SelectedValue);
        _cust.PartnerType = this.PartnerTypeID;
        _cust.PartnerValidated = true;
        _cust.PartnerValue = string.Empty;
        _cust.PartnerWebsite = txtWebsite.Text;
        _cust.PartnerEmail2 = txtEmailAddress2.Text;
        _cust.PartnerGender = ddlGender.SelectedValue;
        _cust.FaceBook = txtFacebook.Text;
        _cust.Tiwtter = txtTiwtter.Text;
        _cust.Length = txtLength.Text;
        _cust.Shoulder = txtShoulder.Text;
        _cust.Chest = txtChest.Text;
        _cust.Bicep = txtBicep.Text;
        _cust.Alert = txtAlert.Text;
        _cust.PartnerSince = BusinessUtility.GetDateTime(txtCustSince.Text, DateFormat.MMddyyyy);
        _cust.Warehousecode = BusinessUtility.GetString(dlWarehouse.SelectedValue);
        _cust.legacyOpenAR = txtLeagcyOpnAR.Text;
        _cust.legacyTotalSales = txtLeagcyTotalSale.Text;

        _cust.ExtendedProperties.Age = 0;
        _cust.ExtendedProperties.DOB = BusinessUtility.GetDateTime(txtDOB.Text, DateFormat.MMddyyyy);
        _cust.ExtendedProperties.EmergencyAltTelephone = txtAlterTelephone.Text;
        _cust.ExtendedProperties.EmergencyContactName = txtEmName.Text;
        _cust.ExtendedProperties.EmergencyRelationship = txtEmRelationship.Text;
        _cust.ExtendedProperties.EmergencyTelephone = txtEmTelephone.Text;
        _cust.ExtendedProperties.FirstName = txtFirstName.Text;

        if (rblIsSpecialGuest.Visible)
        {
            _cust.ExtendedProperties.GuestType = BusinessUtility.GetInt(rblIsSpecialGuest.SelectedValue);
        }
        else
        {
            _cust.ExtendedProperties.GuestType = (int)this.GuestType;
        }
        _cust.ExtendedProperties.Illnesses = txtIllnesses.Text;
        _cust.ExtendedProperties.IsMember = rblMember.SelectedValue;
        _cust.ExtendedProperties.IsPartOfStaffInPast = rblHaveYouPartOfStaffBefore.SelectedValue == "1";
        _cust.ExtendedProperties.LastName = txtLastName.Text;
        _cust.ExtendedProperties.FirstName = txtFirstName.Text;
        _cust.ExtendedProperties.Medication = txtMedication.Text;
        _cust.ExtendedProperties.MembershipID = txtMembership.Text;
        _cust.ExtendedProperties.Nationality = txtNationality.Text;
        _cust.ExtendedProperties.PartnerID = this.PartnerID;
        _cust.ExtendedProperties.ReasonToJoinStaff = txtResonToJoin.Text;
        _cust.ExtendedProperties.ReceiveNewsLetters = chkNewsLetter.Checked;
        _cust.ExtendedProperties.Sex = rblSex.SelectedValue;
        _cust.ExtendedProperties.SpirtualName = txtSpritualName.Text;
        _cust.ExtendedProperties.WorkExperience = txtWorkEx.Text;
        _cust.ExtendedProperties.EnableSocialPlugins = chkEnableSocialPlugin.Checked;
        if (Request.QueryString.AllKeys.Contains("create_POS") && Request.QueryString["create_POS"] == "1")
        {
            ////Set Addresses
            //_billToAddress.AddressCity = txtPCity.Text;
            //_billToAddress.AddressCountry = txtPCountry.Text;
            //_billToAddress.AddressLine1 = txtPAddressLine1.Text;
            //_billToAddress.AddressLine2 = txtPAddressLine2.Text;
            //_billToAddress.AddressLine3 = string.Empty;
            //_billToAddress.AddressPostalCode = txtPPostalCode.Text;
            //_billToAddress.AddressState = txtPState.Text;
            //_billToAddress.AddressRef = this.AddressRef;
            //_billToAddress.AddressSourceID = this.PartnerID;
            //_billToAddress.AddressType = AddressType.BILL_TO_ADDRESS;
            //_billToAddress.FirstName = txtBillFirstName.Text;
            //_billToAddress.LastName = txtBillLastName.Text;
            //_billToAddress.AddressID = BusinessUtility.GetInt(hdnBillAddID.Value);

            //_shipToAddress.AddressCity = txtPSCity.Text;
            //_shipToAddress.AddressCountry = txtPSCountry.Text;
            //_shipToAddress.AddressLine1 = txtPSAddressLine1.Text;
            //_shipToAddress.AddressLine2 = txtPSAddressLine2.Text;
            //_shipToAddress.AddressLine3 = string.Empty;
            //_shipToAddress.AddressPostalCode = txtPSPostalCode.Text;
            //_shipToAddress.AddressState = txtPSState.Text;
            //_shipToAddress.AddressRef = this.AddressRef;
            //_shipToAddress.AddressSourceID = this.PartnerID;
            //_shipToAddress.AddressType = AddressType.SHIP_TO_ADDRESS;
            //_shipToAddress.FirstName = txtShipFirstName.Text;
            //_shipToAddress.LastName = txtShipLastName.Text;
            //_shipToAddress.AddressID = BusinessUtility.GetInt(hdnShpAddID.Value);

            //Set Addresses
            _billToAddress.AddressCity = txtCity.Text;
            _billToAddress.AddressCountry = txtCountry.Text;
            _billToAddress.AddressLine1 = txtAddressLine1.Text;
            _billToAddress.AddressLine2 = txtAddressLine2.Text;
            _billToAddress.AddressLine3 = string.Empty;
            _billToAddress.AddressPostalCode = txtPostalCode.Text;
            _billToAddress.AddressState = txtState.Text;
            _billToAddress.AddressRef = this.AddressRef;
            _billToAddress.AddressSourceID = this.PartnerID;
            _billToAddress.AddressType = AddressType.BILL_TO_ADDRESS;
            _billToAddress.FirstName = txtBillFirstName.Text;
            _billToAddress.LastName = txtBillLastName.Text;
            _billToAddress.AddressID = BusinessUtility.GetInt(hdnBillAddID.Value);

            _shipToAddress.AddressCity = txtSCity.Text;
            _shipToAddress.AddressCountry = txtSCountry.Text;
            _shipToAddress.AddressLine1 = txtSAddressLine1.Text;
            _shipToAddress.AddressLine2 = txtSAddressLine2.Text;
            _shipToAddress.AddressLine3 = string.Empty;
            _shipToAddress.AddressPostalCode = txtSPostalCode.Text;
            _shipToAddress.AddressState = txtSState.Text;
            _shipToAddress.AddressRef = this.AddressRef;
            _shipToAddress.AddressSourceID = this.PartnerID;
            _shipToAddress.AddressType = AddressType.SHIP_TO_ADDRESS;
            _shipToAddress.FirstName = txtShipFirstName.Text;
            _shipToAddress.LastName = txtShipLastName.Text;
            _shipToAddress.AddressID = BusinessUtility.GetInt(hdnShpAddID.Value);
        }
        else
        {
            //Set Addresses
            _billToAddress.AddressCity = txtCity.Text;
            _billToAddress.AddressCountry = txtCountry.Text;
            _billToAddress.AddressLine1 = txtAddressLine1.Text;
            _billToAddress.AddressLine2 = txtAddressLine2.Text;
            _billToAddress.AddressLine3 = string.Empty;
            _billToAddress.AddressPostalCode = txtPostalCode.Text;
            _billToAddress.AddressState = txtState.Text;
            _billToAddress.AddressRef = this.AddressRef;
            _billToAddress.AddressSourceID = this.PartnerID;
            _billToAddress.AddressType = AddressType.BILL_TO_ADDRESS;
            _billToAddress.FirstName = txtBillFirstName.Text;
            _billToAddress.LastName = txtBillLastName.Text;
            _billToAddress.AddressID = BusinessUtility.GetInt(hdnBillAddID.Value);

            _shipToAddress.AddressCity = txtSCity.Text;
            _shipToAddress.AddressCountry = txtSCountry.Text;
            _shipToAddress.AddressLine1 = txtSAddressLine1.Text;
            _shipToAddress.AddressLine2 = txtSAddressLine2.Text;
            _shipToAddress.AddressLine3 = string.Empty;
            _shipToAddress.AddressPostalCode = txtSPostalCode.Text;
            _shipToAddress.AddressState = txtSState.Text;
            _shipToAddress.AddressRef = this.AddressRef;
            _shipToAddress.AddressSourceID = this.PartnerID;
            _shipToAddress.AddressType = AddressType.SHIP_TO_ADDRESS;
            _shipToAddress.FirstName = txtShipFirstName.Text;
            _shipToAddress.LastName = txtShipLastName.Text;
            _shipToAddress.AddressID = BusinessUtility.GetInt(hdnShpAddID.Value);
        }
    }

    public int PartnerID
    {
        get
        {
            int custid = 0;
            int.TryParse(Request.QueryString["custid"], out custid);
            return custid;
        }
    }

    private StatusGuestType GuestType
    {
        get
        {
            return QueryStringHelper.GetGuestType("gType");
        }
    }

    private string PartnerType
    {
        get
        {
            return QueryStringHelper.GetPartnerType("pType");
        }
    }

    private int PartnerTypeID
    {
        get
        {
            switch (this.PartnerType)
            {
                case StatusCustomerTypes.END_CLINET:
                    return (int)PartnerTypeIDs.EndClient;
                default:
                    return (int)PartnerTypeIDs.Distributer;
            }
        }
    }

    public string AddressRef
    {
        get
        {
            switch (this.PartnerType)
            {
                case StatusCustomerTypes.END_CLINET:
                    return AddressReference.END_CLIENT;
                default:
                    return AddressReference.DISTRIBUTER;
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                if ((BusinessUtility.GetString(Request.QueryString["InvokeSrc"]).ToUpper() == "POS".ToUpper() && BusinessUtility.GetString(Request.QueryString["srchKey"]).ToUpper() != ""))
                {
                    if (txtAddressLine1.Text != "" && txtCity.Text != "" && txtPostalCode.Text != "" && txtCountry.Text != "" && txtState.Text != "" && (txtSAddressLine1.Text == "" || txtSCity.Text == "" || txtSPostalCode.Text == "" || txtSCountry.Text == "" || txtSState.Text == ""))
                    {
                        MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblPleaseValidateAddress);
                        return;
                    }
                }

                SetData();
                List<Addresses> lstAddress = new List<Addresses>();
                lstAddress.Add(_billToAddress);
                lstAddress.Add(_shipToAddress);
                if (this.PartnerID > 0)
                {
                    string dulicateEmailAddress = System.Configuration.ConfigurationManager.AppSettings["DuplicatEmailAddress"];
                    _cust.PartnerEmail = _cust.PartnerEmail.ToLower().Trim() == dulicateEmailAddress.ToLower().Trim() ? string.Format("{0:yyyyMMddhhmmss}_{1}", DateTime.Now, dulicateEmailAddress) : _cust.PartnerEmail;
                    _cust.Update(dbHelp, CurrentUser.UserID, lstAddress);
                    //Assign Sales Representetives
                    List<int> lstRep = new List<int>();
                    foreach (ListItem item in lbxSalesRepresentativs.Items)
                    {
                        if (item.Selected)
                        {
                            lstRep.Add(BusinessUtility.GetInt(item.Value));
                        }
                    }
                    _cust.AssignSalesRepresentatives(dbHelp, _cust.PartnerID, this.PartnerType, lstRep);
                    this.AssignCategories(_cust.PartnerID);

                    //Set Custom fields Data
                    CustomFields1.Save();

                    if ((BusinessUtility.GetString(Request.QueryString["InvokeSrc"]).ToUpper() == "POS".ToUpper() && BusinessUtility.GetString(Request.QueryString["srchKey"]).ToUpper() != ""))
                    {
                        Response.Redirect(string.Format("~/POS/POS.aspx?srchKey={0}", BusinessUtility.GetString(Request.QueryString["srchKey"])), false);
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.CMCustomerUpdatedSuccessfully);
                        Response.Redirect(string.Format("~/Partner/ViewCustomer.aspx?pType={0}&gType={1}", this.PartnerType, (int)this.GuestType), false);
                    }
                }
                else
                {
                    string dulicateEmailAddress = System.Configuration.ConfigurationManager.AppSettings["DuplicatEmailAddress"];
                    _cust.PartnerEmail = _cust.PartnerEmail.ToLower().Trim() == dulicateEmailAddress.ToLower().Trim() ? string.Format("{0:yyyyMMddhhmmss}_{1}", DateTime.Now, dulicateEmailAddress) : _cust.PartnerEmail;
                    _cust.Insert(dbHelp, CurrentUser.UserID, lstAddress);
                    //Assign Sales Representetives
                    List<int> lstRep = new List<int>();
                    foreach (ListItem item in lbxSalesRepresentativs.Items)
                    {
                        if (item.Selected)
                        {
                            lstRep.Add(BusinessUtility.GetInt(item.Value));
                        }
                    }
                    _cust.AssignSalesRepresentatives(dbHelp, _cust.PartnerID, this.PartnerType, lstRep);
                    this.AssignCategories(_cust.PartnerID);

                    //Set Custom fields Data



                    if (_cust.PartnerID > 0 && Request.QueryString.AllKeys.Contains("cmd") && Request.QueryString["cmd"].ToLower().Trim() == "changeGuest".ToLower())
                    {
                        int riID = 0;
                        int.TryParse(Request.QueryString["riID"], out riID);
                        new ReservationItems().ChangeGuest(_cust.PartnerID, riID);

                        Response.Redirect(string.Format("~/Sales/SalesEdit.aspx?SOID={0}", Request.QueryString["ordID"]), false);
                    }
                    else if (Request.QueryString.AllKeys.Contains("create_sales") && Request.QueryString["create_sales"] == "1")
                    {
                        Response.Redirect(string.Format("~/Sales/SalesEdit.aspx?custID={0}&ComID={1}&whsID={2}", _cust.PartnerID, CurrentUser.DefaultCompanyID, CurrentUser.UserDefaultWarehouse), false);
                    }
                    else if (Request.QueryString.AllKeys.Contains("create_POS") && Request.QueryString["create_POS"] == "1")
                    {
                        Response.Redirect(string.Format("~/POS/POS.aspx?custID={0}", _cust.PartnerID), false);
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.CMCustomerAddedSuccessfully);
                        Response.Redirect(string.Format("~/Partner/ViewCustomer.aspx?pType={0}&gType={1}", this.PartnerType, (int)this.GuestType), false);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == CustomExceptionCodes.DUPLICATE_KEY)
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgEmailIDAlreadyExists);
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
                }
            }
        }
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if ((BusinessUtility.GetString(Request.QueryString["InvokeSrc"]).ToUpper() == "POS".ToUpper() && BusinessUtility.GetString(Request.QueryString["srchKey"]).ToUpper() != ""))
        {
            Response.Redirect(string.Format("~/POS/POS.aspx?srchKey={0}", BusinessUtility.GetString(Request.QueryString["srchKey"])), false);
            return;
        }

        if (Request.QueryString.AllKeys.Contains<string>("create_sales") && Request.QueryString["create_sales"] == "1")
        {
            Response.Redirect("~/Sales/ViewSalesOrder.aspx");
        }
        Response.Redirect(string.Format("~/Partner/ViewCustomer.aspx?pType={0}&gType={1}", this.PartnerType, (int)this.GuestType));
    }

    private string GetCustomerName()
    {
        if (liGuest1.Visible && (!string.IsNullOrEmpty(txtFirstName.Text.Trim()) || !string.IsNullOrEmpty(txtLastName.Text.Trim())))
        {
            return string.Format("{0} {1}", txtLastName.Text, txtFirstName.Text);
        }
        return txtPartnerFullName.Text;
    }

    #region Categoroy Methods
    private void FillCategoryDataList()
    {
        lBoxAssignedCateg.Items.Clear();

        BLLCategories objCateg = new BLLCategories();
        int lang = (int)ApplicationLanguage.English;
        if (Globals.CurrentAppLanguageCode == "fr")
        {
            lang = (int)ApplicationLanguage.French;
        }

        System.Data.DataTable dtCategories = new System.Data.DataTable();
        dtCategories = objCateg.GetAllCategories(lang);
        lBoxAssignedCateg.DataSource = objCateg.GetAllCategories(lang);
        lBoxAssignedCateg.DataTextField = "TextDescription";
        lBoxAssignedCateg.DataValueField = "CategoryID";
        lBoxAssignedCateg.DataBind();

        if (this.PartnerID > 0)
        {
            List<int> categList = new List<int>();
            categList = objCateg.GetAllassignedCategories(this.PartnerID);

            foreach (var item in categList)
            {
                ListItem li = lBoxAssignedCateg.Items.FindByValue(item.ToString());
                if (li != null)
                {
                    li.Selected = true;
                }
            }
        }
    }

    private void AssignCategories(int partnerID)
    {
        BLLCategories objCateg = new BLLCategories();
        List<int> categList = new List<int>();

        foreach (ListItem lItem in lBoxAssignedCateg.Items)
        {
            if (lItem.Selected)
            {
                categList.Add(BusinessUtility.GetInt(lItem.Value));
            }
        }

        objCateg.AssignCategoriesToCustomer(categList.ToArray(), partnerID);
    }


    #endregion


    protected void lnkAddCategory_Click(object sender, EventArgs e)
    {
        NameValueCollection nvc = HttpUtility.ParseQueryString(string.Empty);
        nvc["returnUrl"] = Server.UrlEncode(Request.RawUrl);
        Response.Redirect("~/Admin/AddEditCategories.aspx?" + nvc.ToString());
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            try
            {
                Partners part = new Partners();
                part.Delete(this.PartnerID, false);
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgDeleteSuccess);
                Response.Redirect(string.Format("~/Partner/ViewCustomer.aspx?pType={0}&gType={1}", this.PartnerType, (int)this.GuestType), false);
            }
            catch
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblAccessDenied);
        }
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlState.Items.Clear();
        if (ddlCountry.SelectedIndex > 0)
        {
            _country.PopulateListControlWithStateCodes(null, ddlCountry.SelectedValue, ddlState, new ListItem(""));
        }
    }

    protected void ddlSCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSState.Items.Clear();
        if (ddlSCountry.SelectedIndex > 0)
        {
            _country.PopulateListControlWithStateCodes(null, ddlSCountry.SelectedValue, ddlSState, new ListItem(""));
        }
    }

    protected void ddlSState_SelectedIndexChanged(object sender, EventArgs e)
    {
        string stateCode = ddlSState.SelectedValue;
        if (!string.IsNullOrEmpty(stateCode))
        {
            _country.PopulateObject(null, ddlSCountry.SelectedValue, stateCode);
            dlTaxGroup.SelectedValue = _country.StateGroupTaxCode.ToString();
        }
    }
}
<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="ViewAlert.aspx.vb" Inherits="Partner_ViewAlert" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="Resources.Resource" %>

<%@ Register src="../Controls/CommonSearch/CRMSearch.ascx" tagname="CRMSearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>
    <uc1:CRMSearch ID="CRMSearch1" runat="server" />
</asp:Content>

<asp:Content ID="cntViewAlert" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle" onkeypress="return disableEnterKey(event)">
        <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <h2>
                        <asp:Literal ID="lblTitle" runat="server"></asp:Literal>
                    </h2>
                </td>
                <td align="right">
                    <asp:UpdateProgress runat="server" ID="upMaster" DisplayAfter="10">
                        <ProgressTemplate>
                            <img src="../Images/wait22trans.gif" alt=""/>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
    </div>
    <div class="divMainContent" >
        <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left">
                    
                </td>
                <td width="20%" align="right" style="padding-right: 15px;">
                    <asp:LinkButton ID="lnkAddInternalUser" runat="server" Visible="false"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="2" height="350" valign="top">
                    <asp:GridView ID="grdAlert" runat="server" AllowSorting="True" DataSourceID="sqldsAlert"
                        AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0"
                        GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
                        CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="AlertID" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="AlertDateTime" HeaderText="<%$ Resources:Resource, grdCMAlertDateTime %>"
                                ReadOnly="True" DataFormatString="{0: dd MMM yyyy HH:mm:ss}" SortExpression="AlertDateTime">
                                <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ContactName" HeaderText="<%$ Resources:Resource, grdCMContactName %>"
                                ReadOnly="True" SortExpression="ContactName">
                                <ItemStyle Width="200px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdCMAlertNote %>" HeaderStyle-ForeColor="#ffffff"
                                HeaderStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <asp:Label ID="lblNote" ForeColor="black" runat="server" Text='<%# Eval("AlertNote") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" Wrap="true" Width="300px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="AlertRefType" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grdCMAlertType %>"
                                ReadOnly="True" SortExpression="AlertRefType">
                                <ItemStyle Width="300px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdCMEdit %>" Visible="false"
                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("AlertID") %>'
                                        CommandName="Edit" ImageUrl="~/images/edit_icon.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdCMDelete %>" HeaderStyle-ForeColor="#ffffff"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                        CommandArgument='<%# Eval("AlertID") %>' CommandName="Delete" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="grid_footer" />
                        <RowStyle CssClass="grid_rowstyle" />
                        <PagerStyle CssClass="grid_footer" />
                        <HeaderStyle CssClass="grid_header" Height="26px" />
                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                        <PagerSettings PageButtonCount="20" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqldsAlert" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <br />    
</asp:Content>

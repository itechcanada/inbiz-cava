﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Partner_ViewCustomer : BasePage
{
    Partners _part = new Partners();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdCustomers))
        {
            txtSearch.Text = Request.QueryString.AllKeys.Contains("sk") ? Request.QueryString["sk"] : string.Empty;

            //Fill Dropdowns
            DropDownHelper.FillDropdown(ddlStatus, "CU", "dlSch", Globals.CurrentAppLanguageCode, null);

            if (this.GuestType == StatusGuestType.Staff)//Fill Search Fields for Staff
            {
                DropDownHelper.FillDropdown(ddlSearchFields, "CU", "dlSh3", Globals.CurrentAppLanguageCode, null);
            }
            else if (this.GuestType == StatusGuestType.SpecialGuest || this.GuestType == StatusGuestType.Guest || this.GuestType == StatusGuestType.CourseParticipants)//Fill Search Fields for Gu
            {
                DropDownHelper.FillDropdown(ddlSearchFields, "CU", "dlSh4", Globals.CurrentAppLanguageCode, null);
            }
            else
            {
                DropDownHelper.FillDropdown(ddlSearchFields, "CU", "dlSh2", Globals.CurrentAppLanguageCode, null);
            }

            

            if (this.PartnerType == StatusCustomerTypes.END_CLINET && this.GuestType == StatusGuestType.Staff)
            {
                btnAdd.Text = Resources.Resource.lblAddStaffMember;
            }
            else if (this.PartnerType == StatusCustomerTypes.END_CLINET &&
                (this.GuestType == StatusGuestType.Guest ||
                this.GuestType == StatusGuestType.SpecialGuest ||
                this.GuestType == StatusGuestType.CourseParticipants))
            {
                //btnAdd.Text = Resources.Resource.lblAddGuestMember;
                btnAdd.Text = Resources.Resource.btnAddNewDistributor;
                liAdmin.Text = Resources.Resource.ViewDistributor;
            }
            else
            {
                btnAdd.Text = Resources.Resource.btnAddNewVIP;
                liAdmin.Text = Resources.Resource.TitleCMViewCustomer;
            }

            txtSearch.Focus();

            // Get User Grid Height
            InbizUser _usr = new InbizUser();
            _usr.PopulateObject(CurrentUser.UserID);
            if (BusinessUtility.GetInt(_usr.UserGridHeight) > 0)
            {
                grdCustomers.Height = _usr.UserGridHeight;
            }
        }
    }

    CustomerCredit _cc = new CustomerCredit();
    protected void grdCustomers_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        _part.PopulateObject(BusinessUtility.GetInt(e.RowKey));
        int idxDSStatusCol = 9;
        if (e.ColumnIndex == 2)
        {
            if (_part.ExtendedProperties.GuestType == (int)StatusGuestType.CourseParticipants || _part.ExtendedProperties.GuestType == (int)StatusGuestType.Guest ||
                        _part.ExtendedProperties.GuestType == (int)StatusGuestType.SpecialGuest || _part.ExtendedProperties.GuestType == (int)StatusGuestType.Staff)
            {
                string url = string.Format("CustomerEdit.aspx?custid={0}&pType={1}&gType={2}&section={3}", e.RowKey, Globals.GetPartnerType(_part.PartnerType), _part.ExtendedProperties.GuestType, (int)CRMSectionKey.CustomerSoHistory);
                e.CellHtml = string.Format(@"<a href=""{0}"" >{1}</a>", url, e.CellHtml);
            }           
        }
        else if (e.ColumnIndex == 6)//Fotmat Status Column image
        {
            e.CellHtml = string.Format(@"<img src=""{0}"" />", ResolveUrl(string.Format("~/images/{0}", e.RowValues[idxDSStatusCol])));
        }
        else if (e.ColumnIndex == 7)
        {
            int soid = _part.GetRecentOrderID(null, BusinessUtility.GetInt(e.RowKey));
            if (soid > 0)
            {
                e.CellHtml = string.Format("<a href='{0}'>{1}</a>", ResolveUrl(string.Format("~/Sales/SalesEdit.aspx?SOID={0}", soid)), soid);
            }
            else
            {
                e.CellHtml = "--";
            }
        }
        else if (e.ColumnIndex == 8)
        {
            //e.CellHtml = string.Format("{0:F}", _cc.GetAvailableCredit(BusinessUtility.GetInt(e.RowKey)));
            //e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
            DbHelper dbHelp = new DbHelper(true);
            double amtDue;
            amtDue = _part.CustomerDueAmount(dbHelp, BusinessUtility.GetInt(e.RowKey));
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(_part.PartnerCreditAvailable) - BusinessUtility.GetDouble(amtDue), _part.PartnerCurrencyCode);

        }
        else if (e.ColumnIndex == 9)
        {
            //e.CellHtml = string.Format("{0:F}", _cc.GetAvailableCredit(BusinessUtility.GetInt(e.RowKey)));
            //e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(e.CellHtml), _part.PartnerCurrencyCode);
        }
        else if (e.ColumnIndex == 10) //Format Edit Link
        {
            string editUrl = "";

            if (_part.PartnerType == 1)
            {
                editUrl = string.Format("CustomerEdit.aspx?custid={0}&pType={1}&gType={2}", e.RowKey, Globals.GetPartnerType(_part.PartnerType), _part.ExtendedProperties.GuestType);
            }
            else
            {
                editUrl = string.Format("CustomerEdit.aspx?custid={0}&pType={1}&gType={2}", e.RowKey, Globals.GetPartnerType(_part.PartnerType), 1);
            }
            e.CellHtml = string.Format(@"<a href=""{0}"">{1}</a>", editUrl, Resources.Resource.lblEdit);
        }
        else if(e.ColumnIndex == 11) //Format Delete Link
        {
            string delUrl = string.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_CUSTOMER, e.RowKey, "reloadGrid");
            e.CellHtml = string.Format(@"<a class=""pop_delete"" href=""{0}"">{1}</a>", delUrl, Resources.Resource.CmdCssDelete);
        }
    }
   
    protected void grdCustomers_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        bool isSalesRescricted = CurrentUser.IsInRole(RoleID.SALES_REPRESENTATIVE) && BusinessUtility.GetBool(Session["SalesRepRestricted"]);
        bool includeStaff = Request.QueryString.AllKeys.Contains("includeStaff") && Request.QueryString["includeStaff"] == "true";
        string showActive = !string.IsNullOrEmpty(Request.QueryString[ddlStatus.ClientID]) ? Request.QueryString[ddlStatus.ClientID] : "1";
        string searchField = !string.IsNullOrEmpty(Request.QueryString[ddlSearchFields.ClientID]) ? Request.QueryString[ddlSearchFields.ClientID] : null;
        string searchText = !string.IsNullOrEmpty(Request.QueryString[txtSearch.ClientID]) ? Request.QueryString[txtSearch.ClientID] : null;
        switch (this.PartnerType)
        {
            case StatusCustomerTypes.DISTRIBUTER:
                sdsCustomers.SelectCommand = _part.GetSearchSql(sdsCustomers.SelectParameters, searchField, searchText, CurrentUser.UserID, isSalesRescricted, PartnerTypeIDs.Distributer, this.GuestType, showActive, includeStaff);
                break;
            case StatusCustomerTypes.END_CLINET:
                sdsCustomers.SelectCommand = _part.GetSearchSql(sdsCustomers.SelectParameters, searchField, searchText, CurrentUser.UserID, isSalesRescricted, PartnerTypeIDs.EndClient, this.GuestType, showActive, includeStaff);
                break;
            case StatusCustomerTypes.LEADS:
                sdsCustomers.SelectCommand = _part.GetSearchSql(sdsCustomers.SelectParameters, searchField, searchText, CurrentUser.UserID, isSalesRescricted, PartnerTypeIDs.Leads, this.GuestType, showActive, includeStaff);
                break;
            case StatusCustomerTypes.RESELLER:
                sdsCustomers.SelectCommand = _part.GetSearchSql(sdsCustomers.SelectParameters, searchField, searchText, CurrentUser.UserID, isSalesRescricted, PartnerTypeIDs.Reseller, this.GuestType, showActive, includeStaff);
                break;
            default:
                sdsCustomers.SelectCommand = _part.GetSearchSql(sdsCustomers.SelectParameters, searchField, searchText, CurrentUser.UserID, isSalesRescricted, PartnerTypeIDs.Distributer, this.GuestType, showActive, includeStaff);
                break;
        }
        
    }

    public StatusGuestType GuestType
    {
        get
        {
            return QueryStringHelper.GetGuestType("gType");
        }
    }

    public string PartnerType
    {
        get
        {
            return QueryStringHelper.GetPartnerType("pType");            
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("CustomerEdit.aspx?pType={0}&gType={1}", this.PartnerType, (int)this.GuestType));
    }
}
Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Partial Class Partner_ViewAlert
    Inherits BasePage

    ' On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If

        lblMsg.Text = ""
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If
        
        lblTitle.Text = CMTitleAlerts
        If Not Page.IsPostBack Then
            subFillGrid()
        End If

        'clsGrid.MaintainParams(Page, grdAlert, SearchPanel, lblMsg, imgSearch)

    End Sub
    'Populate Grid
    Public Sub subFillGrid()
        Dim objAlert As New clsAlerts
        sqldsAlert.SelectCommand = objAlert.funFillGrid()  'fill Alert Record
        objAlert = Nothing
    End Sub
    'On Page Index Change
    Protected Sub grdAlert_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdAlert.PageIndexChanging
        subFillGrid()
    End Sub
    'On Row Data Bound
    Protected Sub grdAlert_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAlert.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & CMConfirmDeleteAlerts & " ')")

            Dim lblNote As Label = CType(e.Row.FindControl("lblNote"), Label)
            lblNote.Text = CType(e.Row.FindControl("lblNote"), Label).Text
            lblNote.Text = lblNote.Text.Replace(Environment.NewLine, "<br />")

            If e.Row.Cells(3).Text = "-" Then
                e.Row.Cells(3).Text = "Customer"
            ElseIf e.Row.Cells(3).Text = "RCV" Then
                e.Row.Cells(3).Text = "Receiving"
            ElseIf e.Row.Cells(3).Text = "COL" Then
                e.Row.Cells(3).Text = "Collection"
            ElseIf e.Row.Cells(3).Text = "SO" Then
                e.Row.Cells(3).Text = "Sales Quotation"
            ElseIf e.Row.Cells(3).Text = "PO" Then
                e.Row.Cells(3).Text = "Purchase Order"
            ElseIf e.Row.Cells(3).Text = "POA" Then
                e.Row.Cells(3).Text = "Purchase Order Approved"
            ElseIf e.Row.Cells(3).Text = "SOA" Then
                e.Row.Cells(3).Text = "Sales Order Approved"
            ElseIf e.Row.Cells(3).Text = "STM" Then
                e.Row.Cells(3).Text = "Sales Followup Activity"
            ElseIf e.Row.Cells(3).Text = "JOB" Then
                e.Row.Cells(3).Text = "Job Planning"
            ElseIf e.Row.Cells(3).Text = "ALE" Then
                e.Row.Cells(3).Text = lblAssignLeads
            End If
        End If
    End Sub
    'Deleting
    Protected Sub grdAlert_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdAlert.RowDeleting
        Dim strAlertID As String = grdAlert.DataKeys(e.RowIndex).Value.ToString()
        lblMsg.Text = CMAlertDeletedSuccessfully
        Dim objAlert As New clsAlerts
        sqldsAlert.DeleteCommand = objAlert.funDeleteAlert(strAlertID) 'Delete Alert
        objAlert = Nothing
        subFillGrid()
    End Sub
    'Editing
    Protected Sub grdAlert_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdAlert.RowEditing
        
    End Sub
    'Sorting
    Protected Sub grdAlert_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdAlert.Sorting
        subFillGrid()
    End Sub
    ' Sql Data Source Event Track
    Protected Sub sqldsAlert_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsAlert.Selected
        If e.AffectedRows = 0 Then
            lblMsg.Text = CMNoDataFound
        End If
    End Sub
   
    ' On Search Image Click
    'Protected Sub imgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
    '    subFillGrid()
    '    lblMsg.Text = ""
    '    txtSearch.Focus()
    'End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
End Class

Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Partial Class Partner_AddEditContact
    Inherits BasePage
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If

        lblError.Text = ""
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If

        If Request.QueryString("disable") = "yes" Then
            cmdAddCommunication.Visible = False
            cmdBack.Visible = False
            cmdReset.Visible = False
            cmdSave.Visible = False
            cmdBackContactView.Visible = True
            grdCommunication.Enabled = False
            imgAlert.Visible = False
        End If

        If Not IsPostBack Then
            lblTitle.Text = CMTitleContacts
            If Request.QueryString("PartnerID") <> "" Then
                If Request.QueryString("ContactID") <> "" Then
                    lblTitle.Text = TitleCMEditContact
                    trActive.Visible = True
                    subFillRecord()
                    subFillCommunication()
                    If Request.QueryString("Module") = "TM" Then
                        tblCommunication.Visible = False
                    Else
                        tblCommunication.Visible = True
                    End If
                Else
                    lblTitle.Text = TitleCMAddContact
                    tblCommunication.Visible = False
                    subFillAddress()
                End If
                subFillPartnerInfo()
            Else
                tblCommunication.Visible = False
            End If
        End If
        If Request.QueryString("OEnt") = "1" Or Request.QueryString("LeadLog") = "1" Then
            cmdAddCommunication.Visible = False
            cmdBack.Visible = True
            cmdReset.Visible = False
            cmdsave.Visible = True
            cmdBackContactView.Visible = True
            grdCommunication.Enabled = False
            imgAlert.Visible = False
        End If

    End Sub
    'Populate Controls from Partner Info
    Public Sub subFillPartnerInfo()
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            Dim sPID As String = Convert.ToString(Request.QueryString("PartnerID"))
            Dim objP As New clsPartners
            objP.PartnerID = sPID
            objP.getPartnersInfo()
            lblPartnerName.Text = objP.PartnerLongName
            If Request.QueryString("ContactID") = "" Then
                txtPhone.Text = objP.PartnerPhone
                txtFax.Text = objP.PartnerFax
                txtNotes.Text = objP.PartnerNote
            End If
            objP = Nothing
        End If
    End Sub
    'Fill Contact Communication Grid
    Private Sub subFillCommunication()
        Dim objComm As New clsContactCommunication
        objComm.ComContactID = Request.QueryString("ContactID")
        sqldsCommunication.SelectCommand = objComm.funFillGrid
        objComm = Nothing
    End Sub
    'Populate Object
    Private Sub subSetData(ByRef objC As clsPartnerContacts)
        objC.ContactPartnerID = Request.QueryString("PartnerID")
        objC.ContactSex = rblstSex.SelectedValue
        objC.ContactSalutation = txtSalutation.Text
        objC.ContactFirstName = txtFirstName.Text
        objC.ContactLastName = txtLastName.Text
        objC.ContactEmail = txtEmail.Text
        objC.ContactSecEmail = txtSecEmail.Text
        objC.ContactActive = rblstActive.SelectedValue

        objC.ContactLangPref = rblstLangPref.SelectedValue
        objC.ContactTitleFr = txtTitleFr.Text
        objC.ContactTitleEn = txtTitleEn.Text
        If txtDOB.Text <> "" Then
            objC.ContactDOB = DateTime.ParseExact(txtDOB.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd")
        Else
            objC.ContactDOB = "sysNull"
        End If

        objC.ContactPrimary = rblstContactPrimary.SelectedValue

        objC.ContactPhone = txtPhone.Text
        objC.ContactPhoneExt = txtPhoneExt.Text
        objC.ContactHomePhone = txtHomePhone.Text
        objC.ContactCellPhone = txtCellPhone.Text
        objC.ContactPagerNo = txtPagerNo.Text
        objC.ContactFax = txtFax.Text
        objC.ContactSelSpecialization = dlContactSpecialization.SelectedValue
        objC.ContactNote = txtNotes.Text

        If Convert.ToString(Request.QueryString("ContactID")) <> "" Then
            objC.ContactLastUpdatedBy = Session("UserID")
            objC.ContactLastUpdatedOn = Now.ToString("yyyy-MM-dd HH:mm:ss")
        Else
            objC.ContactCreatedBy = Session("UserID")
            objC.ContactLastUpdatedBy = Session("UserID")
            objC.ContactCreatedOn = Now.ToString("yyyy-MM-dd HH:mm:ss")
            objC.ContactLastUpdatedOn = Now.ToString("yyyy-MM-dd HH:mm:ss")
        End If
    End Sub
    'Populate HQ Address Object
    Private Sub subSetHQAddress(ByRef objAdd As clsAddress, ByVal sID As String)
        objAdd.AddressRef = "C"
        objAdd.AddressType = "B"
        objAdd.AddressSourceID = sID
        objAdd.AddressLine1 = txtAddLine1.Text
        objAdd.AddressLine2 = txtAddLine2.Text
        objAdd.AddressLine3 = txtDistrict.Text
        objAdd.AddressCity = txtCity.Text
        objAdd.AddressState = txtProv.Text
        objAdd.AddressCountry = txtCountry.Text
        objAdd.AddressPostalCode = txtPostalCode.Text
    End Sub
    'Populate Controls
    Public Sub subFillRecord()
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            Dim sPID As String = Convert.ToString(Request.QueryString("PartnerID"))
            Dim sCID As String = Convert.ToString(Request.QueryString("ContactID"))
            Dim objP As New clsPartners
            objP.PartnerID = sPID
            objP.getPartnersInfo()
            Dim objC As New clsPartnerContacts
            objC.ContactID = sCID
            objC.getPartnerContactsInfo()
            rblstSex.SelectedValue = objC.ContactSex
            txtSalutation.Text = objC.ContactSalutation
            txtFirstName.Text = objC.ContactFirstName
            txtLastName.Text = objC.ContactLastName
            txtEmail.Text = objC.ContactEmail
            txtSecEmail.Text = objC.ContactSecEmail
            rblstActive.SelectedValue = objC.ContactActive
            rblstLangPref.SelectedValue = objC.ContactLangPref
            txtTitleFr.Text = objC.ContactTitleFr
            txtTitleEn.Text = objC.ContactTitleEn
            If objC.ContactDOB <> "" Then
                txtDOB.Text = CDate(objC.ContactDOB).ToString("MM/dd/yyyy")
            End If
            rblstContactPrimary.SelectedValue = objC.ContactPrimary
            txtPhone.Text = objC.ContactPhone
            txtPhoneExt.Text = objC.ContactPhoneExt
            txtHomePhone.Text = objC.ContactHomePhone
            txtCellPhone.Text = objC.ContactCellPhone
            txtPagerNo.Text = objC.ContactPagerNo
            txtFax.Text = objC.ContactFax
            dlContactSpecialization.SelectedValue = objC.ContactSelSpecialization
            txtNotes.Text = objC.ContactNote

            'For Partners
            lblCreated.Text = lblCMCreated
            lblCreatedDate.Text = CDate(objP.PartnerCreatedOn).ToString("MMMM dd, yyyy")
            lblCreatedBy.Text = objP.funGetCreatedBy(objP.PartnerCreatedBy)
            lblModified.Text = lblCMModified
            lblModifiedDate.Text = CDate(objP.PartnerLastUpdatedOn).ToString("MMMM dd, yyyy")
            lblModifiedBy.Text = objP.funGetModifiedBy(objP.PartnerLastUpdatedBy)

            'For Contacts
            'lblCntCreated.Text = lblCMCreated
            'lblCntCreatedDate.Text = CDate(objC.ContactCreatedOn).ToString("dd MMMM yyyy")
            'lblCntCreatedBy.Text = objC.funGetCreatedBy(objC.ContactCreatedBy)
            'lblCntModified.Text = lblCMModified
            'lblCntModifiedDate.Text = CDate(objC.ContactLastUpdatedOn).ToString("dd MMMM yyyy")
            'lblCntModifiedBy.Text = objC.funGetModifiedBy(objC.ContactLastUpdatedBy)

            objP = Nothing
            objC = Nothing

            Dim objAdr As New clsAddress
            subSetHQAddControl(objAdr, sCID)
            objAdr = Nothing
        End If
    End Sub
    'Populate HQ Address Controls
    Private Sub subSetHQAddControl(ByRef objAdd As clsAddress, ByVal sID As String)
        objAdd.getAddressInfo("C", "B", sID)
        txtAddLine1.Text = objAdd.AddressLine1
        txtAddLine2.Text = objAdd.AddressLine2
        txtDistrict.Text = objAdd.AddressLine3
        txtCity.Text = objAdd.AddressCity
        txtProv.Text = objAdd.AddressState
        txtCountry.Text = objAdd.AddressCountry
        txtPostalCode.Text = objAdd.AddressPostalCode
    End Sub
    'Populate Controls from Partner Address
    Public Sub subFillAddress()
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            Dim sPID As String = Convert.ToString(Request.QueryString("PartnerID"))
            Dim objAdd As New clsAddress
            objAdd.getAddressInfo(Request.QueryString("CustType"), "B", sPID)
            txtAddLine1.Text = objAdd.AddressLine1
            txtAddLine2.Text = objAdd.AddressLine2
            txtDistrict.Text = objAdd.AddressLine3
            txtCity.Text = objAdd.AddressCity
            txtProv.Text = objAdd.AddressState
            txtCountry.Text = objAdd.AddressCountry
            txtPostalCode.Text = objAdd.AddressPostalCode
            objAdd = Nothing
        End If
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    'On Add Communication
    Protected Sub cmdAddCommunication_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddCommunication.ServerClick
        If Request.QueryString("PartnerID") <> "" And Request.QueryString("ContactID") <> "" Then
            Response.Redirect("AddCommunication.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&ContactID=" & Request.QueryString("ContactID") & "&mp=" & Request.QueryString("mp"))
        End If
    End Sub
    'On Page Index Change
    Protected Sub grdCommunication_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdCommunication.PageIndexChanging
        subFillCommunication()
    End Sub
    'On Row Command
    Protected Sub grdCommunication_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdCommunication.RowCommand
        If e.CommandName = "ViewComm" Then
            Dim strComID As String = e.CommandArgument
            Response.Redirect("FollowUp.aspx?CommID=" & strComID & "&ContactID=" & Request.QueryString("ContactID") & "&PartnerID=" & Request.QueryString("PartnerID") & "&mp=" & Request.QueryString("mp"))
        End If
    End Sub
    'On Row Data Bound
    Protected Sub grdCommunication_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdCommunication.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDeleteContact"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & CMConfirmDeleteContact & " ')")
            Dim txtCommID As TextBox = CType(e.Row.FindControl("txtCommID"), TextBox)
            Dim lblAD As Label = CType(e.Row.FindControl("lblAD"), Label)
            Dim strComID As String = txtCommID.Text
            If strComID <> "" Then
                Dim objAlert As New clsAlerts
                lblAD.Text = objAlert.funGetAlertDateTime(strComID)
                If lblAD.Text <> "" And IsDate(lblAD.Text) Then
                    lblAD.Text = CDate(objAlert.funGetAlertDateTime(strComID)).ToString("dd MMM yyyy HH:mm")
                End If
                objAlert = Nothing
            End If

            Dim strData As String = ""
            Select Case e.Row.Cells(3).Text
                Case 1
                    strData = liCMCommEmail
                Case 2
                    strData = liCMCommFax
                Case 3
                    strData = liCMCommPhone
                Case 4
                    strData = liCMCommMassEmail
            End Select
            e.Row.Cells(3).Text = strData
        End If
    End Sub
    ' On Row Deleting
    Protected Sub grdCommunication_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdCommunication.RowDeleting

    End Sub
    ' On Row Editing
    Protected Sub grdCommunication_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdCommunication.RowEditing
        Dim strComID As String = grdCommunication.DataKeys(e.NewEditIndex).Value.ToString()
        Dim txtContactID As TextBox = CType(grdCommunication.Rows(e.NewEditIndex).FindControl("txtContactID"), TextBox)
        Response.Redirect("AddEditAlert.aspx?CommID=" & strComID & "&ContactID=" & txtContactID.Text & "&PartnerID=" & Request.QueryString("PartnerID") & "&mp=" & Request.QueryString("mp"))
    End Sub
   
    ' On Sorting
    Protected Sub grdCommunication_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdCommunication.Sorting
        subFillCommunication()
    End Sub
    'Populate Specialization
    Private Sub subPopulateSpecialization()
        Dim objPS As New clsPartnerSpecialization
        objPS.PopulateSpecialization(dlContactSpecialization)
        objPS = Nothing
    End Sub
    Protected Sub imgAlert_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAlert.Click
        Response.Redirect("AddEditAlert.aspx?CommID=0&ContactID=" & Request.QueryString("ContactID") & "&PartnerID=" & Request.QueryString("PartnerID") & "&mp=" & Request.QueryString("mp"))
    End Sub

    Protected Sub cmdBackContactView_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBackContactView.ServerClick
        If Request.QueryString("OEnt") = "1" Then
            Response.Redirect("~/Partner/AddEditCustomer.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&OEnt=" & Request.QueryString("OEnt"))
        ElseIf Request.QueryString("LeadLog") = "1" Then
            Response.Redirect("~/Partner/AddEditCustomer.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&LeadLog=" & Request.QueryString("LeadLog"))
        else
            Response.Redirect("~/Partner/ViewContact.aspx")
        End If
    End Sub
    Protected Sub cmdBack_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.ServerClick
        If Convert.ToString(Request.QueryString("mp")) = "1" And Request.QueryString("addCon") = "" Then
            Response.Redirect("ViewContact.aspx")
        ElseIf Request.QueryString("Module") = "TM" Then
            Response.Redirect("~/Partner/AddEditCustomer.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&Module=" & Request.QueryString("Module") & "&SFU=" & Request.QueryString("SFU") & "&LP=" & Request.QueryString("LP"))
        ElseIf Request.QueryString("OEnt") = "1" Then
            Response.Redirect("~/Partner/AddEditCustomer.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&OEnt=" & Request.QueryString("OEnt"))
        ElseIf Request.QueryString("LeadLog") = "1" Then
            Response.Redirect("~/Partner/AddEditCustomer.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&LeadLog=" & Request.QueryString("LeadLog"))
        ElseIf Request.QueryString("addCon") = "1" Then
            Response.Redirect("~/Partner/SelectPartner.aspx")
        Else
            Response.Redirect("AddEditCustomer.aspx?PartnerID=" & Request.QueryString("PartnerID"))
        End If
    End Sub

    Protected Sub cmdReset_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReset.ServerClick
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            subFillRecord()
            Exit Sub
        End If
        rblstSex.SelectedValue = "F"
        txtSalutation.Text = ""
        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtAddLine1.Text = ""
        txtAddLine2.Text = ""
        txtCity.Text = ""
        txtDistrict.Text = ""
        txtProv.Text = ""
        txtCountry.Text = ""
        txtPostalCode.Text = ""
        txtEmail.Text = ""
        txtSecEmail.Text = ""
        rblstActive.SelectedValue = 1
        rblstLangPref.SelectedValue = "fr"
        txtTitleFr.Text = ""
        txtTitleEn.Text = ""
        txtDOB.Text = ""
        rblstContactPrimary.SelectedValue = 0
        txtPhone.Text = ""
        txtPhoneExt.Text = ""
        txtHomePhone.Text = ""
        txtCellPhone.Text = ""
        txtPagerNo.Text = ""
        txtFax.Text = ""
        dlContactSpecialization.SelectedValue = 0
        txtNotes.Text = ""
    End Sub
    ' Insert/Update Contact
    Protected Sub cmdsave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.ServerClick
        If Not Page.IsValid Then
            Exit Sub
        End If

        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            Dim objContact As New clsPartnerContacts
            Dim strContactID As String = ""
            Dim strPartnerID As String = ""
            strPartnerID = Convert.ToString(Request.QueryString("PartnerID"))
            subSetData(objContact)

            If Convert.ToString(Request.QueryString("ContactID")) <> "" Then
                strContactID = Convert.ToString(Request.QueryString("ContactID"))
                objContact.ContactID = strContactID
                objContact.ContactPartnerID = strPartnerID
                objContact.updatePartnerContacts()

                Dim objAdr As New clsAddress
                objAdr.getAddressInfo("C", "B", strContactID)
                If objAdr.funCheckDuplicateAddress() = True Then
                    objAdr.AddressID = objAdr.funGetAddressID()
                    subSetHQAddress(objAdr, strContactID)
                    objAdr.updateAddress()
                Else
                    subSetHQAddress(objAdr, strContactID)
                    objAdr.insertAddress()
                End If
                objAdr = Nothing

                Session.Add("Msg", CMContactUpdatedSuccessfully)
                If Convert.ToString(Request.QueryString("mp")) = "1" Then
                    Response.Redirect("ViewContact.aspx")
                ElseIf Request.QueryString("OEnt") = "1" Then
                    Session.Add("Msg", CMCustomerUpdatedSuccessfully)
                    Response.Redirect("~/Sales/Approval.aspx" & Session("OrderEntry"))
                ElseIf Request.QueryString("LeadLog") = "1" Then
                    Session.Add("Msg", CMCustomerUpdatedSuccessfully)
                    Response.Redirect("~/TeleMarketing/viewLeadsLog.aspx?PartnerID=" & Request.QueryString("PartnerID"))
                Else
                    If Request.QueryString("Module") = "TM" Then
                        Dim objCompany As New clsCompanyInfo
                        objCompany.GetCompanyAddress()
                        Response.Redirect("~/Sales/Create.aspx?custID=" & strPartnerID & "&custName=" & lblPartnerName.Text & "&ComID=" & objCompany.CompanyID & "&whsID=" & Session("UserWarehouse") & "&Module=" & Request.QueryString("Module") & "&SFU=" & Request.QueryString("SFU") & "&LP=" & Request.QueryString("LP"))
                    End If
                    Response.Redirect("AddEditCustomer.aspx?PartnerID=" & strPartnerID)
                End If
                Exit Sub
            Else
                objContact.IsSubscribeEmail = 1
                If objContact.insertPartnerContacts(strContactID) = True Then 'Insert Partner
                    If strContactID <> "" Then
                        Dim objAdr As New clsAddress
                        subSetHQAddress(objAdr, strContactID)
                        objAdr.insertAddress()
                        objAdr = Nothing
                    End If
                    If (Request.QueryString("adCst") = "Y") Then
                        Session.Add("Msg", CMCustomerAddedSuccessfully)
                        Response.Redirect("~/Sales/Generate.aspx?cID=" & Request.QueryString("PartnerID") & "&cName=" & lblPartnerName.Text)
                    End If
                    Session.Add("Msg", CMContactAddedSuccessfully)
                    If Convert.ToString(Request.QueryString("mp")) = "1" Then
                        Response.Redirect("ViewContact.aspx")
                    ElseIf Request.QueryString("OEnt") = "1" Then
                        Session.Add("Msg", CMCustomerUpdatedSuccessfully)
                        Response.Redirect("~/Sales/Approval.aspx" & Session("OrderEntry"))
                    ElseIf Request.QueryString("LeadLog") = "1" Then
                        Session.Add("Msg", CMCustomerUpdatedSuccessfully)
                        Response.Redirect("~/TeleMarketing/viewLeadsLog.aspx?PartnerID=" & Request.QueryString("PartnerID"))
                    Else
                        If Request.QueryString("Module") = "TM" Then
                            Dim objCompany As New clsCompanyInfo
                            objCompany.GetCompanyAddress()
                            Response.Redirect("~/Sales/Create.aspx?custID=" & strPartnerID & "&custName=" & lblPartnerName.Text & "&ComID=" & objCompany.CompanyID & "&whsID=" & Session("UserWarehouse") & "&Module=" & Request.QueryString("Module") & "&SFU=" & Request.QueryString("SFU") & "&LP=" & Request.QueryString("LP"))
                        End If
                        Response.Redirect("AddEditCustomer.aspx?PartnerID=" & strPartnerID)
                    End If
                    Exit Sub
                End If
            End If

        End If
    End Sub
End Class


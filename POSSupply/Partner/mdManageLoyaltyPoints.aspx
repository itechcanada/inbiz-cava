﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdManageLoyaltyPoints.aspx.cs" Inherits="Common_mdManageLoyaltyPoints" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <asp:Panel ID="pnlCommon" runat="server">
        <table border="0" cellpadding="5" cellspacing="5" style="width:600px">
            <tr>
                <td style="width: 80px">                
                    <asp:Label ID="lblPoints" Text="<%$Resources:Resource, lblPoints%>" runat="server" Font-Bold="true" />
                </td>
                <td>
                    <asp:TextBox ID="txtPoints" runat="server" />
                </td>
            </tr>
            <tr valign="top">
                <td style="width: 80px">
                    <asp:Label ID="Label1" Text="<%$Resources:Resource, lblNotes%>" runat="server" Font-Bold="true" />
                </td>
                <td>
                    <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" />
                </td>
            </tr>
            <tr id="trProductsGrid" runat="server">
                <td colspan="2">
                    <div id="grid_wrapper" style="width: 595px">
                    <trirand:JQGrid runat="server" ID="grdLoyalty" Height="200px" AutoWidth="True" OnCellBinding="grdLoyalty_CellBinding"
                        OnDataRequesting="grdLoyalty_DataRequesting">
                        <Columns>
                            <trirand:JQGridColumn DataField="prdUPCCode" HeaderText="<%$ Resources:Resource, grdvUPCCode %>"
                                TextAlign="Center" />
                            <trirand:JQGridColumn DataField="prdName" HeaderText="<%$ Resources:Resource, grdPoProductName %>"
                                Editable="false" TextAlign="Left" />
                            <trirand:JQGridColumn DataField="prdEndUserSalesPrice" HeaderText="<%$ Resources:Resource, lblPrdEndUserSalesPrice %>"
                                Editable="false" TextAlign="Right" DataFormatString="{0:C}" />
                            <trirand:JQGridColumn DataField="OnHeadQty" HeaderText="<%$ Resources:Resource, grdvOnHandQty %>"
                                Editable="false" TextAlign="Center" Visible="false" />
                            <trirand:JQGridColumn DataField="Points" HeaderText="Points"
                                Editable="false" TextAlign="Center" />                           
                            <trirand:JQGridColumn DataField="ProductID" HeaderText="" PrimaryKey="True"
                                TextAlign="Center" Visible="false" />
                        </Columns>
                        <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                            ShowDeleteButton="false" ShowSearchButton="false" />                        
                        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" ShowFooter="true" />
                        <ClientSideEvents LoadComplete="jqGridResize" RowSelect="rowSelect" />
                    </trirand:JQGrid>
                    </div>
                </td>
            </tr>
        </table>
        <div id="pnlSearch">
        </div>
        <div class="div_command">  
            <asp:HiddenField ID="hdnProductIDtoRedeem" runat="server" />                 
            <asp:Button ID="btnSave" Text="<%$Resources:Resource,btnSave%>" runat="server" 
                onclick="btnSave_Click" />
            <asp:Button ID="btnCancel" Text="<%$Resources:Resource, btnCancel%>" runat="server" CausesValidation="false" OnClientClick="jQuery.FrameDialog.closeDialog();" />
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        $grid = $("#<%=grdLoyalty.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "pnlSearch",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=grdLoyalty.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
            $("#<%=hdnProductIDtoRedeem.ClientID%>").val("");
        }       

        $(document).ready(function () {            
            $("#<%=txtPoints.ClientID%>").focus();
        });

        function rowSelect(id) {
            var celValue = $grid.jqGrid('getCell', id, 'Points');
            $("#<%=txtPoints.ClientID%>").val(celValue);
            $("#<%=hdnProductIDtoRedeem.ClientID%>").val(id);
        }

        $("#<%=txtPoints.ClientID%>").change(function () {
            var pOpt = '<%=Request.QueryString["pointOption"]%>';
            var val = parseInt($(this).val());
            if (isNaN(val)) {
                $(this).val("0");
            }
            /*else {                
                if (pOpt != "add" && val > 0) {
                    val = (-1) * val;
                    $(this).val(val);
                }
            }*/
        });
    </script>
</asp:Content>


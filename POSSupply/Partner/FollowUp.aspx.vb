Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Partial Class Partner_FollowUp
    Inherits BasePage
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If

        lblError.Text = ""
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If
        lblTitle.Text = TitleCMFollowUp
        If Not IsPostBack Then
            If Request.QueryString("PartnerID") <> "" Then
                If Request.QueryString("ContactID") <> "" Then
                    subFillRecord()
                    subFillFollowUp()
                    tblCommunication.Visible = True
                Else
                    tblCommunication.Visible = False
                End If
            Else
                tblCommunication.Visible = False
            End If
        End If
    End Sub
    'Fill Contact Communication FollowUp Grid
    Private Sub subFillFollowUp()
        Dim objCommFU As New clsContactComFollowUp
        objCommFU.ComFollowUpComID = Request.QueryString("CommID")
        sqldsCommunication.SelectCommand = objCommFU.funFillGridByCommID
        objCommFU = Nothing
    End Sub
    'Populate Object
    Private Sub subSetData(ByRef objF As clsContactComFollowUp)
        objF.ComFollowUpComID = Request.QueryString("CommID")
        objF.ComFollowUpEmail = txtFollowUpEmail.Text
        objF.ComFollowUpText = txtFollowUpText.Text
        objF.ComFollowUpDateTime = DateTime.ParseExact(txtFollowUpDateTime.Text & " " & txtFollowUpTime.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd HH:mm:ss")
        objF.ComFollowUpUserID = Session("UserID")
    End Sub
    ' Insert/Update Contact
    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        If txtFollowUpText.Text = "" And txtFollowUpEmail.Text = "" Then
            lblError.Text = msgCMPlzEntFFollowUpInformation
            Exit Sub
        End If
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" And Convert.ToString(Request.QueryString("ContactID")) <> "" Then
            Dim objFU As New clsContactComFollowUp
            Dim strPartnerID As String = ""
            strPartnerID = Convert.ToString(Request.QueryString("PartnerID"))
            subSetData(objFU)

            If Convert.ToString(Request.QueryString("FupID")) <> "" Then
                objFU.ContactComFollowUpID = Convert.ToString(Request.QueryString("FupID"))
                
            Else
                
                If objFU.insertContactComFollowUp = True Then 'Insert FollowUp
                    Session.Add("Msg", CMFollowUpAddedSuccessfully)
                    subFillFollowUp()
                    subResetControls()
                    'If Convert.ToString(Request.QueryString("mp")) = "1" Then
                    '    Response.Redirect("ViewContact.aspx")
                    'Else
                    '    Response.Redirect("AddEditPartner.aspx?PartnerID=" & strPartnerID)
                    'End If
                    'Exit Sub
                End If
            End If

        End If
    End Sub
    ' Reset Controls
    Protected Sub cmdReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReset.Click
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            subFillRecord()
            Exit Sub
        End If
        subResetControls()
    End Sub
    ' Reset Controls
    Private Sub subResetControls()
        txtFollowUpText.Text = ""
        txtFollowUpEmail.Text = ""
        txtFollowUpDateTime.Text = ""
        txtFollowUpTime.Text = ""
    End Sub
    'Populate Controls
    Public Sub subFillRecord()
        If Convert.ToString(Request.QueryString("PartnerID")) <> "" Then
            Dim sPID As String = Convert.ToString(Request.QueryString("PartnerID"))
            Dim sCID As String = Convert.ToString(Request.QueryString("ContactID"))
            Dim objP As New clsPartners
            objP.PartnerID = sPID
            objP.getPartnersInfo()
            Dim objC As New clsPartnerContacts
            objC.ContactID = sCID
            objC.getPartnerContactsInfo()
            
            'For Partners
            lblCreated.Text = lblCMCreated
            lblCreatedDate.Text = CDate(objP.PartnerCreatedOn).ToString("MMMM dd, yyyy")
            lblCreatedBy.Text = objP.funGetCreatedBy(objP.PartnerCreatedBy)
            lblModified.Text = lblCMModified
            lblModifiedDate.Text = CDate(objP.PartnerLastUpdatedOn).ToString("MMMM dd, yyyy")
            lblModifiedBy.Text = objP.funGetModifiedBy(objP.PartnerLastUpdatedBy)

            'For Contacts
            lblCntCreated.Text = lblCMCreated
            lblCntCreatedDate.Text = CDate(objC.ContactCreatedOn).ToString("dd MMMM yyyy")
            lblCntCreatedBy.Text = objC.funGetCreatedBy(objC.ContactCreatedBy)
            lblCntModified.Text = lblCMModified
            lblCntModifiedDate.Text = CDate(objC.ContactLastUpdatedOn).ToString("dd MMMM yyyy")
            lblCntModifiedBy.Text = objC.funGetModifiedBy(objC.ContactLastUpdatedBy)

            objP = Nothing
            objC = Nothing
            
        End If
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    'On Add Communication
    Protected Sub cmdAddCommunication_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddCommunication.Click
        If Request.QueryString("PartnerID") <> "" And Request.QueryString("ContactID") <> "" Then
            Response.Redirect("AddCommunication.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&ContactID=" & Request.QueryString("ContactID") & "&mp=" & Request.QueryString("mp"))
        End If
    End Sub
    'On Page Index Change
    Protected Sub grdCommunication_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdCommunication.PageIndexChanging
        subFillFollowUp()
    End Sub
    'On Row Command
    Protected Sub grdCommunication_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdCommunication.RowCommand
        If e.CommandName = "ViewComm" Then
            Dim strComID As String = grdCommunication.DataKeys(e.CommandArgument).Value.ToString()
            Response.Redirect("FollowUp.aspx?CommID=" & strComID & "&ContactID=" & Request.QueryString("ContactID") & "&PartnerID=" & Request.QueryString("PartnerID") & "&mp=" & Request.QueryString("mp"))
        End If
    End Sub
    'On Row Data Bound
    Protected Sub grdCommunication_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdCommunication.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDeleteContact"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & CMConfirmDeleteContact & " ')")
            
        End If
    End Sub
    ' On Row Deleting
    Protected Sub grdCommunication_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdCommunication.RowDeleting

    End Sub
    ' On Row Editing
    Protected Sub grdCommunication_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdCommunication.RowEditing
        Dim strComID As String = grdCommunication.DataKeys(e.NewEditIndex).Value.ToString()
        Dim txtContactID As TextBox = CType(grdCommunication.Rows(e.NewEditIndex).FindControl("txtContactID"), TextBox)
        Response.Redirect("AddEditAlert.aspx?CommID=" & strComID & "&ContactID=" & txtContactID.Text & "&PartnerID=" & Request.QueryString("PartnerID"))
    End Sub
    'On Back
    Protected Sub cmdBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        If Convert.ToString(Request.QueryString("mp")) = "1" Then
            Response.Redirect("ViewContact.aspx")
        Else
            Response.Redirect("AddEditContact.aspx?PartnerID=" & Request.QueryString("PartnerID") & "&ContactID=" & Request.QueryString("ContactID"))
        End If
    End Sub
    ' On Sorting
    Protected Sub grdCommunication_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdCommunication.Sorting
        subFillFollowUp()
    End Sub
End Class


<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false" CodeFile="AddEditAlert.aspx.vb" Inherits="Partner_AddEditAlert" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="Resources.Resource" %>

<%@ Register src="../Controls/CommonSearch/CRMSearch.ascx" tagname="CRMSearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();        
    </script>
    <uc1:CRMSearch ID="CRMSearch1" runat="server" />
</asp:Content>

<asp:Content ID="cntAddEditAlert" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal></h2>
                </td>
                <td style="text-align:right;">
                    <asp:UpdateProgress runat="server" ID="upComm" DisplayAfter="10">
                        <ProgressTemplate>
                            <img src="../Images/wait22trans.gif" alt="" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
    </div>

    <div class="divMainContent">
        <div align="right" id="divCrtMod" runat="server" style="vertical-align: top;">
            <table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
                <tr>
                    <td width="70%">
                        <asp:UpdatePanel ID="upnlMsg" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                                <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td width="30%" align="right" bgcolor="#FFFFFF" valign="top">
                        <table style="border: 0;" bgcolor="#FFFFFF" class="table">
                            <tr>
                                <td height="5">
                                </td>
                            </tr>
                            <tr class="trheight">
                                <td class="tdAlignLeft">
                                    <asp:Label ID="lblCreated" CssClass="lblBold" runat="server" /><br />
                                    <asp:Label ID="lblCreatedDate" CssClass="lblSimple" runat="server" /><br />
                                    <asp:Label ID="lblCreatedBy" CssClass="lblSimple" runat="server" />
                                </td>
                                <td width="10px">
                                    &nbsp;
                                </td>
                                <td class="tdAlignLeft">
                                    <asp:Label ID="lblModified" CssClass="lblBold" runat="server" /><br />
                                    <asp:Label ID="lblModifiedDate" CssClass="lblSimple" runat="server" /><br />
                                    <asp:Label ID="lblModifiedBy" CssClass="lblSimple" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
        </div>
    
    </div>
    
    <div class="divMainContent">
        <table border="0" cellpadding="0" cellspacing="0" style="width:100%;">            
            <tr>
                <td valign="top" colspan="2">
                    <table width="100%" style="border: 0;" bgcolor="#FFFFFF" class="table">
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblAlertDate" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMAlertDate%>"
                                    runat="server" /><br />
                                &nbsp;
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtAlertDateTime" runat="server" MaxLength="19" Width="150px" />
                                <asp:ImageButton ID="imgAlertDate" CausesValidation="false" runat="server" ToolTip="Click to show calendar"
                                    ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                                <span class="style1">*</span><br />
                                <%=lblCMAlertDateFormat %>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" TargetControlID="txtAlertDateTime"
                                    runat="server" PopupButtonID="imgAlertDate" Format="MM/dd/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:RequiredFieldValidator ID="reqvalAlertDateTime" runat="server" ControlToValidate="txtAlertDateTime"
                                    ErrorMessage="<%$ Resources:Resource, reqvalCMAlertDateTime%>" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                                <div style="display: none;">
                                    <asp:TextBox ID="txtDate" Enabled="true" runat="server" Width="0px" MaxLength="19">
                                    </asp:TextBox>
                                </div>
                                <asp:CustomValidator ID="custvalAlertDate" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<%$ Resources:Resource, msgCMPlzEntAlertDateGTCurDate%>" Enabled="false"
                                    ClientValidationFunction="funCheckAlertDate" Display="None">
                                </asp:CustomValidator>
                                <asp:CompareValidator ID="cmpvalAlertDate" EnableClientScript="true" SetFocusOnError="true"
                                    ControlToValidate="txtAlertDateTime" ControlToCompare="txtDate" Operator="GreaterThanEqual"
                                    Type="Date" Display="None" Enabled="false" ErrorMessage="<%$ Resources:Resource, msgCMPlzEntAlertDateGTCurDate %>"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblAlertTime" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMAlertTime%>"
                                    runat="server" /><br />
                                &nbsp;
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtAlertTime" runat="server" Width="150px" MaxLength="8" />
                                <span class="style1">*</span><br />
                                <%=lblCMAlertTimeFormat %>
                                <ajaxToolkit:AutoCompleteExtender ID="acExAlertTime" runat="server" TargetControlID="txtAlertTime"
                                    MinimumPrefixLength="1" EnableCaching="true" ServicePath="~/FilterService.asmx"
                                    ServiceMethod="GetTime">
                                </ajaxToolkit:AutoCompleteExtender>
                                <asp:RequiredFieldValidator ID="reqvalAlertTime" runat="server" ControlToValidate="txtAlertTime"
                                    ErrorMessage="<%$ Resources:Resource, reqvalCMAlertTime%>" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="trheight">
                            <td class="tdAlign">
                                <asp:Label ID="lblNotes" CssClass="lblBold" Text="<%$ Resources:Resource, lblCMNotes%>"
                                    runat="server" />
                            </td>
                            <td class="tdAlignLeft">
                                <asp:TextBox ID="txtNotes" runat="server" TextMode="multiLine" Rows="5" MaxLength="250"
                                    Width="100%" />
                            </td>
                        </tr>
                    </table>
                </td>                
            </tr>
            <tr class="trheight">
                <td align="right" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;
                    padding-right: 5px;">
                    <table width="100%">
                        <tr>
                            <td height="20" align="center" width="25%">
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="cmdSave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                        style="min-width: 120px; text-align: center;">
                                        <%=Resources.Resource.cmdCssSave%></span></a></div>
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="cmdBack" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                        <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssBack%></span></a></div>
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="cmdReset" runat="server" visible="false" causesvalidation="false" class="ovalbutton"
                                        href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssCancel%></span></a></div>
                            </td>
                            <td height="20" align="center" width="15%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsAlert" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
    <br />
    <br />
    <script language="javascript" type="text/javascript">
        function funCheckAlertDate(source, args) {
            var curdate = new Date();

            if (window.document.getElementById('<%=txtAlertDateTime.ClientID%>').value > curdate) {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }
    </script>
</asp:Content>
 
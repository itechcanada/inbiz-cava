Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Partial Class Partner_SelectPartner
    Inherits BasePage
    Dim objCompany As New clsCompanyInfo
    Dim objCust As New clsExtUser
    ' On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If

        lblMsg.Text = ""
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If
        
        lblTitle.Text = TitleCMAddContact

        If Not Page.IsPostBack Then
            'subPopulatePartners()
        End If

        clsGrid.MaintainParams(Page, grdCustomer, SearchPanel, lblMsg, imgSearch)
        txtPartner.Focus()
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    'On Back
    Protected Sub cmdBack_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.ServerClick
        Response.Redirect("~/Admin/home.aspx")
    End Sub
    'On Submit
    Protected Sub cmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.ServerClick
        If txtPartner.Text = "" Then
            lblError.Text = msgCMPartnerName
            Exit Sub
        Else
            'Dim objP As New clsPartners
            'Dim strPartnerID As String = ""
            'If objP.funCheckPartnerName(txtPartner.Text, strPartnerID) = True Then
            '    objP = Nothing
            Response.Redirect("~/Partner/AddEditContact.aspx?PartnerID=" & hdnCustID.Value & "&mp=1&addCon=1")
            '    Else
            '    lblError.Text = msgCMPartnerNameNotFound
            'End If
            'objP = Nothing
        End If
    End Sub
    ' Reset Controls
    Protected Sub cmdReset_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReset.ServerClick
        'dlPartner.SelectedValue = 0
    End Sub

    Protected Sub imgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
        lblMsg.Text = ""
        subCustomerSearch(txtPartner.Text)
    End Sub
    Protected Sub grdCustomer_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdCustomer.PageIndexChanged
        lblMsg.Text = ""
        subCustomerSearch(txtPartner.Text)
    End Sub
    'Data Bound
    Protected Sub grdCustomer_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdCustomer.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim txtTotalOrders As TextBox = CType(e.Row.FindControl("txtTotalOrders"), TextBox)
            Dim txtTotalInvoices As TextBox = CType(e.Row.FindControl("txtTotalInvoices"), TextBox)
            Dim txtTotalAmtRcvd As TextBox = CType(e.Row.FindControl("txtTotalAmtRcvd"), TextBox)
            Dim txtLastAmtRcvd As TextBox = CType(e.Row.FindControl("txtLastAmtRcvd"), TextBox)
            Dim strHistory As New StringBuilder
            strHistory.Append("" & msgTtlNoSO & " " & txtTotalOrders.Text)
            strHistory.Append("<br> " & msgTtlNoInv & " " & txtTotalInvoices.Text)
            Dim strAmtRcvd As New StringBuilder
            If (txtTotalAmtRcvd.Text <> "") Then
                strAmtRcvd.Append(String.Format("{0:F}", CDbl(txtTotalAmtRcvd.Text)))
            End If
            strHistory.Append("<br> " & totalAmtRcvd & " " & strAmtRcvd.ToString)
            Dim strLastAmtRcvdOn As New StringBuilder
            If (txtLastAmtRcvd.Text <> "") Then
                strLastAmtRcvdOn.Append(txtLastAmtRcvd.Text)
            End If
            strHistory.Append("<br>" & LastAmtRcvdOn & " " & strLastAmtRcvdOn.ToString)
            txtTotalOrders.Text = strHistory.ToString
            e.Row.Cells(1).Text = strHistory.ToString
            Dim txtPartnerName As TextBox = CType(e.Row.FindControl("txtPartnerName"), TextBox)
            Dim txtPartnerAcronyme As TextBox = CType(e.Row.FindControl("txtPartnerAcronyme"), TextBox)
            Dim lblPartnerName As Label = CType(e.Row.FindControl("lblPartnerName"), Label)
            lblPartnerName.Text = txtPartnerAcronyme.Text & "<BR/>" & txtPartnerName.Text & "<BR/>" & funGetCustomerAddress(grdCustomer.DataKeys(e.Row.RowIndex).Value.ToString)
            lblPartnerName.Text = lblPartnerName.Text.Replace(Environment.NewLine, "<br />")
            strHistory = Nothing
            strAmtRcvd = Nothing
            strLastAmtRcvdOn = Nothing
            'e.Row.Cells(3).Text = ""
        End If
    End Sub
    'Click on Select Button in grid
    Protected Sub grdCustomer_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdCustomer.RowEditing
        hdnCustID.Value = grdCustomer.DataKeys(e.NewEditIndex).Value.ToString
        txtPartner.Text = grdCustomer.Rows(e.NewEditIndex).Cells(3).Text
        cmdSave.Visible = True
        txtPartner.Focus()
    End Sub
    'Soring Customer grid 
    Protected Sub grdCustomer_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdCustomer.Sorting
        lblMsg.Text = ""
        subCustomerSearch(txtPartner.Text)
    End Sub
    'Search customer 
    Protected Sub subCustomerSearch(ByVal SrchTxt As String)
        sqldsCustomer.SelectCommand = objCust.funfillGridCustomerDetail(SrchTxt)
    End Sub
    ' Sql Data Source Event Track
    Protected Sub sqldsCustomer_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsCustomer.Selected
        If e.AffectedRows = 0 And lblMsg.Text = "" Then
            lblMsg.Text = NoDataFound
            cmdSave.Visible = False
        End If
    End Sub
    Private Function funGetCustomerAddress(ByVal strCustID As String) As String
        Dim strAddress As String
        Dim objCust As New clsExtUser
        objCust.CustID = strCustID
        objCust.getCustomerInfo()
        If objCust.CustType <> "" Then
            strAddress = funPopulateAddress(objCust.CustType, "B", objCust.CustID).Replace("<br>", vbCrLf)
        End If
        objCust = Nothing
        Return strAddress
    End Function
    'Populate Address
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)

        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + ","
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + ","
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3
        End If
        strAddress += "<br>"
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + ","
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + "<br />"
            Else
                strAddress += objAdr.AddressState + "<br />"
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + "<br />"
            Else
                strAddress += objAdr.AddressCountry + "<br />"
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += "Postal Code: " + objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function
End Class

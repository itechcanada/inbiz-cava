﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="AddressCollection.aspx.cs" Inherits="Partner_AddressCollection" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <style type="text/css">
        select
        {
            width: 179px;
        }
        input, textarea, select
        {
            border: 1px solid #797D7F;
            margin-bottom: 0;
        }
        
        .ButtonCss
        {
            background: url("../lib/css/inbiz/images/ui-bg_highlight-soft_20_526785_1x100.png") repeat-x scroll 50% 50% #DBDDDD;
            border: 1px solid #DBDDDD;
            color: #FFFEFD;
            font-weight: bold;
        }
    </style>
    <div class="div_command">
    </div>
    <div id="grid_wrapper" style="width: 100%;">
        <trirand:JQGrid runat="server" ID="grdAddress" DataSourceID="sdsAddress" Height="150px"
            AutoWidth="True" OnDataRequesting="grdAddress_DataRequesting" OnCellBinding="grdAddress_CellBinding">
            <Columns>
                <trirand:JQGridColumn DataField="addressID" HeaderText="" PrimaryKey="True" Visible="false" />
                <trirand:JQGridColumn DataField="addressType" HeaderText="<%$ Resources:Resource, lblAddType %>"
                    Editable="false" Width="40" TextAlign="Center" />
                <trirand:JQGridColumn DataField="addressItem" HeaderText="<%$ Resources:Resource, lblAddressItem %>"
                    Editable="false" Width="40" TextAlign="Center" />
                <trirand:JQGridColumn DataField="addressLine1" HeaderText="<%$ Resources:Resource, lblAddressLine1 %>"
                    Editable="false" />
                <trirand:JQGridColumn DataField="addressLine2" HeaderText="<%$ Resources:Resource, lblAddressLine2 %>"
                    Editable="false" Visible="false" />
                <trirand:JQGridColumn DataField="addressLine3" HeaderText="<%$ Resources:Resource, lblAddressLine3 %>"
                    Editable="false" Visible="false" />
                <trirand:JQGridColumn DataField="addressCity" HeaderText="<%$ Resources:Resource, lblCity %>"
                    Editable="false" Visible="false" />
                <trirand:JQGridColumn DataField="addressState" HeaderText="<%$ Resources:Resource, lblStateOrProvince %>"
                    Editable="false" Visible="false" />
                <trirand:JQGridColumn DataField="addressCountry" HeaderText="<%$ Resources:Resource, lblCountry %>"
                    Editable="false" Visible="false" />
                <trirand:JQGridColumn DataField="addressPostalCode" HeaderText="<%$ Resources:Resource, lblPostalCode %>"
                    Editable="false" Visible="false" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdvEdit %>" DataField="addressID"
                    Sortable="false" TextAlign="Center" Width="50">
                </trirand:JQGridColumn>
                <trirand:JQGridColumn DataField="addressID" HeaderText="<%$ Resources:Resource, lnkCustomerSelect %>"
                    Editable="false" Sortable="false" Width="90" />
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="gridLoadComplete" />
            <%--RowSelect="rowSelect"--%>
        </trirand:JQGrid>
        <iCtrl:IframeDialog ID="mdEdit" Width="750" Height="450" Title="Delete" Dragable="true"
            TriggerSelectorClass="pop_edit" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
            runat="server">
        </iCtrl:IframeDialog>
        <iCtrl:IframeDialog ID="mdInTransit" Width="600" Height="280" Title="In Transit"
            Dragable="true" TriggerSelectorClass="pop_intransit" TriggerSelectorMode="Class"
            UrlSelector="TriggerControlHrefAttribute" runat="server">
        </iCtrl:IframeDialog>
        <iCtrl:IframeDialog ID="mdTags" Width="400" Height="280" Title="Product Tags" Dragable="true"
            TriggerSelectorClass="pop_tags" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
            runat="server">
        </iCtrl:IframeDialog>
        <asp:SqlDataSource ID="sdsAddress" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
        <asp:HiddenField ID="hdnIdToEdit" runat="server" />
        <asp:HiddenField ID="hdnSrc" runat="server" />
        <asp:HiddenField ID="hdnCtrl" runat="server" />
        <asp:HiddenField ID="hdnAddType" runat="server" />
    </div>
    <h3>
        <asp:Literal ID="ltSectionTitle" Text="" runat="server" />
    </h3>
    <asp:Panel ID="pnlEdit" runat="server" DefaultButton="btnSave">
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="lblFirstName" Text="<%$Resources:Resource, lblCustFirstName %>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtFirstName" runat="server" MaxLength="45" />
                </div>
                <div class="lbl">
                    <asp:Label ID="lblLastName" Text="<%$Resources:Resource, lblCustLastName %>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="45" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblAddressLine1" Text="<%$Resources:Resource, lblAddressLine1 %>"
                        runat="server" meta:resourcekey="lblAddressLine1Resource1" />
                    *</div>
                <div class="input">
                    <asp:TextBox ID="txtAddressLine1" runat="server" MaxLength="45"  />
                    <asp:RequiredFieldValidator ID="rfvAddressLine1" ControlToValidate="txtAddressLine1"
                        SetFocusOnError="True" runat="server" ErrorMessage="*" ValidationGroup="submit" ></asp:RequiredFieldValidator>
                </div>
                <div class="lbl">
                    <asp:Label ID="lblAddressLine2" Text="<%$Resources:Resource, lblAddressLine2 %>"
                        runat="server" meta:resourcekey="lblAddressLine2Resource1" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtAddressLine2" runat="server" MaxLength="45"  />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblCity" Text="<%$Resources:Resource, lblCity%>" runat="server"  />
                    *</div>
                <div class="input">
                    <asp:TextBox ID="txtCity" runat="server" MaxLength="45"  />
                    <asp:RequiredFieldValidator ID="rfvCity" ControlToValidate="txtCity" SetFocusOnError="True"
                        runat="server" ErrorMessage="*" ValidationGroup="submit"></asp:RequiredFieldValidator>
                </div>
                <div class="lbl">
                    <asp:Label ID="lblCountry" Text="<%$Resources:Resource, lblCountry%>" runat="server"
                         />
                    *</div>
                <div class="input">
                    <asp:DropDownList ID="ddlCountry" runat="server" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtCountry" runat="server" MaxLength="45" 
                        Style="display: none;" />
                    <asp:RequiredFieldValidator ID="rfvCountry" ControlToValidate="ddlCountry" SetFocusOnError="True"
                        runat="server" ErrorMessage="*" ValidationGroup="submit"></asp:RequiredFieldValidator>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblPostalCode" Text="<%$Resources:Resource, lblPostalCode%>" runat="server"
                        />
                    *</div>
                <div class="input">
                    <asp:TextBox ID="txtPostalCode" runat="server" MaxLength="15"  />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txtPostalCode"
                        SetFocusOnError="True" runat="server" ErrorMessage="*" ValidationGroup="submit"></asp:RequiredFieldValidator>
                </div>
                <div class="lbl">
                    <asp:Label ID="lblState" Text="<%$Resources:Resource, lblStateOrProvince %>" runat="server"
                        />
                    *</div>
                <div class="input">
                    <asp:DropDownList ID="ddlState" runat="server">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtState" runat="server" MaxLength="45" 
                        Style="display: none;" />
                    <asp:RequiredFieldValidator ID="rfvState" ControlToValidate="txtState" SetFocusOnError="True"
                        runat="server" ErrorMessage="*" ValidationGroup="submit"></asp:RequiredFieldValidator>
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
        <div class="div_command">
            <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" ValidationGroup="submit" OnClick="btnSave_Click" />
            <asp:Button ID="btnReset" Text="<%$Resources:Resource, btnReset%>" runat="server"
                CausesValidation="false" OnClick="btnReset_Click" OnClientClick="reloadGrid(); return false;" />
        </div>
    </asp:Panel>
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdAddress.ClientID %>";

        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        function reloadGrid(event, ui) {
            resetForm();
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function gridLoadComplete(data) {
            $(window).trigger("resize");
            ApplyGridButtonSetting();
        }

//        function deleteAddress(id) {
//            if (confirm("Are you sure you want to delete?")) {
//                var dataToPost = {};
//                dataToPost.deleteItem = 1;
//                dataToPost.idToDelete = id;
//                $grid = $("#" + gridID);
//                for (var k in dataToPost) {
//                    $grid.setPostDataItem(k, dataToPost[k]);
//                }
//                $grid.trigger("reloadGrid");

//                for (var k in dataToPost) {
//                    $grid.removePostDataItem(k);
//                }
//            }
//        }

        function resetForm() {
            $("#ctl00_cphMaster_hdnIdToEdit").val("");
            $("#ctl00_cphMaster_txtAddressLine1").val("");
            $("#ctl00_cphMaster_txtAddressLine2").val("");
            $("#ctl00_cphMaster_txtCity").val("");
            $("#ctl00_cphMaster_txtCountry").val("");
            $("#ctl00_cphMaster_ddlCountry").val("");
            $("#ctl00_cphMaster_txtState").val("");
            $("#ctl00_cphMaster_ddlState").val("");
            $("#ctl00_cphMaster_txtPostalCode").val("");
            $("#ctl00_cphMaster_txtFirstName").val("");
            $("#ctl00_cphMaster_txtLastName").val("");
        }


        function rowSelect(id) {
            var dataToPost = {};
            dataToPost.id = id;
            //            var eleToBlock = $("#ctl00_ctl00_cphFullWidth_cphMaster_ctlContent_pnlEdit");
            //            masterBlockContentArea(eleToBlock, "Please wait...");
            $.ajax({
                type: "POST",
                url: 'AddressCollection.aspx/GetCustomerAddress',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(dataToPost),
                success: function (data) {
                    var addrObj = data.d;
                    if (addrObj != null && addrObj.AddressID > 0) {
                        $("#ctl00_cphMaster_hdnIdToEdit").val(addrObj.AddressID);
                        $("#ctl00_cphMaster_txtAddressLine1").val(addrObj.AddressLine1);
                        $("#ctl00_cphMaster_txtAddressLine2").val(addrObj.AddressLine2);
                        $("#ctl00_cphMaster_txtCity").val(addrObj.AddressCity);
                        $("#ctl00_cphMaster_txtCountry").val(addrObj.AddressCountry);
                        $("#ctl00_cphMaster_ddlCountry").val(addrObj.AddressCountry);
                        $("#ctl00_cphMaster_ddlCountry").trigger("change");
                        $("#ctl00_cphMaster_txtState").val(addrObj.AddressState);
                        $("#ctl00_cphMaster_ddlState").val(addrObj.AddressState);
                        $("#ctl00_cphMaster_txtPostalCode").val(addrObj.AddressPostalCode);
                        $("#ctl00_cphMaster_txtFirstName").val(addrObj.FirstName);
                        $("#ctl00_cphMaster_txtLastName").val(addrObj.LastName);
                    }
                    //                    masterUnblockContentArea(eleToBlock);
                },
                error: function (request, status, errorThrown) {

                    //                    masterUnblockContentArea(eleToBlock);
                }
            });
        }

        $("#<%=ddlCountry.ClientID%>").change(function () {
            var country = $(this).val();
            $("#<%=txtCountry.ClientID%>").val(country);
            $('option', $("#<%=ddlState.ClientID%>")).remove();
            $("#<%=ddlState.ClientID%>").append($("<option />").val("").text(""));
            for (var i = 0; i < _countryStateList.length; i++) {
                if (_countryStateList[i].Country == country) {
                    $("#<%=ddlState.ClientID%>").append($("<option />").val(_countryStateList[i].StateCode).text(_countryStateList[i].StateCode));
                }
            }
        });

        $("#<%=ddlState.ClientID%>").live("change", function () {
            var state = $(this).val();
            $("#<%=txtState.ClientID%>").val(state);
        });


        function SelectAddress(custID) {
            var src = $("#ctl00_cphMaster_hdnSrc").val();
            var ctlrID = $("#ctl00_cphMaster_hdnCtrl").val();
            var addType = $("#ctl00_cphMaster_hdnAddType").val();
            if (src == 'cust') {
                jQuery.FrameDialog.closeDialog();
            }
            else if (src == 'SO') {
                parent.setOrderAddress(custID, ctlrID, addType);
                jQuery.FrameDialog.closeDialog();
            }
        }

        function ApplyGridButtonSetting() {
            $('.updateQty').each(function () {
                $(this).addClass("ButtonCss ui-button");
            });
        }  
           
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
</asp:Content>

Imports System
Imports System.IO
Imports System.Data.Odbc
Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Configuration.ConfigurationManager

Partial Class Shp_Print
    Inherits BasePage
    Private sHtmlData As String = ""
    'On Page Load
    Dim objSO As New clsOrders
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            subClose()
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("SOID") <> "" And Request.QueryString("AsmL") <> "" Then
                subHTMLGenAsmL(Request.QueryString("SOID"))
            End If
            If Request.QueryString("SOID") <> "" And Request.QueryString("AsmL") = "" Then
                subHTMLGen(Request.QueryString("SOID"))
            End If
        End If
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Request.QueryString("SOID") <> "" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(objSO.funGetCustomerLanguage(Request.QueryString("SOID")))
        Else
            If Not Session("Language") = "en-CA" Then
                Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
            End If
        End If
    End Sub
    ' Sub routine to close popup
    Private Sub subClose()
        Response.Write("<script>window.opener.location.reload();</script>")
        Response.Write("<script>window.close();</script>")
    End Sub
    'Printing Assembly List
    Private Sub subHTMLGenAsmL(ByVal strSOID As String)
        Dim objUser As New clsUser
        Dim objPrd As New clsProducts
        Dim objPrdKit As New clsProductKits
        Dim objAssVendor As New clsPrdAssociateVendor


        Dim objSOI As New clsOrderItems
        Dim objPrdQty As New clsPrdQuantity

        Dim objPrn As New clsPrint
        Dim objCom As New clsCommon
        Dim sOrdData As String = ""
        Dim TotalUnits As Int32 = 0
        If strSOID <> "" Then

            objSO.OrdID = strSOID
            objSO.getOrdersInfo()

            Dim drSOI As OdbcDataReader
            drSOI = objSOI.GetOrderProductsByOrderId(strSOID)


            '//////////////////////////////////////////////////////
            sHtmlData += "<h1>" & prnAsmblyList & "</h1><hr><br/>"
            sHtmlData += "<table border=0 cellpadding=5 cellspacing=0 width=100%>"
            sHtmlData += "<tr><td align=left width=35%> " & lblAssListDate & " &nbsp;<b>" & DateTime.Now & "</b></td>"
            sHtmlData += "<td align=left width=35%>" & lblAssListDateOfShipment & " &nbsp;<b>" & objSO.OrdShpDate & "</b></td>"
            sHtmlData += "<td align=left width=30%>&nbsp;</td></tr>"

            sHtmlData += "<tr><td align=left> " & lblAssListCustPO & " &nbsp;<b>" & objSO.OrdCustPO & "</b></td>"
            sHtmlData += "<td align=left>" & lblAssListSRep & " &nbsp;<b>" & objUser.funUserName(objSO.OrdSalesRepID) & "</b></td>"
            sHtmlData += "<td align=left>&nbsp;</td></tr>"

            sHtmlData += "<tr height=10px><td colspan=3></td></tr>"
            While (drSOI.Read)
                objPrd.ProductID = drSOI("ordProductID").ToString
                objPrd.subGetProductsInfo()
                Dim vendors As String = objAssVendor.GetVendorNames(objPrd.ProductID)

                Dim Qty As Int32 = objSOI.GetOrderProductsQty(strSOID, objPrd.ProductID, objPrd.PrdIsKit)
                TotalUnits += Qty
                sOrdData += "<tr>"
                sOrdData += "<td align=left>" & objPrd.PrdIntID & "</td>"
                'sOrdData += "<td align=left>" & vendors & "</td>"
                sOrdData += "<td align=left>" & objPrd.PrdUPCCode & "</td>"
                sOrdData += "<td align=left>" & objPrd.PrdName & "</td>"
                sOrdData += "<td align=right>" & Qty & "</td>"
                sOrdData += "</tr>"

                ' Execute if Prduct Is kit
                If (objPrd.PrdIsKit = "1") Then
                    sHtmlData += "  <tr><td align=left>" & prnPrdIntId & "&nbsp;<b>" & objPrd.PrdIntID & "</b></td>"
                    sHtmlData += "<td align=left>" & prnPrdUPCCode & " &nbsp;<b>" & objPrd.PrdUPCCode & "</b></td>"
                    sHtmlData += "<td align=left>&nbsp;</td></tr>"
                    'sHtmlData += "<td align=left>" & prnVndorID & ":  &nbsp;<b>" & vendors & " </b></td></tr>"
                    'sHtmlData += "<tr height=10px><td colspan=3></td></tr>"
                    sHtmlData += "<tr><td colspan=3 align=left>" & prnProductName & " &nbsp;&nbsp;<b>" & objPrd.PrdName & "</b></p></td></tr>"
                    'sHtmlData += "<tr height=10px><td colspan=3></td></tr>"
                    sHtmlData += "<tr><td colspan=3 align=left><b>" & prnProductKitList & "</b></td></tr>"
                    sHtmlData += "<tr>"
                    sHtmlData += "<td colspan=3>"
                    sHtmlData += "<table width=100% border=1 cellspacing=0 cellpadding=5>"
                    sHtmlData += "<thead>"
                    sHtmlData += "<th width=16% scope=col align=left>" & prnInternalId & "</th>"
                    'sHtmlData += "<th width=16% scope=col align=left>" & prnVndorID & "</th>"
                    sHtmlData += "<th width=16% scope=col align=left>" & prnBarCode & "</th>"
                    sHtmlData += "<th width=24% scope=col align=left>" & prnName & "</th>"
                    sHtmlData += "<th width=14% scope=col align=right>" & prnNoOfUnits & "</th>"
                    sHtmlData += "<th width=6% scope=col align=right>" & prnBlock & "</th>"
                    sHtmlData += "<th width=6% scope=col align=right>" & prnFloor & "</th>"
                    sHtmlData += "<th width=6% scope=col align=right>" & prnAisle & "</th>"
                    sHtmlData += "<th width=6% scope=col align=right>" & prnRack & "</th>"
                    sHtmlData += "<th width=6% scope=col align=right>" & prnBin & "</th>"
                    sHtmlData += "</thead>"

                    Dim drItems As OdbcDataReader
                    drItems = objPrdKit.GetProductKitsInfoByPrdId(objPrd.ProductID)
                    While (drItems.Read)
                        objPrd.ProductID = drItems("prdIncludeID").ToString
                        objPrd.subGetProductsInfo()

                        objPrdQty.ProductID = objPrd.ProductID
                        objPrdQty.prdWhsCode = objSO.OrdShpWhsCode
                        objPrdQty.getProductQuantityByPrdIDnWhs()

                        sHtmlData += "<tr>"
                        sHtmlData += "<td align=left>" & IIf(objPrd.PrdIntID = "", "--", objPrd.PrdIntID) & "</td>"
                        'sHtmlData += "<td align=left>" & objAssVendor.GetVendorNames(objPrd.ProductID) & "</td>"
                        sHtmlData += "<td align=left>" & objPrd.PrdUPCCode & "</td>"
                        sHtmlData += "<td align=left>" & objPrd.PrdName & "</td>"
                        sHtmlData += "<td align=right>" & drItems("prdIncludeQty").ToString & "</td>"
                        sHtmlData += "<td align=right>" & IIf(objPrdQty.PrdBlock = "", "--", objPrdQty.PrdBlock) & "</td>"
                        sHtmlData += "<td align=right>" & IIf(objPrdQty.PrdFloor = "", "--", objPrdQty.PrdFloor) & "</td>"
                        sHtmlData += "<td align=right>" & IIf(objPrdQty.PrdAisle = "", "--", objPrdQty.PrdAisle) & "</td>"
                        sHtmlData += "<td align=right>" & IIf(objPrdQty.PrdRack = "", "--", objPrdQty.PrdRack) & "</td>"
                        sHtmlData += "<td align=right>" & IIf(objPrdQty.PrdBin = "", "--", objPrdQty.PrdBin) & "</td>"
                        sHtmlData += "</tr>"
                    End While
                    sHtmlData += "</table>"
                    sHtmlData += "</td>"
                    sHtmlData += "</tr>"
                    'Else
                    'sHtmlData += "<tr><td colspan=3 align=left><p><b>" & prnProductName & " &nbsp;&nbsp;" & objPrd.PrdName & "</b></p></td></tr>"
                End If
                sHtmlData += "<tr height=10px><td colspan=3></td></tr>"
            End While

            sHtmlData += "<tr height=10px align=left><td colspan=3><b>--------------------</b></td></tr>"
            sHtmlData += "<tr><td colspan=3 align=left><b>" & prnOrderNo & ": &nbsp; " & strSOID & "</b> </td></tr>"
            'sHtmlData += "<tr height=10px><td colspan=3></td></tr>"
            sHtmlData += "<tr><td colspan=3 align=left><b> " & prnTotalNoOfUnits & ": &nbsp; " & TotalUnits & " </b></td></tr>"
            sHtmlData += "<tr>"
            sHtmlData += "<td colspan=3>"
            sHtmlData += "<table width=100% border=1 cellspacing=0 cellpadding=5>"
            sHtmlData += "<thead>"
            sHtmlData += "<th width=16% scope=col align=left>" & prnInternalId & "</th>"
            'sHtmlData += "<th width=16% scope=col align=left>" & prnVndorID & "</th>"
            sHtmlData += "<th width=16% scope=col align=left>" & prnBarCode & "</th>"
            sHtmlData += "<th width=37% scope=col align=left>" & prnName & "</th>"
            sHtmlData += "<th width=15% scope=col align=right>" & prnTotalNoOfUnits & "</th>"
            sHtmlData += " </thead>"

            sHtmlData += sOrdData

            sHtmlData += "</table>"
            sHtmlData += "</td>"
            sHtmlData += "</tr>"
            sHtmlData += "</table>"
            lblData.ForeColor = Drawing.Color.Black
            lblData.Text = sHtmlData
        End If
    End Sub
    'Printing Content
    Private Sub subHTMLGen(ByVal strReqID As String)
        Dim objCompany As New clsCompanyInfo
        Dim objPrn As New clsPrint
        Dim objCom As New clsCommon

        Dim objOrder As New clsOrders
        Dim objOrderItms As New clsOrderItems

        If strReqID <> "" Then
            objOrder.OrdID = strReqID
            objOrder.getOrdersInfo()
            objCompany.CompanyID = objOrder.ordCompanyID

            objCompany.getCompanyInfo()
            Dim strAddress As String = objCompany.CompanyAddressLine1 & IIf(objCompany.CompanyAddressLine2 <> "", ", " & objCompany.CompanyAddressLine2, "") & Microsoft.VisualBasic.Chr(10) & IIf(objCompany.CompanyCity <> "", objCompany.CompanyCity, "") & IIf(objCompany.CompanyState <> "", ", " & objCompany.CompanyState, "") & IIf(objCompany.CompanyCountry <> "", ", " & objCompany.CompanyCountry, "") & IIf(objCompany.CompanyPostalCode <> "", ", " & objCompany.CompanyPostalCode, "")
            strAddress = IIf(strAddress <> "", strAddress, "-")
            Dim strTelFax As String = lblPhone & " " & IIf(objCompany.CompanyPOPhone <> "", objCompany.CompanyPOPhone, "-") & " | " & lblFax & " " & IIf(objCompany.CompanyFax <> "", objCompany.CompanyFax, "-")
            Dim strEmail As String = lblEmail & " " & objCompany.CompanyEmail

            Dim objUser As New clsUser
            objUser.UserID = objOrder.OrdLastUpdateBy
            objUser.getUserInfo()

            Dim strUseName As String = objUser.UserFirstName & " " & objUser.UserLastName

            Dim strCurrency As String = ""
            Dim strVendorInfo As String = ""
            strCurrency = objOrder.OrdCurrencyCode
            Dim objCust As New clsExtUser
            objCust.CustID = objOrder.OrdCustID
            objCust.CustType = objOrder.OrdCustType
            objCust.getCustomerInfoByType()
            Dim strCustName As String = objCust.CustName


            ' Bill To
            strVendorInfo += strCustName & "<BR>"
            strVendorInfo += Microsoft.VisualBasic.Chr(10)
            strVendorInfo += funPopulateAddress(objOrder.OrdCustType, "B", objOrder.OrdCustID)

            ' Shipping
            Dim strShippingInfo As String = ""
            If objOrder.OrdShpBlankPref = "0" Then
                strShippingInfo += strCustName
            End If
            strShippingInfo += "<BR>"
            strShippingInfo += Microsoft.VisualBasic.Chr(10)
            strShippingInfo += funPopulateAddress(objOrder.OrdCustType, "S", objOrder.OrdCustID)

            '//////////////////////////////////////////////////////
            sHtmlData += "<table width='800' border='1' align='center' cellpadding='0' cellspacing='0'>"
            sHtmlData += "<tr><td>"

            'Header Start
            If objOrder.OrdShpBlankPref = "0" Then
                sHtmlData += "<table width='800' border='0' align='center' cellspacing='0' cellpadding='5'>"
                sHtmlData += "<tr valign='middle'>"
                sHtmlData += "<td width='13%' rowspan='2' height=60 align='left'> "
                If objCompany.CompanyLogoPath <> "" Then
                    sHtmlData += "<img id=img src=" & ConfigurationManager.AppSettings("CompanyLogoPath") & objCompany.CompanyLogoPath & " " & objCom.funImageSize(objCompany.CompanyLogoPath, "90", "80", "Comp") & "  />"
                End If
                sHtmlData += "</td>"
                sHtmlData += "<td width='62%' height=60 align='left'> "
                sHtmlData += "<b>" & objCompany.CompanyName & "</b><br /><br />"
                sHtmlData += strAddress & "<br />"
                sHtmlData += strTelFax & "<br />"
                sHtmlData += strEmail
                sHtmlData += "</td>"
                sHtmlData += "<td width='25%' height=60 align='Right'>"
                sHtmlData += "<H2>" & prnShipPackingList & "</H2>"
                sHtmlData += "</td>"
                sHtmlData += "</tr>"
                sHtmlData += "</table>"
            Else
                sHtmlData += "<table width='800' border='0' align='center' cellspacing='0' cellpadding='5'>"
                sHtmlData += "<tr valign='middle'>"
                sHtmlData += "<td width='13%' rowspan='2' height=60 align='left'> "
                sHtmlData += "</td>"
                sHtmlData += "<td width='62%' height=60 align='left'> "
                sHtmlData += "</td>"
                sHtmlData += "<td width='25%' height=60 align='Right'>"
                sHtmlData += "<H2>" & prnShipPackingList & "</H2>"
                sHtmlData += "</td>"
                sHtmlData += "</tr>"
                sHtmlData += "</table>"
            End If

            'Header Close

            'Line Start
            sHtmlData += "<table width='100%' cellpadding='0' cellspacing='0' style='border: 1px solid #666666; background: #FFFFFF;'><tr><td  width='100%' align='center'><img src='../images/spacer1.gif' width='800' style='border: 1px;background: #FFFFFF; background-color:#330066;'></td></tr></table>"
            'Line Close

            'Invoice Date/No/BillTO/ShipTo Start
            sHtmlData += "<table width='800' border='0' height='40' align='center' cellpadding='5' cellspacing='0' style='border: 0px solid #666666; background: #FFFFFF;'>"
            sHtmlData += "<tr><td>&nbsp;</td><td>"
            'Date/ No Start
            sHtmlData += "<table  border='1' width='50%' align='right' cellpadding='5'  cellspacing='0'>"
            sHtmlData += "<tr><td width='30%' align='left' height='20' >"
            sHtmlData += prnDate + ": <b>" + IIf(objOrder.OrdCreatedOn <> "", CDate(objOrder.OrdCreatedOn).ToString("MMM-dd yyyy"), "--")
            sHtmlData += "</b></td></tr>"

            sHtmlData += "<tr><td width='30%' align='left' height='20'>"
            sHtmlData += prnOrderNo + "#: <b>" + objOrder.OrdID
            'sHtmlData += "</b></td></tr>"

            'sHtmlData += "<tr><td width='30%' align='left' height='20'>"
            sHtmlData += "</b><BR/>"
            sHtmlData += prnShpLstCustPO + "#: <b>" + objOrder.OrdCustPO
            sHtmlData += "</b><BR/>"

            sHtmlData += prnShpLstAcronym + "#: <b>" + objCust.CustAcronyme
            sHtmlData += "</b><BR/>"

            If objOrder.OrdShpBlankPref = "0" Then
                'sHtmlData += "<tr><td width='30%' align='left' height='20'>"
                sHtmlData += lblAssListSRep + "#: <b>" + objUser.funUserName(objOrder.OrdSalesRepID) + "</b>"
            End If

            sHtmlData += "</td></tr>"
            'Date/No Close
            sHtmlData += "</table></td></tr>"

            sHtmlData += "<tr>"
            'BillTo Start
            sHtmlData += "<td>"

            sHtmlData += "<table align='center' cellpadding='5' cellspacing='0' style='min-height:120;'>"
            sHtmlData += "<tr>"
            sHtmlData += "<td align='left'><font>" + prnShpPckLstBillTo + "</font></td>"
            sHtmlData += "</tr>"
            sHtmlData += "<tr>"
            sHtmlData += "<td align='left' width='375' height='100' style='border: 1px solid #666666; background: #FFFFFF;' valign=top>" + strVendorInfo + "</td>"
            sHtmlData += "</tr>"
            sHtmlData += "</table>"

            sHtmlData += "</td>"
            'BillTo Close
            'ShipTo Start
            sHtmlData += "<td>"

            sHtmlData += "<table align='center' cellpadding='5' cellspacing='0' style='min-height:120;'>"
            sHtmlData += "<tr>"
            sHtmlData += "<td align='left'><font>" + prnShipTo + "</font></td>"
            sHtmlData += "</tr>"
            sHtmlData += "<tr>"
            sHtmlData += "<td align='left' width='375' height='100' style='border: 1px solid #666666; background: #FFFFFF;' valign=top>" + strShippingInfo + "</td>"
            sHtmlData += "</tr>"
            sHtmlData += "</table>"

            sHtmlData += "</td>"
            'ShipTo Close
            sHtmlData += "</tr>"
            sHtmlData += "</table>"
            'Invoice Date/No/BillTO/ShipTo Close
            Dim sStyle As String = "style='border: 1px solid #666666;'"
            'Invoice Items Start
            sHtmlData += "<br /><br />"
            sHtmlData += "<table border='1' align='center' width='99%' cellpadding='5' cellspacing='0'>"
            'Header Start
            sHtmlData += "<tr>"
            sHtmlData += "<td align='left' height='30'><b>" + grdPOUPCCode + "</b></td>"
            sHtmlData += "<td align='left' height='30'><b>" + POProduct + "</b></td>"
            sHtmlData += "<td align='right' height='30'><b>" + POQuantity + "</b></td>"
            sHtmlData += "</tr>"
            'Header Close

            'Data Loop Start
            Dim drItems As OdbcDataReader
            drItems = objOrderItms.GetDataReader(objOrderItms.funFillGrid(strReqID))
            Dim dblSubTotal As Double = 0
            Dim dblTotal As Double = 0
            While drItems.Read
                sHtmlData += "<tr>"
                sHtmlData += "<td align='left' height='35'>" & drItems("prdUPCCode").ToString & "</td>"
                sHtmlData += "<td align='left' height='35'>" & drItems("prdName") & "</td>"
                sHtmlData += "<td align='right' height='35'>" & drItems("ordProductQty") & "</td>"
                sHtmlData += "</tr>"
            End While
            drItems.Close()
            'Data Loop Close

            sHtmlData += "</table>"
            sHtmlData += "<br /><br />"

            'Footer Start
            sHtmlData += "<table width='800' border='0'>"
            sHtmlData += "<tr>"

            'Ftr Left TD Start
            sHtmlData += "<td valign=top>"
            sHtmlData += "<table border='0' align='left' cellspacing='0' cellpadding='5'>"

            If objOrder.OrdShpBlankPref = "0" Then
                sHtmlData += "<tr>"
                sHtmlData += "<td width='75%' height=50 align=left>" & prnAuthorisedby & " " & strUseName & "</td>"
                sHtmlData += "</tr>"
            End If
        
            sHtmlData += "<tr>"
            sHtmlData += "<td width='25%' height=50 align=left>" & prnMemo & " " & objOrder.OrdComment & "</td>"
            sHtmlData += "</tr>"
            sHtmlData += "<tr>"
            sHtmlData += "<td  height=50 align=left>" & lblPacklstShippingTerms & " " & objOrder.ordShippingTerms & "</td>"
            sHtmlData += "</tr>"
            sHtmlData += "</table>"
            sHtmlData += "</td>"
            'Ftr Left TD Close

            'Ftr Right TD Start
            sHtmlData += "<td valign=top>"

            sHtmlData += "</td>"
            'Ftr Right TD Close

            sHtmlData += "</tr>"
            sHtmlData += "</table>"
            'Footer Close

            sHtmlData += "<br /><br />"
            sHtmlData += "</td></tr>"
            sHtmlData += "</table>"

            objOrder.CloseDatabaseConnection()
            objOrder = Nothing
            objOrderItms.CloseDatabaseConnection()
            objOrderItms = Nothing

            lblData.ForeColor = Drawing.Color.Black
            lblData.Text = sHtmlData
        End If
    End Sub
    ' Get Address
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)
        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + "<br />"
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + "<br />"
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + "<br />"
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + "<br />"
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + "<br />"
            Else
                strAddress += objAdr.AddressState + "<br />"
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + "<br />"
            Else
                strAddress += objAdr.AddressCountry + "<br />"
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function
    'Get Warehouse Address
    Protected Function funWarehouseAddress(ByVal strWhsCode As String)
        Dim strData As String = ""
        If strWhsCode <> "" Then
            Dim objWhs As New clsWarehouses
            objWhs.WarehouseCode = strWhsCode
            objWhs.getWarehouseInfo()
            If objWhs.WarehouseDescription <> "" Then
                strData += objWhs.WarehouseDescription + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseAddressLine1 <> "" Then
                strData += objWhs.WarehouseAddressLine1 + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseAddressLine2 <> "" Then
                strData += objWhs.WarehouseAddressLine2 + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseCity <> "" Then
                strData += objWhs.WarehouseCity + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseState <> "" Then
                strData += objWhs.WarehouseState + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseCountry <> "" Then
                strData += objWhs.WarehouseCountry + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehousePostalCode <> "" Then
                strData += objWhs.WarehousePostalCode
            End If
            objWhs = Nothing
        End If
        Return strData
    End Function
End Class

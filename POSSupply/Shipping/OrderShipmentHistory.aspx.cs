﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Shipping_OrderShipmentHistory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void grdOrderSH_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 5)
        {
            e.CellHtml = string.Format("<a onclick=OrderEdit('{0}')>   {1}</a>", e.RowValues[0], Resources.Resource.lblEdit);
        }
        if (e.ColumnIndex == 4)
        {
            //string strurl = "../invoice/ViewInvoiceDetails.aspx?SOID=" + e.RowValues[4] + "&ComID=5";
            e.CellHtml = string.Format("<a onclick=InvoiceEdit('{0}')>   {1}</a>", e.RowValues[4], e.RowValues[4]);
        }
    }

    protected void grdOrderSH_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        Orders objord = new Orders();
        grdOrderSH.DataSource = objord.GetShipmentHistoryDT(null, this.OrderID, Globals.CurrentAppLanguageCode);
    }

    public int OrderID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["ordid"], out id);
            return id;
        }
    }
}
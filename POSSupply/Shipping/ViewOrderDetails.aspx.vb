Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Partial Class Shipping_ViewOrderDetails
    Inherits BasePage
    Protected TaxString As String
    Private strSOID As String = ""
    Private objSO As New clsOrders
    Private objSOItems As New clsOrderItems
    Private objSOIP As New clsOrderItemProcess
    Private objCust As New clsExtUser
    Private objStatus As New clsSysStatus
    Private objJob As New clsJobPlanning
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If

        If Convert.ToString(Request.QueryString("SOID")) <> "" Then
            strSOID = Convert.ToString(Request.QueryString("SOID"))
        End If

        If Request.QueryString("JobDtl") = "1" Then
            imgPrintPackList.Visible = False
            'imgPrintAsmlyList.Visible = False
            cmdReset.Visible = False
            cmdBack.Visible = True
            lblTitle.Text = lblViewJobDtl
        Else
            lblTitle.Text = lblViewShippingOrderDetails '"View Order Details"
        End If

        If Not Page.IsPostBack Then
            subfillSelRepForJobDtl()
            If Request.QueryString("JobDtl") = "1" Then
                objStatus.subGetStatus(dlStatus, "SH", "JBSts")
            Else
                objStatus.subGetStatus(dlStatus, "SH", "SHSts")
            End If

            subPopulateCurrency()
            strSOID = Convert.ToString(Request.QueryString("SOID"))
            subFillGrid()
            objSO.OrdID = strSOID
            objSO.getOrders()
            subPopulateWarehouse(objSO.ordCompanyID)

            subPopulateTerms()
            If objSO.OrdCreatedOn <> "" Then
                lblSODate.Text = CDate(objSO.OrdCreatedOn).ToString("MMM-dd yyyy")
            End If

            txtTerms.Text = objSO.ordShippingTerms
            lblSOID.Text = objSO.OrdID
            objCust.CustID = objSO.OrdCustID
            objCust.CustType = objSO.OrdCustType
            objCust.getCustomerInfoByType()
            txtNetTerms.Text = objCust.CustNetTerms
            dlCurrencyCode.SelectedValue = objSO.OrdCurrencyCode
            txtExRate.Text = objSO.OrdCurrencyExRate
            txtBillToAddress.Text = funPopulateAddress(objCust.CustType, "B", objCust.CustID).Replace("<br>", vbCrLf)
            txtShpToAddress.Text = funPopulateAddress(objCust.CustType, "S", objCust.CustID).Replace("<br>", vbCrLf)
            lblAddress.Text = funPopulateAddress(objCust.CustType, "S", objCust.CustID)
            lblCustomerName.Text = objCust.CustName
            'lblTerms.Text = objCust.CustNetTerms
            lblFax.Text = objCust.CustFax
            'lblAmount.Text = objSO.OrdCurrencyCode & " " & CDbl(objSO.Amount) + CDbl(objSOIP.getProcessCostForOrderNo())
            txtNotes.Text = objSO.OrdComment
            dlStatus.SelectedValue = objSO.OrdStatus
            rblstShipBlankPref.SelectedValue = objSO.OrdShpBlankPref
            rblstISWeb.SelectedValue = objSO.OrdSaleWeb
            If objSO.OrdShpDate <> "" Then
                txtShippingDate.Text = CDate(objSO.OrdShpDate).ToString("MM/dd/yyyy")
            End If
            txtShpTrackNo.Text = objSO.OrdShpTrackNo
            txtShpCost.Text = objSO.OrdShpCost
            txtShpCode.Text = objSO.OrdShpCode
            dlCurrencyCode.SelectedValue = objSO.OrdCurrencyCode
            txtCustPO.Text = objSO.OrdCustPO
            dlShpWarehouse.SelectedValue = objSO.OrdShpWhsCode
            'subCalcTax()
            Me.imgPrintPackList.Attributes.Add("OnClick", "return openPDF('print.aspx?SOID=" & Request.QueryString("SOID") & " ')")
            Me.imgPrintAsmlyList.Attributes.Add("OnClick", "return openPDF('print.aspx?SOID=" & Request.QueryString("SOID") & "&AsmL=T')")
        Else
            If Convert.ToString(Request.QueryString("SOID")) <> "" And strSOID <> "" Then
                ' subCalcTax()
            End If
        End If
    End Sub
    Public Sub subfillSelRepForJobDtl()
        sqldsdetail.SelectCommand = objJob.funFillGrid(Request.QueryString("SOID"))  'fill Orders
    End Sub

    'Populate Address
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)
        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + "<br>"
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + "<br>"
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + "<br>"
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + "<br>"
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + "<br>"
            Else
                strAddress += objAdr.AddressState + "<br>"
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + "<br>"
            Else
                strAddress += objAdr.AddressCountry + "<br>"
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += lblPostalCode & " " + objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function
    ' Sql Data Source Event Track
    Protected Sub sqldsOrdItems_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsOrdItems.Selected
        subCalcAmount()
    End Sub
    'Populate Grid
    Public Sub subFillGrid()
        sqldsOrdItems.SelectCommand = objSOItems.funFillGrid(strSOID)  'fill Order Items
        objSOIP.OrdID = strSOID
        objSOItems.OrdID = strSOID
        sqldsAddProcLst.SelectCommand = objSOIP.funFillGridForProcess 'fill Order Items Process
    End Sub
    'On Row Data Bound
    Protected Sub grdOrdItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdOrdItems.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & SOItemConfirmDelete & " ')")
            'e.Row.Cells(9).Text = String.Format("{0:F}", CDbl(e.Row.Cells(9).Text))
            'e.Row.Cells(11).Text = String.Format("{0:F}", CDbl(e.Row.Cells(11).Text))
        End If
    End Sub
    'Deleting
    Protected Sub grdOrdItems_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdOrdItems.RowDeleting
        Dim strItemID As String = grdOrdItems.DataKeys(e.RowIndex).Value.ToString()
        objSOItems.OrderItemID = strItemID
        sqldsOrdItems.DeleteCommand = objSOItems.funDeleteSOItems()
        lblMsg.Text = msgSOOrderItemDeletedSuccessfully '"Sales Order Item Deleted Successfully"
        subFillGrid()
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub

    'Populate Object
    Private Sub subSetData(ByRef objS As clsOrders)
        objS.OrdType = objS.OrdType
        objS.OrdCustType = objS.OrdCustType
        objS.OrdCustID = objS.OrdCustID
        objS.OrdCreatedOn = CDate(objS.OrdCreatedOn).ToString("yyyy-MM-dd hh:mm:ss")
        objS.OrdCreatedFromIP = objS.OrdCreatedFromIP
        objS.OrdStatus = dlStatus.SelectedValue
        If dlStatus.SelectedValue = "S" Then
            objS.OrdStatus = "D"
        End If
        If dlStatus.SelectedValue = "V" Then
            objS.OrdVerified = 1
            objS.OrdVerifiedBy = Session("UserID").ToString
        Else
            objS.OrdVerified = objS.OrdVerified
            objS.OrdVerifiedBy = objS.OrdVerifiedBy
        End If
        objS.OrdSalesRepID = objS.OrdSalesRepID
        objS.OrdSaleWeb = rblstISWeb.SelectedValue
        objS.OrdShpBlankPref = rblstShipBlankPref.SelectedValue
        objS.OrdLastUpdateBy = Session("UserID").ToString
        objS.OrdLastUpdatedOn = Now.ToString("yyyy-MM-dd hh:mm:ss")
        objS.OrdShpCode = txtShpCode.Text
        objS.OrdShpWhsCode = dlShpWarehouse.SelectedValue
        objS.OrdShpCost = txtShpCost.Text
        objS.QutExpDate = objS.QutExpDate
        Dim strDate As String = ""
        If IsDate(objS.QutExpDate) = True Then
            strDate = CDate(objS.QutExpDate).ToString("yyyy-MM-dd HH:MM:ss")
        End If
        objS.QutExpDate = strDate
        If txtShippingDate.Text <> "" Then
            If IsDate(DateTime.ParseExact(txtShippingDate.Text, "MM/dd/yyyy", Nothing)) = True Then
                objS.OrdShpDate = DateTime.ParseExact(txtShippingDate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
            Else
                If IsDate(objS.OrdShpDate) = True Then
                    objS.OrdShpDate = CDate(objS.OrdShpDate).ToString("yyyy-MM-dd hh:mm:ss")
                Else
                    objS.OrdShpDate = ""
                End If
            End If
        Else
            If IsDate(objS.OrdShpDate) = True Then
                objS.OrdShpDate = CDate(objS.OrdShpDate).ToString("yyyy-MM-dd hh:mm:ss")
            Else
                objS.OrdShpDate = ""
            End If
        End If
        objS.OrdShpTrackNo = txtShpTrackNo.Text
        objS.OrdCurrencyCode = dlCurrencyCode.SelectedValue
        objS.OrdCurrencyExRate = txtExRate.Text
        objS.OrdComment = txtNotes.Text
        objS.OrdCustPO = txtCustPO.Text
        If IsDate(objS.QutExpDate) = True Then
            objS.QutExpDate = CDate(objS.QutExpDate).ToString("yyyy-MM-dd hh:mm:ss")
        Else
            objS.QutExpDate = ""
        End If
        Dim str As String = objSO.ordCompanyID

        If objS.InvRefNo = "" Then
            objS.InvRefNo = "0"
        End If
        If objS.invDate = "" Then
            objS.invDate = "sysNull"
        Else
            objS.invDate = CDate(objS.invDate).ToString("yyyy-MM-dd hh:mm:ss")
        End If

    End Sub
    'On Submit
    Protected Sub cmdSave_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.ServerClick
        If Not Page.IsValid Then
            'Exit Sub
        End If
        If dlStatus.SelectedValue = "A" And txtCustPO.Text = "" Then
            lblMsg.Text = msgSOPlzEntCustomerPO '"Please enter Customer PO."
            txtCustPO.Focus()
            Exit Sub
        ElseIf dlStatus.SelectedItem.Text = "" Then
            lblMsg.Text = msgSOPlzSelStatus '"Please select Order Status."
            dlStatus.Focus()
            Exit Sub
        End If

        objSO.OrdID = Request.QueryString("SOID")
        objSO.getOrdersInfo()
        subSetData(objSO)
        If dlStatus.SelectedValue = "S" Then
            If objSO.updateOrders() = True Then
                'Update quantity SORsv and OnHand
                objSO.updateProductQuantityOnShipping()
                'Invoice Process Start
                funInvoiceProcess()
                'Invoice Process End
                Session.Add("Msg", OrderShippedSuccessfully)
            Else
                Session.Add("Msg", OrderNotShippedSuccessfully)
            End If
        ElseIf dlStatus.SelectedValue = "Z" Then
            If objSO.updateOrders() = True Then
                Session.Add("Msg", OrderHeldSuccessfully)
            Else
                Session.Add("Msg", OrderNotHeldSuccessfully)
            End If
        Else
            If objSO.updateOrders() = True Then
                Session.Add("Msg", OrderUpdatedSuccessfully)
            Else
                Session.Add("Msg", OrderNotUpdatedSuccessfully)
            End If
        End If
        If Request.QueryString("JobDtl") = "1" Then
            Response.Redirect("Jobs.aspx")
        Else
            Response.Redirect("Default.aspx")
        End If
    End Sub
    ' On Back
    Protected Sub cmdBack_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReset.ServerClick
        Response.Redirect("ViewShipping.aspx?SOID=" & Request.QueryString("SOID").ToString & "&opt=" & Request.QueryString("opt").ToString & "&srh=" & Request.QueryString("srh").ToString & "&whs=" & Request.QueryString("whs").ToString)
    End Sub
    'Get Address Listing
    Protected Function funAddressListing(ByVal strID As String)
        Dim strData As String = ""
        If strID <> "" Then
            Dim objWhs As New clsWarehouses
            objWhs.WarehouseCode = strID
            objWhs.getWarehouseInfo()
            strData = "<table align='left' width=100% cellspacing='0' cellpadding='0' border='0' style='border:0px'><tr><td align='left'>"
            If objWhs.WarehouseCode <> "" Then
                'strData += "<tr><td align='left'>" + objWhs.WarehouseCode + "</td></tr>"
            End If
            If objWhs.WarehouseDescription <> "" Then
                strData += objWhs.WarehouseDescription + "<br>"
            End If
            If objWhs.WarehouseAddressLine1 <> "" Then
                strData += objWhs.WarehouseAddressLine1 + "<br>"
            End If
            If objWhs.WarehouseAddressLine2 <> "" Then
                strData += objWhs.WarehouseAddressLine2 + "<br>"
            End If
            If objWhs.WarehouseCity <> "" Then
                strData += objWhs.WarehouseCity + "<br>"
            End If
            If objWhs.WarehouseState <> "" Then
                strData += objWhs.WarehouseState + "<br>"
            End If
            If objWhs.WarehouseCountry <> "" Then
                strData += objWhs.WarehouseCountry + "<br>"
            End If
            If objWhs.WarehousePostalCode <> "" Then
                strData += objWhs.WarehousePostalCode + "<br>"
            End If
            strData += "</table>"
            objWhs = Nothing
        End If
        Return strData
    End Function
    'Populate Currency
    Private Sub subPopulateCurrency()
        Dim objCur As New clsCurrencies
        objCur.PopulateCurrency(dlCurrencyCode)
        objCur = Nothing
    End Sub
    'Populate Warehouse
    Private Sub subPopulateWarehouse(ByVal strCompanyID As String)
        Dim objWhs As New clsWarehouses
        objWhs.PopulateWarehouse(dlShpWarehouse, strCompanyID)
        objWhs = Nothing
    End Sub
    'Populate Terms
    Private Sub subPopulateTerms()
        Dim objComp As New clsCompanyInfo
        objComp.CompanyID = objSO.ordCompanyID
        objComp.getCompanyInfo()
        'txtTerms.Text = objComp.CompanyShpToTerms
        objComp = Nothing
    End Sub
    'On Row Data Bound
    Protected Sub grdAddProcLst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAddProcLst.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & SOProcessItemConfirmDelete & " ')")
            e.Row.Cells(3).Text = String.Format("{0:F}", CDbl(e.Row.Cells(3).Text))
            e.Row.Cells(4).Text = String.Format("{0:F}", CDbl(e.Row.Cells(4).Text))
            e.Row.Cells(5).Text = String.Format("{0:F}", CDbl(e.Row.Cells(5).Text))
            e.Row.Cells(8).Text = String.Format("{0:F}", CDbl(e.Row.Cells(8).Text))
        End If
    End Sub
    ' On Row Deleting
    Protected Sub grdAddProcLst_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdAddProcLst.RowDeleting
        Dim strItemID As String = grdAddProcLst.DataKeys(e.RowIndex).Value.ToString()
        objSOIP.OrderItemProcID = strItemID
        sqldsAddProcLst.DeleteCommand = objSOIP.funDeleteSOItemProcess()
        lblMsg.Text = msgSOProcessDeletedSuccessfully '"Process Deleted Successfully"
        subFillGrid()
    End Sub
    ' Sql Data Source Event Track
    Protected Sub sqldsAddProcLst_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsAddProcLst.Selected
        subCalcAmount()
    End Sub
    ' Calcualte Amount
    Private Sub subCalcAmount()
        objSOIP.OrdID = strSOID
        objSOItems.OrdID = strSOID
        'lblAmount.Text = dlCurrencyCode.SelectedValue & " " & (CDbl(objSOItems.getItemCostForOrderNo) + CDbl(objSOIP.getProcessCostForOrderNo()))
        If grdAddProcLst.Rows.Count > 0 Then
            lblProcessList.Visible = True
        Else
            lblProcessList.Visible = False
        End If
    End Sub
    ' Calculate Tax
    Private Sub subCalcTax()
        'lblSubTotal.Text = "0"
        Dim drSOItems As Data.Odbc.OdbcDataReader
        Dim objPrn As New clsPrint
        drSOItems = objSOItems.GetDataReader(objSOItems.funFillGrid(strSOID))
        Dim dblSubTotal As Double = 0
        Dim dblTax As Double = 0
        Dim dblProcessTotal As Double = 0
        Dim dblTotal As Double = 0
        Dim strTaxArray(,) As String
        Dim dblDiscountApplied As Double = 0
        Dim dblAmount As Double = 0
        While drSOItems.Read
            Dim strTax As ArrayList
            Dim strTaxItem As String
            strTax = objPrn.funCalcTax(drSOItems("ordProductID"), drSOItems("ordShpWhsCode"), drSOItems("amount"), "", drSOItems("ordCustID"), drSOItems("ordCustType"), drSOItems("ordProductTaxGrp").ToString)
            Dim intI As Integer = 0
            While strTax.Count - 1 >= intI
                strTaxItem = strTax.Item(intI).ToString
                strTaxArray = objPrn.funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                intI += 1
            End While

            'If drSOItems("amount").ToString <> "0" Then
            '    dblDiscountApplied = CDbl(drSOItems("amount")) * (CDbl(drSOItems("ordProductDiscount")) / 100)
            '    dblAmount = CDbl(drSOItems("amount")) - dblDiscountApplied
            '    dblSubTotal = CDbl(dblSubTotal) + dblAmount
            'Else
            '    dblSubTotal = CDbl(dblSubTotal) + CDbl(drSOItems("amount"))
            'End If

            dblSubTotal = CDbl(dblSubTotal) + CDbl(drSOItems("amount"))
            'lblSubTotal.Text = String.Format("{0:F}", dblSubTotal) 'dblSubTotal.ToString
        End While
        Dim i As Integer = 0
        Dim j As Integer = 0
        If Not strTaxArray Is Nothing Then
            j = strTaxArray.Length / 2
        End If
        TaxString = ""
        While i < j
            TaxString &= "<tr><td align=left><b>" & strTaxArray(0, i) & ":</b></td><td align=right><b>" & dlCurrencyCode.SelectedValue & " " & String.Format("{0:F}", strTaxArray(1, i)) & "</b></td></tr>"
            dblTax += strTaxArray(1, i)
            i += 1
        End While
        objSOItems.OrdID = strSOID
        dblProcessTotal = CDbl(objSOIP.getProcessCostForOrderNo())
        dblTotal = dblTotal + dblSubTotal + dblProcessTotal + dblTax
        ' lblAmount.Text = String.Format("{0:F}", dblTotal)
        'lblSubTotal.Text = dlCurrencyCode.SelectedValue & " " & lblSubTotal.Text
        'lblAmount.Text = dlCurrencyCode.SelectedValue & " " & lblAmount.Text
        drSOItems.Close()
        objSOItems.CloseDatabaseConnection()
        objPrn = Nothing
    End Sub
    'Invoice Process 
    Private Function funInvoiceProcess() As Boolean
        Dim bFlag As Boolean = False
        If Request.QueryString("SOID") <> "" Then
            objSO.OrdID = Request.QueryString("SOID")
            objSO.getOrdersInfo()
            Dim objInv As New clsInvoices
            subSetInvoiceData(objInv, objSO)
            objInv.InvForOrderNo = Request.QueryString("SOID")
            objInv.InvCompanyID = objSO.ordCompanyID
            'bFlag = objInv.funUpdateInvoiceComment
            If objInv.funCheckInvoiceForOrderNo = False Then
                If objInv.funCheckInvoiceExits() = False Then
                    If objInv.insertInvoices() = True Then
                        Dim strInvID As String
                        strInvID = objInv.funGetInvoiceID()
                        objInv.funUpdateInvRefNo(objInv.InvRefType, objInv.InvCompanyID, strInvID)
                        Dim objInvItem As New clsInvoiceItems
                        'Populate Invoice Item
                        bFlag = objInvItem.funPopulateInvoiceItem(Request.QueryString("SOID"), strInvID)
                        objInvItem = Nothing
                        'Populate Invoice Item Process
                        Dim objINIP As New clsInvItemProcess
                        bFlag = objINIP.funPopulateInvoiceItemProcess(Request.QueryString("SOID"), strInvID)
                    End If
                Else
                    If objInv.updateInvoices("Yes") = True Then
                        Dim strInvID As String
                        ' strInvID = objInv.funGetInvoiceID()
                        '  objInv.funUpdateInvRefNo(objInv.InvRefType, objInv.InvCompanyID, strInvID)
                        ' Dim objInvItem As New clsInvoiceItems
                        'Populate Invoice Item
                        'bFlag = objInvItem.funPopulateInvoiceItem(Request.QueryString("SOID"), strInvID)
                        'objInvItem = Nothing
                        'Populate Invoice Item Process
                        'Dim objINIP As New clsInvItemProcess
                        'bFlag = objINIP.funPopulateInvoiceItemProcess(Request.QueryString("SOID"), strInvID)
                    End If
                End If
            Else
                bFlag = True ' Invoice already generated
            End If
        End If
        Return bFlag
    End Function
    'Populate Invoice Object
    Private Sub subSetInvoiceData(ByRef objIN As clsInvoices, ByRef objS As clsOrders)
        objIN.InvCustType = objS.OrdCustType
        objIN.InvCustID = objS.OrdCustID
        objIN.InvCreatedOn = Now.ToString("yyyy-MM-dd hh:mm:ss")
        objIN.InvStatus = "S"
        objIN.InvComment = objS.OrdComment
        objIN.InvLastUpdateBy = Session("UserID").ToString
        objIN.InvLastUpdatedOn = Now.ToString("yyyy-MM-dd hh:mm:ss")
        objIN.InvShpCost = objS.OrdShpCost
        objIN.InvCurrencyCode = objS.OrdCurrencyCode
        objIN.InvCurrencyExRate = objS.OrdCurrencyExRate
        objIN.InvCustPO = objS.OrdCustPO
        objIN.InvShpWhsCode = objS.OrdShpWhsCode
        objIN.InvRefNo = 0
        objIN.InvRefType = "IV"
        objIN.InvCommission = objS.ordercommission
    End Sub
    Protected Sub cmdBack_ServerClick1(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.ServerClick
        Response.Redirect("~/shipping/jobs.aspx")
    End Sub
End Class

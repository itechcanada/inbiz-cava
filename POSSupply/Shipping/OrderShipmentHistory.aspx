﻿<%@ Page Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="OrderShipmentHistory.aspx.cs" Inherits="Shipping_OrderShipmentHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div style="padding: 5px;">
        <div id="grid_wrapper">
            <trirand:JQGrid runat="server" ID="grdOrderSH" Height="100%" PagerSettings-PageSize="50"
                AutoWidth="true" OnDataRequesting="grdOrderSH_DataRequesting" OnCellBinding="grdOrderSH_CellBinding">
                <Columns>
                    <trirand:JQGridColumn DataField="OrderID" HeaderText="<%$ Resources:Resource, cmdCssOrderNo %>"
                        PrimaryKey="true" />
                    <trirand:JQGridColumn DataField="CreatedBy" HeaderText="<%$ Resources:Resource, lblCreatedBy %>" />
                    <trirand:JQGridColumn DataField="CreatedOn" HeaderText="<%$ Resources:Resource, grdCreatedOn %>" />
                    <trirand:JQGridColumn DataField="OrderStatus" HeaderText="<%$ Resources:Resource, lblOrderStatus %>" />
                    <trirand:JQGridColumn DataField="InvoiceNo" HeaderText="<%$ Resources:Resource, liARInvoiceNo %>" />
                    <trirand:JQGridColumn DataField="OrderID" HeaderText="<%$ Resources:Resource, POSEdit %>" />
                </Columns>
                <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                    ShowDeleteButton="false" ShowSearchButton="false" />
                <SortSettings InitialSortColumn="" />
                <AppearanceSettings AlternateRowBackground="True" />
                <ClientSideEvents LoadComplete="gridLoadComplete" />
            </trirand:JQGrid>
        </div>
    </div>
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdOrderSH.ClientID %>";

        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }
        
        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        function reloadGrid(event, ui) {
            resetForm();
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function gridLoadComplete(data) {
            $(window).trigger("resize");
        }


        function OrderEdit(ordID) {
            parent.redirectOrderEdit(ordID);
            jQuery.FrameDialog.closeDialog();
        }
        function InvoiceEdit(invID) {
            parent.redirectInvoiceEdit(invID);
            jQuery.FrameDialog.closeDialog();
        }
        
           
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
</asp:Content>

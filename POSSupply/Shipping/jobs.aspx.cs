﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Shipping_jobs : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.JOB_PLANNER) && !CurrentUser.IsInRole(RoleID.JOB_PLANNING_MANAGER))
        {
             Response.Redirect("~/Errors/AccessDenied.aspx");
        }
        UIHelper.SetJqGridDefaults(grdJobs);
        if (!IsPagePostBack(grdJobs))
        {
            btnAssignJob.Visible = CurrentUser.IsInRole(RoleID.JOB_PLANNING_MANAGER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR);           
        }
    }

    protected void grdJobs_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string custName = Request.QueryString[txtCustomerName.ClientID];
        string custPhone = Request.QueryString[txtCustPhoneNo.ClientID];
        int orderID = 0;
        int.TryParse(Request.QueryString[txtOrderNo.ClientID], out orderID);
        string assignTo = Request.QueryString[txtAssign.ClientID];
        DateTime fDate = BusinessUtility.GetDateTime(Request.QueryString[txtFromDate.ClientID], DateFormat.MMddyyyy);
        DateTime tDate = BusinessUtility.GetDateTime(Request.QueryString[txtToDate.ClientID], DateFormat.MMddyyyy);
        sdsJobs.SelectCommand = GridSqlHelper.GetAllJobPlanning(sdsJobs.SelectParameters, custName, custPhone, orderID, assignTo, fDate, tDate);
    }

    protected void grdJobs_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 6)
        {
            e.CellHtml = new JobPlanning().GetJobAssignedUsers(BusinessUtility.GetInt(e.RowKey));
        }
        else if (e.ColumnIndex == 7)
        {
            string strurl = ResolveUrl("~/Sales/ViewOrderDetails.aspx") + "?SOID=" + e.RowValues[0];
            e.CellHtml = string.Format("<a href=\"{0}\">{1}</a>", strurl, Resources.Resource.lblEdit);
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="validateSKU.aspx.cs" Inherits="Shipping_validateSKU" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <style type="text/css">
        select
        {
            width: 179px;
        }
        input, textarea, select
        {
            border: 1px solid #797D7F;
            margin-bottom: 0;
        }
        
        .ButtonCss
        {
            background: url("../lib/css/inbiz/images/ui-bg_highlight-soft_20_526785_1x100.png") repeat-x scroll 50% 50% #DBDDDD;
            border: 1px solid #DBDDDD;
            color: #FFFEFD;
            font-weight: bold;
        }
    </style>
    <asp:Panel ID="pnlContent" runat="server">
        <div>
            <table border="0" width="100%" >
                <tr style="height: auto; padding: 0;">
                    <td >
                        <asp:Label ID="lblSKU" Text="<%$Resources:Resource, lblSrchSKU %>" runat="server" />
                    </td>
                    <td style='width: 175px;' >
                        <asp:TextBox ID="txtSKU" runat="server" onkeydown='CollectSKU(event);' />
                        <asp:RequiredFieldValidator ID="rqtxtSKU" ControlToValidate="txtSKU" SetFocusOnError="True"
                            runat="server" Text="<%$Resources:Resource, lblRequiredSKU%>" ValidationGroup="validateSKU" Display="None"></asp:RequiredFieldValidator>
                    </td>
                    <td valign='top' style='width: 100px;'><asp:Button ID="btnValidateSKU" Text="<%$Resources:Resource, lblValidateSKU%>" runat="server" 
                            ValidationGroup="validateSKULIST" OnClick="btnValidateSKU_Click" />
                    <%--<input type="button" value="<%=Resources.Resource.lbGo%>" onclick='SaveSKUTemp();' style="display:none;"/>--%>
                        <%--<asp:Button ID="btnGO" Text="<%$Resources:Resource, lbGo%>" runat="server" ValidationGroup="validateSKU"
                             />--%><%--OnClick="btnGO_Click"--%>
                    </td>
                    <td>
                        <asp:Label ID="lblAuto" Text="<%$Resources:Resource, lblAuto %>" runat="server" />
                    </td>
                    <td>
                         <asp:CheckBox ID="chkAutoValidate" runat="server" Text=""  />
                    </td>
                    <td valign='top' align="right">
                        <asp:Button ID="btnValidateOrder" Text="<%$Resources:Resource, lblValidateOrder%>"
                            runat="server" OnClick="btnValidateOrder_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan='5'>
                        <asp:HiddenField id="hdnSkuList" runat="server" Value=''/>
                    </td>
                </tr>
            </table>
            
        </div>
        <div id="spnSKUlst" style="width:100%;height:auto;display:block;word-wrap: break-word;text-align:justify;"></div>
    </asp:Panel>
    <div id="grid_wrapper" style="width: 100%;">
        <trirand:JQGrid runat="server" ID="grdAddress" Height="180px" AutoWidth="True" OnDataRequesting="grdAddress_DataRequesting"
            OnCellBinding="grdAddress_CellBinding">
            <Columns>
                <trirand:JQGridColumn DataField="ProductID" HeaderText="" PrimaryKey="True" Visible="false" />
                <trirand:JQGridColumn DataField="SKU" HeaderText="<%$ Resources:Resource, lblSrchSKU %>"
                    Editable="false" TextAlign="Left" />
                <trirand:JQGridColumn DataField="Collection" HeaderText="<%$ Resources:Resource, lblCollection %>"
                    Editable="false" TextAlign="Left" />
                <trirand:JQGridColumn DataField="Style" HeaderText="<%$ Resources:Resource, lblStyle %>"
                    Editable="false" TextAlign="Left" />
                <trirand:JQGridColumn DataField="Color" HeaderText="<%$ Resources:Resource, lblColor %>"
                    Editable="false" TextAlign="Left" />
                <trirand:JQGridColumn DataField="Size" HeaderText="<%$ Resources:Resource, lblSize %>"
                    Editable="false" TextAlign="Left" />
                <trirand:JQGridColumn DataField="ShpQty" HeaderText="<%$ Resources:Resource, lblShipQty %>"
                    Editable="false" Width="100" TextAlign="Right" />
                <trirand:JQGridColumn DataField="OrdQty" HeaderText="<%$ Resources:Resource, lblOrderQty %>"
                    Editable="false" Width="100" TextAlign="Right" />
                <trirand:JQGridColumn DataField="Status" HeaderText="<%$ Resources:Resource, lblStatus %>"
                    Editable="false" Width="100" TextAlign="Center" />
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="gridLoadComplete" />
        </trirand:JQGrid>
    </div>
    <h3>
        <asp:Literal ID="ltSectionTitle" Text="" runat="server" />
    </h3>
    <asp:Panel ID="pnlEdit" runat="server" DefaultButton="btnSave">
        <div class="div_command">
            <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" ValidationGroup="submit"
                OnClick="btnSave_Click" />
            <asp:Button ID="btnReset" Text="<%$Resources:Resource, btnCancel%>" runat="server"
                CausesValidation="false" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
        </div>
    </asp:Panel>
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdAddress.ClientID %>";

        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        function reloadGrid(event, ui) {
            resetForm();
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function gridLoadComplete(data) {
            $(window).trigger("resize");
            ApplyGridButtonSetting();
        }

        function resetForm() {

        }


        function rowSelect(id) {
        }

        function ApplyGridButtonSetting() {
            $('.updateQty').each(function () {
                $(this).addClass("ButtonCss ui-button");
            });
        }

        var vSKULst = '';
        function CollectSKU(e) {
            if (e.keyCode == 13) {
                $chkTag = $("#<%=chkAutoValidate.ClientID%>");
                if ($chkTag.is(':checked')) {
                    //alert("Mukesh");
                    $('#<%=hdnSkuList.ClientID%>').val('');
                    vSKULst = $('#<%=txtSKU.ClientID%>').val();
                    $('#<%=hdnSkuList.ClientID%>').val(vSKULst);
                    $('#<%=btnValidateSKU.ClientID%>').click();
                }
                else {
                    //alert("Mehta");
                    SaveSKUTemp();
                }
            }
        }

        function SaveSKUTemp() {
            if ($('#<%=txtSKU.ClientID%>').val() != '') {
                if (vSKULst == '') {
                    vSKULst = $('#<%=txtSKU.ClientID%>').val();
                }
                else {
                    vSKULst += ", " + $('#<%=txtSKU.ClientID%>').val();
                }

                $('#<%=hdnSkuList.ClientID%>').val(vSKULst);
                $('#spnSKUlst').html(vSKULst);
                $('#<%=txtSKU.ClientID%>').val('');
                $('#<%=txtSKU.ClientID%>').focus();
            }
        }

        $("#<%=chkAutoValidate.ClientID%>").click(function () {
            $('#<%=txtSKU.ClientID%>').focus();
        });

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="PrintFeedExLabel.aspx.cs" Inherits="Shipping_PrintFeedExLabel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div style="float: right;">
        <table id="tblDownLoad" runat="server" visible="false">
            <tr>
                <td>
                    <asp:Button ID="btnDonwLoadDtlFile" runat="server" OnClick="btnDonwLoadDtlFile_OnClick"
                        Text="<%$Resources:Resource,lblDownLoadDetailLabel %>" />
                    <asp:HiddenField ID="hdnPkgDtlPdfFilePath" runat="server" />
                </td>
                <td>
                    <asp:Button ID="btnDonwLoadCmpltDtlFile" runat="server" OnClick="btnDonwLoadCmpltDtlFile_OnClick"
                        Text="<%$Resources:Resource,lblDownLoadCompleteDetailLabel %>" />
                    <asp:HiddenField ID="hdnPkgCmpltDtlPdfFilePath" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    <asp:HiddenField ID="hdnErrDesc" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" runat="Server">
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Shipping_Default : BasePage
{    
    private Orders _objOrder = new Orders();
    // On Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security
        if (!CurrentUser.IsInRole(RoleID.SHIPPING_MANAGER) && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR)) 
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        if (!Page.IsPostBack)
        {
            SysWarehouses wh = new SysWarehouses();
            wh.FillWharehouse(null, dlWarehouse, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));
            if (dlWarehouse.Items.Count > 1)
            {
                dlWarehouse.Items.Insert(1, new ListItem(Resources.Resource.liAllWarehouse, "ALL"));
            }
            else
            {
                dlWarehouse.Items.Add(new ListItem(Resources.Resource.liAllWarehouse, "ALL"));
            }
            dlWarehouse.SelectedValue = "ALL";
            //txtSearch.Attributes.Add("onKeyPress", "doClick('" + btncmdUPC.ClientID + "',event)");
            txtSearch.Focus();
        }
    }

    // Search Sub Routine
    private void Search()
    {
        if (string.IsNullOrEmpty(txtSearch.Text))
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.PlzEntSearchCriteria);
            txtSearch.Focus();
        }
    }


    protected void btnOrderNO_Click(object sender, System.EventArgs e)
    {
        if (string.IsNullOrEmpty(txtSearch.Text))
        {
            Search();
            return;
        }        
        _objOrder.OrdShpWhsCode = dlWarehouse.SelectedValue;
        int oid = _objOrder.GetOrderIDToShip(null, BusinessUtility.GetInt(txtSearch.Text));        
        //if (_objOrder.CheckShippingForOrder("ON", txtSearch.Text, dlWarehouse.SelectedValue))
        //{
        //    Response.Redirect("ViewShipping.aspx?opt=ON&srh=" + txtSearch.Text + "&whs=" + dlWarehouse.SelectedValue);
        //}
        if (oid > 0)
        {
            Response.Redirect(string.Format("Shipment.aspx?oid={0}", oid));
        }
        else
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);
    }
    //'Search by Receipt
    protected void btnCustomerName_Click(object sender, System.EventArgs e)
    {
        if (string.IsNullOrEmpty(txtSearch.Text))
        {
            Search();
            return;
        }
        if (_objOrder.CheckShippingForOrder("CN", txtSearch.Text, dlWarehouse.SelectedValue))
            Response.Redirect("ViewShipping.aspx?opt=CN&srh=" + txtSearch.Text + "&whs=" + dlWarehouse.SelectedValue);
        else
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);
    }

    protected void btnSalesAgentName_Click(object sender, System.EventArgs e)
    {
        if (string.IsNullOrEmpty(txtSearch.Text))
        {
            Search();
            return;
        }

        if (_objOrder.CheckShippingForOrder("AN", txtSearch.Text, dlWarehouse.SelectedValue))
            Response.Redirect("ViewShipping.aspx?opt=AN&srh=" + txtSearch.Text + "&whs=" + dlWarehouse.SelectedValue);
        else
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);
    }

    protected void btnTrackNo_Click(object sender, System.EventArgs e)
    {
        if (string.IsNullOrEmpty(txtSearch.Text))
        {
            Search();
            return;
        }        
        if (_objOrder.CheckShippingForOrder("TN", txtSearch.Text, dlWarehouse.SelectedValue))
            Response.Redirect("ViewShipping.aspx?opt=TN&srh=" + txtSearch.Text + "&whs=" + dlWarehouse.SelectedValue);
        else
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);
    }

    //'Search by Today
    protected void btncmdToday_Click(object sender, System.EventArgs e)
    {        
        if (_objOrder.CheckShippingForOrder("T", txtSearch.Text, dlWarehouse.SelectedValue))
        {
            Response.Redirect("ViewShipping.aspx?opt=T&srh=" + Server.UrlEncode(txtSearch.Text) + "&whs=" + dlWarehouse.SelectedValue);
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);
        }
    }
    //'Search by This Week
    protected void btncmdThisWeek_Click(object sender, System.EventArgs e)
    {
        if (_objOrder.CheckShippingForOrder("TW", txtSearch.Text, dlWarehouse.SelectedValue))
        {
            Response.Redirect("ViewShipping.aspx?opt=TW&srh=" + Server.UrlEncode(txtSearch.Text) + "&whs=" + dlWarehouse.SelectedValue);
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);
        }
    }
    //'Search by Next Week
    protected void btncmdNextWeek_Click(object sender, System.EventArgs e)
    {
        _objOrder.OrdShpWhsCode = dlWarehouse.SelectedValue;
        if (_objOrder.CheckShippingForOrder("NW", txtSearch.Text, dlWarehouse.SelectedValue))
        {
            Response.Redirect("ViewShipping.aspx?opt=NW&srh=" + Server.UrlEncode(txtSearch.Text) + "&whs=" + dlWarehouse.SelectedValue);
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);
        }
    }
    //'Search by All
    protected void btncmdAll_Click(object sender, System.EventArgs e)
    {
        if (_objOrder.CheckShippingForOrder("ALL", txtSearch.Text, dlWarehouse.SelectedValue))
        {
            Response.Redirect("ViewShipping.aspx?opt=ALL&srh=" + Server.UrlEncode(txtSearch.Text) + "&whs=" + dlWarehouse.SelectedValue);
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);
        }
    }
    protected void btncmdTomorrow_Click(object sender, EventArgs e)
    {
        _objOrder.OrdShpWhsCode = dlWarehouse.SelectedValue;
        if (_objOrder.CheckShippingForOrder("TM", txtSearch.Text, dlWarehouse.SelectedValue))
        {
            Response.Redirect("ViewShipping.aspx?opt=TM&srh=" + txtSearch.Text + "&whs=" + dlWarehouse.SelectedValue);
        }
        else
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);
    }    
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Shipping_JobsAssign : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.JOB_PLANNING_MANAGER))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        if (!IsPostBack)
        {
            lblOrderNo.Text = Request.QueryString["oid"];
            txtHour.Text = "12";
            txtMin.Text = "00";            
            DropDownHelper.FillJobPlanningUser(dlJobPlanningMgr, new ListItem(Resources.Resource.lblselect, ""));
        }
    }    

    private int[] OrderIDs
    {
        get {
            string strArr = Request.QueryString["oid"];
            if (!string.IsNullOrEmpty(strArr))
            {
                string[] arr = strArr.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                return Array.ConvertAll<string, int>(arr, a => BusinessUtility.GetInt(a));
            }
            return new List<int>().ToArray();
        }
    }
    protected void btnAssignTask_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                JobPlanning jp;
                Orders ord;
                foreach (var oid in this.OrderIDs)
                {
                    jp = new JobPlanning();
                    ord = new Orders();

                    ord.PopulateObject(dbHelp, oid);
                    jp.Active = true;
                    jp.CustomerID = ord.OrdCustID;
                    jp.CustomerType = ord.OrdCustType;
                    string dateString = string.Format("{0} {1:00}:{2:00} {3}", txtDueDateTime.Text, BusinessUtility.GetInt(txtHour.Text), BusinessUtility.GetInt(txtMin.Text), ddlTime.SelectedValue);
                    jp.JobDueDate = BusinessUtility.GetDateTime(dateString, "MM/dd/yyyy hh:mm tt");
                    jp.JobNote = txtNote.Text;
                    jp.OrderID = ord.OrdID;
                    jp.UserID = BusinessUtility.GetInt(dlJobPlanningMgr.SelectedValue);
                    jp.Save(dbHelp);

                    //Generate Alert
                    AlertHelper.SetTaskAssignedAlert(dbHelp, jp.JobDueDate, jp.UserID, CurrentUser.UserID, oid);
                }
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgJobPlanningAsgnSuccessfully);

                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                }
                else
                {
                    Globals.RegisterCloseDialogScript(Page);
                }       
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError + ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.Services.Protocols;
//using ShipWebServiceClient.ShipServiceWebReference;
using ShipWebServiceClient;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Shipping_PrintFeedExLabel : System.Web.UI.Page
{

    protected void btnDonwLoadDtlFile_OnClick(object sender, EventArgs e)
    {
        //DownloadFile("Inv_2013121013451.csv");
        DownloadFile(hdnPkgDtlPdfFilePath.Value);
    }

    protected void btnDonwLoadCmpltDtlFile_OnClick(object sender, EventArgs e)
    {
        DownloadFile(hdnPkgCmpltDtlPdfFilePath.Value);
    }

    protected void DownloadFile(string name)
    {
        try
        {
            if (name == "")
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblDownloadFileNotFound);
                return;
            }

            string _fileName;
            //string _path = Request.PhysicalApplicationPath + "Upload/FeedExLabel/" + name;
            string _path = name;
            System.IO.FileInfo _file = new System.IO.FileInfo(_path);
            if (_file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + _file.Name);
                Response.AddHeader("Content-Length", _file.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(_file.FullName);
                Response.End();

                /* Dodnload PDF
                StringBuilder sbSavePath = new StringBuilder();
                sbSavePath.Append(DateTime.Now.Day);
                sbSavePath.Append("-");
                sbSavePath.Append(DateTime.Now.Month);
                sbSavePath.Append("-");
                sbSavePath.Append(DateTime.Now.Year);

                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpResponse objResponce = context.Response;
                String test = HttpContext.Current.Request.QueryString["ReportName"];
                HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + test);
                objResponce.WriteFile(context.Server.MapPath(@"Reports\" + sbSavePath + @"\" + test));
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.End();*/
            }
            else
            {
                ClientScript.RegisterStartupScript(Type.GetType("System.String"), "messagebox", "<script type=\"text/javascript\">alert('File not Found');</script>");
            }
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblDownLoadFailure);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                DbHelper dbHelp = new DbHelper(true);
                Orders ord = new Orders();
                ord.PopulateObject(dbHelp, OrderID);

                SysWarehouses sWhs = new SysWarehouses();
                sWhs.PopulateObject(ord.OrdShpToWhsCode, dbHelp);

                Partners _cust = new Partners();
                Addresses _shipToAddress = new Addresses();
                _cust.PopulateObject(dbHelp, BusinessUtility.GetInt(ord.OrdCustID));
                _shipToAddress = _cust.GetShipToAddress(dbHelp, _cust.PartnerID, _cust.PartnerType);
                ShipWebServiceClient.FeedEx.OrderID = OrderID;
                ShipWebServiceClient.FeedEx.FeedExLabelPath = FeedExLabelPath;
                ShipWebServiceClient.FeedEx.FeedExKey = FeedExKey;
                ShipWebServiceClient.FeedEx.FeedExPwd = FeedExPwd;
                ShipWebServiceClient.FeedEx.FeedExAccountNumber = FeedExAccountNumber;
                ShipWebServiceClient.FeedEx.FeedExMeterNumber = FeedExMeterNumber;
                ShipWebServiceClient.FeedEx.WhsName = BusinessUtility.GetString(sWhs.WarehouseDescription);
                if (BusinessUtility.GetString(sWhs.WarehousePhone) != "")
                {
                    ShipWebServiceClient.FeedEx.WhsPhone = BusinessUtility.GetString(sWhs.WarehousePhone);
                }
                else
                {
                    ShipWebServiceClient.FeedEx.WhsPhone = "....";
                }

                if (BusinessUtility.GetString(sWhs.WarehouseAddressLine1) != "")
                {
                    ShipWebServiceClient.FeedEx.WhsAdd1 = BusinessUtility.GetString(sWhs.WarehouseAddressLine1);
                }
                else
                {
                    ShipWebServiceClient.FeedEx.WhsAdd1 = "....  ";
                }

                if (BusinessUtility.GetString(sWhs.WarehouseAddressLine2) != "")
                {
                    ShipWebServiceClient.FeedEx.WhsAdd2 = BusinessUtility.GetString(sWhs.WarehouseAddressLine2);
                }
                else
                {
                    ShipWebServiceClient.FeedEx.WhsAdd2 = "....  ";
                }


                if (BusinessUtility.GetString(sWhs.WarehouseCity) != "")
                {
                    ShipWebServiceClient.FeedEx.WhsCity = BusinessUtility.GetString(sWhs.WarehouseCity);
                }
                else
                {
                    ShipWebServiceClient.FeedEx.WhsCity = "....  ";
                }

                if (BusinessUtility.GetString(sWhs.WarehouseState) != "")
                {
                    ShipWebServiceClient.FeedEx.WhsState = BusinessUtility.GetString(sWhs.WarehouseState);
                }
                else
                {
                    ShipWebServiceClient.FeedEx.WhsState = "....  ";
                }

                if (BusinessUtility.GetString(sWhs.WarehousePostalCode) != "")
                {
                    ShipWebServiceClient.FeedEx.WhsPostalCode = BusinessUtility.GetString(sWhs.WarehousePostalCode);
                }
                else
                {
                    ShipWebServiceClient.FeedEx.WhsPostalCode = "....  ";
                }

                if (BusinessUtility.GetString(sWhs.WarehouseCountry) != "")
                {
                    ShipWebServiceClient.FeedEx.WhsCountryCode = BusinessUtility.GetString(sWhs.WarehouseCountry);
                }
                else
                {
                    ShipWebServiceClient.FeedEx.WhsCountryCode = "....  ";
                }

                //ShipWebServiceClient.FeedEx.WhsPhone = "514-934-3737";
                ShipWebServiceClient.FeedEx.WhsAdd1 = BusinessUtility.GetString(sWhs.WarehouseAddressLine1);
                ShipWebServiceClient.FeedEx.WhsAdd2 = BusinessUtility.GetString(sWhs.WarehouseAddressLine2);
                ShipWebServiceClient.FeedEx.WhsCity = BusinessUtility.GetString(sWhs.WarehouseCity);
                ShipWebServiceClient.FeedEx.WhsState = BusinessUtility.GetString(sWhs.WarehouseState);
                ShipWebServiceClient.FeedEx.WhsPostalCode = BusinessUtility.GetString(sWhs.WarehousePostalCode);
                ShipWebServiceClient.FeedEx.WhsCountryCode = BusinessUtility.GetString(sWhs.WarehouseCountry);
                //ShipWebServiceClient.FeedEx.WhsAdd1 = BusinessUtility.GetString(sWhs.WarehouseAddressLine1);
                //ShipWebServiceClient.FeedEx.WhsAdd2 = BusinessUtility.GetString(sWhs.WarehouseAddressLine2);
                //ShipWebServiceClient.FeedEx.WhsCity = "Harrison";
                //ShipWebServiceClient.FeedEx.WhsState = "AR";
                //ShipWebServiceClient.FeedEx.WhsPostalCode = "72601";
                //ShipWebServiceClient.FeedEx.WhsCountryCode = "US";

                ShipWebServiceClient.FeedEx.ShpPersonName = BusinessUtility.GetString(_cust.PartnerLongName);
                ShipWebServiceClient.FeedEx.ShpPersonPhone = BusinessUtility.GetString(_cust.PartnerPhone);
                ShipWebServiceClient.FeedEx.ShpPersonAdd1 = BusinessUtility.GetString(_shipToAddress.AddressLine1);
                ShipWebServiceClient.FeedEx.ShpPersonAdd2 = BusinessUtility.GetString(_shipToAddress.AddressLine2);
                ShipWebServiceClient.FeedEx.ShpPersonCity = BusinessUtility.GetString(_shipToAddress.AddressCity);
                ShipWebServiceClient.FeedEx.ShpPersonState = BusinessUtility.GetString(_shipToAddress.AddressState);
                ShipWebServiceClient.FeedEx.ShpPersonPostalCode = BusinessUtility.GetString(_shipToAddress.AddressPostalCode);
                ShipWebServiceClient.FeedEx.ShpPersonCountryCode = BusinessUtility.GetString(_shipToAddress.AddressCountry);

                //ShipWebServiceClient.FeedEx.ShpPersonAdd1 = BusinessUtility.GetString(_shipToAddress.AddressLine1);
                //ShipWebServiceClient.FeedEx.ShpPersonAdd2 = BusinessUtility.GetString(_shipToAddress.AddressLine2);
                //ShipWebServiceClient.FeedEx.ShpPersonCity = "Austin";
                //ShipWebServiceClient.FeedEx.ShpPersonState = "TX";
                //ShipWebServiceClient.FeedEx.ShpPersonPostalCode = "73301";
                //ShipWebServiceClient.FeedEx.ShpPersonCountryCode = "US";

                ShipWebServiceClient.FeedEx.PayorAccountNo = PayorAccountNo;
                ShipWebServiceClient.FeedEx.PayorCountryCode = BusinessUtility.GetString(sWhs.WarehouseCountry);
                ShipWebServiceClient.FeedEx.PkgWeight = PkgWeight;
                ShipWebServiceClient.FeedEx.PkgLength = PkgLength;
                ShipWebServiceClient.FeedEx.PkgWidth = PkgWidth;
                ShipWebServiceClient.FeedEx.PkgHeight = PkgHeight;
                ShipWebServiceClient.FeedEx.CollectionAmount = CollectionAmount;
                ShipWebServiceClient.FeedEx.CollectionCurrencyCode = CollectionCurrencyCode;
                ShipWebServiceClient.FeedEx.ShowPDFFile = ShowPdf;
                ShipWebServiceClient.FeedEx.FeedexLabel();

                if (ShipWebServiceClient.FeedEx.Status == "0")
                {
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.lblFeedExLabelCreated);
                    hdnPkgDtlPdfFilePath.Value = ShipWebServiceClient.FeedEx.PkgDtlPdfPath;
                    hdnPkgCmpltDtlPdfFilePath.Value = ShipWebServiceClient.FeedEx.PkgCmpltDtlPdfPath;
                    tblDownLoad.Visible = true;
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, BusinessUtility.GetString(ShipWebServiceClient.FeedEx.ErrDesc));
                    hdnErrDesc.Value = BusinessUtility.GetString(ShipWebServiceClient.FeedEx.ErrDesc);
                }
            }
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblFeedExLabelFailure);
            hdnErrDesc.Value = BusinessUtility.GetString(ex.Message);
        }

    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    #region ReadValueFromWebConfig
    private static string FeedExLabelPath
    {
        get
        {
            return HttpContext.Current.Request.PhysicalApplicationPath + BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["FeedExLablePath"]);
        }
    }

    private static string FeedExKey
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["FeedExKey"]);
        }
    }

    private static string FeedExPwd
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["FeedExPwd"]);
        }
    }

    private static string FeedExAccountNumber
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["FeedExAccountNumber"]);
        }
    }

    private static string FeedExMeterNumber
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["FeedExMeterNumber"]);
        }
    }

    private static string PayorAccountNo
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["PayorAccountNo"]);
        }
    }

    private static decimal PkgWeight
    {
        get
        {
            return BusinessUtility.GetDecimal(System.Configuration.ConfigurationManager.AppSettings["PkgWeight"]);
        }
    }

    private static string PkgLength
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["PkgLength"]);
        }
    }

    private static string PkgWidth
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["PkgWidth"]);
        }
    }

    private static string PkgHeight
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["PkgHeight"]);
        }
    }

    private static decimal CollectionAmount
    {
        get
        {
            return BusinessUtility.GetDecimal(System.Configuration.ConfigurationManager.AppSettings["CollectionAmount"]);
        }
    }

    private static string CollectionCurrencyCode
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["CollectionCurrencyCode"]);
        }
    }

    private static bool ShowPdf
    {
        get
        {
            if (BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["ShowPDF"]) == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    #endregion

    private static int OrderID
    {
        get
        {
            int id = 0;
            int.TryParse(HttpContext.Current.Request.QueryString["oid"], out id);
            return id;
        }
    }

}
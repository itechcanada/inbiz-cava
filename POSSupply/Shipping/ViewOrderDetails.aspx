<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="ViewOrderDetails.aspx.vb" Inherits="Shipping_ViewOrderDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divModuleAlert').corner();
        $('#divAlertContent').corner();
        $('#divMainContainerTitle').corner();
    </script>
    <div id="divModuleAlert" class="divSectionTitle">
        <h2>
            <%=Resources.Resource.lblSearchOptions%>
        </h2>
    </div>
    <div id="divAlertContent" class="divSectionContent">
    </div>

</asp:Content>

<asp:Content ID="cntViewOrderDetails" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal>
                    </h2>
                </td>
                <td style="text-align: right;">
                    
                </td>                
                <td align="right" style="width: 150px;">
                    <div class="buttonwrapper">
                        <a id="imgPrintPackList" runat="server" causesvalidation="false" class="ovalbutton"
                            href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                <%=Resources.Resource.btnPrntPackList%></span></a></div>
                </td>
                <td align="right" style="width: 150px;">
                    <div class="buttonwrapper">
                        <a id="imgPrintAsmlyList" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                            style="min-width: 120px; text-align: center;">
                            <%=Resources.Resource.btnPrntAsmlyList%></span></a></div>
                </td>
            </tr>
        </table>               
    </div>

    <div class="divMainContent">
        <%-- onkeypress="return disableEnterKey(event)"--%>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" colspan="2">
                    <asp:UpdatePanel ID="upnlMsg" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table cellpadding="2" cellspacing="2" width="100%">
                        <tr>
                            <td align="left" width="18%">
                                <asp:Label ID="lblSONumber" Text="<%$ Resources:Resource, lblSOOrderNo %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left" width="29%">
                                <asp:Label ID="lblSOID" CssClass="lblBold" runat="server" />
                            </td>
                            <td width="6%">
                            </td>
                            <td align="left" width="18%">
                                <asp:Label ID="lblSODate1" Text="<%$ Resources:Resource, lblSOOrderDate %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left" width="29%">
                                <asp:Label ID="lblSODate" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblCustomerName1" Text="<%$ Resources:Resource, lblSOCustomerName %>"
                                    CssClass="lblBold" runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblCustomerName" CssClass="lblBold" runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:Label ID="Label3" Text="<%$ Resources:Resource, POFax %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblFax" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr id="trAddress" runat="server" visible="false">
                            <td align="left" valign="top">
                                <asp:Label ID="Label1" Text="<%$ Resources:Resource, lblSOReceivingAddress %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblAddress" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <% If Request.QueryString("JobDtl") = "1" Then%>
            <tr height="5">
                <td colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <asp:GridView ID="grdSelRepDetail" runat="server" AllowSorting="True" DataSourceID="sqldsdetail"
                        AllowPaging="True" PageSize="10" PagerSettings-Mode="Numeric" CellPadding="0"
                        GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
                        CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="JobPlanningID"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="userName" HeaderText="<%$ Resources:Resource, grdSalesRepName %>"
                                ReadOnly="True" SortExpression="userName">
                                <ItemStyle Width="90px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="JobDueDate" HeaderText="<%$ Resources:Resource, grdDueDateTime %>"
                                ReadOnly="True" SortExpression="JobDueDate">
                                <ItemStyle Width="75px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="jobNote" HeaderText="<%$ Resources:Resource, grdManagerNotes %>"
                                ReadOnly="True" SortExpression="jobNote">
                                <ItemStyle Width="150px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                        <FooterStyle CssClass="grid_footer" />
                        <RowStyle CssClass="grid_rowstyle" />
                        <PagerStyle CssClass="grid_footer" />
                        <HeaderStyle CssClass="grid_header" Height="26px" />
                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                        <PagerSettings PageButtonCount="20" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqldsdetail" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                </td>
            </tr>
            <% End If%>
            <tr>
                <td height="5" colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <asp:GridView ID="grdOrdItems" runat="server" AllowSorting="false" DataSourceID="sqldsOrdItems"
                        AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0"
                        GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
                        CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="orderItemID"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="ordProductID" HeaderText="<%$ Resources:Resource, POProductID %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff" Visible="false">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="prdIntID" HeaderText="<%$ Resources:Resource, POInternalID %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="prdExtID" HeaderText="<%$ Resources:Resource, POExternalID %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="prdUPCCode" HeaderText="<%$ Resources:Resource, grdPOUPCCode %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="prdName" HeaderText="<%$ Resources:Resource, POProduct %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="200px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdPOShippingAddress %>" HeaderStyle-ForeColor="#ffffff"
                                HeaderStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <div>
                                        <%#funAddressListing(Eval("ordShpWhsCode"))%>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="120px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="sysTaxCodeDescText" NullDisplayText="--" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="<%$ Resources:Resource, POTaxGrp %>" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordProductDiscount" NullDisplayText="0" Visible="false"
                                HeaderStyle-HorizontalAlign="Right" HeaderText="<%$ Resources:Resource, PODiscount %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordProductQty" HeaderText="<%$ Resources:Resource, POQuantity %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordProductUnitPrice" Visible="false" HeaderText="<%$ Resources:Resource, POUnitPrice %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordCurrencyCode" Visible="false" HeaderText="" ReadOnly="True"
                                HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="amount" Visible="false" HeaderText="<%$ Resources:Resource, POAmount %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordShpWhsCode" Visible="false" HeaderText="<%$ Resources:Resource, POWarehouseCode %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, POConfirm %>" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-ForeColor="#ffffff" Visible="false">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" Visible="false"
                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                        CommandArgument='<%# Eval("orderItemID") %>' CommandName="Delete" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="grid_footer" />
                        <RowStyle CssClass="grid_rowstyle" />
                        <PagerStyle CssClass="grid_footer" />
                        <HeaderStyle CssClass="grid_header" Height="26px" />
                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                        <PagerSettings PageButtonCount="20" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqldsOrdItems" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="top" align="left">
                    <br />
                    <br />
                    <asp:Label ID="lblProcessList" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOProcessItemList %>"
                        runat="server" Visible="false" /><br />
                    <asp:GridView ID="grdAddProcLst" runat="server" AllowSorting="False" AllowPaging="True"
                        PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0" 
                        GridLines="none" AutoGenerateColumns="False"
                        DataSourceID="sqldsAddProcLst" Style="border-collapse: separate;" CssClass="view_grid650"
                        UseAccessibleHeader="False" DataKeyNames="orderItemProcID" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="orderItemProcID" HeaderStyle-ForeColor="#ffffff" HeaderText="ID:"
                                Visible="false" ReadOnly="True" SortExpression="orderItemProcID">
                                <ItemStyle Width="70px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcCode" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdProcessCode %>"
                                Visible="false" ReadOnly="True" SortExpression="ordItemProcCode">
                                <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ProcessDescription" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdProcessDescription %>"
                                ReadOnly="True" SortExpression="ProcessDescription">
                                <ItemStyle Width="250px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcFixedPrice" HeaderText="<%$ Resources:Resource, grdProcessFixedCost %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcPricePerHour" HeaderText="<%$ Resources:Resource, grdProcessCostPerHour %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcPricePerUnit" HeaderText="<%$ Resources:Resource, grdProcessCostPerUnit %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcHours" HeaderText="<%$ Resources:Resource, grdSOTotalHour %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcUnits" HeaderText="<%$ Resources:Resource, grdSOTotalUnit %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ProcessCost" HeaderText="<%$ Resources:Resource, grdSOProcessCost %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdEdit %>" Visible="false"
                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("orderItemProcID") %>'
                                        CommandName="Edit" ImageUrl="~/images/edit_icon.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" Visible="false"
                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                        CausesValidation="false" CommandArgument='<%# Eval("orderItemProcID") %>' CommandName="Delete" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="grid_footer" />
                        <RowStyle CssClass="grid_rowstyle" />
                        <PagerStyle CssClass="grid_footer" />
                        <HeaderStyle CssClass="grid_header" Height="26px" />
                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                        <PagerSettings PageButtonCount="20" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqldsAddProcLst" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                </td>
            </tr>
            <tr height="15">
                <td colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div id="divTerms" runat="server" align="center">
                        <table width="100%" style="border: 0;" border="0" align="center">
                            <tr>
                                <td colspan="7" align="right">
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign" width="23%">
                                    <asp:Label ID="lblShpToAddress" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShpToAddress %>"
                                        runat="server" />
                                </td>
                                <td width="1%">
                                </td>
                                <td align="left" width="28%">
                                    <asp:TextBox ID="txtShpToAddress" runat="server" TextMode="MultiLine" Rows="5" ReadOnly="true">
                                    </asp:TextBox>
                                    <asp:CustomValidator ID="custValShpToAddress" runat="server" SetFocusOnError="true"
                                        ErrorMessage="<%$ Resources:Resource, msgSOPlzEntShpToAddress %>" ClientValidationFunction="funCheckShpToAddress"
                                        Display="None">
                                    </asp:CustomValidator>
                                </td>
                                <td width="1%">
                                </td>
                                <td class="tdAlign" width="24%">
                                    <asp:Label ID="lblBillToAddress" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOBillToAddress %>"
                                        runat="server" />
                                </td>
                                <td width="1%">
                                </td>
                                <td align="left" width="22%">
                                    <asp:TextBox ID="txtBillToAddress" runat="server" TextMode="MultiLine" Rows="5" ReadOnly="true">
                                    </asp:TextBox>
                                    <asp:CustomValidator ID="custvalBillToAddress" runat="server" SetFocusOnError="true"
                                        ErrorMessage="<%$ Resources:Resource, msgSOPlzEntBillToAddress %>" ClientValidationFunction="funCheckBillToAddress"
                                        Display="None">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblTerms" CssClass="lblBold" Text="<%$ Resources:Resource, POShippingTerms%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtTerms" runat="server" TextMode="MultiLine" Rows="3" ReadOnly="true">
                                    </asp:TextBox>
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="lblNetTerms" CssClass="lblBold" Text="<%$ Resources:Resource, lblSONetTerms%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtNetTerms" runat="server" TextMode="SingleLine" ReadOnly="true"
                                        MaxLength="4">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblResellerShipBlankPref" CssClass="lblBold" Text="<%$ Resources:Resource, lblResellerShipBlankPref%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:RadioButtonList ID="rblstShipBlankPref" runat="server" RepeatDirection="Horizontal"
                                        Enabled="false">
                                        <asp:ListItem Text="<%$ Resources:Resource, lblYes%>" Value="1">
                                        </asp:ListItem>
                                        <asp:ListItem Text="<%$ Resources:Resource, lblNo%>" Value="0" Selected="True">
                                        </asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="lblISWeb" Text="<%$ Resources:Resource, lblSOSalesISWeb%>" CssClass="lblBold"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:RadioButtonList ID="rblstISWeb" runat="server" RepeatDirection="Horizontal"
                                        Enabled="false">
                                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1">
                                        </asp:ListItem>
                                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0" Selected="True">
                                        </asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblShpWarehouse" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingWarehouse %>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="dlShpWarehouse" runat="server" Width="155px" Enabled="false">
                                    </asp:DropDownList>
                                    <asp:CustomValidator ID="custvalShpWarehouse" runat="server" SetFocusOnError="true"
                                        ErrorMessage="<%$ Resources:Resource, lblSOPlzSelShippingWarehouse %>" ClientValidationFunction="funCheckShippingWarehouseCode"
                                        Display="None">
                                    </asp:CustomValidator>
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="lblShpTrackNo" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingTrackNo %>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtShpTrackNo" runat="server" Width="150px" MaxLength="25">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblShpCost" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingCost %>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtShpCost" runat="server" Width="150px" MaxLength="6" ReadOnly="true">
                                    </asp:TextBox>
                                    <asp:CompareValidator ID="comvalShpCost" EnableClientScript="true" SetFocusOnError="true"
                                        ControlToValidate="txtShpCost" Operator="DataTypeCheck" Type="double" Display="None"
                                        ErrorMessage="<%$ Resources:Resource, lblSOPlzEntNumericValueInShippingCost %>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="lblShpCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingCode %>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtShpCode" runat="server" Width="150px" MaxLength="15">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblShpDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingDate %>">
                                    </asp:Label>
                                </td>
                                <td>
                                </td>
                                <td align="left" colspan="5">
                                    <asp:TextBox ID="txtShippingDate" ReadOnly="true" runat="server" Width="90px" MaxLength="15">
                                    </asp:TextBox>
                                    <asp:ImageButton ID="imgCalShpDate" CausesValidation="false" runat="server" Enabled="false"
                                        ToolTip="Click to show calendar" ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender4" TargetControlID="txtShippingDate"
                                        runat="server" PopupButtonID="imgCalShpDate" Format="MM/dd/yyyy" Enabled="false">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RequiredFieldValidator ID="reqvalShpDate" runat="server" ControlToValidate="txtShippingDate"
                                        ErrorMessage="<%$ Resources:Resource, lblSOPlzEntShippingDate %>" SetFocusOnError="true"
                                        Display="None">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblNotes" CssClass="lblBold" Text="<%$ Resources:Resource, PONotes%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left" colspan="5">
                                    <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Rows="3" Width="550px">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblCustPO" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOCustomerPO%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtCustPO" runat="server" Width="130px" MaxLength="35" ReadOnly="true">
                                    </asp:TextBox>
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="lblPOStatus" CssClass="lblBold" Text="<%$ Resources:Resource, lblStatus%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="dlStatus" runat="server" Width="135px">
                                    </asp:DropDownList>
                                    <span class="style1">*</span>
                                    <asp:CustomValidator ID="custvalStatus" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, msgSOPlzSelStatus %>"
                                        ClientValidationFunction="funCheckStatus" Display="None">
                                    </asp:CustomValidator>
                                    <asp:CustomValidator ID="custvalStatusAndPO" runat="server" SetFocusOnError="true"
                                        ErrorMessage="<%$ Resources:Resource, msgSOPlzEntCustomerPO %>" ClientValidationFunction="funCheckStatusAndCustPO"
                                        Display="None">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr height="30" id="trCurrency" runat="server" visible="false">
                <td class="tdAlign">
                    <asp:Label ID="lblPOCurrencyCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOCurrencyCode%>"
                        runat="server" />
                </td>
                <td class="tdAlignLeft">
                    <asp:DropDownList ID="dlCurrencyCode" runat="server" Width="135px">
                    </asp:DropDownList>
                    <span class="style1">*</span>
                    <asp:CustomValidator ID="custvalCurrencyCode" runat="server" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:Resource, custvalPOCurrencyCode%>" ClientValidationFunction="funCheckCurrencyCode"
                        Display="None">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr height="30" id="trExchageRate" runat="server" visible="false">
                <td class="tdAlign">
                    <asp:Label ID="lblPOExRate" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOExRate%>"
                        runat="server" />
                </td>
                <td class="tdAlignLeft">
                    <asp:TextBox ID="txtExRate" runat="server" Width="130px" MaxLength="5">
                    </asp:TextBox>
                </td>
            </tr>
            <tr height="30">
                <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                    <table width="100%">
                        <tr>
                            <td height="20" align="center" width="30%">
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="cmdsave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                        style="min-width: 120px; text-align: center;">
                                        <%=Resources.Resource.cmdCssSave%></span></a></div>
                            </td>
                            <td width="50%" align="left">
                                <div class="buttonwrapper">
                                    <a id="cmdReset" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                        <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssBack%></span></a></div>
                                <div class="buttonwrapper">
                                    <a id="cmdBack" runat="server" visible="false" causesvalidation="false" class="ovalbutton"
                                        href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssBack%></span></a></div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsSales" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
    <br />
    <br />
    <script type="text/javascript" language="javascript">

        function funConfirm(id) {
            var frm = document.forms[0];
            for (i = 0; i < frm.elements.length; i++) {
                if (frm.elements[i].type == "checkbox") {
                    if (frm.elements[i].checked == false) {
                        alert('<% =Resources.Resource.POConfirmItems %>');
                        return false;
                    }
                }
            }
            return true;
        }
        function funCheckCurrencyCode(source, args) {
            if (document.getElementById('<%=dlCurrencyCode.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
        function funCheckShippingWarehouseCode(source, args) {
            if (document.getElementById('<%=dlShpWarehouse.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
        function funCheckStatus(source, args) {
            if (document.getElementById('<%=dlStatus.ClientID%>').selectedIndex == 0) {
                document.getElementById('<%=dlStatus.ClientID%>').focus();
                args.IsValid = false;
            }
        }
        function funCheckStatusAndCustPO(source, args) {
            if (document.getElementById('<%=dlStatus.ClientID%>').value == "A" &&
        document.getElementById('<%=txtCustPO.ClientID%>').value == "") {
                document.getElementById('<%=txtCustPO.ClientID%>').focus();
                args.IsValid = false;
            }
        }
        function funCheckShpToAddress(source, args) {
            if (document.getElementById('<%=txtShpToAddress.ClientID%>').value == "") {
                args.IsValid = false;
            }
        }
        function funCheckBillToAddress(source, args) {
            if (document.getElementById('<%=txtBillToAddress.ClientID%>').value == "") {
                args.IsValid = false;
            }
        }
        function openPDF(strOpen) {
            open(strOpen, "", 'height=365,width=810,left=180,top=150,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');
            return false;
        }
    </script>
</asp:Content>

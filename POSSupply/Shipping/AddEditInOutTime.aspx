﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="AddEditInOutTime.aspx.cs" Inherits="Shipping_AddEditInOutTime" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <ul class="form">
                <li>
                    <div class="lbl">
                        <asp:Label Text="<%$Resources:Resource, lblUsers %>" ID="lblSelectTaxGroup" runat="server" /></div>
                    <div class="input">
                        <asp:DropDownList ID="dlUser" runat="server" Width="200px" AutoPostBack="true">
                        </asp:DropDownList>
                        <span class="style1">*</span>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="dlUser"
                            ErrorMessage="<%$ Resources:Resource, reqvalTaxDescription%>" SetFocusOnError="true"
                            Display="Dynamic" ValidationGroup="TexAdd1"></asp:RequiredFieldValidator>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblTaxDescription" CssClass="lblBold" Text="<%$ Resources:Resource, lblIn%>"
                            runat="server" /></div>
                    <div class="input">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding-right: 2px;">
                                    <asp:TextBox ID="txtInDate" runat="server" Width="75px" CssClass="datepicker" />
                                </td>
                                <td style="padding: 0 2px;">
                                    <asp:TextBox ID="txtInHH" runat="server" Width="25px" CssClass="integerTextField"
                                        MaxLength="2" Text="00" />
                                </td>
                                <td style="padding: 0 2px;">
                                    <asp:TextBox ID="txtInMM" runat="server" Width="25px" CssClass="integerTextField"
                                        MaxLength="2" Text="00" />
                                </td>
                                <td style="padding: 0 2px;">
                                    <asp:DropDownList runat="Server" ID="ddlInTT" Width="50" Visible="false">
                                        <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0 2px;">
                                    (mm/dd/yyyy)
                                </td>
                                <td style="padding: 0 2px;">
                                    (hh)
                                </td>
                                <td style="padding: 0 2px;">
                                    (mm)
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li id="liOut" runat="server">
                    <div class="lbl">
                        <asp:Label ID="lblTaxPercentage" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblOut%>">
                        </asp:Label></div>
                    <div class="input">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding-right: 2px;">
                                    <asp:TextBox ID="txtOutDate" runat="server" Width="75px" CssClass="datepicker" />
                                </td>
                                <td style="padding: 0 2px;">
                                    <asp:TextBox ID="txtOutHH" runat="server" Width="25px" CssClass="integerTextField"
                                        MaxLength="2" Text="00" />
                                </td>
                                <td style="padding: 0 2px;">
                                    <asp:TextBox ID="txtOutMM" runat="server" Width="25px" CssClass="integerTextField"
                                        MaxLength="2" Text="00" />
                                </td>
                                <td style="padding: 0 2px;">
                                    <asp:DropDownList runat="Server" ID="ddlOutTT" Width="50" Visible="false">
                                        <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0 2px;">
                                    (mm/dd/yyyy)
                                </td>
                                <td style="padding: 0 2px;">
                                    (hh)
                                </td>
                                <td style="padding: 0 2px;">
                                    (mm)
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
            </ul>
            <div class="div_command">
                <asp:Button ID="btnSave" runat="server" Text="<%$Resources:Resource,cmdCssSave%>"
                    ValidationGroup="TexAdd1" OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" Text="<%$Resources:Resource, btnCancel%>" runat="server"
                    CausesValidation="false" OnClientClick="jQuery.FrameDialog.closeDialog();" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            var vSearchKey = getParameterByName('ClockInID');
            if (vSearchKey == 0) {



                var DateTime = new Date();
                var strYear = DateTime.getFullYear();
                var strMonth = DateTime.getMonth() + 1;
                var strDay = DateTime.getDate();
                var strHours = DateTime.getHours();
                var strMinutes = DateTime.getMinutes();
                var strSeconds = DateTime.getSeconds();


                var sDate = '';
                if (strMonth < 10) {
                    sDate += "0" + strMonth;
                }
                else {
                    sDate += strMonth;
                }

                sDate += "/";

                if (strDay < 10) {
                    sDate += "0" + strDay;
                }
                else {
                    sDate += strDay;
                }
                sDate += "/";
                sDate += strYear;

                $("#<%=txtInDate.ClientID%>").val(sDate);

                var sHr = '';
                if (strHours < 10) {
                    sHr += "0" + strHours;
                }
                else {
                    sHr += strHours;
                }

                $("#<%=txtInHH.ClientID%>").val(sHr);

                var sMin = '';
                if (strMinutes < 10) {
                    sMin += "0" + strMinutes;
                }
                else {
                    sMin += strMinutes;
                }

                $("#<%=txtInMM.ClientID%>").val(sMin);
            }
        });

        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
            return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
   
    </script>
</asp:Content>

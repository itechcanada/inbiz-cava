﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="JobsAssign.aspx.cs" Inherits="Shipping_JobsAssign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <ul class="form">
        <li>
             <div class="lbl">
                <asp:Label ID="Label1" CssClass="lblBold" Text="<%$ Resources:Resource, prnOrderNo%>"
                                                runat="server" />
            </div>
            <div class="input">
                <asp:Label ID="lblOrderNo" Font-Bold="true" Text="" runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblJobPlanningMgr" CssClass="lblBold" Text="<%$ Resources:Resource, lblJobPlanningMgr%>"
                                                runat="server" />
            </div>
            <div class="input">
                <asp:DropDownList ID="dlJobPlanningMgr" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ControlToValidate="dlJobPlanningMgr"
                    runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
             <div class="lbl">
                <asp:Label ID="lblCAFollowupDateTime" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblDueDate %>">
                                            </asp:Label>
            </div>
            <div class="input">
                <asp:TextBox ID="txtDueDateTime" CssClass="datepicker" Enabled="true" runat="server" Width="90px" MaxLength="20">
                                            </asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="*" ControlToValidate="txtDueDateTime"
                    runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="Label3" CssClass="lblBold" Text="<%$ Resources:Resource, lblDueTime%>"
                    runat="server" />
            </div>
             <div class="input" style="width:400px;">
                <asp:TextBox ID="txtHour" runat="server" Width="40" CssClass="integerTextField" MaxLength="2" ></asp:TextBox> : 
                <asp:TextBox ID="txtMin" runat="server" Width="40" CssClass="integerTextField" MaxLength="2" ></asp:TextBox>
                <asp:DropDownList runat="Server" ID="ddlTime" Width="45">
                    <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                    <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                </asp:DropDownList>
                (hh:mm)
                <asp:RangeValidator ID="rgVHour" runat="server" ControlToValidate="txtHour" Type="Integer"
                    ErrorMessage="<%$ Resources:Resource, rgVHour%>" MaximumValue="12" MinimumValue="1"
                    Display="None"></asp:RangeValidator>
                <asp:RangeValidator ID="rgVMin" runat="server" ControlToValidate="txtMin" Type="Integer"
                    ErrorMessage="<%$ Resources:Resource, rgVMin%>" MaximumValue="59" MinimumValue="0"
                    Display="None"></asp:RangeValidator>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="lblARNote" CssClass="lblBold" Text="<%$ Resources:Resource, lblARNote%>"
                                                runat="server" />
            </div>
            <div class="input" style="width:400px;">
                    <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="3" Width="350px">
                                            </asp:TextBox>
            </div>
            <div class="clearBoth">
            </div>
        </li>
    </ul>
    <div class="div_command">          
        <asp:Button ID="btnAssignTask" Text="<%$Resources:Resource, lblAssignTask %>" 
            runat="server" onclick="btnAssignTask_Click" />         
        <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
            CausesValidation="False" 
            OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" /> 
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


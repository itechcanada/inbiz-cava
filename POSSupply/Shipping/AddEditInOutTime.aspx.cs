﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using System.Data;


public partial class Shipping_AddEditInOutTime : System.Web.UI.Page
{
    string[] _aarKey = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString.AllKeys.Contains("ClockInID"))
            {
                _aarKey = Request.QueryString["ClockInID"].Split('~');
            }
            MessageState.SetGlobalMessage(MessageType.Success, "");
            if (!IsPostBack)
            {
                InbizUser objUser = new InbizUser();
                objUser.FillAllUsers(null, dlUser, null);
                ListItem lstFindUser = dlUser.Items.FindByValue(BusinessUtility.GetString(CurrentUser.UserID));
                if (lstFindUser != null)
                {
                    lstFindUser.Selected = true;
                }

                if (CurrentUser.IsInRole(RoleID.POS_MANAGER))
                {
                    dlUser.Enabled = true;
                }
                else
                {
                    dlUser.Enabled = false;
                }

                //Response.Write(DateTime.Now);
                ////txtInDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                //DateTime dtInTime = DateTime.Now.ToLocalTime();

                ////txtInDate.Text = DateTime.Now.ToClientTime().ToString("MM/dd/yyyy");
                //txtInDate.Text = dtInTime.ToString("MM/dd/yyyy");
                //txtInHH.Text = BusinessUtility.GetString(dtInTime.Hour);
                //txtInMM.Text = BusinessUtility.GetString(dtInTime.Minute);
                //ddlInTT.SelectedValue = BusinessUtility.GetString(dtInTime);

                //txtOutDate.Text = DateTime.Now.ToString("MM/dd/yyyy");

                liOut.Visible = false;


                if (this.ClockInID > 0)
                {
                    liOut.Visible = true;
                    UsersClockIn objUsrClockIn = new UsersClockIn();
                    objUsrClockIn.idUsersClockIn = this.ClockInID;
                    DataTable dt = objUsrClockIn.GetClockInList(null, "", "");
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            foreach (ListItem li in dlUser.Items)
                            {
                                if (li.Selected == true)
                                    li.Selected = false;
                            }


                            lstFindUser = dlUser.Items.FindByValue(BusinessUtility.GetString(dt.Rows[0]["UID"]));
                            if (lstFindUser != null)
                            {
                                lstFindUser.Selected = true;
                            }
                            DateTime dtIn = BusinessUtility.GetDateTime(dt.Rows[0]["In"]);


                            txtInDate.Text = dtIn.ToString("MM/dd/yyyy");
                            txtInHH.Text = BusinessUtility.GetString(dtIn.Hour.ToString("D2"));
                            txtInMM.Text = BusinessUtility.GetString(dtIn.Minute.ToString("D2"));
                            //ddlInTT.SelectedValue = BusinessUtility.GetString(dtIn.ToString("tt"));

                            if (BusinessUtility.GetString(dt.Rows[0]["Out"]) != "")
                            {
                                DateTime dtOut = BusinessUtility.GetDateTime(dt.Rows[0]["Out"]);
                                txtOutDate.Text = dtOut.ToString("MM/dd/yyyy");
                                txtOutHH.Text = BusinessUtility.GetString(dtOut.Hour.ToString("D2"));
                                txtOutMM.Text = BusinessUtility.GetString(dtOut.Minute.ToString("D2"));
                                //ddlOutTT.SelectedValue = BusinessUtility.GetString(dtOut.ToString("tt"));
                            }
                        }
                    }
                }

                dlUser.Focus();
            }
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }
    private int ClockInID
    {
        get
        {
            int id = 0;
            if (_aarKey != null && _aarKey.Length >= 1)
            {
                int.TryParse(_aarKey[0], out id);
            }
            return id;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                UsersClockIn objUsrClockIn = new UsersClockIn();
                int hh, mm;
                int.TryParse(txtInHH.Text, out hh);
                int.TryParse(txtInMM.Text, out mm);
                string strInDate = string.Format("{0} {1:00}:{2:00}", txtInDate.Text, hh, mm); //, ddlInTT.SelectedValue
                objUsrClockIn.InDateTime = BusinessUtility.GetDateTime(txtInDate.Text, DateFormat.MMddyyyy);
                objUsrClockIn.InDateTime = objUsrClockIn.InDateTime.AddHours(hh).AddMinutes(mm).AddSeconds(00);

                int.TryParse(txtOutHH.Text, out hh);
                int.TryParse(txtOutMM.Text, out mm);
                string strOutDate = string.Format("{0} {1:00}:{2:00}", txtOutDate.Text, hh, mm);//, ddlOutTT.SelectedValue
                objUsrClockIn.OutDateTime = BusinessUtility.GetDateTime(txtOutDate.Text, DateFormat.MMddyyyy);
                objUsrClockIn.OutDateTime = objUsrClockIn.OutDateTime.AddHours(hh).AddMinutes(mm).AddSeconds(00);


                objUsrClockIn.idUsersClockIn = this.ClockInID;
                objUsrClockIn.UserIDInOut = BusinessUtility.GetInt(dlUser.SelectedItem.Value);
                if (this.WarehouseCode == "")
                {
                    objUsrClockIn.WhsCode = CurrentUser.UserDefaultWarehouse;
                }
                else
                {
                    objUsrClockIn.WhsCode = this.WarehouseCode;
                }
                //strInDate = strInDate.Replace("-", "/");
                //strOutDate = strOutDate.Replace("-", "/");
                ////objUsrClockIn.InDateTime = BusinessUtility.GetDateTime(strInDate, "MM/dd/yyyy hh:mm:ss");// tt
                ////objUsrClockIn.OutDateTime = BusinessUtility.GetDateTime(strOutDate, "MM/dd/yyyy hh:mm:ss");// tt
                //objUsrClockIn.InDateTime = Convert.ToDateTime(strInDate);
                //objUsrClockIn.OutDateTime = Convert.ToDateTime(strOutDate);

                if (objUsrClockIn.Save(dbHelp) == true)
                {
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.lblClockInSaved);
                    if (IsCallFromPOS == false)
                    {
                        Globals.RegisterCloseDialogScript(this, "parent.reloadGrid();", true);
                        Globals.RegisterReloadParentScript(this);
                        Globals.RegisterCloseDialogScript(this);
                    }
                    else
                    {
                        Globals.RegisterCloseDialogScript(this, "parent.redirectToPOS();", true);
                        Globals.RegisterCloseDialogScript(this);
                    }
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.lblClockNotSaved);
                }
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    public string WarehouseCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["whsCode"]);
        }
    }

    private bool IsCallFromPOS
    {
        get
        {
            if (BusinessUtility.GetString(Request.QueryString["InvokeSrc"]).ToUpper() == "POS".ToUpper())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
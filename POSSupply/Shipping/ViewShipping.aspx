﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="ViewShipping.aspx.cs" Inherits="Shipping_ViewShipping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
    <style type="text/css">
        .td_hack
        {
            /*font-size: 9px;*/
            height: 100%;
            line-height: 10px;
            margin-left: -2px;
            margin-right: -2px;
            padding: 2px 4px;
            position: relative;
            width: 100%;
        }
        .shippingHeld
        {
            background-color: #FFE4E4;
            color: #000 !important;
            cursor: pointer;
            font-weight: normal;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <%= Resources.Resource.lblShipping%>
        </b>
    </div>
    <div style="float: right;">
     <asp:Button ID="btnShipmentAll" Text="<%$Resources:Resource, lblShipmentAll%> " runat="server"  OnClientClick="return shipmentList();"
                         OnClick="btnShipment_Command" />
        <asp:Button ID="btnPrintPackingLists" Text="<%$ Resources:Resource, lblPrintPackingLists %>"
            runat="server" CausesValidation="False" OnClick="btnPrintPackingList_Click" />
        <%--<asp:Button ID="btnPrintPackingList" 
            Text="<%$Resources:Resource, lblPrintPackingLists%>" runat="server" 
            CausesValidation="false" onclick="" />--%>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div onkeypress="return disableEnterKey(event)" class="divMainContent">
        <div id="grid_wrapper" style="width: 100%;">
            <trirand:JQGrid runat="server" ID="jgrdSHP" DataSourceID="sqldsSHP" AutoWidth="True"
               MultiSelect="true" OnCellBinding="jgrdSHP_CellBinding" OnDataRequesting="jgrdSHP_DataRequesting"
                Height="400" OnDataRequested="jgrdSHP_DataRequested">
                <Columns>
                    <trirand:JQGridColumn DataField="ordID" HeaderText="<%$ Resources:Resource, grdSOOrderNo %>"
                        Editable="false" PrimaryKey="true" />
                    <trirand:JQGridColumn DataField="PartnerAcronyme" HeaderText="<%$ Resources:Resource, grdCMAcronyme %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="CustomerName" HeaderText="<%$ Resources:Resource, grdSOCustomerName %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="AgentName" HeaderText="<%$ Resources:Resource, grdSOAgentName %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="ordShpWhsCode" HeaderText="<%$ Resources:Resource, lblFromWhs %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="ordShpToWhsCode" HeaderText="<%$ Resources:Resource, lblToWhs %>" 
                        Editable="false" />
                    <trirand:JQGridColumn DataField="ordShpTrackNo" HeaderText="<%$ Resources:Resource, grdSOShpTrackNo %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="ordStatus" HeaderText="<%$ Resources:Resource, grdPOStatus %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="ordShpDate" HeaderText="<%$ Resources:Resource, grdSOShippingDate %>"
                        Editable="false" DataFormatString="{0:MMM dd, yyyy}" />
                    <trirand:JQGridColumn DataField="QuotePrintCounter" HeaderText="<%$ Resources:Resource, lblQuotePrintCounter %>"
                        Editable="false" TextAlign="Center" />
                    <trirand:JQGridColumn DataField="SalesOrderPrintCounter" HeaderText="<%$ Resources:Resource, lblSalesOrderPrintCounter %>"
                        Editable="false" TextAlign="Center" />
                    <trirand:JQGridColumn DataField="PackingListPrintCounter" HeaderText="<%$ Resources:Resource, lblPackingListPrintCounter %>"
                        Editable="false" TextAlign="Center" />
                    <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdARDetail %>" DataField="ordID"
                        Sortable="false" TextAlign="Center" Width="50" />
                    <%--<trirand:JQGridColumn HeaderText="<%$ Resources:Resource, lblShipments %>" DataField="ordID" Sortable="false" TextAlign="Center" Width="50" />--%>
                </Columns>
                <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                    ShowDeleteButton="false" ShowSearchButton="false" />
                <SortSettings InitialSortColumn=""></SortSettings>
                <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                <ClientSideEvents LoadComplete="jqGridResize" />
            </trirand:JQGrid>
            <asp:SqlDataSource ID="sqldsSHP" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                ProviderName="MySql.Data.MySqlClient"></asp:SqlDataSource>
        </div>
        <br />
        <div class="div_command">
            <asp:Button ID="btncmdBack" runat="server" Text="<%$ Resources:Resource,cmdInvBack%>"
                CausesValidation="false" OnClick="btncmdBack_Click" Width="150px" />
        </div>
        <asp:ValidationSummary ID="valsProcurement" runat="server" ShowMessageBox="true"
            ShowSummary="false" />
    </div>
    <asp:HiddenField  ID="hdnPrintOrderIDs" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        Search form</div>
                    <div class="icon">
                        <img src="../../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, liWarehouseLocation%>"
                        AssociatedControlID="dlWarehouse" CssClass="filter-key"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:DropDownList ID="dlWarehouse" runat="server" Width="185px" ValidationGroup="PrdSrch">
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"
                        AssociatedControlID="dlSearch" CssClass="filter-key"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:DropDownList ID="dlSearch" runat="server" Width="185px" ValidationGroup="PrdSrch">
                        <asp:ListItem Text="Search By" Value="" />
                        <asp:ListItem Value="ON" Text="Order Number" />
                        <asp:ListItem Text="Customer Name" Value="CN" />
                        <asp:ListItem Text="Sale Agent Name" Value="AN" />
                        <asp:ListItem Text="Track No." Value="TN" />
                        <asp:ListItem Text="Today" Value="T" />
                        <asp:ListItem Text="Tomorrow" Value="TM" />
                        <asp:ListItem Text="This Week" Value="TW" />
                        <asp:ListItem Text="Next Week" Value="NW" />
                        <asp:ListItem Text="All" Value="A" />
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword%>"
                        AssociatedControlID="txtSearch" CssClass="filter-key"></asp:Label>
                </h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch" ValidationGroup="PrdSrch"></asp:TextBox>
                    &nbsp;</div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="search" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="Literal1"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $grid = $("#<%=jgrdSHP.ClientID%>");
        
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=jgrdSHP.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        function reloadGrid() {
            $grid.trigger("reloadGrid");
        }

        function goToShipmentPage(oid) {
            if (isNaN(oid)) {
                alert("Invalid Odrder Number!");
                return false;
            }
            var $dialog = jQuery.FrameDialog.create({
                url: "Shipment.aspx?callBack=reloadGrid&oid=" + oid,
                //url: '/InbizWebDvlp/Sales/mdSearchCustomer.aspx',
                title: "<%=Resources.Resource.lblUpdateShipment%>",
                loadingClass: "loading-image",
                modal: true,
                width: "800",
                height: "560",
                autoOpen: false
            });
            $dialog.dialog('open');
            return false;
        }

        $(document).ready(function () {

        });


        function LoadShipmentPopup(ordIDs) {
            var $dialog = jQuery.FrameDialog.create({
                url: "../shipping/Shipment.aspx?callBack=setEmptyText&oids=" + ordIDs,
                title: "<%=Resources.Resource.lblUpdateShipment%>",
                loadingClass: "loading-image",
                modal: true,
                width: "800",
                height: "560",
                autoOpen: false
            }).bind("dialogclose", function () {
                popupOpen = false;
            });
            $dialog.dialog('open');
            popupOpen = true;
            return false;
        }

        function setEmptyText() {
            window.location.href = window.location.href;
        }

        function shipmentList() {
            var arrRooms = $grid.getGridParam('selarrrow');
            if (arrRooms.length <= 0) {
                alert("<%=Resources.Resource.msgJobSlectOneOrder%>");
                return false;
            }
            //alert(arrRooms.join(","));
            $("#<%=hdnPrintOrderIDs.ClientID%>").val(arrRooms.join(","))

        }

    </script>
</asp:Content>

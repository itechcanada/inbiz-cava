﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Trirand.Web.UI.WebControls;

using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

using Newtonsoft.Json;
using System.Data;
using System.Text;
public partial class Shipping_validateSKU : System.Web.UI.Page
{
    //List<ShippingReturnValidation> lstShippingValidation = new List<ShippingReturnValidation>();

    protected void Page_Load(object sender, EventArgs e)
    {
        MessageState.SetGlobalMessage(MessageType.Success, "");
        if (!CurrentUser.IsInRole(RoleID.SHIPPING_MANAGER) && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Globals.RegisterParentRedirectPageUrl(this, ResolveUrl("~/Errors/AccessDenied.aspx"));
            return;
        }

        if (!IsPostBack)
        {
            if (ValidationOrderID > 0)
            {
                pnlEdit.Visible = false;
                pnlContent.Visible = false;
            }

        }
        txtSKU.Focus();
    }

    protected void grdAddress_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (ValidationOrderID > 0)
        {
            ShippingReturnValidation objShippingValidation = new ShippingReturnValidation();
            grdAddress.DataSource = objShippingValidation.GetShippingHistory(this.ValidationOrderID, Globals.CurrentAppLanguageCode, this.validationFor);
        }
        else
        {
            this.dtValidateOrder.DefaultView.Sort = "Seq ASC";
            grdAddress.DataSource = (this.dtValidateOrder);
            //grdAddress.DataSource = lstShippingValidation;
        }
    }

    protected void grdAddress_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 8)
        {
            string sImageName = "";
            if (e.CellHtml == "1")
            {
                sImageName = "vAdd";
            }
            else if (e.CellHtml == "2")
            {
                sImageName = "vSub";
            }
            else if (e.CellHtml == "3")
            {
                sImageName = "vTick";
            }
            e.CellHtml = string.Format("<img  src='../Images/{0}.png' style='width:18px;border:none;' />", sImageName);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {

                if (this.dtValidateOrder.Rows.Count > 0)
                {
                    ShippingReturnValidation objShippingValidation = new ShippingReturnValidation();
                    if (objShippingValidation.SaveShippingValidationHistory(this.dtValidateOrder, this.OrderID, CurrentUser.UserID, this.validationFor) == true)
                    {
                        int isValidateSKU = 0;
                        DataView dvValidateSKU = this.dtValidateOrder.DefaultView;
                        dvValidateSKU.RowFilter = "STATUS < 3";
                        if (dvValidateSKU.ToTable().Rows.Count <= 0)
                        {
                            isValidateSKU = 1;

                            if (this.validationFor.ToUpper() == "planshippment".ToUpper())
                            {

                                DataTable dtValidateOrder = (DataTable)Session["validateorder"];
                                var lstFromSession = Session["SHIPMENT_CART"] as List<SalesCartEntity>;
                                int id = 0;
                                int qtyToUpdate = 0;
                                if (dtValidateOrder != null)
                                {
                                    foreach (DataRow dRowValidate in dtValidateOrder.Rows)
                                    {
                                        if (BusinessUtility.GetInt(dRowValidate["Status"]) == 3)
                                        {
                                            id = BusinessUtility.GetInt(dRowValidate["ProductID"]);
                                            qtyToUpdate = BusinessUtility.GetInt(dRowValidate["ShpQty"]);
                                            var itemtoUpdate = lstFromSession.Where(i => i.ProductID == id).FirstOrDefault();
                                            if (itemtoUpdate != null)
                                            {
                                                itemtoUpdate.ToShipQty = qtyToUpdate;
                                                Session["SHIPMENT_CART"] = lstFromSession;
                                            }
                                        }
                                    }
                                }



                                //var lstFromSession = Session["SHIPMENT_CART"] as List<SalesCartEntity>;
                                //int id = 0;
                                //int qtyToUpdate = 0;
                                //if (lstFromSession != null && int.TryParse(Request.QueryString["editRowKey"], out id) && id > 0)
                                //{
                                //    int.TryParse(Request.QueryString["qtyToShip"], out qtyToUpdate);
                                //    var itemtoUpdate = lstFromSession.Where(i => i.ID == id).FirstOrDefault();
                                //    if (itemtoUpdate != null)
                                //    {
                                //        //if (itemtoUpdate.Quantity >= itemtoUpdate.PreviousReturnedQty - qtyToUpdate)
                                //        if (itemtoUpdate.PreviousReturnedQty >= qtyToUpdate)
                                //        {
                                //            itemtoUpdate.ToShipQty = qtyToUpdate;
                                //            Session["SHIPMENT_CART"] = lstFromSession;
                                //        }
                                //        else
                                //        {
                                //            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblOverShipQtyMsg);
                                //        }
                                //    }
                                //}

                            }
                            else if (this.validationFor.ToUpper() == "shipping".ToUpper())
                            {
                            }
                            else //(this.validationFor.ToUpper() != "shipping".ToUpper())
                            {
                                DataTable dtValidateOrder = (DataTable)Session["validateorder"];
                                var lstFromSession = Session["RETURN_CART"] as List<ReturnCart>;
                                int id = 0;
                                int qtyToUpdate = 0;
                                if (dtValidateOrder != null)
                                {
                                    foreach (DataRow dRowValidate in dtValidateOrder.Rows)
                                    {
                                        if (BusinessUtility.GetInt(dRowValidate["Status"]) == 3)
                                        {
                                            id = BusinessUtility.GetInt(dRowValidate["ProductID"]);
                                            qtyToUpdate = BusinessUtility.GetInt(dRowValidate["ShpQty"]);
                                            var itemtoUpdate = lstFromSession.Where(i => i.ProductID == id).FirstOrDefault();
                                            if (itemtoUpdate != null)
                                            {
                                                itemtoUpdate.ProductToReturnQty = qtyToUpdate;
                                                Session["RETURN_CART"] = lstFromSession;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        Globals.RegisterCloseDialogScript(this.Page, string.Format("parent.ValidateSKU('{0}');", BusinessUtility.GetString(isValidateSKU)), true);
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
                    }
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblSKUNotFound);
                }
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
            }
        }
    }

    protected void ResetPage()
    {
        txtSKU.Text = "";
        txtSKU.Focus();
    }

    protected string OrderID
    {
        get
        {
            //int id = 0;
            //int.TryParse(Request.QueryString["oid"], out id);
            //return id;

            return BusinessUtility.GetString(Request.QueryString["oid"]);
        }
    }

    protected int ValidationOrderID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["vOid"], out id);
            return id;
        }
    }

    protected string validationFor
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["validationfor"]);
        }
    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    //protected void btnGO_Click(object sender, EventArgs e)
    //{
    //    //if (Page.IsValid)
    //    //{
    //    //    try
    //    //    {
    //    //        Product objProduct = new Product();
    //    //        int productID = objProduct.GetProductID(null, txtSKU.Text);
    //    //        if (productID > 0)
    //    //        {
    //    //            if (ChkProductIDExist(productID))
    //    //            {
    //    //                DataRow[] customerRow = this.dtValidateOrder.Select("productid = " + productID + "");
    //    //                int iShpQty = BusinessUtility.GetInt(customerRow[0]["ShpQty"]) + 1;
    //    //                int iOrdQty = BusinessUtility.GetInt(customerRow[0]["OrdQty"]);
    //    //                int iStatus = this.SKUStatus(iShpQty, iOrdQty);
    //    //                customerRow[0]["ShpQty"] = iShpQty;
    //    //                customerRow[0]["Status"] = iStatus;
    //    //                if (iShpQty > iOrdQty)
    //    //                {

    //    //                    this.dtValidateOrder.RejectChanges();
    //    //                }
    //    //                else
    //    //                {
    //    //                    this.dtValidateOrder.AcceptChanges();
    //    //                }
    //    //            }
    //    //            else
    //    //            {
    //    //                int iShpQty = 1;
    //    //                int iOrdQty = 0;
    //    //                OrderItems objOrdItems = new OrderItems();
    //    //                var vOrdItem = objOrdItems.GetOrderItemList(this.OrderID, productID);

    //    //                if (vOrdItem != null)
    //    //                {
    //    //                    if (vOrdItem.Count > 0)
    //    //                    {
    //    //                        iOrdQty = BusinessUtility.GetInt(vOrdItem[0].OrdProductQty);
    //    //                    }
    //    //                    else
    //    //                    {
    //    //                        MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblSKUNotFound);
    //    //                        return;
    //    //                    }
    //    //                }
    //    //                int iStatus = this.SKUStatus(iShpQty, iOrdQty);
    //    //                Product.ProductClothDesc objProductClothDesc = new Product.ProductClothDesc();
    //    //                objProductClothDesc.ProductID = productID;
    //    //                objProductClothDesc.getClothDesc(null, CurrentUser.UserID);

    //    //                string pColor = "";
    //    //                string pCollection = "";
    //    //                string pStyle = BusinessUtility.GetString(objProductClothDesc.Style);

    //    //                //Get Color Name
    //    //                ProductColor objProductColor = new ProductColor();
    //    //                objProductColor.ColorID = BusinessUtility.GetInt(objProductClothDesc.Color);
    //    //                var lst = objProductColor.GetAllColorList(null, Globals.CurrentAppLanguageCode);
    //    //                if (lst.Count > 0)
    //    //                {
    //    //                    pColor = lst[0].ColorName;
    //    //                }

    //    //                // Get Size Name
    //    //                string sSize = "";
    //    //                ProductSize objProductSize = new ProductSize();
    //    //                objProductSize.SizeID = BusinessUtility.GetInt(objProductClothDesc.Size);
    //    //                var lstSize = objProductSize.GetAllSizeList(null, Globals.CurrentAppLanguageCode);
    //    //                if (lstSize.Count > 0)
    //    //                {
    //    //                    sSize = lstSize[0].SizeName;
    //    //                }

    //    //                // Get Collection Name
    //    //                Collection objCollection = new Collection();
    //    //                objCollection.CollectionID = BusinessUtility.GetInt(objProductClothDesc.Collection);
    //    //                var lstCollection = objCollection.GetAllCollectionList(null, Globals.CurrentAppLanguageCode);
    //    //                if (lstCollection.Count > 0)
    //    //                {
    //    //                    pCollection = lstCollection[0].ShortName;
    //    //                }

    //    //                //lstShippingValidation.Add(
    //    //                //new ShippingReturnValidation
    //    //                //{
    //    //                //    ProductID = productID,
    //    //                //    SKU = txtSKU.Text,
    //    //                //    Color = pColor,
    //    //                //    Size = sSize,
    //    //                //    ShpQty = iShpQty,
    //    //                //    OrdQty = iOrdQty,
    //    //                //    Status = iStatus,
    //    //                //    Seq = 1
    //    //                //});

    //    //                DataRow dRow = (DataRow)this.dtValidateOrder.NewRow();
    //    //                dRow["ProductID"] = productID;
    //    //                dRow["SKU"] = txtSKU.Text;
    //    //                dRow["Collection"] = pCollection;
    //    //                dRow["Style"] = pStyle;
    //    //                dRow["Color"] = pColor;
    //    //                dRow["Size"] = sSize;
    //    //                dRow["ShpQty"] = iShpQty;
    //    //                dRow["OrdQty"] = iOrdQty;
    //    //                dRow["Status"] = iStatus;
    //    //                dRow["Seq"] = 1;
    //    //                this.dtValidateOrder.Rows.Add(dRow);
    //    //                this.dtValidateOrder.AcceptChanges();
    //    //            }
    //    //            ResetPage();
    //    //        }
    //    //        else
    //    //        {
    //    //            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblInvalidSKU);
    //    //        }
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
    //    //    }
    //    //}
    //}

    private bool ChkProductIDExist(int iProductID)
    {
        bool isExists = false;

        foreach (DataRow dRow in this.dtValidateOrder.Rows)
        {
            if (BusinessUtility.GetInt(dRow["ProductID"]) == iProductID)
            {
                isExists = true;
            }
        }
        return isExists;
    }

    private int SKUStatus(int iShpQty, int iOrdQty)
    {
        int iStatus = 1;
        if (iShpQty == iOrdQty)
        {
            iStatus = 3;
        }
        else if (iShpQty > iOrdQty)
        {
            iStatus = 1;
        }
        else if (iShpQty < iOrdQty)
        {
            iStatus = 2;
        }
        return iStatus;
    }

    protected void btnValidateOrder_Click(object sender, EventArgs e)
    {

        OrderItems objOrdItems = new OrderItems();
        var lstOrdItems = objOrdItems.GetOrderItemList(null, this.OrderID);
        foreach (var itemOrder in lstOrdItems)
        {
            
            int iOrdProductID = BusinessUtility.GetInt(itemOrder.OrdProductID);
            int iOrdProductQty = BusinessUtility.GetInt(itemOrder.OrdProductQty);

            if (ChkProductIDExist(iOrdProductID) == false)
            {
                int iShpQty = 0;
                int iStatus = this.SKUStatus(iShpQty, iOrdProductQty);
                Product.ProductClothDesc objProductClothDesc = new Product.ProductClothDesc();
                objProductClothDesc.ProductID = iOrdProductID;
                objProductClothDesc.getClothDesc(null, CurrentUser.UserID);

                string pCollection = "";
                string pStyle = BusinessUtility.GetString(objProductClothDesc.Style);

                string pColor = "";
                ProductColor objProductColor = new ProductColor();
                objProductColor.ColorID = BusinessUtility.GetInt(objProductClothDesc.Color);
                var lst = objProductColor.GetAllColorList(null, Globals.CurrentAppLanguageCode);
                if (lst.Count > 0)
                {
                    pColor = lst[0].ColorName;
                }

                string sSize = "";
                ProductSize objProductSize = new ProductSize();
                objProductSize.SizeID = BusinessUtility.GetInt(objProductClothDesc.Size);
                var lstSize = objProductSize.GetAllSizeList(null, Globals.CurrentAppLanguageCode);
                if (lstSize.Count > 0)
                {
                    sSize = lstSize[0].SizeName;
                }


                // Get Collection Name
                Collection objCollection = new Collection();
                objCollection.CollectionID = BusinessUtility.GetInt(objProductClothDesc.Collection);
                var lstCollection = objCollection.GetAllCollectionList(null, Globals.CurrentAppLanguageCode);
                if (lstCollection.Count > 0)
                {
                    pCollection = lstCollection[0].ShortName;
                }

                Product pbjProduct = new Product();
                pbjProduct.PopulateObject(iOrdProductID);
                DataRow dRow = (DataRow)this.dtValidateOrder.NewRow();
                dRow["ProductID"] = iOrdProductID;
                dRow["SKU"] = pbjProduct.PrdUPCCode;
                dRow["Color"] = pColor;
                dRow["Size"] = sSize;
                dRow["ShpQty"] = iShpQty;
                dRow["OrdQty"] = iOrdProductQty;
                dRow["Status"] = iStatus;
                dRow["Seq"] = 0;
                dRow["Collection"] = pCollection;
                dRow["Style"] = pStyle;
                this.dtValidateOrder.Rows.Add(dRow);
                this.dtValidateOrder.AcceptChanges();
            }
        }

    }

    private DataTable dtValidateOrder
    {
        get
        {
            if (Session["validateorder"] == null)
            {
                DataTable dtValidateOrder = new DataTable();
                dtValidateOrder.TableName = "OrderValidate";
                dtValidateOrder.Columns.Add(new DataColumn { ColumnName = "ProductID", DataType = typeof(Int32), Unique = true });
                dtValidateOrder.Columns.Add(new DataColumn { ColumnName = "SKU", DataType = typeof(string), Unique = true });
                dtValidateOrder.Columns.Add(new DataColumn { ColumnName = "Collection", DataType = typeof(string) });
                dtValidateOrder.Columns.Add(new DataColumn { ColumnName = "Style", DataType = typeof(string) });
                dtValidateOrder.Columns.Add(new DataColumn { ColumnName = "Color", DataType = typeof(string) });
                dtValidateOrder.Columns.Add(new DataColumn { ColumnName = "Size", DataType = typeof(string) });
                dtValidateOrder.Columns.Add(new DataColumn { ColumnName = "ShpQty", DataType = typeof(Int32), DefaultValue = 0 });
                dtValidateOrder.Columns.Add(new DataColumn { ColumnName = "OrdQty", DataType = typeof(Int32), DefaultValue = 0 });
                dtValidateOrder.Columns.Add(new DataColumn { ColumnName = "Status", DataType = typeof(Int32), DefaultValue = 1 }); // value :- 1 for + , 2 for -, 3 for check sign
                dtValidateOrder.Columns.Add(new DataColumn { ColumnName = "Seq", DataType = typeof(Int32), DefaultValue = 1 });

                Session["validateorder"] = dtValidateOrder;
                return dtValidateOrder;
            }
            else
            {
                return (DataTable)Session["validateorder"];
            }
        }
    }


    protected void btnValidateSKU_Click(object sender, EventArgs e)
    {
        if (hdnSkuList.Value == "")
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblSKUNotFound);
            return;
        }
        if (chkAutoValidate.Checked)
        {
            txtSKU.Enabled = false;
        }

        try
        {
            List<SKUError> lstSKUErr = new List<SKUError>();
            string[] arrProductSKU = BusinessUtility.GetString(hdnSkuList.Value).Split(',');

            foreach (string sSKU in arrProductSKU)
            {
                Product objProduct = new Product();
                int productID = objProduct.GetProductID(null, sSKU.Trim());
                if (productID > 0)
                {
                    if (ChkProductIDExist(productID))
                    {
                        DataRow[] customerRow = this.dtValidateOrder.Select("productid = " + productID + "");
                        int iShpQty = BusinessUtility.GetInt(customerRow[0]["ShpQty"]) + 1;
                        int iOrdQty = BusinessUtility.GetInt(customerRow[0]["OrdQty"]);
                        int iStatus = this.SKUStatus(iShpQty, iOrdQty);
                        customerRow[0]["ShpQty"] = iShpQty;
                        customerRow[0]["Status"] = iStatus;

                        if (iShpQty > iOrdQty)
                        {
                            AddError(lstSKUErr, sSKU, Resources.Resource.errCouldNotValidateOverQty);
                            this.dtValidateOrder.RejectChanges();
                        }
                        else
                        {
                            this.dtValidateOrder.AcceptChanges();
                        }

                        //this.dtValidateOrder.AcceptChanges();
                    }
                    else
                    {
                        int iShpQty = 1;
                        int iOrdQty = 0;
                        OrderItems objOrdItems = new OrderItems();
                        var vOrdItem = objOrdItems.GetOrderItemList(this.OrderID, productID);
                        OrderReturn rtn = new OrderReturn();
                        string[] iorderIDs = this.OrderID.Split(",".ToArray());
                        string[] iOrderItemsIDs = objOrdItems.GetOrderItemIDs(null, iorderIDs, productID);
                        int iReturnQty = 0;
                        if (iOrderItemsIDs.Length > 0)
                        {
                            iReturnQty = BusinessUtility.GetInt(rtn.GetPreviousReturnedQty(null, iOrderItemsIDs));
                        }
                        
                        Boolean isSKUinOrder = false;

                        if (vOrdItem != null)
                        {
                            if (vOrdItem.Count > 0)
                            {
                                iOrdQty = BusinessUtility.GetInt(vOrdItem[0].OrdProductQty) - iReturnQty;
                                if (iOrdQty > 0)
                                    isSKUinOrder = true;
                                else
                                {
                                    isSKUinOrder = false;
                                    AddError(lstSKUErr, sSKU, Resources.Resource.errCouldNotValidateOverQty);
                                }
                            }
                            else
                            {
                                isSKUinOrder = false;
                                AddError(lstSKUErr, sSKU, Resources.Resource.lblSKUNotFoundinOrder);
                                //MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblSKUNotFound);// 
                                //return;
                            }
                        }
                        if (isSKUinOrder == true)
                        {
                            int iStatus = this.SKUStatus(iShpQty, iOrdQty);
                            Product.ProductClothDesc objProductClothDesc = new Product.ProductClothDesc();
                            objProductClothDesc.ProductID = productID;
                            objProductClothDesc.getClothDesc(null, CurrentUser.UserID);

                            string pColor = "";
                            string pCollection = "";
                            string pStyle = BusinessUtility.GetString(objProductClothDesc.Style);

                            //Get Color Name
                            ProductColor objProductColor = new ProductColor();
                            objProductColor.ColorID = BusinessUtility.GetInt(objProductClothDesc.Color);
                            var lst = objProductColor.GetAllColorList(null, Globals.CurrentAppLanguageCode);
                            if (lst.Count > 0)
                            {
                                pColor = lst[0].ColorName;
                            }

                            // Get Size Name
                            string sSize = "";
                            ProductSize objProductSize = new ProductSize();
                            objProductSize.SizeID = BusinessUtility.GetInt(objProductClothDesc.Size);
                            var lstSize = objProductSize.GetAllSizeList(null, Globals.CurrentAppLanguageCode);
                            if (lstSize.Count > 0)
                            {
                                sSize = lstSize[0].SizeName;
                            }

                            // Get Collection Name
                            Collection objCollection = new Collection();
                            objCollection.CollectionID = BusinessUtility.GetInt(objProductClothDesc.Collection);
                            var lstCollection = objCollection.GetAllCollectionList(null, Globals.CurrentAppLanguageCode);
                            if (lstCollection.Count > 0)
                            {
                                pCollection = lstCollection[0].ShortName;
                            }

                            DataRow dRow = (DataRow)this.dtValidateOrder.NewRow();
                            dRow["ProductID"] = productID;
                            dRow["SKU"] = sSKU;
                            dRow["Collection"] = pCollection;
                            dRow["Style"] = pStyle;
                            dRow["Color"] = pColor;
                            dRow["Size"] = sSize;
                            dRow["ShpQty"] = iShpQty;
                            dRow["OrdQty"] = iOrdQty;
                            dRow["Status"] = iStatus;
                            dRow["Seq"] = 1;
                            this.dtValidateOrder.Rows.Add(dRow);
                            this.dtValidateOrder.AcceptChanges();
                        }
                    }
                }
                else
                {
                    //MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblInvalidSKU);
                    AddError(lstSKUErr, sSKU, Resources.Resource.lblInvalidSKU);
                }
            }

            if (lstSKUErr.Count > 0)
            {
                ShowErrorMsg(lstSKUErr);
            }
            ResetPage();
            txtSKU.Enabled = true;
        }
        catch (Exception ex)
        {
            txtSKU.Enabled = true;
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
            
        }
    }

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }

    public void AddError(List<SKUError> lstErro, string sSKU, string sErrorMsg)
    {
        if (SKUAddedinErro(lstErro, sSKU) == false)
        {
            lstErro.Add(new SKUError { SKUCode = sSKU, SKUErrMsg = sErrorMsg });
        }
    }

    public bool SKUAddedinErro(List<SKUError> lstErro, string sSKU)
    {
        bool isFind = false;
        foreach (SKUError skuerr in lstErro)
        {
            if (BusinessUtility.GetString(skuerr.SKUCode) == sSKU.Trim())
            {
                isFind = true;
            }
        }

        return isFind;
    }

    public void ShowErrorMsg(List<SKUError> lstErro)
    {
        StringBuilder sbError = new StringBuilder();
        foreach (SKUError skuErr in lstErro)
        {
            sbError.Append(skuErr.SKUCode + " " + skuErr.SKUErrMsg + "</br>");
        }
        if (BusinessUtility.GetString(sbError) != "")
        {
            MessageState.SetGlobalMessage(MessageType.Failure, BusinessUtility.GetString(sbError));
        }
    }
}

public class SKUError
{
    public string SKUCode { get; set; }
    public string SKUErrMsg { get; set; }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using System.Data;
using iTECH.Library.DataAccess.MySql;

public partial class Shipping_ViewShipping : BasePage
{
    private Orders objOrders = new Orders();
    private string _searchText, _whs, _opt;
    // On Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security 
        if (!CurrentUser.IsInRole(RoleID.SHIPPING_MANAGER) && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }
        _opt = BusinessUtility.GetString(Request.QueryString["opt"]);
        _searchText = BusinessUtility.GetString(Request.QueryString["srh"]);
        _whs = BusinessUtility.GetString(Request.QueryString["whs"]);

        if (!IsPagePostBack(jgrdSHP))
        {
            SysWarehouses wh = new SysWarehouses();
            wh.FillWharehouse(null, dlWarehouse, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));
            if (dlWarehouse.Items.Count > 1)
                dlWarehouse.Items.Insert(1, new ListItem(Resources.Resource.liAllWarehouse, "ALL"));
            else
                dlWarehouse.Items.Add(new ListItem(Resources.Resource.liAllWarehouse, "ALL"));

            FillSearchOptions();

            // Get User Grid Height
            InbizUser _usr = new InbizUser();
            _usr.PopulateObject(CurrentUser.UserID);
            if (BusinessUtility.GetInt(_usr.UserGridHeight) > 0)
            {
                jgrdSHP.Height = _usr.UserGridHeight;
            }
        }
    }

    //Populate Grid
    public void FillSearchOptions()
    {
        if (Request.QueryString.Count > 0)
        {
            try
            {
                dlSearch.SelectedValue = BusinessUtility.GetString(Request.QueryString["opt"]);
                dlWarehouse.SelectedValue = BusinessUtility.GetString(Request.QueryString["whs"]);
                txtSearch.Text = BusinessUtility.GetString(Request.QueryString["srh"]);
            }
            catch
            { }
        }
    }

    private void SetData(PurchaseOrders objPurOrd)
    {
        objPurOrd.PoAuthroisedByUserID = CurrentUser.UserID;
    }
    

    protected void btncmdBack_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("~/Shipping/Default.aspx");
    }


    protected void jgrdSHP_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 0)
        {
            if (string.IsNullOrEmpty(BusinessUtility.GetString(e.RowValues[0])))
            {
                e.CellHtml = "N/A";
            }
        }
        if (e.ColumnIndex == 1)
        {
            if (string.IsNullOrEmpty(BusinessUtility.GetString(e.RowValues[1])))
            {
                e.CellHtml = "N/A";
            }
        }
        if (e.ColumnIndex == 2)
        {
            if (string.IsNullOrEmpty(BusinessUtility.GetString(e.RowValues[2])))
            {
                e.CellHtml = "N/A";
            }
        }
        if (e.ColumnIndex == 3)
        {
            if (string.IsNullOrEmpty(BusinessUtility.GetString(e.RowValues[3])))
            {
                e.CellHtml = "N/A";
            }
        }
        if (e.ColumnIndex == 7)
        {
            string strData = "<div class='td_hack {0}'>{1}</div>";
            switch (e.RowValues[8].ToString())
            {
                case SOStatus.NEW:
                    strData = string.Format(strData, "", Resources.Resource.liGenerated);
                    break;
                case SOStatus.APPROVED:                    
                    strData = string.Format(strData, "", Resources.Resource.liApproved);
                    break;
                case SOStatus.IN_PROGRESS:                    
                    strData = string.Format(strData, "", Resources.Resource.liInProcess);
                    break;
                case SOStatus.HELD:                    
                    strData = string.Format(strData, "", Resources.Resource.liHeld);
                    break;
                case SOStatus.SHIPPING_HELD:                    
                    strData = string.Format(strData, "shippingHeld", Resources.Resource.liShippingHeld);
                    break;
                case SOStatus.SHIPPED:                    
                    strData = string.Format(strData, "", Resources.Resource.liShipped);
                    break;                
                case SOStatus.INVOICED:                    
                    strData = string.Format(strData, "", Resources.Resource.liInvoiced);
                    break;
                case SOStatus.CLOSED:                    
                    strData = string.Format(strData, "", Resources.Resource.liClosed);
                    break;
                case SOStatus.READY_FOR_SHIPPING:                    
                    strData = string.Format(strData, "", Resources.Resource.liReadyForShipping);
                    break;
            }
            e.CellHtml = strData;
            if (string.IsNullOrEmpty(BusinessUtility.GetString(strData)))
                e.CellHtml = "N/A";
        }

        if (e.ColumnIndex == 12)
        {
            e.CellHtml = string.Format("<a href='javascript:;' onclick='goToShipmentPage({0});'>{1}</a>", e.RowValues[0], "Detail");
        }
        if (e.ColumnIndex == 13)
        {
            e.CellHtml = string.Format("<input type='checkbox' onclick='checkBox({0},{1})' />", e.RowValues[0],e.RowKey);
           
        }
    }

    protected void jgrdSHP_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains("_history"))
        {
            sqldsSHP.SelectCommand = objOrders.GetShippingSQL(sqldsSHP.SelectParameters, Request.QueryString[dlSearch.ClientID], Request.QueryString[txtSearch.ClientID], Request.QueryString[dlWarehouse.ClientID]);
        }
        else
        {
            sqldsSHP.SelectCommand = objOrders.GetShippingSQL(sqldsSHP.SelectParameters, _opt, _searchText, _whs);
        }
    }
    protected void btnPrintPackingList_Click(object sender, EventArgs e)
    {
        //btnPrintPackageList.OnClientClick = string.Format("return openPDF('{0}?ReqID={1}&DocTyp=PKG')", ResolveUrl("~/Common/print.aspx"), this.OrderID);
        try
        {
            //Get List of orderids to print packing lists
            if (Session["LIST_ORDERS_TO_PRINT"] != null)
            {
                List<int> lstOrders = (List<int>)Session["LIST_ORDERS_TO_PRINT"];
                int idx = 0;
                string urlFormat = ResolveUrl("~/Common/Print.aspx") + "?ReqID={0}&DocTyp=PKG";
                string windowOpen = "window.open('{0}', '{1}{2}', 'height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');";
                string script = string.Empty;
                DateTime dtFlag = DateTime.Now;
                foreach (var item in lstOrders)
                {
                    script = string.Format(windowOpen, string.Format(urlFormat, item), "PackingList", idx);
                    ClientScript.RegisterStartupScript(this.GetType(), string.Format("PackingList{0}", idx), script, true);
                    idx++;                   
                }
            }
            else
            {

            }
        }
        catch
        {
            
        }
        finally
        { 

        }
    }
    protected void jgrdSHP_DataRequested(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestedEventArgs e)
    {
        //Store orders in session to print assembly
        List<int> lstOrders = new List<int>();
        int id = 0;
        foreach (DataRow item in e.DataTable.Rows)
        {
            id = BusinessUtility.GetInt(item["ordID"]);
            string statusToSkip = BusinessUtility.GetString(item["ordStatus"]);
            if (id > 0 && !lstOrders.Contains(id) && statusToSkip == SOStatus.READY_FOR_SHIPPING)
            {
                lstOrders.Add(id);
            }
        }
        Session["LIST_ORDERS_TO_PRINT"] = lstOrders;
    }
    protected void btnShipment_Command(object sender, EventArgs e)
    {

        DbHelper dbHelp = new DbHelper(true);
        Orders ord = new Orders();
        if (this.OrderIDs.Length > 0)
        {
            bool flag = false;
            DataTable dtOrders = ord.GetShippingOrderList(dbHelp, this.OrderIDs);
            if (dtOrders.Rows.Count > 1)
            {
                for (int i = 1; i < dtOrders.Rows.Count; i++)
                {
                    if ((dtOrders.Rows[i - 1][2].ToString() == dtOrders.Rows[i][2].ToString()) && (dtOrders.Rows[i - 1][3].ToString() == dtOrders.Rows[i][3].ToString()) && (dtOrders.Rows[i - 1][4].ToString() == dtOrders.Rows[i][4].ToString()) && (dtOrders.Rows[i - 1][5].ToString() == dtOrders.Rows[i][5].ToString()) && (dtOrders.Rows[i - 1][6].ToString() == dtOrders.Rows[i][6].ToString()) && (dtOrders.Rows[i - 1][7].ToString() == dtOrders.Rows[i][7].ToString()) && (dtOrders.Rows[i - 1][8].ToString() == dtOrders.Rows[i][8].ToString()) && (dtOrders.Rows[i - 1][1].ToString() == dtOrders.Rows[i][1].ToString()) && (dtOrders.Rows[i - 1][9].ToString() == dtOrders.Rows[i][9].ToString())) //&& 
                    {
                        flag = true;
                    }
                    else
                    {
                        flag = false;
                        break;
                    }
                }
            }
            else
            {
                if (dtOrders.Rows.Count == 1)
                {
                    flag = true;
                }
            }
            if (flag)
            {
                string[] oType = Array.ConvertAll<int, string>(this.OrderIDs, o => o.ToString());
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallShipmentpage", string.Format("LoadShipmentPopup('{0}');", string.Join(",", oType)), true);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblmsgShippingAddressNotValidate);
            }
        }
    }

    public int[] OrderIDs
    {
        get
        {
            string strArr = hdnPrintOrderIDs.Value;//Request.QueryString["oid"];
            if (!string.IsNullOrEmpty(strArr))
            {
                string[] arr = strArr.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                return Array.ConvertAll<string, int>(arr, a => BusinessUtility.GetInt(a));
            }
            return new List<int>().ToArray();
        }
    }
}
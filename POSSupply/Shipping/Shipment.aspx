﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="Shipment.aspx.cs" Inherits="Shipping_Shipment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <style type="text/css">
        input[type=submit]
        {
            height: 50px;
        }
        input[type=text], select
        {
            padding: 5px;
            font-size: 16px;
        }
        .rblst td input
        {
            margin: 0px;
            margin-right: 10px;
            vertical-align: middle;
        }
        .validate
        {
            background-color:Green;
            background-image:none;            
        }
        
        .inValidate
        {
            background-color:Red;
            background-image:none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <asp:Panel ID="pnlUpdate" runat="server" onkeypress="return disableEnterKey(event)">
        <asp:HiddenField ID="hdnErrDesc" runat="server" />
        <%--<div style="float: right;height:100px;">
        <table id="tblDownLoad" runat="server" visible="true">
            <tr>
                <td>
                    <asp:Button ID="btnDonwLoadDtlFile" runat="server" OnClick="btnDonwLoadDtlFile_OnClick"
                        Text="<%$Resources:Resource,lblDownLoadDetailLabel %>" />
                    <asp:HiddenField ID="hdnPkgDtlPdfFilePath" runat="server" />
                </td>
                <td>
                    <asp:Button ID="btnDonwLoadCmpltDtlFile" runat="server" OnClick="btnDonwLoadCmpltDtlFile_OnClick"
                        Text="<%$Resources:Resource,lblDownLoadCompleteDetailLabel %>" />
                    <asp:HiddenField ID="hdnPkgCmpltDtlPdfFilePath" runat="server" />
                </td>
            </tr>
        </table>
    </div>--%>
        <div style="padding: 5px;">
            <div style="text-align: right;">
                <table border="0" width="100%">
                    <tr>
                        <td align="right">
                            <table id="tblDownLoad" runat="server" border="0" visible="false">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnDonwLoadDtlFile" runat="server" OnClick="btnDonwLoadDtlFile_OnClick"
                                            UseSubmitBehavior="false" ValidationGroup="Download" Text="<%$Resources:Resource,lblDownLoadDetailLabel %>" />
                                        <asp:HiddenField ID="hdnPkgDtlPdfFilePath" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnDonwLoadCmpltDtlFile" runat="server" OnClick="btnDonwLoadCmpltDtlFile_OnClick"
                                            UseSubmitBehavior="false" ValidationGroup="Download" Text="<%$Resources:Resource,lblDownLoadCompleteDetailLabel %>" />
                                        <asp:HiddenField ID="hdnPkgCmpltDtlPdfFilePath" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
<%--            <h3>
                <asp:Label ID="Label1" Text="<%$Resources:Resource,lblUpdateShipment %>" runat="server" />
            </h3>--%>
            <table border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td style="font-weight: bold">
                        <asp:Label ID="Label2" Text="<%$Resources:Resource, lblOrderNumber%>" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="ltOrderNumber" Text="" runat="server" />
                    </td>
                    <td style="font-weight: bold">
                        <asp:Label ID="Label3" Text="<%$Resources:Resource, lblSuggestedShipmentDate%>" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="ltShipmentDate" Text="" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <asp:Label ID="Label4" Text="<%$Resources:Resource, lblCustomerID%>" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="ltCustomerID" Text="" runat="server" />
                    </td>
                    <td style="font-weight: bold">
                        <asp:Label ID="Label5" Text="<%$Resources:Resource, lblCustomerName%>" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="ltCustomerName" Text="" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <asp:Label ID="Label6" Text="<%$Resources:Resource, lblCreatedBy%>" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="ltOrderCreatedBy" Text="" runat="server" />
                    </td>
                    <td style="font-weight: bold">
                        <asp:Label ID="Label7" Text="<%$Resources:Resource, lblApprovedBy%>" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="ltOrderApproveBy" Text="" runat="server" />
                    </td>
                </tr>
                <tr valign="top">
                    <td style="font-weight: bold" rowspan="4">
                        <asp:Label ID="lblShippingAdd" Text="" runat="server" />
                    </td>
                    <td rowspan="4">
                        <asp:Literal ID="ltShippingAddress" Text="" runat="server" />
                    </td>
                    <td style="font-weight: bold">
                        <asp:Label ID="Label9" Text="<%$Resources:Resource, lblApprovedBy%>" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="Literal2" Text="" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <asp:Label ID="Label11" Text="<%$Resources:Resource, lblPhone%>" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="ltPhone" Text="" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <asp:Label ID="Label10" Text="<%$Resources:Resource, lblFax%>" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="ltFax" Text="" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <asp:Label ID="Label12" Text="<%$Resources:Resource, lblEmail%>" runat="server" />
                    </td>
                    <td>
                        <asp:Literal ID="ltEmail" Text="" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <asp:Label ID="lblShipmentPrice" AssociatedControlID="txtShipmentPrice" Text="<%$Resources:Resource, lblShipmentPrice%>"
                            runat="server" />
                    </td>
                    <td>
                        <%--Edited by mukesh 20130524--%>
                        <asp:TextBox ID="txtShipmentPrice" CssClass="numericTextField" runat="server" Width="100px" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="*" ControlToValidate="txtShipmentPrice"
                            runat="server" Display="Dynamic" />
                    </td>
                    <%--//Added by mukesh 20130524--%>
                    <td style="font-weight: bold">
                        <asp:Label ID="lblShipmentInternalPrice" AssociatedControlID="txtShipmentInternalPrice"
                            Text="<%$Resources:Resource, lblShipmentInternalPrice%>" runat="server" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtShipmentInternalPrice" CssClass="numericTextField" runat="server"
                            Width="100px" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="*" ControlToValidate="txtShipmentInternalPrice"
                            runat="server" Display="Dynamic" />
                    </td>
                    <%--//Added end--%>
                </tr>
                <tr>
                    <td style="font-weight: bold" colspan="4">
                        <asp:RadioButtonList ID="rblShippingCompany" CssClass="rblst" CellPadding="5" CellSpacing="5"
                            runat="server" RepeatDirection="Horizontal">
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please select shipping company!"
                            ControlToValidate="rblShippingCompany" runat="server" Display="Dynamic" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <asp:Label ID="Label14" Text="<%$Resources:Resource, lblShipmentTrackingNo%>" runat="server" />
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtShipmentTrackingNo" runat="server" Width="250px" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="*" ControlToValidate="txtShipmentTrackingNo"
                            runat="server" Display="Dynamic" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <asp:Label ID="Label15" Text="<%$Resources:Resource, lblShipmentDate%>" runat="server" />
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtShipmentDate" CssClass="datepicker" runat="server" ph="MM/DD/YYYY"
                            Width="100px" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="*" ControlToValidate="txtShipmentDate"
                            runat="server" Display="Dynamic" />
                    </td>
                </tr>
                <tr style="display:none;">
                    <td style="font-weight: bold">
                        <asp:Label ID="Label16" Text="<%$Resources:Resource, lblUpdateStatusToShipped%>"
                            runat="server" />
                    </td>
                    <td colspan="3">
                        <asp:CheckBox ID="chkUpdateStatusToShipped" Text="" runat="server" Checked="true" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold">
                        <asp:Label ID="Label13" Text="<%$Resources:Resource, lblEmailCustomer%>" runat="server" />
                    </td>
                    <td colspan="3">
                        <asp:CheckBox ID="chkEmailCustomer" Text="" runat="server" Checked="true" />
                    </td>
                </tr>
            </table>
            <div class="div_command">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td style="text-align: left;">
                            <asp:Button ID="btnHeld1" Text="<%$Resources:Resource,lblPutOnHold%>" runat="server"
                                CausesValidation="false" />
                        </td>
                        <td>
                            <%--<input type="button" value="validate Sku #" onclick="ShowValidateSKU()"/>--%>
                            <%--<asp:Button ID="btnValidateSKU" Text="<%$Resources:Resource,lblValidateSKU%>" OnClick="ShowValidateSKU()"
                                runat="server"  CausesValidation="false" />--%>
                            <asp:Button ID="btnValidateSKU" runat="server" Text="<%$Resources:Resource,lblValidateSKU%>"   />
                            <iCtrl:IframeDialog ID="mdbtnValidateSKU" TriggerControlID="btnValidateSKU" Width="780"
                                Height="450" Url="AddEditCurrency.aspx?jscallback=reloadGrid" Title="<%$Resources:Resource,lblValidateSKU%>"
                                runat="server">
                            </iCtrl:IframeDialog>
                            <asp:Button ID="btnPrintFeedExLabel" Text="<%$Resources:Resource,lblPrintFeedExLabel%>"
                                runat="server" OnClick="btnPrintFeedExLabel_Click" CausesValidation="false" />
                            <asp:Button ID="btnTicket" Text="<%$Resources:Resource,lblTicket%>" runat="server"
                                OnClick="btnTicket_Click" OnClientClick="" CausesValidation="false" />
                            <asp:Button Text="<%$Resources:Resource, lblPrint %>" ID="btnPrint" runat="server" OnClick="btnPrint_Click"
                                CausesValidation="False" OnClientClick="" />
                            <asp:Button ID="btnUpdate" Text="<%$Resources:Resource,lblUpdate%>" runat="server"
                                OnClick="btnUpdate_Click" />
                            <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
                                CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="reasonToHeld">
            <table border="0" cellpadding="3" cellspacing="3" style="width: 100%;">
                <tr>
                    <td style="text-align: right; font-size: 13px;">
                        <asp:Label ID="lblReasonToHeld" Text="<%$Resources:Resource,lblReasonToHeld %>" runat="server" />
                    </td>
                    <td style="font-size: 13px;">
                        <asp:DropDownList ID="ddlHeldReason" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr id="trOtherReason" style="display: none;">
                    <td>
                    </td>
                    <td style="font-size: 13px;">
                        <asp:TextBox ID="txtOtherReason" runat="server" />
                    </td>
                </tr>
            </table>
            <div class="div_command" style="margin-top: 5px;">
                <asp:Button ID="btnHeld" Text="<%$Resources:Resource,lblPutOnHold%>" runat="server"
                    CausesValidation="false" OnClick="btnHeld_Click" />
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $(".datepicker").addPlaceholder();
        $(".placeholder").addPlaceholder();

        $(document).ready(function () {
            $("#<%=lblShipmentPrice.ClientID%>").trigger("click");
            $("#<%=txtShipmentPrice.ClientID%>").focus(function () {
                $(this).select();
            });
        });

        function goToSalesEditPage() {
            parent.location.href = '<%=ResolveUrl(string.Format("~/Sales/ViewOrderDetails.aspx?SOID={0}", this.OrderID))%>';
            return false;
        }

        $("#reasonToHeld").dialog({
            modal: true,
            autoOpen: false,
            resizable: false,
            width: 500,
            height: 190,
            title: '<%=Resources.Resource.lblReasonToHeld%>'
        }).parent().appendTo(jQuery("form:first"));

        $("#<%=btnHeld1.ClientID%>").click(function () {
            $("#reasonToHeld").dialog("open");
            return false;
        });

        $("#<%=ddlHeldReason.ClientID%>").change(function () {
            if ($(this).val() == "4") {
                $("#trOtherReason").show();
                $("#<%=txtOtherReason.ClientID%>").focus();
            }
            else {
                $("#trOtherReason").hide();
            }
        });
        //Added By mukesh 20130429
        $("#<%=rblShippingCompany.ClientID%>").click(function () {
            $("#<%=txtShipmentTrackingNo.ClientID%>").focus();
        });
        //Added end


        function ValidateSKU(validSKU) {
            if (validSKU == "1") {
                $("#<%=btnValidateSKU.ClientID%>").addClass("validate");
            }
            else {
                $("#<%=btnValidateSKU.ClientID%>").addClass("inValidate");
            }
        }
    </script>
</asp:Content>

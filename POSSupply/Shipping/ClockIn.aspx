﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="ClockIn.aspx.cs" Inherits="Shipping_ClockIn" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphHead">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <%= Resources.Resource.lblView%>:
            <%= Resources.Resource.lblViewClockIn%></b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <asp:Button ID="AddClockIn" runat="server" Text="<%$ Resources:Resource, lblAddClockIn %>" />
        <iCtrl:IframeDialog ID="mdCreateCurrency" TriggerControlID="AddClockIn" Width="450"
            Height="320" Url="AddEditInOutTime.aspx" Title="<%$ Resources:Resource, lblAddEditInOutDateTime %>"
            runat="server" OnCloseClientFunction="reloadGrid">
        </iCtrl:IframeDialog>
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif"
            CssClass="csv_export" />
        <asp:LinkButton ID="lnkAddClockIn" runat="server" Visible="false"></asp:LinkButton>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        <%= Resources.Resource.lblSearchForm%></div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="Label3" CssClass="filter-key" AssociatedControlID="dlUser" runat="server"
                        Text="<%$ Resources:Resource, lblSearchUser %>"></asp:Label></h4>
                <div class="inner">
                    <asp:DropDownList ID="dlUser" runat="server" CssClass="filter-key">
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" CssClass="filter-key" AssociatedControlID="txtDate"
                        runat="server" Text="<%$ Resources:Resource, lblDate %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtDate" CssClass="datepicker"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntViewClockIn" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="jgdvClockIn" Height="300px" AutoWidth="True" OnCellBinding="jgdvClockIn_CellBinding"
                    OnDataRequesting="jgdvClockIn_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="idUsersClockIn" HeaderText="" PrimaryKey="True"
                            Visible="false" />
                        <trirand:JQGridColumn DataField="UserID" HeaderText="<%$ Resources:Resource, lblUserID %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="UserName" HeaderText="<%$ Resources:Resource, lblUserName %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="Location" HeaderText="<%$ Resources:Resource, lblLocation %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="In" HeaderText="<%$ Resources:Resource, lblIn %>" DataFormatString="{0:MM/dd/yyyy HH:mm}"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="Out" HeaderText="<%$ Resources:Resource, lblOut %>" DataFormatString="{0:MM/dd/yyyy HH:mm}"
                            Editable="false">
                          <%--  <Formatter>
                                <trirand:CustomFormatter FormatFunction="nullFormatter" />
                            </Formatter>--%>
                        </trirand:JQGridColumn>
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="idUsersClockIn"
                            Sortable="false" TextAlign="Center" Width="50" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="idUsersClockIn"
                            Sortable="false" TextAlign="Center" Width="50" Visible="false" />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents LoadComplete="jqGridResize" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sqldsTaxes" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
            </div>
            <iCtrl:IframeDialog ID="mdEditTax" Height="360" Width="490" Title="<%$ Resources:Resource, lblAddEditInOutDateTime %>"
                Dragable="true" TriggerSelectorClass="edit-Tax" TriggerSelectorMode="Class" OnCloseClientFunction="reloadGrid"
                UrlSelector="TriggerControlHrefAttribute" runat="server">
            </iCtrl:IframeDialog>
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
    <script type="text/javascript">
        $grid = $("#<%=jgdvClockIn.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=jgdvClockIn.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        function reloadGrid() {
            $("#<%=jgdvClockIn.ClientID%>").trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function deleteRow(rowID) {
            if (confirm("<%=Resources.Resource.msgDeleteCategory%>")) {
                var dataToPost = {};
                dataToPost.cmdDelete = "1"; //$grid.jqCustomSearch
                dataToPost.idUsersClockIn = rowID;
                $grid.jqCustomSearch(dataToPost);
            }
        }

        function redirectToPOS() {
            window.location.href = ('../POS/POS.aspx');
        }

        var nullFormatter = function (cellvalue, options, rowObject) {
            if (cellvalue === undefined || cellvalue === '') {
                cellvalue = '--';
            }
            return cellvalue;
        }
    </script>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="jobs.aspx.cs" Inherits="Shipping_jobs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <asp:Literal runat="server" ID="lblTitle"></asp:Literal>
        </b>
    </div>
    <div style="float: left;">
        <asp:Button ID="btnAssignJob" Text="Assign Task" runat="server" OnClientClick="return assignTask();" />  
    </div>
    <div style="float: right;">       
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <asp:Panel ID="pnlGridWrapper" runat="server" Width="100%">                        
                <trirand:JQGrid runat="server" ID="grdJobs" DataSourceID="sdsJobs"
                    Height="300px" AutoWidth="True" ondatarequesting="grdJobs_DataRequesting" 
                    MultiSelect="true" oncellbinding="grdJobs_CellBinding">
                    <Columns>
                        <trirand:JQGridColumn DataField="ordID" HeaderText="<%$ Resources:Resource, grdSOOrderNo %>"
                            PrimaryKey="True" />
                        <trirand:JQGridColumn DataField="PartnerAcronyme" HeaderText="<%$ Resources:Resource, grdCMAcronyme %>"
                            Editable="false" Visible="false" />
                        <trirand:JQGridColumn DataField="CustomerName" HeaderText="<%$ Resources:Resource, grdSOCustomerName %>"
                            Editable="false" />                        
                        <trirand:JQGridColumn DataField="PartnerPhone" HeaderText="<%$ Resources:Resource, grdCMPhone %>"
                            Editable="false" />                        
                        <trirand:JQGridColumn DataField="ordShpDate" HeaderText="<%$Resources:Resource,grdSOShippingDate%>" Editable="false" Visible="false" />                        
                        <trirand:JQGridColumn DataField="Jobduedate" HeaderText="<%$ Resources:Resource, grdDueDate %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="AssignedTo" HeaderText="<%$ Resources:Resource, grdAssignedTo %>"
                            Editable="false" />
                        <%--<trirand:JQGridColumn DataField="orderTypeDesc" HeaderText="<%$ Resources:Resource, grduserOderType %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="orderRejectReason" HeaderText="<%$ Resources:Resource, grdRejectedReason %>"
                            Editable="false" Visible="false"/>--%>
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="ordID"
                            Sortable="false" TextAlign="Center" Width="80" />
                    </Columns>                    
                    <ClientSideEvents LoadComplete="jqGridResize" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sdsJobs" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>            
            </asp:Panel>                       
            <br />
            <div class="div_command">                                
                <asp:Button ID="btnBack" Text="<%$Resources:Resource, lblGoBack%>" 
                    CausesValidation="false" runat="server" Visible="false" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntLeft" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" ID="pnlSearch">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        <asp:Literal ID="ltSearchFormTitle" Text="<%$Resources:Resource, lblSearchForm%>" runat="server" />
                     </div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lblCoCustomerName%>"
                        AssociatedControlID="txtCustomerName" CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                      <asp:TextBox runat="server" Width="180px" ID="txtCustomerName"></asp:TextBox>                    
                </div>
                <h4>
                    <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lblCoPhoneNo %>"
                        AssociatedControlID="txtCustPhoneNo" CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtCustPhoneNo"></asp:TextBox>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtAssign" runat="server" Text="<%$ Resources:Resource, lblColAssignedTo %>"
                        CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtAssign"></asp:TextBox>
                </div>
                <h4>
                    <asp:Label ID="Label1" AssociatedControlID="txtOrderNo" runat="server" Text="<%$ Resources:Resource, lblSOOrderNo %>"
                        CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtOrderNo"></asp:TextBox>
                </div>
                <h4>
                    <asp:Label ID="Label2" AssociatedControlID="txtFromDate" runat="server" Text="<%$ Resources:Resource, lblFromDueDate %>"
                        CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" CssClass="datepicker" Width="180px" ID="txtFromDate"></asp:TextBox>
                </div>
                <h4>
                    <asp:Label ID="Label3" AssociatedControlID="txtToDate" runat="server" Text="<%$ Resources:Resource, lblToDueDate %>"
                        CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" CssClass="datepicker" Width="180px" ID="txtToDate"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">                        
                        <asp:Button ID="btnSearch" Text="<%$Resources:Resource, Search%>" runat="server" CausesValidation="false" />                        
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="cphScripts" runat="Server">        
    <script type="text/javascript">
        $grid = $("#<%=grdJobs.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=pnlSearch.ClientID %>",
            searchButtonID: "<%=btnSearch.ClientID %>",
            gridWrapPanleID: "<%=pnlGridWrapper.ClientID %>"
        });

        function jqGridResize() {
            $grid.jqResizeAfterLoad("<%=pnlGridWrapper.ClientID%>", 0);
        }

        function reloadAfterAssing() {
            $grid.trigger("reloadGrid");
        }

        function assignTask() {
            var arrRooms = $grid.getGridParam('selarrrow');
            if (arrRooms.length <= 0) {
                alert("<%=Resources.Resource.msgJobSlectOneOrder%>");
                return false;
            }
            dataToPost = {}
            dataToPost.oid = arrRooms.join(",");
            dataToPost.jscallback = "reloadAfterAssing";
            var $dialog = jQuery.FrameDialog.create({ url: 'JobsAssign.aspx?' + $.param(dataToPost),
                title: "<%=Resources.Resource.cmdCssAssignJobs %>",
                loadingClass: "loading-image",
                modal: true,
                width: 850,
                height: 500,
                autoOpen: false
            });
            $dialog.dialog('open');

            return false;
        }                 

    </script>
</asp:Content>



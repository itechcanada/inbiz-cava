﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;
using System.Configuration;

using System.IO;

public partial class Shipping_Shipment : BasePage
{
    private string _shippingNotificationTemplateFile = "~/EmailTemplates/ShippingNotificationToCustomer.htm";

    protected void Page_Load(object sender, EventArgs e)
    {
        //Security
        if (!CurrentUser.IsInRole(RoleID.SHIPPING_MANAGER) && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Globals.RegisterParentRedirectPageUrl(this, ResolveUrl("~/Errors/AccessDenied.aspx"));
            return;
        }

        MessageState.SetGlobalMessage(MessageType.Success, "");
        if (!Page.IsPostBack)
        {
            FillFormData();
            string[] oids = this.OrderIDs.Split(',');
            int orderIDs = 0;
            if (oids.Length > 0)
            {
                orderIDs = BusinessUtility.GetInt(oids[0]);
            }

            if (orderIDs > 0)
            {
                if (oids.Length > 1)
                {
                    mdbtnValidateSKU.Url = string.Format("validateSKU.aspx?oid={0}&validationfor={1}", string.Join(",", oids), "shipping");
                    //mdbtnValidateSKU.Url = string.Format("validateSKU.aspx?oid={0}&validationfor={1}", orderIDs, "shipping");
                }
                else
                {
                    mdbtnValidateSKU.Url = string.Format("validateSKU.aspx?oid={0}&validationfor={1}", orderIDs, "shipping");
                }
            }
            else
            {
                mdbtnValidateSKU.Url = string.Format("validateSKU.aspx?oid={0}&validationfor={1}", this.OrderID, "shipping");
            }
            //Added by mukesh 20130612
            if (txtShipmentPrice.Text == "")
            {
                txtShipmentPrice.Focus();
            }
            else
            {
                txtShipmentInternalPrice.Focus();
            }
            //Added end
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        Session["validateorder"] = null;
        Orders ord = new Orders();
        ord.PopulateObject(this.OrderID);
        if (ord.OrderTypeCommission == 2)
        {
            lblShippingAdd.Text = Resources.Resource.lblShpToWarehouse;
        }
        else
        {
            lblShippingAdd.Text = Resources.Resource.titleShippingAddress;
        }
        string[] oids = this.OrderIDs.Split(',');
        int orderIDs = 0;
        if (oids.Length > 0)
        {
            orderIDs = BusinessUtility.GetInt(oids[0]);
        }

        if (orderIDs > 0)
        {
            lblShippingAdd.Text = Resources.Resource.lblShpToWarehouse;
        }
    }

    private void FillFormData()
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            dbHelp.OpenDatabaseConnection();
            //Fill Dropdown
            DropDownHelper.FillDropdown(dbHelp, ddlHeldReason, "SO", "dlShh", Globals.CurrentAppLanguageCode, null);
            int oid = new Orders().GetOrderIDToShip(dbHelp, this.OrderID);
            string[] oids = this.OrderIDs.Split(',');
            int orderIDs = 0;
            if (oids.Length > 0)
            {
                orderIDs = BusinessUtility.GetInt(oids[0]);
            }
            if (oid <= 0 && orderIDs <= 0)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.NoDataFound);
                Globals.RegisterCloseDialogScript(this, "parent.getGlobalMessage();", true);
                return;
            }

            SysShippingCompany shpComp = new SysShippingCompany();
            shpComp.PopulateListControl(dbHelp, rblShippingCompany, null);

            SysProcessGroup pg = new SysProcessGroup();
            pg.PopulateObject(dbHelp, BusinessUtility.GetInt(AppConfiguration.AppSettings[AppSettingKey.ShippingProcessID]));
            //txtShipmentPrice.Text = string.Format("{0:F}", pg.ProcessFixedCost);
            txtShipmentDate.Text = DateTime.Now.ToString("MM/dd/yyyy");

            if (orderIDs == 0)
            {
                orderIDs = this.OrderID;
            }
            if (orderIDs > 0)
            {
                Orders ord = new Orders();
                ord.PopulateObject(dbHelp, orderIDs);

                //Check if order has already a shipping process
                OrderItemProcess process = new OrderItemProcess();
                int ordProcessItemID = process.GetOrderProcessItemID(dbHelp, pg.ProcessCode, ord.OrdID);
                if (ordProcessItemID > 0) //No need to allow enter shipping price just set N/A and make field disabled
                {
                    txtShipmentPrice.Text = "N/A";
                    txtShipmentPrice.Enabled = false;
                    //Added by mukesh 20130524
                    txtShipmentInternalPrice.Text = process.GetOrderProcessInternalCost(dbHelp, pg.ProcessCode, ord.OrdID).ToString();
                    //Added end
                }

                Partners part = new Partners();
                part.PopulateObject(dbHelp, ord.OrdCustID);
                InbizUser userCreater = new InbizUser();
                InbizUser userApprover = new InbizUser();

                //Addresses shipToAddress = part.GetShipToAddress(dbHelp, ord.OrdCustID, Globals.GetPartnerTypeID(ord.OrdCustType));
                Addresses shipToAddress = new Addresses();
                //shipToAddress.PopulateObject(dbHelp, ord.InvCustID, ord.InvCustType, AddressType.SHIP_TO_ADDRESS);
                shipToAddress.GetOrderAddress(ord.OrdID, AddressType.SHIP_TO_ADDRESS);

                ltCustomerID.Text = part.PartnerAcronyme;
                ltCustomerName.Text = part.PartnerLongName;
                ltEmail.Text = part.PartnerEmail;
                ltFax.Text = part.PartnerFax;
                ltOrderApproveBy.Text = userApprover.GetUserName(dbHelp, ord.OrdVerifiedBy);
                ltOrderCreatedBy.Text = userCreater.GetUserName(dbHelp, ord.OrdCreatedBy);
                if (ord.OrderTypeCommission == 2)
                {
                    if (oids.Length > 1)
                    {
                        ltOrderNumber.Text = string.Join(",", oids) + " " + " <font color='red' style='font-weight:bold'>" + Resources.Resource.lbltransfer + "</font>";
                    }
                    else
                    {
                        ltOrderNumber.Text = orderIDs.ToString() + " " + " <font color='red' style='font-weight:bold'>" + Resources.Resource.lbltransfer + "</font>";
                    }
                }
                else
                {
                    if (oids.Length > 1)
                    {
                        ltOrderNumber.Text = string.Join(",", oids);
                    }
                    else
                    {
                        ltOrderNumber.Text = orderIDs.ToString();
                    }
                }
                ltPhone.Text = part.PartnerPhone;
                ltShipmentDate.Text = ord.OrdShpDate.ToString();

                if (ord.OrderTypeCommission == 2)
                {
                    SysWarehouses sWhs = new SysWarehouses();
                    sWhs.PopulateObject(ord.OrdShpToWhsCode, dbHelp);
                    ltShippingAddress.Text = sWhs.WarehouseAddressLine1 + " " + sWhs.WarehouseAddressLine2 + "</br>" + sWhs.WarehouseCity + " " + sWhs.WarehouseState + " " + sWhs.WarehouseCountry + "</br>" + sWhs.WarehousePostalCode + " " + sWhs.WarehouseFax + " " + sWhs.WarehousePhone;
                }
                else
                {
                    ltShippingAddress.Text = shipToAddress.ToHtml();
                }

                txtShipmentTrackingNo.Text = ord.OrdShpCode;
                //chkUpdateStatusToShipped.Checked = ord.OrdStatus == SOStatus.SHIPPED;
                
            }
            //else
            //{
            //    if (oids.Length > 0)
            //    {
            //        orderIDs = BusinessUtility.GetInt(oids[0]);
            //    }
            //}
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    protected int OrderID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["oid"], out id);
            return id;
        }
    }
    protected string OrderIDs
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["oids"]);
        }
    }
    protected void btnTicket_Click(object sender, EventArgs e)
    {
        string[] oids = this.OrderIDs.Split(',');
        int orderIDs = 0;
        if (oids.Length > 0)
        {
            orderIDs = BusinessUtility.GetInt(oids[0]);
        }
        if (orderIDs == 0)
        {
            orderIDs = this.OrderID;
        }
        if (oids.Length > 1)
        {
            string urlFormat = ResolveUrl("~/Common/Print.aspx") + "?ReqIDs={0}&DocTyp=TKT";
            string windowOpen = "window.open('{0}', '{1}', 'height=365,width=810,left=180,top=150,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');";
            string script = string.Empty;
                script = string.Format(windowOpen, string.Format(urlFormat, string.Join(",", oids)), "Ticket");
                ClientScript.RegisterStartupScript(this.GetType(), "PrintTicket", script, true);
        }
        else
        {
            string urlFormat = ResolveUrl("~/Common/Print.aspx") + "?ReqIDs={0}&DocTyp=TKT";
            string windowOpen = "window.open('{0}', '{1}', 'height=365,width=810,left=180,top=150,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');";
            string script = string.Empty;
            script = string.Format(windowOpen, string.Format(urlFormat, orderIDs), "Ticket");
            ClientScript.RegisterStartupScript(this.GetType(), "PrintTicket", script, true);
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Shipping/Default.aspx");
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                Orders ord = new Orders();
                string[] oids = this.OrderIDs.Split(',');
                int orderIDs = 0;
                if (oids.Length > 0)
                {
                    orderIDs = BusinessUtility.GetInt(oids[0]);
                }
                if (orderIDs == 0)
                {
                    orderIDs = this.OrderID;
                }
                if (oids.Length > 1)
                {
                    for (int i = 0; i < oids.Length; i++)
                    {
                        ord.PopulateObject(dbHelp, BusinessUtility.GetInt(oids[i]));
                        ord.OrdShpDate = BusinessUtility.GetDateTime(txtShipmentDate.Text, DateFormat.MMddyyyy);
                        ord.OrdShpCode = string.Format("{0}-{1}", rblShippingCompany.SelectedItem.Text, txtShipmentTrackingNo.Text);
                        if (ord.OrderTypeCommission == 2)
                        {
                            ord.OrdStatus = SOStatus.TRANSFERRED;
                        }
                        else
                        {
                            if (chkUpdateStatusToShipped.Checked)
                            {
                                ord.OrdStatus = SOStatus.SHIPPED;
                            }
                        }
                        ord.Update(dbHelp, CurrentUser.UserID);

                        //Add shipping process item to the order
                        SysProcessGroup pg = new SysProcessGroup();
                        pg.PopulateObject(dbHelp, BusinessUtility.GetInt(AppConfiguration.AppSettings[AppSettingKey.ShippingProcessID]));

                        //Populate Order Customer Info to set tax code for shipping process
                        Partners part = new Partners();
                        part.PopulateObject(dbHelp, ord.OrdCustID);
                        Addresses shpAddr = part.GetShipToAddress(dbHelp, part.PartnerID, part.PartnerType);
                        CountryStateTaxGroup tx = new CountryStateTaxGroup();
                        tx.PopulateObject(dbHelp, shpAddr.AddressCountry, shpAddr.AddressState);
                        OrderItemProcess process = new OrderItemProcess();
                        InvoiceItemProcess invProcess = new InvoiceItemProcess();
                        Invoice inv = new Invoice();
                        double fixedCost = BusinessUtility.GetDouble(txtShipmentPrice.Text) / oids.Length;
                        double internalCost = BusinessUtility.GetDouble(txtShipmentInternalPrice.Text) / oids.Length; //Added by mukesh 20130524
                        //Check if order already exists shipping process
                        int ordProcessItemID = process.GetOrderProcessItemID(dbHelp, pg.ProcessCode, ord.OrdID);
                        if (ordProcessItemID <= 0)
                        {
                            SysWarehouses sWhs = new SysWarehouses();
                            sWhs.PopulateObject(ord.OrdShpWhsCode, dbHelp);

                            process.OrdID = ord.OrdID;
                            process.OrdItemProcCode = pg.ProcessCode;
                            process.OrdItemProcFixedPrice = fixedCost;
                            process.OrdItemProcHours = 0;
                            process.OrdItemProcPricePerHour = 0;
                            process.OrdItemProcPricePerUnit = 0;
                            process.OrdItemProcUnits = 0;
                            process.SysTaxCodeDescID = sWhs.WarehouseRegTaxCode;
                            process.OrdItemProcInternalCost = internalCost; //Added by mukesh 20130524
                            process.Insert(dbHelp);
                        }
                        else
                        {
                            //Need to update internal cost because it is not hidden.
                            process.UpdateOrderProcessInternalCost(dbHelp, pg.ProcessCode, ord.OrdID, internalCost);
                        }
                        //check invoice exist
                        if (ord.OrderTypeCommission != (int)OrderCommission.Transfer)
                        {
                            if (!inv.IsInvoiceExist(ord.OrdID, InvoiceReferenceType.INVOICE))
                            {
                                bool isDistinctInvoice = ConfigurationManager.AppSettings["SeparateInvID"].ToLower() == "yes";
                                inv.PopulateObjectByOrder(dbHelp, ord.OrdID, InvoicesStatus.SUBMITTED, InvoiceReferenceType.INVOICE, CurrentUser.UserID);
                                inv.Insert(dbHelp, CurrentUser.UserID);
                                if (inv.InvID > 0)
                                {
                                    if (inv.InvRefNo == 0)
                                    {
                                        inv.UpdateInvoiceRefNo(dbHelp, inv.InvID, inv.InvCompanyID, inv.InvRefType, isDistinctInvoice);
                                    }
                                    InvoiceItems invItems = new InvoiceItems();
                                    invItems.AddInvoiceItemsFromOrder(dbHelp, ord.OrdID, inv.InvID);

                                    //Update account receivable
                                    PreAccountRcv arc = new PreAccountRcv();
                                    arc.MoveToActualAR(dbHelp, CurrentUser.UserID, ord.OrdID, inv.InvID);

                                    //Once Invoice generated successfully just make a entry in sls Analysis table 
                                    new SlsAnalysis().RecordOnInvoiceGeneration(dbHelp, inv.InvID);
                                }
                            }
                        }
                        //If Invoice was generated then need process item to added in invoice also                     
                        int invID = inv.GetInvoiceID(dbHelp, ord.OrdID);
                        if (invID > 0)
                        {
                            int invProcessItemID = invProcess.GetInvoiceProcessItemID(dbHelp, pg.ProcessCode, invID);
                            if (invProcessItemID <= 0)
                            {
                                invProcess.InvItemProcCode = pg.ProcessCode;
                                invProcess.InvItemProcFixedPrice = fixedCost;
                                invProcess.InvItemProcHours = 0;
                                invProcess.InvItemProcPricePerHour = 0;
                                invProcess.InvItemProcPricePerUnit = 0;
                                invProcess.InvItemProcUnits = 0;
                                invProcess.Invoices_invID = invID;
                                invProcess.SysTaxCodeDescID = 0;
                                invProcess.InvItemProcInternalCost = internalCost; //Added by mukesh 20130524
                                invProcess.Insert(dbHelp);
                            }
                            else
                            {
                                invProcess.UpdateInvoiceProcessInternalCost(dbHelp, pg.ProcessCode, invProcessItemID, internalCost);
                            }
                        }
                        //Adjust Products quantity in Inventory
                        //Check if default warehouse are associated with any other shp whs
                        SysWarehouses whs = new SysWarehouses();
                        whs.PopulateObject(ord.OrdShpWhsCode, dbHelp);
                        if (!string.IsNullOrEmpty(whs.ShippingWarehouse))
                        {
                            ord.UpdateInventoryOnShipping(dbHelp, BusinessUtility.GetInt(oids[i]), whs.ShippingWarehouse, BusinessUtility.GetString(InvMovmentSrc.SHP), CurrentUser.UserID);
                        }
                        else
                        {
                            ord.UpdateInventoryOnShipping(dbHelp, BusinessUtility.GetInt(oids[i]), ord.OrdShpWhsCode, BusinessUtility.GetString(InvMovmentSrc.SHP), CurrentUser.UserID);
                        }
                        //Send an Email to Customer if checkbox checked
                        if (chkEmailCustomer.Checked)
                        {
                            //To Do to send replace & format email template & email to customer
                            string emailTemp = File.ReadAllText(Server.MapPath(_shippingNotificationTemplateFile));
                            int sIdx, eIdx;
                            sIdx = emailTemp.IndexOf("@START_PRODUCT_DESCRIPTION_TEMP@");
                            eIdx = emailTemp.IndexOf("@END_PRODUCT_DESCRIPTION_TEMP@");
                            string itemListTemp = string.Empty;
                            if (sIdx > -1 && eIdx > -1)
                            {
                                itemListTemp = emailTemp.Substring(sIdx, eIdx - sIdx).Replace("@START_PRODUCT_DESCRIPTION_TEMP@", "");
                            }

                            emailTemp = emailTemp.Replace("#CUSTOMER_CONTACT_NAME#", !string.IsNullOrEmpty(part.ExtendedProperties.EmergencyContactName) ? part.ExtendedProperties.EmergencyContactName : part.PartnerLongName);
                            emailTemp = emailTemp.Replace("#COMPANY_NAME#", whs.WarehouseDescription);
                            emailTemp = emailTemp.Replace("#SHIPPING_CARRIER#", rblShippingCompany.SelectedItem.Text);
                            emailTemp = emailTemp.Replace("#SHIP_DATE#", ord.OrdShpDate.ToString("MMM dd, yyyy"));
                            emailTemp = emailTemp.Replace("#SHIPPING_TRACKING_NO#", txtShipmentTrackingNo.Text);
                            emailTemp = emailTemp.Replace("#SHIPPING_ADDRESS#", CommonPrint.GetAddress(shpAddr));
                            emailTemp = emailTemp.Replace("#COMPANY_WEBSITE#", part.PartnerWebsite);
                            //emailTemp = emailTemp.Replace("#COMPANY_EMAIL#", whs.WarehouseEmailID);
                            //emailTemp = emailTemp.Replace("#COMPANY_PHONE#", whs.WarehousePhone);
                            emailTemp = emailTemp.Replace("#ORDER_NUMBER#", ord.OrdID.ToString());

                            SysCompanyInfo objCompanyInfo = new SysCompanyInfo();
                            objCompanyInfo.PopulateObject(whs.WarehouseCompanyID);
                            emailTemp = emailTemp.Replace("#COMPANY_EMAIL#", objCompanyInfo.CompanyEmail);
                            emailTemp = emailTemp.Replace("#COMPANY_PHONE#", objCompanyInfo.CompanyPhoneno);

                            OrderItems ordItem = new OrderItems();
                            var lst = ordItem.GetOrderItemList(dbHelp, ord.OrdID);
                            string itemList = string.Empty;
                            foreach (var item in lst)
                            {
                                itemList += itemListTemp.Replace("#PRODUCT_DESCRIPTION#", item.OrderItemDesc);
                            }
                            emailTemp = emailTemp.Replace("#PRODUCT_DESCRIPTION_LIST#", itemList);
                            if (ord.OrderTypeCommission == 2)
                            {

                                Partners partformail = new Partners();
                                partformail.PopulateObject(dbHelp, ord.OrdLastUpdateBy);

                                if (!string.IsNullOrEmpty(partformail.PartnerEmail))
                                {
                                    EmailHelper.SendEmail(AppConfiguration.AppSettings[AppSettingKey.DefaultEmailSender], partformail.PartnerEmail, emailTemp, "Shipping", true);
                                }

                                Partners partformailSR = new Partners();
                                partformailSR.PopulateObject(dbHelp, ord.OrdSalesRepID);
                                if (!string.IsNullOrEmpty(partformailSR.PartnerEmail))
                                {
                                    EmailHelper.SendEmail(AppConfiguration.AppSettings[AppSettingKey.DefaultEmailSender], partformailSR.PartnerEmail, emailTemp, "Shipping", true);
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(part.PartnerEmail))
                                {
                                    EmailHelper.SendEmail(AppConfiguration.AppSettings[AppSettingKey.DefaultEmailSender], part.PartnerEmail, emailTemp, "Shipping", true);
                                }
                            }
                        }
                        //To Do to Add Action Log            
                        new OrderActionLog().AddLog(dbHelp, CurrentUser.UserID, ord.OrdID, "Order Shipped", string.Empty);
                    }

                    ord.SaveShipmentGroup(this.OrderIDs);

                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgOrderSavedSuccessfully);
                    //Response.Redirect("Default.aspx", false);
                    if (this.JsCallBack != string.Empty)
                    {
                        Globals.RegisterCloseDialogScript(this, string.Format("parent.{0}(); parent.getGlobalMessage();", this.JsCallBack), true);
                    }
                    else
                    {
                        Globals.RegisterCloseDialogScript(this, "parent.getGlobalMessage();", true);
                    }
                }
                else
                {
                    ord.PopulateObject(dbHelp, orderIDs);
                    ord.OrdShpDate = BusinessUtility.GetDateTime(txtShipmentDate.Text, DateFormat.MMddyyyy);
                    ord.OrdShpCode = string.Format("{0}-{1}", rblShippingCompany.SelectedItem.Text, txtShipmentTrackingNo.Text);

                    if (ord.OrderTypeCommission == 2)
                    {
                        ord.OrdStatus = SOStatus.TRANSFERRED;
                    }
                    else
                    {
                        if (chkUpdateStatusToShipped.Checked)
                        {
                            ord.OrdStatus = SOStatus.SHIPPED;
                        }
                    }

                    ord.Update(dbHelp, CurrentUser.UserID);

                    //Add shipping process item to the order
                    SysProcessGroup pg = new SysProcessGroup();
                    pg.PopulateObject(dbHelp, BusinessUtility.GetInt(AppConfiguration.AppSettings[AppSettingKey.ShippingProcessID]));

                    //Populate Order Customer Info to set tax code for shipping process
                    Partners part = new Partners();
                    part.PopulateObject(dbHelp, ord.OrdCustID);
                    Addresses shpAddr = part.GetShipToAddress(dbHelp, part.PartnerID, part.PartnerType);

                    CountryStateTaxGroup tx = new CountryStateTaxGroup();
                    tx.PopulateObject(dbHelp, shpAddr.AddressCountry, shpAddr.AddressState);

                    OrderItemProcess process = new OrderItemProcess();
                    InvoiceItemProcess invProcess = new InvoiceItemProcess();
                    Invoice inv = new Invoice();
                    double fixedCost = BusinessUtility.GetDouble(txtShipmentPrice.Text);
                    double internalCost = BusinessUtility.GetDouble(txtShipmentInternalPrice.Text); //Added by mukesh 20130524
                    //Check if order already exists shipping process
                    int ordProcessItemID = process.GetOrderProcessItemID(dbHelp, pg.ProcessCode, ord.OrdID);
                    if (ordProcessItemID <= 0)
                    {
                        SysWarehouses sWhs = new SysWarehouses();
                        sWhs.PopulateObject(ord.OrdShpWhsCode, dbHelp);

                        process.OrdID = ord.OrdID;
                        process.OrdItemProcCode = pg.ProcessCode;
                        process.OrdItemProcFixedPrice = fixedCost;
                        process.OrdItemProcHours = 0;
                        process.OrdItemProcPricePerHour = 0;
                        process.OrdItemProcPricePerUnit = 0;
                        process.OrdItemProcUnits = 0;
                        process.SysTaxCodeDescID = sWhs.WarehouseRegTaxCode;
                        process.OrdItemProcInternalCost = internalCost; //Added by mukesh 20130524
                        process.Insert(dbHelp);
                    }
                    else
                    {
                        //Need to update process cost
                        //process.UpdateProcessFixedCost(dbHelp, fixedCost, ordProcessItemID); //2012-12-19 No need to allow update shipping price from this screen
                        //Added by mukesh 20130606
                        //Need to update internal cost because it is not hidden.
                        process.UpdateOrderProcessInternalCost(dbHelp, pg.ProcessCode, ord.OrdID, internalCost);
                        //Added end
                    }
                    //check invoice exist

                    if (ord.OrderTypeCommission != (int)OrderCommission.Transfer)
                    {
                        if (!inv.IsInvoiceExist(ord.OrdID, InvoiceReferenceType.INVOICE))
                        {
                            bool isDistinctInvoice = ConfigurationManager.AppSettings["SeparateInvID"].ToLower() == "yes";
                            inv.PopulateObjectByOrder(dbHelp, ord.OrdID, InvoicesStatus.SUBMITTED, InvoiceReferenceType.INVOICE, CurrentUser.UserID);
                            inv.Insert(dbHelp, CurrentUser.UserID);
                            if (inv.InvID > 0)
                            {
                                if (inv.InvRefNo == 0)
                                {
                                    inv.UpdateInvoiceRefNo(dbHelp, inv.InvID, inv.InvCompanyID, inv.InvRefType, isDistinctInvoice);
                                }
                                InvoiceItems invItems = new InvoiceItems();
                                invItems.AddInvoiceItemsFromOrder(dbHelp, ord.OrdID, inv.InvID);

                                //Update account receivable
                                PreAccountRcv arc = new PreAccountRcv();
                                arc.MoveToActualAR(dbHelp, CurrentUser.UserID, ord.OrdID, inv.InvID);

                                //Once Invoice generated successfully just make a entry in sls Analysis table 
                                new SlsAnalysis().RecordOnInvoiceGeneration(dbHelp, inv.InvID);
                            }
                        }
                    }
                    //If Invoice was generated then need process item to added in invoice also                     
                    int invID = inv.GetInvoiceID(dbHelp, ord.OrdID);
                    if (invID > 0)
                    {
                        int invProcessItemID = invProcess.GetInvoiceProcessItemID(dbHelp, pg.ProcessCode, invID);
                        if (invProcessItemID <= 0)
                        {
                            invProcess.InvItemProcCode = pg.ProcessCode;
                            invProcess.InvItemProcFixedPrice = fixedCost;
                            invProcess.InvItemProcHours = 0;
                            invProcess.InvItemProcPricePerHour = 0;
                            invProcess.InvItemProcPricePerUnit = 0;
                            invProcess.InvItemProcUnits = 0;
                            invProcess.Invoices_invID = invID;
                            invProcess.SysTaxCodeDescID = 0;
                            invProcess.InvItemProcInternalCost = internalCost; //Added by mukesh 20130524
                            invProcess.Insert(dbHelp);
                        }
                        else
                        {
                            //2012-12-19 If we are not update shipping process if it already exsits then no need to update invoice process item as well
                            //invProcess.UpdateProcessFixedCost(dbHelp, fixedCost, invProcessItemID);
                            //Added by mukesh 20130606
                            invProcess.UpdateInvoiceProcessInternalCost(dbHelp, pg.ProcessCode, invProcessItemID, internalCost);
                            //Added end
                        }
                    }

                    //Adjust Products quantity in Inventory

                    //Check if default warehouse are associated with any other shp whs
                    SysWarehouses whs = new SysWarehouses();
                    whs.PopulateObject(ord.OrdShpWhsCode, dbHelp);
                    if (!string.IsNullOrEmpty(whs.ShippingWarehouse))
                    {
                        ord.UpdateInventoryOnShipping(dbHelp, orderIDs, whs.ShippingWarehouse, BusinessUtility.GetString(InvMovmentSrc.SHP), CurrentUser.UserID);
                    }
                    else
                    {
                        ord.UpdateInventoryOnShipping(dbHelp, orderIDs, ord.OrdShpWhsCode, BusinessUtility.GetString(InvMovmentSrc.SHP), CurrentUser.UserID);
                    }

                    //Send an Email to Customer if checkbox checked
                    if (chkEmailCustomer.Checked)
                    {
                        //To Do to send replace & format email template & email to customer
                        string emailTemp = File.ReadAllText(Server.MapPath(_shippingNotificationTemplateFile));
                        int sIdx, eIdx;
                        sIdx = emailTemp.IndexOf("@START_PRODUCT_DESCRIPTION_TEMP@");
                        eIdx = emailTemp.IndexOf("@END_PRODUCT_DESCRIPTION_TEMP@");
                        string itemListTemp = string.Empty;
                        if (sIdx > -1 && eIdx > -1)
                        {
                            itemListTemp = emailTemp.Substring(sIdx, eIdx - sIdx).Replace("@START_PRODUCT_DESCRIPTION_TEMP@", "");
                        }

                        emailTemp = emailTemp.Replace("#CUSTOMER_CONTACT_NAME#", !string.IsNullOrEmpty(part.ExtendedProperties.EmergencyContactName) ? part.ExtendedProperties.EmergencyContactName : part.PartnerLongName);
                        emailTemp = emailTemp.Replace("#COMPANY_NAME#", whs.WarehouseDescription);
                        emailTemp = emailTemp.Replace("#SHIPPING_CARRIER#", rblShippingCompany.SelectedItem.Text);
                        emailTemp = emailTemp.Replace("#SHIP_DATE#", ord.OrdShpDate.ToString("MMM dd, yyyy"));
                        emailTemp = emailTemp.Replace("#SHIPPING_TRACKING_NO#", txtShipmentTrackingNo.Text);
                        emailTemp = emailTemp.Replace("#SHIPPING_ADDRESS#", CommonPrint.GetAddress(shpAddr));
                        emailTemp = emailTemp.Replace("#COMPANY_WEBSITE#", part.PartnerWebsite);
                        //emailTemp = emailTemp.Replace("#COMPANY_EMAIL#", whs.WarehouseEmailID);
                        //emailTemp = emailTemp.Replace("#COMPANY_PHONE#", whs.WarehousePhone);
                        emailTemp = emailTemp.Replace("#ORDER_NUMBER#", ord.OrdID.ToString());

                        SysCompanyInfo objCompanyInfo = new SysCompanyInfo();
                        objCompanyInfo.PopulateObject(whs.WarehouseCompanyID);
                        emailTemp = emailTemp.Replace("#COMPANY_EMAIL#", objCompanyInfo.CompanyEmail);
                        emailTemp = emailTemp.Replace("#COMPANY_PHONE#", objCompanyInfo.CompanyPhoneno);

                        OrderItems ordItem = new OrderItems();
                        var lst = ordItem.GetOrderItemList(dbHelp, ord.OrdID);
                        string itemList = string.Empty;
                        foreach (var item in lst)
                        {
                            itemList += itemListTemp.Replace("#PRODUCT_DESCRIPTION#", item.OrderItemDesc);
                        }
                        emailTemp = emailTemp.Replace("#PRODUCT_DESCRIPTION_LIST#", itemList);
                        if (ord.OrderTypeCommission == 2)
                        {

                            Partners partformail = new Partners();
                            partformail.PopulateObject(dbHelp, ord.OrdLastUpdateBy);

                            if (!string.IsNullOrEmpty(partformail.PartnerEmail))
                            {
                                EmailHelper.SendEmail(AppConfiguration.AppSettings[AppSettingKey.DefaultEmailSender], partformail.PartnerEmail, emailTemp, "Shipping", true);
                            }

                            Partners partformailSR = new Partners();
                            partformailSR.PopulateObject(dbHelp, ord.OrdSalesRepID);
                            if (!string.IsNullOrEmpty(partformailSR.PartnerEmail))
                            {
                                EmailHelper.SendEmail(AppConfiguration.AppSettings[AppSettingKey.DefaultEmailSender], partformailSR.PartnerEmail, emailTemp, "Shipping", true);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(part.PartnerEmail))
                            {
                                EmailHelper.SendEmail(AppConfiguration.AppSettings[AppSettingKey.DefaultEmailSender], part.PartnerEmail, emailTemp, "Shipping", true);
                            }
                        }
                    }

                    ord.SaveShipmentGroup(BusinessUtility.GetString( orderIDs));

                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgOrderSavedSuccessfully);
                    //Response.Redirect("Default.aspx", false);
                    if (this.JsCallBack != string.Empty)
                    {
                        Globals.RegisterCloseDialogScript(this, string.Format("parent.{0}(); parent.getGlobalMessage();", this.JsCallBack), true);
                    }
                    else
                    {
                        Globals.RegisterCloseDialogScript(this, "parent.getGlobalMessage();", true);
                    }

                    //To Do to Add Action Log            
                    new OrderActionLog().AddLog(dbHelp, CurrentUser.UserID, ord.OrdID, "Order Shipped", string.Empty);
                }
            }
            catch
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    private string JsCallBack
    {
        get
        {
            return !string.IsNullOrEmpty(Request.QueryString["callBack"]) ? Request.QueryString["callBack"] : string.Empty;
        }
    }

    protected void btnHeld_Click(object sender, EventArgs e)
    {
        string[] oids = this.OrderIDs.Split(',');
        int orderIDs = 0;
        if (oids.Length > 0)
        {
            orderIDs = BusinessUtility.GetInt(oids[0]);
        }
        if (orderIDs == 0)
        {
            orderIDs = this.OrderID;
        }

        if (orderIDs <= 0)
        {
            return;
        }
        //Set Order status help
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            if (oids.Length > 1)
            {
                Orders ord = new Orders();
                for (int i = 0; i < oids.Length; i++)
                {
                    ord.UpdateOrderStatus(dbHelp, BusinessUtility.GetInt(oids[i]), SOStatus.SHIPPING_HELD);

                    //Set Reason to held
                    int heldReasonCode = BusinessUtility.GetInt(ddlHeldReason.SelectedValue);
                    string reason = heldReasonCode == (int)OrderHeldReasonCode.Other ? txtOtherReason.Text : string.Empty;
                    ord.SetReasontoPutOnHold(dbHelp, BusinessUtility.GetInt(oids[i]), (OrderHeldReasonCode)heldReasonCode, reason);
                }
            }
            else
            {
                Orders ord = new Orders();
                ord.UpdateOrderStatus(dbHelp, orderIDs, SOStatus.SHIPPING_HELD);

                //Set Reason to held
                int heldReasonCode = BusinessUtility.GetInt(ddlHeldReason.SelectedValue);
                string reason = heldReasonCode == (int)OrderHeldReasonCode.Other ? txtOtherReason.Text : string.Empty;
                ord.SetReasontoPutOnHold(dbHelp, orderIDs, (OrderHeldReasonCode)heldReasonCode, reason);
            }
            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgOrderSavedSuccessfully);
            if (this.JsCallBack != string.Empty)
            {
                Globals.RegisterCloseDialogScript(this, string.Format("parent.{0}(); parent.getGlobalMessage();", this.JsCallBack), true);
            }
            else
            {
                Globals.RegisterCloseDialogScript(this, "parent.getGlobalMessage();", true);
            }
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    protected void btnPrintFeedExLabel_Click(object sender, EventArgs e)
    {
        //Page page = (Page)HttpContext.Current.Handler;
        //string target = "_blank";
        //string windowFeatures = "";
        //string url = "PrintFeedExLabel.aspx?oid=" + BusinessUtility.GetString(this.OrderID);
        //if (page == null)
        //{
        //    throw new InvalidOperationException("Error redirecting, please try again.");
        //}
        //url = page.ResolveClientUrl(url);
        //string script;
        //if (!String.IsNullOrEmpty(windowFeatures))
        //{
        //    script = @"window.open(""{0}"", ""{1}"", ""{2}"");";
        //}
        //else
        //{
        //    script = @"window.open(""{0}"", ""{1}"");";
        //}
        //script = String.Format(script, url, target, windowFeatures);
        //ScriptManager.RegisterStartupScript(page, typeof(Page), "Redirect", script, true);

        PrintFeedExLabel(this.OrderID);
    }

    protected void btnDonwLoadDtlFile_OnClick(object sender, EventArgs e)
    {
        //DownloadFile("Inv_2013121013451.csv");
        DownloadFile(hdnPkgDtlPdfFilePath.Value);
    }

    protected void btnDonwLoadCmpltDtlFile_OnClick(object sender, EventArgs e)
    {
        DownloadFile(hdnPkgCmpltDtlPdfFilePath.Value);
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        string[] oids = this.OrderIDs.Split(',');
        int orderIDs = 0;
        if (oids.Length > 0)
        {
            orderIDs = BusinessUtility.GetInt(oids[0]);
        }
        if (orderIDs == 0)
        {
            orderIDs = this.OrderID;
        }
        if (oids.Length > 1)
        {
            //int idx = 0;
            string urlFormat = ResolveUrl("~/Common/Print.aspx") + "?ReqID={0}&DocTyp=PKG";
            string windowOpen = "window.open('{0}', '{1}{2}', 'height=365,width=810,left=180,top=150,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');";
            string script = string.Empty;
            for (int i = 0; i < oids.Length; i++)
            {
                script = string.Format(windowOpen, string.Format(urlFormat, BusinessUtility.GetInt(oids[i])), "Document", i);
                ClientScript.RegisterStartupScript(this.GetType(), string.Format("PrintOrderList{0}", i), script, true);
            }

            //btnPrint.OnClientClick = string.Format("return openPDF('{0}?ReqID={1}&DocTyp=PKG')", ResolveUrl("~/Common/print.aspx"), string.Join(",", oids));
        }
        else
        {
            //btnPrint.OnClientClick = string.Format("return printToPdf('{0}')", orderIDs);
            btnPrint.OnClientClick = string.Format("return openPDF('{0}?ReqID={1}&DocTyp=PKG')", ResolveUrl("~/Common/print.aspx"), orderIDs);
        }
    }
    protected void DownloadFile(string name)
    {
        try
        {
            if (name == "")
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblDownloadFileNotFound);
                return;
            }

            string _fileName;
            //string _path = Request.PhysicalApplicationPath + "Upload/FeedExLabel/" + name;
            string _path = name;
            System.IO.FileInfo _file = new System.IO.FileInfo(_path);
            if (_file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + _file.Name);
                Response.AddHeader("Content-Length", _file.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(_file.FullName);
                Response.End();

                /* Dodnload PDF
                StringBuilder sbSavePath = new StringBuilder();
                sbSavePath.Append(DateTime.Now.Day);
                sbSavePath.Append("-");
                sbSavePath.Append(DateTime.Now.Month);
                sbSavePath.Append("-");
                sbSavePath.Append(DateTime.Now.Year);

                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpResponse objResponce = context.Response;
                String test = HttpContext.Current.Request.QueryString["ReportName"];
                HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + test);
                objResponce.WriteFile(context.Server.MapPath(@"Reports\" + sbSavePath + @"\" + test));
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.End();*/
            }
            else
            {
                ClientScript.RegisterStartupScript(Type.GetType("System.String"), "messagebox", "<script type=\"text/javascript\">alert('File not Found');</script>");
            }
        }
        catch
        {
            //MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblDownLoadFailure);
        }
    }

    protected void PrintFeedExLabel(int OrderID)
    {
        try
        {
            //if (!IsPostBack)
            {
                PrintFedExLabel();
                FedExRateCalculator();
            }
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblFeedExLabelFailure);
            hdnErrDesc.Value = BusinessUtility.GetString(ex.Message);
        }

    }

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    private void PrintFedExLabel()
    {
        DbHelper dbHelp = new DbHelper(true);
        Orders ord = new Orders();
        string[] oids = this.OrderIDs.Split(',');
        int orderIDs = 0;
        if (oids.Length > 0)
        {
            orderIDs = BusinessUtility.GetInt(oids[0]);
        }
        if (orderIDs == 0)
        {
            orderIDs = this.OrderID;
        }
        ord.PopulateObject(dbHelp, orderIDs);

        SysWarehouses sWhs = new SysWarehouses();
        sWhs.PopulateObject(ord.OrdShpToWhsCode, dbHelp);

        //Response.Write("Fdx Key :- " + FeedExKey + "</br>" + "Fdx Pwd :- " + FeedExPwd + "</br>" + "Fdx A/c :- " + FeedExAccountNumber + "</br>" + "Fdx Mtr :- " + FeedExMeterNumber + "</br>");
        Partners _cust = new Partners();
        Addresses _shipToAddress = new Addresses();
        _cust.PopulateObject(dbHelp, BusinessUtility.GetInt(ord.OrdCustID));
        _shipToAddress = _cust.GetShipToAddress(dbHelp, _cust.PartnerID, _cust.PartnerType);
        ShipWebServiceClient.FeedEx.OrderID = orderIDs;
        ShipWebServiceClient.FeedEx.FeedExLabelPath = FeedExLabelPath;
        ShipWebServiceClient.FeedEx.FeedExKey = FeedExKey;
        ShipWebServiceClient.FeedEx.FeedExPwd = FeedExPwd;
        ShipWebServiceClient.FeedEx.FeedExAccountNumber = FeedExAccountNumber;
        ShipWebServiceClient.FeedEx.FeedExMeterNumber = FeedExMeterNumber;
        ShipWebServiceClient.FeedEx.WhsName = BusinessUtility.GetString(sWhs.WarehouseDescription);
        if (BusinessUtility.GetString(sWhs.WarehousePhone) != "")
        {
            ShipWebServiceClient.FeedEx.WhsPhone = BusinessUtility.GetString(sWhs.WarehousePhone);
        }
        else
        {
            ShipWebServiceClient.FeedEx.WhsPhone = "....";
        }

        if (BusinessUtility.GetString(sWhs.WarehouseAddressLine1) != "")
        {
            ShipWebServiceClient.FeedEx.WhsAdd1 = BusinessUtility.GetString(sWhs.WarehouseAddressLine1);
        }
        else
        {
            ShipWebServiceClient.FeedEx.WhsAdd1 = "....  ";
        }

        if (BusinessUtility.GetString(sWhs.WarehouseAddressLine2) != "")
        {
            ShipWebServiceClient.FeedEx.WhsAdd2 = BusinessUtility.GetString(sWhs.WarehouseAddressLine2);
        }
        else
        {
            ShipWebServiceClient.FeedEx.WhsAdd2 = "....  ";
        }


        if (BusinessUtility.GetString(sWhs.WarehouseCity) != "")
        {
            ShipWebServiceClient.FeedEx.WhsCity = BusinessUtility.GetString(sWhs.WarehouseCity);
        }
        else
        {
            ShipWebServiceClient.FeedEx.WhsCity = "....  ";
        }

        if (BusinessUtility.GetString(sWhs.WarehouseState) != "")
        {
            ShipWebServiceClient.FeedEx.WhsState = BusinessUtility.GetString(sWhs.WarehouseState);
        }
        else
        {
            ShipWebServiceClient.FeedEx.WhsState = "....  ";
        }

        if (BusinessUtility.GetString(sWhs.WarehousePostalCode) != "")
        {
            ShipWebServiceClient.FeedEx.WhsPostalCode = BusinessUtility.GetString(sWhs.WarehousePostalCode);
        }
        else
        {
            ShipWebServiceClient.FeedEx.WhsPostalCode = "....  ";
        }

        if (BusinessUtility.GetString(sWhs.WarehouseCountry) != "")
        {
            ShipWebServiceClient.FeedEx.WhsCountryCode = BusinessUtility.GetString(sWhs.WarehouseCountry);
        }
        else
        {
            ShipWebServiceClient.FeedEx.WhsCountryCode = "....  ";
        }

        //ShipWebServiceClient.FeedEx.WhsPhone = "514-934-3737";
        ShipWebServiceClient.FeedEx.WhsAdd1 = BusinessUtility.GetString(sWhs.WarehouseAddressLine1);
        ShipWebServiceClient.FeedEx.WhsAdd2 = BusinessUtility.GetString(sWhs.WarehouseAddressLine2);
        ShipWebServiceClient.FeedEx.WhsCity = BusinessUtility.GetString(sWhs.WarehouseCity);
        ShipWebServiceClient.FeedEx.WhsState = BusinessUtility.GetString(sWhs.WarehouseState);
        ShipWebServiceClient.FeedEx.WhsPostalCode = BusinessUtility.GetString(sWhs.WarehousePostalCode);
        ShipWebServiceClient.FeedEx.WhsCountryCode = BusinessUtility.GetString(sWhs.WarehouseCountry);
        //ShipWebServiceClient.FeedEx.WhsAdd1 = BusinessUtility.GetString(sWhs.WarehouseAddressLine1);
        //ShipWebServiceClient.FeedEx.WhsAdd2 = BusinessUtility.GetString(sWhs.WarehouseAddressLine2);
        //ShipWebServiceClient.FeedEx.WhsCity = "Harrison";
        //ShipWebServiceClient.FeedEx.WhsState = "AR";
        //ShipWebServiceClient.FeedEx.WhsPostalCode = "72601";
        //ShipWebServiceClient.FeedEx.WhsCountryCode = "US";

        Addresses addr = new Addresses();
        addr.GetOrderAddress(orderIDs, AddressType.SHIP_TO_ADDRESS);


        ShipWebServiceClient.FeedEx.ShpPersonName = BusinessUtility.GetString(_cust.PartnerLongName);
        ShipWebServiceClient.FeedEx.ShpPersonPhone = BusinessUtility.GetString(_cust.PartnerPhone);
        ShipWebServiceClient.FeedEx.ShpPersonAdd1 = BusinessUtility.GetString(addr.AddressLine1);
        ShipWebServiceClient.FeedEx.ShpPersonAdd2 = BusinessUtility.GetString(addr.AddressLine2);
        ShipWebServiceClient.FeedEx.ShpPersonCity = BusinessUtility.GetString(addr.AddressCity);
        ShipWebServiceClient.FeedEx.ShpPersonState = BusinessUtility.GetString(addr.AddressState);
        ShipWebServiceClient.FeedEx.ShpPersonPostalCode = BusinessUtility.GetString(addr.AddressPostalCode);
        ShipWebServiceClient.FeedEx.ShpPersonCountryCode = BusinessUtility.GetString(addr.AddressCountry);
        //ShipWebServiceClient.FeedEx.ShpPersonAdd1 = BusinessUtility.GetString(_shipToAddress.AddressLine1);
        //ShipWebServiceClient.FeedEx.ShpPersonAdd2 = BusinessUtility.GetString(_shipToAddress.AddressLine2);
        //ShipWebServiceClient.FeedEx.ShpPersonCity = BusinessUtility.GetString(_shipToAddress.AddressCity);
        //ShipWebServiceClient.FeedEx.ShpPersonState = BusinessUtility.GetString(_shipToAddress.AddressState);
        //ShipWebServiceClient.FeedEx.ShpPersonPostalCode = BusinessUtility.GetString(_shipToAddress.AddressPostalCode);
        //ShipWebServiceClient.FeedEx.ShpPersonCountryCode = BusinessUtility.GetString(_shipToAddress.AddressCountry);

        //ShipWebServiceClient.FeedEx.ShpPersonAdd1 = BusinessUtility.GetString(_shipToAddress.AddressLine1);
        //ShipWebServiceClient.FeedEx.ShpPersonAdd2 = BusinessUtility.GetString(_shipToAddress.AddressLine2);
        //ShipWebServiceClient.FeedEx.ShpPersonCity = "Austin";
        //ShipWebServiceClient.FeedEx.ShpPersonState = "TX";
        //ShipWebServiceClient.FeedEx.ShpPersonPostalCode = "73301";
        //ShipWebServiceClient.FeedEx.ShpPersonCountryCode = "US";

        #region GetCurrencyCodeAsCountry
        string sCountryCurrencyCode = "";
        CountryStateTaxGroup objCountry = new CountryStateTaxGroup();
        var vCountryList = objCountry.GetListCountry(null, ShipWebServiceClient.FeedEx.ShpPersonCountryCode);
        if (vCountryList.Count > 0)
        {
            sCountryCurrencyCode = vCountryList[0].CountryCurrencyCode;
        }
        #endregion

        ShipWebServiceClient.FeedEx.PayorAccountNo = PayorAccountNo;
        ShipWebServiceClient.FeedEx.PayorCountryCode = BusinessUtility.GetString(sWhs.WarehouseCountry);
        ShipWebServiceClient.FeedEx.PkgWeight = PkgWeight;
        ShipWebServiceClient.FeedEx.PkgLength = PkgLength;
        ShipWebServiceClient.FeedEx.PkgWidth = PkgWidth;
        ShipWebServiceClient.FeedEx.PkgHeight = PkgHeight;
        ShipWebServiceClient.FeedEx.CollectionAmount = (decimal)CollectionAmount;
        if (sCountryCurrencyCode != "")
        {
            ShipWebServiceClient.FeedEx.CollectionCurrencyCode = sCountryCurrencyCode;
        }
        else
        {
            ShipWebServiceClient.FeedEx.CollectionCurrencyCode = CollectionCurrencyCode;
        }
        ShipWebServiceClient.FeedEx.ShowPDFFile = ShowPdf;
        ShipWebServiceClient.FeedEx.FeedexLabel();

        if (ShipWebServiceClient.FeedEx.Status == "0")
        {
            //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.lblFeedExLabelCreated);
            hdnPkgDtlPdfFilePath.Value = ShipWebServiceClient.FeedEx.PkgDtlPdfPath;
            hdnPkgCmpltDtlPdfFilePath.Value = ShipWebServiceClient.FeedEx.PkgCmpltDtlPdfPath;
            txtShipmentTrackingNo.Text = ShipWebServiceClient.FeedEx.TranckingNo;
            tblDownLoad.Visible = true;
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, BusinessUtility.GetString(ShipWebServiceClient.FeedEx.ErrDesc));
            hdnErrDesc.Value = BusinessUtility.GetString(ShipWebServiceClient.FeedEx.ErrDesc);
        }
    }

    private void FedExRateCalculator()
    {
        DbHelper dbHelp = new DbHelper(true);
        Orders ord = new Orders();
        string[] oids = this.OrderIDs.Split(',');
        int orderIDs = 0;
        if (oids.Length > 0)
        {
            orderIDs = BusinessUtility.GetInt(oids[0]);
        }
        if (orderIDs == 0)
        {
            orderIDs = this.OrderID;
        }
        ord.PopulateObject(dbHelp, orderIDs);

        SysWarehouses sWhs = new SysWarehouses();
        sWhs.PopulateObject(ord.OrdShpToWhsCode, dbHelp);

        //Response.Write("Fdx Key :- " + FeedExKey + "</br>" + "Fdx Pwd :- " + FeedExPwd + "</br>" + "Fdx A/c :- " + FeedExAccountNumber + "</br>" + "Fdx Mtr :- " + FeedExMeterNumber + "</br>");
        Partners _cust = new Partners();
        Addresses _shipToAddress = new Addresses();
        _cust.PopulateObject(dbHelp, BusinessUtility.GetInt(ord.OrdCustID));
        _shipToAddress = _cust.GetShipToAddress(dbHelp, _cust.PartnerID, _cust.PartnerType);

        ShipWebServiceClient.FeedExRateCalculator.OrderID = orderIDs;
        ShipWebServiceClient.FeedExRateCalculator.FeedExLabelPath = FeedExLabelPath;
        ShipWebServiceClient.FeedExRateCalculator.FeedExKey = FeedExKey;
        ShipWebServiceClient.FeedExRateCalculator.FeedExPwd = FeedExPwd;
        ShipWebServiceClient.FeedExRateCalculator.FeedExAccountNumber = FeedExAccountNumber;
        ShipWebServiceClient.FeedExRateCalculator.FeedExMeterNumber = FeedExMeterNumber;
        ShipWebServiceClient.FeedExRateCalculator.WhsName = BusinessUtility.GetString(sWhs.WarehouseDescription);
        if (BusinessUtility.GetString(sWhs.WarehousePhone) != "")
        {
            ShipWebServiceClient.FeedExRateCalculator.WhsPhone = BusinessUtility.GetString(sWhs.WarehousePhone);
        }
        else
        {
            ShipWebServiceClient.FeedExRateCalculator.WhsPhone = "....";
        }

        if (BusinessUtility.GetString(sWhs.WarehouseAddressLine1) != "")
        {
            ShipWebServiceClient.FeedExRateCalculator.WhsAdd1 = BusinessUtility.GetString(sWhs.WarehouseAddressLine1);
        }
        else
        {
            ShipWebServiceClient.FeedExRateCalculator.WhsAdd1 = "....  ";
        }

        if (BusinessUtility.GetString(sWhs.WarehouseAddressLine2) != "")
        {
            ShipWebServiceClient.FeedExRateCalculator.WhsAdd2 = BusinessUtility.GetString(sWhs.WarehouseAddressLine2);
        }
        else
        {
            ShipWebServiceClient.FeedExRateCalculator.WhsAdd2 = "....  ";
        }


        if (BusinessUtility.GetString(sWhs.WarehouseCity) != "")
        {
            ShipWebServiceClient.FeedExRateCalculator.WhsCity = BusinessUtility.GetString(sWhs.WarehouseCity);
        }
        else
        {
            ShipWebServiceClient.FeedExRateCalculator.WhsCity = "....  ";
        }

        if (BusinessUtility.GetString(sWhs.WarehouseState) != "")
        {
            ShipWebServiceClient.FeedExRateCalculator.WhsState = BusinessUtility.GetString(sWhs.WarehouseState);
        }
        else
        {
            ShipWebServiceClient.FeedExRateCalculator.WhsState = "....  ";
        }

        if (BusinessUtility.GetString(sWhs.WarehousePostalCode) != "")
        {
            ShipWebServiceClient.FeedExRateCalculator.WhsPostalCode = BusinessUtility.GetString(sWhs.WarehousePostalCode);
        }
        else
        {
            ShipWebServiceClient.FeedExRateCalculator.WhsPostalCode = "....  ";
        }

        if (BusinessUtility.GetString(sWhs.WarehouseCountry) != "")
        {
            ShipWebServiceClient.FeedExRateCalculator.WhsCountryCode = BusinessUtility.GetString(sWhs.WarehouseCountry);
        }
        else
        {
            ShipWebServiceClient.FeedExRateCalculator.WhsCountryCode = "....  ";
        }

        ShipWebServiceClient.FeedExRateCalculator.WhsAdd1 = BusinessUtility.GetString(sWhs.WarehouseAddressLine1);
        ShipWebServiceClient.FeedExRateCalculator.WhsAdd2 = BusinessUtility.GetString(sWhs.WarehouseAddressLine2);
        ShipWebServiceClient.FeedExRateCalculator.WhsCity = BusinessUtility.GetString(sWhs.WarehouseCity);
        ShipWebServiceClient.FeedExRateCalculator.WhsState = BusinessUtility.GetString(sWhs.WarehouseState);
        ShipWebServiceClient.FeedExRateCalculator.WhsPostalCode = BusinessUtility.GetString(sWhs.WarehousePostalCode);
        ShipWebServiceClient.FeedExRateCalculator.WhsCountryCode = BusinessUtility.GetString(sWhs.WarehouseCountry);


        //ShipWebServiceClient.FeedExRateCalculator.WhsCity = BusinessUtility.GetString("Austin");
        //ShipWebServiceClient.FeedExRateCalculator.WhsState = BusinessUtility.GetString("TX");
        //ShipWebServiceClient.FeedExRateCalculator.WhsPostalCode = BusinessUtility.GetString("73301");
        //ShipWebServiceClient.FeedExRateCalculator.WhsCountryCode = BusinessUtility.GetString("US");

        Addresses addr = new Addresses();
        addr.GetOrderAddress(orderIDs, AddressType.SHIP_TO_ADDRESS);

        ShipWebServiceClient.FeedExRateCalculator.ShpPersonName = BusinessUtility.GetString(_cust.PartnerLongName);
        ShipWebServiceClient.FeedExRateCalculator.ShpPersonPhone = BusinessUtility.GetString(_cust.PartnerPhone);
        ShipWebServiceClient.FeedExRateCalculator.ShpPersonAdd1 = BusinessUtility.GetString(addr.AddressLine1);
        ShipWebServiceClient.FeedExRateCalculator.ShpPersonAdd2 = BusinessUtility.GetString(addr.AddressLine2);
        ShipWebServiceClient.FeedExRateCalculator.ShpPersonCity = BusinessUtility.GetString(addr.AddressCity);
        ShipWebServiceClient.FeedExRateCalculator.ShpPersonState = BusinessUtility.GetString(addr.AddressState);
        ShipWebServiceClient.FeedExRateCalculator.ShpPersonPostalCode = BusinessUtility.GetString(addr.AddressPostalCode);
        ShipWebServiceClient.FeedExRateCalculator.ShpPersonCountryCode = BusinessUtility.GetString(addr.AddressCountry);


        //ShipWebServiceClient.FeedExRateCalculator.ShpPersonCity = BusinessUtility.GetString("Collierville");
        //ShipWebServiceClient.FeedExRateCalculator.ShpPersonState = BusinessUtility.GetString("TN");
        //ShipWebServiceClient.FeedExRateCalculator.ShpPersonPostalCode = BusinessUtility.GetString("38017");
        //ShipWebServiceClient.FeedExRateCalculator.ShpPersonCountryCode = BusinessUtility.GetString("US");

        #region GetCurrencyCodeAsCountry
        string sCountryCurrencyCode = "";
        CountryStateTaxGroup objCountry = new CountryStateTaxGroup();
        var vCountryList = objCountry.GetListCountry(null, ShipWebServiceClient.FeedExRateCalculator.ShpPersonCountryCode);
        if (vCountryList.Count > 0)
        {
            sCountryCurrencyCode = vCountryList[0].CountryCurrencyCode;
        }
        #endregion

        ShipWebServiceClient.FeedExRateCalculator.PayorAccountNo = PayorAccountNo;
        ShipWebServiceClient.FeedExRateCalculator.PayorCountryCode = BusinessUtility.GetString(sWhs.WarehouseCountry);
        ShipWebServiceClient.FeedExRateCalculator.PkgWeight = PkgWeight;
        ShipWebServiceClient.FeedExRateCalculator.PkgLength = PkgLength;
        ShipWebServiceClient.FeedExRateCalculator.PkgWidth = PkgWidth;
        ShipWebServiceClient.FeedExRateCalculator.PkgHeight = PkgHeight;
        ShipWebServiceClient.FeedExRateCalculator.CollectionAmount = (decimal)CollectionAmount;
        if (sCountryCurrencyCode != "")
        {
            ShipWebServiceClient.FeedExRateCalculator.CollectionCurrencyCode = sCountryCurrencyCode;
        }
        else
        {
            ShipWebServiceClient.FeedExRateCalculator.CollectionCurrencyCode = CollectionCurrencyCode;
        }
        ShipWebServiceClient.FeedExRateCalculator.FeedexRateCalculator();

        if (ShipWebServiceClient.FeedExRateCalculator.Status == "0")
        {
            //MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.lblFeedExLabelCreated);
            txtShipmentPrice.Text = BusinessUtility.GetString(ShipWebServiceClient.FeedExRateCalculator.ShipmentPrice);
            tblDownLoad.Visible = true;
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, BusinessUtility.GetString(ShipWebServiceClient.FeedExRateCalculator.ErrDesc));
            hdnErrDesc.Value += BusinessUtility.GetString(ShipWebServiceClient.FeedExRateCalculator.ErrDesc);
        }
    }

    #region ReadValueFromWebConfig
    private static string FeedExLabelPath
    {
        get
        {
            return HttpContext.Current.Request.PhysicalApplicationPath + BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["FeedExLablePath"]);
        }
    }

    private static string FeedExKey
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["FeedExKey"]);
        }
    }

    private static string FeedExPwd
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["FeedExPwd"]);
        }
    }

    private static string FeedExAccountNumber
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["FeedExAccountNumber"]);
        }
    }

    private static string FeedExMeterNumber
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["FeedExMeterNumber"]);
        }
    }

    private static string PayorAccountNo
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["PayorAccountNo"]);
        }
    }

    private static decimal PkgWeight
    {
        get
        {
            return BusinessUtility.GetDecimal(System.Configuration.ConfigurationManager.AppSettings["PkgWeight"]);
        }
    }

    private static string PkgLength
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["PkgLength"]);
        }
    }

    private static string PkgWidth
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["PkgWidth"]);
        }
    }

    private static string PkgHeight
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["PkgHeight"]);
        }
    }

    private static Double CollectionAmount
    {
        get
        {
            return BusinessUtility.GetDouble(System.Configuration.ConfigurationManager.AppSettings["CollectionAmount"]);
        }
    }

    private static string CollectionCurrencyCode
    {
        get
        {
            return BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["CollectionCurrencyCode"]);
        }
    }

    private static bool ShowPdf
    {
        get
        {
            if (BusinessUtility.GetString(System.Configuration.ConfigurationManager.AppSettings["ShowPDF"]) == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    #endregion

    //private static int OrderID
    //{
    //    get
    //    {
    //        int id = 0;
    //        int.TryParse(HttpContext.Current.Request.QueryString["oid"], out id);
    //        return id;
    //    }
    //}


}
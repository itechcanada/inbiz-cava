﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/fullWidth.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Shipping_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <style type="text/css">
        input[type=submit]{height:50px;}
        input[type=text], select {padding:5px; font-size:16px;}        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <h4>
        <%=Resources.Resource.lblShipping%>
    </h4>
    <div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" colspan="2">
                    <asp:TextBox runat="server" Width="195px" ID="txtSearch" ValidationGroup="PrdSrch"
                        AutoPostBack="false"> </asp:TextBox><span class="style1"> *</span>
                    <asp:RequiredFieldValidator ID="reqvalSearch" runat="server" ControlToValidate="txtSearch"
                        ErrorMessage="<%$ Resources:Resource, PlzEntSearchCriteria %>" ValidationGroup="PrdSrch"
                        SetFocusOnError="true" Display="None"> </asp:RequiredFieldValidator>
                    <asp:DropDownList ID="dlWarehouse" runat="server" Width="280px" ValidationGroup="PrdSrch">
                    </asp:DropDownList>
                    <span class="style1">*</span>
                    <asp:CustomValidator ID="custvalPOWarehouse" runat="server" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:Resource, custvalPOWarehouse%>" ValidationGroup="PrdSrch"
                        ClientValidationFunction="funCheckWarehouseCode" Display="None"> </asp:CustomValidator>
                    <asp:CustomValidator ID="custvalWarehouse" runat="server" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:Resource, custvalPOWarehouse%>" ValidationGroup="Srch"
                        ClientValidationFunction="funCheckWarehouseCode" Display="None"> </asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <table align="center" cellpadding="5" cellspacing="5">
                        <tr>
                            <td>
                                <asp:Button ID="btnOrderNO" runat="server" CausesValidation="false" Text="<%$ Resources:Resource,cmdCssOrderNo%>"
                                    ValidationGroup="PrdSrch" OnClick="btnOrderNO_Click" Width="150px" />
                            </td>
                            <td>
                                <asp:Button ID="btnCustomerName" runat="server" CausesValidation="false" Text="<%$ Resources:Resource,cmdCssCustomerName%>"
                                    ValidationGroup="PrdSrch" OnClick="btnCustomerName_Click" Width="150px" />
                            </td>
                            <td>
                                <asp:Button ID="btnSalesAgentName" runat="server" CausesValidation="false" Text="<%$ Resources:Resource,cmdCssSalesAgentName%>"
                                    ValidationGroup="PrdSrch" OnClick="btnSalesAgentName_Click" Width="150px" />
                            </td>
                            <td>
                                <asp:Button ID="btnTrackNo" runat="server" CausesValidation="false" Text="<%$ Resources:Resource,cmdCssShippingTrackNo%>"
                                    ValidationGroup="PrdSrch" OnClick="btnTrackNo_Click" Width="150px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="10" colspan="2">
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <br />
                    <div>
                        <asp:Button ID="btncmdToday" runat="server" Text="<%$ Resources:Resource,cmdCssToday%>"
                            ValidationGroup="Srch" OnClick="btncmdToday_Click" Width="150px" />
                    </div>
                    <br />
                    <div>
                        <asp:Button ID="btncmdTomorrow" runat="server" Text="<%$ Resources:Resource,lblTomorrow%>"
                            ValidationGroup="Srch" Width="150px" OnClick="btncmdTomorrow_Click" /></div>
                    <br />
                    <div>
                        <asp:Button ID="btncmdThisWeek" runat="server" Text="<%$ Resources:Resource,cmdCssThisWeek%>"
                            ValidationGroup="Srch" OnClick="btncmdThisWeek_Click" Width="150px" /></div>
                    <br />
                    <div>
                        <asp:Button ID="btncmdNextWeek" runat="server" Text="<%$ Resources:Resource,cmdCssNextWeek%>"
                            ValidationGroup="Srch" OnClick="btncmdNextWeek_Click" Width="150px" /></div>
                    <br />
                    <div>
                        <asp:Button ID="btncmdAll" runat="server" Text="<%$ Resources:Resource,cmdCssAll%>"
                            ValidationGroup="Srch" OnClick="btncmdAll_Click" Width="150px" />
                    </div>
                    <br />
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsShipping" runat="server" ShowMessageBox="false" ShowSummary="false"
            ValidationGroup="PrdSrch" />
        <asp:ValidationSummary ID="valsShippingW" runat="server" ShowMessageBox="false" ShowSummary="false"
            ValidationGroup="Srch" />
    </div>
    <br />
    <br />
    <asp:HiddenField id="hdnTitleUpdateShipment" runat="server" Value="<%$ Resources:Resource,lblOrderUpdateShipment%>"/>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        function funCheckWarehouseCode(source, args) {
            if (document.getElementById('<%=dlWarehouse.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
        function doClick(buttonName, e) {
            //the purpose of this function is to allow the enter key to 
            //point to the correct button to click.
            var key;

            if (window.event)
                key = window.event.keyCode;     //IE
            else
                key = e.which;     //firefox

            if (key == 13) {
                //Get the button the user wants to have clicked
                var btn = document.getElementById(buttonName);
                if (btn != null) { //If we find the button click it
                    btn.click();
                    event.keyCode = 0
                }
            }
        }
    </script>
    <script language="javascript" type="text/javascript">
        var popupOpen = false;      
        function goToShipmentPage() {           
            var oid = $("#<%=txtSearch.ClientID%>").val();
            if (isNaN(oid)) {                
                alert("Invalid Odrder Number!");
                return false;
            }

            var vTitle = $("#<%=hdnTitleUpdateShipment.ClientID%>").val();

            var $dialog = jQuery.FrameDialog.create({
                url: "Shipment.aspx?callBack=setEmptyText&oid=" + oid,
                title: vTitle, // "<%=Resources.Resource.lblUpdateShipment%>"
                loadingClass: "loading-image",
                modal: true,
                width: "800",
                height: "560",
                autoOpen: false
            }).bind("dialogclose", function () {
                popupOpen = false;
                $("#<%=txtSearch.ClientID%>").focus();
            });
            $dialog.dialog('open');
            popupOpen = true;
            return false;
        }
       
        $("#<%=txtSearch.ClientID%>").keypress(function (e) {
            if (e.which == 13 && !popupOpen) {
                $("#<%=btnOrderNO.ClientID%>").trigger("click");
            }
        });
        $("#<%=btnOrderNO.ClientID%>").click(function () {
            if (!popupOpen) {
                return goToShipmentPage();
            }
            else {
                popupOpen = false;
                return false;
            }
        });

        function setEmptyText() {
            $("#<%=txtSearch.ClientID%>").val("");
            $("#<%=txtSearch.ClientID%>").focus();
        }
                 
    </script>
</asp:Content>

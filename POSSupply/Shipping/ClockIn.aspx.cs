﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Shipping_ClockIn : BasePage
{
    private string strTaxID = "";
    private UsersClockIn objUsersClockIn = new UsersClockIn();
    // On Page Load
    protected void Page_Load(object sender, System.EventArgs e)
    {
        //if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        //{
        //    Response.Redirect("~/AdminLogin.aspx");
        //}
        if (!IsPagePostBack(jgdvClockIn))
        {
            string sPosLink = "AddEditInOutTime.aspx?whsCode="+ this.WarehouseCode;
            if (IsCallFromPOS == true)
            {
                if (this.WarehouseCode != "")
                {
                    sPosLink += "&InvokeSrc=POS";
                }
                else
                {
                    sPosLink += "?InvokeSrc=POS";
                }
            }
            mdCreateCurrency.Url = sPosLink;
            lnkAddClockIn.Text = Resources.Resource.lblAddClockIn;
            lblTitle.Text = Resources.Resource.lblView + " : " + Resources.Resource.lblViewClockIn;

            InbizUser objUser = new InbizUser();
            ListItem liItem = new ListItem { Text = Resources.Resource.chkPOSAlluser, Value = "0" };
            objUser.FillAllUsers(null, dlUser, liItem);
            ListItem lstFindUser = dlUser.Items.FindByValue(BusinessUtility.GetString("0"));
            if (lstFindUser != null)
            {
                lstFindUser.Selected = true;
            } 
            if (Convert.ToString(Session["UserRole"]) == "1")
            {
                lnkAddClockIn.Text = "";
            }
            dlUser.Focus();
            //txtSearch.Focus();
        }
    }
    protected void lnkAddClockIn_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("~/Shipping/AddEditInOutTime.aspx?whsCode="+ this.WarehouseCode);
    }

    protected void jgdvClockIn_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 6)
        {
            string sPosLink = "";
            if (IsCallFromPOS == true)
            {
                sPosLink = "&InvokeSrc=POS";
            }
            e.CellHtml = string.Format("<a class=edit-Tax  href=AddEditInOutTime.aspx?ClockInID={0}&whsCode={1}" + sPosLink + " >" + Resources.Resource.grdEdit + "</a>", e.RowValues[0], this.WarehouseCode);
        }
        if (e.ColumnIndex == 7)
        {
            e.CellHtml = string.Format("<a href=\"{0}\" onclick='deleteRow({1});'>{2}</a>", "javascript:void(0);", e.RowValues[0], Resources.Resource.delete);
        }
    }

    protected void jgdvClockIn_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        string dUser = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_dlUser"]);
        string sDate = BusinessUtility.GetString(Request.QueryString["ctl00_ctl00_cphFullWidth_cphLeft_txtDate"]);
        if (Request.QueryString.AllKeys.Contains("cmdDelete"))
        {
            int id = 0;
            if (int.TryParse(Request.QueryString["idUsersClockIn"], out id))
            {
                objUsersClockIn.idUsersClockIn = id;
                objUsersClockIn.UserID = CurrentUser.UserID;
                objUsersClockIn.Delete (null);
            }
        }
        jgdvClockIn.DataSource = objUsersClockIn.GetClockInList(null, dUser, sDate);
        
    }

    private bool IsCallFromPOS
    {
        get
        {
            if (BusinessUtility.GetString(Request.QueryString["InvokeSrc"]).ToUpper() == "POS".ToUpper())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public string WarehouseCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["whsCode"]);
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdminLogin.aspx.cs" Inherits="AdminLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="lib/css/inbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/scripts/jquery-1.5.1.min.js"></script>
    <script type="text/javascript" src="lib/scripts/jquery-ui-1.8.11.custom.min.js"></script>
    <link href="lib/css/login.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("button, input:submit", "").button();
        });    	   
    </script>    
</head>
<body>
    <div id="outer">
        <div id="middle">
            <div id="inner" class="inner">
                <div class="glow">
                    <div class="content">
                        <div class="logo" style="height:36px;width:208px;">
                        </div>
                        <%--  <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut auctor, purus at condimentum
                            cursus, orci elit felis.
                            <br />
                            <br />
                            <asp:Label ID="lblMsg" runat="server" ForeColor="red" CssClass="lblBold"></asp:Label>
                        </p>--%>
                        <div style="height: 30px">
                            <p>
                                <asp:Label ID="lblMsg" runat="server" ForeColor="red" CssClass="lblBold" />
                            </p>
                        </div>
                        <br />
                        <br />
                        <form id="form1" runat="server">
                        <table>
                            <tr>
                                <td class="text">
                                    <asp:Label ID="lblID" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblLoginID %>"></asp:Label>
                                </td>
                                <td class="input">
                                    <asp:TextBox ID="txtLoginID" Width="250px" runat="server" MaxLength="75"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvalLoginID" runat="server" ControlToValidate="txtLoginID"
                                        ErrorMessage="<%$ Resources:Resource, reqvalLoginID %>" SetFocusOnError="true"
                                        Display="None"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="text">
                                    <asp:Label ID="lblPwd" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblPassword %>"></asp:Label>
                                </td>
                                <td class="input">
                                    <asp:TextBox ID="txtPwd" Width="250px" runat="server" MaxLength="35" 
                                        TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvalPwd" runat="server" ControlToValidate="txtPwd"
                                        ErrorMessage="<%$ Resources:Resource, reqvalPassword %>" SetFocusOnError="true"
                                        Display="None"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td class="forgetLink">
                                    <a href="ForgotPassword.aspx">
                                        <%= Resources.Resource.lblForgetPwd %></a>                                   
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnLogin" runat="server" Text="<%$ Resources:Resource, cmdCssLogin%>" OnClick="btnLogin_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnReset" runat="server" Text="<%$ Resources:Resource, cmdCssReset%>" OnClick="btnReset_Click"
                                        CausesValidation="False" />
                                </td>
                            </tr>
                        </table>
                        <asp:Panel runat="server" ID="pnlMain" DefaultButton="btnLogin">
                            <asp:ValidationSummary ID="valsDefault" runat="server" DisplayMode="List" ShowMessageBox="true"
                                ShowSummary="false" />
                        </asp:Panel>
                        <script type="text/javascript">
   function funReset()
    {
      document.getElementById('<%=txtLoginID.ClientID%>').value = "";
      document.getElementById('<%=txtPwd.ClientID%>').value = "";
      document.getElementById('<%=lblMsg.ClientID%>').value = "";
      return false;
    } 
    if('<%= Request.QueryString["lang"]%>' == 'logout'){
        //self.close();
        if(opener!=null){
            opener.reset();
        }
    }
    <% if (Request.QueryString["AutoConnect"]=="True") { %>	
		setTimeout(function(){if(document.getElementById('<%=txtLoginID.ClientID%>').value!=""&&document.getElementById('<%=txtPwd.ClientID%>').value!=""){
			
			$('#ctl00_cphDefault_btnSubmit').click()
        }},15000)
    <% } %>    

                        </script>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

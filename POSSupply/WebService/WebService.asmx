﻿<%@ WebService Language="C#" Class="WebService" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using System.Xml;
using iTECH.WebService.Utility;
using iTECH.Library.DataAccess.MySql;
using iTECH.InbizERP.BusinessLogic.MobileReservation;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
[System.Web.Script.Services.ScriptService]

public class WebService : System.Web.Services.WebService
{
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string Authentication( string emailID, string password, string deviceID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        MReservation mobileReservation = new MReservation();
        int userID = -1;
        try
        {
            userID = mobileReservation.GetUser(emailID, password);
            if (userID > 0)
            {
                Tokens objtoken = new Tokens();
                objtoken.UserID = userID;
                objtoken.ExpirePreviousToken();
                objtoken.TokenData = SingleSignOn.CreateToken(AuthTokenUser(emailID, deviceID));
                objtoken.CheckInType = 1;//By Deafult Set
                AuthToken objAuthToken = new AuthToken(AuthTokenUser(emailID, deviceID));
                objtoken.DeviceID = deviceID;
                objtoken.IsActive = true;
                objtoken.TokenNo = objtoken.Insert();
                dictResult.OkStatus(ResponseCode.SUCCESS);
                dictResult.Add("TokenNo", objtoken.TokenData);
                dictResult.Add("EmailID", emailID);
                dictResult.Add("UserID", userID);
            }
            else
                throw new Exception(ExceptionMessage.INVALIDUSER);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "Authentication", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.NOTREGISTER : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }   

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ReservationDetails(string emailID, string tokenNo, string deviceID, string checkInDate, string checkOutDate)
    {
        ResultDictionary dictResult = new ResultDictionary();
        MReservation mobileReservation = new MReservation();
        try
        {
            if (Authorization(emailID, tokenNo, deviceID))
            {
                dictResult.OkStatus(ResponseCode.SUCCESS);
                dictResult.Add("Reservation", mobileReservation.ReservationDetails(null, BusinessUtility.GetDateTime(checkInDate), BusinessUtility.GetDateTime(checkOutDate)));
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "ReservationDetails", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.NOTREGISTER : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string Reservation(string emailID, string tokenNo, string deviceID, string reservationID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        MReservation mobileReservation = new MReservation();
        try
        {
            if (Authorization(emailID, tokenNo, deviceID))
            {
                dictResult.OkStatus(ResponseCode.SUCCESS);
                dictResult.Add("ReservationFor", mobileReservation.ReservationDetails(null, BusinessUtility.GetInt(reservationID)));
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "ReservationDetails", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.NOTREGISTER : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string CreateReservation(string emailID, string tokenNo, string deviceID, string reservationNote, string guestName, string noOfGuest, string bedID, string checkInDate, string checkOutDate, string userID)
    {
        ResultDictionary dictResult = new ResultDictionary();
        MReservation mobileReservation = new MReservation();
        try
        {
            if (Authorization(emailID, tokenNo, deviceID))
            {
                bool isSuccess = mobileReservation.MakeReservation(null, guestName, BusinessUtility.GetInt(noOfGuest), BusinessUtility.GetInt(bedID), BusinessUtility.GetDateTime(checkInDate), BusinessUtility.GetDateTime(checkOutDate), BusinessUtility.GetInt(ConfigurationManager.AppSettings["ReservationPartnerID"]), BusinessUtility.GetInt(userID));
                dictResult.OkStatus(isSuccess ? ResponseCode.SUCCESS : ResponseCode.UNSUCCESS);
            }
            else
                throw new Exception(ExceptionMessage.INVALIDTOKEN);
        }
        catch (Exception ex)
        {
            File.AppendAllText(HttpContext.Current.Server.MapPath("~/log.txt"), string.Format("\n {0} Error:: {1}\n{2}", "CreateReservation", DateTime.Now, ex.Message));
            dictResult.ErrorStatus(ex.Message == ExceptionMessage.INVALIDTOKEN ? ResponseCode.NOTREGISTER : ResponseCode.EXCEPTION);
        }
        return dictResult.ResultInInJSONFormat;
    }


    #region Private Methods
    private bool Authorization(string emailID, string tokenNo, string deviceID)
    {
        bool check1 = new SingleSignOn().Authenticate(AuthTokenUser(emailID, deviceID), tokenNo);
        bool check2 = new Tokens().IsValidToken(emailID, deviceID);
        return check1 && check2;
    }
    private string AuthTokenUser(string emailID, string deviceID)
    {
        return string.Format("{0}{1}", emailID, deviceID);
    }


    //private void PageInit(string xmlPath, string title)
    //{
    //    DataSet _dateSet = null;
    //    string _strSQL = "";
    //    bool _hasLanguageParameter = false;
    //    try
    //    {

    //        if (string.IsNullOrEmpty(xmlPath))
    //            throw new Exception(ExceptionMessage.INVALID_CONFIGURAUIN_SOURCE);
    //        else { xmlPath = HttpContext.Current.Server.MapPath("~") + "/WebService/XML/" + xmlPath + ".xml"; }
                        
    //        if (!File.Exists(Server.MapPath(xmlPath)))
    //            throw new Exception(ExceptionMessage.INVALID_CONFIGURAUIN_SOURCE);

    //        _dateSet = new DataSet("DataSource");
    //        _dateSet.ReadXml(Server.MapPath(xmlPath));
    //        DataRow settingRow = null;
    //        if (_dateSet.Tables["Setting"] != null)
    //        {
    //            settingRow = _dateSet.Tables["Setting"].Rows[0];
    //            string webServiceTitle = BusinessUtility.GetString(settingRow["WebServiceName"]);
    //            if (!webServiceTitle.ToLower().Equals(title))
    //                throw new Exception(ExceptionMessage.INVALID_WEB_METHOD);
                
    //            _hasLanguageParameter = BusinessUtility.GetBool(settingRow["HasLanguageParameter"]);
    //            _strSQL = BusinessUtility.GetString(settingRow["Sql"]);
    //        }

    //        ////Build Column Modal
    //        //DataTable columns = _dateSet.Tables["Column"];
    //        //if (columns != null)
    //        //{
    //        //    foreach (DataRow dr in columns.Rows)
    //        //    {
    //        //        JQGridColumn col = new JQGridColumn();
    //        //        col.HeaderText = BusinessUtility.GetString(dr["HeaderText"]);
    //        //        col.DataField = BusinessUtility.GetString(dr["DataField"]);
    //        //        col.PrimaryKey = BusinessUtility.GetBool(dr["PrimaryKey"]);
    //        //        col.Sortable = false;
    //        //        col.Visible = BusinessUtility.GetBool(dr["Visible"]);
    //        //        if (columns.Columns.Contains("TextAlign"))
    //        //        {
    //        //            col.TextAlign = this.GetTextAlignment(BusinessUtility.GetString(dr["TextAlign"]));
    //        //        }
    //        //        if (columns.Columns.Contains("HeaderWidth"))
    //        //        {
    //        //            int width = BusinessUtility.GetInt(dr["HeaderWidth"]);
    //        //            col.Width = width > 0 ? width : col.Width;
    //        //        }
    //        //        grdReport.Columns.Add(col);
    //        //    }
    //        //}

    //        DataTable filterParams = _dateSet.Tables["SqlParameter"];
    //        //if (filterParams != null)
    //        //{
    //        //    foreach (DataRow dr in filterParams.Rows)
    //        //    {                    
    //        //        string ctrlType = BusinessUtility.GetString(dr["ControlType"]);
    //        //        string dType = BusinessUtility.GetString(dr["Type"]);
    //        //        string dValue = BusinessUtility.GetString(dr["DefaultValue"]);
    //        //        if (ctrlType == "TextBox" || ctrlType == "DatePicker")
    //        //        {
    //        //            TextBox tb = new TextBox();
    //        //            tb.ID = BusinessUtility.GetString(dr["Name"]);
    //        //            tb.Attributes["filterKey"] = BusinessUtility.GetString(dr["Name"]);
    //        //            if (ctrlType == "DatePicker")
    //        //            {
    //        //                tb.CssClass = "datepicker";
    //        //            }
    //        //            if (dType.ToUpper() == "DATE")
    //        //            {
    //        //                tb.Text = BusinessUtility.GetDateTimeString(this.GetRequestedDate(dValue), DateFormat.MMddyyyy);
    //        //            }
    //        //            else
    //        //            {
    //        //                tb.Text = dValue;
    //        //            }
    //        //            phSearchFilters.Controls.Add(tb);
    //        //        }                       
    //        //    }
    //        //}
            
    //    //         string conString = string.Empty;
    //    //string conProvider = string.Empty;
    //    //if (_dateSet != null)
    //    //{
    //    //    //Set connection to datasource if available
    //    //    if (_dateSet.Tables.Contains("Connection"))
    //    //    {
    //    //        DataTable dtConnection = _dateSet.Tables["Connection"];

    //    //        if (dtConnection.Rows.Count > 0)
    //    //        {
    //    //            conString = BusinessUtility.GetString(dtConnection.Rows[0]["ConnectionString"]);
    //    //            conProvider = BusinessUtility.GetString(dtConnection.Rows[0]["providerName"]);
    //    //        }
    //    //        if (!string.IsNullOrEmpty(conString))
    //    //        {
    //    //            sdsReport.ConnectionString = conString;
    //    //        }
    //    //        if (!string.IsNullOrEmpty(conProvider))
    //    //        {
    //    //            sdsReport.ProviderName = conProvider;
    //    //        }
    //    //    }

    //    }
    //    catch (Exception ex) { throw ex; }
    //}       
    #endregion
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

using Newtonsoft.Json;

public partial class Admin_ManageAlertEdit : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.FillForm();
        }
    }

    private void FillForm()
    {
        DbHelper dbHelp = new DbHelper(true);
        NewAlert na = new NewAlert();
        try
        {
            na.PopulateByAlertName(dbHelp, this.AlertName, AppLanguageCode.EN);
            lblAlertType.Text = na.AlertDesc;
            txtBodyEn.Text = na.AlertBody;
            txtSubjectEn.Text = na.AlertSubject;

            na.PopulateByAlertName(dbHelp, this.AlertName, AppLanguageCode.FR);
            txtBodyFr.Text = na.AlertBody;
            txtSubjectFr.Text = na.AlertSubject;
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private string AlertName
    {
        get {
            return  Convert.ToString(Request.QueryString["aName"]);
        }
    }
   
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                NewAlert na = new NewAlert();
                //for English
                na.AlertBody = txtBodyEn.Text;
                na.AlertLang = AppLanguageCode.EN;
                na.AlertName = this.AlertName;
                na.AlertSubject = txtSubjectEn.Text;
                na.Save(dbHelp);

                //for French
                na.AlertBody = txtBodyFr.Text;
                na.AlertLang = AppLanguageCode.FR;
                na.AlertName = this.AlertName;
                na.AlertSubject = txtSubjectFr.Text;
                na.Save(dbHelp);
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgAlertAddedSuccessfully);
                Globals.RegisterReloadParentScript(this);
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
}
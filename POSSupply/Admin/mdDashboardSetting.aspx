﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdDashboardSetting.aspx.cs" Inherits="Admin_mdDashboardSetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <style type="text/css">
        .ul.form li div.lbl{width:90px !important;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div style="padding: 5px; height: 100px; overflow: auto;">
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="Label1" Text="<%$Resources:Resource, lblWhs %>" runat="server" />
                </div>
                <div class="input">
                    <asp:ListBox CssClass="modernized_select" ID="lbxWhs" runat="server" SelectionMode="Multiple" Width="250px">                        
                    </asp:ListBox>
                </div>
                <div class="clearBoth"></div>
            </li>            
        </ul>        
    </div>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" 
            onclick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
            OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


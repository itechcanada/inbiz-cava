﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AddEditIP.aspx.cs" Inherits="Admin_AddEditIP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <ul class="form">
                <li>
                    <div class="lbl">
                        <asp:Label Text="<%$Resources:Resource, lblUsers %>" ID="lblSelectTaxGroup" runat="server" /></div>
                    <div class="input">
                        <asp:DropDownList ID="dlUser" runat="server" Width="200px" AutoPostBack="true">
                        </asp:DropDownList>
                        <span class="style1">*</span>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="dlUser"
                            ErrorMessage="<%$ Resources:Resource, reqvalTaxDescription%>" SetFocusOnError="true"
                            Display="Dynamic" ValidationGroup="TexAdd1"></asp:RequiredFieldValidator>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblTaxDescription" CssClass="lblBold" Text="<%$ Resources:Resource, lblIn%>"
                            runat="server" /></div>
                    <div class="input">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding-right: 2px;">
                                    <asp:TextBox ID="txtInDate" runat="server" Width="75px" CssClass="datepicker" />
                                </td>
                                <td style="padding: 0 2px;">
                                    <asp:TextBox ID="txtInHH" runat="server" Width="25px" CssClass="integerTextField" MaxLength="2" Text="00" />
                                </td>
                                <td style="padding: 0 2px;">
                                    <asp:TextBox ID="txtInMM" runat="server" Width="25px" CssClass="integerTextField" MaxLength="2" Text="00" />
                                </td>
                                <td style="padding: 0 2px;">
                                    <asp:DropDownList runat="Server" ID="ddlInTT" Width="50">
                                        <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0 2px;">
                                    (mm/dd/yyyy)
                                </td>
                                <td style="padding: 0 2px;">
                                    (hh)
                                </td>
                                <td style="padding: 0 2px;">
                                    (mm)
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblTaxPercentage" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblOut%>">
                        </asp:Label></div>
                    <div class="input">
                    <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding-right: 2px;">
                                    <asp:TextBox ID="txtOutDate" runat="server" Width="75px" CssClass="datepicker" />
                                </td>
                                <td style="padding: 0 2px;">
                                    <asp:TextBox ID="txtOutHH" runat="server" Width="25px" CssClass="integerTextField" MaxLength="2" Text="00" />
                                </td>
                                <td style="padding: 0 2px;">
                                    <asp:TextBox ID="txtOutMM" runat="server" Width="25px" CssClass="integerTextField" MaxLength="2" Text="00" />
                                </td>
                                <td style="padding: 0 2px;">
                                    <asp:DropDownList runat="Server" ID="ddlOutTT" Width="50">
                                        <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0 2px;">
                                    (mm/dd/yyyy)
                                </td>
                                <td style="padding: 0 2px;">
                                    (hh)
                                </td>
                                <td style="padding: 0 2px;">
                                    (mm)
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                       </div>
                    <div class="clearBoth">
                    </div>
                </li>
            </ul>
            <div class="div_command">
                <asp:Button ID="btnSave" runat="server" Text="<%$Resources:Resource,cmdCssSave%>"
                    ValidationGroup="TexAdd1" OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" Text="<%$Resources:Resource, btnCancel%>" runat="server"
                    CausesValidation="false" OnClientClick="jQuery.FrameDialog.closeDialog();" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
</asp:Content>



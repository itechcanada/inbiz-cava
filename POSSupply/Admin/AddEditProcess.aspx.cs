﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_AddEditProcess : BasePage
{    
    SysProcessGroup _objProGrp = new SysProcessGroup();
    //On Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security

        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }        
        
        if (!IsPostBack)
        {
            FillRecord();
            txtProcessCode.Focus();
        }
    }
    //Populate Controls
    public void FillRecord()
    {
        if (this.ProcessID > 0) {
            _objProGrp.PopulateObject(this.ProcessID);
            _objProGrp.ProcessID = this.ProcessID;            
            //Get Process Info
            txtProcessCode.Text = _objProGrp.ProcessCode;
            //txtProcessCode.ReadOnly = true;
            txtProcessDescription.Text = _objProGrp.ProcessDescription;
            txtProcessFixedCost.Text = string.Format("{0:F}", _objProGrp.ProcessFixedCost);
            txtProcessCostPerHour.Text = string.Format("{0:F}", _objProGrp.ProcessCostPerHour); 
            txtProcessCostPerUnit.Text = string.Format("{0:F}", _objProGrp.ProcessCostPerUnit); 
        }        
    }
    //Populate Object
    private void SetData()
    {
        _objProGrp.ProcessID = this.ProcessID;
        _objProGrp.ProcessCode = txtProcessCode.Text;
        _objProGrp.ProcessDescription = txtProcessDescription.Text;
        _objProGrp.ProcessFixedCost = BusinessUtility.GetDouble(txtProcessFixedCost.Text);
        _objProGrp.ProcessCostPerHour = BusinessUtility.GetDouble(txtProcessCostPerHour.Text);
        _objProGrp.ProcessCostPerUnit = BusinessUtility.GetDouble(txtProcessCostPerUnit.Text);
    }
    
    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        if (Page.IsValid)
        {
            SetData();
            try
            {
                if (this.ProcessID > 0)
                {
                    _objProGrp.ProcessID = this.ProcessID;
                    _objProGrp.Update();
                    //Update Process 
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.ProcessUpdatedSuccessfully);
                    if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                    {
                        Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                    }
                    else
                    {
                        Globals.RegisterCloseDialogScript(Page);
                    }
                }
                else
                {
                    _objProGrp.Insert();
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.ProcessAddedSuccessfully);
                    if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                    {
                        Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                    }
                    else
                    {
                        Globals.RegisterCloseDialogScript(Page);
                    }
                }
            }
            catch (Exception ex)
            {
                switch (ex.Message)
                {
                    case CustomExceptionCodes.PROCESS_CODE_ALREADY_EXISTS:
                        lblError.Text = Resources.Resource.ProcessCodeAlreadyExists;
                        txtProcessCode.Focus();
                        break;
                    case CustomExceptionCodes.PROCESS_DESCRIPTION_ALREADY_EXISTS:
                        lblError.Text = Resources.Resource.ProcessDescriptionAlreadyExists;
                        txtProcessCode.Focus();
                        break;
                    default:
                        lblError.Text = "Critical Error!";
                        txtProcessCode.Focus();
                        break;
                }
            }
        }
    }

    public int ProcessID {
        get {
            int prid = 0;
            int.TryParse(Request.QueryString["ProcessID"], out prid);
            return prid;
        }
    }

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_ViewGlobalParameters : BasePage
{

    private SysCompanyInfo _objSysCompanyInfo = new SysCompanyInfo();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }        

        lblTitle.Text = Resources.Resource.ViewCompany;


        txtSearch.Focus();
    }


    protected void jgdvGlobalParameter_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 4)
        {
            string strurl = "AddEditCompany.aspx?CompanyID=" + e.RowValues[0];
            e.CellHtml = string.Format("<a href=\"{0}\">" + Resources.Resource.grdEdit + "</a>", strurl);
        }
        if (e.ColumnIndex == 5)
        {
            string delUrl = string.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_COMPANY, e.RowKey, "reloadGrid");
            e.CellHtml = string.Format(@"<a class=""pop_delete"" href=""{0}"">" + Resources.Resource.delete + "</a>", delUrl);
        }
    }

    protected void jgdvGlobalParameter_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains<string>("_history"))
        {
            sqldsCompany.SelectCommand = _objSysCompanyInfo.GetFilterQueryBySearchCriteria(sqldsCompany.SelectParameters, Request.QueryString[txtSearch.ClientID]);
        }
        else
        {
            sqldsCompany.SelectCommand = _objSysCompanyInfo.GetFilterQueryBySearchCriteria(sqldsCompany.SelectParameters, Request.QueryString[txtSearch.ClientID]);

        }
    }

    protected void btnAddCompany_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("~/Admin/AddEditCompany.aspx?t=0");
    }
}
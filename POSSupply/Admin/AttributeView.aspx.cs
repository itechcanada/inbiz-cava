﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_AttributeView : BasePage
{
    Attributes _attr = new Attributes();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Security Check TO DO

        if (!IsPagePostBack(grdAttribute)) { 

        }
    }

    protected void grdAttribute_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        int imgColIdx = 2;
        int activeColIdx = 3;

        if (e.ColumnIndex == 2)
        {
            string img = BusinessUtility.GetString(e.RowValues[imgColIdx]);
            if (!string.IsNullOrEmpty(img)) {
                e.CellHtml = string.Format(@"<img src=""{0}"" />", ResolveUrl(ImageUploadPath.ATTRIBUTE_IMAGE_ICONS + ThumbSizePrefix._32x32 + img));
            }
            else
            {
                e.CellHtml = "NA";
            }
        }
        else if (e.ColumnIndex == 3) { 

        }
        else if (e.ColumnIndex == 4)
        {
            e.CellHtml = string.Format(@"<a class=""pop_edit""  href=""AttributeEdit.aspx?attrid={0}&jscallback={1}"" >{2}</a>", e.RowValues[0], "reloadGrid", Resources.Resource.lblEdit);
        }
        if (e.ColumnIndex == 5)
        {
            string delUrl = string.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_ATTRIBUTE, e.RowKey, "reloadGrid");
            e.CellHtml = string.Format(@"<a class=""pop_delete"" href=""{0}"">{1}</a>", delUrl, Resources.Resource.delete);
        }
    }

    protected void grdAttribute_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains<string>("_history")) {
            sdsAttributes.SelectCommand = _attr.GetSql(sdsAttributes.SelectParameters, Request.QueryString[txtSearch.ClientID], Globals.CurrentAppLanguageCode);
        }
        else
        {
            sdsAttributes.SelectCommand = _attr.GetSql(sdsAttributes.SelectParameters, txtSearch.Text, Globals.CurrentAppLanguageCode);
        }
    }
}
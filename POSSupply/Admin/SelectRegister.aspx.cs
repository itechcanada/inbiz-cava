﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using System.Threading;

public partial class Admin_SelectRegister : BasePage
{
    SysRegister objRegister = new SysRegister();
    protected void Page_Load(object sender, EventArgs e)
    {

        //Security 
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.POS_USER) && !CurrentUser.IsInRole(RoleID.POS_MANAGER)) {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }
        

        //All Sessions are assigned using the query string passed at login time
        if (!string.IsNullOrEmpty(this.RegCode))
        {
            Session["RegCode"] = RegCode;            
            objRegister.PopulateObject(this.RegCode);
            if (!string.IsNullOrEmpty(objRegister.SysRegCode))
            {
                Session["RegMerchantID"] = objRegister.SysRegMerchantId;
                Session["host"] = objRegister.SysRegHost;
                Session["hostPort"] = objRegister.SysRegHostPort;
                Session["terminalID"] = objRegister.SysRegTerminalID;
                Session["RegName"] = objRegister.SysRegDesc;
                Session["RegLogFilePath"] = objRegister.SysRegLogFilePath.Replace("\\", "/");
                Session["RegMessage"] = objRegister.SysRegMessage;
                Session["RegWhsCode"] = objRegister.SysRegWhsCode;
                Response.Redirect("~/POS/POS.aspx");
            }
            else
            {
                Response.Redirect("SelectRegister.aspx");
            }
        }
        else
        {
            if (!Page.IsPostBack)
            {
                if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
                {
                    DropDownHelper.FillPosRegister(dlRegister, new ListItem(Resources.Resource.msgSelectRegister, "0"));
                    return;
                }


                DropDownHelper.FillPosRegister(dlRegister, CurrentUser.UserDefaultWarehouse, new ListItem(Resources.Resource.msgSelectRegister, "0"));
                if (dlRegister.Items.Count == 2 && !string.IsNullOrEmpty(dlRegister.Items[1].Value))
                {
                    objRegister.PopulateObject(dlRegister.Items[1].Value);
                    Session["RegCode"] = objRegister.SysRegCode;
                    Session["RegMerchantID"] = objRegister.SysRegMerchantId;
                    Session["host"] = objRegister.SysRegHost;
                    Session["hostPort"] = objRegister.SysRegHostPort;
                    Session["terminalID"] = objRegister.SysRegTerminalID;
                    Session["RegName"] = objRegister.SysRegDesc;
                    Session["RegLogFilePath"] = objRegister.SysRegLogFilePath.Replace("\\", "/");
                    Session["RegMessage"] = objRegister.SysRegMessage;
                    Session["RegWhsCode"] = objRegister.SysRegWhsCode;
                    Response.Redirect("~/POS/POS.aspx");
                }
            }
        }
    }
    
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid) {
            objRegister.PopulateObject(dlRegister.SelectedValue);
            Session["RegCode"] = objRegister.SysRegCode;
            Session["RegMerchantID"] = objRegister.SysRegMerchantId;
            Session["host"] = objRegister.SysRegHost;
            Session["hostPort"] = objRegister.SysRegHostPort;
            Session["terminalID"] = objRegister.SysRegTerminalID;
            Session["RegName"] = objRegister.SysRegDesc;
            Session["RegLogFilePath"] = objRegister.SysRegLogFilePath.Replace("\\", "/");
            Session["RegMessage"] = objRegister.SysRegMessage;
            Session["RegWhsCode"] = objRegister.SysRegWhsCode;
            Response.Redirect("~/POS/POS.aspx");
        }       
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        dlRegister.SelectedValue = "0";
    }

    private string RegCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["RegCode"]);
        }
    }
}
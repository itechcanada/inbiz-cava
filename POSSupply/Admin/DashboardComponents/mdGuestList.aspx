﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdGuestList.aspx.cs" Inherits="Admin_DashboardComponents_mdGuestList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div id="grid_wrapper">
        <trirand:JQGrid runat="server" ID="grdGuests" DataSourceID="sdsGuests" Height="200px"
            Width="100%" AutoWidth="true" PagerSettings-PageSize="50" 
            ondatarequesting="grdGuests_DataRequesting">
            <Columns>
                <trirand:JQGridColumn DataField="orderItemID" PrimaryKey="true" Visible="false" />
                <trirand:JQGridColumn DataField="PartnerLongName" HeaderText="Guest" TextAlign="Left" />
                <trirand:JQGridColumn DataField="orderItemDesc" HeaderText="Desc" TextAlign="Left" />               
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn="" />
            <AppearanceSettings AlternateRowBackground="True" />            
        </trirand:JQGrid>
        <asp:SqlDataSource ID="sdsGuests" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdGuests.ClientID %>";
        var $grid = $("#<%=grdGuests.ClientID %>");            
        

        //Function To Resize the grid
        function resize_the_grid() {
            $grid.fluidGrid({ example: '#grid_wrapper', offset: -2 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $grid.onJqGridPaging();
        }


        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $grid.onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);


        function reloadGrid(event, ui) {
            $grid.trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function loadGrid() {
            if (!grid_isloaded) {
                grid_isloaded = true;
            } else
                reloadGrid();
        }

        function gridLoadComplete(data) {
            $(window).trigger("resize");
        }

        function setSelectedRow(key) {
            var grid = $grid;
            grid.setSelection(key);
        }             
    </script>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using iTECH.Library.Utilities;

public partial class Admin_DashboardComponents_EventCalendar : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && grdEvents.AjaxCallBackMode == Trirand.Web.UI.WebControls.AjaxCallBackMode.None)
        {
            
        }
    }    

    protected void grdEvents_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        //List<TopEvents> lResult = new List<TopEvents>();
        //for (int i = 1; i < 5; i++)
        //{
        //    TopEvents ev = new TopEvents();
        //    ev.Date = DateTime.Today.AddDays(i - 1);
        //    ev.ID = i;
        //    string anc = @"<a class=""edit-Register""  href=""DashboardComponents/mdGuestList.aspx?ondate={0}&is={1}"" ></a>";
        //    ev.Description = string.Format("<div style='line-height:22px; font-size:12px;'><a class='list_guest' href='{0}'>{1} guests</a> checking in ", );
        //    ev.Description += string.Format("<br /><a href='#'>{1} guests</a> checking out</div>");
        //    lResult.Add(ev);
        //}

        grdEvents.DataSource = DashboardDataAPI.GetEvents();
    }
    protected void grdEvents_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 2)
        {
            string anc = @"<a class=""list_guest""  href=""DashboardComponents/mdGuestList.aspx?ondate={0}&is={1}"" >{2} guests</a>";
            e.CellHtml = "<div class='event_desc'>";
            e.CellHtml += string.Format(anc, Server.UrlEncode(BusinessUtility.GetDateTimeString((DateTime)e.RowValues[1], DateFormat.MMddyyyy)), "checkin", e.RowValues[2]);
            e.CellHtml += "checking in ";
            e.CellHtml += "<br>";
            e.CellHtml += string.Format(anc, Server.UrlEncode(BusinessUtility.GetDateTimeString((DateTime)e.RowValues[1], DateFormat.MMddyyyy)), "checkout", e.RowValues[3]);
            e.CellHtml += " checking out ";
            e.CellHtml += "</div>";
        }
    }
}
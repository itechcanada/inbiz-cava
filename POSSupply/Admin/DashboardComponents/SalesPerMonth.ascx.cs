﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;

using Newtonsoft.Json;

public partial class Admin_DashboardComponents_SalesPerMonth : System.Web.UI.UserControl
{    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DashboardSettings.FillWhsCods(rbtnOptions, new ListItem("All", ""));

            DbHelper dbHelp = new DbHelper(true);
            try
            {
                foreach (ListItem item in ddlSalesBy.Items)
                {
                    DashboardDataAPI.GetSalesCompairDataByMonths(dbHelp, ddlRange.SelectedValue, rbtnOptions.SelectedValue, item.Value);
                }
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }            
        }
    }    
}
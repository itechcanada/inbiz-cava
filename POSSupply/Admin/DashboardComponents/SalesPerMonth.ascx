﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SalesPerMonth.ascx.cs" Inherits="Admin_DashboardComponents_SalesPerMonth" %>
<asp:Panel ID="pnlCommands" runat="server" Height="60px">
    <div style="margin:2px 0px;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:DropDownList ID="ddlRange" runat="server" Style="padding: 4px;" Width="150px">
                        <asp:ListItem Text="12m" Value="12" />                        
                        <asp:ListItem Text="6m" Value="6" />
                        <%--<asp:ListItem Text="Sales 2012 vs 2011" Value="2012-2013" />--%> 
                    </asp:DropDownList>                    
                </td>
                <td style="text-align: right;">
                    <asp:RadioButtonList ID="rbtnOptions" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                        <%--<asp:ListItem Value="" Text="All" Selected="True" />
                        <asp:ListItem Text="Store1" />
                        <asp:ListItem Text="Online" />--%>
                    </asp:RadioButtonList>
                </td>
            </tr> 
            <tr>
                <td colspan="2">
                    <asp:DropDownList ID="ddlSalesBy" runat="server" Style="padding: 4px;" Width="250px">
                        <asp:ListItem Text="Total Sales" Value="" />
                        <asp:ListItem Text="Product Sales - POS" Value="POS" />
                        <asp:ListItem Text="Product Sales - Whole Sale" Value="SALE" />
                        <asp:ListItem Text="Product Sales - Online" Value="ONLINE" />
                        <asp:ListItem Text="Accommodation Sales - Total" Value="ATOTAL" />
                        <asp:ListItem Text="Accommodation Sales - Online" Value="AONLINE" />
                    </asp:DropDownList>
                </td>              
            </tr>           
        </table>                
    </div>
</asp:Panel>
<asp:Panel ID="pnlChart" CssClass="" runat="server" Height="270px" style="border:1px solid #ccc; margin-bottom:5px;">
    
</asp:Panel>
<asp:Panel ID="pnlScripts" runat="server">    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=rbtnOptions.ClientID%>").buttonset();                
            initSalesChartByMonth($("#<%=pnlChart.ClientID%>"), $("#<%=ddlRange.ClientID%>").val(), "", "", false, false);

            $("#<%=ddlRange.ClientID%>").change(function () {               
                salesSettingChanged();
            });

            $("#<%=rbtnOptions.ClientID%> :radio").click(function () {
                salesSettingChanged();                
            });            

            $("#<%=ddlSalesBy.ClientID%>").change(function () {
                salesSettingChanged();
            });
        });

        function salesSettingChanged() {
            var whs = "";
            var salesBy = $("#<%=ddlSalesBy.ClientID%>").val();           
            $("#<%=rbtnOptions.ClientID%> :radio").each(function () {
                if ($(this).is(':checked')) {
                    whs = $(this).val();
                }
            });            
            
            callbackSalesChartData($("#<%=pnlChart.ClientID%>"), $("#<%=ddlRange.ClientID%>").val(), whs, salesBy, true);
        }
    </script>
</asp:Panel>

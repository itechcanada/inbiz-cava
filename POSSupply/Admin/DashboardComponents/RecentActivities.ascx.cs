﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using iTECH.Library.Utilities;

public partial class Admin_DashboardComponents_RecentActivities : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && grdEvents.AjaxCallBackMode == Trirand.Web.UI.WebControls.AjaxCallBackMode.None)
        {

        }
    }

    protected void grdEvents_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        grdEvents.DataSource = DashboardDataAPI.GetActivities();
    }
    protected void grdEvents_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 1)
        {
            //string anc = @"<a class=""list_guest""  href=""DashboardComponents/mdGuestList.aspx?ondate={0}&is={1}"" >{2} guests</a>";
            //e.CellHtml = "<div class='event_desc'>";
            //e.CellHtml += string.Format(anc, Server.UrlEncode(BusinessUtility.GetDateTimeString((DateTime)e.RowValues[1], DateFormat.MMddyyyy)), "checkin", e.RowValues[2]);
            //e.CellHtml += "checking in ";
            //e.CellHtml += "<br>";
            //e.CellHtml += string.Format(anc, Server.UrlEncode(BusinessUtility.GetDateTimeString((DateTime)e.RowValues[1], DateFormat.MMddyyyy)), "checkout", e.RowValues[3]);
            //e.CellHtml += " checking out ";
            //e.CellHtml += "</div>";
            e.CellHtml = "<div class='event_desc'>";
            switch (e.RowValues[1].ToString())
            {
                case "RSV":
                    e.CellHtml += string.Format("A Reservation of <b> {0} {1:F}</b> made by {2} on {3:MMM d, yyyy h:m:tt}", e.RowValues[2], e.RowValues[3], e.RowValues[7], e.RowValues[5]);
                    break;
                default:
                    e.CellHtml += string.Format("A Sale of <b> {0} {1:F}</b> made by {2} on {3:MMM d, yyyy h:m:tt}", e.RowValues[2], e.RowValues[3], e.RowValues[7], e.RowValues[5]);
                    break;
            }
            e.CellHtml += "</div>";
        }
    }
}
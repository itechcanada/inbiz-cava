﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using Trirand.Web.UI.WebControls;

public partial class Admin_DashboardComponents_mdGuestList : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void grdGuests_DataRequesting(object sender, JQGridDataRequestEventArgs e)
    {
        DateTime onDate = BusinessUtility.GetDateTime(Request.QueryString["ondate"], DateFormat.MMddyyyy);
        bool isCheckIn = Request.QueryString["is"] == "checkin";
        sdsGuests.SelectCommand = DashboardDataAPI.GetguestListSql(sdsGuests.SelectParameters, onDate, isCheckIn);
    }
}
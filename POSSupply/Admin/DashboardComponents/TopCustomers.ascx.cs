﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

using Newtonsoft.Json;

public partial class Admin_DashboardComponents_TopCustomers : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DashboardSettings.FillWhsCods(rbtnOptions, new ListItem("All", ""));
        }
    }
}
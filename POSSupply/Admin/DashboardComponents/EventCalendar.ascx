﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EventCalendar.ascx.cs" Inherits="Admin_DashboardComponents_EventCalendar" %>
<asp:Panel ID="pnlChart" CssClass="" runat="server" Height="330px" style="margin:2px 0px 5px 0px;">
    <trirand:JQGrid runat="server" ID="grdEvents" Height="325px"
        AutoWidth="True" ondatarequesting="grdEvents_DataRequesting" 
        oncellbinding="grdEvents_CellBinding">
        <Columns>
            <trirand:JQGridColumn DataField="ID" HeaderText="" PrimaryKey="True" Visible="false" />
            <trirand:JQGridColumn CssClass="date_column" DataField="Date" HeaderText=""
                Editable="false" DataFormatString="{0:ddd MMM dd}" Width="70"  />
            <trirand:JQGridColumn DataField="ID" HeaderText=""
                Editable="false" />            
        </Columns>
        <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" 
            ScrollBarPaging="True" />
        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
            ShowDeleteButton="false" ShowSearchButton="false" />
        <SortSettings InitialSortColumn=""></SortSettings>
        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="false" />        
        <ClientSideEvents GridInitialized="grdInited" BeforeRowSelect="grdRowSelect" />
    </trirand:JQGrid>
    <iCtrl:IframeDialog ID="mdViewGuest" Width="800" Height="350" Title="List" Dragable="true"
        TriggerSelectorClass="list_guest" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
        runat="server">
    </iCtrl:IframeDialog>
</asp:Panel>
<asp:Panel ID="pnlScripts" runat="server">    
    
</asp:Panel>
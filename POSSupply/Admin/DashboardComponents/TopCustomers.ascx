﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopCustomers.ascx.cs" Inherits="Admin_DashboardComponents_TopCustomers" %>
<asp:Panel ID="pnlCommands" runat="server" Height="30px">
    <div style="margin:2px 0px;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:DropDownList ID="ddlRange" runat="server" Style="padding: 4px; width:80px;">
                        <asp:ListItem Value="12" Text="12m" />
                        <asp:ListItem Value="6" Text="6m" />
                    </asp:DropDownList>
                </td>
                <td style="text-align: right;">
                    <asp:RadioButtonList ID="rbtnOptions" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                        <%--<asp:ListItem Value="" Text="All" Selected="True" />
                        <asp:ListItem Text="Store1" />
                        <asp:ListItem Text="Online" />--%>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>        

    </div>
</asp:Panel>
<asp:Panel ID="pnlChart" CssClass="" runat="server" Height="300px" style="border:1px solid #ccc; margin-bottom:5px;">
    
</asp:Panel>
<asp:Panel ID="pnlScripts" runat="server">    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=rbtnOptions.ClientID%>").buttonset();
            initTopCustomerChartData($("#<%=pnlChart.ClientID%>"),  $("#<%=ddlRange.ClientID%>").val(), "");

            $("#<%=ddlRange.ClientID%>").change(function () {
                var whs = "";
                $("#<%=rbtnOptions.ClientID%> :radio").each(function () {
                    if ($(this).is(':checked')) {
                        whs = $(this).val();
                    }
                });
                callbackTopCustomerChartData($("#<%=pnlChart.ClientID%>"),  $("#<%=ddlRange.ClientID%>").val(), whs, true);
            });

            $("#<%=rbtnOptions.ClientID%> :radio").click(function () {
                callbackTopCustomerChartData($("#<%=pnlChart.ClientID%>"),  $("#<%=ddlRange.ClientID%>").val(), $(this).val(), true);
            });
        });
    </script>
</asp:Panel>

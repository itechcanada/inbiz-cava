﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccountReceivable.ascx.cs" Inherits="Admin_DashboardComponents_AccountReceivable" %>
<asp:Panel ID="pnlCommands" runat="server" Height="30px">
    <div style="margin:2px 0px;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    
                </td>
                <td style="text-align: right;">
                    <asp:RadioButtonList ID="rbtnOptions" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                        <%--<asp:ListItem Value="" Text="All" Selected="True" />
                        <asp:ListItem Text="Store1" />
                        <asp:ListItem Text="Online" />--%>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
<asp:Panel ID="pnlChart" CssClass="" runat="server" Height="300px" style="border:1px solid #ccc; margin-bottom:5px;">
</asp:Panel>
<asp:Panel ID="pnlScripts" runat="server">
    <script id="scriptInit" type="text/javascript">
        $(document).ready(function () {
            $("#<%=rbtnOptions.ClientID%>").buttonset();
            initRcvPieChartData($("#<%=pnlChart.ClientID%>"), "");

            $("#<%=rbtnOptions.ClientID%> :radio").click(function () {
                callbackRcvPieChartData($("#<%=pnlChart.ClientID%>"), $(this).val(), true);
            });
        });
	</script>
</asp:Panel>
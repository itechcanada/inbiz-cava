﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecentActivities.ascx.cs" Inherits="Admin_DashboardComponents_RecentActivities" %>
<asp:Panel ID="pnlChart" CssClass="" runat="server" Height="330px" style="margin:2px 0px 5px 0px;">
    <trirand:JQGrid runat="server" ID="grdEvents" Height="325px"
        AutoWidth="True" ondatarequesting="grdEvents_DataRequesting" 
        oncellbinding="grdEvents_CellBinding">
        <Columns>
            <trirand:JQGridColumn DataField="ID" HeaderText="" PrimaryKey="True" Visible="false" />
            <trirand:JQGridColumn DataField="ID" HeaderText="" Editable="false" />
        </Columns>
        <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" 
            ScrollBarPaging="True" />
        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
            ShowDeleteButton="false" ShowSearchButton="false" />
        <SortSettings InitialSortColumn=""></SortSettings>
        <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="false" />        
        <ClientSideEvents GridInitialized="grdInited" BeforeRowSelect="grdRowSelect" />
    </trirand:JQGrid>    
</asp:Panel>
<asp:Panel ID="pnlScripts" runat="server">    
    
</asp:Panel>
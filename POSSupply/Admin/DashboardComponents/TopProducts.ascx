﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopProducts.ascx.cs" Inherits="Admin_DashboardComponents_TopProducts" %>

<asp:Panel ID="pnlCommands" runat="server" Height="60px">
    <div style="margin:2px 0px;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:DropDownList ID="ddlRange" runat="server" Style="padding: 4px; width:80px;">
                        <asp:ListItem Value="12" Text="12m" />
                        <asp:ListItem Value="6" Text="6m" />
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlBy" runat="server" Style="padding: 4px; width:100px;">
                        <asp:ListItem Value="sale" Text="Sales" />
                        <asp:ListItem Value="qty" Text="Units Sold" />
                    </asp:DropDownList>
                </td>
                <td style="text-align: right;">
                    <asp:RadioButtonList ID="rbtnOptions" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">                       
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:DropDownList ID="ddlSalesBy" runat="server" Style="padding: 4px;" Width="250px">
                        <asp:ListItem Text="Products - Total" Value="PTOTAL" />
                        <asp:ListItem Text="Products - Online" Value="PONLINE" />
                        <asp:ListItem Text="Accommodations - Total" Value="ATOTAL" />
                        <asp:ListItem Text="Accommodations - Online" Value="AONLINE" />
                        <asp:ListItem Text="Top Products By AvgGP" Value="AVGGP" />
                    </asp:DropDownList>
                </td>               
            </tr>   
        </table>        

    </div>
</asp:Panel>
<asp:Panel ID="pnlChart" CssClass="" runat="server" Height="270px" style="border:1px solid #ccc; margin-bottom:5px;">
    
</asp:Panel>
<asp:Panel ID="pnlScripts" runat="server">    
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=rbtnOptions.ClientID%>").buttonset();
            initTopProductsChartData($("#<%=pnlChart.ClientID%>"), $("#<%=ddlRange.ClientID%>").val(), $("#<%=ddlBy.ClientID%>").val(), "", $("#<%=ddlSalesBy.ClientID%>").val(), false);

            $("#<%=ddlRange.ClientID%>").change(function () {                
                topProductsSettingsChanged();
            });

            $("#<%=ddlBy.ClientID%>").change(function () {                
                topProductsSettingsChanged();
            });

            $("#<%=rbtnOptions.ClientID%> :radio").click(function () {                
                topProductsSettingsChanged();
            });            

            $("#<%=ddlSalesBy.ClientID%>").change(function () {
                topProductsSettingsChanged();
            });
        });

        function topProductsSettingsChanged() {
            var whs = "";
            var salesBy = $("#<%=ddlSalesBy.ClientID%>").val();
            var isWebSale = false;
            $("#<%=rbtnOptions.ClientID%> :radio").each(function () {
                if ($(this).is(':checked')) {
                    whs = $(this).val();
                }
            });
            
            callbackTopProductsChartData($("#<%=pnlChart.ClientID%>"), $("#<%=ddlRange.ClientID%>").val(), $("#<%=ddlBy.ClientID%>").val(), whs, salesBy, true);
        }
    </script>
</asp:Panel>
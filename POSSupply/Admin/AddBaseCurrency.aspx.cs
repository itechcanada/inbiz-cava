﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_AddBaseCurrency : BasePage
{
    SysCompanyInfo objCompany = new SysCompanyInfo();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security check for authorization
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }        

        btnSave.Attributes.Add("onclick", "javascript:return " + "confirm('" + Resources.Resource.msgNewChagesOnNextLogin + " ')");
        if (!IsPostBack)
        {
            string strCurValue = objCompany.GetBaseCurrency();
            DropDownHelper.FillCurrencyDropDown(dlCurrencyCode, new ListItem(Resources.Resource.lstSelectCurrency, "0"));

            if (!string.IsNullOrEmpty(strCurValue))
            {
                dlCurrencyCode.SelectedValue = strCurValue;
            }
        }
    }
    
    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        if (!Page.IsValid)
        {
            return;
        }
        objCompany.CompanyBasCur = dlCurrencyCode.SelectedValue;
        objCompany.UpdateBaseCurrency(objCompany.CompanyBasCur);

        SysCurrencies objCurrencies = new SysCurrencies();
        objCurrencies.CurrencyCode = objCompany.CompanyBasCur;
        objCurrencies.CurrencyRelativePrice = 1;
        objCurrencies.CurrencyActive = true;
        objCurrencies.Update();
        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.CompanyBaseCurrencyAddedSuccessfully);        
        //Response.Redirect("../Adminlogin.aspx")
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Trirand.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;

public partial class Admin_ViewInternalUser : BasePage
{        
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsJqGridCall(grdUsers)) {
            if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
            {
                lnkAddInternalUser.Text = Resources.Resource.AddInternalUser;
                lblTitle.Text = Resources.Resource.ViewInternalUser;
            }
        }       
        
        if (!IsPagePostBack(grdUsers))
        {            
            DropDownHelper.FillDropdown(ddlStatus, "US", "dlSea", Globals.CurrentAppLanguageCode, null);
        }
        txtSearch.Focus();
    }

    protected void lnkAddInternalUser_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("~/Admin/AddEditUser.aspx");
    }

    protected void  grdUsers_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 5)
        {
            string strurl = "AddEditUser.aspx?UserID=" + e.RowValues[0];
            e.CellHtml = string.Format("<a href=\"{0}\">" + Resources.Resource.grdEdit + "</a>", strurl);
        }
        else if (e.ColumnIndex == 6)
        {
             string delUrl = string.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_USER, e.RowKey, "reloadGrid");
            e.CellHtml = string.Format(@"<a class=""pop_delete"" href=""{0}"">{1}</a>", delUrl, Resources.Resource.delete);
        }
    }

    InbizUser _objUser = new InbizUser();
    protected void  grdUsers_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains<string>("_history"))
        {
            sqldsInternalUser.SelectCommand = _objUser.GetSql(sqldsInternalUser.SelectParameters, Request.QueryString[ddlStatus.ClientID], Request.QueryString[txtSearch.ClientID]);
        }
        else
        {
            sqldsInternalUser.SelectCommand = _objUser.GetSql(sqldsInternalUser.SelectParameters, Request.QueryString[ddlStatus.ClientID], Request.QueryString[txtSearch.ClientID]);
        }
    }
}
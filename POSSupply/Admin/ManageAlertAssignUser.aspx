﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="ManageAlertAssignUser.aspx.cs" Inherits="Admin_ManageAlertAssignUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div id="dialogAssignUser" style="padding:5px;">
        <h3>            
            <asp:Label ID="Label1" Text="<%$Resources:Resource, lblUsers%>" runat="server" />
        </h3>
        <asp:ListBox CssClass="modernized_select" ID="ddlUsers" runat="server" Width="600px" data-placeholder="<%$ Resources:Resource, lblSelectUser %>"
            SelectionMode="Multiple"></asp:ListBox>
    </div>
    <div class="div_command">
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" 
            onclick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, Cancel%>" CausesValidation="false"
            OnClientClick="jQuery.FrameDialog.closeDialog();" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


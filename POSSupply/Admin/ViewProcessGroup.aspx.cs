﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.WebControls;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;

public partial class Admin_ViewProcessGroup : BasePage
{    
    private SysProcessGroup _objProGrp = new SysProcessGroup();
    // On Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            btnCreateProcesGroup.Visible = false;
        }

        if (!IsPagePostBack(jgdvProcess)) {
            lblTitle.Text = Resources.Resource.ViewProcess;
            txtSearch.Focus();
        }               
    }

    protected void jgdvProcess_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 5)
        {
            e.CellHtml = string.Format(@"<a class=""edit-Process""  href=""AddEditProcess.aspx?ProcessID={0}&jscallback={1}"" >Edit</a>", e.RowValues[0], "reloadGrid");
        }
        if (e.ColumnIndex == 6)
        {
            string delUrl = string.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_PRCESS_GROUP, e.RowKey, "reloadGrid");
            e.CellHtml = string.Format(@"<a class=""pop_delete"" href=""{0}"">Delete</a>", delUrl);
        }
    }

    protected void jgdvProcess_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        sqldsProcess.SelectCommand = _objProGrp.GetSql(sqldsProcess.SelectParameters, Request.QueryString[txtSearch.ClientID]);
    }
}
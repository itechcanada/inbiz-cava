﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ViewVendor.aspx.cs" Inherits="Admin_ViewVendor" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="cphHead" runat="Server">
   <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <%= Resources.Resource.lblView%>:
            <%= Resources.Resource.lblVendor%>
        </b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <%--:Flag_Amit :--%>
        <asp:Button ID="btnAddVender" CausesValidation="false" runat="server" Text="<%$ Resources:Resource,btnimgAddVendor%>" OnClick="btnAddVender_Click" />
        <%--:Flag_Amit :--%>
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif" CssClass="csv_export" />        
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="cntViewVendor" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="jgdvVendor" DataSourceID="sqldsVendor" Height="300px"
                    AutoWidth="True" OnCellBinding="jgdvVendor_CellBinding" OnDataRequesting="jgdvVendor_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="vendorID" HeaderText="" PrimaryKey="True" Visible="false" />
                        <trirand:JQGridColumn DataField="vendorName" HeaderText="<%$ Resources:Resource, grdVendorName %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="vendorEmailID" HeaderText="<%$ Resources:Resource, grdVendorEmailID %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="vendorFax" HeaderText="<%$ Resources:Resource, grdVendorFax %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="vendorPhone" HeaderText="<%$ Resources:Resource, grdVendorPhone %>"
                            Editable="false" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="vendorID"
                            Sortable="false" TextAlign="Center" Width="50" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="vendorID"
                            Sortable="false" TextAlign="Center" Width="50" Visible="false" />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents LoadComplete="gridLoadComplete" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sqldsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
                <iCtrl:IframeDialog ID="mdDelProcess" Width="400" Height="120" Title="Delete" Dragable="true"
                    TriggerSelectorClass="pop_delete" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
                    runat="server">
                </iCtrl:IframeDialog>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $grid = $("#<%=jgdvVendor.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=jgdvVendor.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }       

        //Fire after load complete of jqgrid
        function gridLoadComplete(data) {
            if (data.rows.length == 1 && data.total == 1) {
                location.href = 'AddEditVendor.aspx?vendorID=' + data.rows[0].id;
            }
            else {
                jqGridResize();
            }
        }   
    </script>
</asp:Content>
<asp:Content ID="cntLeft" ContentPlaceHolderID="cphLeft" runat="Server">
    <div >
        <asp:Panel runat="server" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                         <%= Resources.Resource.lblSearchForm%></div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lblStatus %>"
                        AssociatedControlID="ddlvendorStatus" CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlvendorStatus" runat="server" Width="175px" >
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>" CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>


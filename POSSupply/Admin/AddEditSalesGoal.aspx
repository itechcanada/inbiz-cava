﻿<%@ Page Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="AddEditSalesGoal.aspx.cs" Inherits="Admin_AddEditSalesGoal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
         <div id="contentBottom" style="padding: 5px; height: 210px; overflow: auto;" onkeypress="return disableEnterKey(event)">
        <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td class="text">
                        <asp:Label ID="lblFromDate" CssClass="lblBold" Text="<%$ Resources:Resource, lblRptFromDate%>"
                            runat="server" />*
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtFromDate" runat="server" Width="90px" CssClass="datepicker" Enabled = "true" />
                         <asp:RequiredFieldValidator ID="reqvalFromDate" runat="server" ControlToValidate="txtFromDate"
                        ErrorMessage="<%$ Resources:Resource, msgRptPlzEntFromDate %>" SetFocusOnError="true" 
                        Display="None"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                       <asp:Label ID="lblToDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblRptToDate%>">
                        </asp:Label>*
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtToDate" runat="server" Width="90px" CssClass="datepicker" />
                         <asp:RequiredFieldValidator ID="reqvalToDate" runat="server" ControlToValidate="txtToDate"
                        ErrorMessage="<%$ Resources:Resource, msgRptPlzEntToDate %>" SetFocusOnError="true" 
                        Display="None">
                    </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label ID="lblSalesAmount" Text="<%$Resources:Resource, lblSalesAmountPerDay %>" 
                            runat="server">
                        </asp:Label>*
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtSalesAmount" runat="server" MaxLength="20" Width="90px"  CssClass="numericTextField" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalSalPriceMinQua" runat="server" ControlToValidate="txtSalesAmount"
                    ErrorMessage="<%$ Resources:Resource, reqvalSalPriceMinQua %>" SetFocusOnError="true"  >*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                 <asp:ValidationSummary ID="valsReport" runat="server" ShowMessageBox="true" ShowSummary="false" />
            </table>
            <div class="div_command">
                <asp:Button ID="btnSave" runat="server" Text="<%$Resources:Resource,cmdCssSave%>"
                     OnClick="btnSave_Click" />
                <asp:Button ID="btnCancel" Text="<%$Resources:Resource, btnCancel%>" runat="server"
                    CausesValidation="false" OnClientClick="jQuery.FrameDialog.closeDialog();" />
            </div>
        </div>
    </div>
</div></asp:Content><asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
<script type="text/javascript">
//$(document)
</script>
</asp:Content>

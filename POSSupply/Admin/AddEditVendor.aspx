﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="AddEditVendor.aspx.cs" Inherits="Admin_AddEditVendor" %>


<asp:Content ID="Content5" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" alt="" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.Administration %></h1>
        <b>
            <%= Resources.Resource.lblAddEdit %>
            :<%= Resources.Resource.lblVendor %></b>
    </div>
    <div>
        <asp:Button ID="btnProduct" CausesValidation="false" runat="server" Text="<%$ Resources:Resource,lblProducts%>"
            OnClick="btnProduct_Click" Visible="false" />
        <asp:Button ID="btnHistroy" CausesValidation="false" runat="server" Text="<%$ Resources:Resource,btnPOHistory%>"
            OnClick="btnHistroy_Click" />
    </div>
    <div class="clear">
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>        
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntAddEditVendor" ContentPlaceHolderID="cphMaster" runat="Server">
    <div >
        <div >
            <h3>
                <%= Resources.Resource.lblVendorInformation%>
            </h3>
            <ul class="form">
                <li>
                    <div class="lbl">
                        <asp:Label Text="<%$Resources:Resource, lblVendorName %>"  ID="lblVendorName" runat="server" />
                        *</div>
                    <div class="input">
                        <asp:TextBox ID="txtvendorName" runat="server" MaxLength="50" />
                        <asp:RequiredFieldValidator ID="reqvalVendorName" ErrorMessage="<%$ Resources:Resource, reqvalVendorName%>" runat="server" ControlToValidate="txtvendorName"
                            SetFocusOnError="true" Display="None" ValidationGroup="SaveVendor"></asp:RequiredFieldValidator>
                    </div>
                    <div class="lbl">
                        <asp:Label Text="<%$Resources:Resource, lblVendorPhone %>"  ID="lblVendorPhone" runat="server" />
                        *</div>
                    <div class="input">
                        <asp:TextBox ID="txtvendorPhone" runat="server" MaxLength="25" />
                        <asp:RequiredFieldValidator ID="reqvalVendorPhone" ErrorMessage="<%$ Resources:Resource, reqvalRegCode%>" runat="server" ControlToValidate="txtvendorPhone"
                            SetFocusOnError="true" Display="None" Enabled="false" ValidationGroup="SaveVendor"></asp:RequiredFieldValidator>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label Text="<%$Resources:Resource, lblVendorFax %>"  ID="lblVendorFax" runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtvendorFax" runat="server" MaxLength="15" />
                    </div>
                    <div class="lbl">
                        <asp:Label Text="<%$Resources:Resource, lblVendorEmailID %>"  ID="lblVendorEmailID" runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtvendorEmailID" runat="server" MaxLength="75" />
                        <asp:RegularExpressionValidator ID="revalVendorEmailID" runat="server" ControlToValidate="txtvendorEmailID"
                            Display="None" ErrorMessage="<%$ Resources:Resource, reqvalVendorEmailID%>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ValidationGroup="SaveVendor"></asp:RegularExpressionValidator>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label Text="<%$Resources:Resource, lblVendorPOPref %>"  ID="lblVendorPOPref" runat="server" />
                    </div>
                    <div class="input">
                        <asp:DropDownList ID="ddlVendorPOPref" runat="server" Width="145px">
                        </asp:DropDownList>
                    </div>
                    <div class="lbl">
                        <asp:Label Text="<%$Resources:Resource, lblVendorStatus %>"  ID="lblVendorStatus" runat="server" />
                    </div>
                    <div class="input">
                        <asp:DropDownList ID="ddlVendorStatus" runat="server" Width="145px">
                        </asp:DropDownList>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label Text="<%$Resources:Resource, lblVendorCurrency %>"  ID="lblVendorCurrency" runat="server" />
                        *</div>
                    <div class="input">
                        <asp:DropDownList ID="dlCurrencyCode" runat="server" Width="145px">
                        </asp:DropDownList>
                        <asp:CustomValidator ID="custvalVendorCurrencyCode" runat="server" SetFocusOnError="true"
                            ErrorMessage="<%$ Resources:Resource, custvalResellerCurrencyCode%>" ClientValidationFunction="funCheckCurrencyCode"
                            Display="None" ValidationGroup="SaveVendor"></asp:CustomValidator>
                    </div>
                    <div class="lbl">
                        <asp:Label Text="<%$Resources:Resource, lblIsValidated %>"  ID="lblIsValidated" runat="server" />
                    </div>
                    <div class="input">
                        <asp:RadioButtonList ID="rblstValidated" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="<%$ Resources:Resource, lblYes%>" Value="1">
                            </asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lblNo%>" Value="0" Selected="True">
                            </asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label Text="<%$Resources:Resource, lblVedContactPersonName %>"  ID="lblVedContactPersonName" runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtContPersonName" runat="server" MaxLength="50" />
                    </div>
                    <div class="lbl">
                        <asp:Label Text="<%$Resources:Resource, lblLanguage %>"  ID="lblLanguage" runat="server" />
                    </div>
                    <div class="input">
                        <asp:DropDownList ID="ddlLanguage" runat="server" Width="145px">
                            <asp:ListItem Value="en" Text="<%$ Resources:Resource, English %>" />
                            <asp:ListItem Value="fr" Text="<%$ Resources:Resource, French %>" />
                            <asp:ListItem Value="sp" Text="<%$ Resources:Resource, Spanish %>" />
                        </asp:DropDownList>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label Text="<%$Resources:Resource, lblVedContactPersonPhone %>"  ID="lblVedContactPersonPhone" runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtContactPersonPhone" runat="server" MaxLength="25" />
                    </div>
                    <div class="lbl">
                        <asp:Label Text="<%$Resources:Resource, lblVedContactPersonEmail %>"  ID="lblVedContactPersonEmail" runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtContactPersonEmailID" runat="server" MaxLength="65" />
                        <asp:RegularExpressionValidator ID="reqexpContactPersonEmailID" runat="server" ControlToValidate="txtContactPersonEmailID"
                            Display="None" ErrorMessage="<%$ Resources:Resource, reqvalVendorCOntactpersonEmailID%>"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="SaveVendor"></asp:RegularExpressionValidator>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li id="trvendorActive" runat="server" visible="false">
                    <div class="lbl">

                        <asp:Label Text="<%$Resources:Resource, lblIsActive %>"  ID="lblIsActive" runat="server" />

                    </div>
                    <div class="input">
                        <asp:RadioButton Text="<%$Resources:Resource, lblYes %>" ID="rdbYes" runat="server" Checked="true" GroupName="venActive" />
                        <asp:RadioButton Text="<%$Resources:Resource, lblNo %>" ID="rdbNo" runat="server" GroupName="venActive" />
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
            </ul>
            
            <%--Head Quater Address Start--%>
            <h3>
                <asp:Label ID="lblAddressTitle" Text="<%$Resources:Resource, lblHeadQuaterAddress %>" runat="server"></asp:Label>
            </h3>
            <iCtrl:Address ID="ucHeadQuaterAddress" runat="server" ValidationGroup="SaveVendor" />
            <%--Head Quater Address End--%>
            <%--Bill To Address Start--%>
             <h3>
                <asp:Label ID="Label1" Text="<%$Resources:Resource, lblBillToAddress %>" runat="server"></asp:Label>
            </h3>
            <iCtrl:Address ID="ucBillToAddress" runat="server" />
            <%--Bill To Address End--%>
            <%--Ship To Address Start--%>
            <h3>
                <asp:Label ID="Label2" Text="<%$Resources:Resource, lblShpToAddress %>" runat="server"></asp:Label>
            </h3>
            <iCtrl:Address ID="ucShpToAddress" runat="server" />
            <%--Ship To Address End--%>            
            <br />
            <div class="div-dialog-command">
                <asp:Button ID="btnDelete" Text="<%$ Resources:Resource, delete %>" runat="server"
                    CausesValidation="false" OnClientClick="return confimrDelete();" OnClick="btnDelete_Click" />
                <asp:Button Text="<%$Resources:Resource, btnSave %>" ID="btnSave" runat="server"
                    ValidationGroup="SaveVendor" OnClick="btnSave_Click" />
                <asp:Button Text="<%$Resources:Resource, btnCancel %>"  ID="btnCancel" CausesValidation="false" runat="server" OnClick="btnCancel_Click" />
            </div>
            <asp:ValidationSummary ID="valsAdminvendor" runat="server" ShowMessageBox="true"
                ShowSummary="false" ValidationGroup="SaveVendor" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script language="javascript" type="text/javascript">
        function funCheckCurrencyCode(source, args) {
            if (document.getElementById('<%=dlCurrencyCode.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }

        function confimrDelete() {
            return confirm("<%=Resources.Resource.confirmDelete%>");
        }
    </script>
</asp:Content>



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.DataAccess.MySql;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using System.Data;

public partial class Admin_AddEditSalesGoal : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (this.SalesGoalID > 0 && this.SalesGoalUserID > 0)
                {
                    UserSalesGoal objUserSalesGoal = new UserSalesGoal();
                    DataTable dt = objUserSalesGoal.GetSalesGoalList(null, this.SalesGoalUserID, this.SalesGoalID);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            DateTime fromDate = BusinessUtility.GetDateTime(dt.Rows[0]["fromDate"]);
                            DateTime toDate = BusinessUtility.GetDateTime(dt.Rows[0]["toDate"]);
                            txtFromDate.Text = fromDate.ToString("MM/dd/yyyy");
                            txtToDate.Text = toDate.ToString("MM/dd/yyyy");
                            txtSalesAmount.Text = BusinessUtility.GetDouble(dt.Rows[0]["salesAmountPerDay"]).ToString();
                        }
                    }
                }
                if (this.SalesGoalID > 0 && !string.IsNullOrEmpty(this.SalesGoalWhsCode))
                {
                    WarehouseSalesGoal objWhsSalesGoal = new WarehouseSalesGoal();
                    DataTable dt = objWhsSalesGoal.GetSalesGoalList(null, this.SalesGoalWhsCode, this.SalesGoalID);
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            DateTime fromDate = BusinessUtility.GetDateTime(dt.Rows[0]["fromDate"]);
                            DateTime toDate = BusinessUtility.GetDateTime(dt.Rows[0]["toDate"]);
                            txtFromDate.Text = fromDate.ToString("MM/dd/yyyy");
                            txtToDate.Text = toDate.ToString("MM/dd/yyyy");
                            txtSalesAmount.Text = BusinessUtility.GetDouble(dt.Rows[0]["salesAmountPerDay"]).ToString();
                        }
                    }
                }
                //txtFromDate.Focus();

            }
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }
    private int SalesGoalID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["IDSalesGoal"], out id);
            return id;
        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DateTime fDate = BusinessUtility.GetDateTime(txtFromDate.Text, DateFormat.MMddyyyy);
        DateTime tDate = BusinessUtility.GetDateTime(txtToDate.Text, DateFormat.MMddyyyy);
        tDate = tDate.AddHours(23).AddMinutes(59).AddSeconds(59);

        if (fDate == DateTime.MinValue)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgRptPlzEntFromDate);
            return;
        }
        if (tDate == DateTime.MinValue)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgRptPlzEntToDate);
            return;
        }
        if (fDate > tDate)
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgRptPlzEntFromDateShouldLessThanToDate);
            return;
        }
        if (IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                UserSalesGoal objUSG = new UserSalesGoal();
                WarehouseSalesGoal objWSG = new WarehouseSalesGoal();

                if (this.SalesGoalUserID > 0)
                {
                    objUSG.SalesGoalUserID = this.SalesGoalUserID;
                    objUSG.idUserSalesGoal = this.SalesGoalID;
                    objUSG.FromDate = fDate;
                    objUSG.ToDate = BusinessUtility.GetDateTime(txtToDate.Text, DateFormat.MMddyyyy);
                    objUSG.SalesAmountPerDay = BusinessUtility.GetDouble(txtSalesAmount.Text);
                    if (objUSG.Save(dbHelp) == true)
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.lblClockInSaved);

                        if (IsCallFromPOS == false && !string.IsNullOrEmpty(this.JsCallBackFunction))
                        {
                            Globals.RegisterCloseDialogScript(this, "parent.reloadGrid();", true);
                            //Globals.RegisterCloseDialogScript(this, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                        }
                        else if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                        {
                            Globals.RegisterCloseDialogScript(this, "parent.redirectToPOS();", true);
                            //Globals.RegisterCloseDialogScript(this, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                        }
                        else
                        {
                            Globals.RegisterCloseDialogScript(this);
                        }
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.lblClockNotSaved);
                    }
                }
                else
                {
                    objWSG.SalesGoalWhsCode = this.SalesGoalWhsCode;
                    objWSG.idwarehousesalesgoal = this.SalesGoalID;
                    objWSG.FromDate = fDate;
                    objWSG.ToDate = BusinessUtility.GetDateTime(txtToDate.Text, DateFormat.MMddyyyy);
                    objWSG.SalesAmountPerDay = BusinessUtility.GetDouble(txtSalesAmount.Text);
                    if (objWSG.Save(dbHelp) == true)
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.lblClockInSaved);

                        if (IsCallFromPOS == false && !string.IsNullOrEmpty(this.JsCallBackFunction))
                        {
                            Globals.RegisterCloseDialogScript(this, "parent.reloadGrid();", true);
                            //Globals.RegisterCloseDialogScript(this, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                        }
                        else if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                        {
                            Globals.RegisterCloseDialogScript(this, "parent.redirectToPOS();", true);
                            //Globals.RegisterCloseDialogScript(this, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                        }
                        else
                        {
                            Globals.RegisterCloseDialogScript(this);
                        }
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.lblClockNotSaved);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }
    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    private bool IsCallFromPOS
    {
        get
        {
            if (BusinessUtility.GetString(Request.QueryString["InvokeSrc"]).ToUpper() == "POS".ToUpper())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    private int SalesGoalUserID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["UserID"], out id);
            return id;
        }
    }
    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }
    private string SalesGoalWhsCode
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["whsCode"]))
            {
                return BusinessUtility.GetString(Request.QueryString["whsCode"]);
            }
            else
                return "";
        }
    }
}
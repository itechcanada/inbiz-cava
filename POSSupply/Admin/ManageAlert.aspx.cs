﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

using Newtonsoft.Json;

public partial class Admin_ManageAlert : BasePage
{
    NewAlert _alert = new NewAlert();
    InbizUser _usr = new InbizUser();
    
    protected void Page_Load(object sender, EventArgs e)
    {       
        //Security 
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }  
      
        //Check for ajax call
        if (Request.Form["isAjaxCall"] == "1" && Request.Form["callBack"] == "delete")
        {
            try
            {
                int userID = 0;
                if (int.TryParse(Request.Form["userID"], out userID) && userID > 0) {
                    _alert.UnAssignUser(null, Request.Form["alertName"], userID);
                }
                Response.Write("ok");
            }
            catch 
            {
                Response.Write("error");
            }            

            Response.End();
            Response.SuppressContent = true;
        }
    }

    int _rowIdx = -1;
    //string _ulstHtml = @"<div id=""inputToken#CONTAINER_ID#"" class=""autocomplete_holder ui-helper-clearfix""><input type=""text"" class=""user_list_to_assign"" alertName=""#ALERT_NAME#"" /></div>";
    string _ulstHtml = (@"<div id=""inputToken#CONTAINER_ID#"" class=""autocomplete_holder ui-helper-clearfix"">#ASSIGNED_USERS# <span class=""add"" onclick=""assignUsersPopup('#ALERT_NAME#');"">@Add<a href=""javascript:;"">+</a></span></div>");
    protected void grdAlerts_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {                
        if (e.ColumnIndex == 2)
        {
            e.CellHtml = _ulstHtml.Replace("#CONTAINER_ID#", e.RowIndex.ToString()).Replace("#ALERT_NAME#", e.RowKey).Replace("@Add", Resources.Resource.lblAdd);
            var v = _alert.GetAlertUserList(null, e.RowKey);
            string htm = string.Empty;
            foreach (var item in v)
            {
                htm += string.Format(@"<span>{0}<a href=""javascript:;"" onclick=""removeAssignedUser(this, {1}, '{2}')"">x</a></span>", _usr.GetUserName(item.UserID), item.UserID, e.RowKey);                   
            }
            e.CellHtml = e.CellHtml.Replace("#ASSIGNED_USERS#", htm);
        }
        else if (e.ColumnIndex == 3)
        {
            e.CellHtml = string.Format(@"<a href=""javascript:;"" onclick=""editPopup('{0}')"">{1}</a>", e.RowKey, Resources.Resource.edit);
        }
    }

    protected void grdAlerts_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        NewAlert al = new NewAlert();

        //if (Request.QueryString.AllKeys.Contains("toAssign")) //Means need to assign user
        //{

        //    int uid = 0;
        //    if (int.TryParse(Request.QueryString["alertUserID"], out uid) && uid > 0)
        //    {
        //        AlertUser au = new AlertUser();
        //        string[] aList = Request.QueryString["alertTypes"].Split(',');
        //        au.AssingAlertToUser(null, uid, aList);
        //    }            
        //}

        string srch = Request.QueryString[txtSearch.ClientID];        
        sdsAlerts.SelectCommand = al.GetSql(sdsAlerts.SelectParameters, srch, Globals.CurrentAppLanguageCode);
    }    
}
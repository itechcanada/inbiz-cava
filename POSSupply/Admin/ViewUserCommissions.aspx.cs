﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.WebControls;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;
using System.Data;

public partial class Admin_ViewUserCommissions : BasePage
{
    OrderTypeDetail _orderTypeDtl = new OrderTypeDetail();
    System.Data.DataTable objDT;
    System.Data.DataRow objDR;

    // On Page Load
    protected void Page_Load(object sender, System.EventArgs e)
    {
        //Security Check                  
        if (this.UserID > 0 && this.UserID != CurrentUser.UserID && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }
        if (!IsPostBack)
        {
            ltSectionTitle.Text = Resources.Resource.lnkUserCommission;
            DropDownHelper.FillOrderType(dlstOderType, Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.lblSelectOrderType));
            if (this.UserID > 0)
            {
                FillOrderTypeGrid();
            }
            
        }
    }

    public int UserID
    {
        get
        {
            int uid = 0;
            int.TryParse(Request.QueryString["UserID"], out uid);
            return uid;
        }
    }

    protected void grdOrderType_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        FillOrderTypeGrid();
    }
    protected void grdOrderType_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        if ((e.Row.RowType == DataControlRowType.DataRow))
        {
            ImageButton imgDelete = (ImageButton)e.Row.FindControl("imgDelete");
            imgDelete.Attributes.Add("onclick", "javascript:return " + "confirm('" + Resources.Resource.ConfirmDeleteOderType + " ')");
        }
    }
    protected void grdOrderType_RowDeleting(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
    {
        Int32 strOrderID = Convert.ToInt32(grdOrderType.DataKeys[e.RowIndex].Value.ToString());
        //lblOTypeMsg.Text = Resources.Resource.msgOrderTypeDeleted;
        sqldsOrderType.DeleteCommand = _orderTypeDtl.DeleteOderTypeDetail(sqldsOrderType.DeleteParameters, strOrderID);
        //Delete OrderID
        FillOrderTypeGrid();
    }
    protected void grdOrderType_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        FillOrderTypeGrid();
    }


    // Populate Order Type Grid
    public void FillOrderTypeGrid()
    {
        sqldsOrderType.SelectCommand = _orderTypeDtl.GetSqlOrderTypes(sqldsOrderType.SelectParameters, this.UserID, Globals.CurrentAppLanguageCode);
    }

    protected void grdOderTypeInsert_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        UpdateOrderTypeGrid();
    }

    protected void grdOderTypeInsert_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        if ((e.Row.RowType == DataControlRowType.DataRow))
        {
            ImageButton imgDelete = (ImageButton)e.Row.FindControl("imgDelete");
            imgDelete.Attributes.Add("onclick", "javascript:return " + "confirm('" + Resources.Resource.ConfirmDeleteOderType + " ')");
        }
    }

    protected void grdOderTypeInsert_RowDeleting(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
    {
        objDT = (DataTable)Session["SalesCart"];
        lblOTypeMsg.Text = Resources.Resource.msgOrderTypeDeleted;
        objDT.Rows[e.RowIndex].Delete();
        txtCommission.Text = "";
        UpdateOrderTypeGrid();
    }

    protected void grdOderTypeInsert_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        UpdateOrderTypeGrid();
    }

    protected void imgAdd_Click(object sender, System.EventArgs e)
    {
        if (this.UserID > 0)
        {
            int orderTypeid = BusinessUtility.GetInt(dlstOderType.SelectedValue);
            _orderTypeDtl.Ordercommission = BusinessUtility.GetDouble(txtCommission.Text);
            _orderTypeDtl.OrderTypeID = orderTypeid;
            _orderTypeDtl.UserID = this.UserID;
            if (_orderTypeDtl.CheckDuplicateOderType(orderTypeid, this.UserID) == true)
            {
                lblOTypeMsg.Text = "OderType already exists.";
                //  Exit Sub
            }
            else
            {
                _orderTypeDtl.OrderTypeDtlID = _orderTypeDtl.InsertOrdertype();
                FillOrderTypeGrid();
            }
        }
        else
        {
            if (AddToOderType())
            {
                lblOTypeMsg.Text = Resources.Resource.msgOrderTypeSaved;
                UpdateOrderTypeGrid();
            }
            else
            {
                // Exit Sub
            }
        }
        txtCommission.Text = "";
        txtCommission.Focus();
    }

    public bool AddToOderType()
    {
        _orderTypeDtl.Ordercommission = BusinessUtility.GetDouble(txtCommission.Text);
        _orderTypeDtl.OrderTypeID = BusinessUtility.GetInt(dlstOderType.SelectedValue);
        int OrderTypeID = BusinessUtility.GetInt(dlstOderType.SelectedValue);
        bool blnMatch = false;
        objDT = (DataTable)Session["SalesCart"];
        foreach (DataRow objDR in objDT.Rows)
        {
            if (BusinessUtility.GetInt(objDR["OrderTypeID"]) == OrderTypeID)
            {
                lblOTypeMsg.Text = "OderType already exists.";
                return false;
                //objDR("Commission") += CDbl(objOrderType.ordercommission)
                //blnMatch = True
                //Exit For
            }
        }

        if (!blnMatch)
        {
            objDR = objDT.NewRow();
            objDR["OrderTypeID"] = _orderTypeDtl.OrderTypeID;
            objDR["OrderTypeDesc"] = dlstOderType.SelectedItem.Text;
            objDR["Commission"] = _orderTypeDtl.Ordercommission;
            objDT.Rows.Add(objDR);
        }
        return true;
    }

    public void UpdateOrderTypeGrid()
    {
        grdOderTypeInsert.DataSource = objDT;
        grdOderTypeInsert.DataBind();
    }

}
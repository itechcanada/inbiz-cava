﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.Web;

public partial class Admin_delete : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!IsPostBack) {            
            if (!UserProfile.IsAuthenticated) {
                Globals.RegisterCloseDialogScript(this);
            }
        }        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        switch (this.CurrentCommand.ToLower()) { 
            case DeleteCommands.DELETE_USER:
                DeleteInbizUser();
                break;
            case DeleteCommands.DELETE_COMPANY:
                DeleteCompany();
                break;
            case DeleteCommands.DELETE_CURRENCY:
                DeleteCurrency();
                break;
            case DeleteCommands.DELETE_PRCESS_GROUP:
                DeleteProcessGroup();
                break;
            case DeleteCommands.DELETE_WAREHOUSE:
                DeleteWarehose();
                break;
            case DeleteCommands.DELETE_POS:
                DeletePosRegister();
                break;
            case DeleteCommands.DELETE_VENDOR:
                DeleteVendor();
                break;
            case DeleteCommands.DELETE_CATEGORY:
                DeleteCategory();
                break;
            case DeleteCommands.DELETE_ATTRIBUTE:
                DeleteAttribute();
                break;
            case "transition":
                DeleteTransition();
                break;
        }
    }

    private void DeleteTransition()
    {
        new clsPOSTransaction().funTransactionDelete(BusinessUtility.GetString(GetKey(0)));
        lblMessage.Text = Resources.Resource.msgDeleteSuccess;
        if (!string.IsNullOrEmpty(this.JsCallBackFunction))
        {
            Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
        }
    }             

    public string CurrentCommand {
        get {
            if (!string.IsNullOrEmpty(Request.QueryString["cmd"]))
            {
                return Request.QueryString["cmd"].ToLower();
            }
            return "";
        }
    }

    private int GetKey(int ofIndex) {
        string cleneStr = StringUtil.GetCleneJoinedString(",", this.KeysToDelete);
        int[] keys = StringUtil.GetArrayFromJoindString(",", cleneStr);
        try
        {
            return keys[ofIndex];
        }
        catch 
        {
            return 0;
        }
    }

    private string GetStringKey(int ofIndex){
        string cleneStr = StringUtil.GetCleneJoinedString(",", this.KeysToDelete);
        string[] keys = cleneStr.Split(',');
        try
        {
            return keys[ofIndex];
        }
        catch 
        {
            return string.Empty;
        }
    }

    public string KeysToDelete {
        get {
            if (!string.IsNullOrEmpty(Request.QueryString["keys"])) {
                return Request.QueryString["keys"];
            }
            return "";
        }
    }

    public string JsCallBackFunction {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }

    #region Delete Functions

    private void DeleteInbizUser() {
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            iTECH.InbizERP.BusinessLogic.InbizUser usr = new iTECH.InbizERP.BusinessLogic.InbizUser();
            iTECH.Library.DataAccess.MySql.DbHelper dbHelp = new iTECH.Library.DataAccess.MySql.DbHelper(true);
            try
            {
                string sqlInactivateUser = string.Format("UPDATE users SET userActive=0 WHERE UserID={0}", this.GetKey(0));
                dbHelp.ExecuteNonQuery(sqlInactivateUser, System.Data.CommandType.Text, null);
                //usr.Delete(this.GetKey(0));
                lblMessage.Text = Resources.Resource.msgDeleteSuccess;
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));                    
                }                
            }
            catch
            {
                lblMessage.Text = "Critical Error!";
            }
        }
        else
        {
            lblMessage.Text = "Access Denied!";
        }

        pnlOk.Visible = true;
        pnlDeleteCommand.Visible = false;
    }

    private void DeleteCompany() {
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            SysCompanyInfo comp = new SysCompanyInfo();

            try
            {
                comp.Delete(this.GetKey(0));
                lblMessage.Text = Resources.Resource.msgDeleteSuccess;
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
                }  
            }
            catch
            {
                lblMessage.Text = "Critical Error!";
            }
        }
        else
        {
            lblMessage.Text = "Access Denied!";
        }

        pnlOk.Visible = true;
        pnlDeleteCommand.Visible = false;
    }

    public void DeleteCurrency()
    {
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            SysCurrencies cur = new SysCurrencies();

            try
            {
                cur.Delete(this.GetStringKey(0));
                lblMessage.Text = Resources.Resource.msgDeleteSuccess;
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
                }
            }
            catch
            {
                lblMessage.Text = "Critical Error!";
            }
        }
        else
        {
            lblMessage.Text = "Access Denied!";
        }

        pnlOk.Visible = true;
        pnlDeleteCommand.Visible = false;
    }

    private void DeleteProcessGroup()
    {
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            SysProcessGroup prg = new SysProcessGroup();
            try
            {
                prg.Delete(this.GetKey(0));
                lblMessage.Text = Resources.Resource.msgDeleteSuccess;
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
                }
            }
            catch
            {
                lblMessage.Text = "Critical Error!";
            }
        }
        else
        {
            lblMessage.Text = "Access Denied!";
        }

        pnlOk.Visible = true;
        pnlDeleteCommand.Visible = false;
    }

    private void DeleteWarehose()
    {
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            SysWarehouses whs = new SysWarehouses();
            try
            {
                whs.Delete(this.GetStringKey(0));
                lblMessage.Text = Resources.Resource.msgDeleteSuccess;
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
                }
            }
            catch
            {
                lblMessage.Text = "Critical Error!";
            }
        }
        else
        {
            lblMessage.Text = "Access Denied!";
        }

        pnlOk.Visible = true;
        pnlDeleteCommand.Visible = false;
    }

    private void DeletePosRegister()
    {
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            SysRegister reg = new SysRegister();
            try
            {
                reg.Delete(this.GetStringKey(0));
                lblMessage.Text = Resources.Resource.msgDeleteSuccess;
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
                }
            }
            catch
            {
                lblMessage.Text = "Critical Error!";
            }
        }
        else
        {
            lblMessage.Text = "Access Denied!";
        }

        pnlOk.Visible = true;
        pnlDeleteCommand.Visible = false;
    }

    private void DeleteVendor()
    {
        //if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        //{
        //    Vendor vendor = new Vendor();

        //    try
        //    {
        //        vendor.Delete(this.GetKey(0));
        //        lblMessage.Text = Resources.Resource.msgDeleteSuccess;
        //        if (!string.IsNullOrEmpty(this.JsCallBackFunction))
        //        {
        //            Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
        //        }
        //    }
        //    catch
        //    {
        //        lblMessage.Text = "Critical Error!";
        //    }
        //}
        //else
        //{
        //    lblMessage.Text = "Access Denied!";
        //}

        //pnlOk.Visible = true;
        //pnlDeleteCommand.Visible = false;
    }



    private void DeleteCategory()
    {
        //if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        //{
        Category categ = new Category();

        try
        {
            OperationResult res = categ.Delete(this.GetKey(0));
            if (res == OperationResult.Failuer) {
                lblMessage.Text = "Selected category has child category & can not be deleted!";
            }
            else
            {
                lblMessage.Text = Resources.Resource.msgDeleteSuccess;
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
                }
            }            
        }
        catch
        {
            lblMessage.Text = "Critical Error!";
        }
        //}
        //else
        //{
        //    lblMessage.Text = "Access Denied!";
        //}

        pnlOk.Visible = true;
        pnlDeleteCommand.Visible = false;
    }

    private void DeleteAttribute()
    {
        //if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        //{
        Attributes attr = new Attributes();

        try
        {
            attr.Delete(this.GetKey(0));
            lblMessage.Text = Resources.Resource.msgDeleteSuccess;
            if (!string.IsNullOrEmpty(this.JsCallBackFunction))
            {
                Globals.RegisterScript(this, string.Format("parent.{0}();", this.JsCallBackFunction));
            }
        }
        catch
        {
            lblMessage.Text = "Critical Error!";
        }
        //}
        //else
        //{
        //    lblMessage.Text = "Access Denied!";
        //}

        pnlOk.Visible = true;
        pnlDeleteCommand.Visible = false;
    }

    #endregion
}
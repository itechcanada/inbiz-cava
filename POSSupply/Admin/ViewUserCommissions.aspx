﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="ViewUserCommissions.aspx.cs" Inherits="Admin_ViewUserCommissions" %>

<%@ Register Src="~/Controls/UsersLeft.ascx" TagName="UserLeftMenu" TagPrefix="uc2" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphHead">
    <%--Keep script file reference here those were required for this page only. 
    Note* if it is to being used on all pages than should pe kept on master page.--%>
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphLeft" runat="Server">
    <asp:Panel runat="server" CssClass="divSectionContent" ID="LeftMenu">
        <uc2:UserLeftMenu ID="UserLeftMenu" runat="server" />
    </asp:Panel>
</asp:Content>
<asp:Content ID="cntViewTaxes" ContentPlaceHolderID="cphMaster" runat="Server">
    <h3>
        <%= Resources.Resource.lblUserCommission%>
    </h3>
    <div class="divMainContent" id="divOrderType" runat="server">
        <asp:Panel runat="server" ID="pnlOrderType">
            <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblUserOrderType %>" ID="lblUserOrderType"
                            runat="server"></asp:Label>
                    </td>
                    <td class="input">
                        <asp:DropDownList ID="dlstOderType" runat="server" Width="160px">
                        </asp:DropDownList>
                        <span class="style1">*</span>
                        <asp:CustomValidator ID="custvalOderType" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, custvalOderType%>"
                            ClientValidationFunction="funCheckOderType" ValidationGroup="OderType" Display="None">
                        </asp:CustomValidator>
                    </td>
                    <td rowspan="3" valign="top" align="right">
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblUserCommission %>" ID="lblUserCommission"
                            runat="server"></asp:Label>
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtCommission" runat="server" Width="120px" MaxLength="5"></asp:TextBox>
                        % <span class="style1">*</span>
                        <asp:CompareValidator ID="compvalCommission" EnableClientScript="true" SetFocusOnError="true"
                            ValidationGroup="OderType" ControlToValidate="txtCommission" Operator="DataTypeCheck"
                            Type="Double" Display="None" ErrorMessage="<%$ Resources:Resource, compvalCommission %>"
                            runat="server" />
                        <asp:RequiredFieldValidator ID="reqValCommission" ErrorMessage="<%$ Resources:Resource, reqValCommission%>"
                            runat="server" ControlToValidate="txtCommission" ValidationGroup="OderType" SetFocusOnError="true"
                            Display="None"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                    </td>
                    <td class="input">
                        <asp:Button ID="imgAdd" ValidationGroup="OderType" runat="server" ToolTip="Add" Text="<%$ Resources:Resource, lblAssign%>"
                            ImageUrl="../images/add_2.png" OnClick="imgAdd_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="4">
                        <asp:Label ID="lblOTypeMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <h3>
        <asp:Literal ID="ltSectionTitle" Text="" runat="server" />
    </h3>
    <asp:Panel runat="server" ID="Panel14">
        <asp:GridView CssClass="mGrid" GridLines="None" ID="grdOrderType" runat="server"
            AllowSorting="false" DataSourceID="sqldsOrderType" AllowPaging="True" PageSize="15"
            PagerSettings-Mode="Numeric" AutoGenerateColumns="False" UseAccessibleHeader="False"
            DataKeyNames="orderTypeDtlID" OnPageIndexChanging="grdOrderType_PageIndexChanging"
            OnRowDataBound="grdOrderType_RowDataBound" OnRowDeleting="grdOrderType_RowDeleting"
            OnSorting="grdOrderType_Sorting">
            <Columns>
                <asp:BoundField DataField="orderTypeDesc" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grduserOderType %>"
                    ReadOnly="True" SortExpression="orderTypeDesc">
                    <ItemStyle Width="230px" Wrap="true" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="ordercommission" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grduserCommission %>"
                    ReadOnly="True" SortExpression="ordercommission">
                    <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" HeaderStyle-ForeColor="#ffffff"
                    HeaderStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                            CommandArgument='<%# Eval("orderTypeDtlID") %>' CommandName="Delete" CausesValidation="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="grid_footer" />
            <RowStyle CssClass="grid_rowstyle" />
            <HeaderStyle CssClass="grid_header" />
            <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
            <PagerSettings PageButtonCount="20" />
        </asp:GridView>
        <asp:SqlDataSource ID="sqldsOrderType" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient"></asp:SqlDataSource>
        <asp:GridView CssClass="mGrid" GridLines="None" ID="grdOderTypeInsert" runat="server"
            AllowSorting="false" AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric"
            AutoGenerateColumns="False" UseAccessibleHeader="False" DataKeyNames="OrderTypeID"
            OnPageIndexChanging="grdOderTypeInsert_PageIndexChanging" OnRowDataBound="grdOderTypeInsert_RowDataBound"
            OnRowDeleting="grdOderTypeInsert_RowDeleting" OnSorting="grdOderTypeInsert_Sorting">
            <Columns>
                <asp:BoundField DataField="OrderTypeID" HeaderText="<%$ Resources:Resource, grduserOderType %>"
                    ReadOnly="True" SortExpression="OrderTypeID" Visible="false">
                    <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="OrderTypeDesc" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grduserOderType %>"
                    ReadOnly="True" SortExpression="OrderTypeDesc">
                    <ItemStyle Width="230px" Wrap="true" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Commission" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grduserCommission %>"
                    ReadOnly="True" SortExpression="Commission">
                    <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" HeaderStyle-ForeColor="#ffffff"
                    HeaderStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                            CommandArgument='<%# Eval("OrderTypeID") %>' CommandName="Delete" CausesValidation="false" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="grid_footer" />
            <RowStyle CssClass="grid_rowstyle" />
            <HeaderStyle CssClass="grid_header" />
            <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
            <PagerSettings PageButtonCount="20" />
        </asp:GridView>
    </asp:Panel>
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ViewInternalUser.aspx.cs" Inherits="Admin_ViewInternalUser" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>    
</asp:Content>
<asp:Content ID="cntTop" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <%= Resources.Resource.lblView%>:
            <%= Resources.Resource.lblInternalUser%></b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <asp:Button ID="lnkAddInternalUser" runat="server" Text="Button" OnClick="lnkAddInternalUser_Click" />
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif" CssClass="csv_export" />
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="cntLeft" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        <%= Resources.Resource.lblSearchForm%></div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label CssClass="filter-key" ID="Label1" runat="server" Text="<%$ Resources:Resource, lblStatus %>"
                        AssociatedControlID="ddlStatus"></asp:Label></h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlStatus" runat="server" Width="175px" >
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" CssClass="filter-key" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch" CssClass="filter-key"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntMaster" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="grdUsers" DataSourceID="sqldsInternalUser"
                    Height="300px" AutoWidth="True"  OnCellBinding="grdUsers_CellBinding"
                    OnDataRequesting="grdUsers_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="userid" HeaderText="" PrimaryKey="True" Visible="false" />
                        <trirand:JQGridColumn DataField="userName" HeaderText="<%$ Resources:Resource, grdUserName %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="userLoginId" HeaderText="<%$ Resources:Resource, grdUserLoginId %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="userModules" HeaderText="<%$ Resources:Resource, grdAssociatedModules %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="userEmail" HeaderText="<%$ Resources:Resource, grdEmail %>"
                            Editable="false" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="userid" 
                            Sortable="false" TextAlign="Center" Width="50" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="userid"
                            Sortable="false" TextAlign="Center" Width="50" >                            
                         </trirand:JQGridColumn>
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents LoadComplete="loadComplete" />
                </trirand:JQGrid>
                <iCtrl:IframeDialog ID="mdDeleteUser" Width="400"  Height="120" Title="Delete"
                    Dragable="true" TriggerSelectorClass="pop_delete" TriggerSelectorMode="Class"
                    UrlSelector="TriggerControlHrefAttribute"
                    runat="server">
                </iCtrl:IframeDialog>
                <asp:SqlDataSource ID="sqldsInternalUser" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $grid = $("#<%=grdUsers.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=grdUsers.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        function loadComplete(data) {
            jqGridResize();            
        }
        
    </script>    
</asp:Content>



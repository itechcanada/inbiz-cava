﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AddEditProcess.aspx.cs" Inherits="Admin_AddEditProcess" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="contentBottom" style="padding: 5px; height: 200px; overflow: auto;" onkeypress="return disableEnterKey(event)">
        <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblProcessCode %>"  ID="lblProcessCode" runat="server" />*
                </td>
                <td class="input">
                    <asp:TextBox ID="txtProcessCode" runat="server" MaxLength="3" />
                    <asp:RequiredFieldValidator ID="reqvalProcessCode" runat="server" ErrorMessage="<%$ Resources:Resource, reqvalProcessCode%>" ControlToValidate="txtProcessCode"
                        SetFocusOnError="true" Display="None" ValidationGroup="ProcessGroup"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblProcessDescription %>"  ID="lblProcessDescription" runat="server" />*
                </td>
                <td class="input">
                    <asp:TextBox ID="txtProcessDescription" runat="server" MaxLength="75" />
                    <asp:RequiredFieldValidator ID="reqvalProcessDescription" ErrorMessage="<%$ Resources:Resource, reqvalProcessDescription%>" runat="server" ControlToValidate="txtProcessDescription"
                        SetFocusOnError="true" Display="None" ValidationGroup="ProcessGroup"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblProcessFixedCost %>"  ID="lblProcessFixedCost" runat="server" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtProcessFixedCost" runat="server" MaxLength="35">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblProcessCostPerHour %>"  ID="lblProcessCostPerHour" runat="server">
                    </asp:Label>
                </td>
                <td class="input">
                    <asp:TextBox ID="txtProcessCostPerHour" runat="server" MaxLength="35">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblProcessCostPerUnit %>"  ID="lblProcessCostPerUnit" runat="server" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtProcessCostPerUnit" runat="server" MaxLength="25" />
                </td>
            </tr>           
        </table>        
    </div>
    <div class="div-dialog-command">
        <asp:Button Text="<%$Resources:Resource, btnSave %>"  ID="btnSave" runat="server" ValidationGroup="ProcessGroup" OnClick="btnSave_Click" />
        <asp:Button Text="<%$Resources:Resource, btnCancel %>"  ID="btnCancel" runat="server" CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
    </div>
    <br />
    <asp:ValidationSummary ID="valsAdminProcess" runat="server" ShowMessageBox="true"
        ShowSummary="false" ValidationGroup="ProcessGroup" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphScripts" runat="Server">
</asp:Content>


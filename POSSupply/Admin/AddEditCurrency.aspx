﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AddEditCurrency.aspx.cs" Inherits="Admin_AddEditCurrency" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="contentBottom" style="padding:5px; height:100px; overflow:auto;">
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
        <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="text" style="width: 52%">
                    <asp:Label Text="<%$Resources:Resource, lblCurrencyCode %>"  ID="lblCurrencyCode" runat="server" />*
                </td>
                <td class="input">
                    <asp:TextBox ID="txtCurrencyCode" runat="server" MaxLength="4" />
                    <asp:RequiredFieldValidator  ID="reqvalCurrencyCode" ErrorMessage="<%$ Resources:Resource, reqvalCurrencyCode%>" runat="server" ControlToValidate="txtCurrencyCode"
                        Display="None"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblCurrencyRelativePrice %>"  ID="lblCurrencyRelativePrice" runat="server" />*
                </td>
                <td class="input">
                    <asp:TextBox ID="txtCurrencyRelativePrice" runat="server" MaxLength="7" />
                    <asp:RequiredFieldValidator ID="reqvalCurrencyRelativePrice" runat="server" ControlToValidate="txtCurrencyRelativePrice"
                        ErrorMessage="<%$ Resources:Resource, reqvalCurrencyRelativePrice%>" SetFocusOnError="true"
                        Display="None"> </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr runat="server" id="trIsActive" visible="false">
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblCurrencyActive %>"  ID="lblCurrencyActive" CssClass="lblBold" runat="server" />
                </td>
                <td class="input">
                    <asp:RadioButton Text="<%$Resources:Resource, lblYes %>" ID="rdbYes" runat="server" GroupName="YesNo" Checked="True" />
                    <asp:RadioButton Text="<%$Resources:Resource, lblNo %>" ID="rdbNo" runat="server" GroupName="YesNo" />
                </td>
            </tr>
        </table>        
    </div>
    <h2>
        </h2>
        <br />
        <div class="div-dialog-command">
            <asp:Button Text="<%$Resources:Resource, btnSave %>"  ID="btnSave" runat="server" OnClick="btnSave_Click" />
            <asp:Button Text="<%$Resources:Resource, btnCancel %>"  ID="btnCancel" runat="server" CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
            <div class="clear">
            </div>
        </div>
        <asp:ValidationSummary ID="valsAdminWarehouse" runat="server" ShowMessageBox="true"
            ShowSummary="false" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphScripts" runat="Server">
</asp:Content>



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.Library.DataAccess.MySql;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_AddEditCompany : BasePage
{
   
    //On Page Load
    SysCompanyInfo _company = new SysCompanyInfo();
    CountryStateTaxGroup _country = new CountryStateTaxGroup();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Security Check
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }        
       
        if (!IsPostBack)
        {                       
            FillRecord();           
            txtCompanyName.Focus();
        }       
    }

    //Populate Controls
    public void FillRecord()
    {
        DbHelper dbHelp = new DbHelper(true);

        try
        {
            DropDownHelper.FillCurrencyDropDown(dlCurrencyCode, new ListItem(Resources.Resource.lstSelectCurrency, "0"));
            _country.PopulateListControlWithCountry(dbHelp, ddlCountryBillTo, new ListItem(""));
            _country.PopulateListControlWithCountry(dbHelp, ddlCountry, new ListItem(""));

            if (this.CompanyID > 0)
            {
                _company.CompanyID = this.CompanyID;
                trIsActive.Visible = true;
                _company.PopulateObject(_company.CompanyID, dbHelp);

                ddlCountry.SelectedValue = _company.CompanyCountry;                
                _country.PopulateListControlWithStateCodes(dbHelp, ddlCountry.SelectedValue, ddlState, new ListItem(""));
                ddlState.SelectedValue = _company.CompanyState;

                ddlCountryBillTo.SelectedValue = _company.CompanyBillToCountry;
                _country.PopulateListControlWithStateCodes(dbHelp, ddlCountryBillTo.SelectedValue, ddlStateBillTo, new ListItem(""));
                ddlStateBillTo.SelectedValue = _company.CompanyBillToState;

                //Get Company Info
                txtCompanyName.Text = _company.CompanyName;
                txtCompanyAddressLine1.Text = _company.CompanyAddressLine1;
                txtCompanyAddressLine2.Text = _company.CompanyAddressLine2;
                txtCompanyCity.Text = _company.CompanyCity;
                txtCompanyState.Text = _company.CompanyState;
                txtCompanyCountry.Text = _company.CompanyCountry;
                txtCompanyPostalCode.Text = _company.CompanyPostalCode;
                txtCompanyBillToAddr1.Text = _company.CompanyBillToAddr1;
                txtCompanyBillToAddr2.Text = _company.CompanyBillToAddr2;
                txtCompanyBillToCity.Text = _company.CompanyBillToCity;
                txtCompanyBillToState.Text = _company.CompanyBillToState;
                txtCompanyBillToCountry.Text = _company.CompanyBillToCountry;
                txtCompanyBillToPostalCode.Text = _company.CompanyBillToPostalCode;
                txtCompanyShpToTerms.Text = _company.CompanyShpToTerms;
                txtCompanyFax.Text = _company.CompanyFax;
                dlCurrencyCode.SelectedValue = _company.CompanyBasCur;
                txtCompanyEmail.Text = _company.CompanyEmail;
                txtCompanyInternetFax.Text = _company.CompanyInternetFax;
                txtCompanyPOTerms.Text = _company.CompanyPOTerms;
                txtCompanyPOName.Text = _company.CompanyPOName;
                txtCompanyPOPhone.Text = _company.CompanyPOPhone;
                txtCompanyPOEmail.Text = _company.CompanyPOEmail;
                rblIsSalesRep.SelectedValue = _company.CompanySalesRepRestricted ? "1" : "0";
                txtCompanyCourierCode.Text = _company.CompanyCourierCode;
                txtGenQuotationRemarks.Text = _company.CompanyGenQuotationRemarks;
                txtGenInvoiceRemarks.Text = _company.CompanyGenInvoiceRemarks;
                txtCompanyPhone.Text = _company.CompanyPhoneno;
                chkRestrictedPhoneFormat.Checked = _company.RestrictedPhoneFormat;
                DropDownHelper.FillDropdown(chkLstStatus, "SO", "SOSts", Globals.CurrentAppLanguageCode, null);

                foreach (ListItem item in chkLstStatus.Items)
                {
                    item.Selected = _company.ValidateOrderStatusBeforeInvoiced(dbHelp, _company.CompanyID, item.Value);
                }              

                ListItem li = ddlCumpanyUnitType.Items.FindByValue(Convert.ToString((int)_company.CompanyUnitType));
                if (li != null)
                {
                    li.Selected = true;
                }
                if (!string.IsNullOrEmpty(_company.CompanyLogoPath))
                {
                    imgLogo.Src = ImageUploadPath.COMPANY_LOGO_UPLOAD_PATH + _company.CompanyLogoPath;
                    imgLogo.Alt = _company.CompanyLogoPath;
                    imgLogo.Width = 80;
                    imgLogo.Height = 50;
                    trLogoImageUrl.Visible = true;
                    trLogoImagePath.Visible = false;
                    hdnCompanyLogo.Value = _company.CompanyLogoPath;
                }
            }
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally {
            dbHelp.CloseDatabaseConnection();
        }        
    }
    

    private void SetData()
    {
        _company.CompanyName = txtCompanyName.Text;
        _company.CompanyAddressLine1 = txtCompanyAddressLine1.Text;
        _company.CompanyAddressLine2 = txtCompanyAddressLine2.Text;
        _company.CompanyCity = txtCompanyCity.Text;
        _company.CompanyState = txtCompanyState.Text;
        _company.CompanyCountry = txtCompanyCountry.Text;
        _company.CompanyPostalCode = txtCompanyPostalCode.Text;
        _company.CompanyBillToAddr1 = txtCompanyBillToAddr1.Text;
        _company.CompanyBillToAddr2 = txtCompanyBillToAddr2.Text;
        _company.CompanyBillToCity = txtCompanyBillToCity.Text;
        _company.CompanyBillToState = txtCompanyBillToState.Text;
        _company.CompanyBillToCountry = txtCompanyBillToCountry.Text;
        _company.CompanyBillToPostalCode = txtCompanyBillToPostalCode.Text;
        _company.CompanyShpToTerms = txtCompanyShpToTerms.Text;
        _company.CompanyFax = txtCompanyFax.Text;
        _company.CompanyPhoneno = txtCompanyPhone.Text;
        _company.CompanyBasCur = dlCurrencyCode.SelectedValue;
        _company.CompanyEmail = txtCompanyEmail.Text;
        _company.CompanyInternetFax = txtCompanyInternetFax.Text;
        _company.CompanyPOTerms = txtCompanyPOTerms.Text;
        _company.CompanyPOName = txtCompanyPOName.Text;
        _company.CompanyPOPhone = txtCompanyPOPhone.Text;
        _company.CompanyPOEmail = txtCompanyPOEmail.Text;
        _company.CompanySalesRepRestricted = rblIsSalesRep.SelectedValue == "1";
        _company.CompanyCourierCode = txtCompanyCourierCode.Text;
        _company.CompanyGenQuotationRemarks = txtGenQuotationRemarks.Text;
        _company.CompanyGenInvoiceRemarks = txtGenInvoiceRemarks.Text;
        _company.CompanyUnitType = (CompanyUnitType)BusinessUtility.GetInt(ddlCumpanyUnitType.SelectedValue);
        _company.RestrictedPhoneFormat = chkRestrictedPhoneFormat.Checked;
    }    

    
    public string UploadFile()
    {
        string strImageName = string.Empty;
        
        if (fileLogoImagePath.HasFile)
        {
            if (FileManager.IsValidImageFile(fileLogoImagePath.FileName)) {
                strImageName = FileManager.GetRandomFileName(fileLogoImagePath.FileName);
                try
                {
                    if (FileManager.UploadImage(fileLogoImagePath.PostedFile, ImageUploadPath.COMPANY_LOGO_UPLOAD_PATH, strImageName)) {
                        return strImageName;
                    }
                    return string.Empty;
                }
                catch 
                {
                    return string.Empty;
                }            
            }
            else
            {
                return string.Empty;
                //MessageState.SetGlobalMessage(MessageType.Failure, "Invalid Image File!");
            }
        }
        else
        {
            return hdnCompanyLogo.Value;
        }
    }
    
    protected void  imgLogoImageUrl_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        trLogoImageUrl.Visible = false;
        trLogoImagePath.Visible = true;
        _company.UpdateCompanyLogo(this.CompanyID, "");        
        hdnCompanyLogo.Value = "";
    }

    protected void  btnSave_Click(object sender, System.EventArgs e)
    {
        if (Page.IsValid)
        {
            SetData();
            _company.CompanyLogoPath = UploadFile();

            List<string> lstStatus = new List<string>();
            foreach (ListItem item in chkLstStatus.Items)
            {
                if (item.Selected)
                {
                    lstStatus.Add(item.Value);
                }
            }

            if (this.CompanyID > 0)
            {
                _company.CompanyID = this.CompanyID;
                _company.Update();
                //Update Company 
                

                //Update status to allow get invoiced
                _company.SetOrderStatusToAllowInvoiceGeneration(null, _company.CompanyID, lstStatus.ToArray());

                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.CompanyUpdatedSuccessfully);
                Response.Redirect("ViewGlobalParameters.aspx");
            }
            else
            {
                _company.Insert();

                //Update status to allow get invoiced
                _company.SetOrderStatusToAllowInvoiceGeneration(null, _company.CompanyID, lstStatus.ToArray());

                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.CompanyAddedSuccessfully);
                Response.Redirect("ViewGlobalParameters.aspx");

                //Resources.Resource.CompanyCodeAlreadyExists;
            }
        }
    }

    protected void  btnCancel_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("~/Admin/ViewGlobalParameters.aspx");
    }

    private int CompanyID {
        get {
            int comId = 0;
            int.TryParse(Request.QueryString["CompanyID"], out comId);
            return comId;
        }
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlState.Items.Clear();
        txtCompanyCountry.Text = ddlCountry.SelectedValue;
        if (ddlCountry.SelectedIndex > 0)
        {
            _country.PopulateListControlWithStateCodes(null, ddlCountry.SelectedValue, ddlState, new ListItem(""));
        }
    }
    protected void ddlCountryBillTo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlStateBillTo.Items.Clear();
        txtCompanyBillToCountry.Text = ddlCountryBillTo.SelectedValue;
        if (ddlCountryBillTo.SelectedIndex > 0)
        {
            _country.PopulateListControlWithStateCodes(null, ddlCountryBillTo.SelectedValue, ddlStateBillTo, new ListItem(""));
        }
    }
}
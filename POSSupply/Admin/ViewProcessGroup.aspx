﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ViewProcessGroup.aspx.cs" Inherits="Admin_ViewProcessGroup" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphHead">
   <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <%= Resources.Resource.lblView%>:
            <%= Resources.Resource.lblProcessGroup%></b>
    </div>
    <div style="float: left; padding-top: 12px;">        
        <asp:Button ID="btnCreateProcesGroup" runat="server" Text="<%$Resources:Resource, lblAddProcessGroup%>" />
        <iCtrl:IframeDialog ID="mdCreateProcesGroup" TriggerControlID="btnCreateProcesGroup"
            Width="450" Height="360" Url="AddEditProcess.aspx?jscallback=reloadGrid" Title="Add Proces Group" runat="server">
        </iCtrl:IframeDialog>
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif" CssClass="csv_export" />        
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        Search form</div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>" CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="Search" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>

<asp:Content ID="cntViewProcessGroup" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="jgdvProcess" DataSourceID="sqldsProcess" Height="300px"
                    AutoWidth="True" OnCellBinding="jgdvProcess_CellBinding" OnDataRequesting="jgdvProcess_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="ProcessID" HeaderText="" PrimaryKey="True" Visible="false" />
                        <trirand:JQGridColumn DataField="ProcessDescription" HeaderText="<%$ Resources:Resource, grdProcessDescription %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="ProcessFixedCost" HeaderText="<%$ Resources:Resource, grdvProcessFixedCost %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="ProcessCostPerHour" HeaderText="<%$ Resources:Resource, grdvProcessCostPerHour %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="ProcessCostPerUnit" HeaderText="<%$ Resources:Resource, grdvProcessCostPerUnit %>"
                            Editable="false" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="ProcessID"
                            Sortable="false" TextAlign="Center" Width="50" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="ProcessID"
                            Sortable="false" TextAlign="Center" Width="50" />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents LoadComplete="jqGridResize" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sqldsProcess" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
                      <iCtrl:IframeDialog ID="mdEditProcess" Height="360" Width="490" Title="Edit process Group"
                    Dragable="true" TriggerSelectorClass="edit-Process" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
                    runat="server">
                </iCtrl:IframeDialog>
                <iCtrl:IframeDialog ID="mdDelProcess" Width="400" Height="120" Title="Delete" Dragable="true"
                    TriggerSelectorClass="pop_delete" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
                    runat="server">
                </iCtrl:IframeDialog>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
    <script type="text/javascript">
        $grid = $("#<%=jgdvProcess.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=jgdvProcess.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }      
        

        function reloadGrid(event, ui) {
            $grid.trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }
    </script>
</asp:Content>


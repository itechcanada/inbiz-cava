﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="CustomFieldView.aspx.cs" Inherits="Admin_CustomFieldView" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="cntTop" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>       
    </div>
    <div style="float: left;">
       <asp:Button ID="btnAddCustomField" runat="server" Text="Add Custom Field" />      
        <iCtrl:IframeDialog ID="mdSearch" TriggerControlID="btnAddCustomField" Width="500" Height="260"
            Url="CustomFieldEdit.aspx" Title="Add Custom Field" runat="server" ></iCtrl:IframeDialog> 
    </div>
    <div style="float: right;">
        <asp:RadioButtonList ID="rblModule" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">               
        </asp:RadioButtonList>        
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="cntMaster" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="grdCustomFields" DataSourceID="sdsCustomFields"
                    Height="300px" AutoWidth="True" 
                    oncellbinding="grdCustomFields_CellBinding" 
                    ondatarequesting="grdCustomFields_DataRequesting" 
                    onrowdeleting="grdCustomFields_RowDeleting">
                    <Columns>
                        <trirand:JQGridColumn DataField="ID" HeaderText="ID"
                            PrimaryKey="True" Width="50" />
                        <trirand:JQGridColumn DataField="Desc" HeaderText="Desc"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="FieldType" HeaderText="FieldType"
                            Editable="false" TextAlign="Center" />                        
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="ID"
                            Sortable="false" TextAlign="Center" Width="80" />
                         <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, delete %>" DataField="ID"
                            Sortable="false" TextAlign="Center" Width="80" />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="loadComplete" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sdsCustomFields" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
                <iCtrl:IframeDialog ID="mdEditCustomField" Width="500" Height="260" Title="Edit Custom Field" Dragable="true"
                    TriggerSelectorClass="pop_edit" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
                    runat="server"></iCtrl:IframeDialog>
            </div>            
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntLeft" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        Search form</div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>               
                <h4>
                    <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"
                        CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="Search" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdCustomFields.ClientID %>";
        $grd = $("#<%=grdCustomFields.ClientID %>");
        var searchPanelID = "<%=SearchPanel.ClientID %>";

        //Function To Resize the grid
        function resize_the_grid() {
            $grd.fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }
        //Client side function before page change.
        function beforePageChange(pgButton) {
            $grd.onJqGridPaging();
        }
        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $grd.onJqGridSorting();
        }
        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();
        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);
        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        $("#<%=rblModule.ClientID%>").buttonset();

        $("#<%=rblModule.ClientID%> :radio").click(function () {
            $grd.setPostDataItem("mid", $(this).val());
            $grd.trigger("reloadGrid", [{ page: 1}]);
        });

        function loadComplete() {
            resize_the_grid();
        }

        function reloadGrid() {
            getGlobalMessage();
            $grd.trigger("reloadGrid");
        }

        function deleteField(rowID) {
            $grd.setSelection(rowID);
            var rowKey = $grd.getGridParam("selrow");
            $grd.jqGrid('delGridRow', rowKey, {
                errorTextFormat: function (data) {
                    if (data.responseText.substr(0, 6) == "<html ") {
                        return jQuery(data.responseText).html();
                    }
                    else {
                        return "<%=Resources.Resource.msgRecordDeletedSuccessfully%>";
                        // or
                        // return "Status: '" + data.statusText + "'. Error code: " +data.status;
                    }
                }
            });
        }
    </script>
</asp:Content>


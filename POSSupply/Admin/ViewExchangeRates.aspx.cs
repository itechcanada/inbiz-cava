﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_ViewExchangeRates : BasePage
{
   
    private SysCurrencies _cur = new SysCurrencies();
    // On Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            btnCreateCurrency.Visible = false;
        }

        if (!IsPostBack) {
            lblTitle.Text = Resources.Resource.ViewCurrency;
        }

        txtSearch.Focus();
    }
   


    protected void jgdvCurrency_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 4)
        {
            e.CellHtml = string.Format(@"<a class=""edit-Currency""  href=""AddEditCurrency.aspx?CurrencyID={0}&jscallback={1}"" >Edit</a>", e.RowValues[0], "reloadGrid");
        }
        if (e.ColumnIndex == 5)
        {
            string delUrl = string.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_CURRENCY, e.RowKey, "reloadGrid");
            e.CellHtml = string.Format(@"<a class=""pop_delete"" href=""{0}"">Delete</a>", delUrl);
        }
    }

    protected void jgdvCurrency_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains<string>("_history"))
        {            
            sqldsCurrency.SelectCommand = _cur.GetSql(sqldsCurrency.SelectParameters, Request.QueryString[txtSearch.ClientID]);
        }
        else
        {
            sqldsCurrency.SelectCommand = _cur.GetSql(sqldsCurrency.SelectParameters, Request.QueryString[txtSearch.ClientID]);
        }
    }
}
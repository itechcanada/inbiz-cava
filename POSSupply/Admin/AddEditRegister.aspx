﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" EnableEventValidation="true" ValidateRequest="false"
    CodeFile="AddEditRegister.aspx.cs" Inherits="Admin_AddEditRegister" %>

<%@ Register Src="../Controls/CommonSearch/CommonKeyword.ascx" TagName="CommonKeyword"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="contentBottom" style="padding: 5px; height: 410px; overflow: auto;" onkeypress="return disableEnterKey(event)">
        <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        <br />
        
        <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td class="text" style="width: 20%">
                    <asp:Label Text="<%$Resources:Resource, lblRegCode %>" ID="lblRegCode" runat="server" />*
                </td>
                <td class="input" style="width: 25%">
                    <asp:TextBox ID="txtRegCode" runat="server" Width="175Px" MaxLength="3" />
                    <asp:RequiredFieldValidator ID="reqvalRegCode" ErrorMessage="<%$ Resources:Resource, reqvalRegCode%>"
                        runat="server" ControlToValidate="txtRegCode" SetFocusOnError="true" Display="None"
                        ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>
                </td>
                <td class="text" style="width: 20%">
                    <asp:Label Text="<%$Resources:Resource, lblRegDescription %>" ID="lblRegDescription"
                        runat="server" />*
                </td>
                <td class="input" style="width: 25%">
                    <asp:TextBox ID="txtRegDescription" runat="server" MaxLength="75" Width="175Px" />
                    <asp:RequiredFieldValidator ID="reqvalRegDescription" ErrorMessage="<%$ Resources:Resource, reqvalRegDescription%>"
                        runat="server" ControlToValidate="txtRegDescription" SetFocusOnError="true" Display="None"
                        ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 10%">
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblRegAddress %>" ID="lblRegAddress" runat="server">
                    </asp:Label>*
                </td>
                <td class="input">
                    <asp:TextBox ID="txtRegAddressLine" runat="server" MaxLength="35" Width="175Px">
                    </asp:TextBox>

                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="<%$ Resources:Resource, reqvalAddress%>"
                        runat="server" ControlToValidate="txtRegAddressLine" Display="None" SetFocusOnError="true"
                        ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>
                </td>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblRegCity %>" ID="lblRegCity" runat="server" />*
                </td>
                <td class="input">
                    <asp:TextBox ID="txtRegCity" runat="server" Width="175Px" MaxLength="25" />
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="<%$ Resources:Resource, reqvalCity%>"
                        runat="server" ControlToValidate="txtRegCity" Display="None" SetFocusOnError="true"
                        ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblRegState %>" ID="lblRegState" runat="server" />*
                </td>
                <td class="input">
                    <asp:TextBox ID="txtRegState" runat="server" Width="175Px" MaxLength="25" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="<%$ Resources:Resource, reqvalState%>"
                        runat="server" ControlToValidate="txtRegState" Display="None" SetFocusOnError="true"
                        ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>
                </td>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblRegPostalCode %>" ID="lblRegPostalCode"
                        runat="server" />*
                </td>
                <td class="input">
                    <asp:TextBox ID="txtRegPostalCode" runat="server" Width="175Px" MaxLength="10" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="<%$ Resources:Resource, reqvalPostalCode%>"
                        runat="server" ControlToValidate="txtRegPostalCode" Display="None" SetFocusOnError="true"
                        ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblRegEmailID %>" ID="lblRegEmailID" runat="server" /><span
                        class="style1">*</span>
                </td>
                <td class="input">
                    <asp:TextBox ID="txtRegEmailID" runat="server" MaxLength="65" Width="175Px">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqvalRegEmailID" ErrorMessage="<%$ Resources:Resource, reqvalRegEmailID%>"
                        runat="server" ControlToValidate="txtRegEmailID" Display="None" SetFocusOnError="true"
                        ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revalRegEmailID" runat="server" ControlToValidate="txtRegEmailID"
                        Display="None" ErrorMessage="<%$ Resources:Resource, revalRegEmailID%>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ValidationGroup="RegisterUser"></asp:RegularExpressionValidator>
                </td>
                <td class="text">
                    <%--<asp:Label Text="<%$Resources:Resource, lblDefaultCustomerID %>" ID="lblCustomerID" runat="server" />--%>
                </td>
                <td class="input">
                    <%--<asp:TextBox ID="txtDefaultCustomerID" CssClass="numericTextField" runat="server" Width="175Px" MaxLength="15" />--%>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblRegPhone %>" ID="lblRegPhone" runat="server" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtRegPhone" runat="server" Width="175Px" MaxLength="15" />
                </td>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblRegFax %>" ID="lblRegFax" runat="server" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtRegFax" runat="server" Width="175Px" MaxLength="15" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblRegWarehouse %>" ID="lblRegWarehouse"
                        runat="server" />
                    *
                </td>
                <td class="input">
                    <asp:DropDownList ID="dlWarehouses" runat="server" Width="180px" CssClass="innerText">
                    </asp:DropDownList>
                    <asp:CustomValidator ID="custvalwarehouse" SetFocusOnError="true" runat="server"
                        ErrorMessage="<%$ Resources:Resource, custvalRegWarehouse %>" ClientValidationFunction="funSelectWarehouses"
                        Display="None" ValidationGroup="RegisterUser"></asp:CustomValidator>
                </td>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblSelectTaxGroup %>" ID="lblSelectTaxGroup"
                        runat="server" />
                    <span class="style1">*</span>
                </td>
                <td class="input">
                    <asp:DropDownList ID="dlTaxCode" runat="server" Width="180px">
                    </asp:DropDownList>
                    <asp:CustomValidator ID="custvalTaxCode" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, custvalRegTaxCode%>"
                        ClientValidationFunction="funCheckSelTaxGroup" Display="None" ValidationGroup="RegisterUser"></asp:CustomValidator>
                </td>
                <td>
                </td>
            </tr>
            <tr runat="server" id="trIsActive" visible="false">
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblRegActive %>" ID="lblRegActive" runat="server" />
                </td>
                <td class="input">
                    <asp:RadioButton Text="<%$Resources:Resource, lblYes %>" ID="rdbYes" runat="server"
                        Checked="true" GroupName="reg" />
                    <asp:RadioButton Text="<%$Resources:Resource, lblNo %>" ID="rdbNo" runat="server"
                        GroupName="reg" />
                </td>
                <td class="text" visible="false">
                    <asp:Label Text="<%$Resources:Resource, lblMerchantID %>" ID="lblMerchantID" runat="server" />
                    *
                </td>
                <td class="input" visible="false">
                    <asp:TextBox ID="txtMerchantID" runat="server" Width="175Px" MaxLength="20" />
                    <asp:RequiredFieldValidator ID="reqvalMerchantID" ErrorMessage="*" runat="server"
                        ControlToValidate="txtRegEmailID" Display="None" SetFocusOnError="true" ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>
                </td>
                <td>
                </td>
            </tr>
            <tr style="display:none;">
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblRegHost %>" ID="lblRegHost" runat="server" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtRegHost" runat="server" Width="175Px" MaxLength="20" />
                </td>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblRegHostPort %>" ID="lblRegHostPort" runat="server" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtRegHostPort" runat="server" Width="175Px" MaxLength="10" />
                </td>
                <td>
                </td>
            </tr>
            <tr style="display:none;">
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblRegTerminal %>" ID="lblRegTerminal" runat="server" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtRegTerminal" runat="server" Width="175Px" MaxLength="10" />
                </td>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblRegLogFile %>" ID="lblRegLogFile" runat="server" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtLogFile" runat="server" Width="175Px" MaxLength="100" />
                    <asp:RegularExpressionValidator ID="revalRegFilePath" runat="server" ControlToValidate="txtLogFile"
                        SetFocusOnError="true" Display="None" ErrorMessage="<%$ Resources:Resource, revalRegFilePath %>"
                        ValidationExpression="^((file:|File:|FILE:)(\\[a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.log|.LOG|.Log)$"
                        ValidationGroup="RegisterUser">
                    </asp:RegularExpressionValidator><br />
                    <span style="font-size: 10px; color: black;">(eg. file:\x:\xxx\xxx.log)</span>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblRegMessage %>" ID="lblRegMessage" runat="server" />
                </td>
                <td class="input">
                    <asp:TextBox ID="txtRegMessage" runat="server" Width="175Px" TextMode="MultiLine" 
                        Rows="3" />
                </td>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblPrintMerchantCopy %>" ID="lblPrintMerchantCopy"
                        runat="server" />
                </td>
                <td class="input">
                    <asp:CheckBox Text="<%$Resources:Resource, chkPrintMerchantCopy %>" ID="chkPrintMerchantCopy"
                        runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblDefaultCustomerID %>" ID="lblCustomerID"
                        runat="server" /><span class="style1">*</span>
                </td>
                <td class="input">
                    <asp:TextBox ID="txtDefaultCustomerID" CssClass="numericTextField" runat="server"
                        Width="175Px" MaxLength="15" />
                    <asp:RequiredFieldValidator ID="reqvalDefaultCustID" ErrorMessage="<%$ Resources:Resource, reqvalDefaultCustomerID%>"
                        runat="server" ControlToValidate="txtDefaultCustomerID" Display="None" SetFocusOnError="true"
                        ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>
                </td>
                <td class="text">
                    <asp:Label Text="<%$Resources:Resource, lblManagerCode %>" ID="lblMangerCode" runat="server" /><%--<span
                        class="style1">*</span>--%>
                </td>
                <td class="input">
                    <asp:TextBox ID="txtManageCode" runat="server" Width="175Px" 
                        MaxLength="50" /> <%--TextMode="Password"--%>
                    <%--<asp:RequiredFieldValidator ID="reqvalManagerCode" ErrorMessage="<%$ Resources:Resource, reqvalManagerCode%>"
                        runat="server" ControlToValidate="txtManageCode" Display="None" SetFocusOnError="true"
                        ValidationGroup="RegisterUser"></asp:RequiredFieldValidator>--%>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
    <div class="div-dialog-command" style="border-top: 1px solid #ccc; padding: 5px;">
        <asp:Button Text="<%$Resources:Resource, btnSave %>" ID="btnSave" runat="server"
            ValidationGroup="RegisterUser" OnClick="btnSave_Click" />
        <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
            CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
    </div>
    <asp:ValidationSummary ID="valsAdminWarehouse" runat="server" ShowMessageBox="true"
        ShowSummary="false" ValidationGroup="RegisterUser" />
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
    <script type="text/javascript">
        function funCheckSelTaxGroup(source, args) {
            if (document.getElementById('<%=dlTaxCode.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
        function funSelectWarehouses(source, arguments) {
            if (window.document.getElementById('<%=dlWarehouses.ClientID%>').value != "0") {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }        
    </script>
</asp:Content>

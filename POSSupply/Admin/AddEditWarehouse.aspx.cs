﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Admin_AddEditWarehouse : BasePage
{    
    SysWarehouses _whs = new SysWarehouses();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Globals.RegisterParentRedirectPageUrl(this, ConfigurationManager.AppSettings["VertualDirectoryAlias"] + "Errors/AccessDenied.aspx");
        }
        
        if (!IsPostBack)
        {                        
            FillRecord();
            txtWarehouseCode.Focus();
        }
    }

    //Populate Controls
    public void FillRecord()
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            dbHelp.Connection.Open();
            SysTaxCodeDesc tg = new SysTaxCodeDesc();
            SysCompanyInfo ci = new SysCompanyInfo();
            tg.FillListControl(dbHelp, dlTaxGroup, new ListItem(Resources.Resource.liTaxGroup, "0"));
            ci.FillCompany(dbHelp, dlCompanyName, new ListItem(Resources.Resource.litComapnyName, "0"));
            _whs.FillWharehouse(dbHelp, ddlShippingWhs, string.Empty, -1, new ListItem(""));
            _whs.FillWharehouse(dbHelp, ddlActiveBin, string.Empty, -1, new ListItem(""));
            _whs.FillWharehouse(dbHelp, ddlReservedBin, string.Empty, -1, new ListItem(""));
            _whs.FillWharehouse(dbHelp, ddlDamagedBin, string.Empty, -1, new ListItem(""));

            if (this.WarehouseCode.Length > 0)
            {
                _whs.WarehouseCode = this.WarehouseCode;
                trIsActive.Visible = true;
                _whs.PopulateObject(_whs.WarehouseCode, dbHelp);
                //Get Warehouse Info
                txtWarehouseCode.Text = _whs.WarehouseCode;
                txtWarehouseCode.ReadOnly = true;
                txtWarehouseDescription.Text = _whs.WarehouseDescription;
                txtWarehouseAddressLine1.Text = _whs.WarehouseAddressLine1;
                txtWarehouseAddressLine2.Text = _whs.WarehouseAddressLine2;
                txtWarehouseCity.Text = _whs.WarehouseCity;
                txtWarehouseState.Text = _whs.WarehouseState;
                txtWarehouseCountry.Text = _whs.WarehouseCountry;
                txtWarehousePostalCode.Text = _whs.WarehousePostalCode;
                txtWarehousePrinterID.Text = _whs.WarehousePrinterID;
                txtWarehouseEmailID.Text = _whs.WarehouseEmailID;
                txtWarehouseFax.Text = _whs.WarehouseFax;
                txtWarehousePhone.Text = _whs.WarehousePhone;
                txtAddPoints.Text = _whs.AddPoints.ToString();
                txtSpent.Text = BusinessUtility.GetCurrencyString(_whs.AddAmtForPoints,Globals.CurrentCultureName).Replace("$","");
                txtRedeemPoints.Text = _whs.RedeemPoints.ToString();
                txtRedeem.Text = BusinessUtility.GetCurrencyString(_whs.RedeemAmtForPoints, Globals.CurrentCultureName).Replace("$","");
                txtMinPoints.Text = _whs.MinPointsForRedemption.ToString();
                rdbYes.Checked = _whs.WarehouseActive;
                chkShowInProductQuery.Checked = _whs.ShowInProductQuery;
                if (_whs.WarehouseRegTaxCode != 0)
                {
                    dlTaxGroup.SelectedValue = _whs.WarehouseRegTaxCode.ToString();
                }
                else
                {
                    dlTaxGroup.SelectedValue = "0";
                }
                if (_whs.WarehouseCompanyID > 0)
                {
                    dlCompanyName.SelectedValue = _whs.WarehouseCompanyID.ToString();
                }
                else
                {
                    dlCompanyName.SelectedValue = "0";
                }
                if (ddlShippingWhs.Items.Count > 1)
                {
                    ListItem liToRemove = ddlShippingWhs.Items.FindByValue(_whs.WarehouseCode);
                    if (liToRemove != null)
                    {
                        ddlShippingWhs.Items.Remove(liToRemove);
                    }
                    ListItem liToSelect = ddlShippingWhs.Items.FindByValue(_whs.ShippingWarehouse);
                    if (liToSelect != null)
                    {
                        liToSelect.Selected = true;
                    }
                }
                if (_whs.ActiveWarehouse != null)
                {
                    ddlActiveBin.SelectedValue = _whs.ActiveWarehouse.ToString();
                }
                if (_whs.ReservedWarehouse != null)
                {
                    ddlReservedBin.SelectedValue = _whs.ReservedWarehouse.ToString();
                }
                if (_whs.DamagedWarehouse != null)
                {
                    ddlDamagedBin.SelectedValue = _whs.DamagedWarehouse.ToString();
                }
            }
        }
        catch
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }        
    }
    //Populate Object
    private void SetData()
    {
        _whs.WarehouseCode = txtWarehouseCode.Text;
        _whs.WarehouseDescription = txtWarehouseDescription.Text;
        _whs.WarehouseAddressLine1 = txtWarehouseAddressLine1.Text;
        _whs.WarehouseAddressLine2 = txtWarehouseAddressLine2.Text;
        _whs.WarehouseCity = txtWarehouseCity.Text;
        _whs.WarehouseState = txtWarehouseState.Text;
        _whs.WarehouseCountry = txtWarehouseCountry.Text;
        _whs.WarehousePostalCode = txtWarehousePostalCode.Text;
        _whs.WarehousePrinterID = txtWarehousePrinterID.Text;
        _whs.WarehouseEmailID = txtWarehouseEmailID.Text;
        _whs.WarehouseFax = txtWarehouseFax.Text;
        _whs.WarehousePhone = txtWarehousePhone.Text;
        _whs.WarehouseActive = rdbYes.Checked;
        _whs.WarehouseRegTaxCode = BusinessUtility.GetInt(dlTaxGroup.SelectedValue);
        _whs.WarehouseCompanyID = BusinessUtility.GetInt(dlCompanyName.SelectedValue);
        _whs.ShippingWarehouse = ddlShippingWhs.SelectedValue;
        _whs.AddPoints = BusinessUtility.GetInt(txtAddPoints.Text);
        _whs.AddAmtForPoints = BusinessUtility.GetDoubleValueFromCurrencyString(txtSpent.Text, Globals.CurrentCultureName);
        _whs.RedeemPoints = BusinessUtility.GetInt(txtRedeemPoints.Text);
        _whs.RedeemAmtForPoints = BusinessUtility.GetDoubleValueFromCurrencyString(txtRedeem.Text, Globals.CurrentCultureName);
        _whs.MinPointsForRedemption = BusinessUtility.GetInt(txtMinPoints.Text);
        _whs.ShowInProductQuery = chkShowInProductQuery.Checked==true;
        _whs.ActiveWarehouse = ddlActiveBin.SelectedValue;
        _whs.ReservedWarehouse = ddlReservedBin.SelectedValue;
        _whs.DamagedWarehouse = ddlDamagedBin.SelectedValue;
    }    

    protected void  btnSave_Click(object sender, System.EventArgs e)
    {
        if (Page.IsValid)
        {
            SetData();
            if (this.WarehouseCode.Length > 0)
            {
                _whs.WarehouseCode = this.WarehouseCode;
                _whs.Update(null);
                //Update Warehouse 

                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.WarehouseUpdatedSuccessfully);
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                }
                else
                {
                    Globals.RegisterCloseDialogScript(Page);
                }                    
            }
            else
            {
                try
                {
                    _whs.Insert(null);
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.WarehouseAddedSuccessfully);
                    if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                    {
                        Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                    }
                    else
                    {
                        Globals.RegisterCloseDialogScript(Page);
                    }    
                }
                catch (Exception ex)
                {
                    switch (ex.Message) { 
                        case CustomExceptionCodes.WAREHOUSE_CODE_ALREADY_EXISTS:
                            lblError.Text = Resources.Resource.WarehouseCodeAlreadyExists;
                            break;
                        case CustomExceptionCodes.WAREHOUSE_DESCRIPTION_ALREADY_EXISTS:
                            lblError.Text = Resources.Resource.WarehouseDescriptionAlreadyExists;
                            break;
                        default:                            
                            lblError.Text = "Critical Error!";
                            break;
                    }
                }
            }
        }
    }

    private string WarehouseCode
    {
        get {
            return BusinessUtility.GetString(Request.QueryString["WarehouseID"]);
        }
    }

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }

    protected void lnbWhsSalesGoals_OnClick(object sender, System.EventArgs e)
    {
        Globals.RegisterParentRedirectPageUrl(this, ConfigurationManager.AppSettings["VertualDirectoryAlias"] + "Admin/ViewUserSalesGoal.aspx?WhsCode=" + this.WarehouseCode);
    }
}
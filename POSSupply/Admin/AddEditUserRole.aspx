﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="AddEditUserRole.aspx.cs" Inherits="Admin_AddEditUserRole" %>

<%@ Register Src="~/Controls/UsersLeft.ascx" TagName="UserLeftMenu" TagPrefix="uc2" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphHead">
    <%--Keep script file reference here those were required for this page only. 
    Note* if it is to being used on all pages than should pe kept on master page.--%>
<%--    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>--%>
        <link href="../lib/css/accordioninbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet"
        type="text/css" />
    <style type="text/css">
        #contentBottom .contentTableForm td.text
        {
            width: 200px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphLeft" runat="Server">
    <asp:Panel runat="server" CssClass="divSectionContent" ID="LeftMenu">
        <uc2:UserLeftMenu ID="UserLeftMenu" runat="server" />
    </asp:Panel>
</asp:Content>
<asp:Content ID="cntViewTaxes" ContentPlaceHolderID="cphMaster" runat="Server">
    <asp:Panel ID="pnlRole" runat="server" >
        <div class="divMainContainerTitle">
            <h3>
                <%= Resources.Resource.lblAssociateModules%>
            </h3>
        </div>
        <asp:Panel CssClass="divMainContent" ID="pnlModules" runat="server">
            <div class="accordionInbiz">
                <div class="accordion">
                    <asp:Repeater ID="rptModules" runat="server" DataSourceID="sdsModules" OnItemDataBound="rptModules_ItemDataBound">
                        <ItemTemplate>
                            <h3>
                                <a href="javascript:void(0);">
                                    <input class="m_check" id="chkModule" type="checkbox" value='<%#Eval("ModuleID")%>'
                                        runat="server" sub_items='<%#"div_"+Container.ItemIndex.ToString() %>' />
                                    <asp:Label ID="lblUserType" Text='<%#Eval("ModuleDisplayName")%>' runat="server" /></a></h3>
                            <div id='<%#"div_"+Container.ItemIndex.ToString() %>'>
                                <asp:DataList ID="dlstRoles" RepeatColumns="3" RepeatDirection="Horizontal" Width="100%"
                                    OnItemDataBound="dlstRoles_ItemDataBound" runat="server">
                                    <ItemStyle Width="200px" />
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <input id="chkRole" class="chk_role_key" type="checkbox" value='<%#Eval("RoleID")%>'
                                                        runat="server" disabled="disabled" onclick="setSelectedRoles()" />
                                                    <asp:HiddenField ID="hdnRoleID" runat="server" Value='<%#Eval("RoleID")%>' />
                                                </td>
                                                <td style="width: 200px;">
                                                    <asp:Label ID="lblRoleName" Text='<%#Eval("RoleDesc")%>' runat="server" />
                                                    <asp:HiddenField ID="hdnOldKey" runat="server" Value='<%#Eval("OldSecurityKey")%>' />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <asp:SqlDataSource ID="sdsModules" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                ProviderName="MySql.Data.MySqlClient"></asp:SqlDataSource>
            <div style="display: none">
                <asp:TextBox Width="240px" ID="txtUserModules" runat="server"></asp:TextBox>
            </div>
            <script language="javascript" type="text/javascript">
                function MakeEnableModules(cbControl, state) {
                    var chkBoxList = document.getElementById(cbControl);
                    var chkBoxCount = chkBoxList.getElementsByTagName("input");
                    var ddl = chkBoxList.getElementsByTagName("select");
                    for (var i = 0; i < chkBoxCount.length; i++) {
                        chkBoxCount[i].checked = state;
                        chkBoxCount[i].disabled = !state;
                        ddl[i].disabled = !state;
                    }
                    //chkBoxList.checked = state;
                    //return false;
                }

                function SelectAllModules(chkUserType, chkItems, state) {
                    var chkUserType = document.getElementById(chkUserType);
                    var chkBoxList = document.getElementById(chkItems);
                    var chkBoxCount = chkBoxList.getElementsByTagName("input");
                    var ddl = chkBoxList.getElementsByTagName("select");
                    for (var i = 0; i < chkBoxCount.length; i++) {
                        chkBoxCount[i].checked = state;
                        chkBoxCount[i].disabled = !state;
                        ddl[i].disabled = !state;
                    }
                    chkUserType.checked = state;
                    //return false;
                }

                $(".m_check").click(function (evt) {
                    evt.stopPropagation();
                    $this = $(this);
                    $relContainer = $("#" + $this.attr("sub_items"));
                    $("input:checkbox", $relContainer).each(function () {
                        this.checked = $this.is(':checked');
                        this.disabled = !$this.is(':checked');
                    });
                    setSelectedRoles();
                });

                function setSelectedRoles() {
                    var roles = [];
                    $(".chk_role_key").each(function () {
                        if (this.checked) {
                            roles.push($(this).next().val());
                        }
                    });
                    $("#<%=hdnRoles.ClientID%>").val(roles.join(","));
                }
                $(document).ready(function () {
                    setSelectedRoles();
                });

                $(function () {
                    $(".accordion").accordion();
                });         
            </script>
        </asp:Panel>
    </asp:Panel>
    <asp:HiddenField ID="hdnRoles" runat="server" />
    <div class="div_command">
        <asp:Button Text="<%$Resources:Resource, btnSave %>" ID="btnSave" runat="server"
            OnClick="btnSave_Click" />
        <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnReset" runat="server"
            CausesValidation="False" OnClick="btnReset_Click" />
    </div>
    <asp:ValidationSummary ID="valsumOrderType" ValidationGroup="OderType" runat="server"
        ShowMessageBox="true" ShowSummary="false" />
    <asp:ValidationSummary ID="valsAdmin" runat="server" ShowMessageBox="true" ShowSummary="false" />
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
</asp:Content>

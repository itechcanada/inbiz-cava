﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.WebControls;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;

public partial class Admin_ViewWarehouse : BasePage
{
  
    private string strWhsID = "";
    private SysWarehouses _objWarehouses = new SysWarehouses();
    // On Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            btnWareHouse.Visible = false;
        }

        if (!IsPagePostBack(jgdvWarehouse)) {
            lblTitle.Text = Resources.Resource.ViewWarehouse;
        }

        txtSearch.Focus();
    }
   


    protected void jgdWarehouse_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 3)
        {
            e.CellHtml = string.Format(@"<a class=""edit-Warehouse""  href=""AddEditWarehouse.aspx?WarehouseID={0}&jscallback={1}"" >" + Resources.Resource.grdEdit + "</a>", e.RowValues[0], "reloadGrid");
        }
        if (e.ColumnIndex == 4)
        {           
            string delUrl = string.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_WAREHOUSE, e.RowKey, "reloadGrid");
            e.CellHtml = string.Format(@"<a class=""pop_delete"" href=""{0}"">" + Resources.Resource.delete + "</a>", delUrl);
        }
    }

    protected void jgdWarehouse_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains<string>("_history"))
        {
            sqldsWarehouse.SelectCommand = _objWarehouses.GetSQL(sqldsWarehouse.SelectParameters, Request.QueryString[txtSearch.ClientID]);
        }
        else
        {
            sqldsWarehouse.SelectCommand = _objWarehouses.GetSQL(sqldsWarehouse.SelectParameters, Request.QueryString[txtSearch.ClientID]);
        }
    }
}
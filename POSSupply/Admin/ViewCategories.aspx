﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false" CodeFile="ViewCategories.aspx.vb" Inherits="Admin_ViewCategories" %>

<%@ Register src="../Controls/CommonSearch/CommonKeyword.ascx" tagname="CommonKeyword" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphLeftPanel" Runat="Server">
     <uc1:CommonKeyword ID="CommonKeyword1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
     <div id="divMainContainerTitle" class="divMainContainerTitle">
        <h2>
            <%=Resources.Resource.lblCustomerCategories %>
        </h2>  
    </div>
    <div id="divMainContent" class="divMainContent">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
             <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:DropDownList ID="ddlAppLanguages" runat="server" 
                        DataSourceID="sdsAppLang" Width="330px" DataTextField="AppLanguageName" 
                        DataValueField="AppLanguageID" AutoPostBack="true">                        
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsAppLang" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:NewConnectionString %>" 
                        ProviderName="<%$ ConnectionStrings:NewConnectionString.ProviderName %>" 
                        SelectCommand="SELECT AppLanguageID, AppLanguageName FROM cdl_sysapplanguages WHERE (IsActive = 1)" ></asp:SqlDataSource>
                </td>
                <td align="right">
                    <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Search" 
                        ImageUrl="../images/xls.png" />
                </td>
            </tr>
            <tr>
                <td colspan="2" height="350" valign="top">
                    <asp:GridView ID="grdRegister" runat="server" AllowSorting="True" DataSourceID="sqlDsCategories"
                        AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0"
                        GridLines="None" AutoGenerateColumns="False" Style="border-collapse: separate;"
                        CssClass="view_grid650" UseAccessibleHeader="False"
                        Width="100%" EnableModelValidation="True" DataKeyNames="CategoryID,DescriptionID">
                        <Columns>
                            <asp:BoundField DataField="CategoryID" HeaderText="<%$ Resources:Resource, grdCategoryID %>"
                                ReadOnly="True" SortExpression="CategoryID">
                                <ItemStyle Width="10%" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TextDescription" HeaderText="<%$ Resources:Resource, grdCategoryName %>"
                                ReadOnly="True" SortExpression="TextDescription">
                                <ItemStyle Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdEdit %>" HeaderStyle-ForeColor="#ffffff"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <a href='AddEditCategories.aspx?cid=<%# Eval("CategoryID") %>'>
                                        <img src="../images/edit_icon.png" alt="" />
                                    </a>                                    
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" HeaderStyle-ForeColor="#ffffff"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                        CommandArgument='<%# Eval("CategoryID") %>' CommandName="Delete" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="grid_footer" />
                        <RowStyle CssClass="grid_rowstyle" />
                        <PagerStyle CssClass="grid_footer" />
                        <HeaderStyle CssClass="grid_header" Height="26px" />
                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                        <PagerSettings PageButtonCount="20" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqlDsCategories" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:NewConnectionString %>" 
                        ProviderName="<%$ ConnectionStrings:NewConnectionString.ProviderName %>" 
                        SelectCommand="SELECT cdl_syscategories.CategoryID, cdl_syscategories.CategoryName, cdl_syscategories.IsActive, cdl_textdescriptor.AppLanguageID, cdl_textdescriptor.TextDescription, cdl_textdescriptor.DescriptionID FROM cdl_syscategories INNER JOIN cdl_textdescriptor ON cdl_syscategories.CategoryName = cdl_textdescriptor.DescriptionID WHERE (cdl_textdescriptor.TextDescription LIKE CONCAT(@SData, '%')) AND (cdl_textdescriptor.AppLanguageID = @AppLanguageID)" >
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlAppLanguages" DefaultValue="1" 
                                Name="@AppLanguageID" PropertyName="SelectedValue" />
                            <asp:QueryStringParameter Name="@SData" QueryStringField="sdata" DefaultValue=""
                                ConvertEmptyStringToNull="False" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
        </table>
    </div>

     <script type="text/javascript" language="javascript">
         $("#divMainContainerTitle").corner();        
    </script>
</asp:Content>


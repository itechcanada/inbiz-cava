﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

public partial class Admin_ViewUserSalesGoal : BasePage
{
    private string strTaxID = "";
    private UserSalesGoal objUserSalesGoal = new UserSalesGoal();
    private WarehouseSalesGoal objWhsSalesGoal = new WarehouseSalesGoal();
    // On Page Load
    protected void Page_Load(object sender, System.EventArgs e)
    {
        //Security Check                  
        if (this.UserID > 0 && this.UserID != CurrentUser.UserID && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }
        //if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        //{
        //    Response.Redirect("~/AdminLogin.aspx");
        //}
        if (!IsPagePostBack(jgdvSalesGoal))
        {
            string sPosLink = "AddEditSalesGoal.aspx";
            if (IsCallFromPOS == true)
            {
                sPosLink += "?InvokeSrc=POS";
            }
            if (this.SalesGoalUserID > 0)
            {
                mdCreateSalesGoal.Url = sPosLink + "?UserID=" + this.SalesGoalUserID;
                ltrPageTitle.Text = Resources.Resource.lnkUserSalesGoal;

                //lnkAddClockIn.Text = Resources.Resource.lnkUserSalesGoal;
                ltrTitle2.Text = Resources.Resource.lblView + " : " + Resources.Resource.lnkUserSalesGoal;
            }
            else
            {
                mdCreateSalesGoal.Url = sPosLink + "?whsCode=" + this.SalesGoalWhsCode;
                ltrPageTitle.Text = Resources.Resource.lblLocationSalesGoals + " - " + this.SalesGoalWhsCode;

                //lnkAddClockIn.Text = Resources.Resource.lblLocationSalesGoals;
                ltrTitle2.Text = Resources.Resource.lblView + " : " + Resources.Resource.lblLocationSalesGoals;
            }


            InbizUser objUser = new InbizUser();
            ListItem liItem = new ListItem { Text = Resources.Resource.chkPOSAlluser, Value = "0" };
            objUser.FillAllUsers(null, dlUser, liItem);
            ListItem lstFindUser = dlUser.Items.FindByValue(BusinessUtility.GetString("0"));
            if (lstFindUser != null)
            {
                lstFindUser.Selected = true;
            }
            if (Convert.ToString(Session["UserRole"]) == "1")
            {
                lnkAddClockIn.Text = "";
            }
            dlUser.Focus();
            //txtSearch.Focus();
        }
    }

    protected void jgdvSalesGoal_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 3)
        {
            e.CellHtml = "$ "+ e.CellHtml;
        }

        if (e.ColumnIndex == 4)
        {
            string sPosLink = "";
            if (IsCallFromPOS == true)
            {
                sPosLink = "&InvokeSrc=POS";
            }
            if(this.SalesGoalUserID>0)
            e.CellHtml = string.Format("<a class=edit-Tax  href=AddEditSalesGoal.aspx?IDSalesGoal={0}&UserID={1}&jscallback={2}" + sPosLink + " >" + Resources.Resource.grdEdit + "</a>", e.RowValues[0], this.SalesGoalUserID, "reloadGrid");
            else
                e.CellHtml = string.Format("<a class=edit-Tax  href=AddEditSalesGoal.aspx?IDSalesGoal={0}&whsCode={1}&jscallback={2}" + sPosLink + " >" + Resources.Resource.grdEdit + "</a>", e.RowValues[0], this.SalesGoalWhsCode, "reloadGrid");
        }
    }

    protected void jgdvSalesGoal_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (this.SalesGoalUserID > 0)
            jgdvSalesGoal.DataSource = objUserSalesGoal.GetSalesGoalList(null, this.SalesGoalUserID, 0);
        else
            jgdvSalesGoal.DataSource = objWhsSalesGoal.GetSalesGoalList(null, this.SalesGoalWhsCode, 0);
    }

    private bool IsCallFromPOS
    {
        get
        {
            if (BusinessUtility.GetString(Request.QueryString["InvokeSrc"]).ToUpper() == "POS".ToUpper())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    private int SalesGoalUserID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["UserID"], out id);
            return id;
        }
    }

    public int UserID
    {
        get
        {
            int uid = 0;
            int.TryParse(Request.QueryString["UserID"], out uid);
            return uid;
        }
    }
    private string SalesGoalWhsCode
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["whsCode"]))
            {
                return BusinessUtility.GetString(Request.QueryString["whsCode"]);
            }
            else
                return "";
        }
    }
}
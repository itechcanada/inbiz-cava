﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ViewGlobalParameters.aspx.cs" Inherits="Admin_ViewGlobalParameters" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="cphHead" runat="Server">
   
</asp:Content>
<asp:Content ID="cntTop" ContentPlaceHolderID="cphTop" runat="Server">
    
        <div class="col1">
            <img src="../lib/image/icon/ico_admin.png" />
        </div>
        <div class="col2">
            <h1>
                <%= Resources.Resource.lblAdministration%></h1>
            <b>
                <%= Resources.Resource.lblView%>:
                <%= Resources.Resource.lblGlobalParameter%></b>
        </div>
     
    <div style="float: left; padding-top: 12px;">
        <asp:Button ID="btnAddCompany" runat="server" Text="<%$ Resources:Resource,btnimgAddCompany %>" OnClick="btnAddCompany_Click" Visible="false"  />
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif" CssClass="csv_export" />        
    </div>
     <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="cntLeft" ContentPlaceHolderID="cphLeft" runat="Server">
    <div >
        <asp:Panel runat="server" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        <%= Resources.Resource.lblSearchForm%></div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch" CssClass="filter-key-txt"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntMaster" ContentPlaceHolderID="cphMaster" runat="Server">
    <div >
        <div id="grid_wrapper" style="width: 100%;">
            <trirand:JQGrid runat="server" ID="jgdvGlobalParameter" DataSourceID="sqldsCompany"
                Height="300px" AutoWidth="True" OnCellBinding="jgdvGlobalParameter_CellBinding"
                OnDataRequesting="jgdvGlobalParameter_DataRequesting">
                <Columns>
                    <trirand:JQGridColumn DataField="CompanyID" HeaderText="" PrimaryKey="True" Visible="false" />
                    <trirand:JQGridColumn DataField="CompanyName" HeaderText="<%$ Resources:Resource, grdCompanyName %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="CompanyEmail" HeaderText="<%$ Resources:Resource, grdCompanyEmail %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="CompanyBasCur" HeaderText="<%$ Resources:Resource, grdCompanyBasCur %>"
                        Editable="false" />
                    <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="CompanyID"
                        Sortable="false" TextAlign="Center" Width="50" />
                    <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="CompanyID"
                        Sortable="false" TextAlign="Center" Width="50" />
                </Columns>
                <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                    ShowDeleteButton="false" ShowSearchButton="false" />
                <SortSettings InitialSortColumn=""></SortSettings>
                <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" 
                    LoadComplete="gridLoadComplete" />
            </trirand:JQGrid>
            <iCtrl:IframeDialog ID="mdDeleteUser" Width="400"  Height="120" Title="Delete"
                Dragable="true" TriggerSelectorClass="pop_delete" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
                runat="server">
            </iCtrl:IframeDialog>
            <asp:SqlDataSource ID="sqldsCompany" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                ProviderName="MySql.Data.MySqlClient"></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=jgdvGlobalParameter.ClientID %>";
        var searchPanelID = "<%=SearchPanel.ClientID %>";


        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        //Callback on grid load complelte 
        function gridLoadComplete(data) {
            $(window).trigger("resize");
        }

        //Reload grid function
        function reloadGrid(event, ui) {            
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }
    </script>
</asp:Content>



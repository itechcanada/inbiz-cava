﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_CategoryEdit : BasePage
{
    Category _categ = new Category();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            BindData();
            txtCategoryEn.Focus();
        }        
    }

    private void BindData()
    {

        DropDownHelper.FillDropdown(ddlCategoryGroup, "CT", "dlGrp", Globals.CurrentAppLanguageCode, null);        

        _categ.FillParentCategory(ddlParentCategory, Globals.CurrentAppLanguageCode, this.CategoryID, StatusCategoryGroup.ALL, new ListItem("-------", "0"));

        if (CategoryID > 0) {
            _categ.PopulateObject(this.CategoryID);
            ddlCategoryGroup.SelectedValue = _categ.CategoryGroup;
            ddlParentCategory.SelectedValue = _categ.ParentID.ToString();

            txtCategoryEn.Text = _categ.CategoryEn;
            txtCategoryFr.Text = _categ.CategoryFr;
        }
    }

    private void SetData() {
        _categ.CategoryID = this.CategoryID;
        _categ.CategoryEn = txtCategoryEn.Text;
        _categ.CategoryFr = txtCategoryFr.Text;
        _categ.CategoryGroup = ddlCategoryGroup.SelectedValue;
        _categ.ParentID = BusinessUtility.GetInt(ddlParentCategory.SelectedValue);
        _categ.IsEditable = true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid) {
            SetData();
            if (this.CategoryID > 0) {
                OperationResult res = _categ.Update();
                if (res == OperationResult.AlreadyExists) {
                    MessageState.SetGlobalMessage(MessageType.Failure, "Provided category name already exists under selected group!");
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Success, "Category updated successfully!");

                    if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                    {
                        Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                    }
                    else
                    {
                        Globals.RegisterCloseDialogScript(Page);
                    }                     
                }
            }
            else
            {
                OperationResult res = _categ.Insert();
                if (res == OperationResult.AlreadyExists)
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, "Provided category name already exists under selected group!");
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Success, "Category added successfully!");
                    if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                    {
                        Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                    }
                    else
                    {
                        Globals.RegisterCloseDialogScript(Page);
                    }     
                }
            }
        }
    }

    private int CategoryID {
        get {
            int id = 0;
            int.TryParse(Request.QueryString["catid"], out id);
            return id;
        }
    }

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }
}
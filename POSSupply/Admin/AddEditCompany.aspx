﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="AddEditCompany.aspx.cs" Inherits="Admin_AddEditCompany" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="cphHead" runat="Server">
    <style type="text/css">
          ul.form li div.lbl {width:180px;}
    </style>
</asp:Content>
<asp:Content ID="cntTop" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" alt="" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.Administration %></h1>
        <b>
            <%= Resources.Resource.lblAddEdit %>:<%= Resources.Resource.lblCompany %></b>
    </div>    
    <div class="clear">
    </div>
</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphLeft" runat="Server">
    <ul id="custMenu" class="menu inbiz-left-menu collapsible">
        <li class="active"><a href="#">Action</a>
            <ul style="display: block;">
                <li><a href="ViewGlobalParameters.aspx">
                    <%=Resources.Resource.lblGlobalParameter %></a> </li>
            </ul>
        </li>
    </ul>
</asp:Content>

<asp:Content ID="cntAddEditCompany" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <h3>
            <asp:Literal Text="<%$Resources:Resource, lblCompanyAddress %>" runat="server" /></h3>
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="lblCompanyName" Text="<%$Resources:Resource, lblCompanyName %>" runat="server" />*
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyName" runat="server" MaxLength="50" />
                    <asp:RequiredFieldValidator ID="reqvalCompanyName" runat="server" ControlToValidate="txtCompanyName"
                        SetFocusOnError="true" Display="None" ValidationGroup="cmpnySave" ErrorMessage="<%$Resources:Resource, reqvalCompanyName %>"></asp:RequiredFieldValidator>
                </div>
                <div class="lbl">
                    <asp:Label ID="lblCompanyAddressLine1" Text="<%$Resources:Resource, lblCompanyAddressLine1 %>"
                        runat="server"> 
                    </asp:Label>
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyAddressLine1" runat="server" MaxLength="35">
                    </asp:TextBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblCompanyAddressLine2" Text="<%$Resources:Resource, lblCompanyAddressLine2 %>"
                        runat="server">
                    </asp:Label>
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyAddressLine2" runat="server" MaxLength="35">
                    </asp:TextBox>
                </div>
                <div class="lbl">
                    <asp:Label ID="lblCompanyCity" Text="<%$Resources:Resource, lblCompanyCity %>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyCity" runat="server" MaxLength="25" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblCompanyCountry" Text="<%$Resources:Resource, lblCompanyCountry %>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyCountry" runat="server" MaxLength="25" Style="display: none;" />
                    <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                    </asp:DropDownList>
                    <script type="text/javascript">                        
                        $("#<%=ddlState.ClientID%>").change(function () {
                            $("#<%=txtCompanyState.ClientID%>").val($(this).val());
                        });
                    </script>
                </div>
                <div class="lbl">
                    <asp:Label ID="lblCompanyState" Text="<%$Resources:Resource, lblCompanyState %>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyState" runat="server" MaxLength="25" Style="display: none;" />
                    <asp:DropDownList ID="ddlState" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="lblCompanyPostalCode" Text="<%$Resources:Resource, lblCompanyPostalCode %>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyPostalCode" runat="server" MaxLength="10" />
                </div>
                 <div class="lbl">
                    <asp:Label ID="Label1" Text="<%$Resources:Resource, lblCompanyUnitType %>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:DropDownList ID="ddlCumpanyUnitType" runat="server">                        
                        <asp:ListItem Text="<%$Resources:Resource, lblMetricUnit %>" Value="1" />
                        <asp:ListItem Text="<%$Resources:Resource, lblEnglishUnit %>" Value="2" />
                    </asp:DropDownList>
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
        <h3>
            <%= Resources.Resource.lblCompanyBillingAddress%>
        </h3>
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyBillToAddr1 %>" ID="lblCompanyBillToAddr1"
                        runat="server">
                    </asp:Label>
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyBillToAddr1" runat="server" MaxLength="35">
                    </asp:TextBox>
                </div>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyBillToAddr2 %>" ID="lblCompanyBillToAddr2"
                        runat="server">
                    </asp:Label>
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyBillToAddr2" runat="server" MaxLength="35">
                    </asp:TextBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyBillToCity %>" ID="lblCompanyBillToCity"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyBillToCity" runat="server" MaxLength="25" />
                </div>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyBillToCountry %>" ID="lblCompanyBillToCountry"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyBillToCountry" runat="server" MaxLength="25" style="display:none;" />
                    <asp:DropDownList ID="ddlCountryBillTo" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlCountryBillTo_SelectedIndexChanged">                    
                    </asp:DropDownList>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl"><asp:Label Text="<%$Resources:Resource, lblCompanyBillToState %>" ID="lblCompanyBillToState"
                        runat="server" />
                    
                </div>
                <div class="input">
                   <asp:TextBox ID="txtCompanyBillToState" runat="server" MaxLength="25" style="display:none;" />
                    <asp:DropDownList ID="ddlStateBillTo" runat="server">                    
                    </asp:DropDownList> 
                    <script type="text/javascript">
                        $("#<%=ddlStateBillTo.ClientID%>").change(function () {
                            $("#<%=txtCompanyBillToState.ClientID%>").val($(this).val());
                        });                       
                    </script>
                </div>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyBillToPostalCode %>" ID="lblCompanyBillToPostalCode"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyBillToPostalCode" runat="server" MaxLength="10" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyCourierCode %>" ID="lblCompanyCourierCode"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyCourierCode" runat="server" MaxLength="45" />
                </div>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyShpToTerms %>" ID="lblCompanyShpToTerms"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyShpToTerms" runat="server" TextMode="MultiLine" Rows="3"
                        MaxLength="255">
                    </asp:TextBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
        <h3>
            <%= Resources.Resource.lblCompanyContacts%>
        </h3>
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblErpCompanyPhone %>" ID="lblErpCompanyPhone"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyPhone" runat="server" MaxLength="15" />
                </div>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyFax %>" ID="lblCompanyFax" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyFax" runat="server" MaxLength="15" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyBasCur %>" ID="lblCompanyBasCur"
                        runat="server" />*
                </div>
                <div class="input">
                    <asp:DropDownList ID="dlCurrencyCode" runat="server">
                    </asp:DropDownList>
                    <asp:CustomValidator ID="custvalVendorCurrencyCode" runat="server" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:Resource, reqvalCompanyBasCur%>" ClientValidationFunction="funCheckCurrencyCode"
                        Display="None" ValidationGroup="cmpnySave"></asp:CustomValidator>
                </div>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyEmail %>" ID="lblCompanyEmail"
                        runat="server" />*
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyEmail" runat="server" MaxLength="65">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqvalCompanyEmail" ErrorMessage="<%$ Resources:Resource, reqvalCompanyEmail%>"
                        runat="server" ControlToValidate="txtCompanyEmail" Display="None" SetFocusOnError="true"
                        ValidationGroup="cmpnySave"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revalCompanyEmail" runat="server" ControlToValidate="txtCompanyEmail"
                        SetFocusOnError="true" Display="None" ErrorMessage="<%$ Resources:Resource, revalCompanyEmail%>"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="cmpnySave"></asp:RegularExpressionValidator>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyInternetFax %>" ID="lblCompanyInternetFax"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyInternetFax" runat="server" MaxLength="65" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
        <h3>
            <%= Resources.Resource.lblCompanyPurchaseDetails%></h3>
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyPOTerms %>" ID="lblCompanyPOTerms"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyPOTerms" runat="server" TextMode="MultiLine" Rows="3"
                        MaxLength="255">
                    </asp:TextBox>
                </div>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyPOName %>" ID="lblCompanyPOName"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyPOName" runat="server" MaxLength="50" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyPOPhone %>" ID="lblCompanyPOPhone"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyPOPhone" runat="server" MaxLength="15" />
                </div>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyPOEmail %>" ID="lblCompanyPOEmail"
                        runat="server" />*
                </div>
                <div class="input">
                    <asp:TextBox ID="txtCompanyPOEmail" runat="server" MaxLength="65">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqvalCompanyPOEmail" ErrorMessage="<%$ Resources:Resource, reqvalCompanyPOEmail%>"
                        runat="server" ControlToValidate="txtCompanyPOEmail" Display="None" SetFocusOnError="true"
                        ValidationGroup="cmpnySave"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revalCompanyPOEmail" runat="server" ControlToValidate="txtCompanyPOEmail"
                        SetFocusOnError="true" Display="None" ErrorMessage="<%$ Resources:Resource, revalCompanyPOEmail%>"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="cmpnySave"></asp:RegularExpressionValidator>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblGenQuotationRemarks %>" ID="lblGenQuotationRemarks"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtGenQuotationRemarks" TextMode="MultiLine" Rows="3" MaxLength="255"
                        runat="server">
                    </asp:TextBox>
                </div>
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblGenInvoiceRemarks %>" ID="lblGenInvoiceRemarks"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtGenInvoiceRemarks" TextMode="MultiLine" Rows="3" MaxLength="255"
                        runat="server">
                    </asp:TextBox>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                 <div class="lbl">                    
                    <asp:Label Text="<%$Resources:Resource, lblStatusToAllowInvoiced %>" ID="Label2"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:CheckBoxList ID="chkLstStatus" runat="server">                        
                    </asp:CheckBoxList>
                </div>
            </li>
            <li runat="server" id="trIsActive" visible="true">
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanySalesRepRestricted %>" ID="lblCompanySalesRepRestricted"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:RadioButtonList ID="rblIsSalesRep" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Text="<%$Resources:Resource, lblYes %>" Selected="True" />
                        <asp:ListItem Value="0" Text="<%$Resources:Resource, lblNo %>" />
                    </asp:RadioButtonList>
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li>
                 <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblRestrictedPhoneFormat %>" ID="Label3"
                        runat="server"> </asp:Label>
                </div>
                <div class="input">
                    <asp:CheckBox ID="chkRestrictedPhoneFormat" Text="" runat="server" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li id="trLogoImagePath" runat="server">
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblLogoImagePath %>" ID="lblLogoImagePath"
                        runat="server"> </asp:Label>
                </div>
                <div class="input">
                    <asp:FileUpload ID="fileLogoImagePath" runat="server" />
                </div>                
                <div class="clearBoth">
                </div>
            </li>
            <li id="trLogoImageUrl" runat="server" visible="false">
                <div class="lbl">
                    <asp:Label Text="<%$Resources:Resource, lblCompanyLogo %>" ID="lblCompanyLogo" runat="server"> </asp:Label>
                </div>
                <div class="input">
                    <a runat="server" id="aTag" target="_blank">
                        <img runat="server" id="imgLogo" /></a>
                    <asp:ImageButton ID="imgLogoImageUrl" CausesValidation="false" runat="server" ImageUrl="~/Images/delete_icon.png"
                        Height="16px" OnClick="imgLogoImageUrl_Click" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
        <div class="clear">
        </div>
        <div class="div-dialog-command">
            <asp:Button Text="<%$Resources:Resource, btnSave %>" ID="btnSave" runat="server"
                ValidationGroup="cmpnySave" OnClick="btnSave_Click" />
            <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
                OnClick="btnCancel_Click" />
        </div>
        <div class="clear">
        </div>
        <asp:HiddenField runat="server" ID="hdnCompanyLogo" />
        <asp:ValidationSummary ID="valsAdminCompany" runat="server" ShowMessageBox="true"
            ShowSummary="false" ValidationGroup="cmpnySave" />
    </div>
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        function funCheckCurrencyCode(source, args) {
            if (document.getElementById('<%=dlCurrencyCode.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
    </script>
</asp:Content>

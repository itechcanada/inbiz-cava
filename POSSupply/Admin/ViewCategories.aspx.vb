﻿Imports iTECH.InbizERP.BusinessLogic

Partial Class Admin_ViewCategories
    Inherits BasePage

    Private objCategories As New BLLCategories

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblMsg.Text = ""
        If Not Session("Msg") Is Nothing AndAlso Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If

        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("sdata")) Then
                CommonKeyword1.SearchData = Request.QueryString("sdata")
            End If
        End If
        clsCSV.ExportCSV(Page, "CustomerCategory", grdRegister, New Integer() {2, 3}, imgCsv)
        AddHandler CommonKeyword1.OnSearch, AddressOf SearchData
    End Sub

    Private Sub SearchData(ByVal sender As Object, ByVal e As EventArgs)
        Response.Redirect(String.Format("~/Admin/ViewCategories.aspx?sdata={0}", CommonKeyword1.SearchData))
    End Sub

    Protected Sub grdRegister_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdRegister.RowDeleting
        objCategories.CategoryID = CType(grdRegister.DataKeys(e.RowIndex).Values(0), Integer)
        objCategories.TextDescriptionID = CType(grdRegister.DataKeys(e.RowIndex).Values(1), Integer)
        If objCategories.Delete() Then
            Session("Msg") = "Category has been deleted successfully."
            Response.Redirect(Request.RawUrl)
        End If
    End Sub
End Class

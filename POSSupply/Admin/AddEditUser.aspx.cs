﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.Web;
using iTECH.WebControls;


public partial class Admin_AddEditUser : BasePage
{
    System.Data.DataTable objDT;
    System.Data.DataRow objDR;

    OrderTypeDetail _orderTypeDtl = new OrderTypeDetail();
    InbizUser _usr = new InbizUser();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Security Check                  
            if (this.UserID > 0 && this.UserID != CurrentUser.UserID && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
            {
                Response.Redirect("~/Errors/AccessDenied.aspx");
            }

            //Hide options from users who are not Administrator
            if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
            {
                pnlAdmin1.Visible = true;
            }
            else
            {
                pnlAdmin1.Visible = false;
            }


            lblTitle.Text = Resources.Resource.AddInternalUser;

            DropDownHelper.FillDropdown(ddlUserType, "US", "dlUTy", Globals.CurrentAppLanguageCode, null);
            DropDownHelper.FillDropdown(ddlStatus, "US", "dlUSt", Globals.CurrentAppLanguageCode, null);
            DropDownHelper.FillDropdown(ddlLanguage, "US", "dlLag", Globals.CurrentAppLanguageCode, null);
            DropDownHelper.FillOrderType(dlstOderType, Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.lblSelectOrderType));

            SysWarehouses wh = new SysWarehouses();
            wh.FillWharehouse(null, dlWarehouse, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));
            sdsModules.SelectCommand = new SecurityModules().GetSql(Globals.CurrentAppLanguageCode);


            if (this.UserID > 0)
            {
                txtLoginID.ReadOnly = true;
                FillOrderTypeGrid();
                FillFormData();
                //Password to edit in popup
                trPasswordPop.Visible = true;

                if (this.UserID == CurrentUser.UserID)
                {
                    lblTitle.Text = Resources.Resource.EditProfile;
                    mdChangePassword.Url = "changepassword.aspx";

                    //Add logout confirmation to save button
                    btnSave.Attributes.Add("onclick", "javascript:return " + "confirm('" + Resources.Resource.msgforEditUserInfo + " ')");
                }
                else
                {
                    lblTitle.Text = Resources.Resource.EditInternalUser;
                    mdChangePassword.Url = string.Format("changepassword.aspx?userid={0}", this.UserID.ToString());
                }
            }
            else
            {
                MakeTempOderType();
            }
            if (this.ActiveSectionIndex == UserSectionKey.Details)
            {
                pnlUserDetail.Visible = true;
                pnlRole.Visible = false;
                txtLoginID.Focus();
            }
            else if (this.ActiveSectionIndex == UserSectionKey.Role)
            {
                pnlUserDetail.Visible = false;
                pnlRole.Visible = true;
            }
            else
            {
                pnlUserDetail.Visible = true;
                pnlRole.Visible = false;
            }
        }
    }


    private UserSectionKey ActiveSectionIndex
    {
        get
        {
            int section = 0;
            if (int.TryParse(Request.QueryString["section"], out section))
            {
                if (Enum.IsDefined(typeof(UserSectionKey), section))
                {
                    return (UserSectionKey)Enum.ToObject(typeof(UserSectionKey), section);
                }
                else
                {
                    return UserSectionKey.Details;
                }
            }
            return UserSectionKey.Details;
        }
    }

    bool chkModuleIsChecked = false;
    protected void rptModules_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlInputCheckBox chkModule = (HtmlInputCheckBox)e.Item.FindControl("chkModule");
            DataList dlstRoles = (DataList)e.Item.FindControl("dlstRoles");
            int moduleID = BusinessUtility.GetInt(DataBinder.Eval(e.Item.DataItem, "ModuleID"));

            dlstRoles.DataSource = new SecurityRoles().GetRolesByModuleID(null, moduleID, Globals.CurrentAppLanguageCode);
            dlstRoles.DataBind();

            chkModule.Checked = chkModuleIsChecked;
            chkModuleIsChecked = false; //Rest
        }
    }

    protected void dlstRoles_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlInputCheckBox chkRole = (HtmlInputCheckBox)e.Item.FindControl("chkRole");
            HiddenField hdnRoleID = (HiddenField)e.Item.FindControl("hdnRoleID");
            if (this.UserID > 0)
            {
                int roleID = BusinessUtility.GetInt(hdnRoleID.Value);
                chkRole.Checked = ProcessUsers.HasUserRole(this.UserID, roleID);
                if (!chkModuleIsChecked)
                    chkModuleIsChecked = chkRole.Checked;
            }
        }
    }

    // Populate Order Type Grid
    public void FillOrderTypeGrid()
    {
        sqldsOrderType.SelectCommand = _orderTypeDtl.GetSqlOrderTypes(sqldsOrderType.SelectParameters, this.UserID, Globals.CurrentAppLanguageCode);
    }

    //Populate Controls
    public void FillFormData()
    {
        _usr.UserID = this.UserID;
        if (_usr.UserID <= 0)
        {
            trPassword.Visible = true;
            trConfPassword.Visible = true;


        }
        else
        {
            trPassword.Visible = false;
            trConfPassword.Visible = false;
            trIsActive.Visible = true;
            _usr.PopulateObject(this.UserID);
            //Get User Info

            txtLoginID.Text = _usr.UserLoginId;
            txtEmail.Text = _usr.UserEmail;
            txtSalutation.Text = _usr.UserSalutation;
            txtFName.Text = _usr.UserFirstName;
            txtLName.Text = _usr.UserLastName;
            txtDesignation.Text = _usr.UserDesignation;
            txtDepartment.Text = _usr.UserDepartment;
            txtPhone.Text = _usr.UserPhone;
            txtPhoneExt.Text = _usr.UserPhoneExt;
            txtFax.Text = _usr.UserFax;
            txtCellPhone.Text = _usr.UserCellPhone;
            txtRole.Text = _usr.UserRoleID.ToString();
            ddlUserType.SelectedValue = _usr.UserType;
            string[] strArr = null;
            strArr = _usr.UserModules.Split(',');
            ddlStatus.SelectedValue = _usr.UserStatus;
            rblstActive.SelectedValue = _usr.UserActive.ToString();
            rblstValidated.SelectedValue = _usr.UserValidated.ToString();
            ddlLanguage.SelectedValue = _usr.UserLang;
            if (!string.IsNullOrEmpty(_usr.UserDefaultWhs))
            {
                dlWarehouse.SelectedValue = _usr.UserDefaultWhs;
            }
            if (_usr.ReceiveNotificationViaEmail)
            {
                rbtnReceiveNotification.SelectedValue = "1";
            }
            else
            {
                rbtnReceiveNotification.SelectedValue = "0";
            }
            txtGridHeight.Text = BusinessUtility.GetString(_usr.UserGridHeight);
        }
    }
    //Populate Object
    private void SetData(InbizUser objUsr)
    {
        objUsr.UserLoginId = txtLoginID.Text;
        objUsr.UserEmail = txtEmail.Text;
        objUsr.UserPassword = txtPassword.Text;
        objUsr.UserSalutation = txtSalutation.Text;
        objUsr.UserFirstName = txtFName.Text;
        objUsr.UserLastName = txtLName.Text;
        objUsr.UserDesignation = txtDesignation.Text;
        objUsr.UserDepartment = txtDepartment.Text;
        objUsr.UserPhone = txtPhone.Text;
        objUsr.UserPhoneExt = txtPhoneExt.Text;
        objUsr.UserFax = txtFax.Text;
        objUsr.UserCellPhone = txtCellPhone.Text;
        objUsr.UserRoleID = Convert.ToInt32(txtRole.Text);
        objUsr.UserType = ddlUserType.SelectedValue;
        //objUsr.UserModules = string.Empty;
        objUsr.UserStatus = ddlStatus.SelectedValue;
        objUsr.UserLastUpdatedAt = DateTime.Now;
        objUsr.ReceiveNotificationViaEmail = rbtnReceiveNotification.SelectedValue == "1";
        if (rblstActive.SelectedValue == "1")
        {
            objUsr.UserActive = true;
        }
        else
        {
            objUsr.UserActive = false;
        }
        if (rblstValidated.SelectedValue == "1")
        {
            objUsr.UserValidated = true;
        }
        else
        {
            objUsr.UserValidated = false;
        }

        objUsr.UserLang = ddlLanguage.SelectedValue;
        objUsr.UserDefaultWhs = dlWarehouse.SelectedValue;
        objUsr.UserGridHeight = BusinessUtility.GetInt(txtGridHeight.Text);
    }


    protected void imgAdd_Click(object sender, System.EventArgs e)
    {
        if (this.UserID > 0)
        {
            int orderTypeid = BusinessUtility.GetInt(dlstOderType.SelectedValue);
            _orderTypeDtl.Ordercommission = BusinessUtility.GetDouble(txtCommission.Text);
            _orderTypeDtl.OrderTypeID = orderTypeid;
            _orderTypeDtl.UserID = this.UserID;
            if (_orderTypeDtl.CheckDuplicateOderType(orderTypeid, this.UserID) == true)
            {
                lblOTypeMsg.Text = "OderType already exists.";
                //  Exit Sub
            }
            else
            {
                _orderTypeDtl.OrderTypeDtlID = _orderTypeDtl.InsertOrdertype();
                FillOrderTypeGrid();
            }
        }
        else
        {
            if (AddToOderType())
            {
                lblOTypeMsg.Text = Resources.Resource.msgOrderTypeSaved;
                UpdateOrderTypeGrid();
            }
            else
            {
                // Exit Sub
            }
        }
        txtCommission.Text = "";
        txtCommission.Focus();
    }

    public void UpdateOrderTypeGrid()
    {
        grdOderTypeInsert.DataSource = objDT;
        grdOderTypeInsert.DataBind();
    }

    // Add Cart
    public bool AddToOderType()
    {
        _orderTypeDtl.Ordercommission = BusinessUtility.GetDouble(txtCommission.Text);
        _orderTypeDtl.OrderTypeID = BusinessUtility.GetInt(dlstOderType.SelectedValue);
        int OrderTypeID = BusinessUtility.GetInt(dlstOderType.SelectedValue);
        bool blnMatch = false;
        objDT = (DataTable)Session["SalesCart"];
        foreach (DataRow objDR in objDT.Rows)
        {
            if (BusinessUtility.GetInt(objDR["OrderTypeID"]) == OrderTypeID)
            {
                lblOTypeMsg.Text = "OderType already exists.";
                return false;
                //objDR("Commission") += CDbl(objOrderType.ordercommission)
                //blnMatch = True
                //Exit For
            }
        }

        if (!blnMatch)
        {
            objDR = objDT.NewRow();
            objDR["OrderTypeID"] = _orderTypeDtl.OrderTypeID;
            objDR["OrderTypeDesc"] = dlstOderType.SelectedItem.Text;
            objDR["Commission"] = _orderTypeDtl.Ordercommission;
            objDT.Rows.Add(objDR);
        }
        return true;
    }

    protected void grdOrderType_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        FillOrderTypeGrid();
    }
    protected void grdOrderType_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        if ((e.Row.RowType == DataControlRowType.DataRow))
        {
            ImageButton imgDelete = (ImageButton)e.Row.FindControl("imgDelete");
            imgDelete.Attributes.Add("onclick", "javascript:return " + "confirm('" + Resources.Resource.ConfirmDeleteOderType + " ')");
        }
    }
    protected void grdOrderType_RowDeleting(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
    {
        Int32 strOrderID = Convert.ToInt32(grdOrderType.DataKeys[e.RowIndex].Value.ToString());
        lblOTypeMsg.Text = Resources.Resource.msgOrderTypeDeleted;
        sqldsOrderType.DeleteCommand = _orderTypeDtl.DeleteOderTypeDetail(sqldsOrderType.DeleteParameters, strOrderID);
        //Delete OrderID
        FillOrderTypeGrid();
    }
    protected void grdOrderType_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        FillOrderTypeGrid();
    }
    // Make Oder type Data Table
    public void MakeTempOderType()
    {
        objDT = new System.Data.DataTable("OderType");
        objDT.Columns.Add("ID", typeof(int));
        objDT.Columns["ID"].AutoIncrement = true;
        objDT.Columns["ID"].AutoIncrementSeed = 1;
        objDT.Columns.Add("OrderTypeID", typeof(string));
        objDT.Columns.Add("OrderTypeDesc", typeof(string));
        objDT.Columns.Add("Commission", typeof(string));
        Session["SalesCart"] = objDT;
    }

    protected void grdOderTypeInsert_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        UpdateOrderTypeGrid();
    }

    protected void grdOderTypeInsert_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        if ((e.Row.RowType == DataControlRowType.DataRow))
        {
            ImageButton imgDelete = (ImageButton)e.Row.FindControl("imgDelete");
            imgDelete.Attributes.Add("onclick", "javascript:return " + "confirm('" + Resources.Resource.ConfirmDeleteOderType + " ')");
        }
    }

    protected void grdOderTypeInsert_RowDeleting(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
    {
        objDT = (DataTable)Session["SalesCart"];
        lblOTypeMsg.Text = Resources.Resource.msgOrderTypeDeleted;
        objDT.Rows[e.RowIndex].Delete();
        txtCommission.Text = "";
        UpdateOrderTypeGrid();
    }

    protected void grdOderTypeInsert_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        UpdateOrderTypeGrid();
    }

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        if (!Page.IsValid)
        {
            return;
        }
        //Dim objUser As New clsUser
        InbizUser objUser = new InbizUser();
        SetData(objUser);
        if (dlWarehouse.SelectedValue == "0")
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPleaseSelectWarehouse);
            return;
        }
        try
        {
            if (this.UserID > 0)
            {
                objUser.UserID = this.UserID;

                if (objUser.Update() == true)
                {
                    //UpdateUserModue(this.UserID);
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.UserUpdatedSuccessfully);
                    if (this.UserID == CurrentUser.UserID)
                    {
                        Response.Redirect("~/AdminLogin.aspx?login=" + txtLoginID.Text, false);
                    }
                    else
                    {
                        Response.Redirect("~/Admin/ViewInternalUser.aspx", false);
                    }
                }
            }
            else
            {
                objUser.Insert();
                if (objUser.UserID > 0)
                {
                    //int intI = 0;
                    //objDT = (DataTable)Session["SalesCart"];
                    //for (intI = 0; intI <= objDT.Rows.Count - 1; intI++)
                    //{
                    //    objDR = objDT.Rows[intI];
                    //    _orderTypeDtl.Ordercommission = BusinessUtility.GetDouble(objDR["Commission"]);
                    //    _orderTypeDtl.OrderTypeID = BusinessUtility.GetInt(objDR["OrderTypeID"]);
                    //    _orderTypeDtl.UserID = objUser.UserID;
                    //    _orderTypeDtl.InsertOrdertype();
                    //}
                    //UpdateUserModue(objUser.UserID);
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.UserAddedSuccessfully);
                    //Response.Redirect("~/Admin/ViewInternalUser.aspx", false);
                    Response.Redirect(string.Format("~/Admin/AddEditUserRole.aspx?UserID={0}&section=2", BusinessUtility.GetString(objUser.UserID)), false);
                }
            }
        }
        catch (Exception ex)
        {
            if (ex.Message == "USER_ALREADY_EXISTS")
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgLoginIDAlreadyExists);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
            }
        }
        finally
        {

        }
    }

    //private string GetOldModules() {
    //    List<string> oldModules = new List<string>();
    //    foreach (RepeaterItem item in rptModules.Items)
    //    {
    //        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem) {
    //            HtmlInputCheckBox chkModule = (HtmlInputCheckBox)item.FindControl("chkModule");
    //            if (chkModule.Checked) {
    //                DataList dlstRoles = (DataList)item.FindControl("dlstRoles");
    //                foreach (DataListItem role in dlstRoles.Items)
    //                {
    //                    if (role.ItemType == ListItemType.Item || role.ItemType == ListItemType.AlternatingItem)
    //                    {
    //                        HtmlInputCheckBox chkRole = (HtmlInputCheckBox)role.FindControl("chkRole");
    //                        HiddenField hdnOldKey = (HiddenField)role.FindControl("hdnOldKey");
    //                        if (chkRole.Checked && !string.IsNullOrEmpty(hdnOldKey.Value.Trim()) && !oldModules.Contains(hdnOldKey.Value.Trim())) {
    //                            oldModules.Add(hdnOldKey.Value.Trim());
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }

    //    return string.Join(",", oldModules.ToArray());
    //}

    private void UpdateUserModue(int userid)
    {
        InbizUser objUser = new InbizUser();

        /*List<SecurityRoleDescription> lstRoles = new List<SecurityRoleDescription>();
        foreach (RepeaterItem item in rptModules.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlInputCheckBox chkModule = (HtmlInputCheckBox)item.FindControl("chkModule");
                int moduleID = BusinessUtility.GetInt(chkModule.Value);
                if (chkModule.Checked)
                {
                    DataList dlstRoles = (DataList)item.FindControl("dlstRoles");
                    foreach (DataListItem role in dlstRoles.Items)
                    {
                        if (role.ItemType == ListItemType.Item || role.ItemType == ListItemType.AlternatingItem)
                        {
                            HtmlInputCheckBox chkRole = (HtmlInputCheckBox)role.FindControl("chkRole");
                            HiddenField hdnRoleID = (HiddenField)role.FindControl("hdnRoleID");
                            HiddenField hdnAttachModules = (HiddenField)role.FindControl("hdnAttachModules");
                            int roleID = BusinessUtility.GetInt(hdnRoleID.Value);
                            if (chkRole.Checked)
                            {
                                SecurityRoleDescription rd = new SecurityRoleDescription();
                                rd.RoleID = roleID;
                                rd.AttachModules = hdnAttachModules.Value;
                                lstRoles.Add(rd);
                            }
                        }
                    }
                }
            }
        }*/
        //if (pnlAdmin1.Visible)
        if (pnlRole.Visible || pnlUserDetail.Visible)
        {
            string[] sRoles = hdnRoles.Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            List<int> lRoles = new List<int>();
            foreach (var item in sRoles)
            {
                int i = 0;
                if (int.TryParse(item, out i) && i > 0 && !lRoles.Contains(i))
                {
                    lRoles.Add(i);
                }
            }
            objUser.AssignRoles(null, lRoles, userid);
        }
    }

    protected void btnReset_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("ViewInternalUser.aspx");

        if (!string.IsNullOrEmpty(this.UserID.ToString()))
        {
            FillFormData();
            return;
        }
        txtLoginID.Text = "";
        txtEmail.Text = "";
        txtPassword.Text = "";
        txtConfirmPwd.Text = "";
        txtFName.Text = "";
        txtLName.Text = "";
        txtDesignation.Text = "";
        txtDepartment.Text = "";
        txtPhone.Text = "";
        txtPhoneExt.Text = "";
        txtFax.Text = "";
        txtCellPhone.Text = "";
        txtRole.Text = "0";
    }


    private int UserID
    {
        get
        {
            int uid = 0;
            int.TryParse(Request.QueryString["UserID"], out uid);
            return uid;
        }
    }
}

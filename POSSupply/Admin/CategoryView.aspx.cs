﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_CategoriesView : BasePage
{
    Category _categ = new Category();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Security Check TO DO

        if (!IsPagePostBack(grdCategories)) { 

        }
    }

    protected void grdCategories_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 4)
        {
            e.CellHtml = string.Format(@"<a class=""pop_edit""  href=""CategoryEdit.aspx?catid={0}&jscallback={1}"" >{2}</a>", e.RowValues[0], "reloadGrid", Resources.Resource.lblEdit);
        }
        if (e.ColumnIndex == 5)
        {
            string delUrl = string.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_CATEGORY, e.RowKey, "reloadGrid");
            e.CellHtml = string.Format(@"<a class=""pop_delete"" href=""{0}"">{1}</a>", delUrl, Resources.Resource.delete);
        }
    }

    protected void grdCategories_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains<string>("_history")) {
            sdsCategories.SelectCommand = _categ.GetSql(sdsCategories.SelectParameters, Request.QueryString[txtSearch.ClientID], Globals.CurrentAppLanguageCode);
        }
        else
        {
            sdsCategories.SelectCommand = _categ.GetSql(sdsCategories.SelectParameters, txtSearch.Text, Globals.CurrentAppLanguageCode);
        }
    }
}
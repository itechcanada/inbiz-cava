﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.Web;
using iTECH.WebControls;
public partial class Admin_AddEditUserRole : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Security Check                  
            if (this.UserID > 0 && this.UserID != CurrentUser.UserID && !CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
            {
                Response.Redirect("~/Errors/AccessDenied.aspx");
            }
            if (this.UserID > 0)
            {
                if (this.UserID == CurrentUser.UserID)
                {
                    //Add logout confirmation to save button
                    btnSave.Attributes.Add("onclick", "javascript:return " + "confirm('" + Resources.Resource.msgforEditUserInfo + " ')");
                }
            }
            sdsModules.SelectCommand = new SecurityModules().GetSql(Globals.CurrentAppLanguageCode);
        }
    }

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        if (!Page.IsValid)
        {
            return;
        }
        //Dim objUser As New clsUser
        InbizUser objUser = new InbizUser();
        //SetData(objUser);
        //if (dlWarehouse.SelectedValue == "0")
        //{
        //    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgPleaseSelectWarehouse);
        //    return;
        //}
        try
        {
            if (this.UserID > 0)
            {
                //objUser.UserID = this.UserID;
                objUser.PopulateObject(this.UserID);

                //if (objUser.Update() == true)
                //{
                    UpdateUserModue(this.UserID);
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.UserUpdatedSuccessfully);
                    if (this.UserID == CurrentUser.UserID)
                    {
                        Response.Redirect("~/AdminLogin.aspx?login=" + objUser.UserLoginId, false);
                    }
                    else
                    {
                        Response.Redirect("~/Admin/ViewInternalUser.aspx", false);
                    }
                //}
            }
            else
            {
                if (objUser.UserID > 0)
                {
                    UpdateUserModue(objUser.UserID);
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.UserAddedSuccessfully);
                    Response.Redirect("~/Admin/ViewInternalUser.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {
            if (ex.Message == "USER_ALREADY_EXISTS")
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgLoginIDAlreadyExists);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
            }
        }
        finally
        {

        }
    }

    //private string GetOldModules() {
    //    List<string> oldModules = new List<string>();
    //    foreach (RepeaterItem item in rptModules.Items)
    //    {
    //        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem) {
    //            HtmlInputCheckBox chkModule = (HtmlInputCheckBox)item.FindControl("chkModule");
    //            if (chkModule.Checked) {
    //                DataList dlstRoles = (DataList)item.FindControl("dlstRoles");
    //                foreach (DataListItem role in dlstRoles.Items)
    //                {
    //                    if (role.ItemType == ListItemType.Item || role.ItemType == ListItemType.AlternatingItem)
    //                    {
    //                        HtmlInputCheckBox chkRole = (HtmlInputCheckBox)role.FindControl("chkRole");
    //                        HiddenField hdnOldKey = (HiddenField)role.FindControl("hdnOldKey");
    //                        if (chkRole.Checked && !string.IsNullOrEmpty(hdnOldKey.Value.Trim()) && !oldModules.Contains(hdnOldKey.Value.Trim())) {
    //                            oldModules.Add(hdnOldKey.Value.Trim());
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }

    //    return string.Join(",", oldModules.ToArray());
    //}

    private void UpdateUserModue(int userid)
    {
        InbizUser objUser = new InbizUser();

        /*List<SecurityRoleDescription> lstRoles = new List<SecurityRoleDescription>();
        foreach (RepeaterItem item in rptModules.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlInputCheckBox chkModule = (HtmlInputCheckBox)item.FindControl("chkModule");
                int moduleID = BusinessUtility.GetInt(chkModule.Value);
                if (chkModule.Checked)
                {
                    DataList dlstRoles = (DataList)item.FindControl("dlstRoles");
                    foreach (DataListItem role in dlstRoles.Items)
                    {
                        if (role.ItemType == ListItemType.Item || role.ItemType == ListItemType.AlternatingItem)
                        {
                            HtmlInputCheckBox chkRole = (HtmlInputCheckBox)role.FindControl("chkRole");
                            HiddenField hdnRoleID = (HiddenField)role.FindControl("hdnRoleID");
                            HiddenField hdnAttachModules = (HiddenField)role.FindControl("hdnAttachModules");
                            int roleID = BusinessUtility.GetInt(hdnRoleID.Value);
                            if (chkRole.Checked)
                            {
                                SecurityRoleDescription rd = new SecurityRoleDescription();
                                rd.RoleID = roleID;
                                rd.AttachModules = hdnAttachModules.Value;
                                lstRoles.Add(rd);
                            }
                        }
                    }
                }
            }
        }*/
        //if (pnlAdmin1.Visible)
        if (pnlRole.Visible )
        {
            string[] sRoles = hdnRoles.Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            List<int> lRoles = new List<int>();
            foreach (var item in sRoles)
            {
                int i = 0;
                if (int.TryParse(item, out i) && i > 0 && !lRoles.Contains(i))
                {
                    lRoles.Add(i);
                }
            }
            objUser.AssignRoles(null, lRoles, userid);
        }
    }

    protected void btnReset_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("ViewInternalUser.aspx");
    }

    bool chkModuleIsChecked = false;
    protected void rptModules_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlInputCheckBox chkModule = (HtmlInputCheckBox)e.Item.FindControl("chkModule");
            DataList dlstRoles = (DataList)e.Item.FindControl("dlstRoles");
            int moduleID = BusinessUtility.GetInt(DataBinder.Eval(e.Item.DataItem, "ModuleID"));

            dlstRoles.DataSource = new SecurityRoles().GetRolesByModuleID(null, moduleID, Globals.CurrentAppLanguageCode);
            dlstRoles.DataBind();

            chkModule.Checked = chkModuleIsChecked;
            chkModuleIsChecked = false; //Rest
        }
    }

    protected void dlstRoles_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlInputCheckBox chkRole = (HtmlInputCheckBox)e.Item.FindControl("chkRole");
            HiddenField hdnRoleID = (HiddenField)e.Item.FindControl("hdnRoleID");
            if (this.UserID > 0)
            {
                int roleID = BusinessUtility.GetInt(hdnRoleID.Value);
                chkRole.Checked = ProcessUsers.HasUserRole(this.UserID, roleID);
                if (!chkModuleIsChecked)
                    chkModuleIsChecked = chkRole.Checked;
            }
        }
    }

    public int UserID
    {
        get
        {
            int uid = 0;
            int.TryParse(Request.QueryString["UserID"], out uid);
            return uid;
        }
    }
}
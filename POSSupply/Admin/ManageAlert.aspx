﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ManageAlert.aspx.cs" Inherits="Admin_ManageAlert" ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
    <script type="text/javascript">
        var _alertsPreLoaded = [];
        var _usersList = [];
    </script>    
    <style type="text/css">
          /*.ui-menu .ui-menu-item { white-space:nowrap; padding:0 10px 0 0; }*/
          div.autocomplete_holder input[type=text]{ width:50px; margin:0 0 2px 0; padding:0 0 3px; position:relative; top:0; float:left; border:none; }
          div.autocomplete_holder span { display:block; width:auto; margin:0 3px 3px 0; padding:3px 20px 4px 8px; position:relative; float:left; text-indent:0; background-color:#eee; border:1px solid #333; -moz-border-radius:7px; -webkit-border-radius:7px; border-radius:7px; color:#333; font:normal 11px Verdana, Sans-serif; }
          div.autocomplete_holder span a { position:absolute; right:8px; top:2px; color:#666; font:bold 12px Verdana, Sans-serif; text-decoration:none; }
          div.autocomplete_holder span a:hover { color:#ff0000 !important; }
          
          div.autocomplete_holder span.add { background-color:#d7ffd7;  }
          div.autocomplete_holder span.add a { font-weight:bold;color:Green; }
          div.autocomplete_holder span.add a:hover { color:#ff0000 !important; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
     <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" alt="" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.Administration %></h1>       
    </div>
    <div class="clear"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" Runat="Server">
<h3>    
    <asp:Label ID="Label2" Text="<%$Resources:Resource,lblManageAlerts%>" runat="server" />
</h3>
    <%--<asp:TextBox ID="TextBox1" TextMode="MultiLine" Width="180px" Height="50px" runat="server" />--%>
    <div id="grid_wrapper" style="width: 100%;">
        <%--<asp:RadioButtonList ID="rblLang" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                <asp:ListItem Value="en" Text="<%$Resources:Resource, lblEnglish%>" Selected="True" />
                <asp:ListItem Value="fr" Text="<%$Resources:Resource, lblFrench%>" />
            </asp:RadioButtonList>--%>
        <trirand:JQGrid runat="server" ID="grdAlerts" DataSourceID="sdsAlerts"
            Height="300px" AutoWidth="true" oncellbinding="grdAlerts_CellBinding" 
            ondatarequesting="grdAlerts_DataRequesting" >
            <Columns>
                <trirand:JQGridColumn DataField="ID" HeaderText= "Alert Type" PrimaryKey="True" Visible="false" />   
                <trirand:JQGridColumn DataField="AlertDesc" HeaderText="<%$ Resources:Resource, lblAlertType %>" />   
                <trirand:JQGridColumn DataField="ID" HeaderText="<%$ Resources:Resource, grdAlertUser %>" Editable="false" CssClass="autocompleteTD" />         
                <trirand:JQGridColumn DataField="ID" HeaderText="<%$ Resources:Resource, cmdCssSave %>" Editable="false" Width="30" TextAlign="Center" />     
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <ClientSideEvents LoadComplete="loadComplete" BeforeRowSelect="beforeRowSelect" />
        </trirand:JQGrid>
        <asp:SqlDataSource ID="sdsAlerts" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
    </div>    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" Runat="Server">
    <asp:Panel runat="server" ID="SearchPanel">
        <div class="searchBar">
            <div class="header">
                <div class="title">
                    <asp:Literal ID="ltSearchForm" Text="<%$Resources:Resource,lblSearchForm %>" runat="server" />
                </div>
                <div class="icon">
                    <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
            </div>
            <h4>
                <asp:Label ID="lblSearchKeyword" CssClass="filter-key" AssociatedControlID="txtSearch"
                    runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
            <div class="inner">
                <asp:TextBox runat="server" Width="180px" ID="txtSearch" CssClass="filter-key"></asp:TextBox>
            </div>
            <div class="footer">
                <div class="submit">
                    <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                </div>
                <br />
            </div>
        </div>
        <br />
    </asp:Panel>
        <div class="clear">
        </div>
    
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">        
        $grid = $("#<%=grdAlerts.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=grdAlerts.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }        
        function loadComplete(data) {
            jqGridResize();           
        }

        //prevent jq grid from row selection
        function beforeRowSelect(rowid, e) {
            return false;
        }

        function saveFieldData(source, key) {
            //alert($(source).val());.
            $tr = $(source).parent().parent();
            $sub = $(".msg_subject", $tr);
            $body = $(".msg_body", $tr);

            var dataToPost = {};
            dataToPost.editRowKey = key;
            dataToPost.msgSubject = $sub.val();
            dataToPost.msgBody = $body.val();

            for (var k in dataToPost) {
                $grid.setPostDataItem(k, dataToPost[k]);
            }

            $grid.trigger("reloadGrid");

            for (var k in dataToPost) {
                $grid.removePostDataItem(k);
            }
        }
    </script>
    <script type="text/javascript">
        function removeAssignedUser(source, userID, alertName) {
            if (confirm("Do you want to unassign user?")) {
                var datatoPost = {};
                datatoPost.isAjaxCall = 1;
                datatoPost.callBack = "delete";
                datatoPost.alertName = alertName;
                datatoPost.userID = userID;

                $.post("ManageAlert.aspx", datatoPost, function (data) {
                    if (data == "ok") {
                        $(source).parent().remove();
                    }
                });
            }            
        }

        function assignUsersPopup(alertName) {                        
            var url = 'ManageAlertAssignUser.aspx';
            var queryData = {};
            queryData.aName = alertName;
            var t = "<%=Resources.Resource.lblAssignAlertToUser%>";

            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 620,
                height: 400,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');

            return false;
        }

        function editPopup(key) {
            var url = 'ManageAlertEdit.aspx';
            var queryData = {};
            queryData.aName = key;            
            var t = "<%=Resources.Resource.lblEditAlert%>";            

            var $dialog = jQuery.FrameDialog.create({ url: url + "?" + $.param(queryData),
                title: t,
                loadingClass: "loading-image",
                modal: true,
                width: 620,
                height: 400,
                autoOpen: false,
                closeOnEscape: true
            });
            $dialog.dialog('open');

            return false;
        }   
    </script>
</asp:Content>


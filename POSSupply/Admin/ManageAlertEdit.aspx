﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="ManageAlertEdit.aspx.cs" Inherits="Admin_ManageAlertEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
   <div style="padding: 5px; height: 280px; overflow: auto;">
        <ul class="form">
            <li>
                <div class="lbl">
                    Alert Type
                </div>
                <div class="input">
                    <asp:Label ID="lblAlertType" Text="" runat="server" />
                </div>
                <div class="clearBoth"></div>
            </li>
            <li>
                <div class="lbl">
                    <%= Resources.Resource.lblSubject%> (EN)
                </div>
                <div class="input" style="width:320px">
                    <asp:TextBox ID="txtSubjectEn" runat="server" Width="300px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" ControlToValidate="txtSubjectEn"
                        runat="server" SetFocusOnError="true" Display="Dynamic" />
                </div>
                <div class="clearBoth"></div>
            </li>
            <li>
                <div class="lbl">
                    <%= Resources.Resource.lblSubject%> (FR)
                </div>
                <div class="input" style="width:320px">
                    <asp:TextBox ID="txtSubjectFr" runat="server" Width="300px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="*" ControlToValidate="txtSubjectFr"
                        runat="server" SetFocusOnError="true" Display="Dynamic" />
                </div>
                <div class="clearBoth"></div>
            </li>
             <li>
                <div class="lbl">
                    Body (EN)
                </div>
                 <div class="input" style="width:320px">
                    <asp:TextBox ID="txtBodyEn" runat="server" TextMode="MultiLine" Rows="5" Columns="35" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="*" ControlToValidate="txtBodyEn"
                        runat="server" SetFocusOnError="true" Display="Dynamic" />
                </div>
                <div class="clearBoth"></div>
            </li>
            <li>
                <div class="lbl">
                    Body (FR)
                </div>
                 <div class="input" style="width:320px">
                    <asp:TextBox ID="txtBodyFr" runat="server" TextMode="MultiLine" Rows="5" Columns="35" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="*" ControlToValidate="txtBodyFr"
                        runat="server" SetFocusOnError="true" Display="Dynamic" />
                </div>
                <div class="clearBoth"></div>
            </li>
        </ul>        
    </div>
    <div class="div_command">        
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" 
            onclick="btnSave_Click" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, btnCancel%>" runat="server" CausesValidation="false" OnClientClick="jQuery.FrameDialog.closeDialog();" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.Web;
using iCtrl = iTECH.WebControls;

public partial class Admin_mdDashboardSetting : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Populate whscodes
            DashboardSettings.FillAllWhsCode(lbxWhs, "", null);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        List<string> lstWhs = new List<string>();
        foreach (ListItem item in lbxWhs.Items)
        {
            if (item.Selected)
            {
                lstWhs.Add(item.Value);
            }
        }

        DashboardSettings.SetWhsMeta(lstWhs);
        Globals.RegisterReloadParentScript(this);
    }
}
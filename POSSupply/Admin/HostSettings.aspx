﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="HostSettings.aspx.cs" Inherits="Admin_HostSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">     
    <div id="grid_wrapper" style="width: 100%;" onkeypress="return disableEnterKey(event)">
        <trirand:JQGrid runat="server" ID="grdSettings" DataSourceID="sdsSettings" Height="400"
            AutoWidth="True" onrowediting="grdSettings_RowEditing">
            <Columns>
                <trirand:JQGridColumn DataField="HostSettingID" HeaderText="ID" PrimaryKey="True" Width="15" TextAlign="Center" />
                <trirand:JQGridColumn DataField="SettingName" HeaderText="Key"
                    Editable="false" Width="120" />
                <trirand:JQGridColumn DataField="SettingValue" HeaderText="Value"
                    Editable="true"  Width="120" />                
            </Columns>
            <PagerSettings PageSize="100" PageSizeOptions="[100,150,200]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <EditInlineCellSettings Enabled="True" />
            <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="gridLoadComplete" />
        </trirand:JQGrid>        
        <asp:SqlDataSource ID="sdsSettings" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand="SELECT * FROM app_host_settings"></asp:SqlDataSource>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
    <asp:Panel runat="server" ID="SearchPanel">
        <div class="searchBar">
            <div class="header">
                <div class="title">
                    Search form</div>
                <div class="icon">
                    <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
            </div>          
            <h4>
                <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
            <div class="inner">
                <asp:TextBox runat="server" Width="180px" ID="txtSearch" CssClass="filter-key"></asp:TextBox>
            </div>
            <div class="footer">
                <div class="submit">
                    <input id="btnSearch" type="button" value="Search" />
                </div>
                <br />
            </div>
        </div>
        <br />
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdSettings.ClientID %>";
        var searchPanelID = "<%=SearchPanel.ClientID %>";


        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function gridLoadComplete(data) {
            $(window).trigger("resize");
        }        
    </script>
</asp:Content>


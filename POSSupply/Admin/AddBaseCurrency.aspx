﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AddBaseCurrency.aspx.cs" Inherits="Admin_AddBaseCurrency" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="contentBottom">
        <br />
        <h3>
            <%= Resources.Resource.EPBaseCurrency%></h3>
        <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="text" style="width: 50%">
                    <asp:Label ID="lblCompanyBasCur" runat="server"  Text="<%$Resources:Resource, lblCompanyBasCur %>"/>*
                </td>
                <td class="input">
                    <asp:DropDownList ID="dlCurrencyCode" runat="server" Width="135px">
                    </asp:DropDownList>
                    <asp:CustomValidator ID="custvalVendorCurrencyCode" runat="server" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:Resource, reqvalCompanyBasCur%>" ClientValidationFunction="funCheckCurrencyCode"
                        Display="None">
                    </asp:CustomValidator>
                </td>
            </tr>
        </table>
        <h2>
        </h2>
        <br />
        <div class="div-dialog-command">
            <asp:Button ID="btnSave" runat="server" Text="<%$Resources:Resource, btnSave %>" ValidationGroup="SaveSkill" OnClick="btnSave_Click" />
            <asp:Button ID="btnCancel" runat="server" Text="<%$Resources:Resource, btnCancel %>" CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
            <div class="clear">
            </div>
        </div>
        <asp:ValidationSummary ID="sumvalWhs" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript" language="javascript">
        function funCheckCurrencyCode(source, args) {
            if (document.getElementById('<%=dlCurrencyCode.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
    </script>
</asp:Content>



﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="AddEditWarehouse.aspx.cs" Inherits="Admin_AddEditWarehouse" %>

<asp:Content ID="cntAddEditWarehouse" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div id="contentBottom" style="padding: 5px; height: 410px; overflow: auto;" onkeypress="return disableEnterKey(event)">
            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
            <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
            <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td class="text" style="width: 35%">
                        <asp:Label Text="<%$Resources:Resource, lblWarehouseCode %>" ID="lblWarehouseCode"
                            runat="server" />*
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtWarehouseCode" runat="server" MaxLength="3" Width="175px" />
                        <asp:RequiredFieldValidator ID="reqvalWarehouseCode" ErrorMessage="<%$ Resources:Resource, reqvalWarehouseCode%>"
                            runat="server" ControlToValidate="txtWarehouseCode" SetFocusOnError="true" Display="None"
                            ValidationGroup="WareHouseAdd"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblWarehouseDescription %>" ID="lblWarehouseDescription"
                            runat="server" />*
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtWarehouseDescription" runat="server" MaxLength="75" Width="175px" />
                        <asp:RequiredFieldValidator ID="reqvalWarehouseDescription" ErrorMessage="<%$ Resources:Resource, reqvalWarehouseDescription%>"
                            runat="server" ControlToValidate="txtWarehouseDescription" SetFocusOnError="true"
                            Display="None" ValidationGroup="WareHouseAdd"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblWarehouseAddressLine1 %>" ID="lblWarehouseAddressLine1"
                            runat="server">
                        </asp:Label>
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtWarehouseAddressLine1" runat="server" MaxLength="35" Width="175px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblWarehouseAddressLine2 %>" ID="lblWarehouseAddressLine2"
                            runat="server">
                        </asp:Label>
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtWarehouseAddressLine2" runat="server" MaxLength="35" Width="175px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblWarehouseCity %>" ID="lblWarehouseCity"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtWarehouseCity" runat="server" MaxLength="25" Width="175px" />
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblWarehouseState %>" ID="lblWarehouseState"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtWarehouseState" runat="server" MaxLength="25" Width="175px" />
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblWarehouseCountry %>" ID="lblWarehouseCountry"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtWarehouseCountry" runat="server" MaxLength="25" Width="175px" />
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblWarehousePostalCode %>" ID="lblWarehousePostalCode"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtWarehousePostalCode" runat="server" MaxLength="10" Width="175px" />
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblWarehousePrinterID %>" ID="lblWarehousePrinterID"
                            runat="server" />
                    </td>
                    <td class="input" width="72%">
                        <asp:TextBox ID="txtWarehousePrinterID" runat="server" MaxLength="50" Width="175px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblWarehouseEmailID %>" ID="lblWarehouseEmailID"
                            runat="server" />*
                    </td>
                    <td class="input" width="72%">
                        <asp:TextBox ID="txtWarehouseEmailID" runat="server" MaxLength="65" Width="175px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalWarehouseEmailID" ErrorMessage="<%$ Resources:Resource, reqvalWarehouseEmailID%>"
                            runat="server" ControlToValidate="txtWarehouseEmailID" Display="None" SetFocusOnError="true"
                            ValidationGroup="WareHouseAdd"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revalWarehouseEmailID" runat="server" ControlToValidate="txtWarehouseEmailID"
                            Display="None" ErrorMessage="<%$ Resources:Resource, revalWarehouseEmailID%>"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="WareHouseAdd"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblWarehouseFax %>" ID="lblWarehouseFax"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtWarehouseFax" runat="server" MaxLength="15" Width="175px" />
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblWarehousePhone %>" ID="lblWarehousePhone"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox ID="txtWarehousePhone" runat="server" MaxLength="15" Width="175px" />
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblSelectTaxGroup %>" ID="lblSelectTaxGroup"
                            runat="server" />*
                    </td>
                    <td class="input">
                        <asp:DropDownList ID="dlTaxGroup" runat="server" Width="180px">
                        </asp:DropDownList>
                        <asp:CustomValidator ID="custvalTaxGroup" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, custvalSelTaxGroup%>"
                            ClientValidationFunction="funCheckSelTaxGroup" Display="None" ValidationGroup="WareHouseAdd"></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblWarehouseTaxCode %>" ID="lblWarehouseTaxCode"
                            runat="server" />*
                    </td>
                    <td class="input">
                        <asp:DropDownList ID="dlCompanyName" runat="server" Width="180px">
                        </asp:DropDownList>
                        <asp:CustomValidator ID="custvalComapany" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, custvalWarehouseCompanyName%>"
                            ClientValidationFunction="funCheckSelectComID" Display="None" ValidationGroup="WareHouseAdd"></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblShippingWarehouse %>" ID="Label1" runat="server" />
                    </td>
                    <td class="input">
                        <asp:DropDownList ID="ddlShippingWhs" runat="server" Width="180px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblShowInProductQuery %>" ID="lblShowInProductQuery"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:CheckBox ID="chkShowInProductQuery" runat="server" Checked="true" />
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblActiveBin %>" ID="lblActiveBin" runat="server" />
                    </td>
                    <td class="input">
                        <asp:DropDownList ID="ddlActiveBin" runat="server" Width="180px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblReservedBin %>" ID="lblReservedBin" runat="server" />
                    </td>
                    <td class="input">
                        <asp:DropDownList ID="ddlReservedBin" runat="server" Width="180px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblDamagedBin %>" ID="lblDamagedBin" runat="server" />
                    </td>
                    <td class="input">
                        <asp:DropDownList ID="ddlDamagedBin" runat="server" Width="180px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <h3>
                            <asp:Literal ID="Literal1" Text="<%$Resources:Resource,lblLoyaltyPoints%>" runat="server" />
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="text">
                        <asp:Label ID="lblAdd" runat="server" Text="<%$Resources:Resource, lblAdd %>" />
                        <asp:TextBox ID="txtAddPoints" runat="server" Width="45px" Style="text-align: right" />
                        <asp:Label ID="lblPointsFor" runat="server" Text="points for $" />
                        <asp:TextBox ID="txtSpent" runat="server" Width="45px" Style="text-align: right" />
                        <asp:Label ID="lblspent" runat="server" Text="<%$Resources:Resource, lblspent %>" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="text">
                        <asp:Label ID="lblRedeem" runat="server" Text="<%$Resources:Resource, lblRedeem %>" />
                        $
                        <asp:TextBox ID="txtRedeem" runat="server" Width="45px" Style="text-align: right" />
                        <asp:Label ID="lblForEvery" runat="server" Text="<%$Resources:Resource, lblForEvery %>" />
                        <asp:TextBox ID="txtRedeemPoints" runat="server" Width="45px" Style="text-align: right" />
                        <asp:Label ID="lblPoints" runat="server" Text="<%$Resources:Resource, lblthepoint %>" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="text">
                        <asp:Label ID="lblMin" runat="server" Text="Min" />
                        <asp:TextBox ID="txtMinPoints" runat="server" Width="35px" Style="text-align: right" />
                        <asp:Label ID="lblPointsNeededFor" runat="server" Text="<%$Resources:Resource, lblPointRedumption %>" />
                    </td>
                </tr>
                <tr runat="server" id="trIsActive" visible="false">
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblWarehouseActive %>" ID="lblWarehouseActive"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:RadioButton Text="<%$Resources:Resource, lblYes %>" ID="rdbYes" runat="server"
                            Checked="True" ValidationGroup="warehouse" GroupName="Whs"/>
                        <asp:RadioButton Text="<%$Resources:Resource, lblNo %>" ID="rdbNo" runat="server" GroupName="Whs"
                            ValidationGroup="warehouse" />
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:LinkButton ID="lnbWhsSalesGoals" runat="server" Text="<%$Resources:Resource, lblLocationSalesGoals %>" style="text-decoration:none;font-weight:bold;" Width="150px" OnClick="lnbWhsSalesGoals_OnClick"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
        <div class="div-dialog-command">
            <asp:Button Text="<%$Resources:Resource, btnSave %>" ID="btnSave" runat="server"
                ValidationGroup="WareHouseAdd" OnClick="btnSave_Click" />
            <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
                CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
        </div>
        <asp:ValidationSummary ID="valsAdminWarehouse" runat="server" ShowMessageBox="true"
            ShowSummary="false" ValidationGroup="WareHouseAdd" />
    </div>
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
    <script type="text/javascript">
        function funCheckSelTaxGroup(source, args) {
            if (document.getElementById('<%=dlTaxGroup.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }

        function funCheckSelectComID(source, args) {
            if (document.getElementById('<%=dlCompanyName.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }

        $("#<%=txtAddPoints.ClientID%>").change(function () {
            var val = parseInt($(this).val());
            if (isNaN(val)) {
                $(this).val("0");
            }
        });
        $("#<%=txtSpent.ClientID%>").change(function () {
            var val = parseInt($(this).val());
            if (isNaN(val)) {
                $(this).val("0");
            }
        });
        $("#<%=txtRedeem.ClientID%>").change(function () {
            var val = parseInt($(this).val());
            if (isNaN(val)) {
                $(this).val("0");
            }
        });
        $("#<%=txtRedeemPoints.ClientID%>").change(function () {
            var val = parseInt($(this).val());
            if (isNaN(val)) {
                $(this).val("0");
            }
        });
        $("#<%=txtMinPoints.ClientID%>").change(function () {
            var val = parseInt($(this).val());
            if (isNaN(val)) {
                $(this).val("0");
            }
        });
    </script>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;

public partial class Admin_CustomFieldView : BasePage
{
    CustomFields _cf = new CustomFields();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdCustomFields))
        {
            rblModule.Items.Add(new ListItem(CustomFieldApplicableSection.CustomerAddEdit.ToString(), ((int)CustomFieldApplicableSection.CustomerAddEdit).ToString()));
            rblModule.Items.Add(new ListItem(CustomFieldApplicableSection.InvoiceAddEdit.ToString(), ((int)CustomFieldApplicableSection.InvoiceAddEdit).ToString()));
            rblModule.Items.Add(new ListItem(CustomFieldApplicableSection.SalesOrderAddEdit.ToString(), ((int)CustomFieldApplicableSection.SalesOrderAddEdit).ToString()));

            rblModule.Items[0].Selected = true;
        }
    }
    protected void grdCustomFields_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        int mid = 0;
        if (!int.TryParse(Request.QueryString["mid"], out mid))
        {
            mid = 1;
        }

        if (Request.QueryString.AllKeys.Contains("_history"))
        {
            sdsCustomFields.SelectCommand = _cf.GetSql(sdsCustomFields.SelectParameters, Request.QueryString[txtSearch.ClientID], mid, Globals.CurrentAppLanguageCode);
        }
        else
        {
            sdsCustomFields.SelectCommand = _cf.GetSql(sdsCustomFields.SelectParameters, txtSearch.Text, mid, Globals.CurrentAppLanguageCode);
        }
    }
    protected void grdCustomFields_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 2)
        {
            switch (BusinessUtility.GetInt(e.CellHtml))
            {
                case (int)CustomFieldType.Currency:
                    e.CellHtml = "Currency";
                    break;
                case (int)CustomFieldType.Double:
                    e.CellHtml = "Double";
                    break;
                case (int)CustomFieldType.Integer:
                    e.CellHtml = "Integer";
                    break;
                case (int)CustomFieldType.StringMultiline:
                    e.CellHtml = "String (Multi Line)";
                    break;
                case (int)CustomFieldType.StringSingleLine:
                    e.CellHtml = "String (Single Line)";
                    break;
                case (int)CustomFieldType.BooleanCheckBox:
                    e.CellHtml = "Boolean (CheckBox)";
                    break;
                default:
                    e.CellHtml = "--";
                    break;
            }
        }
        else if (e.ColumnIndex == 3)
        {
            e.CellHtml = string.Format("<a class='pop_edit' href='CustomFieldEdit.aspx?cid={0}' >{1}</a>", e.RowKey, Resources.Resource.lblEdit);
        }
        else if(e.ColumnIndex == 4)
        {
            e.CellHtml = string.Format("<a onclick='deleteField({0});' href='javascript:;'>{1}</a>", e.RowKey, Resources.Resource.delete);
        }
    }
    protected void grdCustomFields_RowDeleting(object sender, Trirand.Web.UI.WebControls.JQGridRowDeleteEventArgs e)
    {
        e.Cancel = true;
        _cf.Delete(BusinessUtility.GetInt(e.RowKey));
    }
}
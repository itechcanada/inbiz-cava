﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_CustomFieldEdit : BasePage
{
    CustomFields _cf = new CustomFields();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillForm();
        }
    }

    private void FillForm()
    {
        ddlModule.Items.Add(new ListItem(CustomFieldApplicableSection.CustomerAddEdit.ToString(), ((int)CustomFieldApplicableSection.CustomerAddEdit).ToString()));
        ddlModule.Items.Add(new ListItem(CustomFieldApplicableSection.InvoiceAddEdit.ToString(), ((int)CustomFieldApplicableSection.InvoiceAddEdit).ToString()));
        ddlModule.Items.Add(new ListItem(CustomFieldApplicableSection.SalesOrderAddEdit.ToString(), ((int)CustomFieldApplicableSection.SalesOrderAddEdit).ToString()));
        
        if (this.ID > 0)
        {
            _cf.PopulateObject(this.ID);
            txtDescEn.Text = _cf.DescEn;
            txtDescFr.Text = _cf.DescFr;
            txtDescSp.Text = _cf.DescSp;
            ddlFieldType.SelectedValue = _cf.FieldType.ToString();
            ddlModule.SelectedValue = _cf.Module.ToString();
        }
    }

    private int ID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["cid"], out id);
            return id;
        }
    }

    private void SetData()
    {
        _cf.IsRequired = chkIsRequired.Checked;        
        _cf.DescEn = txtDescEn.Text;
        _cf.DescFr = txtDescFr.Text;
        _cf.DescSp = txtDescSp.Text;
        _cf.FieldType = BusinessUtility.GetInt(ddlFieldType.SelectedValue);
        _cf.Module = BusinessUtility.GetInt(ddlModule.SelectedValue);
        _cf.ID = this.ID;
        _cf.IsActive = true;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            this.SetData();
            if (this.ID > 0)
            {
                _cf.Update();
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgRecordUpdatedSuccessfully);
                Globals.RegisterCloseDialogScript(this, "parent.reloadGrid();", true);
            }
            else
            {
                _cf.Insert();
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgRecordAddedSuccess);
                Globals.RegisterCloseDialogScript(this, "parent.reloadGrid();", true);
            }
        }
    }
}
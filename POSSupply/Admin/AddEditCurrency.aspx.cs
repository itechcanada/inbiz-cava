﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_AddEditCurrency : BasePage
{   
    //On Page Load
    SysCurrencies _cur = new SysCurrencies();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security check for authorization
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }        
       
        if (!IsPostBack)
        {
            FillRecord();
            txtCurrencyCode.Focus();
        }
    }
    public void FillRecord()
    {
        if (this.CurrencyCode.Length > 0) {
            _cur.CurrencyCode = this.CurrencyCode;
            trIsActive.Visible = true;
            _cur.PopulateObject(this.CurrencyCode);
            txtCurrencyCode.Text = _cur.CurrencyCode;
            txtCurrencyRelativePrice.Text = string.Format("{0:F}", _cur.CurrencyRelativePrice);
            rdbYes.Checked = _cur.CurrencyActive;
        }       
    }

    //Populate Object
    private void SetData()
    {
        _cur.CurrencyCode = txtCurrencyCode.Text;
        _cur.CurrencyRelativePrice = BusinessUtility.GetDouble(txtCurrencyRelativePrice.Text);
        _cur.CurrencyActive = rdbYes.Checked;
    }
    

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        if (Page.IsValid)
        {
            SetData();

            if (this.CurrencyCode.Length > 0)
            {
                _cur.CurrencyCode = this.CurrencyCode;
                _cur.Update();
                //Update Currency 
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.CurrencyUpdatedSuccessfully);
                if (!string.IsNullOrEmpty(this.JsCallBackFunction)) {
                    Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                }
                else
                {
                    Globals.RegisterCloseDialogScript(Page);
                }                
            }
            else
            {
                //Insert Currency
                _cur.Insert();
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.CurrencyAddedSuccessfully);
                if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                {
                    Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                }
                else
                {
                    Globals.RegisterCloseDialogScript(Page);
                }   
            }
        }
    }    

    private string CurrencyCode {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["CurrencyID"]);
        }
    }

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }
}

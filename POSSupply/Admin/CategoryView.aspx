﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="CategoryView.aspx.cs" Inherits="Admin_CategoriesView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <%= Resources.Resource.lblImport%>:
            <%= Resources.Resource.lblCustomerUser%></b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <asp:Button ID="btnAdd" runat="server" Text="Add Category" />
        <iCtrl:IframeDialog ID="mdAdd" TriggerControlID="btnAdd" Width="400" Height="240"
            Url="CategoryEdit.aspx?jscallback=reloadGrid" Title="Category" runat="server">
        </iCtrl:IframeDialog>
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif" CssClass="csv_export" />
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="grid_wrapper" style="width: 100%;">
        <trirand:JQGrid runat="server" ID="grdCategories" DataSourceID="sdsCategories" Height="300px"
            AutoWidth="True" OnCellBinding="grdCategories_CellBinding" OnDataRequesting="grdCategories_DataRequesting">
            <Columns>
                <trirand:JQGridColumn DataField="CategoryID" HeaderText=""
                    Editable="false" PrimaryKey="True" Visible="false" />
                <trirand:JQGridColumn DataField="CategoryGroup" Editable="false" />
                <trirand:JQGridColumn DataField="CategoryName" Editable="false" />
                <trirand:JQGridColumn DataField="ParentCategory" Editable="false" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="CategoryID"
                    Sortable="false" TextAlign="Center" Width="50" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="CategoryID"
                    Sortable="false" TextAlign="Center" Width="50" />
            </Columns>
            <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn=""></SortSettings>
            <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
            <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" />
        </trirand:JQGrid>
        <asp:SqlDataSource ID="sdsCategories" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
        <iCtrl:IframeDialog ID="mdEdit" Width="400" Height="240" Title="Edit Category"
            Dragable="true" TriggerSelectorClass="pop_edit" TriggerSelectorMode="Class"
            UrlSelector="TriggerControlHrefAttribute" runat="server">
        </iCtrl:IframeDialog>
        <iCtrl:IframeDialog ID="mdDelete" Width="600" Height="120" Title="Delete" Dragable="true"
            TriggerSelectorClass="pop_delete" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
            runat="server">
        </iCtrl:IframeDialog>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" Runat="Server">
    <div>
        <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        Search form</div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label CssClass="filter-key" ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch" ></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="Search" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%= Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdCategories.ClientID %>";
        var searchPanelID = "<%=SearchPanel.ClientID %>";


        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }
    </script>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true" CodeFile="ViewExchangeRates.aspx.cs" Inherits="Admin_ViewExchangeRates" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <%= Resources.Resource.lblView%>:
            <%= Resources.Resource.lblExchangeRate%></b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <asp:Button ID="btnCreateCurrency" runat="server" Text="Add Currency" />
        <iCtrl:IframeDialog ID="mdCreateCurrency" TriggerControlID="btnCreateCurrency" Width="550"
            Height="250" Url="AddEditCurrency.aspx?jscallback=reloadGrid" Title="Add/Edit Currency" runat="server">
        </iCtrl:IframeDialog>
    </div>
    <div style="float: right;">        
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif" CssClass="csv_export" />
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        Search form</div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch" CssClass="filter-key-txt"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="Search" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntViewExchangeRates" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="jgdvCurrency" DataSourceID="sqldsCurrency" Height="300px"
                    AutoWidth="True" OnCellBinding="jgdvCurrency_CellBinding" OnDataRequesting="jgdvCurrency_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="CurrencyCode" HeaderText="" PrimaryKey="True" Visible="false" />
                        <trirand:JQGridColumn DataField="CurrencyCode" HeaderText="<%$ Resources:Resource, grdCurrencyCode %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="CompanyBasCur" HeaderText="<%$ Resources:Resource, grdCompanyBasCur %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="CurrencyRelativePrice" HeaderText="<%$ Resources:Resource, grdCurrencyRelativePrice %>"
                            Editable="false" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="CurrencyCode"
                            Sortable="false" TextAlign="Center" Width="50" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="CurrencyCode"
                            Sortable="false" TextAlign="Center" Width="50" />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" />
                </trirand:JQGrid>
                <iCtrl:IframeDialog ID="mdEditCurreny" Height="250" Width="490" Title="Add/Edit Currency"
                    Dragable="true" TriggerSelectorClass="edit-Currency" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
                    runat="server">
                </iCtrl:IframeDialog>
                <iCtrl:IframeDialog ID="mdDeleteCur" Width="400" Height="120" Title="Delete" Dragable="true"
                    TriggerSelectorClass="pop_delete" TriggerSelectorMode="Class" UrlSelector="TriggerControlHrefAttribute"
                    runat="server">
                </iCtrl:IframeDialog>
                <asp:SqlDataSource ID="sqldsCurrency" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient"></asp:SqlDataSource>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="cntScript" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        //Variables
        var gridID = "<%=jgdvCurrency.ClientID %>";
        var searchPanelID = "<%=SearchPanel.ClientID %>";


        //Function To Resize the grid
        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }

        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        function reloadGrid(event, ui) {
            $('#' + gridID).trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }
    </script>
</asp:Content>



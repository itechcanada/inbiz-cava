﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="CustomFieldEdit.aspx.cs" Inherits="Admin_CustomFieldEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <style type="text/css">
        .ul.form li div.lbl{width:90px !important;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
<div style="padding: 5px; height: 150px; overflow: auto;">
    <ul class="form">
        <li>
            <div class="lbl">
                 <asp:Label ID="Label4" Text="Module" runat="server" />
            </div>
            <div class="input">
                <asp:DropDownList ID="ddlModule" runat="server">                                 
                </asp:DropDownList>            
            </div>
            <div class="clearBoth">
            </div>
        </li>    
        <li>
            <div class="lbl">
                <asp:Label ID="Label7" Text="Desc(En)" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtDescEn" runat="server" />
                <asp:RequiredFieldValidator ID="rfvAddressLine1" ErrorMessage="<%$ Resources:Resource, reqvalRegCode%>"
                    runat="server" ControlToValidate="txtDescEn" Text="*" SetFocusOnError="True"> </asp:RequiredFieldValidator>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="Label1" Text="Desc(Fr)" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtDescFr" runat="server" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="<%$ Resources:Resource, reqvalRegCode%>"
                    runat="server" ControlToValidate="txtDescFr" Text="*" SetFocusOnError="True"> </asp:RequiredFieldValidator>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label ID="Label2" Text="Desc(Sp)" runat="server" />
            </div>
            <div class="input">
                <asp:TextBox ID="txtDescSp" runat="server" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="<%$ Resources:Resource, reqvalRegCode%>"
                    runat="server" ControlToValidate="txtDescSp" Text="*" SetFocusOnError="True"> </asp:RequiredFieldValidator>
            </div>
            <div class="clearBoth">
            </div>
        </li>
         <li>
            <div class="lbl">
                 <asp:Label ID="Label3" Text="Field Type" runat="server" />
            </div>
            <div class="input">
                <asp:DropDownList ID="ddlFieldType" runat="server">
                    <asp:ListItem Text="Integer" Value="1" />
                    <asp:ListItem Text="Double" Value="2" />
                    <asp:ListItem Text="Currency" Value="3" />
                    <asp:ListItem Text="String (Single Line)" Selected="True" Value="4" />
                    <asp:ListItem Text="String (MultiLine)" Value="5" />
                    <asp:ListItem Text="Boolean (CheckBox)" Value="6" />
                </asp:DropDownList>            
            </div>
            <div class="clearBoth">
            </div>
        </li>  
        <li>
            <div class="lbl">
                 <asp:Label ID="Label5" Text="Is Required" runat="server" />
            </div>
            <div class="input">
                <asp:CheckBox ID="chkIsRequired" Text="" runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>              
    </ul>    
</div>
<div class="div_command">
    <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" 
        onclick="btnSave_Click"  />
    <asp:Button ID="btnCancel" Text="<%$Resources:Resource, btnCancel%>" runat="server" CausesValidation="false" OnClientClick="jQuery.FrameDialog.closeDialog();" />
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using System.Configuration;

using iTECH.Library.DataAccess.MySql;

partial class Admin_AddEditRegister : BasePage
{
    SysWarehouses _whs = new SysWarehouses();
    SysTaxCodeDesc _taxCode = new SysTaxCodeDesc();
    SysRegister _reg = new SysRegister();

    protected void Page_Load(object sender, System.EventArgs e)
    {
        //Security
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        if (!Page.IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                SysWarehouses wh = new SysWarehouses();
                SysTaxCodeDesc tg = new SysTaxCodeDesc();
                dbHelp.OpenDatabaseConnection();
                tg.FillListControl(dbHelp, dlTaxCode, new ListItem("--" + Resources.Resource.Select + "--", "0"));
                wh.FillWharehouse(dbHelp, dlWarehouses, "", CurrentUser.DefaultCompanyID, new ListItem("---" + Resources.Resource.Select + "---", "0"));
                if (!string.IsNullOrEmpty(this.RegCode))
                {
                    FillRecord(dbHelp);
                }
                txtRegCode.Focus();
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    //Populate Controls
    public void FillRecord(DbHelper dbHelp)
    {
        if (this.RegCode.Length > 0)
        {
            _reg.SysRegCode = this.RegCode;
            trIsActive.Visible = true;

            _reg.PopulateObject(dbHelp, this.RegCode); // Populate object information by regCode

            txtRegCode.Text = _reg.SysRegCode;
            txtRegCode.ReadOnly = true;
            txtRegDescription.Text = _reg.SysRegDesc;
            txtRegAddressLine.Text = _reg.SysRegAddress;
            txtMerchantID.Text = _reg.SysRegMerchantId;
            txtRegCity.Text = _reg.SysRegCity;
            txtRegState.Text = _reg.SysRegState;
            txtRegPostalCode.Text = _reg.SysRegPostalCode;
            txtRegEmailID.Text = _reg.SysRegEmailID;
            txtRegFax.Text = _reg.SysRegFax;
            txtRegPhone.Text = _reg.SysRegPhone;
            rdbYes.Checked = _reg.SysRegActive;
            txtRegMessage.Text = _reg.SysRegMessage;
            txtLogFile.Text = _reg.SysRegLogFilePath;
            txtRegHost.Text = _reg.SysRegHost;
            txtRegHostPort.Text = _reg.SysRegHostPort;
            txtRegTerminal.Text = _reg.SysRegTerminalID;
            dlTaxCode.SelectedValue = BusinessUtility.GetString(_reg.SysRegTaxCode);
            dlWarehouses.SelectedValue = _reg.SysRegWhsCode;
            chkPrintMerchantCopy.Checked = _reg.SysRegPrintMerchantCopy;
            txtDefaultCustomerID.Text = BusinessUtility.GetString(_reg.SysDefaultCustomerID);
            txtManageCode.Text = BusinessUtility.GetString(_reg.SysManagerCode);
        }
    }
    //Populate Object from control values
    private void SetData()
    {
        _reg.SysRegCode = txtRegCode.Text;
        _reg.SysRegDesc = txtRegDescription.Text;
        _reg.SysRegAddress = txtRegAddressLine.Text;
        _reg.SysRegCity = txtRegCity.Text;
        _reg.SysRegState = txtRegState.Text;
        _reg.SysRegPostalCode = txtRegPostalCode.Text;
        _reg.SysRegEmailID = txtRegEmailID.Text;
        _reg.SysRegFax = txtRegFax.Text;
        _reg.SysRegPhone = txtRegPhone.Text;
        _reg.SysRegActive = rdbYes.Checked;
        _reg.SysRegTaxCode = BusinessUtility.GetInt(dlTaxCode.SelectedValue);
        _reg.SysRegWhsCode = dlWarehouses.SelectedValue;
        _reg.SysRegMerchantId = txtMerchantID.Text;
        _reg.SysRegHost = txtRegHost.Text;
        _reg.SysRegHostPort = txtRegHostPort.Text;
        _reg.SysRegTerminalID = txtRegTerminal.Text;
        _reg.SysRegLogFilePath = txtLogFile.Text;
        _reg.SysRegMessage = txtRegMessage.Text;
        _reg.SysRegPrintMerchantCopy = chkPrintMerchantCopy.Checked;
        _reg.SysDefaultCustomerID = BusinessUtility.GetInt(txtDefaultCustomerID.Text);
        _reg.SysManagerCode = BusinessUtility.GetString(txtManageCode.Text);
    }

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                DbHelper dbHelp = new DbHelper(true);
                Partners _cust = new Partners();
                _cust = new Partners();
                _cust.PopulateObject(dbHelp, BusinessUtility.GetInt(txtDefaultCustomerID.Text));
                if (BusinessUtility.GetString(_cust.PartnerLongName) == "")
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblmsgInvalidCustomer);
                    return;
                }

                SetData();
                if (!string.IsNullOrEmpty(this.RegCode))
                {
                    _reg.SysRegCode = this.RegCode;
                    _reg.Update();
                    //Update Register                    
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgRegUpdatedSuccessfully);
                    if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                    {
                        Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                    }
                    else
                    {
                        Globals.RegisterCloseDialogScript(Page);
                    }
                }
                else
                {
                    //Insert Register                 
                    OperationResult res = _reg.Insert();
                    if (res == OperationResult.Success)
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.RegAddedSuccessfully);
                        if (!string.IsNullOrEmpty(this.JsCallBackFunction))
                        {
                            Globals.RegisterCloseDialogScript(Page, string.Format("parent.{0}();", this.JsCallBackFunction), true);
                        }
                        else
                        {
                            Globals.RegisterCloseDialogScript(Page);
                        }
                    }
                    else if (res == OperationResult.AlreadyExists)
                    {
                        lblError.Text = Resources.Resource.RegCodeAlreadyExists;
                        txtRegCode.Focus();
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
                        txtRegCode.Focus();
                    }
                }
            }
            catch
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
                txtRegCode.Focus();
            }
        }
    }

    private string RegCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["RID"]);
        }
    }

    public string JsCallBackFunction
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.QueryString["jscallback"]))
            {
                return Request.QueryString["jscallback"];
            }
            return "";
        }
    }
}


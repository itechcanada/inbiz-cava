﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.Web;
using iCtrl = iTECH.WebControls;

public partial class Admin_changepassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.UserID <= 0)
        {
            Globals.RegisterCloseDialogScript(this);
            return;
        }

        if (!IsPostBack) {
            trCurrentPassword.Visible = this.IsProfilePassword;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (IsValid) {
            iTECH.InbizERP.BusinessLogic.InbizUser usr = new iTECH.InbizERP.BusinessLogic.InbizUser();

            if (this.IsProfilePassword) {
                if (usr.ChangePassword(this.UserID, txtCurrentPassword.Text, txtPassword.Text)) {
                    iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Success, Resources.Resource.msgPassworHasBeenChanged);
                    Globals.RegisterCloseDialogScript(this);
                }
                else
                {
                    lblError.Text = "Provided current password not matching with existing password!";
                }
            }
            else
            {
                usr.ChangePassword(this.UserID, txtPassword.Text);
                iCtrl.MessageState.SetGlobalMessage(iCtrl.MessageType.Success, Resources.Resource.msgPassworHasBeenChanged);
                Globals.RegisterCloseDialogScript(this);
            }
        }
    }

    private int UserID {
        get {
            int userid = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["userid"])) {
                userid = BusinessUtility.GetInt(Request.QueryString["userid"]);
            }
            if (userid == 0 && Session["UserID"] != null) {
                return BusinessUtility.GetInt(Session["UserID"].ToString());
            }
            return userid;
        }
    }

    private bool IsProfilePassword {
        get {
            int userid = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["userid"]))
            {
                userid = BusinessUtility.GetInt(Request.QueryString["userid"]);
            }

            return userid == 0 && Session["UserID"] != null;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.WebControls;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;

public partial class Admin_ViewVendor : BasePage
{   
    Vendor _vendor = new Vendor();    
    //On Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.PROCURMENT_MANAGER)) {
            Response.Redirect("~/Errors/AccessDenined.aspx");
        }

        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            btnAddVender.Visible = false;
        }

        if (!IsPagePostBack(jgdvVendor)) {
            lblTitle.Text = Resources.Resource.ViewVendor;
            DropDownHelper.FillDropdown(ddlvendorStatus, "VN", "dlSea", Globals.CurrentAppLanguageCode, null);
        }
        
        txtSearch.Focus();
    }
   

    protected void jgdvVendor_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 2)
        {
            if (string.IsNullOrEmpty(BusinessUtility.GetString(e.RowValues[3])))
            {
                e.CellHtml = "N/A";
            }
        }
        if (e.ColumnIndex == 3)
        {
            if (string.IsNullOrEmpty(BusinessUtility.GetString(e.RowValues[4])))
            {
                e.CellHtml = "N/A";
            }
        }
        if (e.ColumnIndex == 4)
        {
            if (string.IsNullOrEmpty(BusinessUtility.GetString(e.RowValues[2])))
            {
                e.CellHtml = "N/A";
            }
        }
        if (e.ColumnIndex == 5)
        {
            string strurl = "AddEditVendor.aspx?vendorID=" + e.RowValues[0];
            e.CellHtml = string.Format("<a href=\"{0}\">" + Resources.Resource.grdEdit + "</a>", strurl);
        }
        if (e.ColumnIndex == 6)
        {
            string delUrl = string.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_VENDOR, e.RowKey, "reloadGrid");
            e.CellHtml = string.Format(@"<a class=""pop_delete"" href=""{0}"">" + Resources.Resource.delete + "</a>", delUrl);
        }
    }

    protected void jgdvVendor_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains<string>("_history"))
        {
            sqldsVendor.SelectCommand = _vendor.GetSQL(sqldsVendor.SelectParameters, Request.QueryString[ddlvendorStatus.SelectedValue], Request.QueryString[txtSearch.ClientID]);
        }
        else
        {
            sqldsVendor.SelectCommand = _vendor.GetSQL(sqldsVendor.SelectParameters, Request.QueryString[ddlvendorStatus.SelectedValue], Request.QueryString[txtSearch.ClientID]);
        }
    }

    protected void btnAddVender_Click(object sender, System.EventArgs e)
    {        
        Response.Redirect("AddEditVendor.aspx");     
    }
}
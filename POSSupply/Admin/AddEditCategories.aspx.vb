﻿Imports iTECH.InbizERP.BusinessLogic

Partial Class Admin_AddEditCategories
    Inherits BasePage

    Private objCategories As New BLLCategories

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Session("Msg") Is Nothing AndAlso Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If

        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("cid")) Then
                FillForm()
            End If
        End If

        AddHandler CommonKeyword1.OnSearch, AddressOf SearchData
    End Sub

    Protected Sub cmdSave_ServerClick(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSave.ServerClick
        If IsValid Then
            FillCategoryObject()
            If (objCategories.CategoryID > 0) Then
                If objCategories.Update() Then
                    Session("Msg") = "Category has been successfully updated."
                    If Request.QueryString.AllKeys.Contains("returnUrl") Then
                        Response.Redirect(Server.UrlDecode(Request.QueryString("returnUrl")))
                    Else
                        Response.Redirect("~/Admin/ViewCategories.aspx")
                    End If
                Else
                    lblMsg.Text = "Error occured while updating category."
                End If
            Else
                If objCategories.Insert() Then
                    Session("Msg") = "Category has been successfully added."
                    If Request.QueryString.AllKeys.Contains("returnUrl") Then
                        Response.Redirect(Server.UrlDecode(Request.QueryString("returnUrl")))
                    Else
                        Response.Redirect("~/Admin/ViewCategories.aspx")
                    End If
                Else
                    lblMsg.Text = "Error occured while adding new category."
                End If
            End If
        End If
    End Sub

    Protected Sub cmdReset_ServerClick(ByVal sender As Object, ByVal e As EventArgs) Handles cmdReset.ServerClick
        If Request.QueryString.AllKeys.Contains("returnUrl") Then
            Response.Redirect(Server.UrlDecode(Request.QueryString("returnUrl")))
        Else
            Response.Redirect("~/Admin/ViewCategories.aspx")
        End If
    End Sub

    Private Sub SearchData(ByVal sender As Object, ByVal e As EventArgs)
        Response.Redirect(String.Format("~/Admin/ViewCategories.aspx?sdata={0}", CommonKeyword1.SearchData))
    End Sub

    Private Sub FillCategoryObject()
        If Not String.IsNullOrEmpty(Request.QueryString("cid")) Then
            Dim catID As Integer
            Integer.TryParse(Request.QueryString("cid"), catID)
            objCategories.CategoryID = catID

            Dim catTextDecID As Integer
            Integer.TryParse(hdnTextDescriptionID.Value, catTextDecID)

            objCategories.TextDescriptionID = catTextDecID
        End If

        objCategories.CategoryName.Add(ApplicationLanguage.English, txtCategoryNameEn.Text.Trim)
        objCategories.CategoryName.Add(ApplicationLanguage.French, IIf(String.IsNullOrEmpty(txtCategoryNameFr.Text.Trim), txtCategoryNameEn.Text.Trim, txtCategoryNameFr.Text.Trim))

        objCategories.IsActive = chkIsActive.Checked
    End Sub

    Private Sub FillForm()
        Dim catID As Integer
        Integer.TryParse(Request.QueryString("cid"), catID)

        objCategories.FillCurrentObject(catID)

        If (objCategories.CategoryID > 0) Then
            txtCategoryNameEn.Text = objCategories.CategoryName(ApplicationLanguage.English)
            txtCategoryNameFr.Text = IIf(objCategories.CategoryName.ContainsKey(ApplicationLanguage.French), objCategories.CategoryName(ApplicationLanguage.French), objCategories.CategoryName(ApplicationLanguage.English))
            chkIsActive.Checked = objCategories.IsActive
            hdnTextDescriptionID.Value = objCategories.TextDescriptionID.ToString()
        Else
            Session("Msg") = "No record found at provided key"
            Response.Redirect("~/Admin/ViewCategories.aspx")
        End If       
    End Sub

End Class

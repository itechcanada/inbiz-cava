﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/fullWidth.master" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="Admin_default" %>

<%@ Register src="DashboardComponents/SalesPerMonth.ascx" tagname="SalesPerMonth" tagprefix="uc1" %>

<%@ Register src="DashboardComponents/TopCustomers.ascx" tagname="TopCustomers" tagprefix="uc2" %>

<%@ Register src="DashboardComponents/TopProducts.ascx" tagname="TopProducts" tagprefix="uc3" %>

<%@ Register src="DashboardComponents/AccountReceivable.ascx" tagname="AccountReceivable" tagprefix="uc4" %>
<%@ Register src="DashboardComponents/EventCalendar.ascx" tagname="EventCalendar" tagprefix="uc5" %>
<%@ Register src="DashboardComponents/RecentActivities.ascx" tagname="RecentActivities" tagprefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">  
    <%=UIHelper.GetScriptTags("blockui", 1)%>      
    <link href="../lib/scripts/collapsible-panel/collapsible.css" rel="stylesheet" type="text/css" />
    <script src="../lib/scripts/wijmo/external/raphael-min.js" type="text/javascript"></script>
    <script src="../lib/scripts/wijmo/external/globalize.min.js" type="text/javascript"></script>
    <script src="../lib/scripts/wijmo/jquery.wijmo.raphael.js" type="text/javascript"></script>
    <script src="../lib/scripts/wijmo/jquery.wijmo.wijchartcore.js" type="text/javascript"></script>
    <script src="../lib/scripts/wijmo/jquery.wijmo.wijbarchart.js" type="text/javascript"></script>
    <script src="../lib/scripts/wijmo/jquery.wijmo.wijpiechart.js" type="text/javascript"></script>    
    <script src="../lib/scripts/collapsible-panel/jquery.collapsiblepanel.js" type="text/javascript"></script>
    <style type="text/css">
        .dash_column{float:left; width:33.33%;}   
        .date_column{font-weight:bold; color:#1A56CC; font-size:14px;}     
        .event_desc{line-height:22px; font-size:12px; padding:5px 0px;} 
        .event_desc a{text-decoration:underline;}   
        .ui-jqgrid tr.jqgrow td {white-space: normal !important;}    
    </style>
    <script type="text/javascript">
        function blockContentArea(elementToBlock, loadingMessage) {
            $(elementToBlock).block({
                message: '<div>' + loadingMessage + '</div>',
                css: { border: '3px solid #a00' }
            });
        }
        function unblockContentArea(elementToBlock) {
            $(elementToBlock).unblock();
        }

        var _pageSessionID = '<%=Session.SessionID%>';
    </script>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphMaster" runat="Server">
    <asp:Panel ID="pnlDashboard" runat="server">
        <div style="text-align:right; padding-right:5px;">
            <asp:HyperLink ID="hlDashSettings" CssClass="dashSettings inbiz_icon icon_setting_24x24" NavigateUrl="~/Admin/mdDashboardSetting.aspx" runat="server" />
        </div>
        <div class="">
            <div class="dash_column" style="display:none;">
                <div class="collapsiblepanel color-gray">
                    <div class="collapsiblepanel-head">
                        <h3>
                            <asp:Label ID="lbl" Text="<%$Resources:Resource, lblCalendarEvents %>" runat="server" /> 
                        </h3>
                        <%--<a class="inbiz_icon icon_fullscreen_32x32" href="#" style="float:right;"></a>
                        <div style="clear:both;"></div>--%>
                    </div>
                    <div class="collapsiblepanel-content">
                        <uc5:EventCalendar ID="EventCalendar1" runat="server" />
                    </div>
                </div>
            </div>
            <div class="dash_column">
                <div class="collapsiblepanel color-gray">
                    <div class="collapsiblepanel-head">
                        <h3>
                            
                            <asp:Label ID="Label1" Text="<%$Resources:Resource, lblSalesPerMonth %>" runat="server" /> 
                        </h3>
                         <a class="inbiz_icon icon_fullscreen_32x32" href="javascript:;" style="float:right;" onclick="fullScreen('sales');"></a>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="collapsiblepanel-content">
                        <div id="divSalesMonth">
                            <uc1:SalesPerMonth ID="SalesPerMonth1" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="dash_column">
                <div class="collapsiblepanel color-gray">
                    <div class="collapsiblepanel-head">
                        <h3>                            
                            <asp:Label ID="Label2" Text="<%$Resources:Resource, lblTopCustomers %>" runat="server" />
                        </h3>
                          <a class="inbiz_icon icon_fullscreen_32x32" href="javascript:;" style="float:right;" onclick="fullScreen('topcust');"></a>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="collapsiblepanel-content">
                        <div id="divTopCust">
                            <uc2:TopCustomers ID="TopCustomers1" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
                        <div class="dash_column">
                <div class="collapsiblepanel color-gray">
                    <div class="collapsiblepanel-head">
                        <h3>                            
                            <asp:Label ID="Label3" Text="<%$Resources:Resource, lblRecentActivity %>" runat="server" />
                        </h3>
                        <%--<a class="inbiz_icon icon_fullscreen_32x32" href="#" style="float:right;"></a>
                        <div style="clear:both;"></div>--%>
                    </div>
                    <div class="collapsiblepanel-content">
                        <uc6:RecentActivities ID="RecentActivities1" runat="server" />
                    </div>
                </div>
            </div>
            <div style="clear: both;">
            </div>
        </div>
        <div>

            <div class="dash_column">
                <div class="collapsiblepanel color-gray">
                    <div class="collapsiblepanel-head">
                        <h3>                            
                            <asp:Label ID="Label4" Text="<%$Resources:Resource, lblTopProducts %>" runat="server" />
                        </h3>
                        <a class="inbiz_icon icon_fullscreen_32x32" href="javascript:;" style="float:right;" onclick="fullScreen('topprd');"></a>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="collapsiblepanel-content">
                        <div id="divTopPrd">
                            <uc3:TopProducts ID="TopProducts1" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="dash_column">
                <div class="collapsiblepanel color-gray">
                    <div class="collapsiblepanel-head">
                        <h3>                                 
                            <asp:Label ID="Label5" Text="<%$Resources:Resource, lblAccounts %>" runat="server" />
                        </h3>
                         <a class="inbiz_icon icon_fullscreen_32x32" href="javascript:;" style="float:right;" onclick="fullScreen('acc');"></a>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="collapsiblepanel-content">
                        <div id="divRcv">
                            <uc4:AccountReceivable ID="AccountReceivable1" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both;">
            </div>
        </div>
    </asp:Panel>
    <div id="divFullScreenSales">       
    </div>
    <div id="divFullScreenTopCust">       
    </div>
    <div id="divFullScreenTopProducts">       
    </div>
    <div id="divFullScreenAccounts">       
    </div>
    <iCtrl:IframeDialog ID="dgSettings" Width="600" Height="220" Title="Dashboard Settings"
        Dragable="true" TriggerSelectorClass="dashSettings" TriggerSelectorMode="Class"
        UrlSelector="TriggerControlHrefAttribute" runat="server">
    </iCtrl:IframeDialog>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" Runat="Server">        
    <asp:Panel ID="pnlDashboardScripts" runat="server">
        <script type="text/javascript">
            var dashSettings = {};
            //$(".collapsiblepanel").collapsiblepanel();
            if (screen.width <= 800) {
                $(".dash_column").css({ width: '100%' });
            }

            $fullscreenSales = $("#divFullScreenSales").dialog({
                modal: true,
                autoOpen: false,
                resizable: false,
                width: 800,
                height: 500
            });
            $fullscreenTopCust = $("#divFullScreenTopCust").dialog({
                modal: true,
                autoOpen: false,
                resizable: false,
                width: 800,
                height: 500
            });
            $fullscreenTopProducts = $("#divFullScreenTopProducts").dialog({
                modal: true,
                autoOpen: false,
                resizable: false,
                width: 800,
                height: 500
            });
            $fullscreenAccounts = $("#divFullScreenAccounts").dialog({
                modal: true,
                autoOpen: false,
                resizable: false,
                width: 800,
                height: 500
            });

            function fullScreen(chart) {
                switch (chart) {
                    case "sales":
                        $fullscreenSales.dialog("open");
                        $.ajax({
                            type: "POST",
                            url: '<%=ResolveUrl("~/ajax/chartAPI.aspx/GetSalesCompairDataByMonths")%>',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: JSON.stringify(dashSettings.SalesChart),
                            success: function (data) {
                                $fullscreenSales.wijbarchart({
                                    horizontal: false,
                                    axis: {
                                        y: {
                                            text: ""
                                        },
                                        x: {
                                            text: ""
                                        }
                                    },
                                    hint: {
                                        content: function () {
                                            return this.data.label + '\n ' + this.y + '';
                                        }
                                    },
                                    header: data.d.Header,
                                    seriesList: data.d.SeriesList
                                });
                            },
                            error: function (request, status, errorThrown) {
                                alert(status);
                            }
                        });
                        break;
                    case "topcust":
                        $fullscreenTopCust.dialog("open");
                        $.ajax({
                            type: "POST",
                            url: '<%=ResolveUrl("~/ajax/chartAPI.aspx/GetTopCustomerData")%>',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: JSON.stringify(dashSettings.TopCustomers),
                            success: function (data) {
                                $fullscreenTopCust.wijbarchart({
                                    horizontal: false,
                                    axis: {
                                        y: {
                                            text: ""
                                        },
                                        x: {
                                            text: ""
                                        }
                                    },
                                    hint: {
                                        content: function () {
                                            return this.data.label + '\n ' + this.y + '';
                                        }
                                    },
                                    header: data.d.Header,
                                    seriesList: data.d.SeriesList
                                });
                            },
                            error: function (request, status, errorThrown) {
                                alert(status);
                            }
                        });
                        break;
                    case "topprd":
                        $fullscreenTopProducts.dialog("open");
                        $.ajax({
                            type: "POST",
                            url: '<%=ResolveUrl("~/ajax/chartAPI.aspx/GetTopProductsData")%>',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: JSON.stringify(dashSettings.TopProducts),
                            success: function (data) {                                
                                $fullscreenTopProducts.wijbarchart({
                                    horizontal: false,
                                    axis: data.d.Axis,
                                    hint: {
                                        content: function () {
                                            return this.data.label + '\n ' + this.y + '';
                                        }
                                    },
                                    header: data.d.Header,
                                    seriesList: data.d.SeriesList,
                                    chartLabelFormatString: data.d.ChartLabelFormatString
                                });
                            },
                            error: function (request, status, errorThrown) {
                                alert(status);
                            }
                        });
                        break;
                    case "acc":
                        $fullscreenAccounts.dialog("open");
                        $.ajax({
                            type: "POST",
                            url: '<%=ResolveUrl("~/ajax/chartAPI.aspx/GetAccountRcvPieData")%>',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: JSON.stringify(dashSettings.Account),
                            success: function (data) {
                                $fullscreenAccounts.wijpiechart({
                                    radius: 140,
                                    legend: { visible: true },
                                    hint: {
                                        content: function () {
                                            return this.data.label + " : " + Globalize.format(this.value / this.total, "p2");
                                        }
                                    },
                                    header: data.d.Header,
                                    seriesList: data.d.SeriesList
                                });
                            },
                            error: function (request, status, errorThrown) {
                                alert(status);
                            }
                        });
                        break;
                }
            }
        </script>
        <script type="text/javascript">
            function initSalesChartByMonth(container, yearRange, whs, salesBy) {                
                callbackSalesChartData(container, yearRange, whs, salesBy, false);
            }

            function callbackSalesChartData(container, yearRange, whs, salesBy, isCallback) {                
                var dataToPost = {};
                dataToPost.sessionID = '<%=Session.SessionID%>';
                dataToPost.yearRange = yearRange;
                dataToPost.whs = whs;
                dataToPost.salesBy = salesBy;
                //dataToPost.isWebSale = isWebSale;                
                dashSettings.SalesChart = dataToPost;       
                blockContentArea($("#divSalesMonth"), "Loading...");                      
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/ajax/chartAPI.aspx/GetSalesCompairDataByMonths")%>',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(dataToPost),
                    success: function (data) {
                        if (isCallback) {
                            $(container).wijbarchart("option", "header", data.d.Header);
                            $(container).wijbarchart("option", "seriesList", data.d.SeriesList);
                        }
                        else {
                            $(container).wijbarchart({
                                horizontal: false,
                                axis: {
                                    y: {
                                        text: ""
                                    },
                                    x: {
                                        text: "",
                                        labels: {
                                            style: {                                                
                                                rotation: -60,
                                            },
                                            textAlign: "near",                                             
                                            width: null
                                        }
                                    }
                                },
                                hint: {
                                    content: function () {
                                        return this.data.label + '\n ' + this.y + '';
                                    }
                                },

                                /*legend: {
                                text: "legend",
                                style: {
                                fill: "#f1f1f1",
                                stroke: "#010101"
                                }
                                },*/
                                header: data.d.Header,
                                seriesList: data.d.SeriesList
                            });                            
                        }
                        unblockContentArea($("#divSalesMonth"));
                    },
                    error: function (request, status, errorThrown) {
                        alert(status);
                    }
                });
            }

            function initTopCustomerChartData(container, yearRange, whs) {
                callbackTopCustomerChartData(container, yearRange, whs, false);
            }
            function callbackTopCustomerChartData(container, yearRange, whs, isCallback) {
                var dataToPost = {};
                dataToPost.sessionID = '<%=Session.SessionID%>';
                dataToPost.yearRange = yearRange;
                dataToPost.whs = whs;
                dashSettings.TopCustomers = dataToPost;
                blockContentArea($("#divTopCust"), "Loading...");
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/ajax/chartAPI.aspx/GetTopCustomerData")%>',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(dataToPost),
                    success: function (data) {
                        if (isCallback) {
                            $(container).wijbarchart("option", "header", data.d.Header);
                            $(container).wijbarchart("option", "seriesList", data.d.SeriesList);
                        }
                        else {
                            $(container).wijbarchart({
                                horizontal: false,
                                axis: {
                                    y: {
                                        text: ""
                                    },
                                    x: {
                                        text: "",
                                        labels: {
                                            style: {                                                
                                                rotation: -60,
                                            },
                                            textAlign: "near",                                             
                                            width: null
                                        }
                                    }
                                },
                                hint: {
                                    content: function () {
                                        return this.data.label + '\n ' + this.y + '';
                                    }
                                },
                                header: data.d.Header,
                                seriesList: data.d.SeriesList
                            });
                        }
                        unblockContentArea($("#divTopCust"));
                    },
                    error: function (request, status, errorThrown) {
                        alert(status);
                    }
                });
            }

            function initTopProductsChartData(container, yearRange, by, whs, salesBy) {
                callbackTopProductsChartData(container, yearRange, by, whs,salesBy, false);
            }
            function callbackTopProductsChartData(container, yearRange, by, whs,salesBy, isCallback) {
                var dataToPost = {};
                dataToPost.sessionID = '<%=Session.SessionID%>';
                dataToPost.yearRange = yearRange;
                dataToPost.by = by;
                dataToPost.whs = whs;
                dataToPost.salesBy = salesBy;
                //dataToPost.isWebSale = isWebSale;
                dashSettings.TopProducts = dataToPost;
                blockContentArea($("#divTopPrd"), "Loading...");
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/ajax/chartAPI.aspx/GetTopProductsData")%>',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(dataToPost),
                    success: function (data) {
                        if (isCallback) {
                            //alert(data.d.Axis.toString());
                            $(container).wijbarchart("option", "header", data.d.Header);
                            $(container).wijbarchart("option", "seriesList", data.d.SeriesList);
                            $(container).wijbarchart("option", "axis", data.d.Axis);
                            $(container).wijbarchart("option", "chartLabelFormatString", data.d.ChartLabelFormatString);
                        }
                        else {
                            $(container).wijbarchart({
                                horizontal: false,
                                axis: {
                                    y: {
                                        text: ""
                                    },
                                    x: {
                                        text: "",
                                        labels: {
                                            style: {                                                
                                                rotation: -60,
                                            },
                                            textAlign: "near",                                             
                                            width: null
                                        }
                                    }
                                },
                                hint: {
                                    content: function () {
                                        return this.data.label + '\n ' + this.y + '';
                                    }
                                },
                                header: data.d.Header,
                                seriesList: data.d.SeriesList,
                                chartLabelFormatString: data.d.ChartLabelFormatString
                            });
                        }
                        unblockContentArea($("#divTopPrd"));
                    },
                    error: function (request, status, errorThrown) {
                        alert(status);
                    }
                });
            }

            function initRcvPieChartData(container, whs) {
                callbackRcvPieChartData(container, whs, false);
            }
            function callbackRcvPieChartData(container, whs, isCallback) {
                var dataToPost = {};
                dataToPost.sessionID = '<%=Session.SessionID%>';
                dataToPost.whs = whs;
                dashSettings.Account = dataToPost;
                blockContentArea($("#divRcv"), "Loading...");
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/ajax/chartAPI.aspx/GetAccountRcvPieData")%>',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(dataToPost),
                    success: function (data) {
                        if (isCallback) {
                            $(container).wijpiechart("option", "header", data.d.Header);
                            $(container).wijpiechart("option", "seriesList", data.d.SeriesList);
                        }
                        else {
                            $(container).wijpiechart({
                                radius: 140,
                                legend: { visible: true },
                                hint: {
                                    content: function () {
                                        return this.data.label + " : " + Globalize.format(this.value / this.total, "p2");
                                    }
                                },
                                 header: data.d.Header,
                                seriesList: data.d.SeriesList
                            });
                        }
                        unblockContentArea($("#divRcv"));
                    },
                    error: function (request, status, errorThrown) {
                        alert(status);
                    }
                });
            }

            function grdInited() {
                $(".ui-jqgrid-pager").hide();
                $(".ui-jqgrid-hbox").hide();
            }

            function grdRowSelect(rowid, e) {
                return false;
            }            
           
        </script>
    </asp:Panel>
</asp:Content>


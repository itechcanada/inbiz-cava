﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="ViewUserAccessIPs.aspx.cs" Inherits="Admin_ViewUserAccessIPs" %>

<%@ Register Src="~/Controls/UsersLeft.ascx" TagName="UserLeftMenu" TagPrefix="uc2" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphHead">
    <%--Keep script file reference here those were required for this page only. 
    Note* if it is to being used on all pages than should pe kept on master page.--%>
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphLeft" runat="Server">
    <asp:Panel runat="server" CssClass="divSectionContent" ID="LeftMenu">
        <uc2:UserLeftMenu ID="UserLeftMenu" runat="server" />
    </asp:Panel>
</asp:Content>
<asp:Content ID="cntViewTaxes" ContentPlaceHolderID="cphMaster" runat="Server">
    <h3>
        <asp:Literal ID="ltSectionTitle" Text="" runat="server" />
    </h3>
    <asp:Panel runat="server" ID="Panel14" DefaultButton="btnSave">
        <table class="contentTableForm" width="100%" style="border: 0;" border="0">
            <tbody>
                <tr>
                    <td valign="top" class="text">
                        <asp:Label ID="lblProdDescr" runat="server" Text="<%$ Resources:Resource, lblIPAddress %>"
                            CssClass="lblBold"></asp:Label>
                    </td>
                    <td class="input tbx_editor">
                        <asp:TextBox ID="txtIPAddress" runat="server" TextMode="MultiLine" Width="440px"
                            Height="100px" onkeyup="return MaxLengthValdation(this.id, 250);" />
                        <%--<asp:RequiredFieldValidator ID="reqvalProdDescr" runat="server" Display="None" SetFocusOnError="true"
                            ErrorMessage="<%$ Resources:Resource, reqvalIPAddresses %>" ControlToValidate="txtIPAddress"></asp:RequiredFieldValidator>
                        <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, noteReqIPAddressMsg %>"></asp:Label>--%>
                    </td>
                </tr>
                <asp:ValidationSummary ID="sumvalDesc" runat="server" ShowSummary="false" ShowMessageBox="true">
                </asp:ValidationSummary>
            </tbody>
        </table>
        <div style="text-align: right; border-top: 1px solid #ccc; margin: 5px 0px; padding: 10px;">
            <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave %>" runat="server"
                OnClick="btnSave_Click" />
                        <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnReset" runat="server"
            CausesValidation="False" OnClick="btnReset_Click" />
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
    <script language="javascript">
        function MaxLengthValdation(ctrlID, maxLength) {

            //get the limit from maxlength attribute
            var limit = maxLength; // parseInt($(this).attr('maxlength'));
            //get the current text inside the textarea
            var text = $("#" + ctrlID).val();
            //count the number of characters in the text
            var chars = text.length;

            //check if there are more characters then allowed
            if (chars > limit) {
                //and if there are use substr to get the text before the limit
                var new_text = text.substr(0, limit);

                //and change the current text with the new text
                $("#" + ctrlID).val(new_text);
            }


        }
    </script>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;

public partial class Admin_default : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //pnlDashboard.Visible = Request.IsLocal; //Because under cunstriction

        if (this.UserModule == "POS")
        {
            Response.Redirect("SelectRegister.aspx?" + Request.QueryString);
        }
        else if (this.UserModule == "RSV")
        {
            Response.Redirect("~/Reservation/default.aspx");
        }
        else if (this.UserModule == "GUEST")
        {
            Response.Redirect("~//Partner/ViewCustomer.aspx?pType=E&gType=1");
        }
        else if (this.UserModule == "KITCHEN" && this.RegCode=="HIT")
        {
            Response.Redirect("~/POS/Kitchen.aspx");
        }
        if (!IsPostBack)
        {
            pnlDashboard.Visible = CurrentUser.IsInRole(RoleID.DASHBOARD_MANAGER);
            pnlDashboardScripts.Visible = pnlDashboard.Visible;
        }
    }

    //Initialize Culture
    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }

    private string UserModule
    {
        get
        {
            return Request.QueryString["UserModules"];
        }
    }

    private string RegCode
    {
        get
        {
            return Request.QueryString["regCode"];
        }
    }
}
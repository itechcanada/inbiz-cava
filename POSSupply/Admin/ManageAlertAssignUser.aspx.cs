﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Admin_ManageAlertAssignUser : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                InbizUser user = new InbizUser();
                user.FillAllUsers(dbHelp, ddlUsers, null);
                NewAlert al = new NewAlert();
                var lst = al.GetAlertUserList(dbHelp, this.AlertName);
                foreach (var item in lst)
                {
                    ListItem li = ddlUsers.Items.FindByValue(item.UserID.ToString());
                    if (li != null)
                    {
                        li.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    private string AlertName {
        get {
            return BusinessUtility.GetString(Request.QueryString["aName"]);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            NewAlert al = new NewAlert();
            var lst = new List<int>();
            int id = 0;
            foreach (ListItem item in ddlUsers.Items)
            {
                id = 0;
                if (item.Selected && int.TryParse(item.Value, out id) && id > 0 && !lst.Contains(id))
                {
                    lst.Add(id);
                }
            }
            al.AssignUsersToAlert(null, this.AlertName, lst.ToArray());

            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgAlertsAssignedToUser);
            Globals.RegisterReloadParentScript(this);
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
        }
    }
}
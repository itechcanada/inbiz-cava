﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_AddEditVendor : BasePage
{   
    //On Page Load
    Vendor _objVendor = new Vendor();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security Check
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }        

        //to show history button only to specific roles
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR) || CurrentUser.IsInRole(RoleID.PROCURMENT_MANAGER) || CurrentUser.IsInRole(RoleID.PO_APPROVER)) {
            btnHistroy.Visible = true;
        }
        else
        {
            btnHistroy.Visible = false;
        }
       
        if (!IsPostBack)
        {
            DropDownHelper.FillDropdown(ddlVendorStatus, "VN", "dlVen", Globals.CurrentAppLanguageCode, null);
            DropDownHelper.FillDropdown(ddlVendorPOPref, "VN", "dlVPO", Globals.CurrentAppLanguageCode, null);
            DropDownHelper.FillCurrencyDropDown(dlCurrencyCode, new ListItem(Resources.Resource.lstSelectCurrency, "0"));
            lblTitle.Text = Resources.Resource.AddVendor;
            btnProduct.Visible = this.VendorID > 0;
            btnDelete.Visible = this.VendorID > 0;
            if (this.VendorID > 0)
            {
                lblTitle.Text = Resources.Resource.EditVendor;
                FillRecord();
            }
        }
        txtvendorName.Focus();
    }
    //Populate Controls
    public void FillRecord()
    {
        if (this.VendorID > 0) {
            _objVendor.VendorID = this.VendorID;
            trvendorActive.Visible = true;
            _objVendor.PopulateObject(null, this.VendorID);
            //Get vendor Info
            txtvendorName.Text = _objVendor.VendorName;
            txtvendorEmailID.Text = _objVendor.VendorEmailID;
            txtvendorFax.Text = _objVendor.VendorFax;
            txtvendorPhone.Text = _objVendor.VendorPhone;
            ddlVendorPOPref.SelectedValue = _objVendor.VendorPOPref;
            ddlVendorStatus.SelectedValue = _objVendor.VendorStatus;
            dlCurrencyCode.SelectedValue = _objVendor.VendorCurrency;
            rdbYes.Checked = _objVendor.VendorActive;
            rblstValidated.SelectedValue = _objVendor.VendorValidated ? "1" : "0";
            txtContPersonName.Text = _objVendor.VendorContactPersonName;
            ddlLanguage.SelectedValue = _objVendor.VendorLang;
            txtContactPersonPhone.Text = _objVendor.VendorContactPersonPhone;
            txtContactPersonEmailID.Text = _objVendor.VendorContactPersonEmailID;
            
            //'Set HeadQuaterAddress address
            ucHeadQuaterAddress.ReferenceTable = AddressReference.VENDOR;
            ucHeadQuaterAddress.AddressType = AddressType.HEAD_OFFICE;
            ucHeadQuaterAddress.SourcePrimaryKey = _objVendor.VendorID;
            ucHeadQuaterAddress.Initialize();

            //'Set Bill To Address 
            ucBillToAddress.ReferenceTable = AddressReference.VENDOR;
            ucBillToAddress.AddressType = AddressType.BILL_TO_ADDRESS;
            ucBillToAddress.SourcePrimaryKey = _objVendor.VendorID;
            ucBillToAddress.Initialize();

            //'Set Shp To Address
            ucShpToAddress.ReferenceTable = AddressReference.VENDOR;
            ucShpToAddress.AddressType = AddressType.SHIP_TO_ADDRESS;
            ucShpToAddress.SourcePrimaryKey = _objVendor.VendorID;
            ucShpToAddress.Initialize();
        }       
    }
    //Populate Object
    private void SetData()
    {
        _objVendor.VendorName = txtvendorName.Text;
        _objVendor.VendorEmailID = txtvendorEmailID.Text;
        _objVendor.VendorFax = txtvendorFax.Text;
        _objVendor.VendorPhone = txtvendorPhone.Text;
        _objVendor.VendorPOPref = ddlVendorPOPref.SelectedValue;
        _objVendor.VendorStatus = ddlVendorStatus.SelectedValue;
        _objVendor.VendorCurrency = dlCurrencyCode.SelectedValue;
        _objVendor.VendorActive = rdbYes.Checked;
        _objVendor.VendorValidated = rblstValidated.SelectedValue == "1";
        _objVendor.VendorContactPersonName = txtContPersonName.Text;
        _objVendor.VendorLang = ddlLanguage.SelectedValue;
        _objVendor.VendorContactPersonPhone = txtContactPersonPhone.Text;
        _objVendor.VendorContactPersonEmailID = txtContactPersonEmailID.Text;

        //' Set Head Quater Address Value
        ucHeadQuaterAddress.ReferenceTable = AddressReference.VENDOR;
        ucHeadQuaterAddress.AddressType = AddressType.HEAD_OFFICE;
        _objVendor.HeadQuaterAddress = ucHeadQuaterAddress.Address;

        //'Set Bill To Address Value 
        ucBillToAddress.ReferenceTable = AddressReference.VENDOR;
        ucBillToAddress.AddressType = AddressType.BILL_TO_ADDRESS;
        _objVendor.BillToAddress = ucBillToAddress.Address;

        //'Set Shp To Address Value
        ucShpToAddress.ReferenceTable = AddressReference.VENDOR;
        ucShpToAddress.AddressType = AddressType.SHIP_TO_ADDRESS;
        _objVendor.ShpToAddress = ucShpToAddress.Address;
    }

    protected void btnProduct_Click(object sender, System.EventArgs e)
    {
        Response.Redirect(string.Format("~/Inventory/ViewProduct.aspx?VdrID={0}", this.VendorID));
    }

    protected void btnHistroy_Click(object sender, System.EventArgs e)
    {
        Response.Redirect(string.Format("~/Procurement/ViewPurchaseOrder.aspx?VendorID={0}", this.VendorID));
    }

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        if (Page.IsValid)
        {
            SetData();
            if (this.VendorID > 0)
            {
                _objVendor.VendorID = this.VendorID;
                _objVendor.Update();
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.VendorUpdatedSuccessfully);                
                Response.Redirect("ViewVendor.aspx");
            }
            else
            {
                try
                {
                    _objVendor.Insert();
                    if (_objVendor.VendorID > 0 && Request.QueryString["adVnd"] == "Y") {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.VendorAddedSuccessfully);
                        //Response.Redirect(string.Format("~/Procurement/Create.aspx?venID={0}", _objVendor.VendorID));
                        Response.Redirect(string.Format("~/Procurement/Create.aspx?VdrID={0}", _objVendor.VendorID), false);
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.VendorAddedSuccessfully);
                        Response.Redirect("ViewVendor.aspx");
                    }
                }
                catch(Exception ex)
                {
                    switch (ex.Message)
                    {
                        case CustomExceptionCodes.VENDOR_ALREADY_EXISTS:
                            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.VendorAlreadyExists);
                            txtvendorName.Focus();
                            break;                        
                    }
                }                              
            }
        }
    }
    
    protected void btnCancel_Click(object sender, System.EventArgs e)
    {
        if (Request.QueryString["adVnd"] == "Y")
        {
            Response.Redirect("~/Procurement/ViewPurchaseOrder.aspx");
        }
        else
        {
            Response.Redirect("~/Admin/ViewVendor.aspx");
        }
        txtvendorName.Text = "";
        txtvendorEmailID.Text = "";
        txtvendorFax.Text = "";
        txtvendorPhone.Text = "";
        ddlVendorPOPref.SelectedValue = "";
        ddlVendorStatus.SelectedValue = "";
        txtContPersonName.Text = "";
    }

    private int VendorID {
        get {
            int vid = 0;
            int.TryParse(Request.QueryString["vendorID"], out vid);
            return vid;
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Vendor vendor = new Vendor();

            try
            {
                vendor.Delete(this.VendorID);
                MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgDeleteSuccess);
                Response.Redirect("ViewVendor.aspx", false);
            }
            catch
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.lblAccessDenied);
        }        
    }
}
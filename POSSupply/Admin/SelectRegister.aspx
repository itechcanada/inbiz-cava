﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/fullWidth.master" AutoEventWireup="true" CodeFile="SelectRegister.aspx.cs" Inherits="Admin_SelectRegister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" Runat="Server">
     <h4>
        <%= Resources.Resource.POSRegisterHeading%>
    </h4>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
   
   <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0">   
        <tr>
            <td class="text" >
                <asp:Label ID="Label1" Text="<%$ Resources:Resource, POSRegister%>" CssClass="lblBold"
                    runat="server" />
            </td>            
            <td class="input">
                <asp:DropDownList ID="dlRegister" runat="server" Width="336px" CssClass="innerText">
                </asp:DropDownList>
                <span class="style1">*</span>
                <asp
                <asp:CustomValidator ID="custvalRegister" SetFocusOnError="true" runat="server" ErrorMessage="<%$ Resources:Resource, POScustvalRegister%>"
                    ClientValidationFunction="funSelectRegister" Display="None"></asp:CustomValidator>
            </td>
        </tr>        

        <tr>
            <td class="text">
            </td>            
            <td class="input">
                <asp:Button ID="btnSave" Text="<%$Resources:Resource, cmdCssSave%>" 
                    runat="server" onclick="btnSave_Click" />
                <asp:Button ID="btnReset" Text="<%$Resources:Resource, cmdCssReset%>" 
                    runat="server" onclick="btnReset_Click" />
            </td>
        </tr>
        <asp:ValidationSummary ID="sumvalWhs" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </table>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" Runat="Server">
    <script type="text/javascript" language="javascript">
        function funSelectRegister(source, arguments) {
            if (window.document.getElementById('<%=dlRegister.ClientID%>').value != "0") {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }



        </script>
</asp:Content>


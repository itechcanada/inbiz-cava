﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.WebControls;
using iTECH.Library.Utilities;
using iTECH.InbizERP.BusinessLogic;

public partial class Admin_ViewUserAccessIPs : BasePage
{
    UserIPs objUserIPs = new UserIPs();

    // On Page Load
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/AdminLogin.aspx");
        }
        if (!IsPostBack)
        {
            ltSectionTitle.Text = Resources.Resource.lnkUserAccessIPs;

            if (UserID > 0)
            {
                objUserIPs.UserID = this.UserID;

                var vIPList = objUserIPs.GetIPsList(null);
                if (vIPList.Count > 0)
                {
                    txtIPAddress.Text = vIPList[0].IPAddress;
                }
            }
            txtIPAddress.Focus();
        }
    }

    public void SetData()
    {
        objUserIPs.UserID = this.UserID;
        var vIPList = objUserIPs.GetIPsList(null);
        if (vIPList.Count > 0)
        {
            objUserIPs.IPAddressID = vIPList[0].IPAddressID;
        }
        objUserIPs.IPAddress = txtIPAddress.Text;
        objUserIPs.UserID = this.UserID;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                SetData();
                if (ValidateIPAddress() == false)
                {
                    return;
                }
                if (objUserIPs.Save(null) == true)
                {
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgUsersIPSaved);
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == "USER_ALREADY_EXISTS")
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgLoginIDAlreadyExists);
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgCriticalError);
                }
            }
        }
    }

    public int UserID
    {
        get
        {
            int uid = 0;
            int.TryParse(Request.QueryString["UserID"], out uid);
            return uid;
        }
    }

    public Boolean ValidateIPAddress()
    {
        if (txtIPAddress.Text == "")
        {
            return true;
        }
        string[] arrIPAddresses = BusinessUtility.GetString(txtIPAddress.Text).Split(',');
        string sInvalidIPAddr = "";
        foreach (string sIPAddr in arrIPAddresses)
        {
            System.Net.IPAddress address;
            if (System.Net.IPAddress.TryParse(sIPAddr, out address))
            {
                //Valid IP, with address containing the IP
            }
            else
            {
                if (sInvalidIPAddr != "")
                {
                    sInvalidIPAddr += "," + sIPAddr;
                }
                else
                {
                    sInvalidIPAddr = sIPAddr;
                }
            }
        }

        if (sInvalidIPAddr != "")
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblInvalidIPAddrFormat + " : " + sInvalidIPAddr);
            return false;
        }

        return true;
    }

    protected void btnReset_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("ViewInternalUser.aspx");
    }

}
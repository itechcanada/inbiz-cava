﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="ViewUserSalesGoal.aspx.cs" Inherits="Admin_ViewUserSalesGoal" %>

<%@ Register Src="~/Controls/UsersLeft.ascx" TagName="UserLeftMenu" TagPrefix="uc2" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphHead">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <asp:Literal runat="server" ID="ltrTitle2"></asp:Literal></b>
    </div>
    <div style="float: left; padding-top: 12px;">
        <asp:Button ID="AddSalesGoal" runat="server" Text="<%$ Resources:Resource, lblAddSalesGoal %>" />
        <iCtrl:IframeDialog ID="mdCreateSalesGoal" TriggerControlID="AddSalesGoal" Height="360" Width="490" Url="AddEditSalesGoal.aspx?jscallback=reloadGrid" Title="<%$ Resources:Resource, lblAddSalesGoal %>"
            runat="server" OnCloseClientFunction="reloadGrid">
        </iCtrl:IframeDialog>
    </div>
    <div style="float: right;">
        <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Download CVS" ImageUrl="~/Images/new/ico_csv.gif"
            CssClass="csv_export" />
        <asp:LinkButton ID="lnkAddClockIn" runat="server" Visible="false"></asp:LinkButton>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
 <asp:Panel runat="server" CssClass="divSectionContent" ID="LeftMenu">
        <uc2:UserLeftMenu ID="UserLeftMenu" runat="server" />
    </asp:Panel>
        <asp:Panel runat="server" ID="SearchPanel" Visible="false">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        <%= Resources.Resource.lblSearchForm%></div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" /></div>
                </div>
                <h4>
                    <asp:Label ID="Label3" CssClass="filter-key" AssociatedControlID="dlUser" runat="server"
                        Text="<%$ Resources:Resource, lblSearchUser %>"></asp:Label></h4>
                <div class="inner">
                    <asp:DropDownList ID="dlUser" runat="server" CssClass="filter-key">
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" CssClass="filter-key" AssociatedControlID="txtDate"
                        runat="server" Text="<%$ Resources:Resource, lblDate %>"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtDate" CssClass="datepicker"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner" style="display:none;">
            <h2>
                <%=Resources.Resource.lblSearchOptions%></h2>
            <p>
                <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntViewClockIn" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
            <h3>
                <asp:Literal runat="server" ID="ltrPageTitle"></asp:Literal>
            </h3>
        <div onkeypress="return disableEnterKey(event)">
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="jgdvSalesGoal" Height="300px" AutoWidth="True"
                    OnCellBinding="jgdvSalesGoal_CellBinding" OnDataRequesting="jgdvSalesGoal_DataRequesting">
                    <Columns>
                        <trirand:JQGridColumn DataField="idUserSalesGoal" HeaderText="" PrimaryKey="True"
                            Visible="false" />
                        <trirand:JQGridColumn DataField="fromDate" HeaderText="<%$ Resources:Resource, lblRptFromDate %>" DataFormatString="{0:MMM dd, yyyy}"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="toDate" HeaderText="<%$ Resources:Resource, lblRptToDate %>"  DataFormatString="{0:MMM dd, yyyy}"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="salesAmountPerDay" HeaderText="<%$ Resources:Resource, lblSalesAmountPerDay %>"
                            Editable="false" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="idUserSalesGoal"
                            Sortable="false" TextAlign="Center" Width="50" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="idUserSalesGoal"
                            Sortable="false" TextAlign="Center" Width="50" Visible="false" />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents LoadComplete="jqGridResize" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sqldsTaxes" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
            </div>
            <iCtrl:IframeDialog ID="mdEditTax" Height="360" Width="490" Title="<%$ Resources:Resource, lblEditSalesGoal %>"
                Dragable="true" TriggerSelectorClass="edit-Tax" TriggerSelectorMode="Class" OnCloseClientFunction="reloadGrid"
                UrlSelector="TriggerControlHrefAttribute" runat="server">
            </iCtrl:IframeDialog>
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" runat="server" ContentPlaceHolderID="cphScripts">
    <script type="text/javascript">
        $grid = $("#<%=jgdvSalesGoal.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=jgdvSalesGoal.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        function deleteRow(rowID) {
            if (confirm("<%=Resources.Resource.msgDeleteCategory%>")) {
                var dataToPost = {};
                dataToPost.cmdDelete = "1"; //$grid.jqCustomSearch
                dataToPost.idUsersClockIn = rowID;
                $grid.jqCustomSearch(dataToPost);
            }
        }

        function redirectToPOS() {
            window.location.href = ('../POS/POS.aspx');
        }


        function reloadGrid(event, ui) {
            $('#' + "<%=jgdvSalesGoal.ClientID%>").trigger("reloadGrid");
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }
    </script>
</asp:Content>

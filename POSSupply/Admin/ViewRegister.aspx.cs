﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Admin_ViewRegister :BasePage
{    
    private SysRegister objSysRegister = new SysRegister();
    protected void Page_Load(object sender, EventArgs e)
    {
        //Security
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
        {
            btnRegister.Visible = false;
        }

        if (!IsPagePostBack(jgdvRegister))
        {
            lblTitle.Text = Resources.Resource.ViewRegister;
        }       
    }

    protected void jgdvRegister_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 2)
        {
            e.CellHtml = string.Format(@"<a class=""edit-Register""  href=""AddEditRegister.aspx?RID={0}&jscallback={1}"" >" + Resources.Resource.grdEdit + "</a>", e.RowValues[0], "reloadGrid");
        }
        if (e.ColumnIndex == 3)
        {
            string delUrl = string.Format("delete.aspx?cmd={0}&keys={1}&jscallback={2}", DeleteCommands.DELETE_POS, e.RowKey, "reloadGrid");
            e.CellHtml = string.Format(@"<a class=""pop_delete"" href=""{0}"">" + Resources.Resource.delete + "</a>", delUrl);
        }
    }

    protected void jgdvRegister_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains<string>("_history"))
        {

            sqldsRegister.SelectCommand = objSysRegister.GetSQL(sqldsRegister.SelectParameters, Request.QueryString[txtSearch.ClientID]);
        }
        else
        {
            sqldsRegister.SelectCommand = objSysRegister.GetSQL(sqldsRegister.SelectParameters, Request.QueryString[txtSearch.ClientID]);
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="AddEditUser.aspx.cs" Inherits="Admin_AddEditUser" %>

<%@ Register Src="~/Controls/CommonSearch/UserSearch.ascx" TagName="UserSearch" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/UsersLeft.ascx" TagName="UserLeftMenu" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <link href="../lib/css/accordioninbiz/jquery-ui-1.8.12.custom.css" rel="stylesheet"
        type="text/css" />
    <style type="text/css">
        #contentBottom .contentTableForm td.text
        {
            width: 200px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" alt="" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.Administration %></h1>
        <b>
            <%= Resources.Resource.lblAddEdit %>
            :<%= Resources.Resource.lblManageUser %></b>
    </div>
    <div class="clear">
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
    <asp:Panel runat="server" CssClass="divSectionContent" ID="LeftMenu">
        <uc2:UserLeftMenu ID="UserLeftMenu" runat="server" />
    </asp:Panel>
    <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel" Visible="false">
        <uc1:UserSearch ID="UserSearch1" runat="server" />
    </asp:Panel>
    <div class="clear">
    </div>
    <div class="inner" style="display: none;">
        <h2>
            <%=Resources.Resource.lblSearchOptions%></h2>
        <p>
            <asp:Literal runat="server" ID="lblTitle"></asp:Literal></p>
    </div>
    <div class="clear">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <asp:Panel ID="pnlUserDetail" runat="server">
            <h3>
                <%= Resources.Resource.Access%>
            </h3>
            <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblLoginID %>" ID="lblLoginID" runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox Width="240px" ID="txtLoginID" runat="server" MaxLength="15" />
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalLoginID" ErrorMessage="<%$ Resources:Resource, reqvalLoginID%>"
                            runat="server" ControlToValidate="txtLoginID" SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="trPasswordPop" runat="server" visible="false">
                    <td class="text">
                    </td>
                    <td class="input">
                        <asp:Button ID="btnChangePassword" Text="Change Password" runat="server" />
                        <iCtrl:IframeDialog ID="mdChangePassword" TriggerControlID="btnChangePassword" Width="400"
                            Height="200" Url="changepassword.aspx" Title="Change Password" runat="server"
                            OnCloseClientFunction="getGlobalMessage">
                        </iCtrl:IframeDialog>
                    </td>
                </tr>
                <tr id="trPassword" runat="server">
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblPassword %>" ID="lblPassword" runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox Width="240px" ID="txtPassword" runat="server" TextMode="Password" MaxLength="75">
                        </asp:TextBox><span class="style1"> *</span>
                        <asp:RequiredFieldValidator ID="reqvalPassword" ErrorMessage="<%$ Resources:Resource, reqvalPassword%>"
                            runat="server" ControlToValidate="txtPassword" SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr id="trConfPassword" runat="server">
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblConfirmPassword %>" ID="lblConfirmPassword"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox Width="240px" ID="txtConfirmPwd" runat="server" TextMode="Password"
                            MaxLength="75"></asp:TextBox><span class="style1"> *</span>
                        <asp:CompareValidator ID="cmpvalPassword2" runat="server" ControlToCompare="txtPassword"
                            ControlToValidate="txtConfirmPwd" ErrorMessage="<%$ Resources:Resource, cmpvalPassword2%>"
                            SetFocusOnError="true" Display="None"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblRecNotViaMail %>" ID="Label1" runat="server" />
                    </td>
                    <td class="input">
                        <asp:RadioButtonList ID="rbtnReceiveNotification" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="<%$Resources:Resource,lblYes%>" Value="1" Selected="True" />
                            <asp:ListItem Text="<%$Resources:Resource,lblNo%>" Value="0" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <h3>
                <%=Resources.Resource.user%></h3>
            <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="text">
                        <%--<asp:Label ID="" Text="<%$ Resources:Resource, lblFName%>" runat="server" />--%>
                        <asp:Label Text="<%$Resources:Resource, lblFName %>" ID="lblFName" runat="server"
                            AssociatedControlID="txtFName"></asp:Label>
                    </td>
                    <td class="input">
                        <asp:TextBox Width="240px" ID="txtFName" runat="server" MaxLength="25" />
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalFName" ErrorMessage="<%$ Resources:Resource, reqvalFName%>"
                            runat="server" ControlToValidate="txtFName" SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <%--<asp:Label ID="lblLName" Text="<%$ Resources:Resource, lblLName%>" runat="server" />--%>
                        <asp:Label Text="<%$Resources:Resource, lblLName %>" ID="lblLName" runat="server"
                            AssociatedControlID="txtLName"></asp:Label>
                    </td>
                    <td class="input">
                        <asp:TextBox Width="240px" ID="txtLName" runat="server" MaxLength="25" />
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalLName" ErrorMessage="<%$ Resources:Resource, reqvalLName%>"
                            runat="server" ControlToValidate="txtLName" SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblSalutation %>" ID="lblSalutation" runat="server"
                            AssociatedControlID="txtSalutation"></asp:Label>
                    </td>
                    <td class="input">
                        <asp:TextBox Width="240px" runat="server" ID="txtSalutation" MaxLength="5"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblDesignation %>" ID="lblDesignation" AssociatedControlID="txtDesignation"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox Width="240px" ID="txtDesignation" runat="server" MaxLength="50" />
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblDepartment %>" ID="lblDepartment" AssociatedControlID="txtDepartment"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox Width="240px" ID="txtDepartment" runat="server" MaxLength="50" />
                    </td>
                </tr>
            </table>
            <h3>
                <%= Resources.Resource.Contact %></h3>
            <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblEmail %>" ID="lblEmail" AssociatedControlID="txtEmail"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox Width="240px" ID="txtEmail" runat="server" MaxLength="75" />
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalEmail" ErrorMessage="<%$ Resources:Resource, reqvalEmail%>"
                            runat="server" ControlToValidate="txtEmail" SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revalEmail" runat="server" ControlToValidate="txtEmail"
                            Display="None" ErrorMessage="<%$ Resources:Resource, revalEmail%>" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblPhone %>" ID="lblPhone" AssociatedControlID="txtPhone"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox Width="240px" ID="txtPhone" runat="server" MaxLength="15" />
                        <asp:RequiredFieldValidator ID="reqvalPhoneNo" ErrorMessage="<%$ Resources:Resource, reqvalPhoneNo%>"
                            runat="server" ControlToValidate="txtPhone" SetFocusOnError="true" Display="None"
                            Enabled="false"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblPhoneExt %>" ID="lblPhoneExt" AssociatedControlID="txtPhoneExt"
                            runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox Width="240px" ID="txtPhoneExt" runat="server" MaxLength="7" />
                        <asp:RequiredFieldValidator ID="reqvalPhoneExt" ErrorMessage="<%$ Resources:Resource, reqvalPhoneExt%>"
                            runat="server" Enabled="false" ControlToValidate="txtPhoneExt" SetFocusOnError="true"
                            Display="None"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblFax %>" ID="lblFax" runat="server" />
                    </td>
                    <td class="input">
                        <asp:TextBox Width="240px" ID="txtFax" runat="server" MaxLength="15" />
                    </td>
                </tr>
                <tr>
                    <td class="text">
                        <asp:Label Text="<%$Resources:Resource, lblCellPhone %>" ID="lblCellPhone" runat="server"
                            AssociatedControlID="txtCellPhone"></asp:Label>
                    </td>
                    <td class="input">
                        <asp:TextBox Width="240px" ID="txtCellPhone" runat="server" MaxLength="15"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="pnlAdmin1" runat="server">
                <h3>
                    <%= Resources.Resource.Status%>
                </h3>
                <table id="tblSecurityRoles" class="contentTableForm" border="0" cellspacing="0"
                    cellpadding="0" runat="server">
                    <tr height="30" id="trRole" runat="server" visible="False">
                        <td class="text">
                            <asp:Label Text="<%$Resources:Resource, lblRole %>" ID="lblRole" AssociatedControlID="txtRole"
                                runat="server" />
                        </td>
                        <td class="input">
                            <asp:TextBox Width="240px" ID="txtRole" runat="server" MaxLength="5" Text="0" />
                            <span class="style1">*</span>
                            <asp:RequiredFieldValidator ID="reqvalRole" ErrorMessage="<%$ Resources:Resource, reqvalRole%>"
                                runat="server" ControlToValidate="txtRole" SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="compvalRole" EnableClientScript="true" SetFocusOnError="true"
                                ControlToValidate="txtRole" Operator="DataTypeCheck" Type="Integer" Display="None"
                                ErrorMessage="<%$ Resources:Resource, compvalRole %>" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text">
                            <asp:Label Text="<%$Resources:Resource, lblUserType %>" ID="lblUserType" runat="server"
                                AssociatedControlID="ddlUserType"></asp:Label>
                        </td>
                        <td class="input">
                            <asp:DropDownList ID="ddlUserType" runat="server" Width="135px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="text">
                            <asp:Label Text="<%$Resources:Resource, lblStatus %>" ID="lblStatus" runat="server"
                                AssociatedControlID="ddlStatus"></asp:Label>
                        </td>
                        <td class="input">
                            <asp:DropDownList ID="ddlStatus" runat="server" Width="135px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr height="30" runat="server" id="trIsActive" visible="false">
                        <td class="text">
                            <asp:Label Text="<%$Resources:Resource, lblIsActive %>" ID="lblIsActive" runat="server" />
                        </td>
                        <td class="input">
                            <asp:RadioButtonList ID="rblstActive" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="<%$ Resources:Resource, lblYes%>" Value="1" Selected="True">
                                </asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, lblNo%>" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="text">
                            <asp:Label Text="<%$Resources:Resource, lblIsValidated %>" ID="lblIsValidated" runat="server" />
                        </td>
                        <td class="input">
                            <asp:RadioButtonList ID="rblstValidated" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="<%$ Resources:Resource, lblYes%>" Value="1">
                                </asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, lblNo%>" Value="0" Selected="True">
                                </asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="text">
                            <asp:Label Text="<%$Resources:Resource, lblLanguage %>" ID="lblLanguage" runat="server"
                                AssociatedControlID="ddlLanguage"></asp:Label>
                        </td>
                        <td class="input">
                            <asp:DropDownList ID="ddlLanguage" runat="server" Width="135px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="text">
                            <asp:Label Text="<%$Resources:Resource, lblWarehouse %>" ID="lblWarehouse" runat="server"
                                AssociatedControlID="dlWarehouse"></asp:Label>
                        </td>
                        <td class="input">
                            <asp:DropDownList ID="dlWarehouse" runat="server" Width="135px">
                            </asp:DropDownList>
                            <span class="style1">*</span>
                            <asp:CustomValidator ID="custvalPOWarehouse" runat="server" SetFocusOnError="true"
                                ErrorMessage="<%$ Resources:Resource, custvalPOWarehouse%>" ClientValidationFunction="funCheckWarehouseCode"
                                Display="None">
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="text">
                            <asp:Label Text="<%$Resources:Resource, lblGridHeight %>" ID="lblGridHeight" runat="server"></asp:Label>
                        </td>
                        <td class="input">
                            <asp:TextBox ID="txtGridHeight" runat="server" Width="120px" CssClass="numericTextField"
                                MaxLength="4"></asp:TextBox><span class="style1">px</span>
                        </td>
                    </tr>
                </table>
  <%--              <h3>
                    <%= Resources.Resource.lblUserCommission%>
                </h3>--%>
                <%--<asp:Label ID="Label2" runat="server"  Text="<%$ Resources:Resource, lblUserCommission%>"></asp:Label>--%>
                <div class="divMainContent" id="divOrderType" runat="server">
                    <asp:Panel runat="server" ID="pnlOrderType" Visible="false">
                        <table class="contentTableForm" border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td class="text">
                                    <asp:Label Text="<%$Resources:Resource, lblUserOrderType %>" ID="lblUserOrderType"
                                        runat="server"></asp:Label>
                                </td>
                                <td class="input">
                                    <asp:DropDownList ID="dlstOderType" runat="server" Width="160px">
                                    </asp:DropDownList>
                                    <span class="style1">*</span>
                                    <asp:CustomValidator ID="custvalOderType" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, custvalOderType%>"
                                        ClientValidationFunction="funCheckOderType" ValidationGroup="OderType" Display="None">
                                    </asp:CustomValidator>
                                </td>
                                <td rowspan="3" valign="top" align="right">
                                    <asp:Label ID="lblOTypeMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                                    <asp:GridView CssClass="mGrid" GridLines="None" ID="grdOrderType" runat="server" Visible="false"
                                        AllowSorting="false" DataSourceID="sqldsOrderType" AllowPaging="True" PageSize="15"
                                        PagerSettings-Mode="Numeric" AutoGenerateColumns="False" UseAccessibleHeader="False"
                                        DataKeyNames="orderTypeDtlID" OnPageIndexChanging="grdOrderType_PageIndexChanging"
                                        OnRowDataBound="grdOrderType_RowDataBound" OnRowDeleting="grdOrderType_RowDeleting"
                                        OnSorting="grdOrderType_Sorting">
                                        <Columns>
                                            <asp:BoundField DataField="orderTypeDesc" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grduserOderType %>"
                                                ReadOnly="True" SortExpression="orderTypeDesc">
                                                <ItemStyle Width="230px" Wrap="true" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ordercommission" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grduserCommission %>"
                                                ReadOnly="True" SortExpression="ordercommission">
                                                <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" HeaderStyle-ForeColor="#ffffff"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                                        CommandArgument='<%# Eval("orderTypeDtlID") %>' CommandName="Delete" CausesValidation="false" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle CssClass="grid_footer" />
                                        <RowStyle CssClass="grid_rowstyle" />
                                        <HeaderStyle CssClass="grid_header" />
                                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                        <PagerSettings PageButtonCount="20" />
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="sqldsOrderType" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                                        ProviderName="MySql.Data.MySqlClient"></asp:SqlDataSource>
                                    <asp:GridView CssClass="mGrid" GridLines="None" ID="grdOderTypeInsert" runat="server"
                                        AllowSorting="false" AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric"
                                        AutoGenerateColumns="False" UseAccessibleHeader="False" DataKeyNames="OrderTypeID"
                                        OnPageIndexChanging="grdOderTypeInsert_PageIndexChanging" OnRowDataBound="grdOderTypeInsert_RowDataBound"
                                        OnRowDeleting="grdOderTypeInsert_RowDeleting" OnSorting="grdOderTypeInsert_Sorting">
                                        <Columns>
                                            <asp:BoundField DataField="OrderTypeID" HeaderText="<%$ Resources:Resource, grduserOderType %>"
                                                ReadOnly="True" SortExpression="OrderTypeID" Visible="false">
                                                <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="OrderTypeDesc" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grduserOderType %>"
                                                ReadOnly="True" SortExpression="OrderTypeDesc">
                                                <ItemStyle Width="230px" Wrap="true" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Commission" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grduserCommission %>"
                                                ReadOnly="True" SortExpression="Commission">
                                                <ItemStyle Width="180px" Wrap="true" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" HeaderStyle-ForeColor="#ffffff"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                                        CommandArgument='<%# Eval("OrderTypeID") %>' CommandName="Delete" CausesValidation="false" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle CssClass="grid_footer" />
                                        <RowStyle CssClass="grid_rowstyle" />
                                        <HeaderStyle CssClass="grid_header" />
                                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                        <PagerSettings PageButtonCount="20" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td class="text">
                                    <asp:Label Text="<%$Resources:Resource, lblUserCommission %>" ID="lblUserCommission"
                                        runat="server"></asp:Label>
                                </td>
                                <td class="input">
                                    <asp:TextBox ID="txtCommission" runat="server" Width="120px" MaxLength="5"></asp:TextBox>
                                    % <span class="style1">*</span>
                                    <asp:CompareValidator ID="compvalCommission" EnableClientScript="true" SetFocusOnError="true"
                                        ValidationGroup="OderType" ControlToValidate="txtCommission" Operator="DataTypeCheck"
                                        Type="Double" Display="None" ErrorMessage="<%$ Resources:Resource, compvalCommission %>"
                                        runat="server" />
                                    <asp:RequiredFieldValidator ID="reqValCommission" ErrorMessage="<%$ Resources:Resource, reqValCommission%>"
                                        runat="server" ControlToValidate="txtCommission" ValidationGroup="OderType" SetFocusOnError="true"
                                        Display="None"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="text">
                                </td>
                                <td class="input">
                                    <asp:Button ID="imgAdd" ValidationGroup="OderType" runat="server" ToolTip="Add" Text="<%$ Resources:Resource, lblAssign%>"
                                        ImageUrl="../images/add_2.png" OnClick="imgAdd_Click" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnlRole" runat="server" Visible="false" >
            <div class="divMainContainerTitle">
                <h3>
                    <%= Resources.Resource.lblAssociateModules%>
                </h3>
            </div>
            <asp:Panel CssClass="divMainContent" ID="pnlModules" runat="server">
                <div class="accordionInbiz">
                    <div class="accordion">
                        <asp:Repeater ID="rptModules" runat="server" DataSourceID="sdsModules" OnItemDataBound="rptModules_ItemDataBound">
                            <ItemTemplate>
                                <h3>
                                    <a href="javascript:void(0);">
                                        <input class="m_check" id="chkModule" type="checkbox" value='<%#Eval("ModuleID")%>'
                                            runat="server" sub_items='<%#"div_"+Container.ItemIndex.ToString() %>' />
                                        <asp:Label ID="lblUserType" Text='<%#Eval("ModuleDisplayName")%>' runat="server" /></a></h3>
                                <div id='<%#"div_"+Container.ItemIndex.ToString() %>'>
                                    <asp:DataList ID="dlstRoles" RepeatColumns="3" RepeatDirection="Horizontal" Width="100%"
                                        OnItemDataBound="dlstRoles_ItemDataBound" runat="server">
                                        <ItemStyle Width="200px" />
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <input id="chkRole" class="chk_role_key" type="checkbox" value='<%#Eval("RoleID")%>'
                                                            runat="server" disabled="disabled" onclick="setSelectedRoles()" />
                                                        <asp:HiddenField ID="hdnRoleID" runat="server" Value='<%#Eval("RoleID")%>' />
                                                    </td>
                                                    <td style="width: 200px;">
                                                        <asp:Label ID="lblRoleName" Text='<%#Eval("RoleDesc")%>' runat="server" />
                                                        <asp:HiddenField ID="hdnOldKey" runat="server" Value='<%#Eval("OldSecurityKey")%>' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <asp:SqlDataSource ID="sdsModules" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient"></asp:SqlDataSource>
                <div style="display: none">
                    <asp:TextBox Width="240px" ID="txtUserModules" runat="server"></asp:TextBox>
                </div>
                <script language="javascript" type="text/javascript">
                    function MakeEnableModules(cbControl, state) {
                        var chkBoxList = document.getElementById(cbControl);
                        var chkBoxCount = chkBoxList.getElementsByTagName("input");
                        var ddl = chkBoxList.getElementsByTagName("select");
                        for (var i = 0; i < chkBoxCount.length; i++) {
                            chkBoxCount[i].checked = state;
                            chkBoxCount[i].disabled = !state;
                            ddl[i].disabled = !state;
                        }
                        //chkBoxList.checked = state;
                        //return false;
                    }

                    function SelectAllModules(chkUserType, chkItems, state) {
                        var chkUserType = document.getElementById(chkUserType);
                        var chkBoxList = document.getElementById(chkItems);
                        var chkBoxCount = chkBoxList.getElementsByTagName("input");
                        var ddl = chkBoxList.getElementsByTagName("select");
                        for (var i = 0; i < chkBoxCount.length; i++) {
                            chkBoxCount[i].checked = state;
                            chkBoxCount[i].disabled = !state;
                            ddl[i].disabled = !state;
                        }
                        chkUserType.checked = state;
                        //return false;
                    }

                    $(".m_check").click(function (evt) {
                        evt.stopPropagation();
                        $this = $(this);
                        $relContainer = $("#" + $this.attr("sub_items"));
                        $("input:checkbox", $relContainer).each(function () {
                            this.checked = $this.is(':checked');
                            this.disabled = !$this.is(':checked');
                        });
                        setSelectedRoles();
                    });

                    function setSelectedRoles() {
                        var roles = [];
                        $(".chk_role_key").each(function () {
                            if (this.checked) {
                                roles.push($(this).next().val());
                            }
                        });
                        $("#<%=hdnRoles.ClientID%>").val(roles.join(","));
                    }
                    $(document).ready(function () {
                        setSelectedRoles();
                    });           
                </script>
            </asp:Panel>
        </asp:Panel>
        <asp:HiddenField ID="hdnRoles" runat="server" />
        <div class="div_command">
            <asp:Button Text="<%$Resources:Resource, btnSave %>" ID="btnSave" runat="server"
                OnClick="btnSave_Click" />
            <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnReset" runat="server"
                CausesValidation="False" OnClick="btnReset_Click" />
        </div>
        <asp:ValidationSummary ID="valsumOrderType" ValidationGroup="OderType" runat="server"
            ShowMessageBox="true" ShowSummary="false" />
        <asp:ValidationSummary ID="valsAdmin" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $(function () {
            $(window).load(function () {
                $(':input:text:visible:enabled:first').focus();
            });
        })
        $(function () {
            $(".accordion").accordion();
        });
    </script>
    <script language="javascript" type="text/javascript">

        function funFillCtrl() {
            if (window.document.getElementById('<%=txtPassword.ClientID%>') != null) {
                window.document.getElementById('<%=txtPassword.ClientID%>').value = "******";
                window.document.getElementById('<%=txtConfirmPwd.ClientID%>').value = "******";
            }
        }


    </script>
    <script language="javascript" type="text/javascript">

        function funCheckWarehouseCode(source, args) {
            if (document.getElementById('<%=dlWarehouse.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }

        function funCheckOderType(source, args) {
            if (document.getElementById('<%=dlstOderType.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
    </script>
</asp:Content>

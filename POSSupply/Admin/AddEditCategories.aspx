﻿<%@ Page Title="" Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false" CodeFile="AddEditCategories.aspx.vb" Inherits="Admin_AddEditCategories" %>

<%@ Register src="../Controls/CommonSearch/CommonKeyword.ascx" tagname="CommonKeyword" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphLeftPanel" Runat="Server">
    <uc1:CommonKeyword ID="CommonKeyword1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <h2>
            Add/Edit Customer Category
        </h2>  
    </div>
    <div id="divMainContent" class="divMainContent">
        <table border="0" cellpadding="2" cellspacing="3" width="100%">
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                    <asp:HiddenField ID="hdnTextDescriptionID" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblCategoryNameEn"  CssClass="lblBold" runat="server" Text="<%$ Resources:Resource, lblCategoryNameEn %>"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtCategoryNameEn" MaxLength="45" runat="server" Width="330Px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCategName" runat="server" Display="Dynamic"  ControlToValidate="txtCategoryNameEn"
                        ErrorMessage="*" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblCategoryNameFr" CssClass="lblBold" runat="server" Text="<%$ Resources:Resource, lblCategoryNameFr %>"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtCategoryNameFr" MaxLength="45" runat="server" Width="330Px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblIsActive" CssClass="lblBold" runat="server" Text="<%$ Resources:Resource, Active %>"></asp:Label>
                </td>
                <td align="left">
                    <asp:CheckBox ID="chkIsActive" Checked="true" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="width:30%;">
                            </td>
                            <td style="width:150px;">
                                <div class="buttonwrapper">
                                    <a id="cmdSave" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                        style="min-width: 120px; text-align: center;">
                                        <%=Resources.Resource.cmdCssSave%></span></a></div>
                            </td>
                            <td align="left" style="width:150px;">
                                <div class="buttonwrapper">
                                    <a id="cmdReset" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                        <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%= Resources.Resource.Cancel%></span></a></div>
                            </td>
                            
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript" language="javascript">
        $("#divMainContainerTitle").corner();        
    </script>
</asp:Content>


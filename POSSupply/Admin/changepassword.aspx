﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="changepassword.aspx.cs" Inherits="Admin_changepassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div align="center">
        <asp:Label ID="lblError" ForeColor="Red" Font-Bold="true" Text="" runat="server" />
    </div>
    <table border="0" cellpadding="5" cellspacing="5" width="100%">
        <tr id="trCurrentPassword" runat="server">
            <td style="width:50%;">
                <asp:Label ID="lblCurrentPassword" Text="Current Password" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="txtCurrentPassword" TextMode="Password" MaxLength="25" runat="server" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="*" runat="server" ControlToValidate="txtCurrentPassword"
                    SetFocusOnError="true" Display="None" ></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="<%$Resources:Resource, lblPassword %>"  ID="lblPassword" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="txtPassword" TextMode="Password" MaxLength="25" runat="server" />
                <asp:RequiredFieldValidator ID="reqvalPassword" ErrorMessage="<%$ Resources:Resource, reqvalPassword%>" runat="server" ControlToValidate="txtPassword"
                    SetFocusOnError="true" Display="None" ></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="<%$Resources:Resource, lblConfirmPassword %>"  ID="lblConfirmPassword" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="txtConfirmPassword" TextMode="Password" MaxLength="25" runat="server" />
                <asp:CompareValidator ID="cmpvalPassword2" runat="server" ControlToCompare="txtPassword"
                    ControlToValidate="txtConfirmPassword" ErrorMessage="<%$ Resources:Resource, cmpvalPassword2%>"
                    SetFocusOnError="true" Display="None" ></asp:CompareValidator>
            </td>
        </tr>        
    </table>
    <asp:ValidationSummary ID="valsAdminWarehouse" runat="server" ShowMessageBox="true"
                ShowSummary="false" />
    <hr />
    <div style="text-align: right;">
        <asp:Button Text="<%$Resources:Resource, btnSave %>"  ID="btnSave" runat="server" OnClick="btnSave_Click" />
        <asp:Button Text="<%$Resources:Resource, btnCancel %>"  ID="btnCancel" runat="server" CausesValidation="false" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


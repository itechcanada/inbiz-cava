﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using iTECH.WebControls;


public partial class Handlers_async_message : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Response.ContentType = "text/html";
        Response.Clear();

        //iTECH.WebControls.MessageState.SetGlobalMessage(iTECH.WebControls.MessageType.Failure, "Test Failed Message!");

        if (MessageState.HasMessage)
        {
            StringBuilder sbControl = new StringBuilder();
            string messegeContainerClass = MessageState.Current.MessageType == (MessageType.Critical | MessageType.Failure) ? "ui-state-error" : "ui-state-highlight";
            if (MessageState.Current.MessageType == MessageType.Critical || MessageState.Current.MessageType == MessageType.Failure)
            {
                messegeContainerClass = "ui-state-error";
            }
            else if (MessageState.Current.MessageType == MessageType.Success)
            {
                messegeContainerClass = "ui-state-success";
            }
            else if (MessageState.Current.MessageType == MessageType.Information)
            {
                messegeContainerClass = "ui-state-highlight";
            }
            else
            {
                messegeContainerClass = "ui-state-error";
            }
            string messageClass = MessageState.Current.MessageType == (MessageType.Critical | MessageType.Failure) ? "ui-icon-alert" : "ui-icon-info";
            if (MessageState.Current.MessageType == MessageType.Failure)
            {
                messageClass = "ui-icon-alert";
            }
            else if (MessageState.Current.MessageType == MessageType.Critical)
            {
                messageClass = "ui-icon-circle-close";
            }
            else if (MessageState.Current.MessageType == MessageType.Success)
            {
                messageClass = "ui-icon-check";
            }
            else
            {
                messageClass = "ui-icon-alert";
                //messageClass = "ui-icon-info";
            }
            sbControl.Append(@"<div class=""error_theme""><div class=""ui-widget"" style=""margin-top:5px;"">")
                .AppendFormat(@"<div style=""padding: 0 .7em;"" class=""{0} ui-corner-all"">", messegeContainerClass)                
                .AppendFormat(@"<p><span style=""float: left; margin-right: .3em;"" class=""ui-icon {0}""></span>", messageClass)                
                .AppendFormat(@"<strong>{0}</strong> {1}", string.Empty, MessageState.Current.Message)                
                .Append(@"</p></div></div></div>");
            LiteralControl lt = new LiteralControl(sbControl.ToString());
            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                using (HtmlTextWriter textWriter = new HtmlTextWriter(sw))
                {
                    lt.RenderControl(textWriter);
                }
            }
            MessageState.Clear();
            Response.Write(sb);
        }
        else
        {
            Response.Write(string.Empty);
        }

        Response.Flush();
        Response.SuppressContent = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
}
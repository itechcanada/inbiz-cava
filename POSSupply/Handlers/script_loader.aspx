<%@ Page Language="C#" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.IO.Compression" %>
<%@ Import Namespace="System.Security.Cryptography" %>
<%@ Import Namespace="System.Text.RegularExpressions" %>
<%
/**
 * tiny_mce_gzip.aspx
 *
 * Copyright 2009, Moxiecode Systems AB
 * Released under LGPL License.
 *
 * License: http://tinymce.moxiecode.com/license
 * Contributing: http://tinymce.moxiecode.com/contributing
 *
 * This file compresses the TinyMCE JavaScript using GZip and
 * enables the browser to do two requests instead of one for each .js file.
 *
 * It's a good idea to use the diskcache option since it reduces the servers workload.
 */
    
	string cacheKey = "", cacheFile = "", content = "", enc, suffix, cachePath;
	bool diskCache, supportsGzip, isJS, compress;
	int i, x, expiresOffset;
    GZipStream gzipStream;
	Encoding encoding = Encoding.GetEncoding("windows-1252");
	byte[] buff;

	// Get input
	diskCache = GetParam("diskcache", "") == "true";
	isJS = GetParam("js", "") == "true";
	compress = GetParam("compress", "true") == "true";	
	suffix = GetParam("suffix", "") == "_src" ? "_src" : "";
	cachePath = Server.MapPath("."); // Cache path, this is where the .gz files will be stored
	expiresOffset = 10; // Cache for 10 days in browser cache

	// Set response headers
	Response.ContentType = "text/javascript";
	Response.Charset = "UTF-8";
	Response.Buffer = false;

	// Setup cache
	Response.Cache.SetExpires(DateTime.Now.AddDays(expiresOffset));
	Response.Cache.SetCacheability(HttpCacheability.Public);
	Response.Cache.SetValidUntilExpires(false);

	// Vary by all parameters and some headers
	Response.Cache.VaryByHeaders["Accept-Encoding"] = true;
	Response.Cache.VaryByParams["theme"] = true;
	Response.Cache.VaryByParams["language"] = true;
	Response.Cache.VaryByParams["plugins"] = true;
	Response.Cache.VaryByParams["lang"] = true;
	Response.Cache.VaryByParams["index"] = true;

    //Get Scripkeys to load 
    string[] arrScripts = Request.QueryString["load"].Split(',');    
    
	// Is called directly then auto init with default settings
	if (!isJS) {
        if (arrScripts.Contains<string>("jquery"))
        {
            Response.WriteFile(Server.MapPath("~/lib/scripts/jquery-1.5.1.min.js"));
            Response.WriteFile(Server.MapPath("~/lib/scripts/jquery-plugins/jquery.cookie.js"));      
        }
        if (arrScripts.Contains<string>("jqueryui"))
        {
            Response.WriteFile(Server.MapPath("~/lib/scripts/jquery-ui-1.8.11.custom.min.js"));
        }
        if (arrScripts.Contains<string>("json"))
        {
            Response.WriteFile(Server.MapPath("~/lib/scripts/json2.js"));
        }
        if (arrScripts.Contains<string>("jsload"))
        {
            Response.WriteFile(Server.MapPath("~/lib/scripts/jquery-plugins/jquery-load.js"));
        }
        if (arrScripts.Contains<string>("messagebox"))
        {
            Response.WriteFile(Server.MapPath("~/lib/scripts/jquery-plugins/jquery.messagebox.js"));
        }
        if (arrScripts.Contains<string>("jqgrid"))
        {
            Response.WriteFile(Server.MapPath("~/lib/scripts/jquery.jqGrid-4.0.0/js/i18n/grid.locale-en.js"));
            Response.WriteFile(Server.MapPath("~/lib/scripts/jquery.jqGrid-4.0.0/js/jquery.jqGrid.min.js"));
            Response.WriteFile(Server.MapPath("~/lib/scripts/jquery.jqGrid-4.0.0/plugins/grid.postext.js"));
            Response.WriteFile(Server.MapPath("~/lib/scripts/jquery.jqGrid-4.0.0/plugins/jquery.jqGrid.fluid.js"));
            Response.WriteFile(Server.MapPath("~/lib/scripts/jquery-plugins/jqgrid.helper.js"));
        }
        if (arrScripts.Contains<string>("framedialog"))
        {
            Response.WriteFile(Server.MapPath("~/lib/scripts/jquery-plugins/jquery-framedialog-1.1.2.js"));
        }
        if (arrScripts.Contains<string>("jqchosen"))
        {
            Response.WriteFile(Server.MapPath("~/lib/scripts/chosen.jquery/chosen.jquery.min.js"));
        }      
		
		Response.Write("//Script End Here");
		return;
	}

	// Setup cache info
	if (diskCache) {
		cacheKey = "";
        foreach (var item in Request.QueryString.AllKeys)
        {
            cacheKey += GetParam(item, "");
        }

		cacheKey = MD5(cacheKey);

		if (compress)
			cacheFile = cachePath + "/lib_" + cacheKey + ".gz";
		else
			cacheFile = cachePath + "/lib_" + cacheKey + ".js";
	}

	// Check if it supports gzip
	enc = Regex.Replace("" + Request.Headers["Accept-Encoding"], @"\s+", "").ToLower();
	supportsGzip = enc.IndexOf("gzip") != -1 || Request.Headers["---------------"] != null;
	enc = enc.IndexOf("x-gzip") != -1 ? "x-gzip" : "gzip";

	// Use cached file disk cache
	if (diskCache && supportsGzip && File.Exists(cacheFile)) {
		Response.AppendHeader("Content-Encoding", enc);
		Response.WriteFile(cacheFile);
		return;
	}

	// Generate GZIP'd content
	if (supportsGzip) {
		if (compress)
			Response.AppendHeader("Content-Encoding", enc);

		if (diskCache && cacheKey != "") {
			// Gzip compress
			if (compress) {
                gzipStream = new GZipStream(File.Create(cacheFile), CompressionMode.Compress, true);
				buff = encoding.GetBytes(content.ToCharArray());
				gzipStream.Write(buff, 0, buff.Length);
				gzipStream.Close();
			} else {
				StreamWriter sw = File.CreateText(cacheFile);
				sw.Write(content);
				sw.Close();
			}

			// Write to stream
			Response.WriteFile(cacheFile);
		} else {
            gzipStream = new GZipStream(Response.OutputStream, CompressionMode.Compress, true);
			buff = encoding.GetBytes(content.ToCharArray());
			gzipStream.Write(buff, 0, buff.Length);
			gzipStream.Close();
		}
	} else
		Response.Write(content);
%><script runat="server">
	public string GetParam(string name, string def) {
		string value = Request.QueryString[name] != null ? "" + Request.QueryString[name] : def;

		return Regex.Replace(value, @"[^0-9a-zA-Z\\-_,]+", "");
	}

	public string GetFileContents(string path) {
		try {
			string content;

			path = Server.MapPath(path);

			if (!File.Exists(path))
				return "";

			StreamReader sr = new StreamReader(path);
			content = sr.ReadToEnd();
			sr.Close();

			return content;
		} catch (Exception ex) {
			// Ignore any errors
		}

		return "";
	}

	public string MD5(string str) {
		MD5 md5 = new MD5CryptoServiceProvider();
		byte[] result = md5.ComputeHash(Encoding.ASCII.GetBytes(str));
		str = BitConverter.ToString(result);

		return str.Replace("-", "");
	}         
</script>
﻿<%@ WebHandler Language="C#" Class="AsyncSetMessageHandler" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Text;
using System.IO;
using System.Web.UI;
using iTECH.WebControls;
using iTECH.Library.Utilities;

public class AsyncSetMessageHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        try
        {
            MessageState.SetGlobalMessage(MessageType.Success, "");
            MessageState.SetGlobalMessage(MessageType.Information, BusinessUtility.GetString("Ram"));
            context.Response.Write("Hello World");
        }
        catch
        {
            context.Response.Write(404);
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}
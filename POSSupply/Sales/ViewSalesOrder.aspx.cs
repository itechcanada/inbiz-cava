﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.InbizERP.BusinessLogic;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;

public partial class Sales_ViewSalesOrder : BasePage
{
    private string strSOID = "";
    private Orders _objOrder = new Orders();
    int _currentRowIdx = -1;
    TotalSummary _lTotal;
    // On Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        //Security
        if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.SALES_MANAGER) && !CurrentUser.IsInRole(RoleID.SALES_REPRESENTATIVE))
        {
            Response.Redirect("~/Errors/AccessDenied.aspx");
        }

        //Set JqGrid variables
        //jqGridHelperScript.SetVariables(jgdvSalesOrderView.ClientID, "grid_wrapper", SearchPanel.ClientID, "btnSearch", 0);
        DropDownHelper.FillDropdown(ddlSearch, "SO", "dlSh1", Globals.CurrentAppLanguageCode, null);
        if (!IsPagePostBack(jgdvSalesOrderView))
        {
            DropDownHelper.FillOrderType(rblstOrderType, Globals.CurrentAppLanguageCode, null);
            if (BusinessUtility.GetInt(Request.QueryString["ordType"]) > 0)
            {
                if (BusinessUtility.GetInt(Request.QueryString["ordType"]) == (int)OrderCommission.POS)
                {
                    rblstOrderType.SelectedValue = ((int)OrderCommission.POS).ToString();
                }
                else if (BusinessUtility.GetInt(Request.QueryString["ordType"]) == (int)OrderCommission.Shipment)
                {
                    rblstOrderType.SelectedValue = ((int)OrderCommission.Shipment).ToString();
                }
                else if (BusinessUtility.GetInt(Request.QueryString["ordType"]) == (int)OrderCommission.Work)
                {
                    rblstOrderType.SelectedValue = ((int)OrderCommission.Work).ToString();
                }
                else
                {
                    rblstOrderType.SelectedValue = ((int)OrderCommission.Transfer).ToString();
                }
                // mdSearch.Url = "mdSearchCustomer.aspx?ordType=T";
            }
            else
            {
                rblstOrderType.SelectedValue = ((int)OrderCommission.Sales).ToString();
            }
            btnBack.Visible = Request.QueryString.AllKeys.Contains("returnUrl");

            DropDownHelper.FillDropdown(ddlSOStatus, "SO", "SOSts", Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.liSelectOrderStatus, ""));

            for (int i = ddlSOStatus.Items.Count - 1; i >= 0; i--)
            {
                string sValue = ddlSOStatus.Items[i].Value;
                if ( sValue == "P" || sValue == "K")
                {
                    ddlSOStatus.Items.RemoveAt(i);
                }

                 if (sValue == "T")
                {
                    ddlSOStatus.Items.FindByValue(sValue).Text = Resources.Resource.lblInTransit;
                }
            }

            txtSearch.Focus();

            // Get User Grid Height
            InbizUser _usr = new InbizUser();
            _usr.PopulateObject(CurrentUser.UserID);
            if (BusinessUtility.GetInt(_usr.UserGridHeight) > 0)
            {
                jgdvSalesOrderView.Height = _usr.UserGridHeight;
            }
        }

        if (((BusinessUtility.GetString(Request.QueryString["InvokeSrc"]) == "POS") /*|| (BusinessUtility.GetString(Request.QueryString["BackFromOrder"]) == "1")*/) && this.WhsCode != "")
        {
            ddlSearch.SelectedValue = "WC";
            foreach (ListItem li in ddlSearch.Items)
            {
                if (li.Value == "WC")
                {
                    li.Selected = true;
                }
            }
            txtSearch.Text = this.WhsCode;

            jgdvSalesOrderView.Columns[13].Visible = true;

            ltrTitle.Text = Resources.Resource.lblView + ": " + Resources.Resource.lblTransfers;
        }
        else
        {
            ltrTitle.Text = Resources.Resource.lblView + ": " + Resources.Resource.lblSalesOrder;
        }

        if (!string.IsNullOrEmpty(this.WhsCode))
        {
            SysRegister sysRegMC = new SysRegister();
            sysRegMC.PopulateObject(this.WhsCode);
            hndIsPrintMerchantCopy.Value = BusinessUtility.GetInt(sysRegMC.SysRegPrintMerchantCopy).ToString();
        }
    }

    protected void jgdvSalesOrderView_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        Orders objOrdr = new Orders();
        objOrdr.PopulateObject(BusinessUtility.GetInt(e.RowValues[0]));
        string ordCurrencyCode = "";
        if (objOrdr != null)
        {
            ordCurrencyCode = objOrdr.OrdCurrencyCode;
        }

        if (_currentRowIdx != e.RowIndex)
        {
            _currentRowIdx = e.RowIndex;
            _lTotal = CalculationHelper.GetOrderTotal(BusinessUtility.GetInt(e.RowValues[0]));
        }

        if (e.ColumnIndex == 5)
        {
            //e.CellHtml = CurrencyFormat.GetReplaceCurrencySymbol( string.Format("{0:F}",BusinessUtility.GetCurrencyString( _lTotal.GrandTotal,Globals.CurrentCultureName)));
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(_lTotal.GrandTotal), ordCurrencyCode);// (string.Format("{0:F}", BusinessUtility.GetCurrencyString(_lTotal.GrandTotal, Globals.CurrentCultureName)));
        }
        else if (e.ColumnIndex == 6)
        {
            //To to get Paid Amout                                               
            //e.CellHtml =CurrencyFormat.GetReplaceCurrencySymbol( string.Format("{0:F}", BusinessUtility.GetCurrencyString(_lTotal.TotalAmountReceived,Globals.CurrentCultureName)));           
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(_lTotal.TotalAmountReceived), ordCurrencyCode);
        }
        else if (e.ColumnIndex == 7)
        {
            //e.CellHtml =CurrencyFormat.GetReplaceCurrencySymbol( string.Format("{0:F}", BusinessUtility.GetCurrencyString(_lTotal.OutstandingAmount,Globals.CurrentCultureName)));                     
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(_lTotal.OutstandingAmount), ordCurrencyCode);
        }
        else if (e.ColumnIndex == 10)
        {
            string strData = "";
            switch (e.CellHtml.ToString())
            {
                case SOStatus.NEW:
                    strData = Resources.Resource.liCreated;
                    break;
                case SOStatus.APPROVED:
                    strData = Resources.Resource.liApproved;
                    break;
                case SOStatus.IN_PROGRESS:
                    strData = Resources.Resource.liInProcess;
                    break;
                case SOStatus.HELD:
                    strData = Resources.Resource.liHeld;
                    break;
                case SOStatus.SHIPPED:
                    strData = Resources.Resource.liShipped;
                    break;
                case SOStatus.SHIPPING_HELD:
                    strData = Resources.Resource.liShippingHeld;
                    break;
                case SOStatus.INVOICED:
                    strData = Resources.Resource.liInvoiced;
                    break;
                case SOStatus.CLOSED:
                    //if (objOrdr.OrderTypeCommission == (int)OrderCommission.Transfer)
                    //{
                    //    strData = Resources.Resource.lblTransferClosed;
                    //}
                    //else
                    //{
                        strData = Resources.Resource.liClosed;
                    //}
                    break;
                case SOStatus.CLOSED_INVOICED:
                    strData = Resources.Resource.liClosedInvoiced;
                    break;
                case SOStatus.READY_FOR_SHIPPING:
                    strData = Resources.Resource.liReadyForShipping;
                    break;
                case SOStatus.TRANSFERRED:
                    if (objOrdr.OrderTypeCommission == (int)OrderCommission.Transfer)
                    {
                        strData = Resources.Resource.lblInTransit;
                    }
                    else
                    {
                        strData = Resources.Resource.lblTransferred;
                    }
                    break;
                case SOStatus.RESERVATION_CANCELED:
                    strData = "Reservation Canceled";
                    break;
            }
            e.CellHtml = strData;
        }
        else if (e.ColumnIndex == 12)
        {
            string strurl = "ViewOrderDetails.aspx?SOID=" + e.RowValues[0];
            if (BusinessUtility.GetInt(Request.QueryString["ordType"]) == 5 && BusinessUtility.GetString(Request.QueryString["InvokeSrc"]) == "POS")
            {
                strurl = "../POS/POS.aspx?laywayOrderID=" + e.RowValues[0];
            }
            else if (BusinessUtility.GetInt(Request.QueryString["ordType"]) == 2 && BusinessUtility.GetString(Request.QueryString["InvokeSrc"]) == "POS")
            {
                strurl += "&ordType=2&InvokeSrc=POS&whsCode=" + Request.QueryString["whsCode"];  //whsCode=OGL
            }
            else
            {
                strurl += "&ordType=" + BusinessUtility.GetString(objOrdr.OrderTypeCommission) + "";
            }
            e.CellHtml = string.Format("<a href=\"{0}\">{1}</a>", strurl, Resources.Resource.lblEdit);
        }

        else if (e.ColumnIndex == 13)
        {
            //string strurl = "ViewOrderDetails.aspx?SOID=" + e.RowValues[0];
            //if (BusinessUtility.GetInt(Request.QueryString["ordType"]) == 5 && BusinessUtility.GetString(Request.QueryString["InvokeSrc"]) == "POS")
            //{
            //    strurl = "../POS/POS.aspx?laywayOrderID=" + e.RowValues[0];
            //}
            e.CellHtml = string.Format("<a  onclick='printReceipt({0})'  href=\"#\">{1}</a>", e.CellHtml, Resources.Resource.grdPrint);
        }

    }

    protected void jgdvSalesOrderView_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        bool isSalesRescricted = CurrentUser.IsInRole(RoleID.SALES_REPRESENTATIVE) && BusinessUtility.GetBool(Session["SalesRepRestricted"]);
        int oType = 0;
        string sSelectedValue = "";

        if (!int.TryParse(Request.QueryString["oType"], out oType))
        {
            oType = (int)OrderCommission.Sales;
            if (BusinessUtility.GetInt(Request.QueryString["ordType"]) == 5)
            {
                oType = (int)OrderCommission.POS;
                if ((BusinessUtility.GetString(Request.QueryString["InvokeSrc"]) == "POS") && this.WhsCode == "")
                {
                    ddlSOStatus.SelectedValue = "2";
                    sSelectedValue = "A";
                }
                else if ((BusinessUtility.GetString(Request.QueryString["InvokeSrc"]) == "POS") && this.WhsCode != "")
                {
                    ddlSearch.SelectedValue = "WC";

                    foreach (ListItem li in ddlSearch.Items)
                    {
                        if (li.Value == "WC")
                        {
                            li.Selected = true;
                        }
                    }

                    txtSearch.Text = this.WhsCode;
                }
            }
            else if (BusinessUtility.GetInt(Request.QueryString["ordType"]) == 8)
            {
                oType = (int)OrderCommission.Shipment;
            }
            else if (BusinessUtility.GetInt(Request.QueryString["ordType"]) == (int)OrderCommission.Work)
            {
                oType = (int)OrderCommission.Work;
            }
            else
            {
                if (BusinessUtility.GetInt(Request.QueryString["ordType"]) > 0)
                {
                    oType = (int)OrderCommission.Transfer;
                    //mdSearch.Url += "?oType=" + oType;
                }
            }
        }
        else
        {
            //oType = (int)OrderCommission.Sales;
        }
        //if (oType == 2)
        //{
        //    //mdSearch.Url += "?oType=" + oType;
        //}

        if (Request.QueryString.AllKeys.Contains("_history"))
        {
            if (sSelectedValue != "")
            {
                sqldsSalesOrderView.SelectCommand = _objOrder.GetSalesSql(sqldsSalesOrderView.SelectParameters, Request.QueryString[ddlSearch.ClientID], sSelectedValue, Request.QueryString[txtSearch.ClientID], this.PartnerID, CurrentUser.UserID, oType, isSalesRescricted);
            }
            else
            {
                sqldsSalesOrderView.SelectCommand = _objOrder.GetSalesSql(sqldsSalesOrderView.SelectParameters, Request.QueryString[ddlSearch.ClientID], Request.QueryString[ddlSOStatus.ClientID], Request.QueryString[txtSearch.ClientID], this.PartnerID, CurrentUser.UserID, oType, isSalesRescricted);
            }
        }
        else
        {
            if (sSelectedValue != "")
            {
                sqldsSalesOrderView.SelectCommand = _objOrder.GetSalesSql(sqldsSalesOrderView.SelectParameters, ddlSearch.SelectedValue, sSelectedValue, txtSearch.Text, this.PartnerID, CurrentUser.UserID, oType, isSalesRescricted);
            }
            else
            {
                sqldsSalesOrderView.SelectCommand = _objOrder.GetSalesSql(sqldsSalesOrderView.SelectParameters, ddlSearch.SelectedValue, ddlSOStatus.SelectedValue, txtSearch.Text, this.PartnerID, CurrentUser.UserID, oType, isSalesRescricted);
            }
        }
    }

    private int PartnerID
    {
        get
        {
            int partnerId = 0;
            int.TryParse(Request.QueryString["PartnerID"], out partnerId);
            return partnerId;
        }
    }

    private string WhsCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["whscode"]);
        }
    }

    protected void imgCsv_Click(object sender, EventArgs e)
    {
        jgdvSalesOrderView.ExportToExcel("export.xls");
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.QueryString["returnUrl"]);
    }

    protected void jgdvSalesOrderView_PreRender(object sender, EventArgs e)
    {

    }
}
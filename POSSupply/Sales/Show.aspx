<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="Show.aspx.vb" Inherits="Sales_Show" %>

<asp:Content ID="cntShowSO" ContentPlaceHolderID="cphMaster" runat="Server">
    <br />
    <br />
    <div align="center" onkeypress="return disableEnterKey(event)">
        <br />
        <br />
        <asp:Label ID="lblMsg" runat="server" CssClass="lblBold"></asp:Label>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <img alt="" runat="server" id="imgFax" visible="false" src="~/Images/fax.png" width="48"
            height="48" />
        &nbsp;&nbsp;&nbsp;
        <img alt="" runat="server" id="imgMail" visible="false" src="~/Images/email.jpg"
            width="48" height="48" />
        &nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="hlnkTemplate" runat="server" Visible="false" Target="_blank" Text="">
            <img id="imgPdf" runat="server" alt="" src="../Images/PDF.jpg" border="0" />
        </asp:HyperLink>
        <table width="100%">
            <tr>
                <td height="20" align="center" width="40%">
                </td>
                <td width="60%">
                    <div class="buttonwrapper">
                        <a id="cmdBack" runat="server" visible="false" class="ovalbutton" href="#"><span
                            class="ovalbutton" style="min-width: 120px; text-align: center;">
                            <%=Resources.Resource.cmdCssBack%></span></a></div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;
using Trirand.Web.UI.WebControls;

public partial class Sales_mdSearchProduct : BasePage
{
    private Orders _ord = new Orders();

    protected string sPHtml = "";
    protected static string sCurrencyBaseCode = "";


    protected void Page_Load(object sender, System.EventArgs e)
    {
        MessageState.SetGlobalMessage(MessageType.Success, "");
        if (!IsPagePostBack(grdProducts))
        {
            DateTime dtS = DateTime.Now;
            FillItemDetails();
            string strData = string.Empty;
            SysCompanyInfo ci = new SysCompanyInfo();
            ci.PopulateObject(CurrentUser.DefaultCompanyID);
            sCurrencyBaseCode = ci.CompanyBasCur;
            DateTime dtE = DateTime.Now;
        }
    }

    private void FillItemDetails()
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            cmdAddProduct.OnClientClick = string.Format("parent.location.href='{0}'; return false;", ResolveUrl("~/Inventory/Product.aspx?ptype=1"));
            SysTaxCodeDesc tg = new SysTaxCodeDesc();
            SysWarehouses wh = new SysWarehouses();
            tg.FillListControl(dbHelp, dlTaxCode, new ListItem(Resources.Resource.liTaxGroup, "0"));
            wh.FillWharehouse(dbHelp, dlWarehouse, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));

            ListItem liWhs = dlWarehouse.Items.FindByValue(this.WarehouseCode);
            foreach (ListItem liItem in dlWarehouse.Items)
            {
                if (liItem.Selected == true)
                    liItem.Selected = false;
            }
            if (liWhs != null)
            {
                liWhs.Selected = true;
            }
            else
            {
                liWhs = dlWarehouse.Items.FindByValue(CurrentUser.UserDefaultWarehouse);
                if (liWhs != null)
                {
                    liWhs.Selected = true;
                }
            }

            Partners part = new Partners();
            part.PopulateObject(dbHelp, SalesCartHelper.CurrentCart.OrderCustomerID);
            ListItem liTax = null;

            if (part.PartnerID > 0)
            {
                liTax = dlTaxCode.Items.FindByValue(part.PartnerTaxCode.ToString());
                txtDiscount.Text = BusinessUtility.GetString(part.PartnerDiscount);
            }
            else
            {
                SysCompanyInfo cinFo = new SysCompanyInfo();
                CountryStateTaxGroup state = new CountryStateTaxGroup();
                cinFo.PopulateObject(CurrentUser.DefaultCompanyID, dbHelp);
                state.PopulateObject(dbHelp, cinFo.CompanyCountry, cinFo.CompanyState);
                liTax = dlTaxCode.Items.FindByValue(state.StateGroupTaxCode.ToString());
            }

            if (liTax != null)
            {
                liTax.Selected = true;
            }
            if (this.ItemID > 0)
            {
                pnlSearch.Visible = false;
                pnlEditProductQuantity.Visible = true;
                btnBackToList.Visible = false;

                SalesCartEntity cartItem = SalesCartHelper.GetItem(this.ItemID);
                if (cartItem != null)
                {
                    txtProductId.Text = cartItem.ProductID.ToString();
                    txtDiscount.Text = string.Format("{0}", cartItem.Discount);
                    if (this.orderType == 2)
                    {
                        txtPrice.Text = "0.0";
                        txtPrice.Enabled = false;
                        dlWarehouse.Enabled = false;
                    }
                    else
                    {
                        txtPrice.Text = CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(cartItem.Price.ToString().Replace(",", ".")));
                    }

                    txtProductName.Text = string.Format("{0}", cartItem.ProductName);
                    txtQuantity.Text = string.Format("{0}", cartItem.Quantity);
                    txtUPCCode.Text = string.Format("{0}", cartItem.UPCCode);
                    dlTaxCode.SelectedValue = cartItem.TaxGrp.ToString();
                    dlWarehouse.SelectedValue = cartItem.WarehouseCode;
                    rblstDiscountType.SelectedValue = cartItem.DisType;
                }

                txtQuantity.Focus();
            }
            else
            {
                txtSearch.Focus();
            }
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    protected void grdProducts_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains("_history"))
        {
            SearchBy = Request.QueryString[dlSearch.ClientID];
            SearchText = Request.QueryString[txtSearch.ClientID];
            SearchCollectionBy = Request.QueryString["_Collection"];
            if (SearchBy != "QS")
            {
                if (SearchCollectionBy == "")
                {
                    if (SearchBy == "ST")
                    {
                        Collection objCollection = new Collection();
                        var t = objCollection.GetStyleCollection(null, SearchText, Globals.CurrentAppLanguageCode);

                        if (t.Count > 0)
                        {
                            if (t != null)
                            {
                                SearchCollectionBy = BusinessUtility.GetString(t[0].CollectionID);
                            }
                        }
                    }
                }
                Product prd = new Product();
                DataTable dt = prd.GetProductsForQuoation(null, SearchBy, SearchText, this.CompanyID, this.ProductType, Globals.CurrentAppLanguageCode, BusinessUtility.GetInt(SearchCollectionBy));
                grdProducts.DataSource = dt;
                if (dt.Rows.Count > 0)
                {
                    btnSelectedStyle.Text = btnSelectedStyle.Text + " " + BusinessUtility.GetString(dt.Rows[0]["Style"]);
                }
            }
        }
        else
        {
            if (SearchBy != "QS")
            {
                Product prd = new Product();
                DataTable dt = prd.GetProductsForQuoation(null, SearchBy, SearchText, this.CompanyID, this.ProductType, Globals.CurrentAppLanguageCode, BusinessUtility.GetInt(SearchCollectionBy));
                grdProducts.DataSource = dt;
                if (dt.Rows.Count > 0)
                {
                    btnSelectedStyle.Text = btnSelectedStyle.Text + " " + BusinessUtility.GetString(dt.Rows[0]["Style"]);
                }
            }

        }
    }

    private int ItemID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["editItemID"], out id);
            return id;
        }
    }

    private int CompanyID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["ComID"], out id);
            return id;
        }
    }
    public int orderType
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["orderType"]);
        }
    }
    private int CustomerID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["custID"], out id);
            return id;
        }
    }

    public string SearchBy
    {
        get { return ((Session["SearchBy"] != null) ? Convert.ToString(Session["SearchBy"]) : string.Empty); }
        set { Session["SearchBy"] = value; }
    }


    public string SearchCollectionBy
    {
        get { return ((Session["SearchCollectionBy"] != null) ? Convert.ToString(Session["SearchCollectionBy"]) : string.Empty); }
        set { Session["SearchCollectionBy"] = value; }
    }

    public string SearchText
    {
        get { return ((Session["SearchText"] != null) ? Session["SearchText"].ToString() : string.Empty); }
        set { Session["SearchText"] = value; }
    }

    private StatusProductType ProductType
    {
        get
        {
            return QueryStringHelper.GetProductType("pType");
        }
    }

    protected void grdProducts_RowSelecting(object sender, Trirand.Web.UI.WebControls.JQGridRowSelectEventArgs e)
    {
        hdnProcessProductID.Value = e.RowKey;
        Product prd = new Product();
        DataRow drProduct = prd.GetProductDetailForQuoation(null, BusinessUtility.GetInt(e.RowKey), this.ProductType, Globals.CurrentAppLanguageCode);

        if (drProduct != null)
        {
            txtProductId.Text = Convert.ToString(drProduct["productID"]);
            txtUPCCode.Text = Convert.ToString(drProduct["prdUPCCode"]);
            txtProductName.Text = Convert.ToString(drProduct["prdName"]);
            if (this.orderType == 2)
            {
                txtPrice.Text = "0.0";
                txtPrice.Enabled = false;
                dlWarehouse.Enabled = false;
            }
            else
            {
                txtPrice.Text = CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(drProduct["Price"].ToString().Replace(",", ".")));
            }

            int txGrp = BusinessUtility.GetInt(drProduct["prdTaxCode"]);
            if (txGrp > 0)
            {
                dlTaxCode.SelectedValue = txGrp.ToString();
            }

            //ListItem liWhs = dlWarehouse.Items.FindByValue(Convert.ToString(drProduct["prdWhsCode"]));
            //foreach (ListItem liItem in dlWarehouse.Items)
            //{
            //    if (liItem.Selected == true)
            //        liItem.Selected = false;
            //}
            //if (liWhs != null)
            //{

            //    liWhs.Selected = true;
            //}
            //else
            //{
            //    liWhs = dlWarehouse.Items.FindByValue(CurrentUser.UserDefaultWarehouse);
            //    if (liWhs != null)
            //    {
            //        liWhs.Selected = true;
            //    }
            //}

            ListItem liWhs = dlWarehouse.Items.FindByValue(this.WarehouseCode);
            foreach (ListItem liItem in dlWarehouse.Items)
            {
                if (liItem.Selected == true)
                    liItem.Selected = false;
            }
            if (liWhs != null)
            {
                liWhs.Selected = true;
            }
            else
            {
                liWhs = dlWarehouse.Items.FindByValue(CurrentUser.UserDefaultWarehouse);
                if (liWhs != null)
                {
                    liWhs.Selected = true;
                }
            }

            txtQuantity.Text = "1";
            hdnOnHeadQty.Value = Convert.ToString(drProduct["prdOhdQty"]);

            pnlSearch.Visible = false;
            pnlEditProductQuantity.Visible = true;
        }

        txtQuantity.Focus();
    }



    protected void btnQuickSearch_OnClick(object sender, System.EventArgs e)
    {
        //Product objProduct = new Product();
        //int productID = objProduct.GetProductID(null, txtSearch.Text);
        //if (productID > 0)
        //{
        //hdnProcessProductID.Value = BusinessUtility.GetString(productID);
        Product prd = new Product();
        //DataRow drProduct = prd.GetProductDetailForQuoation(null, BusinessUtility.GetInt(productID), this.ProductType, Globals.CurrentAppLanguageCode);
        DataRow drProduct = prd.GetProductDetailBySKU(null, BusinessUtility.GetString(txtSearch.Text), this.ProductType, Globals.CurrentAppLanguageCode);
        if (drProduct != null)
        {
            txtProductId.Text = Convert.ToString(drProduct["productID"]);
            txtUPCCode.Text = Convert.ToString(drProduct["prdUPCCode"]);
            txtProductName.Text = Convert.ToString(drProduct["prdName"]);
            if (this.orderType == 2)
            {
                txtPrice.Text = "0.0";
                txtPrice.Enabled = false;
                dlWarehouse.Enabled = false;
            }
            else
            {
                txtPrice.Text = CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(drProduct["Price"].ToString().Replace(",", ".")));
            }

            int txGrp = BusinessUtility.GetInt(drProduct["prdTaxCode"]);
            if (txGrp > 0)
            {
                dlTaxCode.SelectedValue = txGrp.ToString();
            }

            ListItem liWhs = dlWarehouse.Items.FindByValue(this.WarehouseCode);
            foreach (ListItem liItem in dlWarehouse.Items)
            {
                if (liItem.Selected == true)
                    liItem.Selected = false;
            }
            if (liWhs != null)
            {
                liWhs.Selected = true;
            }
            else
            {
                liWhs = dlWarehouse.Items.FindByValue(CurrentUser.UserDefaultWarehouse);
                if (liWhs != null)
                {
                    liWhs.Selected = true;
                }
            }

            txtQuantity.Text = "1";
            hdnOnHeadQty.Value = Convert.ToString(drProduct["prdOhdQty"]);

            SalesCartEntity item = SalesCartHelper.GetItem(this.ItemID);
            if (item == null)
            {
                item = new SalesCartEntity();
            }
            this.SetData(item);
            if (this.ItemID > 0)
            {
                SalesCartHelper.EditCartItem(item);
                Globals.RegisterCloseDialogScript(Page, "parent.reloadGridFromPrdPage(); ", true);
                
                
            }
            else
            {
                lblMsg.Text = "";
                SalesCartHelper.AddToCart(item, true);
                Globals.RegisterScript(Page, "parent.reloadGridFromPrdPage(); ");
            }
            txtSearch.Text = "";
            txtSearch.Focus();
        }
    //}
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblInvalidSKU);
            txtSearch.Focus();
        }

    }

    public void SetData(SalesCartEntity item)
    {
        item.ID = this.ItemID;
        item.Discount = BusinessUtility.GetInt(txtDiscount.Text);
        item.DisType = rblstDiscountType.SelectedValue;
        item.IsCanceled = false;
        item.Price = BusinessUtility.GetDouble(txtPrice.Text);
        item.ProductID = BusinessUtility.GetInt(txtProductId.Text);
        item.ProductName = txtProductName.Text;
        item.Quantity = BusinessUtility.GetDouble(txtQuantity.Text);
        item.RangePrice = 0.0D;
        item.TaxGrp = BusinessUtility.GetInt(dlTaxCode.SelectedValue);
        item.TaxGrpDesc = dlTaxCode.SelectedIndex > 0 ? dlTaxCode.SelectedItem.Text : string.Empty;
        item.UPCCode = txtUPCCode.Text;
        item.WarehouseCode = dlWarehouse.SelectedValue;
        Product objProduct = new Product();
        DataTable dtProduct = objProduct.GetProductColorSize(null, BusinessUtility.GetInt(txtProductId.Text));
        foreach (DataRow drItem in dtProduct.Rows)
        {
            item.PrdColor = BusinessUtility.GetString(drItem["Color"]);
            item.PrdSize = BusinessUtility.GetString(drItem["Size"]);
        }
    }

    protected void imgAdd_Click(object sender, System.EventArgs e)
    {
        if (IsValid)
        {
            SalesCartEntity item = SalesCartHelper.GetItem(this.ItemID);
            if (item == null)
            {
                item = new SalesCartEntity();
            }
            this.SetData(item);
            if (this.ItemID > 0)
            {
                SalesCartHelper.EditCartItem(item);
                Globals.RegisterCloseDialogScript(Page, "parent.reloadGrid(); ", true);
            }
            else
            {
                lblMsg.Text = "";
                if (BusinessUtility.GetDouble(txtQuantity.Text) <= 0)
                {
                    lblMsg.Text = Resources.Resource.msgPOItemQuantity;
                    txtQuantity.Focus();
                    return;
                }
                if (dlWarehouse.SelectedValue == "0")
                {
                    lblMsg.Text = Resources.Resource.custvalPOWarehouse;
                    dlWarehouse.Focus();
                    return;
                }
                SalesCartHelper.AddToCart(item, true);

                pnlSearch.Visible = true;
                pnlEditProductQuantity.Visible = false;

                Globals.RegisterScript(Page, "parent.reloadGrid(); ");

                txtSearch.Focus();
            }
        }
    }

    protected void grdProducts_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 7)
        {
            e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
        }
    }

    protected void btnBackToList_Click(object sender, EventArgs e)
    {
        pnlSearch.Visible = true;
        pnlEditProductQuantity.Visible = false;
    }

    public string WarehouseCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["whsID"]);
        }
    }

    protected void btnSelectedStyle_Onclick(object sender, EventArgs e)
    {

        imgAdd.Visible = false;
        liBarCode.Visible = false;
        liPrice.Visible = false;
        liDiscount.Visible = false;
        btnAddProduct.Visible = true;
        txtProductName.Text = Convert.ToString(hdnPname.Value);
        Product prd = new Product();

        string sStyle = "";
        sStyle = hdnStyle.Value;
        if (dlSearch.SelectedValue == "ST" && sStyle == "")
        {
            sStyle = txtSearch.Text;
        }

        DataTable dt = prd.GetProductDetailStyle(null, BusinessUtility.GetString(sStyle), this.ProductType, Globals.CurrentAppLanguageCode, BusinessUtility.GetInt(this.SearchCollectionBy), "");
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                var result = from row in dt.AsEnumerable()
                             group row by row.Field<string>("Size") into grp
                             select new
                             {
                                 Size = grp.Key,
                                 MemberCount = grp.Count()
                             };

                string sHtmlHeading = "<tr> ";
                sHtmlHeading += "<td> </td>";
                string sHtmlSizeTotalContent = "";
                string sHtmlContent = "";
                int iItemSizeCount = 0;

                string sDisabled = "";
                if (this.orderType == 2)
                {
                    sDisabled = "disabled";
                }




                DataTable dtProductColor = prd.GetProductDetailGroupbyColor(null, BusinessUtility.GetString(hdnStyle.Value), this.ProductType, Globals.CurrentAppLanguageCode, BusinessUtility.GetInt(this.SearchCollectionBy), "");


                foreach (var item in result)
                {
                    sHtmlHeading += "<td> <b> " + BusinessUtility.GetString(item.Size) + " </b> </td>";
                    iItemSizeCount = BusinessUtility.GetInt(item.MemberCount);
                    sHtmlSizeTotalContent += "<td style='vertical-align:top;'><span id=" + "sizetotal_" + BusinessUtility.GetString(item.Size).Replace("/", "_").Replace(" ", "_") + " type='text' style='width:100px;font-size:13px;font-weight:bold;'>" + dtProductColor.Rows.Count + "</span>  </td> ";
                }
                sHtmlHeading += "<td style='font-size:13px'><b> " + Resources.Resource.lblTotal + " </b> </td>";
                sHtmlHeading += "<td><b> Price </b> </td>";
                sHtmlHeading += "<td><b> Discount</b> </br> <input type='radio' checked='checked' name='discountType' " + sDisabled + " value='P'>Percentage  <input type='radio' name='discountType' " + sDisabled + " value='A'>Absolute </td>";
                sHtmlHeading += "</tr>";


                int i = 0;
                int sGrandTotal = 0;
                foreach (DataRow dRow in dtProductColor.Rows)
                {
                    string sClass = "cssPqty";
                    sHtmlContent += "<tr>";
                    sHtmlContent += "<td>Color  <b> " + BusinessUtility.GetString(dRow["Color"]) + "</b> </br>  SKU #    </td> ";
                    DataView datavw = new DataView();
                    datavw = dt.DefaultView;
                    datavw.RowFilter = "Color='" + BusinessUtility.GetString(dRow["Color"]) + "'";
                    DataTable dtProduct = datavw.ToTable();
                    int sColorQty = 0;
                    if (dtProduct.Rows.Count > 0)
                    {
                        string sCss = "";
                        string sColor = BusinessUtility.GetString(dRow["Color"]).Trim();
                        string sColorTotalID = "total_" + BusinessUtility.GetString(dRow["productID"]);
                        string sColorID = "clr_" + BusinessUtility.GetString(dRow["ColorID"]).Trim();

                        string sFunctionCall = "onblur='CalcColorTotalQty(this);'";
                        foreach (DataRow drProduct in dtProduct.Rows)
                        {
                            string sSize = BusinessUtility.GetString(drProduct["Size"]).Trim().Replace("/", "_").Replace(" ", "_");
                            string sSizeTotalID = "sizetotal_" + sSize;
                            sCss = sClass + " " + sColorID + " " + sSize + " " + "integerTextField";
                            sHtmlContent += "<td><input id=" + "qty" + BusinessUtility.GetString(drProduct["productID"]) + " value ='1' " + sFunctionCall + "  pname='" + BusinessUtility.GetString(drProduct["prdName"]) + "' pbarcode='" + BusinessUtility.GetString(drProduct["prdUPCCode"]) + "'  class='" + sCss + "' ppid=" + BusinessUtility.GetString(dRow["productID"]) + "   pid=" + BusinessUtility.GetString(drProduct["productID"]) + "  color=" + sColorID + "  totalToSave=" + sColorTotalID + "  sizeS=" + BusinessUtility.GetString(sSize) + " sizetotalToSave=" + sSizeTotalID + "  type='text'   style='width:100px;'/> </br> " + BusinessUtility.GetString(drProduct["prdUPCCode"]) + " </td> ";
                            //sHtmlContent += "<td><input id=" + "qty" + BusinessUtility.GetString(drProduct["productID"]) + " value ='1' pname='" + BusinessUtility.GetString(drProduct["prdName"]) + "' pbarcode='" + BusinessUtility.GetString(drProduct["prdUPCCode"]) + "'  class=" + sClass + " ppid=" + BusinessUtility.GetString(dRow["productID"]) + "   pid=" + BusinessUtility.GetString(drProduct["productID"]) + "  type='text' CssClass='numericTextField'  style='width:100px;'/> </br> " + BusinessUtility.GetString(drProduct["prdUPCCode"]) + " </td> ";
                            sColorQty += 1;
                        }
                    }
                    sGrandTotal += sColorQty;
                    sHtmlContent += "<td style='vertical-align:top;'><span id=" + "total_" + BusinessUtility.GetString(dRow["productID"]) + " type='text' class='colorTotal' style='width:100px;font-size:13px;font-weight:bold;'>" + sColorQty + "</span>  </td> ";
                    if (this.orderType == 2)
                    {
                        sHtmlContent += "<td style='vertical-align:top;'>" + sCurrencyBaseCode + "&nbsp;<input id=" + "price_" + BusinessUtility.GetString(dRow["productID"]) + " type='text' " + sDisabled + " CssClass='numericTextField' value='" + CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble("0".Replace(",", "."))) + "'  style='width:100px;'/>  </td> ";
                    }
                    else
                    {
                        sHtmlContent += "<td style='vertical-align:top;'>" + sCurrencyBaseCode + "&nbsp;<input id=" + "price_" + BusinessUtility.GetString(dRow["productID"]) + " type='text' " + sDisabled + " CssClass='numericTextField' value='" + CurrencyFormat.GetAmountInUSCulture(BusinessUtility.GetDouble(dRow["Price"].ToString().Replace(",", "."))) + "'  style='width:100px;'/>  </td> ";
                    }
                    sHtmlContent += "<td style='vertical-align:top;'><input id=" + "Discount_" + BusinessUtility.GetString(dRow["productID"]) + " type='text' " + sDisabled + "  CssClass='numericTextField' value='" + txtDiscount.Text + "'  style='width:100px;'/>  </td> ";
                    sHtmlContent += "</tr>";
                    i += 1;
                }

                sHtmlContent += "<tr>";
                sHtmlContent += "<td style='font-size:13px'><b> " + Resources.Resource.lblTotal + " </b> </td>";
                sHtmlContent += sHtmlSizeTotalContent;
                sHtmlContent += "<td style='vertical-align:top;'><span id='grandtotal' type='text' style='width:100px;font-size:15px;font-weight:bold;'>" + sGrandTotal + "</span>  </td><td>&nbsp;</td><td>&nbsp;</td>";

                string sHtmlTable = "<table style='border:1;width:100%;padding:5px;'> ";
                sHtmlTable += sHtmlHeading;
                sHtmlTable += sHtmlContent;
                sHtmlTable += "</table> ";
                sPHtml = sHtmlTable;
            }
        }

        txtQuantity.Text = "1";
        pnlSearch.Visible = false;
        pnlEditProductQuantity.Visible = true;
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<Collection> GetStyleCollection(string style)
    {
        Collection objCollection = new Collection();
        var t = objCollection.GetStyleCollection(null, style, Globals.CurrentAppLanguageCode);
        return t;
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SaveProductDetails(List<productDetail> ProductDetail)
    {
        try
        {
            var lstProductDetail = ProductDetail;
            foreach (var itemP in lstProductDetail)
            {
                if (BusinessUtility.GetDouble(itemP.PQTY) > 0)
                {
                    SalesCartEntity item = SalesCartHelper.GetItem(BusinessUtility.GetInt(itemP.PID));
                    if (item == null)
                    {
                        item = new SalesCartEntity();
                    }
                    item.ID = BusinessUtility.GetInt(0);
                    item.Discount = BusinessUtility.GetInt(itemP.PDisc);
                    item.DisType = BusinessUtility.GetString(itemP.PDType);
                    item.IsCanceled = false;
                    item.Price = BusinessUtility.GetDouble(itemP.PPrice);
                    item.ProductID = BusinessUtility.GetInt(itemP.PID);
                    item.ProductName = BusinessUtility.GetString(itemP.PName);
                    item.Quantity = BusinessUtility.GetDouble(itemP.PQTY);
                    item.RangePrice = 0.0D;
                    item.TaxGrp = BusinessUtility.GetInt(itemP.PTaxGroup);
                    item.TaxGrpDesc = BusinessUtility.GetString(itemP.PTaxDesc);
                    item.UPCCode = BusinessUtility.GetString(itemP.PBarcode);
                    item.WarehouseCode = BusinessUtility.GetString(itemP.PWareHouseCode);
                    Product objProduct = new Product();
                    DataTable dtProduct = objProduct.GetProductColorSize(null, BusinessUtility.GetInt(itemP.PID));
                    foreach (DataRow drItem in dtProduct.Rows)
                    {
                        item.PrdColor = BusinessUtility.GetString(drItem["Color"]);
                        item.PrdSize = BusinessUtility.GetString(drItem["Size"]);
                    }
                    SalesCartHelper.AddToCart(item, true);
                }
            }
            return "success";
        }
        catch (Exception e)
        {
            return "false";
        }
    }

    public class productDetail
    {
        public string PID { get; set; }
        public string PQTY { get; set; }
        public string PDisc { get; set; }
        public string PPrice { get; set; }
        public string PDType { get; set; }
        public string PName { get; set; }
        public string PBarcode { get; set; }
        public string PTaxGroup { get; set; }
        public string PTaxDesc { get; set; }
        public string PWareHouseCode { get; set; }
    }

}


Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.IO
Imports clsCommon
Imports clsDataClass
Imports System.Data

Partial Class Sales_Approval
    Inherits BasePage
    Protected TaxString As String
    Protected TaxStringForProcessItem As String
    Private strSOID As String = ""
    Private objSO As New clsOrders
    Private objSOItems As New clsOrderItems
    Private objSOIP As New clsOrderItemProcess
    Private objCust As New clsExtUser
    Private objTAx As New clsTaxCodeDesc

    Private objOrderType As New clsOrderType
    Private strProcessID As String = ""
    Private objAssLead As New clsAssignLead
    Private clsStatus As New clsSysStatus
    Private objSellRep As New clsSalesRepCustomer
    Private objUser As New clsExtUser
    Private objPartner As New clsPartners
    Private objSRC As New clsSalesRepCustomer
    Private objLead As New clsleadLog
    Private dblTotal As Double = 0

    Dim objOdrJnrl As clsOrderJournal
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If

        divItems.Visible = False
        If Request.QueryString("SOID") <> "" Then
            hdnOneItem.Value = objSOItems.funCountSOItems(Request.QueryString("SOID"))
        End If

        If Session("SalesReload") = "1" Then
            Session("SalesReload") = "0"
            Response.Redirect("Approval.aspx?SOID=" & Request.QueryString("SOID") & "&Aprov=" & Request.QueryString("Aprov"))
        End If

        If Request.QueryString("SOID") <> "" Then
            strSOID = Request.QueryString("SOID")
        End If

        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            If Session("Msg") = "." Then
                lblMsg.Text = ""
            End If
            Session.Remove("Msg")
        End If


        cmdApprove.Attributes.Add("onclick", "javascript:return " & _
                                        "confirm('" & msgApproveOrder & "')")

        cmdCancel.Attributes.Add("onclick", "javascript:return " & _
                                       "confirm('" & msgRejestOrder & "')")


        Dim bEditFlag As Boolean = False
        If Not Page.IsPostBack Then
            clsStatus.subGetStatus(dlStatus, "SO", "SOSts")
            objAssLead.subGetReason(dlReason, "OR") 'rejested reason
            objOrderType.subGetOrderType(dlstOderType)
            subPopulateCurrency()

            objTAx.PopulateTaxGroup(dlTaxCode)
            objTAx.PopulateTaxGroup(dlTaXGup1)
            If Request.QueryString("SOID") <> "" Then
                txtDate.Text = Now.ToString("MM/dd/yyyy")
                cmdGnratInvoice.Visible = False
                strSOID = Request.QueryString("SOID")
                subFillGrid()
                objSO.OrdID = strSOID
                objSO.getOrders()

                If objSO.InvRefNo <> "" Then
                    txtInvNo.Text = objSO.InvRefNo
                End If

                If objSO.invDate <> "" Then
                    txtInvDate.Text = CDate(objSO.invDate).ToString("MM/dd/yyyy")
                End If

                Dim objInv As New clsInvoices
                objInv.InvForOrderNo = Request.QueryString("SOID")
                objInv.InvRefType = "IV"
                If objInv.funCheckInvoiceForOrderNo = True Then
                    bEditFlag = True
                End If

                subPopulateWarehouse(objSO.ordCompanyID)

                subPopulateTerms()
                hdnWebhouse.Value = objSO.OrdShpWhsCode
                hdnCreatedUserID.Value = objSO.ordCreatedBy
                hdnSaleAgentID.Value = objSO.OrdSalesRepID
                hdnOrderID.Value = objSO.ordercommission

                lblSODate.Text = IIf(objSO.OrdCreatedOn <> "", CDate(objSO.OrdCreatedOn).ToString("MMM-dd yyyy"), "--")
                Dim str As String = "--"
                If (IsDate(objSO.QutExpDate)) Then
                    str = CDate(objSO.QutExpDate).ToString("MMM-dd yyyy")
                End If

                dlstOderType.SelectedValue = objSO.ordercommission
                lblSOQEDate.Text = str
                lblSOID.Text = objSO.OrdID
                objCust.CustID = objSO.OrdCustID
                objCust.CustType = objSO.OrdCustType
                objCust.getCustomerInfoByType()
                txtTerms.Text = objSO.ordShippingTerms
                txtNetTerms.Text = objSO.ordNetTerms
                dlCurrencyCode.SelectedValue = objSO.OrdCurrencyCode
                txtBillToAddress.Text = funPopulateAddress(objCust.CustType, "B", objCust.CustID).Replace("<br>", vbCrLf)
                txtShpToAddress.Text = funPopulateAddress(objCust.CustType, "S", objCust.CustID).Replace("<br>", vbCrLf)
                lblAddress.Text = funPopulateAddress(objCust.CustType, "S", objCust.CustID)
                lblCustomerName.Text = objCust.CustName
                hdnCustId.Value = objCust.CustID
                hdnCustTyp.Value = objCust.CustType
                hdnCurExRate.Value = objSO.OrdCurrencyExRate
                txtExRate.Text = objSO.OrdCurrencyExRate
                lblFax.Text = objCust.CustFax
                txtNotes.Text = objSO.OrdComment

                Dim intI As Int16 = 0
                Dim IntJ As Int16 = 0
                If objSO.OrdStatus = "A" Then
                    IntJ = 1
                ElseIf objSO.OrdStatus = "P" Then
                    IntJ = 2
                ElseIf objSO.OrdStatus = "H" Then
                    IntJ = 3
                ElseIf objSO.OrdStatus = "S" Then
                    IntJ = 4
                ElseIf objSO.OrdStatus = "D" Or objSO.OrdStatus = "C" Then
                    IntJ = 5
                    dlReason.Visible = True
                    lblRejectedReason.Visible = True
                    dlReason.SelectedItem.Text = objSO.orderRejectReason
                End If
                While intI < IntJ
                    dlStatus.Items.RemoveAt(1)
                    intI += 1
                End While
                Dim objUser As New clsUser
                If objSO.OrdStatus = "N" Then
                    lblApprovedBy.Text = "--"
                Else
                    lblApprovedBy.Text = objUser.funUserName(objSO.OrdVerifiedBy)
                End If
                lblCreatedBy.Text = objUser.funUserName(objSO.OrdSalesRepID)

                Dim objPrtCon As New clsPartnerContacts
                objPrtCon.subPartnerContactsInfo(objCust.CustID)
                lblContactName.Text = objPrtCon.ContactFirstName
                lblContactPhoneNo.Text = objPrtCon.ContactPhone

                dlStatus.SelectedValue = objSO.OrdStatus
                hdnPreStatus.Value = objSO.OrdStatus
                lblStatus.Text = dlStatus.SelectedItem.Text
                rblstShipBlankPref.SelectedValue = objSO.OrdShpBlankPref
                rblstISWeb.SelectedValue = objSO.OrdSaleWeb


                If (Session("UserModules").ToString.Contains("QOA") = True Or Session("UserModules").ToString.Contains("ADM") = True) And Request.QueryString("aprov") <> "y" Then
                    lblTitle.Text = lblEditSalesOrder

                    If objSO.OrdStatus = "A" Or objSO.OrdStatus = "N" Then
                        lblTitle.Text = lblEditQuotation
                    End If

                    cmdApprove.Visible = False
                    cmdCancel.Visible = False
                Else
                    If Request.QueryString("aprov") = "y" Then
                        cmdSave.Visible = False
                        cmdSoCancel.Visible = False
                        cmdCpyOrder.Visible = False
                        cmdGnratInvoice.Visible = False
                        lblTitle.Text = prnQuotation '"Approve Sales Order"
                        dlStatus.SelectedValue = "A"

                        dlReason.Visible = True
                        lblRejectedReason.Visible = True
                        custvalReason.Visible = True
                    Else
                        If objSO.OrdStatus = "N" Then
                            dlStatus.Enabled = False
                        Else
                            dlStatus.Enabled = True
                        End If
                        lblTitle.Text = lblEditSalesOrder
                        cmdApprove.Visible = False
                        cmdCancel.Visible = False
                        imgFax.Visible = False
                        imgMail.Visible = False
                    End If
                End If

                If objSO.OrdShpDate <> "" Then
                    txtShippingDate.Text = CDate(objSO.OrdShpDate).ToString("MM/dd/yyyy")
                End If
                txtShpTrackNo.Text = objSO.OrdShpTrackNo
                txtShpCost.Text = objSO.OrdShpCost
                txtShpCode.Text = objSO.OrdShpCode
                dlCurrencyCode.SelectedValue = objSO.OrdCurrencyCode
                txtCustPO.Text = objSO.OrdCustPO
                If objSO.QutExpDate <> "" Then
                    txtQutExpDate.Text = CDate(objSO.QutExpDate).ToString("MM/dd/yyyy")
                Else
                    txtQutExpDate.Text = Now.ToString("MM/dd/yyyy") 'Set Today as qut exp date
                End If
                dlShpWarehouse.SelectedValue = objSO.OrdShpWhsCode
                subCalcTax()
                Dim strDocTyp As String = funStatusDocType()
                Me.imgPdf.Attributes.Add("OnClick", "return openPDF('ShowPdf.aspx?SOID=" & Request.QueryString("SOID") & "&DocTyp=" & strDocTyp & "')")
                If bEditFlag = True Then
                    grdAddProcLst.Columns(9).Visible = False
                    grdAddProcLst.Columns(10).Visible = False
                    CmdAddSalesProcess.Visible = False
                    grdOrdItems.Columns(15).Visible = False
                    grdOrdItems.Columns(16).Visible = False
                Else
                    Me.CmdAddSalesProcess.Attributes.Add("OnClick", "return openPopup('../sales/CreateSalesProcess.aspx?SOID=" & Request.QueryString("SOID") & "&Type=SO&custID=1""')")
                    Me.txtShpToAddress.Attributes.Add("OnClick", "return popUp('" & divShipToAddress.ClientID & "');")
                    Me.txtBillToAddress.Attributes.Add("OnClick", "return popUp('" & divBillToAddress.ClientID & "');")
                End If

                Me.txtWhsShpToAddress.Attributes.Add("OnClick", "return popUp('" & divWhsShpAddress.ClientID & "');")
                Me.imgMail.Attributes.Add("OnClick", "return popUp('" & divMail.ClientID & "');")
                Me.imgFax.Attributes.Add("OnClick", "return popUp('" & divFax.ClientID & "');")
                Me.cmdFax.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & ConfirmFaxNumber & " ')")
                txtFaxTo.Text = lblFax.Text
                txtAttention.Text = lblCustomerName.Text
                txtFaxSubject.Text = strDocTyp & "-" & lblSOID.Text
                txtMailTo.Text = objCust.CustEmail
                txtMailSubject.Text = strDocTyp & "-" & lblSOID.Text
            End If
        End If
        If (objSO.OrdStatus <> "N") And (Session("UserModules").ToString.Contains("INV") = True Or Session("UserModules").ToString.Contains("ADM") = True) Then
            If Request.QueryString("aprov") = "y" Then
                cmdGnratInvoice.Visible = False
            Else
                cmdGnratInvoice.Visible = True
            End If
        Else
            cmdGnratInvoice.Visible = False
        End If

        If objSO.OrdStatus = "A" Or objSO.OrdStatus = "N" Then
            dlstOderType.Enabled = True
        End If


        Dim OrderIdcount As String

        Dim objOrdInvFrequency As New clsOrdInvFrequency()
        OrderIdcount = objOrdInvFrequency.CheckSubcription(Request.QueryString("SOID"))

        If OrderIdcount > 0 Then
            CBSubscription.Checked = True
        Else
            CBSubscription.Checked = False

        End If
        txtDiscount.ReadOnly = True
    End Sub
    'Populate Address
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)

        If (sType = "B") Then
            txtBillToAddressLine1.Text = objAdr.AddressLine1
            txtBillToAddressLine2.Text = objAdr.AddressLine2
            txtBillToAddressLine3.Text = objAdr.AddressLine3
            txtBillToAddressCity.Text = objAdr.AddressCity
            If objAdr.AddressState <> "" Then
                If objAdr.getStateName <> "" Then
                    txtBillToAddressState.Text = objAdr.getStateName
                Else
                    txtBillToAddressState.Text = objAdr.getStateName
                End If
            End If
            If objAdr.AddressCountry <> "" Then
                If objAdr.getCountryName <> "" Then
                    txtBillToAddressCountry.Text = objAdr.getCountryName
                Else
                    txtBillToAddressCountry.Text = objAdr.AddressCountry
                End If
            End If
            txtBillToAddressPostalCode.Text = objAdr.AddressPostalCode
        Else
            txtShpToAddressLine1.Text = objAdr.AddressLine1
            txtShpToAddressLine2.Text = objAdr.AddressLine2
            txtShpToAddressLine3.Text = objAdr.AddressLine3
            txtShpToAddressCity.Text = objAdr.AddressCity
            If objAdr.AddressState <> "" Then
                If objAdr.getStateName <> "" Then
                    txtShpToAddressState.Text = objAdr.getStateName
                Else
                    txtShpToAddressState.Text = objAdr.getStateName
                End If
            End If
            If objAdr.AddressCountry <> "" Then
                If objAdr.getCountryName <> "" Then
                    txtShpToAddressCountry.Text = objAdr.getCountryName
                Else
                    txtShpToAddressCountry.Text = objAdr.AddressCountry
                End If
            End If
            txtShpToAddressPostalCode.Text = objAdr.AddressPostalCode
        End If

        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + "<br>"
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + "<br>"
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + "<br>"
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + "<br>"
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + "<br>"
            Else
                strAddress += objAdr.AddressState + "<br>"
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + "<br>"
            Else
                strAddress += objAdr.AddressCountry + "<br>"
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += lblPostalCode & " " + objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function
    ' Sql Data Source Event Track
    Protected Sub sqldsOrdItems_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsOrdItems.Selected
        subCalcAmount()
    End Sub
    'Populate Grid
    Public Sub subFillGrid()
        sqldsOrdItems.SelectCommand = objSOItems.funFillGrid(strSOID)  'fill Order Items
        objSOIP.OrdID = strSOID
        objSOItems.OrdID = strSOID
        sqldsAddProcLst.SelectCommand = objSOIP.funFillGridForProcess 'fill Order Items Process
    End Sub
    'On Row Data Bound
    Protected Sub grdOrdItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdOrdItems.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            If hdnOneItem.Value <= 2 Then
                imgDelete.Attributes.Add("onclick", "javascript:return " & _
                                                "alert('" & msgPlzKeepOnePrdInSO & " ')")
            Else
                imgDelete.Attributes.Add("onclick", "javascript:return " & _
                                 "confirm('" & SOItemConfirmDelete & " ')")
            End If
            e.Row.Cells(9).Text = String.Format("{0:F}", CDbl(e.Row.Cells(9).Text))
            Dim objCur As New clsCurrencies
            e.Row.Cells(12).Text = String.Format("{0:F}", CDbl(e.Row.Cells(12).Text)) & objCur.funBaseCurConversion(hdnCurExRate.Value, e.Row.Cells(12).Text)
            Dim txtDisType As TextBox = CType(e.Row.Cells(15).FindControl("txtDisType"), TextBox)
            If txtDisType.Text = "P" Then
                e.Row.Cells(7).Text = e.Row.Cells(7).Text & "%"
            End If
            e.Row.Cells(4).Text = e.Row.Cells(4).Text.Replace(Environment.NewLine, "<br>")

            Dim dblPrice As Double = 0
            Dim dblSugstPrice As Double = 0
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim lblSgstPrice As Label = CType(e.Row.FindControl("lblSuggestPrice"), Label)
                lblSgstPrice.Text = clsCommon.getProductSalePrice(e.Row.Cells(0).Text, e.Row.Cells(8).Text)

                e.Row.Cells(10).BackColor = e.Row.BackColor
                If e.Row.Cells(9).Text <> "" Then
                    dblPrice = Convert.ToDouble(e.Row.Cells(9).Text)
                End If
                If lblSgstPrice.Text <> "" Then
                    dblSugstPrice = Convert.ToDouble(lblSgstPrice.Text)
                End If
                If (dblPrice < dblSugstPrice) Then
                    e.Row.Cells(10).BackColor = Drawing.Color.Pink
                Else
                    e.Row.Cells(10).BackColor = e.Row.BackColor
                End If
            End If
        End If
    End Sub
    'Deleting
    Protected Sub grdOrdItems_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdOrdItems.RowDeleting
        Dim strItemID As String = grdOrdItems.DataKeys(e.RowIndex).Value.ToString()
        If hdnOneItem.Value >= 2 Then
            objSOItems.OrderItemID = strItemID
            sqldsOrdItems.DeleteCommand = objSOItems.funDeleteSOItems()
            lblMsg.Text = msgSOOrderItemDeletedSuccessfully
        End If
        If hdnOneItem.Value = 1 Then
            sqldsOrdItems.DeleteCommand = " DELETE FROM orderitems WHERE orderItemID ='0'"
            lblMsg.Text = ""
        End If
        subFillGrid()
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    ' On Reject
    Protected Sub cmdCancel_severClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.ServerClick
        strSOID = Request.QueryString("SOID")
        objSO.OrdID = strSOID
        objSO.OrdStatus = "C"
        objSO.orderRejectReason = dlReason.SelectedItem.Text
        objSO.funUpdateOrder("RejestReason")
        objSO.OrdVerified = 0
        objSO.OrdVerifiedBy = Session("UserID")
        objSO.subUpdatedVerified()
        Response.Redirect("Show.aspx?Msg=2&SOID=" & strSOID)
    End Sub
    'Get Address Listing
    Protected Function funAddressListing(ByVal strID As String)
        Dim strData As String = ""
        If strID <> "" Then
            Dim objWhs As New clsWarehouses
            objWhs.WarehouseCode = strID
            objWhs.getWarehouseInfo()
            strData = "<table align='left' width=100% cellspacing='0' cellpadding='0' border='0' style='border:0px'> <tr><td align='left'>"
            If objWhs.WarehouseCode <> "" Then
                'strData += "<tr><td align='left'>" + objWhs.WarehouseCode + "</td></tr>"
            End If
            If objWhs.WarehouseDescription <> "" Then
                strData += objWhs.WarehouseDescription & "<br>"
            End If
            If objWhs.WarehouseAddressLine1 <> "" Then
                strData += objWhs.WarehouseAddressLine1 & "<br>"
            End If
            If objWhs.WarehouseAddressLine2 <> "" Then
                strData += objWhs.WarehouseAddressLine2 & "<br>"
            End If
            If objWhs.WarehouseCity <> "" Then
                strData += objWhs.WarehouseCity & "<br>"
            End If
            If objWhs.WarehouseState <> "" Then
                strData += objWhs.WarehouseState & "<br>"
            End If
            If objWhs.WarehouseCountry <> "" Then
                strData += objWhs.WarehouseCountry & "<br>"
            End If
            If objWhs.WarehousePostalCode <> "" Then
                strData += objWhs.WarehousePostalCode & "<br>"
            End If
            strData += "</td></tr></table>"
            objWhs = Nothing
        End If
        Return strData
    End Function
    'Populate Currency
    Private Sub subPopulateCurrency()
        Dim objCur As New clsCurrencies
        objCur.PopulateCurrency(dlCurrencyCode)
        objCur = Nothing
    End Sub
    'Populate Warehouse
    Private Sub subPopulateWarehouse(ByVal strCompanyID As String)
        Dim objWhs As New clsWarehouses
        objWhs.PopulateWarehouse(dlShpWarehouse, strCompanyID)
        objWhs = Nothing
    End Sub
    'Populate Terms
    Private Sub subPopulateTerms()
        Dim objComp As New clsCompanyInfo
        objComp.CompanyID = objSO.ordCompanyID
        objComp.getCompanyInfo()
        'txtTerms.Text = objComp.CompanyShpToTerms
        lblSOPOComNameTitle.Text = objComp.CompanyName
        'Dim objWhs As New clsWarehouses
        'objWhs.WarehouseCompanyID = objComp.CompanyID
        'lblSOPOwhsName.Text = objWhs.funGetWhsName
        objComp = Nothing
        'objWhs = Nothing
    End Sub
    'On Row Data Bound
    Protected Sub grdAddProcLst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAddProcLst.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & SOProcessItemConfirmDelete & " ')")
            e.Row.Cells(4).Text = String.Format("{0:F}", CDbl(e.Row.Cells(4).Text))
            e.Row.Cells(5).Text = String.Format("{0:F}", CDbl(e.Row.Cells(5).Text))
            e.Row.Cells(6).Text = String.Format("{0:F}", CDbl(e.Row.Cells(6).Text))
            e.Row.Cells(9).Text = String.Format("{0:F}", CDbl(e.Row.Cells(9).Text))

            '' You can add the tax details here 
            If (e.Row.Cells(10).Text <> "0") Then
                Dim dtTaxDetails As DataTable
                dtTaxDetails = objSOIP.funGetTaxDetailByTaxCode(Convert.ToInt32(e.Row.Cells(10).Text))
                Dim calTax As Double
                Dim calTaxForLbl As Double
                calTax = Convert.ToDouble(e.Row.Cells(9).Text)
                If (dtTaxDetails.Rows.Count > 0) Then
                    Dim i As Integer = 0
                    For i = 0 To dtTaxDetails.Rows.Count - 1
                        calTax += ((calTax * Convert.ToDouble(dtTaxDetails.Rows(i)("sysTaxPercentage").ToString())) / 100)
                        calTaxForLbl += ((calTax * Convert.ToDouble(dtTaxDetails.Rows(i)("sysTaxPercentage").ToString())) / 100)

                        TaxStringForProcessItem &= "<tr><td align=right><b>" & dtTaxDetails.Rows(i)("sysTaxDesc").ToString() & ":</b></td><td align=right><b>" & dlCurrencyCode.SelectedValue & " " & String.Format("{0:F}", calTaxForLbl) & "</b></td></tr>"
                    Next
                    lblSubTotalProcessItemAmount.Text = dlCurrencyCode.SelectedValue & " " & e.Row.Cells(9).Text

                    calTaxForLbl += dblTotal
                    lblAmount.Text = String.Format("{0:F}", calTaxForLbl)
                    Dim objCur As New clsCurrencies
                    lblAmount.Text = dlCurrencyCode.SelectedValue & " " & lblAmount.Text & " " & objCur.funBaseCurConversion(hdnCurExRate.Value, calTaxForLbl)

                End If

            End If
        End If
    End Sub
    ' On Row Deleting
    Protected Sub grdAddProcLst_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdAddProcLst.RowDeleting
        Dim strItemID As String = grdAddProcLst.DataKeys(e.RowIndex).Value.ToString()
        objSOIP.OrderItemProcID = strItemID
        sqldsAddProcLst.DeleteCommand = objSOIP.funDeleteSOItemProcess()
        lblMsg.Text = msgSOProcessDeletedSuccessfully
        subFillGrid()
    End Sub
    ' Sql Data Source Event Track
    Protected Sub sqldsAddProcLst_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsAddProcLst.Selected
        subCalcAmount()
    End Sub
    ' Calcualte Amount
    Private Sub subCalcAmount()
        objSOIP.OrdID = strSOID
        objSOItems.OrdID = strSOID
        If grdAddProcLst.Rows.Count > 0 Then
            lblProcessList.Visible = True
        Else
            lblProcessList.Visible = False
        End If
    End Sub
    ' Calculate Tax
    Private Sub subCalcTax()
        lblSubTotal.Text = "0"
        Dim drSOItems As Data.Odbc.OdbcDataReader
        Dim objPrn As New clsPrint
        Dim objCur As New clsCurrencies
        drSOItems = objSOItems.GetDataReader(objSOItems.funFillGrid(strSOID))
        Dim dblSubTotal As Double = 0
        Dim dblTax As Double = 0
        Dim dblProcessTotal As Double = 0
        ''Dim dblTotal As Double = 0
        Dim dblDiscountApplied As Double = 0
        Dim dblAmount As Double = 0
        Dim strTaxArray(,) As String
        Dim strProcessTax As ArrayList
        Dim strTaxItem As String
        While drSOItems.Read
            Dim strTax As ArrayList
            strTax = objPrn.funCalcTax(drSOItems("ordProductID"), drSOItems("ordShpWhsCode"), drSOItems("amount"), "", drSOItems("ordCustID"), drSOItems("ordCustType"), Convert.ToString(drSOItems("ordProductTaxGrp")))
            Dim intI As Integer = 0
            While strTax.Count - 1 >= intI
                strTaxItem = strTax.Item(intI).ToString
                strTaxArray = objPrn.funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                intI += 1
            End While
            dblSubTotal = CDbl(dblSubTotal) + CDbl(drSOItems("amount"))
            lblSubTotal.Text = String.Format("{0:F}", dblSubTotal) 'dblSubTotal.ToString
        End While
        Dim i As Integer = 0
        Dim j As Integer = 0

        objSOItems.OrdID = strSOID
        dblProcessTotal = CDbl(objSOIP.getProcessCostForOrderNo())
        strTaxArray = objPrn.funTotalProcessTax(dblProcessTotal, dlShpWarehouse.SelectedValue, strTaxArray)

        If Not strTaxArray Is Nothing Then
            j = strTaxArray.Length / 2
        End If
        TaxString = ""
        '''''''Here you can add the Tex .....which you store on data base..........
        While i < j
            TaxString &= "<tr><td align=right><b>" & strTaxArray(0, i) & ":</b></td><td align=right><b>" & dlCurrencyCode.SelectedValue & " " & String.Format("{0:F}", strTaxArray(1, i)) & "</b></td></tr>"
            dblTax += strTaxArray(1, i)
            i += 1
        End While

        dblTotal = dblTotal + dblSubTotal + dblProcessTotal + dblTax
        lblAmount.Text = String.Format("{0:F}", dblTotal)
        Dim objUser As New clsUser
        Dim strCommission As String = objOrderType.funOderCommission(hdnOrderID.Value, hdnSaleAgentID.Value, lblSubTotal.Text)
        If strCommission = 0 Then
            trCommission.Visible = False
        End If
        lblCommAgentName.Text = objUser.funUserName(hdnSaleAgentID.Value)
        lblCommission.Text = dlCurrencyCode.SelectedValue & " " & strCommission
        lblSubTotal.Text = dlCurrencyCode.SelectedValue & " " & lblSubTotal.Text
        lblAmount.Text = dlCurrencyCode.SelectedValue & " " & lblAmount.Text & " " & objCur.funBaseCurConversion(hdnCurExRate.Value, dblTotal)
        drSOItems.Close()
        objSOItems.CloseDatabaseConnection()
        objPrn = Nothing
        Dim ivid As Integer = 0
        Dim soid As Integer = 0
        Integer.TryParse(Request.QueryString("SOID"), soid)
        'ltPartialPayment.Text = iTECH.InbizERP.BusinessLogic.ProcessInvoice.GetPartialPaymentHtml(soid, ivid, iTECH.InbizERP.BusinessLogic.CurrentUser.DefaultCompanyID)
    End Sub
    'Return Status Document Type
    Private Function funStatusDocType() As String
        'Dim sStatus As String = ""
        'sStatus = "QO"
        'Return sStatus
        Dim sStatus As String = ""
        Select Case hdnPreStatus.Value
            Case "N", "A", "V", "H"
                sStatus = "QO"
            Case "P", "I", "S", "C", "Z"
                sStatus = "SO"
            Case Else
                sStatus = "QO"
        End Select
        Return sStatus
    End Function

    'Save Address Object
    'Protected Sub cmdBillToAddress_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBillToAddress.ServerClick

    'End Sub

    'Protected Sub cmdShpToAddress_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdShpToAddress.ServerClick

    'End Sub

    Protected Sub grdOrdItems_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdOrdItems.RowEditing
        lblMsg.Text = ""
        divItems.Visible = True

        divViewPrd.Style("position") = "absolute"
        divViewPrd.Style("top") = "250px"
        divViewPrd.Style("left") = hdnLeft.Value & "px"

        txtOrdItmId.Text = grdOrdItems.DataKeys(e.NewEditIndex).Value.ToString
        txtProductId.Text = grdOrdItems.Rows(e.NewEditIndex).Cells(0).Text
        txtUPCCode.Text = grdOrdItems.Rows(e.NewEditIndex).Cells(3).Text
        txtProductName.Text = grdOrdItems.Rows(e.NewEditIndex).Cells(4).Text.Replace("<br>", Environment.NewLine)
        txtWhsShpToAddress.Text = funGetWhsAddress(dlShpWarehouse.SelectedValue) 'grdOrdItems.Rows(e.NewEditIndex).Cells(6).Text
        txtQuantity.Text = grdOrdItems.Rows(e.NewEditIndex).Cells(8).Text

        Dim objSOI As New clsOrderItems
        objSOI.OrderItemID = grdOrdItems.DataKeys(e.NewEditIndex).Value.ToString
        objSOI.getOrderItemsInfo()
        txtPrice.Text = objSOI.OrdProductUnitPrice
        'txtUnitPrice.Text = grdOrdItems.Rows(e.NewEditIndex).Cells(9).Text
        Dim str As String = objSOI.ordProductTaxGrp
        dlTaxCode.SelectedValue = str
        txtDiscount.Text = grdOrdItems.Rows(e.NewEditIndex).Cells(7).Text
        Dim txtDisType As TextBox = CType(grdOrdItems.Rows(e.NewEditIndex).FindControl("txtDisType"), TextBox)
        rblstDiscountType.SelectedValue = txtDisType.Text
        txtDiscountVal.Text = txtDiscount.Text
        strSOID = Request.QueryString("SOID")
        subCalcTax()
        subFillGrid()
        divPriceRnge.Style("Display") = "none"
    End Sub
    'Protected Sub imgAdd_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgAdd.ServerClick

    'End Sub
    Protected Function funGetWhsAddress(ByVal strID As String)
        Dim strData As String = ""
        If strID <> "" Then
            Dim objWhs As New clsWarehouses
            objWhs.WarehouseCode = strID
            objWhs.getWarehouseInfo()
            If objWhs.WarehouseDescription <> "" Then
                strData += objWhs.WarehouseDescription + " "
                txtWhsShpToDesc.Text = objWhs.WarehouseDescription
            End If
            If objWhs.WarehouseAddressLine1 <> "" Then
                strData += objWhs.WarehouseAddressLine1 + " "
                txtWhsShpToAddressLine1.Text = objWhs.WarehouseAddressLine1
            End If
            If objWhs.WarehouseAddressLine2 <> "" Then
                strData += objWhs.WarehouseAddressLine2 + " "
                txtWhsShpToAddressLine2.Text = objWhs.WarehouseAddressLine2
            End If
            If objWhs.WarehouseCity <> "" Then
                strData += objWhs.WarehouseCity + " "
                txtWhsShpToAddressCity.Text = objWhs.WarehouseCity
            End If
            If objWhs.WarehouseState <> "" Then
                strData += objWhs.WarehouseState + " "
                txtWhsShpToAddressState.Text = objWhs.WarehouseState
            End If
            If objWhs.WarehouseCountry <> "" Then
                strData += objWhs.WarehouseCountry + " "
                txtWhsShpToAddressCountry.Text = objWhs.WarehouseCountry
            End If
            If objWhs.WarehousePostalCode <> "" Then
                strData += objWhs.WarehousePostalCode + " "
                txtWhsShpToAddressPostalCode.Text = objWhs.WarehousePostalCode
            End If
            objWhs = Nothing
        End If
        Return strData
    End Function
    'Protected Sub cmdWhsShpToAddress_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdWhsShpToAddress.ServerClick

    'End Sub
    'Protected Sub cmdAddDiscount_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddDiscount.ServerClick

    'End Sub
    Protected Sub grdAddProcLst_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdAddProcLst.RowEditing
        strProcessID = grdAddProcLst.DataKeys(e.NewEditIndex).Value.ToString()
        divSalesProcess.Style("display") = "block"
        txtProcessID.Text = strProcessID
        objSOIP.OrderItemProcID = strProcessID
        objSOIP.getOrderItemProcessInfo()
        txtProcessDescription.Text = grdAddProcLst.Rows(e.NewEditIndex).Cells(2).Text
        txtProcessFixedCost.Text = objSOIP.OrdItemProcFixedPrice
        txtProcessCostPerHour.Text = objSOIP.OrdItemProcPricePerHour
        txtProcessCostPerUnit.Text = objSOIP.OrdItemProcPricePerUnit
        txtTotalHour.Text = objSOIP.OrdItemProcHours
        dlTaXGup1.SelectedValue = objSOIP.sysTaxCodeDescID
        txtTotalUnit.Text = objSOIP.OrdItemProcUnits
        subFillGrid()
        subCalcTax()
        txtProcessFixedCost.Focus()
    End Sub
    Protected Sub imgAddProcess_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAddProcess.Click
        If IsNumeric(txtProcessFixedCost.Text) <> True Then
            lblMsg.Text = msgSOPlzEntFixedCost '"Please enter Fixed Cost."
            txtProcessFixedCost.Focus()
            Exit Sub
        End If
        If IsNumeric(txtProcessCostPerHour.Text) <> True Then
            lblMsg.Text = msgSOPlzEntCostPerHour '"Please enter Cost/Hour."
            txtProcessCostPerHour.Focus()
            Exit Sub
        End If
        If IsNumeric(txtProcessCostPerUnit.Text) <> True Then
            lblMsg.Text = msgSOPlzEntCostPerUnit '"Please enter Cost/Unit."
            txtProcessCostPerUnit.Focus()
            Exit Sub
        End If
        If IsNumeric(txtTotalHour.Text) <> True Then
            lblMsg.Text = msgSOPlzEntTotalHour '"Please enter Total Hour."
            txtTotalHour.Focus()
            Exit Sub
        End If
        If IsNumeric(txtTotalUnit.Text) <> True Then
            lblMsg.Text = msgSOPlzEntTotalUnit '"Please enter Total Unit."
            txtTotalUnit.Focus()
            Exit Sub
        End If
        objSOIP.OrderItemProcID = txtProcessID.Text
        objSOIP.getOrderItemProcessInfo()
        objSOIP.OrdID = lblSOID.Text
        objSOIP.OrdItemProcCode = objSOIP.OrdItemProcCode
        objSOIP.sysTaxCodeDescID = dlTaXGup1.SelectedValue.ToString()
        objSOIP.OrdItemProcFixedPrice = txtProcessFixedCost.Text
        objSOIP.OrdItemProcPricePerHour = txtProcessCostPerHour.Text
        objSOIP.OrdItemProcHours = txtTotalHour.Text
        objSOIP.OrdItemProcPricePerUnit = txtProcessCostPerUnit.Text
        objSOIP.OrdItemProcUnits = txtTotalUnit.Text
        objSOIP.updateOrderItemProcess()
        divSalesProcess.Style("display") = "none"
        Response.Redirect("~/Sales/Approval.aspx?SOID=" & Request.QueryString("SOID"))
    End Sub
    ' On Approve
    Protected Sub cmdApprove_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdApprove.ServerClick
        'strSOID = Request.QueryString("SOID")
        'objSO.OrdID = strSOID
        'objSO.OrdStatus = "A"
        'objSO.OrdCustPO = txtCustPO.Text
        'objSO.ordShippingTerms = txtTerms.Text
        'objSO.ordNetTerms = txtNetTerms.Text
        'objSO.funApproveOrder()
        If subSave() = False Then
            Exit Sub
        End If
        objSO.OrdVerified = 1
        objSO.OrdVerifiedBy = Session("UserID")
        objSO.OrdID = Request.QueryString("SOID")
        objSO.subUpdatedVerified()
        Response.Redirect("ViewSalesOrder.aspx?Msg=1")
    End Sub
    Protected Sub cmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.ServerClick
        If subSave() = False Then
            Exit Sub
        End If
        Response.Redirect("ViewSalesOrder.aspx")
    End Sub
    Public Function subSave() As Boolean
        If Not Page.IsValid Then
            'Exit Sub
        End If

        If dlStatus.SelectedValue = "K" Then 'Reservation Canceled
            'New iTECH.Busi
            Dim rrr As New iTECH.InbizERP.BusinessLogic.Reservation()
            rrr.CancelReservation(iTECH.Library.Utilities.BusinessUtility.GetInt(Request.QueryString("SOID")))
        End If

        If dlStatus.SelectedValue = "P" And txtCustPO.Text = "" Then
            'lblMsg.Text = msgSOPlzEntCustomerPO '"Please enter Customer PO."
            'txtCustPO.Focus()
            'Exit Sub
        ElseIf dlStatus.SelectedItem.Text = "" Then
            lblMsg.Text = msgSOPlzSelStatus '"Please select Order Status."
            dlStatus.Focus()
            Return False
        End If
        objSO.OrdID = Request.QueryString("SOID")
        objSO.getOrdersInfo()
        subSetData(objSO)
        If ConfigurationManager.AppSettings("AllowCustomInvoiceEntry").ToLower = "true" Then
            If objSO.funDuplicateInvoice("IV", objSO.ordCompanyID, objSO.InvRefNo, Request.QueryString("SOID")) = True Then
                lblMsg.Text = lblInvoiceExists & "(" & objSO.InvRefNo & ")"
                txtInvNo.Focus()
                Return False
            End If
        End If
        If dlStatus.SelectedValue = "P" Then
            If objSO.updateOrders() = True Then
                'Update quantity QuoteRsv and SORsv
                If hdnPreStatus.Value <> "S" Then
                    If hdnPreStatus.Value = "I" Or hdnPreStatus.Value = "P" Or hdnPreStatus.Value = "C" Or hdnPreStatus.Value = "Z" Then
                        'Skip the update and invoice process
                    Else
                        objSO.updateProductQuantityOnProcess()
                        'Invoice Process Start
                        'funInvoiceProcess()
                        'Invoice Process End
                        Session.Add("Msg", OrderUpdatedSuccessfully)
                    End If
                Else
                    Session.Add("Msg", OrderUpdatedSuccessfully)
                End If
            Else
                Session.Add("Msg", OrderNotUpdatedSuccessfully)
            End If
            'For Shipped status
            'Devendra 14_07_09 start
        ElseIf dlStatus.SelectedValue = "S" And hdnPreStatus.Value <> "S" Then
            If (hdnPreStatus.Value = "P" Or hdnPreStatus.Value = "Z") Then
                objSO.OrdStatus = "D"
                If objSO.updateOrders() = True Then
                    'Update quantity SORsv and OnHand
                    objSO.updateProductQuantityOnShipping()
                    'Invoice Process Start
                    funInvoiceProcess()
                    'Invoice Process End
                    Session.Add("Msg", OrderShippedSuccessfully)
                Else
                    Session.Add("Msg", OrderNotShippedSuccessfully)
                End If
            End If
            'Devendra 14_07_09 End
        Else
            If objSO.updateOrders() = True Then
                If hdnPreStatus.Value = "N" And objSO.OrdStatus = "A" Then
                    subGenerateAlert(Request.QueryString("SOID"), hdnCreatedUserID.Value, lblCustomerName.Text)
                End If
                Session.Add("Msg", OrderUpdatedSuccessfully)
            Else
                Session.Add("Msg", OrderNotUpdatedSuccessfully)
            End If
        End If


        Return True

    End Function
    ' Inserts Alert for SO Quotation  
    Protected Sub subGenerateAlert(ByVal strSOID As String, ByVal alrtUsrID As String, ByVal custName As String)
        Dim objAlert As New clsAlerts
        objAlert.AlertPartnerContactID = strSOID ' SO ID
        objAlert.AlertComID = 0
        objAlert.AlertDateTime = DateTime.Now().ToString("yyyy-MM-dd HH:mm:ss") 'alrtDateTime
        objAlert.AlertUserID = alrtUsrID
        ' Create message
        Dim strNote As String = ""
        strNote = lblQoApproved & "<br><br>" 'Sale Quotation Approved.
        strNote += alrtlblSOID & " " & strSOID & "<br>" 'So No.
        strNote += alrtlblCustName & " " & custName & "<br>" 'Customer:
        Dim objUsr As New clsUser
        objUsr.UserID = Session("UserID")
        objUsr.getUserInfo()
        strNote += alrtlblAsgnBy & " " & objUsr.UserSalutation & " " & objUsr.UserFirstName & " " & objUsr.UserLastName 'Assigned by:
        objUsr = Nothing
        objAlert.AlertNote = strNote
        objAlert.AlertRefType = "SOA"
        objAlert.insertAlerts()
    End Sub
    'Populate Object
    Private Sub subSetData(ByRef objS As clsOrders)
        If dlStatus.SelectedValue = "" Then
            objS.OrdType = objS.OrdType
        ElseIf dlStatus.SelectedValue = "N" Then
            objS.OrdType = "QT"
        ElseIf dlStatus.SelectedValue = "A" Then
            objS.OrdType = "SO"
        ElseIf dlStatus.SelectedValue = "P" Then
            objS.OrdType = "WO"
        Else
            objS.OrdType = objS.OrdType
        End If
        objS.OrdCustType = objS.OrdCustType
        objS.OrdCustID = objS.OrdCustID
        Try
            objS.OrdCreatedOn = CDate(objS.OrdCreatedOn).ToString("yyyy-MM-dd hh:mm:ss")
        Catch ex As Exception

        End Try

        objS.OrdCreatedFromIP = objS.OrdCreatedFromIP
        objS.OrdStatus = dlStatus.SelectedValue
        If dlStatus.SelectedValue = "V" Then
            objS.OrdVerified = 1
            objS.OrdVerifiedBy = Session("UserID").ToString
        Else
            objS.OrdVerified = objS.OrdVerified
            objS.OrdVerifiedBy = objS.OrdVerifiedBy
        End If
        objS.OrdSalesRepID = objS.OrdSalesRepID
        objS.OrdSaleWeb = rblstISWeb.SelectedValue
        objS.OrdShpBlankPref = rblstShipBlankPref.SelectedValue
        objS.OrdLastUpdateBy = Session("UserID").ToString
        objS.OrdLastUpdatedOn = Now.ToString("yyyy-MM-dd hh:mm:ss")
        objS.OrdShpCode = txtShpCode.Text
        objS.OrdShpWhsCode = dlShpWarehouse.SelectedValue
        objS.OrdShpCost = txtShpCost.Text

        objS.ordercommission = dlstOderType.SelectedValue
        objSO.ordShippingTerms = txtTerms.Text
        objSO.ordNetTerms = txtNetTerms.Text
        'objS.ordCompanyID = Request.QueryString("ComID")
        If txtShippingDate.Text <> "" Then
            If IsDate(DateTime.ParseExact(txtShippingDate.Text, "MM/dd/yyyy", Nothing)) = True Then
                objS.OrdShpDate = DateTime.ParseExact(txtShippingDate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
            Else
                If IsDate(objS.OrdShpDate) = True Then
                    objS.OrdShpDate = CDate(objS.OrdShpDate).ToString("yyyy-MM-dd hh:mm:ss")
                Else
                    objS.OrdShpDate = ""
                End If
            End If
        Else
            If IsDate(objS.OrdShpDate) = True Then
                objS.OrdShpDate = CDate(objS.OrdShpDate).ToString("yyyy-MM-dd hh:mm:ss")
            Else
                objS.OrdShpDate = ""
            End If
        End If
        objS.OrdShpTrackNo = txtShpTrackNo.Text
        objS.OrdCurrencyCode = dlCurrencyCode.SelectedValue
        objS.OrdCurrencyExRate = txtExRate.Text
        objS.OrdComment = txtNotes.Text
        objS.OrdCustPO = txtCustPO.Text

        'Automatically assign Assign Sale Rep. and rejection of orders
        If dlStatus.SelectedItem.Value = "C" Then
            If dlReason.SelectedItem.Value = 0 Then
                objSO.orderRejectReason = ""
            Else
                objSO.orderRejectReason = dlReason.SelectedItem.Text
                Dim objDC As New clsDataClass
                Dim strSQL As String = "Select UserID from salesrepcustomer where CustomerID='" + hdnCustId.Value + "' limit 1"
                Dim UserID As String = Convert.ToString(objDC.GetScalarData(strSQL))
                objAssLead.CustID = hdnCustId.Value
                objAssLead.UserID = UserID
                objAssLead.CustType = objUser.CustType
                objAssLead.Active = "1"
                If objAssLead.funDuplicateLeads = False Then
                    objAssLead.funInsertAssignLead()
                Else
                    objAssLead.funReassigend()
                End If

                subSetData()
                objLead.funInsertLeadLog()


            End If

        Else
            objSO.orderRejectReason = ""
        End If


        objS.InvRefNo = txtInvNo.Text
        If txtInvDate.Text <> "" Then
            Try
                objS.invDate = DateTime.ParseExact(txtInvDate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
            Catch ex As Exception
                Try
                    objS.invDate = DateTime.ParseExact(txtInvDate.Text, "MM-dd-yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
                Catch ex1 As Exception

                End Try
            End Try
            'objS.invDate = DateTime.ParseExact(txtInvDate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
        Else
            objS.invDate = "sysNull"
        End If

        If txtQutExpDate.Text <> "" Then
            Try
                objS.QutExpDate = DateTime.ParseExact(txtQutExpDate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
            Catch ex As Exception
                Try
                    objS.QutExpDate = DateTime.ParseExact(txtQutExpDate.Text, "MM-dd-yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
                Catch ex1 As Exception
                    objS.QutExpDate = ""
                End Try
            End Try
            'If IsDate(DateTime.ParseExact(txtQutExpDate.Text, "MM/dd/yyyy", Nothing)) = True Then
            '    objS.QutExpDate = DateTime.ParseExact(txtQutExpDate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
            'Else
            '    If IsDate(objS.QutExpDate) = True Then
            '        objS.QutExpDate = CDate(objS.QutExpDate).ToString("yyyy-MM-dd hh:mm:ss")
            '    Else
            '        objS.QutExpDate = ""
            '    End If
            'End If
        Else
            If IsDate(objS.QutExpDate) = True Then
                objS.QutExpDate = CDate(objS.QutExpDate).ToString("yyyy-MM-dd hh:mm:ss")
            Else
                objS.QutExpDate = ""
            End If
        End If
    End Sub
    'Invoice Process 
    Private Function funInvoiceProcess() As Boolean
        Dim bFlag As Boolean = False
        If Request.QueryString("SOID") <> "" Then
            objSO.OrdID = Request.QueryString("SOID")
            objSO.getOrdersInfo()
            Dim objInv As New clsInvoices
            subSetInvoiceData(objInv, objSO)
            If objInv.funCheckInvoiceForOrderNo = False Then
                If objInv.insertInvoices() = True Then
                    Dim strInvID As String
                    strInvID = objInv.funGetInvoiceID()
                    If objInv.InvRefNo = "0" Then
                        objInv.funUpdateInvRefNo(objInv.InvRefType, objInv.InvCompanyID, strInvID)
                    End If
                    Dim objInvItem As New clsInvoiceItems
                    'Populate Invoice Item
                    bFlag = objInvItem.funPopulateInvoiceItem(Request.QueryString("SOID"), strInvID)
                    objInvItem = Nothing
                    'Populate Invoice Item Process
                    Dim objINIP As New clsInvItemProcess
                    bFlag = objINIP.funPopulateInvoiceItemProcess(Request.QueryString("SOID"), strInvID)

                End If
            End If
        End If
        Return bFlag
    End Function

    'Code to craete subscription invoice if order is processed & allow invoice subscription
    Private Function SubscriptionInvoiceProcess() As Boolean
        Dim bFlag As Boolean = False
        If Request.QueryString("SOID") <> "" Then
            objSO.OrdID = Request.QueryString("SOID")
            objSO.getOrdersInfo()
            Dim objOrdInvFreq As New clsOrdInvFrequency
            Dim objInv As New clsInvoices
            subSetInvoiceData(objInv, objSO)
            objInv.InvRefNo = clsCommon.GetRandomNumber()
            If objOrdInvFreq.ValidateSubscriptionInvoice(objSO.OrdID) = True Then
                If objInv.insertInvoices() = True Then
                    Dim strInvID As String
                    strInvID = objInv.funGetInvoiceID()
                    If objInv.InvRefNo = "0" Then
                        objInv.funUpdateInvRefNo(objInv.InvRefType, objInv.InvCompanyID, strInvID)
                    End If
                    Dim objInvItem As New clsInvoiceItems
                    'Populate Invoice Item
                    bFlag = objInvItem.funPopulateInvoiceItem(Request.QueryString("SOID"), strInvID)
                    objInvItem = Nothing
                    'Populate Invoice Item Process
                    Dim objINIP As New clsInvItemProcess
                    bFlag = objINIP.funPopulateInvoiceItemProcess(Request.QueryString("SOID"), strInvID)

                    bFlag = objOrdInvFreq.UpdateLastCratedInvoiceDate(objSO.OrdID)
                End If
            Else

            End If
        End If
        Return bFlag
    End Function

    'Populate Invoice Object
    Private Sub subSetInvoiceData(ByRef objIN As clsInvoices, ByRef objS As clsOrders)
        objIN.InvCustType = objS.OrdCustType
        objIN.InvCustID = objS.OrdCustID
        objIN.InvStatus = "S"
        objIN.InvComment = objS.OrdComment
        objIN.InvLastUpdateBy = Session("UserID").ToString
        objIN.InvLastUpdatedOn = Now.ToString("yyyy-MM-dd hh:mm:ss")
        objIN.InvShpCost = objS.OrdShpCost
        objIN.InvCurrencyCode = objS.OrdCurrencyCode
        objIN.InvCurrencyExRate = objS.OrdCurrencyExRate
        objIN.InvCustPO = objS.OrdCustPO
        objIN.InvShpWhsCode = objS.OrdShpWhsCode
        objIN.InvForOrderNo = objS.OrdID
        objIN.InvCompanyID = objS.ordCompanyID
        objIN.InvRefType = "IV"
        If objS.InvRefNo <> "" Then
            objIN.InvRefNo = objS.InvRefNo
            objIN.InvCreatedOn = CDate(objS.invDate).ToString("yyyy-MM-dd hh:mm:ss")
        Else
            objIN.InvRefNo = 0
            objIN.InvCreatedOn = Now.ToString("yyyy-MM-dd hh:mm:ss")
        End If
        objIN.InvCommission = objS.ordercommission
    End Sub
    Protected Sub cmdSoCancel_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSoCancel.ServerClick
        Response.Redirect("ViewOrderDetails.aspx?SOID=" & strSOID)
    End Sub
    ' On Send Mail Clicks
    'Protected Sub cmdMail_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdMail.ServerClick

    'End Sub
    ' On Send Fax Clicks
    'Protected Sub cmdFax_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFax.ServerClick

    'End Sub
    Protected Sub cmdGnratInvoice_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGnratInvoice.ServerClick
        ' Response.Redirect("~/Invoice/GenerateInvoice.aspx?gnIn=Y&SOID=" + Request.QueryString("SOID"))
        If Not Page.IsValid Then
            'Exit Sub
        End If

        If (dlStatus.SelectedValue = "P" Or dlStatus.SelectedValue = "A") And txtCustPO.Text = "" Then
            'lblMsg.Text = msgSOPlzEntCustomerPO '"Please enter Customer PO."
            'txtCustPO.Focus()
            'Exit Sub
        ElseIf dlStatus.SelectedItem.Text = "" Then
            lblMsg.Text = msgSOPlzSelStatus '"Please select Order Status."
            dlStatus.Focus()
            Exit Sub
        End If
        objSO.OrdID = Request.QueryString("SOID")
        objSO.getOrdersInfo()



        If objSO.OrdStatus = "A" Then
            objSO.OrdStatus = "P"
            objSO.OrdCustPO = txtCustPO.Text
            'objSO.funApproveOrder()
            subSetData(objSO)

            If ConfigurationManager.AppSettings("AllowCustomInvoiceEntry").ToLower = "true" Then
                If objSO.funDuplicateInvoice("IV", objSO.ordCompanyID, objSO.InvRefNo, "") = True Then
                    lblMsg.Text = lblInvoiceExists & "(" & objSO.InvRefNo & ")"
                    txtInvNo.Focus()
                    Exit Sub
                End If
            End If

            If objSO.updateOrders() = True Then
                'Update quantity SORsv and OnHand
                objSO.updateProductQuantityOnProcess()
                'Invoice Process Start
                funInvoiceProcess()
                'Invoice Process End
                objSO.OrdStatus = "P"
                objSO.funUpdateOrder()
                Session.Add("Msg", OrderShippedSuccessfully)
                Dim objInv As New clsInvoices
                objInv.InvForOrderNo = objSO.OrdID
                Response.Redirect("~/invoice/ViewInvoiceDetails.aspx?SOID=" & objInv.funGetInvoiceID & "&ComID=" & objSO.ordCompanyID)
            Else
                Session.Add("Msg", OrderNotShippedSuccessfully)
            End If
        End If
        If objSO.OrdStatus = "P" Then
            If CBSubscription.Checked Then
                If SubscriptionInvoiceProcess() = True Then
                    objSO.OrdStatus = "P"
                    objSO.funUpdateOrder()
                    Session.Add("Msg", InvoiceGeneratedSuccessfully)
                    Dim objInv As New clsInvoices
                    objInv.InvForOrderNo = objSO.OrdID
                    Response.Redirect("~/invoice/ViewInvoiceDetails.aspx?SOID=" & objInv.funGetInvoiceID & "&ComID=" & objSO.ordCompanyID)
                Else
                    lblMsg.Text = "Today is not a valid date to create subscription invoice." 'InvoiceCouldNotCreatedSuccessfully
                End If
            Else
                If funInvoiceProcess() = True Then
                    objSO.OrdStatus = "P"
                    objSO.funUpdateOrder()
                    Session.Add("Msg", InvoiceGeneratedSuccessfully)
                    Dim objInv As New clsInvoices
                    objInv.InvForOrderNo = objSO.OrdID
                    Response.Redirect("~/invoice/ViewInvoiceDetails.aspx?SOID=" & objInv.funGetInvoiceID & "&ComID=" & objSO.ordCompanyID)
                Else
                    'Retrieve Invoice No & let user review the invoice
                    Dim iID As Integer = iTECH.InbizERP.BusinessLogic.ProcessInvoice.GetInvoiceID("IV", objSO.OrdID)
                    If iID > 0 Then
                        Response.Redirect("~/invoice/ViewInvoiceDetails.aspx?SOID=" & iID & "&ComID=" & objSO.ordCompanyID)
                    End If
                    lblMsg.Text = InvoiceProcessError 'InvoiceCouldNotCreatedSuccessfully
                End If
            End If
        Else
            lblMsg.Text = OrderStatusIsNotInProcess '"Order status is not ""In Process""."
        End If
    End Sub
    Protected Sub cmdCpyOrder_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCpyOrder.ServerClick
        objSO.OrdID = Request.QueryString("SOID")
        objSO.getOrdersInfo()
        Response.Redirect("Create.aspx?SOID=" & Request.QueryString("SOID") & "&custID=" & objSO.OrdCustID & "&custName=" & objSO.OrdCustName & "&ComID=" & objSO.ordCompanyID & "&whsID=" & objSO.OrdShpWhsCode)
    End Sub
    Protected Sub cmdCustomerDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCustomerDetail.ServerClick
        Session("OrderEntry") = "?SOID=" & Request.QueryString("SOID") & "&Aprov=" & Request.QueryString("Aprov")
        Response.Redirect("~/Partner/AddEditCustomer.aspx?PartnerID=" & hdnCustId.Value & "&OEnt=1")
    End Sub

    Protected Sub cmdBillToAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBillToAddress.Click
        Dim objAdd As New clsAddress
        objAdd.AddressRef = hdnCustTyp.Value
        objAdd.AddressType = "B"
        objAdd.AddressSourceID = hdnCustId.Value
        objAdd.AddressLine1 = txtBillToAddressLine1.Text
        objAdd.AddressLine2 = txtBillToAddressLine2.Text
        objAdd.AddressLine3 = txtBillToAddressLine3.Text
        objAdd.AddressCity = txtBillToAddressCity.Text
        objAdd.AddressState = txtBillToAddressState.Text
        objAdd.AddressCountry = txtBillToAddressCountry.Text
        objAdd.AddressPostalCode = txtBillToAddressPostalCode.Text
        If objAdd.funCheckDuplicateAddress() = True Then
            objAdd.AddressID = objAdd.funGetAddressID()
            objAdd.updateAddress()
        Else
            objAdd.insertAddress()
        End If
        txtBillToAddress.Text = funPopulateAddress(hdnCustTyp.Value, "B", hdnCustId.Value).Replace("<br>", vbCrLf)
    End Sub
    Protected Sub cmdShpToAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdShpToAddress.Click
        Dim objAdd As New clsAddress
        objAdd.AddressRef = hdnCustTyp.Value
        objAdd.AddressType = "S"
        objAdd.AddressSourceID = hdnCustId.Value
        objAdd.AddressLine1 = txtShpToAddressLine1.Text
        objAdd.AddressLine2 = txtShpToAddressLine2.Text
        objAdd.AddressLine3 = txtShpToAddressLine3.Text
        objAdd.AddressCity = txtShpToAddressCity.Text
        objAdd.AddressState = txtShpToAddressState.Text
        objAdd.AddressCountry = txtShpToAddressCountry.Text
        objAdd.AddressPostalCode = txtShpToAddressPostalCode.Text
        If objAdd.funCheckDuplicateAddress() = True Then
            objAdd.AddressID = objAdd.funGetAddressID()
            objAdd.updateAddress()
        Else
            objAdd.insertAddress()
        End If
        txtShpToAddress.Text = funPopulateAddress(hdnCustTyp.Value, "S", hdnCustId.Value).Replace("<br>", vbCrLf)
        lblAddress.Text = funPopulateAddress(hdnCustTyp.Value, "S", hdnCustId.Value)
    End Sub
    Protected Sub cmdWhsShpToAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdWhsShpToAddress.Click
        Dim objWhs As New clsWarehouses
        objWhs.WarehouseCode = dlShpWarehouse.SelectedValue
        objWhs.WarehouseDescription = txtWhsShpToDesc.Text
        objWhs.WarehouseAddressLine1 = txtWhsShpToAddressLine1.Text
        objWhs.WarehouseAddressLine2 = txtWhsShpToAddressLine2.Text
        objWhs.WarehouseCity = txtWhsShpToAddressCity.Text
        objWhs.WarehouseState = txtWhsShpToAddressState.Text
        objWhs.WarehouseCountry = txtWhsShpToAddressCountry.Text
        objWhs.WarehousePostalCode = txtWhsShpToAddressPostalCode.Text
        objWhs.updateWarehouseAddress()
        txtWhsShpToAddress.Text = funGetWhsAddress(dlShpWarehouse.SelectedValue)
        divItems.Visible = True
    End Sub

    Protected Sub imgAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAdd.Click
        lblMsg.Text = ""
        If IsNumeric(txtQuantity.Text) <> True Then
            lblMsg.Text = msgSOItemQuantity '"Please enter Order Item Quantity."
            txtQuantity.Focus()
            Exit Sub
        End If

        ' If IsNumeric(txtDiscount.Text) = True Then
        If rblstDiscountType.SelectedValue = "P" Then
            If CDbl(txtDiscount.Text.TrimEnd("%")) > 100 Then
                lblMsg.Text = msgDiscountgreaterThen100 '"Discount should not be greater than 100%."
                txtDiscount.Focus()
                Exit Sub
            End If
        End If
        'End If
        If IsNumeric(txtPrice.Text) <> True Then
            lblMsg.Text = msgSOItemPrice '"Please enter Order Item Price."
            txtPrice.Focus()
            Exit Sub
        End If
        If IsNumeric(txtPrice.Text) = True Then
            If CDbl(txtPrice.Text) < 0 Then
                lblMsg.Text = msgSOItemQuantityGT0 '"Please enter Order Item Price greater than 0."
                txtPrice.Focus()
                Exit Sub
            End If
        End If

        Dim objOI As New clsOrderItems
        objOI.OrderItemID = txtOrdItmId.Text
        objOI.OrdID = Request.QueryString("SOID")
        objOI.OrdProductID = txtProductId.Text
        objOI.OrdProductQty = txtQuantity.Text
        objOI.OrdProductUnitPrice = txtPrice.Text
        objOI.ordProductTaxGrp = dlTaxCode.SelectedItem.Value
        objOI.ordProductDiscount = txtDiscount.Text.TrimEnd("%")
        objOI.ordProductDiscountType = rblstDiscountType.SelectedValue
        objOI.orderItemDesc = txtProductName.Text
        objOI.updateOrderItems()
        divItems.Visible = False
        Response.Redirect("~/Sales/Approval.aspx?SOID=" & Request.QueryString("SOID") & "&Aprov=" & Request.QueryString("Aprov"))
    End Sub

    Protected Sub cmdAddDiscount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddDiscount.Click
        divDiscount.Style("display") = "none"
        If txtDiscountVal.Text = "" Then
            txtDiscountVal.Text = "0"
        End If
        txtDiscount.Text = txtDiscountVal.Text
        If rblstDiscountType.SelectedValue = "P" Then
            txtDiscount.Text = txtDiscountVal.Text & "%"
        End If
        txtDiscount.Focus()
        divItems.Visible = True
    End Sub

    'Protected Sub cmdAddDiscount_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddDiscount.ServerClick

    'End Sub

    Protected Sub cmdFax_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFax.Click
        If Request.QueryString("SOID") <> "" Then
            Dim strDocTyp As String = funStatusDocType()
            Dim strContent As String = ""
            Dim objPrn As New clsPrintHTML(Request.QueryString("SOID"))
            strContent = objPrn.PrintRequestDetail(Request.QueryString("SOID"), strDocTyp)

            Dim strFileName As String = Request.QueryString("DocTyp") + "_" + Request.QueryString("SOID") + "_" + Now.ToString("yyyyMMddhhmmss") + ".html"
            Dim strFullName As String = Server.MapPath("~") + "/pdf/" + strFileName
            File.WriteAllText(strFullName, strContent)

            Dim strFaxInfo() As String
            Dim objCI As New clsCompanyInfo
            objSO.OrdID = Request.QueryString("SOID")
            objSO.getOrders()
            objCI.CompanyID = objSO.ordCompanyID
            strFaxInfo = objCI.funGetFirstCompanyFaxInfo().Split("~")
            Dim strMailFrom As String = Session("EmailID").ToString
            Dim strMailTo As String = ""
            Dim strSub As String = txtFaxSubject.Text
            Dim strMsg As String = ""
            If txtFaxTo.Text.Contains(strFaxInfo(0)) = False Then
                strMailTo = objCI.funFaxNumbers(txtFaxTo.Text) & "@" & strFaxInfo(0)
            Else
                strMailTo = txtFaxTo.Text
            End If
            If strFaxInfo(1).Contains("username:") Then
                strMsg += strFaxInfo(1) & vbCrLf
            End If
            If strFaxInfo(2).Contains("code:") Then
                strMsg += strFaxInfo(2) & vbCrLf
            End If
            strMsg += "Attention: " & txtAttention.Text & vbCrLf
            strMsg += "Message: "
            strMsg += txtFaxMessage.Text
            Dim obj As New clsCommon
            If obj.SendFax(strSub, strMsg, strMailTo, strMailFrom, strFullName) = True Then
                Dim objDoc As New clsDocuments
                objDoc.SysDocType = strDocTyp
                objDoc.SysDocDistType = "F"
                objDoc.SysDocDistDateTime = Now.ToString("yyyy-MM-dd hh:mm:ss")
                objDoc.SysDocPath = strFullName
                objDoc.SysDocEmailTo = ""
                objDoc.SysDocFaxToPhone = strMailTo
                objDoc.SysDocFaxToAttention = txtAttention.Text
                objDoc.SysDocBody = txtFaxMessage.Text
                objDoc.SysDocSubject = strSub
                objDoc.insertDocuments()
                objDoc = Nothing
            End If
            obj = Nothing
        End If
    End Sub

    Protected Sub cmdMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdMail.Click
        If Request.QueryString("SOID") <> "" Then
            Dim strDocTyp As String = funStatusDocType()
            Dim strContent As String = ""
            Dim objPrn As New clsPrintHTML(Request.QueryString("SOID"))
            strContent = objPrn.PrintRequestDetail(Request.QueryString("SOID"), strDocTyp)

            Dim strFileName As String = Request.QueryString("DocTyp") + "_" + Request.QueryString("SOID") + "_" + Now.ToString("yyyyMMddhhmmss") + ".html"
            Dim strFullName As String = Server.MapPath("~") + "/pdf/" + strFileName
            File.WriteAllText(strFullName, strContent)

            Dim strMailFrom As String = Session("EmailID").ToString
            Dim strMailTo As String = txtMailTo.Text
            Dim strSub As String = txtMailSubject.Text
            Dim strMsg As String = ""
            strMsg = txtMailMessage.Text
            Dim obj As New clsCommon
            If obj.SendMail(strSub, strMsg, strMailTo, strMailFrom, strFullName) = True Then
                Dim objDoc As New clsDocuments
                objDoc.SysDocType = strDocTyp
                objDoc.SysDocDistType = "E"
                objDoc.SysDocDistDateTime = Now.ToString("yyyy-MM-dd hh:mm:ss")
                objDoc.SysDocPath = strFullName
                objDoc.SysDocEmailTo = strMailTo
                objDoc.SysDocFaxToPhone = ""
                objDoc.SysDocFaxToAttention = ""
                objDoc.SysDocBody = strMsg
                objDoc.SysDocSubject = strSub
                objDoc.insertDocuments()
                objDoc = Nothing
            End If
            obj = Nothing
        End If
    End Sub

    Protected Sub lbnPriceRange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbnPriceRange.Click
        Dim objProductSalePrice As New clsProductSalesPrice
        objProductSalePrice.ProductID = txtProductId.Text
        sqldsPriceRange.SelectCommand = objProductSalePrice.getGridViewFillerSQL()
        objProductSalePrice = Nothing
        divPriceRnge.Style("Display") = "block"
        divItems.Visible = True
    End Sub

    Protected Sub cmdSuggestedPriceOK_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSuggestedPriceOK.ServerClick
        If (hdnSelectedId.Value <> "non") Then
            divPriceRnge.Style("Display") = "none"
            Dim objProductSalePrice As New clsProductSalesPrice
            objProductSalePrice.ProductSalesPriceID = hdnSelectedId.Value
            objProductSalePrice.subGetProductSalesPriceInfoById()

            txtQuantity.Text = objProductSalePrice.FromQty
            txtPrice.Text = objProductSalePrice.SalesPrice
            objProductSalePrice = Nothing
        End If
        divItems.Visible = True
    End Sub

    Protected Sub cmdNotes_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNotes.ServerClick
        If Request.Url.AbsoluteUri.Contains("&PartnerID=") Then
            Response.Redirect("~/partner/Notes.aspx" & Request.UrlReferrer.Query & "&SA=1")
        Else
            Response.Redirect("~/partner/Notes.aspx" & Request.UrlReferrer.Query & "&PartnerID=" & hdnCustId.Value & "&SA=1")
        End If
    End Sub
    Private Sub subSetData()
        objLead.AssignLeadsID = hdnCustId.Value

        Dim Status_AM_PM As String
        Status_AM_PM = Hour(Now).ToString.Trim
        If Hour(Now).ToString.Trim <= 11 Then
            Status_AM_PM = "AM"

        ElseIf Hour(Now).ToString.Trim >= 12 Then
            Status_AM_PM = "PM"
        End If

        objLead.LeadsFollowup = DateTime.ParseExact(Now.ToString("MM/dd/yyyy"), "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd") & " " & fundate(Hour(Now).ToString.Trim, Status_AM_PM) & ":" & funMin(Minute(Now).ToString.Trim) & ":00"
        objLead.LeadsFollowupUserID = Session("UserID")
        objLead.LeadsLogText = "Rejected: " + dlReason.SelectedItem.Text
        objLead.LeadsActivityDatetime = Now.ToString("yyyy-MM-dd HH:mm:ss")
    End Sub
    Public Function fundate(ByVal intHour As String, ByVal strType As String) As String
        Dim intValue As String
        If strType = "PM" Then
            If intHour <= 11 Then
                intValue = intHour + 12
            Else
                intValue = intHour
            End If
        ElseIf strType = "AM" Then
            If intHour = "12" Then
                intValue = "00"
            ElseIf intHour = "11" Then
                intValue = intHour
            End If
            If intHour <= 9 Then
                intValue = "0" & intHour
            End If
        End If
        Return intValue
    End Function
    Public Function funMin(ByVal intMin As String) As String
        Dim intValue As String
        If intMin <= 9 Then
            If intMin = "00" Then
                intValue = intMin
            Else
                intValue = "0" & intMin
            End If
        Else
            intValue = intMin
        End If
        Return intValue
    End Function
End Class

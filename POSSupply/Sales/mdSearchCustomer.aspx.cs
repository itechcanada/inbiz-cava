﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using iTECH.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;

using Trirand.Web.UI.WebControls;

public partial class Sales_mdSearchCustomer : BasePage
{
    Partners _part = new Partners();

    protected void Page_Load(object sender, EventArgs e)
    {

        int ordType = BusinessUtility.GetInt(Request.QueryString["orderType"]);
        string invokSrc = BusinessUtility.GetString(Request.QueryString["invokSrc"]);
        txtSearch.Text = Request.QueryString["keyword"] != null ? Request.QueryString["keyword"] : string.Empty;
        if (ordType == 2)
        {
            lblcompanyID.Visible = false;
            dlWarehouses.Visible = false;
        }
        else
        {
            tblTran.Visible = false;
        }


        if (invokSrc == "Invoice")
        {
            lblcompanyID.Visible = false;
            dlWarehouses.Visible = false;
        }

        if (!IsPagePostBack(grdCustomers))
        {
            if (!CurrentUser.IsAutheticated)
            {
                pnlSession.Visible = true;
                pnlResults.Visible = false;
            }
            else
            {
                pnlSession.Visible = false;
                pnlResults.Visible = true;
            }

            string isPOSSearch = BusinessUtility.GetString(Request.QueryString["orderFrom"]);
            if (!string.IsNullOrEmpty(isPOSSearch))
            {
                btnAddCustomer.OnClientClick = string.Format("parent.location.href='{0}?pType=D&gType=0&create_POS=1&InvokeSrc=POS'; return false;", ResolveUrl("~/Partner/CustomerEdit.aspx"));
            }
            else
            {
                btnAddCustomer.OnClientClick = string.Format("parent.location.href='{0}?pType=E&gType=1&create_sales=1'; return false;", ResolveUrl("~/Partner/CustomerEdit.aspx"));
            }
            SysWarehouses wh = new SysWarehouses();

            string sWhsCodeTobeSelected = CurrentUser.UserDefaultWarehouse;
            if (this.WarehouseCode != "")
            {
                sWhsCodeTobeSelected = this.WarehouseCode;
            }

            wh.FillWharehouse(null, dlWarehouses, sWhsCodeTobeSelected, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));
            //wh.FillWharehouse(null, dlFromWhs, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation,"0"));
            if (invokSrc == "POS")
            {
                wh.FillWharehouse(null, dlFromWhs, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem("", "0"));
                dlFromWhs.SelectedIndex = 0;
                //wh.FillWharehouse(null, dlToWhs, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));
                wh.FillWharehouse(null, dlToWhs, sWhsCodeTobeSelected, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));
            }
            else
            {
                wh.FillAllWharehouse(null, dlFromWhs, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem("", "0"));
                dlFromWhs.SelectedIndex = 0;
                //wh.FillWharehouse(null, dlToWhs, CurrentUser.UserDefaultWarehouse, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));
                wh.FillAllWharehouse(null, dlToWhs, sWhsCodeTobeSelected, CurrentUser.DefaultCompanyID, new ListItem(Resources.Resource.liWarehouseLocation, "0"));
            }
        }
        //if ((MessageState.Current.Message == Resources.Resource.msgFormWareHouseRequired) || (MessageState.Current.Message == Resources.Resource.msgFormToNotSame))
        //{
        //    dlFromWhs.Focus();
        //}
        //else if (BusinessUtility.GetString(MessageState.Current.Message) == "")
        //{
        //    txtSearch.Focus();
        //}
        //MessageState.SetGlobalMessage(MessageType.Success, "");
        //MessageState.Clear();
    }

    protected void grdCustomers_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        string data = "";

        if (e.ColumnIndex == 1) //Set Customer Name & Address            
        {
            data = BusinessUtility.GetString(e.RowValues[1]) + " ";
            data += BusinessUtility.GetString(e.RowValues[2]);
            _part.PopulateObject(BusinessUtility.GetInt(e.RowKey));
            Addresses addr = new Addresses();
            string pType = Globals.GetPartnerType(_part.PartnerType);
            addr.PopulateObject(BusinessUtility.GetInt(e.RowKey), pType, AddressType.BILL_TO_ADDRESS);
            e.CellHtml = data.Trim();
        }
        else if (e.ColumnIndex == 2)
        {
            double balAmt = ProcessInvoice.GetCustomerUnpaidAmount(BusinessUtility.GetInt(e.RowKey));
            string style = balAmt > 0 ? "style='color:red;'" : "";

            data = string.Format("<div {0}>{1} : {2} <br >", style, Resources.Resource.msgTtlNoSO, e.RowValues[3]);
            data += string.Format("{0} : {1} <br >", Resources.Resource.msgTtlNoInv, e.RowValues[4]);
            data += string.Format("{0} : {1:F} <br >", Resources.Resource.totalAmtRcvd, BusinessUtility.GetDouble(e.RowValues[5]));
            data += string.Format("{0} : {1} <br >", Resources.Resource.LastAmtRcvdOn, BusinessUtility.GetDateTime(e.RowValues[6]) != DateTime.MinValue ? BusinessUtility.GetDateTime(e.RowValues[6]).ToString("MMM dd, yyyy") : "---");
            data += string.Format("{0} : {1:F} </div>", Resources.Resource.lblUnPaidAmount, balAmt);

            e.CellHtml = data.Trim();
        }
    }

    protected void grdCustomers_DataRequesting(object sender, JQGridDataRequestEventArgs e)
    {
        bool isSalesRescricted = CurrentUser.IsInRole(RoleID.SALES_REPRESENTATIVE) && BusinessUtility.GetBool(Session["SalesRepRestricted"]);
        string isPOSSearch = BusinessUtility.GetString(Request.QueryString["orderFrom"]);
        if (!string.IsNullOrEmpty(isPOSSearch) && string.IsNullOrEmpty(Request.QueryString[txtSearch.ClientID]))
        {
            sdsCustomers.SelectCommand = "";
        }
        else
        {
            if (Request.QueryString.AllKeys.Contains<string>("_history"))
            {
                sdsCustomers.SelectCommand = _part.GetSearchSql(sdsCustomers.SelectParameters, Request.QueryString[txtSearch.ClientID], CurrentUser.UserID, isSalesRescricted,0);
            }
            else
            {
                sdsCustomers.SelectCommand = _part.GetSearchSql(sdsCustomers.SelectParameters, txtSearch.Text, CurrentUser.UserID, isSalesRescricted,0);
            }
        }
    }

    protected void grdCustomers_RowSelecting(object sender, JQGridRowSelectEventArgs e)
    {
        _part.PopulateObject(BusinessUtility.GetInt(e.RowKey));
        string url = "";
        string isPOSSearch = BusinessUtility.GetString(Request.QueryString["orderFrom"]);
        if (!string.IsNullOrEmpty(isPOSSearch))
        {
            Globals.RegisterCloseDialogScript(Page, string.Format("parent.setCurrentGuest({0});", e.RowKey), true);
        }
        else
        {
            int ordType = BusinessUtility.GetInt(Request.QueryString["orderType"]);
            string invokSrc = BusinessUtility.GetString(Request.QueryString["invokSrc"]);
            if (invokSrc == "Invoice")
            {
                Globals.RegisterCloseDialogScript(Page, string.Format("parent.setCurrentGuest({0});", e.RowKey), true);
            }
            if (ordType == 2)
            {
                if (ValidatePage() == false)
                {
                    return;
                }

                if (invokSrc == "POS")
                {
                    url = ResolveUrl("~/Sales/SalesEdit.aspx") + string.Format("?custID={0}&custName={1}&ComID={2}&FromWhsID={3}&ToWhsID={4}&orderType={5}&InvokeSrc={6}&ShowLPannel=true", e.RowKey, _part.PartnerLongName, CurrentUser.DefaultCompanyID, dlFromWhs.SelectedValue, dlToWhs.SelectedValue, ordType, "POS");
                }
                else
                {
                    url = ResolveUrl("~/Sales/SalesEdit.aspx") + string.Format("?custID={0}&custName={1}&ComID={2}&FromWhsID={3}&ToWhsID={4}&orderType={5}", e.RowKey, _part.PartnerLongName, CurrentUser.DefaultCompanyID, dlFromWhs.SelectedValue, dlToWhs.SelectedValue, ordType);
                }
            }
            else if (ordType == (int)OrderCommission.Work)
            {
                url = ResolveUrl("~/Sales/SalesEdit.aspx") + string.Format("?custID={0}&custName={1}&ComID={2}&whsID={3}&orderType={4}", e.RowKey, _part.PartnerLongName, CurrentUser.DefaultCompanyID, dlWarehouses.SelectedValue, ordType);
                //url = ResolveUrl("~/Sales/SalesEdit.aspx") + string.Format("?custID={0}&custName={1}&ComID={2}&FromWhsID={3}&ToWhsID={4}&orderType={5}", e.RowKey, _part.PartnerLongName, CurrentUser.DefaultCompanyID, dlFromWhs.SelectedValue, dlToWhs.SelectedValue, ordType);
            }
            else
            {
                url = ResolveUrl("~/Sales/SalesEdit.aspx") + string.Format("?custID={0}&custName={1}&ComID={2}&whsID={3}", e.RowKey, _part.PartnerLongName, CurrentUser.DefaultCompanyID, dlWarehouses.SelectedValue);
            }


            Globals.RegisterParentRedirectPageUrl(this, url);
        }
    }

    protected Boolean ValidatePage()
    {
        if (dlFromWhs.SelectedValue == "0")
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgFormWareHouseRequired);
            return false;
        }
        else if (dlFromWhs.SelectedValue == dlToWhs.SelectedValue)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgFormToNotSame);
            return false;
        }
        return true;
    }

    public string WarehouseCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["whsCode"]);
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    CodeFile="ViewSalesOrder.aspx.cs" Inherits="Sales_ViewSalesOrder" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
</asp:Content>
<asp:Content ID="cntTop" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <%= Resources.Resource.lblAdministration%></h1>
        <b>
            <asp:Literal ID="ltrTitle" runat="server"></asp:Literal>
            <%--            <%= Resources.Resource.lblView%>:
            <%= Resources.Resource.lblSalesOrder%>--%>
        </b>
    </div>
    <div style="float: left;">
        <asp:Button ID="btnCreateOrder" runat="server" Text="<%$Resources:Resource, btnCreateOrder%>"
            CausesValidation="false" OnClientClick="return createOrder();" />
        <%-- <iCtrl:IframeDialog ID="mdSearch" TriggerControlID="btnCreateOrder" Width="950" Height="460"
            Url="mdSearchCustomer.aspx" Title="<%$Resources:Resource, lblSelectCustomer%>" runat="server" >
        </iCtrl:IframeDialog> --%>
    </div>
    <div style="float: right;">
        <asp:RadioButtonList ID="rblstOrderType" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
        </asp:RadioButtonList>
        <asp:LinkButton ID="imgCsv" runat="server" CssClass="inbiz_icon icon_export_28" Text="Export To Excel"
            OnClick="imgCsv_Click" />
        <asp:LinkButton ID="lnkAddEditVendor" runat="server" Visible="false"></asp:LinkButton>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="cntMaster" ContentPlaceHolderID="cphMaster" runat="Server">
    <div>
        <div onkeypress="return disableEnterKey(event)">
            <div id="grid_wrapper" style="width: 100%;">
                <trirand:JQGrid runat="server" ID="jgdvSalesOrderView" DataSourceID="sqldsSalesOrderView"
                    Height="300px" AutoWidth="true" OnCellBinding="jgdvSalesOrderView_CellBinding"
                    OnDataRequesting="jgdvSalesOrderView_DataRequesting" OnPreRender="jgdvSalesOrderView_PreRender">
                    <Columns>
                        <trirand:JQGridColumn DataField="ordID" HeaderText="<%$ Resources:Resource, grdSOOrderNo %>"
                            PrimaryKey="True" />
                        <trirand:JQGridColumn DataField="PartnerAcronyme" HeaderText="<%$ Resources:Resource, grdCMAcronyme %>"
                            Editable="false" Visible="false" />
                        <trirand:JQGridColumn DataField="CustomerName" HeaderText="<%$ Resources:Resource, grdSOCustomerName %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="ordShpWhsCode" HeaderText="<%$ Resources:Resource, lblFromWhs %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="ordShpToWhsCode" HeaderText="<%$ Resources:Resource, lblToWhs %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="amount" HeaderText="<%$ Resources:Resource, POTotalAmount %>"
                            Editable="false" TextAlign="Right" />
                        <trirand:JQGridColumn DataField="ordID" HeaderText="<%$ Resources:Resource, grdSOAmountPaid %>"
                            Editable="false" TextAlign="Right" />
                        <trirand:JQGridColumn DataField="ordID" HeaderText="<%$ Resources:Resource, grdSOTotalBalance %>"
                            Editable="false" TextAlign="Right" />
                        <trirand:JQGridColumn DataField="ordID" HeaderText="CheckIn-CheckOut" Editable="false"
                            Visible="false" />
                        <trirand:JQGridColumn DataField="ordDate" HeaderText="<%$ Resources:Resource, grdSOCreatedDate %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="ordStatus" HeaderText="<%$ Resources:Resource, grdPOStatus %>"
                            Editable="false" />
                        <trirand:JQGridColumn DataField="orderTypeDesc" HeaderText="<%$ Resources:Resource, grduserOderType %>"
                            Editable="false" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="ordID"
                            Sortable="false" TextAlign="Center" Width="80" />
                        <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdPrint %>" DataField="ordID"
                            Sortable="false" TextAlign="Center" Width="80" Visible="false" />
                    </Columns>
                    <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn=""></SortSettings>
                    <AppearanceSettings AlternateRowBackground="True" HighlightRowsOnHover="True" />
                    <ClientSideEvents LoadComplete="loadComplete" />
                </trirand:JQGrid>
                <asp:SqlDataSource ID="sqldsSalesOrderView" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                    ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
            </div>
            <br />
            <div class="div_command">
                <asp:Button ID="btnBack" Text="<%$Resources:Resource, lblGoBack%>" CausesValidation="false"
                    runat="server" OnClick="btnBack_Click" />
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hndIsPrintMerchantCopy" runat="server" />
</asp:Content>
<asp:Content ID="cntLeft" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
        <asp:Panel runat="server" ID="SearchPanel">
            <div class="searchBar">
                <div class="header">
                    <div class="title">
                        <%= Resources.Resource.lblSearchForm%>
                    </div>
                    <div class="icon">
                        <img src="../lib/image/iconSearch.png" style="width: 17px" />
                    </div>
                </div>
                <h4>
                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"
                        AssociatedControlID="ddlSearch" CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlSearch" runat="server" Width="175px">
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lblStatus %>"
                        AssociatedControlID="ddlSOStatus" CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:DropDownList ID="ddlSOStatus" runat="server" Width="175px">
                    </asp:DropDownList>
                </div>
                <h4>
                    <asp:Label ID="lblSearchKeyword" AssociatedControlID="txtSearch" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword %>"
                        CssClass="filter-key"></asp:Label></h4>
                <div class="inner">
                    <asp:TextBox runat="server" Width="180px" ID="txtSearch"></asp:TextBox>
                </div>
                <div class="footer">
                    <div class="submit">
                        <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                    </div>
                    <br />
                </div>
            </div>
            <br />
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="inner">
        </div>
        <div class="clear">
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $grid = $("#<%=jgdvSalesOrderView.ClientID%>");
        $grid.initGridHelper({
            searchPanelID: "<%=SearchPanel.ClientID %>",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize() {
            $("#<%=jgdvSalesOrderView.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }

        $("#<%=rblstOrderType.ClientID%>").buttonset();

        $("#<%=rblstOrderType.ClientID%> :radio").click(function () {

            //            $("#<%=ddlSOStatus.ClientID%> >option[value='A']").remove();
            //            $("#<%=ddlSOStatus.ClientID%> >option[value='P']").remove();
            //            $("#<%=ddlSOStatus.ClientID%> >option[value='H']").remove();
            //            $("#<%=ddlSOStatus.ClientID%> >option[value='K']").remove();
            //            $("#<%=ddlSOStatus.ClientID%> >option[value='S']").remove();

            var postData = {};
            postData["oType"] = $(this).val();
            $grid.jqCustomSearch(postData);
        });

        function loadComplete(data) {
            //            if (data.rows.length == 1 && data.total == 1) {
            //                location.href = 'ViewOrderDetails.aspx?SOID=' + data.rows[0].id;
            //            }
            //            else {
            jqGridResize();
            //            }
        }
        function createOrder() {
            dataToPost = {}
            dataToPost.orderType = $('#<%=rblstOrderType.ClientID %> input[type=radio]:checked').val();
            var invokSrc = $.getParamValue('InvokeSrc');
            var whsCode = $.getParamValue('whsCode');
            var vordType = $.getParamValue('ordType');
            var vTitle = "<%=Resources.Resource.lblOrderSelectCustomer %>"
            if (invokSrc == "POS") {
                dataToPost.invokSrc = invokSrc;
                dataToPost.keyword = $.getParamValue('custID');
                dataToPost.whsCode = whsCode;
            }


            chkType = $('#<%=rblstOrderType.ClientID %> input[type=radio]:checked').val();

            if (chkType == "2") {
                vTitle = "<%=Resources.Resource.lblTransferSelectCustomer %>"
            }


            if ((invokSrc == "POS") && vordType == "2") {
                var vTitle = "<%=Resources.Resource.lblTransferSelectWarehouse %>";
            }

            var vBackToOrder = $.getParamValue('BackFromOrder');
            if (vBackToOrder != "1") {
                var $dialog = jQuery.FrameDialog.create({
                    url: 'mdSearchCustomer.aspx?' + $.param(dataToPost),
                    title: vTitle, // "<%=Resources.Resource.lblSelectCustomer %>"
                    //loadingClass: "loading-image",
                    modal: true,
                    width: 900,
                    height: 460,
                    autoOpen: false
                });
                $dialog.dialog('open');
                }
                return false;
            }

            $.extend({
                getParamValue: function (paramName) {
                    parName = paramName.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
                    var pattern = '[\\?&]' + paramName + '=([^&#]*)';
                    var regex = new RegExp(pattern);
                    var matches = regex.exec(window.location.href);
                    if (matches == null) return '';
                    else return decodeURIComponent(matches[1].replace(/\+/g, ' '));
                }
            });

            $(document).ready(function () {
                var invokSrc = $.getParamValue('InvokeSrc');
                var ordTypePOS = $.getParamValue('ordType');
                if (invokSrc == "POS" && ordTypePOS == "2") {
                    createOrder();
                }
            });



            function printReceipt(printOrdID) {
                window.open('./PrintOrder.aspx?ordID=' + printOrdID + '&type=layway', 'Printing Receipt', 'width=300,height=300,toolbar=0,menubar=0,location=0,scrollbars=0,resizable=0,directories=0,status=0');

                if ($("#<%=hndIsPrintMerchantCopy.ClientID%>").val() == "1") {
                window.open('./PrintOrder.aspx?ordID=' + printOrdID + '&type=layway', 'Merchant Receipt', 'width=300,height=300,toolbar=0,menubar=0,location=0,scrollbars=0,resizable=0,directories=0,status=0');
            }
        }
    </script>
</asp:Content>

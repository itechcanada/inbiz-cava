Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Partial Class Sales_CreateSalesProcess
    Inherits BasePage
    Private strProcessID As String = ""
    Private objProGrp As New clsProcessGroup
    Dim objInvIP As New clsInvItemProcess
    Dim objOrdIP As New clsOrderItemProcess
    Public objTAx As New clsTaxCodeDesc
    'Private objSO As New clsOrders
    Dim objDT_SP As System.Data.DataTable
    Dim objDR_SP As System.Data.DataRow
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            subClose()
            Exit Sub
            'Response.Redirect("~/AdminLogin.aspx")
        End If
        If (Request.Browser.Browser = "Firefox") Then
            Response.Cache.SetNoStore()
        End If
        If Request.QueryString("custID") = "" Then
            Response.Redirect("Generate.aspx")
        End If
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If
        If Not IsPostBack Then
            lblTitle.Text = lblSOAddSalesProcess '"Add Sales Process"
            objTAx.PopulateTaxGroup(dlTaxCode)
            'subSearch()
            If Convert.ToString(Session("IsSalesProcessCart")) <> "Yes" Then
                makeCart()
                Session("IsSalesProcessCart") = "Yes"
            End If
            If Request.QueryString("SOID") = "" Then
                If Convert.ToString(Session("SalesProcessCart")) <> "" Then
                    grdAddProcLst.DataSource = Session("SalesProcessCart")
                    grdAddProcLst.DataBind()
                End If
            Else
                ' Session.Remove("SalesProcessCart")
            End If
            txtSearch.Focus()
        End If
    End Sub

    ' Make Cart
    Sub makeCart()
        objDT_SP = New System.Data.DataTable("Cart")
        objDT_SP.Columns.Add("ID", GetType(Integer))
        objDT_SP.Columns("ID").AutoIncrement = True
        objDT_SP.Columns("ID").AutoIncrementSeed = 1

        objDT_SP.Columns.Add("ProcessID", GetType(Integer))

        objDT_SP.Columns.Add("ProcessCode", GetType(String))
        'Add the 2 field one is Tax Grp d and Tax Grp Name 
        objDT_SP.Columns.Add("TaxGrpID", GetType(Integer))
        objDT_SP.Columns.Add("TaxGrpName", GetType(String))

        objDT_SP.Columns.Add("ProcessDescription", GetType(String))
        objDT_SP.Columns.Add("ProcessFixedCost", GetType(Double))
        objDT_SP.Columns.Add("ProcessCostPerHour", GetType(Double))
        objDT_SP.Columns.Add("ProcessCostPerUnit", GetType(Double))
        objDT_SP.Columns.Add("TotalHour", GetType(Integer))
        objDT_SP.Columns.Add("TotalUnit", GetType(Integer))

        Session("SalesProcessCart") = objDT_SP
    End Sub
    ' Add Cart
    Sub AddToCart()
        If Convert.ToString(Session("SalesProcessCart")) <> "" Then

            objDT_SP = Session("SalesProcessCart")
            Dim Product = txtProcessID.Text
            Dim blnMatch As Boolean = False

            For Each objDR_SP In objDT_SP.Rows
                If objDR_SP("ProcessID") = Product Then
                    blnMatch = True
                    Exit For
                End If
            Next
            If Request.QueryString("SOID") <> "" Then
                If Request.QueryString("Type") = "SO" Then
                    objOrdIP.OrdID = Request.QueryString("SOID")
                    objOrdIP.OrdItemProcCode = txtProcessCode.Text
                    If objOrdIP.funDuplicateProcess() = True Then
                        lblMsg.Text = msgSOProcessAlreadyExist '"Process already exist."
                        Exit Sub
                    End If
                Else
                    objInvIP.Invoices_invID = Request.QueryString("SOID")
                    objInvIP.InvItemProcCode = txtProcessCode.Text
                    If objInvIP.funDuplicateProcess() = True Then
                        lblMsg.Text = msgSOProcessAlreadyExist '"Process already exist."
                        Exit Sub
                    End If
                End If
               
            End If

            If Not blnMatch Then
                objDR_SP = objDT_SP.NewRow
                objDR_SP("ProcessID") = txtProcessID.Text
                objDR_SP("ProcessCode") = txtProcessCode.Text
                objDR_SP("ProcessDescription") = txtProcessDescription.Text

                If IsNumeric(txtProcessFixedCost.Text) = True Then
                    objDR_SP("ProcessFixedCost") = txtProcessFixedCost.Text
                Else
                    Exit Sub
                End If
                If IsNumeric(txtProcessCostPerHour.Text) = True Then
                    objDR_SP("ProcessCostPerHour") = txtProcessCostPerHour.Text
                Else
                    Exit Sub
                End If
                If IsNumeric(txtTotalHour.Text) = True Then
                    objDR_SP("TotalHour") = txtTotalHour.Text
                Else
                    Exit Sub
                End If
                If IsNumeric(txtProcessCostPerUnit.Text) = True Then
                    objDR_SP("ProcessCostPerUnit") = txtProcessCostPerUnit.Text
                Else
                    Exit Sub
                End If
                If IsNumeric(txtTotalUnit.Text) = True Then
                    objDR_SP("TotalUnit") = txtTotalUnit.Text
                Else
                    Exit Sub
                End If
                '' Add the 2 column value in this table
                If dlTaxCode.SelectedIndex = 0 Then
                    objDR_SP("TaxGrpID") = 0
                    objDR_SP("TaxGrpName") = "Warehouse Tax"
                Else
                    objDR_SP("TaxGrpID") = Convert.ToInt16(dlTaxCode.SelectedValue.ToString())
                    objDR_SP("TaxGrpName") = dlTaxCode.SelectedItem.ToString()
                End If
               

                objDT_SP.Rows.Add(objDR_SP)
            Else
                lblMsg.Text = msgSOProcessAlreadyExist '"Process already exist."
                Exit Sub
            End If
            lblMsg.Text = msgSOProcessAddedSuccessfully '"Process Added Successfully"
        Else
            lblMsg.Text = ""
        End If
    End Sub

    ' On Row Deleting
    Protected Sub grdAddProcLst_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdAddProcLst.RowDeleting
        If Convert.ToString(Session("SalesProcessCart")) <> "" Then
            objDT_SP = Session("SalesProcessCart")
            objDT_SP.Rows(e.RowIndex).Delete()
            Session("SalesProcessCart") = objDT_SP
            grdAddProcLst.DataSource = objDT_SP
            grdAddProcLst.DataBind()
            lblMsg.Text = msgSOProcessRemovedFromList '"Process Removed From List"
        End If
    End Sub
    'Editing
    Protected Sub grdProcess_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdProcess.RowEditing
        strProcessID = grdProcess.DataKeys(e.NewEditIndex).Value.ToString()
        divItems.Style("display") = "block"
        txtProcessID.Text = strProcessID
        txtProcessCode.Text = grdProcess.Rows(e.NewEditIndex).Cells(0).Text
        txtProcessDescription.Text = grdProcess.Rows(e.NewEditIndex).Cells(1).Text.Replace("&nbsp;", "")
        txtProcessFixedCost.Text = grdProcess.Rows(e.NewEditIndex).Cells(2).Text.Replace("&nbsp;", "")
        txtProcessCostPerHour.Text = grdProcess.Rows(e.NewEditIndex).Cells(3).Text.Replace("&nbsp;", "")
        txtProcessCostPerUnit.Text = grdProcess.Rows(e.NewEditIndex).Cells(4).Text.Replace("&nbsp;", "")
        txtTotalHour.Text = ""
        txtTotalUnit.Text = ""
    End Sub
    ' Sql Data Source Event Track
    Protected Sub sqldsProcess_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsProcess.Selected
        If e.AffectedRows = 0 Then
            lblMsg.Text = NoDataFound
        End If
    End Sub
    ' Search Sub Routine
    Private Sub subSearch()
        If txtSearch.Text <> "" Then
            sqldsProcess.SelectCommand = objProGrp.funFillGrid(txtSearch.Text)  'fill Process Record
            lblMsg.Text = ""
        Else
            sqldsProcess.SelectCommand = objProGrp.funFillGrid()  'fill Process Record
            lblMsg.Text = ""
            txtSearch.Focus()
        End If
        divItems.Style("display") = "none"
    End Sub
    ' On Search Image Click
    Protected Sub imgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
        subSearch()
    End Sub
    
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    'On Product Items Add in List
    'Protected Sub imgAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAdd.Click

    'End Sub
    '' On Save
    'Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click

    'End Sub
    Public Sub subSaveProcess(ByVal strID As String)

        If Session("SalesProcessCart").ToString <> "" Then
            objDT_SP = Session("SalesProcessCart")
            For idx As Int16 = 0 To objDT_SP.Rows.Count - 1
                If Request.QueryString("Type") = "SO" Then
                    subSetDataSOProcess(idx)
                    objOrdIP.OrdID = strID
                    If objOrdIP.insertOrderItemProcess() = True Then 'Insert Sales inv Item Process

                    End If
                Else
                    subSetDataProcess(idx)
                    objInvIP.Invoices_invID = strID
                    If objInvIP.insertInvItemProcess() = True Then 'Insert Sales inv Item Process
                      
                    End If

                End If
                
            Next
            Session("SalesCart") = ""
            Session("IsSalesCart") = ""
            Session("SalesProcessCart") = ""
            Session("IsSalesProcessCart") = ""
        End If
    End Sub
    ' Populate Object
    Private Sub subSetDataProcess(ByVal strEditItem As String)
        If strEditItem <> "" And Convert.ToString(Session("SalesProcessCart")) <> "" Then
            objDT_SP = Session("SalesProcessCart")
            objDR_SP = objDT_SP.Rows(strEditItem)
            objInvIP.InvItemProcCode = objDR_SP("ProcessCode").ToString
            objInvIP.InvItemProcFixedPrice = objDR_SP("ProcessFixedCost").ToString
            objInvIP.InvItemProcPricePerHour = objDR_SP("ProcessCostPerHour").ToString
            objInvIP.InvItemProcHours = objDR_SP("TotalHour").ToString
            objInvIP.InvItemProcPricePerUnit = objDR_SP("ProcessCostPerUnit").ToString
            objInvIP.InvItemProcUnits = objDR_SP("TotalUnit").ToString
        End If
    End Sub
    ' Populate Object
    Private Sub subSetDataSOProcess(ByVal strEditItem As String)
        If strEditItem <> "" And Convert.ToString(Session("SalesProcessCart")) <> "" Then
            objDT_SP = Session("SalesProcessCart")
            objDR_SP = objDT_SP.Rows(strEditItem)
            objOrdIP.OrdItemProcCode = objDR_SP("ProcessCode").ToString
            objOrdIP.OrdItemProcFixedPrice = objDR_SP("ProcessFixedCost").ToString
            objOrdIP.OrdItemProcPricePerHour = objDR_SP("ProcessCostPerHour").ToString
            objOrdIP.OrdItemProcHours = objDR_SP("TotalHour").ToString
            objOrdIP.OrdItemProcPricePerUnit = objDR_SP("ProcessCostPerUnit").ToString
            objOrdIP.OrdItemProcUnits = objDR_SP("TotalUnit").ToString
        End If
    End Sub

    ' Populate Object
    Private Sub subSetData(ByRef objOI As clsOrderItems, ByVal strEditItem As String)
        If strEditItem <> "" And Convert.ToString(Session("SalesProcessCart")) <> "" Then
            objDT_SP = Session("SalesProcessCart")
            objDR_SP = objDT_SP.Rows(strEditItem)
            objOI.OrdProductID = objDR_SP("ProductID").ToString
            objOI.OrdProductQty = objDR_SP("Quantity").ToString
            objOI.OrdProductUnitPrice = objDR_SP("Price").ToString
            objOI.OrdWarehouse = objDR_SP("WarehouseCode").ToString
        End If
    End Sub
    'Return SOID 
    Public Function funGetSOID() As String
        Dim objDataClass As New clsDataClass
        Dim strSql, strData As String
        objDataClass.OpenDatabaseConnection()
        strSql = "SELECT Max(ordID) FROM orders"
        strData = objDataClass.GetScalarData(strSql)
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return strData
    End Function
    ' On Reset
    'Protected Sub cmdReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReset.Click
    '    Session("SalesProcessCart") = ""
    '    Session("IsSalesProcessCart") = ""
    '    dlCustomer.SelectedValue = "0"
    '    'Response.Redirect("Create.aspx")
    'End Sub
    ' On Text Change
    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        subSearch()
        txtSearch.Focus()
    End Sub
    ' Sub routine to close popup
    Private Sub subClose()
        Response.Write("<script>window.opener.location.reload();</script>")
        Response.Write("<script>window.close();</script>")
    End Sub

    Protected Sub cmdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.ServerClick
        Session("SalesReload") = "1"
        If Request.QueryString("SOID") <> "" Then
            subSaveProcess(Request.QueryString("SOID"))
        End If
        subClose()
    End Sub

    Protected Sub imgAdd_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgAdd.ServerClick
        If Convert.ToString(Session("eItm")) <> "" Then
            'subEditToCart(Convert.ToString(Session("eItm")))
            Session.Remove("eItm")
        Else
            lblMsg.Text = ""
            If IsNumeric(txtProcessFixedCost.Text) <> True Then
                lblMsg.Text = msgSOPlzEntFixedCost '"Please enter Fixed Cost."
                txtProcessFixedCost.Focus()
                Exit Sub
            End If
            If IsNumeric(txtProcessCostPerHour.Text) <> True Then
                lblMsg.Text = msgSOPlzEntCostPerHour '"Please enter Cost/Hour."
                txtProcessCostPerHour.Focus()
                Exit Sub
            End If
            If IsNumeric(txtProcessCostPerUnit.Text) <> True Then
                lblMsg.Text = msgSOPlzEntCostPerUnit '"Please enter Cost/Unit."
                txtProcessCostPerUnit.Focus()
                Exit Sub
            End If
            If IsNumeric(txtTotalHour.Text) <> True Then
                lblMsg.Text = msgSOPlzEntTotalHour '"Please enter Total Hour."
                txtTotalHour.Focus()
                Exit Sub
            End If
            If IsNumeric(txtTotalUnit.Text) <> True Then
                lblMsg.Text = msgSOPlzEntTotalUnit '"Please enter Total Unit."
                txtTotalUnit.Focus()
                Exit Sub
            End If
            AddToCart()
            If Convert.ToString(Session("SalesProcessCart")) <> "" Then
                grdAddProcLst.DataSource = Session("SalesProcessCart")
                grdAddProcLst.DataBind()
                lblProcessList.Visible = True
                divItems.Style("display") = "none"
                txtSearch.Focus()
            End If
        End If
        'subSearch()
    End Sub
End Class

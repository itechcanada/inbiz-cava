Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports clsCommon

Partial Class Sales_Default
    Inherits BasePage
    Private strSOID As String = ""
    Private objSO As New clsOrders
    Private clsStatus As New clsSysStatus
    ' On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        lblTitle.Text = lblGeneratedSOReadyForApproval
        If Not Page.IsPostBack Then
            clsStatus.subGetStatus(dlSearch, "SO", "dlSh1", "NO")
            subFillGrid()
        End If
        clsGrid.MaintainParams(Page, grdPO, SearchPanel, lblMsg, imgSearch)
        clsCSV.ExportCSV(Page, "SO_approve", grdPO, New Integer() {6}, imgCsv)

        txtSearch.Focus()
    End Sub
    'Populate Grid
    Public Sub subFillGrid()
        sqldsPO.SelectCommand = objSO.funFillGridForQuotationApproval("", "")
    End Sub
    'On Page Index Change
    Protected Sub grdOrder_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdPO.PageIndexChanging
        subFillGrid()
    End Sub
    'On Row Data Bound
    Protected Sub grdOrder_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdPO.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            e.Row.Cells(3).Text = String.Format("{0:F}", funCalcTax(e.Row.Cells(0).Text))
        End If
    End Sub
    ' Calculate Tax
    Private Function funCalcTax(ByVal SOID As String) As Double
        Dim objSOItems As New clsOrderItems
        Dim drSOItems As Data.Odbc.OdbcDataReader
        Dim objPrn As New clsPrint
        drSOItems = objSOItems.GetDataReader(objSOItems.funFillGrid(SOID))
        Dim dblSubTotal As Double = 0
        Dim dblTax As Double = 0
        Dim dblProcessTotal As Double = 0
        Dim dblTotal As Double = 0
        Dim dblDiscountApplied As Double = 0
        Dim dblAmount As Double = 0
        Dim strTaxArray(,) As String
        Dim strWhs As String
        While drSOItems.Read
            Dim strTax As ArrayList
            Dim strTaxItem As String
            strTax = objPrn.funCalcTax(drSOItems("ordProductID"), drSOItems("ordShpWhsCode"), drSOItems("amount"), "", drSOItems("ordCustID"), drSOItems("ordCustType"), drSOItems("ordProductTaxGrp").ToString)
            Dim intI As Integer = 0
            strWhs = drSOItems("ordShpWhsCode")

            While strTax.Count - 1 >= intI
                strTaxItem = strTax.Item(intI).ToString
                strTaxArray = objPrn.funAddTax(strTaxArray, strTaxItem.Substring(0, strTax.Item(intI).IndexOf("@,@")), CDbl(strTaxItem.Substring(strTax.Item(intI).IndexOf("@,@") + 3)))
                intI += 1
            End While
            dblSubTotal = CDbl(dblSubTotal) + CDbl(drSOItems("amount"))
        End While
        Dim i As Integer = 0
        Dim j As Integer = 0

        Dim objSOIP As New clsOrderItemProcess
        objSOIP.OrdID = SOID
        dblProcessTotal = CDbl(objSOIP.getProcessCostForOrderNo())
        strTaxArray = objPrn.funTotalProcessTax(dblProcessTotal, strWhs, strTaxArray)
        If Not strTaxArray Is Nothing Then
            j = strTaxArray.Length / 2
        End If
        While i < j
            dblTax += strTaxArray(1, i)
            i += 1
        End While
       
        dblTotal = dblTotal + dblSubTotal + dblProcessTotal + dblTax
        drSOItems.Close()
        objSOItems.CloseDatabaseConnection()
        objSOIP.CloseDatabaseConnection()
        objSOIP = Nothing
        objPrn = Nothing
        Return dblTotal
    End Function
    'Deleting
    Protected Sub grdOrder_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdPO.RowDeleting
        strSOID = grdPO.DataKeys(e.RowIndex).Value.ToString()
        lblMsg.Text = msgSOOrderDeletedSuccessfully '"Order has been Deleted Successfully."
        objSO.OrdID = strSOID
        objSO.funDeleteOrderItemProcess() 'Delete Sales Order Item Process
        objSO.funDeleteOrderItems() 'Delete Sales Order Items
        sqldsPO.DeleteCommand = objSO.funDeleteOrder() 'Delete Order
        subFillGrid()
    End Sub
    'Editing
    Protected Sub grdOrder_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdPO.RowEditing
        strSOID = grdPO.DataKeys(e.NewEditIndex).Value.ToString()
        Response.Redirect("~/Sales/Approval.aspx?SOID=" & strSOID & "&Aprov=y")
    End Sub
    'Sorting
    Protected Sub grdOrder_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdPO.Sorting
        subFillGrid()
    End Sub
    ' Sql Data Source Event Track
    Protected Sub sqldsOrder_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsPO.Selected
        If e.AffectedRows = 0 And lblMsg.Text = "" Then
            lblMsg.Text = NoDataFound
        End If
    End Sub
    ' Search Sub Routine
    Private Sub subSearch()
        'If txtSearch.Text <> "" Then
        'sqldsPO.SelectCommand = objPO.funGeneratedPOListFillGrid(dlSearch.SelectedValue, txtSearch.Text)
        sqldsPO.SelectCommand = objSO.funFillGridForQuotationApproval(dlSearch.SelectedValue, funRemove(txtSearch.Text))
        lblMsg.Text = ""
        'Else
        'lblMsg.Text = PlzEntSearchCriteria
        'subFillGrid()
        'txtSearch.Focus()
        'End If
    End Sub
    ' On Search Image Click
    Protected Sub imgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
        If txtSearch.Text <> "" Then
            subSearch()
        Else
            lblMsg.Text = PlzEntSearchCriteria
            subFillGrid()
            txtSearch.Focus()
        End If
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    'On Text Change
    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        subSearch()
    End Sub
End Class

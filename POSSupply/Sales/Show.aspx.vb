Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Configuration.ConfigurationManager
Partial Class Sales_Show
    Inherits BasePage
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Request.QueryString("Msg") = "1" Then
            lblMsg.Text = msgSOApprovedSuccessfully '"Order Approved Successfully"
            If Request.QueryString("SOID") <> "" And Request.QueryString("Msg") = 1 Then
                Dim strPath As String = ""
                Dim objPrn As New clsPrint(Request.QueryString("SOID"))
                strPath = objPrn.PrintSalesRequestDetail(Request.QueryString("SOID"))
                hlnkTemplate.NavigateUrl = AppSettings("PDFPath") & strPath
                hlnkTemplate.Visible = True
                imgPdf.Visible = True
                imgFax.Visible = True
                imgMail.Visible = True
                objPrn = Nothing
            End If
        ElseIf Request.QueryString("Msg") = 2 Then
            lblMsg.Text = msgSORejectSuccessfully '"Order Reject Successfully"
            'hlnkTemplate.NavigateUrl = "Default.aspx"
            'hlnkTemplate.Visible = True
            'hlnkTemplate.Target = ""
            'imgPdf.Src = "../images/Back.png"
            imgFax.Visible = False
            imgMail.Visible = False
            cmdBack.Visible = True
        End If
    End Sub

    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub

    Protected Sub cmdBack_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.ServerClick
        Response.Redirect("Default.aspx")
    End Sub
End Class

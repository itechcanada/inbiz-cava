<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CreateSalesProcess.aspx.vb"
    Inherits="Sales_CreateSalesProcess" %>

<%@ Import Namespace="Resources.Resource" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <%=Session("CompanyName")%></title>
    <link href="../Css/style.css" rel="stylesheet" type="text/css" />
    <link rel="Stylesheet" href="../css/grid.css" type="text/css" />
    <link type="text/css" rel="stylesheet" media="all" href="../css/popup.css" />
    <script language="javascript">
        function funSelectCustomer(source, arguments) {
            if (window.document.getElementById('<%=dlCustomer.ClientID%>').value != "0") {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="MasterScriptManager" runat="Server">
    </asp:ScriptManager>
    <div align="center">
        <table width="950" class="table">
            <tr>
                <td class="superHeader" height="20" align="center" colspan="2">
                    <asp:Label CssClass="lblBold" runat="server" ID="lblTitle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td height="5" colspan="2">
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:UpdatePanel ID="upnlMsg" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="imgAdd" EventName="serverClick" />
                            <asp:AsyncPostBackTrigger ControlID="txtSearch" EventName="TextChanged" />
                            <asp:AsyncPostBackTrigger ControlID="imgSearch" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2" id="trVendor" runat="server" visible="false">
                    <asp:DropDownList ID="dlCustomer" CssClass="innerText" runat="server" Width="250px">
                    </asp:DropDownList>
                    <span class="style1">*</span>
                    <asp:CustomValidator ID="custvalCustomer" SetFocusOnError="true" runat="server" ErrorMessage="<%$ Resources:Resource, msgSOPlzSelCustomer %>"
                        ClientValidationFunction="funSelectCustomer" Display="None">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <table id="Table1" cellpadding="1" cellspacing="1" border="0" width="260" runat="server">
                        <tr>
                            <td align="left" colspan="2">
                                <asp:Label CssClass="lblBold" runat="server" ID="lblSearch" Text="<%$ Resources:Resource, ProcessSearch%>">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:TextBox runat="server" Width="255px" ID="txtSearch" ValidationGroup="PrdSrch"
                                    AutoPostBack="true">
                                </asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:ImageButton ID="imgSearch" runat="server" AlternateText="Search" ImageUrl="../images/search-btn.gif"
                                    ValidationGroup="PrdSrch" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left" valign="top">
                    <asp:UpdatePanel ID="upnlSearchProducts" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="grdProcess" runat="server" AllowSorting="True" DataSourceID="sqldsProcess"
                                AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0"
                                GridLines="none" AutoGenerateColumns="False" CssClass="view_grid" Style="border-collapse: separate;"
                                UseAccessibleHeader="False" DataKeyNames="ProcessID" Width="750px">
                                <Columns>
                                    <asp:BoundField DataField="ProcessCode" HeaderText="<%$ Resources:Resource, grdProcessCode %>"
                                        ReadOnly="True" SortExpression="ProcessCode">
                                        <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ProcessDescription" HeaderText="<%$ Resources:Resource, grdProcessDescription %>"
                                        ReadOnly="True" SortExpression="ProcessDescription">
                                        <ItemStyle Width="250px" Wrap="true" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ProcessFixedCost" HeaderStyle-HorizontalAlign="Right"
                                        HeaderText="<%$ Resources:Resource, grdProcessFixedCost %>" ReadOnly="True" SortExpression="ProcessFixedCost">
                                        <ItemStyle Width="200px" Wrap="true" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ProcessCostPerHour" HeaderStyle-HorizontalAlign="Right"
                                        HeaderText="<%$ Resources:Resource, grdProcessCostPerHour %>" ReadOnly="True"
                                        HeaderStyle-ForeColor="#ffffff">
                                        <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ProcessCostPerUnit" HeaderStyle-HorizontalAlign="Right"
                                        HeaderText="<%$ Resources:Resource, grdProcessCostPerUnit %>" ReadOnly="True"
                                        HeaderStyle-ForeColor="#ffffff">
                                        <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="" HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("ProcessID") %>'
                                                CommandName="Edit" CausesValidation="false" ImageUrl="~/images/add_24.png" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" Visible="false"
                                        HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                                CommandArgument='<%# Eval("ProcessID") %>' CommandName="Delete" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="grid_footer" />
                                <RowStyle CssClass="grid_rowstyle" />
                                <PagerStyle CssClass="grid_footer" />
                                <HeaderStyle CssClass="grid_header" />
                                <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                <PagerSettings PageButtonCount="20" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="sqldsProcess" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="imgAdd" EventName="serverClick" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr height="30">
                <td class="tdAlignLeft" colspan="2">
                    <asp:UpdatePanel ID="upnlProductItems" runat="server">
                        <ContentTemplate>
                            <div id="divItems" runat="server" style="display: none; width: 900;"><%--changes the Width of Div before thet is wass 820--%>
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td width='15px' background="../Images/popup_left.png">
                                        </td>
                                        <td class="popup_title_newbg">
                                            <div style="display: none;">
                                                <asp:Label ID="lblProcessID" Text="Process ID:" runat="server" /></div>
                                        </td>
                                        <td class="popup_title_newbg">
                                            <asp:Label ID="lblProcessCode" Text="<%$ Resources:Resource, lblProcessCode %>" runat="server" />
                                        </td>
                                        <td class="popup_title_newbg" width="10">
                                        </td>
                                        <td class="popup_title_newbg">
                                            <asp:Label ID="lblProcessDescription" runat="server" Text="<%$ Resources:Resource, lblProcessDescription %>" />
                                        </td>
                                       <td class="popup_title_newbg" width="60"></td>
                                            <%-- Changes by Ankit Choure :: Put here the DDl for Tax--%>
                                               <td class="popup_title_newbg" width="160">
                                                        <asp:Label ID="lblPOTaxGroup" runat="server"  Text="Tax Group"></asp:Label>
                                                    </td>
                                                   
                                        <td class="popup_title_newbg">
                                            <asp:Label ID="lblProcessFixedCost" Text="<%$ Resources:Resource, lblProcessFixedCost %>"
                                                runat="server" />
                                        </td>
                                        <td class="popup_title_newbg">
                                            <asp:Label ID="lblProcessCostPerHour" Text="<%$ Resources:Resource, lblProcessCostPerHour %>"
                                                runat="server" />
                                        </td>
                                        <td class="popup_title_newbg">
                                            <asp:Label ID="lblProcessCostPerUnit" Text="<%$ Resources:Resource, lblProcessCostPerUnit%>"
                                                runat="server" />
                                        </td>
                                        <td class="popup_title_newbg">
                                            <asp:Label ID="lblTotalHour" Text="<%$ Resources:Resource, lblSOTotalHour%>" runat="server" />
                                        </td>
                                        <td class="popup_title_newbg">
                                            <asp:Label ID="lblTotalUnit" Text="<%$ Resources:Resource, lblSOTotalUnit%>" runat="server" />
                                        </td>
                                        <td width="100" class="popup_title_newbg">
                                        </td>
                                        <td width='15px' background="../Images/popup_right.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="14">
                                            <table cellpadding="0" width="100%" cellspacing="0" class="popup_content_newbg">
                                                <tr height="50">
                                                    <td width="15">
                                                    </td>
                                                    <td>
                                                        <div style="display: none;">
                                                            <asp:TextBox ID="txtProcessID" runat="server" Width="60px" ReadOnly="true" BackColor="LightGray"></asp:TextBox></div>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtProcessCode" runat="server" CssClass="popup_input" Width="80px"
                                                            ReadOnly="true" BackColor="LightGray">
                                                        </asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtProcessDescription" runat="server" CssClass="popup_input" Width="185px"
                                                            ReadOnly="true" BackColor="LightGray">
                                                        </asp:TextBox>
                                                    </td>
                                                   <%-- Changes by Ankit Choure :: Put here the DDl for Tax--%>
                                                    <td>
                                                    <asp:DropDownList ID="dlTaxCode" CssClass="dropdown_pop" runat="server" Width="160px">
                                                        </asp:DropDownList>
                                                        </td>
                                                    <td>
                                                        <asp:TextBox ID="txtProcessFixedCost" runat="server" Width="70px" CssClass="popup_input"
                                                            ValidationGroup="lstSrch">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqvalQuantity" runat="server" ControlToValidate="txtProcessFixedCost"
                                                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntFixedCost%>" SetFocusOnError="true"
                                                            Display="None" ValidationGroup="lstSrch">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtProcessCostPerHour" runat="server" Width="60px" CssClass="popup_input"
                                                            ValidationGroup="lstSrch">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqvalPrice" runat="server" ControlToValidate="txtProcessCostPerHour"
                                                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntCostPerHour%>" SetFocusOnError="true"
                                                            Display="None" ValidationGroup="lstSrch">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtProcessCostPerUnit" runat="server" Width="60px" CssClass="popup_input"
                                                            ValidationGroup="lstSrch">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqvalProcessCostPerUnit" runat="server" ControlToValidate="txtProcessCostPerUnit"
                                                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntCostPerUnit%>" SetFocusOnError="true"
                                                            Display="None" ValidationGroup="lstSrch">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTotalHour" runat="server" Width="70px" CssClass="popup_input"
                                                            ValidationGroup="lstSrch">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqvalTotalHour" runat="server" ControlToValidate="txtTotalHour"
                                                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntTotalHour%>" SetFocusOnError="true"
                                                            Display="None" ValidationGroup="lstSrch">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTotalUnit" runat="server" Width="70px" CssClass="popup_input"
                                                            ValidationGroup="lstSrch">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqvalTotalUnit" runat="server" ControlToValidate="txtTotalUnit"
                                                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntTotalUnit%>" SetFocusOnError="true"
                                                            Display="None" ValidationGroup="lstSrch">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <div class="buttonwrapper">
                                                            <a id="imgAdd" runat="server" causesvalidation="false" class="ovalbutton" href="#"><span
                                                                class="ovalbutton" style="min-width: 75px; text-align: center;">
                                                                <%=Resources.Resource.cmdCssAdd%></span></a></div>
                                                        <%-- <asp:ImageButton ID="imgAdd" runat="server" ImageUrl="../images/add.png" 
                    AlternateText="" ValidationGroup="lstSrch"/>--%>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="imgAdd" EventName="serverClick" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr height="30">
                <td class="tdAlignLeft" colspan="2">
                    <div id="divItemsGrid" runat="server">
                        <br />
                        <br />
                        <asp:UpdatePanel ID="upnlItemsList" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="lblProcessList" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOProcessItemList %>"
                                    runat="server" Visible="false" /><br />
                                <asp:GridView ID="grdAddProcLst" runat="server" AllowSorting="False" AllowPaging="True"
                                    PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0" GridLines="none" AutoGenerateColumns="False"
                                    CssClass="view_grid" Style="border-collapse: separate;" UseAccessibleHeader="False"
                                    DataKeyNames="ProcessID" Width="750px">
                                    <Columns>
                                        <asp:BoundField DataField="ProcessID" HeaderStyle-ForeColor="#ffffff" HeaderText="Process ID:"
                                            Visible="false" ReadOnly="True" SortExpression="ProcessID">
                                            <ItemStyle Width="70px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProcessCode" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdProcessCode %>"
                                            ReadOnly="True" SortExpression="ProcessCode">
                                            <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProcessDescription" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdProcessDescription %>"
                                            ReadOnly="True" SortExpression="ProcessDescription">
                                            <ItemStyle Width="250px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>

                                         <asp:BoundField DataField="TaxGrpName" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdTaxDescription %>"
                                            ReadOnly="True" SortExpression="ProcessDescription">
                                            <ItemStyle Width="250px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>










                                        <asp:BoundField DataField="ProcessFixedCost" HeaderStyle-HorizontalAlign="Right"
                                            HeaderText="<%$ Resources:Resource, grdProcessFixedCost %>" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                            <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProcessCostPerHour" HeaderStyle-HorizontalAlign="Right"
                                            HeaderText="<%$ Resources:Resource, grdProcessCostPerHour %>" ReadOnly="True"
                                            HeaderStyle-ForeColor="#ffffff">
                                            <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProcessCostPerUnit" HeaderStyle-HorizontalAlign="Right"
                                            HeaderText="<%$ Resources:Resource, grdProcessCostPerUnit %>" ReadOnly="True"
                                            HeaderStyle-ForeColor="#ffffff">
                                            <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TotalHour" HeaderText="<%$ Resources:Resource, grdSOTotalHour %>"
                                            HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                            <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TotalUnit" HeaderText="<%$ Resources:Resource, grdSOTotalUnit %>"
                                            HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                            <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, grdEdit %>" Visible="false"
                                            HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("ProcessID") %>'
                                                    CommandName="Edit" ImageUrl="~/images/edit_icon.png" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="30px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" HeaderStyle-ForeColor="#ffffff"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                                    CommandArgument='<%# Eval("ProcessID") %>' CommandName="Delete" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="30px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="grid_footer" />
                                    <RowStyle CssClass="grid_rowstyle" />
                                    <PagerStyle CssClass="grid_footer" />
                                    <HeaderStyle CssClass="grid_header" />
                                    <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                    <PagerSettings PageButtonCount="20" />
                                </asp:GridView>
                                <asp:SqlDataSource ID="sqldsAddProcLst" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                                <br />
                                <br />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="imgAdd" EventName="serverClick" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <asp:HiddenField ID="hdnCtr" runat="server" />
                </td>
            </tr>
            <tr height="30">
                <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                    <table width="100%">
                        <tr>
                            <td align="left" width="40%">
                            </td>
                            <td align="left" width="60%">
                                <div class="buttonwrapper">
                                    <a id="cmdSave" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                        <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssNext%></span></a></div>
                            </td>
                        </tr>
                    </table>
                    <%--<asp:Button runat="server" ID="cmdSave" CssClass="imgNext" />&nbsp;&nbsp;
            <asp:Button runat="server" ID="cmdReset" CssClass="imgReset" 
            CausesValidation="false" Visible="false" />--%>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="valsSales" runat="server" ShowMessageBox="true" ShowSummary="false" />
        <asp:ValidationSummary ID="valsSales1" runat="server" ShowMessageBox="true" ShowSummary="false"
            ValidationGroup="lstSrch" />
    </div>
    <br />
    <br />
    </form>
</body>
</html>

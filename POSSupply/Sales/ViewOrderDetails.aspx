﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/twoColumn.master" AutoEventWireup="true"
    EnableViewState="true" EnableSessionState="True" CodeFile="ViewOrderDetails.aspx.cs"
    Inherits="Sales_ViewOrderDetails" %>

<%@ Register Src="../Controls/new/CustomFields.ascx" TagName="CustomFields" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <link href="../lib/scripts/collapsible-panel/collapsible_salesedit.css" rel="stylesheet"
        type="text/css" />
    <script src="../lib/scripts/collapsible-panel/jquery.collapsiblepanel.js" type="text/javascript"></script>
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
    <style type="text/css">
        .dialog li {
            line-height: 30px;
        }

        table.readonlye_info {
            margin-bottom: 5px;
        }

            table.readonlye_info td {
                padding: 2px 5px;
            }

                table.readonlye_info td.bold {
                    font-weight: bold;
                }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTop" runat="Server">
    <div class="col1">
        <img src="../lib/image/icon/ico_admin.png" />
    </div>
    <div class="col2">
        <h1>
            <asp:Literal ID="ltAdmin" Text="<%$Resources:Resource, lblAdministration%>" runat="server" />
        </h1>
        <b>
            <asp:Literal runat="server" ID="lblTitle" Text="<%$Resources:Resource, lblOrderCreate %>"></asp:Literal></b>
    </div>
    <div style="float: left;">
        <%--<div id="divAddProduct" class="dropdown" runat="server">
            <a href="#" class="button inbiz" style="margin-left:0px;">
                <span class="label"><%=Resources.Resource.lblAddProduct%></span>
                <span class="toggle"></span>
            </a>
            <div class="dropdown-slider" style="z-index:1000;">
                   <asp:HyperLink ID="popAddProduct" CssClass="ddm addProduct" NavigateUrl="#" runat="server"
                ToolTip="<%$Resources:Resource, lblSOAddPrdInQuotationItemList %>">
                <span class="label">
                    <%=Resources.Resource.lblAddProduct%></span>
                <iCtrl:IframeDialog ID="mdAddProduct" Width="800" TriggerSelectorMode="Class" TriggerSelectorClass="addProduct"
                    Height="460" UrlSelector="TriggerControlHrefAttribute" runat="server"></iCtrl:IframeDialog>
            </asp:HyperLink>
                <asp:HyperLink ID="popAddChildren" CssClass="ddm addProduct" NavigateUrl="#" runat="server" ToolTip="<%$Resources:Resource, lblSOAddPrdInQuotationItemList %>">
                    <span class="label"><%=Resources.Resource.lblAddChildren%></span>
                    <iCtrl:IframeDialog ID="IframeDialog4" Width="800" TriggerSelectorMode="Class" TriggerSelectorClass="addProduct"
                        Height="460" UrlSelector="TriggerControlHrefAttribute"
                        runat="server"></iCtrl:IframeDialog>
                </asp:HyperLink>                         
                <asp:HyperLink ID="popAddCourse" CssClass="ddm addProduct" NavigateUrl="#" runat="server" ToolTip="<%$Resources:Resource, lblSOAddPrdInQuotationItemList %>">
                    <span class="label"><%=Resources.Resource.lblAddCourse%></span>
                    <iCtrl:IframeDialog ID="IframeDialog5" Width="800" TriggerSelectorMode="Class" TriggerSelectorClass="addProduct"
                        Height="460" UrlSelector="TriggerControlHrefAttribute"
                        runat="server"></iCtrl:IframeDialog>
                </asp:HyperLink>
                <asp:HyperLink ID="popAddProcess" CssClass="ddm addProcess" NavigateUrl="#" runat="server" ToolTip="<%$Resources:Resource, lblSelectService %>">
                    <span class="label"><%=Resources.Resource.lblAddService%></span>
                    <iCtrl:IframeDialog ID="IframeDialog6" Width="800" TriggerSelectorMode="Class" TriggerSelectorClass="addProcess"
                        Height="460" UrlSelector="TriggerControlHrefAttribute"
                        runat="server"></iCtrl:IframeDialog>
                </asp:HyperLink>
                <asp:HyperLink ID="popAddDiscount" CssClass="ddm addDiscount" NavigateUrl="#" runat="server" ToolTip="<%$Resources:Resource, lblAddDiscount %>">
                    <span class="label"><%=Resources.Resource.lblAddDiscount%></span>
                    <iCtrl:IframeDialog ID="IframeDialog7" Width="500" TriggerSelectorMode="Class" TriggerSelectorClass="addDiscount"
                        Height="260" UrlSelector="TriggerControlHrefAttribute"
                        runat="server"></iCtrl:IframeDialog>
                </asp:HyperLink>
            </div>
            <!-- /.dropdown-slider -->
        </div>--%>
        <!-- /.dropdown -->
        <%--<div id ="modal_dialog"></div>--%>
    </div>
    <div style="float: right;">
        <div style="text-align: right; margin-bottom: 5px;">
            <span id="popPlanShipment" runat="server">
                <asp:Button ID="btnPlanShipment" runat="server" Text="<%$Resources:Resource, lblPlanShipment %>"
                    CausesValidation="false" />
                <iCtrl:IframeDialog ID="dgPlanShipment" TriggerControlID="btnPlanShipment" Width="1100"
                    Height="500" Url="mdPlanShipment.aspx" Title="<%$Resources:Resource, lblOrderPlanShipment %>"
                    runat="server"></iCtrl:IframeDialog>
                <asp:Button ID="btnTicket" Text="<%$Resources:Resource,lblTicket%>" runat="server"
                    OnClick="btnTicket_Click" OnClientClick="" CausesValidation="false" />
                <%--Width="750"--%>
            </span><span id="popReceivePayment" runat="server">
                <asp:Button ID="btnReceivePayment" runat="server" Text="<%$Resources:Resource, lblReceivePayment %>"
                    CausesValidation="false" />
                <iCtrl:IframeDialog ID="dgReceivePayment" TriggerControlID="btnReceivePayment" Width="750"
                    Height="500" Url="~/AccountsReceivable/mdReceive.aspx" Title="<%$Resources:Resource, lblReceivePayment %>"
                    runat="server"></iCtrl:IframeDialog>
            </span><span id="popRefundPayment" runat="server">
                <asp:Button ID="btnRefund" runat="server" Text="<%$Resources:Resource, lblRefund %>"
                    CausesValidation="false" />
                <iCtrl:IframeDialog ID="dgRefund" TriggerControlID="btnRefund" Width="750" Height="560"
                    Url="~/AccountsReceivable/mdReceive.aspx" Title="<%$Resources:Resource, lblRefund %>"
                    runat="server"></iCtrl:IframeDialog>
            </span>
            <%--btnCheckOut--%>
            <input type="button" value="<%=Resources.Resource.lblPrintTermCondition%>" onclick="window.open('terms.htm', 'Print', 'height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');" />
            <asp:Button ID="btnPrihntAssembly" Text="<%$Resources:Resource, btnPrntAsmlyList %>"
                runat="server" CausesValidation="false" Visible="false" />
            <asp:Button ID="btnPrintPackageList" Text="<%$Resources:Resource, btnPrntPackList %>"
                runat="server" CausesValidation="false" Visible="false" />
            <span id="spnFax" runat="server">
                <asp:LinkButton CssClass="inbiz_icon icon_fax_28" runat="server" ID="imgFax" />
                <iCtrl:IframeDialog ID="mdFax" TriggerControlID="imgFax" Width="550" Height="260"
                    Url="~/Common/mdSendMailOrFax.aspx" Title="<%$Resources:Resource, lblPOFax %>"
                    runat="server"></iCtrl:IframeDialog>
            </span><span id="spnMail" runat="server">
                <asp:LinkButton CssClass="inbiz_icon icon_email_28" runat="server" ID="imgMail" />
                <iCtrl:IframeDialog ID="mdMail" TriggerControlID="imgMail" Width="550" Height="260"
                    Url="~/Common/mdSendMailOrFax.aspx" Title="<%$Resources:Resource, lblPOMail %>"
                    runat="server"></iCtrl:IframeDialog>
            </span><span id="spnPrint" runat="server">
                <asp:LinkButton CssClass="inbiz_icon icon_print_28" runat="server" ID="imgPdf" CausesValidation="false" />
            </span>
        </div>
        <div style="text-align: right;">
        </div>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMaster" runat="Server">
    <asp:Panel CssClass="panel" ID="ulSoDetails" runat="server">
        <h3>
            <asp:Label CssClass="trigger_icon" ID="ltTitle" Text="<%$Resources:Resource, lblSOCreateQuotation %>"
                runat="server" />
        </h3>
        <div class="panelcontent">
            <table class="readonlye_info" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="bold">
                        <asp:Label ID="titleSoNo" Text="<%$ Resources:Resource, lblSOQuotationNo %>" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valSoNo" Text="" runat="server" />
                    </td>
                    <td></td>
                    <td class="bold">
                        <asp:Label ID="titleReservationStatus" Text="Reservation Status" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valReservationStatus" Text="" runat="server" />
                    </td>
                    <td></td>
                    <td class="bold">
                        <asp:Label ID="Label10" Text="Balance Due" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valBalanceDue" Text="" runat="server" />
                    </td>
                    <td></td>
                    <td class="bold">
                        <asp:Label ID="titleCustomerName" Text="<%$ Resources:Resource, lblSOCustomerName %>"
                            runat="server" />
                    </td>
                    <td>
                        <asp:HyperLink ID="valCustomerName" Text="" runat="server" />
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblGP" Text="<%$ Resources:Resource,lblGP%>" runat="server" />
                        <%--added by mukesh 20130606--%>
                    </td>
                    <td id="tdGP" runat="server">
                        <asp:Label ID="txtGP" Text="" runat="server" Visible="false" />
                        <div id="dvGP">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="bold">
                        <asp:Label ID="titleOrderDate" Text="<%$ Resources:Resource, lblSOOrderDate %>" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valOrderDate" Text="" runat="server" />
                        <asp:TextBox ID="txtOrderDate" CssClass="datepicker" runat="server" Visible="false"
                            Width="70px" />
                    </td>
                    <td></td>
                    <td class="bold">
                        <asp:Label ID="titleStatus" Text="Sales Order Status" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valStatus" Text="" runat="server" />
                        <div id="popReason" style="display: none;">
                            <b>
                                <asp:Literal ID="ltReason" Text="" runat="server" /></b>
                        </div>
                        <script type="text/javascript">
                            function popReasonToHeld() {
                                $("#popReason").dialog({ title: '<%=Resources.Resource.lblReason%>' });
                            }                            
                        </script>
                    </td>
                    <td></td>
                    <td class="bold">
                        <asp:Label ID="Label11" Text="Invoice No" runat="server" />
                    </td>
                    <td>
                        <asp:LinkButton ID="btnGenerateInvoice" Text="<%$Resources:Resource, btnGenrateInvoice%>"
                            CommandArgument="" runat="server" CausesValidation="false" OnClick="btnGenerateInvoice_Click" />
                        <asp:HyperLink ID="hlInvoice" runat="server" Visible="false"></asp:HyperLink>
                    </td>
                    <td></td>
                    <td class="bold">
                        <asp:Label ID="lblCustEmail" Text="<%$Resources:Resource, lblEmail %>" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valCustEmail" Text="" runat="server" />
                    </td>
                    <td class="bold">
                        <asp:Label ID="lblGPPercentage" Text="<%$ Resources:Resource,lblGPPercentage%>" runat="server" />
                        <%--Added by mukesh 20130406--%>
                    </td>
                    <td id="tdGPPct" runat="server">
                        <asp:Label ID="txtGPPercentage" Text="" runat="server" Visible="false" />
                        <div id="dvGPPercentage">
                        </div>
                        <%--Added by mukesh 20130607--%>
                    </td>
                </tr>
                <tr>
                    <td class="bold">
                        <asp:Label ID="Label12" Text="Order Type" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valOrderType" Text="" runat="server" />
                    </td>
                    <td></td>
                    <td class="bold">
                        <asp:Label ID="Label9" Text="<%$ Resources:Resource, lblTransactionID %>" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valTransactionID" Text="" runat="server" />
                    </td>
                    <td></td>
                    <td class="bold">
                        <asp:Label ID="Label8" Text="<%$ Resources:Resource, lblReceiptNo %>" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valReceiptNo" Text="" runat="server" />
                    </td>
                    <td></td>
                    <td class="bold">
                        <asp:Label ID="lblCustPhone" Text="Phone #" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valCustPhone" Text="" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="bold">
                        <asp:Label ID="titleCreatedBy" Text="<%$ Resources:Resource, lblCreatedBy %>" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valCreatedBy" Text="" runat="server" />
                    </td>
                    <td></td>
                    <td class="bold">
                        <asp:Label ID="titleApprovedBy" Text="<%$ Resources:Resource, lblApprovedBy %>" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="valApprovedBy" Text="" runat="server" />
                    </td>
                    <td></td>
                    <td class="bold">
                        <asp:Label ID="Label13" Text="<%$ Resources:Resource,lblSalesRep%>" runat="server" />
                    </td>
                    <td colspan="4">
                        <asp:ListBox ID="lbxSalesRepresentativs" SelectionMode="Multiple" runat="server"
                            Width="400px" CssClass="modernized_select"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                    <td class="bold">
                        <asp:HyperLink ID="hlReturnHistory" NavigateUrl="" Text="" runat="server" Visible="false" />
                    </td>
                    <td class="bold">
                        <asp:HyperLink ID="hlShippingValidationHistory" NavigateUrl="" Text="" runat="server"
                            Visible="false" />
                    </td>
                    <td></td>
                    <td class="bold"></td>
                    <td></td>
                    <td></td>
                    <td class="bold"></td>
                    <td></td>
                    <td></td>
                    <td class="bold"></td>
                    <td></td>
                </tr>
                <%--<tr>
                
                <td>
                    <asp:Label ID="titleContactPhone" Text="<%$ Resources:Resource, lblARContactPhone %>"
                        runat="server" />
                </td>
                <td>
                    <asp:Label ID="valContactPhone" Text="" runat="server" />
                </td>
                 <td>
                    
                </td>
                <td>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="titleFax" Text="<%$ Resources:Resource, POFax %>" runat="server" />
                </td>
                <td>
                    <asp:Label ID="valFax" Text="" runat="server" />
                </td>
                <td>
                   
                </td>
                <td>
                    <asp:Label ID="valApprovedBy" Text="" runat="server" />
                </td>
            </tr>--%>
            </table>
        </div>
    </asp:Panel>
    <div id="grid_wrapper" style="width: 100%;">
        <h3>
            <asp:Label ID="lblQuotationItems" Text="<%$Resources:Resource,lblOrderItems %>" runat="server" />
        </h3>
        <trirand:JQGrid runat="server" ID="grdProducts" Height="100%" PagerSettings-PageSize="50"
            AutoWidth="true" OnDataRequesting="grdProducts_DataRequesting" OnCellBinding="grdProducts_CellBinding">
            <Columns>
                <trirand:JQGridColumn DataField="ProductID" HeaderText="ID" Width="50" />
                <trirand:JQGridColumn HeaderText="Guest" DataField="ItemGuestName" Sortable="false"
                    TextAlign="Center" Visible="false" />
                <trirand:JQGridColumn DataField="OriginalPrdName" HeaderText="<%$ Resources:Resource, grdPOProductName %>" />
                <trirand:JQGridColumn DataField="UPCCode" HeaderText="UPC" Editable="false" Width="70"
                    Visible="True" />
                <trirand:JQGridColumn DataField="PrdColor" HeaderText="<%$ Resources:Resource, grdvPrdColor %>" />
                <trirand:JQGridColumn DataField="PrdSize" HeaderText="<%$ Resources:Resource, lblProductSize %>" />
                <trirand:JQGridColumn DataField="ProductName" HeaderText="Description" />
                <trirand:JQGridColumn DataField="Quantity" HeaderText="<%$ Resources:Resource, grdPOQty %>"
                    Width="30" TextAlign="Right" />
                <trirand:JQGridColumn DataField="PreviousReturnedQty" HeaderText="<%$ Resources:Resource, lblBalanceQty %>"
                    Width="90" TextAlign="Right" />
                <trirand:JQGridColumn DataField="Price" HeaderText="<%$ Resources:Resource, grdPOPrice %>"
                    Width="90" TextAlign="Right">
                    <%-- <Formatter>
                        <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                            ThousandsSeparator="," />
                    </Formatter>--%>
                </trirand:JQGridColumn>
                <trirand:JQGridColumn DataField="TaxGrpDesc" HeaderText="<%$ Resources:Resource, grdPOTaxGroup %>"
                    Width="60" />
                <trirand:JQGridColumn DataField="Discount" HeaderText="<%$ Resources:Resource, grdSaleDiscount %>"
                    Width="60" />
                <trirand:JQGridColumn DataField="WarehouseCode" HeaderText="<%$ Resources:Resource, grdPOWhs %>"
                    Width="60" />
                <trirand:JQGridColumn DataField="ItemTotal" HeaderText="<%$ Resources:Resource, POAmount %>"
                    Width="90" TextAlign="Right">
                    <%--<Formatter>
                        <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                            ThousandsSeparator="," />
                    </Formatter>--%>
                </trirand:JQGridColumn>
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="ProductID"
                    Sortable="false" TextAlign="Center" Width="50" />
                <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="ProductID"
                    Sortable="false" TextAlign="Center" Width="50" />
                <trirand:JQGridColumn DataField="ID" HeaderText="" Visible="false" PrimaryKey="true" />
                <trirand:JQGridColumn HeaderText="Delete" DataField="ProductID" Sortable="false"
                    TextAlign="Center" Width="100" Visible="false" />
                <trirand:JQGridColumn HeaderText="Move" DataField="ProductID" Sortable="false" TextAlign="Center"
                    Width="100" Visible="false" />
            </Columns>
            <PagerSettings PageSize="100" PageSizeOptions="[100,150,200]" />
            <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                ShowDeleteButton="false" ShowSearchButton="false" />
            <SortSettings InitialSortColumn="" />
            <AppearanceSettings AlternateRowBackground="True" />
            <ClientSideEvents LoadComplete="gridLoadComplete" BeforeRowSelect="beforeRowSelect" />
        </trirand:JQGrid>
        <div>
            <table id="subTotalSummary1" border="0" cellpadding="0" cellspacing="0" style="float: right; font-weight: 700; text-align: right;">
            </table>
            <div style="clear: both;">
            </div>
        </div>
        <asp:SqlDataSource ID="sdsProducts" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
            ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
        <iCtrl:IframeDialog ID="mdEditProduct" Height="510" Width="800" Title="<%$ Resources:Resource, lblEditProduct %>"
            Dragable="true" TriggerSelectorClass="edit_Product" TriggerSelectorMode="Class"
            UrlSelector="TriggerControlHrefAttribute" runat="server"></iCtrl:IframeDialog>
        <iCtrl:IframeDialog ID="IframeDialog3" Height="410" Width="800" Title="Search Guest"
            Dragable="true" TriggerSelectorClass="search_guest" TriggerSelectorMode="Class"
            UrlSelector="TriggerControlHrefAttribute" runat="server"></iCtrl:IframeDialog>
    </div>
    <div id="grid_wrapper1" style="width: 100%; margin-top: 15px;" collapsible="false">
        <div>
            <h3>
                <asp:Label ID="Label1" Text="<%$Resources:Resource, btnimgAddProcess%>" runat="server" />
            </h3>
        </div>
        <div>
            <trirand:JQGrid runat="server" ID="grdProcess" Height="100%" PagerSettings-PageSize="50"
                AutoWidth="true" OnCellBinding="grdProcess_CellBinding" OnDataRequesting="grdProcess_DataRequesting">
                <Columns>
                    <trirand:JQGridColumn DataField="ProcessID" HeaderText="" PrimaryKey="true" Visible="false" />
                    <trirand:JQGridColumn DataField="ProcessCode" HeaderText="<%$ Resources:Resource, grdProcessCode %>"
                        Editable="false" />
                    <trirand:JQGridColumn DataField="ProcessDescription" HeaderText="<%$ Resources:Resource, grdProcessDescription %>" />
                    <trirand:JQGridColumn DataField="TaxGrpName" HeaderText="<%$ Resources:Resource, grdTaxDescription %>" />
                    <trirand:JQGridColumn DataField="ProcessFixedCost" HeaderText="<%$ Resources:Resource, grdProcessFixedCost %>">
                        <%--<Formatter>
                            <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                                ThousandsSeparator="," />
                        </Formatter>--%>
                    </trirand:JQGridColumn>
                    <trirand:JQGridColumn DataField="ProcessCostPerHour" HeaderText="<%$ Resources:Resource, grdProcessCostPerHour %>">
                        <%-- <Formatter>
                            <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                                ThousandsSeparator="," />
                        </Formatter>--%>
                    </trirand:JQGridColumn>
                    <trirand:JQGridColumn DataField="ProcessCostPerUnit" HeaderText="<%$ Resources:Resource, grdProcessCostPerUnit %>">
                        <%--<Formatter>
                            <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                                ThousandsSeparator="," />
                        </Formatter>--%>
                    </trirand:JQGridColumn>
                    <trirand:JQGridColumn DataField="TotalHour" HeaderText="<%$ Resources:Resource, grdSOTotalHour %>" />
                    <trirand:JQGridColumn DataField="TotalUnit" HeaderText="<%$ Resources:Resource, grdSOTotalUnit %>" />
                    <%--Added by mukesh 20130523 start--%>
                    <trirand:JQGridColumn DataField="ProcessInternalCost" HeaderText="<%$ Resources:Resource, grdProcessInternalCost %>">
                        <%-- <Formatter>
                            <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                                ThousandsSeparator="," />
                        </Formatter>--%>
                    </trirand:JQGridColumn>
                    <%--Added end--%>
                    <trirand:JQGridColumn DataField="ProcessTotal" HeaderText="<%$ Resources:Resource, grdSOProcessCost %>">
                        <%--<Formatter>
                            <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                                ThousandsSeparator="," />
                        </Formatter>--%>
                    </trirand:JQGridColumn>
                    <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdEdit %>" DataField="ProcessID"
                        Sortable="false" TextAlign="Center" Width="50" />
                    <trirand:JQGridColumn HeaderText="<%$ Resources:Resource, grdDelete %>" DataField="ProcessID"
                        Sortable="false" TextAlign="Center" Width="50" />
                </Columns>
                <PagerSettings PageSize="100" PageSizeOptions="[100,150,200]" />
                <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                    ShowDeleteButton="false" ShowSearchButton="false" />
                <SortSettings InitialSortColumn="" />
                <AppearanceSettings AlternateRowBackground="True" />
                <ClientSideEvents LoadComplete="gridLoadComplete1" BeforeRowSelect="beforeRowSelect" />
            </trirand:JQGrid>
            <iCtrl:IframeDialog ID="IframeDialog2" Height="510" Width="800" Title="Edit Process Detail"
                Dragable="true" TriggerSelectorClass="edit_Process_Item" TriggerSelectorMode="Class"
                UrlSelector="TriggerControlHrefAttribute" runat="server"></iCtrl:IframeDialog>
            <div>
                <table id="subTotalSummary2" border="0" cellpadding="0" cellspacing="0" style="float: right; font-weight: 700; text-align: right;">
                </table>
                <div style="clear: both;">
                </div>
            </div>
        </div>
    </div>
    <div style="margin-top: 5px; border-top: 1px solid #ccc;">
        <table id="divTotalSummary" border="0" cellpadding="0" cellspacing="0" style="display: none; float: right; font-weight: 700; text-align: right;">
        </table>
        <div style="clear: both;">
        </div>
        <div id="divPayHistory" class="dialog" style="width: 400px;" title="Pay History">
        </div>
    </div>
    <div style="clear: both;">
    </div>
    <div class="panelcollapsed">
        <h3>
            <asp:Label CssClass="trigger_icon" ID="Label6" Text="<%$Resources:Resource,lblOtherDetails%>"
                runat="server" />
        </h3>
        <div class="panelcontent">
            <ul class="form">
                <li>
                    <div class="lbl">
                        <asp:Label ID="lbl6" runat="server" Text="<%$Resources:Resource, lblSOCustomer%>"> </asp:Label>
                    </div>
                    <div class="input">
                        <asp:Label ID="lblName" runat="server" Text="" Font-Bold="true"> </asp:Label>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="Label2" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblUserOrderType%>"></asp:Label>
                        *
                    </div>
                    <div class="input">
                        <asp:DropDownList ID="dlstOderType" runat="server" Width="170px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="dlstOderType"
                            ErrorMessage="<%$ Resources:Resource, custvalOderType %>" InitialValue="0" SetFocusOnError="true"
                            Text="*"> </asp:RequiredFieldValidator>
                    </div>
                    <div id="divOrderStatus" style="float: left;" runat="server" visible="true">
                        <div class="lbl">
                            <asp:Label ID="Label4" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblStatus%>"></asp:Label>
                            *
                        </div>
                        <div class="input">
                            <table cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="dlStatus" runat="server" Width="170px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblReason" runat="server" CssClass="lblBold" Font-Bold="true" Text="<%$ Resources:Resource, lblReason%>"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlReason" runat="server" Width="170px"  >
                                            <%--<asp:ListItem  Text="<%$Resources:Resource, lblSelectReason%>" Value="" Selected="True" />--%>
                                            <asp:ListItem Text="<%$Resources:Resource, lblVoided%>" Value="VOD" Selected="True" />
                                            <asp:ListItem Text="<%$Resources:Resource, lblNoInventory%>" Value="NOI" />
                                            <asp:ListItem Text="<%$Resources:Resource, lblCreatedInError%>" Value="CIE" />
                                            <asp:ListItem Text="<%$Resources:Resource, lblReceived%>" Value="REC" />
                                        </asp:DropDownList>
                                        <asp:Button ID="btnShipNow" Text="<%$Resources:Resource, btnShipNow%>" runat="server" />
                                        <iCtrl:IframeDialog ID="dgShipNow" TriggerControlID="btnShipNow" Width="1100" Height="500"
                                            Url="mdPlanShipment.aspx?shipnow=1" Title="<%$Resources:Resource, lblPlanShipment %>"
                                            runat="server"></iCtrl:IframeDialog>
                                    </td>
                                </tr>
                            </table>
                            <asp:RequiredFieldValidator ID="custvalStatus" runat="server" SetFocusOnError="true"
                                ControlToValidate="dlStatus" ErrorMessage="<%$ Resources:Resource, msgSOPlzSelStatus %>"
                                InitialValue="0"> </asp:RequiredFieldValidator><%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                                ControlToValidate="ddlReason" ErrorMessage="<%$ Resources:Resource, msgCloseReason %>"
                                InitialValue=""> </asp:RequiredFieldValidator>--%>
                        </div>
                        <div class="input">
                        </div>
                        <div class="clearBoth">
                        </div>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li id="liCustomInvoice" runat="server">
                    <div class="lbl">
                        <asp:Label ID="Label3" CssClass="lblBold" Text="<%$ Resources:Resource, lblINInvoiceNo %>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtInvNo" Width="150px" runat="server"></asp:TextBox>
                        <asp:RangeValidator ID="regVal" runat="server" ErrorMessage="<%$ Resources:Resource, msgInvNoGreThanZero%>"
                            SetFocusOnError="true" ControlToValidate="txtInvNo" Type="double" MinimumValue="0.001"
                            Text="*" MaximumValue="100000000000"></asp:RangeValidator>
                    </div>
                    <div class="lbl">
                        <asp:Label ID="Label5" CssClass="lblBold" Text="<%$ Resources:Resource, lblINInvoiceDate %>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtInvDate" CssClass="datepicker" Width="75px" MaxLength="15" runat="server"></asp:TextBox>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblShpToAddress" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShpToAddress %>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:Label ID="txtShpToAddress" runat="server"> </asp:Label>
                        <asp:HiddenField ID="txtShpToAddressTxt" runat="server" Value="0"></asp:HiddenField>
                        <br />
                        <asp:HyperLink ID="hlEditShippingAddress" CssClass="edit_address" NavigateUrl=""
                            runat="server" Text="[Edit Ship To Address]" />
                        <iCtrl:IframeDialog ID="mdAddress" TriggerSelectorMode="Class" TriggerSelectorClass="edit_address"
                            Width="900" Height="500" Title="<%$ Resources:Resource, lblSOShpToAddress %>"
                            UrlSelector="TriggerControlHrefAttribute" runat="server"></iCtrl:IframeDialog>
                        <%--<asp:RequiredFieldValidator ID="custValShpToAddress" runat="server" SetFocusOnError="true" ControlToValidate="txtShpToAddress"
                        ErrorMessage="<%$ Resources:Resource, msgSOPlzEntShpToAddress %>" 
                       Text="*">
                    </asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="lbl">
                        <asp:Label ID="lblBillToAddress" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOBillToAddress %>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:Label ID="txtBillToAddress" runat="server"> </asp:Label>
                        <asp:HiddenField ID="txtBillToAddressTxt" runat="server" Value="0"></asp:HiddenField>
                        <br />
                        <asp:HyperLink ID="hlEditBillToAddress" CssClass="edit_address" NavigateUrl="" runat="server"
                            Text="[Edit Bill To Address]" />
                        <%--<asp:RequiredFieldValidator ID="custvalBillToAddress" runat="server" SetFocusOnError="true" ControlToValidate="txtBillToAddress"
                        ErrorMessage="<%$ Resources:Resource, msgSOPlzEntBillToAddress %>" 
                        Text="*">
                    </asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblTerms" CssClass="lblBold" Text="<%$ Resources:Resource, POShippingTerms%>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtTerms" runat="server" TextMode="MultiLine" Rows="3"> </asp:TextBox>
                    </div>
                    <div class="lbl">
                        <asp:Label ID="lblNetTerms" CssClass="lblBold" Text="<%$ Resources:Resource, lblSONetTerms%>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <%--<asp:TextBox MaxLength="4" ID="txtNetTerms" runat="server"></asp:TextBox>--%>
                        <asp:DropDownList ID="ddlNetTerms" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li style="display: none;">
                    <div class="lbl">
                        <asp:Label ID="lblResellerShipBlankPref" CssClass="lblBold" Text="<%$ Resources:Resource, lblResellerShipBlankPref%>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:RadioButtonList ID="rblstShipBlankPref" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="<%$ Resources:Resource, lblYes%>" Value="1">
                            </asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lblNo%>" Value="0" Selected="True">
                            </asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="lbl">
                        <asp:Label ID="lblISWeb" Text="<%$ Resources:Resource, lblSOSalesISWeb%>" CssClass="lblBold"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:RadioButtonList ID="rdlstISWeb" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1">
                            </asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0" Selected="True">
                            </asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblShpWarehouse" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingWarehouse %>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:DropDownList ID="dlShpWarehouse" runat="server" Width="170px" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="custvalShpWarehouse" runat="server" SetFocusOnError="true"
                            ErrorMessage="<%$ Resources:Resource, lblSOPlzSelShippingWarehouse %>" Text="*"
                            ControlToValidate="dlShpWarehouse" InitialValue="0"> </asp:RequiredFieldValidator>
                    </div>
                    <div id="lblShpWare" runat="server" class="lbl" style="display: none;">
                        <asp:Label ID="lblShipToWarehouse" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToWarehouse %>"
                            runat="server" />
                        <asp:Label ID="lblShpTrackNo" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingTrackNo %>"
                            runat="server" Visible="false" />
                    </div>
                    <div id="dlShpToWare" runat="server" class="input" style="display: none;">
                        <asp:DropDownList ID="dlShpToWarehouse" runat="server" Width="170px">
                        </asp:DropDownList>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="true"
                            ErrorMessage="<%$ Resources:Resource, lblSOPlzSelShippingWarehouse %>" Text="*"
                            ControlToValidate="dlShpToWarehouse" InitialValue="0"> </asp:RequiredFieldValidator>--%>
                        <asp:TextBox ID="txtShpTrackNo" runat="server" MaxLength="25" onkeypress="return disableEnterKey(event)"
                            Visible="false"></asp:TextBox>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <%--<div class="lbl">
                    <asp:Label ID="lblShpCost" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingCost %>"
                        runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtShpCost" runat="server" Width="150px" MaxLength="6" onkeypress="return disableEnterKey(event)">
                    </asp:TextBox>
                    <asp:CompareValidator ID="comvalShpCost" EnableClientScript="true" SetFocusOnError="true"
                        ControlToValidate="txtShpCost" Operator="DataTypeCheck" Type="double" Display="None"
                        ErrorMessage="<%$ Resources:Resource, lblSOPlzEntNumericValueInShippingCost %>"
                        runat="server" />
                </div>--%>
                    <div class="lbl">
                        <asp:Label ID="lblShpDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingDate %>"> </asp:Label>
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtShippingDate" Enabled="true" runat="server" Width="90px" MaxLength="15"
                            onkeypress="return disableEnterKey(event)" CssClass="datepicker"></asp:TextBox>
                    </div>
                    <div id="divShpCode" runat="server" visible="false">
                        <div class="lbl" style="padding-top: 5px;">
                            <asp:Label ID="lblShpCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblShipmentTrackingNo %>"
                                runat="server" />
                        </div>
                        <div class="input">
                            <table>
                                <tr>
                                    <td valign="top" style="padding-top: 5px;">
                                        <asp:TextBox ID="txtShpCode" runat="server" MaxLength="15"></asp:TextBox>
                                    </td>
                                    <td valign="top">
                                        <asp:HyperLink ID="hypTrackLnk" Target="_blank" runat="server">
                                            <img src="../Images/track_icon.png"  height="30" >
                                        </asp:HyperLink>
                                        <%--<asp:HyperLink ID="hypTrackLnk" runat="server" target="_blank">
                                        <asp:ImageButton ID="imgBtnTracking" runat="server"  ImageUrl="~/Images/track_icon.png" Height="35px" />
                                        </asp:HyperLink>--%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <%--<div class="lbl">
                    <asp:Label ID="lblShpCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingCode %>"
                        runat="server" Visible="False" />
                </div>
                <div class="input">   
                    <asp:TextBox ID="txtShpCode" runat="server" Width="150px" MaxLength="15" onkeypress="return disableEnterKey(event)"
                        Visible="False"></asp:TextBox>
                </div>--%>
                    <div class="lbl">
                        <asp:Label ID="lblNotes" CssClass="lblBold" Text="<%$ Resources:Resource, PONotes%>"
                            runat="server" />
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Rows="3" Width="595px"> </asp:TextBox>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblQutExpDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOQutExpDate %>"> </asp:Label>
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtQutExpDate" Enabled="true" runat="server" Width="90px" MaxLength="15"
                            onkeypress="return disableEnterKey(event)" CssClass="datepicker"></asp:TextBox>
                    </div>
                    <div class="lbl">
                        <asp:Label ID="Label7" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOCustomerPO %>"> </asp:Label>
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtCustPO" runat="server" MaxLength="35" onkeypress="return disableEnterKey(event)"></asp:TextBox>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <uc1:CustomFields ID="CustomFields1" runat="server" />
    <div class="div_command">
        <asp:Button ID="btnDuplicateOrder" Text="<%$Resources:Resource, cmdCssDuplicateOrder%>"
            runat="server" OnClick="btnDuplicateOrder_Click" Visible="false" />
        <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" OnClick="btnSave_Click" />
        <%-- <asp:Button ID="btnInvAndGen" Text="Generate Invoice & Receive Payment" runat="server"
            OnClick="btnInvAndGen_Click" />--%>
        <asp:Button ID="btnSaveAndGoToCal" Text="<%$Resources:Resource, lblSaveAndGoBackToCalendar%>"
            runat="server" OnClick="btnSaveAndGoToCal_Click" Visible="False" />
        <asp:Button ID="btnCancelReservation" Text="<%$Resources:Resource, lblCancelReservation%>"
            runat="server" CausesValidation="false" />
        <asp:Button ID="btnCancel" Text="<%$Resources:Resource, lblBack%>" runat="server"
            OnClick="btnCancel_Click" CausesValidation="false" />
    </div>
    <asp:ValidationSummary ID="valsSales" runat="server" ShowMessageBox="true" ShowSummary="false" />
    <asp:HiddenField ID="hdnCompanyID" Value="" runat="server" />
    <asp:HiddenField ID="hdnPartnerID" Value="" runat="server" />
    <asp:HiddenField ID="hdnParterTypeID" Value="" runat="server" />
    <asp:HiddenField ID="hdnPartnerCurrencyCode" Value="" runat="server" />
    <asp:HiddenField ID="hdnInvoiceNoForOrder" Value="" runat="server" />
    <asp:HiddenField ID="hdnSalesAgentID" Value="" runat="server" />
    <asp:HiddenField ID="hdnWhsCode" Value="" runat="server" />
    <asp:HiddenField ID="hdnCurrrencyExchangeRate" Value="" runat="server" />
    <asp:HiddenField ID="hdnPreviousStatus" Value="" runat="server" />
    <asp:HiddenField ID="hdnPartialTranferURl" Value="" runat="server" />
    <asp:HiddenField ID="hdnPartialReturnURl" Value="" runat="server" />
    <asp:HiddenField ID="hdnAddProductURl" Value="" runat="server" />
    <div id="divReasonToCencelOrder" title="<%=Resources.Resource.lblCancelReservation%>"
        style="display: none;">
        <asp:Label ID="lblReasontoCancel" Text="<%$Resources:Resource,lblGiveReasontoCancel%>"
            runat="server" />
        <asp:TextBox ID="txtReasonToCancel" runat="server" TextMode="MultiLine" Width="300px"
            Height="80px" />
        <div class="div_command">
            <asp:Button ID="lblCancelReservationPop" Text="<%$Resources:Resource, lblCancelReservation%>"
                runat="server" OnClick="lblCancelReservationPop_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLeft" runat="Server">
    <div>
    </div>
    <ul id="orderMenu" class="menu inbiz-left-menu collapsible">
        <li class="active" id="divAddProduct" runat="server"><a href="#">Action</a>
            <ul style="display: block;">
                <li class="open">
                    <asp:HyperLink ID="popAddProduct" CssClass="addProduct" NavigateUrl="#" runat="server"
                        ToolTip="<%$Resources:Resource, lblSOAddPrdInQuotationItemList %>" Text="<%$Resources:Resource, lblAddProduct%>">                                      
                    </asp:HyperLink>
                    <iCtrl:IframeDialog ID="mdAddProduct" Width="1000" TriggerSelectorMode="Class" TriggerSelectorClass="addProduct"
                        Height="150" Position="top" UrlSelector="TriggerControlHrefAttribute" runat="server"></iCtrl:IframeDialog><%--Width="1000" Height="460"--%>
                </li>
                <li id="liChildren" runat="server" visible="false">
                    <asp:HyperLink ID="popAddChildren" CssClass="addProduct" NavigateUrl="#" runat="server"
                        Text="<%$Resources:Resource, lblAddChildren %>" ToolTip="<%$Resources:Resource, lblAddChildren %>">
                    </asp:HyperLink>
                    <iCtrl:IframeDialog ID="IframeDialog4" Width="800" TriggerSelectorMode="Class" TriggerSelectorClass="addProduct"
                        Height="460" UrlSelector="TriggerControlHrefAttribute" runat="server"></iCtrl:IframeDialog>
                </li>
                <li id="liCourse" runat="server" visible="false">
                    <asp:HyperLink ID="popAddCourse" CssClass="addProduct" NavigateUrl="#" runat="server"
                        Text="<%$Resources:Resource, lblAddCourse %>" ToolTip="<%$Resources:Resource, lblAddCourse %>">                    
                    </asp:HyperLink>
                </li>
                <li>
                    <asp:HyperLink ID="popAddServices" CssClass="addProduct" NavigateUrl="#" runat="server"
                        Text="<%$Resources:Resource, lblAddService %>" ToolTip="<%$Resources:Resource, lblSelectService %>">
                    </asp:HyperLink>
                </li>
                <li>
                    <asp:HyperLink ID="popAddProcess" CssClass="addProcess" NavigateUrl="#" runat="server"
                        Text="<%$Resources:Resource, btnimgAddProcess %>" ToolTip="<%$Resources:Resource, btnimgAddProcess %>">
                    </asp:HyperLink>
                    <iCtrl:IframeDialog ID="IframeDialog6" Width="800" TriggerSelectorMode="Class" TriggerSelectorClass="addProcess"
                        Height="460" UrlSelector="TriggerControlHrefAttribute" runat="server"></iCtrl:IframeDialog>
                </li>
                <li class="" id="liDiscount" runat="server" visible="false">
                    <asp:HyperLink ID="popAddDiscount" CssClass="addDiscount" NavigateUrl="#" runat="server"
                        Text="<%$Resources:Resource, lblAddDiscount %>" ToolTip="<%$Resources:Resource, lblAddDiscount %>">                                        
                    </asp:HyperLink>
                    <iCtrl:IframeDialog ID="IframeDialog7" Width="500" TriggerSelectorMode="Class" TriggerSelectorClass="addDiscount"
                        Height="260" UrlSelector="TriggerControlHrefAttribute" runat="server"></iCtrl:IframeDialog>
                </li>
            </ul>
        </li>
        <li id="divReturns" class="active" runat="server"><a href="#">
            <%= Resources.Resource.lblReturns%></a>
            <ul style="display: block;">
                <li>
                    <%--<asp:LinkButton ID="lnkFullReturn" Text="Full Return" runat="server" 
                        CausesValidation="false" onclick="lnkFullReturn_Click" />--%>
                    <asp:HyperLink ID="hlFullReturn" NavigateUrl="#" runat="server" ToolTip="<%$Resources:Resource, lblFullReturn %>"
                        Text="<%$Resources:Resource, lblFullReturn %>" Visible="false">                                      
                    </asp:HyperLink>
                </li>
                <li>
                    <asp:HyperLink ID="hlPartialReturn" NavigateUrl="#" runat="server" Text="<%$Resources:Resource, lblPartialReturn %>"
                        ToolTip="<%$Resources:Resource, lblPartialReturn %>">
                    </asp:HyperLink>
                </li>
                <li>
                    <asp:HyperLink ID="hlBackUrl" NavigateUrl="#" runat="server" Text="<%$Resources:Resource, lblGoBack %>"
                        ToolTip="<%$Resources:Resource, lblGoBack %>">
                    </asp:HyperLink>
                </li>
            </ul>
        </li>
        <li id="divTransfers" class="active" visible="false" runat="server"><a href="#">
            <%= Resources.Resource.lblTransfers%></a>
            <ul style="display: block;">
                <li>
                    <%--<asp:LinkButton ID="lnkFullReturn" Text="Full Return" runat="server" 
                        CausesValidation="false" onclick="lnkFullReturn_Click" />--%>
                    <asp:HyperLink ID="hlFullTransfer" NavigateUrl="#" runat="server" ToolTip="<%$Resources:Resource, lblFullTransfer %>"
                        Text="<%$Resources:Resource, lblFullTransfer %>">                                      
                    </asp:HyperLink>
                </li>
                <li>
                    <asp:HyperLink ID="hlPartialTransfer" NavigateUrl="#" runat="server" Text="<%$Resources:Resource, lblPartialTransfer %>"
                        ToolTip="<%$Resources:Resource, lblPartialTransfer %>">
                    </asp:HyperLink>
                </li>
                <li>
                    <asp:HyperLink ID="hlBackUrlFullReturn" NavigateUrl="#" runat="server" Text="<%$Resources:Resource, lblGoBack %>"
                        ToolTip="<%$Resources:Resource, lblGoBack %>">
                    </asp:HyperLink>
                </li>
            </ul>
        </li>
        <li class="active" id="divNavigation" runat="server"><a href="#">Navigation</a>
            <ul style="display: block;">
                <li>
                    <asp:HyperLink ID="hlviewSalesOrders" CssClass="" NavigateUrl="~/Sales/ViewSalesOrder.aspx"
                        Text="<%$Resources:Resource, lblSOViewSalesOrder%>" runat="server" /></li>
            </ul>
        </li>
    </ul>
    <asp:Button ID="btnOrderNO" runat="server" CausesValidation="false" Style="display: none;"
        Text="<%$ Resources:Resource,cmdCssOrderNo%>" OnClick="btnOrderNO_Click" Width="150px" />
    <asp:HiddenField ID="hdnShpToOrder" runat="server" />
    <asp:HiddenField ID="hdnTitleUpdateShipment" runat="server" Value="<%$ Resources:Resource,lblOrderUpdateShipment%>" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $("#<%=btnCancelReservation.ClientID%>").live("click", function () {
            if (confirm("<%=Resources.Resource.msgAreyouSureYouWantToCancelWholeReservation%>")) {
                $this = $(this);
                $dialog = jQuery.FrameDialog.create({ url: '<%=ResolveUrl("~/Reservation/mdPartialReservationMove.aspx") + "?action=whole&oid=" + this.OrderID%>',
                    title: "<%=Resources.Resource.lblCancelReservation %>",
                    loadingClass: "loading-image",
                    modal: true,
                    width: 750,
                    height: 510,
                    autoOpen: false,
                    close: function (ev, ui) {
                        $(this).dialog('destroy');
                        if (_isRequireReload) {
                            location.href = location.href;
                        }
                    }
                });
                $dialog.dialog('open'); return false;
            }
            return false;
        })
    </script>
    <script type="text/javascript">
        $(".cancel_rsv").live("click", function () {
            $this = $(this);
            $dialog = jQuery.FrameDialog.create({ url: $this.attr("href"),
                title: "<%=Resources.Resource.lblCancelReservation %>",
                loadingClass: "loading-image",
                modal: true,
                width: 750,
                height: 510,
                autoOpen: false,
                close: function (ev, ui) {
                    $(this).dialog('destroy');
                    if (_isRequireReload) {
                        location.href = location.href;
                    }
                }
            });
            $dialog.dialog('open'); return false;
        })
    </script>
    <script type="text/javascript">
        $.ajaxSetup({ 'cache': true });
        var _isRequireReload = false;

        function tryReloadThisPage() {
            if (_isRequireReload) {
                location.href = location.href;
            }
        }
    </script>
    <!--Functions For Order Items Grid-->
    <script type="text/javascript">
        $grid1 = $("#<%=grdProducts.ClientID%>");
        $grid1.initGridHelper({
            searchPanelID: "pnlSearch",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper"
        });

        function jqGridResize1() {
            $("#<%=grdProducts.ClientID%>").jqResizeAfterLoad("grid_wrapper", 0);
        }


        function reloadGrid(event, ui) {
            $grid1.trigger("reloadGrid");
            jqGridResize1();
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function gridLoadComplete(data) {
            if (data.rows.length > 0 && data.total > 0) {
                var invokSrc = $.getParamValue('InvokeSrc');
                if (invokSrc != "POS") {
                    showOrderTotalSummary();
                }
            }
            jqGridResize1();
        }


        function reloadGridFromPrdPage() {
            $grid1.trigger("reloadGrid");
            jqGridResize1();
        }
    </script>
    <!--Functions For Process Grid-->
    <script type="text/javascript">
        $grid2 = $("#<%=grdProcess.ClientID%>");
        $grid2.initGridHelper({
            searchPanelID: "pnlSearch",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapper1"
        });

        function jqGridResize2() {
            $("#<%=grdProcess.ClientID%>").jqResizeAfterLoad("grid_wrapper1", 0);
        }
        $("#grid_wrapper1").hide();

        function reloadProcessGrid(event, ui) {
            $("#grid_wrapper1").show();
            $grid2.trigger("reloadGrid");
            jqGridResize2();
            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        function gridLoadComplete1(data) {
            var rCount = $grid2.getGridParam("reccount");
            if (rCount > 0) {
                $("#grid_wrapper1").show();
                jqGridResize2();
            }
            else {
                $("#grid_wrapper1").hide();
            }

            if (data.rows.length > 0 && data.total > 0) {
                var invokSrc = $.getParamValue('InvokeSrc');
                if (invokSrc != "POS") {
                    showOrderTotalSummary();
                }
            }

            jqGridResize2();
        }
    </script>
    <script type="text/javascript">
        $(".delete_Product").live('click', function () {           
            var dataToPost = {};
            dataToPost.sessionID = '<%=Session.SessionID%>';
            dataToPost.productID = $(this).attr('data_key');                        
            $.ajax({
                type: "POST",
                url: "../ajax/salesAPI.aspx/DeleteCartItem",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(dataToPost),
                success: function (data) {
                    reloadGrid();
                },
                error: function (request, status, errorThrown) {
                    alert(status);
                }
            });
        });

        //Delete Process Item
        $(".delete_Process_Item").live('click', function () {
            var dataToPost = {};
            dataToPost.sessionID = '<%=Session.SessionID%>';
             dataToPost.processid = $(this).attr('data_key');    
             $.ajax({
                 type: "POST",
                 url: "../ajax/salesAPI.aspx/DeleteProcessCartItem",
                 contentType: "application/json; charset=utf-8",
                 dataType: "json",
                 data: JSON.stringify(dataToPost),
                 success: function (data) {
                     reloadProcessGrid();
                 },
                 error: function (request, status, errorThrown) {
                     alert(status);
                 }
             });
        });

         //Show Total Summary
         function showOrderTotalSummary() {
             //$("#divTotalSummary");
             var dataToPost = {};
             dataToPost.sessionID = '<%=Session.SessionID%>';
            dataToPost.partnerid = '<%=hdnPartnerID.Value%>';  
            dataToPost.ordID = <%=this.OrderID%>;
            $.ajax({
                type: "POST",
                url: "../ajax/salesAPI.aspx/GetCalculatedTotalHtml",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(dataToPost),
                success: function (data) {
                    $("#divTotalSummary").show();
                    $("#subTotalSummary1").html(data.d[0]);
                    $("#subTotalSummary2").html(data.d[1]);
                    $("#divTotalSummary").html(data.d[2]);
                    $("#divPayHistory").html(data.d[3]); 
                    $("#dvGP").html(data.d[4]); //Added by mukesh 20130607                     
                    $("#dvGPPercentage").html(data.d[5]); //Added by mukesh 20130607                     

                    $(".opener").click(function () {
                        $(".dialog").dialog("open");
                        return false;
                    });
                },
                error: function (request, status, errorThrown) {
                    alert(status);
                }
            });
        }

        $(".dialog").dialog({
            autoOpen: false, modal: true, width: '500px'
        });

        function changeCustomer() {
            location.href = location.href;
        }

        //prevent jq grid from row selection
        function beforeRowSelect(rowid, e) {
            return false;
        }

        function goToAddNewGuestPage(pType, gType, riID) {
            par = {};
            par.pType = pType;
            par.gType = gType;
            par.ordID = <%=this.OrderID%>;
            par.riID = riID;
            par.cmd = "changeGuest";
            par.backOrder = "<%=Server.UrlEncode(Request.RawUrl)%>";
            location.href = '<%=ResolveUrl("~/Partner/CustomerEdit.aspx")%>' + "?" + $.param(par);
            return false;
        }            
    </script>
    <script type="text/javascript">
        $("#divReasonToCencelOrder").dialog({
            autoOpen: false,
            height: 200,
            width: 330,
            modal: true
        }).parent().appendTo(jQuery("form:first"));

        function askReasonToCancel() {
            $("#divReasonToCencelOrder").dialog("open");
        }

        //Similar function require to use same reservation cancel form
        function reloadGridOnScrollPos() {
            location.href = location.href;
        }                       
    </script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<=lnkFullReturn.ClientID>").live("click", function (e) {
                return confirm("Would you like to process full return?");
            });

            $("#<%=hlFullReturn.ClientID%>").live("click", function (e) {
                $this = $(this);
                if (confirm("Would you like to process full return?")) {
                    var $dialog = jQuery.FrameDialog.create({
                        url: $this.attr("href"),
                        title: ($(this).attr("title")) ? $(this).attr("title") : "Full Return",
                        loadingClass: "loading-image",
                        modal: true,
                        width: "800",
                        height: "550",
                        autoOpen: false
                    });
                    $dialog.dialog('open'); return false;
                }
                return false;
            });


            $("#<%=hlPartialReturn.ClientID%>").live("click", function (e) {
                $this = $(this);
                if (confirm("Would you like to process partial return?")) {
                    var $dialog = jQuery.FrameDialog.create({
                        url: $this.attr("href"),
                        title: ($(this).attr("title")) ? $(this).attr("title") : "Partial Return",
                        loadingClass: "loading-image",
                        modal: true,
                        width: "820",//750
                        height: "550",
                        autoOpen: false
                    });
                    $dialog.dialog('open'); return false;
                }
                return false;
            });

            
            //Shipping Validation History Popup
            $("#<%=hlShippingValidationHistory.ClientID%>").live("click", function (e) {
                $this = $(this);
                var pageTitle = "";
                pageTitle = "<%=Resources.Resource.lblValidateSKU%>";
                var $dialog = jQuery.FrameDialog.create({
                    url: $this.attr("href"),
                    //title: (pageTitle == "1") ? "<%=Resources.Resource.lblReturnTransfer%>" : ($(this).attr("title")) ? $(this).attr("title") : "<%=Resources.Resource.lblFullTransfer%>",
                    title: pageTitle,
                    loadingClass: "loading-image",
                    modal: true,
                    width: "800",
                    height: "550",
                    autoOpen: false
                });
                $dialog.dialog('open'); return false;

                return false;
            });

            //Return History Popup
            $("#<%=hlReturnHistory.ClientID%>").live("click", function (e) {
                $this = $(this);
                var pageTitle = $.getParamValue('isreturn');
                pageTitle = $("#<%=hlReturnHistory.ClientID%>").html();
                var $dialog = jQuery.FrameDialog.create({
                    url: $this.attr("href"),
                    //title: (pageTitle == "1") ? "<%=Resources.Resource.lblReturnTransfer%>" : ($(this).attr("title")) ? $(this).attr("title") : "<%=Resources.Resource.lblFullTransfer%>",
                    title: pageTitle,
                    loadingClass: "loading-image",
                    modal: true,
                    width: "800",
                    height: "550",
                    autoOpen: false
                });
                $dialog.dialog('open'); return false;

                return false;
            });


            $("#<%=hlFullTransfer.ClientID%>").live("click", function (e) {
                $this = $(this);
                if (confirm("<%=Resources.Resource.msgCnfrmFullTransfer%>")) {
                    var $dialog = jQuery.FrameDialog.create({
                        url: $this.attr("href"),
                        title: ($(this).attr("title")) ? $(this).attr("title") : "<%=Resources.Resource.lblFullTransfer%>",
                        loadingClass: "loading-image",
                        modal: true,
                        width: "800",
                        height: "550",
                        autoOpen: false
                    });
                    $dialog.dialog('open'); return false;
                }
                return false;
            });


            $("#<%=hlPartialTransfer.ClientID%>").live("click", function (e) {
                $this = $(this);
                if (confirm("<%=Resources.Resource.msgCnfrmPartialTransfer%>")) {
                    var $dialog = jQuery.FrameDialog.create({
                        url: $this.attr("href"),
                        title: ($(this).attr("title")) ? $(this).attr("title") : "<%=Resources.Resource.lblPartialReturn%>",
                        loadingClass: "loading-image",
                        modal: true,
                        width: "800",//750
                        height: "550",
                        autoOpen: false
                    });
                    $dialog.dialog('open'); return false;
                }
                return false;
            });

        });


        $("#<%=dlstOderType.ClientID%>").change(function () {
            ShowWareHouseCode();
        });


        $(document).ready(function () {
            ShowWareHouseCode();
        });


        function ShowWareHouseCode() {
            var sOrderType = $("#<%=dlstOderType.ClientID %> :selected").val();
            if (sOrderType == "2") {
                $("#<%=lblShpWare.ClientID%>").css("display", "block");
                $("#<%=dlShpToWare.ClientID%>").css("display", "block");
            }
            else {
                $("#<%=lblShpWare.ClientID%>").css("display", "none");
                $("#<%=dlShpToWare.ClientID%>").css("display", "none");
            }
        }

        $.extend({
            getParamValue: function (paramName) {
                parName = paramName.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
                var pattern = '[\\?&]' + paramName + '=([^&#]*)';
                var regex = new RegExp(pattern);
                var matches = regex.exec(window.location.href);
                if (matches == null) return '';
                else return decodeURIComponent(matches[1].replace(/\+/g, ' '));
            }
        });

        $(".SetExchangeRate").live('keyup', function (e) {
            var sExcRate = Number($("#txtExchangeRate").val());
            $("#<%=hdnCurrrencyExchangeRate.ClientID%>").val(sExcRate);

        });

        function returnToPOS(secLang) {
            window.location.href = ('../POS/POS.aspx');
        }


        function PrintReturnReceipt(secLang) {
            window.open('../pos/Print.aspx?status=1&ReturnReceipt=' + 'yes', 'Printing Receipt', 'width=300,height=300,toolbar=0,menubar=0,location=0,scrollbars=0,resizable=0,directories=0,status=0');
        }


        function PrintRcpt(secLang) {
            window.open('../pos/Print.aspx?status=1&ReturnReceipt=' + 'yes', 'Printing Receipt', 'width=300,height=300,toolbar=0,menubar=0,location=0,scrollbars=0,resizable=0,directories=0,status=0');
        }

        function printReceipt(printOrdID,accRcbID,secLang) {
            PrintRcpt(secLang);
            window.open('../Common/Print.aspx?ReqID=' + printOrdID + '&AccRcbID=' + accRcbID + '&DocTyp=RtnRcpt', 'MerchantD Receipt', 'width=300,height=300,toolbar=0,menubar=0,location=0,scrollbars=0,resizable=0,directories=0,status=0');
        }
        var isToCallCloseEvent =0;
        function PrintReturnReceipt(secLang, isPrint, partID) {
            if (isPrint == "1") {
                window.open('../pos/Print.aspx?status=1&ReturnReceipt=' + 'yes', 'Printing Receipt', 'width=300,height=300,toolbar=0,menubar=0,location=0,scrollbars=0,resizable=0,directories=0,status=0');
                window.location.href = ('../POS/POS.aspx?partID=' + partID);
            }
            else {
                window.location.href = ('../POS/POS.aspx?pandingPrintReceipt=1&partID=' + partID);
                isToCallCloseEvent ="1";
            }
        }

        function ShowPartialReturn() {
            var $dialog = jQuery.FrameDialog.create({
                url: $("#<%=hdnPartialReturnURl.ClientID%>").val(),
                    title: ($("#<%=hlPartialReturn.ClientID%>").attr("title")) ? $("#<%=hlPartialReturn.ClientID%>").attr("title") : "Partial Return",
                    loadingClass: "loading-image",
                    modal: true,
                    width: "800",
                    height: "550",
                    autoOpen: false
                    ,
                    close: function() 
                    {
                        if(isToCallCloseEvent =="0")
                        {
                            var invokSrc = $.getParamValue('InvokeSrc');
                            if (invokSrc == "POS") 
                            {
                                var custID = $("#<%=hdnPartnerID.ClientID%>").val();
                                if(custID>0)
                                {
                                    window.location.href= ('../POS/POS.aspx?partID='+custID);
                                }
                            }
                        }
                    }
                });
                $dialog.dialog('open'); return false;
            }

            function ShowPratialTransfer() {
                var $dialog = jQuery.FrameDialog.create({
                    url: $("#<%=hdnPartialTranferURl.ClientID%>").val(),
                    title: "<%=Resources.Resource.lblPartialTransfer%>",
                    loadingClass: "loading-image",
                    modal: true,
                    width: "800",
                    height: "550",
                    autoOpen: false
                });
                $dialog.dialog('open'); return false;
            }

            function setOrderAddress(addrID, ctlrID, addType) {
                $("#"+ctlrID +"Txt" ).val(addrID);
                var dataToPost = {};
                dataToPost.addrId = addrID;
                dataToPost.ordID = <%=this.OrderID%>;
            dataToPost.addType = addType;
            $.ajax({
                type: "POST",
                url: 'ViewOrderDetails.aspx/SetCustomerOrderAddress',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(dataToPost),
                success: function (data) {
                    var addrObj = data.d;
                    $("#" + ctlrID).html(addrObj);
                },
                error: function (request, status, errorThrown) {
                    alert(status);
                    //                    masterUnblockContentArea(eleToBlock);
                }
            });
        }


        function redirectOrderEdit(ordID) {
            window.location.href = '../Sales/ViewOrderDetails.aspx?SOID=' + ordID;
        }
        function redirectInvoiceEdit(invID) {
            window.location.href = '../invoice/ViewInvoiceDetails.aspx?SOID=' + invID + '&ComID=5';
        }


        function ShowAddProduct() {
            var vTitle ="<%=Resources.Resource.lblOrderAddProduct%>";

            var vordType = $.getParamValue('orderType');
            if(vordType =="2")
            {
                vTitle =  "<%=Resources.Resource.lblTransferAddProduct%>";
             }

             var $dialog = jQuery.FrameDialog.create({
                 url:  $("#<%=hdnAddProductURl.ClientID%>").val(),
                title: vTitle,
                loadingClass: "loading-image",
                modal: true,
                width: "1000",
                //height: "500",
                height: "150",
                position:"top",
                autoOpen: false
            });
            $dialog.dialog('open'); return false;
        }

        $(document).ready(function () {
            var invokSrc = $.getParamValue('InvokeSrc');
            var isReturnLink = $.getParamValue('isreturn');
            var isTransferLink = $.getParamValue('isTransfer');
            var ordFromPOS = $.getParamValue('orderType');

            if (invokSrc == "POS" && isReturnLink == "1") {
                //$("#<%=hlPartialReturn.ClientID%>").trigger("click");
                ShowPartialReturn();
            }
            if (invokSrc == "POS" && isTransferLink == "1") {
                ShowPratialTransfer();

            }
            if (invokSrc == "POS" && ordFromPOS == "2") {
                ShowAddProduct();
            }



            if( $("#<%=dlStatus.ClientID%> :selected").val() !="C")
            {
                $("#<%=ddlReason.ClientID%>").hide();
                $("#<%=lblReason.ClientID%>").hide();
            }

        });

        function jqGridResize() {
        }

        function LoadShipmentPopup(ordID)
        {
            $("#<%=hdnShpToOrder.ClientID%>").val(ordID);
            $("#<%=btnOrderNO.ClientID%>").trigger("click");
        }

        $("#<%=btnOrderNO.ClientID%>").click(function () {
            if (!popupOpen) {
                return goToShipmentPage();
            }
            else {
                popupOpen = false;
                return false;
            }
        });

        var popupOpen = false;      
        function goToShipmentPage() {           
            var oid = $("#<%=hdnShpToOrder.ClientID%>").val();
            if (isNaN(oid)) {                
                alert("Invalid Odrder Number!");
                return false;
            }

            var  vTitle = $("#<%=hdnTitleUpdateShipment.ClientID%>").val();

            var $dialog = jQuery.FrameDialog.create({
                url: "../shipping/Shipment.aspx?callBack=setEmptyText&oid=" + oid,
                title: vTitle, // "<%=Resources.Resource.lblUpdateShipment%>"
                loadingClass: "loading-image",
                modal: true,
                width: "800",
                height: "560",
                autoOpen: false
            }).bind("dialogclose", function () {
                popupOpen = false;
            });
            $dialog.dialog('open');
            popupOpen = true;
            return false;
        }

        function setEmptyText() {
                
            var invokSrc = $.getParamValue('InvokeSrc');
            var isReturnLink = $.getParamValue('isreturn');
            var isTransferLink = $.getParamValue('isTransfer');
            var ordFromPOS = $.getParamValue('ordType');
            if(  invokSrc =="POS" && ordFromPOS =="2")
            {
                window.location.href ="../sales/ViewSalesOrder.aspx?ordType=2&BackFromOrder=1&InvokeSrc=POS&PartnerID=" + $("#<%=hdnPartnerID.ClientID%>").val();
           }
           else
           {
               window.location.href ="../sales/ViewSalesOrder.aspx";//?ordType=" + $( "#<%=dlstOderType.ClientID%> option:selected" ).value(); +"" ?ordType="+ ordFromPOS +"
           }
       }


       function ScrollPageAtTop()
       {
           $(window).scrollTop(0);
       }

       $("#<%=dlStatus.ClientID%>").live("change", function () {
            var state = $(this).val();
            if(state =="C")
            {
                $("#<%=ddlReason.ClientID%>").show();
                $("#<%=lblReason.ClientID%>").show();
                $("#<%=btnShipNow.ClientID%>").hide();
            }
            else
            {
                $("#<%=ddlReason.ClientID%>").hide();
                $("#<%=lblReason.ClientID%>").hide();
                $("#<%=btnShipNow.ClientID%>").show();
            }
        });

    </script>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    CodeFile="mdPlanShipment.aspx.cs" Inherits="Sales_mdPlanShipment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript" src="../lib/scripts/jquery-plugins/JqGridHelper2.js"></script>
    <style type="text/css">
        .validate
        {
            background-color:Green;
            background-image:none;            
        }
        .inValidate
        {
            background-color:Red;
            background-image:none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="formContainer">
        <asp:Panel ID="pnlEditGrid" runat="server" Style="padding: 5px;">
            <h3>
                <asp:Label ID="Label1" Text="<%$Resources:Resource, lblSetQuantityToBeShipped %>"
                    runat="server" />
            </h3>
            <div id="grid_wrapperShipment">
                <trirand:JQGrid runat="server" ID="JQGridShipment" Height="100%" PagerSettings-PageSize="50"
                    AutoWidth="true" OnDataRequesting="JQGridShipment_DataRequesting" OnCellBinding="JQGridShipment_CellBinding">
                    <Columns>
                        <trirand:JQGridColumn DataField="ID" HeaderText="ID" Width="50" Visible="false" PrimaryKey="true" />
                        <trirand:JQGridColumn DataField="ProductID" HeaderText="ID" Width="50" />
                        <trirand:JQGridColumn DataField="UPCCode" HeaderText="UPC" Editable="false" Width="120"
                            Visible="true" />
                        <trirand:JQGridColumn DataField="OriginalPrdName" HeaderText="<%$ Resources:Resource, grdPOProductName %>"
                            Width="150" />
                        <trirand:JQGridColumn DataField="ProductName" HeaderText="Description" Width="150" />
                        <trirand:JQGridColumn DataField="PrdCollection" HeaderText="<%$ Resources:Resource, lblCollection %>" Width="100" />
                        <trirand:JQGridColumn DataField="PrdStyle" HeaderText="<%$ Resources:Resource, lblGrdStyle %>" Width="100" />
                        <trirand:JQGridColumn DataField="PrdColor" HeaderText="<%$ Resources:Resource, lblGrdColor %>" Width="100" />
                        <trirand:JQGridColumn DataField="PrdSize" HeaderText="<%$ Resources:Resource, lblSize %>" Width="50" />
                        <trirand:JQGridColumn DataField="Quantity" HeaderText="<%$ Resources:Resource, lblQtyInOrder%>"
                            Width="100" TextAlign="Center" />
                        <trirand:JQGridColumn DataField="PreviousReturnedQty" HeaderText="<%$ Resources:Resource, lblBalanceQty%>"
                            Width="100" TextAlign="Center" />
                        <trirand:JQGridColumn DataField="ToShipQty" HeaderText="<%$ Resources:Resource, lblToShipQty%>"
                            Width="100" TextAlign="Center" />
                        <trirand:JQGridColumn DataField="ID" HeaderText="Save" Editable="false" Width="30"
                            TextAlign="Center" />
                    </Columns>
                    <PagerSettings PageSize="100" PageSizeOptions="[100,150,200]" />
                    <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                        ShowDeleteButton="false" ShowSearchButton="false" />
                    <SortSettings InitialSortColumn="" />
                    <AppearanceSettings AlternateRowBackground="True" />
                    <ClientSideEvents LoadComplete="gridLoadComplete" BeforeRowSelect="beforeRowSelect" />
                </trirand:JQGrid>
            </div>
            <div class="div_command">
                            <asp:Button ID="btnValidateSKU" runat="server" Text="<%$Resources:Resource,lblValidateSKU%>"   />
                            <iCtrl:IframeDialog ID="mdbtnValidateSKU" TriggerControlID="btnValidateSKU" Width="780"
                                Height="450" Url="AddEditCurrency.aspx?jscallback=reloadGrid" Title="<%$Resources:Resource,lblOrderNewValidateSKU%>"
                                runat="server">
                            </iCtrl:IframeDialog>
                <asp:Button ID="btnShipment" Text="<%$Resources:Resource, lblCreateShipment %>" runat="server" 
                    OnClientClick="" CausesValidation="false" OnClick="btnShipment_Click" />
                <asp:Button Text="<%$Resources:Resource, btnCancel %>" ID="btnCancel" runat="server"
                    CausesValidation="False" OnClientClick="jQuery.FrameDialog.closeDialog(); return false;" />
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $("#global_message_container").prependTo("#formContainer");

        $grid1 = $("#<%=JQGridShipment.ClientID%>");
        $grid1.initGridHelper({
            searchPanelID: "pnlSearch",
            searchButtonID: "btnSearch",
            gridWrapPanleID: "grid_wrapperShipment"
        });

        function jqGridResize() {
            $("#<%=JQGridShipment.ClientID%>").jqResizeAfterLoad("grid_wrapperShipment", 0);
        }

        function reloadGrid(event, ui) {
            $grid1.trigger("reloadGrid");
            jqGridResize();
        }

        function gridLoadComplete(data) {
            jqGridResize();

            //Call back Sync Message
            if (typeof getGlobalMessage == 'function') {
                getGlobalMessage();
            }
        }

        //prevent jq grid from row selection
        function beforeRowSelect(rowid, e) {
            return false;
        }

        function saveShipmentDataInCart(eleId, key) {
            $inp = $("#" + eleId);

            var dataToPost = {};
            dataToPost.editRowKey = key;
            dataToPost.qtyToShip = $inp.val();

            for (var k in dataToPost) {
                $grid1.setPostDataItem(k, dataToPost[k]);
            }

            $grid1.trigger("reloadGrid");

            for (var k in dataToPost) {
                $grid1.removePostDataItem(k);
            }
        }


        $(document).ready(function () {
            parent.ScrollPageAtTop();
            var vdialogID = $.FrameDialog._getUid();
            window.parent.$("#" + vdialogID).dialog({ position: 'center' });
        });

        function ValidateSKU(validSKU) {
            if (validSKU == "1") {
                $("#<%=btnValidateSKU.ClientID%>").addClass("validate");
                $grid1.trigger("reloadGrid");
            }
            else {
                $("#<%=btnValidateSKU.ClientID%>").addClass("inValidate");
                $grid1.trigger("reloadGrid");
            }
        }

    </script>
</asp:Content>

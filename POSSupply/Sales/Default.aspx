<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="Default.aspx.vb" Inherits="Sales_Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();
        $('#divSectionTitle1').corner();
        $('#<%=SearchPanel.ClientID %>').corner();
    </script>
   
    <div id="divSectionTitle1" class="divSectionTitle">
        <h2>
            <%= Resources.Resource.lblSearchOptions %>
        </h2>
    </div>
    <asp:Panel runat="server" CssClass="divSectionContent" ID="SearchPanel" DefaultButton="imgSearch">
        <table id="Table1" cellpadding="1" cellspacing="1" border="0" runat="server">
            <tr>
                <td align="left">
                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"></asp:Label>
                    <br class="brMargin5"/>
                    <asp:DropDownList ID="dlSearch" runat="server" Width="175px" 
                        ValidationGroup="PrdSrch">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lblSearchKeyword%>"></asp:Label>
                    <br class="brMargin5"/>
                    <asp:TextBox runat="server" Width="170px" ID="txtSearch" ValidationGroup="PrdSrch"
                        AutoPostBack="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:ImageButton ID="imgSearch" runat="server" AlternateText="Search" ImageUrl="../images/search-btn.gif"
                        ValidationGroup="PrdSrch" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

<asp:Content ID="cntSalesGenerated" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal>
                    </h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td align="right" style="width: 150px;">
                    
                </td>
            </tr>
        </table>       
        
    </div>

    <div class="divMainContent" onkeypress="return disableEnterKey(event)">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center" colspan="2">
                    <asp:UpdatePanel ID="upnlMsg" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td height="2" colspan="2">
                </td>
            </tr>
            <tr>
                <td align="left">
                    
                </td>
                <td align="right">
                    <asp:ImageButton ID="imgCsv" runat="server" AlternateText="Search" 
                        ImageUrl="../images/xls.png" />
                </td>
            </tr>
            <tr>
                <td height="3" colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="2" height="350" valign="top">
                    <asp:GridView ID="grdPO" runat="server" AllowSorting="True" DataSourceID="sqldsPO"
                        AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0"
                        GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
                        CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="ordID" 
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="ordID" HeaderText="<%$ Resources:Resource, grdSOOrderNo %>"
                                ReadOnly="True" SortExpression="ordID">
                                <ItemStyle Width="90px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PartnerAcronyme" HeaderText="<%$ Resources:Resource, grdCMAcronyme %>"
                                ReadOnly="True" SortExpression="PartnerAcronyme">
                                <ItemStyle Width="75px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CustomerName" HeaderText="<%$ Resources:Resource, grdSOCustomerName %>"
                                ReadOnly="True" SortExpression="CustomerName">
                                <ItemStyle Width="240px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="amount" HeaderText="<%$ Resources:Resource, POTotalAmount %>"
                                ReadOnly="True" SortExpression="amount" HeaderStyle-HorizontalAlign="Right">
                                <HeaderStyle CssClass="price" />
                                <ItemStyle Width="150px" Wrap="true" CssClass="price" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordDate" HeaderText="<%$ Resources:Resource, grdSOCreatedDate %>"
                                ReadOnly="True" SortExpression="ordDate">
                                <ItemStyle Width="150px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="orderTypeDesc" NullDisplayText="-" HeaderText="<%$ Resources:Resource, grduserOderType %>"
                                ReadOnly="True" SortExpression="orderTypeDesc">
                                <ItemStyle Width="150px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="" HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("ordID") %>'
                                        CommandName="Edit" ImageUrl="~/images/Approve.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="70px" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="grid_footer" />
                        <RowStyle CssClass="grid_rowstyle" />
                        <PagerStyle CssClass="grid_footer" />
                        <HeaderStyle CssClass="grid_header" Height="26px" />
                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                        <PagerSettings PageButtonCount="20" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqldsPO" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <br />
</asp:Content>

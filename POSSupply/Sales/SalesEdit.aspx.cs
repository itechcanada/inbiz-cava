﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;

public partial class Sales_SalesEdit : BasePage
{    
    protected void Page_PreInit(object sender, EventArgs e)
    {
        string url = "~/Sales/ViewOrderDetails.aspx";
        if (!string.IsNullOrEmpty(url))
        {
            Response.Redirect(url + "?" + Request.QueryString);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {        
        
    }
}
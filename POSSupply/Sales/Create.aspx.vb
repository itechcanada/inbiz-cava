Imports Resources.Resource
Imports System.Configuration
Imports System.Threading
Imports System.Globalization
Imports AjaxControlToolkit
Imports System.Data.Odbc
Imports System.Data
Imports clsCommon
Partial Class Sales_Create
    Inherits BasePage
    Private strProcessID As String = ""
    Private objSO As New clsOrders
    Private objCust As New clsExtUser
    Dim objDT As System.Data.DataTable
    Dim objDR As System.Data.DataRow
    Dim objDT_SP As System.Data.DataTable
    Dim objDR_SP As System.Data.DataRow
    Public objTAx As New clsTaxCodeDesc
    Public objProduct As New clsProducts
    Dim objOrderType As New clsOrderType
    Public objordinvfrequency As New clsOrdInvFrequency
    Dim objPrdKit As New clsProductKits
    Public objPrdDesc As New clsPrdDescriptions
    Public objPrdQuantity As New clsPrdQuantity
    Dim objDTKit As System.Data.DataTable
    Dim objDRKit As System.Data.DataRow
    Dim objOrdkit As New clsOrderKit
    Dim objOdrJnrl As New clsOrderJournal
    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Convert.ToString(Request.QueryString("lang")) = "logout" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If (Request.Browser.Browser = "Firefox") Then
            Response.Cache.SetNoStore()
        End If
        If Request.QueryString("Module") = "TM" Then
            cmdReset.Visible = False
        Else
            cmdback.Visible = False
        End If
        If Session("OrderEntry") <> "" Then
            Session.Remove("OrderEntry")
        End If
        If Request.QueryString("custID") <> "" And Request.QueryString("custName") <> "" Then
            lblName.Text = lblSOCustomer & " " & Request.QueryString("custName")
        End If
        divDiscount.Style("Display") = "none"
        divKit.Style("display") = "none"
        If Request.QueryString("SOID") = "" Then
            If Request.QueryString("custID") = "" Then
                Response.Redirect("Generate.aspx")
            ElseIf Session("SalesReload") = "1" Then
                Session("SalesReload") = "0"
                Response.Redirect("Create.aspx?custID=" & Request.QueryString("custID") & "&custName=" & Request.QueryString("custName") & "&ComID=" & Request.QueryString("ComID") & "&whsID=" & Request.QueryString("whsID") & "&Module=" & Request.QueryString("Module") & "&SFU=" & Request.QueryString("SFU") & "&LP=" & Request.QueryString("LP"))
            End If
        End If
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            If Session("Msg") = "." Then
                lblMsg.Text = ""
            End If
            Session.Remove("Msg")
        End If

        If Not IsPostBack Then            
            For i As Integer = 1 To 31
                ddlDays.Items.Add(i.ToString())
            Next

            objProduct.subGetProduct(dlProduct, "1")
            subMakeKit()
            objOrderType.subGetOrderType(dlstOderType)
            objTAx.PopulateTaxGroup(dlTaxCode)

            lblTitle.Text = lblSOCreateQuotation
            txtDate.Text = Now.ToString("MM/dd/yyyy")
            If ((Date.Now.Hour = 0) Or (Date.Now.Hour < 7) Or (Date.Now.Hour = 7 AndAlso Date.Now.Minute < 59)) Then
                txtShippingDate.Text = Now.ToString("MM/dd/yyyy")
            Else
                txtShippingDate.Text = Now.AddDays(1).ToString("MM/dd/yyyy")
            End If
            txtQutExpDate.Text = Now.ToString("MM/dd/yyyy")

            subPopulateCurrency()
            subPopulateWarehouse()
            subPopulateCustomer()
            subPopulateTerms()
            If Convert.ToString(Session("IsSalesCart")) <> "Yes" Then
                makeCart()
                Session("IsSalesCart") = "Yes"
            End If
            If Request.QueryString("SOID") <> "" Then
                copyOrder()
                If Convert.ToString(Session("IsSalesProcessCart")) <> "Yes" Then
                    makeProcessCart()
                    Session("IsSalesProcessCart") = "Yes"
                End If
            End If
            If Convert.ToString(Session("SalesCart")) <> "" Then
                grdRequest.DataSource = Session("SalesCart")
                grdRequest.DataBind()
                If grdRequest.Rows.Count > 0 Then
                    lblPOIL.Visible = True
                Else
                    lblPOIL.Visible = False
                End If
            End If
            If Convert.ToString(Session("SalesProcessCart")) <> "" Then
                grdAddProcLst.DataSource = Session("SalesProcessCart")
                grdAddProcLst.DataBind()
                If grdAddProcLst.Rows.Count > 0 Then
                    divPrcList.Style.Add(System.Web.UI.HtmlTextWriterStyle.Display, "")
                    lblProcessList.Visible = True
                Else
                    divPrcList.Style.Add(System.Web.UI.HtmlTextWriterStyle.Display, "none")
                    lblProcessList.Visible = False
                End If
            End If
            TriggerCustFavButtonCommand()
            txtSearch.Focus()
            Me.txtShpToAddress.Attributes.Add("OnClick", "return popUp('" & divShipToAddress.ClientID & "');")
            Me.txtBillToAddress.Attributes.Add("OnClick", "return popUp('" & divBillToAddress.ClientID & "');")
        End If

            If Convert.ToString(Session("SalesCart")) <> "" Then
                If grdRequest.Rows.Count > 0 Then
                    lblPOIL.Visible = True
                Else
                    lblPOIL.Visible = False
                End If
            End If
            If Convert.ToString(Session("SalesProcessCart")) <> "" Then
                If grdAddProcLst.Rows.Count > 0 Then
                    divPrcList.Style.Add(System.Web.UI.HtmlTextWriterStyle.Display, "")
                    lblProcessList.Visible = True
                Else
                    divPrcList.Style.Add(System.Web.UI.HtmlTextWriterStyle.Display, "none")
                    lblProcessList.Visible = False
                End If
            End If
            If Request.Browser.Browser.ToString() = "IE" Then
                divViewPrd.Style("left") = "205"
            Else
                divViewPrd.Style("left") = "210"
            End If
            txtDiscount.ReadOnly = True
            'DivSubcription.Style("display") = "none"
    End Sub
    ' Make Cart
    Sub makeCart()
        objDT = New System.Data.DataTable("Cart")
        objDT.Columns.Add("ID", GetType(Integer))
        objDT.Columns("ID").AutoIncrement = True
        objDT.Columns("ID").AutoIncrementSeed = 1
        objDT.Columns.Add("ProductID", GetType(Integer))
        objDT.Columns.Add("UPCCode", GetType(String))
        objDT.Columns.Add("ProductName", GetType(String))
        objDT.Columns.Add("Quantity", GetType(Double))
        objDT.Columns.Add("Price", GetType(Double))
        objDT.Columns.Add("RangePrice", GetType(Double))
        objDT.Columns.Add("WarehouseCode", GetType(String))
        objDT.Columns.Add("TaxGrp", GetType(String))
        objDT.Columns.Add("Discount", GetType(String))
        objDT.Columns.Add("DisType", GetType(String))

        Session("SalesCart") = objDT
    End Sub
    ' Add Cart
    Sub AddToCart()
        If Convert.ToString(Session("SalesCart")) <> "" Then
            objDT = Session("SalesCart")
            Dim Product = txtProductId.Text
            Dim blnMatch As Boolean = False

            For Each objDR In objDT.Rows
                If objDR("ProductID") = Product Then
                    If Convert.ToString(Session("eItm")) = "" Then
                        objDR("Quantity") += txtQuantity.Text
                        objDR("Price") = txtPrice.Text
                        objDR("RangePrice") = getProductSalePrice(txtProductId.Text, objDR("Quantity"))
                    Else
                        objDR("Quantity") = txtQuantity.Text
                        objDR("Price") = txtPrice.Text
                        objDR("RangePrice") = getProductSalePrice(txtProductId.Text, objDR("Quantity"))
                    End If
                    blnMatch = True
                    Exit For
                End If
            Next

            If Not blnMatch Then
                objDR = objDT.NewRow
                objDR("ProductID") = txtProductId.Text
                objDR("ProductName") = txtProductName.Text
                objDR("UPCCode") = txtUPCCode.Text

                If IsNumeric(txtQuantity.Text) = True Then
                    objDR("Quantity") = txtQuantity.Text
                Else
                    Exit Sub
                End If
                If IsNumeric(txtPrice.Text) = True Then
                    objDR("Price") = txtPrice.Text
                    objDR("RangePrice") = getProductSalePrice(txtProductId.Text, objDR("Quantity"))
                Else
                    Exit Sub
                End If

                objDR("WarehouseCode") = dlWarehouse.SelectedValue.ToString
                objDR("TaxGrp") = dlTaxCode.SelectedValue.ToString
                objDR("Discount") = txtDiscount.Text
                objDR("DisType") = rblstDiscountType.SelectedValue
                objDT.Rows.Add(objDR)
            End If
            txtQuantity.Text = ""
            lblMsg.Text = msgSOItemAddedSuccessfully '"Item Added Successfully"
        Else
            lblMsg.Text = ""
        End If
    End Sub
    ' On Edit Details
    Private Sub subEditDetails(ByVal strEditItem As String)
        If strEditItem <> "" Then
            objDT = Session("SalesCart")
            objDR = objDT.Rows(strEditItem)
            txtProductId.Text = objDR("ProductID").ToString
            txtProductName.Text = objDR("ProductName").ToString
            txtUPCCode.Text = objDR("UPCCode").ToString
            txtQuantity.Text = objDR("Quantity").ToString
            txtPrice.Text = objDR("Price").ToString
            dlWarehouse.SelectedValue = objDR("WarehouseCode")
            dlTaxCode.SelectedValue = objDR("TaxGrp")
            txtDiscount.Text = objDR("Discount")
            rblstDiscountType.SelectedValue = objDR("DisType")
            If rblstDiscountType.SelectedValue = "P" Then
                txtDiscount.Text = txtDiscount.Text & "%"
            End If
        End If
    End Sub
    ' On Edit To Cart
    Private Sub subEditToCart(ByVal strEditItem As String)
        objDT = Session("SalesCart")
        objDR = objDT.Rows(strEditItem)
        objDR("ProductID") = txtProductId.Text
        objDR("ProductName") = txtProductName.Text
        objDR("UPCCode") = txtUPCCode.Text
        objDR("Quantity") = txtQuantity.Text
        objDR("Price") = txtPrice.Text
        objDR("WarehouseCode") = dlWarehouse.SelectedValue
        objDR("TaxGrp") = dlTaxCode.SelectedValue.ToString
        objDR("Discount") = txtDiscount.Text
        objDR("DisType") = rblstDiscountType.SelectedValue
        txtDiscountVal.Text = txtDiscount.Text
    End Sub

    ' On Purchase Order Items Row Editing
    Protected Sub grdRequest_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdRequest.RowEditing
        lblMsg.Text = ""
        Session.Add("eItm", e.NewEditIndex)
        hlCloseProductEditPop.NavigateUrl = String.Format("javascript:funCloseLoc({0});", "false")
        divItems.Visible = True
        txtQuantity.Focus()
        txtProductId.Text = grdRequest.Rows(e.NewEditIndex).Cells(0).Text
        txtUPCCode.Text = grdRequest.Rows(e.NewEditIndex).Cells(1).Text
        txtProductName.Text = grdRequest.Rows(e.NewEditIndex).Cells(2).Text
        txtQuantity.Text = grdRequest.Rows(e.NewEditIndex).Cells(3).Text
        txtPrice.Text = grdRequest.Rows(e.NewEditIndex).Cells(4).Text
        dlWarehouse.SelectedValue = grdRequest.Rows(e.NewEditIndex).Cells(6).Text
        dlTaxCode.SelectedValue = grdRequest.Rows(e.NewEditIndex).Cells(7).Text
        txtDiscount.Text = grdRequest.Rows(e.NewEditIndex).Cells(8).Text
        rblstDiscountType.SelectedValue = grdRequest.Rows(e.NewEditIndex).Cells(11).Text
        txtDiscountVal.Text = txtDiscount.Text
        divViewPrd.Style("display") = "block"
        divPriceRnge.Style("Display") = "none"
        pnlProductSearchGrid.Style(HtmlTextWriterStyle.Display) = "none"
    End Sub
    ' On Row Deleting
    Protected Sub grdRequest_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdRequest.RowDeleting
        If Convert.ToString(Session("SalesCart")) <> "" Then
            objDT = Session("SalesCart")
            objDT.Rows(e.RowIndex).Delete()
            Session("SalesCart") = objDT
            grdRequest.DataSource = objDT
            grdRequest.DataBind()
            lblMsg.Text = msgSOItemRemovedFromList '"Item Removed From List"
        End If
    End Sub
    Protected Sub grdProcess_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdProcess.PageIndexChanging
        GetProductSearchHistory()
        'subSearch()
    End Sub
    'Editing
    Protected Sub grdProcess_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdProcess.RowEditing
        Session.Remove("eItm")
        txtQuantity.Focus()
        hlCloseProductEditPop.NavigateUrl = String.Format("javascript:funCloseLoc({0});", "true")
        strProcessID = grdProcess.DataKeys(e.NewEditIndex).Value.ToString()
        divItems.Visible = True
        txtProductId.Text = grdProcess.Rows(e.NewEditIndex).Cells(0).Text
        txtUPCCode.Text = grdProcess.Rows(e.NewEditIndex).Cells(1).Text
        txtProductName.Text = grdProcess.Rows(e.NewEditIndex).Cells(2).Text
        txtQuantity.Text = "1"
        txtQuantity.ToolTip = CInt(grdProcess.Rows(e.NewEditIndex).Cells(3).Text) - funCheckProductQuantity(txtProductId.Text)
        txtPrice.Text = grdProcess.Rows(e.NewEditIndex).Cells(4).Text
        dlTaxCode.SelectedValue = IIf(String.IsNullOrEmpty(hdnCustWhs.Value), "0", hdnCustWhs.Value) 'grdProcess.Rows(e.NewEditIndex).Cells(5).Text
        txtDiscount.Text = grdProcess.Rows(e.NewEditIndex).Cells(6).Text
        Dim txtDisType As TextBox = CType(grdProcess.Rows(e.NewEditIndex).FindControl("txtDisType"), TextBox)
        Dim txtwhs As TextBox = CType(grdProcess.Rows(e.NewEditIndex).FindControl("txtwhs"), TextBox)
        dlWarehouse.SelectedValue = txtwhs.Text
        rblstDiscountType.SelectedValue = txtDisType.Text
        txtDiscountVal.Text = txtDiscount.Text
        If rblstDiscountType.SelectedValue = "P" Then
            txtDiscount.Text = txtDiscount.Text & "%"
        End If
        divViewPrd.Style("display") = "block"
        divPriceRnge.Style("Display") = "none"
        pnlProductSearchGrid.Style(HtmlTextWriterStyle.Display) = "none"

        e.Cancel = True
        'GetProductSearchHistory()
    End Sub
    'Return Existing Product Quantity
    Private Function funCheckProductQuantity(ByVal sPrdID As String) As Integer
        Dim nCount As Double = 0
        Try
            If Session("SalesCart").ToString <> "" Then
                Dim oDT As System.Data.DataTable
                Dim oDR As System.Data.DataRow
                oDT = Session("SalesCart")
                Dim intCtr As Int16 = 0
                For Each oDR In oDT.Rows
                    intCtr += 1
                Next
                If intCtr = 0 Then
                    Return 0
                ElseIf intCtr > 0 Then
                    Dim blnMatch As Boolean = False
                    For Each oDR In oDT.Rows
                        If oDR("ProductID") = sPrdID Then
                            nCount = oDR("Quantity")
                            blnMatch = True
                            Exit For
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
        End Try
        Return nCount
    End Function
    ' Sql Data Source Event Track
    Protected Sub sqldsProcess_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsProcess.Selected
        If e.AffectedRows = 0 Then
            lblMsg.Text = NoDataFound
        End If
    End Sub
    ' Search Sub Routine
    Private Sub subSearch()
        If txtSearch.Text <> "" Then
            sqldsProcess.SelectCommand = objSO.funFillGridForQuotation(dlSearch.SelectedValue, txtSearch.Text, Request.QueryString("ComID"))
            lblMsg.Text = ""
        Else
            sqldsProcess.SelectCommand = objSO.funFillGridForQuotation(dlSearch.SelectedValue, txtSearch.Text, Request.QueryString("ComID"))
            lblMsg.Text = ""
            txtSearch.Focus()
        End If
        Session("PreviousSearchSql") = sqldsProcess.SelectCommand
        'divItems.Visible = False        
    End Sub

    Private Sub AlphaSearch(ByVal strAlpha As String)
        sqldsProcess.SelectCommand = objSO.SearchProductAdvanceForQuotation(strAlpha, Request.QueryString("ComID"), Request.QueryString("custID"))
        Session("PreviousSearchSql") = sqldsProcess.SelectCommand
        lblMsg.Text = ""
        txtSearch.Focus()
        pnlProductSearchGrid.Style.Add(HtmlTextWriterStyle.Display, "block")
        divViewPrd.Style("display") = "none"
        'divItems.Visible = False
    End Sub

    Private Sub GetProductSearchHistory()
        If Not IsPostBack Then
            Session.Remove("PreviousSearchSql")
        End If

        If Not Session("PreviousSearchSql") Is Nothing Then
            sqldsProcess.SelectCommand = Session("PreviousSearchSql")            
            lblMsg.Text = ""            
            'divItems.Visible = False
        End If
        pnlProductSearchGrid.Style.Add(HtmlTextWriterStyle.Display, "block")
        divViewPrd.Style("display") = "none"
    End Sub

    Protected Sub btnAlpha_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        AlphaSearch(e.CommandArgument)
    End Sub

    ' On Search Image Click
    Protected Sub imgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
        subSearch()
    End Sub
    'Populate Object
    Private Sub subSetData(ByRef objS As clsOrders)
        objS.OrdType = "QT"
        objS.OrdCustType = objCust.CustType
        objS.OrdCustID = objCust.CustID
        objS.OrdCreatedOn = Now.ToString("yyyy-MM-dd hh:mm:ss")
        objS.OrdCreatedFromIP = Context.Request.ServerVariables("REMOTE_ADDR").ToString
        If (Session("UserModules").ToString.Contains("QOA") = True Or Session("UserModules").ToString.Contains("ADM") = True) Then
            objS.OrdStatus = "A"
            objS.OrdVerified = 1
            objS.OrdVerifiedBy = Session("UserID")
        Else
            objS.OrdStatus = "N"
            objS.OrdVerified = 0
            objS.OrdVerifiedBy = ""
        End If
        objS.OrdStatus = "R" 'Ovverride the logic of approving order now it will Autometially gets Ready to ship
        objS.OrdSalesRepID = Session("UserID").ToString
        objS.ordCreatedBy = Session("UserID").ToString
        objS.OrdSaleWeb = rdlstISWeb.SelectedValue
        objS.OrdShpBlankPref = rblstShipBlankPref.SelectedValue
        objS.OrdLastUpdateBy = ""
        objS.OrdLastUpdatedOn = ""
        objS.OrdShpCode = txtShpCode.Text
        objS.OrdShpWhsCode = dlShpWarehouse.SelectedValue
        objS.OrdShpCost = txtShpCost.Text
        objS.ordCompanyID = Request.QueryString("ComID")
        If txtShippingDate.Text <> "" Then
            If IsDate(DateTime.ParseExact(txtShippingDate.Text, "MM/dd/yyyy", Nothing)) = True Then
                objS.OrdShpDate = DateTime.ParseExact(txtShippingDate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
                'objS.OrdShpDate = funDateformat(txtShippingDate.Text)
            Else
                objS.OrdShpDate = ""
            End If
        Else
            objS.OrdShpDate = ""
        End If

        objS.OrdShpTrackNo = txtShpTrackNo.Text
        objS.OrdCurrencyCode = dlCurrencyCode.SelectedValue
        Dim objCur As New clsCurrencies
        objS.OrdCurrencyExRate = objCur.getRelativePrice(dlCurrencyCode.SelectedValue)
        objCur = Nothing
        objS.OrdComment = txtNotes.Text
        objS.OrdCustPO = txtCustPO.Text
        If txtQutExpDate.Text <> "" Then
            If IsDate(DateTime.ParseExact(txtQutExpDate.Text, "MM/dd/yyyy", Nothing)) = True Then
                objS.QutExpDate = DateTime.ParseExact(txtQutExpDate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
                'objS.QutExpDate = funDateformat(txtQutExpDate.Text).ToString("yyyy-MM-dd hh:mm:ss")
            Else
                objS.QutExpDate = ""
            End If
        Else
            objS.QutExpDate = ""
        End If
        objS.ordShippingTerms = txtTerms.Text
        objS.ordNetTerms = txtNetTerms.Text
        objS.ordercommission = dlstOderType.SelectedValue
        objS.InvRefNo = txtInvNo.Text
        'Dim myDTFI As DateTimeFormatInfo = New DateTimeFormatInfo()
        'myDTFI.ShortDatePattern = "yyyy-MM-dd"

        If txtInvDate.Text <> "" Then
            'If IsDate(DateTime.ParseExact(txtInvDate.Text, "MM/dd/yyyy", Nothing)) = True Then
            'objS.invDate = funDateformat(txtInvDate.Text).ToString("yyyy-MM-dd hh:mm:ss")
            'objS.invDate = Convert.ToDateTime(txtInvDate.Text).ToString("yyyy-MM-dd hh:mm:ss")
            'objS.invDate = DateTime.Parse(txtInvDate.Text, myDTFI)
            'objS.invDate = DateTime.ParseExact(txtInvDate.Text, "yyyy-MM-dd hh:mm:ss", )
            'objS.invDate = Format(CDate(txtInvDate.Text), ("yyyy-MM-dd hh:mm:ss"))
            'objS.invDate = DateTime.ParseExact(txtInvDate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
            objS.invDate = iTECH.Library.Utilities.BusinessUtility.GetDateTime(txtInvDate.Text, iTECH.Library.Utilities.DateFormat.MMddyyyy).ToString("yyyy-MM-dd hh:mm:ss") 'DateTime.ParseExact(txtInvDate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
            'End If
        Else
        objS.invDate = "sysNull"
        End If
    End Sub
    'Populate Currency
    Private Sub subPopulateCurrency()
        Dim objCur As New clsCurrencies
        objCur.PopulateCurrency(dlCurrencyCode)
        objCur = Nothing
    End Sub
    'Populate Warehouse
    Private Sub subPopulateWarehouse()
        Dim objWhs As New clsWarehouses
        objWhs.PopulateWarehouse(dlWarehouse, Request.QueryString("ComID"))
        objWhs.PopulateWarehouse(dlShpWarehouse, Request.QueryString("ComID"))
        dlWarehouse.SelectedValue = Request.QueryString("whsID")
        dlShpWarehouse.SelectedValue = Request.QueryString("whsID")
        objWhs = Nothing
    End Sub
    'Populate Customer
    Private Sub subPopulateCustomer()
        Dim objCust As New clsExtUser
        objCust.CustID = Request.QueryString("custID")
        objCust.CustName = Request.QueryString("custName")
        objCust.getCustomerInfo()
        hdnCustWhs.Value = objCust.CustTaxCode
        txtNetTerms.Text = objCust.CustNetTerms
        dlCurrencyCode.SelectedValue = objCust.CustCurrency
        If objCust.CustType <> "" Then
            txtBillToAddress.Text = funPopulateAddress(objCust.CustType, "B", objCust.CustID).Replace("<br>", vbCrLf)
            txtShpToAddress.Text = funPopulateAddress(objCust.CustType, "S", objCust.CustID).Replace("<br>", vbCrLf)
        End If
        objCust = Nothing
    End Sub
    'Populate Address
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)

        If (sType = "B") Then
            txtBillToAddressLine1.Text = objAdr.AddressLine1
            txtBillToAddressLine2.Text = objAdr.AddressLine2
            txtBillToAddressLine3.Text = objAdr.AddressLine3
            txtBillToAddressCity.Text = objAdr.AddressCity
            If objAdr.AddressState <> "" Then
                If objAdr.getStateName <> "" Then
                    txtBillToAddressState.Text = objAdr.getStateName
                Else
                    txtBillToAddressState.Text = objAdr.getStateName
                End If
            End If
            If objAdr.AddressCountry <> "" Then
                If objAdr.getCountryName <> "" Then
                    txtBillToAddressCountry.Text = objAdr.getCountryName
                Else
                    txtBillToAddressCountry.Text = objAdr.AddressCountry
                End If
            End If
            txtBillToAddressPostalCode.Text = objAdr.AddressPostalCode
        Else
            txtShpToAddressLine1.Text = objAdr.AddressLine1
            txtShpToAddressLine2.Text = objAdr.AddressLine2
            txtShpToAddressLine3.Text = objAdr.AddressLine3
            txtShpToAddressCity.Text = objAdr.AddressCity
            If objAdr.AddressState <> "" Then
                If objAdr.getStateName <> "" Then
                    txtShpToAddressState.Text = objAdr.getStateName
                Else
                    txtShpToAddressState.Text = objAdr.getStateName
                End If
            End If
            If objAdr.AddressCountry <> "" Then
                If objAdr.getCountryName <> "" Then
                    txtShpToAddressCountry.Text = objAdr.getCountryName
                Else
                    txtShpToAddressCountry.Text = objAdr.AddressCountry
                End If
            End If
            txtShpToAddressPostalCode.Text = objAdr.AddressPostalCode
        End If

        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + "<br>"
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + "<br>"
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + "<br>"
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + "<br>"
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + "<br>"
            Else
                strAddress += objAdr.AddressState + "<br>"
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + "<br>"
            Else
                strAddress += objAdr.AddressCountry + "<br>"
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += "Postal Code: " + objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function
    'Populate Terms
    Private Sub subPopulateTerms()
        Dim objComp As New clsCompanyInfo
        objComp.CompanyID = Request.QueryString("ComID") 'objComp.funGetFirstCompanyName
        objComp.getCompanyInfo()
        txtTerms.Text = objComp.CompanyShpToTerms
        txtNotes.Text = ""
        objComp = Nothing
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub

    
    ''On Product Items Add in List
    'Protected Sub imgAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgAdd.ServerClick

    'End Sub
    ' On Save
    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Page.IsValid Then
            'Exit Sub
        End If

        objDT = Session("SalesCart")
        Dim intCtr As Int16 = 0
        For Each objDR In objDT.Rows
            intCtr += 1
        Next
        If intCtr = 0 Then
            lblMsg.Text = msgSOPlzAddProductInQuotationList '"Please add product in Quotation List."
            Exit Sub
        End If
        objCust.CustID = Request.QueryString("custID").ToString
        objCust.CustName = Request.QueryString("custName").ToString
        objCust.getCustomerInfo()
        subSetData(objSO)

        If ConfigurationManager.AppSettings("AllowCustomInvoiceEntry").ToLower = "true" Then
            If objSO.funDuplicateInvoice("IV", objSO.ordCompanyID, objSO.InvRefNo, "") = True Then
                lblMsg.Text = lblInvoiceExists
                txtInvNo.Focus()
                Exit Sub
            End If
        End If

        If objSO.insertOrders() = True Then 'Insert Sales Order
            Dim strID As String = funGetSOID()
            Dim objSOI As New clsOrderItems
            Dim objPrdQty As New clsPrdQuantity
            objDT = Session("SalesCart")
            For idx As Int16 = 0 To objDT.Rows.Count - 1
                subSetData(objSOI, idx)
                objSOI.OrdID = strID
                If objSOI.insertOrderItems() = True Then 'Insert Sales Order Items
                    objSOI.updateProductQuantity()
                End If
            Next

            ' insert into journal table
            Dim grdRowscount As Integer = grdRequest.Rows.Count
            Dim prdcount As Integer
            Dim tagid As String
            tagid = GetTagId()
            If GetTagId() = "" Then
                tagid = 1
            Else
                tagid = GetTagId() + 1
            End If
            For prdcount = 0 To grdRowscount - 1
                objOdrJnrl.Tagid = tagid
                objOdrJnrl.WhenOrdered = Now.ToString("yyyy-MM-dd hh:mm:ss")
                objOdrJnrl.Quantity = grdRequest.Rows(prdcount).Cells(3).Text
                objOdrJnrl.ProductId = grdRequest.Rows(prdcount).Cells(0).Text
                objOdrJnrl.unit = ""
                objOdrJnrl.WarehouseOrdered = dlShpWarehouse.SelectedItem.Text
                objOdrJnrl.Datetime = Now.ToString("yyyy-MM-dd hh:mm:ss")
                objOdrJnrl.UnitPrice = grdRequest.Rows(prdcount).Cells(4).Text
                objOdrJnrl.SalesOrder = funGetSOID()
                objOdrJnrl.InsertOrderJournal()

            Next


            If rbSubscription.Checked = "true" Then

                If txtDayMonth.Text.LastIndexOf(",") > 0 Then
                    Dim daysArr() As String = txtDayMonth.Text.Split(",")
                    Dim dayArrList As New ArrayList()

                    For index As Integer = 0 To daysArr.Length - 1
                        dayArrList.Add(Convert.ToInt32(daysArr(index)))
                    Next

                    dayArrList.Sort()

                    objordinvfrequency.OrderID = strID
                    objordinvfrequency.DayofTheMonth1 = IIf(dayArrList.Count > 0, dayArrList(0).ToString(), "0")
                    objordinvfrequency.DayofTheMonth2 = IIf(dayArrList.Count > 1, dayArrList(1).ToString(), "0")
                    objordinvfrequency.DayofTheMonth3 = IIf(dayArrList.Count > 2, dayArrList(2).ToString(), "0")
                    objordinvfrequency.DayofTheMonth4 = IIf(dayArrList.Count > 3, dayArrList(3).ToString(), "0")
                    objordinvfrequency.DayofTheMonth5 = IIf(dayArrList.Count > 4, dayArrList(4).ToString(), "0")

                    If txtEnddate.Text <> "" Then
                        objordinvfrequency.EndDateTime = DateTime.ParseExact(txtEnddate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
                    Else
                        objordinvfrequency.EndDateTime = "sysNull"
                    End If
                    If txtInvDate.Text <> "" Then
                        objordinvfrequency.LastInvoiceCreatedOnDT = DateTime.ParseExact(txtInvDate.Text, "MM/dd/yyyy", Nothing).ToString("yyyy-MM-dd hh:mm:ss")
                    Else
                        objordinvfrequency.LastInvoiceCreatedOnDT = "sysNull"
                    End If
                    objordinvfrequency.insertSubcription()

                End If


            End If
            Dim objSOIP As New clsOrderItemProcess
            Try
                If Session("SalesProcessCart").ToString <> "" Then
                    objDT_SP = Session("SalesProcessCart")
                    For idx As Int16 = 0 To objDT_SP.Rows.Count - 1
                        subSetDataProcess(objSOIP, idx)
                        objSOIP.OrdID = strID
                        If objSOIP.insertOrderItemProcess() = True Then 'Insert Sales Order Item Process

                        End If
                    Next
                End If
            Catch ex As Exception

            End Try



            Session("SalesCart") = ""
            Session("IsSalesCart") = ""
            Session("SalesProcessCart") = ""
            Session("IsSalesProcessCart") = ""
            Session.Remove("eItm")
            If (Session("UserModules").ToString.Contains("QOA") = True Or Session("UserModules").ToString.Contains("ADM") = True) Then
                ' Response.Redirect("~/Sales/ViewOrderDetails.aspx?Msg=1&SOID=" & strID)
            Else
                GetUserForQuotationAlert(strID, objCust.CustID, objCust.CustName)
            End If

            If Request.QueryString("Module") = "TM" Then
                Dim objAssLead As New clsAssignLead
                objAssLead.CustID = Request.QueryString("custID").ToString
                objAssLead.Active = "0"
                objAssLead.funUpdateAssignLead()
                Response.Redirect("~/TeleMarketing/LeadsActivities.aspx?Msg=1&SFU=" & Request.QueryString("SFU") & "&LP=" & Request.QueryString("LP"))
            Else
                Response.Redirect("~/Sales/viewSalesOrder.aspx?Msg=1")
            End If
        End If
    End Sub

    'get tag id
    Public Function GetTagId() As String
        Dim objDataClass As New clsDataClass
        Dim strSql, strData As String
        objDataClass.OpenDatabaseConnection()
        strSql = "SELECT Max(tagid) FROM orderjournal"
        strData = Convert.ToString(objDataClass.GetScalarData(strSql))
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return strData
    End Function


    Public Sub GetUserForQuotationAlert(ByVal strSOID As String, ByVal strCustID As String, ByVal strCustName As String)
        Dim strSQL As String
        Dim drObj As OdbcDataReader
        Dim objData As New clsDataClass
        strSQL = "SELECT  userID, userModules FROM users where userActive='1'"
        drObj = objData.GetDataReader(strSQL)
        While drObj.Read
            If drObj.Item("userModules").ToString.Contains("QOA") = True Or drObj.Item("userModules").ToString.Contains("ADM") = True Then
                subGenerateAlert(strSOID, drObj.Item("userID").ToString, strCustID, strCustName)
            End If
        End While
        drObj.Close()
        objData.CloseDatabaseConnection()
    End Sub
    ' Inserts Alert for SO Quotation  
    Protected Sub subGenerateAlert(ByVal strSOID As String, ByVal alrtUsrID As String, ByVal strCustID As String, ByVal custName As String)
        Dim objAlert As New clsAlerts
        objAlert.AlertPartnerContactID = strSOID ' SO ID
        objAlert.AlertComID = 0
        objAlert.AlertDateTime = DateTime.Now().ToString("yyyy-MM-dd HH:mm:ss") 'alrtDateTime
        objAlert.AlertUserID = alrtUsrID
        ' Create message
        Dim strNote As String = ""
        strNote = lblQoWaitingForYouApproval & "<br><br>" 'Sale Quotation for Approval.
        strNote += alrtlblSOID & " " & strSOID & "<br>" 'So No.
        strNote += alrtlblCustName & " " & custName & "<br>" 'Customer:
        Dim objUsr As New clsUser
        objUsr.UserID = Session("UserID")
        objUsr.getUserInfo()
        strNote += alrtlblAsgnBy & " " & objUsr.UserSalutation & " " & objUsr.UserFirstName & " " & objUsr.UserLastName 'Assigned by:
        objUsr = Nothing
        objAlert.AlertNote = strNote
        objAlert.AlertRefType = "SO"
        objAlert.insertAlerts()
    End Sub


    ' Populate Object
    Private Sub subSetData(ByRef objOI As clsOrderItems, ByVal strEditItem As String)
        If strEditItem <> "" And Convert.ToString(Session("SalesCart")) <> "" Then
            objDT = Session("SalesCart")
            objDR = objDT.Rows(strEditItem)
            objOI.OrdProductID = objDR("ProductID").ToString
            objOI.OrdProductQty = objDR("Quantity").ToString
            objOI.OrdProductUnitPrice = objDR("Price").ToString
            objOI.OrdWarehouse = objDR("WarehouseCode").ToString
            objOI.ordProductTaxGrp = objDR("TaxGrp").ToString
            objOI.ordProductDiscount = objDR("Discount").ToString.TrimEnd("%")
            objOI.orderItemDesc = objDR("ProductName").ToString
            objOI.ordProductDiscountType = objDR("DisType").ToString
        End If
    End Sub
    ' Populate Object
    Private Sub subSetDataProcess(ByRef objOIP As clsOrderItemProcess, ByVal strEditItem As String)
        If strEditItem <> "" And Convert.ToString(Session("SalesProcessCart")) <> "" Then
            objDT_SP = Session("SalesProcessCart")
            objDR_SP = objDT_SP.Rows(strEditItem)
            objOIP.OrdItemProcCode = objDR_SP("ProcessCode").ToString
            objOIP.OrdItemProcFixedPrice = objDR_SP("ProcessFixedCost").ToString
            objOIP.OrdItemProcPricePerHour = objDR_SP("ProcessCostPerHour").ToString
            objOIP.OrdItemProcHours = objDR_SP("TotalHour").ToString
            objOIP.OrdItemProcPricePerUnit = objDR_SP("ProcessCostPerUnit").ToString
            objOIP.OrdItemProcUnits = objDR_SP("TotalUnit").ToString
            objOIP.sysTaxCodeDescID = objDR_SP("TaxGrpID").ToString
        End If
    End Sub

    'Return SOID 
    Public Function funGetSOID() As String
        Dim objDataClass As New clsDataClass
        Dim strSql, strData As String
        objDataClass.OpenDatabaseConnection()
        strSql = "SELECT Max(ordID) FROM orders"
        strData = objDataClass.GetScalarData(strSql)
        objDataClass.CloseDatabaseConnection()
        objDataClass = Nothing
        Return strData
    End Function
    ' On Reset
    Protected Sub cmdReset_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("SalesCart") = ""
        Session("IsSalesCart") = ""
        'dlCustomer.SelectedValue = "0"
        Response.Redirect("Create.aspx")
    End Sub
    ' On Text Change
    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        subSearch()
        txtSearch.Focus()
    End Sub

    Protected Sub grdAddProcLst_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdAddProcLst.Load

    End Sub
    ' On Row Deleting
    Protected Sub grdAddProcLst_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdAddProcLst.RowDeleting
        If Convert.ToString(Session("SalesProcessCart")) <> "" Then
            objDT_SP = Session("SalesProcessCart")
            objDT_SP.Rows(e.RowIndex).Delete()
            Session("SalesProcessCart") = objDT_SP
            grdAddProcLst.DataSource = objDT_SP
            grdAddProcLst.DataBind()
            lblMsg.Text = msgSOProcessRemovedFromList '"Process Removed From List"
        End If
    End Sub
    ' On Column Sorting
    Protected Sub grdProcess_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdProcess.Sorting
        GetProductSearchHistory()
        'subSearch()
    End Sub

    Protected Sub copyOrder()
        Dim strSQL As String
        Dim strSOID As String
        strSOID = Convert.ToString(Request.QueryString("SOID"))

        strSQL = "SELECT p.productID,p.prdUPCCode,CASE oi.orderItemDesc='' WHEN False THEN oi.orderItemDesc Else p.prdName  END as prdName, oi.ordProductQty, oi.ordProductUnitPrice, o.ordShpWhsCode, oi.ordProductTaxGrp, oi.ordProductDiscount, oi.ordID,oi.ordProductDiscountType FROM orderitems as oi inner join products as p on p.productID = oi.ordProductID "
        strSQL += " inner join orders as o on o.ordID = oi.ordID and o.ordID =" & strSOID

        Dim objDC As New clsDataClass
        Dim drObj As OdbcDataReader
        drObj = objDC.GetDataReader(strSQL)
        objDT = Session("SalesCart")
        While drObj.Read

            Dim Product = drObj.Item("productID").ToString()
            Dim blnMatch As Boolean = False

            For Each objDR In objDT.Rows
                If objDR("ProductID") = Product Then
                    objDR("Quantity") += drObj.Item("productID").ToString()
                    objDR("Price") = drObj.Item("productID").ToString()
                    blnMatch = True
                    Exit For
                End If
            Next

            If Not blnMatch Then
                objDR = objDT.NewRow
                objDR("ProductID") = drObj.Item("productID").ToString()
                objDR("ProductName") = drObj.Item("prdName").ToString()
                objDR("UPCCode") = drObj.Item("prdUPCCode").ToString()
                objDR("Quantity") = drObj.Item("ordProductQty").ToString()
                objDR("Price") = drObj.Item("ordProductUnitPrice").ToString()
                objDR("WarehouseCode") = drObj.Item("ordShpWhsCode").ToString()
                objDR("TaxGrp") = drObj.Item("ordProductTaxGrp").ToString()
                objDR("Discount") = drObj.Item("ordProductDiscount").ToString()
                objDR("DisType") = drObj.Item("ordProductDiscountType").ToString()
                objDT.Rows.Add(objDR)
            End If
        End While
        drObj.Close()
        objDC.CloseDatabaseConnection()

        Dim objOrd As New clsOrders

        objSO.OrdID = strSOID
        objSO.getOrders()
        objCust.CustID = objSO.OrdCustID
        objCust.CustType = objSO.OrdCustType
        objCust.getCustomerInfoByType()
        txtNetTerms.Text = objCust.CustNetTerms
        dlCurrencyCode.SelectedValue = objSO.OrdCurrencyCode
        txtBillToAddress.Text = funPopulateAddress(objCust.CustType, "B", objCust.CustID).Replace("<br>", vbCrLf)
        txtShpToAddress.Text = funPopulateAddress(objCust.CustType, "S", objCust.CustID).Replace("<br>", vbCrLf)
        Dim c As New clsCurrencies

        txtNotes.Text = objSO.OrdComment
        rblstShipBlankPref.SelectedValue = objSO.OrdShpBlankPref
        rdlstISWeb.SelectedValue = objSO.OrdSaleWeb
        If objSO.OrdShpDate <> "" Then
            txtShippingDate.Text = CDate(objSO.OrdShpDate).ToString("MM/dd/yyyy")
        End If
        txtShpTrackNo.Text = objSO.OrdShpTrackNo
        txtShpCost.Text = objSO.OrdShpCost
        txtShpCode.Text = objSO.OrdShpCode
        dlCurrencyCode.SelectedValue = objSO.OrdCurrencyCode
        txtExRate.Text = objSO.OrdCurrencyExRate
        txtCustPO.Text = objSO.OrdCustPO
        If objSO.QutExpDate <> "" Then
            txtQutExpDate.Text = CDate(objSO.QutExpDate).ToString("MM/dd/yyyy")
        End If
    End Sub
    ' Make Process Cart
    Sub makeProcessCart()
        Dim objDT_SP As System.Data.DataTable
        Dim objDR_SP As System.Data.DataRow

        objDT_SP = New System.Data.DataTable("Cart")
        objDT_SP.Columns.Add("ID", GetType(Integer))
        objDT_SP.Columns("ID").AutoIncrement = True
        objDT_SP.Columns("ID").AutoIncrementSeed = 1

        objDT_SP.Columns.Add("TaxGrpID", GetType(Integer))
        objDT_SP.Columns.Add("TaxGrpName", GetType(String))

        objDT_SP.Columns.Add("ProcessID", GetType(Integer))
        objDT_SP.Columns.Add("ProcessCode", GetType(String))
        objDT_SP.Columns.Add("ProcessDescription", GetType(String))
        objDT_SP.Columns.Add("ProcessFixedCost", GetType(Double))
        objDT_SP.Columns.Add("ProcessCostPerHour", GetType(Double))
        objDT_SP.Columns.Add("ProcessCostPerUnit", GetType(Double))
        objDT_SP.Columns.Add("TotalHour", GetType(Integer))
        objDT_SP.Columns.Add("TotalUnit", GetType(Integer))

        Session("SalesProcessCart") = objDT_SP

        Dim strSQL As String
        Dim strSOID As String
        strSOID = Convert.ToString(Request.QueryString("SOID"))

        strSQL = "SELECT s.ProcessID, o.ordItemProcCode, s.ProcessDescription, o.ordItemProcFixedPrice, o.ordItemProcPricePerHour, o.ordItemProcPricePerUnit, o.ordItemProcHours, o.ordItemProcUnits "
        strSQL += "FROM orderitemprocess as o inner join sysprocessgroup as s on s.ProcessCode = o.ordItemProcCode where ordId = " & strSOID

        Dim objDC As New clsDataClass
        Dim drObj As OdbcDataReader
        drObj = objDC.GetDataReader(strSQL)
        objDT_SP = Session("SalesProcessCart")
        While drObj.Read

            Dim Product = drObj.Item("ProcessID").ToString()
            Dim blnMatch As Boolean = False

            For Each objDR_SP In objDT_SP.Rows
                If objDR_SP("ProcessID") = Product Then
                    blnMatch = True
                    Exit For
                End If
            Next
            If Not blnMatch Then
                objDR_SP = objDT_SP.NewRow
                objDR_SP("ProcessID") = drObj.Item("ProcessID").ToString()
                objDR_SP("ProcessCode") = drObj.Item("ordItemProcCode").ToString()
                objDR_SP("ProcessDescription") = drObj.Item("ProcessDescription").ToString()
                objDR_SP("ProcessFixedCost") = drObj.Item("ordItemProcFixedPrice").ToString()
                objDR_SP("ProcessCostPerHour") = drObj.Item("ordItemProcPricePerHour").ToString()
                objDR_SP("TotalHour") = drObj.Item("ordItemProcHours").ToString()
                objDR_SP("ProcessCostPerUnit") = drObj.Item("ordItemProcPricePerUnit").ToString()
                objDR_SP("TotalUnit") = drObj.Item("ordItemProcUnits").ToString()
                objDT_SP.Rows.Add(objDR_SP)
            End If
        End While
    End Sub
    'Save Address Object
    'Protected Sub cmdBillToAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBillToAddress.ServerClick

    'End Sub

    'Protected Sub cmdShpToAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdShpToAddress.ServerClick

    'End Sub
    'Protected Sub cmdAddDiscount_Click1(ByVal sender As Object, ByVal e As System.Web.UI.) Handles cmdAddDiscount.Click

    'End Sub
    Protected Sub txtDiscount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDiscount.TextChanged
        divDiscount.Style("display") = "block"
        If rblstDiscountType.SelectedValue = "P" Then
            txtDiscountVal.Text = txtDiscount.Text.TrimEnd("%")
        Else
            txtDiscountVal.Text = txtDiscount.Text
        End If
        txtDiscountVal.Focus()
    End Sub
    Protected Sub grdAddProcLst_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdAddProcLst.RowEditing
        ' Dim strProcessID As String = grdAddProcLst.DataKeys(e.NewEditIndex).Value.ToString()

        strProcessID = grdAddProcLst.DataKeys(e.NewEditIndex).Value.ToString()
        divSalesProcess.Style("display") = "block"

        txtProcessID.Text = strProcessID
        ' txtProcessCode.Text = grdAddProcLst.Rows(e.NewEditIndex).Cells(1).Text
        txtProcessDescription.Text = grdAddProcLst.Rows(e.NewEditIndex).Cells(2).Text
        txtTaxGrp.Text = grdAddProcLst.Rows(e.NewEditIndex).Cells(3).Text

        txtProcessFixedCost.Text = grdAddProcLst.Rows(e.NewEditIndex).Cells(4).Text
        txtProcessCostPerHour.Text = grdAddProcLst.Rows(e.NewEditIndex).Cells(5).Text
        txtProcessCostPerUnit.Text = grdAddProcLst.Rows(e.NewEditIndex).Cells(6).Text
        txtTotalHour.Text = grdAddProcLst.Rows(e.NewEditIndex).Cells(7).Text
        txtTotalUnit.Text = grdAddProcLst.Rows(e.NewEditIndex).Cells(8).Text
    End Sub

    Protected Sub imgAddProcess_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAddProcess.Click
        If IsNumeric(txtProcessFixedCost.Text) <> True Then
            lblMsg.Text = msgSOPlzEntFixedCost '"Please enter Fixed Cost."
            txtProcessFixedCost.Focus()
            Exit Sub
        End If
        If IsNumeric(txtProcessCostPerHour.Text) <> True Then
            lblMsg.Text = msgSOPlzEntCostPerHour '"Please enter Cost/Hour."
            txtProcessCostPerHour.Focus()
            Exit Sub
        End If
        If IsNumeric(txtProcessCostPerUnit.Text) <> True Then
            lblMsg.Text = msgSOPlzEntCostPerUnit '"Please enter Cost/Unit."
            txtProcessCostPerUnit.Focus()
            Exit Sub
        End If
        If IsNumeric(txtTotalHour.Text) <> True Then
            lblMsg.Text = msgSOPlzEntTotalHour '"Please enter Total Hour."
            txtTotalHour.Focus()
            Exit Sub
        End If
        If IsNumeric(txtTotalUnit.Text) <> True Then
            lblMsg.Text = msgSOPlzEntTotalUnit '"Please enter Total Unit."
            txtTotalUnit.Focus()
            Exit Sub
        End If
        objDT_SP = Session("SalesProcessCart")
        Dim ProcessID = txtProcessID.Text

        For Each objDR_SP In objDT_SP.Rows
            If objDR_SP("ProcessID") = ProcessID Then

                'objDR_SP("ProcessCode") = txtProcessCode.Text
                objDR_SP("ProcessDescription") = txtProcessDescription.Text
                objDR_SP("ProcessFixedCost") = txtProcessFixedCost.Text
                objDR_SP("ProcessCostPerHour") = txtProcessCostPerHour.Text
                objDR_SP("TotalHour") = txtTotalHour.Text
                objDR_SP("ProcessCostPerUnit") = txtProcessCostPerUnit.Text
                objDR_SP("TotalUnit") = txtTotalUnit.Text
                
                Exit For
            End If
        Next
        Session("SalesProcessCart") = objDT_SP
        grdAddProcLst.DataSource = Session("SalesProcessCart")
        grdAddProcLst.DataBind()
        divSalesProcess.Style("display") = "none"
        'Response.Redirect("~/Sales/Create.aspx?SOID=" & Request.QueryString("SOID") & "&CustID=" & Request.QueryString("CustID") & "&CustName=" & Request.QueryString("CustName") & "&ComID=" & Request.QueryString("ComID"))
    End Sub
    Protected Sub CmdPrdSave_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdPrdSave.ServerClick
        If AddToKit() = True Then
            subCalculateTotalPrice()
            lblKitMsg.Text = msgPrdKitList
            subFillGrid()
        End If
        subPosition()
    End Sub
    Public Sub subFillGrid()
        grdvProductKit.DataSource = objDTKit
        grdvProductKit.DataBind()
    End Sub
    Protected Sub grdvProductKit_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdvProductKit.PageIndexChanging
        subFillGrid()
    End Sub
    Protected Sub grdvProductKit_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdvProductKit.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim imgDelete As ImageButton = CType(e.Row.FindControl("imgDelete"), ImageButton)
            imgDelete.Attributes.Add("onclick", "javascript:return " & _
                "confirm('" & ConfirmDeleteKitProduct & " ')")
        End If
    End Sub
    Protected Sub grdvProductKit_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdvProductKit.RowDeleting
        objDTKit = Session("kit")
        lblKitMsg.Text = msgOrderTypeDeleted
        objDTKit.Rows(e.RowIndex).Delete()
        txtProdQty.Text = ""
        subCalculateTotalPrice()
        subPosition()
        subFillGrid()
    End Sub
    Protected Sub grdvProductKit_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdvProductKit.Sorting
        subFillGrid()
    End Sub
    ' Make Oder type Data Table
    Sub subMakeKit()
        objDTKit = New System.Data.DataTable("PrdKit")
        objDTKit.Columns.Add("ID", GetType(Integer))
        objDTKit.Columns("ID").AutoIncrement = True
        objDTKit.Columns("ID").AutoIncrementSeed = 1
        objDTKit.Columns.Add("prdIncludeID", GetType(String))
        objDTKit.Columns.Add("prdName", GetType(String))
        objDTKit.Columns.Add("prdIntID", GetType(String))
        objDTKit.Columns.Add("prdIncludeQty", GetType(String))
        objDTKit.Columns.Add("prdPrice", GetType(String))
        objDTKit.Columns.Add("prdUnitPrice", GetType(String))
        Session("Kit") = objDTKit
    End Sub
    ' Add Cart
    Public Function AddToKit() As Boolean
        objPrdKit.PrdIncludeQty = txtProdQty.Text
        objPrdKit.PrdIncludeID = dlProduct.SelectedValue

        Dim blnMatch As Boolean = False
        objDTKit = Session("kit")
        For Each objDRKit In objDTKit.Rows
            If objDRKit("prdIncludeID") = objPrdKit.PrdIncludeID Then
                lblKitMsg.Text = msgKitPrdExists
                Return False
            End If
        Next

        If Not blnMatch Then
            objDRKit = objDTKit.NewRow
            objDRKit("prdIncludeID") = objPrdKit.PrdIncludeID
            objProduct.ProductID = objPrdKit.PrdIncludeID
            objProduct.subGetProductsInfo()
            objDRKit("prdName") = objProduct.PrdName
            objDRKit("prdIntID") = objProduct.PrdIntID
            objDRKit("prdIncludeQty") = objPrdKit.PrdIncludeQty
            objDRKit("prdUnitPrice") = objProduct.PrdEndUserSalesPrice
            objDRKit("prdPrice") = objPrdKit.PrdIncludeQty * objProduct.PrdEndUserSalesPrice
            objDTKit.Rows.Add(objDRKit)
        End If


        Return True
    End Function
    Public Sub subCalculateTotalPrice()
        Dim dblTotalPrice As Double
        For Each objDRKit In objDTKit.Rows
            trCmdKit.Visible = True
            trKitPrice.Visible = True
            trAvaiWhs.Visible = True
            trPrd.Visible = True
            trBarCode.Visible = True
            If objDRKit("prdPrice") <> "" Then
                dblTotalPrice += objDRKit("prdPrice")
            End If
        Next
        If objDTKit.Rows.Count = 0 Then
            trCmdKit.Visible = False
            trKitPrice.Visible = False
            trAvaiWhs.Visible = False
            trPrd.Visible = False
            trBarCode.Visible = False
        End If
        lblWhsh.Text = funGetAvailWhs()
        txtKitPrice.Text = dblTotalPrice - (dblTotalPrice * ConfigurationManager.AppSettings("KitPercentage")) / 100
        ' txtProdQty.Text = ""
        'dlProduct.SelectedValue = 0
    End Sub


    ' Populate objects of PrdQuantity By given ProductID and warehouse
    Public Function funGetAvailWhs() As String
        Dim objData As New clsDataClass
        Dim strSQL, strHtml, strWComID As String
        strSQL = "SELECT  CompanyID FROM syscompanyinfo where CompanyID in (SELECT WarehouseCompanyID FROM syswarehouses where WarehouseCode='" & Session("UserWarehouse") & "') "
        strWComID = objData.GetScalarData(strSQL).ToString()
        Dim drObj As OdbcDataReader
        strSQL = "SELECT  WarehouseCode,WarehouseDescription FROM syswarehouses where WarehouseCompanyID='" & strWComID & "' "
        drObj = objData.GetDataReader(strSQL)
        strHtml = "<table width='400' border='1' cellpadding=0 cellspacing=0  style='border-color:lightgray;'> "
        While drObj.Read
            strHtml += "<tr><td width='50%' >"
            strHtml += drObj.Item("WarehouseCode").ToString & ":" & drObj.Item("WarehouseDescription").ToString
            strHtml += "</td><td width='50%'>"
            strHtml += funGetQty(drObj.Item("WarehouseCode").ToString)
            strHtml += "</td></tr>"
        End While
        strHtml += "</table> "
        drObj.Close()
        objData.CloseDatabaseConnection()
        Return strHtml
    End Function
    Public Function funGetQty(ByVal strWhsValue As String) As String
        Dim dblI As Double
        Dim dblOnHandQty As Double = 0
        Dim intI As Integer = 0
        For Each objDRKit In objDTKit.Rows
            dblI = (objPrdQuantity.funGetPrdQty(objDRKit("prdIncludeID"), strWhsValue)) / (objDRKit("prdIncludeQty"))
            If intI = 0 Then
                dblOnHandQty = dblI
            End If
            If dblOnHandQty > dblI Then
                'If dblI > 0.99 Then
                dblOnHandQty = dblI
                'End If
            End If
            intI += 1
        Next

        dblOnHandQty = Math.Floor(dblOnHandQty)
        Return dblOnHandQty
    End Function
    Protected Sub cmdFinalSubmit_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFinalSubmit.ServerClick
        Dim strOrdKitID As String
        objOrdkit.ordKitbarcode = txtBarcode.Text
        objOrdkit.ordKitName = txtProdName.Text
        objOrdkit.ordKitPrice = txtKitPrice.Text
        objOrdkit.funInsertOrderKit(strOrdKitID)
        objDTKit = Session("kit")
        For Each objDRKit In objDTKit.Rows
            objOrdkit.PrdIncludeID = objDRKit("prdIncludeID")
            objOrdkit.PrdIncludeQty = objDRKit("prdIncludeQty")
            objOrdkit.PrdID = strOrdKitID
            objOrdkit.funInsertOrdProductKits()
        Next
        lblMsg.Text = msgKitPriceAdded
        divKit.Style("display") = "none"
        objDTKit.Rows.Clear()
        txtBarcode.Text = ""
        txtProdName.Text = ""
        txtKitPrice.Text = ""
        objDRKit = Nothing
        objDTKit = Nothing
        subFillGrid()
    End Sub

    Public Sub subPosition()
        divKit.Style("display") = "block"
        divKit.Style("position") = "absolute"
        divKit.Style("left") = "200px" 'hdnLeft.Value & "px"
        divKit.Style("top") = "250px"
    End Sub

    Protected Sub cmdAddPriceKit_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddPriceKit.ServerClick
        subPosition()
        trCmdKit.Visible = False
        trKitPrice.Visible = False
        trAvaiWhs.Visible = False
        trPrd.Visible = False
        trBarCode.Visible = False
        lblKitMsg.Text = ""
        txtProdQty.Text = ""
        dlProduct.SelectedValue = 0
        objDTKit = Session("kit")
        objDTKit.Rows.Clear()
        objDRKit = Nothing
        objDTKit = Nothing
        subFillGrid()
        txtBarcode.Text = ""
        txtProdName.Text = ""
        txtKitPrice.Text = ""
        divViewPrd.Style("display") = "none"
    End Sub
    Protected Sub cmdback_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdback.ServerClick
        If Request.QueryString("Module") = "TM" Then
            Response.Redirect("~/TeleMarketing/LeadsActivities.aspx?SFU=" & Request.QueryString("SFU") & "&LP=" & Request.QueryString("LP"))
        End If
    End Sub
    Protected Sub imgAdd_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAdd.Click
        If Convert.ToString(Session("eItm")) <> "" Then
            subEditToCart(Convert.ToString(Session("eItm")))
            AddToCart()
        Else
            lblMsg.Text = ""
            If IsNumeric(txtQuantity.Text) <> True Then
                lblMsg.Text = msgSOItemQuantity '"Please enter Order Item Quantity."
                txtQuantity.Focus()
                Exit Sub
            End If
            'If IsNumeric(txtDiscount.Text) = True Then
            If rblstDiscountType.SelectedValue = "P" Then
                If CDbl(txtDiscount.Text.TrimEnd("%")) > 100 Then
                    lblMsg.Text = msgDiscountgreaterThen100 '"Discount should not be greater than 100%."
                    txtDiscount.Focus()
                    Exit Sub
                End If
            End If
            'End If
            If IsNumeric(txtQuantity.Text) = True Then
                'If CDbl(txtQuantity.Text) > CDbl(txtQuantity.ToolTip) Then
                '    lblMsg.Text = msgSOItemQuantityLessThanOnHand & "-(" & txtQuantity.ToolTip & ")."
                '    txtQuantity.Focus()
                '    Exit Sub
                'End If
                If CDbl(txtQuantity.Text) <= 0 Then
                    lblMsg.Text = msgSOItemQuantityGT0LessThanOnHand & "-(" & txtQuantity.ToolTip & ")."
                    txtQuantity.Focus()
                    Exit Sub
                End If
            End If
            If IsNumeric(txtPrice.Text) <> True Then
                lblMsg.Text = msgSOItemPrice '"Please enter Order Item Price."
                txtPrice.Focus()
                Exit Sub
            End If
            If IsNumeric(txtPrice.Text) = True Then
                If CDbl(txtPrice.Text) < 0 Then
                    lblMsg.Text = msgSOItemQuantityGT0 '"Please enter Order Item Price greater than 0."
                    txtPrice.Focus()
                    Exit Sub
                End If
            End If
            If dlWarehouse.SelectedValue = "0" Then
                lblMsg.Text = Resources.Resource.custvalPOWarehouse '"Please select Warehouse."
                dlWarehouse.Focus()
                Exit Sub
            End If
            AddToCart()
            pnlProductSearchGrid.Style(HtmlTextWriterStyle.Display) = "block"
        End If

        If Convert.ToString(Session("SalesCart")) <> "" Then
            grdRequest.DataSource = Session("SalesCart")
            grdRequest.DataBind()
            lblPOIL.Visible = True
            divItems.Visible = False
            txtSearch.Focus()
        End If
        divPriceRnge.Style("Display") = "none"
        'subSearch()

        'display product grid div only if it has rows
        divItemsGrid.Style("display") = IIf(grdRequest.Rows.Count > 0, "", "none")
        txtSearch.Text = ""
        txtSearch.Focus()
    End Sub

    Protected Sub cmdAddDiscount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddDiscount.Click
        divDiscount.Style("display") = "none"
        If txtDiscountVal.Text = "" Then
            txtDiscountVal.Text = "0"
        End If
        txtDiscount.Text = txtDiscountVal.Text
        If rblstDiscountType.SelectedValue = "P" Then
            txtDiscount.Text = txtDiscountVal.Text & "%"
        End If
        txtDiscount.Focus()
    End Sub

    Protected Sub cmdShpToAddress_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdShpToAddress.Click
        Dim objC As New clsExtUser
        objC.CustID = Request.QueryString("custID")
        objC.CustName = Request.QueryString("custName")
        objC.getCustomerInfo()
        Dim objAdd As New clsAddress
        objAdd.AddressRef = objC.CustType
        objAdd.AddressType = "S"
        objAdd.AddressSourceID = objC.CustID
        objAdd.AddressLine1 = txtShpToAddressLine1.Text
        objAdd.AddressLine2 = txtShpToAddressLine2.Text
        objAdd.AddressLine3 = txtShpToAddressLine3.Text
        objAdd.AddressCity = txtShpToAddressCity.Text
        objAdd.AddressState = txtShpToAddressState.Text
        objAdd.AddressCountry = txtShpToAddressCountry.Text
        objAdd.AddressPostalCode = txtShpToAddressPostalCode.Text
        If objAdd.funCheckDuplicateAddress() = True Then
            objAdd.AddressID = objAdd.funGetAddressID()
            objAdd.updateAddress()
        Else
            objAdd.insertAddress()
        End If
        subPopulateCustomer()
    End Sub

    Protected Sub cmdBillToAddress_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBillToAddress.Click
        Dim objC As New clsExtUser
        objC.CustID = Request.QueryString("custID")
        objC.CustName = Request.QueryString("custName")
        objC.getCustomerInfo()

        Dim objAdd As New clsAddress
        objAdd.AddressRef = objC.CustType
        objAdd.AddressType = "B"
        objAdd.AddressSourceID = objC.CustID
        objAdd.AddressLine1 = txtBillToAddressLine1.Text
        objAdd.AddressLine2 = txtBillToAddressLine2.Text
        objAdd.AddressLine3 = txtBillToAddressLine3.Text
        objAdd.AddressCity = txtBillToAddressCity.Text
        objAdd.AddressState = txtBillToAddressState.Text
        objAdd.AddressCountry = txtBillToAddressCountry.Text
        objAdd.AddressPostalCode = txtBillToAddressPostalCode.Text
        If objAdd.funCheckDuplicateAddress() = True Then
            objAdd.AddressID = objAdd.funGetAddressID()
            objAdd.updateAddress()
        Else
            objAdd.insertAddress()
        End If
        subPopulateCustomer()
    End Sub

    Protected Sub lbnPriceRange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbnPriceRange.Click
        Dim objProductSalePrice As New clsProductSalesPrice
        objProductSalePrice.ProductID = txtProductId.Text
        sqldsPriceRange.SelectCommand = objProductSalePrice.getGridViewFillerSQL()
        objProductSalePrice = Nothing
        divPriceRnge.Style("Display") = "block"
    End Sub

    Protected Sub cmdSuggestedPriceOK_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSuggestedPriceOK.ServerClick
        If (hdnSelectedId.Value <> "non") Then
            divPriceRnge.Style("Display") = "none"
            Dim objProductSalePrice As New clsProductSalesPrice
            objProductSalePrice.ProductSalesPriceID = hdnSelectedId.Value
            objProductSalePrice.subGetProductSalesPriceInfoById()

            txtQuantity.Text = objProductSalePrice.FromQty
            txtPrice.Text = objProductSalePrice.SalesPrice
            objProductSalePrice = Nothing
        End If
    End Sub
    Protected Sub grdRequest_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdRequest.RowDataBound
        Dim dblPrice As Double = 0
        Dim dblSugstPrice As Double = 0
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            e.Row.Cells(5).BackColor = e.Row.BackColor
            If e.Row.Cells(4).Text <> "" Then
                dblPrice = Convert.ToDouble(e.Row.Cells(4).Text)
            End If
            If e.Row.Cells(5).Text <> "" And e.Row.Cells(5).Text <> "&nbsp;" Then
                dblSugstPrice = Convert.ToDouble(e.Row.Cells(5).Text)
            End If
            If (dblPrice < dblSugstPrice) Then
                e.Row.Cells(5).BackColor = Drawing.Color.Pink
            Else
                e.Row.Cells(5).BackColor = e.Row.BackColor
            End If
        End If
    End Sub

    Protected Sub rbSubscription_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSubscription.CheckedChanged
        'DivSubcription.Style("display") = "block"
    End Sub

    'In order to allow whole row clickable 
    Protected Sub grdProcess_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdProcess.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'If e.Row.Cells(2).Text.ToLower().Contains("unit") Then
            '    e.Row.Cells(2).ForeColor = Drawing.Color.Green
            'End If

            Dim lnkSingleClick As LinkButton = CType(e.Row.FindControl("lnkSingleClick"), LinkButton)
            Dim jsSingle As String = ClientScript.GetPostBackClientHyperlink(lnkSingleClick, "")
            e.Row.Attributes("onclick") = jsSingle
        End If
    End Sub

    'In order to handle event validation for row click
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For Each r As GridViewRow In grdProcess.Rows
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(r.UniqueID + "$ctl00")
            End If
        Next

        MyBase.Render(writer)
    End Sub

    'Protected Sub grdProcess_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdProcess.RowCommand
    '    Dim _gridView As GridView = CType(sender, GridView)
    '    _gridView.EditIndex = -1
    '    Dim _selectedIndex As Integer = Integer.Parse(e.CommandArgument.ToString)
    '    Dim _commandName As String = e.CommandName
    '    Dim _eventArgument As String = Request.Form("__EVENTARGUMENT")
    '    Select Case (_commandName)
    '        Case "Edit"
    '            _gridView.SelectedIndex = _selectedIndex
    '        Case "DoubleClick"
    '            _gridView.EditIndex = _selectedIndex
    '            _gridView.SelectedIndex = -1                
    '    End Select
    'End Sub

    'Trigger Cust Fav button Command
    Private Sub TriggerCustFavButtonCommand()
        Dim t As Type = GetType(Button)
        Dim p() As Object = New Object(0) {}
        p(0) = New System.Web.UI.WebControls.CommandEventArgs(String.Empty, btnCustFav.CommandArgument)
        Dim m As System.Reflection.MethodInfo = t.GetMethod("OnCommand", (System.Reflection.BindingFlags.NonPublic Or System.Reflection.BindingFlags.Instance))
        m.Invoke(btnCustFav, p)
    End Sub

End Class

<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="Generate.aspx.vb" Inherits="Sales_Generate" ValidateRequest="false" %>

<%@ Register src="../Controls/CommonSearch/OrderSearch.ascx" tagname="OrderSearch" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();
    </script>

    <uc1:OrderSearch ID="OrderSearch1" runat="server" />

</asp:Content>

<asp:Content ID="cntGeneratePO" ContentPlaceHolderID="cphMaster" runat="Server">
    <asp:UpdatePanel ID="upnlSearchCustomer" runat="server">
        <ContentTemplate>
            <div id="divMainContainerTitle" class="divMainContainerTitle">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td style="width: 300px;">
                            <h2>
                                <asp:Literal runat="server" ID="lblTitle"></asp:Literal>
                            </h2>
                        </td>
                        <td style="text-align: right;">
                        </td>
                        <td align="right" style="width: 150px;">
                        </td>
                    </tr>
                </table>
            </div>
            <asp:Panel CssClass="divMainContent" runat="server" ID="pnlSO" DefaultButton="cmdSearch">
                <table width="100%" class="table">
                    <tr>
                        <td align="center" colspan="5">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td height="2" colspan="3">
                        </td>
                    </tr>
                    <tr id="trCustomer" runat="server" valign="middle">
                        <td width="20%" align="right">
                            <asp:Label ID="lblCustomer" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOCustomer %>"></asp:Label>
                        </td>
                        <td width="1%">
                        </td>
                        <td align="left" width="33%">
                            <asp:HiddenField ID="hdnCustID" runat="server"></asp:HiddenField>
                            <asp:TextBox ID="txtSearch" CssClass="innerText" runat="server" Width="218px" ValidationGroup="vgCustSrch"></asp:TextBox><span
                                class="style1"> *</span>
                            <asp:CustomValidator ID="custvalCustomer" SetFocusOnError="true" runat="server" ErrorMessage="<%$ Resources:Resource, msgPleaseEntrCustName %>"
                                ClientValidationFunction="funSelectCustomer" Display="None">
                            </asp:CustomValidator>
                        </td>
                        <td align="left" width="8%">
                            <asp:ImageButton ID="imgSearch" runat="server" CausesValidation="false" AlternateText="Search"
                                ImageUrl="../images/search-btn.gif" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td align="left" width="38%">
                            <div class="buttonwrapper">
                                <a id="cmdAddCustomer" runat="server" causesvalidation="false" class="ovalbutton"
                                    href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                        <%=Resources.Resource.cmdCssAddNewCustomer%></span></a></div>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td align="right">
                            <asp:Label ID="lblcompanyID" CssClass="lblBold" Text="<%$ Resources:Resource, lblSoWhs%>"
                                runat="server" />
                        </td>
                        <td width="1%">
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="dlWarehouses" runat="server" Width="225px" CssClass="innerText">
                            </asp:DropDownList>
                            <span class="style1">*</span>
                            <asp:CustomValidator ID="custvalWarehouse" runat="server" SetFocusOnError="true"
                                ErrorMessage="<%$ Resources:Resource, custvalSoWhs%>" ClientValidationFunction="funWarehousesName"
                                Display="None">
                            </asp:CustomValidator>
                        </td>
                        <td>
                        </td>
                        <td>
                            <div style="height: 22px">
                                <asp:UpdateProgress runat="server" ID="Updateprogress" AssociatedUpdatePanelID="upnlSearchCustomer"
                                    DisplayAfter="10">
                                    <ProgressTemplate>
                                        <img src="../Images/wait22trans.gif" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="height: 10px">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <%--Grid view Start for Customer--%>
                    <tr>
                        <td align="center" colspan="5">
                            <asp:GridView ID="grdCustomer" runat="server" AllowSorting="True" DataSourceID="sqldsCustomer"
                                AllowPaging="True" PageSize="10" PagerSettings-Mode="Numeric" CellPadding="0"
                                GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
                                CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="PartnerID"
                                Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, lblCustName %>" HeaderStyle-ForeColor="#ffffff"
                                        HeaderStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblPartnerName" ForeColor="black"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="250px" Wrap="true" HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, lblHistory %>" ReadOnly="True"
                                        SortExpression="TotalOrders">
                                        <ItemStyle Width="350px" Wrap="true" HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="" HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ToolTip="Select" ID="imgSelect" runat="server" CommandArgument='<%# Eval("PartnerID") %>'
                                                CommandName="Edit" CausesValidation="false" ImageUrl="~/images/arrow_24.png" />
                                            <div style="display: none;">
                                                <asp:LinkButton ID="lnkSingleClick" Text="Edit" runat="server" CommandArgument='<%# Eval("PartnerID") %>'
                                                        CommandName="Edit" CausesValidation="false" Style="display: none;" />
                                                <asp:TextBox runat="server" Width="0px" ID="txtPartnerAcronyme" Text='<%# Eval("PartnerAcronyme") %>'>
                                                </asp:TextBox>
                                                <asp:TextBox runat="server" Width="0px" ID="txtPartnerName" Text='<%# Eval("PartnerLongName") %>'>
                                                </asp:TextBox>
                                                <asp:TextBox runat="server" Width="0px" ID="txtTotalOrders" Text='<%# Eval("TotalOrders") %>'>
                                                </asp:TextBox>
                                                <asp:TextBox runat="server" Width="0px" ID="txtTotalInvoices" Text='<%# Eval("TotalInvoices") %>'>
                                                </asp:TextBox>
                                                <asp:TextBox runat="server" Width="0px" ID="txtTotalAmtRcvd" Text='<%# Eval("TotalAmtRcvd") %>'>
                                                </asp:TextBox>
                                                <asp:TextBox runat="server" Width="0px" ID="txtLastAmtRcvd" Text='<%# Eval("LastAmtRcvd") %>'>
                                                </asp:TextBox>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="100px" />
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="" DataField="PartnerLongName" ReadOnly="True">
                                        <ItemStyle Width="0px" Wrap="true" HorizontalAlign="Left" ForeColor="white" Font-Size="0"
                                            VerticalAlign="Top" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle CssClass="grid_footer" />
                                <RowStyle CssClass="grid_rowstyle cursor_point" />
                                <PagerStyle CssClass="grid_footer" />
                                <HeaderStyle CssClass="grid_header" Height="26px" />
                                <AlternatingRowStyle CssClass="grid_alter_rowstyle cursor_point" />
                                <PagerSettings PageButtonCount="20" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="sqldsCustomer" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <%--Grid view End for Customer--%>
                    <tr height="30">
                        <td align="center" colspan="5">
                            <table width="100%">
                                <tr>
                                    <td height="20" align="center" width="30%">
                                    </td>
                                    <td width="70%">
                                        <div class="buttonwrapper">
                                            <a id="cmdSave" visible="false" runat="server" class="ovalbutton" href="#"><span
                                                class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                <%=Resources.Resource.cmdCssNext%></span></a></div>
                                    </td>
                                </tr>
                            </table>
                            <asp:Button runat="server" ID="cmdReset" CssClass="imgReset" CausesValidation="false"
                                Visible="false" />
                            <asp:Button runat="server" ID="cmdSearch" Style="display: none;" CssClass="imgReset"
                                CausesValidation="false" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:ValidationSummary ID="valsProcurement" runat="server" ShowMessageBox="true"
        ShowSummary="false" />
    <script language="javascript" type="text/javascript">

        function funWarehousesName(source, arguments) {
            if (window.document.getElementById('<%=dlWarehouses.ClientID%>').value != "0") {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }

        function funSelectCustomer(source, arguments) {
            if (window.document.getElementById('<%=txtSearch.ClientID%>').value != "") {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }

       
    </script>
</asp:Content>

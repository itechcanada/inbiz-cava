﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

public partial class Sales_mdSalesProcess : BasePage
{
    SysProcessGroup _process = new SysProcessGroup();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdProcess))
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.OpenDatabaseConnection();
                SysTaxCodeDesc tg = new SysTaxCodeDesc();
                tg.FillListControl(dbHelp, dlTaxCode, new ListItem(Resources.Resource.liTaxGroup, "0"));



                SysWarehouses sWhs = new SysWarehouses();
                sWhs.PopulateObject(SalesCartHelper.CurrentCart.OrderWarehouseCode, dbHelp);


                ListItem liTax = dlTaxCode.Items.FindByValue(sWhs.WarehouseRegTaxCode.ToString());

                if (liTax != null)
                {
                    liTax.Selected = true;
                }
                if (this.ProcessID > 0)
                {
                    FillDetails(this.ProcessID);
                }
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    private void FillDetails(int processid)
    {
        if (processid > 0)
        {
            SalesProcessEntity item = SalesCartHelper.GetProcessItem(processid);
            if (item != null)
            {
                txtProcessCode.Text = item.ProcessCode;
                //txtProcessCostPerHour.Text = string.Format("{0:F}", item.ProcessCostPerHour);
                //txtProcessCostPerUnit.Text = string.Format("{0:F}", item.ProcessCostPerUnit);
                //txtProcessDescription.Text = item.ProcessDescription;
                //txtProcessFixedCost.Text = string.Format("{0:F}", item.ProcessFixedCost);
                //txtTotalHour.Text = BusinessUtility.GetString(item.TotalHour);
                //txtTotalUnit.Text = BusinessUtility.GetString(item.TotalUnit);
                //txtProcessInternalCost.Text = string.Format("{0:F}", item.ProcessInternalCost); //Added by mukesh 20130523

                txtProcessCostPerHour.Text = CurrencyFormat.GetAmountInUSCulture(item.ProcessCostPerHour);
                txtProcessCostPerUnit.Text = CurrencyFormat.GetAmountInUSCulture(item.ProcessCostPerUnit);
                txtProcessDescription.Text = item.ProcessDescription;
                txtProcessFixedCost.Text = CurrencyFormat.GetAmountInUSCulture(item.ProcessFixedCost);
                txtTotalHour.Text = BusinessUtility.GetString(item.TotalHour);
                txtTotalUnit.Text = BusinessUtility.GetString(item.TotalUnit);
                txtProcessInternalCost.Text = CurrencyFormat.GetAmountInUSCulture(item.ProcessInternalCost); //Added by mukesh 20130523

                dlTaxCode.SelectedValue = item.TaxGrpID.ToString();

                hdnProcessID.Value = BusinessUtility.GetString(item.ProcessID);
                btnBackToList.Visible = false;
            }
            else
            {
                SysProcessGroup itemProcess = new SysProcessGroup();
                itemProcess.PopulateObject(processid);

                txtProcessCode.Text = itemProcess.ProcessCode;//BusinessUtility.GetString(row["ProcessCode"]);
                txtProcessCostPerHour.Text = CurrencyFormat.GetAmountInUSCulture(itemProcess.ProcessCostPerHour);
                txtProcessCostPerUnit.Text = CurrencyFormat.GetAmountInUSCulture(itemProcess.ProcessCostPerUnit);
                txtProcessDescription.Text = itemProcess.ProcessDescription;
                txtProcessFixedCost.Text = CurrencyFormat.GetAmountInUSCulture(itemProcess.ProcessFixedCost);
                txtTotalHour.Text = "0";
                txtTotalUnit.Text = "0";
                txtProcessInternalCost.Text = CurrencyFormat.GetAmountInUSCulture(0.00); //Added by mukesh 20130523
                hdnProcessID.Value = itemProcess.ProcessID.ToString();
            }

            //Set Visibility of panle
            pnlDetails.Visible = true;
            pnlGrid.Visible = false;
        }
    }

    protected void grdProcess_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 3)
        {
            e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
        }
        else if (e.ColumnIndex == 4)
        {
            e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
        }
        else if (e.ColumnIndex == 5)
        {
            e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
        }
    }

    protected void grdProcess_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        if (Request.QueryString.AllKeys.Contains<string>("_history"))
        {
            sdsProcess.SelectCommand = _process.GetSql(sdsProcess.SelectParameters, Request.QueryString[txtSearch.ClientID]);
        }
        else
        {
            sdsProcess.SelectCommand = _process.GetSql(sdsProcess.SelectParameters, txtSearch.Text);
        }
    }

    protected void grdProcess_RowSelecting(object sender, Trirand.Web.UI.WebControls.JQGridRowSelectEventArgs e)
    {
        int id = BusinessUtility.GetInt(e.RowKey);
        if (id > 0)
        {
            FillDetails(id);
        }
    }
    protected void btnBackToList_Click(object sender, EventArgs e)
    {
        pnlDetails.Visible = false;
        pnlGrid.Visible = true;
        hdnProcessID.Value = "";
    }

    private void SetData(SalesProcessEntity item)
    {
        item.ProcessID = this.ProcessID;
        item.ProcessCode = txtProcessCode.Text;
        item.TaxGrpID = BusinessUtility.GetInt(dlTaxCode.SelectedValue);
        item.TaxGrpName = dlTaxCode.SelectedIndex > 0 ? dlTaxCode.SelectedItem.Text : string.Empty;
        item.ProcessDescription = txtProcessDescription.Text;
        item.ProcessFixedCost = BusinessUtility.GetDouble(txtProcessFixedCost.Text);
        item.ProcessCostPerHour = BusinessUtility.GetDouble(txtProcessCostPerHour.Text);
        item.ProcessCostPerUnit = BusinessUtility.GetDouble(txtProcessCostPerUnit.Text);
        item.TotalHour = BusinessUtility.GetInt(txtTotalHour.Text);
        item.TotalUnit = BusinessUtility.GetInt(txtTotalUnit.Text);
        item.ProcessInternalCost = BusinessUtility.GetDouble(txtProcessInternalCost.Text); //Added by mukesh 20130523
        item.ProcessTotal = this.ExchangeRate * (BusinessUtility.GetDouble(txtProcessFixedCost.Text) + (BusinessUtility.GetDouble(txtProcessCostPerHour.Text) * BusinessUtility.GetDouble(txtTotalHour.Text)) + (BusinessUtility.GetDouble(txtProcessCostPerUnit.Text) * BusinessUtility.GetDouble(txtTotalUnit.Text)));
        item.OrderID = this.OrderID;
    }

    protected void imgAdd_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            SalesProcessEntity item = SalesCartHelper.GetProcessItem(this.ProcessID);
            if (item == null)
            {
                item = new SalesProcessEntity();
            }
            this.SetData(item);
            if (this.ProcessID > 0)
            {
                SalesCartHelper.EditToProcessCart(item);
                Globals.RegisterCloseDialogScript(Page, "parent.reloadProcessGrid(); ", true);
                pnlDetails.Visible = false;
                pnlGrid.Visible = true;
                hdnProcessID.Value = "";
            }
            else
            {
                SalesCartHelper.AddToProcessCart(item);
                //Globals.RegisterScript(Page, "parent.reloadProcessGrid(); ");
                Globals.RegisterCloseDialogScript(Page, "parent.reloadProcessGrid(); ", true);
            }
        }
    }



    private int ProcessID
    {
        get
        {
            int pid = 0;
            int.TryParse(Request.QueryString["processid"], out pid);
            return pid;
        }
    }

    private int OrderID
    {
        get
        {
            int oid = 0;
            int.TryParse(Request.QueryString["orderid"], out oid);
            return oid;
        }
    }

    private double ExchangeRate
    {
        get
        {
            double exrate = 1.0D;
            double.TryParse(Request.QueryString["exrate"], out exrate);
            return exrate;
        }
    }
}
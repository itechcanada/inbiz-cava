﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.Library.DataAccess.MySql;
using iTECH.WebControls;

using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Sales_PrintOrder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if ((this.PrintType == "LAYWAY") && (this.OrderID > 0))
            {
                  PrintContent.Text = CommonPrint.GetPOSLaywayHtmlSnipt(this.OrderID, Globals.CurrentAppLanguageCode);
            }
        }

    }

    private string PrintType
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["type"]).ToUpper();
        }
    }

    private int OrderID
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["ordID"]);
        }
    } 

    protected override void InitializeCulture()
    {
        Globals.SetCultureInfo(Globals.CurrentCultureName);
        base.InitializeCulture();
    }
}
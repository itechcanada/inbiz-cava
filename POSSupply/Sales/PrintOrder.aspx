<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintOrder.aspx.cs" Inherits="Sales_PrintOrder" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<%--    <title>
        <%=prjCompanyTitle%></title>--%>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
    <link href="../Css/style.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin: 0;">
    <form id="form1" runat="server">
    <div align="Left" id="div1">
        <div style="padding-right:5px; text-align:right;"">
            <a href="javascript:;" onclick="window.print();" style="display:none;">Print</a></div>
        <asp:Literal ID="PrintContent" runat="server"></asp:Literal>
    </div>
    <asp:Panel ID="pnlPrintScript" runat="server">
        <script type="text/javascript">
            var PrintCommandObject = null;

            function printPage() {
                if (PrintCommandObject) {
                    try {
                        PrintCommandObject.ExecWB(6, 2);
                        PrintCommandObject.outerHTML = "";
                    }
                    catch (e) {
                        window.print();

                    }
                }
                else {
                    window.print();
                }
                var browserName = navigator.appName;
                if (navigator.appName != "Microsoft Internet Explorer") {

                    window.close();
                }
                else {
                    if (Request.QueryString["cardtype"] == "R") {
                        location.href = "Receipts.aspx";
                    } else {
                        location.href = "POS.aspx";
                    }
                }
            }

            window.onload = function () {

                var vMailID = getParameterByName('printGiftReceipt');
                if (vMailID == "1") {
                    document.getElementById("tblReceipt").style.display = "none";
                }
                Minimize();
                if (navigator.appName == "Microsoft Internet Explorer") {
                    // attach and initialize print command ActiveX object
                    try {
                        var PrintCommand = '<object id="PrintCommandObject" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2" width="0" height="0"></object>';
                        document.body.insertAdjacentHTML('beforeEnd', PrintCommand);
                    }
                    catch (e) { }
                }
                setTimeout(printPage, 1);
            };


            function Minimize() {
                window.innerWidth = 10;
                window.innerHeight = 10;
                window.screenX = screen.width;
                window.screenY = screen.height;
                alwaysLowered = true;
            }




            function getParameterByName(name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
                return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            }
        </script>
    </asp:Panel>
    </form>
</body>
</html>
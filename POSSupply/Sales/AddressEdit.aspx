﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="AddressEdit.aspx.cs" Inherits="Sales_AddressEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <style type="text/css">
        .ul.form li div.lbl{width:90px !important;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
<div style="padding: 5px; height: 250px; overflow: auto;">
    <ul class="form">
        <li>
            <div class="lbl">
                <asp:Label Text="<%$Resources:Resource, lblAddressLine1 %>"  ID="lblAddressLine1" runat="server" AssociatedControlID="txtAddressLine1"></asp:Label>*
            </div>
            <div class="input">
                <asp:TextBox ID="txtAddressLine1" runat="server" />
                <asp:RequiredFieldValidator ID="rfvAddressLine1" ErrorMessage="<%$ Resources:Resource, reqvalRegCode%>"
                    runat="server" ControlToValidate="txtAddressLine1" Text="*" SetFocusOnError="True"> </asp:RequiredFieldValidator>
            </div>
            <div class="clearBoth">
            </div>
        </li>
         <li>
            <div class="lbl">
                <asp:Label Text="<%$Resources:Resource, lblAddressLine2 %>"  ID="Label1" runat="server" AssociatedControlID="txtAddressLine1"></asp:Label>*
            </div>
            <div class="input">
               <asp:TextBox ID="txtAddressLine2" runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label Text="<%$Resources:Resource, lblAddressLine3 %>"  ID="Label2" runat="server" AssociatedControlID="txtAddressLine1"></asp:Label>*
            </div>
            <div class="input">
               <asp:TextBox ID="txtAddressLine3" runat="server" />
            </div>
            <div class="clearBoth">
            </div>
        </li>
         <li>
            <div class="lbl">
                <asp:Label Text="<%$Resources:Resource, lblCountry %>"  ID="Label3" runat="server" AssociatedControlID="txtAddressLine1"></asp:Label>*
            </div>
            <div class="input">
                <asp:TextBox ID="txtCountry" runat="server" />
            <asp:RequiredFieldValidator ID="rfvCountry" ErrorMessage="<%$ Resources:Resource, rfvCountry%>" runat="server" ControlToValidate="txtCountry"
               Text="*" SetFocusOnError="True">
            </asp:RequiredFieldValidator>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label Text="<%$Resources:Resource, lblStateOrProvince %>"  ID="Label4" runat="server" AssociatedControlID="txtAddressLine1"></asp:Label>*
            </div>
            <div class="input">
                <asp:TextBox ID="txtState" runat="server" />
            <asp:RequiredFieldValidator ID="rfvState" ErrorMessage="<%$ Resources:Resource, rfvState%>" runat="server" ControlToValidate="txtState"
                Display="None" SetFocusOnError="True">
            </asp:RequiredFieldValidator>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label Text="<%$Resources:Resource, lblCity %>"  ID="Label5" runat="server" AssociatedControlID="txtAddressLine1"></asp:Label>*
            </div>
            <div class="input">
                 <asp:TextBox ID="txtCity" runat="server" />
            <asp:RequiredFieldValidator ID="rfvCity" ErrorMessage="<%$ Resources:Resource, rfvCity%>" runat="server" ControlToValidate="txtCity"
                Display="None" SetFocusOnError="True">
            </asp:RequiredFieldValidator>
            </div>
            <div class="clearBoth">
            </div>
        </li>
        <li>
            <div class="lbl">
                <asp:Label Text="<%$Resources:Resource, lblPostalCode %>"  ID="Label6" runat="server" AssociatedControlID="txtAddressLine1"></asp:Label>*
            </div>
            <div class="input">
                <asp:TextBox ID="txtPostalCode" runat="server" />
            <asp:RequiredFieldValidator ID="rfvPostalCode" ErrorMessage="<%$ Resources:Resource, rfvPostalCode%>" runat="server" ControlToValidate="txtPostalCode"
                Display="None" SetFocusOnError="True">
            </asp:RequiredFieldValidator>
            </div>
            <div class="clearBoth">
            </div>
        </li>
    </ul>
    <asp:TextBox ID="txtData" TextMode="MultiLine" runat="server" style="display:none;"  />
</div>
<div class="div_command">
    <asp:Button ID="btnSave" Text="<%$Resources:Resource, btnSave%>" runat="server" 
        onclick="btnSave_Click" />
    <asp:Button ID="btnCancel" Text="<%$Resources:Resource, btnCancel%>" runat="server" CausesValidation="false" OnClientClick="jQuery.FrameDialog.closeDialog();" />
</div>
<asp:HiddenField ID="hdnAddressID" runat="server" />
<asp:HiddenField ID="hdnAddressRef" runat="server" />
<asp:HiddenField ID="hdnAddressType" runat="server" />
<asp:HiddenField ID="hdnAddressSourceID" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


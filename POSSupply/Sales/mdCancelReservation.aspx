﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdCancelReservation.aspx.cs" Inherits="Sales_mdCancelReservation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <style type="text/css">
        ul.form li div.lbl{width:150px !important;}        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" runat="Server">
    <div style="padding: 5px 10px;">
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="lblGuest" Text="<%$Resources:Resource, lblGuestName%>" runat="server" />
                </div>
                <div class="input">
                    <asp:Label ID="valGuest" Text="" runat="server" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
             <li>
                <div class="lbl">
                    <asp:Label ID="Label1" Text="<%$Resources:Resource, lblTotalPaidAmount%>" runat="server" />
                </div>
                <div class="input">
                    <asp:Label ID="valTotalPaid" Text="" runat="server" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
             <li id="liRefundAmt" runat="server">
                <div class="lbl">
                    <asp:Label ID="Label5" Text="<%$Resources:Resource, lblRefundAmount%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtRefundAmount" runat="server" MaxLength="8" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
            <li >
                <div class="lbl">
                    <asp:Label ID="Label2" Text="<%$Resources:Resource, lblAdminFee%>" runat="server" />
                </div>
                <div class="input">
                    <asp:TextBox ID="txtAdminFee" runat="server" MaxLength="8" />
                </div>
                <div class="clearBoth">
                </div>
            </li>
        </ul>
        <div class="div-dialog-command" style="border-top:1px solid #ccc; padding:5px;">
            <asp:Button ID="btnCancel" Text="<%$Resources:Resource, lblCancelReservation%>" runat="server" 
                onclick="btnCancel_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="Server">
    <script type="text/javascript">
        $("#<%=btnCancel.ClientID%>").click(function () {
            parent._isRequireReload = true;
        });
     </script>
</asp:Content>


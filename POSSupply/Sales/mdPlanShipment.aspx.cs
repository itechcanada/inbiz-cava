﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Sales_mdPlanShipment : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(JQGridShipment))
        {

        }

        if (SalesCartHelper.CurrentCart.Items.Count > 0)
        {
            mdbtnValidateSKU.Url = string.Format("~/shipping/validateSKU.aspx?oid={0}&validationfor={1}", BusinessUtility.GetString(SalesCartHelper.CurrentCart.Items[0].OrderID), "planshippment");
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgSOPlzAddProductInQuotationList);
            Globals.RegisterReloadParentScript(this);
        }

        if (!IsPostBack)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                List<SalesCartEntity> lstCurrentCart = Session["SHIPMENT_CART"] as List<SalesCartEntity>;
                if (SalesCartHelper.CurrentCart.Items.Count > 0)
                {
                    Orders _ord = new Orders();
                    _ord.PopulateObject(dbHelp, SalesCartHelper.CurrentCart.Items[0].OrderID);
                    if (_ord.OrderTypeCommission == (int)OrderCommission.Transfer)
                    {
                        mdbtnValidateSKU.Title = Resources.Resource.lblTransferNewValidateSKU;
                    }
                    else
                    {
                        mdbtnValidateSKU.Title = Resources.Resource.lblOrderNewValidateSKU;
                    }
                }

            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError + ex.Message);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }



        }
    }
    private int OrderID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["orderid"], out id);
            return id;
        }
    }

    private bool IsShipNow
    {
        get
        {
            if (BusinessUtility.GetString(Request.QueryString["shipnow"]) == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    private int InvoiceID
    {
        get
        {
            int id = 0;
            int.TryParse(Request.QueryString["invid"], out id);
            return id;
        }
    }
    protected void JQGridShipment_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {
        if (e.ColumnIndex == 11)//11
        {
            e.CellHtml = string.Format("<input id='shpQty_{0}' type='text' value='{1}' style='width:70px;'/>", e.RowIndex, e.CellHtml);
        }
        else if (e.ColumnIndex == 12)//12
        {
            e.CellHtml = string.Format(@"<a class=""inbiz_icon icon_save_24x24"" href=""javascript:;"" onclick=""saveShipmentDataInCart('shpQty_{0}', '{1}')"" >Save</a>", e.RowIndex, e.RowKey);
        }
    }

    protected void JQGridShipment_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {

        if (Request.QueryString.AllKeys.Contains("editRowKey"))
        {
            var lstFromSession = Session["SHIPMENT_CART"] as List<SalesCartEntity>;
            int id = 0;
            int qtyToUpdate = 0;
            if (lstFromSession != null && int.TryParse(Request.QueryString["editRowKey"], out id) && id > 0)
            {
                int.TryParse(Request.QueryString["qtyToShip"], out qtyToUpdate);
                var itemtoUpdate = lstFromSession.Where(i => i.ID == id).FirstOrDefault();
                if (itemtoUpdate != null)
                {
                    //if (itemtoUpdate.Quantity >= itemtoUpdate.PreviousReturnedQty - qtyToUpdate)
                    if (itemtoUpdate.PreviousReturnedQty >= qtyToUpdate)
                    {
                        itemtoUpdate.ToShipQty = qtyToUpdate;
                        Session["SHIPMENT_CART"] = lstFromSession;
                    }
                    else
                    {
                        MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblOverShipQtyMsg);
                    }
                }
            }
        }
        JQGridShipment.DataSource = Session["SHIPMENT_CART"] != null ? Session["SHIPMENT_CART"] : SalesCartHelper.CurrentCart.Items;
    }

    protected void btnShipment_Click(object sender, EventArgs e)
    {
        List<SalesCartEntity> lstCurrentCart = Session["SHIPMENT_CART"] as List<SalesCartEntity>;
        var itemtoUpdate = lstCurrentCart.Where(i => i.ToShipQty > 0).FirstOrDefault();
        if (itemtoUpdate != null)
        {
            //btnShipment.Visible = true;
        }
        else
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.errShpQtyZero);
            return;
        }



        DbHelper dbHelp = new DbHelper(true);
        try
        {
            this.InsertOrder(dbHelp);

        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError + ex.Message);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }


    private void InsertOrder(DbHelper dbHelp)
    {
        try
        {

            List<SalesCartEntity> lstCurrentCart = Session["SHIPMENT_CART"] as List<SalesCartEntity>;
            List<SalesCartEntity> itemtoUpdate = lstCurrentCart.Where(i => i.Quantity != i.ToShipQty).ToList();



            if (SalesCartHelper.CurrentCart.Items.Count > 0)
            {
                if (itemtoUpdate.Count > 0)
                {
                    Partners _part = new Partners();
                    _part.PopulateObject(dbHelp, SalesCartHelper.CurrentCart.Items[0].GuestID);
                    int partTypeID = _part.GetPartnerTypeID(dbHelp, _part.PartnerID);
                    SysCurrencies cur = new SysCurrencies();
                    cur.PopulateObject(_part.PartnerCurrencyCode);
                    Orders _ord = new Orders();
                    _ord.PopulateObject(dbHelp, SalesCartHelper.CurrentCart.Items[0].OrderID);
                    Orders ord = new Orders();
                    ord.InvDate = DateTime.Now;
                    ord.InvRefNo = 0;
                    ord.OrdComment = string.Empty;
                    ord.OrdCompanyID = CurrentUser.DefaultCompanyID;
                    ord.OrdCreatedBy = CurrentUser.UserID;
                    ord.OrdCreatedFromIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    ord.OrdCreatedOn = DateTime.Now;
                    ord.OrdCurrencyCode = _part.PartnerCurrencyCode;
                    ord.OrdCurrencyExRate = cur.CurrencyRelativePrice;
                    ord.OrdCustID = _part.PartnerID;
                    ord.OrdCustType = Globals.GetPartnerType(partTypeID);
                    ord.OrdDiscount = SalesCartHelper.CurrentCart.OrdDiscount;//0.00D;
                    ord.OrdDiscountType = SalesCartHelper.CurrentCart.OrdDiscountType;//"P";
                    ord.OrderRejectReason = string.Empty;
                    if (_ord.OrderTypeCommission == (int)OrderCommission.Transfer)
                    {
                        ord.OrderTypeCommission = (int)OrderCommission.Transfer;
                    }
                    else
                    {
                        ord.OrderTypeCommission = (int)OrderCommission.Shipment;
                    }

                    ord.OrdLastUpdateBy = CurrentUser.UserID;
                    ord.OrdLastUpdatedOn = DateTime.Now;
                    ord.OrdNetTerms = _ord.OrdNetTerms;
                    ord.OrdSalesRepID = _ord.OrdSalesRepID;
                    ord.OrdSaleWeb = _ord.OrdSaleWeb ? true : false;
                    ord.OrdShippingTerms = _ord.OrdShippingTerms;
                    ord.OrdShpBlankPref = false;
                    ord.OrdShpCode = string.Empty;
                    ord.OrdShpCost = 0.00D;
                    ord.OrdShpDate = DateTime.Now;
                    ord.OrdShpTrackNo = string.Empty;
                    ord.OrdShpWhsCode = _ord.OrdShpWhsCode;
                    ord.OrdShpToWhsCode = _ord.OrdShpToWhsCode;
                    if (this.IsShipNow)
                    {
                        ord.OrdStatus = SOStatus.READY_FOR_SHIPPING;
                    }
                    else
                    {
                        ord.OrdStatus = SOStatus.NEW;
                    }
                    ord.OrdType = SOType.QUOTATION;
                    ord.OrdVerified = true;
                    ord.OrdVerifiedBy = CurrentUser.UserID;
                    ord.QutExpDate = DateTime.MinValue;
                    ord.OrdCustPO = string.Empty;
                    ord.OrdRegCode = _ord.OrdRegCode;
                    ord.ParentOrdID = _ord.OrdID;
                    ////Save Order

                    if (ord.Insert(dbHelp, CurrentUser.UserID))
                    {
                        if (ord.OrdID > 0)
                        {

                            Addresses _billToAddress = new Addresses();
                            Addresses _shipToAddress = new Addresses();
                            // Save Order Shippment and Billing Address
                            _billToAddress.GetOrderAddress(_ord.OrdID, AddressType.BILL_TO_ADDRESS);

                            ord.SaveOrderAddresse(ord.OrdID, _billToAddress.AddressLine1, _billToAddress.AddressLine2, _billToAddress.AddressLine3, _billToAddress.AddressCity, _billToAddress.AddressState, _billToAddress.AddressCountry, _billToAddress.AddressPostalCode, AddressType.BILL_TO_ADDRESS);
                            ////Addresses shipToAddress = new Addresses();

                            _shipToAddress.GetOrderAddress(_ord.OrdID, AddressType.SHIP_TO_ADDRESS);
                            ord.SaveOrderAddresse(ord.OrdID, _shipToAddress.AddressLine1, _shipToAddress.AddressLine2, _shipToAddress.AddressLine3, _shipToAddress.AddressCity, _shipToAddress.AddressState, _shipToAddress.AddressCountry, _shipToAddress.AddressPostalCode, AddressType.SHIP_TO_ADDRESS);

                            List<OrderItems> ordItems = new List<OrderItems>();
                            List<SalesCartEntity> CurrentCart = Session["SHIPMENT_CART"] as List<SalesCartEntity>;
                            foreach (var dr in CurrentCart)
                            {
                                if (dr.ToShipQty > 0)
                                {
                                    OrderItems oi = new OrderItems();
                                    oi.OrderItemDesc = dr.ProductName;
                                    oi.OrdID = ord.OrdID;
                                    oi.OrdProductDiscount = dr.Discount;
                                    oi.OrdProductDiscountType = dr.DisType;
                                    oi.OrdProductID = dr.ProductID;
                                    oi.OrdProductQty = dr.ToShipQty;
                                    oi.OrdProductTaxGrp = dr.TaxGrp;
                                    oi.OrdProductUnitPrice = dr.Price;
                                    oi.OrdGuestID = dr.GuestID;

                                    ordItems.Add(oi);
                                }
                            }

                            //Add Order Items to OrderItems Table
                            OrderItems ordItem = new OrderItems();
                            ordItem.AddOrderItems(dbHelp, ordItems);

                            // Retrive Customers Sale Rep
                            List<int> lstRep = _part.GetCustomerSalesRep(_part.PartnerID);
                            if (lstRep.Count > 0)
                            {
                                ord.SetOrderSalesRepUsers(dbHelp, ord.OrdID, lstRep.ToArray());
                            }
                            else
                            {
                                //Add Order Sales Rep users by default to creator
                                ord.SetOrderSalesRepUsers(dbHelp, ord.OrdID, new int[] { CurrentUser.UserID });
                            }

                            //To Do Here for Generate Alert
                            //Get List of users who ahs QOA or ADM module & generate alert for those
                            if (ord.OrdStatus == SOStatus.NEW)
                            {
                                AlertHelper.SetAlertQuotationToApprove(dbHelp, ord.OrdID, CurrentUser.UserID);
                            }


                            #region SetParentOrderCloseIncaseCreateShippmentofTransferOrder
                            if (_ord.OrderTypeCommission == (int)OrderCommission.Transfer)
                            {
                                Orders _ordParent = new Orders();
                                _ordParent.PopulateObject(dbHelp, SalesCartHelper.CurrentCart.Items[0].OrderID);
                                _ordParent.OrdStatus = SOStatus.CLOSED;
                                _ordParent.OrdClosedReason = OrderClosedReason.Voided;
                                _ordParent.Update(dbHelp, CurrentUser.UserID);
                            }
                            #endregion

                            //To do add into log
                            new OrderActionLog().AddLog(dbHelp, CurrentUser.UserID, ord.OrdID, "Order created", ord.OrdComment);
                            Session.Remove("SHIPMENT_CART");
                            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgShipmentCreatedSuccessfully);
                            if (this.IsShipNow)
                            {
                                Globals.RegisterCloseDialogScript(this.Page, string.Format("parent.LoadShipmentPopup('{0}');", BusinessUtility.GetString(ord.OrdID)), true);
                            }
                            else
                            {
                                Globals.RegisterCloseDialogScript(this.Page, string.Format("parent.setEmptyText();"), true);
                            }
                            //Globals.RegisterReloadParentScript(this);
                        }
                    }
                }
                else
                {
                    Orders _ord = new Orders();
                    _ord.PopulateObject(dbHelp, SalesCartHelper.CurrentCart.Items[0].OrderID);
                    _ord.OrdStatus = SOStatus.READY_FOR_SHIPPING;
                    _ord.Update(dbHelp, CurrentUser.UserID);
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgShipmentCreatedSuccessfully);
                    if (this.IsShipNow)
                    {
                        Globals.RegisterCloseDialogScript(this.Page, string.Format("parent.LoadShipmentPopup('{0}');", BusinessUtility.GetString(_ord.OrdID)), true);
                    }
                    else
                    {
                        Globals.RegisterCloseDialogScript(this.Page, string.Format("parent.setEmptyText();"), true);
                    }
                    Globals.RegisterReloadParentScript(this);

                }
            }


        }
        catch (Exception ex)
        {
            //throw;
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
    }


    protected void Page_Init(object sender, EventArgs e)
    {
        Session["validateorder"] = null;
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdAddDiscountToOrder.aspx.cs" Inherits="Sales_mdAddDiscountToOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div class="padding:5px;">
        <ul class="form">
            <li>
                <div class="lbl">
                    <asp:Label ID="Label1" Text="<%$Resources:Resource,lblActualOrderTotal %>" runat="server" />
                </div>
                <div class="input">
                    <asp:Label ID="valOrderTotal" runat="server" />
                </div>
                <div class="clearBoth"></div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="Label2" Text="<%$Resources:Resource,lblEffectedOrderTotal %>" runat="server" />                    
                </div>
                <div class="input">
                    <asp:Label ID="valEffectedOrderTotal" runat="server" />
                </div>
                <div class="clearBoth"></div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="Label4" Text="<%$Resources:Resource,lblItemTotal %>" runat="server" /> <br />
                    (<asp:Label ID="Label5" Text="<%$Resources:Resource,lblDiscountAppliedOnIt %>" runat="server" />)                   
                </div>
                <div class="input">
                    <asp:Label ID="valItemTotal" runat="server" />
                </div>
                <div class="clearBoth"></div>
            </li>
            <li>
                <div class="lbl">
                    <asp:Label ID="Label3" Text="<%$Resources:Resource,lblNewDiscountToApply %>" runat="server" />                     
                </div>
                <div class="input" style="width:60px;">
                    <asp:TextBox ID="txtDiscount" runat="server" MaxLength="10" Width="50px" />                    
                </div>
                <div class="input" style="width:190px;">
                    <asp:RadioButtonList ID="rblstDiscountType" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="P" Text="<%$ Resources:Resource, lblPrdBlanketDisPercentage %>"
                            Selected="True" />
                        <asp:ListItem Value="A" Text="<%$ Resources:Resource, lblPrdBlanketDisAbsolute %>" />
                    </asp:RadioButtonList>
                </div>
                <div class="clearBoth"></div>
            </li>
        </ul>
    </div>
    <div class="div-dialog-command" style="border-top:1px solid #ccc; padding:5px;">
        <asp:Button ID="btnApply" Text="<%$Resources:Resource, lblApply %>" runat="server" 
            onclick="btnApply_Click" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" Runat="Server">
</asp:Content>


    
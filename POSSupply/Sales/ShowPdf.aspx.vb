Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports System.Configuration.ConfigurationManager
''
Imports System
Imports System.IO
Imports System.Data.Odbc
Imports System.Web.HttpContext
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTECH.InbizERP.BusinessLogic
Imports iTECH.Library.Utilities

Partial Class SO_Show
    Inherits BasePage

    Private _contentControl As Control

    'On Page Load
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            subClose()
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("SOID") <> "" Then
                If Request.QueryString("DocTyp") = "QO" Or Request.QueryString("DocTyp") = "SO" Or Request.QueryString("DocTyp") = "IN" Then
                    Dim htmlSnnipt As String = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
                    htmlSnnipt += "<html xmlns=""http://www.w3.org/1999/xhtml""><head><title>Print</title></head><body>#DATA_TO_WRITE#</body></html>"

                    'Initialize Print Data
                    '_contentControl = Page.LoadControl("~/PrintTemplates/OrderTemplate.ascx")
                    'phData.Controls.Clear()
                    'phData.Controls.Add(_contentControl)
                    '_contentControl.ID = "ctlContent"
                    'Dim ctrl As IPrint = DirectCast(_contentControl, IPrint)
                    'ctrl.DocType = Me.GetDocType()
                    'ctrl.RequestID = BusinessUtility.GetInt(Request.QueryString("SOID"))
                    'ctrl.InitDetails()

                    'Dim sb As New System.Text.StringBuilder()
                    'Using sw As New StringWriter(sb)
                    '    Using textWriter As New HtmlTextWriter(sw)
                    '        _contentControl.RenderControl(textWriter)
                    '    End Using
                    'End Using

                    'Dim strContent As String = htmlSnnipt.Replace("#DATA_TO_WRITE#", sb.ToString())
                    Dim reqID As Integer = 0
                    Integer.TryParse(Request.QueryString("SOID"), reqID)
                    Dim strDocTyp As String = Me.GetDocType()
                    Dim htmlToMail As String = String.Empty
                    Select Case strDocTyp.ToUpper().Trim()
                        Case "QO"
                            htmlToMail = CommonPrint.GetOrderPrintHtmlSnipt(Me, "QO", reqID)
                            Exit Select
                        Case "SO"
                            htmlToMail = CommonPrint.GetOrderPrintHtmlSnipt(Me, "SO", reqID)
                            Exit Select
                        Case "IN"
                            htmlToMail = CommonPrint.GetInvoicePrintHtmlSnipt(Me, reqID)
                            Exit Select
                        Case "PO"

                            Exit Select
                    End Select

                    Dim strContent As String = htmlSnnipt.Replace("#DATA_TO_WRITE#", htmlToMail)

                    'Dim objPrn As New clsPrintHTML(Request.QueryString("SOID"))
                    'strContent = objPrn.PrintRequestDetail(Request.QueryString("SOID"), Request.QueryString("DocTyp"))

                    Dim strFileName As String = Request.QueryString("DocTyp") + "_" + Request.QueryString("SOID") + "_" + Now.ToString("yyyyMMddhhmmss") + ".html"
                    Dim strFullName As String = Server.MapPath("~") + "/pdf/" + strFileName
                    If (Not File.Exists(strFullName)) Then
                        File.WriteAllText(strFullName, strContent)

                        Dim objDoc As New clsDocuments
                        objDoc.SysDocType = "PO"
                        objDoc.SysDocDistType = "P"
                        objDoc.SysDocDistDateTime = Now.ToString("yyyy-MM-dd hh:mm:ss")
                        objDoc.SysDocPath = AppSettings("PDFPath") + "" + strFileName
                        objDoc.SysDocEmailTo = ""
                        objDoc.SysDocFaxToPhone = ""
                        objDoc.SysDocFaxToAttention = ""
                        objDoc.SysDocBody = ""
                        objDoc.SysDocSubject = ""
                        objDoc.insertDocuments()
                        objDoc = Nothing
                    End If
                    Response.Redirect("../Common/Print.aspx?ReqID=" + Request.QueryString("SOID") + "&DocTyp=" + Request.QueryString("DocTyp"))
                End If
            End If
        End If
    End Sub

    Private Function GetDocType() As String
        Return BusinessUtility.GetString(Request.QueryString("DocTyp"))
    End Function

    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    ' Sub routine to close popup
    Private Sub subClose()
        Response.Write("<script>window.opener.location.reload();</script>")
        Response.Write("<script>window.close();</script>")
    End Sub
    'Purchase Order
    Private Function funSOGen(ByVal strSOID As String) As String
        Return ""
    End Function
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)
        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3 + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + Microsoft.VisualBasic.Chr(10)
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + Microsoft.VisualBasic.Chr(10)
            Else
                strAddress += objAdr.AddressState + Microsoft.VisualBasic.Chr(10)
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + Microsoft.VisualBasic.Chr(10)
            Else
                strAddress += objAdr.AddressCountry + Microsoft.VisualBasic.Chr(10)
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function

    'Get Warehouse Address
    Protected Function funWarehouseAddress(ByVal strWhsCode As String)
        Dim strData As String = ""
        If strWhsCode <> "" Then
            Dim objWhs As New clsWarehouses
            objWhs.WarehouseCode = strWhsCode
            objWhs.getWarehouseInfo()
            If objWhs.WarehouseDescription <> "" Then
                strData += objWhs.WarehouseDescription + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseAddressLine1 <> "" Then
                strData += objWhs.WarehouseAddressLine1 + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseAddressLine2 <> "" Then
                strData += objWhs.WarehouseAddressLine2 + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseCity <> "" Then
                strData += objWhs.WarehouseCity + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseState <> "" Then
                strData += objWhs.WarehouseState + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehouseCountry <> "" Then
                strData += objWhs.WarehouseCountry + Microsoft.VisualBasic.Chr(10)
            End If
            If objWhs.WarehousePostalCode <> "" Then
                strData += objWhs.WarehousePostalCode
            End If
            objWhs = Nothing
        End If
        Return strData
    End Function
    
End Class

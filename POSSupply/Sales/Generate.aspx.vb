Imports Resources.Resource
Imports System.Threading
Imports System.Globalization
Imports iTECH.InbizERP.BusinessLogic
Imports iTECH.Library.Utilities

Partial Class Sales_Generate
    Inherits BasePage
    'On Page Load
    Dim objCompany As New clsCompanyInfo
    Dim objCust As New clsExtUser
    Dim _dueAmountForOnlyCust As Double
 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginID") = "" Or Session("UserModules") = "" Then
            Response.Redirect("~/AdminLogin.aspx")
        End If
        If Session("Msg") <> "" Then
            lblMsg.Text = Session("Msg").ToString
            Session.Remove("Msg")
        End If

        If Not IsPostBack Then
            lblTitle.Text = lblSOCreateQuotation '"Create Quotation"
            subPopulateWarehouse()
            If (Request.QueryString("cID") <> "") Then
                Dim strCust = Request.QueryString("cID")
                hdnCustID.Value = Request.QueryString("cID")
                txtSearch.Text = Request.QueryString("cName")
                cmdSave.Visible = True
            Else
                cmdSave.Visible = False
            End If
        End If
        txtSearch.Focus()
    End Sub
    Private Sub subPopulateWarehouse()
        Dim objWhs As New clsWarehouses
        objWhs.PopulateWarehouse(dlWarehouses)
        dlWarehouses.Items.RemoveAt(0)
        Dim itm As New ListItem
        itm.Value = 0
        itm.Text = liWarehouseLocation
        dlWarehouses.Items.Insert(0, itm)
        objWhs = Nothing
    End Sub
    'Initialize Culture
    Protected Overrides Sub InitializeCulture()
        If Session("Language") = "" Or Session("Language") Is Nothing Then
            Session("Language") = "en-CA"
        End If
        If Not Session("Language") = "en-CA" Then
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(Session("Language").ToString)
        End If
    End Sub
    'On Save
    Protected Sub cmdSave_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.ServerClick
        Dim strCompantID As String = objCompany.funGetCompanyID(dlWarehouses.SelectedValue)
        Response.Redirect("Create.aspx?custID=" & hdnCustID.Value & "&custName=" & txtSearch.Text.Trim & "&ComID=" & strCompantID & "&whsID=" & dlWarehouses.SelectedValue)
    End Sub
    'Add New Customer
    Protected Sub cmdAddCustomer_serverClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddCustomer.ServerClick
        Response.Redirect("~/partner/AddEditCustomer.aspx?adCst=Y")
    End Sub
    'Search customer 
    Protected Sub subCustomerSearch(ByVal SrchTxt As String)
        sqldsCustomer.SelectCommand = objCust.funfillGridCustomerDetail(SrchTxt)
    End Sub
    'Customer grid page index change event
    'image search button click event
    Protected Sub imgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
        lblMsg.Text = ""
        subCustomerSearch(txtSearch.Text)
    End Sub
    Protected Sub grdCustomer_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdCustomer.PageIndexChanged
        lblMsg.Text = ""
        subCustomerSearch(txtSearch.Text)
    End Sub
    'Data Bound
    Protected Sub grdCustomer_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdCustomer.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            
            Dim txtTotalOrders As TextBox = CType(e.Row.FindControl("txtTotalOrders"), TextBox)
            Dim txtTotalInvoices As TextBox = CType(e.Row.FindControl("txtTotalInvoices"), TextBox)
            Dim txtTotalAmtRcvd As TextBox = CType(e.Row.FindControl("txtTotalAmtRcvd"), TextBox)
            Dim txtLastAmtRcvd As TextBox = CType(e.Row.FindControl("txtLastAmtRcvd"), TextBox)
            Dim strHistory As New StringBuilder
            strHistory.Append("" & msgTtlNoSO & " " & txtTotalOrders.Text)
            strHistory.Append("<br> " & msgTtlNoInv & " " & txtTotalInvoices.Text)
            Dim strAmtRcvd As New StringBuilder
            If (txtTotalAmtRcvd.Text <> "") Then
                strAmtRcvd.Append(String.Format("{0:F}", CDbl(txtTotalAmtRcvd.Text)))
            End If
            strHistory.Append("<br> " & totalAmtRcvd & " " & strAmtRcvd.ToString)
            Dim strLastAmtRcvdOn As New StringBuilder
            If (txtLastAmtRcvd.Text <> "") Then
                strLastAmtRcvdOn.Append(txtLastAmtRcvd.Text)
            End If
            strHistory.Append("<br>" & LastAmtRcvdOn & " " & strLastAmtRcvdOn.ToString)

            Dim balAmount As Double
            Dim custID As Integer = BusinessUtility.GetInt(DataBinder.Eval(e.Row.DataItem, "PartnerID"))
            balAmount = ProcessInvoice.GetCustomerUnpaidAmount(custID)


            strHistory.AppendFormat("<br> Unpaid Amount:{0:F}", balAmount)
            _dueAmountForOnlyCust = balAmount
            txtTotalOrders.Text = strHistory.ToString
            e.Row.Cells(1).Text = strHistory.ToString

           

            Dim txtPartnerName As TextBox = CType(e.Row.FindControl("txtPartnerName"), TextBox)
            Dim txtPartnerAcronyme As TextBox = CType(e.Row.FindControl("txtPartnerAcronyme"), TextBox)
            Dim lblPartnerName As Label = CType(e.Row.FindControl("lblPartnerName"), Label)
            lblPartnerName.Text = txtPartnerAcronyme.Text & "<BR/>" & txtPartnerName.Text & "<BR/>" & funGetCustomerAddress(grdCustomer.DataKeys(e.Row.RowIndex).Value.ToString)
            lblPartnerName.Text = lblPartnerName.Text.Replace(Environment.NewLine, "<br />")

            If balAmount > 0 Then
                lblPartnerName.ForeColor = Drawing.Color.Red
            End If
            strHistory = Nothing
            strAmtRcvd = Nothing
            strLastAmtRcvdOn = Nothing
            'e.Row.Cells(3).Text = ""

            'Bind RowClick Event 
            Dim lnkSingleClick As LinkButton = CType(e.Row.FindControl("lnkSingleClick"), LinkButton)
            Dim jsSingle As String = ClientScript.GetPostBackClientHyperlink(lnkSingleClick, "")
            e.Row.Attributes("onclick") = jsSingle
        End If
    End Sub

    'In order to handle event validation for grid row click
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        For Each r As GridViewRow In grdCustomer.Rows
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(r.UniqueID + "$ctl00")
            End If
        Next

        MyBase.Render(writer)
    End Sub

    'Click on Select Button in grid
    Protected Sub grdCustomer_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdCustomer.RowEditing
        hdnCustID.Value = grdCustomer.DataKeys(e.NewEditIndex).Value.ToString
        txtSearch.Text = grdCustomer.Rows(e.NewEditIndex).Cells(3).Text
        cmdSave.Visible = True
        If dlWarehouses.SelectedIndex > 0 Then
            Dim strCompantID As String = objCompany.funGetCompanyID(dlWarehouses.SelectedValue)
            Response.Redirect("Create.aspx?custID=" & hdnCustID.Value & "&custName=" & txtSearch.Text.Trim & "&ComID=" & strCompantID & "&whsID=" & dlWarehouses.SelectedValue)
        End If        
    End Sub
    'Soring Customer grid 
    Protected Sub grdCustomer_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdCustomer.Sorting
        lblMsg.Text = ""
        subCustomerSearch(txtSearch.Text)
    End Sub
    ' Sql Data Source Event Track
    Protected Sub sqldsCustomer_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sqldsCustomer.Selected
        If e.AffectedRows = 0 And lblMsg.Text = "" Then
            lblMsg.Text = NoDataFound
            cmdSave.Visible = False
        End If
    End Sub
    Private Function funGetCustomerAddress(ByVal strCustID As String) As String
        Dim strAddress As String
        Dim objCust As New clsExtUser
        objCust.CustID = strCustID
        objCust.getCustomerInfo()
        If objCust.CustType <> "" Then
            strAddress = funPopulateAddress(objCust.CustType, "B", objCust.CustID).Replace("<br>", vbCrLf)
        End If
        objCust = Nothing
        Return strAddress
    End Function
    'Populate Address
    Private Function funPopulateAddress(ByVal sRef As String, ByVal sType As String, ByVal sSource As String) As String
        Dim strAddress As String = ""
        Dim objAdr As New clsAddress
        objAdr.getAddressInfo(sRef, sType, sSource)

        If objAdr.AddressLine1 <> "" Then
            strAddress += objAdr.AddressLine1 + ","
        End If
        If objAdr.AddressLine2 <> "" Then
            strAddress += objAdr.AddressLine2 + ","
        End If
        If objAdr.AddressLine3 <> "" Then
            strAddress += objAdr.AddressLine3
        End If
        strAddress += "<br>"
        If objAdr.AddressCity <> "" Then
            strAddress += objAdr.AddressCity + ","
        End If
        If objAdr.AddressState <> "" Then
            If objAdr.getStateName <> "" Then
                strAddress += objAdr.getStateName + "<br />"
            Else
                strAddress += objAdr.AddressState + "<br />"
            End If
        End If
        If objAdr.AddressCountry <> "" Then
            If objAdr.getCountryName <> "" Then
                strAddress += objAdr.getCountryName + "<br />"
            Else
                strAddress += objAdr.AddressCountry + "<br />"
            End If
        End If
        If objAdr.AddressPostalCode <> "" Then
            strAddress += "Postal Code: " + objAdr.AddressPostalCode
        End If
        objAdr = Nothing
        Return strAddress
    End Function

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        lblMsg.Text = ""
        subCustomerSearch(txtSearch.Text)

    End Sub

    Protected Sub grdCustomer_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdCustomer.DataBound
        If (grdCustomer.Rows.Count = 1 AndAlso _dueAmountForOnlyCust = 0) Then
            'ClientScript.RegisterStartupScript(Me.GetType(), "GridRowEdit", ClientScript.GetPostBackEventReference(grdCustomer, "Edit$" + "0"), True)
            Dim t As Type = GetType(GridView)
            Dim p() As Object = New Object(0) {}
            p(0) = New System.Web.UI.WebControls.GridViewEditEventArgs(0)
            Dim m As System.Reflection.MethodInfo = t.GetMethod("OnRowEditing", (System.Reflection.BindingFlags.NonPublic Or System.Reflection.BindingFlags.Instance))
            m.Invoke(grdCustomer, p)
        End If
    End Sub
End Class

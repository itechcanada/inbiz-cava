<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="Create.aspx.vb" Inherits="Sales_Create" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="Resources.resource" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divSectionTitle').corner();
        $('#divSectionContent').corner();
        $('#divMainContainerTitle').corner();
    </script>
    <div id="divSectionTitle" class="divSectionTitle">
        <h2>
            <%=Resources.Resource.lblSearchOptions%>
        </h2>
    </div>
    <div id="divSectionContent" class="divSectionContent">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="imgSearch">
                    <table border="0" cellpadding="0" cellspacing="3">
                        <tr>
                            <td>
                                <asp:Label ID="lblSearchBy" runat="server" Text="<%$Resources:Resource, lblSearchBy %>">            
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="dlSearch" runat="server" Width="175px" ValidationGroup="PrdSrch">
                                    <asp:ListItem Value="PN" Text="<%$ Resources:Resource, liPrdName %>" />
                                    <asp:ListItem Value="IN" Text="<%$ Resources:Resource, liPrdInveInternalID %>" />
                                    <asp:ListItem Value="EX" Text="<%$ Resources:Resource, liPrdExternalID %>" />
                                    <asp:ListItem Value="UC" Text="<%$ Resources:Resource, liPrdUPCCodeID %>" />
                                    <asp:ListItem Value="PD" Text="<%$ Resources:Resource, liPrdDescription %>" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox runat="server" Width="170px" ID="txtSearch" ValidationGroup="PrdSrch"
                                    AutoPostBack="true">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="imgSearch" runat="server" AlternateText="Search" ImageUrl="../images/search-btn.gif"
                                    ValidationGroup="PrdSrch" />
                            </td>
                        </tr>
                         <tr>
                    <td >
                        <asp:Button ID="btnAlpha" CommandArgument="A" Text="A" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button1" CommandArgument="B" Text="B" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button2" CommandArgument="C" Text="C" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button3" CommandArgument="D" Text="D" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button4" CommandArgument="E" Text="E" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button5" CommandArgument="F" Text="F" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button6" CommandArgument="G" Text="G" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button7" CommandArgument="H" Text="H" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button8" CommandArgument="I" Text="I" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button9" CommandArgument="J" Text="J" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button10" CommandArgument="K" Text="K" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button11" CommandArgument="L" Text="L" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button12" CommandArgument="M" Text="M" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button13" CommandArgument="N" Text="N" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button14" CommandArgument="O" Text="O" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />                       
                        <asp:Button ID="Button15" CommandArgument="P" Text="P" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button16" CommandArgument="Q" Text="Q" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button17" CommandArgument="R" Text="R" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button18" CommandArgument="S" Text="S" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button19" CommandArgument="T" Text="T" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button20" CommandArgument="U" Text="U" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button21" CommandArgument="V" Text="V" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button22" CommandArgument="W" Text="W" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button23" CommandArgument="X" Text="X" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button24" CommandArgument="Y" Text="Y" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="Button25" CommandArgument="Z" Text="Z" runat="server" Width="30px" OnCommand="btnAlpha_Command" CausesValidation="false" />
                    </td>
                </tr> 
                <tr>
                    <td>
                        <asp:Button ID="btnCustFav" CommandArgument="CF" Text="Cust Fav" runat="server" OnCommand="btnAlpha_Command" CausesValidation="false" />
                        <asp:Button ID="btnMyFav" CommandArgument="MF" Text="My Fav" runat="server" OnCommand="btnAlpha_Command" CausesValidation="false" />
                    </td>
                </tr>  
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <table border="0" cellpadding="2" cellspacing="5" width="100%" style="display:none;">
        <tr>
            <td align="left">
                <div class="buttonwrapper" style="margin-left:30px;">
                    <a id="cmdAddPriceKit" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                        style="min-width: 80px; text-align: center;">
                        <%=Resources.Resource.cmdCssPriceKit%></span></a>
                </div>
            </td>
        </tr>
        <tr>
            <td align="left" >
                <div style="margin-left:40px;">
                    <asp:CheckBox ID="rbSubscription" runat="server" Text="Subscription" />
                </div>
            </td>
        </tr>
    </table>

</asp:Content>
<asp:Content ID="cntCreate" ContentPlaceHolderID="cphMaster" Runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal>
                    </h2>
                </td> 
                <td>
                    <asp:UpdateProgress runat="server" ID="Updateprogress" DisplayAfter="10">
                        <ProgressTemplate>
                            <img src="../Images/wait22trans.gif" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>               
                <td align="right" style="padding-right:10px;">
                    <asp:Label ID="lblName" runat="server" CssClass="leftHeading">
                    </asp:Label>
                </td>
            </tr>
        </table>       
        <asp:HiddenField runat="server" ID="hdnLeft" />
        <asp:HiddenField runat="server" ID="hdnCustWhs" />
    </div>  

    

<div class="divMainContent" >
    <asp:UpdatePanel ID="upnlMsg" runat="server">
        <ContentTemplate>
            <div align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label></div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="imgAdd" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <div id="extraContainer">
        <%--popup div kit starts here --%>
        <div id="divKit" runat="server" style="width: 750px;">
            <table width="100%" cellpadding="0px" cellspacing="0">
                <tr>
                    <td width='15px' background="../Images/popup_left.png">
                    </td>
                    <td align="left" valign="middle" class="popup_title_newbg">
                        <span class="discount_title">
                            <%=lblMakePrdKit%></span>
                    </td>
                    <td class="popup_title_newbg" align="right">
                        <a href="javascript:funCloseLoc(false);">
                            <img border="0" src='../images/close_popup_btn.gif' width="24" height="24" /></a>
                    </td>
                    <td width='15px' background="../Images/popup_right.png">
                    </td>
                </tr>
            </table>
            <table width="100%" class="popup_content_newbg">
                <tr>
                    <td height="5">
                    </td>
                </tr>
                <tr height="25">
                    <td class="tdAlign" width="35%">
                        <asp:Label ID="Label3" Text="<%$ Resources:Resource, lblPrdProductName %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td width="1%">
                    </td>
                    <td align="left" width="64%">
                        <asp:DropDownList ID="dlProduct" runat="server" ValidationGroup="Kit" Width="336px"
                            CssClass="innerText">
                        </asp:DropDownList>
                        <span class="style1">*</span>
                        <asp:CustomValidator ID="custvalProd" ValidationGroup="Kit" SetFocusOnError="true"
                            runat="server" ErrorMessage="<%$ Resources:Resource, CustvalProd %>" ClientValidationFunction="funSelectProduct"
                            Display="None"></asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tdAlign">
                        <asp:Label ID="Label4" Text="<%$ Resources:Resource, lblPrdProductQuantity %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtProdQty" runat="server" MaxLength="20" ValidationGroup="Kit"
                            CssClass="innerText" /><span class="style1"> *</span>
                        <asp:RequiredFieldValidator ID="reqvalProdQty" runat="server" ValidationGroup="Kit"
                            ControlToValidate="txtProdQty" ErrorMessage="<%$ Resources:Resource, reqvalProdQty %>"
                            SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cmvProdQty" EnableClientScript="true" ValidationGroup="Kit"
                            SetFocusOnError="true" ControlToValidate="txtProdQty" Operator="DataTypeCheck"
                            Type="Double" Display="None" ErrorMessage="<%$ Resources:Resource, cmvProdQty %>"
                            runat="server" />
                    </td>
                </tr>
                <tr height="25">
                    <td class="tdAlign">
                    </td>
                    <td>
                    </td>
                    <td align="left">
                        <table width="100%">
                            <tr>
                                <td width="25%">
                                    <div class="buttonwrapper">
                                        <a id="CmdPrdSave" runat="server" validationgroup="Kit" class="ovalbutton" href="#">
                                            <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                <%=Resources.Resource.cmdCssAddTokit%></span></a></div>
                                </td>
                                <td width="75%" align="left">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="5">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="Center">
                        <asp:Label ID="lblKitMsg" Text="" ForeColor="green" CssClass="lblBold" runat="server" />
                    </td>
                </tr>
                <tr valign="top">
                    <td align="center" colspan="3">
                        <asp:GridView ID="grdvProductKit" runat="server" AllowSorting="True" AllowPaging="True"
                            PageSize="10" PagerSettings-Mode="Numeric" CellPadding="0" GridLines="none" AutoGenerateColumns="False"
                            UseAccessibleHeader="False" Style="border-collapse: separate;" CssClass="view_grid650"
                            DataKeyNames="prdIncludeID" Width="650px">
                            <Columns>
                                <asp:BoundField DataField="prdIncludeID" HeaderStyle-ForeColor="#ffffff" HeaderStyle-Wrap="false"
                                    Visible="false" HeaderText="<%$ Resources:Resource, grdvProductName %>" ReadOnly="True">
                                    <ItemStyle Width="10px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="prdName" HeaderStyle-ForeColor="#ffffff" HeaderStyle-Wrap="false"
                                    HeaderText="<%$ Resources:Resource, grdvProductName %>" ReadOnly="True">
                                    <ItemStyle CssClass="col_prd_name" Width="200px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="prdIntID" HeaderStyle-ForeColor="#ffffff" HeaderStyle-Wrap="false"
                                    HeaderText="<%$ Resources:Resource, grdvProductIntID %>" ReadOnly="True">
                                    <ItemStyle Width="100px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="prdIncludeQty" HeaderStyle-ForeColor="#ffffff" HeaderStyle-Wrap="false"
                                    HeaderText="<%$ Resources:Resource, grdvPrdQty %>" ReadOnly="True">
                                    <ItemStyle Width="150px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="prdUnitPrice" HeaderStyle-ForeColor="#ffffff" HeaderStyle-Wrap="false"
                                    HeaderText="<%$ Resources:Resource, grdPOPrdCostPrice %>" ReadOnly="True">
                                    <ItemStyle Width="150px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="prdPrice" HeaderStyle-ForeColor="#ffffff" HeaderStyle-Wrap="false"
                                    HeaderText="<%$ Resources:Resource, msgPrice %>" ReadOnly="True">
                                    <ItemStyle Width="150px" Wrap="true" HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, grdvDelete %>" HeaderStyle-HorizontalAlign="Center"
                                    HeaderStyle-ForeColor="white">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                            ToolTip="<%$ Resources:Resource, grdvDelete %>" CommandArgument='<%# Eval("prdIncludeID") %>'
                                            CommandName="Delete" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle CssClass="grid_footer" />
                            <RowStyle CssClass="grid_rowstyle" />
                            <PagerStyle CssClass="grid_footer" />
                            <HeaderStyle CssClass="grid_header" />
                            <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                            <PagerSettings PageButtonCount="20" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td height="10">
                    </td>
                </tr>
                <tr runat="Server" id="trPrd" visible="false">
                    <td class="tdAlign">
                        <asp:Label ID="lblProdName" Text="<%$ Resources:Resource, lblkitName %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtProdName" runat="server" MaxLength="250" ValidationGroup="prdKit"
                            CssClass="txtBoxPrd" /><span class="style1"> *</span>
                        <asp:RequiredFieldValidator ID="reqvalProdName" runat="server" ValidationGroup="prdKit"
                            ControlToValidate="txtProdName" ErrorMessage="<%$ Resources:Resource, reqvalKitName %>"
                            SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td height="5">
                    </td>
                </tr>
                <tr runat="Server" id="trBarCode" visible="false">
                    <td class="tdAlign">
                        <asp:Label ID="lblBarcode" Text="<%$ Resources:Resource, lblPrdBarcodeNew %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtBarcode" runat="server" ValidationGroup="prdKit" MaxLength="35"
                            CssClass="txtBoxPrd" /><span class="style1"> *</span>
                        <asp:RequiredFieldValidator ID="reqvalBarCode" runat="server" ValidationGroup="prdKit"
                            ControlToValidate="txtBarcode" ErrorMessage="<%$ Resources:Resource, reqvalKitBarCode %>"
                            SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td height="5">
                    </td>
                </tr>
                <tr runat="Server" id="trAvaiWhs" visible="false" valign="Top">
                    <td class="tdAlign">
                        <asp:Label ID="Label7" Text="<%$ Resources:Resource, lblAvailWarehouse %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblWhsh" Text="" CssClass="lblBold" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td height="5">
                    </td>
                </tr>
                <tr runat="Server" id="trKitPrice" visible="false">
                    <td class="tdAlign">
                        <asp:Label ID="Label6" Text="<%$ Resources:Resource, lblKitPrice %>" CssClass="lblBold"
                            runat="server" />
                    </td>
                    <td>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtKitPrice" runat="server" MaxLength="20" ValidationGroup="prdKit"
                            CssClass="txtBoxPrd" /><span class="style1"> *</span>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="prdKit"
                            ControlToValidate="txtKitPrice" ErrorMessage="<%$ Resources:Resource, reqvalProdKit %>"
                            SetFocusOnError="true" Display="None"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" EnableClientScript="true" ValidationGroup="prdKit"
                            SetFocusOnError="true" ControlToValidate="txtKitPrice" Operator="DataTypeCheck"
                            Type="Double" Display="None" ErrorMessage="<%$ Resources:Resource, cmvProdkitPrice %>"
                            runat="server" />
                    </td>
                </tr>
                <tr height="25" runat="server" id="trCmdKit" visible="False">
                    <td class="tdAlign">
                    </td>
                    <td>
                    </td>
                    <td align="left">
                        <table width="100%">
                            <tr>
                                <td width="25%">
                                    <div class="buttonwrapper">
                                        <a id="cmdFinalSubmit" runat="server" validationgroup="prdKit" class="ovalbutton"
                                            href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                <%=Resources.Resource.cmdCssSubmit%></span></a></div>
                                </td>
                                <td width="75%" align="left">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <%-- popup idv kit ends here --%>
        <%-- porecess popup div stats here --%>
        <asp:UpdatePanel ID="upnlSearchProducts" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlProductSearchGrid" runat="server" style="display:none;">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr>
                                <td background="../Images/popup_left.png" width="15">
                                </td>
                                <td align="left" valign="middle" class="popup_title_newbg">
                                    <span class="discount_title">Product Search Result</span>
                                </td>
                                <td align="right" class="popup_title_newbg">
                                    <a href="javascript:funClosePrdSearchPop();">
                                        <img border="0" width="24" height="24" src="../images/close_popup_btn.gif"></a>
                                </td>
                                <td background="../Images/popup_right.png" style="width: 15px">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="popup_content_newbg" width="100%">
                        <tbody>
                            <tr>
                                <td>
                                    <asp:GridView ID="grdProcess" runat="server" AllowSorting="True" DataSourceID="sqldsProcess"
                                        AllowPaging="True" PageSize="25" PagerSettings-Mode="Numeric" CellPadding="0"
                                        GridLines="none" AutoGenerateColumns="False" CssClass="view_grid" Style="border-collapse: separate;"
                                        UseAccessibleHeader="False" DataKeyNames="ProductID" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="ProductID" HeaderText="<%$ Resources:Resource, grdPoProductID %>"
                                                ReadOnly="True" SortExpression="ProductID">
                                                <ItemStyle Width="70px" Wrap="true" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="prdUPCCode" HeaderText="<%$ Resources:Resource, grdPOUPCCode %>"
                                                ReadOnly="True" SortExpression="prdUPCCode">
                                                <ItemStyle Width="70px" Wrap="true" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="prdName" HeaderText="<%$ Resources:Resource, grdPOProductName %>"
                                                ReadOnly="True" SortExpression="prdName">
                                                <ItemStyle CssClass="col_prd_name" Width="180px" Wrap="true" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="prdOhdQty" HeaderStyle-HorizontalAlign="Right" HeaderText="<%$ Resources:Resource, grdvOnHandQty %>"
                                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                <ItemStyle Width="90px" Wrap="true" HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Price" HeaderStyle-HorizontalAlign="Right" HeaderText="<%$ Resources:Resource, grdPOPrdCostPrice %>"
                                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                <ItemStyle Width="90px" Wrap="true" HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="prdTaxCode" NullDisplayText="0" HeaderStyle-HorizontalAlign="Right"
                                                HeaderText="<%$ Resources:Resource, POTaxGrp %>" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="prdDiscount" NullDisplayText="0" HeaderStyle-HorizontalAlign="Right"
                                                HeaderText="<%$ Resources:Resource, PODiscount %>" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="" HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("ProductID") %>'
                                                        CommandName="Edit" CausesValidation="false" ImageUrl="~/images/add_24.png" />
                                                    <asp:LinkButton ID="lnkSingleClick" Text="Edit" runat="server" CommandArgument='<%# Eval("ProductID") %>'
                                                        CommandName="Edit" CausesValidation="false" Style="display: none;" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <div style="display: none;">
                                                        <asp:TextBox runat="server" Width="0px" ID="txtDisType" Text='<%# Eval("prdDiscountType") %>'>
                                                        </asp:TextBox>
                                                        <asp:TextBox runat="server" Width="0px" ID="txtwhs" Text='<%# Eval("prdWhsCode") %>'>
                                                        </asp:TextBox>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="0" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle CssClass="grid_footer" />
                                        <RowStyle CssClass="grid_rowstyle cursor_point" />
                                        <PagerStyle CssClass="grid_footer" />
                                        <HeaderStyle CssClass="grid_header" Height="26px" />
                                        <AlternatingRowStyle CssClass="grid_alter_rowstyle cursor_point" />
                                        <PagerSettings PageButtonCount="20" />
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="sqldsProcess" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                                </td>
                            </tr>
                        </tbody>
                    </table>                    
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="imgAdd" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <%-- porecess popup div ends here --%>
        <%-- product item edit starts here --%>
        <asp:UpdatePanel ID="upnlProductItems" runat="server">
            <ContentTemplate>
                <div id="divItems" runat="server" visible="false">
                    <div class="popup_container" id="divViewPrd" runat="server" style="display: none;">
                        <asp:Panel ID="pnlAddProduct" runat="server" DefaultButton="imgAdd">
                            <table cellspacing="0" cellpadding="0" width="756">
                                <tbody>
                                    <tr class="popup_title_bg">
                                        <td align="center" colspan="9">
                                            <div class="popup_title">
                                                <span id="lblAddProduct"><b>
                                                    <%=lblSOAddPrdInQuotationItemList%></b></span></div>
                                            <div class="pop_close_cont">
                                                <asp:HyperLink ID="hlCloseProductEditPop" ImageUrl="~/images/close_popup_btn.gif" NavigateUrl="javascript:void(0);" runat="server" />
                                                <%--<a href="javascript:funCloseLoc();" name="Close">
                                                    <img alt="Close" src="../images/close_popup_btn.gif" border="0" />
                                                </a>--%>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr colspan="8">
                                        <td class="popup_contentbg" valign="top">
                                            <table cellpadding="0" cellspacing="2" border="0" width="753">
                                                <tr>
                                                    <td colspan="7" height="6">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="tdPrdIdLbl" runat="server" visible="false">
                                                        <asp:Label ID="lblPOProductID1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, POProductID1 %>"></asp:Label>
                                                    </td>
                                                    <td class="popup_lbl">
                                                        <asp:Label ID="lblUPCCode" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOUPCCode %>"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblPOProductName1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, POProductName1 %>"></asp:Label>
                                                    </td>
                                                    <td class="popup_lbl">
                                                        <asp:Label ID="lblPOTaxGroup" runat="server" CssClass="lblBold" Text="Tax Group:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblPODiscount" runat="server" CssClass="lblBold" Text="Discount:"></asp:Label>
                                                    </td>
                                                    <td class="popup_lbl">
                                                        <asp:Label ID="lblQuantity" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOQuantity %>"></asp:Label>
                                                    </td>
                                                    <td class="popup_lbl">
                                                        <asp:Label ID="lblPrice" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, grdPOPrdCostPrice %>"></asp:Label>
                                                    </td>
                                                    <td id="tdPrdWhsLbl" runat="server" visible="false">
                                                        <asp:Label ID="lblPOWarehouse" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOWarehouse%>"></asp:Label>
                                                    </td>
                                                    <td rowspan="2">
                                                        <img src="../images/popup_line.gif" alt="" />
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td id="tdPrdIdTxt" runat="server" visible="false">
                                                        <asp:TextBox ID="txtProductId" runat="server" Width="60px" BackColor="LightGray"
                                                            ReadOnly="true"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtUPCCode" runat="server" Width="100px" CssClass="popup_input"
                                                            BackColor="LightGray" ReadOnly="true">
                                                        </asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtProductName" runat="server" CssClass="popup_textarea" Width="170px"
                                                            TextMode="MultiLine" Rows="4" ReadOnly="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="dlTaxCode" CssClass="dropdown_pop" runat="server" Width="130px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDiscount" runat="server" Width="50px" CssClass="popup_input"
                                                            onkeyup="return funOpenDiscount();" onkeydown="return funOpenDiscount();" onclick="return funOpenDiscount();"
                                                            ValidationGroup="lstSrch" MaxLength="10"></asp:TextBox>
                                                        <div id="divDiscount" align="left" runat="server" class="discount_pop_wrapper" style="display: none;">
                                                            <%--<asp:Panel runat=server ID=onldiscount DefaultButton=cmdAddDiscount>--%>
                                                            <table cellpadding="0" class="discount_cont_bg" cellspacing="0">
                                                                <tr>
                                                                    <td width="86%" align="center">
                                                                        <asp:Label CssClass="discount_title" runat="server" ID="lblNote1" Text="<%$ Resources:Resource, lblPrdBlanketDis %>"></asp:Label>
                                                                    </td>
                                                                    <td style="padding-right: 15px;" align="right">
                                                                        <a name="Close" href="javascript:funCloseDiscount();">
                                                                            <img border="0" src="../images/close_popup_btn.gif" alt="Close" /></a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="10">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" colspan="2">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" width="90%">
                                                                            <tr>
                                                                                <td class="tdAlign">
                                                                                    <asp:Label ID="lblDiscount" Text="<%$ Resources:Resource, lblPrdBDis %>" CssClass="lblBold"
                                                                                        runat="server" />
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:TextBox ValidationGroup="Dicsount" CssClass="popup_input" ID="txtDiscountVal"
                                                                                        runat="server" Width="85px" />
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <asp:RadioButtonList ID="rblstDiscountType" CssClass="discount_radio_lbl" ValidationGroup="Dicsount"
                                                                                        runat="server" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdBlanketDisPercentage %>" Selected="true"
                                                                                            Value="P"></asp:ListItem>
                                                                                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdBlanketDisAbsolute %>" Value="A"></asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2" align="center">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td height="20" align="right" width="45%">
                                                                                                <img src="../images/add_icon.gif" alt="add">
                                                                                            </td>
                                                                                            <td width="55%">
                                                                                                <asp:LinkButton ID="cmdAddDiscount" runat="server" CssClass="lblBold" ValidationGroup="Dicsount"
                                                                                                    Text="<%$ Resources:Resource, cmdCssAdd %>" AlternateText=""></asp:LinkButton>
                                                                                                <%-- <asp:ImageButton id="cmdAddDiscount" runat="server" ValidationGroup="Dicsount" text="ADD" AlternateText=""></asp:ImageButton>
                                        <div class="buttonwrapper"><a ID="cmdAddDiscount" ValidationGroup="Dicsount" runat="server"  class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:80px;text-align:center;"><%=Resources.Resource.cmdCssAdd%></span></a></div>--%>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <%--<asp:ImageButton runat=server  ID=cmdAddDiscount ValidationGroup=Dicsount ImageUrl="~/images/add.png" />--%>
                                                                                    <asp:CompareValidator ID="compvalDis" EnableClientScript="true" SetFocusOnError="true"
                                                                                        ValidationGroup="Dicsount" ControlToValidate="txtDiscountVal" Operator="DataTypeCheck"
                                                                                        Type="Double" Display="None" ErrorMessage="<%$ Resources:Resource, compvalPrdDiscount %>"
                                                                                        runat="server" />
                                                                                    <asp:CustomValidator ID="custvalPrD" runat="server" ValidationGroup="Dicsount" ErrorMessage="<%$ Resources:Resource, CustvalPrdDisParcentage %>"
                                                                                        ClientValidationFunction="funCalParcentageDiscount" Display="None"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="3">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:ValidationSummary ID="ValsDiscount" ValidationGroup="Dicsount" runat="server"
                                                                ShowMessageBox="true" ShowSummary="false" />
                                                            <%--</asp:Panel>--%>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtQuantity" CssClass="popup_input" runat="server" Width="50px"
                                                            ValidationGroup="lstSrch"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqvalQuantity" runat="server" Display="None" ErrorMessage="<%$ Resources:Resource, msgSOItemQuantity%>"
                                                            SetFocusOnError="true" ValidationGroup="lstSrch" ControlToValidate="txtQuantity">
                                                        </asp:RequiredFieldValidator>
                                                        <asp:RangeValidator ID="rngvalQuantity" runat="server" Display="None" ErrorMessage="<%$ Resources:Resource, msgSOItemQuantityGT0LessThanOnHand%>"
                                                            SetFocusOnError="true" ValidationGroup="lstSrch" ControlToValidate="txtQuantity"
                                                            Type="Double" MinimumValue="0.001" MaximumValue="100000">
                                                        </asp:RangeValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPrice" CssClass="popup_input" runat="server" Width="50px" ValidationGroup="lstSrch">
                                                        </asp:TextBox><br />
                                                        <asp:LinkButton runat="server" ID="lbnPriceRange" Text="<%$ Resources:Resource, lblSuggestedPrice%>"></asp:LinkButton>
                                                        <asp:RequiredFieldValidator ID="reqvalPrice" runat="server" Display="None" ErrorMessage="<%$ Resources:Resource, msgSOItemPrice%>"
                                                            SetFocusOnError="true" ValidationGroup="lstSrch" ControlToValidate="txtPrice">
                                                        </asp:RequiredFieldValidator><asp:RangeValidator ID="rngvalPrice" runat="server"
                                                            Display="None" ErrorMessage="<%$ Resources:Resource, msgSOItemPriceGT0%>" SetFocusOnError="true"
                                                            ValidationGroup="lstSrch" ControlToValidate="txtPrice" Type="double" MinimumValue="0.001"
                                                            MaximumValue="1000000000">
                                                        </asp:RangeValidator>
                                                        <!--Division for Suggested Price-->
                                                        <div id="divPriceRnge" align="left" runat="server" style="width: 300px; position: absolute;
                                                            top: 5px; left: 200px; text-align: center;">
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td width='15px' background="../Images/popup_left.png">
                                                                    </td>
                                                                    <td align="center" valign="middle" class="popup_title_newbg">
                                                                        <span class="discount_title">
                                                                            <asp:Label ID="lblPriceRange" runat="server" Text="<%$ Resources:Resource, lblSuggestedPrice%>"></asp:Label></span>
                                                                    </td>
                                                                    <td class="popup_title_newbg" align="right">
                                                                        <a href="javascript:funClosePriceRangeLoc();">
                                                                            <img border="0" src='../images/close_popup_btn.gif' width="24" height="24" /></a>
                                                                    </td>
                                                                    <td width='15px' background="../Images/popup_right.png">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table id="tblRangePrice" border="0" cellpadding="2" cellspacing="2" width="300"
                                                                style="background: url('../images/popup_content_newbg.gif') repeat-x bottom;
                                                                border-style: solid; border-width: thin;">
                                                                <tr>
                                                                    <td align="center">
                                                                        <div>
                                                                            <asp:GridView ID="grdvwSuggestedPrice" runat="server" AllowPaging="True" PageSize="10"
                                                                                PagerSettings-Mode="Numeric" CellPadding="0" GridLines="none" AutoGenerateColumns="False"
                                                                                CssClass="view_grid" Style="border-collapse: separate;" DataSourceID="sqldsPriceRange"
                                                                                Width="270px">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="" HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                                                                        <ItemTemplate>
                                                                                            <input type="radio" value="<%# Eval("id") %>" name="rbSuggestedPrice" />
                                                                                            <%-- <asp:RadioButton id="rdbSelectedPrice" runat="server" GroupName="gnPriceRange"></asp:RadioButton>--%>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="fromQty" HeaderText="From Qty" ReadOnly="True">
                                                                                        <ItemStyle Width="50px" Wrap="True" HorizontalAlign="Center" />
                                                                                        <HeaderStyle ForeColor="White" Wrap="False" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="toQty" HeaderText="To Qty" ReadOnly="True">
                                                                                        <ItemStyle Width="50px" Wrap="True" HorizontalAlign="Center" />
                                                                                        <HeaderStyle ForeColor="White" Wrap="False" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="SalesPrice" HeaderText="Sale Price" ReadOnly="True">
                                                                                        <ItemStyle Width="50px" Wrap="True" HorizontalAlign="Center" />
                                                                                        <HeaderStyle ForeColor="White" Wrap="False" />
                                                                                    </asp:BoundField>
                                                                                </Columns>
                                                                                <RowStyle CssClass="grid_rowstyle" />
                                                                                <PagerStyle CssClass="grid_footer" />
                                                                                <HeaderStyle CssClass="grid_header" Height="15" />
                                                                                <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                                                                <PagerSettings PageButtonCount="5" />
                                                                                <AlternatingRowStyle BackColor="#EDEDEE" ForeColor="#284775" />
                                                                                <EmptyDataTemplate>
                                                                                    <div style="text-align: center; border: none;">
                                                                                        <br />
                                                                                        <br />
                                                                                        <b>
                                                                                            <%= lblNoRecordFound %></b>
                                                                                        <br />
                                                                                        <br />
                                                                                    </div>
                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                            <asp:SqlDataSource ID="sqldsPriceRange" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                                                                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 10px;">
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" style="text-align: center;">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td height="20" align="center" width="20%">
                                                                                </td>
                                                                                <td width="80%">
                                                                                    <div class="buttonwrapper">
                                                                                        <a id="cmdSuggestedPriceOK" onclick="javascript:getSelectedRadio();" runat="server"
                                                                                            class="ovalbutton" href="#"><span class="ovalbutton" style="min-width: 50px; text-align: center;
                                                                                                margin-right: 10px;">
                                                                                                <%=Resources.Resource.Ok%></span></a> <a id="cmdSuggestedPriceCancel" runat="server"
                                                                                                    class="ovalbutton" href="javascript:funClosePriceRangeLoc();"><span class="ovalbutton"
                                                                                                        style="min-width: 50px; text-align: center;">
                                                                                                        <%=Resources.Resource.cmdCssCancel%></span></a></div>
                                                                                    <asp:HiddenField ID="hdnSelectedId" runat="server"></asp:HiddenField>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <!--Division End for Suggested Price-->
                                                    </td>
                                                    <td id="tdPrdWhsTxt" runat="server" visible="false">
                                                        <asp:DropDownList ID="dlWarehouse" runat="server" Width="120px" ValidationGroup="lstSrch"
                                                            Enabled="false">
                                                        </asp:DropDownList>
                                                        <asp:CustomValidator ID="custvalPOWarehouse" runat="server" Display="None" ClientValidationFunction="funCheckWarehouseCode"
                                                            ErrorMessage="<%$ Resources:Resource, custvalPOWarehouse%>" SetFocusOnError="true"
                                                            ValidationGroup="lstSrch">
                                                        </asp:CustomValidator>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td width="80px" align="center">
                                                        <%--<div class="buttonwrapper"><a ID="imgAdd" runat="server" ValidationGroup="lstSrch"  class="ovalbutton" href="#" ><span class="ovalbutton" style="min-width:80px;text-align:center;"><%=Resources.Resource.cmdCssAdd%></span></a></div>--%>
                                                        <asp:ImageButton ID="imgAdd" runat="server" ValidationGroup="lstSrch" ImageUrl="../images/add_pop_btn.gif"
                                                            AlternateText=""></asp:ImageButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="cmdAddDiscount" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="imgAdd" EventName="Click"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="grdRequest" EventName="RowEditing"></asp:AsyncPostBackTrigger>
                <asp:AsyncPostBackTrigger ControlID="grdRequest" EventName="RowCommand"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <%-- product item edit ends here --%>
        <%-- product Items grid starts here--%>
        <asp:UpdatePanel ID="upnlItemsList" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divItemsGrid" runat="server" style="display:none; padding: 5px;">
                    <div style="margin: 4px 0px 4px 0px;">
                        <asp:Label ID="lblPOIL" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOQuotationItemList %>"
                            runat="server" Visible="false" /><br />
                    </div>
                    <asp:GridView ID="grdRequest" runat="server" AllowSorting="False" AllowPaging="True"
                        PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0" GridLines="none" AutoGenerateColumns="False"
                        CssClass="view_grid" Style="border-collapse: separate;" UseAccessibleHeader="False"
                        DataKeyNames="ProductID" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="ProductID" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdPoProductID %>"
                                ReadOnly="True" SortExpression="ProductID" Visible="true">
                                <ItemStyle Width="70px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="UPCCode" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdPOUPCCode %>"
                                ReadOnly="True" SortExpression="UPCCode">
                                <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ProductName" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdPOProductName %>"
                                ReadOnly="True" SortExpression="ProductName">
                                <ItemStyle CssClass="col_prd_name" Width="280px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Quantity" HeaderText="<%$ Resources:Resource, grdSOQuantity %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Price" HeaderStyle-HorizontalAlign="Right" HeaderText="<%$ Resources:Resource, grdPOPrdCostPrice %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="RangePrice" HeaderStyle-HorizontalAlign="Right" HeaderText="<%$ Resources:Resource, lblSuggestedPrice %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="WarehouseCode" HeaderText="<%$ Resources:Resource, grdPOWhsCode %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TaxGrp" NullDisplayText="0" HeaderStyle-HorizontalAlign="Right"
                                HeaderText="Tax Group" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Discount" NullDisplayText="0" HeaderStyle-HorizontalAlign="Right"
                                HeaderText="<%$ Resources:Resource, grdSaleDiscount %>" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdEdit %>" Visible="true"
                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEditIL" runat="server" CommandArgument='<%# Eval("ProductID") %>'
                                        CommandName="Edit" CausesValidation="false" ImageUrl="~/images/edit_icon.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" HeaderStyle-ForeColor="#ffffff"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDeleteIL" runat="server" ImageUrl="~/images/delete_icon.png"
                                        CausesValidation="false" CommandArgument='<%# Eval("ProductID") %>' CommandName="Delete" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="DisType">
                                <ItemStyle Width="0" Font-Size="0" ForeColor="white" />
                            </asp:BoundField>
                        </Columns>
                        <%--<FooterStyle CssClass="grid_footer" />--%>
                        <RowStyle CssClass="grid_rowstyle" />
                        <PagerStyle CssClass="grid_footer" />
                        <HeaderStyle CssClass="grid_header" Height="26px" />
                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                        <PagerSettings PageButtonCount="20" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqldsRequest" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                    <br />
                    <br />
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="imgAdd" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <%-- product Items grid ends here--%>
        <%-- sales process starts here --%>
        <div id="divSalesProcess" runat="server" style="display: none; width: 805;">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td width='15px' background="../Images/popup_left.png">
                    </td>
                    <td class="popup_title_newbg">
                        <div style="display: none;">
                            <asp:Label ID="lblProcessID" Text="Process ID:" runat="server" /></div>
                    </td>
                    <td class="popup_title_newbg" align="left">
                        <asp:Label ID="lblProcessDescription" runat="server" Text="<%$ Resources:Resource, lblProcessDescription %>" />
                    </td>
                     <%-- Changes by Ankit Choure :: Put here the DDl for Tax--%>
                                               <td class="popup_title_newbg" width="160">
                                                        <asp:Label ID="Label9" runat="server"  Text="Tax Group"></asp:Label>
                                                    </td>


                    <td class="popup_title_newbg" align="left">
                        <asp:Label ID="lblProcessFixedCost" Text="<%$ Resources:Resource, lblProcessFixedCost %>"
                            runat="server" />
                    </td>
                    <td class="popup_title_newbg" align="left">
                        <asp:Label ID="lblProcessCostPerHour" Text="<%$ Resources:Resource, lblProcessCostPerHour %>"
                            runat="server" />
                    </td>
                    <td class="popup_title_newbg" align="left">
                        <asp:Label ID="lblProcessCostPerUnit" Text="<%$ Resources:Resource, lblProcessCostPerUnit%>"
                            runat="server" />
                    </td>
                    <td class="popup_title_newbg" align="left">
                        <asp:Label ID="lblTotalHour" Text="<%$ Resources:Resource, lblSOTotalHour%>" runat="server" />
                    </td>
                    <td class="popup_title_newbg" align="left">
                        <asp:Label ID="lblTotalUnit" Text="<%$ Resources:Resource, lblSOTotalUnit%>" runat="server" />
                    </td>
                    <td width="90" class="popup_title_newbg" align="right">
                        <a href="javascript:funCloseProcessLoc();" name="Close">
                            <img alt="Close" src="../images/close_popup_btn.gif" border="0" />
                        </a>
                    </td>
                    <td width='15px' background="../Images/popup_right.png">
                    </td>
                </tr>
                <tr>
                    <td colspan="12">
                        <table cellpadding="0" width="100%" cellspacing="0" class="popup_content_newbg">
                            <tr height="50px">
                                <td width="15">
                                </td>
                                <td>
                                    <div style="display: none;">
                                        <asp:TextBox ID="txtProcessID" runat="server" Width="0px" ReadOnly="true" BackColor="LightGray"></asp:TextBox></div>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtProcessDescription" CssClass="popup_input" runat="server" Width="140px"
                                        ReadOnly="true" BackColor="LightGray">
                                    </asp:TextBox>
                                </td>




                                  <td>
                                                   <asp:TextBox ID="txtTaxGrp" CssClass="popup_input" runat="server" Width="140px"
                                        ReadOnly="true" BackColor="LightGray">
                                    </asp:TextBox>
                                                        </td>
                                
                                
                                
                                <td>

                                    <asp:TextBox ID="txtProcessFixedCost" CssClass="popup_input" runat="server" Width="75px"
                                        ValidationGroup="lstSrch">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvalQuantity1" runat="server" ControlToValidate="txtProcessFixedCost"
                                        ErrorMessage="<%$ Resources:Resource, msgSOPlzEntFixedCost%>" SetFocusOnError="true"
                                        Display="None" ValidationGroup="valProcess">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtProcessCostPerHour" CssClass="popup_input" runat="server" Width="75px"
                                        ValidationGroup="lstSrch">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvalPrice1" runat="server" ControlToValidate="txtProcessCostPerHour"
                                        ErrorMessage="<%$ Resources:Resource, msgSOPlzEntCostPerHour%>" SetFocusOnError="true"
                                        Display="None" ValidationGroup="valProcess">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtProcessCostPerUnit" CssClass="popup_input" runat="server" Width="75px"
                                        ValidationGroup="lstSrch">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvalProcessCostPerUnit" runat="server" ControlToValidate="txtProcessCostPerUnit"
                                        ErrorMessage="<%$ Resources:Resource, msgSOPlzEntCostPerUnit%>" SetFocusOnError="true"
                                        Display="None" ValidationGroup="valProcess">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTotalHour" CssClass="popup_input" runat="server" Width="75px"
                                        ValidationGroup="valProcess">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvalTotalHour" runat="server" ControlToValidate="txtTotalHour"
                                        ErrorMessage="<%$ Resources:Resource, msgSOPlzEntTotalHour%>" SetFocusOnError="true"
                                        Display="None" ValidationGroup="valProcess">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTotalUnit" CssClass="popup_input" runat="server" Width="75px"
                                        ValidationGroup="valProcess">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvalTotalUnit" runat="server" ControlToValidate="txtTotalUnit"
                                        ErrorMessage="<%$ Resources:Resource, msgSOPlzEntTotalUnit%>" SetFocusOnError="true"
                                        Display="None" ValidationGroup="valProcess">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <asp:ImageButton ID="imgAddProcess" runat="server" ImageUrl="../images/update.png"
                                        AlternateText="" ValidationGroup="valProcess" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <%-- sales process starts here --%>
        <%-- Process grid starts here --%>
        <div id="divPrcList" runat="server">
            <asp:Label ID="lblProcessList" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOProcessItemList %>"
                runat="server" Visible="false" /><br />
            <asp:GridView ID="grdAddProcLst" runat="server" AllowSorting="False" AllowPaging="True"
                PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0" GridLines="none" AutoGenerateColumns="False"
                CssClass="view_grid" Style="border-collapse: separate;" UseAccessibleHeader="False"
                DataKeyNames="ProcessID" Width="100%">
                <Columns>
                    <asp:BoundField DataField="ProcessID" HeaderStyle-ForeColor="#ffffff" HeaderText="Process ID:"
                        Visible="false" ReadOnly="True" SortExpression="ProcessID">
                        <ItemStyle Width="70px" Wrap="true" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProcessCode" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdProcessCode %>"
                        Visible="false" ReadOnly="True" SortExpression="ProcessCode">
                        <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProcessDescription" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdProcessDescription %>"
                        ReadOnly="True" SortExpression="ProcessDescription">
                        <ItemStyle Width="250px" Wrap="true" HorizontalAlign="Left" />
                    </asp:BoundField>



                    <asp:BoundField DataField="TaxGrpName" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdTaxDescription %>"
                                            ReadOnly="True" SortExpression="ProcessDescription">
                                            <ItemStyle Width="250px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>





                    <asp:BoundField DataField="ProcessFixedCost" HeaderStyle-HorizontalAlign="Right"
                        HeaderText="<%$ Resources:Resource, grdProcessFixedCost %>" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                        <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProcessCostPerHour" HeaderStyle-HorizontalAlign="Right"
                        HeaderText="<%$ Resources:Resource, grdProcessCostPerHour %>" ReadOnly="True"
                        HeaderStyle-ForeColor="#ffffff">
                        <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProcessCostPerUnit" HeaderStyle-HorizontalAlign="Right"
                        HeaderText="<%$ Resources:Resource, grdProcessCostPerUnit %>" ReadOnly="True"
                        HeaderStyle-ForeColor="#ffffff">
                        <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TotalHour" HeaderText="<%$ Resources:Resource, grdSOTotalHour %>"
                        HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                        <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TotalUnit" HeaderText="<%$ Resources:Resource, grdSOTotalUnit %>"
                        HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                        <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdEdit %>" Visible="true"
                        HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgEditPL" runat="server" CommandArgument='<%# Eval("ProcessID") %>'
                                CommandName="Edit" ImageUrl="~/images/edit_icon.png" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" HeaderStyle-ForeColor="#ffffff"
                        HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgDeletePL" runat="server" ImageUrl="~/images/delete_icon.png"
                                CausesValidation="false" CommandArgument='<%# Eval("ProcessID") %>' CommandName="Delete" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="grid_rowstyle" />
                <PagerStyle CssClass="grid_footer" />
                <HeaderStyle CssClass="grid_header" />
                <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                <PagerSettings PageButtonCount="20" />
            </asp:GridView>
            <asp:SqlDataSource ID="sqldsAddProcLst" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                ProviderName="System.Data.Odbc"></asp:SqlDataSource>
            <asp:HiddenField ID="hdnCtr" runat="server" />
        </div>
        <%-- Process grid ends here --%>
        <%--Ship To Address Start--%>
        <div id="divShipToAddress" runat="server" style="display: none; position: relative;
            left: 0px; top: 0px; border: 3px; border-color: black; margin: 0; height: 0;
            width: 350px;">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width='15px' background="../Images/popup_left.png">
                    </td>
                    <td align="left" valign="middle" class="popup_title_newbg">
                        <span class="discount_title">
                            <%=titleShpToAddress%></span>
                    </td>
                    <td class="popup_title_newbg" align="right">
                        <a href="javascript:funClosePopUp('<%=divShipToAddress.ClientID %>');">
                            <img border="0" src='../images/close_popup_btn.gif' width="24" height="24" /></a>
                    </td>
                    <td width='15px' background="../Images/popup_right.png">
                    </td>
                </tr>
            </table>
            <table width="100%" class="popup_content_newbg">
                <tr height="30">
                    <td class="tdAlign">
                        <asp:Label ID="lblShpToAddressLine1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressLine1%>">
                        </asp:Label>
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtShpToAddressLine1" runat="server" MaxLength="35" Width="200px"
                            CssClass="popup_input">
                        </asp:TextBox>
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalShpToAddressLine1" runat="server" ValidationGroup="valsShpToAddress"
                            ControlToValidate="txtShpToAddressLine1" ErrorMessage="<%$ Resources:Resource, reqvalShpToAddressLine1 %>"
                            SetFocusOnError="true" Display="None">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr height="30">
                    <td class="tdAlign">
                        <asp:Label ID="lblShpToAddressLine2" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressLine2%>">
                        </asp:Label>
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtShpToAddressLine2" runat="server" MaxLength="35" CssClass="popup_input"
                            Width="200px">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr height="30">
                    <td class="tdAlign">
                        <asp:Label ID="lblShpToAddressLine3" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressLine3%>">
                        </asp:Label>
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtShpToAddressLine3" runat="server" MaxLength="35" CssClass="popup_input"
                            Width="200px">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr height="30">
                    <td class="tdAlign">
                        <asp:Label ID="lblShpToAddressCity" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressCity%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtShpToAddressCity" runat="server" Width="200px" MaxLength="25"
                            CssClass="popup_input" />
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalShpToAddressCity" runat="server" ValidationGroup="valsShpToAddress"
                            ControlToValidate="txtShpToAddressCity" ErrorMessage="<%$ Resources:Resource, PlzEntShpToAddressCity %>"
                            SetFocusOnError="true" Display="None">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr height="30" id="trShpToState" runat="server" visible="true">
                    <td class="tdAlign">
                        <asp:Label ID="lblShpToAddressState" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressState%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtShpToAddressState" runat="server" Width="200px" MaxLength="25"
                            CssClass="popup_input" />
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalShpToAddressState" runat="server" ValidationGroup="valsShpToAddress"
                            ControlToValidate="txtShpToAddressState" ErrorMessage="<%$ Resources:Resource, PlzEntShpToState %>"
                            SetFocusOnError="true" Display="None">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr height="30" id="trShpToCountry" runat="server" visible="true">
                    <td class="tdAlign">
                        <asp:Label ID="lblShpToAddressCountry" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressCountry%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtShpToAddressCountry" runat="server" Width="200px" MaxLength="25"
                            CssClass="popup_input" />
                    </td>
                </tr>
                <tr height="30">
                    <td class="tdAlign">
                        <asp:Label ID="lblShpToAddressPostalCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressPostalCode %>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtShpToAddressPostalCode" runat="server" Width="200Px" MaxLength="10"
                            CssClass="popup_input" />
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalShpToAddressPostalCode" runat="server" ValidationGroup="valsShpToAddress"
                            ControlToValidate="txtShpToAddressPostalCode" ErrorMessage="<%$ Resources:Resource, PlzEntShpToAddressPostalCode %>"
                            SetFocusOnError="true" Display="None">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                        <table width="100%">
                            <tr>
                                <td height="20" align="right" width="50%">
                                    <img src="../images/add_icon.gif" alt="add">
                                </td>
                                <td width="50%">
                                    <asp:LinkButton ID="cmdShpToAddress" runat="server" CssClass="lblBold" ValidationGroup="valsShpToAddress"
                                        Text="<%$ Resources:Resource, cmdCssAdd %>" AlternateText=""></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <%--Ship To Address End--%>
        <%--Bill To Address Start--%>
        <div id="divBillToAddress" runat="server" style="display: none; position: relative;
            left: 380px; top: 0px; width: 350px; border: 3px; border-color: black; margin: 0;
            height: 0;">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width='15px' background="../Images/popup_left.png">
                    </td>
                    <td align="left" valign="middle" class="popup_title_newbg">
                        <span class="discount_title">
                            <%=titleBillToAddress%></span>
                    </td>
                    <td class="popup_title_newbg" align="right">
                        <a href="javascript:funClosePopUp('<%=divBillToAddress.ClientID %>');">
                            <img border="0" src='../images/close_popup_btn.gif' width="24" height="24" /></a>
                    </td>
                    <td width='15px' background="../Images/popup_right.png">
                    </td>
                </tr>
            </table>
            <table width="100%" class="popup_content_newbg">
                <tr height="30">
                    <td class="tdAlign">
                        <asp:Label ID="lblBillToAddressLine1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressLine1%>">
                        </asp:Label>
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtBillToAddressLine1" runat="server" MaxLength="35" Width="200px"
                            CssClass="popup_input">
                        </asp:TextBox>
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalBillToAddressLine1" runat="server" ValidationGroup="valsBillToAddress"
                            ControlToValidate="txtBillToAddressLine1" ErrorMessage="<%$ Resources:Resource, reqvalBillToAddressLine1 %>"
                            SetFocusOnError="true" Display="None">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr height="30">
                    <td class="tdAlign">
                        <asp:Label ID="lblBillToAddressLine2" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressLine2%>">
                        </asp:Label>
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtBillToAddressLine2" runat="server" MaxLength="35" CssClass="popup_input"
                            Width="200px">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr height="30">
                    <td class="tdAlign">
                        <asp:Label ID="lblBillToAddressLine3" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressLine3%>">
                        </asp:Label>
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtBillToAddressLine3" runat="server" MaxLength="35" CssClass="popup_input"
                            Width="200px">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr height="30">
                    <td class="tdAlign">
                        <asp:Label ID="lblBillToAddressCity" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressCity%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtBillToAddressCity" runat="server" Width="200px" MaxLength="25"
                            CssClass="popup_input" />
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalBillToAddressCity" runat="server" ValidationGroup="valsBillToAddress"
                            ControlToValidate="txtBillToAddressCity" ErrorMessage="<%$ Resources:Resource, PlzEntBillToAddressCity %>"
                            SetFocusOnError="true" Display="None">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr height="30" id="trBillToState" runat="server" visible="true">
                    <td class="tdAlign">
                        <asp:Label ID="lblBillToAddressState" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressState%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtBillToAddressState" runat="server" Width="200px" MaxLength="25"
                            CssClass="popup_input" />
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalBillToAddressState" runat="server" ValidationGroup="valsBillToAddress"
                            ControlToValidate="txtBillToAddressState" ErrorMessage="<%$ Resources:Resource, PlzEntBillToState %>"
                            SetFocusOnError="true" Display="None">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr height="30" id="trBillToCountry" runat="server" visible="true">
                    <td class="tdAlign">
                        <asp:Label ID="lblBillToAddressCountry" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressCountry%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtBillToAddressCountry" runat="server" Width="200px" MaxLength="25"
                            CssClass="popup_input" />
                    </td>
                </tr>
                <tr height="30">
                    <td class="tdAlign">
                        <asp:Label ID="lblBillToAddressPostalCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressPostalCode %>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtBillToAddressPostalCode" runat="server" Width="200Px" MaxLength="10"
                            CssClass="popup_input" />
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalBillToAddressPostalCode" runat="server" ValidationGroup="valsBillToAddress"
                            ControlToValidate="txtBillToAddressPostalCode" ErrorMessage="<%$ Resources:Resource, PlzEntBillToAddressPostalCode %>"
                            SetFocusOnError="true" Display="None">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                        <table width="100%">
                            <tr>
                                <td height="20" align="right" width="50%">
                                    <img src="../images/add_icon.gif" alt="add">
                                </td>
                                <td width="50%">
                                    <asp:LinkButton ID="cmdBillToAddress" runat="server" CssClass="lblBold" ValidationGroup="valsBillToAddress"
                                        Text="<%$ Resources:Resource, cmdCssAdd %>" AlternateText=""></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <%--<asp:Button runat="server" ID="cmdBillToAddress" CssClass="imgSave" ValidationGroup="valsBillToAddress"  />--%>
                    </td>
                </tr>
                <%--Bill To Address End--%>
            </table>
        </div>
        <%-- bill to address ends --%>
    </div>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr id="divSubscription" style="display: none;">
            <td colspan="2">
                <div>
                    <div class="divSectionTitle">
                        <h2>
                            <asp:Literal runat="server" ID="Label8" Text="<%$ Resources:Resource, prnInvSubCriptionDtl %>"></asp:Literal>
                        </h2>
                    </div>
                    <div class="divSectionContent" style="min-height: 0px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr id="trSubscriptionDetails">
                                <td class="tdAlign" style="width: 16%">
                                    <span class="lblBold" style="width: 40px;"><%= Resources.Resource.lblAddDaysOfMonth%> </span>
                                    <br />
                                </td>
                                <td align="left" style="width: 16%">
                                    <asp:DropDownList ID="ddlDays" runat="server">
                                        <asp:ListItem Text="0" />
                                    </asp:DropDownList>
                                    <a id="hlAddDays" href="javascript:void(0);">
                                        <img src="../Images/add_icon.gif" alt="Add" /> </a>                                    
                                </td>
                                <td class="tdAlign" style="width: 16%">
                                    <asp:Label class="lblBold" ID="lblSelectedDays" runat="server" Text="<%$Resources:Resource, lblToBeInvoicedOn %>"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtDayMonth" runat="server" Text="" ReadOnly="true"></asp:TextBox>
                                    <a id="hlClear" href="javascript:void(0);">Clear</a>
                                </td>
                                <td class="tdAlign" style="width: 16%">
                                    <span class="lblBold" style="width: 40px;"><%=Resources.Resource.lblSelectEndDate %></span>
                                </td>
                                <td align="left" style="width: 16%">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtEnddate" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Image runat="server" ID="imgbtnEnddate" ToolTip="Click to show calendar"
                                                    ImageUrl="~/images/calendar.gif"></asp:Image>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender" TargetControlID="txtEnddate"
                                                    runat="server" PopupButtonID="imgbtnEnddate" Format="MM/dd/yyyy">
                                                </ajaxToolkit:CalendarExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <span style="color: Red; padding-left: 10px;">Note* : Maximum 5 days subscription allowed
                            for each invoice.</span>
                    </div>
                </div>
            </td>
        </tr>                
        <tr>
            <td colspan="2" align="center">
                <div id="divTerms" runat="server" align="center">
                    
                    <table width="100%" style="border: 0;" border="0" align="center">                        
                         <tr height="25">                            
                            <td class="tdAlign">
                                <asp:Label ID="Label1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblUserOrderType%>"></asp:Label>
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="dlstOderType" runat="server" Width="135px">
                                </asp:DropDownList><span class="style1"> *</span>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="dlstOderType"
                                    ErrorMessage="<%$ Resources:Resource, custvalOderType %>" InitialValue="0" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                                <%--<asp:CustomValidator ID="custvalOderType" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<%$ Resources:Resource, custvalOderType%>" 
                                    ClientValidationFunction="funCheckOderType" Display="None" >
                                    </asp:CustomValidator>--%>
                            </td>
                             <td colspan="4">
                                 <table align="right">
                                     <tr>
                                         <td>
                                             <div class="buttonwrapper">
                                                 <a id="cmdsave" runat="server" class="ovalbutton" href="#" onserverclick="cmdSave_Click"><span class="ovalbutton"
                                                     style="min-width: 120px; text-align: center;">
                                                     <%=Resources.Resource.cmdCssSave%></span></a></div>
                                         </td>
                                         <td>
                                             <div class="buttonwrapper">
                                                 <a id="cmdReset" runat="server" causesvalidation="false" class="ovalbutton" href="#" onserverclick="cmdReset_Click">
                                                     <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                         <%=Resources.Resource.cmdCssReset%></span></a></div>
                                             <div class="buttonwrapper">
                                                 <a id="cmdback" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                                                     <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                         <%=Resources.Resource.cmdCssback%></span></a></div>
                                         </td>
                                     </tr>
                                 </table>
                             </td>
                         </tr>                      
                        
                        <% If ConfigurationManager.AppSettings("AllowCustomInvoiceEntry").ToLower = "true" Then%>
                        <tr>
                            <td class="tdAlign">
                                <asp:Label ID="Label2" CssClass="lblBold" Text="<%$ Resources:Resource, lblINInvoiceNo %>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtInvNo" Width="150px" runat="server"></asp:TextBox>
                                <asp:RangeValidator ID="regVal" runat="server" Display="None" ErrorMessage="<%$ Resources:Resource, msgInvNoGreThanZero%>"
                                    SetFocusOnError="true" ControlToValidate="txtInvNo" Type="double" MinimumValue="0.001"
                                    MaximumValue="100000000000"></asp:RangeValidator>
                                <%-- <asp:RequiredFieldValidator ID="reqvalInvNo" runat="server" 
                    ControlToValidate="txtInvNo" 
                ErrorMessage="<%$ Resources:Resource, lblPlzEntInvNo %>" 
                SetFocusOnError=true Display="None"></asp:RequiredFieldValidator>--%>
                            </td>
                            <td>
                            </td>
                            <td class="tdAlign">
                                <asp:Label ID="Label5" CssClass="lblBold" Text="<%$ Resources:Resource, lblINInvoiceDate %>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtInvDate" Width="75px" MaxLength="15" runat="server"></asp:TextBox>
                                <asp:Image ID="imgInv" CausesValidation="false" runat="server" ToolTip="Click to show calendar"
                                    ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                                <%=lblPODateFormat%>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="txtInvDate"
                                    runat="server" PopupButtonID="imgInv" Format="MM/dd/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <%-- <asp:RequiredFieldValidator ID="reqvalInvDate" runat="server" 
                ControlToValidate="txtInvDate" 
                ErrorMessage="<%$ Resources:Resource, lblPlzEntInvDate %>" 
                SetFocusOnError=true Display="None">
                </asp:RequiredFieldValidator>--%>
                                <asp:CompareValidator ID="comvalInvDate" EnableClientScript="true" SetFocusOnError="true"
                                    ControlToValidate="txtInvDate" ControlToCompare="txtDate" Operator="DataTypeCheck"
                                    Type="Date" Display="None" ErrorMessage="<%$ Resources:Resource, lblValidInvoiceDate %>"
                                    runat="server" />
                            </td>
                        </tr>
                        <% End If%>
                        <tr height="25">
                            <td class="tdAlign" width="23%">
                                <asp:Label ID="lblShpToAddress" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShpToAddress %>"
                                    runat="server" />
                            </td>
                            <td width="1%">
                            </td>
                            <td align="left" width="28%">
                                <asp:TextBox ID="txtShpToAddress" runat="server" TextMode="MultiLine" Rows="5">
                                </asp:TextBox>
                                <asp:CustomValidator ID="custValShpToAddress" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<%$ Resources:Resource, msgSOPlzEntShpToAddress %>" ClientValidationFunction="funCheckShpToAddress"
                                    Display="None">
                                </asp:CustomValidator>
                            </td>
                            <td width="1%">
                            </td>
                            <td class="tdAlign" width="24%">
                                <asp:Label ID="lblBillToAddress" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOBillToAddress %>"
                                    runat="server" />
                            </td>
                            <td width="1%">
                            </td>
                            <td align="left" width="22%">
                                <asp:TextBox ID="txtBillToAddress" runat="server" TextMode="MultiLine" Rows="5">
                                </asp:TextBox>
                                <asp:CustomValidator ID="custvalBillToAddress" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<%$ Resources:Resource, msgSOPlzEntBillToAddress %>" ClientValidationFunction="funCheckBillToAddress"
                                    Display="None">
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr height="25">
                            <td class="tdAlign">
                                <asp:Label ID="lblTerms" CssClass="lblBold" Text="<%$ Resources:Resource, POShippingTerms%>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtTerms" runat="server" TextMode="MultiLine" Rows="3">
                                </asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td class="tdAlign">
                                <asp:Label ID="lblNetTerms" CssClass="lblBold" Text="<%$ Resources:Resource, lblSONetTerms%>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox MaxLength="4" ID="txtNetTerms" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr height="25" style="display:none;">
                            <td class="tdAlign">
                                <asp:Label ID="lblResellerShipBlankPref" CssClass="lblBold" Text="<%$ Resources:Resource, lblResellerShipBlankPref%>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rblstShipBlankPref" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="<%$ Resources:Resource, lblYes%>" Value="1">
                                    </asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:Resource, lblNo%>" Value="0" Selected="True">
                                    </asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td>
                            </td>
                            <td class="tdAlign">
                                <asp:Label ID="lblISWeb" Text="<%$ Resources:Resource, lblSOSalesISWeb%>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rdlstISWeb" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1">
                                    </asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0" Selected="True">
                                    </asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr height="25">
                            <td class="tdAlign">
                                <asp:Label ID="lblShpWarehouse" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingWarehouse %>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="dlShpWarehouse" runat="server" Width="155px">
                                </asp:DropDownList>
                                <asp:CustomValidator ID="custvalShpWarehouse" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<%$ Resources:Resource, lblSOPlzSelShippingWarehouse %>" ClientValidationFunction="funCheckShippingWarehouseCode"
                                    Display="None">
                                </asp:CustomValidator>
                            </td>
                            <td>
                            </td>
                            <td class="tdAlign">
                                <asp:Label ID="lblShpTrackNo" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingTrackNo %>"
                                    runat="server" Visible="False" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtShpTrackNo" runat="server" Width="150px" MaxLength="25" 
                                    onkeypress="return disableEnterKey(event)" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr height="25" id="trShippingCost" runat="server" visible="false">
                            <td class="tdAlign">
                                <asp:Label ID="lblShpCost" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingCost %>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left" colspan="5" valign="middle">
                                <asp:TextBox ID="txtShpCost" runat="server" Width="150px" MaxLength="6" onkeypress="return disableEnterKey(event)">
                                </asp:TextBox>
                                <asp:CompareValidator ID="comvalShpCost" EnableClientScript="true" SetFocusOnError="true"
                                    ControlToValidate="txtShpCost" Operator="DataTypeCheck" Type="double" Display="None"
                                    ErrorMessage="<%$ Resources:Resource, lblSOPlzEntNumericValueInShippingCost %>"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr height="25">
                            <td class="tdAlign">
                                <asp:Label ID="lblShpDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingDate %>">
                                </asp:Label>
                            </td>
                            <td>
                            </td>
                            <td align="left" valign="middle">
                                <asp:TextBox ID="txtShippingDate" Enabled="true" runat="server" Width="90px" MaxLength="15"
                                    onkeypress="return disableEnterKey(event)">
                                </asp:TextBox>
                                <asp:ImageButton ID="imgCalShpDate" CausesValidation="false" runat="server" ToolTip="Click to show calendar"
                                    ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                                <%=lblPODateFormat%>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" TargetControlID="txtShippingDate"
                                    runat="server" PopupButtonID="imgCalShpDate" Format="MM/dd/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:RequiredFieldValidator ID="reqvalShpDate" runat="server" ControlToValidate="txtShippingDate"
                                    ErrorMessage="<%$ Resources:Resource, lblSOPlzEntShippingDate %>" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                                <div style="display: none;">
                                    <asp:TextBox ID="txtDate" Enabled="true" runat="server" Width="0px" MaxLength="15">
                                    </asp:TextBox>
                                </div>
                                <asp:CompareValidator ID="cmpvalShpDate" EnableClientScript="true" SetFocusOnError="true"
                                    ControlToValidate="txtShippingDate" ControlToCompare="txtDate" Operator="GreaterThanEqual"
                                    Type="Date" Display="None" ErrorMessage="<%$ Resources:Resource, lblSOPlzEntShippingDateGTCurDate %>"
                                    runat="server" />
                            </td>
                            <td>
                            </td>
                            <td class="tdAlign">
                                <asp:Label ID="lblShpCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingCode %>"
                                    runat="server" Visible="False" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtShpCode" runat="server" Width="150px" MaxLength="15" 
                                    onkeypress="return disableEnterKey(event)" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr height="25">
                            <td class="tdAlign" style="height: 25px">
                                <asp:Label ID="lblNotes" CssClass="lblBold" Text="<%$ Resources:Resource, PONotes%>"
                                    runat="server" />
                            </td>
                            <td style="height: 25px">
                            </td>
                            <td align="left" colspan="5" style="height: 25px">
                                <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Rows="3" Width="550px">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr height="25">
                            <td class="tdAlign">
                                <asp:Label ID="lblQutExpDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOQutExpDate %>">
                                </asp:Label>
                            </td>
                            <td>
                            </td>
                            <td align="left" valign="middle">
                                <asp:TextBox ID="txtQutExpDate" Enabled="true" runat="server" Width="90px" MaxLength="15"
                                    onkeypress="return disableEnterKey(event)">
                                </asp:TextBox>
                                <asp:ImageButton ID="imgCalQutExpDate" CausesValidation="false" runat="server" ToolTip="Click to show calendar"
                                    ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                                <%=lblPODateFormat %>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender5" TargetControlID="txtQutExpDate"
                                    runat="server" PopupButtonID="imgCalQutExpDate" Format="MM/dd/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:RequiredFieldValidator ID="reqvalQutExpDate" runat="server" ControlToValidate="txtQutExpDate"
                                    ErrorMessage="<%$ Resources:Resource, lblSOPlzEntQutExpDate %>" SetFocusOnError="true"
                                    Display="None">
                                </asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cmpvalQutExpDate" EnableClientScript="true" SetFocusOnError="true"
                                    ControlToValidate="txtQutExpDate" ControlToCompare="txtDate" Operator="GreaterThanEqual"
                                    Type="Date" Display="None" ErrorMessage="<%$ Resources:Resource, lblSOErrQutExpDate %>"
                                    runat="server" />
                            </td>
                            <td colspan="4">
                                <table align="right">
                                     <tr>
                                         <td>
                                             <div class="buttonwrapper">
                                                 <a id="cmdSave2" runat="server" class="ovalbutton" href="#" onserverclick="cmdSave_Click"><span class="ovalbutton"
                                                     style="min-width: 120px; text-align: center;">
                                                     <%=Resources.Resource.cmdCssSave%></span></a></div>
                                         </td>
                                         <td>
                                             <div class="buttonwrapper">
                                                 <a id="cmdReset2" runat="server" causesvalidation="false" class="ovalbutton" href="#"  onserverclick="cmdReset_Click">
                                                     <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                                         <%=Resources.Resource.cmdCssReset%></span></a></div>                                             
                                         </td>
                                     </tr>
                                 </table>
                            </td>
                            
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr height="30" id="trCurrency" runat="server" visible="false">
            <td class="tdAlign">
                <asp:Label ID="lblPOCurrencyCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOCurrencyCode%>"
                    runat="server" />
            </td>
            <td class="tdAlignLeft">
                <asp:DropDownList ID="dlCurrencyCode" runat="server" Width="135px">
                </asp:DropDownList>
                <span class="style1">*</span>
                <asp:CustomValidator ID="custvalPOCurrencyCode" runat="server" SetFocusOnError="true"
                    ErrorMessage="<%$ Resources:Resource, custvalPOCurrencyCode%>" ClientValidationFunction="funCheckCurrencyCode"
                    Display="None">
                </asp:CustomValidator>
            </td>
        </tr>
        <tr height="30" id="trExchageRate" runat="server" visible="false">
            <td class="tdAlign">
                <asp:Label ID="lblPOExRate" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOExRate%>"
                    runat="server" />
            </td>
            <td class="tdAlignLeft">
                <asp:TextBox ID="txtExRate" runat="server" Width="130px" MaxLength="5">
                </asp:TextBox>
            </td>
        </tr>
        <tr height="30" id="trCustPo" runat="server" visible="false">
            <td class="tdAlign">
                <asp:Label ID="lblCustPO" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOCustomerPO%>"
                    runat="server" />
            </td>
            <td class="tdAlignLeft">
                <asp:TextBox ID="txtCustPO" runat="server" Width="130px" MaxLength="35" onkeypress="return disableEnterKey(event)">
                </asp:TextBox>
            </td>
        </tr>
        <tr height="30">
            <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                
            </td>
        </tr>
    </table>
 
    <asp:ValidationSummary ID="valsBillToAddress" ValidationGroup="valsBillToAddress"  runat="server" ShowMessageBox="true"
        ShowSummary="false" />
    <asp:ValidationSummary ID="valsShpToAddress"  ValidationGroup="valsShpToAddress" runat="server" ShowMessageBox="true"
        ShowSummary="false" />

    <asp:ValidationSummary ID="valsSales" runat="server" ShowMessageBox="true"
        ShowSummary="false" />
    <asp:ValidationSummary ID="valsSales1" runat="server" ShowMessageBox="true"
        ShowSummary="false" ValidationGroup="lstSrch"/>    
		    <asp:ValidationSummary ID="ValsProcess" runat="server" ShowMessageBox="true"
        ShowSummary="false" ValidationGroup="valProcess"/> 
            <asp:ValidationSummary ID="valsumKit"  runat="server" ShowMessageBox="true" ShowSummary="false" ValidationGroup="Kit"/>
            <asp:ValidationSummary ID="ValidationSummary1"  runat="server" ShowMessageBox="true" ShowSummary="false" ValidationGroup="prdKit"/>
</div>                    
    <script language="javascript" type="text/javascript">
        function funCheckCurrencyCode(source, args) {
            if (document.getElementById('<%=dlCurrencyCode.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
        function funCheckWarehouseCode(source, args) {
            if (document.getElementById('<%=dlWarehouse.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }

        function funCheckShippingWarehouseCode(source, args) {
            if (document.getElementById('<%=dlShpWarehouse.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
        function funCheckShpToAddress(source, args) {
            if (document.getElementById('<%=txtShpToAddress.ClientID%>').value == "") {
                args.IsValid = false;
            }
        }
        function funCheckBillToAddress(source, args) {
            if (document.getElementById('<%=txtBillToAddress.ClientID%>').value == "") {
                args.IsValid = false;
            }
        }
        function funOpenLocation() {
            document.getElementById('<%=divViewPrd.ClientID%>').style.display = "block";
            return false;
        }
        function funCloseLoc(isOpenSearchReslt) {
            document.getElementById('<%=divKit.ClientID%>').style.display = "none";
            document.getElementById('<%=divViewPrd.ClientID%>').style.display = "none";
            document.getElementById('<%=lblMsg.ClientID%>').innerHTML = "";
            if (isOpenSearchReslt) {
                document.getElementById('<%=pnlProductSearchGrid.ClientID%>').style.display = "block";
            }
        }
        function funClosePrdSearchPop() {
            document.getElementById('<%=lblMsg.ClientID%>').innerHTML = "";
            document.getElementById('<%=pnlProductSearchGrid.ClientID%>').style.display = "none";
        }
        function funClosePopUp(strOption) {
            document.getElementById(strOption).style.display = "none";
        }
        function popUp(strOption) {
            document.getElementById(strOption).style.display = "block";
            return false;
        }

        function funOpenDiscount() {
            document.getElementById('<%=divDiscount.ClientID%>').style.display = "block";
            var strDiscount = document.getElementById('<%= txtDiscount.ClientID%>').value;
            if (window.document.getElementById('<%=rblstDiscountType.ClientID%>_0').checked == true) {
                document.getElementById('<%= txtDiscountVal.ClientID%>').value = strDiscount.replace("%", "").trim();
            }
            else {
                document.getElementById('<%= txtDiscountVal.ClientID%>').value = strDiscount;
            }
            document.getElementById('<%= txtDiscountVal.ClientID%>').focus();
            return false;
        }

        function funCloseDiscount() {
            document.getElementById('<%=divDiscount.ClientID%>').style.display = "none";
        }

        function funCalParcentageDiscount(source, arguments) {
            if (window.document.getElementById('<%=rblstDiscountType.ClientID%>_0').checked == true && document.getElementById('<%= txtDiscountVal.ClientID%>').value >= 100) {
                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;
            }
        }

        function funCloseProcessLoc() {
            document.getElementById('<%=divSalesProcess.ClientID%>').style.display = "none";

        }

        function funCheckOderType(source, args) {
            if (document.getElementById('<%=dlstOderType.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }

        function funSelectProduct(source, arguments) {
            if (window.document.getElementById('<%=dlProduct.ClientID%>').value != "0") {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }

        function funPrdKit() {
            var browserWidth = 0;
            browserWidth = (document.documentElement.clientWidth - 750) / 2;
            document.getElementById('<%=divKit.ClientID%>').style.position = "absolute";
            document.getElementById('<%=divKit.ClientID%>').style.left = browserwidth + "px";
            document.getElementById('<%=divKit.ClientID%>').style.top = "250px";
            document.getElementById('<%=divKit.ClientID%>').style.display = "block";
            var hdn = document.getElementById('<%=hdnLeft.ClientID%>');
            hdn.value = browserWidth;
            return false;
        }

        var browserWidth = 0;
        browserWidth = (document.documentElement.clientWidth - 750) / 2;
        var hdn = document.getElementById('<%=hdnLeft.ClientID%>');
        hdn.value = browserWidth;


        function funClosePriceRangeLoc() {
            document.getElementById('<%=divPriceRnge.ClientID%>').style.display = "none";
        }

        function getSelectedRadio() {
            var selectedRadio = getRadioValue("rbSuggestedPrice", false);
            document.getElementById('<%=hdnSelectedId.ClientID%>').value = selectedRadio;
            if (selectedRadio == 'non') {
                alert("<%=pleaseSelectAChoice%>");
            }
            else {
                document.getElementById('<%=divPriceRnge.ClientID%>').style.display = "none";
            }
        }

        function setSuggestedPriceRadio() {
            alert(document.getElementById('<%=hdnSelectedId.ClientID%>').value);
            setRadioValue("rbSuggestedPrice", document.getElementById('<%=hdnSelectedId.ClientID%>').value);
        }

        function getRadioValue(RadioGrpName, isParent) {
            var elements = null;
            if (isParent) {
                elements = parent.document.getElementsByName(RadioGrpName);
            } else {
                elements = document.getElementsByName(RadioGrpName);
            }
            var i = 0;
            while (i < elements.length) {
                if ($(elements[i]).attr("checked")) {
                    return $(elements[i]).attr("value")
                }
                i++;
            }
            return 'non';
        }

        function setRadioValue(RadioGrpName, value) {
            var elements = document.getElementsByName(RadioGrpName);
            var i = 0;
            while (i < elements.length) {
                if ($(elements[i]).attr("value") == value) {
                    $(elements[i]).attr("checked", true);
                } else {
                    $(elements[i]).attr("checked", false);
                }
                i++;
            }
        }
        
    </script>

    <script type="text/javascript" language="javascript">
        $("#<%=rbSubscription.ClientID %>").click(function () {
            var ele1 = document.getElementById("divSubscription");

            if (this.checked) {
                ele1.style.display = "";
            }
            else {
                ele1.style.display = "none";
            }
        });

        $("#hlAddDays").click(function () {
            var txtEle = document.getElementById("<%=txtDayMonth.ClientID %>");
            var daysEle = document.getElementById("<%=ddlDays.ClientID %>");

            if (daysEle.value == "0") {
                alert("Please select valid day.")
            }
            else {
                if (txtEle.value.length == 0) {
                    txtEle.value = daysEle.value;
                }
                else {
                    var strArr = txtEle.value.split(",");
                    var allowAdd = true;
                    if (strArr.length == 5) {
                        alert("Only five subscription days to be allowed to enter.");
                    }
                    else {
                        $(strArr).each(function () {
                            if (this == daysEle.value) {
                                allowAdd = false;
                            }
                        });

                        if (allowAdd) {
                            txtEle.value += "," + daysEle.value;
                        }
                        else {
                            alert("Selected day already added.");
                        }
                    }
                }
            }
        });

        $("#hlClear").click(function () {
            $("#<%=txtDayMonth.ClientID %>").val("");
        });

        $("#ctl00_cphMaster_txtQuantity").live("focus", function(){
            $(this).select();
        });


        
               
    </script>
</asp:Content>

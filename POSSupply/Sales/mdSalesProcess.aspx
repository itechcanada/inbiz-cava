﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true" CodeFile="mdSalesProcess.aspx.cs" Inherits="Sales_mdSalesProcess" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphHead" runat="Server">   
    <style type="text/css">
        ul.form li div.lbl{width:120px !important;}  
        ul.form li div.input{width:150px !important;}         
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div style="padding: 5px 10px;">
        <asp:Panel runat="server" ID="pnlGrid">
            <asp:Label ID="lblMsg" runat="server" ForeColor="Red" />
            <asp:Panel ID="pnlSearch" runat="server">
                <table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label5" runat="server" Text="Keyword" AssociatedControlID="txtSearch"
                                CssClass="filter-key"></asp:Label>
                        </td>
                        <td valign="middle" align="left">
                            <asp:TextBox runat="server" Width="195px" ID="txtSearch" ValidationGroup="PrdSrch">
                            </asp:TextBox>
                        </td>
                        <td>
                            <input id="btnSearch" type="button" value="Search" />
                        </td>
                    </tr>
                </table>
                <%--Grid view Start for Process--%>
                <div id="grid_wrapper" style="width: 100%">
                    <trirand:JQGrid runat="server" ID="grdProcess" DataSourceID="sdsProcess" Height="250px"
                        AutoWidth="true" PagerSettings-PageSize="50" OnCellBinding="grdProcess_CellBinding"
                        OnDataRequesting="grdProcess_DataRequesting" OnRowSelecting="grdProcess_RowSelecting">
                        <Columns>
                            <trirand:JQGridColumn DataField="ProcessID" HeaderText="" PrimaryKey="true" Visible="false" />
                            <trirand:JQGridColumn DataField="ProcessCode" HeaderText="<%$ Resources:Resource, grdProcessCode %>"
                                Editable="false" />
                            <trirand:JQGridColumn DataField="ProcessDescription" HeaderText="<%$ Resources:Resource, grdProcessDescription %>" />
                            <trirand:JQGridColumn DataField="ProcessFixedCost" HeaderText="<%$ Resources:Resource, grdProcessFixedCost %>">
                               <%-- <Formatter>
                                    <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                                        ThousandsSeparator="," />
                                </Formatter>--%>
                            </trirand:JQGridColumn>
                            <trirand:JQGridColumn DataField="ProcessCostPerHour" HeaderText="<%$ Resources:Resource, grdProcessCostPerHour %>">
                                <%--<Formatter>
                                    <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                                        ThousandsSeparator="," />
                                </Formatter>--%>
                            </trirand:JQGridColumn>
                            <trirand:JQGridColumn DataField="ProcessCostPerUnit" HeaderText="<%$ Resources:Resource, grdProcessCostPerUnit %>">
                                <%--<Formatter>
                                    <trirand:NumberFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="0.00"
                                        ThousandsSeparator="," />
                                </Formatter>--%>
                            </trirand:JQGridColumn>
                        </Columns>
                        <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                            ShowDeleteButton="false" ShowSearchButton="false" />
                        <SortSettings InitialSortColumn="" />
                        <AppearanceSettings AlternateRowBackground="True" />
                        <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="gridLoadComplete" />
                    </trirand:JQGrid>
                    <asp:SqlDataSource ID="sdsProcess" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                        ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
                    <br />
                </div>
            </asp:Panel>
        </asp:Panel>
        <%--Edit Selected Item--%>
        <asp:Panel ID="pnlDetails" runat="server" Visible="false">
            <h3>
                Edit Process Details</h3>
            <ul class="form">
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblProcessCode" runat="server" Text="<%$ Resources:Resource, lblProcessCode %>"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtProcessCode" runat="server" CssClass="popup_input" Width="80px"
                            ReadOnly="true" BackColor="LightGray">
                        </asp:TextBox>
                    </div>
                    <div class="lbl">
                        <asp:Label ID="lblProcessDescription" runat="server" Text="<%$ Resources:Resource, lblProcessDescription %>"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtProcessDescription" runat="server" CssClass="popup_input" Width="185px"
                            ReadOnly="true" BackColor="LightGray">
                        </asp:TextBox>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblPOTaxGroup" runat="server" CssClass="lblBold" Text="Tax Group:"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:DropDownList ID="dlTaxCode" runat="server" Width="130px">
                        </asp:DropDownList>
                    </div>
                    <div class="lbl">
                        <asp:Label ID="lblProcessFixedCost" runat="server" Text="<%$ Resources:Resource, lblProcessFixedCost %>"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtProcessFixedCost" runat="server" Width="70px" CssClass="popup_input"
                            ValidationGroup="lstSrch">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalQuantity" runat="server" ControlToValidate="txtProcessFixedCost"
                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntFixedCost%>" SetFocusOnError="true"
                            Display="None" ValidationGroup="lstSrch">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblProcessCostPerHour" runat="server" Text="<%$ Resources:Resource, lblProcessCostPerHour%>"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtProcessCostPerHour" runat="server" Width="60px" CssClass="popup_input"
                            ValidationGroup="lstSrch">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalPrice" runat="server" ControlToValidate="txtProcessCostPerHour"
                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntCostPerHour%>" SetFocusOnError="true"
                            Display="None" ValidationGroup="lstSrch">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="lbl">
                        <asp:Label ID="lblProcessCostPerUnit" runat="server" Text="<%$ Resources:Resource, lblProcessCostPerUnit %>"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtProcessCostPerUnit" runat="server" Width="60px" CssClass="popup_input"
                            ValidationGroup="lstSrch">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalProcessCostPerUnit" runat="server" ControlToValidate="txtProcessCostPerUnit"
                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntCostPerUnit%>" SetFocusOnError="true"
                            Display="None" ValidationGroup="lstSrch">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblTotalHour" runat="server" Text="<%$ Resources:Resource, lblSOTotalHour %>"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtTotalHour" runat="server" Width="70px" CssClass="popup_input"
                            ValidationGroup="lstSrch">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalTotalHour" runat="server" ControlToValidate="txtTotalHour"
                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntTotalHour%>" SetFocusOnError="true"
                            Display="None" ValidationGroup="lstSrch">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="lbl">
                        <asp:Label ID="lblTotalUnit" runat="server" Text="<%$ Resources:Resource, lblSOTotalUnit%>"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtTotalUnit" runat="server" Width="70px" CssClass="popup_input"
                            ValidationGroup="lstSrch">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalTotalUnit" runat="server" ControlToValidate="txtTotalUnit"
                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntTotalUnit%>" SetFocusOnError="true"
                            Display="None" ValidationGroup="lstSrch">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <%--Added by mukesh 20130523 start--%>
                <li>
                    <div class="lbl">
                        <asp:Label ID="lblInternalCost" runat="server" Text="<%$ Resources:Resource, lblSOInternalCost%>"></asp:Label>
                    </div>
                    <div class="input">
                        <asp:TextBox ID="txtProcessInternalCost" runat="server" Width="70px" CssClass="popup_input"
                            ValidationGroup="lstSrch">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalInternalCost" runat="server" ControlToValidate="txtProcessInternalCost"
                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntInternalCost%>" SetFocusOnError="true"
                            Display="None" ValidationGroup="lstSrch">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="clearBoth">
                    </div>
                </li>
                <%--//Added end --%>
            </ul>
            <h2>
            </h2>
            <div align="right" style="margin-top: 5px;">
                <asp:Button ID="imgAdd" runat="server" ValidationGroup="lstSrch" 
                    Text="Add Process" onclick="imgAdd_Click" />
                <asp:Button ID="btnBackToList" Text="Back to List" runat="server" OnClick="btnBackToList_Click" />
            </div>
            <script type="text/javascript">
                function pageLoad() {
                    //alert("Test");
                    $("#<%=txtProcessFixedCost.ClientID%>").focus(function () {
                        $(this).select();
                    }).focus();
                }
            </script>
        </asp:Panel>
    </div>    
    <asp:HiddenField ID="hdnProcessID" runat="server" />    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphScripts" runat="Server">    
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdProcess.ClientID %>";
        var searchPanelID = "<%=pnlSearch.ClientID %>";

        function resize_the_grid() {
            $('#' + gridID).fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $('#' + gridID).onJqGridPaging();
        }


        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $('#' + gridID).onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID });

        function gridLoadComplete(data) {
            $(window).trigger("resize");
            $("#<%=txtProcessFixedCost.ClientID%>").focus();
        }
                    
    </script>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/popup.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="mdSearchProduct.aspx.cs" Inherits="Sales_mdSearchProduct" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphHead" runat="Server">
    <style type="text/css">
        ul.form li div.lbl
        {
            width: 100px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMaster" runat="Server">
    <div style="padding: 5px 10px;">
        <asp:Panel runat="server" ID="pnlPo">
            <asp:Label ID="lblMsg" runat="server" ForeColor="Red" />
            <asp:Panel ID="pnlSearch" runat="server">
                <table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <%--<asp:Panel ID="pnlSearcInner" runat="server" DefaultButton="btnQuickSearch">--%>
                        <td align="left" style="width: 100px;">
                            <asp:Label ID="Label5" runat="server" Text="Keyword" AssociatedControlID="txtSearch"
                                CssClass="filter-key"></asp:Label>
                        </td>
                        <td valign="middle" align="left">
                            <asp:TextBox runat="server" Width="195px" ID="txtSearch" ValidationGroup="PrdSrch">
                            </asp:TextBox>
                            <%--                                <asp:RequiredFieldValidator ID="rqtxtSKU" ControlToValidate="txtSearch" SetFocusOnError="True"
                                    runat="server" Text="<%$Resources:Resource, lblRequiredSKU%>" ValidationGroup="validateSKU"></asp:RequiredFieldValidator>--%>
                        </td>
                        <td align="left" style="width: 100px;">
                            <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lblSearchBy%>"
                                AssociatedControlID="dlSearch" CssClass="filter-key"></asp:Label>
                        </td>
                        <td valign="middle" align="left">
                            <asp:DropDownList ID="dlSearch" runat="server" Width="145px">
                                <%--
<asp:ListItem Value="PN" Text="<%$ Resources:Resource, liPrdName %>" />
                                    <asp:ListItem Value="IN" Text="<%$ Resources:Resource, liPrdInveInternalID %>" />
                                    <asp:ListItem Value="EX" Text="<%$ Resources:Resource, liPrdExternalID %>" />
                                    <asp:ListItem Value="UC" Text="<%$ Resources:Resource, liPrdUPCCodeID %>" />
                                    <asp:ListItem Value="PD" Text="<%$ Resources:Resource, liPrdDescription %>" />--%>
                                <asp:ListItem Value="PN" Text="<%$ Resources:Resource, lblSrchName %>" />
                                <asp:ListItem Value="UC" Text="<%$ Resources:Resource, lblSrchSKU %>" />
                                <asp:ListItem Value="QS" Text="<%$ Resources:Resource, lblQuickSKU %>" Selected="True" />
                                <asp:ListItem Value="ST" Text="<%$ Resources:Resource, lblSrchStyle %>" />
                                <asp:ListItem Value="PD" Text="<%$ Resources:Resource, lblSrchDescription %>" />
                                <asp:ListItem Value="IN" Text="<%$ Resources:Resource, lblSrchInternalID %>" />
                                <asp:ListItem Value="EX" Text="<%$ Resources:Resource, lblSrchExternalID %>" />
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnQuickSearch" CausesValidation="false" runat="server" Style="display: block;
                                visibility: hidden; width: 0px;" Text="<%$ Resources:Resource, lblQuickSearch %>"
                                ValidationGroup="validateSKU" OnClick="btnQuickSearch_OnClick" />
                        </td>
                        <td>
                            <input id="btnSearch" type="button" value="<%=Resources.Resource.lblSearch%>" />
                        </td>
                        <td>
                            <asp:Button ID="cmdAddProduct" CausesValidation="false" runat="server" Text="<%$ Resources:Resource, lblAddNewProduct %>" Visible="false" />
                        </td>
                        <%--</asp:Panel>--%>
                    </tr>
                    <tr id="trCollection" style="display: none;">
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCollection" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnSelectedStyle" runat="server" Text="<%$ Resources:Resource, lblSelectStyle %>"
                                OnClick="btnSelectedStyle_Onclick" />
                            <asp:HiddenField ID="hdnStyle" runat="server" Value="" />
                            <asp:HiddenField ID="hdnPname" runat="server" Value="" />
                        </td>
                    </tr>
                </table>
                <%--                <asp:Panel ID="plLetterSearch" runat="server" HorizontalAlign="Center">
                    <asp:Repeater ID="LetterSearch" runat="server" 
                        onitemdatabound="LetterSearch_ItemDataBound">
                        <ItemTemplate>      
                            <a href="javascript:void(0);" class="alpha_search" search-data='<%# Container.DataItem %>'style="font-weight:bold;"><%# Container.DataItem %></a>                      
                            <input id="rdoAlpha" type="radio" name="lsearch" value="<%# Container.DataItem %>" runat="server" style="display:none;" />&nbsp;&nbsp;
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>--%>
                <%--Grid view Start for Customer--%>
                <div id="grid_wrapper" style="width: 100%; display: none;">
                    <trirand:JQGrid runat="server" ID="grdProducts" Height="250px" AutoWidth="true" PagerSettings-PageSize="50"
                        OnDataRequesting="grdProducts_DataRequesting" OnRowSelecting="grdProducts_RowSelecting"
                        OnCellBinding="grdProducts_CellBinding">
                        <Columns>
                            <trirand:JQGridColumn DataField="ProductID" HeaderText="<%$ Resources:Resource, grdPoProductID %>"
                                PrimaryKey="true" />
                            <trirand:JQGridColumn DataField="Collection" HeaderText="<%$ Resources:Resource, lblCollection %>"
                                Editable="false" />
                            <trirand:JQGridColumn DataField="Style" HeaderText="<%$ Resources:Resource, lblStyle %>"
                                Editable="false" />
                            <trirand:JQGridColumn DataField="prdUPCCode" HeaderText="<%$ Resources:Resource, grdPOUPCCode %>"
                                Editable="false" />
                            <trirand:JQGridColumn DataField="prdName" HeaderText="<%$ Resources:Resource, grdPOProductName %>" />
                            <trirand:JQGridColumn DataField="Color" HeaderText="<%$ Resources:Resource, lblColor %>"
                                Editable="false" />
                            <trirand:JQGridColumn DataField="Size" HeaderText="<%$ Resources:Resource, lblSize %>"
                                Editable="false" />
                            <%--<trirand:JQGridColumn DataField="prdOhdQty" HeaderText="<%$ Resources:Resource, grdvOnHandQty %>" />--%>
                            <trirand:JQGridColumn DataField="Price" HeaderText="<%$ Resources:Resource, grdPOPrdCostPrice %>">
                                <%-- <Formatter>
                                    <trirand:CurrencyFormatter DecimalPlaces="2" DecimalSeparator="." DefaultValue="00"
                                        Prefix="$" ThousandsSeparator="," />
                                </Formatter>--%>
                            </trirand:JQGridColumn>
                            <%--<trirand:JQGridColumn DataField="prdTaxCode" HeaderText="Tax Group" />
                            <trirand:JQGridColumn DataField="prdDiscount" HeaderText="<%$ Resources:Resource, grdSaleDiscount %>" />--%>
                        </Columns>
                        <PagerSettings PageSize="20" PageSizeOptions="[20,50,100]" />
                        <ToolBarSettings ShowEditButton="false" ShowRefreshButton="True" ShowAddButton="false"
                            ShowDeleteButton="false" ShowSearchButton="false" />
                        <SortSettings InitialSortColumn="" />
                        <AppearanceSettings AlternateRowBackground="True" />
                        <ClientSideEvents BeforePageChange="beforePageChange" ColumnSort="columnSort" LoadComplete="gridLoadComplete" />
                    </trirand:JQGrid>
                    <asp:SqlDataSource ID="sqldsProcess" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
                        ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
                    <br />
                </div>
            </asp:Panel>
            <%--Edit Selected Item--%>
            <asp:Panel ID="pnlEditProductQuantity" runat="server" Visible="false">
                <h3>
                    <%= Resources.Resource.lblEditProductQuantity%>
                </h3>
                <ul class="form">
                    <li id="liBarCode" runat="server">
                        <div class="lbl">
                            <asp:Label ID="lblPOProductID1" runat="server" Text="<%$ Resources:Resource, POProductID1 %>"></asp:Label>
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtProductId" runat="server" Width="60px" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblUPCCode" runat="server" Text="<%$ Resources:Resource, lblPOUPCCode %>"></asp:Label>
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtUPCCode" runat="server" Width="160px" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <asp:Label ID="lblPOProductName1" runat="server" Text="<%$ Resources:Resource, POProductName1 %>"></asp:Label>
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtProductName" runat="server" Width="180px" TextMode="MultiLine"
                                Rows="4" ReadOnly="False"></asp:TextBox>
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblPOTaxGroup" runat="server" CssClass="lblBold" Text="Tax Group:"></asp:Label>
                        </div>
                        <div class="input">
                            <asp:DropDownList ID="dlTaxCode" runat="server" Width="130px">
                            </asp:DropDownList>
                        </div>
                        <div class="clearBoth">
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblPOWarehouse" runat="server" Text="<%$ Resources:Resource, lblPOWarehouse%>"></asp:Label>
                        </div>
                        <div class="input">
                            <asp:DropDownList ID="dlWarehouse" runat="server" Width="180px" ValidationGroup="lstSrch">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvWareHouse" SetFocusOnError="true" InitialValue="0"
                                ErrorMessage="<%$ Resources:Resource, custvalPOWarehouse%>" ControlToValidate="dlWarehouse"
                                runat="server" ValidationGroup="lstSrch" Display="Dynamic" />
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li id="liDiscount" runat="server">
                        <div class="lbl">
                            <asp:Label ID="lblPODiscount" runat="server" Text="Discount:"></asp:Label>
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtDiscount" runat="server" Width="50px" ValidationGroup="lstSrch"
                                MaxLength="10"></asp:TextBox>
                            <asp:RadioButtonList ID="rblstDiscountType" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="P" Text="<%$ Resources:Resource, lblPrdBlanketDisPercentage %>"
                                    Selected="True" />
                                <asp:ListItem Value="A" Text="<%$ Resources:Resource, lblPrdBlanketDisAbsolute %>" />
                            </asp:RadioButtonList>
                        </div>
                        <div class="lbl">
                            <asp:Label ID="lblQuantity" runat="server" Text="<%$ Resources:Resource, lblQty %>"></asp:Label>
                        </div>
                        <div class="input">
                            <asp:TextBox ID="txtQuantity" runat="server" Width="40px" ValidationGroup="lstSrch"
                                MaxLength="4"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqvalQuantity" runat="server" ValidationGroup="lstSrch"
                                Display="Dynamic" ErrorMessage="<%$ Resources:Resource, msgPOItemQuantity%>"
                                SetFocusOnError="true" ControlToValidate="txtQuantity"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="comvalPOQty" ValidationGroup="lstSrch" EnableClientScript="true"
                                SetFocusOnError="true" ControlToValidate="txtQuantity" Operator="DataTypeCheck"
                                Type="Double" Display="Dynamic" ErrorMessage="<%$ Resources:Resource, msgPOItemQtyNumCheck %>"
                                runat="server" />
                            <asp:RangeValidator ID="rngvalQuantity" runat="server" Display="Dynamic" ErrorMessage="<%$ Resources:Resource, msgPOItemQtyGT0%>"
                                SetFocusOnError="true" ValidationGroup="lstSrch" ControlToValidate="txtQuantity"
                                CultureInvariantValues="true" Type="Double" MinimumValue="0.001" MaximumValue="100000"></asp:RangeValidator>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                    <li id="liPrice" runat="server">
                        <div class="lbl">
                            <asp:Label ID="lblPrice" runat="server" Text="<%$ Resources:Resource, grdPOPrdCostPrice %>"></asp:Label>
                        </div>
                        <div class="input">
                            <%=sCurrencyBaseCode %>&nbsp;<asp:TextBox ID="txtPrice" runat="server" Width="50px"
                                ValidationGroup="lstSrch" MaxLength="10"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqvalPrice" runat="server" ValidationGroup="lstSrch"
                                Display="Dynamic" ErrorMessage="<%$ Resources:Resource, msgPOItemPrice%>" SetFocusOnError="true"
                                ControlToValidate="txtPrice"></asp:RequiredFieldValidator>
                            <%--<asp:CompareValidator ID="comvalPOPrice" ValidationGroup="lstSrch" EnableClientScript="true"
                                SetFocusOnError="true" ControlToValidate="txtPrice" Operator="DataTypeCheck"
                                Minimumvalue="1" Type="double" Display="Dynamic" ErrorMessage="<%$ Resources:Resource, msgPOItemPriceNumCheck %>"
                                runat="server" />
                            <asp:RangeValidator ID="rngvalPrice" runat="server" Display="Dynamic" ErrorMessage="<%$ Resources:Resource, msgPOItemPriceGT0%>"
                                SetFocusOnError="true" ValidationGroup="lstSrch" ControlToValidate="txtPrice"
                                CultureInvariantValues="true" Type="double" MinimumValue="0.00" MaximumValue="1000000000"></asp:RangeValidator>--%>
                        </div>
                        <div class="clearBoth">
                        </div>
                    </li>
                </ul>
                <%=sPHtml%>
                <h2>
                </h2>
                <div align="right" style="margin-top: 5px;">
                    <asp:Button ID="imgAdd" runat="server" ValidationGroup="lstSrch" Text="<%$ Resources:Resource, lblPrdAddProduct%>"
                        OnClick="imgAdd_Click" />
                    <input id="btnAddProduct" runat="server" visible="false" type="button" onclick="GetProductQty();"
                        value="<%$ Resources:Resource, lblPrdAddProduct%>" />
                    <asp:Button ID="btnBackToList" Text="<%$ Resources:Resource, lblBackToList%>" runat="server"
                        OnClick="btnBackToList_Click" />
                </div>
                <script type="text/javascript">
                    function pageLoad() {
                        //alert("Test");
                        $("#<%=txtQuantity.ClientID%>").focus(function () {
                            $(this).select();
                        }).focus();
                    }
                </script>
                <asp:HiddenField ID="hdnOnHeadQty" runat="server" />
            </asp:Panel>
        </asp:Panel>
    </div>
    <asp:SqlDataSource ID="sdsProductDetails" runat="server" ConnectionString="<%$ ConnectionStrings:NewConnectionString %>"
        ProviderName="MySql.Data.MySqlClient" SelectCommand=""></asp:SqlDataSource>
    <asp:HiddenField ID="hdnProcessProductID" runat="server" />
    <asp:HiddenField ID="hdnVendorID" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphScripts" runat="Server">
    <%--<script type="text/javascript">
        $(document).ready(function () {
            swapValues = [];
            $(".wm").each(function (i) {
                swapValues[i] = $(this).val();
                $(this).focus(function () {
                    if ($(this).val() == swapValues[i]) {
                        $(this).val("").removeClass("watermark")
                    }
                }).blur(function () {
                    if ($.trim($(this).val()) == "") { $(this).val(swapValues[i]).addClass("watermark") } 
                })
            })
        }); 
    </script>--%>
    <script type="text/javascript">
        //Variables
        var gridID = "<%=grdProducts.ClientID %>";
        var searchPanelID = "<%=pnlSearch.ClientID %>";
        var $grid = $("#<%=grdProducts.ClientID %>");

        function resize_the_grid() {
            $grid.fluidGrid({ example: '#grid_wrapper', offset: -0 });
        }

        //Client side function before page change.
        function beforePageChange(pgButton) {
            $grid.onJqGridPaging();
        }


        //Client side function on sorting
        function columnSort(index, iCol, sortorder) {
            $grid.onJqGridSorting();
        }

        //Call Grid Resizer function to resize grid on page load.
        resize_the_grid();

        //Bind window.resize event so that grid can be resized on windo get resized.
        $(window).resize(resize_the_grid);

        //Initialize the search panel to perform client side search.
        $('#' + searchPanelID).initJqGridCustomSearch({ jqGridID: gridID, toBeRemoveParams: '_alphaArgument' });

        function gridLoadComplete(data) {
            $(window).trigger("resize");
            if (data.rows.length > 0) {
                var v = data.rows[0].cell;
                if (v != "undefined") {

                    var sSearch = $("#<%=dlSearch.ClientID %> :selected").val();

                    if (sSearch == "ST") {
                        var sStyle = (v[2]);
                        var btnText = $("#<%=btnSelectedStyle.ClientID%>").val();
                        $("#<%=btnSelectedStyle.ClientID%>").val("<%=Resources.Resource.lblSelectStyle%>" + " " + sStyle);
                        $("#<%=hdnStyle.ClientID%>").val(sStyle);
                        $("#<%=hdnPname.ClientID%>").val(v[4]);
                    }
                }
            }
            $("#btnSearch").removeClass("ui-state-focus");
            $("#<%=txtSearch.ClientID%>").focus();
        }


        $("#<%=ddlCollection.ClientID%>").live('change', function (e) {
            var sText = $("#<%=ddlCollection.ClientID %> :selected").text();
            var sCollectionID = $("#<%=ddlCollection.ClientID %> :selected").val();
            GridPost();
        });

        $("#<%=dlSearch.ClientID%>").live('change', function (e) {
            var sSrchText = $("#<%=dlSearch.ClientID %> :selected").text();
            var sSrchValue = $("#<%=dlSearch.ClientID %> :selected").val();
            var vdialogID = $.FrameDialog._getUid();
            if (sSrchValue == "QS") {
                window.parent.$("#" + vdialogID).dialog({ height: 150,  });
                window.parent.$("#" + vdialogID).dialog({  position: 'top' });
                $("#trCollection").css("display", "none");
                $("#grid_wrapper").css("display", "none");
                return;
            }
            else {
                window.parent.$("#" + vdialogID).dialog({ height: 460 });
                window.parent.$("#" + vdialogID).dialog({ position: 'top' });
                window.parent.$("#" + vdialogID).css("height","460px");
                window.parent.$("#" + vdialogID+"-VIEW").css("height","460px");
                return;
            }
        });



        function GridPost() {
            var sCollectionID = $("#<%=ddlCollection.ClientID %> :selected").val();
            var filterArr = {};
            filterArr["_Collection"] = sCollectionID;
            $grid.appendPostData(filterArr);
            //            $grid.trigger("reloadGrid", [{ page: 1}]);
            $grid.trigger("reloadGrid");
            resize_the_grid();
            //           
            return false;
        }

        var btnSearchClick = '';

        $("#btnSearch").live('click', function (e) {

            var sSearchText = $("#<%=txtSearch.ClientID%>").val();
            var sSearch = $("#<%=dlSearch.ClientID %> :selected").val();

            if (sSearch == "ST") {
                $("#grid_wrapper").css("display", "block");
                $("#trCollection").css("display", "");
                $.ajax({
                    type: "POST",
                    url: "mdSearchProduct.aspx/GetStyleCollection",
                    dataType: "json",
                    data: "{ style: '" + sSearchText + "'}",
                    contentType: "application/json;",
                    success: function (data) {
                        var addressList = data.d;
                        $('option', $("#<%=ddlCollection.ClientID%>")).remove();
                        for (var i = 0; i < addressList.length; i++) {
                            $("#<%=ddlCollection.ClientID%>").append($("<option />").val(addressList[i].CollectionID).text(addressList[i].ShortName));
                        }
                        $("#<%=ddlCollection.ClientID%>").trigger("change");
                        GridPost();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    }
                });
            }
            else if (sSearch == "QS") {
                $("#<%=btnQuickSearch.ClientID%>").trigger("click");
            }
            else {
                $("#grid_wrapper").css("display", "block");
                $("#trCollection").css("display", "none");

                $("#<%=txtSearch.ClientID%>").val("");
                $("#<%=txtSearch.ClientID%>").focus();
            }
        });


        //        //Alphabat Search
        //        $(".alpha_search").click(function () {
        //            var alphaFilter = {};
        //            alphaFilter["_history"] = 0;
        //            alphaFilter["_alphaArgument"] = $(this).attr("search-data");
        //            $grid.appendPostData(alphaFilter);
        //            $grid.trigger("reloadGrid", [{ page: 1}]);
        //        });

        function GetProductQty() {
            var vDiscountType = ($('input[name=discountType]:radio:checked').val());
            var sTaxGroup = $("#<%=dlTaxCode.ClientID %> :selected").val();
            var sWareHouseCode = $("#<%=dlWarehouse.ClientID %> :selected").val();
            var sTaxDesc = $("#<%=dlTaxCode.ClientID %> :selected").text();

            var JSONObject = new Array();
            $('.cssPqty').each(function () {
                var obj = new Object();
                var ppID = $(this).attr("ppid");
                obj.PID = $(this).attr("pid");
                obj.PQTY = $(this).val();
                obj.PDisc = $("#Discount_" + ppID).val();
                obj.PPrice = $("#price_" + ppID).val();
                obj.PDType = vDiscountType;
                obj.PName = $(this).attr("pname");
                obj.PBarcode = $(this).attr("pbarcode");
                obj.PTaxGroup = sTaxGroup;
                obj.PTaxDesc = sTaxDesc;
                obj.PWareHouseCode = sWareHouseCode;

                JSONObject.push(obj);
            });

            $.ajax({
                type: "POST",
                url: "mdSearchProduct.aspx/SaveProductDetails",
                dataType: "json",
                data: JSON.stringify({ ProductDetail: JSONObject }),
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.d == "success") {
                        parent.reloadGrid();
                        jQuery.FrameDialog.closeDialog();
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }

        function CalcColorTotalQty(elementID) {
            var vTotQty = Number("0");
            var vSizeTotQty = Number("0");
            var vColorToCalcTotal = $("#" + elementID.id).attr("color");
            var vSizeToCalcTotal = $("#" + elementID.id).attr("sizeS");
            var vToSaveTotal = $("#" + elementID.id).attr("totalToSave");
            var vToSizeSaveTotal = $("#" + elementID.id).attr("sizetotalToSave");
            $('.' + vColorToCalcTotal).each(function () {
                vTotQty = vTotQty + Number($(this).val());
            });
            $("#" + vToSaveTotal).html(vTotQty);

            $('.' + vSizeToCalcTotal).each(function () {
                vSizeTotQty = vSizeTotQty + Number($(this).val());
            });
            $("#" + vToSizeSaveTotal).html(vSizeTotQty);

            var vGrandTotalQty = Number("0");
            $('.colorTotal').each(function () {
                vGrandTotalQty = vGrandTotalQty + Number($(this).html());
            });
            $("#grandtotal").html(vGrandTotalQty);
        }


    </script>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;


public partial class Sales_ViewOrderDetails : BasePage
{
    Partners _part = new Partners();
    SysCompanyInfo _comInfo = new SysCompanyInfo();
    Orders _ord = new Orders();
    Addresses _billToAddress = new Addresses();
    Addresses _shipToAddress = new Addresses();
    string ordCurrencyCode = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPagePostBack(grdProducts, grdProcess))
        {
            if (!CurrentUser.IsInRole(RoleID.ADMINISTRATOR) && !CurrentUser.IsInRole(RoleID.SALES_MANAGER) && !CurrentUser.IsInRole(RoleID.SALES_REPRESENTATIVE))
            {
                Response.Redirect("~/Errors/AccessDenied.aspx");
            }

            //Set confirmation for Cancel Reservation Button            
            //btnCancelReservation.OnClientClick = "askReasonToCancel(); return false;";
            //lblCancelReservationPop.OnClientClick = string.Format(@"return confirm(""{0}"");", Resources.Resource.msgAreyouSureYouWantToCancelWholeReservation);

            SalesCartHelper.Dispose();
            InitializeOrderDetails();
            CustomFields1.Initialize(this.OrderID, CustomFieldApplicableSection.SalesOrderAddEdit);
        }

        if (CurrentUser.IsInRole(RoleID.INVENTORY_READ_ONLY))
        {
            lblGP.Visible = false;
            txtGP.Visible = false;
            lblGPPercentage.Visible = false;
            txtGPPercentage.Visible = false;
            tdGPPct.Visible = false;
            tdGP.Visible = false;
        }
    }

    protected void page_init(object sender, EventArgs e)
    {
        Orders ord = new Orders();
        ord.PopulateObject(this.OrderID);
        if (ord.OrderTypeCommission == (int)OrderCommission.Transfer)
        {
            hlReturnHistory.Text = Resources.Resource.lblReturnTransfer;
        }
        else
        {
            hlReturnHistory.Text = Resources.Resource.lblReturnHistory;
        }
        if (ord.ChkISParentID(ord.OrdID))
        {
            hlReturnHistory.Text = Resources.Resource.lblShipmentHistory;
            hlReturnHistory.NavigateUrl = string.Format("~/Shipping/OrderShipmentHistory.aspx?ordid={0}", this.OrderID);
        }

        hlShippingValidationHistory.Text = Resources.Resource.lblShippingValidationHistory;
        hlShippingValidationHistory.NavigateUrl = string.Format("~/Shipping/ValidateSKU.aspx?vOid={0}&validationfor={1}", this.OrderID, "shipping");

        if (ord != null)
        {
            ordCurrencyCode = ord.OrdCurrencyCode;
        }
        if (BusinessUtility.GetString(ordCurrencyCode) == "")
        {
            Partners objPartnr = new Partners();
            //objPartnr.PopulateObject(dbHelp, this.CustomerID);
            objPartnr.PopulateObject(this.CustomerID);
            ordCurrencyCode = objPartnr.PartnerCurrencyCode;
        }

    }

    private void ShowHideSections(DbHelper dbHelp, int invoiceID)
    {
        //Functionality Level
        btnCancelReservation.Visible = false; //By default se false        
        ulSoDetails.Visible = this.OrderID > 0; //If So > 0     
        //btnPrihntAssembly.Visible = this.OrderID > 0;
        btnPrintPackageList.Visible = false;//this.OrderID > 0;
        divShpCode.Visible = this.OrderID > 0;
        spnFax.Visible = this.OrderID > 0;
        spnMail.Visible = this.OrderID > 0;
        spnPrint.Visible = this.OrderID > 0;
        //lbxSalesRepresentativs.Enabled = CurrentUser.IsInRole(RoleID.QUOTATION_APPROVER);
        txtOrderDate.Visible = CurrentUser.IsInRole(RoleID.QUOTATION_APPROVER);
        valOrderDate.Visible = !txtOrderDate.Visible;
        if (invoiceID > 0)
        {
            hlReturnHistory.Visible = this.OrderID > 0 && new InvoiceReturn().HasReturn(dbHelp, invoiceID);
        }
        else
        {
            hlReturnHistory.Visible = this.OrderID > 0 && new OrderReturn().HasReturn(dbHelp, this.OrderID);
        }
        if (_ord.ChkISParentID(_ord.OrdID))
        {
            hlReturnHistory.NavigateUrl = string.Format("~/Shipping/OrderShipmentHistory.aspx?ordid={0}", this.OrderID);
        }

        btnSaveAndGoToCal.Visible = _ord.OrderTypeCommission == (int)OrderCommission.Reservation;

        //if (_ord.OrderTypeCommission == (int)OrderCommission.Transfer)
        //{
        //    btnPlanShipment.Visible = false;
        //}

        //If Order Type IS Reservation then Hide AddEdit Columns & Add Reservation Cancel Option to individual Item
        //Prevent from Addimg & editing Product 
        if (_ord.OrderTypeCommission == (int)OrderCommission.Reservation)
        {
            btnCancelReservation.Visible = this.OrderID > 0 && _ord.OrdStatus != SOStatus.RESERVATION_CANCELED;

            grdProducts.Columns[12].Visible = false; //Product Delete Link
            grdProducts.Columns[14].Visible = true; //Enable Edit Reservation Link
            grdProducts.Columns[2].Visible = true; //Enable Guest Column
            grdProducts.Columns[15].Visible = true; //Enable to move reservation date
        }



        #region DisableItems_and_Remove_Items_From_OrderStatusDropDown
        if (_ord.OrderTypeCommission == (int)OrderCommission.Transfer)
        {
            for (int i = dlStatus.Items.Count - 1; i >= 0; i--)
            {
                string sValue = dlStatus.Items[i].Value;
                if (sValue == "A" || sValue == "P" || sValue == "H" || sValue == "K")
                {
                    dlStatus.Items.RemoveAt(i);
                }
                else if ((sValue == "S") || (sValue == "T"))
                {

                    if (_ord.OrdStatus != SOStatus.TRANSFERRED && _ord.OrdStatus != SOStatus.SHIPPED)
                    {
                        dlStatus.Items.FindByValue(sValue).Attributes.Add("Disabled", "Disabled");
                    }
                }


                if (sValue == "S")
                {
                    dlStatus.Items.RemoveAt(i);
                }

                if (sValue == "C")
                {
                    //dlStatus.Items.FindByValue(sValue).Text = Resources.Resource.lblTransferClosed;
                }
                else if (sValue == "T")
                {
                    dlStatus.Items.FindByValue(sValue).Text = Resources.Resource.lblInTransit;
                }


                if ((sValue == "C"))
                {
                    if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
                    {
                    }
                    else
                    {
                        if (_ord.OrdStatus != SOStatus.CLOSED)
                            dlStatus.Items.FindByValue(sValue).Attributes.Add("Disabled", "Disabled");
                    }
                }
            }
        }
        else if (_ord.OrderTypeCommission == (int)OrderCommission.Work)
        {
            for (int i = dlStatus.Items.Count - 1; i >= 0; i--)
            {
                string sValue = dlStatus.Items[i].Value;
                if (sValue == "A" || sValue == "H" || sValue == "K")
                {
                    dlStatus.Items.RemoveAt(i);
                }
                else if ((sValue == "S") || (sValue == "T"))
                {

                    if (_ord.OrdStatus != SOStatus.TRANSFERRED && _ord.OrdStatus != SOStatus.SHIPPED)
                    {
                        dlStatus.Items.FindByValue(sValue).Attributes.Add("Disabled", "Disabled");
                    }
                }


                if (sValue == "S")
                {
                    dlStatus.Items.RemoveAt(i);
                }

                if (sValue == "C")
                {
                    //dlStatus.Items.FindByValue(sValue).Text = Resources.Resource.lblTransferClosed;
                }
                else if (sValue == "T")
                {
                    dlStatus.Items.FindByValue(sValue).Text = Resources.Resource.lblInTransit;
                }


                if ((sValue == "C"))
                {
                    if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
                    {
                    }
                    else
                    {
                        if (_ord.OrdStatus != SOStatus.CLOSED)
                            dlStatus.Items.FindByValue(sValue).Attributes.Add("Disabled", "Disabled");
                    }
                }
            }
        }
        else
        {
            for (int i = dlStatus.Items.Count - 1; i >= 0; i--)
            {
                string sValue = dlStatus.Items[i].Value;
                if (sValue == "T" || sValue == "P" || sValue == "H" || sValue == "K")
                {
                    //if ((BusinessUtility.GetInt(this.orderType) == (int)OrderCommission.Work) && (sValue != "P"))
                        dlStatus.Items.RemoveAt(i);
                }
                else if ((sValue == "S"))
                {
                    if (_ord.OrdStatus != SOStatus.SHIPPED)
                        dlStatus.Items.FindByValue(sValue).Attributes.Add("Disabled", "Disabled");
                }
                if ((sValue == "C"))
                {
                    if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
                    {
                    }
                    else
                    {
                        if (_ord.OrdStatus != SOStatus.CLOSED)
                            dlStatus.Items.FindByValue(sValue).Attributes.Add("Disabled", "Disabled");
                    }
                }
            }
        }
        #endregion

        //btnPrintPackageList.Visible = _ord.OrderTypeCommission == (int)OrderCommission.Gift || _ord.OrderTypeCommission == (int)OrderCommission.Sales || _ord.OrderTypeCommission == (int)OrderCommission.Transfer;
        btnDuplicateOrder.Visible = _ord.OrderTypeCommission == (int)OrderCommission.Gift || _ord.OrderTypeCommission == (int)OrderCommission.Sales || _ord.OrderTypeCommission == (int)OrderCommission.Transfer;


        //Show Hide Section based on app settings
        if (ConfigurationManager.AppSettings["AllowCustomInvoiceEntry"].ToLower() == "true")
        {
            liCustomInvoice.Visible = true;
        }
        else
        {
            liCustomInvoice.Visible = false;
        }

        // Show Hide ShippingValidation History
        ShippingReturnValidation objShpValHistory = new ShippingReturnValidation();
        DataTable dtShpValHistory = objShpValHistory.GetShippingHistory(this.OrderID, Globals.CurrentAppLanguageCode, "shipping");
        if (dtShpValHistory != null)
        {
            if (dtShpValHistory.Rows.Count > 0)
            {
                hlShippingValidationHistory.Visible = true;
            }
            else
            {
                hlShippingValidationHistory.Visible = false;
            }
        }
        else
        {
            hlShippingValidationHistory.Visible = false;
        }

        if (invoiceID > 0)
        {
            ////Prevent from Addimg & editing Product & Process
            //grdProducts.Columns[13].Visible = false; //Cancel Item (For reservation item)
            //grdProducts.Columns[11].Visible = false; //Edit Product Link
            //grdProducts.Columns[12].Visible = false; //Delete Product Link

            //grdProducts.Columns[14].Visible = false; //Edit Reservation Link
            //grdProducts.Columns[15].Visible = false; //Move Reservation

            //grdProcess.Columns[10].Visible = false; //Process Item Edit
            //grdProcess.Columns[11].Visible = false; //Process Item Delete


            ////Hide receiving paument & refund feature on order just do it from actual account recivable
            //popReceivePayment.Visible = false;
            //popRefundPayment.Visible = false;

            //btnCancelReservation.Visible = false;

            //Prevent from Addimg & editing Product & Process
            grdProducts.Columns[15].Visible = false; //Cancel Item (For reservation item)
            grdProducts.Columns[13].Visible = false; //Edit Product Link
            grdProducts.Columns[14].Visible = false; //Delete Product Link

            grdProducts.Columns[16].Visible = false; //Edit Reservation Link
            grdProducts.Columns[17].Visible = false; //Move Reservation

            grdProcess.Columns[10].Visible = false; //Process Item Edit
            grdProcess.Columns[11].Visible = false; //Process Item Delete

            //Hide PlanShipment,receiving paument & refund feature on order just do it from actual account recivable
            popPlanShipment.Visible = true;
            popReceivePayment.Visible = false;
            popRefundPayment.Visible = false;

            btnCancelReservation.Visible = false;

        }

        divAddProduct.Visible = invoiceID <= 0 && _ord.OrdStatus != SOStatus.RESERVATION_CANCELED;

        //Security Level 
        //Allow User to update order status
        dlStatus.Enabled = dlStatus.Enabled && (CurrentUser.IsInRole(RoleID.ADMINISTRATOR) || CurrentUser.IsInRole(RoleID.QUOTATION_APPROVER));
        TotalSummary lTotal = CalculationHelper.GetOrderTotal(dbHelp, this.OrderID);
        if (this.OrderID > 0)
        {
            btnGenerateInvoice.Visible = this.OrderID > 0 && (CurrentUser.IsInRole(RoleID.INVOICE_GENERATION) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR));
            popPlanShipment.Visible = this.OrderID > 0 && (CurrentUser.IsInRole(RoleID.INVOICE_GENERATION) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR));
            popReceivePayment.Visible = this.OrderID > 0 && (CurrentUser.IsInRole(RoleID.INVOICE_GENERATION) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR)) && lTotal.OutstandingAmount > 0;
            popRefundPayment.Visible = this.OrderID > 0 && (CurrentUser.IsInRole(RoleID.INVOICE_GENERATION) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR)) && lTotal.TotalAmountReceived > 0;
            int parentinvID = new Invoice().GetInvoiceID(dbHelp, _ord.ParentOrdID, InvoiceReferenceType.INVOICE);
            if (parentinvID > 0)
            {
                btnGenerateInvoice.Visible = false;
                popPlanShipment.Visible = false;
                popReceivePayment.Visible = false;
                popRefundPayment.Visible = false;
            }

            if (_ord.ParentOrdID > 0)
            {
                popPlanShipment.Visible = false;
            }
            if (btnGenerateInvoice.Visible && invoiceID > 0)
            {
                btnGenerateInvoice.Visible = false;
                hlInvoice.Visible = true;
                hlInvoice.Text = btnGenerateInvoice.Text;
                hlInvoice.NavigateUrl = string.Format("~/invoice/ViewInvoiceDetails.aspx?SOID={0}&ComID={1}", btnGenerateInvoice.Text, hdnCompanyID.Value);
                if (_ord.ChkISParentID(_ord.OrdID))
                {
                    hlReturnHistory.Visible = true;
                    popPlanShipment.Visible = true;
                    Session["SHIPMENT_CART"] = SalesCartHelper.CurrentCart.Items;
                }
            }
            else
            {
                if (_ord.ChkISParentID(_ord.OrdID))
                {
                    hlReturnHistory.Visible = true;
                    btnGenerateInvoice.Visible = false;
                }
                Session["SHIPMENT_CART"] = SalesCartHelper.CurrentCart.Items;
            }

            if (_ord.OrderTypeCommission == (int)OrderCommission.Transfer)
            {
                btnGenerateInvoice.Visible = false;
            }

        }
        else
        {
            btnGenerateInvoice.Visible = false;
            popPlanShipment.Visible = false;
            popReceivePayment.Visible = false;
            popRefundPayment.Visible = false;
        }

        //Check Return section only need to return when order status is shipped
        //if (_ord.OrdStatus == SOStatus.SHIPPED && Request.QueryString.AllKeys.Contains("isreturn") && Request.QueryString["isreturn"] == "1")


        if ((_ord.OrdStatus == SOStatus.SHIPPED || _ord.OrdStatus == SOStatus.TRANSFERRED) && Request.QueryString.AllKeys.Contains("isreturn") && Request.QueryString["isreturn"] == "1")
        {
            divReturns.Visible = true;
            divTransfers.Visible = true;
            btnCancel.Visible = false;
            btnDuplicateOrder.Visible = false;
            btnSave.Visible = false;
            btnSaveAndGoToCal.Visible = false;
            popPlanShipment.Visible = false;

            //Check condition for full return if not paid then just need to perform full return so enable link button
            //lnkFullReturn.Visible = lTotal.TotalAmountReceived <= 0;
            hlFullReturn.Visible = true;//!lnkFullReturn.Visible;

            hlBackUrl.NavigateUrl = Request.QueryString["returnUrl"];
            hlBackUrlFullReturn.NavigateUrl = Request.QueryString["returnUrl"];

            divAddProduct.Visible = false;
            divNavigation.Visible = false;
        }
        else
        {
            divReturns.Visible = false;
            divTransfers.Visible = false;
        }

        if (BusinessUtility.GetString(_ord.OrderTypeCommission) == "2")
        {
            divReturns.Visible = false;
        }
        else
        {
            divTransfers.Visible = false;
        }


        //Prevent from Addimg & editing Product & Process if Order Status Not = New

        if (BusinessUtility.GetString(_ord.OrdStatus) == "")
        {
            _ord.OrdStatus = SOStatus.NEW;
        }

        if (BusinessUtility.GetString(_ord.OrdStatus) != SOStatus.NEW)
        {
            //Prevent from Addimg & editing Product & Process
            grdProducts.Columns[15].Visible = false; //Cancel Item (For reservation item)
            grdProducts.Columns[13].Visible = false; //Edit Product Link
            grdProducts.Columns[14].Visible = false; //Delete Product Link

            grdProducts.Columns[16].Visible = false; //Edit Reservation Link
            grdProducts.Columns[17].Visible = false; //Move Reservation

            grdProcess.Columns[10].Visible = false; //Process Item Edit
            grdProcess.Columns[11].Visible = false; //Process Item Delete

            //Hide receiving paument & refund feature on order just do it from actual account recivable
            popReceivePayment.Visible = false;
            popRefundPayment.Visible = false;

            btnCancelReservation.Visible = false;

            divAddProduct.Visible = false;
        }

        if ((_ord.OrdStatus == SOStatus.SHIPPED || _ord.OrdStatus == SOStatus.TRANSFERRED || _ord.OrdStatus == SOStatus.CLOSED))
        {
            btnShipNow.Visible = false;
            btnPlanShipment.Visible = false;
        }

    }

    private void InitializeOrderDetails()
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            dbHelp.Connection.Open(); //Make sure to open connection first to optimize performance
            int invID = 0;
            int custID = this.CustomerID;

            //Add Checkout button confirmation box
            btnGenerateInvoice.OnClientClick = string.Format(@"return confirm(""{0}"");", Resources.Resource.msgConfirmCreateInvoice);
            btnDuplicateOrder.OnClientClick = string.Format(@"return confirm(""{0}"");", Resources.Resource.confirmDuplicateOrder);

            //Populate DDL
            _ord.FillOrderType(dbHelp, dlstOderType, Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.lblSelectOrderType, "0"));

            string sWhsCodeTobeSelected = CurrentUser.UserDefaultWarehouse;
            if (this.WarehouseCode != "")
            {
                sWhsCodeTobeSelected = this.WarehouseCode;
            }
            new SysWarehouses().FillWharehouse(dbHelp, dlShpWarehouse, sWhsCodeTobeSelected, this.CompanyID, new ListItem("", "0"));
            new SysWarehouses().FillWharehouse(dbHelp, dlShpToWarehouse, CurrentUser.UserDefaultWarehouse, this.CompanyID, new ListItem("", "0"));
            DropDownHelper.FillDropdown(dbHelp, dlStatus, "SO", "SOSts", Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.liSelectOrderStatus, "0"));
            //new InbizUser().FillSalesRepresentatives(dbHelp, lbxSalesRepresentativs, null, CurrentUser.UserDefaultWarehouse);
            new InbizUser().FillSalesRepresentatives(dbHelp, lbxSalesRepresentativs, null, "");
            new NetTerm().PopulateListControl(dbHelp, ddlNetTerms, new ListItem(""));

            if (CurrentUser.IsInRole(RoleID.ADMINISTRATOR) == false && CurrentUser.IsInRole(RoleID.QUOTATION_APPROVER) == false && CurrentUser.IsInRole(RoleID.SALES_MANAGER) == false)
            {
                dlStatus.SelectedValue = SOStatus.NEW;
                dlStatus.Enabled = false;
            }
            else
            {
                dlStatus.SelectedValue = SOStatus.APPROVED;
            }

            if (this.OrderID > 0)
            {

                _ord.PopulateObject(dbHelp, this.OrderID);

                //this.orderCurrencyCode = _ord.OrdCurrencyCode;

                TotalSummary tsOrder = CalculationHelper.GetOrderTotal(dbHelp, this.OrderID);
                _ord.FillSalesRepresentativesCommission(dbHelp, lbxSalesRepresentativs, this.OrderID);

                //ListItem liOrdType = dlstOderType.Items.FindByValue(_ord.OrderTypeCommission.ToString());
                //if (liOrdType != null)
                //{

                //}
                valOrderType.Text = _ord.OrdSaleWeb ? "Online" : CurrentUser.UserName;
                btnGenerateInvoice.Text = _ord.OrderTypeCommission == (int)OrderCommission.Reservation ? Resources.Resource.lblCheckout : Resources.Resource.btnGenrateInvoice;

                if (_ord.OrderTypeCommission == (int)OrderCommission.Shipment)
                {
                    _ord.FillOrderALLType(dbHelp, dlstOderType, Globals.CurrentAppLanguageCode, new ListItem(Resources.Resource.lblSelectOrderType, "0"));
                    dlstOderType.Enabled = false;
                }

                //Get Reservation checkin date
                ReservationItems ri = new Reservation().GetFirstReservationItem(dbHelp, this.OrderID);
                if (ri.ReservationItemID > 0)
                {
                    if (ri.CheckInDate.Date > DateTime.Today)
                    {
                        valReservationStatus.Text = string.Format("{0} day(s) to Check-in", ri.CheckInDate.Date.Subtract(DateTime.Today).Days);
                    }
                    else if (ri.CheckInDate.Date <= DateTime.Today && ri.CheckOutDate > DateTime.Today)
                    {
                        valReservationStatus.Text = string.Format("Checked-in {0:MMM dd, yyyy}", ri.CheckInDate);
                    }
                    else if (ri.CheckOutDate == DateTime.Today)
                    {
                        valReservationStatus.Text = string.Format("Checked-in {0:MMM dd, yyyy}", ri.CheckOutDate);
                    }
                    else if (ri.CheckOutDate.Date < DateTime.Today)
                    {
                        valReservationStatus.Text = string.Format("{0} day(s) past check-out", DateTime.Today.Subtract(ri.CheckOutDate.Date).Days);
                    }
                }
                else
                {
                    valReservationStatus.Text = "N/A";
                }

                //Populate Status Dropdown
                int iLenght = 0;
                switch (_ord.OrdStatus.ToUpper().Trim())
                {
                    case SOStatus.APPROVED:
                        iLenght = 1;
                        break;
                    case SOStatus.IN_PROGRESS:
                        iLenght = 2;
                        break;
                    case SOStatus.HELD:
                        iLenght = 3;
                        break;

                    case SOStatus.READY_FOR_SHIPPING:
                        iLenght = 4;
                        break;
                    case SOStatus.SHIPPED:
                        iLenght = 5;
                        break;
                    case SOStatus.TRANSFERRED:
                        iLenght = 6;
                        break;
                    case SOStatus.CLOSED:
                        iLenght = 7;
                        break;
                }
                if (dlStatus.Items.Count > iLenght)
                {
                    int iStart = 0;
                    while (iStart < iLenght)
                    {
                        dlStatus.Items.RemoveAt(1);
                        iStart++;
                    }
                }
                if (_ord.OrdStatus == SOStatus.SHIPPING_HELD)
                {
                    dlStatus.Items.Add(new ListItem(Resources.Resource.liShippingHeld, SOStatus.SHIPPING_HELD));
                    dlStatus.Enabled = false;
                }
                dlStatus.SelectedValue = _ord.OrdStatus;

                ListItem reservationCancel = dlStatus.Items.FindByValue(SOStatus.RESERVATION_CANCELED);
                if (reservationCancel != null && _ord.OrdStatus != SOStatus.RESERVATION_CANCELED)
                {
                    dlStatus.Items.Remove(reservationCancel);
                }
                else if (_ord.OrdStatus == SOStatus.RESERVATION_CANCELED)
                {
                    dlStatus.SelectedValue = _ord.OrdStatus;
                    dlStatus.Enabled = false;
                }

                txtBillToAddress.Text = ""; //TO DO
                txtCustPO.Text = _ord.OrdCustPO;
                txtInvDate.Text = BusinessUtility.GetDateTimeString(_ord.InvDate, DateFormat.MMddyyyy);
                txtInvNo.Text = _ord.InvRefNo.ToString();
                //txtNetTerms.Text = _ord.OrdNetTerms;
                ListItem li = ddlNetTerms.Items.FindByText(_ord.OrdNetTerms);
                if (li != null)
                {
                    li.Selected = true;
                }
                txtNotes.Text = _ord.OrdComment;
                txtQutExpDate.Text = BusinessUtility.GetDateTimeString(_ord.QutExpDate, DateFormat.MMddyyyy);
                txtShippingDate.Text = BusinessUtility.GetDateTimeString(_ord.OrdShpDate, DateFormat.MMddyyyy);
                txtShpToAddress.Text = ""; //TO DO
                txtShpTrackNo.Text = _ord.OrdShpTrackNo;
                txtTerms.Text = _ord.OrdShippingTerms;
                txtShpCode.Text = _ord.OrdShpCode;
                dlShpWarehouse.SelectedValue = _ord.OrdShpWhsCode;
                dlShpToWarehouse.SelectedValue = _ord.OrdShpToWhsCode;

                dlstOderType.SelectedValue = _ord.OrderTypeCommission.ToString();
                if (dlstOderType.SelectedValue == "2")
                {
                    dlstOderType.Enabled = false;
                    dlShpWarehouse.Enabled = false;
                    dlShpToWarehouse.Enabled = false;
                    hlReturnHistory.NavigateUrl = string.Format("~/Returns/mdReturnHistory.aspx?oid={0}&invid={1}&IsTransfer=1", this.OrderID, invID);
                }
                else
                {
                    hlReturnHistory.NavigateUrl = string.Format("~/Returns/mdReturnHistory.aspx?oid={0}&invid={1}", this.OrderID, invID);
                }

                rblstShipBlankPref.SelectedValue = _ord.OrdShpBlankPref ? "1" : "0";
                rdlstISWeb.SelectedValue = _ord.OrdSaleWeb ? "1" : "0";

                hdnCompanyID.Value = _ord.OrdCompanyID.ToString();
                hdnSalesAgentID.Value = _ord.OrdSalesRepID.ToString();
                hdnPartnerID.Value = _ord.OrdCustID.ToString();
                hdnParterTypeID.Value = Globals.GetPartnerTypeID(_ord.OrdCustType).ToString();
                hdnPartnerCurrencyCode.Value = _ord.OrdCurrencyCode;
                hdnWhsCode.Value = _ord.OrdShpWhsCode;
                hdnCurrrencyExchangeRate.Value = _ord.OrdCurrencyExRate.ToString();
                hdnPreviousStatus.Value = _ord.OrdStatus;

                //hlEditBillToAddress.NavigateUrl = string.Format("~/Common/mdAddressEdit.aspx?addrSourceID={0}&addrRef={1}&addrType={2}&ctrl={3}", hdnPartnerID.Value, _ord.OrdCustType, AddressType.BILL_TO_ADDRESS, txtBillToAddress.ClientID);
                //hlEditShippingAddress.NavigateUrl = string.Format("~/Common/mdAddressEdit.aspx?addrSourceID={0}&addrRef={1}&addrType={2}&ctrl={3}", hdnPartnerID.Value, _ord.OrdCustType, AddressType.SHIP_TO_ADDRESS, txtShpToAddress.ClientID);
                hlEditBillToAddress.NavigateUrl = string.Format("~/Partner/AddressCollection.aspx?addType={0}&custID={1}&pType={2}&src={3}&ctrl={4}", AddressType.BILL_TO_ADDRESS, hdnPartnerID.Value, _ord.OrdCustType, "SO", txtBillToAddress.ClientID);
                hlEditShippingAddress.NavigateUrl = string.Format("~/Partner/AddressCollection.aspx?addType={0}&custID={1}&pType={2}&src={3}&ctrl={4}", AddressType.SHIP_TO_ADDRESS, hdnPartnerID.Value, _ord.OrdCustType, "SO", txtShpToAddress.ClientID);

                //Populate Address
                //Addresses billtoAddress = _part.GetBillToAddress(dbHelp, _ord.OrdCustID, Globals.GetPartnerTypeID(_ord.OrdCustType));
                //Addresses shipToAddress = _part.GetShipToAddress(dbHelp, _ord.OrdCustID, Globals.GetPartnerTypeID(_ord.OrdCustType));

                Addresses billtoAddress = new Addresses();
                billtoAddress.GetOrderAddress(this.OrderID, AddressType.BILL_TO_ADDRESS);

                Addresses shipToAddress = new Addresses();
                shipToAddress.GetOrderAddress(this.OrderID, AddressType.SHIP_TO_ADDRESS);

                txtBillToAddress.Text = billtoAddress.ToHtml();
                txtShpToAddress.Text = shipToAddress.ToHtml();

                //Make Cart with Order Items
                SalesCartHelper.InitCart(dbHelp, this.OrderID);

                //If Invoice Created then Disable edit/delete option in grid
                invID = new Invoice().GetInvoiceID(dbHelp, this.OrderID, InvoiceReferenceType.INVOICE);
                hdnInvoiceNoForOrder.Value = invID.ToString();
                if (invID > 0)
                {
                    btnGenerateInvoice.Text = invID.ToString();
                }


                FillReadOnlyOrderDetails(dbHelp);
                lblName.Text = string.Format("{0}", _part.PartnerLongName);



                List<string> lstReceiptNo = new List<string>();
                valBalanceDue.Text = string.Format("{0:F}", BusinessUtility.GetCurrencyString(tsOrder.OutstandingAmount, Globals.CurrentCultureName));
                foreach (var item in tsOrder.AmountReceivedList)
                {
                    if (!string.IsNullOrEmpty(item.ReceiptNo))
                    {
                        lstReceiptNo.Add(item.ReceiptNo);
                    }
                }
                valReceiptNo.Text = string.Join(", ", lstReceiptNo.ToArray());

                //btnPrihntAssembly.OnClientClick = string.Format("return openPDF('{0}?SOID={1}&AsmL=T')", ResolveUrl("~/Shipping/print.aspx"), this.OrderID);
                btnPrintPackageList.OnClientClick = string.Format("return openPDF('{0}?ReqID={1}&DocTyp=PKG')", ResolveUrl("~/Common/print.aspx"), this.OrderID);
                string docType = _ord.OrdStatus == SOStatus.NEW ? "QO" : "SO";
                mdFax.Url = string.Format("~/Common/mdSendMailOrFax.aspx?DocTyp={0}&SOID={1}&cmd={2}", docType, this.OrderID, "F"); //F stands for Fax
                mdMail.Url = string.Format("~/Common/mdSendMailOrFax.aspx?DocTyp={0}&SOID={1}&cmd={2}", docType, this.OrderID, "M"); //M stands for Mail

                string printUrl = string.Format("../Common/Print.aspx?ReqID={0}&DocTyp={1}", this.OrderID, docType);
                imgPdf.Attributes.Add("onclick", string.Format("window.open('{0}', 'Print', 'height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no'); return false;", printUrl));

                dgReceivePayment.Url = string.Format("~/AccountsReceivable/mdReceive.aspx?oid={0}&rcvType={1}&invID={2}&rmvSession=true", this.OrderID, "receive", invID);
                dgRefund.Url = string.Format("~/AccountsReceivable/mdReceive.aspx?oid={0}&rcvType={1}&amtToRefund={2}&invID={3}&rmvSession=true", this.OrderID, "refund", "0.00", invID);

                hlFullReturn.NavigateUrl = string.Format("~/Returns/mdReturn.aspx?fullReturn=1&invid={0}&orderid={1}", invID, this.OrderID);
                hlPartialReturn.NavigateUrl = string.Format("~/Returns/mdReturn.aspx?invid={0}&orderid={1}", invID, this.OrderID);
                hlFullTransfer.NavigateUrl = string.Format("~/Returns/mdReturn.aspx?fullReturn=1&invid={0}&orderid={1}&IsTransfer=1", invID, this.OrderID);
                hlPartialTransfer.NavigateUrl = string.Format("~/Returns/mdReturn.aspx?invid={0}&orderid={1}&IsTransfer=1", invID, this.OrderID);

                hdnPartialTranferURl.Value = string.Format("../Returns/mdReturn.aspx?invid={0}&orderid={1}&IsTransfer=1", invID, this.OrderID);
                hdnPartialReturnURl.Value = string.Format("../Returns/mdReturn.aspx?invid={0}&orderid={1}&regcode={2}", invID, this.OrderID, this.POSRegCode);

                //Set selected Sales rep users
                //Retreive Sales Rep 
                List<int> lstRep = _ord.GetOrderSalesRepUsers(dbHelp, this.OrderID);
                foreach (ListItem item in lbxSalesRepresentativs.Items)
                {
                    if (lstRep.Contains(BusinessUtility.GetInt(item.Value)))
                    {
                        item.Selected = true;
                    }
                }

                string[] sShipingCompany = _ord.OrdShpCode.Split('-');

                if (sShipingCompany.Length > 0)
                {
                    SysShippingCompany objShippingCompny = new SysShippingCompany();
                    var sSHippingCompany = objShippingCompny.GetShippingCompany(null, 0, BusinessUtility.GetString(sShipingCompany[0]));
                    if (sSHippingCompany.Count > 0)
                    {
                        hypTrackLnk.NavigateUrl = (sSHippingCompany[0].ShipCompanyUrl + BusinessUtility.GetString(_ord.OrdShpCode)).Replace(BusinessUtility.GetString(sSHippingCompany[0].ShipCompany) + "-", "").Trim();
                    }
                }

                if (BusinessUtility.GetString(_ord.OrdClosedReason) != "")
                {
                    ddlReason.SelectedValue = _ord.OrdClosedReason;
                }
            }
            else
            {
                SalesCartHelper.InitLocalCart(dbHelp, this.WarehouseCode, this.CustomerID);

                if (this.orderType == 2)
                {
                    dlstOderType.SelectedValue = ((int)OrderCommission.Transfer).ToString();
                    dlstOderType.Enabled = false;
                    dlShpWarehouse.SelectedValue = this.WhsFromCode;
                    dlShpWarehouse.Enabled = false;
                    dlShpToWarehouse.SelectedValue = this.WhsToCode;
                    dlShpToWarehouse.Enabled = false;
                }
                else if (this.orderType == ((int)OrderCommission.Work))
                {
                    dlstOderType.SelectedValue = ((int)OrderCommission.Work).ToString();
                }
                else
                {
                    dlstOderType.SelectedValue = ((int)OrderCommission.Sales).ToString();
                }
                //Initialize all Date Inputs
                txtShippingDate.Text = BusinessUtility.GetDateTimeString(DateTime.Now, DateFormat.MMddyyyy);
                txtQutExpDate.Text = BusinessUtility.GetDateTimeString(DateTime.Now, DateFormat.MMddyyyy);

                //Set Customer Data
                _part.PopulateObject(dbHelp, this.CustomerID);
                int partTypeID = _part.GetPartnerTypeID(dbHelp, _part.PartnerID);

                hdnCompanyID.Value = this.CompanyID.ToString();
                hdnSalesAgentID.Value = CurrentUser.UserID.ToString();
                hdnPartnerID.Value = _part.PartnerID.ToString();
                hdnParterTypeID.Value = partTypeID.ToString();
                hdnPartnerCurrencyCode.Value = _part.PartnerCurrencyCode;
                hdnWhsCode.Value = this.WarehouseCode;

                SysCurrencies cur = new SysCurrencies();
                cur.PopulateObject(_part.PartnerCurrencyCode);



                //double exchangeRate = SysCurrencies.GetRelativePrice(dbHelp, hdnPartnerCurrencyCode.Value);
                double exchangeRate = BusinessUtility.GetDouble(cur.CurrencyRelativePrice);//SysCurrencies.GetRelativePrice(dbHelp, hdnPartnerCurrencyCode.Value);
                hdnCurrrencyExchangeRate.Value = exchangeRate.ToString();

                //hlEditBillToAddress.NavigateUrl = string.Format("~/Common/mdAddressEdit.aspx?addrSourceID={0}&addrRef={1}&addrType={2}&ctrl={3}", this.CustomerID, Globals.GetPartnerType(partTypeID), AddressType.BILL_TO_ADDRESS, txtBillToAddress.ClientID);
                //hlEditShippingAddress.NavigateUrl = string.Format("~/Common/mdAddressEdit.aspx?addrSourceID={0}&addrRef={1}&addrType={2}&ctrl={3}", this.CustomerID, Globals.GetPartnerType(partTypeID), AddressType.SHIP_TO_ADDRESS, txtShpToAddress.ClientID);
                hlEditBillToAddress.NavigateUrl = string.Format("~/Partner/AddressCollection.aspx?addType={0}&custID={1}&pType={2}&src={3}&ctrl={4}", AddressType.BILL_TO_ADDRESS, this.CustomerID, Globals.GetPartnerType(partTypeID), "SO", txtBillToAddress.ClientID);
                hlEditShippingAddress.NavigateUrl = string.Format("~/Partner/AddressCollection.aspx?addType={0}&custID={1}&pType={2}&src={3}&ctrl={4}", AddressType.SHIP_TO_ADDRESS, this.CustomerID, Globals.GetPartnerType(partTypeID), "SO", txtShpToAddress.ClientID);

                //Populate Address
                //Addresses billtoAddress = _part.GetBillToAddress(dbHelp, _part.PartnerID, partTypeID);
                //Addresses shipToAddress = _part.GetShipToAddress(dbHelp, _part.PartnerID, partTypeID);

                Addresses billtoAddress = new Addresses();
                billtoAddress.GetCustomerAddress(_part.PartnerID, AddressType.BILL_TO_ADDRESS);

                Addresses shipToAddress = new Addresses();
                shipToAddress.GetCustomerAddress(_part.PartnerID, AddressType.SHIP_TO_ADDRESS);

                txtBillToAddress.Text = billtoAddress.ToHtml();
                txtShpToAddress.Text = shipToAddress.ToHtml();

                //Populate compny Information
                _comInfo.PopulateObject(this.CompanyID, dbHelp);
                txtTerms.Text = _comInfo.CompanyShpToTerms;
                txtNotes.Text = string.Empty;

                lblName.Text = string.Format("{0}", _part.PartnerLongName);
                //txtNetTerms.Text = _part.PartnerInvoiceNetTerms;
                ListItem li = ddlNetTerms.Items.FindByText(_part.PartnerInvoiceNetTerms);
                if (li != null)
                {
                    li.Selected = true;
                }
            }

            //Order discount
            popAddDiscount.NavigateUrl = string.Format("mdAddDiscountToOrder.aspx?oid={0}", this.OrderID);
            //Build Popup Url to add Product & Process
            if (this.orderType == 2 || dlstOderType.SelectedValue == "2")
            {
                int otype = this.orderType;
                if (otype == 0)
                {
                    otype = 2;
                }

                popAddProduct.ToolTip = Resources.Resource.lblTransferAddProduct;
                lblTitle.Text = Resources.Resource.lblTransferCreate;
                lblQuotationItems.Text = Resources.Resource.lblTransferItems;
                popAddServices.ToolTip = Resources.Resource.lblTransferAddServices;
                popAddProcess.ToolTip = Resources.Resource.lblTransferAddProcess;
                dgPlanShipment.Title = Resources.Resource.lblTransferPlanShipment;
                dgShipNow.Title = Resources.Resource.lblTransferPlanShipment;
                hdnTitleUpdateShipment.Value = Resources.Resource.lblTransferUpdateShipment;

                hlviewSalesOrders.NavigateUrl += "?ordType=" + otype;
                popAddProduct.NavigateUrl = string.Format("mdSearchProduct.aspx?pType={0}&custID={1}&orderType={2}&whsID={3}", (int)StatusProductType.Product, hdnPartnerID.Value, otype, WhsFromCode);
                hdnAddProductURl.Value = string.Format("mdSearchProduct.aspx?pType={0}&custID={1}&orderType={2}&whsID={3}", (int)StatusProductType.Product, hdnPartnerID.Value, otype, WhsFromCode);
            }
            else
            {
                popAddProduct.ToolTip = Resources.Resource.lblOrderAddProduct;
                lblTitle.Text = Resources.Resource.lblOrderCreate;
                lblQuotationItems.Text = Resources.Resource.lblOrderItems;
                popAddServices.ToolTip = Resources.Resource.lblOrderAddServices;
                popAddProcess.ToolTip = Resources.Resource.lblOrderAddProcess;
                dgPlanShipment.Title = Resources.Resource.lblOrderPlanShipment;
                dgShipNow.Title = Resources.Resource.lblOrderPlanShipment;
                hdnTitleUpdateShipment.Value = Resources.Resource.lblOrderUpdateShipment;
                popAddProduct.NavigateUrl = string.Format("mdSearchProduct.aspx?pType={0}&custID={1}&whsID={2}", (int)StatusProductType.Product, hdnPartnerID.Value, this.WarehouseCode);
            }

            popAddChildren.NavigateUrl = string.Format("mdSearchProduct.aspx?pType={0}&custID={1}&whsID={2}", (int)StatusProductType.ChildUnder12, hdnPartnerID.Value, this.WarehouseCode);
            popAddProcess.NavigateUrl = string.Format("mdSalesProcess.aspx?exrate={0}&orderid={1}", hdnCurrrencyExchangeRate.Value, this.OrderID);
            popAddCourse.NavigateUrl = string.Format("mdSearchProduct.aspx?pType={0}&custID={1}&whsID={2}", (int)StatusProductType.CourseProduct, hdnPartnerID.Value, this.WarehouseCode);
            popAddServices.NavigateUrl = string.Format("mdSearchProduct.aspx?pType={0}&custID={1}&whsID={2}", (int)StatusProductType.ServiceProduct, hdnPartnerID.Value, this.WarehouseCode);
            //show hide button 
            this.ShowHideSections(dbHelp, invID);
        }
        catch (Exception ex)
        {
            MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError + ex.Message);
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }

    private void FillReadOnlyOrderDetails(DbHelper dbHelp)
    {
        if (_ord.OrdStatus == SOStatus.APPROVED || _ord.OrdStatus == SOStatus.NEW)
        {
            lblTitle.Text = Resources.Resource.lblEditSalesOrder;
            ltTitle.Text = Resources.Resource.lblOrderInformation; //Resources.Resource.lblEditSalesOrder;
        }
        else
        {
            lblTitle.Text = Resources.Resource.lblEditQuotation;
            ltTitle.Text = Resources.Resource.lblOrderInformation;
        }

        _comInfo.PopulateObject(_ord.OrdCompanyID, dbHelp);
        _part.PopulateObject(dbHelp, _ord.OrdCustID);

        InbizUser user = new InbizUser();

        valApprovedBy.Text = user.GetUserName(dbHelp, _ord.OrdVerifiedBy);
        //valCompanyName.Text = _comInfo.CompanyName;
        //valContactName.Text = "";
        //valContactPhone.Text = "";
        valCreatedBy.Text = user.GetUserName(dbHelp, _ord.OrdCreatedBy);
        valCustomerName.Text = _part.PartnerLongName;
        valCustomerName.NavigateUrl = ResolveUrl(string.Format("~/Partner/CustomerEdit.aspx?custid={0}&pType={1}&gType={2}", _part.PartnerID, Globals.GetPartnerType(_part.PartnerType), _part.ExtendedProperties.GuestType));
        //valFax.Text = _part.PartnerFax;
        valOrderDate.Text = BusinessUtility.GetDateTimeString(_ord.OrdCreatedOn, DateFormat.MMddyyyyHHmmss);
        txtOrderDate.Text = BusinessUtility.GetDateTimeString(_ord.OrdCreatedOn, DateFormat.MMddyyyy);
        //valQutExp.Text = BusinessUtility.GetDateTimeString(_ord.QutExpDate, DateFormat.MMddyyyyHHmmss);
        valSoNo.Text = _ord.OrdID.ToString();
        valStatus.Text = dlStatus.SelectedItem.Text;
        if (dlStatus.SelectedValue == SOStatus.SHIPPING_HELD)
        {
            OrderHoldReason reason = _ord.GetHoldReason(dbHelp, this.OrderID);
            if (reason.OrderID > 0)
            {
                switch (reason.OrdHeldReasonCode)
                {
                    case OrderHeldReasonCode.InsufficientInventory:
                        valStatus.Text = string.Format("<a href='javascript:;' onclick='popReasonToHeld()'>{0}</a>", dlStatus.SelectedItem.Text);
                        ltReason.Text = Resources.Resource.lblInsufficientInventory;
                        break;
                    case OrderHeldReasonCode.IncompleteAddress:
                        valStatus.Text = string.Format("<a href='javascript:;' onclick='popReasonToHeld()'>{0}</a>", dlStatus.SelectedItem.Text);
                        ltReason.Text = Resources.Resource.lblIncompleteAddress;
                        break;
                    case OrderHeldReasonCode.OrderNotValidated:
                        valStatus.Text = string.Format("<a href='javascript:;' onclick='popReasonToHeld()'>{0}</a>", dlStatus.SelectedItem.Text);
                        ltReason.Text = Resources.Resource.lblOrderNotValidated;
                        break;
                    case OrderHeldReasonCode.Other:
                        valStatus.Text = string.Format("<a href='javascript:;' onclick='popReasonToHeld()'>{0}</a>", dlStatus.SelectedItem.Text);
                        ltReason.Text = reason.OtherReason;
                        break;
                }
            }
        }

        valCustPhone.Text = _part.PartnerPhone;
        valCustEmail.Text = _part.PartnerEmail;
        valTransactionID.Text = new OrderTransaction().GetSaleTransactionID(dbHelp, _ord.OrdID);
    }

    protected void grdProducts_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        grdProducts.DataSource = SalesCartHelper.CurrentCart.Items;
        grdProducts.DataBind();
    }
    protected void grdProducts_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {

        string sValue = "<div style='background-color:#FA9189;width:100%;'>{0}</div>";

        int dsIdxDiscountType = 10;
        int dsIdxDiscount = 9;
        int dsIdxProductID = 12;
        int dsIdxGuestID = 13;
        int dsIdxRsvID = 14;
        int dsIdxRsItmID = 15;
        int dsIdxOItmID = 16;
        int dsIdxOrderItemCancel = 19;

        //    if (BusinessUtility.GetInt(e.RowValues[16]) == 0 && BusinessUtility.GetInt(e.RowValues[17]) == 0) //Set row backgroud color
        //    {
        //        e.CellHtml = " ";
        //  //      .ui-jqgrid tr.ui-row-ltr td {
        ////background-color: red;
        //    }

        //else 
        if (e.ColumnIndex == 4) //Column Product Color Name
        {
            ////e.CellHtml = e.CellHtml.Replace(Environment.NewLine, "<br>");
            //int iProductID = BusinessUtility.GetInt(e.CellHtml);
            //Product objProduct = new Product();
            //DataTable dtProduct = objProduct.GetProductColorSize(null, BusinessUtility.GetInt(iProductID));
            //foreach (DataRow drItem in dtProduct.Rows)
            //{
            //    e.CellHtml = BusinessUtility.GetString(drItem["Color"]);
            //}

        }
        else if (e.ColumnIndex == 5) //Column Product Size Name
        {
            ////e.CellHtml = e.CellHtml.Replace(Environment.NewLine, "<br>");
            //int iProductID = BusinessUtility.GetInt(e.CellHtml);
            //Product objProduct = new Product();
            //DataTable dtProduct = objProduct.GetProductColorSize(null, BusinessUtility.GetInt(iProductID));
            //foreach (DataRow drItem in dtProduct.Rows)
            //{
            //    e.CellHtml = BusinessUtility.GetString(drItem["Size"]);
            //}
            ////e.CellHtml = "Avtar";
        }

        if (e.ColumnIndex == 11) //Format Discount Column
        {
            string disType = BusinessUtility.GetString(e.RowValues[dsIdxDiscountType]);
            if (disType == "P")
            {
                e.CellHtml = string.Format("{0} %", e.RowValues[dsIdxDiscount]);
            }
            else
            {
                e.CellHtml = string.Format("{0:F}", e.RowValues[dsIdxDiscount]);
            }
        }
        else if (e.ColumnIndex == 1) //Allow to modify guest for bed selected
        {
            if (!BusinessUtility.GetBool(e.RowValues[dsIdxOrderItemCancel]))
            {
                int gidCol = 13;
                Partners p = new Partners();
                p.PopulateObject(BusinessUtility.GetInt(e.RowValues[gidCol]));
                string url = ResolveUrl(string.Format("~/Common/mdSearchGuest.aspx?pType={0}&gType={1}&ritmid={2}&opt=change_guest&jscallback=changeCustomer", Globals.GetPartnerType(p.PartnerType), p.ExtendedProperties.GuestType, e.RowValues[dsIdxRsItmID]));
                e.CellHtml += string.Format(@"<a class='search_guest inbiz_icon icon_edit_24'  href=""{0}"" >  {1}</a>", url, "");
            }

            //e.CellHtml = e.RowValues[13].ToString();
        }

        else if (e.ColumnIndex == 9) //Allow to modify guest for bed selected
        {
            //e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(e.CellHtml));

        }
        else if (e.ColumnIndex == 13) //Allow to modify guest for bed selected
        {
            //e.CellHtml = BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName);
            Orders objOrdr = new Orders();
            //DbHelper dbHelp = new DbHelper(true);
            //bool mustClose = false;
            //if (dbHelp == null)
            //{
            //    mustClose = true;
            //    dbHelp = new DbHelper(true);
            //}

            //objOrdr.PopulateObject(this.OrderID);
            //string ordCurrencyCode = "";
            //if (objOrdr != null)
            //{
            //    ordCurrencyCode = objOrdr.OrdCurrencyCode;
            //}
            //if (BusinessUtility.GetString(ordCurrencyCode) == "")
            //{
            //    Partners objPartnr = new Partners();
            //    //objPartnr.PopulateObject(dbHelp, this.CustomerID);
            //    objPartnr.PopulateObject( this.CustomerID);
            //    ordCurrencyCode = objPartnr.PartnerCurrencyCode;
            //}
            e.CellHtml = CurrencyFormat.GetCompanyCurrencyFormat(BusinessUtility.GetDouble(e.CellHtml), ordCurrencyCode);
        }

        else if (e.ColumnIndex == 14)//Format Edit Order Iterm link
        {
            if (!BusinessUtility.GetBool(e.RowValues[dsIdxOrderItemCancel]))
            {
                string url = string.Format("mdSearchProduct.aspx?editItemID={0}", e.RowKey);
                e.CellHtml = string.Format("<a class='edit_Product' href='{0}' >{1}</a>", url, Resources.Resource.lblEdit);
            }
            else
            {
                e.CellHtml = "";
            }
        }
        else if (e.ColumnIndex == 15)//Format Delete Order Iterm link
        {
            string url = string.Format("<a data_key='{0}' class=' delete_Product' href='javascript:void(0);' >{1}</a>", e.RowKey, Resources.Resource.delete);
            e.CellHtml = url;
        }
        else if (e.ColumnIndex == 17)//Format Edit Reservation Item link
        {
            if (BusinessUtility.GetInt(e.RowValues[dsIdxRsItmID]) > 0)
            {
                if (BusinessUtility.GetBool(e.RowValues[dsIdxOrderItemCancel]))
                {
                    e.CellHtml = "<b style='color:red'>" + Resources.Resource.lblCanceled + "</b>";
                }
                else
                {
                    //string url = string.Format("mdCancelReservation.aspx?rid={0}&ritmid={1}&gid={2}&oid={3}&oitmid={4}", e.RowValues[dsIdxRsvID], e.RowValues[dsIdxRsItmID], e.RowValues[dsIdxGuestID], this.OrderID, e.RowValues[dsIdxOItmID]);
                    string url = ResolveUrl("~/Reservation/mdPartialReservationMove.aspx") + string.Format("?rid={0}&oid={1}&oitmid={2}&action=cancel", e.RowValues[dsIdxRsItmID], this.OrderID, e.RowValues[dsIdxOItmID]);
                    e.CellHtml = string.Format("<a class='cancel_rsv' href='{0}' >{1}</a>", url, Resources.Resource.lblCancelReservation);
                }
            }
            else
            {
                string url = string.Format("<a data_key='{0}' class=' delete_Product' href='javascript:void(0);' >{1}</a>", e.RowKey, Resources.Resource.delete);
                e.CellHtml = url;
            }
        }
        else if (e.ColumnIndex == 18) //Move Reservation Link
        {
            if (!BusinessUtility.GetBool(e.RowValues[dsIdxOrderItemCancel]))
            {
                if (BusinessUtility.GetInt(e.RowValues[21]) == (int)StatusProductType.Accommodation)
                {
                    e.CellHtml = string.Format("<a class='' href='{0}?rid={1}&ritmid={2}&bedid={3}&soid={4}&cmd=move' >{5}</a>", ResolveUrl("~/Reservation/ReservationCalendar.aspx"), e.RowValues[dsIdxRsvID], e.RowValues[dsIdxRsItmID], e.RowValues[dsIdxProductID], this.OrderID, Resources.Resource.lblMove);
                }
                else
                {
                    e.CellHtml = "";
                }
            }
            else
            {
                e.CellHtml = "";
            }
        }


        //if (BusinessUtility.GetInt(e.RowValues[16]) == 0 && BusinessUtility.GetInt(e.RowValues[17]) == 0) //Set row backgroud color
        //{
        //    ProductQuantity objProdQty = new ProductQuantity();
        //    objProdQty.PopulateObject(null, BusinessUtility.GetInt(e.RowValues[dsIdxProductID]), Convert.ToString(e.RowValues[6]));
        //    Int64 iAvailableForSale = Convert.ToInt64(objProdQty.PrdOhdQty) - Convert.ToInt64(ProcessInventory.GetReserverQuantity(BusinessUtility.GetInt(e.RowValues[dsIdxProductID]), Convert.ToString(e.RowValues[6])));
        //    if (Convert.ToInt64(e.RowValues[3]) > iAvailableForSale)
        //        e.CellHtml = string.Format(sValue, e.CellHtml); //"<span style='background-color:red'>" + e.CellHtml + "</span>";
        //}

        if (e.ColumnIndex == 0)
        {
            if (BusinessUtility.GetInt(e.RowValues[16]) == 0 && BusinessUtility.GetInt(e.RowValues[17]) == 0)
            {
                ProductQuantity objProdQty = new ProductQuantity();
                objProdQty.PopulateObject(null, BusinessUtility.GetInt(e.RowValues[dsIdxProductID]), Convert.ToString(e.RowValues[6]));
                Int64 iAvailableForSale = Convert.ToInt64(objProdQty.PrdOhdQty) - Convert.ToInt64(ProcessInventory.GetReserverQuantity(BusinessUtility.GetInt(e.RowValues[dsIdxProductID]), Convert.ToString(e.RowValues[6])));
                if (Convert.ToInt64(e.RowValues[3]) > iAvailableForSale)
                {
                    //e.CellHtml = string.Format(sValue, e.CellHtml); 
                    isFillColor = true;
                }
                else
                {
                    isFillColor = false;
                }
            }
        }

        if (isFillColor == true)
        {
            //if (BusinessUtility.GetInt(e.RowValues[16]) == 0 && BusinessUtility.GetInt(e.RowValues[17]) == 0) //Set row backgroud color
            //{
                e.CellHtml = string.Format(sValue, e.CellHtml);
            //}
        }
    }

    bool isFillColor = false;

    protected void grdProcess_DataRequesting(object sender, Trirand.Web.UI.WebControls.JQGridDataRequestEventArgs e)
    {
        grdProcess.DataSource = SalesCartHelper.CurrentCart.ProcessItems;
        grdProcess.DataBind();
    }

    protected void grdProcess_CellBinding(object sender, Trirand.Web.UI.WebControls.JQGridCellBindEventArgs e)
    {

        if (e.ColumnIndex == 11)//Format Edit Process Item link //edited by mukesh 20130523
        {
            string url = string.Format("mdSalesProcess.aspx?processid={0}&orderid={1}", e.RowKey, this.OrderID);
            e.CellHtml = string.Format("<a class='edit_Process_Item' href='{0}' >{1}</a>", url, Resources.Resource.lblEdit);
        }
        else if (e.ColumnIndex == 12)//Format Delete Process Item link //Edited by mukesh 20130523
        {
            string url = string.Format("<a data_key='{0}' class='delete_Process_Item' href='javascript:void(0);' >{1}</a>", e.RowKey, Resources.Resource.delete);
            e.CellHtml = url;
        }
        else if (e.ColumnIndex == 4)
        {
            e.CellHtml = CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName));
        }
        else if (e.ColumnIndex == 5)
        {
            e.CellHtml = CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName));
        }
        else if (e.ColumnIndex == 6)
        {
            e.CellHtml = CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName));
        }
        else if (e.ColumnIndex == 9)
        {
            e.CellHtml = CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName));
        }
        else if (e.ColumnIndex == 10)
        {
            e.CellHtml = CurrencyFormat.GetReplaceCurrencySymbol(BusinessUtility.GetCurrencyString(BusinessUtility.GetDouble(e.CellHtml), Globals.CurrentCultureName));
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Session.Remove("SHIPMENT_CART");
        if (IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.OpenDatabaseConnection();
                if (dlStatus.SelectedValue != "C")
                {
                    if (SalesCartHelper.CurrentCart.Items.Count <= 0)
                    {
                        MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgSOPlzAddProductInQuotationList);
                        return;
                    }
                }
                if (this.OrderID > 0)
                {
                    this.UpdateOrder(dbHelp);
                    if (this.orderType == 2 || dlstOderType.SelectedValue == "2")
                    {
                        //ToWhsID=OGL&orderType=2&InvokeSrc=POS
                        //ordType=2&whsCode=OGL&BackFromOrder=1
                        if (Request.QueryString["InvokeSrc"] == "POS")
                        {
                            Response.Redirect("~/POS/pos.aspx", false);
                            //Response.Redirect("ViewSalesOrder.aspx?ordType=2&BackFromOrder=1&whsCode=" + Request.QueryString["ToWhsID"] + "&InvokeSrc=POS&PartnerID=" + BusinessUtility.GetString(hdnPartnerID.Value) + "", false);
                        }
                        else
                        {
                            Response.Redirect("ViewSalesOrder.aspx?ordType=2", false);
                        }
                    }
                    else if (this.orderType == 8 || dlstOderType.SelectedValue == "8")
                    {
                        Response.Redirect("ViewSalesOrder.aspx?ordType=8", false);
                    }
                    else if (this.orderType == 5 || dlstOderType.SelectedValue == "5")
                    {
                        Response.Redirect("ViewSalesOrder.aspx?ordType=5", false);
                    }
                    else if (this.orderType == (int)OrderCommission.Work || BusinessUtility.GetInt(dlstOderType.SelectedValue) == (int)OrderCommission.Work)
                    {
                        Response.Redirect("ViewSalesOrder.aspx?ordType=" + (int)OrderCommission.Work, false);
                    }
                    else
                    {
                        Response.Redirect("ViewSalesOrder.aspx", false);
                    }
                }
                else
                {
                    this.InsertOrder(dbHelp);
                    if (this.orderType == 2 || dlstOderType.SelectedValue == "2")
                    {
                        if (Request.QueryString["InvokeSrc"] == "POS")
                        {
                            Response.Redirect("~/POS/pos.aspx", false);
                            //Response.Redirect("ViewSalesOrder.aspx?ordType=2&BackFromOrder=1&whsCode=" + Request.QueryString["ToWhsID"] + "&InvokeSrc=POS&PartnerID=" + BusinessUtility.GetString(hdnPartnerID.Value) + "", false);
                        }
                        else
                        {
                            Response.Redirect("ViewSalesOrder.aspx?ordType=2", false);
                        }
                    }
                    else if (this.orderType == 8 || dlstOderType.SelectedValue == "8")
                    {
                        Response.Redirect("ViewSalesOrder.aspx?ordType=8", false);
                    }
                    else if (this.orderType == 5 || dlstOderType.SelectedValue == "5")
                    {
                        Response.Redirect("ViewSalesOrder.aspx?ordType=5", false);
                    }
                    else if (this.orderType == (int)OrderCommission.Work || BusinessUtility.GetInt(dlstOderType.SelectedValue) == (int)OrderCommission.Work)
                    {
                        Response.Redirect("ViewSalesOrder.aspx?ordType=" + (int)OrderCommission.Work, false);
                    }
                    else
                    {
                        Response.Redirect("ViewSalesOrder.aspx", false);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnGenerateInvoice_Click(object sender, EventArgs e)
    {
        //if (IsValid)
        //{
        //Check if Invoice IS Already generated
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            dbHelp.OpenDatabaseConnection();
            if (BusinessUtility.GetInt(hdnInvoiceNoForOrder.Value) > 0)
            {
                Response.Redirect(string.Format("~/invoice/ViewInvoiceDetails.aspx?SOID={0}&ComID={1}", hdnInvoiceNoForOrder.Value, hdnCompanyID.Value), false);
            }

            int invID = GenerateInvoice(dbHelp);
            if (invID > 0)
            {
                Response.Redirect(string.Format("~/invoice/ViewInvoiceDetails.aspx?SOID={0}&ComID={1}", invID, hdnCompanyID.Value), false);
            }
        }
        catch (Exception ex)
        {
            if (ex.Message == CustomExceptionCodes.INVOICE_ALREADY_EXISTS_FOR_GIVEN_ORDER)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvoiceAlreadyExists + ex.Message);
            }
            else
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError + " : " + ex.Message);
            }
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
        //}
    }

    private void SetData()
    {
        _ord.OrdID = this.OrderID;
        _ord.InvDate = BusinessUtility.GetDateTime(txtInvDate.Text, DateFormat.MMddyyyy);
        _ord.InvRefNo = BusinessUtility.GetInt(txtInvNo.Text);
        _ord.OrdComment = txtNotes.Text;
        _ord.OrdCompanyID = BusinessUtility.GetInt(hdnCompanyID.Value);
        _ord.OrdCreatedBy = CurrentUser.UserID;
        _ord.OrdCreatedFromIP = Request.ServerVariables["REMOTE_ADDR"].ToString();
        _ord.OrdCurrencyCode = hdnPartnerCurrencyCode.Value;
        _ord.OrdCurrencyExRate = BusinessUtility.GetDouble(hdnCurrrencyExchangeRate.Value);
        _ord.OrdCustID = BusinessUtility.GetInt(hdnPartnerID.Value);
        _ord.OrdCustPO = txtCustPO.Text;
        _ord.OrdCustType = Globals.GetPartnerType(BusinessUtility.GetInt(hdnParterTypeID.Value));
        _ord.OrderRejectReason = string.Empty;
        _ord.OrderTypeCommission = BusinessUtility.GetInt(dlstOderType.SelectedValue);
        _ord.OrdLastUpdateBy = CurrentUser.UserID;
        //_ord.OrdNetTerms = txtNetTerms.Text;
        _ord.OrdNetTerms = ddlNetTerms.SelectedIndex > -1 ? ddlNetTerms.SelectedItem.Text : string.Empty;
        _ord.OrdSalesRepID = BusinessUtility.GetInt(hdnSalesAgentID.Value);
        _ord.OrdSaleWeb = rdlstISWeb.SelectedValue == "1";
        _ord.OrdShippingTerms = txtTerms.Text;
        _ord.OrdShpBlankPref = rblstShipBlankPref.SelectedValue == "1";
        _ord.OrdShpCode = txtShpCode.Text;
        _ord.OrdShpCost = 0.0D; //BusinessUtility.GetDouble(txtShpCost.Text);
        _ord.OrdShpDate = BusinessUtility.GetDateTime(txtShippingDate.Text, DateFormat.MMddyyyy);
        _ord.OrdShpTrackNo = txtShpTrackNo.Text;
        _ord.OrdShpWhsCode = dlShpWarehouse.SelectedValue;
        if (_ord.OrderTypeCommission == (int)OrderCommission.Transfer)
        {
            _ord.OrdShpToWhsCode = dlShpToWarehouse.SelectedValue;
        }
        if (this.OrderID <= 0)
        {
            //if (CurrentUser.IsInRole(RoleID.QUOTATION_APPROVER) || CurrentUser.IsInRole(RoleID.ADMINISTRATOR))
            //{
            //    _ord.OrdStatus = dlStatus.SelectedValue;
            //    //_ord.OrdStatus = SOStatus.APPROVED; //SOStatus.READY_FOR_SHIPPING; //Diractly set to Ready to ship status
            //}
            //else
            //{
            //    _ord.OrdStatus = SOStatus.NEW;
            //}
            _ord.OrdStatus = SOStatus.NEW;
        }
        else
        {
            _ord.OrdStatus = dlStatus.SelectedValue;
        }


        _ord.OrdType = SOType.QUOTATION;
        _ord.OrdVerified = _ord.OrdStatus != SOStatus.NEW;
        _ord.OrdVerifiedBy = CurrentUser.UserID;
        _ord.QutExpDate = BusinessUtility.GetDateTime(txtQutExpDate.Text, DateFormat.MMddyyyy);
        _ord.OrdCreatedOn = CurrentUser.IsInRole(RoleID.QUOTATION_APPROVER) ? BusinessUtility.GetDateTime(txtOrderDate.Text, DateFormat.MMddyyyy) : DateTime.Now;
        _ord.OrdCreatedOn = _ord.OrdCreatedOn == DateTime.MinValue ? DateTime.Now : _ord.OrdCreatedOn;
        _ord.OrdLastUpdatedOn = DateTime.Now;

        //Set Descoount field 
        _ord.OrdDiscount = SalesCartHelper.CurrentCart.OrdDiscount;
        _ord.OrdDiscountType = SalesCartHelper.CurrentCart.OrdDiscountType;

        if (dlStatus.SelectedValue == "C")
        {
            _ord.OrdClosedReason = ddlReason.SelectedItem.Value;
        }
        else
        {
            _ord.OrdClosedReason = string.Empty;
        }
    }

    private void InsertOrder(DbHelper dbHelp)
    {
        try
        {
            SetData(); //Initialize Order Object with form data

            //If custom invoice entry allowed then check of Inv Ref No existance
            if (ConfigurationManager.AppSettings["AllowCustomInvoiceEntry"].ToLower() == "true")
            {
                if (new Invoice().IsInvoiceExists(dbHelp, InvoiceReferenceType.INVOICE, _ord.OrdCompanyID, _ord.InvRefNo, 0))
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblInvoiceExists);
                    txtInvNo.Focus();
                    return;
                }
            }

            if (_ord.Insert(dbHelp, CurrentUser.UserID))
            {
                if (_ord.OrdID > 0)
                {
                    //Set custom fields data
                    CustomFields1.Save();

                    // Save Order Shippment and Billing Address
                    if (BusinessUtility.GetInt(txtBillToAddressTxt.Value) > 0)
                    {
                        Addresses addr = new Addresses();
                        addr.GetAddress(BusinessUtility.GetInt(txtBillToAddressTxt.Value));
                        _ord.SaveOrderAddresse(_ord.OrdID, addr.AddressLine1, addr.AddressLine2, addr.AddressLine3, addr.AddressCity, addr.AddressState, addr.AddressCountry, addr.AddressPostalCode, AddressType.BILL_TO_ADDRESS);
                    }
                    else
                    {
                        _billToAddress.GetCustomerAddress(_ord.OrdCustID, AddressType.BILL_TO_ADDRESS);
                        _ord.SaveOrderAddresse(_ord.OrdID, _billToAddress.AddressLine1, _billToAddress.AddressLine2, _billToAddress.AddressLine3, _billToAddress.AddressCity, _billToAddress.AddressState, _billToAddress.AddressCountry, _billToAddress.AddressPostalCode, AddressType.BILL_TO_ADDRESS);
                    }
                    //Addresses shipToAddress = new Addresses();
                    if (BusinessUtility.GetInt(txtShpToAddressTxt.Value) > 0)
                    {
                        Addresses addr = new Addresses();
                        addr.GetAddress(BusinessUtility.GetInt(txtShpToAddressTxt.Value));
                        _ord.SaveOrderAddresse(_ord.OrdID, addr.AddressLine1, addr.AddressLine2, addr.AddressLine3, addr.AddressCity, addr.AddressState, addr.AddressCountry, addr.AddressPostalCode, AddressType.SHIP_TO_ADDRESS);
                    }
                    else
                    {
                        _shipToAddress.GetCustomerAddress(_ord.OrdCustID, AddressType.SHIP_TO_ADDRESS);
                        _ord.SaveOrderAddresse(_ord.OrdID, _shipToAddress.AddressLine1, _shipToAddress.AddressLine2, _shipToAddress.AddressLine3, _shipToAddress.AddressCity, _shipToAddress.AddressState, _shipToAddress.AddressCountry, _shipToAddress.AddressPostalCode, AddressType.SHIP_TO_ADDRESS);
                    }

                    List<OrderItems> ordItems = SalesCartHelper.GetOrderItemsFromCart(_ord.OrdID);
                    //Add Order Items to OrderItems Table
                    OrderItems ordItem = new OrderItems();
                    ordItem.AddOrderItems(dbHelp, ordItems);

                    // Retrive Customers Sale Rep
                    Partners _cust = new Partners();
                    List<int> lstRep = _cust.GetCustomerSalesRep(this.CustomerID);
                    if (lstRep.Count > 0)
                    {
                        _ord.SetOrderSalesRepUsers(dbHelp, _ord.OrdID, lstRep.ToArray());
                    }
                    else
                    {
                        //Add Order Sales Rep users by default to creator
                        _ord.SetOrderSalesRepUsers(dbHelp, _ord.OrdID, new int[] { CurrentUser.UserID });
                    }
                    //To for Order journal Logic

                    //To Do Subscription Logic [Invoice Frequency]

                    //To Do here for Order Item Process                
                    List<OrderItemProcess> ordITemProcess = SalesCartHelper.GetProcessItemsFromCart(_ord.OrdID);

                    OrderItemProcess objProcess = new OrderItemProcess();
                    objProcess.AddProcessItems(dbHelp, ordITemProcess);

                    //To Do Here for Generate Alert
                    //Get List of users who ahs QOA or ADM module & generate alert for those
                    if (_ord.OrdStatus == SOStatus.NEW)
                    {
                        AlertHelper.SetAlertQuotationToApprove(dbHelp, _ord.OrdID, CurrentUser.UserID);
                    }

                    //Send alert to Quotation approver users


                    //To do add into log
                    new OrderActionLog().AddLog(dbHelp, CurrentUser.UserID, _ord.OrdID, "Order created", _ord.OrdComment);
                }
            }
        }
        catch (Exception ex)
        {
            throw;
            //MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
    }

    private void UpdateOrder(DbHelper dbHelp)
    {
        try
        {
            SetData(); //Initialize Order Object with form data
            _ord.OrdID = this.OrderID;

            //If custom invoice entry allowed then check of Inv Ref No existance
            if (ConfigurationManager.AppSettings["AllowCustomInvoiceEntry"].ToLower() == "true")
            {
                if (new Invoice().IsInvoiceExists(dbHelp, InvoiceReferenceType.INVOICE, _ord.OrdCompanyID, _ord.InvRefNo, 0))
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.lblInvoiceExists);
                    txtInvNo.Focus();
                    return;
                }
            }

            _ord.Update(dbHelp, CurrentUser.UserID);

            //Update Order Sales Reps
            List<int> srUsers = new List<int>();
            foreach (ListItem item in lbxSalesRepresentativs.Items)
            {
                if (item.Selected)
                {
                    srUsers.Add(BusinessUtility.GetInt(item.Value));
                }
            }
            _ord.SetOrderSalesRepUsers(dbHelp, _ord.OrdID, srUsers.ToArray());

            //Set custom fields data
            CustomFields1.Save();

            //Need To Credit.. Paid Amount to User If Reservation gets Canceled 
            if (_ord.OrdStatus == SOStatus.RESERVATION_CANCELED)
            {
                Reservation res = new Reservation();
                res.CancelReservation(dbHelp, _ord.OrdID);
            }
            else if (_ord.OrdStatus == SOStatus.READY_FOR_SHIPPING && hdnPreviousStatus.Value != SOStatus.READY_FOR_SHIPPING)
            {
                //Need to send alert for shipping mangers & shipping users
                AlertHelper.SetAlertForReadyToReceive(dbHelp, _ord.OrdID, CurrentUser.UserID);
            }

            //To Do to Add Action Log            
            new OrderActionLog().AddLog(dbHelp, CurrentUser.UserID, _ord.OrdID, "Order updated", _ord.OrdComment);
        }
        catch (Exception ex)
        {
            throw;
            //MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
        }
    }

    private int GenerateInvoice(DbHelper dbHelp)
    {
        if (this.OrderID <= 0)
        {
            this.InsertOrder(dbHelp);
        }
        else
        {
            this.UpdateOrder(dbHelp);
        }
        if (_ord.OrdID > 0)
        {
            try
            {
                //Before generating invoice we should validate wether it is allowed or not for current order status
                bool isValid = new SysCompanyInfo().ValidateOrderStatusBeforeInvoiced(dbHelp, _ord.OrdCompanyID, _ord.OrdStatus);
                if (!isValid)
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, "Invoice generation not allowed for current order status!");
                    return -1;
                }

                //Update Quantity In Inventory
                ProductQuantity pQty = new ProductQuantity();
                //pQty.UpdateProuctQuantityOnSalesProcess(_ord.OrdID); //No need to update now because we are handling ths through database an on base of order staus 2012-12-20

                //Generate Invoice
                Invoice objInv = new Invoice();
                bool isDistinctInvoice = ConfigurationManager.AppSettings["SeparateInvID"].ToLower() == "yes";
                objInv.PopulateObjectByOrder(dbHelp, _ord.OrdID, InvoicesStatus.SUBMITTED, InvoiceReferenceType.INVOICE, CurrentUser.UserID);
                objInv.Insert(dbHelp, CurrentUser.UserID);
                if (objInv.InvID > 0)
                {
                    if (objInv.InvRefNo == 0)
                    {
                        objInv.UpdateInvoiceRefNo(dbHelp, objInv.InvID, objInv.InvCompanyID, objInv.InvRefType, isDistinctInvoice);
                    }
                    InvoiceItems invItems = new InvoiceItems();
                    invItems.AddInvoiceItemsFromOrder(dbHelp, _ord.OrdID, objInv.InvID);

                    InvoiceItemProcess invProcess = new InvoiceItemProcess();
                    invProcess.AddProcessInvoiceItemsForOrder(dbHelp, _ord.OrdID, objInv.InvID);

                    //Update account receivable
                    PreAccountRcv arc = new PreAccountRcv();
                    arc.MoveToActualAR(dbHelp, CurrentUser.UserID, _ord.OrdID, objInv.InvID);

                    //Once Invoice generated successfully just make a entry in sls Analysis table 
                    new SlsAnalysis().RecordOnInvoiceGeneration(dbHelp, objInv.InvID);
                }

                return objInv.InvID;
            }
            catch (Exception ex)
            {
                throw;
                //if (ex.Message == CustomExceptionCodes.INVOICE_ALREADY_EXISTS_FOR_GIVEN_ORDER)
                //{
                //    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvoiceAlreadyExists);
                //}
                //else
                //{
                //    MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
                //} 
            }
        }
        else
        {
            return 0;
        }
    }

    public int CompanyID
    {
        get
        {
            int cid = 0;
            int.TryParse(Request.QueryString["ComID"], out cid);
            return cid > 0 ? cid : CurrentUser.DefaultCompanyID;
        }
    }

    public int OrderID
    {
        get
        {
            int oid = 0;
            int.TryParse(Request.QueryString["SOID"], out oid);
            return oid;
        }
    }

    public int CustomerID
    {
        get
        {
            int cid = 0;
            int.TryParse(Request.QueryString["custID"], out cid);
            return cid;
        }
    }

    public string WarehouseCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["whsID"]);
        }
    }
    public string WhsFromCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["FromWhsID"]);
        }
    }
    public string WhsToCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["ToWhsID"]);
        }
    }
    public int orderType
    {
        get
        {
            return BusinessUtility.GetInt(Request.QueryString["orderType"]);
        }
    }


    //public string orderCurrencyCode { get; set; }

    //public int orderType
    //{
    //    get
    //    {
    //        return BusinessUtility.GetInt(Request.QueryString["orderType"]);
    //    }
    //}


    private string GetDocType()
    {
        string sStatus = "";
        switch (hdnPreviousStatus.Value)
        {
            case "N":
            case "A":
            case "V":
            case "H":
                sStatus = "QO";
                break;
            case "P":
            case "I":
            case "S":
            case "C":
            case "Z":
                sStatus = "SO";
                break;
            default:
                sStatus = "QO";
                break;
        }
        return sStatus;
    }

    #region WebMethods
    [System.Web.Services.WebMethod]
    public static string DeleteCartItem(string productID)
    {
        try
        {
            SalesCartHelper.DeleteCartItem(BusinessUtility.GetInt(productID));
            return "1";
        }
        catch
        {
            return "0";
        }
    }

    [System.Web.Services.WebMethod]
    public static string DeleteProcessCartItem(string processid)
    {
        try
        {
            SalesCartHelper.DeleteProcessCartItem(BusinessUtility.GetInt(processid));
            return "1";
        }
        catch
        {
            return "0";
        }
    }

    [System.Web.Services.WebMethod]
    public static string[] GetCalculatedTotalHtml(string partnerid, int ordID)
    {
        try
        {
            List<string> lstHtmlData = new List<string>();
            TotalSummary lTotal = SalesCartHelper.GetOrderTotal();
            //Set Lables for html to be generated
            //lTotal.SetLabel(TotalSummary.LabelKey.BalanceAmount, Resources.Resource.lblBalanceAmount);
            //lTotal.SetLabel(TotalSummary.LabelKey.GrandSubTotal, Resources.Resource.lblSubTotalOrder);
            //lTotal.SetLabel(TotalSummary.LabelKey.GrandTotal, Resources.Resource.lblOrderTotal);
            //lTotal.SetLabel(TotalSummary.LabelKey.ReceivedAmount, Resources.Resource.lblReceivedAmount);
            //lTotal.SetLabel(TotalSummary.LabelKey.SubTotalItems, Resources.Resource.lblSubTotalItems);
            //lTotal.SetLabel(TotalSummary.LabelKey.SubTotalServices, Resources.Resource.lblSubTotalServices);
            //lTotal.SetLabel(TotalSummary.LabelKey.TotalDiscount, Resources.Resource.lblDiscount);
            //lTotal.SetLabel(TotalSummary.LabelKey.TotalTax, Resources.Resource.lblTotalTax);

            lstHtmlData.Add(CommonHtml.GetItemTotalHtml(lTotal, BusinessUtility.GetInt(partnerid), BusinessUtility.GetInt(ordID)));
            lstHtmlData.Add(CommonHtml.GetProcessTotalHtml(lTotal));
            lstHtmlData.Add(CommonHtml.GetGrandTotalHtml(lTotal));
            return lstHtmlData.ToArray();
        }
        catch
        {
            return new List<string>().ToArray();
        }
    }

    #endregion

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (this.orderType == 2 || dlstOderType.SelectedValue == "2")
        {
            Response.Redirect("ViewSalesOrder.aspx?ordType=2");
        }
        else if (this.orderType == 8 || dlstOderType.SelectedValue == "8")
        {
            Response.Redirect("ViewSalesOrder.aspx?ordType=8", false);
        }
        else if (this.orderType == 5 || dlstOderType.SelectedValue == "5")
        {
            Response.Redirect("ViewSalesOrder.aspx?ordType=5", false);
        }
        else if (this.orderType == (int)OrderCommission.Work || BusinessUtility.GetInt(dlstOderType.SelectedValue) == (int)OrderCommission.Work)
        {
            Response.Redirect("ViewSalesOrder.aspx?ordType=" + (int)OrderCommission.Work, false);
        }
        else
        {
            Response.Redirect("ViewSalesOrder.aspx");
        }
        Session.Remove("SHIPMENT_CART");
    }
    protected void btnInvAndGen_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.OpenDatabaseConnection();
                //Check if Invoice IS Already generated
                if (BusinessUtility.GetInt(hdnInvoiceNoForOrder.Value) > 0)
                {
                    Response.Redirect(string.Format("~/AccountsReceivable/InvoicePayment.aspx?SOID={0}", hdnInvoiceNoForOrder.Value), false);
                }

                int invID = GenerateInvoice(dbHelp);
                if (invID > 0)
                {
                    Response.Redirect(string.Format("~/AccountsReceivable/InvoicePayment.aspx?SOID={0}", invID), false);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == CustomExceptionCodes.INVOICE_ALREADY_EXISTS_FOR_GIVEN_ORDER)
                {
                    MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvoiceAlreadyExists);
                }
                else
                {
                    MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
                }
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void btnSaveAndGoToCal_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            DbHelper dbHelp = new DbHelper(true);
            try
            {
                dbHelp.OpenDatabaseConnection();
                if (this.OrderID > 0)
                {
                    this.UpdateOrder(dbHelp);
                    Response.Redirect("~/Reservation/ReservationCalendar.aspx?soid=" + this.OrderID, false);
                }
            }
            catch (Exception ex)
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
            finally
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    protected void lblCancelReservationPop_Click(object sender, EventArgs e)
    {
        if (this.OrderID > 0)
        {
            //Reservation ORDER Canceld
            Orders ord = new Orders();
            ord.CancelReservationOrder(this.OrderID, txtReasonToCancel.Text);

            //To Do to Add Action Log            
            new OrderActionLog().AddLog(null, CurrentUser.UserID, this.OrderID, "Reservation canceled", "");

            MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgReservationCanceledSuccessfully);
            Response.Redirect(Request.RawUrl);
        }
    }

    protected void btnDuplicateOrder_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            try
            {
                int id = _ord.CreateDuplicateOrder(this.OrderID);
                if (id > 0)
                {
                    MessageState.SetGlobalMessage(MessageType.Success, Resources.Resource.msgOrderSuccessfullyDuplicated);
                    Response.Redirect("ViewOrderDetails.aspx?SOID=" + id, false);
                }
            }
            catch
            {
                MessageState.SetGlobalMessage(MessageType.Critical, Resources.Resource.msgCriticalError);
            }
        }
    }

    public string POSRegCode
    {
        get
        {
            return BusinessUtility.GetString(Request.QueryString["regcode"]);
        }
    }


    protected void btnOrderNO_Click(object sender, System.EventArgs e)
    {
        if (BusinessUtility.GetInt(hdnShpToOrder.Value) <= 0)
        {
            return;
        }
        Response.Redirect(string.Format("Shipment.aspx?oid={0}", BusinessUtility.GetInt(hdnShpToOrder.Value)));
    }

    protected void btnTicket_Click(object sender, EventArgs e)
    {
        int orderIDs = 0;
        if (orderIDs == 0)
        {
            orderIDs = this.OrderID;
        }
        if (orderIDs > 0)
        {
            string urlFormat = ResolveUrl("~/Common/Print.aspx") + "?ReqIDs={0}&DocTyp=TKT";
            string windowOpen = "window.open('{0}', '{1}', 'height=365,width=810,left=180,top=150,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');";
            string script = string.Empty;
            script = string.Format(windowOpen, string.Format(urlFormat, orderIDs), "Ticket");
            ClientScript.RegisterStartupScript(this.GetType(), "PrintTicket", script, true);
        }
    }

    #region Web Methods
    [System.Web.Services.WebMethod]
    public static string SetCustomerOrderAddress(int addrId, int ordID, string addType)
    {
        Addresses objAddress = new Addresses();
        objAddress.GetAddress(addrId);


        // Set Order Address
        if (ordID > 0)
        {
            Orders objOrders = new Orders();
            objOrders.SaveOrderAddresse(ordID, objAddress.AddressLine1, objAddress.AddressLine2, objAddress.AddressLine3, objAddress.AddressCity, objAddress.AddressState, objAddress.AddressCountry, objAddress.AddressPostalCode, addType);
        }
        return objAddress.ToHtml();
    }
    #endregion


}
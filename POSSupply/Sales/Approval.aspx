<%@ Page Language="VB" MasterPageFile="~/AdminMaster.master" AutoEventWireup="false"
    CodeFile="Approval.aspx.vb" Inherits="Sales_Approval" ValidateRequest="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="Resources.resource" %>
<%@ Register src="../Controls/CommonSearch/OrderSearch.ascx" tagname="OrderSearch" tagprefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLeftPanel" runat="Server">
    <script language="javascript" type="text/javascript">
        $('#divMainContainerTitle').corner();
    </script>

    <uc1:OrderSearch ID="OrderSearch1" runat="server" />
</asp:Content>

<asp:Content ID="cntSalesApproval" ContentPlaceHolderID="cphMaster" runat="Server">
    <div id="divMainContainerTitle" class="divMainContainerTitle">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="width: 300px;">
                    <h2>
                        <asp:Literal runat="server" ID="lblTitle"></asp:Literal>
                    </h2>
                </td>
                <td style="text-align: right;">
                    
                </td>
                <td align="right" style="width: 150px;">
                    <div class="buttonwrapper">
                        <a id="cmdNotes" runat="server" causesvalidation="false" class="ovalbutton" href="#">
                            <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                <%=Resources.Resource.lblCustomerNotes%>
                            </span></a>
                    </div>
                </td>
                <td style="width: 150px;">
                    <div class="buttonwrapper">
                        <a id="cmdCustomerDetail" runat="server" causesvalidation="false" class="ovalbutton"
                            href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                <%=Resources.Resource.btnColCustomerDetail%>
                            </span></a>
                    </div>
                </td>
            </tr>
        </table>       
        <asp:HiddenField runat="server" ID="hdnLeft" />
        <asp:HiddenField runat="server" ID="hdnOneItem" />
        <asp:HiddenField runat="server" ID="hdnCreatedUserID" />
        <asp:HiddenField runat="server" ID="hdnPartnerID" />        
    </div>

    <div class="divMainContent">
        <%-- onkeypress="return disableEnterKey(event)"--%>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td height="5" colspan="2">
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:UpdatePanel ID="upnlMsg" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblMsg" runat="server" ForeColor="green" Font-Bold="true"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td height="20" colspan="2" align="right">
                    <table align="right">
                        <tr>
                            <td width="400px">
                                &nbsp;
                            </td>
                            <td>                                
                                <a href="javascript:void(0);" title="Print Terms" onclick="window.open('terms.htm', 'Print', 'height=1,width=1,right=1,bottom=1,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no,minimize=no');">
                                    <asp:Image ID="printTerms" ImageUrl="~/images/print.gif" runat="server" />
                                </a>
                            </td>
                            <td>
                                <asp:ImageButton runat="server" ID="imgFax" ImageUrl="~/Images/fax.png" Width="48"
                                    Height="48" />
                            </td>                            
                            <td>
                                <asp:ImageButton runat="server" ID="imgMail" ImageUrl="~/Images/email.jpg" Width="48"
                                    Height="48" />
                            </td>
                            <td>
                                <asp:ImageButton runat="server" ID="imgPdf" CausesValidation="false" ImageUrl="~/Images/Printer1.png"
                                    Width="48" Height="48" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <asp:HiddenField ID="hdnWebhouse" runat="server" />
            <tr>
                <td height="20" colspan="2">
                    <table width="100%">
                        <tr>
                            <td width="20%">
                                <asp:Label ID="lblSONumber" Text="<%$ Resources:Resource, lblSOQuotationNo %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td width="30%">
                                <asp:Label ID="lblSOID" CssClass="lblBold" runat="server" />
                            </td>
                            <td width="2%">
                            </td>
                            <td align="left" width="18%">
                                <asp:Label ID="lblSOPOComName" Text="<%$ Resources:Resource, lblSOPOComName %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left" width="30%">
                                <asp:Label ID="lblSOPOComNameTitle" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblSODate1" Text="<%$ Resources:Resource, lblSOOrderDate %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblSODate" CssClass="lblBold" runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblCustomerName1" Text="<%$ Resources:Resource, lblSOCustomerName %>"
                                    CssClass="lblBold" runat="server" />
                                <%--<asp:Label ID="lblSOPOwhs" Text="<%$ Resources:Resource, lblSOPOwhs %>" CssClass="lblBold"
                                    runat="server" />--%>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblCustomerName" CssClass="lblBold" runat="server" />
                                <asp:HiddenField ID="hdnCustId" runat="server"></asp:HiddenField>
                                <asp:HiddenField ID="hdnCustTyp" runat="server"></asp:HiddenField>
                                <asp:HiddenField ID="hdnCurExRate" EnableViewState="false" runat="server" />
                                <%--<asp:Label ID="lblSOPOwhsName" CssClass="lblBold" runat="server" />--%>
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblSOQEDate1" Text="<%$ Resources:Resource, lblSOQutExpDate %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblSOQEDate" CssClass="lblBold" runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:Label ID="Label7" Text="<%$ Resources:Resource, lblARContactNm %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblContactName" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label2" Text="<%$ Resources:Resource, lblSOPOStatus %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblStatus" CssClass="lblBold" runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:Label ID="Label8" Text="<%$ Resources:Resource, lblARContactPhone %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblContactPhoneNo" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label6" Text="<%$ Resources:Resource, lblCreatedBy %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblCreatedBy" CssClass="lblBold" runat="server" />
                            </td>
                            <td>
                            </td>
                            <td align="left">
                                <asp:Label ID="Label3" Text="<%$ Resources:Resource, POFax %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblFax" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label5" Text="<%$ Resources:Resource, lblApprovedBy %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblApprovedBy" CssClass="lblBold" runat="server" />
                            </td>
                            <td>
                            </td>
                            <td colspan="2" align="left">
                                <asp:CheckBox ID="CBSubscription" Text="Subcription&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                    Enabled="false" TextAlign="Left" CssClass="lblBold" runat="server" style="display:none;"/>
                            </td>
                        </tr>
                        <tr height="5">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr id="trAddress" runat="server" visible="false">
                            <td align="left">
                                <asp:Label ID="Label1" Text="<%$ Resources:Resource, lblSOReceivingAddress %>" CssClass="lblBold"
                                    runat="server" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblAddress" CssClass="lblBold" runat="server" />
                            </td>
                        </tr>
                        <tr height="15">
                            <td colspan="2">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <div id="divItems" style="width: 850;" runat="server" visible="false">
                        <div style="display: block; width: 850;" id="divViewPrd" runat="server">
                            <asp:Panel ID="pnlAddProduct" runat="server">
                                <table cellspacing="0" cellpadding="0" width="800">
                                    <tr>
                                        <td width='15px' background="../Images/popup_left.png">
                                        </td>
                                        <td align="left" valign="middle" class="popup_title_newbg">
                                            <span class="discount_title">
                                                <%=lblSOEditPrdInQuotationItemList%>
                                            </span>
                                        </td>
                                        <td class="popup_title_newbg" align="right">
                                            <a href="javascript:funCloseLoc();">
                                                <img alt="" border="0" src='../images/close_popup_btn.gif' width="24" height="24" /></a>
                                        </td>
                                        <td width='15px' background="../Images/popup_right.png">
                                        </td>
                                    </tr>
                                </table>
                                <table class="popup_content_newbg" cellspacing="0" cellpadding="0" width="800">
                                    <tr>
                                        <td colspan="7" height="6">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="tdOrdItmLb1" runat="server" visible="false">
                                            <asp:Label ID="lblPOItmID1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, POProductID1 %>"></asp:Label>
                                        </td>
                                        <td id="tdPrdIdLbl" runat="server" visible="false">
                                            <asp:Label ID="lblPOProductID1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, POProductID1 %>"></asp:Label>
                                        </td>
                                        <td class="popup_lbl">
                                            <asp:Label ID="lblUPCCode" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOUPCCode %>"></asp:Label>
                                        </td>
                                        <td class="popup_lbl">
                                            <asp:Label ID="lblPOProductName1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, POProductName1 %>"></asp:Label>
                                        </td>
                                        <td class="popup_lbl">
                                            <asp:Label ID="lblShpToAdd" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, titleShippingAddress %>"></asp:Label>
                                        </td>
                                        <td class="popup_lbl">
                                            <asp:Label ID="lblPOTaxGroup" runat="server" CssClass="lblBold" Text="Tax Group:"></asp:Label>
                                        </td>
                                        <td class="popup_lbl">
                                            <asp:Label ID="lblPODiscount" runat="server" CssClass="lblBold" Text="Discount:"></asp:Label>
                                        </td>
                                        <td class="popup_lbl">
                                            <asp:Label ID="lblQuantity" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOQuantity %>"></asp:Label>
                                        </td>
                                        <td class="popup_lbl">
                                            <asp:Label ID="lblActPrice" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, grdPOPrdCostPrice %>"></asp:Label>
                                        </td>
                                        <%--<TD><asp:Label id="lblPrice" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, grdPOPrdCostPrice %>"></asp:Label> </TD>--%>
                                        <td rowspan="3">
                                            <img src="../images/popup_line.gif" alt="" />
                                        </td>
                                        <td>
                                        </td>
                                        <%--<TD id="tdPrdWhsLbl" runat="server" visible="false"><asp:Label id="lblPOWarehouse" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOWarehouse%>"></asp:Label> </TD>--%>
                                    </tr>
                                    <tr>
                                        <td colspan="7" height="6">
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td id="tdOrdItmIdTxt" runat="server" visible="false">
                                            <asp:TextBox ID="txtOrdItmId" runat="server" Width="0px" BackColor="LightGray" ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td id="tdPrdIdTxt" runat="server" visible="false">
                                            <asp:TextBox ID="txtProductId" runat="server" Width="0px" BackColor="LightGray" ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtUPCCode" CssClass="popup_input" runat="server" Width="100px"
                                                BackColor="LightGray" ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtProductName" CssClass="popup_textarea" runat="server" Width="135px"
                                                TextMode="MultiLine" Rows="4" ReadOnly="False"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtWhsShpToAddress" CssClass="popup_textarea" runat="server" Width="135px"
                                                TextMode="MultiLine" Rows="4" ReadOnly="False"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlTaxCode" CssClass="dropdown_pop" runat="server" Width="100px">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDiscount" CssClass="popup_input" runat="server" Width="50px"
                                                onkeyup="return funOpenDiscount();" onkeydown="return funOpenDiscount();" onclick="return funOpenDiscount();"
                                                ValidationGroup="lstSrch" MaxLength="3"></asp:TextBox>
                                            <div id="divDiscount" align="left" runat="server" style="position: absolute; display: none;
                                                width: 220;">
                                                <table cellpadding="0" cellspacing="0" width="220">
                                                    <tr>
                                                        <td width='15px' background="../Images/popup_left.png">
                                                        </td>
                                                        <td align="left" valign="middle" class="popup_title_newbg">
                                                            <span class="discount_title">
                                                                <%=lblPrdBlanketDis%>
                                                            </span>
                                                        </td>
                                                        <td class="popup_title_newbg" align="right">
                                                            <a href="javascript:funCloseDiscount();">
                                                                <img alt="" border="0" src='../images/close_popup_btn.gif' width="24" height="24" /></a>
                                                        </td>
                                                        <td width='15px' background="../Images/popup_right.png">
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellpadding="0" cellspacing="0" width="200" class="popup_content_newbg">
                                                    <tr>
                                                        <td height='10'>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdAlign">
                                                            <asp:Label ID="lblDiscount" Text="<%$ Resources:Resource, lblPrdBDis %>" CssClass="lblBold"
                                                                runat="server" />
                                                        </td>
                                                        <td align="left">
                                                            <asp:TextBox ValidationGroup="Dicsount" ID="txtDiscountVal" runat="server" Width="85px" />
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:RadioButtonList ID="rblstDiscountType" ValidationGroup="Dicsount" runat="server"
                                                                RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="<%$ Resources:Resource, lblPrdBlanketDisPercentage %>" Selected="true"
                                                                    Value="P"></asp:ListItem>
                                                                <asp:ListItem Text="<%$ Resources:Resource, lblPrdBlanketDisAbsolute %>" Value="A"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" align="center">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td height="20" align="right" width="45%">
                                                                        <img src="../images/add_icon.gif" alt="add" />
                                                                    </td>
                                                                    <td width="55%">
                                                                        <asp:LinkButton ID="cmdAddDiscount" runat="server" CssClass="lblBold" ValidationGroup="Dicsount"
                                                                            Text="<%$ Resources:Resource, cmdCssAdd %>" AlternateText=""></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:CompareValidator ID="compvalDis" EnableClientScript="true" SetFocusOnError="true"
                                                                ValidationGroup="Dicsount" ControlToValidate="txtDiscountVal" Operator="DataTypeCheck"
                                                                Type="Double" Display="None" ErrorMessage="<%$ Resources:Resource, compvalPrdDiscount %>"
                                                                runat="server" />
                                                            <asp:CustomValidator ID="custvalPrD" runat="server" ValidationGroup="Dicsount" ErrorMessage="<%$ Resources:Resource, CustvalPrdDisParcentage %>"
                                                                ClientValidationFunction="funCalParcentageDiscount" Display="None"></asp:CustomValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height='10'>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:ValidationSummary ID="ValsDiscount" ValidationGroup="Dicsount" runat="server"
                                                    ShowMessageBox="true" ShowSummary="false" />
                                            </div>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuantity" CssClass="popup_input" runat="server" Width="50px"
                                                ValidationGroup="lstSrch"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqvalQuantity" runat="server" Display="None" ErrorMessage="<%$ Resources:Resource, msgSOItemQuantity%>"
                                                SetFocusOnError="true" ValidationGroup="lstSrch" ControlToValidate="txtQuantity">
                                            </asp:RequiredFieldValidator>
                                            <asp:RangeValidator ID="rngvalQuantity" runat="server" Display="None" ErrorMessage="<%$ Resources:Resource, msgSOItemQuantityGT0LessThanOnHand%>"
                                                SetFocusOnError="true" ValidationGroup="lstSrch" ControlToValidate="txtQuantity"
                                                Type="Double" MinimumValue="0.001" MaximumValue="100000">
                                            </asp:RangeValidator>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPrice" CssClass="popup_input" runat="server" Width="50px" ValidationGroup="lstSrch">
                                            </asp:TextBox><br />
                                            <asp:LinkButton runat="server" ID="lbnPriceRange" Text="<%$ Resources:Resource, lblSuggestedPrice%>"></asp:LinkButton>
                                            <asp:RequiredFieldValidator ID="reqvalPrice" runat="server" Display="None" ErrorMessage="<%$ Resources:Resource, msgSOItemPrice%>"
                                                SetFocusOnError="true" ValidationGroup="lstSrch" ControlToValidate="txtPrice">
                                            </asp:RequiredFieldValidator><asp:RangeValidator ID="rngvalPrice" runat="server"
                                                Display="None" ErrorMessage="<%$ Resources:Resource, msgSOItemPriceGT0%>" SetFocusOnError="true"
                                                ValidationGroup="lstSrch" ControlToValidate="txtPrice" Type="double" MinimumValue="0.001"
                                                MaximumValue="1000000000">
                                            </asp:RangeValidator>
                                            <!--Division for Suggested Price-->
                                            <div id="divPriceRnge" align="left" runat="server" style="width: 300px; position: absolute;
                                                top: 5px; left: 200px; text-align: center;">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width='15px' background="../Images/popup_left.png">
                                                        </td>
                                                        <td align="center" valign="middle" class="popup_title_newbg">
                                                            <span class="discount_title">
                                                                <asp:Label ID="lblPriceRange" runat="server" Text="<%$ Resources:Resource, lblSuggestedPrice%>"></asp:Label></span>
                                                        </td>
                                                        <td class="popup_title_newbg" align="right">
                                                            <a href="javascript:funClosePriceRangeLoc();">
                                                                <img alt="" border="0" src='../images/close_popup_btn.gif' width="24" height="24" /></a>
                                                        </td>
                                                        <td width='15px' background="../Images/popup_right.png">
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table id="tblRangePrice" border="0" cellpadding="2" cellspacing="2" width="300"
                                                    style="background: url('../images/popup_content_newbg.gif') repeat-x bottom;
                                                    border-style: solid; border-width: thin;">
                                                    <tr>
                                                        <td align="center">
                                                            <div>
                                                                <asp:GridView ID="grdvwSuggestedPrice" runat="server" AllowPaging="True" PageSize="10"
                                                                    PagerSettings-Mode="Numeric" CellPadding="0" GridLines="none" AutoGenerateColumns="False"
                                                                    CssClass="view_grid" Style="border-collapse: separate;" DataSourceID="sqldsPriceRange"
                                                                    Width="270px">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="" HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <input type="radio" value="<%# Eval("id") %>" name="rbSuggestedPrice" />
                                                                                <%-- <asp:RadioButton id="rdbSelectedPrice" runat="server" GroupName="gnPriceRange"></asp:RadioButton>--%>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="fromQty" HeaderText="From Qty" ReadOnly="True">
                                                                            <ItemStyle Width="50px" Wrap="True" HorizontalAlign="Center" />
                                                                            <HeaderStyle ForeColor="White" Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="toQty" HeaderText="To Qty" ReadOnly="True">
                                                                            <ItemStyle Width="50px" Wrap="True" HorizontalAlign="Center" />
                                                                            <HeaderStyle ForeColor="White" Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="SalesPrice" HeaderText="Sale Price" ReadOnly="True">
                                                                            <ItemStyle Width="50px" Wrap="True" HorizontalAlign="Center" />
                                                                            <HeaderStyle ForeColor="White" Wrap="False" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="grid_rowstyle" />
                                                                    <PagerStyle CssClass="grid_footer" />
                                                                    <HeaderStyle CssClass="grid_header" Height="15" />
                                                                    <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                                                                    <PagerSettings PageButtonCount="5" />
                                                                    <AlternatingRowStyle BackColor="#EDEDEE" ForeColor="#284775" />
                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center; border: none;">
                                                                            <br />
                                                                            <br />
                                                                            <b>
                                                                                <%= lblNoRecordFound %>
                                                                            </b>
                                                                            <br />
                                                                            <br />
                                                                        </div>
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                                <asp:SqlDataSource ID="sqldsPriceRange" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                                                    ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 10px;">
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="text-align: center;">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td height="20" align="center" width="20%">
                                                                    </td>
                                                                    <td width="80%">
                                                                        <div class="buttonwrapper">
                                                                            <a id="cmdSuggestedPriceOK" onclick="javascript:getSelectedRadio();" runat="server"
                                                                                class="ovalbutton" href="#"><span class="ovalbutton" style="min-width: 50px; text-align: center;
                                                                                    margin-right: 10px;">
                                                                                    <%=Resources.Resource.Ok%>
                                                                                </span></a><a id="cmdSuggestedPriceCancel" runat="server" class="ovalbutton" href="javascript:funClosePriceRangeLoc();">
                                                                                    <span class="ovalbutton" style="min-width: 50px; text-align: center;">
                                                                                        <%=Resources.Resource.cmdCssCancel%>
                                                                                    </span></a>
                                                                        </div>
                                                                        <asp:HiddenField ID="hdnSelectedId" runat="server"></asp:HiddenField>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <!--Division End for Suggested Price-->
                                        </td>
                                        <td>
                                        </td>
                                        <td width="80px" align="center">
                                            <asp:ImageButton ID="imgAdd" runat="server" ValidationGroup="lstSrch" ImageUrl="../images/add_pop_btn.gif"
                                                AlternateText=""></asp:ImageButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" height="6">
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                    </div>
                    <asp:GridView ID="grdOrdItems" runat="server" AllowSorting="false" DataSourceID="sqldsOrdItems"
                        AllowPaging="True" PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0"
                        GridLines="none" AutoGenerateColumns="False" Style="border-collapse: separate;"
                        CssClass="view_grid650" UseAccessibleHeader="False" DataKeyNames="orderItemID"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="ordProductID" HeaderText="<%$ Resources:Resource, POProductID %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff" Visible="true">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="prdIntID" HeaderText="<%$ Resources:Resource, POInternalID %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="prdExtID" HeaderText="<%$ Resources:Resource, POExternalID %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="prdUPCCode" HeaderText="<%$ Resources:Resource, grdPOUPCCode %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="prdName" HeaderText="<%$ Resources:Resource, POProduct %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle CssClass="col_prd_name" Width="200px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdPOShippingAddress %>" HeaderStyle-ForeColor="#ffffff"
                                HeaderStyle-HorizontalAlign="left" Visible="False">
                                <ItemTemplate>
                                    <div>
                                        <%--<%#funAddressListing(Eval("ordShpWhsCode"))%>--%>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="120px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="sysTaxCodeDescText" NullDisplayText="--" HeaderStyle-HorizontalAlign="center"
                                HeaderText="<%$ Resources:Resource, POTaxGrp %>" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordProductDiscount" NullDisplayText="0" HeaderStyle-HorizontalAlign="Right"
                                HeaderText="<%$ Resources:Resource, grdSaleDiscount %>" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordProductQty" HeaderText="<%$ Resources:Resource, POQuantity %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordProductUnitPrice" HeaderText="<%$ Resources:Resource, POUnitPrice %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, lblSuggestedPrice %>" HeaderStyle-ForeColor="#ffffff"
                                HeaderStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblSuggestPrice" Text="" ForeColor="Black"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="120px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="ordCurrencyCode" HeaderText="" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="amount" HeaderText="<%$ Resources:Resource, POAmount %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordShpWhsCode" Visible="false" HeaderText="<%$ Resources:Resource, POWarehouseCode %>"
                                ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, POConfirm %>" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-ForeColor="#ffffff" Visible="false">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdEdit %>" Visible="true"
                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("orderItemID") %>'
                                        CommandName="Edit" ImageUrl="~/images/edit_icon.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" Visible="true"
                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                        CommandArgument='<%# Eval("orderItemID") %>' CommandName="Delete" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <div style="display: none;">
                                        <asp:TextBox runat="server" Width="0px" ID="txtDisType" Text='<%# Eval("ordProductDiscountType") %>'>
                                        </asp:TextBox>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="0" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="grid_footer" />
                        <RowStyle CssClass="grid_rowstyle" />
                        <PagerStyle CssClass="grid_footer" />
                        <HeaderStyle CssClass="grid_header" Height="26px" />
                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                        <PagerSettings PageButtonCount="20" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqldsOrdItems" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                </td>
            </tr>
            <tr runat="server" id="trCommission">
                <td colspan="2" align="right" style="padding-right: 10px;">
                    <asp:HiddenField runat="server" ID="hdnSaleAgentID" />
                    <asp:HiddenField runat="server" ID="hdnOrderID" />
                    <br />
                    <asp:Label ID="lblAgent" runat="server" Text="<%$ Resources:Resource, lblCommissionAgentName %>"
                        CssClass="lblBold"></asp:Label>&nbsp;&nbsp;
                    <asp:Label ID="lblCommAgentName" runat="server" Text="" CssClass="lblBold"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblCom" runat="server" Text="<%$ Resources:Resource, lblSalesCommission %>"
                        CssClass="lblBold"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:Label ID="lblCommission"
                            runat="server" Text="" CssClass="lblBold"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdAlignLeft" colspan="2">
                    <asp:UpdatePanel ID="upnlProductItems1" runat="server">
                        <ContentTemplate>
                            <div id="divSalesProcess" runat="server" style="display: none; width: 805;">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td width='15px' background="../Images/popup_left.png">
                                        </td>
                                        <td class="popup_title_newbg">
                                            <div style="display: none;">
                                                <asp:Label ID="lblProcessID" Text="Process ID:" runat="server" /></div>
                                        </td>
                                        <td class="popup_title_newbg" align="left">
                                            <asp:Label ID="lblProcessDescription" runat="server" Text="<%$ Resources:Resource, lblProcessDescription %>" />
                                        </td>

                                          <td class="popup_title_newbg" width="160">
                                                        <asp:Label ID="Label11" runat="server"  Text="Tax Group"></asp:Label>
                                                    </td>
                                                   



                                        <td class="popup_title_newbg" align="left">
                                            <asp:Label ID="lblProcessFixedCost" Text="<%$ Resources:Resource, lblProcessFixedCost %>"
                                                runat="server" />
                                        </td>
                                        <td class="popup_title_newbg" align="left">
                                            <asp:Label ID="lblProcessCostPerHour" Text="<%$ Resources:Resource, lblProcessCostPerHour %>"
                                                runat="server" />
                                        </td>
                                        <td class="popup_title_newbg" align="left">
                                            <asp:Label ID="lblProcessCostPerUnit" Text="<%$ Resources:Resource, lblProcessCostPerUnit%>"
                                                runat="server" />
                                        </td>
                                        <td class="popup_title_newbg" align="left">
                                            <asp:Label ID="lblTotalHour" Text="<%$ Resources:Resource, lblSOTotalHour%>" runat="server" />
                                        </td>
                                        <td class="popup_title_newbg" align="left">
                                            <asp:Label ID="lblTotalUnit" Text="<%$ Resources:Resource, lblSOTotalUnit%>" runat="server" />
                                        </td>
                                        <td width="90" class="popup_title_newbg" align="right">
                                            <a href="javascript:funCloseProcessLoc();" name="Close">
                                                <img alt="Close" src="../images/close_popup_btn.gif" border="0" />
                                            </a>
                                        </td>
                                        <td width='15px' background="../Images/popup_right.png">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="12">
                                            <table cellpadding="0" width="100%" cellspacing="0" class="popup_content_newbg">
                                                <tr>
                                                    <td width="15">
                                                    </td>
                                                    <td>
                                                        <div style="display: none;">
                                                            <asp:TextBox ID="txtProcessID" runat="server" Width="0px" ReadOnly="true" BackColor="LightGray"></asp:TextBox></div>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtProcessDescription" CssClass="popup_input" runat="server" Width="140px"
                                                            ReadOnly="true" BackColor="LightGray">
                                                        </asp:TextBox>
                                                    </td>
                                                    <td>
                                                    <asp:DropDownList ID="dlTaXGup1" CssClass="dropdown_pop" runat="server" Width="160px">
                                                        </asp:DropDownList>
                                                        </td>
                                                    <td>
                                                    <td>
                                                        <asp:TextBox ID="txtProcessFixedCost" CssClass="popup_input" runat="server" Width="75px"
                                                            ValidationGroup="lstSrch">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqvalQuantity1" runat="server" ControlToValidate="txtProcessFixedCost"
                                                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntFixedCost%>" SetFocusOnError="true"
                                                            Display="None" ValidationGroup="valProcess">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtProcessCostPerHour" CssClass="popup_input" runat="server" Width="75px"
                                                            ValidationGroup="lstSrch">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqvalPrice1" runat="server" ControlToValidate="txtProcessCostPerHour"
                                                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntCostPerHour%>" SetFocusOnError="true"
                                                            Display="None" ValidationGroup="valProcess">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtProcessCostPerUnit" CssClass="popup_input" runat="server" Width="75px"
                                                            ValidationGroup="lstSrch">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqvalProcessCostPerUnit" runat="server" ControlToValidate="txtProcessCostPerUnit"
                                                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntCostPerUnit%>" SetFocusOnError="true"
                                                            Display="None" ValidationGroup="valProcess">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTotalHour" CssClass="popup_input" runat="server" Width="75px"
                                                            ValidationGroup="valProcess">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqvalTotalHour" runat="server" ControlToValidate="txtTotalHour"
                                                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntTotalHour%>" SetFocusOnError="true"
                                                            Display="None" ValidationGroup="valProcess">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTotalUnit" CssClass="popup_input" runat="server" Width="75px"
                                                            ValidationGroup="valProcess">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="reqvalTotalUnit" runat="server" ControlToValidate="txtTotalUnit"
                                                            ErrorMessage="<%$ Resources:Resource, msgSOPlzEntTotalUnit%>" SetFocusOnError="true"
                                                            Display="None" ValidationGroup="valProcess">
                                                        </asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="imgAddProcess" runat="server" ImageUrl="../images/update.png"
                                                            AlternateText="" ValidationGroup="valProcess" />
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="imgAddProcess" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr style="display:none;">
                <td align="center" colspan="2">
                    <table width="100%" >
                        <tr>
                            <td align="left" width="75%">
                                <asp:Label ID="lblProcessList" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOProcessItemList %>"
                                    runat="server" Visible="false" />
                            </td>
                            <td align="left" width="25%">
                                <div class="buttonwrapper">
                                    <a id="CmdAddSalesProcess" runat="server" causesvalidation="false" class="ovalbutton"
                                        href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.btnAddSalesProcess%>
                                        </span></a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="top" align="left">
                    <asp:GridView ID="grdAddProcLst" runat="server" AllowSorting="False" AllowPaging="True"
                        PageSize="15" PagerSettings-Mode="Numeric" CellPadding="0" 
                        GridLines="none" AutoGenerateColumns="False"
                        DataSourceID="sqldsAddProcLst" Style="border-collapse: separate;" CssClass="view_grid650"
                        UseAccessibleHeader="False" DataKeyNames="orderItemProcID" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="orderItemProcID" HeaderStyle-ForeColor="#ffffff" HeaderText='ID:" '
                                Visible="false" ReadOnly="True" SortExpression="orderItemProcID">
                                <ItemStyle Width="70px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcCode" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdProcessCode %>"
                                Visible="false" ReadOnly="True" SortExpression="ordItemProcCode">
                                <ItemStyle Width="120px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ProcessDescription" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdProcessDescription %>"
                                ReadOnly="True" SortExpression="ProcessDescription">
                                <ItemStyle Width="250px" Wrap="true" HorizontalAlign="Left" />
                            </asp:BoundField>


                            
                                         <asp:BoundField DataField="sysTaxCodeDescText" HeaderStyle-ForeColor="#ffffff" HeaderText="<%$ Resources:Resource, grdTaxDescription %>"
                                            ReadOnly="True" SortExpression="ProcessDescription">
                                            <ItemStyle Width="250px" Wrap="true" HorizontalAlign="Left" />
                                        </asp:BoundField>


                            <asp:BoundField DataField="ordItemProcFixedPrice" HeaderText="<%$ Resources:Resource, grdProcessFixedCost %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcPricePerHour" HeaderStyle-HorizontalAlign="Right"
                                HeaderText="<%$ Resources:Resource, grdProcessCostPerHour %>" ReadOnly="True"
                                HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcPricePerUnit" HeaderText="<%$ Resources:Resource, grdProcessCostPerUnit %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcHours" HeaderText="<%$ Resources:Resource, grdSOTotalHour %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ordItemProcUnits" HeaderText="<%$ Resources:Resource, grdSOTotalUnit %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ProcessCost" HeaderText="<%$ Resources:Resource, grdSOProcessCost %>"
                                HeaderStyle-HorizontalAlign="Right" ReadOnly="True" HeaderStyle-ForeColor="#ffffff">
                                <ItemStyle Width="80px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>

                             <asp:BoundField DataField="sysTaxCodeDescID" HeaderStyle-ForeColor="#ffffff" HeaderText="Tax Code"
                                ReadOnly="True" SortExpression="ordItemProcCode">
                                <ItemStyle Width="50px" Wrap="true" HorizontalAlign="Right" />
                            </asp:BoundField>


                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdEdit %>" Visible="true"
                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# Eval("orderItemProcID") %>'
                                        CommandName="Edit" ImageUrl="~/images/edit_icon.png" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, grdDelete %>" Visible="true"
                                HeaderStyle-ForeColor="#ffffff" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon.png"
                                        CausesValidation="false" CommandArgument='<%# Eval("orderItemProcID") %>' CommandName="Delete" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="grid_footer" />
                        <RowStyle CssClass="grid_rowstyle" />
                        <PagerStyle CssClass="grid_footer" />
                        <HeaderStyle CssClass="grid_header" Height="26px" />
                        <AlternatingRowStyle CssClass="grid_alter_rowstyle" />
                        <PagerSettings PageButtonCount="20" />
                    </asp:GridView>
                    <asp:SqlDataSource ID="sqldsAddProcLst" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.Odbc"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div id="divTerms" runat="server" align="center">
                        <table width="100%" style="border: 0;" border="0" align="center">
                            <tr>
                                <td colspan="5">
                                    <%--Ship To Address Start--%>
                                    <div id="divShipToAddress" runat="server" style="display: none; position: relative;
                                        left: 0px; top: 0px; border: 3px; border-color: black; margin: 0; height: 0;
                                        width: 350px;">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width='15px' background="../Images/popup_left.png">
                                                </td>
                                                <td align="left" valign="middle" class="popup_title_newbg">
                                                    <span class="discount_title">
                                                        <%=titleShpToAddress%>
                                                    </span>
                                                </td>
                                                <td class="popup_title_newbg" align="right">
                                                    <a href="javascript:funClosePopUp('<%=divShipToAddress.ClientID %>');">
                                                        <img alt="" border="0" src='../images/close_popup_btn.gif' width="24" height="24" /></a>
                                                </td>
                                                <td width='15px' background="../Images/popup_right.png">
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" class="popup_content_newbg">
                                            <tr height="30">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblShpToAddressLine1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressLine1%>">
                                                    </asp:Label>
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:TextBox ID="txtShpToAddressLine1" runat="server" MaxLength="35" Width="200px"
                                                        CssClass="popup_input">
                                                    </asp:TextBox>
                                                    <span class="style1">*</span>
                                                    <asp:RequiredFieldValidator ID="reqvalShpToAddressLine1" runat="server" ValidationGroup="valsShpToAddress"
                                                        ControlToValidate="txtShpToAddressLine1" ErrorMessage="<%$ Resources:Resource, reqvalShpToAddressLine1 %>"
                                                        SetFocusOnError="true" Display="None">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr height="30">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblShpToAddressLine2" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressLine2%>">
                                                    </asp:Label>
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:TextBox ID="txtShpToAddressLine2" runat="server" MaxLength="35" CssClass="popup_input"
                                                        Width="200px">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr height="30">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblShpToAddressLine3" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressLine3%>">
                                                    </asp:Label>
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:TextBox ID="txtShpToAddressLine3" runat="server" MaxLength="35" CssClass="popup_input"
                                                        Width="200px">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr height="30">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblShpToAddressCity" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressCity%>"
                                                        runat="server" />
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:TextBox ID="txtShpToAddressCity" runat="server" Width="200px" MaxLength="25"
                                                        CssClass="popup_input" />
                                                    <span class="style1">*</span>
                                                    <asp:RequiredFieldValidator ID="reqvalShpToAddressCity" runat="server" ValidationGroup="valsShpToAddress"
                                                        ControlToValidate="txtShpToAddressCity" ErrorMessage="<%$ Resources:Resource, PlzEntShpToAddressCity %>"
                                                        SetFocusOnError="true" Display="None">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr height="30" id="trShpToState" runat="server" visible="true">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblShpToAddressState" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressState%>"
                                                        runat="server" />
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:TextBox ID="txtShpToAddressState" runat="server" Width="200px" MaxLength="25"
                                                        CssClass="popup_input" />
                                                    <span class="style1">*</span>
                                                    <asp:RequiredFieldValidator ID="reqvalShpToAddressState" runat="server" ValidationGroup="valsShpToAddress"
                                                        ControlToValidate="txtShpToAddressState" ErrorMessage="<%$ Resources:Resource, PlzEntShpToState %>"
                                                        SetFocusOnError="true" Display="None">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr height="30" id="trShpToCountry" runat="server" visible="true">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblShpToAddressCountry" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressCountry%>"
                                                        runat="server" />
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:TextBox ID="txtShpToAddressCountry" runat="server" Width="200px" MaxLength="25"
                                                        CssClass="popup_input" />
                                                </td>
                                            </tr>
                                            <tr height="30">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblShpToAddressPostalCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressPostalCode %>"
                                                        runat="server" />
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:TextBox ID="txtShpToAddressPostalCode" runat="server" Width="200Px" MaxLength="10"
                                                        CssClass="popup_input" />
                                                    <span class="style1">*</span>
                                                    <asp:RequiredFieldValidator ID="reqvalShpToAddressPostalCode" runat="server" ValidationGroup="valsShpToAddress"
                                                        ControlToValidate="txtShpToAddressPostalCode" ErrorMessage="<%$ Resources:Resource, PlzEntShpToAddressPostalCode %>"
                                                        SetFocusOnError="true" Display="None">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                                                    <table width="100%">
                                                        <tr>
                                                            <td height="20" align="right" width="50%">
                                                                <img src="../images/add_icon.gif" alt="add" />
                                                            </td>
                                                            <td width="50%">
                                                                <asp:LinkButton ID="cmdShpToAddress" runat="server" CssClass="lblBold" ValidationGroup="valsShpToAddress"
                                                                    Text="<%$ Resources:Resource, cmdCssAdd %>" AlternateText=""></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <%--Ship To Address End--%>
                                    <%--Bill To Address Start--%>
                                    <div id="divBillToAddress" runat="server" style="display: none; position: relative;
                                        left: 380px; top: 0px; width: 350px; border: 3px; border-color: black; margin: 0;
                                        height: 0;">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width='15px' background="../Images/popup_left.png">
                                                </td>
                                                <td align="left" valign="middle" class="popup_title_newbg">
                                                    <span class="discount_title">
                                                        <%=titleBillToAddress%>
                                                    </span>
                                                </td>
                                                <td class="popup_title_newbg" align="right">
                                                    <a href="javascript:funClosePopUp('<%=divBillToAddress.ClientID %>');">
                                                        <img alt="" border="0" src='../images/close_popup_btn.gif' width="24" height="24" /></a>
                                                </td>
                                                <td width='15px' background="../Images/popup_right.png">
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" class="popup_content_newbg">
                                            <tr height="30">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblBillToAddressLine1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressLine1%>">
                                                    </asp:Label>
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:TextBox ID="txtBillToAddressLine1" runat="server" MaxLength="35" Width="200px"
                                                        CssClass="popup_input">
                                                    </asp:TextBox>
                                                    <span class="style1">*</span>
                                                    <asp:RequiredFieldValidator ID="reqvalBillToAddressLine1" runat="server" ValidationGroup="valsBillToAddress"
                                                        ControlToValidate="txtBillToAddressLine1" ErrorMessage="<%$ Resources:Resource, reqvalBillToAddressLine1 %>"
                                                        SetFocusOnError="true" Display="None">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr height="30">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblBillToAddressLine2" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressLine2%>">
                                                    </asp:Label>
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:TextBox ID="txtBillToAddressLine2" runat="server" MaxLength="35" CssClass="popup_input"
                                                        Width="200px">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr height="30">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblBillToAddressLine3" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressLine3%>">
                                                    </asp:Label>
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:TextBox ID="txtBillToAddressLine3" runat="server" MaxLength="35" CssClass="popup_input"
                                                        Width="200px">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr height="30">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblBillToAddressCity" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressCity%>"
                                                        runat="server" />
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:TextBox ID="txtBillToAddressCity" runat="server" Width="200px" MaxLength="25"
                                                        CssClass="popup_input" />
                                                    <span class="style1">*</span>
                                                    <asp:RequiredFieldValidator ID="reqvalBillToAddressCity" runat="server" ValidationGroup="valsBillToAddress"
                                                        ControlToValidate="txtBillToAddressCity" ErrorMessage="<%$ Resources:Resource, PlzEntBillToAddressCity %>"
                                                        SetFocusOnError="true" Display="None">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr height="30" id="trBillToState" runat="server" visible="true">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblBillToAddressState" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressState%>"
                                                        runat="server" />
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:TextBox ID="txtBillToAddressState" runat="server" Width="200px" MaxLength="25"
                                                        CssClass="popup_input" />
                                                    <span class="style1">*</span>
                                                    <asp:RequiredFieldValidator ID="reqvalBillToAddressState" runat="server" ValidationGroup="valsBillToAddress"
                                                        ControlToValidate="txtBillToAddressState" ErrorMessage="<%$ Resources:Resource, PlzEntBillToState %>"
                                                        SetFocusOnError="true" Display="None">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr height="30" id="trBillToCountry" runat="server" visible="true">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblBillToAddressCountry" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressCountry%>"
                                                        runat="server" />
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:TextBox ID="txtBillToAddressCountry" runat="server" Width="200px" MaxLength="25"
                                                        CssClass="popup_input" />
                                                </td>
                                            </tr>
                                            <tr height="30">
                                                <td class="tdAlign">
                                                    <asp:Label ID="lblBillToAddressPostalCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblBillToAddressPostalCode %>"
                                                        runat="server" />
                                                </td>
                                                <td class="tdAlignLeft">
                                                    <asp:TextBox ID="txtBillToAddressPostalCode" runat="server" Width="200Px" MaxLength="10"
                                                        CssClass="popup_input" />
                                                    <span class="style1">*</span>
                                                    <asp:RequiredFieldValidator ID="reqvalBillToAddressPostalCode" runat="server" ValidationGroup="valsBillToAddress"
                                                        ControlToValidate="txtBillToAddressPostalCode" ErrorMessage="<%$ Resources:Resource, PlzEntBillToAddressPostalCode %>"
                                                        SetFocusOnError="true" Display="None">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                                                    <table width="100%">
                                                        <tr>
                                                            <td height="20" align="right" width="50%">
                                                                <img src="../images/add_icon.gif" alt="add" />
                                                            </td>
                                                            <td width="50%">
                                                                <asp:LinkButton ID="cmdBillToAddress" runat="server" CssClass="lblBold" ValidationGroup="valsBillToAddress"
                                                                    Text="<%$ Resources:Resource, cmdCssAdd %>" AlternateText=""></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <%--<asp:Button runat="server" ID="cmdBillToAddress" CssClass="imgSave" ValidationGroup="valsBillToAddress"  />--%>
                                                </td>
                                            </tr>
                                            <%--Bill To Address End--%>
                                        </table>
                                    </div>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                                <td colspan="2" align="right">
                                    <table align="right" cellpadding="2" >
                                        <tr>
                                            <td align="right">
                                                <asp:Label ID="lblSubTotal1" runat="server" CssClass="lblBold" Text="Product Sub Total">
                                                </asp:Label>
                                                <%--<%$ Resources:Resource, lblSOSubTotal %>--%>
                                            </td>
                                            <td align="right">
                                                <asp:Label ID="lblSubTotal" runat="server" CssClass="lblBold"></asp:Label>
                                            </td>
                                        </tr>

                                        <%=TaxString %>
                                        <tr>
                                            <td align="right">
                                                <asp:Label ID="lblSubTotalProcessItem"  runat="server" CssClass="lblBold" Text="Process Item Sub Total">
                                                </asp:Label>
                                            </td>
                                            <td align="right">
                                                <asp:Label ID="lblSubTotalProcessItemAmount"  runat="server" CssClass="lblBold"></asp:Label>
                                            </td>
                                        </tr>
                                        <%= TaxStringForProcessItem%>

                                           <tr>
                                            <td colspan="2" align="right">
                                               <div style="border-bottom:1px dashed #000;" >&nbsp;</div>
                                             <%-- ---------------------------------------------%>
                                            </td>
                                        </tr>
                                       
                                        <tr>
                                            <td align="right">
                                                <asp:Label ID="lblAmount1" Text="<%$ Resources:Resource, lblSOTotal %>" CssClass="lblBold"
                                                    runat="server" />
                                            </td>
                                            <td align="right">
                                                <asp:Label ID="lblAmount" CssClass="lblBold" runat="server" />
                                            </td>
                                        </tr>
                                        <asp:Literal ID="ltPartialPayment" Text="" runat="server" />
                                    </table>
                                </td>
                            </tr>
                            <% If Session("UserModules").ToString.Contains("SAL") = True Or Session("UserModules").ToString.Contains("ADM") = True Then%>
                            <% If ConfigurationManager.AppSettings("AllowCustomInvoiceEntry").ToLower = "true" Then%>
                            <tr>
                                <td class="tdAlign">
                                    <asp:Label ID="Label9" CssClass="lblBold" Text="<%$ Resources:Resource, lblINInvoiceNo %>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtInvNo" Width="150px" runat="server"></asp:TextBox>
                                    <asp:RangeValidator ID="regVal" runat="server" ValidationGroup="Sale" Display="None"
                                        ErrorMessage="<%$ Resources:Resource, msgInvNoGreThanZero%>" SetFocusOnError="true"
                                        ControlToValidate="txtInvNo" Type="double" MinimumValue="0.001" MaximumValue="100000000000"></asp:RangeValidator>
                                    <%-- <asp:RequiredFieldValidator ID="reqvalInvNo" runat="server" 
                 ControlToValidate="txtInvNo" 
                ErrorMessage="<%$ Resources:Resource, lblPlzEntInvNo %>" 
                SetFocusOnError=true Display="None"></asp:RequiredFieldValidator>--%>
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="Label10" CssClass="lblBold" Text="<%$ Resources:Resource, lblINInvoiceDate %>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtInvDate" Width="75px" MaxLength="15" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="imgInv" CausesValidation="false" runat="server" ToolTip="Click to show calendar"
                                        ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                                    <%=lblPODateFormat%>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" TargetControlID="txtInvDate"
                                        runat="server" PopupButtonID="imgInv" Format="MM/dd/yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                    <%-- <asp:RequiredFieldValidator ID="reqvalInvDate" runat="server" 
                ControlToValidate="txtInvDate" 
                ErrorMessage="<%$ Resources:Resource, lblPlzEntInvDate %>" 
                SetFocusOnError=true Display="None">
                </asp:RequiredFieldValidator>--%>
                                    <div style="display: none;">
                                        <asp:TextBox ID="txtDate" Enabled="true" runat="server" Width="0px" MaxLength="15">
                                        </asp:TextBox>
                                    </div>
                                    <asp:CompareValidator ID="comvalInvDate" EnableClientScript="true" ValidationGroup="Sale"
                                        SetFocusOnError="true" ControlToValidate="txtInvDate" ControlToCompare="txtDate"
                                        Operator="DataTypeCheck" Type="Date" Display="None" ErrorMessage="<%$ Resources:Resource, lblValidInvoiceDate %>"
                                        runat="server" />
                                </td>
                            </tr>
                            <% End If%>
                            <tr height="25">
                                <td class="tdAlign" width="23%">
                                    <asp:Label ID="lblShpToAddress" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShpToAddress %>"
                                        runat="server" />
                                </td>
                                <td width="1%">
                                </td>
                                <td align="left" width="28%">
                                    <asp:TextBox ID="txtShpToAddress" runat="server" TextMode="MultiLine" Rows="5" ReadOnly="false">
                                    </asp:TextBox>
                                </td>
                                <td width="1%">
                                </td>
                                <td class="tdAlign" width="24%">
                                    <asp:Label ID="lblBillToAddress" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOBillToAddress %>"
                                        runat="server" />
                                </td>
                                <td width="1%">
                                </td>
                                <td align="left" width="22%">
                                    <asp:TextBox ID="txtBillToAddress" runat="server" TextMode="MultiLine" Rows="5" ReadOnly="false">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblTerms" CssClass="lblBold" Text="<%$ Resources:Resource, POShippingTerms%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtTerms" runat="server" TextMode="MultiLine" Rows="3">
                                    </asp:TextBox>
                                    <asp:ValidationSummary ID="valsSales" ValidationGroup="Sale" runat="server" ShowMessageBox="true"
                                        ShowSummary="false" />
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="lblNetTerms" CssClass="lblBold" Text="<%$ Resources:Resource, lblSONetTerms%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtNetTerms" MaxLength="4" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr height="25" id="trPrefAndIsWeb" runat="server" style="display:none;">
                                <td class="tdAlign">
                                    <asp:Label ID="lblResellerShipBlankPref" CssClass="lblBold" Text="<%$ Resources:Resource, lblResellerShipBlankPref%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:RadioButtonList ID="rblstShipBlankPref" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="<%$ Resources:Resource, lblYes%>" Value="1">
                                        </asp:ListItem>
                                        <asp:ListItem Text="<%$ Resources:Resource, lblNo%>" Value="0" Selected="True">
                                        </asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="lblISWeb" Text="<%$ Resources:Resource, lblSOSalesISWeb%>" CssClass="lblBold"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:RadioButtonList ID="rblstISWeb" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdYes %>" Value="1">
                                        </asp:ListItem>
                                        <asp:ListItem Text="<%$ Resources:Resource, lblPrdNo %>" Value="0" Selected="True">
                                        </asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr height="25" id="trShpWHSAndTrackNo" runat="server">
                                <td class="tdAlign">
                                    <asp:Label ID="lblShpWarehouse" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingWarehouse %>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="dlShpWarehouse" runat="server" Width="155px" ValidationGroup="Sale">
                                    </asp:DropDownList>
                                    <asp:CustomValidator ID="custvalShpWarehouse" runat="server" SetFocusOnError="true"
                                        ErrorMessage="<%$ Resources:Resource, lblSOPlzSelShippingWarehouse %>" ClientValidationFunction="funCheckShippingWarehouseCode"
                                        Display="None" ValidationGroup="Sale">
                                    </asp:CustomValidator>
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="lblShpTrackNo" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingTrackNo %>"
                                        runat="server" Visible="false" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtShpTrackNo" runat="server" Width="150px" MaxLength="25" Visible="false">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr height="25" id="trShpCostAndCode" runat="server">
                                <td class="tdAlign">
                                    <asp:Label ID="lblShpDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingDate %>">
                                    </asp:Label>
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtShippingDate" Enabled="true" runat="server" Width="90px" MaxLength="15"
                                        ValidationGroup="Sale">
                                    </asp:TextBox>
                                    <asp:ImageButton ID="imgCalShpDate" CausesValidation="false" runat="server" ToolTip="Click to show calendar"
                                        ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender4" TargetControlID="txtShippingDate"
                                        runat="server" PopupButtonID="imgCalShpDate" Format="MM/dd/yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RequiredFieldValidator ID="reqvalShpDate" runat="server" ControlToValidate="txtShippingDate"
                                        ErrorMessage="<%$ Resources:Resource, lblSOPlzEntShippingDate %>" SetFocusOnError="true"
                                        Display="none" ValidationGroup="Sale">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="lblShpCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingCode %>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtShpCode" runat="server" Width="150px" MaxLength="15">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr height="25" runat="server" visible="false">
                                <td class="tdAlign">
                                    <asp:Label ID="lblShpCost" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOShippingCost %>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left" colspan="5">
                                    <asp:TextBox ID="txtShpCost" runat="server" Width="150px" MaxLength="6" ValidationGroup="Sale">
                                    </asp:TextBox>
                                    <asp:CompareValidator ID="comvalShpCost" EnableClientScript="true" SetFocusOnError="true"
                                        ControlToValidate="txtShpCost" Operator="DataTypeCheck" Type="double" ErrorMessage="<%$ Resources:Resource, lblSOPlzEntNumericValueInShippingCost %>"
                                        Display="none" ValidationGroup="Sale" runat="server" />
                                </td>
                            </tr>
                            <tr height="25" id="trNotes" runat="server">
                                <td class="tdAlign">
                                    <asp:Label ID="lblNotes" CssClass="lblBold" Text="<%$ Resources:Resource, PONotes%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left" colspan="5">
                                    <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Rows="3" Width="550px">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblCustPO" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOCustomerPO%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtCustPO" runat="server" Width="130px" MaxLength="35" onkeypress="return disableEnterKey(event)">
                                    </asp:TextBox>
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="lblPOStatus" CssClass="lblBold" Text="<%$ Resources:Resource, lblStatus%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="dlStatus" runat="server" Width="135px" ValidationGroup="Sale">
                                        <%-- <asp:ListItem Value="" Text="<%$ Resources:Resource, liSelectOrderStatus %>" />
                    <asp:ListItem Value="N" Text="<%$ Resources:Resource, liCreated %>" />
                    <asp:ListItem Value="A" Text="<%$ Resources:Resource, liApproved %>"/>
                    <asp:ListItem Value="P" Text="<%$ Resources:Resource, liInProcess %>"/>
                    <asp:ListItem Value="H" Text="<%$ Resources:Resource, liHeld %>"/>
                    <asp:ListItem Value="S" Text="<%$ Resources:Resource, liShipped %>"/>
                    <asp:ListItem Value="I" Text="<%$ Resources:Resource, liInvoiced %>"/> 
                    <asp:ListItem Value="C" Text="<%$ Resources:Resource, liClosed %>"/>   --%>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnPreStatus" runat="server" />
                                    <span class="style1">*</span>
                                    <asp:CustomValidator ID="custvalStatus" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:Resource, msgSOPlzSelStatus %>"
                                        ClientValidationFunction="funCheckStatus" Display="None" ValidationGroup="Sale">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr height="25">
                                <td class="tdAlign">
                                    <asp:Label ID="lblQutExpDate" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblSOQutExpDate %>">
                                    </asp:Label>
                                </td>
                                <td>
                                </td>
                                <td align="left" valign="middle">
                                    <asp:TextBox ID="txtQutExpDate" ValidationGroup="Sale" Enabled="true" runat="server"
                                        Width="90px" MaxLength="15" onkeypress="return disableEnterKey(event)">
                                    </asp:TextBox>
                                    <asp:ImageButton ID="imgCalQutExpDate" CausesValidation="false" runat="server" ToolTip="Click to show calendar"
                                        ImageUrl="~/images/calendar.gif" ImageAlign="AbsMiddle" />
                                    <%=lblPODateFormat %>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender5" TargetControlID="txtQutExpDate"
                                        runat="server" PopupButtonID="imgCalQutExpDate" Format="MM/dd/yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RequiredFieldValidator ID="reqvalQutExpDate" runat="server" ControlToValidate="txtQutExpDate"
                                        ErrorMessage="<%$ Resources:Resource, lblSOPlzEntQutExpDate %>" SetFocusOnError="true"
                                        Display="None" ValidationGroup="Sale">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td>
                                </td>
                                <td class="tdAlign">
                                    <asp:Label ID="Label4" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblUserOrderType%>"></asp:Label>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:Label ID="lblRejectedReason" Visible="false" CssClass="lblBold" Text="<%$ Resources:Resource, lblRejectedReason%>"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="dlstOderType" runat="server" Width="135px">
                                    </asp:DropDownList>
                                    <br />
                                    <br />
                                    <asp:DropDownList ValidationGroup="Rejected" Visible="false" ID="dlReason" runat="Server"
                                        Width="135px">
                                    </asp:DropDownList>
                                    <asp:CustomValidator ID="custvalReason" Visible="false" ValidationGroup="Rejected"
                                        SetFocusOnError="true" runat="server" ErrorMessage="<%$ Resources:Resource, custvalRejectedReason %>"
                                        ClientValidationFunction="funSelectReason" Display="None"></asp:CustomValidator>
                                    <%--<span class="style1"> *</span>
                <asp:CustomValidator ID="custvalOderType" runat="server" SetFocusOnError="true"
                ErrorMessage="<%$ Resources:Resource, custvalOderType%>" 
                ClientValidationFunction="funCheckOderType" Display="None" ValidationGroup="Sale" >
                </asp:CustomValidator>--%>
                                </td>
                            </tr>
                            <% End If%>
                        </table>
                    </div>
                </td>
            </tr>
            <asp:ValidationSummary ID="valsRejectedReason" runat="server" ShowMessageBox="true"
                ValidationGroup="Rejected" ShowSummary="false" />
            <tr height="30" id="trCurrency" runat="server" visible="false">
                <td class="tdAlign">
                    <asp:Label ID="lblPOCurrencyCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOCurrencyCode%>"
                        runat="server" />
                </td>
                <td class="tdAlignLeft">
                    <asp:DropDownList ID="dlCurrencyCode" runat="server" Width="135px">
                    </asp:DropDownList>
                    <span class="style1">*</span>
                    <asp:CustomValidator ID="custvalPOCurrencyCode" runat="server" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:Resource, custvalPOCurrencyCode%>" ClientValidationFunction="funCheckCurrencyCode"
                        Display="None">
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr height="30" id="trExchageRate" runat="server" visible="false">
                <td class="tdAlign">
                    <asp:Label ID="lblPOExRate" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOExRate%>"
                        runat="server" />
                </td>
                <td class="tdAlignLeft">
                    <asp:TextBox ID="txtExRate" runat="server" Width="130px" MaxLength="5">
                    </asp:TextBox>
                </td>
            </tr>
            <tr height="15">
                <td colspan="2">
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                    <table width="100%">
                        <tr>
                            <td height="20" align="center" width="10%">
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="cmdSave" runat="server" validationgroup="Sale" class="ovalbutton" href="#"><span
                                        class="ovalbutton" style="min-width: 120px; text-align: center;">
                                        <%=Resources.Resource.cmdCssSave%>
                                    </span></a>
                                </div>
                            </td>
                            <td width="20%" align="left">
                                <div class="buttonwrapper">
                                    <a id="cmdSoCancel" runat="server" class="ovalbutton" href="#"><span class="ovalbutton"
                                        style="min-width: 120px; text-align: center;">
                                        <%=Resources.Resource.cmdCssCancel%>
                                    </span></a>
                                </div>
                                <div class="buttonwrapper">
                                    <a id="cmdApprove" runat="server" validationgroup="Sale" class="ovalbutton" href="#">
                                        <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssApprove%>
                                        </span></a>
                                </div>
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="cmdCancel" validationgroup="Rejected" runat="server" class="ovalbutton" href="#">
                                        <span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssReject%>
                                        </span></a>
                                </div>
                                <div class="buttonwrapper">
                                    <a id="cmdCpyOrder" validationgroup="Sale" onclick="cmdCpyOrder_Click" runat="server"
                                        class="ovalbutton" href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssDuplicateOrder%>
                                        </span></a>
                                </div>
                            </td>
                            <td width="20%">
                                <div class="buttonwrapper">
                                    <a id="cmdGnratInvoice" validationgroup="Sale" runat="server" onclick="return confirm('This will generate a new invoice.. ');"
                                        class="ovalbutton" href="#"><span class="ovalbutton" style="min-width: 120px; text-align: center;">
                                            <%=Resources.Resource.cmdCssGenerateInvoice%>
                                        </span></a>
                                </div>
                            </td>
                            <td height="20" align="center" width="10%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%-- <asp:ValidationSummary ID="valsSales" runat="server" ShowMessageBox="true"
        ShowSummary="false" />--%>
        </table>
        <%--Warehouse Shipping Address Start--%>
        <div id="divWhsShpAddress" runat="server" style="display: none; position: absolute;
            left: 400px; top: 300px; width: 400px;">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width='15px' background="../Images/popup_left.png">
                    </td>
                    <td align="left" valign="middle" class="popup_title_newbg">
                        <span class="discount_title">
                            <%=titleShippingAddress%>
                        </span>
                    </td>
                    <td class="popup_title_newbg" align="right">
                        <a href="javascript:funClosePopUp('<%=divWhsShpAddress.ClientID %>');">
                            <img alt="" border="0" src='../images/close_popup_btn.gif' width="24" height="24" /></a>
                    </td>
                    <td width='15px' background="../Images/popup_right.png">
                    </td>
                </tr>
            </table>
            <table width="100%" class="popup_content_newbg">
                <tr height="30">
                    <td class="tdAlign">
                        <asp:Label ID="lblWhsShpToDesc" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblWarehouseDescription%>">
                        </asp:Label>
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtWhsShpToDesc" runat="server" MaxLength="35" CssClass="popup_input"
                            Width="200px">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr height="30">
                    <td class="tdAlign">
                        <asp:Label ID="lblWhsShpToAddressLine1" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressLine1%>">
                        </asp:Label>
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtWhsShpToAddressLine1" runat="server" MaxLength="35" CssClass="popup_input"
                            Width="200px">
                        </asp:TextBox>
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalWhsShpToAddressLine1" runat="server" ValidationGroup="valsWhsShpToAddress"
                            ControlToValidate="txtWhsShpToAddressLine1" ErrorMessage="<%$ Resources:Resource, reqvalShpToAddressLine1 %>"
                            SetFocusOnError="true" Display="None">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr height="30">
                    <td class="tdAlign">
                        <asp:Label ID="lblWhsShpToAddressLine2" runat="server" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressLine2%>">
                        </asp:Label>
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtWhsShpToAddressLine2" runat="server" MaxLength="35" CssClass="popup_input"
                            Width="200px">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr height="30">
                    <td class="tdAlign">
                        <asp:Label ID="lblWhsShpToAddressCity" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressCity%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtWhsShpToAddressCity" runat="server" Width="200px" CssClass="popup_input"
                            MaxLength="25" />
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalWhsShpToAddressCity" runat="server" ValidationGroup="valsWhsShpToAddress"
                            ControlToValidate="txtWhsShpToAddressCity" ErrorMessage="<%$ Resources:Resource, PlzEntShpToAddressCity %>"
                            SetFocusOnError="true" Display="None">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr height="30" id="tr1" runat="server" visible="true">
                    <td class="tdAlign">
                        <asp:Label ID="lblWhsShpToAddressState" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressState%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtWhsShpToAddressState" runat="server" CssClass="popup_input" Width="200px"
                            MaxLength="25" />
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalWhsShpToAddressState" runat="server" ValidationGroup="valsWhsShpToAddress"
                            ControlToValidate="txtWhsShpToAddressState" ErrorMessage="<%$ Resources:Resource, PlzEntShpToState %>"
                            SetFocusOnError="true" Display="None">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr height="30" id="tr2" runat="server" visible="true">
                    <td class="tdAlign">
                        <asp:Label ID="lblWhsShpToAddressCountry" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressCountry%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtWhsShpToAddressCountry" CssClass="popup_input" runat="server"
                            Width="200px" MaxLength="25" />
                    </td>
                </tr>
                <tr height="30">
                    <td class="tdAlign">
                        <asp:Label ID="lblWhsShpToAddressPostalCode" CssClass="lblBold" Text="<%$ Resources:Resource, lblShpToAddressPostalCode %>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtWhsShpToAddressPostalCode" CssClass="popup_input" runat="server"
                            Width="200Px" MaxLength="10" />
                        <span class="style1">*</span>
                        <asp:RequiredFieldValidator ID="reqvalWhsShpToAddressPostalCode" runat="server" ValidationGroup="valsWhsShpToAddress"
                            ControlToValidate="txtWhsShpToAddressPostalCode" ErrorMessage="<%$ Resources:Resource, PlzEntShpToAddressPostalCode %>"
                            SetFocusOnError="true" Display="None">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                        <table width="100%">
                            <tr>
                                <td height="20" align="right" width="50%">
                                    <img src="../images/add_icon.gif" alt="add" />
                                </td>
                                <td width="50%">
                                    <asp:LinkButton ID="cmdWhsShpToAddress" runat="server" CssClass="lblBold" ValidationGroup="valsWhsShpToAddress"
                                        Text="<%$ Resources:Resource, cmdCssAdd %>" AlternateText=""></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <%--
            <asp:Button runat="server" ID="cmdWhsShpToAddress" CssClass="imgSave" ValidationGroup="valsWhsShpToAddress" />--%>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divMail" runat="server" style="display: none; position: absolute; left: 572px;
            top: 170px; width: 395px">
            <table width="100%" cellpadding="0px" cellspacing="0">
                <tr>
                    <td width='15px' background="../Images/popup_left.png">
                    </td>
                    <td align="left" valign="middle" class="popup_title_newbg">
                        <span class="discount_title">
                            <%=lblPOMail%>
                        </span>
                    </td>
                    <td class="popup_title_newbg" align="right">
                        <a href="javascript:funClosePopUp('<%=divMail.ClientID %>');">
                            <img alt="" border="0" src='../images/close_popup_btn.gif' width="24" height="24" /></a>
                    </td>
                    <td width='15px' background="../Images/popup_right.png">
                    </td>
                </tr>
            </table>
            <table width="100%" class="popup_content_newbg">
                <tr>
                    <td class="tdAlign">
                        <asp:Label ID="lblMailTo" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOMailTo%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtMailTo" CssClass="popup_input" runat="server" Width="300px" ValidationGroup="mail">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalMailTo" runat="server" ControlToValidate="txtMailTo"
                            ValidationGroup="mail" Display="None" ErrorMessage="<%$ Resources:Resource, reqvalMailTo%>"
                            SetFocusOnError="true">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revalexpEmail" runat="server" ControlToValidate="txtMailTo"
                            SetFocusOnError="true" ValidationGroup="mail" Display="None" ErrorMessage="<%$ Resources:Resource, revalexpEmail%>"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tdAlign">
                        <asp:Label ID="lblMailSubject" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOSubject%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtMailSubject" CssClass="popup_input" runat="server" Width="300px"
                            ValidationGroup="mail">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdAlign">
                        <asp:Label ID="lblMailMessage" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOMessage%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtMailMessage" runat="server" CssClass="popup_input" TextMode="MultiLine"
                            Rows="4" Width="300px" ValidationGroup="mail">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                        <table width="100%">
                            <tr>
                                <td height="20" align="right" width="50%">
                                    <img src="../images/add_icon.gif" alt="add" />
                                </td>
                                <td width="50%">
                                    <asp:LinkButton ID="cmdMail" runat="server" CssClass="lblBold" ValidationGroup="mail"
                                        Text="<%$ Resources:Resource, cmdCssAdd %>" AlternateText=""></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divFax" runat="server" style="display: none; position: absolute; left: 572px;
            top: 170px; width: 395px;">
            <table width="100%" cellpadding="0px" cellspacing="0">
                <tr>
                    <td width='15px' background="../Images/popup_left.png">
                    </td>
                    <td align="left" valign="middle" class="popup_title_newbg">
                        <span class="discount_title">
                            <%=lblPOFax%>
                        </span>
                    </td>
                    <td class="popup_title_newbg" align="right">
                        <a href="javascript:funClosePopUp('<%=divFax.ClientID %>');">
                            <img alt="" border="0" src='../images/close_popup_btn.gif' width="24" height="24" /></a>
                    </td>
                    <td width='15px' background="../Images/popup_right.png">
                    </td>
                </tr>
            </table>
            <table width="100%" class="popup_content_newbg">
                <tr>
                    <td class="tdAlign">
                        <asp:Label ID="lblFaxTo" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOFaxTo%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtFaxTo" runat="server" Width="300px" ValidationGroup="Fax" CssClass="popup_input">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqvalFax" runat="server" ControlToValidate="txtFaxTo"
                            ValidationGroup="Fax" Display="None" ErrorMessage="<%$ Resources:Resource, reqvalFax%>"
                            SetFocusOnError="true">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="tdAlign">
                        <asp:Label ID="lblAttention" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOAttention%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtAttention" CssClass="popup_input" runat="server" Width="300px"
                            ValidationGroup="Fax">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdAlign">
                        <asp:Label ID="lblFaxSubject" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOSubject%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtFaxSubject" CssClass="popup_input" runat="server" Width="300px"
                            ValidationGroup="Fax">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdAlign">
                        <asp:Label ID="lblFaxMessage" CssClass="lblBold" Text="<%$ Resources:Resource, lblPOMessage%>"
                            runat="server" />
                    </td>
                    <td class="tdAlignLeft">
                        <asp:TextBox ID="txtFaxMessage" CssClass="popup_input" runat="server" TextMode="MultiLine"
                            Rows="4" Width="300px" ValidationGroup="Fax">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2" style="border-top: solid 1px #E6EDF5; padding-top: 8px;">
                        <table width="100%">
                            <tr>
                                <td height="20" align="right" width="50%">
                                    <img src="../images/add_icon.gif" alt="add" />
                                </td>
                                <td width="50%">
                                    <asp:LinkButton ID="cmdFax" runat="server" CssClass="lblBold" ValidationGroup="Fax"
                                        Text="<%$ Resources:Resource, cmdCssAdd %>" AlternateText=""></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <%--Warehouse Shipping Address End--%>
        <asp:ValidationSummary ID="valsWhsShpToAddress" ValidationGroup="valsWhsShpToAddress"
            runat="server" ShowMessageBox="true" ShowSummary="false" />
        <asp:ValidationSummary ID="valsBillToAddress" ValidationGroup="valsBillToAddress"
            runat="server" ShowMessageBox="true" ShowSummary="false" />
        <asp:ValidationSummary ID="valsShpToAddress" ValidationGroup="valsShpToAddress" runat="server"
            ShowMessageBox="true" ShowSummary="false" />
        <asp:ValidationSummary ID="ValsProcess" runat="server" ShowMessageBox="true" ShowSummary="false"
            ValidationGroup="valProcess" />
        <asp:ValidationSummary ID="valsSales1" runat="server" ShowMessageBox="true" ShowSummary="false"
            ValidationGroup="lstSrch" />
        <asp:ValidationSummary ID="valsMail" runat="server" ShowMessageBox="true" ValidationGroup="mail"
            ShowSummary="false" />
        <asp:ValidationSummary ID="valsFax" runat="server" ShowMessageBox="true" ShowSummary="false"
            ValidationGroup="Fax" />
    </div>
    <br />
    <br />
    <script type="text/javascript" language="javascript">

        function funConfirm(id) {
            var frm = document.forms[0];
            for (i = 0; i < frm.elements.length; i++) {
                if (frm.elements[i].type == "checkbox") {
                    if (frm.elements[i].checked == false) {
                        alert('<% =Resources.Resource.POConfirmItems %>');
                        return false;
                    }
                }
            }
            return true;
        }
        function funCheckStatus(source, args) {
            if (document.getElementById('<%=dlStatus.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
        function funCheckShpToAddress(source, args) {
            if (document.getElementById('<%=txtShpToAddress.ClientID%>').value == "") {
                args.IsValid = false;
            }
        }
        function funCheckBillToAddress(source, args) {
            if (document.getElementById('<%=txtBillToAddress.ClientID%>').value == "") {
                args.IsValid = false;
            }
        }
        function openPDF(strOpen) {
            open(strOpen, "", 'height=365,width=810,left=180,top=150,resizable=yes,location=no,scrollbars=yes,toolbar=no,status=no');
            return false;
        }
        function funClosePopUp(strOption) {
            document.getElementById(strOption).style.display = "none";
        }
        function popUp(strOption) {
            document.getElementById(strOption).style.display = "block";
            return false;
        }
        function funOpenLocation() {
            document.getElementById('<%=divItems.ClientID%>').style.display = "block";
            return false;
        }
        function funCloseLoc() {
            document.getElementById('<%=divItems.ClientID%>').style.display = "none";
            document.getElementById('<%=lblMsg.ClientID%>').innerHTML = "";
        }

        function funOpenDiscount() {
            document.getElementById('<%=divDiscount.ClientID%>').style.display = "block";
            var strDiscount = document.getElementById('<%= txtDiscount.ClientID%>').value;
            if (window.document.getElementById('<%=rblstDiscountType.ClientID%>_0').checked == true) {
                document.getElementById('<%= txtDiscountVal.ClientID%>').value = strDiscount.replace("%", "").trim();
            }
            else {
                document.getElementById('<%= txtDiscountVal.ClientID%>').value = strDiscount;
            }
            document.getElementById('<%= txtDiscountVal.ClientID%>').focus();
            return false;
        }

        function funCloseDiscount() {
            document.getElementById('<%=divDiscount.ClientID%>').style.display = "none";
        }

        function funCalParcentageDiscount(source, arguments) {
            if (window.document.getElementById('<%=rblstDiscountType.ClientID%>_0').checked == true && document.getElementById('<%= txtDiscountVal.ClientID%>').value >= 100) {
                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;
            }
        }

        function funCloseProcessLoc() {
            document.getElementById('<%=divSalesProcess.ClientID%>').style.display = "none";
        }

        function funClosePopUp(strOption) {
            document.getElementById(strOption).style.display = "none";
        }

        function popUp(strOption) {
            document.getElementById(strOption).style.display = "block";
            return false;
        }

        var browserWidth = 0, browserHeight = 0;
        browserwidth2 = (document.documentElement.clientWidth - 850) / 2;
        var hdn = document.getElementById('<%=hdnLeft.ClientID%>');
        hdn.value = browserwidth2;

        function funCheckShippingWarehouseCode(source, args) {
            if (document.getElementById('<%=dlShpWarehouse.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }
        function funCheckOderType(source, args) {
            if (document.getElementById('<%=dlstOderType.ClientID%>').selectedIndex == 0) {
                args.IsValid = false;
            }
        }

        function openPopup(strOpen) {
            open(strOpen, "AddItemProcess", 'height=365,width=840,left=180,top=150,resizable=no,location=no,scrollbars=yes,toolbar=no,status=no');
            return false;
        }

        function funSelectReason(source, arguments) {
            if (window.document.getElementById('<%=dlReason.ClientID%>').value != "0") {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }

        function funClosePriceRangeLoc() {
            document.getElementById('<%=divPriceRnge.ClientID%>').style.display = "none";
        }

        function getSelectedRadio() {
            var selectedRadio = getRadioValue("rbSuggestedPrice", false);
            document.getElementById('<%=hdnSelectedId.ClientID%>').value = selectedRadio;
            if (selectedRadio == 'non') {
                alert("<%=pleaseSelectAChoice%>");
            }
            else {
                document.getElementById('<%=divPriceRnge.ClientID%>').style.display = "none";
            }
        }

        function setSuggestedPriceRadio() {
            alert(document.getElementById('<%=hdnSelectedId.ClientID%>').value);
            setRadioValue("rbSuggestedPrice", document.getElementById('<%=hdnSelectedId.ClientID%>').value);
        }

        function getRadioValue(RadioGrpName, isParent) {
            var elements = null;
            if (isParent) {
                elements = parent.document.getElementsByName(RadioGrpName);
            } else {
                elements = document.getElementsByName(RadioGrpName);
            }
            var i = 0;
            while (i < elements.length) {
                if ($(elements[i]).attr("checked")) {
                    return $(elements[i]).attr("value")
                }
                i++;
            }
            return 'non';
        }

        function setRadioValue(RadioGrpName, value) {
            var elements = document.getElementsByName(RadioGrpName);
            var i = 0;
            while (i < elements.length) {
                if ($(elements[i]).attr("value") == value) {
                    $(elements[i]).attr("checked", true);
                } else {
                    $(elements[i]).attr("checked", false);
                }
                i++;
            }
        }

     
    </script>
</asp:Content>

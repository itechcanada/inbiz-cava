﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;

using Trirand.Web.UI.WebControls;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.Library.DataAccess.MySql;

public partial class Sales_mdAddDiscountToOrder : BasePage
{   
    Orders _ord = new Orders();

    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!IsPostBack)
        {
            this.SetOrderTotal();
        }
        txtDiscount.Focus();
    }    

    private int OrderID
    {
        get {
            int oid = 0;
            int.TryParse(Request.QueryString["oid"], out oid);
            return oid;
        }
    }    

    protected void btnApply_Click(object sender, EventArgs e)
    {
        double amt = 0;        
        if (!double.TryParse(txtDiscount.Text, out amt))
        {
            MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidAmount);
            return;
        }       
        if (this.OrderID > 0)
        {
            _ord.ApplyDiscount(this.OrderID, amt, rblstDiscountType.SelectedValue);

            //To Do to Add Action Log            
            new OrderActionLog().AddLog(null, CurrentUser.UserID, this.OrderID, "Discount applied", string.Format("Discount of {0} applied to entire Sales Order!", amt));
        }
        SalesCartHelper.SetOrderDiscount(amt, rblstDiscountType.SelectedValue);

        string script = string.Format("parent.reloadGrid();");
        Globals.RegisterCloseDialogScript(Page, script, true);
    }

    private void SetOrderTotal()
    {       
        _ord.PopulateObject(this.OrderID);
        txtDiscount.Text = _ord.OrdDiscount.ToString();
        rblstDiscountType.SelectedValue = _ord.OrdDiscountType == "A" ? "A" : "P";

        TotalSummary lTotal = SalesCartHelper.GetOrderTotal();
        double actualTotal = lTotal.GrandTotal + lTotal.AdditionalDiscount;
        //valOrderTotal.Text = string.Format("{0:F}",  BusinessUtility.GetCurrencyString(actualTotal,Globals.CurrentCultureName));
        //valItemTotal.Text = string.Format("{0:F}",BusinessUtility.GetCurrencyString( lTotal.ItemSubTotal,Globals.CurrentCultureName));
        //valEffectedOrderTotal.Text = string.Format("{0:F}",BusinessUtility.GetCurrencyString( lTotal.GrandTotal,Globals.CurrentCultureName));

        valOrderTotal.Text = string.Format("{0:F}",CurrencyFormat.GetCompanyCurrencyFormat(actualTotal, _ord.OrdCurrencyCode));
        valItemTotal.Text = string.Format("{0:F}", CurrencyFormat.GetCompanyCurrencyFormat(lTotal.ItemSubTotal, _ord.OrdCurrencyCode));
        valEffectedOrderTotal.Text = string.Format("{0:F}", CurrencyFormat.GetCompanyCurrencyFormat(lTotal.GrandTotal, _ord.OrdCurrencyCode));
    }
}
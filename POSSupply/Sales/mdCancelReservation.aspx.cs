﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

using MySql.Data.MySqlClient;
using iTECH.Library.DataAccess.MySql;
using iTECH.InbizERP.BusinessLogic;
using iTECH.Library.Utilities;
using iTECH.WebControls;
using iTECH.PaymentProvider;

public partial class Sales_mdCancelReservation : BasePage
{        
    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!IsPostBack)
        {            
            FillForm();
        }
    }

    private void FillForm()
    {
        Partners part = new Partners();
        Orders ord = new Orders();
        ord.PopulateObject(this.OrderID);

        int partID = this.GuestID;
        if (partID <= 0)
        {
            partID = ord.OrdCustID;
        }

        part.PopulateObject(partID);
        valGuest.Text = part.PartnerLongName;

        

        //Get Paid Amount
        TotalSummary lTotal = CalculationHelper.GetOrderTotal(this.OrderID);

        if (this.OrderItemID > 0)
        {
            Product prd = this.GetAdminFeeProduct(null);
            double amtToRefundOrCredit = new OrderItems().GetItemSubTotal(this.OrderItemID);
            if (amtToRefundOrCredit > lTotal.TotalAmountReceived)
            {
                txtRefundAmount.Text = string.Format("{0:F}", lTotal.TotalAmountReceived);                
            }
            else
            {
                txtRefundAmount.Text = string.Format("{0:F}", amtToRefundOrCredit);
            }
            txtAdminFee.Text = string.Format("{0:F}", prd.PrdEndUserSalesPrice);
        }
        else
        {
            txtRefundAmount.Text = string.Format("{0:F}", lTotal.TotalAmountReceived);
        }

        valTotalPaid.Text = string.Format("{0} {1:F}", lTotal.CurrencyCode, lTotal.TotalAmountReceived);
    }
    
    private int ReservationItemID
    {
        get
        {
            int rsvItmID = 0;
            int.TryParse(Request.QueryString["ritmid"], out rsvItmID);
            return rsvItmID;
        }
    }

    private int GuestID
    {
        get
        {
            int gid = 0;
            int.TryParse(Request.QueryString["gid"], out gid);
            return gid;
        }
    }

    private int OrderID
    {
        get {
            int oid = 0;
            int.TryParse(Request.QueryString["oid"], out oid);
            return oid;
        }
    }

    private int OrderItemID
    {
        get
        {
            int oid = 0;
            int.TryParse(Request.QueryString["oitmid"], out oid);
            return oid;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {    //Get Paid Amount
            TotalSummary lTotal = CalculationHelper.GetOrderTotal(this.OrderID);
            double amttoRefund = 0.00D;
            double adminFee = 0.00D;
            if (!double.TryParse(txtRefundAmount.Text, out amttoRefund))
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidAmountToBeRefund);
                return;
            }
            if (!double.TryParse(txtAdminFee.Text, out adminFee))
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgInvalidAdminFeeEntered);
                return;
            }
            if (amttoRefund > lTotal.TotalAmountReceived)
            {
                MessageState.SetGlobalMessage(MessageType.Failure, Resources.Resource.msgRefunAmtShouldBeLessThenOrEqualReceivedAmount);
                return;
            }

            if (this.OrderItemID > 0)
            {
                OrderItems oi = new OrderItems();

                this.AddAdminFee();

                //Update Order Items & Set As canceled
                oi.CancelReservationItem(this.OrderItemID);

                //Update Cart Item
                this.UpdateCartItem();
            }

            //Again Traverse Order Total
            lTotal = CalculationHelper.GetOrderTotal(this.OrderID);

            if (lTotal.OutstandingAmount < 0)
            {
                Response.Redirect(string.Format("~/AccountsReceivable/mdReceive.aspx?oid={0}&rcvType={1}&amtToRefund={2}&adminFee={3}", this.OrderID, "refund", (-1.00D * lTotal.OutstandingAmount), adminFee));
            }
            else
            {
                Globals.RegisterCloseDialogScript(Page, "parent.reloadGrid(); ", true);
            }
        }     
    }   

    private void UpdateCartItem()
    {
        SalesCartHelper.SetItemAsCanceled(this.OrderItemID);
    }

    private Product GetAdminFeeProduct(DbHelper dbHelp)
    {
        bool closeConnection = false;
        if (dbHelp == null)
        {
            dbHelp = new DbHelper();
            closeConnection = true;
        }
        try
        {
            Product prd = new Product();
            prd.PopulateAdminFeeProduct(dbHelp);
            return prd;
        }
        catch
        {
            throw;
        }
        finally
        {
            if (closeConnection)
            {
                dbHelp.CloseDatabaseConnection();
            }
        }
    }

    private void AddAdminFee()
    {
        DbHelper dbHelp = new DbHelper(true);
        try
        {
            double adminFee = 0.00D;
            double.TryParse(txtAdminFee.Text, out adminFee);
            Orders ord = new Orders();
            OrderItems oi = new OrderItems();            
            ord.PopulateObject(dbHelp, this.OrderID);
            Product prd = this.GetAdminFeeProduct(dbHelp);
            if (prd.ProductID > 0)
            {
                oi.OrderItemDesc = prd.PrdName;
                oi.OrdGuestID = ord.OrdCustID;
                oi.OrdID = ord.OrdID;
                oi.OrdIsItemCanceled = false;
                oi.OrdProductDiscount = 0;
                oi.OrdProductDiscountType = "P";
                oi.OrdProductID = prd.ProductID;
                oi.OrdProductQty = 1;
                oi.OrdProductUnitPrice = adminFee;
                oi.OrdReservationItemID = 0;
                oi.Insert(dbHelp);
            }
        }
        finally
        {
            dbHelp.CloseDatabaseConnection();
        }
    }
}